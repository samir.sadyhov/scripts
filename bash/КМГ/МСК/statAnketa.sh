#!/bin/bash

synergyUser="Administrator"
synergyPass="\$ystemAdm1n"
synergyURL="http://127.0.0.1:8080/Synergy"

tmpSQL1="/tmp/statSQL1.msk"
tmpSQL2="/tmp/statSQL2.msk"
tmpSQL3="/tmp/statSQL3.msk"
tmpfile="/tmp/statData.msk"

# Реестр \98 Настройки компаний\
regCompany="d9b4245b-7be7-4a6d-b085-0b7943653a96"

# Реестр \01. Анкета\
regAnketa="6b9edece-ad7f-4478-aba4-80d456e7fea4"

# Реестр \Статистика по анкете "Уровень социальной фрустрированности респондента"\
regStat="d46de425-f343-4b1c-a1ac-398a750262b8"
formStat="d65c0a79-5245-4b26-aa26-395d0d21456f"

vopros=(
  ''
  '1. Своим образованием'
  '2. Взаимоотношениями с коллегами по работе'
  '3. Взаимоотношениями с руководством на работе'
  '4. Содержанием своей работы в целом'
  '5. Условиями профессиональной деятельности (напр., оснащенностью рабочего места, наличием инструментов и т.д.)'
  '6. Своим положением в обществе '
  '7. Материальным положением'
  '8. Жилищно-бытовыми условиями'
  '9. Отношениями с супругом(ой). '
  '10. Отношениями с детьми'
  '11. Отношениями с родителями'
  '12. Обстановкой в обществе (государстве)	'
  '13. Отношениями с друзьями, ближайшими знакомыми'
  '14. Сферой услуг и бытового обслуживания'
  '15. Сферой медицинского обслуживания'
  '16. Проведением свободного времени'
  '17. Возможностью проводить отпуск'
  '18. Возможностью выбора места работы'
  '19. Своим образом жизни в целом'
)

# ищем новые или измененные записи в реестре \01. Анкета\
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT company.cmp_data, company.cmp_key, DATE_FORMAT(NOW(), '%d.%m.%Y') AS dateData, NOW() AS dateKey
FROM registry_documents rg
JOIN asf_data_index company ON company.uuid=rg.asfDataID AND company.cmp_id='company'
JOIN asf_data ON asf_data.uuid=rg.asfDataID
JOIN (SELECT rg.documentid, parent.cmp_data, parent.cmp_key
FROM registry_documents rg JOIN asf_data_index parent ON parent.uuid=rg.asfDataID AND parent.cmp_id='parent'
WHERE rg.registryID = '$regCompany' AND rg.deleted IS NULL AND rg.active='Y') parent ON parent.documentid=company.cmp_key
WHERE registryID = '$regAnketa' AND rg.deleted IS NULL AND rg.active='Y' AND DATE(asf_data.modified)=DATE(NOW())
GROUP BY company.cmp_key;" > $tmpSQL1

# если есть новые записи то пересчитать и записать результат в реестр \Статистика по анкете "Уровень социальной фрустрированности респондента"\
if [ -s $tmpSQL1 ];
then

# Количество строк в файле tmpSQL1
countStr=`cat $tmpSQL1 | wc -l`

for i in `seq 2 $countStr`;
do
companyName=`cat $tmpSQL1 | sed -n $i'p' | cut -f1`
companyKey=`cat $tmpSQL1 | sed -n $i'p' | cut -f2`
dateData=`cat $tmpSQL1 | sed -n $i'p' | cut -f3`
dateKey=`cat $tmpSQL1 | sed -n $i'p' | cut -f4`
allCompanyKey+="'"`cat $tmpSQL1 | sed -n $i'p' | cut -f2`"',"

# пересчет
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT
ROUND(v1o1/allAnket*100, 1) AS v1o1, ROUND(v1o2/allAnket*100, 1) AS v1o2, ROUND(v1o3/allAnket*100, 1) AS v1o3, ROUND(v1o4/allAnket*100, 1) AS v1o4,
100-(ROUND(v1o1/allAnket*100, 1)+ROUND(v1o2/allAnket*100, 1)+ROUND(v1o3/allAnket*100, 1)+ROUND(v1o4/allAnket*100, 1)) AS v1o5,
ROUND(v2o1/allAnket*100, 1) AS v2o1, ROUND(v2o2/allAnket*100, 1) AS v2o2, ROUND(v2o3/allAnket*100, 1) AS v2o3, ROUND(v2o4/allAnket*100, 1) AS v2o4,
100-(ROUND(v2o1/allAnket*100, 1)+ROUND(v2o2/allAnket*100, 1)+ROUND(v2o3/allAnket*100, 1)+ROUND(v2o4/allAnket*100, 1)) AS v2o5,
ROUND(v3o1/allAnket*100, 1) AS v3o1, ROUND(v3o2/allAnket*100, 1) AS v3o2, ROUND(v3o3/allAnket*100, 1) AS v3o3, ROUND(v3o4/allAnket*100, 1) AS v3o4,
100-(ROUND(v3o1/allAnket*100, 1)+ROUND(v3o2/allAnket*100, 1)+ROUND(v3o3/allAnket*100, 1)+ROUND(v3o4/allAnket*100, 1)) AS v3o5,
ROUND(v4o1/allAnket*100, 1) AS v4o1, ROUND(v4o2/allAnket*100, 1) AS v4o2, ROUND(v4o3/allAnket*100, 1) AS v4o3, ROUND(v4o4/allAnket*100, 1) AS v4o4,
100-(ROUND(v4o1/allAnket*100, 1)+ROUND(v4o2/allAnket*100, 1)+ROUND(v4o3/allAnket*100, 1)+ROUND(v4o4/allAnket*100, 1)) AS v4o5,
ROUND(v5o1/allAnket*100, 1) AS v5o1, ROUND(v5o2/allAnket*100, 1) AS v5o2, ROUND(v5o3/allAnket*100, 1) AS v5o3, ROUND(v5o4/allAnket*100, 1) AS v5o4,
100-(ROUND(v5o1/allAnket*100, 1)+ROUND(v5o2/allAnket*100, 1)+ROUND(v5o3/allAnket*100, 1)+ROUND(v5o4/allAnket*100, 1)) AS v5o5,
ROUND(v6o1/allAnket*100, 1) AS v6o1, ROUND(v6o2/allAnket*100, 1) AS v6o2, ROUND(v6o3/allAnket*100, 1) AS v6o3, ROUND(v6o4/allAnket*100, 1) AS v6o4,
100-(ROUND(v6o1/allAnket*100, 1)+ROUND(v6o2/allAnket*100, 1)+ROUND(v6o3/allAnket*100, 1)+ROUND(v6o4/allAnket*100, 1)) AS v6o5,
ROUND(v7o1/allAnket*100, 1) AS v7o1, ROUND(v7o2/allAnket*100, 1) AS v7o2, ROUND(v7o3/allAnket*100, 1) AS v7o3, ROUND(v7o4/allAnket*100, 1) AS v7o4,
100-(ROUND(v7o1/allAnket*100, 1)+ROUND(v7o2/allAnket*100, 1)+ROUND(v7o3/allAnket*100, 1)+ROUND(v7o4/allAnket*100, 1)) AS v7o5,
ROUND(v8o1/allAnket*100, 1) AS v8o1, ROUND(v8o2/allAnket*100, 1) AS v8o2, ROUND(v8o3/allAnket*100, 1) AS v8o3, ROUND(v8o4/allAnket*100, 1) AS v8o4,
100-(ROUND(v8o1/allAnket*100, 1)+ROUND(v8o2/allAnket*100, 1)+ROUND(v8o3/allAnket*100, 1)+ROUND(v8o4/allAnket*100, 1)) AS v8o5,
ROUND(v9o1/allAnket*100, 1) AS v9o1, ROUND(v9o2/allAnket*100, 1) AS v9o2, ROUND(v9o3/allAnket*100, 1) AS v9o3, ROUND(v9o4/allAnket*100, 1) AS v9o4,
100-(ROUND(v9o1/allAnket*100, 1)+ROUND(v9o2/allAnket*100, 1)+ROUND(v9o3/allAnket*100, 1)+ROUND(v9o4/allAnket*100, 1)) AS v9o5,
ROUND(v10o1/allAnket*100, 1) AS v10o1, ROUND(v10o2/allAnket*100, 1) AS v10o2, ROUND(v10o3/allAnket*100, 1) AS v10o3, ROUND(v10o4/allAnket*100, 1) AS v10o4,
100-(ROUND(v10o1/allAnket*100, 1)+ROUND(v10o2/allAnket*100, 1)+ROUND(v10o3/allAnket*100, 1)+ROUND(v10o4/allAnket*100, 1)) AS v10o5,
ROUND(v11o1/allAnket*100, 1) AS v11o1, ROUND(v11o2/allAnket*100, 1) AS v11o2, ROUND(v11o3/allAnket*100, 1) AS v11o3, ROUND(v11o4/allAnket*100, 1) AS v11o4,
100-(ROUND(v11o1/allAnket*100, 1)+ROUND(v11o2/allAnket*100, 1)+ROUND(v11o3/allAnket*100, 1)+ROUND(v11o4/allAnket*100, 1)) AS v11o5,
ROUND(v12o1/allAnket*100, 1) AS v12o1, ROUND(v12o2/allAnket*100, 1) AS v12o2, ROUND(v12o3/allAnket*100, 1) AS v12o3, ROUND(v12o4/allAnket*100, 1) AS v12o4,
100-(ROUND(v12o1/allAnket*100, 1)+ROUND(v12o2/allAnket*100, 1)+ROUND(v12o3/allAnket*100, 1)+ROUND(v12o4/allAnket*100, 1)) AS v12o5,
ROUND(v13o1/allAnket*100, 1) AS v13o1, ROUND(v13o2/allAnket*100, 1) AS v13o2, ROUND(v13o3/allAnket*100, 1) AS v13o3, ROUND(v13o4/allAnket*100, 1) AS v13o4,
100-(ROUND(v13o1/allAnket*100, 1)+ROUND(v13o2/allAnket*100, 1)+ROUND(v13o3/allAnket*100, 1)+ROUND(v13o4/allAnket*100, 1)) AS v13o5,
ROUND(v14o1/allAnket*100, 1) AS v14o1, ROUND(v14o2/allAnket*100, 1) AS v14o2, ROUND(v14o3/allAnket*100, 1) AS v14o3, ROUND(v14o4/allAnket*100, 1) AS v14o4,
100-(ROUND(v14o1/allAnket*100, 1)+ROUND(v14o2/allAnket*100, 1)+ROUND(v14o3/allAnket*100, 1)+ROUND(v14o4/allAnket*100, 1)) AS v14o5,
ROUND(v15o1/allAnket*100, 1) AS v15o1, ROUND(v15o2/allAnket*100, 1) AS v15o2, ROUND(v15o3/allAnket*100, 1) AS v15o3, ROUND(v15o4/allAnket*100, 1) AS v15o4,
100-(ROUND(v15o1/allAnket*100, 1)+ROUND(v15o2/allAnket*100, 1)+ROUND(v15o3/allAnket*100, 1)+ROUND(v15o4/allAnket*100, 1)) AS v15o5,
ROUND(v16o1/allAnket*100, 1) AS v16o1, ROUND(v16o2/allAnket*100, 1) AS v16o2, ROUND(v16o3/allAnket*100, 1) AS v16o3, ROUND(v16o4/allAnket*100, 1) AS v16o4,
100-(ROUND(v16o1/allAnket*100, 1)+ROUND(v16o2/allAnket*100, 1)+ROUND(v16o3/allAnket*100, 1)+ROUND(v16o4/allAnket*100, 1)) AS v16o5,
ROUND(v17o1/allAnket*100, 1) AS v17o1, ROUND(v17o2/allAnket*100, 1) AS v17o2, ROUND(v17o3/allAnket*100, 1) AS v17o3, ROUND(v17o4/allAnket*100, 1) AS v17o4,
100-(ROUND(v17o1/allAnket*100, 1)+ROUND(v17o2/allAnket*100, 1)+ROUND(v17o3/allAnket*100, 1)+ROUND(v17o4/allAnket*100, 1)) AS v17o5,
ROUND(v18o1/allAnket*100, 1) AS v18o1, ROUND(v18o2/allAnket*100, 1) AS v18o2, ROUND(v18o3/allAnket*100, 1) AS v18o3, ROUND(v18o4/allAnket*100, 1) AS v18o4,
100-(ROUND(v18o1/allAnket*100, 1)+ROUND(v18o2/allAnket*100, 1)+ROUND(v18o3/allAnket*100, 1)+ROUND(v18o4/allAnket*100, 1)) AS v18o5,
ROUND(v19o1/allAnket*100, 1) AS v19o1, ROUND(v19o2/allAnket*100, 1) AS v19o2, ROUND(v19o3/allAnket*100, 1) AS v19o3, ROUND(v19o4/allAnket*100, 1) AS v19o4,
100-(ROUND(v19o1/allAnket*100, 1)+ROUND(v19o2/allAnket*100, 1)+ROUND(v19o3/allAnket*100, 1)+ROUND(v19o4/allAnket*100, 1)) AS v19o5

FROM (SELECT COUNT(DISTINCT rg.asfDataID) AS allAnket,
SUM(IF(vopros.cmp_id='frustrated_otvet1' AND vopros.cmp_key='4',1,0)) AS v1o1, SUM(IF(vopros.cmp_id='frustrated_otvet1' AND vopros.cmp_key='3',1,0)) AS v1o2,
SUM(IF(vopros.cmp_id='frustrated_otvet1' AND vopros.cmp_key='2',1,0)) AS v1o3, SUM(IF(vopros.cmp_id='frustrated_otvet1' AND vopros.cmp_key='1',1,0)) AS v1o4,
SUM(IF(vopros.cmp_id='frustrated_otvet2' AND vopros.cmp_key='4',1,0)) AS v2o1, SUM(IF(vopros.cmp_id='frustrated_otvet2' AND vopros.cmp_key='3',1,0)) AS v2o2,
SUM(IF(vopros.cmp_id='frustrated_otvet2' AND vopros.cmp_key='2',1,0)) AS v2o3, SUM(IF(vopros.cmp_id='frustrated_otvet2' AND vopros.cmp_key='1',1,0)) AS v2o4,
SUM(IF(vopros.cmp_id='frustrated_otvet3' AND vopros.cmp_key='4',1,0)) AS v3o1, SUM(IF(vopros.cmp_id='frustrated_otvet3' AND vopros.cmp_key='3',1,0)) AS v3o2,
SUM(IF(vopros.cmp_id='frustrated_otvet3' AND vopros.cmp_key='2',1,0)) AS v3o3, SUM(IF(vopros.cmp_id='frustrated_otvet3' AND vopros.cmp_key='1',1,0)) AS v3o4,
SUM(IF(vopros.cmp_id='frustrated_otvet4' AND vopros.cmp_key='4',1,0)) AS v4o1, SUM(IF(vopros.cmp_id='frustrated_otvet4' AND vopros.cmp_key='3',1,0)) AS v4o2,
SUM(IF(vopros.cmp_id='frustrated_otvet4' AND vopros.cmp_key='2',1,0)) AS v4o3, SUM(IF(vopros.cmp_id='frustrated_otvet4' AND vopros.cmp_key='1',1,0)) AS v4o4,
SUM(IF(vopros.cmp_id='frustrated_otvet5' AND vopros.cmp_key='4',1,0)) AS v5o1, SUM(IF(vopros.cmp_id='frustrated_otvet5' AND vopros.cmp_key='3',1,0)) AS v5o2,
SUM(IF(vopros.cmp_id='frustrated_otvet5' AND vopros.cmp_key='2',1,0)) AS v5o3, SUM(IF(vopros.cmp_id='frustrated_otvet5' AND vopros.cmp_key='1',1,0)) AS v5o4,
SUM(IF(vopros.cmp_id='frustrated_otvet6' AND vopros.cmp_key='4',1,0)) AS v6o1, SUM(IF(vopros.cmp_id='frustrated_otvet6' AND vopros.cmp_key='3',1,0)) AS v6o2,
SUM(IF(vopros.cmp_id='frustrated_otvet6' AND vopros.cmp_key='2',1,0)) AS v6o3, SUM(IF(vopros.cmp_id='frustrated_otvet6' AND vopros.cmp_key='1',1,0)) AS v6o4,
SUM(IF(vopros.cmp_id='frustrated_otvet7' AND vopros.cmp_key='4',1,0)) AS v7o1, SUM(IF(vopros.cmp_id='frustrated_otvet7' AND vopros.cmp_key='3',1,0)) AS v7o2,
SUM(IF(vopros.cmp_id='frustrated_otvet7' AND vopros.cmp_key='2',1,0)) AS v7o3, SUM(IF(vopros.cmp_id='frustrated_otvet7' AND vopros.cmp_key='1',1,0)) AS v7o4,
SUM(IF(vopros.cmp_id='frustrated_otvet8' AND vopros.cmp_key='4',1,0)) AS v8o1, SUM(IF(vopros.cmp_id='frustrated_otvet8' AND vopros.cmp_key='3',1,0)) AS v8o2,
SUM(IF(vopros.cmp_id='frustrated_otvet8' AND vopros.cmp_key='2',1,0)) AS v8o3, SUM(IF(vopros.cmp_id='frustrated_otvet8' AND vopros.cmp_key='1',1,0)) AS v8o4,
SUM(IF(vopros.cmp_id='frustrated_otvet9' AND vopros.cmp_key='4',1,0)) AS v9o1, SUM(IF(vopros.cmp_id='frustrated_otvet9' AND vopros.cmp_key='3',1,0)) AS v9o2,
SUM(IF(vopros.cmp_id='frustrated_otvet9' AND vopros.cmp_key='2',1,0)) AS v9o3, SUM(IF(vopros.cmp_id='frustrated_otvet9' AND vopros.cmp_key='1',1,0)) AS v9o4,
SUM(IF(vopros.cmp_id='frustrated_otvet10' AND vopros.cmp_key='4',1,0)) AS v10o1, SUM(IF(vopros.cmp_id='frustrated_otvet10' AND vopros.cmp_key='3',1,0)) AS v10o2,
SUM(IF(vopros.cmp_id='frustrated_otvet10' AND vopros.cmp_key='2',1,0)) AS v10o3, SUM(IF(vopros.cmp_id='frustrated_otvet10' AND vopros.cmp_key='1',1,0)) AS v10o4,
SUM(IF(vopros.cmp_id='frustrated_otvet11' AND vopros.cmp_key='4',1,0)) AS v11o1, SUM(IF(vopros.cmp_id='frustrated_otvet11' AND vopros.cmp_key='3',1,0)) AS v11o2,
SUM(IF(vopros.cmp_id='frustrated_otvet11' AND vopros.cmp_key='2',1,0)) AS v11o3, SUM(IF(vopros.cmp_id='frustrated_otvet11' AND vopros.cmp_key='1',1,0)) AS v11o4,
SUM(IF(vopros.cmp_id='frustrated_otvet12' AND vopros.cmp_key='4',1,0)) AS v12o1, SUM(IF(vopros.cmp_id='frustrated_otvet12' AND vopros.cmp_key='3',1,0)) AS v12o2,
SUM(IF(vopros.cmp_id='frustrated_otvet12' AND vopros.cmp_key='2',1,0)) AS v12o3, SUM(IF(vopros.cmp_id='frustrated_otvet12' AND vopros.cmp_key='1',1,0)) AS v12o4,
SUM(IF(vopros.cmp_id='frustrated_otvet13' AND vopros.cmp_key='4',1,0)) AS v13o1, SUM(IF(vopros.cmp_id='frustrated_otvet13' AND vopros.cmp_key='3',1,0)) AS v13o2,
SUM(IF(vopros.cmp_id='frustrated_otvet13' AND vopros.cmp_key='2',1,0)) AS v13o3, SUM(IF(vopros.cmp_id='frustrated_otvet13' AND vopros.cmp_key='1',1,0)) AS v13o4,
SUM(IF(vopros.cmp_id='frustrated_otvet14' AND vopros.cmp_key='4',1,0)) AS v14o1, SUM(IF(vopros.cmp_id='frustrated_otvet14' AND vopros.cmp_key='3',1,0)) AS v14o2,
SUM(IF(vopros.cmp_id='frustrated_otvet14' AND vopros.cmp_key='2',1,0)) AS v14o3, SUM(IF(vopros.cmp_id='frustrated_otvet14' AND vopros.cmp_key='1',1,0)) AS v14o4,
SUM(IF(vopros.cmp_id='frustrated_otvet15' AND vopros.cmp_key='4',1,0)) AS v15o1, SUM(IF(vopros.cmp_id='frustrated_otvet15' AND vopros.cmp_key='3',1,0)) AS v15o2,
SUM(IF(vopros.cmp_id='frustrated_otvet15' AND vopros.cmp_key='2',1,0)) AS v15o3, SUM(IF(vopros.cmp_id='frustrated_otvet15' AND vopros.cmp_key='1',1,0)) AS v15o4,
SUM(IF(vopros.cmp_id='frustrated_otvet16' AND vopros.cmp_key='4',1,0)) AS v16o1, SUM(IF(vopros.cmp_id='frustrated_otvet16' AND vopros.cmp_key='3',1,0)) AS v16o2,
SUM(IF(vopros.cmp_id='frustrated_otvet16' AND vopros.cmp_key='2',1,0)) AS v16o3, SUM(IF(vopros.cmp_id='frustrated_otvet16' AND vopros.cmp_key='1',1,0)) AS v16o4,
SUM(IF(vopros.cmp_id='frustrated_otvet17' AND vopros.cmp_key='4',1,0)) AS v17o1, SUM(IF(vopros.cmp_id='frustrated_otvet17' AND vopros.cmp_key='3',1,0)) AS v17o2,
SUM(IF(vopros.cmp_id='frustrated_otvet17' AND vopros.cmp_key='2',1,0)) AS v17o3, SUM(IF(vopros.cmp_id='frustrated_otvet17' AND vopros.cmp_key='1',1,0)) AS v17o4,
SUM(IF(vopros.cmp_id='frustrated_otvet18' AND vopros.cmp_key='4',1,0)) AS v18o1, SUM(IF(vopros.cmp_id='frustrated_otvet18' AND vopros.cmp_key='3',1,0)) AS v18o2,
SUM(IF(vopros.cmp_id='frustrated_otvet18' AND vopros.cmp_key='2',1,0)) AS v18o3, SUM(IF(vopros.cmp_id='frustrated_otvet18' AND vopros.cmp_key='1',1,0)) AS v18o4,
SUM(IF(vopros.cmp_id='frustrated_otvet19' AND vopros.cmp_key='4',1,0)) AS v19o1, SUM(IF(vopros.cmp_id='frustrated_otvet19' AND vopros.cmp_key='3',1,0)) AS v19o2,
SUM(IF(vopros.cmp_id='frustrated_otvet19' AND vopros.cmp_key='2',1,0)) AS v19o3, SUM(IF(vopros.cmp_id='frustrated_otvet19' AND vopros.cmp_key='1',1,0)) AS v19o4
FROM registry_documents rg
JOIN asf_data_index company ON company.uuid=rg.asfDataID AND company.cmp_id='company'
JOIN asf_data_index dateAnk ON dateAnk.uuid=rg.asfDataID AND dateAnk.cmp_id='date'
LEFT JOIN asf_data_index vopros ON vopros.uuid=rg.asfDataID AND vopros.cmp_id LIKE 'frustrated_otvet%'
WHERE rg.registryID='$regAnketa' AND company.cmp_key='$companyKey'
AND rg.deleted IS NULL AND rg.active='Y' AND YEAR(dateAnk.cmp_key)=YEAR(NOW()) ) t;" > $tmpSQL2

# присвоение переменных
v1o1=`cat $tmpSQL2 | sed -n 2p | cut -f1`
v1o2=`cat $tmpSQL2 | sed -n 2p | cut -f2`
v1o3=`cat $tmpSQL2 | sed -n 2p | cut -f3`
v1o4=`cat $tmpSQL2 | sed -n 2p | cut -f4`
v1o5=`cat $tmpSQL2 | sed -n 2p | cut -f5`
v2o1=`cat $tmpSQL2 | sed -n 2p | cut -f6`
v2o2=`cat $tmpSQL2 | sed -n 2p | cut -f7`
v2o3=`cat $tmpSQL2 | sed -n 2p | cut -f8`
v2o4=`cat $tmpSQL2 | sed -n 2p | cut -f9`
v2o5=`cat $tmpSQL2 | sed -n 2p | cut -f10`
v3o1=`cat $tmpSQL2 | sed -n 2p | cut -f11`
v3o2=`cat $tmpSQL2 | sed -n 2p | cut -f12`
v3o3=`cat $tmpSQL2 | sed -n 2p | cut -f13`
v3o4=`cat $tmpSQL2 | sed -n 2p | cut -f14`
v3o5=`cat $tmpSQL2 | sed -n 2p | cut -f15`
v4o1=`cat $tmpSQL2 | sed -n 2p | cut -f16`
v4o2=`cat $tmpSQL2 | sed -n 2p | cut -f17`
v4o3=`cat $tmpSQL2 | sed -n 2p | cut -f18`
v4o4=`cat $tmpSQL2 | sed -n 2p | cut -f19`
v4o5=`cat $tmpSQL2 | sed -n 2p | cut -f20`
v5o1=`cat $tmpSQL2 | sed -n 2p | cut -f21`
v5o2=`cat $tmpSQL2 | sed -n 2p | cut -f22`
v5o3=`cat $tmpSQL2 | sed -n 2p | cut -f23`
v5o4=`cat $tmpSQL2 | sed -n 2p | cut -f24`
v5o5=`cat $tmpSQL2 | sed -n 2p | cut -f25`
v6o1=`cat $tmpSQL2 | sed -n 2p | cut -f26`
v6o2=`cat $tmpSQL2 | sed -n 2p | cut -f27`
v6o3=`cat $tmpSQL2 | sed -n 2p | cut -f28`
v6o4=`cat $tmpSQL2 | sed -n 2p | cut -f29`
v6o5=`cat $tmpSQL2 | sed -n 2p | cut -f30`
v7o1=`cat $tmpSQL2 | sed -n 2p | cut -f31`
v7o2=`cat $tmpSQL2 | sed -n 2p | cut -f32`
v7o3=`cat $tmpSQL2 | sed -n 2p | cut -f33`
v7o4=`cat $tmpSQL2 | sed -n 2p | cut -f34`
v7o5=`cat $tmpSQL2 | sed -n 2p | cut -f35`
v8o1=`cat $tmpSQL2 | sed -n 2p | cut -f36`
v8o2=`cat $tmpSQL2 | sed -n 2p | cut -f37`
v8o3=`cat $tmpSQL2 | sed -n 2p | cut -f38`
v8o4=`cat $tmpSQL2 | sed -n 2p | cut -f39`
v8o5=`cat $tmpSQL2 | sed -n 2p | cut -f40`
v9o1=`cat $tmpSQL2 | sed -n 2p | cut -f41`
v9o2=`cat $tmpSQL2 | sed -n 2p | cut -f42`
v9o3=`cat $tmpSQL2 | sed -n 2p | cut -f43`
v9o4=`cat $tmpSQL2 | sed -n 2p | cut -f44`
v9o5=`cat $tmpSQL2 | sed -n 2p | cut -f45`
v10o1=`cat $tmpSQL2 | sed -n 2p | cut -f46`
v10o2=`cat $tmpSQL2 | sed -n 2p | cut -f47`
v10o3=`cat $tmpSQL2 | sed -n 2p | cut -f48`
v10o4=`cat $tmpSQL2 | sed -n 2p | cut -f49`
v10o5=`cat $tmpSQL2 | sed -n 2p | cut -f50`
v11o1=`cat $tmpSQL2 | sed -n 2p | cut -f51`
v11o2=`cat $tmpSQL2 | sed -n 2p | cut -f52`
v11o3=`cat $tmpSQL2 | sed -n 2p | cut -f53`
v11o4=`cat $tmpSQL2 | sed -n 2p | cut -f54`
v11o5=`cat $tmpSQL2 | sed -n 2p | cut -f55`
v12o1=`cat $tmpSQL2 | sed -n 2p | cut -f56`
v12o2=`cat $tmpSQL2 | sed -n 2p | cut -f57`
v12o3=`cat $tmpSQL2 | sed -n 2p | cut -f58`
v12o4=`cat $tmpSQL2 | sed -n 2p | cut -f59`
v12o5=`cat $tmpSQL2 | sed -n 2p | cut -f60`
v13o1=`cat $tmpSQL2 | sed -n 2p | cut -f61`
v13o2=`cat $tmpSQL2 | sed -n 2p | cut -f62`
v13o3=`cat $tmpSQL2 | sed -n 2p | cut -f63`
v13o4=`cat $tmpSQL2 | sed -n 2p | cut -f64`
v13o5=`cat $tmpSQL2 | sed -n 2p | cut -f65`
v14o1=`cat $tmpSQL2 | sed -n 2p | cut -f66`
v14o2=`cat $tmpSQL2 | sed -n 2p | cut -f67`
v14o3=`cat $tmpSQL2 | sed -n 2p | cut -f68`
v14o4=`cat $tmpSQL2 | sed -n 2p | cut -f69`
v14o5=`cat $tmpSQL2 | sed -n 2p | cut -f70`
v15o1=`cat $tmpSQL2 | sed -n 2p | cut -f71`
v15o2=`cat $tmpSQL2 | sed -n 2p | cut -f72`
v15o3=`cat $tmpSQL2 | sed -n 2p | cut -f73`
v15o4=`cat $tmpSQL2 | sed -n 2p | cut -f74`
v15o5=`cat $tmpSQL2 | sed -n 2p | cut -f75`
v16o1=`cat $tmpSQL2 | sed -n 2p | cut -f76`
v16o2=`cat $tmpSQL2 | sed -n 2p | cut -f77`
v16o3=`cat $tmpSQL2 | sed -n 2p | cut -f78`
v16o4=`cat $tmpSQL2 | sed -n 2p | cut -f79`
v16o5=`cat $tmpSQL2 | sed -n 2p | cut -f80`
v17o1=`cat $tmpSQL2 | sed -n 2p | cut -f81`
v17o2=`cat $tmpSQL2 | sed -n 2p | cut -f82`
v17o3=`cat $tmpSQL2 | sed -n 2p | cut -f83`
v17o4=`cat $tmpSQL2 | sed -n 2p | cut -f84`
v17o5=`cat $tmpSQL2 | sed -n 2p | cut -f85`
v18o1=`cat $tmpSQL2 | sed -n 2p | cut -f86`
v18o2=`cat $tmpSQL2 | sed -n 2p | cut -f87`
v18o3=`cat $tmpSQL2 | sed -n 2p | cut -f88`
v18o4=`cat $tmpSQL2 | sed -n 2p | cut -f89`
v18o5=`cat $tmpSQL2 | sed -n 2p | cut -f90`
v19o1=`cat $tmpSQL2 | sed -n 2p | cut -f91`
v19o2=`cat $tmpSQL2 | sed -n 2p | cut -f92`
v19o3=`cat $tmpSQL2 | sed -n 2p | cut -f93`
v19o4=`cat $tmpSQL2 | sed -n 2p | cut -f94`
v19o5=`cat $tmpSQL2 | sed -n 2p | cut -f95`

# формирование правильной строки data для дочерней компании
newData='KAKAKA"data":[{"id":"text","type":"label","label":"Результаты опроса работников","value":" "},{"id":"cmp-vour89","type":"label","label":" ","value":" "},{"id":"cmp-4gxmgl","type":"label","label":"Компания:","value":" "},{"id":"company","type":"reglink","value":"'$companyName'","key":"'$companyKey'","valueID":"'$companyKey'","username":"Admin Admin Admin","userID":"1"},{"id":"cmp-gj2zj9","type":"label","label":" ","value":" "},{"id":"cmp-3o7zet","type":"label","label":"Дата:","value":" "},{"id":"cmp-ai74da","type":"label","label":" ","value":" "},{"id":"date","type":"date","value":"'$dateData'","key":"'$dateKey'"},{"id":"cmp-v1pgnk","type":"label","label":" ","value":" "},{"id":"line","type":"label","label":"Насколько Вы удовлетворены:","value":" "},{"id":"column1","type":"label","label":"Совершенно не удовлетворен","value":" "},{"id":"column2","type":"label","label":"Не удовлетворен","value":" "},{"id":"column3","type":"label","label":"В чем-то удовлетворен, в чем-то - нет","value":" "},{"id":"column4","type":"label","label":"Удовлетворен","value":" "},{"id":"column5","type":"label","label":"Полностью удовлетворен","value":" "},'
newData=$newData'{"id":"line1","type":"textbox","label":" ","value":"'${vopros[1]}'"},{"id":"line2","type":"textbox","label":" ","value":"'${vopros[2]}'"},{"id":"line3","type":"textbox","label":" ","value":"'${vopros[3]}'"},{"id":"line4","type":"textbox","label":" ","value":"'${vopros[4]}'"},{"id":"line5","type":"textbox","label":" ","value":"'${vopros[5]}'"},{"id":"line6","type":"textbox","label":" ","value":"'${vopros[6]}'"},{"id":"line7","type":"textbox","label":" ","value":"'${vopros[7]}'"},{"id":"line8","type":"textbox","label":" ","value":"'${vopros[8]}'"},{"id":"line9","type":"textbox","label":" ","value":"'${vopros[9]}'"},{"id":"line10","type":"textbox","label":" ","value":"'${vopros[10]}'"},{"id":"line11","type":"textbox","label":" ","value":"'${vopros[11]}'"},{"id":"line12","type":"textbox","label":" ","value":"'${vopros[12]}'"},{"id":"line13","type":"textbox","label":" ","value":"'${vopros[13]}'"},{"id":"line14","type":"textbox","label":" ","value":"'${vopros[14]}'"},{"id":"line15","type":"textbox","label":" ","value":"'${vopros[15]}'"},{"id":"line16","type":"textbox","label":" ","value":"'${vopros[16]}'"},{"id":"line17","type":"textbox","label":" ","value":"'${vopros[17]}'"},{"id":"line18","type":"textbox","label":" ","value":"'${vopros[18]}'"},{"id":"line19","type":"textbox","label":" ","value":"'${vopros[19]}'"},'
newData=$newData'{"id":"line1_column1","type":"textbox","label":" ","value":"'$v1o1'"},{"id":"line1_column2","type":"textbox","label":" ","value":"'$v1o2'"},{"id":"line1_column3","type":"textbox","label":" ","value":"'$v1o3'"},{"id":"line1_column4","type":"textbox","label":" ","value":"'$v1o4'"},{"id":"line1_column5","type":"textbox","label":" ","value":"'$v1o5'"},{"id":"line2_column1","type":"textbox","label":" ","value":"'$v2o1'"},{"id":"line2_column2","type":"textbox","label":" ","value":"'$v2o2'"},{"id":"line2_column3","type":"textbox","label":" ","value":"'$v2o3'"},{"id":"line2_column4","type":"textbox","label":" ","value":"'$v2o4'"},{"id":"line2_column5","type":"textbox","label":" ","value":"'$v2o5'"},{"id":"line3_column1","type":"textbox","label":" ","value":"'$v3o1'"},{"id":"line3_column2","type":"textbox","label":" ","value":"'$v3o2'"},{"id":"line3_column3","type":"textbox","label":" ","value":"'$v3o3'"},{"id":"line3_column4","type":"textbox","label":" ","value":"'$v3o4'"},{"id":"line3_column5","type":"textbox","label":" ","value":"'$v3o5'"},{"id":"line4_column1","type":"textbox","label":" ","value":"'$v4o1'"},{"id":"line4_column2","type":"textbox","label":" ","value":"'$v4o2'"},{"id":"line4_column3","type":"textbox","label":" ","value":"'$v4o3'"},{"id":"line4_column4","type":"textbox","label":" ","value":"'$v4o4'"},{"id":"line4_column5","type":"textbox","label":" ","value":"'$v4o5'"},{"id":"line5_column1","type":"textbox","label":" ","value":"'$v5o1'"},{"id":"line5_column2","type":"textbox","label":" ","value":"'$v5o2'"},{"id":"line5_column3","type":"textbox","label":" ","value":"'$v5o3'"},{"id":"line5_column4","type":"textbox","label":" ","value":"'$v5o4'"},{"id":"line5_column5","type":"textbox","label":" ","value":"'$v5o5'"},{"id":"line6_column1","type":"textbox","label":" ","value":"'$v6o1'"},{"id":"line6_column2","type":"textbox","label":" ","value":"'$v6o2'"},{"id":"line6_column3","type":"textbox","label":" ","value":"'$v6o3'"},{"id":"line6_column4","type":"textbox","label":" ","value":"'$v6o4'"},{"id":"line6_column5","type":"textbox","label":" ","value":"'$v6o5'"},{"id":"line7_column1","type":"textbox","label":" ","value":"'$v7o1'"},{"id":"line7_column2","type":"textbox","label":" ","value":"'$v7o2'"},{"id":"line7_column3","type":"textbox","label":" ","value":"'$v7o3'"},{"id":"line7_column4","type":"textbox","label":" ","value":"'$v7o4'"},{"id":"line7_column5","type":"textbox","label":" ","value":"'$v7o5'"},{"id":"line8_column1","type":"textbox","label":" ","value":"'$v8o1'"},{"id":"line8_column2","type":"textbox","label":" ","value":"'$v8o2'"},{"id":"line8_column3","type":"textbox","label":" ","value":"'$v8o3'"},{"id":"line8_column4","type":"textbox","label":" ","value":"'$v8o4'"},{"id":"line8_column5","type":"textbox","label":" ","value":"'$v8o5'"},{"id":"line9_column1","type":"textbox","label":" ","value":"'$v9o1'"},{"id":"line9_column2","type":"textbox","label":" ","value":"'$v9o2'"},{"id":"line9_column3","type":"textbox","label":" ","value":"'$v9o3'"},{"id":"line9_column4","type":"textbox","label":" ","value":"'$v9o4'"},{"id":"line9_column5","type":"textbox","label":" ","value":"'$v9o5'"},'
newData=$newData'{"id":"line10_column1","type":"textbox","label":" ","value":"'$v10o1'"},{"id":"line10_column2","type":"textbox","label":" ","value":"'$v10o2'"},{"id":"line10_column3","type":"textbox","label":" ","value":"'$v10o3'"},{"id":"line10_column4","type":"textbox","label":" ","value":"'$v10o4'"},{"id":"line10_column5","type":"textbox","label":" ","value":"'$v10o5'"},{"id":"line11_column1","type":"textbox","label":" ","value":"'$v11o1'"},{"id":"line11_column2","type":"textbox","label":" ","value":"'$v11o2'"},{"id":"line11_column3","type":"textbox","label":" ","value":"'$v11o3'"},{"id":"line11_column4","type":"textbox","label":" ","value":"'$v11o4'"},{"id":"line11_column5","type":"textbox","label":" ","value":"'$v11o5'"},{"id":"line12_column1","type":"textbox","label":" ","value":"'$v12o1'"},{"id":"line12_column2","type":"textbox","label":" ","value":"'$v12o2'"},{"id":"line12_column3","type":"textbox","label":" ","value":"'$v12o3'"},{"id":"line12_column4","type":"textbox","label":" ","value":"'$v12o4'"},{"id":"line12_column5","type":"textbox","label":" ","value":"'$v12o5'"},{"id":"line13_column1","type":"textbox","label":" ","value":"'$v13o1'"},{"id":"line13_column2","type":"textbox","label":" ","value":"'$v13o2'"},{"id":"line13_column3","type":"textbox","label":" ","value":"'$v13o3'"},{"id":"line13_column4","type":"textbox","label":" ","value":"'$v13o4'"},{"id":"line13_column5","type":"textbox","label":" ","value":"'$v13o5'"},{"id":"line14_column1","type":"textbox","label":" ","value":"'$v14o1'"},{"id":"line14_column2","type":"textbox","label":" ","value":"'$v14o2'"},{"id":"line14_column3","type":"textbox","label":" ","value":"'$v14o3'"},{"id":"line14_column4","type":"textbox","label":" ","value":"'$v14o4'"},{"id":"line14_column5","type":"textbox","label":" ","value":"'$v14o5'"},{"id":"line15_column1","type":"textbox","label":" ","value":"'$v15o1'"},{"id":"line15_column2","type":"textbox","label":" ","value":"'$v15o2'"},{"id":"line15_column3","type":"textbox","label":" ","value":"'$v15o3'"},{"id":"line15_column4","type":"textbox","label":" ","value":"'$v15o4'"},{"id":"line15_column5","type":"textbox","label":" ","value":"'$v15o5'"},{"id":"line16_column1","type":"textbox","label":" ","value":"'$v16o1'"},{"id":"line16_column2","type":"textbox","label":" ","value":"'$v16o2'"},{"id":"line16_column3","type":"textbox","label":" ","value":"'$v16o3'"},{"id":"line16_column4","type":"textbox","label":" ","value":"'$v16o4'"},{"id":"line16_column5","type":"textbox","label":" ","value":"'$v16o5'"},{"id":"line17_column1","type":"textbox","label":" ","value":"'$v17o1'"},{"id":"line17_column2","type":"textbox","label":" ","value":"'$v17o2'"},{"id":"line17_column3","type":"textbox","label":" ","value":"'$v17o3'"},{"id":"line17_column4","type":"textbox","label":" ","value":"'$v17o4'"},{"id":"line17_column5","type":"textbox","label":" ","value":"'$v17o5'"},{"id":"line18_column1","type":"textbox","label":" ","value":"'$v18o1'"},{"id":"line18_column2","type":"textbox","label":" ","value":"'$v18o2'"},{"id":"line18_column3","type":"textbox","label":" ","value":"'$v18o3'"},{"id":"line18_column4","type":"textbox","label":" ","value":"'$v18o4'"},{"id":"line18_column5","type":"textbox","label":" ","value":"'$v18o5'"},{"id":"line19_column1","type":"textbox","label":" ","value":"'$v19o1'"},{"id":"line19_column2","type":"textbox","label":" ","value":"'$v19o2'"},{"id":"line19_column3","type":"textbox","label":" ","value":"'$v19o3'"},{"id":"line19_column4","type":"textbox","label":" ","value":"'$v19o4'"},{"id":"line19_column5","type":"textbox","label":" ","value":"'$v19o5'"}]'
newData=$(echo $newData | sed 's/KAKAKA//')

# создание записи реестра
#wget --quiet --http-user="$synergyUser" --http-password="$synergyPass" --output-document $tmpfile "$synergyURL/rest/api/registry/create_doc?registryID=$regStat"
curl --user $synergyUser:$synergyPass --request GET "$synergyURL/rest/api/registry/create_doc?registryID=$regStat" --output $tmpfile --silent
# берем uuid созданной записи
newUUID=`cat $tmpfile | sed "s/.*\dataUUID\": \"\(.*\)\", \"asfNodeID.*/\1/"`
# выполняем api метод POST (изменяем значение поля на форме)
curl --user $synergyUser:$synergyPass --request POST "$synergyURL/rest/api/asforms/data/save" --data "formUUID=$formStat" --data "uuid=$newUUID" --data-urlencode "data=$newData" --output $tmpfile --silent
# активация записи реестра
#wget --quiet --http-user="$synergyUser" --http-password="$synergyPass" --output-document $tmpfile "$synergyURL/rest/api/registry/activate_doc?dataUUID=$newUUID"
curl --user $synergyUser:$synergyPass --request GET "$synergyURL/rest/api/registry/activate_doc?dataUUID=$newUUID" --output $tmpfile --silent

done
fi

# проверяем был ли пересчет по дочерним компаниям, если да то формируем запись по родителю
if [ "$allCompanyKey" ]
then

# Массив ID дочерних компаний
allCompanyKey=`echo $allCompanyKey | rev | sed 's/,//' | rev`

# Поиск всех родителей
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT parent.cmp_data, parent.cmp_key, DATE_FORMAT(NOW(), '%d.%m.%Y') AS dateData, NOW() AS dateKey
FROM registry_documents rg JOIN asf_data_index parent ON parent.uuid=rg.asfDataID AND parent.cmp_id='parent'
WHERE rg.registryID='$regCompany' AND rg.deleted IS NULL AND rg.active='Y' AND rg.documentid IN ($allCompanyKey)
GROUP BY parent.cmp_key;" > $tmpSQL1

# проверяем, есть ли родители для которых нужно посчитать
if [ -s $tmpSQL1 ];
then

# Количество строк в файле tmpSQL1
countStr=`cat $tmpSQL1 | wc -l`

for i in `seq 2 $countStr`;
do
parentName=`cat $tmpSQL1 | sed -n $i'p' | cut -f1`
parentKey=`cat $tmpSQL1 | sed -n $i'p' | cut -f2`
dateData=`cat $tmpSQL1 | sed -n $i'p' | cut -f3`
dateKey=`cat $tmpSQL1 | sed -n $i'p' | cut -f4`

# Вытаскиваем всех дочек родителя
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT CONCAT(GROUP_CONCAT(DISTINCT CONCAT('\'',rg.documentID) SEPARATOR '\', '), '\'')
FROM registry_documents rg
JOIN asf_data_index parent ON parent.uuid=rg.asfDataID AND parent.cmp_id='parent'
JOIN asf_data_index status ON status.uuid=rg.asfDataID AND status.cmp_id='settings_isActive'
WHERE rg.registryID='$regCompany' AND parent.cmp_key='$parentKey'
AND rg.deleted IS NULL AND rg.active='Y' AND status.cmp_key='true'" > $tmpSQL3
allCompanyKey=`cat $tmpSQL3 | sed -n 2p | cut -f1`

# пересчет для родителя
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT
ROUND(v1o1/allAnket*100, 1) AS v1o1, ROUND(v1o2/allAnket*100, 1) AS v1o2, ROUND(v1o3/allAnket*100, 1) AS v1o3, ROUND(v1o4/allAnket*100, 1) AS v1o4,
100-(ROUND(v1o1/allAnket*100, 1)+ROUND(v1o2/allAnket*100, 1)+ROUND(v1o3/allAnket*100, 1)+ROUND(v1o4/allAnket*100, 1)) AS v1o5,
ROUND(v2o1/allAnket*100, 1) AS v2o1, ROUND(v2o2/allAnket*100, 1) AS v2o2, ROUND(v2o3/allAnket*100, 1) AS v2o3, ROUND(v2o4/allAnket*100, 1) AS v2o4,
100-(ROUND(v2o1/allAnket*100, 1)+ROUND(v2o2/allAnket*100, 1)+ROUND(v2o3/allAnket*100, 1)+ROUND(v2o4/allAnket*100, 1)) AS v2o5,
ROUND(v3o1/allAnket*100, 1) AS v3o1, ROUND(v3o2/allAnket*100, 1) AS v3o2, ROUND(v3o3/allAnket*100, 1) AS v3o3, ROUND(v3o4/allAnket*100, 1) AS v3o4,
100-(ROUND(v3o1/allAnket*100, 1)+ROUND(v3o2/allAnket*100, 1)+ROUND(v3o3/allAnket*100, 1)+ROUND(v3o4/allAnket*100, 1)) AS v3o5,
ROUND(v4o1/allAnket*100, 1) AS v4o1, ROUND(v4o2/allAnket*100, 1) AS v4o2, ROUND(v4o3/allAnket*100, 1) AS v4o3, ROUND(v4o4/allAnket*100, 1) AS v4o4,
100-(ROUND(v4o1/allAnket*100, 1)+ROUND(v4o2/allAnket*100, 1)+ROUND(v4o3/allAnket*100, 1)+ROUND(v4o4/allAnket*100, 1)) AS v4o5,
ROUND(v5o1/allAnket*100, 1) AS v5o1, ROUND(v5o2/allAnket*100, 1) AS v5o2, ROUND(v5o3/allAnket*100, 1) AS v5o3, ROUND(v5o4/allAnket*100, 1) AS v5o4,
100-(ROUND(v5o1/allAnket*100, 1)+ROUND(v5o2/allAnket*100, 1)+ROUND(v5o3/allAnket*100, 1)+ROUND(v5o4/allAnket*100, 1)) AS v5o5,
ROUND(v6o1/allAnket*100, 1) AS v6o1, ROUND(v6o2/allAnket*100, 1) AS v6o2, ROUND(v6o3/allAnket*100, 1) AS v6o3, ROUND(v6o4/allAnket*100, 1) AS v6o4,
100-(ROUND(v6o1/allAnket*100, 1)+ROUND(v6o2/allAnket*100, 1)+ROUND(v6o3/allAnket*100, 1)+ROUND(v6o4/allAnket*100, 1)) AS v6o5,
ROUND(v7o1/allAnket*100, 1) AS v7o1, ROUND(v7o2/allAnket*100, 1) AS v7o2, ROUND(v7o3/allAnket*100, 1) AS v7o3, ROUND(v7o4/allAnket*100, 1) AS v7o4,
100-(ROUND(v7o1/allAnket*100, 1)+ROUND(v7o2/allAnket*100, 1)+ROUND(v7o3/allAnket*100, 1)+ROUND(v7o4/allAnket*100, 1)) AS v7o5,
ROUND(v8o1/allAnket*100, 1) AS v8o1, ROUND(v8o2/allAnket*100, 1) AS v8o2, ROUND(v8o3/allAnket*100, 1) AS v8o3, ROUND(v8o4/allAnket*100, 1) AS v8o4,
100-(ROUND(v8o1/allAnket*100, 1)+ROUND(v8o2/allAnket*100, 1)+ROUND(v8o3/allAnket*100, 1)+ROUND(v8o4/allAnket*100, 1)) AS v8o5,
ROUND(v9o1/allAnket*100, 1) AS v9o1, ROUND(v9o2/allAnket*100, 1) AS v9o2, ROUND(v9o3/allAnket*100, 1) AS v9o3, ROUND(v9o4/allAnket*100, 1) AS v9o4,
100-(ROUND(v9o1/allAnket*100, 1)+ROUND(v9o2/allAnket*100, 1)+ROUND(v9o3/allAnket*100, 1)+ROUND(v9o4/allAnket*100, 1)) AS v9o5,
ROUND(v10o1/allAnket*100, 1) AS v10o1, ROUND(v10o2/allAnket*100, 1) AS v10o2, ROUND(v10o3/allAnket*100, 1) AS v10o3, ROUND(v10o4/allAnket*100, 1) AS v10o4,
100-(ROUND(v10o1/allAnket*100, 1)+ROUND(v10o2/allAnket*100, 1)+ROUND(v10o3/allAnket*100, 1)+ROUND(v10o4/allAnket*100, 1)) AS v10o5,
ROUND(v11o1/allAnket*100, 1) AS v11o1, ROUND(v11o2/allAnket*100, 1) AS v11o2, ROUND(v11o3/allAnket*100, 1) AS v11o3, ROUND(v11o4/allAnket*100, 1) AS v11o4,
100-(ROUND(v11o1/allAnket*100, 1)+ROUND(v11o2/allAnket*100, 1)+ROUND(v11o3/allAnket*100, 1)+ROUND(v11o4/allAnket*100, 1)) AS v11o5,
ROUND(v12o1/allAnket*100, 1) AS v12o1, ROUND(v12o2/allAnket*100, 1) AS v12o2, ROUND(v12o3/allAnket*100, 1) AS v12o3, ROUND(v12o4/allAnket*100, 1) AS v12o4,
100-(ROUND(v12o1/allAnket*100, 1)+ROUND(v12o2/allAnket*100, 1)+ROUND(v12o3/allAnket*100, 1)+ROUND(v12o4/allAnket*100, 1)) AS v12o5,
ROUND(v13o1/allAnket*100, 1) AS v13o1, ROUND(v13o2/allAnket*100, 1) AS v13o2, ROUND(v13o3/allAnket*100, 1) AS v13o3, ROUND(v13o4/allAnket*100, 1) AS v13o4,
100-(ROUND(v13o1/allAnket*100, 1)+ROUND(v13o2/allAnket*100, 1)+ROUND(v13o3/allAnket*100, 1)+ROUND(v13o4/allAnket*100, 1)) AS v13o5,
ROUND(v14o1/allAnket*100, 1) AS v14o1, ROUND(v14o2/allAnket*100, 1) AS v14o2, ROUND(v14o3/allAnket*100, 1) AS v14o3, ROUND(v14o4/allAnket*100, 1) AS v14o4,
100-(ROUND(v14o1/allAnket*100, 1)+ROUND(v14o2/allAnket*100, 1)+ROUND(v14o3/allAnket*100, 1)+ROUND(v14o4/allAnket*100, 1)) AS v14o5,
ROUND(v15o1/allAnket*100, 1) AS v15o1, ROUND(v15o2/allAnket*100, 1) AS v15o2, ROUND(v15o3/allAnket*100, 1) AS v15o3, ROUND(v15o4/allAnket*100, 1) AS v15o4,
100-(ROUND(v15o1/allAnket*100, 1)+ROUND(v15o2/allAnket*100, 1)+ROUND(v15o3/allAnket*100, 1)+ROUND(v15o4/allAnket*100, 1)) AS v15o5,
ROUND(v16o1/allAnket*100, 1) AS v16o1, ROUND(v16o2/allAnket*100, 1) AS v16o2, ROUND(v16o3/allAnket*100, 1) AS v16o3, ROUND(v16o4/allAnket*100, 1) AS v16o4,
100-(ROUND(v16o1/allAnket*100, 1)+ROUND(v16o2/allAnket*100, 1)+ROUND(v16o3/allAnket*100, 1)+ROUND(v16o4/allAnket*100, 1)) AS v16o5,
ROUND(v17o1/allAnket*100, 1) AS v17o1, ROUND(v17o2/allAnket*100, 1) AS v17o2, ROUND(v17o3/allAnket*100, 1) AS v17o3, ROUND(v17o4/allAnket*100, 1) AS v17o4,
100-(ROUND(v17o1/allAnket*100, 1)+ROUND(v17o2/allAnket*100, 1)+ROUND(v17o3/allAnket*100, 1)+ROUND(v17o4/allAnket*100, 1)) AS v17o5,
ROUND(v18o1/allAnket*100, 1) AS v18o1, ROUND(v18o2/allAnket*100, 1) AS v18o2, ROUND(v18o3/allAnket*100, 1) AS v18o3, ROUND(v18o4/allAnket*100, 1) AS v18o4,
100-(ROUND(v18o1/allAnket*100, 1)+ROUND(v18o2/allAnket*100, 1)+ROUND(v18o3/allAnket*100, 1)+ROUND(v18o4/allAnket*100, 1)) AS v18o5,
ROUND(v19o1/allAnket*100, 1) AS v19o1, ROUND(v19o2/allAnket*100, 1) AS v19o2, ROUND(v19o3/allAnket*100, 1) AS v19o3, ROUND(v19o4/allAnket*100, 1) AS v19o4,
100-(ROUND(v19o1/allAnket*100, 1)+ROUND(v19o2/allAnket*100, 1)+ROUND(v19o3/allAnket*100, 1)+ROUND(v19o4/allAnket*100, 1)) AS v19o5

FROM (SELECT COUNT(DISTINCT rg.asfDataID) AS allAnket,
SUM(IF(vopros.cmp_id='frustrated_otvet1' AND vopros.cmp_key='4',1,0)) AS v1o1, SUM(IF(vopros.cmp_id='frustrated_otvet1' AND vopros.cmp_key='3',1,0)) AS v1o2,
SUM(IF(vopros.cmp_id='frustrated_otvet1' AND vopros.cmp_key='2',1,0)) AS v1o3, SUM(IF(vopros.cmp_id='frustrated_otvet1' AND vopros.cmp_key='1',1,0)) AS v1o4,
SUM(IF(vopros.cmp_id='frustrated_otvet2' AND vopros.cmp_key='4',1,0)) AS v2o1, SUM(IF(vopros.cmp_id='frustrated_otvet2' AND vopros.cmp_key='3',1,0)) AS v2o2,
SUM(IF(vopros.cmp_id='frustrated_otvet2' AND vopros.cmp_key='2',1,0)) AS v2o3, SUM(IF(vopros.cmp_id='frustrated_otvet2' AND vopros.cmp_key='1',1,0)) AS v2o4,
SUM(IF(vopros.cmp_id='frustrated_otvet3' AND vopros.cmp_key='4',1,0)) AS v3o1, SUM(IF(vopros.cmp_id='frustrated_otvet3' AND vopros.cmp_key='3',1,0)) AS v3o2,
SUM(IF(vopros.cmp_id='frustrated_otvet3' AND vopros.cmp_key='2',1,0)) AS v3o3, SUM(IF(vopros.cmp_id='frustrated_otvet3' AND vopros.cmp_key='1',1,0)) AS v3o4,
SUM(IF(vopros.cmp_id='frustrated_otvet4' AND vopros.cmp_key='4',1,0)) AS v4o1, SUM(IF(vopros.cmp_id='frustrated_otvet4' AND vopros.cmp_key='3',1,0)) AS v4o2,
SUM(IF(vopros.cmp_id='frustrated_otvet4' AND vopros.cmp_key='2',1,0)) AS v4o3, SUM(IF(vopros.cmp_id='frustrated_otvet4' AND vopros.cmp_key='1',1,0)) AS v4o4,
SUM(IF(vopros.cmp_id='frustrated_otvet5' AND vopros.cmp_key='4',1,0)) AS v5o1, SUM(IF(vopros.cmp_id='frustrated_otvet5' AND vopros.cmp_key='3',1,0)) AS v5o2,
SUM(IF(vopros.cmp_id='frustrated_otvet5' AND vopros.cmp_key='2',1,0)) AS v5o3, SUM(IF(vopros.cmp_id='frustrated_otvet5' AND vopros.cmp_key='1',1,0)) AS v5o4,
SUM(IF(vopros.cmp_id='frustrated_otvet6' AND vopros.cmp_key='4',1,0)) AS v6o1, SUM(IF(vopros.cmp_id='frustrated_otvet6' AND vopros.cmp_key='3',1,0)) AS v6o2,
SUM(IF(vopros.cmp_id='frustrated_otvet6' AND vopros.cmp_key='2',1,0)) AS v6o3, SUM(IF(vopros.cmp_id='frustrated_otvet6' AND vopros.cmp_key='1',1,0)) AS v6o4,
SUM(IF(vopros.cmp_id='frustrated_otvet7' AND vopros.cmp_key='4',1,0)) AS v7o1, SUM(IF(vopros.cmp_id='frustrated_otvet7' AND vopros.cmp_key='3',1,0)) AS v7o2,
SUM(IF(vopros.cmp_id='frustrated_otvet7' AND vopros.cmp_key='2',1,0)) AS v7o3, SUM(IF(vopros.cmp_id='frustrated_otvet7' AND vopros.cmp_key='1',1,0)) AS v7o4,
SUM(IF(vopros.cmp_id='frustrated_otvet8' AND vopros.cmp_key='4',1,0)) AS v8o1, SUM(IF(vopros.cmp_id='frustrated_otvet8' AND vopros.cmp_key='3',1,0)) AS v8o2,
SUM(IF(vopros.cmp_id='frustrated_otvet8' AND vopros.cmp_key='2',1,0)) AS v8o3, SUM(IF(vopros.cmp_id='frustrated_otvet8' AND vopros.cmp_key='1',1,0)) AS v8o4,
SUM(IF(vopros.cmp_id='frustrated_otvet9' AND vopros.cmp_key='4',1,0)) AS v9o1, SUM(IF(vopros.cmp_id='frustrated_otvet9' AND vopros.cmp_key='3',1,0)) AS v9o2,
SUM(IF(vopros.cmp_id='frustrated_otvet9' AND vopros.cmp_key='2',1,0)) AS v9o3, SUM(IF(vopros.cmp_id='frustrated_otvet9' AND vopros.cmp_key='1',1,0)) AS v9o4,
SUM(IF(vopros.cmp_id='frustrated_otvet10' AND vopros.cmp_key='4',1,0)) AS v10o1, SUM(IF(vopros.cmp_id='frustrated_otvet10' AND vopros.cmp_key='3',1,0)) AS v10o2,
SUM(IF(vopros.cmp_id='frustrated_otvet10' AND vopros.cmp_key='2',1,0)) AS v10o3, SUM(IF(vopros.cmp_id='frustrated_otvet10' AND vopros.cmp_key='1',1,0)) AS v10o4,
SUM(IF(vopros.cmp_id='frustrated_otvet11' AND vopros.cmp_key='4',1,0)) AS v11o1, SUM(IF(vopros.cmp_id='frustrated_otvet11' AND vopros.cmp_key='3',1,0)) AS v11o2,
SUM(IF(vopros.cmp_id='frustrated_otvet11' AND vopros.cmp_key='2',1,0)) AS v11o3, SUM(IF(vopros.cmp_id='frustrated_otvet11' AND vopros.cmp_key='1',1,0)) AS v11o4,
SUM(IF(vopros.cmp_id='frustrated_otvet12' AND vopros.cmp_key='4',1,0)) AS v12o1, SUM(IF(vopros.cmp_id='frustrated_otvet12' AND vopros.cmp_key='3',1,0)) AS v12o2,
SUM(IF(vopros.cmp_id='frustrated_otvet12' AND vopros.cmp_key='2',1,0)) AS v12o3, SUM(IF(vopros.cmp_id='frustrated_otvet12' AND vopros.cmp_key='1',1,0)) AS v12o4,
SUM(IF(vopros.cmp_id='frustrated_otvet13' AND vopros.cmp_key='4',1,0)) AS v13o1, SUM(IF(vopros.cmp_id='frustrated_otvet13' AND vopros.cmp_key='3',1,0)) AS v13o2,
SUM(IF(vopros.cmp_id='frustrated_otvet13' AND vopros.cmp_key='2',1,0)) AS v13o3, SUM(IF(vopros.cmp_id='frustrated_otvet13' AND vopros.cmp_key='1',1,0)) AS v13o4,
SUM(IF(vopros.cmp_id='frustrated_otvet14' AND vopros.cmp_key='4',1,0)) AS v14o1, SUM(IF(vopros.cmp_id='frustrated_otvet14' AND vopros.cmp_key='3',1,0)) AS v14o2,
SUM(IF(vopros.cmp_id='frustrated_otvet14' AND vopros.cmp_key='2',1,0)) AS v14o3, SUM(IF(vopros.cmp_id='frustrated_otvet14' AND vopros.cmp_key='1',1,0)) AS v14o4,
SUM(IF(vopros.cmp_id='frustrated_otvet15' AND vopros.cmp_key='4',1,0)) AS v15o1, SUM(IF(vopros.cmp_id='frustrated_otvet15' AND vopros.cmp_key='3',1,0)) AS v15o2,
SUM(IF(vopros.cmp_id='frustrated_otvet15' AND vopros.cmp_key='2',1,0)) AS v15o3, SUM(IF(vopros.cmp_id='frustrated_otvet15' AND vopros.cmp_key='1',1,0)) AS v15o4,
SUM(IF(vopros.cmp_id='frustrated_otvet16' AND vopros.cmp_key='4',1,0)) AS v16o1, SUM(IF(vopros.cmp_id='frustrated_otvet16' AND vopros.cmp_key='3',1,0)) AS v16o2,
SUM(IF(vopros.cmp_id='frustrated_otvet16' AND vopros.cmp_key='2',1,0)) AS v16o3, SUM(IF(vopros.cmp_id='frustrated_otvet16' AND vopros.cmp_key='1',1,0)) AS v16o4,
SUM(IF(vopros.cmp_id='frustrated_otvet17' AND vopros.cmp_key='4',1,0)) AS v17o1, SUM(IF(vopros.cmp_id='frustrated_otvet17' AND vopros.cmp_key='3',1,0)) AS v17o2,
SUM(IF(vopros.cmp_id='frustrated_otvet17' AND vopros.cmp_key='2',1,0)) AS v17o3, SUM(IF(vopros.cmp_id='frustrated_otvet17' AND vopros.cmp_key='1',1,0)) AS v17o4,
SUM(IF(vopros.cmp_id='frustrated_otvet18' AND vopros.cmp_key='4',1,0)) AS v18o1, SUM(IF(vopros.cmp_id='frustrated_otvet18' AND vopros.cmp_key='3',1,0)) AS v18o2,
SUM(IF(vopros.cmp_id='frustrated_otvet18' AND vopros.cmp_key='2',1,0)) AS v18o3, SUM(IF(vopros.cmp_id='frustrated_otvet18' AND vopros.cmp_key='1',1,0)) AS v18o4,
SUM(IF(vopros.cmp_id='frustrated_otvet19' AND vopros.cmp_key='4',1,0)) AS v19o1, SUM(IF(vopros.cmp_id='frustrated_otvet19' AND vopros.cmp_key='3',1,0)) AS v19o2,
SUM(IF(vopros.cmp_id='frustrated_otvet19' AND vopros.cmp_key='2',1,0)) AS v19o3, SUM(IF(vopros.cmp_id='frustrated_otvet19' AND vopros.cmp_key='1',1,0)) AS v19o4
FROM registry_documents rg
JOIN asf_data_index company ON company.uuid=rg.asfDataID AND company.cmp_id='company'
JOIN asf_data_index dateAnk ON dateAnk.uuid=rg.asfDataID AND dateAnk.cmp_id='date'
LEFT JOIN asf_data_index vopros ON vopros.uuid=rg.asfDataID AND vopros.cmp_id LIKE 'frustrated_otvet%'
WHERE rg.registryID='$regAnketa' AND company.cmp_key IN ($allCompanyKey)
AND rg.deleted IS NULL AND rg.active='Y' AND YEAR(dateAnk.cmp_key)=YEAR(NOW()) ) t;" > $tmpSQL2

# присвоение переменных
v1o1=`cat $tmpSQL2 | sed -n 2p | cut -f1`
v1o2=`cat $tmpSQL2 | sed -n 2p | cut -f2`
v1o3=`cat $tmpSQL2 | sed -n 2p | cut -f3`
v1o4=`cat $tmpSQL2 | sed -n 2p | cut -f4`
v1o5=`cat $tmpSQL2 | sed -n 2p | cut -f5`
v2o1=`cat $tmpSQL2 | sed -n 2p | cut -f6`
v2o2=`cat $tmpSQL2 | sed -n 2p | cut -f7`
v2o3=`cat $tmpSQL2 | sed -n 2p | cut -f8`
v2o4=`cat $tmpSQL2 | sed -n 2p | cut -f9`
v2o5=`cat $tmpSQL2 | sed -n 2p | cut -f10`
v3o1=`cat $tmpSQL2 | sed -n 2p | cut -f11`
v3o2=`cat $tmpSQL2 | sed -n 2p | cut -f12`
v3o3=`cat $tmpSQL2 | sed -n 2p | cut -f13`
v3o4=`cat $tmpSQL2 | sed -n 2p | cut -f14`
v3o5=`cat $tmpSQL2 | sed -n 2p | cut -f15`
v4o1=`cat $tmpSQL2 | sed -n 2p | cut -f16`
v4o2=`cat $tmpSQL2 | sed -n 2p | cut -f17`
v4o3=`cat $tmpSQL2 | sed -n 2p | cut -f18`
v4o4=`cat $tmpSQL2 | sed -n 2p | cut -f19`
v4o5=`cat $tmpSQL2 | sed -n 2p | cut -f20`
v5o1=`cat $tmpSQL2 | sed -n 2p | cut -f21`
v5o2=`cat $tmpSQL2 | sed -n 2p | cut -f22`
v5o3=`cat $tmpSQL2 | sed -n 2p | cut -f23`
v5o4=`cat $tmpSQL2 | sed -n 2p | cut -f24`
v5o5=`cat $tmpSQL2 | sed -n 2p | cut -f25`
v6o1=`cat $tmpSQL2 | sed -n 2p | cut -f26`
v6o2=`cat $tmpSQL2 | sed -n 2p | cut -f27`
v6o3=`cat $tmpSQL2 | sed -n 2p | cut -f28`
v6o4=`cat $tmpSQL2 | sed -n 2p | cut -f29`
v6o5=`cat $tmpSQL2 | sed -n 2p | cut -f30`
v7o1=`cat $tmpSQL2 | sed -n 2p | cut -f31`
v7o2=`cat $tmpSQL2 | sed -n 2p | cut -f32`
v7o3=`cat $tmpSQL2 | sed -n 2p | cut -f33`
v7o4=`cat $tmpSQL2 | sed -n 2p | cut -f34`
v7o5=`cat $tmpSQL2 | sed -n 2p | cut -f35`
v8o1=`cat $tmpSQL2 | sed -n 2p | cut -f36`
v8o2=`cat $tmpSQL2 | sed -n 2p | cut -f37`
v8o3=`cat $tmpSQL2 | sed -n 2p | cut -f38`
v8o4=`cat $tmpSQL2 | sed -n 2p | cut -f39`
v8o5=`cat $tmpSQL2 | sed -n 2p | cut -f40`
v9o1=`cat $tmpSQL2 | sed -n 2p | cut -f41`
v9o2=`cat $tmpSQL2 | sed -n 2p | cut -f42`
v9o3=`cat $tmpSQL2 | sed -n 2p | cut -f43`
v9o4=`cat $tmpSQL2 | sed -n 2p | cut -f44`
v9o5=`cat $tmpSQL2 | sed -n 2p | cut -f45`
v10o1=`cat $tmpSQL2 | sed -n 2p | cut -f46`
v10o2=`cat $tmpSQL2 | sed -n 2p | cut -f47`
v10o3=`cat $tmpSQL2 | sed -n 2p | cut -f48`
v10o4=`cat $tmpSQL2 | sed -n 2p | cut -f49`
v10o5=`cat $tmpSQL2 | sed -n 2p | cut -f50`
v11o1=`cat $tmpSQL2 | sed -n 2p | cut -f51`
v11o2=`cat $tmpSQL2 | sed -n 2p | cut -f52`
v11o3=`cat $tmpSQL2 | sed -n 2p | cut -f53`
v11o4=`cat $tmpSQL2 | sed -n 2p | cut -f54`
v11o5=`cat $tmpSQL2 | sed -n 2p | cut -f55`
v12o1=`cat $tmpSQL2 | sed -n 2p | cut -f56`
v12o2=`cat $tmpSQL2 | sed -n 2p | cut -f57`
v12o3=`cat $tmpSQL2 | sed -n 2p | cut -f58`
v12o4=`cat $tmpSQL2 | sed -n 2p | cut -f59`
v12o5=`cat $tmpSQL2 | sed -n 2p | cut -f60`
v13o1=`cat $tmpSQL2 | sed -n 2p | cut -f61`
v13o2=`cat $tmpSQL2 | sed -n 2p | cut -f62`
v13o3=`cat $tmpSQL2 | sed -n 2p | cut -f63`
v13o4=`cat $tmpSQL2 | sed -n 2p | cut -f64`
v13o5=`cat $tmpSQL2 | sed -n 2p | cut -f65`
v14o1=`cat $tmpSQL2 | sed -n 2p | cut -f66`
v14o2=`cat $tmpSQL2 | sed -n 2p | cut -f67`
v14o3=`cat $tmpSQL2 | sed -n 2p | cut -f68`
v14o4=`cat $tmpSQL2 | sed -n 2p | cut -f69`
v14o5=`cat $tmpSQL2 | sed -n 2p | cut -f70`
v15o1=`cat $tmpSQL2 | sed -n 2p | cut -f71`
v15o2=`cat $tmpSQL2 | sed -n 2p | cut -f72`
v15o3=`cat $tmpSQL2 | sed -n 2p | cut -f73`
v15o4=`cat $tmpSQL2 | sed -n 2p | cut -f74`
v15o5=`cat $tmpSQL2 | sed -n 2p | cut -f75`
v16o1=`cat $tmpSQL2 | sed -n 2p | cut -f76`
v16o2=`cat $tmpSQL2 | sed -n 2p | cut -f77`
v16o3=`cat $tmpSQL2 | sed -n 2p | cut -f78`
v16o4=`cat $tmpSQL2 | sed -n 2p | cut -f79`
v16o5=`cat $tmpSQL2 | sed -n 2p | cut -f80`
v17o1=`cat $tmpSQL2 | sed -n 2p | cut -f81`
v17o2=`cat $tmpSQL2 | sed -n 2p | cut -f82`
v17o3=`cat $tmpSQL2 | sed -n 2p | cut -f83`
v17o4=`cat $tmpSQL2 | sed -n 2p | cut -f84`
v17o5=`cat $tmpSQL2 | sed -n 2p | cut -f85`
v18o1=`cat $tmpSQL2 | sed -n 2p | cut -f86`
v18o2=`cat $tmpSQL2 | sed -n 2p | cut -f87`
v18o3=`cat $tmpSQL2 | sed -n 2p | cut -f88`
v18o4=`cat $tmpSQL2 | sed -n 2p | cut -f89`
v18o5=`cat $tmpSQL2 | sed -n 2p | cut -f90`
v19o1=`cat $tmpSQL2 | sed -n 2p | cut -f91`
v19o2=`cat $tmpSQL2 | sed -n 2p | cut -f92`
v19o3=`cat $tmpSQL2 | sed -n 2p | cut -f93`
v19o4=`cat $tmpSQL2 | sed -n 2p | cut -f94`
v19o5=`cat $tmpSQL2 | sed -n 2p | cut -f95`

# формирование правильной строки data для родительской компании
newData='KAKAKA"data":[{"id":"text","type":"label","label":"Результаты опроса работников","value":" "},{"id":"cmp-vour89","type":"label","label":" ","value":" "},{"id":"cmp-4gxmgl","type":"label","label":"Компания:","value":" "},{"id":"company","type":"reglink","value":"'$parentName'","key":"'$parentKey'","valueID":"'$parentKey'","username":"Admin Admin Admin","userID":"1"},{"id":"cmp-gj2zj9","type":"label","label":" ","value":" "},{"id":"cmp-3o7zet","type":"label","label":"Дата:","value":" "},{"id":"cmp-ai74da","type":"label","label":" ","value":" "},{"id":"date","type":"date","value":"'$dateData'","key":"'$dateKey'"},{"id":"cmp-v1pgnk","type":"label","label":" ","value":" "},{"id":"line","type":"label","label":"Насколько Вы удовлетворены:","value":" "},{"id":"column1","type":"label","label":"Совершенно не удовлетворен","value":" "},{"id":"column2","type":"label","label":"Не удовлетворен","value":" "},{"id":"column3","type":"label","label":"В чем-то удовлетворен, в чем-то - нет","value":" "},{"id":"column4","type":"label","label":"Удовлетворен","value":" "},{"id":"column5","type":"label","label":"Полностью удовлетворен","value":" "},'
newData=$newData'{"id":"line1","type":"textbox","label":" ","value":"'${vopros[1]}'"},{"id":"line2","type":"textbox","label":" ","value":"'${vopros[2]}'"},{"id":"line3","type":"textbox","label":" ","value":"'${vopros[3]}'"},{"id":"line4","type":"textbox","label":" ","value":"'${vopros[4]}'"},{"id":"line5","type":"textbox","label":" ","value":"'${vopros[5]}'"},{"id":"line6","type":"textbox","label":" ","value":"'${vopros[6]}'"},{"id":"line7","type":"textbox","label":" ","value":"'${vopros[7]}'"},{"id":"line8","type":"textbox","label":" ","value":"'${vopros[8]}'"},{"id":"line9","type":"textbox","label":" ","value":"'${vopros[9]}'"},{"id":"line10","type":"textbox","label":" ","value":"'${vopros[10]}'"},{"id":"line11","type":"textbox","label":" ","value":"'${vopros[11]}'"},{"id":"line12","type":"textbox","label":" ","value":"'${vopros[12]}'"},{"id":"line13","type":"textbox","label":" ","value":"'${vopros[13]}'"},{"id":"line14","type":"textbox","label":" ","value":"'${vopros[14]}'"},{"id":"line15","type":"textbox","label":" ","value":"'${vopros[15]}'"},{"id":"line16","type":"textbox","label":" ","value":"'${vopros[16]}'"},{"id":"line17","type":"textbox","label":" ","value":"'${vopros[17]}'"},{"id":"line18","type":"textbox","label":" ","value":"'${vopros[18]}'"},{"id":"line19","type":"textbox","label":" ","value":"'${vopros[19]}'"},'
newData=$newData'{"id":"line1_column1","type":"textbox","label":" ","value":"'$v1o1'"},{"id":"line1_column2","type":"textbox","label":" ","value":"'$v1o2'"},{"id":"line1_column3","type":"textbox","label":" ","value":"'$v1o3'"},{"id":"line1_column4","type":"textbox","label":" ","value":"'$v1o4'"},{"id":"line1_column5","type":"textbox","label":" ","value":"'$v1o5'"},{"id":"line2_column1","type":"textbox","label":" ","value":"'$v2o1'"},{"id":"line2_column2","type":"textbox","label":" ","value":"'$v2o2'"},{"id":"line2_column3","type":"textbox","label":" ","value":"'$v2o3'"},{"id":"line2_column4","type":"textbox","label":" ","value":"'$v2o4'"},{"id":"line2_column5","type":"textbox","label":" ","value":"'$v2o5'"},{"id":"line3_column1","type":"textbox","label":" ","value":"'$v3o1'"},{"id":"line3_column2","type":"textbox","label":" ","value":"'$v3o2'"},{"id":"line3_column3","type":"textbox","label":" ","value":"'$v3o3'"},{"id":"line3_column4","type":"textbox","label":" ","value":"'$v3o4'"},{"id":"line3_column5","type":"textbox","label":" ","value":"'$v3o5'"},{"id":"line4_column1","type":"textbox","label":" ","value":"'$v4o1'"},{"id":"line4_column2","type":"textbox","label":" ","value":"'$v4o2'"},{"id":"line4_column3","type":"textbox","label":" ","value":"'$v4o3'"},{"id":"line4_column4","type":"textbox","label":" ","value":"'$v4o4'"},{"id":"line4_column5","type":"textbox","label":" ","value":"'$v4o5'"},{"id":"line5_column1","type":"textbox","label":" ","value":"'$v5o1'"},{"id":"line5_column2","type":"textbox","label":" ","value":"'$v5o2'"},{"id":"line5_column3","type":"textbox","label":" ","value":"'$v5o3'"},{"id":"line5_column4","type":"textbox","label":" ","value":"'$v5o4'"},{"id":"line5_column5","type":"textbox","label":" ","value":"'$v5o5'"},{"id":"line6_column1","type":"textbox","label":" ","value":"'$v6o1'"},{"id":"line6_column2","type":"textbox","label":" ","value":"'$v6o2'"},{"id":"line6_column3","type":"textbox","label":" ","value":"'$v6o3'"},{"id":"line6_column4","type":"textbox","label":" ","value":"'$v6o4'"},{"id":"line6_column5","type":"textbox","label":" ","value":"'$v6o5'"},{"id":"line7_column1","type":"textbox","label":" ","value":"'$v7o1'"},{"id":"line7_column2","type":"textbox","label":" ","value":"'$v7o2'"},{"id":"line7_column3","type":"textbox","label":" ","value":"'$v7o3'"},{"id":"line7_column4","type":"textbox","label":" ","value":"'$v7o4'"},{"id":"line7_column5","type":"textbox","label":" ","value":"'$v7o5'"},{"id":"line8_column1","type":"textbox","label":" ","value":"'$v8o1'"},{"id":"line8_column2","type":"textbox","label":" ","value":"'$v8o2'"},{"id":"line8_column3","type":"textbox","label":" ","value":"'$v8o3'"},{"id":"line8_column4","type":"textbox","label":" ","value":"'$v8o4'"},{"id":"line8_column5","type":"textbox","label":" ","value":"'$v8o5'"},{"id":"line9_column1","type":"textbox","label":" ","value":"'$v9o1'"},{"id":"line9_column2","type":"textbox","label":" ","value":"'$v9o2'"},{"id":"line9_column3","type":"textbox","label":" ","value":"'$v9o3'"},{"id":"line9_column4","type":"textbox","label":" ","value":"'$v9o4'"},{"id":"line9_column5","type":"textbox","label":" ","value":"'$v9o5'"},'
newData=$newData'{"id":"line10_column1","type":"textbox","label":" ","value":"'$v10o1'"},{"id":"line10_column2","type":"textbox","label":" ","value":"'$v10o2'"},{"id":"line10_column3","type":"textbox","label":" ","value":"'$v10o3'"},{"id":"line10_column4","type":"textbox","label":" ","value":"'$v10o4'"},{"id":"line10_column5","type":"textbox","label":" ","value":"'$v10o5'"},{"id":"line11_column1","type":"textbox","label":" ","value":"'$v11o1'"},{"id":"line11_column2","type":"textbox","label":" ","value":"'$v11o2'"},{"id":"line11_column3","type":"textbox","label":" ","value":"'$v11o3'"},{"id":"line11_column4","type":"textbox","label":" ","value":"'$v11o4'"},{"id":"line11_column5","type":"textbox","label":" ","value":"'$v11o5'"},{"id":"line12_column1","type":"textbox","label":" ","value":"'$v12o1'"},{"id":"line12_column2","type":"textbox","label":" ","value":"'$v12o2'"},{"id":"line12_column3","type":"textbox","label":" ","value":"'$v12o3'"},{"id":"line12_column4","type":"textbox","label":" ","value":"'$v12o4'"},{"id":"line12_column5","type":"textbox","label":" ","value":"'$v12o5'"},{"id":"line13_column1","type":"textbox","label":" ","value":"'$v13o1'"},{"id":"line13_column2","type":"textbox","label":" ","value":"'$v13o2'"},{"id":"line13_column3","type":"textbox","label":" ","value":"'$v13o3'"},{"id":"line13_column4","type":"textbox","label":" ","value":"'$v13o4'"},{"id":"line13_column5","type":"textbox","label":" ","value":"'$v13o5'"},{"id":"line14_column1","type":"textbox","label":" ","value":"'$v14o1'"},{"id":"line14_column2","type":"textbox","label":" ","value":"'$v14o2'"},{"id":"line14_column3","type":"textbox","label":" ","value":"'$v14o3'"},{"id":"line14_column4","type":"textbox","label":" ","value":"'$v14o4'"},{"id":"line14_column5","type":"textbox","label":" ","value":"'$v14o5'"},{"id":"line15_column1","type":"textbox","label":" ","value":"'$v15o1'"},{"id":"line15_column2","type":"textbox","label":" ","value":"'$v15o2'"},{"id":"line15_column3","type":"textbox","label":" ","value":"'$v15o3'"},{"id":"line15_column4","type":"textbox","label":" ","value":"'$v15o4'"},{"id":"line15_column5","type":"textbox","label":" ","value":"'$v15o5'"},{"id":"line16_column1","type":"textbox","label":" ","value":"'$v16o1'"},{"id":"line16_column2","type":"textbox","label":" ","value":"'$v16o2'"},{"id":"line16_column3","type":"textbox","label":" ","value":"'$v16o3'"},{"id":"line16_column4","type":"textbox","label":" ","value":"'$v16o4'"},{"id":"line16_column5","type":"textbox","label":" ","value":"'$v16o5'"},{"id":"line17_column1","type":"textbox","label":" ","value":"'$v17o1'"},{"id":"line17_column2","type":"textbox","label":" ","value":"'$v17o2'"},{"id":"line17_column3","type":"textbox","label":" ","value":"'$v17o3'"},{"id":"line17_column4","type":"textbox","label":" ","value":"'$v17o4'"},{"id":"line17_column5","type":"textbox","label":" ","value":"'$v17o5'"},{"id":"line18_column1","type":"textbox","label":" ","value":"'$v18o1'"},{"id":"line18_column2","type":"textbox","label":" ","value":"'$v18o2'"},{"id":"line18_column3","type":"textbox","label":" ","value":"'$v18o3'"},{"id":"line18_column4","type":"textbox","label":" ","value":"'$v18o4'"},{"id":"line18_column5","type":"textbox","label":" ","value":"'$v18o5'"},{"id":"line19_column1","type":"textbox","label":" ","value":"'$v19o1'"},{"id":"line19_column2","type":"textbox","label":" ","value":"'$v19o2'"},{"id":"line19_column3","type":"textbox","label":" ","value":"'$v19o3'"},{"id":"line19_column4","type":"textbox","label":" ","value":"'$v19o4'"},{"id":"line19_column5","type":"textbox","label":" ","value":"'$v19o5'"}]'
newData=$(echo $newData | sed 's/KAKAKA//')

# создание записи реестра
#wget --quiet --http-user="$synergyUser" --http-password="$synergyPass" --output-document $tmpfile "$synergyURL/rest/api/registry/create_doc?registryID=$regStat"
curl --user $synergyUser:$synergyPass --request GET "$synergyURL/rest/api/registry/create_doc?registryID=$regStat" --output $tmpfile --silent
# берем uuid созданной записи
newUUID=`cat $tmpfile | sed "s/.*\dataUUID\": \"\(.*\)\", \"asfNodeID.*/\1/"`
# выполняем api метод POST (изменяем значение поля на форме)
curl --user $synergyUser:$synergyPass --request POST "$synergyURL/rest/api/asforms/data/save" --data "formUUID=$formStat" --data "uuid=$newUUID" --data-urlencode "data=$newData" --output $tmpfile --silent
# активация записи реестра
#wget --quiet --http-user="$synergyUser" --http-password="$synergyPass" --output-document $tmpfile "$synergyURL/rest/api/registry/activate_doc?dataUUID=$newUUID"
curl --user $synergyUser:$synergyPass --request GET "$synergyURL/rest/api/registry/activate_doc?dataUUID=$newUUID" --output $tmpfile --silent

done
fi
fi

# удаление временных файлов
rm -f $tmpSQL1
rm -f $tmpSQL2
rm -f $tmpSQL3
rm -f $tmpfile
