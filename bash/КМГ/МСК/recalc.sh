#!/bin/bash

synergyUser="Administrator"
synergyPass="\$ystemAdm1n"
synergyURL="http://127.0.0.1:8080/Synergy"

# Журнал пересчетов
log_registryid="012cab7f-66ba-4371-aa7e-600606666320"
log_formid="7e3dae8b-49e9-4737-9435-c24e4db936df"

tmpfile="/tmp/recalc.msk"

curl --user $synergyUser:$synergyPass --request GET "$synergyURL/rest/api/registry/create_doc?registryID=$log_registryid" --output $tmpfile --silent
newUUID=`cat $tmpfile | sed "s/.*\dataUUID\": \"\(.*\)\", \"asfNodeID.*/\1/"`
curl --user $synergyUser:$synergyPass --request GET "$synergyURL/rest/api/registry/activate_doc?dataUUID=$newUUID" --output $tmpfile --silent

#cat $tmpfile
rm -f $tmpfile
