#!/bin/bash
scriptName="blockProblem"
synergyUser="Administrator"
synergyPass="\$ystemAdm1n"
synergyURL="http://127.0.0.1:8080/Synergy"

date1=$(date +%d.%m.%Y)
date2=$(date +%F%t%T)

# вспомогательные настройки
logFile="/var/log/synergy/scripts.log"
bpSQL1="/tmp/bpSQL1.msk"
bpSQL2="/tmp/bpSQL2.msk"
tmpfile="/tmp/bpData.msk"

# Реестр \Блок проблем\
registryid="4b963fe7-e6f1-4a87-9350-8b5225d62c4e"
formid="af0bcfdb-b4c3-472c-9c82-9b93b9dfb377"
# Реестр \01. Анкета\
regAnketa="6b9edece-ad7f-4478-aba4-80d456e7fea4"
# Реестры 02 и 03
registry02="dd58781b-cb75-44c0-a289-bac63428f1e2"
registry03="f7789302-1fb7-4b8e-8e53-5707def8c804"
# Реестр \98 Настройки компаний\
regCompany="d9b4245b-7be7-4a6d-b085-0b7943653a96"

# Функция получения текущего времени
function now_time() {
  date +"%Y-%m-%d %H:%M:%S"
}
# Функция логирования
function logging() {
  echo -e "`now_time` #$1# [$2] $3" >> $logFile
}

echo '' >> $logFile
logging $scriptName START "Начало работы скрипта"
logging $scriptName STATUS "Поиск компаний по которым были измененения в реестрах: анкета или 02 или 03"
# ищем новые или измененные записи в реестрах: анкета, 02 и 03
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT company.cmp_data, company.cmp_key
FROM registry_documents rg
JOIN asf_data_index company ON company.uuid=rg.asfDataID AND company.cmp_id='company'
JOIN asf_data ON asf_data.uuid=rg.asfDataID
JOIN (SELECT rg.documentid, parent.cmp_data, parent.cmp_key
FROM registry_documents rg JOIN asf_data_index parent ON parent.uuid=rg.asfDataID AND parent.cmp_id='parent'
WHERE rg.registryID='$regCompany' AND rg.deleted IS NULL AND rg.active='Y') parent ON parent.documentid=company.cmp_key
WHERE registryID IN ('$registry02', '$registry03', '$regAnketa')
AND rg.deleted IS NULL AND rg.active='Y' AND DATE(asf_data.modified)=DATE(NOW())
GROUP BY company.cmp_key;" > $bpSQL1

# если есть новые записи то пересчитать и записать результат в реестр блок проблем
if [ -s $bpSQL1 ];
then
  logging $scriptName RESULT ":\n `cat $bpSQL1`"
  # Количество строк в файле bpSQL1
  countStr=`cat $bpSQL1 | wc -l`

  for i in `seq 2 $countStr`;
  do
    companyKey=`cat $bpSQL1 | sed -n $i'p' | cut -f2`
    companyName=`cat $bpSQL1 | sed -n $i'p' | cut -f1`
    allCompanyKey+="'"`cat $bpSQL1 | sed -n $i'p' | cut -f2`"',"

    echo '' >> $logFile
    logging $scriptName INFO "companyName: $companyName, documentid: [$companyKey]"

    # сбор всех нерешенных проблем по компании
    logging $scriptName STATUS "сбор всех нерешенных проблем по компании"
    mysql -uroot -proot -e "use synergy; set names utf8;
    SELECT ProblemType,
    CASE WHEN CountDate BETWEEN 0 AND 30 THEN Vopros+Anketa
    WHEN CountDate BETWEEN 31 AND 60 THEN Vopros+ROUND(Anketa*0.95)
    WHEN CountDate BETWEEN 61 AND 90 THEN Vopros+ROUND(Anketa*0.90)
    WHEN CountDate BETWEEN 91 AND 120 THEN Vopros+ROUND(Anketa*0.85)
    WHEN CountDate BETWEEN 121 AND 150 THEN Vopros+ROUND(Anketa*0.80)
    WHEN CountDate BETWEEN 151 AND 180 THEN Vopros+ROUND(Anketa*0.70)
    WHEN CountDate BETWEEN 181 AND 210 THEN Vopros+ROUND(Anketa*0.60)
    WHEN CountDate BETWEEN 211 AND 240 THEN Vopros+ROUND(Anketa*0.50)
    WHEN CountDate BETWEEN 241 AND 270 THEN Vopros+ROUND(Anketa*0.40)
    WHEN CountDate BETWEEN 271 AND 300 THEN Vopros+ROUND(Anketa*0.30)
    WHEN CountDate BETWEEN 301 AND 330 THEN Vopros+ROUND(Anketa*0.20)
    WHEN CountDate > 330  THEN Vopros+ROUND(Anketa*0.10) END AS allProblem,
    Vopros, VoprosYear,
    CASE WHEN CountDate BETWEEN 0 AND 30 THEN Anketa
    WHEN CountDate BETWEEN 31 AND 60 THEN ROUND(Anketa*0.95)
    WHEN CountDate BETWEEN 61 AND 90 THEN ROUND(Anketa*0.90)
    WHEN CountDate BETWEEN 91 AND 120 THEN ROUND(Anketa*0.85)
    WHEN CountDate BETWEEN 121 AND 150 THEN ROUND(Anketa*0.80)
    WHEN CountDate BETWEEN 151 AND 180 THEN ROUND(Anketa*0.70)
    WHEN CountDate BETWEEN 181 AND 210 THEN ROUND(Anketa*0.60)
    WHEN CountDate BETWEEN 211 AND 240 THEN ROUND(Anketa*0.50)
    WHEN CountDate BETWEEN 241 AND 270 THEN ROUND(Anketa*0.40)
    WHEN CountDate BETWEEN 271 AND 300 THEN ROUND(Anketa*0.30)
    WHEN CountDate BETWEEN 301 AND 330 THEN ROUND(Anketa*0.20)
    WHEN CountDate > 330  THEN ROUND(Anketa*0.10) END AS Anketa

    FROM (SELECT
    (SELECT DATEDIFF(DATE(NOW()), MAX(DATE(registry_documents.created))) FROM registry_documents
    JOIN asf_data_index c ON c.uuid=registry_documents.asfDataID AND c.cmp_id='company'
    WHERE registryID='$regAnketa' AND c.cmp_key='$companyKey') AS CountDate,
    sdiv.div_value*1 AS ProblemType, IFNULL(t1.sumProblem,0) AS Vopros,
    IFNULL(t2.sumProblem,0) AS VoprosYear, IFNULL(t3.sumProblem,0) AS Anketa

    FROM sc_dictionary_item_value sdiv
    LEFT JOIN (SELECT company.cmp_data AS companyName, COUNT(*) AS sumProblem, blok.cmp_key AS ProblemKey
    FROM registry_documents rg
    JOIN asf_data_index company ON company.uuid=rg.asfDataID AND company.cmp_id='company'
    JOIN asf_data_index blok ON blok.uuid=rg.asfDataID AND blok.cmp_id LIKE 'Problem-%'
    JOIN asf_data_index status ON status.uuid=rg.asfDataID AND status.cmp_id=CONCAT('desicion-', SUBSTRING_INDEX(blok.cmp_id, '-', -1))
    WHERE rg.registryID IN ('$registry02', '$registry03') AND rg.deleted IS NULL AND rg.active='Y'
    AND status.cmp_key=0 AND company.cmp_key='$companyKey' GROUP BY blok.cmp_data) t1 ON t1.ProblemKey=sdiv.div_value

    LEFT JOIN (SELECT company.cmp_data AS companyName, COUNT(*) AS sumProblem, blok.cmp_key AS ProblemKey
    FROM registry_documents rg
    JOIN asf_data_index company ON company.uuid=rg.asfDataID AND company.cmp_id='company'
    JOIN asf_data_index blok ON blok.uuid=rg.asfDataID AND blok.cmp_id LIKE 'Problem-%'
    JOIN asf_data_index status ON status.uuid=rg.asfDataID AND status.cmp_id=CONCAT('desicion-', SUBSTRING_INDEX(blok.cmp_id, '-', -1))
    WHERE rg.registryID IN ('$registry02', '$registry03') AND rg.deleted IS NULL AND rg.active='Y'
    AND YEAR(rg.created)=YEAR(NOW()) AND company.cmp_key='$companyKey' GROUP BY blok.cmp_data) t2 ON t2.ProblemKey=sdiv.div_value

    LEFT JOIN (SELECT company.cmp_data AS companyName,
    SUM(CASE WHEN problem.cmp_key IN (1, 0) THEN 1
    WHEN problem.cmp_key=2 AND status.cmp_key=1 THEN 1 ELSE 0 END) AS sumProblem,
    SUBSTRING_INDEX(problem.cmp_id, 'problem_otvet', -1)*1 AS problemKey
    FROM registry_documents rg
    JOIN asf_data_index company ON company.uuid=rg.asfDataID AND company.cmp_id='company'
    JOIN asf_data_index problem ON problem.uuid=rg.asfDataID AND problem.cmp_id LIKE 'problem_otvet%'
    JOIN asf_data_index status ON status.uuid=rg.asfDataID AND status.cmp_id=CONCAT('block_importance', SUBSTRING_INDEX(problem.cmp_id, 'problem_otvet', -1))
    WHERE rg.registryID='$regAnketa' AND problem.cmp_key IN ('2','1','0') AND rg.deleted IS NULL AND rg.active='Y'
    AND company.cmp_key='$companyKey' GROUP BY problem.cmp_id) t3 ON t3.ProblemKey=sdiv.div_value
    WHERE sdiv.dc_id ='2f22224c-df0f-4483-bdd4-e83be8c21a85') t ORDER BY ProblemType;" > $bpSQL2
    logging $scriptName RESULT ":\n `cat $bpSQL2`"

    s1=`cat $bpSQL2 | sed -n 2p | cut -f2`
    s2=`cat $bpSQL2 | sed -n 3p | cut -f2`
    s3=`cat $bpSQL2 | sed -n 4p | cut -f2`
    s4=`cat $bpSQL2 | sed -n 5p | cut -f2`
    s5=`cat $bpSQL2 | sed -n 6p | cut -f2`
    s6=`cat $bpSQL2 | sed -n 7p | cut -f2`
    s7=`cat $bpSQL2 | sed -n 8p | cut -f2`
    s8=`cat $bpSQL2 | sed -n 9p | cut -f2`
    s9=`cat $bpSQL2 | sed -n 10p | cut -f2`
    s10=`cat $bpSQL2 | sed -n 11p | cut -f2`
    s11=`cat $bpSQL2 | sed -n 12p | cut -f2`
    s12=`cat $bpSQL2 | sed -n 13p | cut -f2`
    s13=`cat $bpSQL2 | sed -n 14p | cut -f2`

    v1=`cat $bpSQL2 | sed -n 2p | cut -f3`
    v2=`cat $bpSQL2 | sed -n 3p | cut -f3`
    v3=`cat $bpSQL2 | sed -n 4p | cut -f3`
    v4=`cat $bpSQL2 | sed -n 5p | cut -f3`
    v5=`cat $bpSQL2 | sed -n 6p | cut -f3`
    v6=`cat $bpSQL2 | sed -n 7p | cut -f3`
    v7=`cat $bpSQL2 | sed -n 8p | cut -f3`
    v8=`cat $bpSQL2 | sed -n 9p | cut -f3`
    v9=`cat $bpSQL2 | sed -n 10p | cut -f3`
    v10=`cat $bpSQL2 | sed -n 11p | cut -f3`
    v11=`cat $bpSQL2 | sed -n 12p | cut -f3`
    v12=`cat $bpSQL2 | sed -n 13p | cut -f3`
    v13=`cat $bpSQL2 | sed -n 14p | cut -f3`

    vy1=`cat $bpSQL2 | sed -n 2p | cut -f4`
    vy2=`cat $bpSQL2 | sed -n 3p | cut -f4`
    vy3=`cat $bpSQL2 | sed -n 4p | cut -f4`
    vy4=`cat $bpSQL2 | sed -n 5p | cut -f4`
    vy5=`cat $bpSQL2 | sed -n 6p | cut -f4`
    vy6=`cat $bpSQL2 | sed -n 7p | cut -f4`
    vy7=`cat $bpSQL2 | sed -n 8p | cut -f4`
    vy8=`cat $bpSQL2 | sed -n 9p | cut -f4`
    vy9=`cat $bpSQL2 | sed -n 10p | cut -f4`
    vy10=`cat $bpSQL2 | sed -n 11p | cut -f4`
    vy11=`cat $bpSQL2 | sed -n 12p | cut -f4`
    vy12=`cat $bpSQL2 | sed -n 13p | cut -f4`
    vy13=`cat $bpSQL2 | sed -n 14p | cut -f4`

    a1=`cat $bpSQL2 | sed -n 2p | cut -f5`
    a2=`cat $bpSQL2 | sed -n 3p | cut -f5`
    a3=`cat $bpSQL2 | sed -n 4p | cut -f5`
    a4=`cat $bpSQL2 | sed -n 5p | cut -f5`
    a5=`cat $bpSQL2 | sed -n 6p | cut -f5`
    a6=`cat $bpSQL2 | sed -n 7p | cut -f5`
    a7=`cat $bpSQL2 | sed -n 8p | cut -f5`
    a8=`cat $bpSQL2 | sed -n 9p | cut -f5`
    a9=`cat $bpSQL2 | sed -n 10p | cut -f5`
    a10=`cat $bpSQL2 | sed -n 11p | cut -f5`
    a11=`cat $bpSQL2 | sed -n 12p | cut -f5`
    a12=`cat $bpSQL2 | sed -n 13p | cut -f5`
    a13=`cat $bpSQL2 | sed -n 14p | cut -f5`

    # формирование правильной строки data
    newData='KAKAKA"data":[{"id":"cmp-6gnzza","type":"label","label":"Блоки проблем","value":" "},{"id":"cmp-wra2lq","type":"label","label":"Компания","value":" "},{"id":"company","type":"reglink","value":"'$companyName'","key":"'$companyKey'","valueID":"'$companyKey'","username":"Admin Admin Admin","userID":"1"},{"id":"cmp-c4s015","type":"label","label":"Дата","value":" "},{"id":"date","type":"date","value":"'$date1'","key":"'$date2'"},{"id":"block","type":"label","label":"Блоки проблем","value":" "},{"id":"value","type":"label","label":"Общий показатель","value":" "},{"id":"circulation","type":"label","label":"Обращения и вопросы сотрудников", "value":" "},{"id":"cmp-xlwr7r","type":"label","label":"Всего обращений за год","value":" "},{"id":"questionnaire","type":"label","label":"Неудовлетворенность работников различными компонентами работы на предприятии (анкета)","value":" "},{"id":"block1","type":"listbox","value":"Жилищные условия","key":"1"},{"id":"value1","type":"textbox","value":"'$s1'"},{"id":"circulation1","type":"textbox","value":"'$v1'"},{"id":"all_1","type":"textbox","value":"'$vy1'"},{"id":"questionnaire1","type":"textbox","value":"'$a1'"},{"id":"block2","type":"listbox","value":"Служебный транспорт","key":"2"},{"id":"value2","type":"textbox","value":"'$s2'"},{"id":"circulation2","type":"textbox","value":"'$v2'"},{"id":"all_2","type":"textbox","value":"'$vy2'"},{"id":"questionnaire2","type":"textbox","value":"'$a2'"},{"id":"block3","type":"listbox","value":"Общественное питание","key":"3"},{"id":"value3","type":"textbox","value":"'$s3'"},{"id":"circulation3","type":"textbox","value":"'$v3'"},{"id":"all_3","type":"textbox","value":"'$vy3'"},{"id":"questionnaire3","type":"textbox","value":"'$a3'"},{"id":"block4","type":"listbox","value":"Оснащение рабочего места","key":"4"},{"id":"value4","type":"textbox","value":"'$s4'"},{"id":"circulation4","type":"textbox","value":"'$v4'"},{"id":"all_4","type":"textbox","value":"'$vy4'"},{"id":"questionnaire4","type":"textbox","value":"'$a4'"},{"id":"block5","type":"listbox","value":"Качество и наличие СИЗ","key":"5"},{"id":"value5","type":"textbox","value":"'$s5'"},{"id":"circulation5","type":"textbox","value":"'$v5'"},{"id":"all_5","type":"textbox","value":"'$vy5'"},{"id":"questionnaire5","type":"textbox","value":"'$a5'"},{"id":"block6","type":"listbox","value":"Условия и безопасность труда","key":"6"},{"id":"value6","type":"textbox","value":"'$s6'"},{"id":"circulation6","type":"textbox","value":"'$v6'"},{"id":"all_6","type":"textbox","value":"'$vy6'"},{"id":"questionnaire6","type":"textbox","value":"'$a6'"},{"id":"block7","type":"listbox","value":"Заработная плата и система мотивации","key":"7"},{"id":"value7","type":"textbox","value":"'$s7'"},{"id":"circulation7","type":"textbox","value":"'$v7'"},{"id":"all_7","type":"textbox","value":"'$vy7'"},{"id":"questionnaire7","type":"textbox","value":"'$a7'"},{"id":"block8","type":"listbox","value":"Карьерный рост, повышение квалификации","key":"8"},{"id":"value8","type":"textbox","value":"'$s8'"},{"id":"circulation8","type":"textbox","value":"'$v8'"},{"id":"all_8","type":"textbox","value":"'$vy8'"},{"id":"questionnaire8","type":"textbox","value":"'$a8'"},{"id":"block9","type":"listbox","value":"Социальный пакет","key":"9"},{"id":"value9","type":"textbox","value":"'$s9'"},{"id":"circulation9","type":"textbox","value":"'$v9'"},{"id":"all_9","type":"textbox","value":"'$vy9'"},{"id":"questionnaire9","type":"textbox","value":"'$a9'"},{"id":"block10","type":"listbox","value":"Содержание работы","key":"10"},{"id":"value10","type":"textbox","value":"'$s10'"},{"id":"circulation10","type":"textbox","value":"'$v10'"},{"id":"all_10","type":"textbox","value":"'$vy10'"},{"id":"questionnaire10","type":"textbox","value":"'$a10'"},{"id":"block11","type":"listbox","value":"Внутренние служебные коммуникации","key":"11"},{"id":"value11","type":"textbox","value":"'$s11'"},{"id":"circulation11","type":"textbox","value":"'$v11'"},{"id":"all_11","type":"textbox","value":"'$vy11'"},{"id":"questionnaire11","type":"textbox","value":"'$a11'"},{"id":"block12","type":"listbox","value":"Организация досуга","key":"12"},{"id":"value12","type":"textbox","value":"'$s12'"},{"id":"circulation12","type":"textbox","value":"'$v12'"},{"id":"all_12","type":"textbox","value":"'$vy12'"},{"id":"questionnaire12","type":"textbox","value":"'$a12'"},{"id":"block13","type":"listbox","value":"Другое","key":"13"},{"id":"value13","type":"textbox","value":"'$s13'"},{"id":"circulation13","type":"textbox","value":"'$v13'"},{"id":"all_13","type":"textbox","value":"'$vy13'"},{"id":"questionnaire13","type":"textbox","value":"'$a13'"}]'
    newData=$(echo $newData | sed 's/KAKAKA//')
    logging $scriptName JSON "Итоговый JSON для передачи в API rest/api/asforms/data/save\n`echo $newData`"
    echo '' >> $logFile

    # создание записи реестра
    logging $scriptName STATUS "Создание записи реестра"
    #wget --quiet --http-user="$synergyUser" --http-password="$synergyPass" --output-document $tmpfile "$synergyURL/rest/api/registry/create_doc?registryID=$registryid"
    curl --user $synergyUser:$synergyPass --request GET "$synergyURL/rest/api/registry/create_doc?registryID=$registryid" --output $tmpfile --silent
    logging $scriptName RESULT "`cat $tmpfile`"

    # берем uuid созданной записи
    newUUID=`cat $tmpfile | sed "s/.*\dataUUID\": \"\(.*\)\", \"asfNodeID.*/\1/"`

    # выполняем api метод POST (изменяем значение поля на форме)
    logging $scriptName STATUS "Сохранение данных в новую запись"
    curl --user $synergyUser:$synergyPass --request POST "$synergyURL/rest/api/asforms/data/save" --data "formUUID=$formid" --data "uuid=$newUUID" --data-urlencode "data=$newData" --output $tmpfile --silent
    logging $scriptName RESULT "`cat $tmpfile`"

    # активация записи реестра
    logging $scriptName STATUS "Активация записи реестра"
    #wget --quiet --http-user="$synergyUser" --http-password="$synergyPass" --output-document $tmpfile "$synergyURL/rest/api/registry/activate_doc?dataUUID=$newUUID"
    curl --user $synergyUser:$synergyPass --request GET "$synergyURL/rest/api/registry/activate_doc?dataUUID=$newUUID" --output $tmpfile --silent
    logging $scriptName RESULT "`cat $tmpfile`"
  done

  # проверяем были ли созданы новые записи по дочерним компаниям, если да то формируем запись по родителю
  if [ "$allCompanyKey" ]
  then
    # Массив ID дочерних компаний
    allCompanyKey=`echo $allCompanyKey | rev | sed 's/,//' | rev`
    echo '------------------------------------------------------------------------' >> $logFile
    logging $scriptName INFO "Пересчет по родителям"
    logging $scriptName INFO "Массив ID дочерних компаний:\n[$allCompanyKey]"

    # Поиск всех родителей
    logging $scriptName STATUS "Поиск всех родителей"
    mysql -uroot -proot -e "use synergy; set names utf8;
    SELECT parent.cmp_data, parent.cmp_key
    FROM registry_documents rg JOIN asf_data_index parent ON parent.uuid=rg.asfDataID AND parent.cmp_id='parent'
    WHERE rg.registryID='$regCompany' AND rg.deleted IS NULL AND rg.active='Y' AND rg.documentid IN ($allCompanyKey)
    GROUP BY parent.cmp_key;" > $bpSQL1
    logging $scriptName RESULT "Родители:\n `cat $bpSQL1`"

    # проверяем, есть ли родители для которых нужно посчитать
    if [ -s $bpSQL1 ];
    then
      countStr=`cat $bpSQL1 | wc -l`
      for i in `seq 2 $countStr`;
      do
        parentKey=`cat $bpSQL1 | sed -n $i'p' | cut -f2`
        parentName=`cat $bpSQL1 | sed -n $i'p' | cut -f1`
        echo '' >> $logFile
        logging $scriptName INFO "parentName: $parentName, documentid: [$parentKey]"
        logging $scriptName STATUS "Пересчет проблем"
        # сумма проблем дочерних компаний родителя
        mysql -uroot -proot -e "use synergy; set names utf8;
        SELECT problem.cmp_key, SUM(a.cmp_data)+SUM(c.cmp_data) AS allProblem,
        SUM(a.cmp_data) AS vopros, SUM(b.cmp_data) AS voprosYear, SUM(c.cmp_data) AS anketa
        FROM registry_documents rg
        LEFT JOIN asf_data_index problem ON problem.uuid=rg.asfDataID AND problem.cmp_id LIKE 'block%'
        LEFT JOIN asf_data_index a ON a.uuid=rg.asfDataID AND a.cmp_id=CONCAT('circulation', SUBSTRING(problem.cmp_id, 6))
        LEFT JOIN asf_data_index b ON b.uuid=rg.asfDataID AND b.cmp_id=CONCAT('all_', SUBSTRING(problem.cmp_id, 6))
        LEFT JOIN asf_data_index c ON c.uuid=rg.asfDataID AND c.cmp_id=CONCAT('questionnaire', SUBSTRING(problem.cmp_id, 6))
        WHERE rg.registryID='$registryid' AND rg.deleted IS NULL AND rg.active='Y'
        AND rg.asfDataID IN (SELECT
        (SELECT asfDataID FROM registry_documents
        JOIN asf_data_index company ON company.uuid=registry_documents.asfDataID AND company.cmp_id='company'
        WHERE registryID='$registryid' AND company.cmp_key=rg.documentID AND registry_documents.deleted IS NULL AND registry_documents.active='Y'
        ORDER BY registry_documents.created DESC LIMIT 1)
        FROM registry_documents rg
        JOIN asf_data_index parent ON parent.uuid=rg.asfDataID AND parent.cmp_id='parent'
        JOIN asf_data_index company ON company.uuid=rg.asfDataID AND company.cmp_id='settings_name'
        WHERE rg.registryID='$regCompany' AND rg.deleted IS NULL AND rg.active='Y' AND parent.cmp_key='$parentKey')
        GROUP BY problem.cmp_key ORDER BY problem.cmp_key * 1;" > $bpSQL2
        logging $scriptName RESULT ":\n `cat $bpSQL2`"
        echo '' >> $logFile

        s1=`cat $bpSQL2 | sed -n 2p | cut -f2`
        s2=`cat $bpSQL2 | sed -n 3p | cut -f2`
        s3=`cat $bpSQL2 | sed -n 4p | cut -f2`
        s4=`cat $bpSQL2 | sed -n 5p | cut -f2`
        s5=`cat $bpSQL2 | sed -n 6p | cut -f2`
        s6=`cat $bpSQL2 | sed -n 7p | cut -f2`
        s7=`cat $bpSQL2 | sed -n 8p | cut -f2`
        s8=`cat $bpSQL2 | sed -n 9p | cut -f2`
        s9=`cat $bpSQL2 | sed -n 10p | cut -f2`
        s10=`cat $bpSQL2 | sed -n 11p | cut -f2`
        s11=`cat $bpSQL2 | sed -n 12p | cut -f2`
        s12=`cat $bpSQL2 | sed -n 13p | cut -f2`
        s13=`cat $bpSQL2 | sed -n 14p | cut -f2`

        v1=`cat $bpSQL2 | sed -n 2p | cut -f3`
        v2=`cat $bpSQL2 | sed -n 3p | cut -f3`
        v3=`cat $bpSQL2 | sed -n 4p | cut -f3`
        v4=`cat $bpSQL2 | sed -n 5p | cut -f3`
        v5=`cat $bpSQL2 | sed -n 6p | cut -f3`
        v6=`cat $bpSQL2 | sed -n 7p | cut -f3`
        v7=`cat $bpSQL2 | sed -n 8p | cut -f3`
        v8=`cat $bpSQL2 | sed -n 9p | cut -f3`
        v9=`cat $bpSQL2 | sed -n 10p | cut -f3`
        v10=`cat $bpSQL2 | sed -n 11p | cut -f3`
        v11=`cat $bpSQL2 | sed -n 12p | cut -f3`
        v12=`cat $bpSQL2 | sed -n 13p | cut -f3`
        v13=`cat $bpSQL2 | sed -n 14p | cut -f3`

        vy1=`cat $bpSQL2 | sed -n 2p | cut -f4`
        vy2=`cat $bpSQL2 | sed -n 3p | cut -f4`
        vy3=`cat $bpSQL2 | sed -n 4p | cut -f4`
        vy4=`cat $bpSQL2 | sed -n 5p | cut -f4`
        vy5=`cat $bpSQL2 | sed -n 6p | cut -f4`
        vy6=`cat $bpSQL2 | sed -n 7p | cut -f4`
        vy7=`cat $bpSQL2 | sed -n 8p | cut -f4`
        vy8=`cat $bpSQL2 | sed -n 9p | cut -f4`
        vy9=`cat $bpSQL2 | sed -n 10p | cut -f4`
        vy10=`cat $bpSQL2 | sed -n 11p | cut -f4`
        vy11=`cat $bpSQL2 | sed -n 12p | cut -f4`
        vy12=`cat $bpSQL2 | sed -n 13p | cut -f4`
        vy13=`cat $bpSQL2 | sed -n 14p | cut -f4`

        a1=`cat $bpSQL2 | sed -n 2p | cut -f5`
        a2=`cat $bpSQL2 | sed -n 3p | cut -f5`
        a3=`cat $bpSQL2 | sed -n 4p | cut -f5`
        a4=`cat $bpSQL2 | sed -n 5p | cut -f5`
        a5=`cat $bpSQL2 | sed -n 6p | cut -f5`
        a6=`cat $bpSQL2 | sed -n 7p | cut -f5`
        a7=`cat $bpSQL2 | sed -n 8p | cut -f5`
        a8=`cat $bpSQL2 | sed -n 9p | cut -f5`
        a9=`cat $bpSQL2 | sed -n 10p | cut -f5`
        a10=`cat $bpSQL2 | sed -n 11p | cut -f5`
        a11=`cat $bpSQL2 | sed -n 12p | cut -f5`
        a12=`cat $bpSQL2 | sed -n 13p | cut -f5`
        a13=`cat $bpSQL2 | sed -n 14p | cut -f5`

        # формирование правильной строки data
        newData='KAKAKA"data":[{"id":"cmp-6gnzza","type":"label","label":"Блоки проблем","value":" "},{"id":"cmp-wra2lq","type":"label","label":"Компания","value":" "},{"id":"company","type":"reglink","value":"'$parentName'","key":"'$parentKey'","valueID":"'$parentKey'","username":"Admin Admin Admin","userID":"1"},{"id":"cmp-c4s015","type":"label","label":"Дата","value":" "},{"id":"date","type":"date","value":"'$date1'","key":"'$date2'"},{"id":"block","type":"label","label":"Блоки проблем","value":" "},{"id":"value","type":"label","label":"Общий показатель","value":" "},{"id":"circulation","type":"label","label":"Обращения и вопросы сотрудников", "value":" "},{"id":"cmp-xlwr7r","type":"label","label":"Всего обращений за год","value":" "},{"id":"questionnaire","type":"label","label":"Неудовлетворенность работников различными компонентами работы на предприятии (анкета)","value":" "},{"id":"block1","type":"listbox","value":"Жилищные условия","key":"1"},{"id":"value1","type":"textbox","value":"'$s1'"},{"id":"circulation1","type":"textbox","value":"'$v1'"},{"id":"all_1","type":"textbox","value":"'$vy1'"},{"id":"questionnaire1","type":"textbox","value":"'$a1'"},{"id":"block2","type":"listbox","value":"Служебный транспорт","key":"2"},{"id":"value2","type":"textbox","value":"'$s2'"},{"id":"circulation2","type":"textbox","value":"'$v2'"},{"id":"all_2","type":"textbox","value":"'$vy2'"},{"id":"questionnaire2","type":"textbox","value":"'$a2'"},{"id":"block3","type":"listbox","value":"Общественное питание","key":"3"},{"id":"value3","type":"textbox","value":"'$s3'"},{"id":"circulation3","type":"textbox","value":"'$v3'"},{"id":"all_3","type":"textbox","value":"'$vy3'"},{"id":"questionnaire3","type":"textbox","value":"'$a3'"},{"id":"block4","type":"listbox","value":"Оснащение рабочего места","key":"4"},{"id":"value4","type":"textbox","value":"'$s4'"},{"id":"circulation4","type":"textbox","value":"'$v4'"},{"id":"all_4","type":"textbox","value":"'$vy4'"},{"id":"questionnaire4","type":"textbox","value":"'$a4'"},{"id":"block5","type":"listbox","value":"Качество и наличие СИЗ","key":"5"},{"id":"value5","type":"textbox","value":"'$s5'"},{"id":"circulation5","type":"textbox","value":"'$v5'"},{"id":"all_5","type":"textbox","value":"'$vy5'"},{"id":"questionnaire5","type":"textbox","value":"'$a5'"},{"id":"block6","type":"listbox","value":"Условия и безопасность труда","key":"6"},{"id":"value6","type":"textbox","value":"'$s6'"},{"id":"circulation6","type":"textbox","value":"'$v6'"},{"id":"all_6","type":"textbox","value":"'$vy6'"},{"id":"questionnaire6","type":"textbox","value":"'$a6'"},{"id":"block7","type":"listbox","value":"Заработная плата и система мотивации","key":"7"},{"id":"value7","type":"textbox","value":"'$s7'"},{"id":"circulation7","type":"textbox","value":"'$v7'"},{"id":"all_7","type":"textbox","value":"'$vy7'"},{"id":"questionnaire7","type":"textbox","value":"'$a7'"},{"id":"block8","type":"listbox","value":"Карьерный рост, повышение квалификации","key":"8"},{"id":"value8","type":"textbox","value":"'$s8'"},{"id":"circulation8","type":"textbox","value":"'$v8'"},{"id":"all_8","type":"textbox","value":"'$vy8'"},{"id":"questionnaire8","type":"textbox","value":"'$a8'"},{"id":"block9","type":"listbox","value":"Социальный пакет","key":"9"},{"id":"value9","type":"textbox","value":"'$s9'"},{"id":"circulation9","type":"textbox","value":"'$v9'"},{"id":"all_9","type":"textbox","value":"'$vy9'"},{"id":"questionnaire9","type":"textbox","value":"'$a9'"},{"id":"block10","type":"listbox","value":"Содержание работы","key":"10"},{"id":"value10","type":"textbox","value":"'$s10'"},{"id":"circulation10","type":"textbox","value":"'$v10'"},{"id":"all_10","type":"textbox","value":"'$vy10'"},{"id":"questionnaire10","type":"textbox","value":"'$a10'"},{"id":"block11","type":"listbox","value":"Внутренние служебные коммуникации","key":"11"},{"id":"value11","type":"textbox","value":"'$s11'"},{"id":"circulation11","type":"textbox","value":"'$v11'"},{"id":"all_11","type":"textbox","value":"'$vy11'"},{"id":"questionnaire11","type":"textbox","value":"'$a11'"},{"id":"block12","type":"listbox","value":"Организация досуга","key":"12"},{"id":"value12","type":"textbox","value":"'$s12'"},{"id":"circulation12","type":"textbox","value":"'$v12'"},{"id":"all_12","type":"textbox","value":"'$vy12'"},{"id":"questionnaire12","type":"textbox","value":"'$a12'"},{"id":"block13","type":"listbox","value":"Другое","key":"13"},{"id":"value13","type":"textbox","value":"'$s13'"},{"id":"circulation13","type":"textbox","value":"'$v13'"},{"id":"all_13","type":"textbox","value":"'$vy13'"},{"id":"questionnaire13","type":"textbox","value":"'$a13'"}]'
        newData=$(echo $newData | sed 's/KAKAKA//')
        logging $scriptName JSON "Итоговый JSON для передачи в API rest/api/asforms/data/save\n`echo $newData`"
        # создание записи реестра
        logging $scriptName STATUS "Создание записи реестра"
        #wget --quiet --http-user="$synergyUser" --http-password="$synergyPass" --output-document $tmpfile "$synergyURL/rest/api/registry/create_doc?registryID=$registryid"
        curl --user $synergyUser:$synergyPass --request GET "$synergyURL/rest/api/registry/create_doc?registryID=$registryid" --output $tmpfile --silent
        logging $scriptName RESULT "`cat $tmpfile`"
        # берем uuid созданной записи
        newUUID=`cat $tmpfile | sed "s/.*\dataUUID\": \"\(.*\)\", \"asfNodeID.*/\1/"`
        # выполняем api метод POST
        logging $scriptName STATUS "Сохранение данных в новую запись"
        curl --user $synergyUser:$synergyPass --request POST "$synergyURL/rest/api/asforms/data/save" --data "formUUID=$formid" --data "uuid=$newUUID" --data-urlencode "data=$newData" --output $tmpfile --silent
        logging $scriptName RESULT "`cat $tmpfile`"
        # активация записи реестра
        logging $scriptName STATUS "Активация записи реестра"
        #wget --quiet --http-user="$synergyUser" --http-password="$synergyPass" --output-document $tmpfile "$synergyURL/rest/api/registry/activate_doc?dataUUID=$newUUID"
        curl --user $synergyUser:$synergyPass --request GET "$synergyURL/rest/api/registry/activate_doc?dataUUID=$newUUID" --output $tmpfile --silent
        logging $scriptName RESULT "`cat $tmpfile`"
      done
    fi
  fi
else
  logging $scriptName INFO "нет новых данных по компаниям"
fi

# удаление временных файлов
logging $scriptName STATUS "Удаление временных файлов"
rm -f $bpSQL1
rm -f $bpSQL2
rm -f $tmpfile

logging $scriptName END "Завершение работы скрипта"
