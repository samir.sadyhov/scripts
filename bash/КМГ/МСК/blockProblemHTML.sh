#!/bin/bash
scriptName="blockProblemHTML"
synergyUser="Administrator"
synergyPass="\$ystemAdm1n"
synergyURL="http://127.0.0.1:8080/Synergy"

date1=$(date +%Y-%m-%d)
date2=$(date +%F%t%T)

# вспомогательные настройки
logFile="/var/log/synergy/scripts.log"
tmpSQL1="/tmp/bpHSQL1.msk"
tmpSQL2="/tmp/bpHSQL2.msk"
tmpSQL3="/tmp/bpHSQL3.msk"
tmpfile="/tmp/bpHData.msk"

# Реестр \Блок проблем (по первой группе риска)\
registryid="4068350e-52be-4d1e-b179-260fc8c32566"
formid="1a9391d9-fe79-4253-88f7-95e72d4f8a80"
# Реестр \01. Анкета\
regAnketa="6b9edece-ad7f-4478-aba4-80d456e7fea4"
# Реестр \98 Настройки компаний\
regCompany="d9b4245b-7be7-4a6d-b085-0b7943653a96"

# Функция получения текущего времени
function now_time() {
  date +"%Y-%m-%d %H:%M:%S"
}
# Функция логирования
function logging() {
  echo -e "`now_time` #$1# [$2] $3" >> $logFile
}

echo '' >> $logFile
logging $scriptName START " Начало работы скрипта"
logging $scriptName STATUS "Поиск компаний по которым были добавлены (изменены) анкеты"
# ищем новые или измененные записи в реестре: 01. анкета
my1="use synergy; set names utf8;
SELECT company.cmp_data companyName, company.cmp_key companyKey
FROM registry_documents rg
JOIN asf_data_index company ON company.uuid=rg.asfDataID AND company.cmp_id='company'
JOIN asf_data ON asf_data.uuid=rg.asfDataID
JOIN (SELECT rg.documentid, parent.cmp_data, parent.cmp_key
FROM registry_documents rg JOIN asf_data_index parent ON parent.uuid=rg.asfDataID AND parent.cmp_id='parent'
WHERE rg.registryID='$regCompany' AND rg.deleted IS NULL AND rg.active='Y') parent ON parent.documentid=company.cmp_key
WHERE rg.registryID='$regAnketa'
AND rg.deleted IS NULL AND rg.active='Y' AND DATE(asf_data.modified)=DATE(NOW())
GROUP BY company.cmp_key;"
logging $scriptName MYSQL "$my1"
mysql -uroot -proot -e "$my1" > $tmpSQL1

# если есть новые записи то пересчитать и записать результат в реестр блок проблем
if [ -s $tmpSQL1 ];
then
  logging $scriptName RESULT ":\n `cat $tmpSQL1`"
  # Количество строк в файле tmpSQL1
  countStr=`cat $tmpSQL1 | wc -l`
  for i in `seq 2 $countStr`;
  do
    companyKey=`cat $tmpSQL1 | sed -n $i'p' | cut -f2`
    companyName=`cat $tmpSQL1 | sed -n $i'p' | cut -f1`
    allCompanyKey+="'"`cat $tmpSQL1 | sed -n $i'p' | cut -f2`"',"
    echo '' >> $logFile
    logging $scriptName INFO "companyName: $companyName, documentid: [$companyKey]"
    logging $scriptName STATUS "Поиск анкет по данной компании входящии в 1 группу риска"
    # собираем все анкеты, входящии в 1 группу риска, по компании
    mysql -uroot -proot -e "use synergy; set names utf8;
    SELECT asfDataID
    FROM (SELECT rg.asfDataID, konflict.Pakt, konflict.Pind, konflict.Pcrim,
    (SELECT CASE WHEN SUM(cmp_key) / 19 < 2 THEN 1 WHEN SUM(cmp_key) / 19 >= 2 AND SUM(cmp_key) / 19 < 3 THEN 2 ELSE 3 END
    FROM asf_data_index WHERE uuid = rg.asfDataID  AND cmp_id LIKE 'frustrated_otvet%') AS A1,
    (SELECT CASE
    WHEN SUM(IF(problem.cmp_key=0 AND status.cmp_key=1, 1, 0)) > 2 THEN 1
    WHEN SUM(IF(problem.cmp_key=0 AND status.cmp_key=1, 1, 0)) = 2 THEN 2
    WHEN SUM(problem.cmp_key) BETWEEN 0 AND 16 THEN 3
    WHEN SUM(problem.cmp_key) BETWEEN 17 AND 32 THEN 2
    WHEN SUM(problem.cmp_key) > 32 THEN 1 END
    FROM asf_data_index problem JOIN asf_data_index status ON status.uuid=problem.uuid
    WHERE problem.uuid=rg.asfDataID AND problem.cmp_id LIKE 'problem_otvet%'
    AND status.cmp_id=CONCAT('block_importance', SUBSTRING_INDEX(problem.cmp_id, 'problem_otvet', -1))) AS A4
    FROM registry_documents rg
    JOIN asf_data_index company ON company.uuid=rg.asfDataID AND company.cmp_id='company'
    LEFT JOIN (SELECT uuid, IF(SUM(IF(cmp_key=1, 1, 0))>0, 1, 0) AS Pakt,
    IF(SUM(IF(cmp_key=2, 1, 0))>1, 1, 0) AS Pcrim, IF(SUM(IF(cmp_key=6, 1, 0))>0, 1, 0) AS Pind
    FROM asf_data_index WHERE cmp_id LIKE 'conflict_otvet%' GROUP BY uuid) konflict ON konflict.uuid=rg.asfDataID
    WHERE rg.registryID='$regAnketa' AND company.cmp_key='$companyKey'
    AND rg.deleted IS NULL AND rg.active='Y' AND YEAR(rg.created)=YEAR(NOW())
    AND (konflict.Pakt=1 OR konflict.Pind=1 OR konflict.Pcrim=1)
    GROUP BY rg.asfDataID) t
    WHERE (A1=3 AND Pakt=1 AND A4=3) OR (A1=3 AND Pcrim=1 AND A4=3) OR (A1=3 AND Pind=1 AND A4=3)
    OR (A1=3 AND Pakt=1 AND A4=2) OR (A1=1 AND Pakt=1 AND A4=3) OR (A1=1 AND Pcrim=1 AND A4=3)
    OR (A1=2 AND Pakt=1 AND A4=3) OR (A1=2 AND Pcrim=1 AND A4=3) OR (A1=2 AND Pind=1 AND A4=3);" > $tmpSQL2

    if [ -s $tmpSQL2 ];
    then
      countStr2=`cat $tmpSQL2 | wc -l`
      # массив анкет (asfDataID)
      for j in `seq 2 $countStr2`;
      do
        allAnket+="'"`cat $tmpSQL2 | sed -n $j'p' | cut -f2`"',"
      done
      # проверка на наличие анкет по компании
      if [ "$allAnket" ]
      then
        allAnket=`echo $allAnket | rev | sed 's/,//' | rev`
        logging $scriptName INFO "Массив анкет (asfDataID):\n[$allAnket]"

        logging $scriptName STATUS "Сбор данных по анкетам"
        # пересчет всех проблем по анкетам
        mysql -uroot -proot -e "use synergy; set names utf8;
        SELECT Type,
        CASE WHEN CountDate BETWEEN 0 AND 30 THEN Anketa
        WHEN CountDate BETWEEN 31 AND 60 THEN ROUND(Anketa*0.95)
        WHEN CountDate BETWEEN 61 AND 90 THEN ROUND(Anketa*0.90)
        WHEN CountDate BETWEEN 91 AND 120 THEN ROUND(Anketa*0.85)
        WHEN CountDate BETWEEN 121 AND 150 THEN ROUND(Anketa*0.80)
        WHEN CountDate BETWEEN 151 AND 180 THEN ROUND(Anketa*0.70)
        WHEN CountDate BETWEEN 181 AND 210 THEN ROUND(Anketa*0.60)
        WHEN CountDate BETWEEN 211 AND 240 THEN ROUND(Anketa*0.50)
        WHEN CountDate BETWEEN 241 AND 270 THEN ROUND(Anketa*0.40)
        WHEN CountDate BETWEEN 271 AND 300 THEN ROUND(Anketa*0.30)
        WHEN CountDate BETWEEN 301 AND 330 THEN ROUND(Anketa*0.20)
        WHEN CountDate > 330  THEN ROUND(Anketa*0.10) END AS Problem
        FROM (SELECT
        (SELECT DATEDIFF(DATE(NOW()), MAX(DATE(registry_documents.created))) FROM registry_documents
        JOIN asf_data_index c ON c.uuid=registry_documents.asfDataID AND c.cmp_id='company'
        WHERE registryID='$regAnketa' AND c.cmp_key='$companyKey') AS CountDate,
        sdiv.div_value*1 AS Type, IFNULL(t2.sumProblem,0) AS Anketa
        FROM sc_dictionary_item_value sdiv
        LEFT JOIN (SELECT
        SUM(CASE WHEN problem.cmp_key IN (1, 0) THEN 1 WHEN problem.cmp_key=2 AND status.cmp_key=1 THEN 1 ELSE 0 END) AS sumProblem,
        SUBSTRING_INDEX(problem.cmp_id, 'problem_otvet', -1)*1 AS problemKey
        FROM asf_data_index problem JOIN asf_data_index status ON status.uuid=problem.uuid
        WHERE problem.cmp_id LIKE 'problem_otvet%' AND problem.cmp_key IN ('2','1','0')
        AND status.cmp_id=CONCAT('block_importance', SUBSTRING_INDEX(problem.cmp_id, 'problem_otvet', -1))
        AND problem.uuid IN ($allAnket) GROUP BY problem.cmp_id) t2 ON t2.ProblemKey=sdiv.div_value
        WHERE sdiv.dc_id ='2f22224c-df0f-4483-bdd4-e83be8c21a85') t ORDER BY Type;" > $tmpSQL3

        if [ -s $tmpSQL3 ];
        then
          logging $scriptName RESULT "Проблемы по анкетам\n `cat $tmpSQL3`"
          a1=`cat $tmpSQL3 | sed -n 2p | cut -f2`
          a2=`cat $tmpSQL3 | sed -n 3p | cut -f2`
          a3=`cat $tmpSQL3 | sed -n 4p | cut -f2`
          a4=`cat $tmpSQL3 | sed -n 5p | cut -f2`
          a5=`cat $tmpSQL3 | sed -n 6p | cut -f2`
          a6=`cat $tmpSQL3 | sed -n 7p | cut -f2`
          a7=`cat $tmpSQL3 | sed -n 8p | cut -f2`
          a8=`cat $tmpSQL3 | sed -n 9p | cut -f2`
          a9=`cat $tmpSQL3 | sed -n 10p | cut -f2`
          a10=`cat $tmpSQL3 | sed -n 11p | cut -f2`
          a11=`cat $tmpSQL3 | sed -n 12p | cut -f2`
          a12=`cat $tmpSQL3 | sed -n 13p | cut -f2`
          a13=`cat $tmpSQL3 | sed -n 14p | cut -f2`

          # формируем html
          textHtml="<html><head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'></head><body><style type='text/css'> #tableDataBP {border-collapse: collapse;} #tableDataBP thead th {border: 1px solid #000; padding: 5px; cursor: pointer; background-color: #6ed3f3;} #tableDataBP thead th:hover {color: blue;} #tableDataBP tbody td {border: 1px solid #000; padding: 5px;} #tableDataBP tbody tr:hover {background-color: #f3fbfd;}</style><table id='tableDataBP'><caption><h2>"$companyName"</h2></caption><thead><tr><th ata-type='string' align='center'>Блоки проблем</th><th data-type='number' align='center'>Неудовлетворенность работников<br>различными компонентами работы<br>на предприятии (анкета)</th></tr></thead><tbody><tr><td>Жилищные условия</td><td align='center'>"$a1"</td></tr><tr><td>Служебный транспорт</td><td align='center'>"$a2"</td></tr><tr><td>Общественное питание</td><td align='center'>"$a3"</td></tr><tr><td>Оснащение рабочего места</td><td align='center'>"$a4"</td></tr><tr><td>Качество и наличие СИЗ</td><td align='center'>"$a5"</td></tr><tr><td>Условия и безопасность труда</td><td align='center'>"$a6"</td></tr><tr><td>Заработная плата и система мотивации</td><td align='center'>"$a7"</td></tr><tr><td>Карьерный рост, повышение квалификации</td><td align='center'>"$a8"</td></tr><tr><td>Социальный пакет</td><td align='center'>"$a9"</td></tr><tr><td>Содержание работы</td><td align='center'>"$a10"</td></tr><tr><td>Внутренние служебные коммуникации</td><td align='center'>"$a11"</td></tr><tr><td>Организация досуга</td><td align='center'>"$a12"</td></tr></tbody></table><script>var grid = document.getElementById('tableDataBP'); grid.onclick = function(e) {if (e.target.tagName != 'TH') return; sortGrid(e.target.cellIndex, e.target.getAttribute('data-type'));}; function sortGrid(colNum, type) {var tbody = grid.getElementsByTagName('tbody')[0]; var rowsArray = [].slice.call(tbody.rows); var compare; switch (type) {case 'number': compare = function(rowA, rowB) {return rowA.cells[colNum].innerHTML - rowB.cells[colNum].innerHTML;}; break; case 'string': compare = function(rowA, rowB) {return rowA.cells[colNum].innerHTML > rowB.cells[colNum].innerHTML ? 1 : -1;}; break;} rowsArray.sort(compare); grid.removeChild(tbody); for (var i = 0; i < rowsArray.length; i++) {tbody.appendChild(rowsArray[i]);} grid.appendChild(tbody);}</script></body></html>"

          # формируем JSON
          newData='KAKAKA"data":[{"id":"cmp-eu4oyg","type":"label","label":"Блоки проблем","value":" "},{"id":"cmp-t4qny6","type":"label","label":"Компания","value":" "},{"id":"company","type":"reglink","value":"'$companyName'","key":"'$companyKey'","valueID":"'$companyKey'","username":"Admin Admin Admin","userID":"1"},{"id":"cmp-onbqc0","type":"label","label":"Дата формирования","value":" "},{"id":"date","type":"date","value":"'$date1'","key":"'$date2'"},{"id":"htmlText","type":"textarea","value":"'$textHtml'"},{"id":"cmp-aws3pi","type":"label","label":"Блоки проблем","value":" "},{"id":"cmp-1rp3pp","type":"label","label":"Неудовлетворенность работников различными компонентами работы на предприятии (анкета)","value":" "},{"id":"block1","type":"listbox","value":"Жилищные условия","key":"1"},{"id":"questionnaire1","type":"textbox","value":"'$a1'"},{"id":"block2","type":"listbox","value":"Служебный транспорт","key":"2"},{"id":"questionnaire2","type":"textbox","value":"'$a2'"},{"id":"block3","type":"listbox","value":"Общественное питание","key":"3"},{"id":"questionnaire3","type":"textbox","value":"'$a3'"},{"id":"block4","type":"listbox","value":"Оснащение рабочего места","key":"4"},{"id":"questionnaire4","type":"textbox","value":"'$a4'"},{"id":"block5","type":"listbox","value":"Качество и наличие СИЗ","key":"5"},{"id":"questionnaire5","type":"textbox","value":"'$a5'"},{"id":"block6","type":"listbox","value":"Условия и безопасность труда","key":"6"},{"id":"questionnaire6","type":"textbox","value":"'$a6'"},{"id":"block7","type":"listbox","value":"Заработная плата и система мотивации","key":"7"},{"id":"questionnaire7","type":"textbox","value":"'$a7'"},{"id":"block8","type":"listbox","value":"Карьерный рост, повышение квалификации","key":"8"},{"id":"questionnaire8","type":"textbox","value":"'$a8'"},{"id":"block9","type":"listbox","value":"Социальный пакет","key":"9"},{"id":"questionnaire9","type":"textbox","value":"'$a9'"},{"id":"block10","type":"listbox","value":"Содержание работы","key":"10"},{"id":"questionnaire10","type":"textbox","value":"'$a10'"},{"id":"block11","type":"listbox","value":"Внутренние служебные коммуникации","key":"11"},{"id":"questionnaire11","type":"textbox","value":"'$a11'"},{"id":"block12","type":"listbox","value":"Организация досуга","key":"12"},{"id":"questionnaire12","type":"textbox","value":"'$a12'"}]'
          newData=$(echo $newData | sed 's/KAKAKA//')
          logging $scriptName JSON "Итоговый JSON для передачи в API rest/api/asforms/data/save\n`echo $newData`"

          logging $scriptName STATUS "Создание записи реестра"
          # создание записи реестра
          #wget --quiet --http-user="$synergyUser" --http-password="$synergyPass" --output-document $tmpfile "$synergyURL/rest/api/registry/create_doc?registryID=$registryid"
          curl --user $synergyUser:$synergyPass --request GET "$synergyURL/rest/api/registry/create_doc?registryID=$registryid" --output $tmpfile --silent
          logging $scriptName RESULT "`cat $tmpfile`"

          # берем uuid созданной записи
          newUUID=`cat $tmpfile | sed "s/.*\dataUUID\": \"\(.*\)\", \"asfNodeID.*/\1/"`

          logging $scriptName STATUS "Сохранение данных в новую запись"
          # выполняем api метод POST (изменяем значение поля на форме)
          curl --user $synergyUser:$synergyPass --request POST "$synergyURL/rest/api/asforms/data/save" --data "formUUID=$formid" --data "uuid=$newUUID" --data-urlencode "data=$newData" --output $tmpfile --silent
          logging $scriptName RESULT "`cat $tmpfile`"

          logging $scriptName STATUS "Активация записи реестра"
          # активация записи реестра
          #wget --quiet --http-user="$synergyUser" --http-password="$synergyPass" --output-document $tmpfile "$synergyURL/rest/api/registry/activate_doc?dataUUID=$newUUID"
          curl --user $synergyUser:$synergyPass --request GET "$synergyURL/rest/api/registry/activate_doc?dataUUID=$newUUID" --output $tmpfile --silent
          logging $scriptName RESULT "`cat $tmpfile`"
        fi
      else
        logging $scriptName STATUS "Нет анкет по данной компании"
      fi
    else
      logging $scriptName STATUS "Нет анкет по данной компании"
    fi
  done
  #---------------
  # тут пересчет по родителям
  allCompanyKey=`echo $allCompanyKey | rev | sed 's/,//' | rev`
  echo '------------------------------------------------------------------------' >> $logFile
  logging $scriptName INFO "Пересчет по родителям"
  logging $scriptName INFO "Массив ID дочерних компаний:\n[$allCompanyKey]"
  logging $scriptName STATUS "Поиск всех родителей"
  # Поиск всех родителей
  mysql -uroot -proot -e "use synergy; set names utf8;
  SELECT parent.cmp_data parentName, parent.cmp_key parentKey
  FROM registry_documents rg JOIN asf_data_index parent ON parent.uuid=rg.asfDataID AND parent.cmp_id='parent'
  WHERE rg.registryID='$regCompany' AND rg.deleted IS NULL AND rg.active='Y' AND rg.documentid IN ($allCompanyKey)
  GROUP BY parent.cmp_key;" > $tmpSQL1
  logging $scriptName RESULT "Родители:\n `cat $tmpSQL1`"

  countStr3=`cat $tmpSQL1 | wc -l`

  for k in `seq 2 $countStr3`;
  do
    parentName=`cat $tmpSQL1 | sed -n $k'p' | cut -f1`
    parentKey=`cat $tmpSQL1 | sed -n $k'p' | cut -f2`
    echo '' >> $logFile
    logging $scriptName INFO "parentName: $parentName, documentid: [$parentKey]"
    logging $scriptName STATUS "Пересчет проблем"
    # сумма проблем дочерних компаний родителя
    mysql -uroot -proot -e "use synergy; set names utf8;
    SELECT problem.cmp_key type, SUM(b.cmp_data) problem
    FROM registry_documents rg
    LEFT JOIN asf_data_index problem ON problem.uuid=rg.asfDataID AND problem.cmp_id LIKE 'block%'
    LEFT JOIN asf_data_index b ON b.uuid=rg.asfDataID AND b.cmp_id=CONCAT('questionnaire', SUBSTRING(problem.cmp_id, 6))
    WHERE rg.registryID='$registryid' AND rg.deleted IS NULL AND rg.active='Y'
    AND rg.asfDataID IN (SELECT
    (SELECT asfDataID FROM registry_documents
    JOIN asf_data_index company ON company.uuid=registry_documents.asfDataID AND company.cmp_id='company'
    WHERE registryID='$registryid' AND company.cmp_key=rg.documentID AND registry_documents.deleted IS NULL AND registry_documents.active='Y'
    ORDER BY registry_documents.created DESC LIMIT 1)
    FROM registry_documents rg
    JOIN asf_data_index parent ON parent.uuid=rg.asfDataID AND parent.cmp_id='parent'
    JOIN asf_data_index company ON company.uuid=rg.asfDataID AND company.cmp_id='settings_name'
    WHERE rg.registryID='$regCompany' AND rg.deleted IS NULL AND rg.active='Y' AND parent.cmp_key='$parentKey')
    GROUP BY problem.cmp_key ORDER BY problem.cmp_key * 1;" > $tmpSQL2

    if [ -s $tmpSQL2 ];
    then
      logging $scriptName RESULT ":\n `cat $tmpSQL2`"
      a1=`cat $tmpSQL2 | sed -n 2p | cut -f2`
      a2=`cat $tmpSQL2 | sed -n 3p | cut -f2`
      a3=`cat $tmpSQL2 | sed -n 4p | cut -f2`
      a4=`cat $tmpSQL2 | sed -n 5p | cut -f2`
      a5=`cat $tmpSQL2 | sed -n 6p | cut -f2`
      a6=`cat $tmpSQL2 | sed -n 7p | cut -f2`
      a7=`cat $tmpSQL2 | sed -n 8p | cut -f2`
      a8=`cat $tmpSQL2 | sed -n 9p | cut -f2`
      a9=`cat $tmpSQL2 | sed -n 10p | cut -f2`
      a10=`cat $tmpSQL2 | sed -n 11p | cut -f2`
      a11=`cat $tmpSQL2 | sed -n 12p | cut -f2`
      a12=`cat $tmpSQL2 | sed -n 13p | cut -f2`
      a13=`cat $tmpSQL2 | sed -n 14p | cut -f2`

      # формируем html
      textHtml="<html><head><meta http-equiv='Content-Type' content='text/html; charset=UTF-8'></head><body><style type='text/css'> #tableDataBP {border-collapse: collapse;} #tableDataBP thead th {border: 1px solid #000; padding: 5px; cursor: pointer; background-color: #6ed3f3;} #tableDataBP thead th:hover {color: blue;} #tableDataBP tbody td {border: 1px solid #000; padding: 5px;} #tableDataBP tbody tr:hover {background-color: #f3fbfd;}</style><table id='tableDataBP'><caption><h2>"$parentName"</h2></caption><thead><tr><th ata-type='string' align='center'>Блоки проблем</th><th data-type='number' align='center'>Неудовлетворенность работников<br>различными компонентами работы<br>на предприятии (анкета)</th></tr></thead><tbody><tr><td>Жилищные условия</td><td align='center'>"$a1"</td></tr><tr><td>Служебный транспорт</td><td align='center'>"$a2"</td></tr><tr><td>Общественное питание</td><td align='center'>"$a3"</td></tr><tr><td>Оснащение рабочего места</td><td align='center'>"$a4"</td></tr><tr><td>Качество и наличие СИЗ</td><td align='center'>"$a5"</td></tr><tr><td>Условия и безопасность труда</td><td align='center'>"$a6"</td></tr><tr><td>Заработная плата и система мотивации</td><td align='center'>"$a7"</td></tr><tr><td>Карьерный рост, повышение квалификации</td><td align='center'>"$a8"</td></tr><tr><td>Социальный пакет</td><td align='center'>"$a9"</td></tr><tr><td>Содержание работы</td><td align='center'>"$a10"</td></tr><tr><td>Внутренние служебные коммуникации</td><td align='center'>"$a11"</td></tr><tr><td>Организация досуга</td><td align='center'>"$a12"</td></tr></tbody></table><script>var grid = document.getElementById('tableDataBP'); grid.onclick = function(e) {if (e.target.tagName != 'TH') return; sortGrid(e.target.cellIndex, e.target.getAttribute('data-type'));}; function sortGrid(colNum, type) {var tbody = grid.getElementsByTagName('tbody')[0]; var rowsArray = [].slice.call(tbody.rows); var compare; switch (type) {case 'number': compare = function(rowA, rowB) {return rowA.cells[colNum].innerHTML - rowB.cells[colNum].innerHTML;}; break; case 'string': compare = function(rowA, rowB) {return rowA.cells[colNum].innerHTML > rowB.cells[colNum].innerHTML ? 1 : -1;}; break;} rowsArray.sort(compare); grid.removeChild(tbody); for (var i = 0; i < rowsArray.length; i++) {tbody.appendChild(rowsArray[i]);} grid.appendChild(tbody);}</script></body></html>"

      # формируем JSON
      newData='KAKAKA"data":[{"id":"cmp-eu4oyg","type":"label","label":"Блоки проблем","value":" "},{"id":"cmp-t4qny6","type":"label","label":"Компания","value":" "},{"id":"company","type":"reglink","value":"'$parentName'","key":"'$parentKey'","valueID":"'$parentKey'","username":"Admin Admin Admin","userID":"1"},{"id":"cmp-onbqc0","type":"label","label":"Дата формирования","value":" "},{"id":"date","type":"date","value":"'$date1'","key":"'$date2'"},{"id":"htmlText","type":"textarea","value":"'$textHtml'"},{"id":"cmp-aws3pi","type":"label","label":"Блоки проблем","value":" "},{"id":"cmp-1rp3pp","type":"label","label":"Неудовлетворенность работников различными компонентами работы на предприятии (анкета)","value":" "},{"id":"block1","type":"listbox","value":"Жилищные условия","key":"1"},{"id":"questionnaire1","type":"textbox","value":"'$a1'"},{"id":"block2","type":"listbox","value":"Служебный транспорт","key":"2"},{"id":"questionnaire2","type":"textbox","value":"'$a2'"},{"id":"block3","type":"listbox","value":"Общественное питание","key":"3"},{"id":"questionnaire3","type":"textbox","value":"'$a3'"},{"id":"block4","type":"listbox","value":"Оснащение рабочего места","key":"4"},{"id":"questionnaire4","type":"textbox","value":"'$a4'"},{"id":"block5","type":"listbox","value":"Качество и наличие СИЗ","key":"5"},{"id":"questionnaire5","type":"textbox","value":"'$a5'"},{"id":"block6","type":"listbox","value":"Условия и безопасность труда","key":"6"},{"id":"questionnaire6","type":"textbox","value":"'$a6'"},{"id":"block7","type":"listbox","value":"Заработная плата и система мотивации","key":"7"},{"id":"questionnaire7","type":"textbox","value":"'$a7'"},{"id":"block8","type":"listbox","value":"Карьерный рост, повышение квалификации","key":"8"},{"id":"questionnaire8","type":"textbox","value":"'$a8'"},{"id":"block9","type":"listbox","value":"Социальный пакет","key":"9"},{"id":"questionnaire9","type":"textbox","value":"'$a9'"},{"id":"block10","type":"listbox","value":"Содержание работы","key":"10"},{"id":"questionnaire10","type":"textbox","value":"'$a10'"},{"id":"block11","type":"listbox","value":"Внутренние служебные коммуникации","key":"11"},{"id":"questionnaire11","type":"textbox","value":"'$a11'"},{"id":"block12","type":"listbox","value":"Организация досуга","key":"12"},{"id":"questionnaire12","type":"textbox","value":"'$a12'"}]'
      newData=$(echo $newData | sed 's/KAKAKA//')
      logging $scriptName JSON "Итоговый JSON для передачи в API rest/api/asforms/data/save\n`echo $newData`"

      logging $scriptName STATUS "Создание записи реестра"
      # создание записи реестра
      # wget --quiet --http-user="$synergyUser" --http-password="$synergyPass" --output-document $tmpfile "$synergyURL/rest/api/registry/create_doc?registryID=$registryid"
      curl --user $synergyUser:$synergyPass --request GET "$synergyURL/rest/api/registry/create_doc?registryID=$registryid" --output $tmpfile --silent
      logging $scriptName RESULT "`cat $tmpfile`"

      # берем uuid созданной записи
      newUUID=`cat $tmpfile | sed "s/.*\dataUUID\": \"\(.*\)\", \"asfNodeID.*/\1/"`

      logging $scriptName STATUS "Сохранение данных в новую запись"
      # выполняем api метод POST (изменяем значение поля на форме)
      curl --user $synergyUser:$synergyPass --request POST "$synergyURL/rest/api/asforms/data/save" --data "formUUID=$formid" --data "uuid=$newUUID" --data-urlencode "data=$newData" --output $tmpfile --silent
      logging $scriptName RESULT "`cat $tmpfile`"

      logging $scriptName STATUS "Активация записи реестра"
      # активация записи реестра
      # wget --quiet --http-user="$synergyUser" --http-password="$synergyPass" --output-document $tmpfile "$synergyURL/rest/api/registry/activate_doc?dataUUID=$newUUID"
      curl --user $synergyUser:$synergyPass --request GET "$synergyURL/rest/api/registry/activate_doc?dataUUID=$newUUID" --output $tmpfile --silent
      logging $scriptName RESULT "`cat $tmpfile`"
    else
      logging $scriptName RESULT "Не найдено записей по дочерним компаниям"
    fi
  done

else
  logging $scriptName INFO "нет новых анкет"
fi

logging $scriptName STATUS "Удаление временных файлов"
# удаление временных файлов
rm -f $tmpSQL1
rm -f $tmpSQL2
rm -f $tmpSQL3
rm -f $tmpfile

logging $scriptName END "Завершение работы скрипта"
