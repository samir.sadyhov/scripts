#!/bin/bash

synergyUser="Administrator"
synergyPass="\$ystemAdm1n"
synergyURL="http://127.0.0.1:8080/Synergy"

tmpSQL1="/tmp/disperChSQL1.msk"
tmpSQL2="/tmp/disperChSQL2.msk"
tmpSQL3="/tmp/disperChSQL3.msk"
tmpSQL4="/tmp/disperChSQL4.msk"
tmpfile="/tmp/disperChData.msk"

# Реестр \Социальная напряженность\
regSoc="46e16e23-af5f-46eb-a792-47679012ee87"
formSoc="17da8e58-fb6c-4534-9ce1-21d0f59ccdbc"

# Реестр \98 Настройки компаний\
regCompany="d9b4245b-7be7-4a6d-b085-0b7943653a96"

# ищем новые записи в реестре \Социальная напряженность\ исключая записи родительской компании
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT rg.asfDataID, company.cmp_data AS companyName, company.cmp_key AS companyKey,
       NOW() AS date1, DATE_FORMAT(NOW(), '%d.%m.%Y') AS date2,
       parent.cmp_data AS parentName, parent.cmp_key AS parentKey
FROM registry_documents rg
JOIN asf_data_index company ON company.uuid=rg.asfDataID AND company.cmp_id='company'
LEFT JOIN (SELECT rg.documentid, parent.cmp_data, parent.cmp_key
FROM registry_documents rg
JOIN asf_data_index parent ON parent.uuid=rg.asfDataID AND parent.cmp_id='parent'
WHERE rg.registryID='$regCompany'
AND rg.deleted IS NULL AND rg.active='Y') parent ON parent.documentid=company.cmp_key
WHERE rg.registryID='$regSoc' AND rg.deleted IS NULL AND rg.active='Y'
  AND company.cmp_key!=parent.cmp_key AND DATE(rg.created)=DATE(NOW())
ORDER BY rg.created;" > $tmpSQL1

# ищем id родительских компаний, по новым записям в реестре \Социальная напряженность\ дочек
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT rg.asfDataID, company.cmp_data AS companyName, company.cmp_key AS companyKey,
       NOW() AS date1, DATE_FORMAT(NOW(), '%d.%m.%Y') AS date2,
       parent.cmp_data AS parentName, parent.cmp_key AS parentKey
FROM registry_documents rg
JOIN asf_data_index company ON company.uuid=rg.asfDataID AND company.cmp_id='company'
LEFT JOIN (SELECT rg.documentid, parent.cmp_data, parent.cmp_key
FROM registry_documents rg
JOIN asf_data_index parent ON parent.uuid=rg.asfDataID AND parent.cmp_id='parent'
WHERE rg.registryID='$regCompany'
AND rg.deleted IS NULL AND rg.active='Y') parent ON parent.documentid=company.cmp_key
WHERE rg.registryID='$regSoc' AND rg.deleted IS NULL AND rg.active='Y'
  AND company.cmp_key!=parent.cmp_key AND DATE(rg.created)=DATE(NOW())
GROUP BY parentKey;" > $tmpSQL4

# если есть новые записи то пересчитать значение дисперсии
if [ -s $tmpSQL1 ];
then

# переменные для новой записи реестра по родительской компании
date1=`cat $tmpSQL1 | sed -n 2p | cut -f4`
date2=`cat $tmpSQL1 | sed -n 2p | cut -f5`


# Количество строк в файле tmpSQL1
countStr=`cat $tmpSQL1 | wc -l`
countStr2=`cat $tmpSQL4 | wc -l`

# Изменение значений дисперсии для каждой компании
for i in `seq 2 $countStr`;
do
docID=`cat $tmpSQL1 | sed -n $i'p' | cut -f1`

# пересчет дисперсии
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT
IF(Pe1=0, 1, Pe1*(1-Pe1)) AS Pe1, IF(Pe2=0, 1, Pe2*(1-Pe2)) AS Pe2, IF(Pe3=0, 1, Pe3*(1-Pe3)) AS Pe3,
IF(Pakt=0, 1, Pakt*(1-Pakt)) AS Pakt, IF(Pind=0, 1, Pind*(1-Pind)) AS Pind, IF(Pcrim=0, 1, Pcrim*(1-Pcrim)) AS Pcrim,
IF(Pout=0, 1, Pout*(1-Pout)) AS Pout, IF(Pmax=0, 1, Pmax*(1-Pmax)) AS Pmax, IF(P0=0, 1, P0*(1-P0)) AS P0,
IF(Paag=0, 1, Paag*(1-Paag)) AS Paag, IF(Pconnect=0, 1, Pconnect*(1-Pconnect)) AS Pconnect,
IF(allRows>1, SUM(POW(X-avgX,2))/(allRows-1), 1) AS X, IF(allRows>1, SUM(POW(Z-avgZ,2))/(allRows-1), 1) AS Z,
IF(Psat=0, 1, Psat*(1-Psat)) AS Psat, IF(allRows>1, SUM(POW(S-avgS,2))/(allRows-1), 1) AS S,
IF(allRows>1, SUM(POW(Dst-avgDst,2))/(allRows-1), 1) AS Dst, IF(allRows>1, SUM(POW(Qst-avgQst,2))/(allRows-1), 1) AS Qst,
IF(Pq=0, 1, Pq*(1-Pq)) AS Pq, IF(allRows>1, SUM(POW(Pdes-avgPdes,2))/(allRows-1), 1) AS Pdes,
IF(Ppp=0, 1, Ppp*(1-Ppp)) AS Ppp, IF(Ppqv=0, 1, Ppqv*(1-Ppqv)) AS Ppqv, IF(Pss=0, 1, Pss*(1-Pss)) AS Pss,

((coefPe1*IF(Pe1=0, 1, Pe1*(1-Pe1)))+(coefPe2*IF(Pe2=0, 1, Pe2*(1-Pe2)))+(coefPe3*IF(Pe3=0, 1, Pe3*(1-Pe3)))+
(coefPakt*IF(Pakt=0, 1, Pakt*(1-Pakt)))+(coefPind*IF(Pind=0, 1, Pind*(1-Pind)))+(coefPcrim*IF(Pcrim=0, 1, Pcrim*(1-Pcrim)))+
(coefPout*IF(Pout=0, 1, Pout*(1-Pout)))+(coefPmax*IF(Pmax=0, 1, Pmax*(1-Pmax)))+(coefP0*IF(P0=0, 1, P0*(1-P0)))+
(coefPaag*IF(Paag=0, 1, Paag*(1-Paag)))+(coefPconnect*IF(Pconnect=0, 1, Pconnect*(1-Pconnect)))+(coefPsat*IF(Psat=0, 1, Psat*(1-Psat)))+
(coefPq*IF(Pq=0, 1, Pq*(1-Pq)))+(coefPpp*IF(Ppp=0, 1, Ppp*(1-Ppp)))+(coefPpqv*IF(Ppqv=0, 1, Ppqv*(1-Ppqv)))+(coefPss*IF(Pss=0, 1, Pss*(1-Pss)))+
(coefS*IF(allRows>1, SUM(POW(S-avgS,2))/(allRows-1), 1))+(coefX*IF(allRows>1, SUM(POW(X-avgX,2))/(allRows-1), 1))+
(coefZ*IF(allRows>1, SUM(POW(Z-avgZ,2))/(allRows-1), 1))+(coefQst*IF(allRows>1, SUM(POW(Qst-avgQst,2))/(allRows-1), 1))+
(coefDst*IF(allRows>1, SUM(POW(Dst-avgDst,2))/(allRows-1), 1))+(coefPdes*IF(allRows>1, SUM(POW(Pdes-avgPdes,2))/(allRows-1), 1)) ) AS CHdispersion,

coefPe1, coefPe2, coefPe3, coefPakt, coefPind, coefPcrim, coefPout, coefPmax, coefP0, coefPaag, coefPconnect,
coefX, coefZ, coefPsat, coefS, coefDst, coefQst, coefPq, coefPdes, coefPpp, coefPpqv, coefPss

FROM (SELECT company.cmp_data AS companyName, company.cmp_key AS companyKey,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='P(e)1_value') AS Pe1,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='P(e)2_value') AS Pe2,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='P(e)3_value') AS Pe3,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='Р(akt)_value') AS Pakt,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='Р(ind)_value') AS Pind,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='P(crim)_value') AS Pcrim,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='Р(out)_value') AS Pout,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='Р(max)_value') AS Pmax,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='Р(0)_value') AS P0,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='Р(aag)_value') AS Paag,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='P(connect)_value') AS Pconnect,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='P(sat)_value') AS Psat,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='P(q)_value') AS Pq,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='P(pp)_value') AS Ppp,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='P(pqv)_value') AS Ppqv,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='P(ss)_value') AS Pss,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='P(e)1_coef') AS coefPe1,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='P(e)2_coef') AS coefPe2,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='P(e)3_coef') AS coefPe3,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='Р(akt)_coef') AS coefPakt,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='Р(ind)_coef') AS coefPind,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='P(crim)_coef') AS coefPcrim,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='Р(out)_coef') AS coefPout,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='Р(max)_coef') AS coefPmax,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='Р(0)_coef') AS coefP0,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='Р(aag)_coef') AS coefPaag,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='P(connect)_coef') AS coefPconnect,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='P(sat)_coef') AS coefPsat,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='P(q)_coef') AS coefPq,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='P(pp)_coef') AS coefPpp,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='P(pqv)_coef') AS coefPpqv,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='P(ss)_coef') AS coefPss,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='S_coef') AS coefS,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='X_coef') AS coefX,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='Z_coef') AS coefZ,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='Qst_coef') AS coefQst,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='Dst_coef') AS coefDst,
(SELECT cmp_data*1 FROM asf_data_index WHERE uuid=rg.asfDataID AND cmp_id='P(des)_coef') AS coefPdes,
othParamVal.S, othParamVal.X, othParamVal.Z, othParamVal.Qst, othParamVal.Dst, othParamVal.Pdes,
othParamAvg.allRows, othParamAvg.avgS, othParamAvg.avgX, othParamAvg.avgZ, othParamAvg.avgQst, othParamAvg.avgDst, othParamAvg.avgPdes

FROM registry_documents rg
JOIN asf_data_index company ON company.uuid=rg.asfDataID AND company.cmp_id='company'
LEFT JOIN
(SELECT company.cmp_key AS companyKey, s.cmp_data AS S, x.cmp_data AS X, z.cmp_data AS Z, qst.cmp_data AS Qst, dst.cmp_data AS Dst, Pdes.cmp_data AS Pdes
FROM registry_documents rg
JOIN asf_data_index company ON company.uuid=rg.asfDataID AND company.cmp_id='company'
LEFT JOIN asf_data_index s ON s.uuid=rg.asfDataID AND s.cmp_id='S_value'
LEFT JOIN asf_data_index x ON x.uuid=rg.asfDataID AND x.cmp_id='X_value'
LEFT JOIN asf_data_index z ON z.uuid=rg.asfDataID AND z.cmp_id='Z_value'
LEFT JOIN asf_data_index qst ON qst.uuid=rg.asfDataID AND qst.cmp_id='Qst_value'
LEFT JOIN asf_data_index dst ON dst.uuid=rg.asfDataID AND dst.cmp_id='Dst_value'
LEFT JOIN asf_data_index Pdes ON Pdes.uuid=rg.asfDataID AND Pdes.cmp_id='P(des)_value'
WHERE rg.registryID='$regSoc'
AND rg.deleted IS NULL AND rg.active='Y') othParamVal ON othParamVal.companyKey=company.cmp_key
LEFT JOIN
(SELECT company.cmp_key AS companyKey, COUNT(*) AS allRows, AVG(s.cmp_data) AS avgS, AVG(x.cmp_data) AS avgX,
AVG(z.cmp_data) AS avgZ, AVG(qst.cmp_data) AS avgQst, AVG(dst.cmp_data) AS avgDst, AVG(Pdes.cmp_data) AS avgPdes
FROM registry_documents rg
JOIN asf_data_index company ON company.uuid=rg.asfDataID AND company.cmp_id='company'
LEFT JOIN asf_data_index s ON s.uuid=rg.asfDataID AND s.cmp_id='S_value'
LEFT JOIN asf_data_index x ON x.uuid=rg.asfDataID AND x.cmp_id='X_value'
LEFT JOIN asf_data_index z ON z.uuid=rg.asfDataID AND z.cmp_id='Z_value'
LEFT JOIN asf_data_index qst ON qst.uuid=rg.asfDataID AND qst.cmp_id='Qst_value'
LEFT JOIN asf_data_index dst ON dst.uuid=rg.asfDataID AND dst.cmp_id='Dst_value'
LEFT JOIN asf_data_index Pdes ON Pdes.uuid=rg.asfDataID AND Pdes.cmp_id='P(des)_value'
WHERE rg.registryID='$regSoc'
AND rg.deleted IS NULL AND rg.active='Y' GROUP BY company.cmp_key) othParamAvg ON othParamAvg.companyKey=company.cmp_key

WHERE rg.registryID='$regSoc' AND rg.deleted IS NULL AND rg.active='Y' AND rg.asfDataID='$docID') t;" > $tmpSQL2

# присвоение переменных дисперсии
Pe1=`cat $tmpSQL2 | sed -n 2p | cut -f1`
Pe2=`cat $tmpSQL2 | sed -n 2p | cut -f2`
Pe3=`cat $tmpSQL2 | sed -n 2p | cut -f3`
Pakt=`cat $tmpSQL2 | sed -n 2p | cut -f4`
Pind=`cat $tmpSQL2 | sed -n 2p | cut -f5`
Pcrim=`cat $tmpSQL2 | sed -n 2p | cut -f6`
Pout=`cat $tmpSQL2 | sed -n 2p | cut -f7`
Pmax=`cat $tmpSQL2 | sed -n 2p | cut -f8`
P0=`cat $tmpSQL2 | sed -n 2p | cut -f9`
Paag=`cat $tmpSQL2 | sed -n 2p | cut -f10`
Pconnect=`cat $tmpSQL2 | sed -n 2p | cut -f11`
X=`cat $tmpSQL2 | sed -n 2p | cut -f12`
Z=`cat $tmpSQL2 | sed -n 2p | cut -f13`
Psat=`cat $tmpSQL2 | sed -n 2p | cut -f14`
S=`cat $tmpSQL2 | sed -n 2p | cut -f15`
Dst=`cat $tmpSQL2 | sed -n 2p | cut -f16`
Qst=`cat $tmpSQL2 | sed -n 2p | cut -f17`
Pq=`cat $tmpSQL2 | sed -n 2p | cut -f18`
Pdes=`cat $tmpSQL2 | sed -n 2p | cut -f19`
Ppp=`cat $tmpSQL2 | sed -n 2p | cut -f20`
Ppqv=`cat $tmpSQL2 | sed -n 2p | cut -f21`
Pss=`cat $tmpSQL2 | sed -n 2p | cut -f22`
CHdispersion=`cat $tmpSQL2 | sed -n 2p | cut -f23`

# Весовой коэффициент
coefPe1=`cat $tmpSQL2 | sed -n 2p | cut -f24`
coefPe2=`cat $tmpSQL2 | sed -n 2p | cut -f25`
coefPe3=`cat $tmpSQL2 | sed -n 2p | cut -f26`
coefPakt=`cat $tmpSQL2 | sed -n 2p | cut -f27`
coefPind=`cat $tmpSQL2 | sed -n 2p | cut -f28`
coefPcrim=`cat $tmpSQL2 | sed -n 2p | cut -f29`
coefPout=`cat $tmpSQL2 | sed -n 2p | cut -f30`
coefPmax=`cat $tmpSQL2 | sed -n 2p | cut -f31`
coefP0=`cat $tmpSQL2 | sed -n 2p | cut -f32`
coefPaag=`cat $tmpSQL2 | sed -n 2p | cut -f33`
coefPconnect=`cat $tmpSQL2 | sed -n 2p | cut -f34`
coefX=`cat $tmpSQL2 | sed -n 2p | cut -f35`
coefZ=`cat $tmpSQL2 | sed -n 2p | cut -f36`
coefPsat=`cat $tmpSQL2 | sed -n 2p | cut -f37`
coefS=`cat $tmpSQL2 | sed -n 2p | cut -f38`
coefDst=`cat $tmpSQL2 | sed -n 2p | cut -f39`
coefQst=`cat $tmpSQL2 | sed -n 2p | cut -f40`
coefPq=`cat $tmpSQL2 | sed -n 2p | cut -f41`
coefPdes=`cat $tmpSQL2 | sed -n 2p | cut -f42`
coefPpp=`cat $tmpSQL2 | sed -n 2p | cut -f43`
coefPpqv=`cat $tmpSQL2 | sed -n 2p | cut -f44`
coefPss=`cat $tmpSQL2 | sed -n 2p | cut -f45`

tmpData=',{"id":"P(e)1_dispersion","type":"numericinput","label":"false","value":"'$Pe1'","key":"'$Pe1'"},{"id":"P(e)2_dispersion","type":"numericinput","label":"false","value":"'$Pe2'","key":"'$Pe2'"},{"id":"P(e)3_dispersion","type":"numericinput","label":"false","value":"'$Pe3'","key":"'$Pe3'"},{"id":"Р(akt)_dispersion","type":"numericinput","label":"false","value":"'$Pakt'","key":"'$Pakt'"},{"id":"Р(ind)_dispersion","type":"numericinput","label":"false","value":"'$Pind'","key":"'$Pind'"},{"id":"P(crim)_dispersion","type":"numericinput","label":"true","value":"'$Pcrim'","key":"'$Pcrim'"},{"id":"Р(out)_dispersion","type":"numericinput","label":"true","value":"'$Pout'","key":"'$Pout'"},{"id":"Р(max)_dispersion","type":"numericinput","label":"true","value":"'$Pmax'","key":"'$Pmax'"},{"id":"Р(0)_dispersion","type":"numericinput","label":"true","value":"'$P0'","key":"'$P0'"},{"id":"Р(aag)_dispersion","type":"numericinput","label":"true","value":"'$Paag'","key":"'$Paag'"},{"id":"P(connect)_dispersion","type":"numericinput","label":"true","value":"'$Pconnect'","key":"'$Pconnect'"},{"id":"X_dispersion","type":"numericinput","label":"true","value":"'$X'","key":"'$X'"},{"id":"Z_dispersion","type":"numericinput","label":"true","value":"'$Z'","key":"'$Z'"},{"id":"P(sat)_dispersion","type":"numericinput","label":"true","value":"'$Psat'","key":"'$Psat'"},{"id":"S_dispersion","type":"numericinput","label":"true","value":"'$S'","key":"'$S'"},{"id":"Dst_dispersion","type":"numericinput","label":"true","value":"'$Dst'","key":"'$Dst'"},{"id":"Qst_dispersion","type":"numericinput","label":"true","value":"'$Qst'","key":"'$Qst'"},{"id":"P(q)_dispersion","type":"numericinput","label":"true","value":"'$Pq'","key":"'$Pq'"},{"id":"P(des)_dispersion","type":"numericinput","label":"true","value":"'$Pdes'","key":"'$Pdes'"},{"id":"P(pp)_dispersion","type":"numericinput","label":"true","value":"'$Ppp'","key":"'$Ppp'"},{"id":"P(pqv)_dispersion","type":"numericinput","label":"true","value":"'$Ppqv'","key":"'$Ppqv'"},{"id":"P(ss)_dispersion","type":"numericinput","label":"true","value":"'$Pss'","key":"'$Pss'"},{"id":"CH_dispersion","type":"textbox","value":"'$CHdispersion'"}]'

# Формирование строки data для передачи в api /rest/api/asforms/data/save
# wget --quiet --http-user="$synergyUser" --http-password="$synergyPass" --output-document $tmpfile "$synergyURL/rest/api/asforms/data/$docID"
curl --user $synergyUser:$synergyPass --request GET "$synergyURL/rest/api/asforms/data/$docID" --output $tmpfile --silent
disperFdata=`cat $tmpfile`

disperFdata=$(echo $disperFdata | sed 's/{"id":"P(e)1_dispersion","type":"numericinput","label":"false","value":"","key":""}//')
disperFdata=$(echo $disperFdata | sed 's/{"id":"P(e)2_dispersion","type":"numericinput","label":"false","value":"","key":""}//')
disperFdata=$(echo $disperFdata | sed 's/{"id":"P(e)3_dispersion","type":"numericinput","label":"false","value":"","key":""}//')
disperFdata=$(echo $disperFdata | sed 's/{"id":"P(akt)_dispersion","type":"numericinput","label":"false","value":"","key":""}//')
disperFdata=$(echo $disperFdata | sed 's/{"id":"Р(ind)_dispersion","type":"numericinput","label":"false","value":"","key":""}//')
disperFdata=$(echo $disperFdata | sed 's/{"id":"P(crim)_dispersion","type":"numericinput","label":"true","value":"","key":""}//')
disperFdata=$(echo $disperFdata | sed 's/{"id":"Р(out)_dispersion","type":"numericinput","label":"true","value":"","key":""}//')
disperFdata=$(echo $disperFdata | sed 's/{"id":"Р(max)_dispersion","type":"numericinput","label":"true","value":"","key":""}//')
disperFdata=$(echo $disperFdata | sed 's/{"id":"Р(0)_dispersion","type":"numericinput","label":"true","value":"","key":""}//')
disperFdata=$(echo $disperFdata | sed 's/{"id":"Р(aag)_dispersion","type":"numericinput","label":"true","value":"","key":""}//')
disperFdata=$(echo $disperFdata | sed 's/{"id":"P(connect)_dispersion","type":"numericinput","label":"true","value":"","key":""}//')
disperFdata=$(echo $disperFdata | sed 's/{"id":"X_dispersion","type":"numericinput","label":"true","value":"","key":""}//')
disperFdata=$(echo $disperFdata | sed 's/{"id":"Z_dispersion","type":"numericinput","label":"true","value":"","key":""}//')
disperFdata=$(echo $disperFdata | sed 's/{"id":"P(sat)_dispersion","type":"numericinput","label":"true","value":"","key":""}//')
disperFdata=$(echo $disperFdata | sed 's/{"id":"S_dispersion","type":"numericinput","label":"true","value":"","key":""}//')
disperFdata=$(echo $disperFdata | sed 's/{"id":"Dst_dispersion","type":"numericinput","label":"true","value":"","key":""}//')
disperFdata=$(echo $disperFdata | sed 's/{"id":"Qst_dispersion","type":"numericinput","label":"true","value":"","key":""}//')
disperFdata=$(echo $disperFdata | sed 's/{"id":"P(q)_dispersion","type":"numericinput","label":"true","value":"","key":""}//')
disperFdata=$(echo $disperFdata | sed 's/{"id":"P(des)_dispersion","type":"numericinput","label":"true","value":"","key":""}//')
disperFdata=$(echo $disperFdata | sed 's/{"id":"P(pp)_dispersion","type":"numericinput","label":"true","value":"","key":""}//')
disperFdata=$(echo $disperFdata | sed 's/{"id":"P(pqv)_dispersion","type":"numericinput","label":"true","value":"","key":""}//')
disperFdata=$(echo $disperFdata | sed 's/{"id":"P(ss)_dispersion","type":"numericinput","label":"true","value":"","key":""}//')
disperFdata=$(echo $disperFdata | sed 's/{"id":"CH_dispersion","type":"textbox","value":""}//')
disperFdata=$(echo $disperFdata | sed 's/{"id":"CH_dispersion","type":"textbox"}//')
disperFdata=$(echo ${disperFdata#*data})
disperFdata=$(echo ${disperFdata%]*})
disperFdata='"data'$disperFdata$tmpData

# выполняем api метод POST (заполняем значениями дисперсии)
curl --user $synergyUser:$synergyPass --request POST "$synergyURL/rest/api/asforms/data/save" --data "formUUID=$formSoc" --data "uuid=$docID" --data-urlencode "data=$disperFdata" --output $tmpfile --silent

# Запускает событие "Изменение" записи реестра
# wget --quiet --http-user="$synergyUser" --http-password="$synergyPass" --output-document $tmpfile "$synergyURL/rest/api/registry/modify_doc?dataUUID=$docID"
curl --user $synergyUser:$synergyPass --request GET "$synergyURL/rest/api/registry/modify_doc?dataUUID=$docID" --output $tmpfile --silent

done


for j in `seq 2 $countStr2`;
do

# ID дочерней компании
shildID=`cat $tmpSQL4 | sed -n $j'p' | cut -f3`
# ID и имя родителя
parentName=`cat $tmpSQL4 | sed -n $j'p' | cut -f6`
parentKey=`cat $tmpSQL4 | sed -n $j'p' | cut -f7`

# подсчитываем значения CH для родительской компании

mysql -uroot -proot -e "use synergy; set names utf8;
SELECT SUM(D*(1/(V+POW(T,2)))) AS CH, 200/59*(SUM(D*(1/(V+POW(T,2))))+8.75) AS CH0_100
FROM (SELECT
(SELECT dispersion.cmp_data
FROM registry_documents rg
JOIN asf_data ad ON ad.uuid=rg.asfDataID
JOIN asf_data_index company ON company.uuid=rg.asfDataID AND company.cmp_id='company'
JOIN asf_data_index dispersion ON dispersion.uuid=rg.asfDataID AND dispersion.cmp_id='CH_dispersion'
WHERE rg.registryID='$regSoc'
AND rg.deleted IS NULL AND rg.active='Y' AND company.cmp_key=childrenComp.childrenKey
ORDER BY ad.modified DESC LIMIT 1) AS V,

(SELECT CH.cmp_data*(59/200)-8.75
FROM registry_documents rg
JOIN asf_data ad ON ad.uuid=rg.asfDataID
JOIN asf_data_index company ON company.uuid=rg.asfDataID AND company.cmp_id='company'
JOIN asf_data_index CH ON CH.uuid=rg.asfDataID AND CH.cmp_id='CH_value'
WHERE rg.registryID='$regSoc'
AND rg.deleted IS NULL AND rg.active='Y' AND company.cmp_key=childrenComp.childrenKey
ORDER BY ad.modified DESC LIMIT 1) AS D,

(SELECT IF((SUM(W*POW(D,2)) - ( POW(SUM(W*D),2) / SUM(W) ) - (COUNT(*)-1)) / (IF(SUM(W)-(SUM(POW(W,2))/SUM(W))=0, 0.001, SUM(W)-(SUM(POW(W,2))/SUM(W))))<0, 0, (SUM(W*POW(D,2)) - ( POW(SUM(W*D),2) / SUM(W) ) - (COUNT(*)-1)) / (IF(SUM(W)-(SUM(POW(W,2))/SUM(W))=0, 0.001, SUM(W)-(SUM(POW(W,2))/SUM(W)))))

FROM (SELECT parent.cmp_data AS parentName, parent.cmp_key AS parentKey, childrenName, childrenKey,

1/(SELECT dispersion.cmp_data
FROM registry_documents rg
JOIN asf_data ad ON ad.uuid=rg.asfDataID
JOIN asf_data_index company ON company.uuid=rg.asfDataID AND company.cmp_id='company'
JOIN asf_data_index dispersion ON dispersion.uuid=rg.asfDataID AND dispersion.cmp_id='CH_dispersion'
WHERE rg.registryID='$regSoc' AND rg.deleted IS NULL AND rg.active='Y' AND company.cmp_key=childrenComp.childrenKey
ORDER BY ad.modified DESC LIMIT 1) AS W,

(SELECT CH.cmp_data*(59/200)-8.75
FROM registry_documents rg
JOIN asf_data ad ON ad.uuid=rg.asfDataID
JOIN asf_data_index company ON company.uuid=rg.asfDataID AND company.cmp_id='company'
JOIN asf_data_index CH ON CH.uuid=rg.asfDataID AND CH.cmp_id='CH_value'
WHERE rg.registryID='$regSoc' AND rg.deleted IS NULL AND rg.active='Y' AND company.cmp_key=childrenComp.childrenKey
ORDER BY ad.modified DESC LIMIT 1) AS D

FROM registry_documents rg
JOIN asf_data_index parent ON parent.uuid=rg.asfDataID AND parent.cmp_id='parent'
LEFT JOIN
(SELECT a.cmp_key AS parentKey, company.cmp_data AS childrenName, rg.documentid AS childrenKey
FROM registry_documents rg
JOIN asf_data_index a ON a.uuid=rg.asfDataID AND a.cmp_id='parent'
JOIN asf_data_index company ON company.uuid=rg.asfDataID AND company.cmp_id='settings_name'
WHERE rg.registryID='$regCompany'
AND rg.deleted IS NULL AND rg.active='Y') childrenComp ON childrenComp.parentKey=parent.cmp_key

WHERE rg.deleted IS NULL AND rg.active='Y' AND rg.documentid='$shildID') tGeterogen ) AS T

FROM registry_documents rg
JOIN asf_data_index parent ON parent.uuid=rg.asfDataID AND parent.cmp_id='parent'
LEFT JOIN
(SELECT a.cmp_key AS parentKey, company.cmp_data AS childrenName, rg.documentid AS childrenKey
FROM registry_documents rg
JOIN asf_data_index a ON a.uuid=rg.asfDataID AND a.cmp_id='parent'
JOIN asf_data_index company ON company.uuid=rg.asfDataID AND company.cmp_id='settings_name'
WHERE rg.registryID='$regCompany' AND rg.deleted IS NULL AND rg.active='Y') childrenComp ON childrenComp.parentKey=parent.cmp_key

WHERE rg.deleted IS NULL AND rg.active='Y' AND rg.documentid='$shildID') t" > $tmpSQL3

# вытаскиваем значения CH для родительской компании
CH=`cat $tmpSQL3 | sed -n 2p | cut -f1`
CH0100=`cat $tmpSQL3 | sed -n 2p | cut -f2`

# формируем строку data для новой записи реестра, родительской компании
chKmgData='"data":[{"id":"company","type":"reglink","value":"'$parentName'","key":"'$parentKey'","valueID":"'$parentKey'","userID":""},{"id":"date","type":"date","value":"'$date2'","key":"'$date1'"},{"id":"P(e)1_coef","type":"textbox","label":" ","value":"'$coefPe1'"},{"id":"P(e)2_coef","type":"textbox","label":" ","value":"'$coefPe2'"},{"id":"P(e)3_coef","type":"textbox","label":" ","value":"'$coefPe3'"},{"id":"Р(akt)_coef","type":"textbox","label":" ","value":"'$coefPakt'"},{"id":"Р(ind)_coef","type":"textbox","label":" ","value":"'$coefPind'"},{"id":"P(crim)_coef","type":"textbox","label":" ","value":"'$coefPcrim'"},{"id":"Р(out)_coef","type":"textbox","label":" ","value":"'$coefPout'"},{"id":"Р(max)_coef","type":"textbox","label":" ","value":"'$coefPmax'"},{"id":"Р(0)_coef","type":"textbox","label":" ","value":"'$coefP0'"},{"id":"Р(aag)_coef","type":"textbox","label":" ","value":"'$coefPaag'"},{"id":"P(connect)_coef","type":"textbox","label":" ","value":"'$coefPconnect'"},{"id":"X_coef","type":"textbox","label":" ","value":"'$coefX'"},{"id":"Z_coef","type":"textbox","label":" ","value":"'$coefZ'"},{"id":"P(sat)_coef","type":"textbox","label":" ","value":"'$coefPsat'"},{"id":"S_coef","type":"textbox","label":" ","value":"'$coefS'"},{"id":"Dst_coef","type":"textbox","label":" ","value":"'$coefDst'"},{"id":"Qst_coef","type":"textbox","label":" ","value":"'$coefQst'"},{"id":"P(q)_coef","type":"textbox","label":" ","value":"'$coefPq'"},{"id":"P(des)_coef","type":"textbox","label":" ","value":"'$coefPdes'"},{"id":"P(pp)_coef","type":"textbox","label":" ","value":"'$coefPpp'"},{"id":"P(pqv)_coef","type":"textbox","label":" ","value":"'$coefPpqv'"},{"id":"P(ss)_coef","type":"textbox","label":" ","value":"'$coefPss'"},{"id":"CH_value","type":"textbox","label":" ","value":"'$CH0100'"},{"id":"CH","type":"textbox","label":" ","value":"'$CH'"}]'

# создание записи реестра
#wget --quiet --http-user="$synergyUser" --http-password="$synergyPass" --output-document $tmpfile "$synergyURL/rest/api/registry/create_doc?registryID=$regSoc"
curl --user $synergyUser:$synergyPass --request GET "$synergyURL/rest/api/registry/create_doc?registryID=$regSoc" --output $tmpfile --silent

# берем uuid созданной записи
newUUID=`cat $tmpfile | sed "s/.*\dataUUID\": \"\(.*\)\", \"asfNodeID.*/\1/"`

# выполняем api метод POST (изменяем значение CH на форме)
curl --user $synergyUser:$synergyPass --request POST "$synergyURL/rest/api/asforms/data/save" --data "formUUID=$formSoc" --data "uuid=$newUUID" --data-urlencode "data=$chKmgData" --output $tmpfile --silent

# активация записи реестра
#wget --quiet --http-user="$synergyUser" --http-password="$synergyPass" --output-document $tmpfile "$synergyURL/rest/api/registry/activate_doc?dataUUID=$newUUID"
curl --user $synergyUser:$synergyPass --request GET "$synergyURL/rest/api/registry/activate_doc?dataUUID=$newUUID" --output $tmpfile --silent

done
fi

# удаление временных файлов
rm -f $tmpSQL1
rm -f $tmpSQL2
rm -f $tmpSQL3
rm -f $tmpSQL4
rm -f $tmpfile
