#!/bin/bash
scriptName="recalcVSPcoef"
synergyUser="Administrator"
synergyPass="\$ystemAdm1n"
synergyURL="http://127.0.0.1:8080/Synergy"


# вспомогательные настройки
logFile="/var/log/synergy/scripts.log"
tmpSQL1="/tmp/coefSQL1.msk"
tmpSQL2="/tmp/coefSQL2.msk"
tmpfile="/tmp/coefData.msk"

# 97. Вспомогательные коэффициенты
regVSPcoef="df566883-7820-4842-a543-204f3d38208f"
formVSPcoef="f4c3c114-a9a7-4827-824f-ba9cb5b17884"

# Реестры 02 и 03
registry02="dd58781b-cb75-44c0-a289-bac63428f1e2"
registry03="f7789302-1fb7-4b8e-8e53-5707def8c804"
# 14. Отчет по травматизму
registry14="61191fa2-037e-44a4-a786-79f11d8cd101"
# 15. Отчет по трудовой дисциплины
registry15="51df80a6-902d-4149-8847-477a2221c855"
# 98 Настройки компаний
regCompany="d9b4245b-7be7-4a6d-b085-0b7943653a96"

# Функция получения текущего времени
function now_time() {
  date +"%Y-%m-%d %H:%M:%S"
}
# Функция логирования
function logging() {
  echo -e "`now_time` #$1# [$2] $3" >> $logFile
}

echo '' >> $logFile
logging $scriptName START "Начало работы скрипта"
logging $scriptName STATUS "Поиск активных компаний"

mysql -uroot -proot -e "use synergy; set names utf8;
SELECT rg.documentid, n.cmp_data FROM registry_documents rg
JOIN asf_data_index a ON a.uuid=rg.asfDataID AND a.cmp_id = 'settings_isActive'
JOIN asf_data_index c ON c.uuid=rg.asfDataID AND c.cmp_id = 'necessity'
JOIN asf_data_index n ON n.uuid=rg.asfDataID AND n.cmp_id = 'settings_name'
WHERE rg.registryID='$regCompany'
AND rg.deleted IS NULL AND rg.active='Y' AND a.cmp_key = 'true' AND c.cmp_key = '1';" > $tmpSQL1

if [ -s $tmpSQL1 ];
then

  # Количество строк в файле tmpSQL1
  countStr=`cat $tmpSQL1 | wc -l`
  for i in `seq 2 $countStr`;
  do
    companyKey=`cat $tmpSQL1 | sed -n $i'p' | cut -f1`
    companyName=`cat $tmpSQL1 | sed -n $i'p' | cut -f2`

    echo '' >> $logFile
    logging $scriptName INFO "companyName: $companyName, documentid: [$companyKey]"

    logging $scriptName STATUS "пересчет вспомогательных коэффициентов"
    mysql -uroot -proot -e "use synergy; set names utf8;
    SELECT companyName, companyID, curdate,
    IF (oldM = newM, oldM, newM) AS writeM, IF (oldM = newM, oldMDate, curdate) AS writeDateM,
    IF (oldN = newN, oldN, newN) AS writeN, IF (oldN = newN, oldNDate, curdate) AS writeDateN,
    IF (oldT1 = newT1, oldT1, newT1) AS writeT1, IF (oldT1 = newT1, oldT1Date, curdate) AS writeDateT1,
    IF (oldT2 = newT2, oldT2, newT2) AS writeT2, IF (oldT2 = newT2, oldT2Date, curdate) AS writeDateT2,
    IF (oldT3 = newT3, oldT3, newT3) AS writeT3, IF (oldT3 = newT3, oldT3Date, curdate) AS writeDateT3,

    CASE
      WHEN oldM = newM THEN 0
      WHEN oldN = newN THEN 0
      WHEN oldT1 = newT1 THEN 0
      WHEN oldT2 = newT2 THEN 0
      WHEN oldT3 = newT3 THEN 0
      ELSE 1
    END AS result
    # 1 параметры изменились, 0 не изменились

    FROM (SELECT company.name companyName, company.id companyID, DATE(NOW()) AS curdate,

      old.MDate AS oldMDate, old.M AS oldM,
      (SELECT IFNULL(SUM(c.cmp_data) / getStepPeriod(MIN(DATE(b.cmp_key)), 3), 0) AS avgCount
      FROM registry_documents rg
      LEFT JOIN asf_data_index a ON a.uuid = rg.asfDataID AND a.cmp_id = 'company'
      LEFT JOIN asf_data_index b ON b.uuid = rg.asfDataID AND b.cmp_id = 'date'
      LEFT JOIN asf_data_index c ON c.uuid = rg.asfDataID AND c.cmp_id = 'count'
      WHERE rg.registryID = '$registry15' AND rg.deleted IS NULL AND rg.active IN ('Y', 'P')
      AND DATE(b.cmp_key) BETWEEN DATE(NOW() - INTERVAL 2 YEAR) AND DATE(NOW())
      AND a.cmp_key = company.id) AS newM,

      old.NDate AS oldNDate, old.N AS oldN,
      (SELECT IFNULL(SUM(c.cmp_data) / getStepPeriod(MIN(DATE(b.cmp_key)), 4), 0)
      FROM registry_documents rg
      LEFT JOIN asf_data_index a ON a.uuid = rg.asfDataID AND a.cmp_id = 'company'
      LEFT JOIN asf_data_index b ON b.uuid = rg.asfDataID
        AND IF(rg.registryID = '$registry03', b.cmp_id = 'meeting_date', b.cmp_id = 'date')
      LEFT JOIN asf_data_index c ON c.uuid = rg.asfDataID AND c.cmp_id = 'count'
      WHERE rg.registryID IN ('$registry02','$registry03') AND rg.deleted IS NULL AND rg.active IN ('Y', 'P')
      AND DATE(b.cmp_key) BETWEEN DATE(NOW() - INTERVAL 2 YEAR) AND DATE(NOW())
      AND a.cmp_key = company.id) AS newN,

      old.T1Date AS oldT1Date, old.T1 AS oldT1,
      (SELECT IFNULL(SUM(c.cmp_data) / getStepPeriod(MIN(DATE(b.cmp_key)), 3), 0)
      FROM registry_documents rg
      LEFT JOIN asf_data_index a ON a.uuid = rg.asfDataID AND a.cmp_id = 'company'
      LEFT JOIN asf_data_index b ON b.uuid = rg.asfDataID AND b.cmp_id = 'date'
      LEFT JOIN asf_data_index c ON c.uuid = rg.asfDataID AND c.cmp_id = 'line1_column1'
      WHERE rg.registryID = '$registry14' AND rg.deleted IS NULL AND rg.active IN ('Y', 'P')
      AND DATE(b.cmp_key) BETWEEN DATE(NOW() - INTERVAL 2 YEAR) AND DATE(NOW())
      AND a.cmp_key = company.id) AS newT1,

      old.T2Date AS oldT2Date, old.T2 AS oldT2,
      (SELECT IFNULL(SUM(c.cmp_data) / getStepPeriod(MIN(DATE(b.cmp_key)), 3), 0)
      FROM registry_documents rg
      LEFT JOIN asf_data_index a ON a.uuid = rg.asfDataID AND a.cmp_id = 'company'
      LEFT JOIN asf_data_index b ON b.uuid = rg.asfDataID AND b.cmp_id = 'date'
      LEFT JOIN asf_data_index c ON c.uuid = rg.asfDataID AND c.cmp_id = 'line2_column1'
      WHERE rg.registryID = '$registry14' AND rg.deleted IS NULL AND rg.active IN ('Y', 'P')
      AND DATE(b.cmp_key) BETWEEN DATE(NOW() - INTERVAL 2 YEAR) AND DATE(NOW())
      AND a.cmp_key = company.id) AS newT2,

      old.T3Date AS oldT3Date, old.T3 AS oldT3,
      (SELECT IFNULL(SUM(c.cmp_data) / getStepPeriod(MIN(DATE(b.cmp_key)), 3), 0)
      FROM registry_documents rg
      LEFT JOIN asf_data_index a ON a.uuid = rg.asfDataID AND a.cmp_id = 'company'
      LEFT JOIN asf_data_index b ON b.uuid = rg.asfDataID AND b.cmp_id = 'date'
      LEFT JOIN asf_data_index c ON c.uuid = rg.asfDataID AND c.cmp_id = 'line3_column1'
      WHERE rg.registryID = '$registry14' AND rg.deleted IS NULL AND rg.active IN ('Y', 'P')
      AND DATE(b.cmp_key) BETWEEN DATE(NOW() - INTERVAL 2 YEAR) AND DATE(NOW())
      AND a.cmp_key = company.id) AS newT3

      FROM (SELECT rg.documentid AS id, a.cmp_data AS name FROM registry_documents rg
      JOIN asf_data_index a ON a.uuid=rg.asfDataID AND a.cmp_id='settings_name'
      WHERE rg.documentid='$companyKey') company

      LEFT JOIN ( SELECT c.cmp_key AS companyID,
        m.cmp_data AS M, md.cmp_data AS MDate,
        n.cmp_data AS N, nd.cmp_data AS NDate,
        t1.cmp_data AS T1, t1d.cmp_data AS T1Date,
        t2.cmp_data AS T2, t2d.cmp_data AS T2Date,
        t3.cmp_data AS T3, t3d.cmp_data AS T3Date

        FROM registry_documents rg
        LEFT JOIN asf_data_index c ON c.uuid = rg.asfDataID AND c.cmp_id = 'company'
        LEFT JOIN asf_data_index m ON m.uuid = rg.asfDataID AND m.cmp_id = 'parameter_M'
        LEFT JOIN asf_data_index md ON md.uuid = rg.asfDataID AND md.cmp_id = 'parameter_M_date'
        LEFT JOIN asf_data_index n ON n.uuid = rg.asfDataID AND n.cmp_id = 'parameter_N'
        LEFT JOIN asf_data_index nd ON nd.uuid = rg.asfDataID AND nd.cmp_id = 'parameter_N_date'
        LEFT JOIN asf_data_index t1 ON t1.uuid = rg.asfDataID AND t1.cmp_id = 'parameter_T1'
        LEFT JOIN asf_data_index t1d ON t1d.uuid = rg.asfDataID AND t1d.cmp_id = 'parameter_T1_date'
        LEFT JOIN asf_data_index t2 ON t2.uuid = rg.asfDataID AND t2.cmp_id = 'parameter_T2'
        LEFT JOIN asf_data_index t2d ON t2d.uuid = rg.asfDataID AND t2d.cmp_id = 'parameter_T2_date'
        LEFT JOIN asf_data_index t3 ON t3.uuid = rg.asfDataID AND t3.cmp_id = 'parameter_T3'
        LEFT JOIN asf_data_index t3d ON t3d.uuid = rg.asfDataID AND t3d.cmp_id = 'parameter_T3_date'

        WHERE rg.registryID = '$regVSPcoef' # 97. Вспомогательные коэффициенты
        AND rg.deleted IS NULL AND rg.active = 'Y'
        AND c.cmp_key = '$companyKey'
        ORDER BY DATE(rg.created) DESC LIMIT 1 ) old ON old.companyID = company.id ) t;"  > $tmpSQL2

    result=`cat $tmpSQL2 | sed -n 2p | cut -f14`
    if [ $result -eq 1 ];
    then
      logging $scriptName STATUS "параметры изменились $result"
      newM=`cat $tmpSQL2 | sed -n 2p | cut -f4`
      newDateM=`cat $tmpSQL2 | sed -n 2p | cut -f5`
      newN=`cat $tmpSQL2 | sed -n 2p | cut -f6`
      newDateN=`cat $tmpSQL2 | sed -n 2p | cut -f7`
      newT1=`cat $tmpSQL2 | sed -n 2p | cut -f8`
      newDateT1=`cat $tmpSQL2 | sed -n 2p | cut -f9`
      newT2=`cat $tmpSQL2 | sed -n 2p | cut -f10`
      newDateT2=`cat $tmpSQL2 | sed -n 2p | cut -f11`
      newT3=`cat $tmpSQL2 | sed -n 2p | cut -f12`
      newDateT3=`cat $tmpSQL2 | sed -n 2p | cut -f13`

      # формирование правильной строки data
      newData='"data":['
      newData=$newData'{"id":"company","type":"reglink","value":"'$companyName'","key":"'$companyKey'","valueID":"'$companyKey'","userID":""},'
      newData=$newData'{"id":"parameter_M","type":"numericinput","label":"false","value":"'$newM'","key":"'$newM'"},'
      newData=$newData'{"id":"parameter_M_date","type":"date","value":"'$newDateM'","key":"'$newDateM' 00:00:00"},'
      newData=$newData'{"id":"parameter_M_duration","type":"numericinput","label":"false","value":"30","key":"30"},'
      newData=$newData'{"id":"parameter_N","type":"numericinput","label":"false","value":"'$newN'","key":"'$newN'"},'
      newData=$newData'{"id":"parameter_N_date","type":"date","value":"'$newDateN'","key":"'$newDateN' 00:00:00"},'
      newData=$newData'{"id":"parameter_N_duration","type":"numericinput","label":"false","value":"7","key":"7"},'
      newData=$newData'{"id":"parameter_T1","type":"numericinput","label":"false","value":"'$newT1'","key":"'$newT1'"},'
      newData=$newData'{"id":"parameter_T1_date","type":"date","value":"'$newDateT1'","key":"'$newDateT1' 00:00:00"},'
      newData=$newData'{"id":"parameter_T1_duration","type":"numericinput","label":"false","value":"30","key":"30"},'
      newData=$newData'{"id":"parameter_T2","type":"numericinput","label":"false","value":"'$newT2'","key":"'$newT2'"},'
      newData=$newData'{"id":"parameter_T2_date","type":"date","value":"'$newDateT2'","key":"'$newDateT2' 00:00:00"},'
      newData=$newData'{"id":"parameter_T2_duration","type":"numericinput","label":"false","value":"30","key":"30"},'
      newData=$newData'{"id":"parameter_T3","type":"numericinput","label":"false","value":"'$newT3'","key":"'$newT3'"},'
      newData=$newData'{"id":"parameter_T3_date","type":"date","value":"'$newDateT3'","key":"'$newDateT3' 00:00:00"},'
      newData=$newData'{"id":"parameter_T3_duration","type":"numericinput","label":"false","value":"30","key":"30"}'
      newData=$newData']'

      logging $scriptName JSON "Итоговый JSON для передачи в API rest/api/asforms/data/save\n$newData"
      echo '' >> $logFile

      # создание записи реестра
      logging $scriptName STATUS "Создание записи реестра"
      curl --user $synergyUser:$synergyPass --request GET "$synergyURL/rest/api/registry/create_doc?registryID=$regVSPcoef" --output $tmpfile --silent
      logging $scriptName RESULT "`cat $tmpfile`"
      # берем uuid созданной записи
      newUUID=`cat $tmpfile | sed "s/.*\dataUUID\": \"\(.*\)\", \"asfNodeID.*/\1/"`
      # выполняем api метод POST
      logging $scriptName STATUS "Сохранение данных в новую запись"
      curl --user $synergyUser:$synergyPass --request POST "$synergyURL/rest/api/asforms/data/save" --data "formUUID=$formVSPcoef" --data "uuid=$newUUID" --data-urlencode "data=$newData" --output $tmpfile --silent
      logging $scriptName RESULT "`cat $tmpfile`"
      # активация записи реестра
      logging $scriptName STATUS "Активация записи реестра"
      curl --user $synergyUser:$synergyPass --request GET "$synergyURL/rest/api/registry/activate_doc?dataUUID=$newUUID" --output $tmpfile --silent
      logging $scriptName RESULT "`cat $tmpfile`"

      echo '' >> $logFile

    else
      logging $scriptName STATUS "параметры не изменились $result"
    fi

  done

fi

# удаление временных файлов
logging $scriptName STATUS "Удаление временных файлов"
rm -f $tmpSQL1
rm -f $tmpSQL2
rm -f $tmpfile

logging $scriptName END "Завершение работы скрипта"
