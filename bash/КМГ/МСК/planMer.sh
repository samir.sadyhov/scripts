#!/bin/bash

synergyUser="Administrator"
synergyPass="\$ystemAdm1n"
synergyURL="http://127.0.0.1:8080/Synergy"

date1=$(date +%d.%m.%Y)
date2=$(date +%F%t%T)

tmpSQL1="/tmp/tmpSQL1.msk"
tmpSQL2="/tmp/tmpSQL2.msk"
tmpfile="/tmp/planData.msk"
tmpJson="/tmp/jsonData.msk"

# Реестры \План мероприятий\ по компаниям --------------------------------------
# АО НК КазМунайГаз
registryKMG="37934c40-268b-4ef5-9546-18c815c0d591"
formKMG="a59576f8-93a0-4d5a-8398-2bf363176b84"

# ТОО Oil Construction Company
registryOKK="06a356d3-fc71-4965-9980-8304478f6d47"
formOKK="f97715cc-b6be-4f94-9f45-ab3ba117bf7f"

# ТОО Oil Services Company
registryOSK="3963a361-9598-471d-8d14-1186061094c4"
formOSK="4f9618f3-3233-41b3-9a6e-9beb22e29721"

# ТОО Oil Transport Corporation
registryOTK="3dc3710b-3fef-49b9-aa16-e72d52a98b88"
formOTK="0e508bf5-1d93-4c33-bc88-5f0a5a00b1ca"

# АО Мангыстаумунайгаз
registryMMG="56b29737-e410-4fcf-a0ad-a1cfd9ffc5b3"
formMMG="c949cc16-848c-466f-9d63-fcc65ddab7a3"

# ТОО Мангыстауэнергомунай
registryMEM="8f426192-0406-4589-85e8-97511b4d9e3d"
formMEM="8d45cb8f-4bba-4a32-9fb3-de7c3a6fb069"

# ТОО Мунайтелеком
registryMTK="e5ab04d0-9045-4aec-ac87-39b3f270aa98"
formMTK="95b9afac-997f-4172-bfd8-dc2f99957885"
# ------------------------------------------------------------------------------

# Реестр \98 Настройки компаний\
registryid2="d9b4245b-7be7-4a6d-b085-0b7943653a96"

# Реестры 02 и 03
registry02="dd58781b-cb75-44c0-a289-bac63428f1e2"
registry03="f7789302-1fb7-4b8e-8e53-5707def8c804"

# SQL запрос, компании со статусом Активно в реестре \98 Настройки компаний\
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT company.cmp_data, rg.documentID FROM registry_documents rg
JOIN asf_data_index company ON company.uuid=rg.asfDataID AND company.cmp_id='settings_name'
JOIN asf_data_index status ON status.uuid=rg.asfDataID AND status.cmp_id='settings_isActive'
WHERE rg.registryID='$registryid2' AND rg.deleted IS NULL AND status.cmp_key='true'
AND rg.documentID NOT IN (SELECT parent.cmp_key FROM registry_documents rg
JOIN asf_data_index parent ON parent.uuid=rg.asfDataID AND parent.cmp_id='parent'
WHERE rg.registryID='$registryid2' AND rg.deleted IS NULL AND rg.active='Y');" > $tmpSQL1

# Количество строк в файле tmpSQL1
countStr=`cat $tmpSQL1 | wc -l`

# создание записи реестра, изменение значения и активация
for i in `seq 2 $countStr`;
do

# формирование первоначальной строки data
newKey=`cat $tmpSQL1 | sed -n $i'p' | cut -f2`
newValue=`cat $tmpSQL1 | sed -n $i'p' | cut -f1`
newData='KAKAKA"data":[{"id":"cmp-mhdsrt","type":"label","label":"Планы мероприятий","value":" "},{"id":"cmp-4bfenv","type":"label","label":"Компания","value":" "},{"id":"company","type":"reglink","value":"'$newValue'","key":"'$newKey'","valueID":"'$newKey'","username":"Admin Admin Admin","userID":"1"},{"id":"cmp-4pwlod","type":"label","label":"Дата формирования","value":" "},{"id":"date","type":"date","value":"'$date1'","key":"'$date2'"},{"id":"cmp-7lip1t","type":"label","label":"Количество ","value":" "},{"id":"count","type":"textbox","value":" "}'

# Присваиваем registryid и formid нужной компании (newKey это documentID в реестре \98 Настройки компаний\, можно проверять по наименованию newValue)
case $newKey in
  14700bc0-9957-11e5-84f7-525400672294)
  registryid=$registryKMG
  formid=$formKMG
  ;;
  d2852840-9955-11e5-84f7-525400672294)
  registryid=$registryOKK
  formid=$formOKK
  ;;
  93fbc9d0-9955-11e5-84f7-525400672294)
  registryid=$registryOSK
  formid=$formOSK
  ;;
  baee56c0-9955-11e5-84f7-525400672294)
  registryid=$registryOTK
  formid=$formOTK
  ;;
  6d495320-9955-11e5-84f7-525400672294)
  registryid=$registryMMG
  formid=$formMMG
  ;;
  e7a6db10-9955-11e5-84f7-525400672294)
  registryid=$registryMEM
  formid=$formMEM
  ;;
  096ffec0-9956-11e5-84f7-525400672294)
  registryid=$registryMTK
  formid=$formMTK
  ;;
esac

# нерешенные проблемы из реестров 02 и 03
mysql -uroot -proot -e "use synergy; set names utf8; SET @pp:=0;
SELECT @pp:=@pp+1 AS nomer, dataPost, stampDate, istochnik, fioRab, textObr, viyaProblem, sferProbValue,
sferProbKey, adresat, prinResh, srokData, srokKey, statusData, statusKey, asfDataID, cmp_id, TableName

FROM (
     SELECT IFNULL(a.cmp_key, '') AS stampDate, company.cmp_data AS companyName, company.cmp_key AS companyKey,
     IFNULL(a.cmp_data, '') AS dataPost, IFNULL(b.cmp_data, '') AS istochnik,
     IFNULL(c.cmp_data, '') AS fioRab, REPLACE(IFNULL(d.cmp_data, ''), '\n', ' ') AS textObr,
     IFNULL(e.cmp_data, ' ') AS viyaProblem, IFNULL(f.cmp_data, '') AS sferProbValue,
     IFNULL(f.cmp_key, '') AS sferProbKey, IFNULL(g.cmp_data, '') AS adresat,
     REPLACE(IFNULL(h.cmp_data, ' '), '\n', ' ') AS prinResh,
     IFNULL(j.cmp_data, '') AS srokData, IFNULL(j.cmp_key, '') AS srokKey,
     k.cmp_data AS statusData, k.cmp_key AS statusKey,
     rg.asfDataID, SUBSTRING(SUBSTRING_INDEX(i.cmp_id, '-', -1), 2) AS cmp_id, 'table02' AS TableName

     FROM registry_documents rg
     LEFT JOIN asf_data_index company ON company.uuid=rg.asfDataID AND company.cmp_id='company'
     LEFT JOIN asf_data_index a ON a.uuid=rg.asfDataID AND a.cmp_id LIKE 'date-b%'
     LEFT JOIN asf_data_index b ON b.uuid=rg.asfDataID AND b.cmp_id=CONCAT('Source_treatment-', SUBSTRING_INDEX(a.cmp_id, '-', -1))
     LEFT JOIN asf_data_index c ON c.uuid=rg.asfDataID AND c.cmp_id=CONCAT('FIO-', SUBSTRING_INDEX(a.cmp_id, '-', -1))
     LEFT JOIN asf_data_index d ON d.uuid=rg.asfDataID AND d.cmp_id=CONCAT('text-', SUBSTRING_INDEX(a.cmp_id, '-', -1))
     LEFT JOIN asf_data_index e ON e.uuid=rg.asfDataID AND e.cmp_id=CONCAT('Problems-', SUBSTRING_INDEX(a.cmp_id, '-', -1))
     LEFT JOIN asf_data_index f ON f.uuid=rg.asfDataID AND f.cmp_id=CONCAT('Problem-', SUBSTRING_INDEX(a.cmp_id, '-', -1))
     LEFT JOIN asf_data_index g ON g.uuid=rg.asfDataID AND g.cmp_id=CONCAT('Destination-', SUBSTRING_INDEX(a.cmp_id, '-', -1))
     LEFT JOIN asf_data_index h ON h.uuid=rg.asfDataID AND h.cmp_id=CONCAT('decision_pr-', SUBSTRING_INDEX(a.cmp_id, '-', -1))
     LEFT JOIN asf_data_index i ON i.uuid=rg.asfDataID AND i.cmp_id=CONCAT('desicion-', SUBSTRING_INDEX(a.cmp_id, '-', -1))
     LEFT JOIN asf_data_index j ON j.uuid=rg.asfDataID AND j.cmp_id=CONCAT('date_srok-', SUBSTRING_INDEX(a.cmp_id, '-', -1))
     LEFT JOIN asf_data_index k ON k.uuid=rg.asfDataID AND k.cmp_id=CONCAT('control-', SUBSTRING_INDEX(c.cmp_id, '-', -1))
     WHERE rg.registryID='$registry02' AND rg.deleted IS NULL AND rg.active='Y' AND i.cmp_key=0 AND company.cmp_key='$newKey'

     UNION ALL

     SELECT IFNULL(a.cmp_key, '') AS stampDate, company.cmp_data AS companyName, company.cmp_key AS companyKey,
     IFNULL(a.cmp_data, '') AS dataPost, IFNULL(b.cmp_data, '') AS istochnik,
     IFNULL(c.cmp_data, '') AS fioRab, REPLACE(IFNULL(d.cmp_data, ''), '\n', ' ') AS textObr,
     IFNULL(e.cmp_data, ' ') AS viyaProblem, IFNULL(f.cmp_data, '') AS sferProbValue,
     IFNULL(f.cmp_key, '') AS sferProbKey, IFNULL(g.cmp_data, '') AS adresat,
     REPLACE(IFNULL(h.cmp_data, ' '), '\n', ' ') AS prinResh,
     IFNULL(j.cmp_data, '') AS srokData, IFNULL(j.cmp_key, '') AS srokKey,
     k.cmp_data AS statusData, k.cmp_key AS statusKey,
     rg.asfDataID, SUBSTRING(SUBSTRING_INDEX(i.cmp_id, '-', -1), 2) AS cmp_id, 'table03' AS TableName

     FROM registry_documents rg
     LEFT JOIN asf_data_index company ON company.uuid=rg.asfDataID AND company.cmp_id='company'
     LEFT JOIN asf_data_index a ON a.uuid=rg.asfDataID AND a.cmp_id='meeting_date'
     LEFT JOIN asf_data_index b ON b.uuid=rg.asfDataID AND b.cmp_id='meeting_type'
     LEFT JOIN asf_data_index c ON c.uuid=rg.asfDataID AND c.cmp_id LIKE 'FIO-%'
     LEFT JOIN asf_data_index d ON d.uuid=rg.asfDataID AND d.cmp_id=CONCAT('text-', SUBSTRING_INDEX(c.cmp_id, '-', -1))
     LEFT JOIN asf_data_index e ON e.uuid=rg.asfDataID AND e.cmp_id=CONCAT('Problems-', SUBSTRING_INDEX(c.cmp_id, '-', -1))
     LEFT JOIN asf_data_index f ON f.uuid=rg.asfDataID AND f.cmp_id=CONCAT('Problem-', SUBSTRING_INDEX(c.cmp_id, '-', -1))
     LEFT JOIN asf_data_index g ON g.uuid=rg.asfDataID AND g.cmp_id=CONCAT('Owner-', SUBSTRING_INDEX(c.cmp_id, '-', -1))
     LEFT JOIN asf_data_index h ON h.uuid=rg.asfDataID AND h.cmp_id=CONCAT('Destination-', SUBSTRING_INDEX(c.cmp_id, '-', -1))
     LEFT JOIN asf_data_index i ON i.uuid=rg.asfDataID AND i.cmp_id=CONCAT('desicion-', SUBSTRING_INDEX(c.cmp_id, '-', -1))
     LEFT JOIN asf_data_index j ON j.uuid=rg.asfDataID AND j.cmp_id=CONCAT('date_srok-', SUBSTRING_INDEX(c.cmp_id, '-', -1))
     LEFT JOIN asf_data_index k ON k.uuid=rg.asfDataID AND k.cmp_id=CONCAT('control-', SUBSTRING_INDEX(c.cmp_id, '-', -1))
     WHERE rg.registryID ='$registry03' AND rg.deleted IS NULL AND rg.active='Y' AND i.cmp_key=0 AND company.cmp_key='$newKey'

     ORDER BY stampDate DESC) t;" > $tmpSQL2

# Количество строк в файле tmpSQL2
countStr2=`cat $tmpSQL2 | wc -l`

# Формирование таблицы по нерешенным вопросам
if [ -s $tmpSQL2 ];
 then
tableData=""

for j in `seq 2 $countStr2`;
do
number=`cat $tmpSQL2 | sed -n $j'p' | cut -f1`
dataPost=`cat $tmpSQL2 | sed -n $j'p' | cut -f2`
stampDate=`cat $tmpSQL2 | sed -n $j'p' | cut -f3`
istochnik=`cat $tmpSQL2 | sed -n $j'p' | cut -f4`
fioRab=`cat $tmpSQL2 | sed -n $j'p' | cut -f5`
fioRab=$(echo $fioRab | sed 's/\\/\\\\/g;s/\"/\\"/g;s/\«/\\"/g;s/'"'"'/\\"/g;s/\»/\\"/g')
textObr=`cat $tmpSQL2 | sed -n $j'p' | cut -f6`
textObr=$(echo $textObr | sed 's/\\/\\\\/g;s/\"/\\"/g;s/\«/\\"/g;s/'"'"'/\\"/g;s/\»/\\"/g')
viyaProblem=`cat $tmpSQL2 | sed -n $j'p' | cut -f7`
viyaProblem=$(echo $viyaProblem | sed 's/\\/\\\\/g;s/\"/\\"/g;s/\«/\\"/g;s/'"'"'/\\"/g;s/\»/\\"/g')
sferProbValue=`cat $tmpSQL2 | sed -n $j'p' | cut -f8`
sferProbKey=`cat $tmpSQL2 | sed -n $j'p' | cut -f9`
adresat=`cat $tmpSQL2 | sed -n $j'p' | cut -f10`
adresat=$(echo $adresat | sed 's/\\/\\\\/g;s/\"/\\"/g;s/\«/\\"/g;s/'"'"'/\\"/g;s/\»/\\"/g')
prinResh=`cat $tmpSQL2 | sed -n $j'p' | cut -f11`
prinResh=$(echo $prinResh | sed 's/\\/\\\\/g;s/\"/\\"/g;s/\«/\\"/g;s/'"'"'/\\"/g;s/\»/\\"/g')
srokData=`cat $tmpSQL2 | sed -n $j'p' | cut -f12`
srokKey=`cat $tmpSQL2 | sed -n $j'p' | cut -f13`
statusData=`cat $tmpSQL2 | sed -n $j'p' | cut -f14`
statusKey=`cat $tmpSQL2 | sed -n $j'p' | cut -f15`
asfDataId=`cat $tmpSQL2 | sed -n $j'p' | cut -f16`
cmpId=`cat $tmpSQL2 | sed -n $j'p' | cut -f17`
tableName=`cat $tmpSQL2 | sed -n $j'p' | cut -f18`
tableData+=',{"id":"Destination-b'$number'","type":"textbox","label":" ","value":"'$adresat'"},{"id":"Problems-b'$number'","type":"textbox","label":" ","value":"'$viyaProblem'"},{"id":"block_problem-b'$number'","type":"listbox","label":" ","value":"'$sferProbValue'","key":"'$sferProbKey'"},{"id":"date-b'$number'","type":"date","label":" ","value":"'$dataPost'","key":"'$stampDate'"},{"id":"date_srok-b'$number'","type":"date","label":" ","value":"'$srokData'","key":"'$srokKey'"},{"id":"decision_pr-b'$number'","type":"textbox","label":" ","value":"'$prinResh'"},{"id":"control-b'$number'","type":"listbox","value":"'$statusData'","key":"'$statusKey'"},{"id":"number-b'$number'","type":"numericinput","label":"false","value":"'$number'","key":"'$number'.000000000000000000"},{"id":"source-b'$number'","type":"textbox","label":" ","value":"'$istochnik'"},{"id":"text-b'$number'","type":"textbox","label":" ","value":"'$textObr'"},{"id":"user-b'$number'","type":"textbox","label":" ","value":"'$fioRab'"},{"id":"sign-b'$number'","type":"check","label":" ","values":["0"],"keys":["На контроле"]},{"id":"linkAsfDataId-b'$number'","type":"textbox","label":" ","value":"'$asfDataId'"},{"id":"linkCmpID-b'$number'","type":"textbox","label":" ","value":"'$cmpId'"},{"id":"TableName-b'$number'","type":"textbox","label":" ","value":"'$tableName'"}'
done

else
tableData=""
fi

# Итоговое формирование правильной строки data для передачи в api rest/api/asforms/data/save
newFdata=$newData',{"id":"appeal","type":"appendable_table","key":"","data":[{"id":"cmp-31jorw","type":"label","label":"Источник","value":" "},{"id":"cmp-4pxn5e","type":"label","label":"Дата поступления","value":" "},{"id":"cmp-g34rnt","type":"label","label":"№ п/п","value":" "},{"id":"cmp-ivvho3","type":"label","label":"Выявленная проблема","value":" "},{"id":"cmp-ksahyc","type":"label","label":"Срок","value":" "},{"id":"cmp-nxs5hx","type":"label","label":"Сфера проблемы","value":" "},{"id":"cmp-p6dn6b","type":"label","label":"Ф.И.О. работника","value":" "},{"id":"cmp-s2orns","type":"label","label":"Принятое решение","value":" "},{"id":"cmp-tm8kg6","type":"label","label":"Текст обращения","value":" "},{"id":"cmp-ybzexu","type":"label","label":"Ответственный/ Адресат обращения","value":" "}'$tableData']}]'
newFdata=$(echo $newFdata | sed 's/KAKAKA//' > $tmpJson)

# создание записи реестра
#wget --quiet --http-user="$synergyUser" --http-password="$synergyPass" --output-document $tmpfile "$synergyURL/rest/api/registry/create_doc?registryID=$registryid"
curl --user $synergyUser:$synergyPass --request GET "$synergyURL/rest/api/registry/create_doc?registryID=$registryid" --output $tmpfile --silent

# берем uuid созданной записи
newUUID=`cat $tmpfile | sed "s/.*\dataUUID\": \"\(.*\)\", \"asfNodeID.*/\1/"`

# выполняем api метод POST (изменяем значение поля на форме)
curl -q --user $synergyUser:$synergyPass --request POST "$synergyURL/rest/api/asforms/data/save" --data "formUUID=$formid" --data "uuid=$newUUID" --data-urlencode "data@$tmpJson" --output $tmpfile --silent

# активация записи реестра
#wget --quiet --http-user="$synergyUser" --http-password="$synergyPass" --output-document $tmpfile "$synergyURL/rest/api/registry/activate_doc?dataUUID=$newUUID"
curl --user $synergyUser:$synergyPass --request GET "$synergyURL/rest/api/registry/activate_doc?dataUUID=$newUUID" --output $tmpfile --silent

done

# удаление временных файлов
rm -f $tmpfile
rm -f $tmpSQL1
rm -f $tmpSQL2
rm -f $tmpJson
