#!/bin/bash

synergyUser="Administrator"
synergyPass="\$ystemAdm1n"
synergyURL="http://127.0.0.1:8080/Synergy"

tmpSQL1="/tmp/ugrSQL1.msk"
tmpSQL2="/tmp/ugrSQL2.msk"
tmpfile="/tmp/ugrData.msk"

# Реестр \Распределение работников по группам опасности\
registryid="22182aae-325c-4a04-8a62-3c3a5cae0659"
formid="cad7a5a3-26cf-4537-88e7-528991e6b952"

# Реестр \98 Настройки компаний\
regCompany="d9b4245b-7be7-4a6d-b085-0b7943653a96"

# Реестр \01. Анкета\
regAnketa="6b9edece-ad7f-4478-aba4-80d456e7fea4"

# Наименование групп опасности
g1name="«Группа риска»: большая вероятность участия в активных протестных действиях"
g2name="Вероятность участия в протесте не самая высокая, вероятнее - увольнение, вся энергия неудовлетворенности и фрустрации скорее всего уйдет на поиск иной работы.  Высокая связанность увеличивает вероятность присоединения к группе риска"
g3name="Неопределенность, могут повести себя по-разному в зависимости от конкретных условий. Высокая связанность повышает вероятность протестных действий"
g4name="Социальная напряженность проявится через social stress: плохое качество работы, отсутствие мотивации к работе, множество мелких нарушений и травм"
g5name="Вероятность участия в протестах низкая"

# ищем новые или измененные записи в реестре \01. Анкета\
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT company.cmp_data, company.cmp_key
FROM registry_documents rg
JOIN asf_data_index company ON company.uuid=rg.asfDataID AND company.cmp_id='company'
JOIN asf_data ON asf_data.uuid=rg.asfDataID
JOIN (SELECT rg.documentid, parent.cmp_data, parent.cmp_key
FROM registry_documents rg JOIN asf_data_index parent ON parent.uuid=rg.asfDataID AND parent.cmp_id='parent'
WHERE rg.registryID='$regCompany' AND rg.deleted IS NULL AND rg.active='Y') parent ON parent.documentid=company.cmp_key
WHERE rg.registryID='$regAnketa' AND rg.deleted IS NULL AND rg.active='Y' AND DATE(asf_data.modified)=DATE(NOW())
GROUP BY company.cmp_key;" > $tmpSQL1

# Количество строк в файле tmpSQL1
countStr=`cat $tmpSQL1 | wc -l`

# если есть новые записи то пересчитать и записать результат в реестр \Распределение работников по группам опасности\
if [ -s $tmpSQL1 ];
then

for i in `seq 2 $countStr`;
do
companyKey=`cat $tmpSQL1 | sed -n $i'p' | cut -f2`
companyName=`cat $tmpSQL1 | sed -n $i'p' | cut -f1`
allCompanyKey+="'"`cat $tmpSQL1 | sed -n $i'p' | cut -f2`"',"

#пересчет
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT DATE(NOW()), NOW(), G1, G2, G3, G4, G5,
ROUND(G1*100/(G1+G2+G3+G4+G5),1) AS percentG1,
ROUND(G2*100/(G1+G2+G3+G4+G5),1) AS percentG2,
ROUND(G3*100/(G1+G2+G3+G4+G5),1) AS percentG3,
ROUND(G4*100/(G1+G2+G3+G4+G5),1) AS percentG4,
ROUND(G5*100/(G1+G2+G3+G4+G5),1) AS percentG5

FROM (SELECT
SUM(CASE
WHEN A1=3 AND Pakt=1 AND A4=3 THEN 1
WHEN A1=3 AND Pcrim=1 AND A4=3 THEN 1
WHEN A1=3 AND Pind=1 AND A4=3 THEN 1
WHEN A1=3 AND Pakt=1 AND A4=2 THEN 1
WHEN A1=1 AND Pakt=1 AND A4=3 THEN 1
WHEN A1=1 AND Pcrim=1 AND A4=3 THEN 1
WHEN A1=2 AND Pakt=1 AND A4=3 THEN 1
WHEN A1=2 AND Pcrim=1 AND A4=3 THEN 1
WHEN A1=2 AND Pind=1 AND A4=3 THEN 1
ELSE 0 END) AS G1,

SUM(CASE
WHEN A1=3 AND Pout=1 AND A4=3 THEN 1
WHEN A1=3 AND Pout=1 AND A4=2 THEN 1
WHEN A1=3 AND Pout=1 AND A4=1 THEN 1
WHEN A1=1 AND Pout=1 AND A4=3 THEN 1
WHEN A1=2 AND Pout=1 AND A4=3 THEN 1
ELSE 0 END ) AS G2,

SUM(CASE
WHEN A1=3 AND Pmax=1 AND A4=3 THEN 1
WHEN A1=3 AND P0=1 AND A4=3 THEN 1
WHEN A1=3 AND Pakt=1 AND A4=1 THEN 1
WHEN A1=3 AND Pcrim=1 AND A4=1 THEN 1
WHEN A1=3 AND Pmax=1 AND A4=1 THEN 1
WHEN A1=3 AND Pind=1 AND A4=1 THEN 1
WHEN A1=3 AND Pcrim=1 AND A4=2 THEN 1
WHEN A1=3 AND Pmax=1 AND A4=2 THEN 1
WHEN A1=3 AND Pind=1 AND A4=2 THEN 1
WHEN A1=1 AND Pmax=1 AND A4=3 THEN 1
WHEN A1=1 AND Pind=1 AND A4=3 THEN 1
WHEN A1=1 AND P0=1 AND A4=3 THEN 1
WHEN A1=2 AND Pmax=1 AND A4=3 THEN 1
WHEN A1=2 AND Pcrim=1 AND A4=2 THEN 1
WHEN A1=2 AND Pmax=1 AND A4=2 THEN 1
ELSE 0 END ) AS G3,

SUM(CASE
WHEN A1=3 AND Paag=1 AND A4=3 THEN 1
WHEN A1=3 AND Paag=1 AND A4=1 THEN 1
WHEN A1=3 AND P0=1 AND A4=1 THEN 1
WHEN A1=3 AND Paag=1 AND A4=2 THEN 1
WHEN A1=3 AND P0=1 AND A4=2 THEN 1
WHEN A1=1 AND Paag=1 AND A4=3 THEN 1
WHEN A1=2 AND Paag=1 AND A4=3 THEN 1
WHEN A1=2 AND P0=1 AND A4=3 THEN 1
ELSE 0 END) AS G4,

SUM(CASE
WHEN A1=1 AND Pakt=1 AND A4=1 THEN 1
WHEN A1=1 AND Pcrim=1 AND A4=1 THEN 1
WHEN A1=1 AND Pout=1 AND A4=1 THEN 1
WHEN A1=1 AND Pmax=1 AND A4=1 THEN 1
WHEN A1=1 AND Paag=1 AND A4=1 THEN 1
WHEN A1=1 AND Pind=1 AND A4=1 THEN 1
WHEN A1=1 AND P0=1 AND A4=1 THEN 1
WHEN A1=1 AND Pakt=1 AND A4=2 THEN 1
WHEN A1=1 AND Pcrim=1 AND A4=2 THEN 1
WHEN A1=1 AND Pout=1 AND A4=2 THEN 1
WHEN A1=1 AND Pmax=1 AND A4=2 THEN 1
WHEN A1=1 AND Paag=1 AND A4=2 THEN 1
WHEN A1=1 AND Pind=1 AND A4=2 THEN 1
WHEN A1=1 AND P0=1 AND A4=2 THEN 1
WHEN A1=2 AND Pakt=1 AND A4=1 THEN 1
WHEN A1=2 AND Pcrim=1 AND A4=1 THEN 1
WHEN A1=2 AND Pout=1 AND A4=1 THEN 1
WHEN A1=2 AND Pmax=1 AND A4=1 THEN 1
WHEN A1=2 AND Paag=1 AND A4=1 THEN 1
WHEN A1=2 AND Pind=1 AND A4=1 THEN 1
WHEN A1=2 AND P0=1 AND A4=1 THEN 1
WHEN A1=2 AND Pakt=1 AND A4=2 THEN 1
WHEN A1=2 AND Pout=1 AND A4=2 THEN 1
WHEN A1=2 AND Paag=1 AND A4=2 THEN 1
WHEN A1=2 AND Pind=1 AND A4=2 THEN 1
WHEN A1=2 AND P0=1 AND A4=2 THEN 1
ELSE 0 END) AS G5

FROM (SELECT
(SELECT CASE WHEN SUM(cmp_key) / 19 < 2 THEN 1 WHEN SUM(cmp_key) / 19 >= 2 AND SUM(cmp_key) / 19 < 3 THEN 2 ELSE 3 END
FROM asf_data_index WHERE uuid = rg.asfDataID  AND cmp_id LIKE 'frustrated_otvet%') AS A1,

(SELECT CASE
WHEN SUM(IF(problem.cmp_key=0 AND status.cmp_key=1, 1, 0)) > 2 THEN 1
WHEN SUM(IF(problem.cmp_key=0 AND status.cmp_key=1, 1, 0)) = 2 THEN 2
WHEN SUM(problem.cmp_key) BETWEEN 0 AND 16 THEN 3
WHEN SUM(problem.cmp_key) BETWEEN 17 AND 32 THEN 2
WHEN SUM(problem.cmp_key) > 32 THEN 1 END
FROM asf_data_index problem JOIN asf_data_index status ON status.uuid=problem.uuid
WHERE problem.uuid=rg.asfDataID AND problem.cmp_id LIKE 'problem_otvet%'
AND status.cmp_id=CONCAT('block_importance', SUBSTRING_INDEX(problem.cmp_id, 'problem_otvet', -1))) AS A4,

konflict.Pakt, konflict.Pind, konflict.Pcrim, konflict.Pout, konflict.Pmax, konflict.P0, konflict.Paag

FROM registry_documents rg
JOIN asf_data_index company ON company.uuid=rg.asfDataID AND company.cmp_id='company'
LEFT JOIN (SELECT uuid,
IF(SUM(IF(cmp_key=1, 1, 0))>0, 1, 0) AS Pakt,
IF(SUM(IF(cmp_key=2, 1, 0))>1, 1, 0) AS Pcrim,
IF(SUM(IF(cmp_key=3, 1, 0))>1, 1, 0) AS Pout,
IF(SUM(IF(cmp_key=4, 1, 0))>3, 1, 0) AS Pmax,
IF(SUM(IF(cmp_key=5, 1, 0))>3, 1, 0) AS Paag,
IF(SUM(IF(cmp_key=6, 1, 0))>0, 1, 0) AS Pind,
IF(SUM(IF(cmp_key=7, 1, 0))>3, 1, 0) AS P0
FROM asf_data_index WHERE cmp_id LIKE 'conflict_otvet%' GROUP BY uuid) konflict ON konflict.uuid=rg.asfDataID
WHERE rg.registryID='$regAnketa' AND rg.deleted IS NULL AND rg.active='Y' AND YEAR(rg.created)=YEAR(NOW()) AND company.cmp_key='$companyKey'
GROUP BY rg.asfDataID) t ) t2;" > $tmpSQL2

d1=`cat $tmpSQL2 | sed -n 2p | cut -f1`
d2=`cat $tmpSQL2 | sed -n 2p | cut -f2`
g1=`cat $tmpSQL2 | sed -n 2p | cut -f3`
g2=`cat $tmpSQL2 | sed -n 2p | cut -f4`
g3=`cat $tmpSQL2 | sed -n 2p | cut -f5`
g4=`cat $tmpSQL2 | sed -n 2p | cut -f6`
g5=`cat $tmpSQL2 | sed -n 2p | cut -f7`
gp1=`cat $tmpSQL2 | sed -n 2p | cut -f8`
gp2=`cat $tmpSQL2 | sed -n 2p | cut -f9`
gp3=`cat $tmpSQL2 | sed -n 2p | cut -f10`
gp4=`cat $tmpSQL2 | sed -n 2p | cut -f11`
gp5=`cat $tmpSQL2 | sed -n 2p | cut -f12`

# формирование правильной строки data для дочерней компании

newData='"data":[{"id":"company","type":"reglink","value":"'$companyName'","key":"'$companyKey'","valueID":"'$companyKey'","username":"Admin Admin Admin","userID":"1"},{"id":"date","type":"date","value":"'$d1'","key":"'$d2'"},{"id":"percent1","type":"textbox","value":"'$gp1'"},{"id":"count1","type":"textbox","value":"'$g1'"},{"id":"name1","type":"label","label":"'$g1name'","value":" "},{"id":"percent2","type":"textbox","value":"'$gp2'"},{"id":"count2","type":"textbox","value":"'$g2'"},{"id":"name2","type":"label","label":"'$g2name'","value":" "},{"id":"percent3","type":"textbox","value":"'$gp3'"},{"id":"count3","type":"textbox","value":"'$g3'"},{"id":"name3","type":"label","label":"'$g3name'","value":" "},{"id":"percent4","type":"textbox","value":"'$gp4'"},{"id":"count4","type":"textbox","value":"'$g4'"},{"id":"name4","type":"label","label":"'$g4name'","value":" "},{"id":"percent5","type":"textbox","value":"'$gp5'"},{"id":"count5","type":"textbox","value":"'$g5'"},{"id":"name5","type":"label","label":"'$g5name'","value":" "}]'

# создание записи реестра
#wget --quiet --http-user="$synergyUser" --http-password="$synergyPass" --output-document $tmpfile "$synergyURL/rest/api/registry/create_doc?registryID=$registryid"
curl --user $synergyUser:$synergyPass --request GET "$synergyURL/rest/api/registry/create_doc?registryID=$registryid" --output $tmpfile --silent

# берем uuid созданной записи
newUUID=`cat $tmpfile | sed "s/.*\dataUUID\": \"\(.*\)\", \"asfNodeID.*/\1/"`

# выполняем api метод POST (изменяем значение поля на форме)
curl --user $synergyUser:$synergyPass --request POST "$synergyURL/rest/api/asforms/data/save" --data "formUUID=$formid" --data "uuid=$newUUID" --data "data=$newData" --output $tmpfile --silent

# активация записи реестра
#wget --quiet --http-user="$synergyUser" --http-password="$synergyPass" --output-document $tmpfile "$synergyURL/rest/api/registry/activate_doc?dataUUID=$newUUID"
curl --user $synergyUser:$synergyPass --request GET "$synergyURL/rest/api/registry/activate_doc?dataUUID=$newUUID" --output $tmpfile --silent

done
fi

# Массив ID дочерних компаний
allCompanyKey=`echo $allCompanyKey | rev | sed 's/,//' | rev`

# Поиск всех родителей
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT parent.cmp_data, parent.cmp_key
FROM registry_documents rg JOIN asf_data_index parent ON parent.uuid=rg.asfDataID AND parent.cmp_id='parent'
WHERE rg.registryID='d9b4245b-7be7-4a6d-b085-0b7943653a96' AND rg.deleted IS NULL AND rg.active='Y' AND rg.documentid IN ($allCompanyKey)
GROUP BY parent.cmp_key;"> $tmpSQL1

# Количество строк в файле tmpSQL1
countStr=`cat $tmpSQL1 | wc -l`

# проверяем, есть ли родители для которых нужно посчитать
if [ -s $tmpSQL1 ];
then

for i in `seq 2 $countStr`;
do
parentKey=`cat $tmpSQL1 | sed -n $i'p' | cut -f2`
parentName=`cat $tmpSQL1 | sed -n $i'p' | cut -f1`

# Пересчет групп опасности для родителей
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT G1, G2, G3, G4, G5,
ROUND(G1*100/sumGroup,1) AS percentG1, ROUND(G2*100/sumGroup,1) AS percentG2,
ROUND(G3*100/sumGroup,1) AS percentG3, ROUND(G4*100/sumGroup,1) AS percentG4,
ROUND(G5*100/sumGroup,1) AS percentG5

FROM (
SELECT SUM(c1.cmp_data)+SUM(c2.cmp_data)+SUM(c3.cmp_data)+SUM(c4.cmp_data)+SUM(c5.cmp_data) AS sumGroup,
       SUM(c1.cmp_data) AS G1, SUM(c2.cmp_data) AS G2, SUM(c3.cmp_data) AS G3,
       SUM(c4.cmp_data) AS G4, SUM(c5.cmp_data) AS G5

FROM registry_documents rg
JOIN asf_data_index company ON company.uuid=rg.asfDataID AND company.cmp_id='company'
JOIN asf_data_index c1 ON c1.uuid=rg.asfDataID AND c1.cmp_id='count1'
JOIN asf_data_index c2 ON c2.uuid=rg.asfDataID AND c2.cmp_id='count2'
JOIN asf_data_index c3 ON c3.uuid=rg.asfDataID AND c3.cmp_id='count3'
JOIN asf_data_index c4 ON c4.uuid=rg.asfDataID AND c4.cmp_id='count4'
JOIN asf_data_index c5 ON c5.uuid=rg.asfDataID AND c5.cmp_id='count5'

WHERE rg.registryID='$registryid' AND rg.deleted IS NULL AND rg.active='Y'
AND rg.asfDataID IN (SELECT
(SELECT asfDataID FROM registry_documents
JOIN asf_data_index company ON company.uuid=registry_documents.asfDataID AND company.cmp_id='company'
WHERE registryID='$registryid' AND company.cmp_key=rg.documentID AND registry_documents.deleted IS NULL AND registry_documents.active='Y'
ORDER BY registry_documents.created DESC LIMIT 1)
FROM registry_documents rg
JOIN asf_data_index parent ON parent.uuid=rg.asfDataID AND parent.cmp_id='parent'
JOIN asf_data_index company ON company.uuid=rg.asfDataID AND company.cmp_id='settings_name'
WHERE rg.registryID='$regCompany' AND rg.deleted IS NULL AND rg.active='Y' AND parent.cmp_key='$parentKey') ) t;" > $tmpSQL2

g1=`cat $tmpSQL2 | sed -n 2p | cut -f1`
g2=`cat $tmpSQL2 | sed -n 2p | cut -f2`
g3=`cat $tmpSQL2 | sed -n 2p | cut -f3`
g4=`cat $tmpSQL2 | sed -n 2p | cut -f4`
g5=`cat $tmpSQL2 | sed -n 2p | cut -f5`
gp1=`cat $tmpSQL2 | sed -n 2p | cut -f6`
gp2=`cat $tmpSQL2 | sed -n 2p | cut -f7`
gp3=`cat $tmpSQL2 | sed -n 2p | cut -f8`
gp4=`cat $tmpSQL2 | sed -n 2p | cut -f9`
gp5=`cat $tmpSQL2 | sed -n 2p | cut -f10`

# формирование правильной строки data для родительской компании
newData='"data":[{"id":"company","type":"reglink","value":"'$parentName'","key":"'$parentKey'","valueID":"'$parentKey'","username":"Admin Admin Admin","userID":"1"},{"id":"date","type":"date","value":"'$d1'","key":"'$d2'"},{"id":"percent1","type":"textbox","value":"'$gp1'"},{"id":"count1","type":"textbox","value":"'$g1'"},{"id":"name1","type":"label","label":"'$g1name'","value":" "},{"id":"percent2","type":"textbox","value":"'$gp2'"},{"id":"count2","type":"textbox","value":"'$g2'"},{"id":"name2","type":"label","label":"'$g2name'","value":" "},{"id":"percent3","type":"textbox","value":"'$gp3'"},{"id":"count3","type":"textbox","value":"'$g3'"},{"id":"name3","type":"label","label":"'$g3name'","value":" "},{"id":"percent4","type":"textbox","value":"'$gp4'"},{"id":"count4","type":"textbox","value":"'$g4'"},{"id":"name4","type":"label","label":"'$g4name'","value":" "},{"id":"percent5","type":"textbox","value":"'$gp5'"},{"id":"count5","type":"textbox","value":"'$g5'"},{"id":"name5","type":"label","label":"'$g5name'","value":" "}]'

# создание записи реестра родительской компании
#wget --quiet --http-user="$synergyUser" --http-password="$synergyPass" --output-document $tmpfile "$synergyURL/rest/api/registry/create_doc?registryID=$registryid"
curl --user $synergyUser:$synergyPass --request GET "$synergyURL/rest/api/registry/create_doc?registryID=$registryid" --output $tmpfile --silent

# берем uuid созданной записи
newUUID=`cat $tmpfile | sed "s/.*\dataUUID\": \"\(.*\)\", \"asfNodeID.*/\1/"`

# выполняем api метод POST
curl --user $synergyUser:$synergyPass --request POST "$synergyURL/rest/api/asforms/data/save" --data "formUUID=$formid" --data "uuid=$newUUID" --data "data=$newData" --output $tmpfile --silent

# активация записи реестра
#wget --quiet --http-user="$synergyUser" --http-password="$synergyPass" --output-document $tmpfile "$synergyURL/rest/api/registry/activate_doc?dataUUID=$newUUID"
curl --user $synergyUser:$synergyPass --request GET "$synergyURL/rest/api/registry/activate_doc?dataUUID=$newUUID" --output $tmpfile --silent

done
fi

# удаление временных файлов
rm -f $tmpSQL1
rm -f $tmpSQL2
rm -f $tmpfile
