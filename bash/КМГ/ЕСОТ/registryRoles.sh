#!/bin/bash

# настройки
scriptName="registryRoles"
logFile="/var/log/synergy/scripts.log"
tmpSQL1="/tmp/rrSQL1.kmg"
tmpSQL2="/tmp/rrSQL2.kmg"


# Функция получения текущего времени
function now_time() {
  date +"%Y-%m-%d %H:%M:%S"
}
# Функция логирования
function logging() {
  echo -e "`now_time` #$1# [$2] $3" >> $logFile
}

echo '' >> $logFile
logging $scriptName START "Начало работы скрипта"

echo '' >> $logFile
logging $scriptName INFO "ДЗО, кмг"

logging $scriptName STATUS "Генерация списка реестров"
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT registryID, name FROM registries
WHERE !hidden;"  > $tmpSQL1

logging $scriptName STATUS "Генерация списка групп"
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT g.id, t.text FROM groups g
LEFT JOIN translations t ON t.translationID = g.translationID AND t.localeID='ru'
WHERE t.text IN ('ДЗО', 'кмг');"  > $tmpSQL2

logging $scriptName STATUS "Добавление записией в таблицу [НАЧАЛО]"
if [ -s $tmpSQL1 ];
then
  if [ -s $tmpSQL2 ];
  then
    rStr=`cat $tmpSQL1 | wc -l`
    gStr=`cat $tmpSQL2 | wc -l`
    for i in `seq 2 $rStr`;
    do
      registryID=`cat $tmpSQL1 | sed -n $i'p' | cut -f1`
      for j in `seq 2 $gStr`;
      do
        groupID=`cat $tmpSQL2 | sed -n $j'p' | cut -f1`
        logging $scriptName RESULT "registryID = [$registryID] groupID = [$groupID]"
        mysql -uroot -proot -e "use synergy; set names utf8;
        DELETE FROM registry_rights WHERE registryID = '$registryID' AND groupID = '$groupID';
        INSERT INTO registry_rights (registryID, groupID, rr_create, rr_list, rr_read, rr_edit, rr_delete, rr_modify)
        VALUES ('$registryID', '$groupID', 'Y', 'Y', 'Y', 'Y', 'Y', 'Y');"
      done
    done
  fi
fi
logging $scriptName STATUS "Добавление записией в таблицу [КОНЕЦ]"


echo '' >> $logFile
logging $scriptName INFO "Кадровики"

logging $scriptName STATUS "Генерация списка реестров"
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT registryID, name FROM registries
WHERE !hidden AND (LOWER(code) LIKE 'приказ%' OR LOWER(code) LIKE 'заявление%' OR LOWER(code) LIKE 'служебн%'
OR code IN ('Справка,_подтверждающая_стаж_работы_', 'Справка_о_сданных_и_несданных_имуществ', 'Справка_с_места_работы', 'Справка_подтверждающая_особый_характер_работы_или_условия_труда', 'Справка_о_заработной_плате', 'Докладная_записка_о_привлечении_работника_к_дисциплинарной_ответственности', 'Квал_требования', 'Обязательство_о_неразглашении_служебной,_коммерческой_тайны', 'Анкета_при_увольнении', 'Больничный', 'Уведомление_об_изменении_условии_труда', 'Уведомление_в_связи_с_достижением_пенсионного_возраста', 'Уведомление_в_связи_со_снижением_объема_производства', 'Уведомление_в_связи_с_ликвидацией', 'Уведомление_о_расторжении_ТД', 'Уведомление_в_связи_с_сокращением', 'Представление_о_приеме_на_работу', 'Должностная_инструкция', 'изменения_наименования_компаниия', 'Обходной_лист_при_увольнении', 'Расчетный_лист', 'Путевой_лист', 'Доп._соглашение_к_ТД_(изменение_в_ТД)', 'Доп._соглашение_к_Трудовому_договору_на_временную_основу', 'Договор_на_обучение', 'Договор_на_обучение_новый'));"  > $tmpSQL1

logging $scriptName STATUS "Генерация списка групп"
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT g.id, t.text FROM groups g
LEFT JOIN translations t ON t.translationID = g.translationID AND t.localeID='ru'
WHERE t.text IN ('Кадровики');"  > $tmpSQL2

logging $scriptName STATUS "Добавление записией в таблицу [НАЧАЛО]"
if [ -s $tmpSQL1 ];
then
  if [ -s $tmpSQL2 ];
  then
    rStr=`cat $tmpSQL1 | wc -l`
    gStr=`cat $tmpSQL2 | wc -l`
    for i in `seq 2 $rStr`;
    do
      registryID=`cat $tmpSQL1 | sed -n $i'p' | cut -f1`
      for j in `seq 2 $gStr`;
      do
        groupID=`cat $tmpSQL2 | sed -n $j'p' | cut -f1`
        logging $scriptName RESULT "registryID = [$registryID] groupID = [$groupID]"
        mysql -uroot -proot -e "use synergy; set names utf8;
        DELETE FROM registry_rights WHERE registryID = '$registryID' AND groupID = '$groupID';
        INSERT INTO registry_rights (registryID, groupID, rr_create, rr_list, rr_read, rr_edit, rr_delete, rr_modify)
        VALUES ('$registryID', '$groupID', 'Y', 'Y', 'Y', 'Y', 'N', 'Y');"
      done
    done
  fi
fi
logging $scriptName STATUS "Добавление записией в таблицу [КОНЕЦ]"


echo '' >> $logFile
logging $scriptName INFO "права на заявления"

logging $scriptName STATUS "Генерация списка реестров"
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT registryID, name FROM registries
WHERE !hidden AND LOWER(code) LIKE 'заявление%';"  > $tmpSQL1

logging $scriptName STATUS "Генерация списка групп"
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT g.id, t.text FROM groups g
LEFT JOIN translations t ON t.translationID = g.translationID AND t.localeID='ru'
WHERE t.text IN ('Бухгалтерия', 'Оплатчики', 'Руководство', 'Социальщики', 'Юристы');"  > $tmpSQL2

logging $scriptName STATUS "Добавление записией в таблицу [НАЧАЛО]"
if [ -s $tmpSQL1 ];
then
  if [ -s $tmpSQL2 ];
  then
    rStr=`cat $tmpSQL1 | wc -l`
    gStr=`cat $tmpSQL2 | wc -l`
    for i in `seq 2 $rStr`;
    do
      registryID=`cat $tmpSQL1 | sed -n $i'p' | cut -f1`
      for j in `seq 2 $gStr`;
      do
        groupID=`cat $tmpSQL2 | sed -n $j'p' | cut -f1`
        logging $scriptName RESULT "registryID = [$registryID] groupID = [$groupID]"
        mysql -uroot -proot -e "use synergy; set names utf8;
        DELETE FROM registry_rights WHERE registryID = '$registryID' AND groupID = '$groupID';
        INSERT INTO registry_rights (registryID, groupID, rr_create, rr_list, rr_read, rr_edit, rr_delete, rr_modify)
        VALUES ('$registryID', '$groupID', 'Y', 'N', 'N', 'Y', 'N', 'Y');"
      done
    done
  fi
fi
logging $scriptName STATUS "Добавление записией в таблицу [КОНЕЦ]"


echo '' >> $logFile
logging $scriptName INFO "права на служебки"

logging $scriptName STATUS "Генерация списка реестров"
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT registryID, name FROM registries
WHERE !hidden AND LOWER(code) LIKE 'служебн%';"  > $tmpSQL1

logging $scriptName STATUS "Генерация списка групп"
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT g.id, t.text FROM groups g
LEFT JOIN translations t ON t.translationID = g.translationID AND t.localeID='ru'
WHERE t.text IN ('Руководство', 'Социальщики');"  > $tmpSQL2

logging $scriptName STATUS "Добавление записией в таблицу [НАЧАЛО]"
if [ -s $tmpSQL1 ];
then
  if [ -s $tmpSQL2 ];
  then
    rStr=`cat $tmpSQL1 | wc -l`
    gStr=`cat $tmpSQL2 | wc -l`
    for i in `seq 2 $rStr`;
    do
      registryID=`cat $tmpSQL1 | sed -n $i'p' | cut -f1`
      for j in `seq 2 $gStr`;
      do
        groupID=`cat $tmpSQL2 | sed -n $j'p' | cut -f1`
        logging $scriptName RESULT "registryID = [$registryID] groupID = [$groupID]"
        mysql -uroot -proot -e "use synergy; set names utf8;
        DELETE FROM registry_rights WHERE registryID = '$registryID' AND groupID = '$groupID';
        INSERT INTO registry_rights (registryID, groupID, rr_create, rr_list, rr_read, rr_edit, rr_delete, rr_modify)
        VALUES ('$registryID', '$groupID', 'Y', 'N', 'N', 'Y', 'N', 'Y');"
      done
    done
  fi
fi
logging $scriptName STATUS "Добавление записией в таблицу [КОНЕЦ]"


echo '' >> $logFile
logging $scriptName INFO "права на приказы"

logging $scriptName STATUS "Генерация списка реестров"
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT registryID, name FROM registries
WHERE !hidden AND LOWER(code) LIKE 'служебн%';"  > $tmpSQL1

logging $scriptName STATUS "Генерация списка групп"
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT g.id, t.text FROM groups g
LEFT JOIN translations t ON t.translationID = g.translationID AND t.localeID='ru'
WHERE t.text IN ('Бухгалтерия', 'Юристы');"  > $tmpSQL2

logging $scriptName STATUS "Добавление записией в таблицу [НАЧАЛО]"
if [ -s $tmpSQL1 ];
then
  if [ -s $tmpSQL2 ];
  then
    rStr=`cat $tmpSQL1 | wc -l`
    gStr=`cat $tmpSQL2 | wc -l`
    for i in `seq 2 $rStr`;
    do
      registryID=`cat $tmpSQL1 | sed -n $i'p' | cut -f1`
      for j in `seq 2 $gStr`;
      do
        groupID=`cat $tmpSQL2 | sed -n $j'p' | cut -f1`
        logging $scriptName RESULT "registryID = [$registryID] groupID = [$groupID]"
        mysql -uroot -proot -e "use synergy; set names utf8;
        DELETE FROM registry_rights WHERE registryID = '$registryID' AND groupID = '$groupID';
        INSERT INTO registry_rights (registryID, groupID, rr_create, rr_list, rr_read, rr_edit, rr_delete, rr_modify)
        VALUES ('$registryID', '$groupID', 'N', 'Y', 'Y', 'N', 'N', 'N');"
      done
    done
  fi
fi
logging $scriptName STATUS "Добавление записией в таблицу [КОНЕЦ]"


echo '' >> $logFile
logging $scriptName INFO "права на приказы Оплатчики"

logging $scriptName STATUS "Генерация списка реестров"
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT registryID, name FROM registries
WHERE !hidden AND LOWER(code) LIKE 'служебн%';"  > $tmpSQL1

logging $scriptName STATUS "Генерация списка групп"
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT g.id, t.text FROM groups g
LEFT JOIN translations t ON t.translationID = g.translationID AND t.localeID='ru'
WHERE t.text IN ('Оплатчики');"  > $tmpSQL2

logging $scriptName STATUS "Добавление записией в таблицу [НАЧАЛО]"
if [ -s $tmpSQL1 ];
then
  if [ -s $tmpSQL2 ];
  then
    rStr=`cat $tmpSQL1 | wc -l`
    gStr=`cat $tmpSQL2 | wc -l`
    for i in `seq 2 $rStr`;
    do
      registryID=`cat $tmpSQL1 | sed -n $i'p' | cut -f1`
      for j in `seq 2 $gStr`;
      do
        groupID=`cat $tmpSQL2 | sed -n $j'p' | cut -f1`
        logging $scriptName RESULT "registryID = [$registryID] groupID = [$groupID]"
        mysql -uroot -proot -e "use synergy; set names utf8;
        DELETE FROM registry_rights WHERE registryID = '$registryID' AND groupID = '$groupID';
        INSERT INTO registry_rights (registryID, groupID, rr_create, rr_list, rr_read, rr_edit, rr_delete, rr_modify)
        VALUES ('$registryID', '$groupID', 'Y', 'Y', 'Y', 'Y', 'N', 'Y');"
      done
    done
  fi
fi
logging $scriptName STATUS "Добавление записией в таблицу [КОНЕЦ]"


echo '' >> $logFile
logging $scriptName INFO "права на приказы Социальщики"

logging $scriptName STATUS "Генерация списка реестров"
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT registryID, name FROM registries
WHERE !hidden AND code IN ('Приказ_о_внесении_изменений_приказа_об_оказании_материальной_помощи', 'Приказ_о_внесении_изменений_приказа_о_награждении', 'Приказ_об_оказании_материальной_помощи');"  > $tmpSQL1

logging $scriptName STATUS "Генерация списка групп"
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT g.id, t.text FROM groups g
LEFT JOIN translations t ON t.translationID = g.translationID AND t.localeID='ru'
WHERE t.text IN ('Социальщики');"  > $tmpSQL2

logging $scriptName STATUS "Добавление записией в таблицу [НАЧАЛО]"
if [ -s $tmpSQL1 ];
then
  if [ -s $tmpSQL2 ];
  then
    rStr=`cat $tmpSQL1 | wc -l`
    gStr=`cat $tmpSQL2 | wc -l`
    for i in `seq 2 $rStr`;
    do
      registryID=`cat $tmpSQL1 | sed -n $i'p' | cut -f1`
      for j in `seq 2 $gStr`;
      do
        groupID=`cat $tmpSQL2 | sed -n $j'p' | cut -f1`
        logging $scriptName RESULT "registryID = [$registryID] groupID = [$groupID]"
        mysql -uroot -proot -e "use synergy; set names utf8;
        DELETE FROM registry_rights WHERE registryID = '$registryID' AND groupID = '$groupID';
        INSERT INTO registry_rights (registryID, groupID, rr_create, rr_list, rr_read, rr_edit, rr_delete, rr_modify)
        VALUES ('$registryID', '$groupID', 'Y', 'Y', 'Y', 'Y', 'N', 'Y');"
      done
    done
  fi
fi
logging $scriptName STATUS "Добавление записией в таблицу [КОНЕЦ]"


echo '' >> $logFile
logging $scriptName INFO "еще 2 реестра для Оплатчики"

logging $scriptName STATUS "Генерация списка реестров"
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT registryID, name FROM registries
WHERE !hidden AND code IN ('Справочник_циклов', 'Баланс_рабочего_времени');"  > $tmpSQL1

logging $scriptName STATUS "Генерация списка групп"
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT g.id, t.text FROM groups g
LEFT JOIN translations t ON t.translationID = g.translationID AND t.localeID='ru'
WHERE t.text IN ('Оплатчики');"  > $tmpSQL2

logging $scriptName STATUS "Добавление записией в таблицу [НАЧАЛО]"
if [ -s $tmpSQL1 ];
then
  if [ -s $tmpSQL2 ];
  then
    rStr=`cat $tmpSQL1 | wc -l`
    gStr=`cat $tmpSQL2 | wc -l`
    for i in `seq 2 $rStr`;
    do
      registryID=`cat $tmpSQL1 | sed -n $i'p' | cut -f1`
      for j in `seq 2 $gStr`;
      do
        groupID=`cat $tmpSQL2 | sed -n $j'p' | cut -f1`
        logging $scriptName RESULT "registryID = [$registryID] groupID = [$groupID]"
        mysql -uroot -proot -e "use synergy; set names utf8;
        DELETE FROM registry_rights WHERE registryID = '$registryID' AND groupID = '$groupID';
        INSERT INTO registry_rights (registryID, groupID, rr_create, rr_list, rr_read, rr_edit, rr_delete, rr_modify)
        VALUES ('$registryID', '$groupID', 'N', 'Y', 'Y', 'N', 'N', 'N');"
      done
    done
  fi
fi
logging $scriptName STATUS "Добавление записией в таблицу [КОНЕЦ]"



echo '' >> $logFile
logging $scriptName INFO "графики, табеля, инициации"

logging $scriptName STATUS "Генерация списка реестров"
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT registryID, name FROM registries
WHERE !hidden AND code IN ('Табель_за_период', 'Табель._месяц', 'График_отпусков', 'График_работы_на_месяц', 'График_за_месяц', 'Годовой_график_new1', 'a121_Смена_и_вахта_сотрудника', 'Инициация_графика_за_месяц1', 'Инициация_графика_работ_за_месяц', 'Инициация_табеля_за_период');"  > $tmpSQL1

logging $scriptName STATUS "Генерация списка групп"
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT g.id, t.text FROM groups g
LEFT JOIN translations t ON t.translationID = g.translationID AND t.localeID='ru'
WHERE t.text IN ('Инициаторы', 'Оплатчики', 'Табель');"  > $tmpSQL2

logging $scriptName STATUS "Добавление записией в таблицу [НАЧАЛО]"
if [ -s $tmpSQL1 ];
then
  if [ -s $tmpSQL2 ];
  then
    rStr=`cat $tmpSQL1 | wc -l`
    gStr=`cat $tmpSQL2 | wc -l`
    for i in `seq 2 $rStr`;
    do
      registryID=`cat $tmpSQL1 | sed -n $i'p' | cut -f1`
      for j in `seq 2 $gStr`;
      do
        groupID=`cat $tmpSQL2 | sed -n $j'p' | cut -f1`
        logging $scriptName RESULT "registryID = [$registryID] groupID = [$groupID]"
        mysql -uroot -proot -e "use synergy; set names utf8;
        DELETE FROM registry_rights WHERE registryID = '$registryID' AND groupID = '$groupID';
        INSERT INTO registry_rights (registryID, groupID, rr_create, rr_list, rr_read, rr_edit, rr_delete, rr_modify)
        VALUES ('$registryID', '$groupID', 'Y', 'Y', 'Y', 'Y', 'N', 'Y');"
      done
    done
  fi
fi
logging $scriptName STATUS "Добавление записией в таблицу [КОНЕЦ]"


echo '' >> $logFile
logging $scriptName INFO "графики, табеля, инициации для Руководство"

logging $scriptName STATUS "Генерация списка реестров"
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT registryID, name FROM registries
WHERE !hidden AND code IN ('Табель_за_период', 'Табель._месяц', 'График_отпусков', 'График_работы_на_месяц', 'График_за_месяц', 'Годовой_график_new1', 'a121_Смена_и_вахта_сотрудника', 'Инициация_графика_за_месяц1', 'Инициация_графика_работ_за_месяц', 'Инициация_табеля_за_период');"  > $tmpSQL1

logging $scriptName STATUS "Генерация списка групп"
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT g.id, t.text FROM groups g
LEFT JOIN translations t ON t.translationID = g.translationID AND t.localeID='ru'
WHERE t.text IN ('Руководство');"  > $tmpSQL2

logging $scriptName STATUS "Добавление записией в таблицу [НАЧАЛО]"
if [ -s $tmpSQL1 ];
then
  if [ -s $tmpSQL2 ];
  then
    rStr=`cat $tmpSQL1 | wc -l`
    gStr=`cat $tmpSQL2 | wc -l`
    for i in `seq 2 $rStr`;
    do
      registryID=`cat $tmpSQL1 | sed -n $i'p' | cut -f1`
      for j in `seq 2 $gStr`;
      do
        groupID=`cat $tmpSQL2 | sed -n $j'p' | cut -f1`
        logging $scriptName RESULT "registryID = [$registryID] groupID = [$groupID]"
        mysql -uroot -proot -e "use synergy; set names utf8;
        DELETE FROM registry_rights WHERE registryID = '$registryID' AND groupID = '$groupID';
        INSERT INTO registry_rights (registryID, groupID, rr_create, rr_list, rr_read, rr_edit, rr_delete, rr_modify)
        VALUES ('$registryID', '$groupID', 'N', 'Y', 'Y', 'Y', 'N', 'Y');"
      done
    done
  fi
fi
logging $scriptName STATUS "Добавление записией в таблицу [КОНЕЦ]"


echo '' >> $logFile
logging $scriptName INFO "договора и доп. соглашения для юристов"

logging $scriptName STATUS "Генерация списка реестров"
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT registryID, name FROM registries
WHERE !hidden AND code IN ('Доп._соглашение_к_ТД_(изменение_в_ТД)', 'Доп._соглашение_к_Трудовому_договору_на_временную_основу', 'Договор_на_обучение', 'Договор_на_обучение_новый');"  > $tmpSQL1

logging $scriptName STATUS "Генерация списка групп"
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT g.id, t.text FROM groups g
LEFT JOIN translations t ON t.translationID = g.translationID AND t.localeID='ru'
WHERE t.text IN ('Юристы');"  > $tmpSQL2

logging $scriptName STATUS "Добавление записией в таблицу [НАЧАЛО]"
if [ -s $tmpSQL1 ];
then
  if [ -s $tmpSQL2 ];
  then
    rStr=`cat $tmpSQL1 | wc -l`
    gStr=`cat $tmpSQL2 | wc -l`
    for i in `seq 2 $rStr`;
    do
      registryID=`cat $tmpSQL1 | sed -n $i'p' | cut -f1`
      for j in `seq 2 $gStr`;
      do
        groupID=`cat $tmpSQL2 | sed -n $j'p' | cut -f1`
        logging $scriptName RESULT "registryID = [$registryID] groupID = [$groupID]"
        mysql -uroot -proot -e "use synergy; set names utf8;
        DELETE FROM registry_rights WHERE registryID = '$registryID' AND groupID = '$groupID';
        INSERT INTO registry_rights (registryID, groupID, rr_create, rr_list, rr_read, rr_edit, rr_delete, rr_modify)
        VALUES ('$registryID', '$groupID', 'N', 'Y', 'Y', 'N', 'N', 'N');"
      done
    done
  fi
fi
logging $scriptName STATUS "Добавление записией в таблицу [КОНЕЦ]"


echo '' >> $logFile
logging $scriptName STATUS "Удаление временных файлов"
rm -f $tmpSQL1
rm -f $tmpSQL2
logging $scriptName END "Завершение работы скрипта"
