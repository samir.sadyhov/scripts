#!/bin/bash

# общие настройки synergy
synergyUser="Administrator"
synergyPass="\$ystemAdm1n"
synergyURL="http://127.0.0.1:8080/Synergy"


# formID по карточкам
cardDepartment="c8790f11-5f76-4c35-ae24-e9aecbdec084" # подразделение
cardPosition="432ec478-7b37-43ec-ae39-90e1b361f981" # должность
cardUser="085bfdcb-3663-43ca-99d2-96e25a2fc813" # пользователь

# вспомогательные настройки
logFile="/var/log/synergy/copyCards.log"
tmpSQL1="/tmp/copyCardsSQL1.kmg"
tmpSQL2="/tmp/copyCardsSQL2.kmg"
tmpSQL3="/tmp/copyCardsSQL3.kmg"
tmpfile="/tmp/copyCardsData.kmg"

# Функция получения текущего времени
function now_time() {
  date +"%Y-%m-%d %H:%M:%S"
}
# Функция логирования
function logging() {
  echo -e "`now_time` [$1] $2" >> $logFile
}

echo '' >> $logFile
logging START "Начало работы скрипта"
logging INFO "ID формы карточки подразделения $cardDepartment"
logging INFO "ID формы карточки должности $cardPosition"
logging INFO "ID формы карточки пользователя $cardUser"

######
######  ЧАСТЬ 1 РАБОТА ПО ДОЛЖНОСТЯМ
######
echo '' >> $logFile
logging MSG "ЧАСТЬ 1 РАБОТА ПО ДОЛЖНОСТЯМ"
# вытащить все ID должностей где есть новые записи в карточке подразделения
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT pfd.positionID, pfd.asf_data_uuid
FROM position_forms_data pfd JOIN asf_data ad ON ad.uuid=pfd.asf_data_uuid
WHERE pfd.form_uuid='$cardPosition' AND ad.fdata LIKE '%payments%'
AND ad.modified BETWEEN NOW()-INTERVAL 1 MINUTE AND NOW();" > $tmpSQL1

# проверка если файл не пустой (т.е. если есть записи в карточке должности)
if [ -s $tmpSQL1 ];
then
  echo '' >> $logFile
  logging RESULT "Результат проверки карточек должностей\n`cat $tmpSQL1`"
  # Количество строк в файле tmpSQL1
  countStr=`cat $tmpSQL1 | wc -l`
  for i in `seq 2 $countStr`;
  do
    positionID=`cat $tmpSQL1 | sed -n $i'p' | cut -f1`
    posAsfDataID=`cat $tmpSQL1 | sed -n $i'p' | cut -f2`
    echo '------------------------------------------------------------------------' >> $logFile
    logging INFO "Должность positionID: $positionID"
    logging INFO "Карточка должности asf_data_uuid: $posAsfDataID"
    # вытащить все ID карточек пользователей
    mysql -uroot -proot -e "use synergy; set names utf8;
    SELECT prfu.asf_data_uuid, prfu.form_uuid FROM positions pos
    JOIN userpositions upos ON upos.positionID=pos.positionID
    LEFT JOIN personal_record_forms_user prfu ON prfu.userid=upos.userID
    WHERE prfu.form_uuid='$cardUser' AND pos.positionID='$positionID'
    AND pos.deleted IS NULL AND pos.pos_typeID IN (1, 2, 32, 256) AND upos.finishdate IS NULL;" > $tmpSQL2

    if [ -s $tmpSQL2 ];
    then
      # Количество строк в файле tmpSQL1
      countStr2=`cat $tmpSQL2 | wc -l`
      for j in `seq 2 $countStr2`;
      do
        cardAsfDataID=`cat $tmpSQL2 | sed -n $j'p' | cut -f1`
        cardFormID=`cat $tmpSQL2 | sed -n $j'p' | cut -f2`
        newData='KAKAKA"data":[{"id":"cmp-287pd4","type":"label","label":"Выплаты","value":" "},{"id":"payments","type":"appendable_table","key":"","data":[{"id":"cmp-fs5lnt","type":"label","label":"Наименование, вид","value":" "},{"id":"cmp-qkr3tf","type":"label","label":"Коэффициент (если он имеется)","value":" "}'
        tableData=""
        # вытащить данные по карточкам для формирования JSON строки
        mysql -uroot -proot -e "use synergy; set names utf8;
        SELECT a.cmp_data linkValue, a.cmp_key linkKey, b.cmp_data valueValue, b.cmp_key valueKey
        FROM asf_data_index a JOIN asf_data_index b ON b.uuid=a.uuid
        WHERE a.uuid='$cardAsfDataID' AND a.cmp_id LIKE 'link%'
        AND b.cmp_id=CONCAT('value-', SUBSTRING_INDEX(a.cmp_id, '-', -1))
        UNION
        SELECT a.cmp_data linkValue, a.cmp_key linkKey, b.cmp_data valueValue, b.cmp_key valueKey
        FROM asf_data_index a JOIN asf_data_index b ON b.uuid=a.uuid
        WHERE a.uuid='$posAsfDataID' AND a.cmp_id LIKE 'link%'
        AND b.cmp_id=CONCAT('value-', SUBSTRING_INDEX(a.cmp_id, '-', -1));" > $tmpSQL3

        if [ -s $tmpSQL3 ];
        then
        # Количество строк в файле tmpSQL3
        countStr3=`cat $tmpSQL3 | wc -l`
        for k in `seq 2 $countStr3`;
        do
          linkValue=`cat $tmpSQL3 | sed -n $k'p' | cut -f1`
          linkKey=`cat $tmpSQL3 | sed -n $k'p' | cut -f2`
          valueValue=`cat $tmpSQL3 | sed -n $k'p' | cut -f3`
          valueKey=`cat $tmpSQL3 | sed -n $k'p' | cut -f4`
          tableData+=',{"id":"link-b'$[$k-1]'","type":"reglink","value":"'$linkValue'","key":"'$linkKey'","valueID":"'$linkKey'","userID":""},{"id":"value-b'$[$k-1]'","type":"numericinput","label":"false","value":"'$valueValue'","key":"'$valueKey'"}'
        done
        fi
        # формируем json (итоговая строка для передачи в апи)
        newData=$newData$tableData']}]'
        newData=$(echo $newData | sed 's/KAKAKA//')
        logging JSON "`echo $newData`"
        logging INFO "Сохранение данных в карточку пользователя [$cardAsfDataID]"
        curl --user $synergyUser:$synergyPass --request POST "$synergyURL/rest/api/asforms/data/save" --data "formUUID=$cardFormID" --data "uuid=$cardAsfDataID" --data-urlencode "data=$newData" --output $tmpfile --silent
        logging STATUS "`cat $tmpfile`"
      done
    fi
  done
else
  logging INFO "Нет измененных карточек по должностям"
fi

###### AND DATE(ad.modified) = DATE(NOW())
######  ЧАСТЬ 2 РАБОТА ПО ДЕПАРТАМЕНТАМ
######
echo '' >> $logFile
logging MSG "ЧАСТЬ 2 РАБОТА ПО ДЕПАРТАМЕНТАМ"

# вытащить все ID департаментов где есть новые записи в карточке подразделения
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT dfd.departmentID, dfd.asf_data_uuid
FROM department_forms_data dfd JOIN asf_data ad ON ad.uuid=dfd.asf_data_uuid
WHERE dfd.form_uuid='$cardDepartment' AND ad.fdata LIKE '%payments%'
AND ad.modified BETWEEN NOW()-INTERVAL 1 MINUTE AND NOW();" > $tmpSQL1

# проверка если файл не пустой (т.е. если есть записи в карточке подразделения)
if [ -s $tmpSQL1 ];
then
  echo '' >> $logFile
  logging RESULT "Результат проверки карточек подразделения\n`cat $tmpSQL1`"
  # Количество строк в файле tmpSQL1
  countStr=`cat $tmpSQL1 | wc -l`

  for i in `seq 2 $countStr`;
  do
    departmentID=`cat $tmpSQL1 | sed -n $i'p' | cut -f1`
    depAsfDataID=`cat $tmpSQL1 | sed -n $i'p' | cut -f2`
    echo '------------------------------------------------------------------------' >> $logFile
    logging INFO "departmentID: $departmentID"
    logging INFO "Карточка подразделения asf_data_uuid: $depAsfDataID"
    # вытащить все ID карточек (подразделений, должностей, пользователей)
    mysql -uroot -proot -e "use synergy; set names utf8;
    # карточки дочерних подразделений
    SELECT dfd.asf_data_uuid, dfd.form_uuid FROM departments dep
    LEFT JOIN department_forms_data dfd ON dfd.departmentid=dep.departmentID
    WHERE dfd.form_uuid='$cardDepartment'
    AND (dep.parentdepartmentid IN (SELECT departmentid FROM departments WHERE parentdepartmentid='$departmentID' )
         OR dep.parentdepartmentid='$departmentID')
    UNION
    # карточки должностей
    SELECT pfd.asf_data_uuid, pfd.form_uuid FROM positions pos
    LEFT JOIN departments dep ON dep.departmentID=pos.departmentid
    LEFT JOIN position_forms_data pfd ON pfd.positionid=pos.positionID
    WHERE pfd.form_uuid='$cardPosition'
    AND (dep.parentdepartmentid IN (SELECT departmentid FROM departments WHERE parentdepartmentid='$departmentID' )
        OR dep.parentdepartmentid='$departmentID' OR pos.departmentid='$departmentID')
    AND pos.deleted IS NULL AND pos.pos_typeID IN (1, 2, 32, 256)
    UNION
    # карточки пользователей
    SELECT prfu.asf_data_uuid, prfu.form_uuid FROM positions pos
    LEFT JOIN departments dep ON dep.departmentID=pos.departmentid
    JOIN userpositions upos ON upos.positionID=pos.positionID
    LEFT JOIN personal_record_forms_user prfu ON prfu.userid=upos.userID
    WHERE prfu.form_uuid='$cardUser'
    AND (dep.parentdepartmentid IN (SELECT departmentid FROM departments WHERE parentdepartmentid='$departmentID' )
         OR dep.parentdepartmentid='$departmentID' OR pos.departmentid='$departmentID')
    AND pos.deleted IS NULL AND pos.pos_typeID IN (1, 2, 32, 256) AND upos.finishdate IS NULL;" > $tmpSQL2

    if [ -s $tmpSQL2 ];
    then
      # Количество строк в файле tmpSQL1
      countStr2=`cat $tmpSQL2 | wc -l`
      for j in `seq 2 $countStr2`;
      do
        cardAsfDataID=`cat $tmpSQL2 | sed -n $j'p' | cut -f1`
        cardFormID=`cat $tmpSQL2 | sed -n $j'p' | cut -f2`
        newData='KAKAKA"data":[{"id":"cmp-287pd4","type":"label","label":"Выплаты","value":" "},{"id":"payments","type":"appendable_table","key":"","data":[{"id":"cmp-fs5lnt","type":"label","label":"Наименование, вид","value":" "},{"id":"cmp-qkr3tf","type":"label","label":"Коэффициент (если он имеется)","value":" "}'
        tableData=""
        # вытащить данные по карточкам для формирования JSON строки
        mysql -uroot -proot -e "use synergy; set names utf8;
        SELECT a.cmp_data linkValue, a.cmp_key linkKey, b.cmp_data valueValue, b.cmp_key valueKey
        FROM asf_data_index a JOIN asf_data_index b ON b.uuid=a.uuid
        WHERE a.uuid='$cardAsfDataID' AND a.cmp_id LIKE 'link%'
        AND b.cmp_id=CONCAT('value-', SUBSTRING_INDEX(a.cmp_id, '-', -1))
        UNION
        SELECT a.cmp_data linkValue, a.cmp_key linkKey, b.cmp_data valueValue, b.cmp_key valueKey
        FROM asf_data_index a JOIN asf_data_index b ON b.uuid=a.uuid
        WHERE a.uuid='$depAsfDataID' AND a.cmp_id LIKE 'link%'
        AND b.cmp_id=CONCAT('value-', SUBSTRING_INDEX(a.cmp_id, '-', -1));" > $tmpSQL3

        if [ -s $tmpSQL3 ];
        then
        # Количество строк в файле tmpSQL3
        countStr3=`cat $tmpSQL3 | wc -l`
        for k in `seq 2 $countStr3`;
        do
          linkValue=`cat $tmpSQL3 | sed -n $k'p' | cut -f1`
          linkKey=`cat $tmpSQL3 | sed -n $k'p' | cut -f2`
          valueValue=`cat $tmpSQL3 | sed -n $k'p' | cut -f3`
          valueKey=`cat $tmpSQL3 | sed -n $k'p' | cut -f4`
          tableData+=',{"id":"link-b'$[$k-1]'","type":"reglink","value":"'$linkValue'","key":"'$linkKey'","valueID":"'$linkKey'","userID":""},{"id":"value-b'$[$k-1]'","type":"numericinput","label":"false","value":"'$valueValue'","key":"'$valueKey'"}'
        done
        fi
        # формируем json (итоговая строка для передачи в апи)
        newData=$newData$tableData']}]'
        newData=$(echo $newData | sed 's/KAKAKA//')
        logging JSON "`echo $newData`"
        logging INFO "Сохранение данных в карточку [$cardAsfDataID]"
        curl --user $synergyUser:$synergyPass --request POST "$synergyURL/rest/api/asforms/data/save" --data "formUUID=$cardFormID" --data "uuid=$cardAsfDataID" --data-urlencode "data=$newData" --output $tmpfile --silent
        logging STATUS "`cat $tmpfile`"
      done
    fi
  done
else
  logging INFO "Нет измененных карточек по департаментам"
fi

echo '' >> $logFile
logging INFO "Удаление временных файлов"
# удаление временных файлов
rm -f $tmpSQL1
rm -f $tmpSQL2
rm -f $tmpSQL3
rm -f $tmpfile

logging END "Завершение работы скрипта"
