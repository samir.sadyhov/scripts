#!/bin/bash

# общие настройки
scriptName="Birthday"
synergyUser="Administrator"
synergyPass="\$ystemAdm1n"
synergyURL="http://127.0.0.1:8080/Synergy"

# личная карточка
formCodeLK="Штатная_расстановка111"

# Реестр "уведомление о днях рождениях"
registryCode="Уведомление_о_днях_рождениях"
formCodeReg="скрипта_Уведомление_о_днях_рождениях"

# вспомогательные настройки
logFile="/var/log/synergy/scripts.log"
tmpSQL1="/tmp/birthdaySQL1.kmg"
tmpfile="/tmp/birthdayData.kmg"

# Функция получения текущего времени
function now_time() {
  date +"%Y-%m-%d %H:%M:%S"
}
# Функция логирования
function logging() {
  echo -e "`now_time` #$1# [$2] $3" >> $logFile
}

echo '' >> $logFile
logging $scriptName START "Начало работы скрипта"

logging $scriptName STATUS "Поиск сотрудников у которых завтра день рождения"
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT DATE_FORMAT(NOW() + INTERVAL 1 DAY, 'День рождения %d.%m.%Y') AS birthday,
CONCAT_WS(' ', CONCAT(UPPER(LEFT(u.lastname, 1)), LOWER(SUBSTRING(u.lastname, 2))),
CONCAT(UPPER(LEFT(u.firstname, 1)), LOWER(SUBSTRING(u.firstname, 2))),
CONCAT(UPPER(LEFT(u.patronymic, 1)), LOWER(SUBSTRING(u.patronymic, 2)))) fio,
(SELECT text FROM translations WHERE translationID=IFNULL(a.translationID, pos.translationID) AND localeID='ru') posName,
IF(parDep.departmentid = 1,
(SELECT text FROM translations WHERE translationID=dep.translationID AND localeID='ru'),
CONCAT('<b>',(SELECT text FROM translations WHERE translationID=parDep.translationID AND localeID='ru'), '</b>',
'<br>', (SELECT text FROM translations WHERE translationID=dep.translationID AND localeID='ru'))
) AS department,
DATE_FORMAT(dr.cmp_key, '%d.%m.%Y') birthday,
CONCAT(YEAR(NOW())-YEAR(dr.cmp_key), ' ', getYearStr(YEAR(NOW())-YEAR(dr.cmp_key))) vozrast,
(SELECT uuid FROM asf_definition WHERE code = '$formCodeReg') uvedFormId,
(SELECT registryID FROM registries WHERE code = '$registryCode') uvedRegId
FROM users u
JOIN userpositions upos ON upos.userID=u.userID
JOIN positions pos ON pos.positionID=upos.positionID
JOIN departments dep ON dep.departmentID=pos.departmentID
JOIN departments parDep ON parDep.departmentID=dep.parentDepartmentID
LEFT JOIN assistant_positions ap ON ap.positionID=pos.positionID
LEFT JOIN assistants a ON a.assistantID=ap.assistantID
LEFT JOIN personal_record_forms_user prfu ON prfu.userid=u.userID
LEFT JOIN asf_data_index dr ON dr.uuid=prfu.asf_data_uuid AND dr.cmp_id='birthday'
WHERE prfu.form_uuid = (SELECT uuid FROM asf_definition WHERE code = '$formCodeLK')
AND u.finished IS NULL AND upos.finishdate IS NULL
AND pos.pos_typeid IN (1,2,256) AND pos.deleted IS NULL
AND DATE_FORMAT(dr.cmp_key, '%m%d') = DATE_FORMAT(NOW() + INTERVAL 1 DAY, '%m%d')
ORDER BY getStruct(dep.struct_number), fio;" > $tmpSQL1
echo '--------------------------------' >> $logFile
logging $scriptName RESULT "\n `cat $tmpSQL1`"
echo '--------------------------------' >> $logFile

if [ -s $tmpSQL1 ];
then
  tableHTML="<table id='tableDocumentNotific'><thead><tr><th>ФИО</th><th>Должность</th><th>Подразделение</th><th>Дата рождения</th><th>Возраст</th></tr></thead><tbody>"
  countStr=`cat $tmpSQL1 | wc -l`
  birthday=`cat $tmpSQL1 | sed -n '2p' | cut -f1`
  formIDUved=`cat $tmpSQL1 | sed -n '2p' | cut -f7`
  registryid=`cat $tmpSQL1 | sed -n '2p' | cut -f8`
  echo '' >> $logFile
  logging $scriptName INFO "$birthday"
  logging $scriptName INFO "ID реестра: $registryid"
  logging $scriptName INFO "ID формы: $formIDUved"

  for i in `seq 2 $countStr`;
  do
    fio=`cat $tmpSQL1 | sed -n $i'p' | cut -f2`
    pos=`cat $tmpSQL1 | sed -n $i'p' | cut -f3`
    dep=`cat $tmpSQL1 | sed -n $i'p' | cut -f4`
    dep=$(echo $dep | sed 's/\\/\\\\/g;s/\"/\\"/g;s/\«/\\"/g;s/'"'"'/\\"/g;s/\»/\\"/g')
    dr=`cat $tmpSQL1 | sed -n $i'p' | cut -f5`
    voz=`cat $tmpSQL1 | sed -n $i'p' | cut -f6`
    tableHTML+="<tr><td>$fio</td><td>$pos</td><td>$dep</td><td align='center'>$dr</td><td align='center'>$voz</td></tr>"
  done
  tableHTML+="</tbody></table><br>"

  # формируем JSON
  newData='KAKAKA"data":[{"id":"dateNow","type":"textbox","value":"'$birthday'"},{"id":"textHTML","type":"textarea","value":"'$tableHTML'"}]'
  newData=$(echo $newData | sed 's/KAKAKA//')
  logging $scriptName JSON "Итоговый JSON для передачи в API rest/api/asforms/data/save\n`echo $newData`"

  # создание записи реестра
  logging $scriptName STATUS "Создание записи реестра"
  curl --user $synergyUser:$synergyPass --request GET "$synergyURL/rest/api/registry/create_doc?registryID=$registryid" --output $tmpfile --silent
  logging $scriptName RESULT "`cat $tmpfile`"

  # берем uuid созданной записи
  newUUID=`cat $tmpfile | sed "s/.*\dataUUID\": \"\(.*\)\", \"asfNodeID.*/\1/"`
  logging $scriptName RESULT "newUUID: $newUUID"

  # выполняем api метод POST
  logging $scriptName STATUS "Сохранение данных в новую запись"
  curl --user $synergyUser:$synergyPass --request POST "$synergyURL/rest/api/asforms/data/save" --data "formUUID=$formIDUved" --data "uuid=$newUUID" --data-urlencode "data=$newData" --output $tmpfile --silent
  logging $scriptName RESULT "`cat $tmpfile`"

  # активация записи реестра
  logging $scriptName STATUS "Активация записи реестра"
  curl --user $synergyUser:$synergyPass --request GET "$synergyURL/rest/api/registry/activate_doc?dataUUID=$newUUID" --output $tmpfile --silent
  logging $scriptName RESULT "`cat $tmpfile`"
else
  logging $scriptName INFO "Нет сотрудников у которых день рождения завтра"
fi

echo '' >> $logFile
logging $scriptName STATUS "Удаление временных файлов"
rm -f $tmpSQL1
rm -f $tmpfile

logging $scriptName END "Завершение работы скрипта"
