#!/bin/bash

# общие настройки
scriptName="expiredDocuments"
synergyUser="Administrator"
synergyPass="\$ystemAdm1n"
synergyURL="http://127.0.0.1:8080/Synergy"

# личная карточка
formID="6e7ee767-7f9b-4f53-b7d9-04a6091d066f"

# Уведомление о просроченных документах
registryid="4522d848-39b8-48c4-8c2b-92698ef05134"
regForm="f449d563-0251-40cc-9941-2bc26ef02734"

# вспомогательные настройки
logFile="/var/log/synergy/scripts.log"
tmpSQL1="/tmp/notificSQL1.kmg"
tmpfile="/tmp/notificData.kmg"
periodDay=30 #кол-во дней

# Функция получения текущего времени
function now_time() {
  date +"%Y-%m-%d %H:%M:%S"
}
# Функция логирования
function logging() {
  echo -e "`now_time` #$1# [$2] $3" >> $logFile
}

echo '' >> $logFile
logging $scriptName START "Начало работы скрипта"

logging $scriptName STATUS "Поиск сотрудников у которых заканчивается срок действия документов через $periodDay дней"
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT CONCAT_WS(' ', u.lastname, u.firstname, u.patronymic) fio,
(SELECT t.text FROM entity_translations et JOIN translations t ON t.translationID = et.translationID
WHERE et.translationID = IFNULL(a.translationID, pos.translationID) AND t.localeID = 'ru') position,
(SELECT t.text FROM entity_translations et JOIN translations t ON t.translationID = et.translationID
WHERE et.translationID = dep.translationID AND t.localeID = 'ru') department,
IFNULL(DATE_FORMAT(d1.cmp_key, '%d.%m.%Y'), '') udLich,
IFNULL(DATE_FORMAT(d2.cmp_key, '%d.%m.%Y'), '') pasport,
IFNULL(DATE_FORMAT(d3.cmp_key, '%d.%m.%Y'), '') vodUd,
DATE_FORMAT(NOW(),'%d.%m.%Y') dateNow,
DATE_FORMAT(NOW()+INTERVAL '$periodDay' DAY, '%d.%m.%Y') srok
FROM users u
JOIN userpositions upos ON upos.userid=u.userid AND upos.finishdate IS NULL
JOIN positions pos ON pos.positionID=upos.positionID AND pos.deleted IS NULL AND pos.pos_typeID IN (1, 2)
JOIN departments dep ON dep.departmentid=pos.departmentid AND dep.deleted IS NULL
LEFT JOIN assistant_positions ap ON ap.positionID=pos.positionID
LEFT JOIN assistants a ON a.assistantID = ap.assistantID
LEFT JOIN personal_record_forms_user prfu ON prfu.userid=u.userID
LEFT JOIN asf_data_index d1 ON d1.uuid=prfu.asf_data_uuid AND d1.cmp_id='finishdateudv'
LEFT JOIN asf_data_index d2 ON d2.uuid=prfu.asf_data_uuid AND d2.cmp_id='finishdatepassport'
LEFT JOIN asf_data_index d3 ON d3.uuid=prfu.asf_data_uuid AND d3.cmp_id='finishdatevodudv'
WHERE u.finished IS NULL
AND prfu.form_uuid='$formID'
AND (DATE(d1.cmp_key)=DATE(NOW()+INTERVAL '$periodDay' DAY)
  OR DATE(d2.cmp_key)=DATE(NOW()+INTERVAL '$periodDay' DAY)
  OR DATE(d3.cmp_key)=DATE(NOW()+INTERVAL '$periodDay' DAY))
ORDER BY getStruct(dep.struct_number), pos.pos_typeID, u.pointer_code;" > $tmpSQL1
echo '--------------------------------' >> $logFile
logging $scriptName RESULT "\n `cat $tmpSQL1`"
echo '--------------------------------' >> $logFile

if [ -s $tmpSQL1 ];
then
  tableHTML="<table id='tableDocumentNotific'><thead><tr><th>ФИО</th><th>Должность</th><th>Подразделение</th><th>Срок действия удостоверения личности</th><th>Срок действия паспорта</th><th>Срок действия водительского удостоверения</th></tr></thead><tbody>"
  logging $scriptName STATUS "Собираем таблицу HTML"
  dateNow=`cat $tmpSQL1 | sed -n 2p | cut -f7`
  srok=`cat $tmpSQL1 | sed -n 2p | cut -f8`
  countStr=`cat $tmpSQL1 | wc -l`
  for i in `seq 2 $countStr`;
  do
    fio=`cat $tmpSQL1 | sed -n $i'p' | cut -f1`
    pos=`cat $tmpSQL1 | sed -n $i'p' | cut -f2`
    pos=$(echo $pos | sed 's/\"//g')
    dep=`cat $tmpSQL1 | sed -n $i'p' | cut -f3`
    dep=$(echo $dep | sed 's/\"//g')
    udlich=`cat $tmpSQL1 | sed -n $i'p' | cut -f4`
    pasport=`cat $tmpSQL1 | sed -n $i'p' | cut -f5`
    vodUd=`cat $tmpSQL1 | sed -n $i'p' | cut -f6`
    tableHTML+="<tr><td>$fio</td><td>$pos</td><td>$dep</td><td class='document-date-srok'>$udlich</td><td class='document-date-srok'>$pasport</td><td class='document-date-srok'>$vodUd</td></tr>"

  done
  tableHTML+="</tbody></table>"
  newData='KAKAKA"data":[{"id":"dateNow","type":"textbox","value":"'$dateNow'"},{"id":"textHTML","type":"textarea","value":"'$tableHTML'"},{"id":"srok","type":"textbox","value":"'$srok'"}]'
  newData=$(echo $newData | sed 's/KAKAKA//')
  logging $scriptName JSON "Итоговый JSON для передачи в API rest/api/asforms/data/save\n`echo $newData`"

  # создание записи реестра
  logging $scriptName STATUS "Создание записи реестра"
  wget --quiet --http-user="$synergyUser" --http-password="$synergyPass" --output-document $tmpfile "$synergyURL/rest/api/registry/create_doc?registryID=$registryid"
  logging $scriptName RESULT "`cat $tmpfile`"

  # берем uuid созданной записи
  newUUID=`cat $tmpfile | sed "s/.*\dataUUID\": \"\(.*\)\", \"asfNodeID.*/\1/"`

  # выполняем api метод POST
  logging $scriptName STATUS "Сохранение данных в новую запись"
  curl --user $synergyUser:$synergyPass --request POST "$synergyURL/rest/api/asforms/data/save" --data "formUUID=$regForm" --data "uuid=$newUUID" --data-urlencode "data=$newData" --output $tmpfile --silent
  logging $scriptName RESULT "`cat $tmpfile`"

  # активация записи реестра
  logging $scriptName STATUS "Активация записи реестра"
  wget --quiet --http-user="$synergyUser" --http-password="$synergyPass" --output-document $tmpfile "$synergyURL/rest/api/registry/activate_doc?dataUUID=$newUUID"
  logging $scriptName RESULT "`cat $tmpfile`"

else
  logging $scriptName INFO "Нет сотрудников у которых заканчивается срок действия документа"
fi
