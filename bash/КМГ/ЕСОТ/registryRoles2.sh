#!/bin/bash

# настройки
scriptName="registryRoles2"
logFile="/var/log/synergy/scripts.log"
tmpSQL1="/tmp/rrSQL1.kmg"
tmpSQL2="/tmp/rrSQL2.kmg"


# Функция получения текущего времени
function now_time() {
  date +"%Y-%m-%d %H:%M:%S"
}
# Функция логирования
function logging() {
  echo -e "`now_time` #$1# [$2] $3" >> $logFile
}

echo '' >> $logFile
logging $scriptName START "Начало работы скрипта"

echo '' >> $logFile
logging $scriptName INFO "Кадровики"

logging $scriptName STATUS "Генерация списка реестров"
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT registryID, name FROM registries
WHERE !hidden AND (LOWER(code) LIKE 'приказ%' OR LOWER(code) LIKE 'заявление%' OR LOWER(code) LIKE 'служебн%'
OR code IN ('Справка,_подтверждающая_стаж_работы_', 'Справка_о_сданных_и_несданных_имуществ', 'Справка_с_места_работы', 'Справка_подтверждающая_особый_характер_работы_или_условия_труда', 'Справка_о_заработной_плате', 'Докладная_записка_о_привлечении_работника_к_дисциплинарной_ответственности', 'Квал_требования', 'Обязательство_о_неразглашении_служебной,_коммерческой_тайны', 'Анкета_при_увольнении', 'Больничный', 'Уведомление_об_изменении_условии_труда', 'Уведомление_в_связи_с_достижением_пенсионного_возраста', 'Уведомление_в_связи_со_снижением_объема_производства', 'Уведомление_в_связи_с_ликвидацией', 'Уведомление_о_расторжении_ТД', 'Уведомление_в_связи_с_сокращением', 'Представление_о_приеме_на_работу', 'Должностная_инструкция', 'изменения_наименования_компаниия', 'Обходной_лист_при_увольнении', 'Расчетный_лист', 'Путевой_лист', 'Доп._соглашение_к_ТД_(изменение_в_ТД)', 'Доп._соглашение_к_Трудовому_договору_на_временную_основу', 'Договор_на_обучение', 'Договор_на_обучение_новый'));"  > $tmpSQL1

logging $scriptName STATUS "Генерация списка групп"
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT g.id, t.text FROM groups g
LEFT JOIN translations t ON t.translationID = g.translationID AND t.localeID='ru'
WHERE t.text IN ('Кадровики');"  > $tmpSQL2

logging $scriptName STATUS "Добавление записией в таблицу [НАЧАЛО]"
if [ -s $tmpSQL1 ];
then
  if [ -s $tmpSQL2 ];
  then
    rStr=`cat $tmpSQL1 | wc -l`
    gStr=`cat $tmpSQL2 | wc -l`
    for i in `seq 2 $rStr`;
    do
      registryID=`cat $tmpSQL1 | sed -n $i'p' | cut -f1`
      for j in `seq 2 $gStr`;
      do
        groupID=`cat $tmpSQL2 | sed -n $j'p' | cut -f1`
        logging $scriptName RESULT "registryID = [$registryID] groupID = [$groupID]"
        mysql -uroot -proot -e "use synergy; set names utf8;
        INSERT INTO registry_rights (registryID, groupID, rr_create, rr_list, rr_read, rr_edit, rr_delete, rr_modify)
        VALUES ('$registryID', '$groupID', 'N', 'Y', 'Y', 'Y', 'N', 'Y');"
      done
    done
  fi
fi
logging $scriptName STATUS "Добавление записией в таблицу [КОНЕЦ]"


echo '' >> $logFile
logging $scriptName STATUS "Удаление временных файлов"
rm -f $tmpSQL1
rm -f $tmpSQL2
logging $scriptName END "Завершение работы скрипта"
