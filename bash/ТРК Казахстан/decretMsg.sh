#!/bin/bash

# общие настройки synergy
synergyUser="1"
synergyPass="1"
synergyURL="http://127.0.0.1:8080/Synergy"

# настройки карточки подразделения
cardDepartment="1a37986a-a3c7-4541-ac42-12bf327eb53a"
cardUser="user"
cardStatus="status"
cardFinDate="finishDate"

# вспомогательные настройки
logFile="/var/log/synergy/decretManager.log"
tmpSQL1="/tmp/decretSQL1.trk"
tmpSQL2="/tmp/decretSQL2.trk"
tmpfile="/tmp/decretData.trk"
# количество дней
intDay=29

# Функция получения текущего времени
function now_time() {
  date +"%Y-%m-%d %H:%M:%S"
}
# Функция логирования
function logging() {
  echo "`now_time` [$1] $2" >> $logFile
}

echo '' >> $logFile
logging decretMSG "Работает скрипт уведомление о скором выходе с отпуска"

# вытащить все ID департаментов где есть записи в карточке подразделения
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT dfd.departmentID, depName.text departmentName, pos.positionID, posName.text positionName,
       IFNULL(upos.userid, 'NONE') userID, getfullname(upos.userid,false) userFIO, dfd.asf_data_uuid
FROM department_forms_data dfd
JOIN departments dep ON dep.departmentID=dfd.departmentid
JOIN entity_translations et ON et.translationID = dep.translationID
JOIN translations depName ON depName.translationID = et.translationID AND depName.localeID = 'ru'
LEFT JOIN positions pos ON pos.departmentID=dfd.departmentid
JOIN entity_translations et2 ON et2.translationID = pos.translationID
JOIN translations posName ON posName.translationID = et2.translationID AND posName.localeID = 'ru'
LEFT JOIN userpositions upos ON upos.positionID=pos.positionID AND upos.finishdate IS NULL
JOIN asf_data_index a ON a.uuid=dfd.asf_data_uuid
WHERE dfd.form_uuid='$cardDepartment' AND a.cmp_id LIKE CONCAT('$cardUser', '%') AND pos.pos_typeID='1'
GROUP BY dfd.departmentID;" > $tmpSQL1

# проверка если файл не пустой (т.е. если есть записи в карточке подразделения)
if [ -s $tmpSQL1 ];
then
  # Количество строк в файле tmpSQL1
  countStr=`cat $tmpSQL1 | wc -l`

  for i in `seq 2 $countStr`;
  do
    departmentName=`cat $tmpSQL1 | sed -n $i'p' | cut -f2`
    positionName=`cat $tmpSQL1 | sed -n $i'p' | cut -f4`
    tmpUserID=`cat $tmpSQL1 | sed -n $i'p' | cut -f5`
    tmpUserFIO=`cat $tmpSQL1 | sed -n $i'p' | cut -f6`
    asfDataID=`cat $tmpSQL1 | sed -n $i'p' | cut -f7`

    # Вытаскиваем всех у кого скоро окончание отпуска
    mysql -uroot -proot -e "use synergy; set names utf8;
    SELECT a.cmp_data, CONCAT(DATE(NOW() + INTERVAL 1 DAY), ' 09:00:00') AS startDate,
           b.cmp_key + INTERVAL 18 HOUR AS finishDate, DATE_FORMAT(b.cmp_key, '%d.%m.%Y')
    FROM asf_data_index a LEFT JOIN asf_data_index b ON b.uuid=a.uuid
    AND b.cmp_id=CONCAT('$cardFinDate', '-', SUBSTRING_INDEX(a.cmp_id, '-', -1))
    WHERE a.uuid='$asfDataID' AND a.cmp_id LIKE CONCAT('$cardUser', '%')
    AND DATE(b.cmp_key)=DATE(NOW() + INTERVAL '$intDay' DAY)
    ORDER BY SUBSTRING(a.cmp_id, INSTR(a.cmp_id,'-b')+2)*1;" > $tmpSQL2

    if [ -s $tmpSQL2 ];
    then
      # Количество строк в файле tmpSQL1
      countStr2=`cat $tmpSQL2 | wc -l`
      for j in `seq 2 $countStr2`; # формирование уведомления
      do
        workName=`cat $tmpSQL2 | sed -n $j'p' | cut -f1`
        workStartDate=`cat $tmpSQL2 | sed -n $j'p' | cut -f2`
        workFinishDate=`cat $tmpSQL2 | sed -n $j'p' | cut -f3`
        dateFinishDecret=`cat $tmpSQL2 | sed -n $j'p' | cut -f4`
        workUserID="1"
        workAuthorID="1"

        if [ $tmpUserID == 'NONE' ];
        then # Если должность пустая
          workComment=$departmentName
          workName="УВЕДОМЛЕНИЕ! Сотрудник "$workName" выходит с отпуска "$dateFinishDecret
          workPriority="0"
          logging decretMSG "$workName"
          logging decretMSG "$workComment"
          # отправка работы
          curl --user $synergyUser:$synergyPass --request POST "$synergyURL/rest/api/workflow/work/create" --data-urlencode "name=$workName" --data-urlencode "startDate=$workStartDate" --data-urlencode "finishDate=$workFinishDate" --data "userID=$workUserID" --data "authorID=$workAuthorID" --data "priority=$workPriority" --data-urlencode "comment=$workComment" --output $tmpfile --silent
          logging decretMSG "`cat $tmpfile`"
          echo '' >> $logFile
        else # Если должность не пустая
          workComment="Необходимо провести работы с замещающим сотрудником "$tmpUserFIO", который находится на должности: '"$positionName"', департамента: '"$departmentName"'. Иначе "$dateFinishDecret"г. сотрудник "$tmpUserFIO" автоматически будет перемещен в резерв и на его место будет назначен сотрудник "$workName
          workName="УВЕДОМЛЕНИЕ! Сотрудник "$workName" выходит с отпуска "$dateFinishDecret"г. Необходимо провести работы с замещающим сотрудником "$tmpUserFIO
          workPriority="3"
          logging decretMSG "$workName"
          logging decretMSG "$workComment"
          # отправка работы
          curl --user $synergyUser:$synergyPass --request POST "$synergyURL/rest/api/workflow/work/create" --data-urlencode "name=$workName" --data-urlencode "startDate=$workStartDate" --data-urlencode "finishDate=$workFinishDate" --data "userID=$workUserID" --data "authorID=$workAuthorID" --data "priority=$workPriority" --data-urlencode "comment=$workComment" --output $tmpfile --silent
          logging decretMSG "`cat $tmpfile`"
          echo '' >> $logFile
        fi
      done
    else
      logging decretMSG "$departmentName. Нет сотрудников у которых завершается отпуск через $intDay дней."
    fi

  done
else
  logging decretMSG "Не найдено подразделений с заполненными карточками"
fi

logging decretMSG "Завершение"
echo '' >> $logFile
