#!/bin/bash

synergyUser="1"
synergyPass="1"
synergyURL="http://127.0.0.1:8080/Synergy"

# карточка подразделения
cardDepartment="1a37986a-a3c7-4541-ac42-12bf327eb53a"
cardUser="user"
cardStatus="status"
cardFinDate="finishDate"

logFile="/var/log/synergy/decretManager.log"
tmpSQL1="/tmp/decretSQL1.trk"
tmpSQL2="/tmp/decretSQL2.trk"
tmpSQL3="/tmp/decretSQL3.trk"
tmpfile="/tmp/decretData.trk"

# json с пустой таблицей
dataEmptyTable='"data":[{"id":"cmp-w4ypt6","type":"label","label":"Декретный отпуск","value":" "},{"id":"dekretTable","type":"appendable_table","key":"","data":[{"id":"cmp-48f5rb","type":"label","label":"ФИО","value":" "},{"id":"cmp-p2hpo7","type":"label","label":"Статус","value":" "},{"id":"cmp-yds4gu","type":"label","label":"Дата выхода","value":" "}]}]'

# Функция получения текущего времени
function now_time() {
    date +"%Y-%m-%d %H:%M:%S"
}

# Функция логирования
function logging() {
    echo "`now_time` [$1] $2" >> $logFile
}

echo '' >> $logFile
echo '' >> $logFile
logging START "Начало работы скрипта"
echo '#############################################' >> $logFile
echo '##### НАСТРОЙКИ #############################' >> $logFile
echo "login: "$synergyUser >> $logFile
echo "Password: "$synergyPass >> $logFile
echo "URL: "$synergyURL >> $logFile
echo "formID карточки подразделения: "$cardDepartment >> $logFile
echo '##### НАСТРОЙКИ #############################' >> $logFile
echo '#############################################' >> $logFile
echo '' >> $logFile

# проверка сроков даты выхода
./decretMsg.sh

# вытащить все ID департаментов где есть записи в карточке подразделения
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT dfd.departmentID, pos.positionID, IFNULL(upos.userid, 'NONE'), dfd.asf_data_uuid
FROM department_forms_data dfd
LEFT JOIN positions pos ON pos.departmentID=dfd.departmentid
LEFT JOIN userpositions upos ON upos.positionID=pos.positionID AND upos.finishdate IS NULL
JOIN asf_data_index a ON a.uuid=dfd.asf_data_uuid
WHERE dfd.form_uuid='$cardDepartment' AND a.cmp_id LIKE CONCAT('$cardUser', '%') AND pos.pos_typeID='1'
GROUP BY dfd.departmentID;" > $tmpSQL1

# проверка если файл не пустой (т.е. если есть записи в карточке подразделения)
if [ -s $tmpSQL1 ];
then

# Количество строк в файле tmpSQL1
countStr=`cat $tmpSQL1 | wc -l`
logging INFO "Подразделений с заполненными карточками: $[$countStr-1]"
echo '' >> $logFile

for i in `seq 2 $countStr`;
do
departmentID=`cat $tmpSQL1 | sed -n $i'p' | cut -f1`
positionID=`cat $tmpSQL1 | sed -n $i'p' | cut -f2`
tmpUserID=`cat $tmpSQL1 | sed -n $i'p' | cut -f3`
asfDataID=`cat $tmpSQL1 | sed -n $i'p' | cut -f4`

logging STATUS "Обработка подразделения $(($i-1))"
logging INFO "departmentID: $departmentID"
logging INFO "positionID: $positionID"
logging INFO "tmpUserID: $tmpUserID"
logging INFO "asfDataID: $asfDataID"

# вытащить первого вышедшего в д\о
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT cmp_key FROM asf_data_index
WHERE uuid='$asfDataID' AND cmp_id LIKE CONCAT('$cardUser', '%')
ORDER BY SUBSTRING(cmp_id, INSTR(cmp_id,'-b')+2)*1 LIMIT 1;" > $tmpSQL2

oneUserID=`cat $tmpSQL2 | sed -n 2p | cut -f1`

logging INFO "ID первого пользователя который вышел в д\о: $oneUserID"

# вытащить все userid у кого дата выхода завтра
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT a.cmp_key FROM asf_data_index a
LEFT JOIN asf_data_index b ON b.uuid=a.uuid AND b.cmp_id=CONCAT('$cardStatus', '-', SUBSTRING_INDEX(a.cmp_id, '-', -1))
LEFT JOIN asf_data_index c ON c.uuid=a.uuid AND c.cmp_id=CONCAT('$cardFinDate', '-', SUBSTRING_INDEX(a.cmp_id, '-', -1))
WHERE a.uuid='$asfDataID' AND a.cmp_id LIKE CONCAT('$cardUser', '%')
AND DATE(c.cmp_key)=DATE(NOW()+INTERVAL 1 DAY)
ORDER BY SUBSTRING(a.cmp_id, INSTR(a.cmp_id,'-b')+2)*1;" > $tmpSQL2


# проверка если файл не пустой (т.е. если есть пользователи у кого дата выхода завтра)
if [ -s $tmpSQL2 ];
then

# Количество строк в файле tmpSQL1
countStr2=`cat $tmpSQL2 | wc -l`
logging INFO "Количество пользователей у которых дата выхода завтра: $(($countStr2-1))"

for j in `seq 2 $countStr2`;
do
userid=`cat $tmpSQL2 | sed -n $j'p' | cut -f1`
echo '' >> $logFile
logging STATUS "Обработка пользователя userid: $userid"

if [ $oneUserID == $userid ]; # проверяем первый ли это юзер или нет
then
# -----------если это первый юзер, пустой json----------------------------------
  if [ $tmpUserID == 'NONE' ];
  then
    logging INFO "назначить на должность [positionID: $positionID] пользователя [userid: $userid]"
    # назначаем на должность
    wget --quiet --http-user="$synergyUser" --http-password="$synergyPass" --output-document $tmpfile "$synergyURL/rest/api/positions/appoint?positionID=$positionID&userID=$userid"
    logging STATUS "`cat $tmpfile`"
    # чистим картоку подразделения (json с пустой таблицей)
    logging JSON "`echo $dataEmptyTable`"
    curl --user $synergyUser:$synergyPass --request POST "$synergyURL/rest/api/asforms/data/save" --data "formUUID=$cardDepartment" --data "uuid=$asfDataID" --data-urlencode "data=$dataEmptyTable" --output $tmpfile --silent
    logging STATUS "`cat $tmpfile`"
    break
  else
    logging INFO "снять с должности [positionID: $positionID] пользователя [userid: $tmpUserID]"
    # снятие с должности
    wget --quiet --http-user="$synergyUser" --http-password="$synergyPass" --output-document $tmpfile "$synergyURL/rest/api/positions/discharge?positionID=$positionID&userID=$tmpUserID"
    logging STATUS "`cat $tmpfile`"
    logging INFO "назначить на должность [positionID: $positionID] пользователя [userid: $userid]"
    # назначаем на должность
    wget --quiet --http-user="$synergyUser" --http-password="$synergyPass" --output-document $tmpfile "$synergyURL/rest/api/positions/appoint?positionID=$positionID&userID=$userid"
    logging STATUS "`cat $tmpfile`"
    # чистим картоку подразделения (json с пустой таблицей)
    logging JSON "`echo $dataEmptyTable`"
    curl --user $synergyUser:$synergyPass --request POST "$synergyURL/rest/api/asforms/data/save" --data "formUUID=$cardDepartment" --data "uuid=$asfDataID" --data-urlencode "data=$dataEmptyTable" --output $tmpfile --silent
    logging STATUS "`cat $tmpfile`"
    break
  fi
# ------------------------------------------------------------------------------
else
# ----------если не первый юзер, выпилить строку в таблице----------------------

# формируем json (начало)
newData='KAKAKA"data":[{"id":"cmp-w4ypt6","type":"label","label":"Декретный отпуск","value":" "},{"id":"dekretTable","type":"appendable_table","key":"","data":[{"id":"cmp-48f5rb","type":"label","label":"ФИО","value":" "},{"id":"cmp-p2hpo7","type":"label","label":"Статус","value":" "},{"id":"cmp-yds4gu","type":"label","label":"Дата выхода","value":" "}'

tableData=""

# вытащить всю таблицу кроме $userid для формирования новой таблицы
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT a.cmp_data AS fio, a.cmp_key AS userid,
       b.cmp_data AS statusValue, b.cmp_key AS statusKey,
       c.cmp_data AS dateValue, c.cmp_key AS dateKey
FROM asf_data_index a
LEFT JOIN asf_data_index b ON b.uuid=a.uuid AND b.cmp_id=CONCAT('$cardStatus', '-', SUBSTRING_INDEX(a.cmp_id, '-', -1))
LEFT JOIN asf_data_index c ON c.uuid=a.uuid AND c.cmp_id=CONCAT('$cardFinDate', '-', SUBSTRING_INDEX(a.cmp_id, '-', -1))
WHERE a.uuid='$asfDataID' AND a.cmp_id LIKE CONCAT('$cardUser', '%')
AND a.cmp_key!='$userid'
ORDER BY SUBSTRING(a.cmp_id, INSTR(a.cmp_id,'-b')+2)*1;" > $tmpSQL3

# проверка если файл не пустой (т.е. если есть пользователи у кого дата выхода завтра)
if [ -s $tmpSQL3 ];
then

# Количество строк в файле tmpSQL3
countStr3=`cat $tmpSQL3 | wc -l`

for k in `seq 2 $countStr3`;
do
NJfio=`cat $tmpSQL3 | sed -n $k'p' | cut -f1`
NJuserid=`cat $tmpSQL3 | sed -n $k'p' | cut -f2`
NJstatusValue=`cat $tmpSQL3 | sed -n $k'p' | cut -f3`
NJstatusKey=`cat $tmpSQL3 | sed -n $k'p' | cut -f4`
NJdateValue=`cat $tmpSQL3 | sed -n $k'p' | cut -f5`
NJdateKey=`cat $tmpSQL3 | sed -n $k'p' | cut -f6`
# формируем json (таблица)
tableData+=',{"id":"'$cardUser'-b'$[$k-1]'","type":"entity","value":"'$NJfio'","key":"'$NJuserid'","formatVersion":"V1"},{"id":"'$cardStatus'-b'$[$k-1]'","type":"listbox","value":"'$NJstatusValue'","key":"'$NJstatusKey'"},{"id":"'$cardFinDate'-b'$[$k-1]'","type":"date","value":"'$NJdateValue'","key":"'$NJdateKey'"}'
done

fi

# формируем json (итоговая строка для передачи в апи)
newData=$newData$tableData']}]'
newData=$(echo $newData | sed 's/KAKAKA//')

  if [ $tmpUserID == 'NONE' ];
  then
    logging INFO "назначить на должность [positionID: $positionID] пользователя [userid: $userid]"
    # назначаем на должность
    wget --quiet --http-user="$synergyUser" --http-password="$synergyPass" --output-document $tmpfile "$synergyURL/rest/api/positions/appoint?positionID=$positionID&userID=$userid"
    logging STATUS "`cat $tmpfile`"
    # выпиливаем строку из таблицы
    logging JSON "`echo $newData`"
    curl --user $synergyUser:$synergyPass --request POST "$synergyURL/rest/api/asforms/data/save" --data "formUUID=$cardDepartment" --data "uuid=$asfDataID" --data-urlencode "data=$newData" --output $tmpfile --silent
    logging STATUS "`cat $tmpfile`"
    break
  else
    logging INFO "снять с должности [positionID: $positionID] пользователя [userid: $tmpUserID]"
    # снятие с должности
    wget --quiet --http-user="$synergyUser" --http-password="$synergyPass" --output-document $tmpfile "$synergyURL/rest/api/positions/discharge?positionID=$positionID&userID=$tmpUserID"
    logging STATUS "`cat $tmpfile`"
    logging INFO "назначить на должность [positionID: $positionID] пользователя [userid: $userid]"
    # назначаем на должность
    wget --quiet --http-user="$synergyUser" --http-password="$synergyPass" --output-document $tmpfile "$synergyURL/rest/api/positions/appoint?positionID=$positionID&userID=$userid"
    logging STATUS "`cat $tmpfile`"
    # выпиливаем строку из таблицы
    logging JSON "`echo $newData`"
    curl --user $synergyUser:$synergyPass --request POST "$synergyURL/rest/api/asforms/data/save" --data "formUUID=$cardDepartment" --data "uuid=$asfDataID" --data-urlencode "data=$newData" --output $tmpfile --silent
    logging STATUS "`cat $tmpfile`"
    break
  fi
# ------------------------------------------------------------------------------
fi

done
else #
  logging INFO "$[$i-1] Нет пользователей у которых дата выхода завтра"
fi

echo '' >> $logFile
done
else
  logging INFO "Не найдено подразделений с заполненными карточками"
fi

# удаление временных файлов
rm -f $tmpSQL1
rm -f $tmpSQL2
rm -f $tmpSQL3
rm -f $tmpfile

logging END "Завершение работы скрипта"
