#!/bin/bash

#настройки синержи
synergyHost="http://127.0.0.1:8080/Synergy"
synergyLogin="1"
synergyPassword="1"

#настройки mysql
mysqlUser="root"
mysqlPass="root"
mysqlHost="127.0.0.1"

# вспомогательные настройки
logFile="/var/log/synergy/scripts.log"
scriptName=${0##*/}
tmpfile=$(mktemp)
tmpsql=$(mktemp)
tmpsql2=$(mktemp)

registryCode="itsm_registry_incidents"
intervalMinute="50" #интервал поиска записей, отнимается от текущего времени, в минутах

function now_time () {
  date +"%Y-%m-%d %H:%M:%S"
}
function logging () {
  echo -e "`now_time` #$scriptName# [$1] $2" >> $logFile
}

# $1 mysql query
# $2 output result to tmp file
function executeQuery () {
  mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8; $1;" > $2 2>/dev/null
}

#получение списка процессов документа
# $1 documentID
function getProcesses () {
  curl --user $synergyLogin:$synergyPassword --request GET "$synergyHost/rest/api/workflow/get_execution_process?documentID=$1" --output $tmpfile --silent 2>/dev/null
}

#Запускает событие "Активации" записи реестра (документа)
# $1 dataUUID
function activateDoc () {
  curl --user $synergyLogin:$synergyPassword --request GET "$synergyHost/rest/api/registry/activate_doc?dataUUID=$1" --output $tmpfile --silent 2>/dev/null
}

echo '' >> $logFile
logging START "Начало работы скрипта"
logging INFO "Получение записей реестра за последнии $intervalMinute минут"

executeQuery "SELECT rg.documentID, a.cmp_key regDate, rg.asfDataID, rg.created, rg.activated, rg.active
FROM (SELECT asfDataID, documentID, created, activated, active FROM registry_documents
WHERE deleted IS NULL AND created BETWEEN NOW() - INTERVAL $intervalMinute MINUTE AND NOW()
AND registryID = (SELECT registryID FROM registries WHERE code = '$registryCode')) rg
LEFT JOIN asf_data_index a ON a.uuid = rg.asfDataID AND a.cmp_id = 'itsm_form_incident_regdate'
WHERE a.cmp_key BETWEEN NOW() - INTERVAL $intervalMinute MINUTE AND NOW()" $tmpsql

if [ -s $tmpsql ];
then
  countStr=`cat $tmpsql | wc -l`

  for i in `seq 2 $countStr`;
  do
    documentID=`cat $tmpsql | sed -n $i'p' | cut -f1`
    regDate=`cat $tmpsql | sed -n $i'p' | cut -f2`
    asfDataID=`cat $tmpsql | sed -n $i'p' | cut -f3`

    echo '' >> $logFile
    logging INFO "asfDataID: $asfDataID :::: documentID: $documentID :::: regDate: $regDate"

    #получаем список процессов
    executeQuery "SELECT a.actionID
    FROM registry_processes rp
    LEFT JOIN processes p ON p.topProcInstID = rp.procInstID AND p.object_type = 4
    LEFT JOIN actions a ON a.topActionID = p.objectID
    WHERE rp.asfDataID = '$asfDataID' LIMIT 1" $tmpsql2

    if [ -s $tmpsql2 ];
    then
      logging INFO "запись уже активирована"
    else
      # изменяем в базе признак активации, на всякий случай
      executeQuery "UPDATE registry_documents SET active = 'N', activated = NULL WHERE documentID = '$documentID'" $tmpsql2

      activateDoc $asfDataID
      logging RESULT "запуск маршрута активации\n`cat $tmpfile`"
    fi

  done
else
  logging INFO "нет новых данных"
fi


echo '' >> $logFile
logging STATUS "Удаление временных файлов"
rm -rf $tmpfile
rm -rf $tmpsql
rm -rf $tmpsql2
logging END "Завершение работы скрипта"
exit 0
