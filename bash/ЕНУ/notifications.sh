#!/bin/bash
scriptName="notifications"
synergyUser="kamyrov"
synergyPass="123456"
synergyURL="http://127.0.0.1:8080/Synergy"

# ID форм карточек пользователей
cardBirthday='1b548b18-659a-49a3-a77e-d8af505fe277' # 01. Личные данные
cardOtpusk='0248959d-050d-4abc-8fe9-e59117babf45' # 05. Отпуск
cardAttest='443307d0-ca0c-4282-a416-012c61e00b03' # 11. Аттестация

##### Настройки отправки уведомлений #####
otpuskDay=7 # уведомлять об окончании отпуска за N дней
attestDay=7 # уведомлять о дате следующей аттестации за N дней

##### НАСТРОЙКИ РЕЕСТРОВ #####
# Уведомления (дни рождения)
regD='3b72fed0-7b7d-4917-bf6d-e5d9cdc52118'
formD='9a1e07c5-deb9-42c6-96f7-25db772ba764'
# Уведомления (отпуска)
regO='3ca3a5fc-e9ae-49b2-856e-eab26729d1cd'
formO='40284fbd-dcd7-4c25-97f4-092dffff63fb'
# Уведомления (аттестация)
regA='99ce9bd3-48fa-4db5-ba96-1b2929706c88'
formA='5d9687d3-daee-4805-afc2-84da34a862a1'
##############################

# вспомогательные настройки
logFile="/var/log/synergy/scripts.log"
tmpSQL1="/tmp/tmpSQL1.enu"
tmpSQL2="/tmp/tmpSQL2.enu"
tmpfile="/tmp/tmpfile.enu"

# Функция получения текущего времени
function now_time() {
  date +"%Y-%m-%d %H:%M:%S"
}
# Функция логирования
function logging() {
  echo -e "`now_time` #$1# [$2] $3" >> $logFile
}

echo '' >> $logFile
logging $scriptName START "Начало работы скрипта"

echo '########################################' >> $logFile
echo '############# ДНИ РОЖДЕНИЯ #############' >> $logFile
echo '########################################' >> $logFile
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT parDep.departmentid,
(SELECT text FROM translations WHERE translationID=IF(dep.departmentid='1', dep.translationID, parDep.translationID) AND localeID='ru') parDepName,
DATE_FORMAT(NOW() + INTERVAL 1 DAY, 'День рождения %d.%m.%Y') AS birthday
FROM users u
JOIN userpositions upos ON upos.userID=u.userID
JOIN positions pos ON pos.positionID=upos.positionID
JOIN departments dep ON dep.departmentID=pos.departmentID
JOIN departments parDep ON parDep.departmentID=dep.parentDepartmentID
LEFT JOIN personal_record_forms_user prfu ON prfu.userid=u.userID
LEFT JOIN asf_data_index dr ON dr.uuid=prfu.asf_data_uuid AND dr.cmp_id='dataroj'
WHERE prfu.form_uuid='$cardBirthday'
AND u.finished IS NULL AND upos.finishdate IS NULL
AND pos.pos_typeid IN (1,2,256) AND pos.deleted IS NULL
AND DATE_FORMAT(dr.cmp_key, '%m%d')=DATE_FORMAT(NOW() + INTERVAL 1 DAY, '%m%d')
GROUP BY parDep.departmentid;" > $tmpSQL1

if [ -s $tmpSQL1 ];
then
  tableHTML=""
  countStr=`cat $tmpSQL1 | wc -l`
  for i in `seq 2 $countStr`;
  do
    parDepID=`cat $tmpSQL1 | sed -n $i'p' | cut -f1`
    parDepName=`cat $tmpSQL1 | sed -n $i'p' | cut -f2`
    birthday=`cat $tmpSQL1 | sed -n $i'p' | cut -f3`

    echo '' >> $logFile
    logging $scriptName INFO "Департамент: $parDepName, departmentid: [$parDepID]"

    logging $scriptName STATUS "Поиск сотрудников у которых день рождения завтра"
    mysql -uroot -proot -e "use synergy; set names utf8;
    SELECT CONCAT_WS(' ', u.lastname, u.firstname, u.patronymic) fio,
    (SELECT text FROM translations WHERE translationID=IFNULL(a.translationID, pos.translationID) AND localeID='ru') posName,
    (SELECT text FROM translations WHERE translationID=dep.translationID AND localeID='ru') department,
    DATE_FORMAT(dr.cmp_key, '%d.%m.%Y') birthday, YEAR(NOW())-YEAR(dr.cmp_key) vozrast
    FROM users u
    JOIN userpositions upos ON upos.userID=u.userID
    JOIN positions pos ON pos.positionID=upos.positionID
    JOIN departments dep ON dep.departmentID=pos.departmentID
    LEFT JOIN assistant_positions ap ON ap.positionID=pos.positionID
    LEFT JOIN assistants a ON a.assistantID=ap.assistantID
    LEFT JOIN personal_record_forms_user prfu ON prfu.userid=u.userID
    LEFT JOIN asf_data_index dr ON dr.uuid=prfu.asf_data_uuid AND dr.cmp_id='dataroj'
    WHERE prfu.form_uuid='$cardBirthday'
    AND u.finished IS NULL AND upos.finishdate IS NULL
    AND pos.pos_typeid IN (1,2,256) AND pos.deleted IS NULL

    AND (dep.parentdepartmentid IN (SELECT departmentid FROM departments WHERE parentdepartmentid='$parDepID')
    OR dep.parentdepartmentid='$parDepID')

    AND DATE_FORMAT(dr.cmp_key, '%m%d')=DATE_FORMAT(NOW() + INTERVAL 1 DAY, '%m%d')
    GROUP BY u.userid ORDER BY fio;" > $tmpSQL2
    logging $scriptName RESULT "\n `cat $tmpSQL2`"

    # формируем html
    tableHTML+="<h3>"$parDepName"</h3><table id='tableNotific'><thead><tr><th>ФИО</th><th>Должность</th><th>Подразделение</th><th>Дата рождения</th><th>Возраст</th></tr></thead><tbody>"
    logging $scriptName STATUS "Собираем таблицу HTML"
    countStr2=`cat $tmpSQL2 | wc -l`
    for j in `seq 2 $countStr2`;
    do
      fio=`cat $tmpSQL2 | sed -n $j'p' | cut -f1`
      pos=`cat $tmpSQL2 | sed -n $j'p' | cut -f2`
      dep=`cat $tmpSQL2 | sed -n $j'p' | cut -f3`
      dr=`cat $tmpSQL2 | sed -n $j'p' | cut -f4`
      voz=`cat $tmpSQL2 | sed -n $j'p' | cut -f5`
      tableHTML+="<tr><td>$fio</td><td>$pos</td><td>$dep</td><td align='center'>$dr</td><td align='center'>$voz</td></tr>"
    done
    tableHTML+="</tbody></table><br>"
  done

  # формируем JSON
  newData='KAKAKA"data":[{"id":"birthday","type":"textbox","value":"'$birthday'"},{"id":"textHTML","type":"textarea","value":"'$tableHTML'"}]'
  newData=$(echo $newData | sed 's/KAKAKA//')
  logging $scriptName JSON "Итоговый JSON для передачи в API rest/api/asforms/data/save\n`echo $newData`"

  # создание записи реестра
  logging $scriptName STATUS "Создание записи реестра"
  wget --quiet --http-user="$synergyUser" --http-password="$synergyPass" --output-document $tmpfile "$synergyURL/rest/api/registry/create_doc?registryID=$regD"
  logging $scriptName RESULT "`cat $tmpfile`"

  # берем uuid созданной записи
  newUUID=`cat $tmpfile | sed "s/.*\dataUUID\": \"\(.*\)\", \"asfNodeID.*/\1/"`

  # выполняем api метод POST
  logging $scriptName STATUS "Сохранение данных в новую запись"
  curl --user $synergyUser:$synergyPass --request POST "$synergyURL/rest/api/asforms/data/save" --data "formUUID=$formD" --data "uuid=$newUUID" --data-urlencode "data=$newData" --output $tmpfile --silent
  logging $scriptName RESULT "`cat $tmpfile`"

  # активация записи реестра
  logging $scriptName STATUS "Активация записи реестра"
  wget --quiet --http-user="$synergyUser" --http-password="$synergyPass" --output-document $tmpfile "$synergyURL/rest/api/registry/activate_doc?dataUUID=$newUUID"
  logging $scriptName RESULT "`cat $tmpfile`"
else
  logging $scriptName INFO "Нет сотрудников у которых день рождения сегодня"
fi


echo '' >> $logFile
echo '########################################' >> $logFile
echo '############### ОТПУСКА ################' >> $logFile
echo '########################################' >> $logFile
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT parDep.departmentid,
(SELECT text FROM translations WHERE translationID=IF(dep.departmentid='1', dep.translationID, parDep.translationID) AND localeID='ru') parDepName
FROM users u
JOIN userpositions upos ON upos.userID=u.userID
JOIN positions pos ON pos.positionID=upos.positionID
JOIN departments dep ON dep.departmentID=pos.departmentID
JOIN departments parDep ON parDep.departmentID=dep.parentDepartmentID
LEFT JOIN personal_record_forms_user prfu ON prfu.userid=u.userID
LEFT JOIN asf_data_index dayX ON dayX.uuid=prfu.asf_data_uuid AND dayX.cmp_id LIKE 'demKunineDeyin%'
WHERE prfu.form_uuid='$cardOtpusk'
AND u.finished IS NULL AND upos.finishdate IS NULL
AND pos.pos_typeid IN (1,2,256) AND pos.deleted IS NULL
AND DATE(dayX.cmp_key - INTERVAL 1 DAY)=DATE(NOW() + INTERVAL 7 DAY)
GROUP BY parDep.departmentid;" > $tmpSQL1

if [ -s $tmpSQL1 ];
then
  countStr=`cat $tmpSQL1 | wc -l`
  for i in `seq 2 $countStr`;
  do
    parDepID=`cat $tmpSQL1 | sed -n $i'p' | cut -f1`
    parDepName=`cat $tmpSQL1 | sed -n $i'p' | cut -f2`

    echo '' >> $logFile
    logging $scriptName INFO "Департамент: $parDepName, departmentid: [$parDepID]"

    logging $scriptName STATUS "Поиск сотрудников у которых заканчивается отпуск через $otpuskDay дней"
    mysql -uroot -proot -e "use synergy; set names utf8;
    SELECT CONCAT_WS(' ', u.lastname, u.firstname, u.patronymic) fio,
    (SELECT text FROM translations WHERE translationID=IFNULL(a.translationID, pos.translationID) AND localeID='ru') posName,
    (SELECT text FROM translations WHERE translationID=dep.translationID AND localeID='ru') department,
    tt.cmp_data typeOtpusk, DATE_FORMAT(dayX.cmp_key, '%d.%m.%Y') dateEnd
    FROM users u
    JOIN userpositions upos ON upos.userID=u.userID
    JOIN positions pos ON pos.positionID=upos.positionID
    JOIN departments dep ON dep.departmentID=pos.departmentID
    LEFT JOIN assistant_positions ap ON ap.positionID=pos.positionID
    LEFT JOIN assistants a ON a.assistantID=ap.assistantID
    LEFT JOIN personal_record_forms_user prfu ON prfu.userid=u.userID
    LEFT JOIN asf_data_index dayX ON dayX.uuid=prfu.asf_data_uuid AND dayX.cmp_id LIKE 'demKunineDeyin%'
    LEFT JOIN asf_data_index tt ON tt.uuid=prfu.asf_data_uuid AND tt.cmp_id = CONCAT('DemalysTuri-', SUBSTRING_INDEX(dayX.cmp_id, '-', -1))
    WHERE prfu.form_uuid='$cardOtpusk'
    AND u.finished IS NULL AND upos.finishdate IS NULL
    AND pos.pos_typeid IN (1,2,256) AND pos.deleted IS NULL
    AND (dep.parentdepartmentid IN (SELECT departmentid FROM departments WHERE parentdepartmentid='$parDepID')
    OR dep.parentdepartmentid='$parDepID')
    AND DATE(dayX.cmp_key - INTERVAL 1 DAY)=DATE(NOW() + INTERVAL '$otpuskDay' DAY)
    GROUP BY u.userid ORDER BY fio;" > $tmpSQL2
    logging $scriptName RESULT "\n `cat $tmpSQL2`"

    # формируем html
    tableHTML="<table id='tableNotific'><thead><tr><th>ФИО</th><th>Должность</th><th>Подразделение</th><th>Тип отпуска</th><th>Дата выхода</th></tr></thead><tbody>"
    logging $scriptName STATUS "Собираем таблицу HTML"
    countStr2=`cat $tmpSQL2 | wc -l`
    for j in `seq 2 $countStr2`;
    do
      fio=`cat $tmpSQL2 | sed -n $j'p' | cut -f1`
      pos=`cat $tmpSQL2 | sed -n $j'p' | cut -f2`
      dep=`cat $tmpSQL2 | sed -n $j'p' | cut -f3`
      to=`cat $tmpSQL2 | sed -n $j'p' | cut -f4`
      dv=`cat $tmpSQL2 | sed -n $j'p' | cut -f5`
      tableHTML+="<tr><td>$fio</td><td>$pos</td><td>$dep</td><td>$to</td><td align='center'>$dv</td></tr>"
    done
    tableHTML+="</tbody></table>"

    # формируем JSON
    newData='KAKAKA"data":[{"id":"fakultet","type":"entity","value":"'$parDepName'","key":"'$parDepID'"},{"id":"textHTML","type":"textarea","value":"'$tableHTML'"}]'
    newData=$(echo $newData | sed 's/KAKAKA//')
    logging $scriptName JSON "Итоговый JSON для передачи в API rest/api/asforms/data/save\n`echo $newData`"

    # создание записи реестра
    logging $scriptName STATUS "Создание записи реестра"
    wget --quiet --http-user="$synergyUser" --http-password="$synergyPass" --output-document $tmpfile "$synergyURL/rest/api/registry/create_doc?registryID=$regO"
    logging $scriptName RESULT "`cat $tmpfile`"

    # берем uuid созданной записи
    newUUID=`cat $tmpfile | sed "s/.*\dataUUID\": \"\(.*\)\", \"asfNodeID.*/\1/"`

    # выполняем api метод POST
    logging $scriptName STATUS "Сохранение данных в новую запись"
    curl --user $synergyUser:$synergyPass --request POST "$synergyURL/rest/api/asforms/data/save" --data "formUUID=$formO" --data "uuid=$newUUID" --data-urlencode "data=$newData" --output $tmpfile --silent
    logging $scriptName RESULT "`cat $tmpfile`"

    # активация записи реестра
    logging $scriptName STATUS "Активация записи реестра"
    wget --quiet --http-user="$synergyUser" --http-password="$synergyPass" --output-document $tmpfile "$synergyURL/rest/api/registry/activate_doc?dataUUID=$newUUID"
    logging $scriptName RESULT "`cat $tmpfile`"
  done
else
  logging $scriptName INFO "Нет сотрудников у которых заканчивается отпуск через $otpuskDay дней"
fi


echo '' >> $logFile
echo '########################################' >> $logFile
echo '############## АТТЕСТАЦИЯ ##############' >> $logFile
echo '########################################' >> $logFile
mysql -uroot -proot -e "use synergy; set names utf8;
SELECT parDep.departmentid,
(SELECT text FROM translations WHERE translationID=IF(dep.departmentid='1', dep.translationID, parDep.translationID) AND localeID='ru') parDepName
FROM users u
JOIN userpositions upos ON upos.userID=u.userID
JOIN positions pos ON pos.positionID=upos.positionID
JOIN departments dep ON dep.departmentID=pos.departmentID
JOIN departments parDep ON parDep.departmentID=dep.parentDepartmentID
LEFT JOIN personal_record_forms_user prfu ON prfu.userid=u.userID
LEFT JOIN asf_data_index dayX ON dayX.uuid=prfu.asf_data_uuid AND dayX.cmp_id LIKE 'datesled%'
WHERE prfu.form_uuid='$cardAttest'
AND u.finished IS NULL AND upos.finishdate IS NULL
AND pos.pos_typeid IN (1,2,256) AND pos.deleted IS NULL
AND DATE(dayX.cmp_key - INTERVAL 1 DAY)=DATE(NOW() + INTERVAL 7 DAY)
GROUP BY parDep.departmentid;" > $tmpSQL1

if [ -s $tmpSQL1 ];
then
  countStr=`cat $tmpSQL1 | wc -l`
  for i in `seq 2 $countStr`;
  do
    parDepID=`cat $tmpSQL1 | sed -n $i'p' | cut -f1`
    parDepName=`cat $tmpSQL1 | sed -n $i'p' | cut -f2`

    echo '' >> $logFile
    logging $scriptName INFO "Департамент: $parDepName, departmentid: [$parDepID]"

    logging $scriptName STATUS "Поиск сотрудников у которых дата следующей аттестации наступает через $otpuskDay дней"
    mysql -uroot -proot -e "use synergy; set names utf8;
    SELECT CONCAT_WS(' ', u.lastname, u.firstname, u.patronymic) fio,
    (SELECT text FROM translations WHERE translationID=IFNULL(a.translationID, pos.translationID) AND localeID='ru') posName,
    (SELECT text FROM translations WHERE translationID=dep.translationID AND localeID='ru') department,
    DATE_FORMAT(dayX.cmp_key, '%d.%m.%Y') dateAttest
    FROM users u
    JOIN userpositions upos ON upos.userID=u.userID
    JOIN positions pos ON pos.positionID=upos.positionID
    JOIN departments dep ON dep.departmentID=pos.departmentID
    LEFT JOIN assistant_positions ap ON ap.positionID=pos.positionID
    LEFT JOIN assistants a ON a.assistantID=ap.assistantID
    LEFT JOIN personal_record_forms_user prfu ON prfu.userid=u.userID
    LEFT JOIN asf_data_index dayX ON dayX.uuid=prfu.asf_data_uuid AND dayX.cmp_id LIKE 'datesled%'
    WHERE prfu.form_uuid='$cardAttest'
    AND u.finished IS NULL AND upos.finishdate IS NULL
    AND pos.pos_typeid IN (1,2,256) AND pos.deleted IS NULL
    AND (dep.parentdepartmentid IN (SELECT departmentid FROM departments WHERE parentdepartmentid='$parDepID')
    OR dep.parentdepartmentid='$parDepID')
    AND DATE(dayX.cmp_key - INTERVAL 1 DAY)=DATE(NOW() + INTERVAL '$attestDay' DAY)
    GROUP BY u.userid ORDER BY fio;" > $tmpSQL2
    logging $scriptName RESULT "\n `cat $tmpSQL2`"

    # формируем html
    tableHTML="<table id='tableNotific'><thead><tr><th>ФИО</th><th>Должность</th><th>Подразделение</th><th>Дата следующей аттестации</th></tr></thead><tbody>"
    logging $scriptName STATUS "Собираем таблицу HTML"
    countStr2=`cat $tmpSQL2 | wc -l`
    for j in `seq 2 $countStr2`;
    do
      fio=`cat $tmpSQL2 | sed -n $j'p' | cut -f1`
      pos=`cat $tmpSQL2 | sed -n $j'p' | cut -f2`
      dep=`cat $tmpSQL2 | sed -n $j'p' | cut -f3`
      datt=`cat $tmpSQL2 | sed -n $j'p' | cut -f4`
      tableHTML+="<tr><td>$fio</td><td>$pos</td><td>$dep</td><td align='center'>$datt</td></tr>"
    done
    tableHTML+="</tbody></table>"

    # формируем JSON
    newData='KAKAKA"data":[{"id":"fakultet","type":"entity","value":"'$parDepName'","key":"'$parDepID'"},{"id":"textHTML","type":"textarea","value":"'$tableHTML'"}]'
    newData=$(echo $newData | sed 's/KAKAKA//')
    logging $scriptName JSON "Итоговый JSON для передачи в API rest/api/asforms/data/save\n`echo $newData`"

    # создание записи реестра
    logging $scriptName STATUS "Создание записи реестра"
    wget --quiet --http-user="$synergyUser" --http-password="$synergyPass" --output-document $tmpfile "$synergyURL/rest/api/registry/create_doc?registryID=$regA"
    logging $scriptName RESULT "`cat $tmpfile`"

    # берем uuid созданной записи
    newUUID=`cat $tmpfile | sed "s/.*\dataUUID\": \"\(.*\)\", \"asfNodeID.*/\1/"`

    # выполняем api метод POST
    logging $scriptName STATUS "Сохранение данных в новую запись"
    curl --user $synergyUser:$synergyPass --request POST "$synergyURL/rest/api/asforms/data/save" --data "formUUID=$formA" --data "uuid=$newUUID" --data-urlencode "data=$newData" --output $tmpfile --silent
    logging $scriptName RESULT "`cat $tmpfile`"

    # активация записи реестра
    logging $scriptName STATUS "Активация записи реестра"
    wget --quiet --http-user="$synergyUser" --http-password="$synergyPass" --output-document $tmpfile "$synergyURL/rest/api/registry/activate_doc?dataUUID=$newUUID"
    logging $scriptName RESULT "`cat $tmpfile`"
  done
else
  logging $scriptName INFO "Нет сотрудников у которых дата следующей аттестации через $otpuskDay дней"
fi

echo '' >> $logFile
logging $scriptName STATUS "Удаление временных файлов"
# удаление временных файлов
rm -f $tmpSQL1
rm -f $tmpSQL2
rm -f $tmpfile

logging $scriptName END "Завершение работы скрипта"
