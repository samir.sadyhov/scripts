$.getScript("https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.8.0/jszip.js");
$.getScript("https://cdnjs.cloudflare.com/ajax/libs/xlsx/0.8.0/xlsx.js");

AS.apiAuth.setCredentials(Cons.creds.login, Cons.creds.password);

const eventShow = {type: 'set_hidden', hidden: false};
const eventHide = {type: 'set_hidden', hidden: true};

const parseExcellToJSON = file => {
  return new Promise(resolve => {
    try {
      const reader = new FileReader();

      reader.onload = function(e) {
        const result = {};
        const data = e.target.result;
        const workbook = XLSX.read(data, {type: 'binary'});
        workbook.SheetNames.forEach(sheetName => {
          const XL_row_object = XLSX.utils.sheet_to_row_object_array(workbook.Sheets[sheetName]);
          result[sheetName] = XL_row_object;
        });
        resolve(result);
      };

      reader.onerror = ex => {
        console.log(ex);
        resolve(null);
      }

      reader.readAsBinaryString(file);
    } catch (e) {
      console.log(`ERROR: ${e.message}`);
    }

  });
}

const getInputFile = () => {
  const selectFileContainer = $('<div class="uk-margin margined" uk-margin style="width: 100%">');
  const formCustom = $('<div uk-form-custom="target: true" style="width: 100%;"></div>');
  const inputFile = $('<input type="file">');

  formCustom.append(inputFile)
  .append(`<input class="uk-input uk-form-width-medium" type="text" style="font-size: 14px; color: #333; background: #fff; min-width: 245px; width: 100%;" placeholder="Выбрать XLSX файл" disabled>`);

  selectFileContainer.append(formCustom);

  return {selectFileContainer, inputFile};
}

const emptyValidator = input => {
  if (input && input.text && input.text !== '') {
    fire({type: 'input_highlight', error: false}, input.code);
    return true;
  } else {
    fire({type: 'input_highlight', error: true}, input.code);
    return false;
  }
}

const getCurrentDate = () => {
  const datetime = new Date();
  return datetime.getFullYear() + '-' +
    ('0' + (datetime.getMonth() + 1)).slice(-2) + '-' +
    ('0' + datetime.getDate()).slice(-2) + ' ' +
    ('0' + datetime.getHours()).slice(-2) + ':' +
    ('0' + datetime.getMinutes()).slice(-2) + ':' +
    ('0' + datetime.getSeconds()).slice(-2);
}

const setLog = (msg, type) => {
  const text = `<b>${getCurrentDate()}</b> [${type.toUpperCase()}] - ${msg}`;
  $('#panelLog').append(`<p class="log-${type.toLowerCase()}">${text}</p>`);
}

const createDocRCC = (registryCode, asfData, sendToActivation = false) => {
  return new Promise((resolve, reject) => {
    const body = JSON.stringify({
      registryCode: registryCode,
      data: asfData,
      sendToActivation: false
    });
    AS.FORMS.ApiUtils.simpleAsyncPost('rest/api/registry/create_doc_rcc',
    res => resolve(res), null, body, "application/json; charset=utf-8",
    err => resolve(err.responseJSON));
  });
}

const createAsfData = row => {
  const asfData = [];
  asfData.push({id: "kw_form_testQuestion_theme", type: "textbox", value: row['Тема']});
  asfData.push({id: "kw_form_testQuestion_name", type: "textbox", value: row['Тестовый вопрос']});
  asfData.push({id: "hcm_form_testQuestion_name_kz", type: "textbox", value: ''});
  asfData.push({id: "kw_form_testQuestion_answerCorrect", type: "listbox", value: row['номер правильного ответа'], key: row['номер правильного ответа']});

  const table = {
    id: 'kw_form_testQuestion_tableAnswers',
    type: 'appendable_table',
    data: []
  }

  for(let i = 1; i <= 4; i++) {
    table.data.push({id: `kw_form_testQuestion_number-b${i}`, type: "textbox", value: String(i)});
    table.data.push({id: `kw_form_testQuestion_answer-b${i}`, type: "textbox", value: row[`Ответ ${i}`]});
  }

  asfData.push(table);

  return asfData;
}

const getAllCountRows = jsonData => {
  let count = 0;
  for(const sheet in jsonData) count += jsonData[sheet].length;
  return count;
}

const setProgres = (countAllRows, finishRows, inputFile) => {
  let percent = 100 * finishRows / countAllRows;
  if(percent > 99) {
    percent = 100;
    Cons.hideLoader();
    fire(eventShow, 'startImportButton');
    inputFile.val(null);
  }

  fire({type: 'change_label', text: localizedText(`${percent}%`, `${percent}%`, `${percent}%`)}, 'progressLabel');
  $(`#panprogressBar`).css({'width': `${percent}%`});
}

const searchInRegistry = params => {
  return new Promise((resolve, reject) => {
    rest.synergyGet(`api/registry/data_ext?${params}`, res => resolve(res), err => reject(err));
  });
}

const searchCourse = courseCode => {
  let params = $.param({
    registryCode: 'kw_registry_course',
    field: 'kw_form_skill_code',
    condition: 'TEXT_EQUALS',
    value: courseCode,
    loadData: false
  });

  return new Promise(resolve => {
    searchInRegistry(params).then(res => {
      res.count == 0 ? resolve(null) : resolve(res.data[0]);
    }).catch(err => {
      resolve(null);
      console.log('searchCourse ERROR', err);
    });
  });
}

const mergeFormData = (uuid,  data) => {
  return new Promise(resolve => {
    AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/asforms/data/merge",
      res => res.errorCode != 0 ? resolve(null) : resolve(res),
      null, JSON.stringify({uuid, data}), "application/json; charset=utf-8",
      err => {
        console.log('mergeFormData ERROR:', err.responseJSON);
        resolve(null);
      }
    );
  });
}

const saveQuestionsToCourse = (uuid, docs, courseCode) => {

  const mergeAsfData = [{
    id: 'kw_form_course_resTest',
    type: 'reglink',
    key: docs.map(x => x.documentID).join(';'),
    valueID: docs.map(x => x.documentID).join(';'),
    value: docs.map(x => x.name).join(';')
  }];

  mergeFormData(uuid, mergeAsfData).then(res => {
    if(res) {
      setLog(`Курс [${courseCode}] успешно обновлен`, 'info');
    } else {
      setLog(`Ошибка сохранения курса [${courseCode}], dataUUID: <b>${uuid}</b>`, 'error');
    }
  });
}

const importData = (jsonData, registryCode, inputFile) => {

  const countAllRows = getAllCountRows(jsonData);
  let finishRows = 0;

  for(const sheet in jsonData) {
    const rows = jsonData[sheet];
    const questions = [];
    const promises = [];
    const courseCode = rows[0]['код курса'];

    setLog(`Импорт данных, лист <b>"${sheet}"</b>, количество строк: <b>${rows.length}</b>`, 'info');

    searchCourse(courseCode).then(course => {
      if(course) {
        setLog(`Код курса: <b>${courseCode}</b>`, 'info');

        rows.forEach(row => {
          questions.push({name: row['Тестовый вопрос']});
          promises.push(createDocRCC(registryCode, createAsfData(row)));
        });

        Promise.all(promises).then(results => {
          results.forEach((res, i) => {
            if(res.errorCode != 0) {
              setLog(`Ошибка при создании тестового вопроса: <b>${res.errorMessage}</b>`, 'error');
            } else {
              setLog(`Создание тестового вопроса <b>"${questions[i].name}"</b>, dataUUID: <b>${res.dataID}</b> documentID: <b>${res.documentID}</b>`, 'info');
            }
            finishRows++;
            setProgres(countAllRows, finishRows, inputFile);
            questions[i].documentID = res?.documentID || null;
          });

          saveQuestionsToCourse(course.dataUUID, questions, courseCode);
        });

      } else {
        setLog(`Не найден курс с кодом: <b>${courseCode}</b>`, 'error');
        finishRows += rows.length;
        setProgres(countAllRows, finishRows, inputFile);
      }
    });

  }
}

pageHandler('import_tests_page', () => {
  const {selectFileContainer, inputFile} = getInputFile();

  $('#panelFileinput').append(selectFileContainer);

  $('#registryCodeInput').off().on('input', () => {
    $('#registryCodeInput').parent().removeClass('cons-color-red');
  });

  $('#startImportButton').off().on('click', e => {
    e.preventDefault();
    e.target.blur();

    fire({type: 'change_label', text: localizedText(`0%`, `0%`, `0%`)}, 'progressLabel');
    $(`#panprogressBar`).css({'width': `0%`});

    const registryCodeInput = $('#registryCodeInput').val();

    if(!registryCodeInput || registryCodeInput == '') {
      showMessage('Не указан код реестра', 'error');
      return;
    }

    if(!inputFile[0].files.length) {
      showMessage('Не выбран XLSX файл для загрузки', 'error');
      return;
    }

    const importFile = inputFile[0].files[0];

    if(importFile.name.substr(importFile.name.lastIndexOf('.') + 1).toLowerCase() != 'xlsx') {
      showMessage('Выбран не подходящий файл для загрузки', 'error');
      return;
    }

    Cons.showLoader();
    fire(eventHide, 'startImportButton');

    setLog(`Код реестра: "${registryCodeInput.text}"`, 'info');
    setLog(`Файл: "${importFile.name}" || размер: ${importFile.size / 1024} КБ`, 'info');

    parseExcellToJSON(importFile).then(res => importData(res, registryCodeInput.text, inputFile));

  });

});
