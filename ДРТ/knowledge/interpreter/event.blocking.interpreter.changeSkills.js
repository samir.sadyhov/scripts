var result = true;
var message = "ok";

const cmp = {
  name: 'hcm_form_userCard_competenceName',
  result: 'hcm_form_userCard_result',
  status: 'hcm_form_userCard_status',
  finishDate: 'hcm_form_userCard_finishDate',
  table: 'hcm_form_userCard_competenceTable',
  tableOther: 'hcm_form_userCard_competenceTable_other'
}

function getPositionCompetence(asfData) {
  let result = null;
  let data = UTILS.getValue(asfData, 'hcm_form_position_competence_name');
  if(data && data.hasOwnProperty('key')) {
    result = [];
    let keys = data.key.split(';');
    let values = data.value.split(';');
    keys.forEach(function(key, i){
      result.push({key: key, value: values[i]});
    });
  }
  return result;
}

function parseCompetenceTable(table, other){
  let result = [];
  if(table.hasOwnProperty('data')){
    let tableLength = UTILS.getTableBlockIndex(table, cmp.name + (other ? '_other' : ''));
    for(let i = 1; i < tableLength; i++) {
      if(UTILS.getValue(table, cmp.name + (other ? '_other-b' : '-b') + i))
      result.push({
        name: UTILS.getValue(table, cmp.name + (other ? '_other-b' : '-b') + i),
        result: UTILS.getValue(table, cmp.result + (other ? '_other-b' : '-b') + i),
        status: UTILS.getValue(table, cmp.status + (other ? '_other-b' : '-b') + i),
        finishDate: UTILS.getValue(table, cmp.finishDate + (other ? '_other-b' : '-b') + i)
      });
    }
  }
  return result;
}

function getUserCompetence(dataUUID) {
  let asfData = API.getFormData(dataUUID);
  let skills = parseCompetenceTable(UTILS.getValue(asfData, cmp.table));
  let otherSkills = parseCompetenceTable(UTILS.getValue(asfData, cmp.tableOther), true);
  return skills.concat(otherSkills);
}

function getResultTable(positionCompetence, userCompetence) {
  let table = {id: cmp.table, type: 'appendable_table', data: []};
  let tableOther = {id: cmp.tableOther, type: 'appendable_table', data: []};
  let otherCompetence = [];
  let tbi = 1;

  userCompetence.forEach(function(item){
    let pos = positionCompetence.filter(function(x){if(x.key == item.name.key) return x});
    if(!pos.length) otherCompetence.push(item);
  });

  positionCompetence.forEach(function(item){
    let competence = userCompetence.filter(function(x){if(x.name.key == item.key) return x});
    if(competence && competence.length) {
      competence = competence[0];
      UTILS.setValue(table, cmp.name + '-b' + tbi, competence.name);
      UTILS.setValue(table, cmp.result + '-b' + tbi, competence.result);
      UTILS.setValue(table, cmp.status + '-b' + tbi, competence.status);
      UTILS.setValue(table, cmp.finishDate + '-b' + tbi, competence.finishDate);
    } else {
      UTILS.setValue(table, cmp.name + '-b' + tbi, {
        type: 'reglink',
        value: item.value,
        key: item.key,
        valueID: item.key
      });
      UTILS.setValue(table, cmp.result + '-b' + tbi, {type: 'reglink'});
      UTILS.setValue(table, cmp.status + '-b' + tbi, {type: 'listbox', value: "Без статуса", key: "0"});
      UTILS.setValue(table, cmp.finishDate + '-b' + tbi, {type: 'date'});
    }
    tbi++;
  });

  tbi=1;
  otherCompetence.forEach(function(item){
    if(item.result.hasOwnProperty('key')) {
      UTILS.setValue(tableOther, cmp.name + '_other-b' + tbi, item.name);
      UTILS.setValue(tableOther, cmp.result + '_other-b' + tbi, item.result);
      UTILS.setValue(tableOther, cmp.status + '_other-b' + tbi, item.status);
      UTILS.setValue(tableOther, cmp.finishDate + '_other-b' + tbi, item.finishDate);
      tbi++;
    }
  });

  return {competenceTable: table, otherCompetenceTable: tableOther};
}

try {
  let currentFormData = API.getFormData(dataUUID);
  let positionCompetence = getPositionCompetence(currentFormData);
  if(!positionCompetence) throw new Error('не выбраны умения');

  let searchResult = API.httpGetMethod('rest/api/registry/data_ext?registryCode=hcm_registry_userCards&loadData=false&field=hcm_form_userCard_reglink&condition=CONTAINS&key=' + documentID);

  if(searchResult.count == 0) throw new Error('не найдено карточек пользователей');

  let tableBooks = UTILS.getValue(currentFormData, 'hcm_form_position_books');
  tableBooks.id = 'hcm_form_userCard_books';

  searchResult.data.forEach(function(item){
    let userCompetence = getUserCompetence(item.dataUUID);
    let tables = getResultTable(positionCompetence, userCompetence);
    let allCourse = UTILS.getTableBlockIndex(tables.competenceTable, cmp.name) - 1;
    API.mergeFormData({
      uuid: item.dataUUID,
      data: [
        tables.competenceTable,
        tables.otherCompetenceTable,
        tableBooks,
        {
          id: 'hcm_form_userCard_all',
          type: 'numericinput',
          key: String(allCourse),
          value: String(allCourse)
        }
      ]
    });
    API.httpGetMethod("rest/api/registry/modify_doc?dataUUID=" + item.dataUUID);
  });

} catch (err) {
  log.error(err.message);
  message = err.message;
}
