const positionCode = 'obuchauschiesya_1'; // код должности которую надо записать в профиль

var result = true;
var message = "ok";

try {

  let searchResult = API.httpGetMethod('rest/api/registry/data_ext?registryCode=hcm_registry_positions&fields=hcm_form_position_position');

  if(searchResult.recordsCount == 0) throw new Error('не найдено записей в реестра карточка должности');

  let successRow = 0;
  let errorRow = 0;
  let foundRow = 0;

  let posInfo = API.httpGetMethod('rest/api/positions/search?pointer_code=' + positionCode);

  if(posInfo.length > 0) {
    posInfo = API.httpGetMethod('rest/api/positions/get?positionID=' + posInfo[0]);
  } else {
    posInfo = null;
  }

  searchResult.result.forEach(function(item, i){

    if(item.fieldValue.hasOwnProperty('hcm_form_position_position')) {

      let searchResult2 = API.httpGetMethod('rest/api/registry/data_ext?registryCode=hcm_registry_trainingProfile&countInPart=1&loadData=false&field=hcm_form_profile_name&condition=TEXT_EQUALS&value=' + encodeURIComponent(item.fieldValue.hcm_form_position_position));

      if(searchResult2.count == 0) {

        let itemAsfData = API.getFormData(item.dataUUID);
        let newRowData = [];

        newRowData.push({
          id: 'hcm_form_profile_name',
          type: 'textbox',
          value: item.fieldValue.hcm_form_position_position
        });

        newRowData.push({
          id: 'hcm_form_position_number',
          type: 'numericinput',
          value: String(i + 1),
          key: String(i + 1)
        });

        if(posInfo) {
          newRowData.push({
            id: 'hcm_form_position_choice',
            type: 'entity',
            key: posInfo.positionID,
            value: posInfo.nameRu
          });
        }

        UTILS.setValue(newRowData, 'hcm_form_position_competence_name', UTILS.getValue(itemAsfData, 'hcm_form_position_competence_name'));
        UTILS.setValue(newRowData, 'hcm_form_position_books', UTILS.getValue(itemAsfData, 'hcm_form_position_books'));

        let resultCreateDoc = API.httpPostMethod("rest/api/registry/create_doc_rcc", {
          registryCode: 'hcm_registry_trainingProfile',
          sendToActivation: true,
          data: newRowData
        }, "application/json; charset=utf-8");

        if(resultCreateDoc.errorCode == 0) {
          successRow++;
        } else {
          errorRow++;
        }

      } else {
        foundRow++;
      }
    } else {
      errorRow++;
    }
  });

  log.info('РЕЗУЛЬТАТ:', 'Всего найдено записей: ' + searchResult.recordsCount, 'Создано новых записей: ' + successRow, 'Записей уже создано ранее: ' + foundRow, 'Записей обработанных с ошибкой: ' + errorRow);

} catch (err) {
  log.error(err.message);
  message = err.message;
}
