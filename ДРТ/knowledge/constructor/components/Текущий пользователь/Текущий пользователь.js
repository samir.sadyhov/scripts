$('#userName').html(`${AS.OPTIONS.currentUser.lastname} ${AS.OPTIONS.currentUser.firstname}`);

const interval = setInterval(() => {
  const {userCard, mainCoursesTable} = Cons.getAppStore();

  if (!userCard) {
    return false
  } else {
    const tmp = Object.values(mainCoursesTable);
    const allCoursesCount = tmp.length || 1;
    const currentCoursesCount = tmp.filter(x => x.hcm_form_userCard_status.key == '2').length || 0;

    clearInterval(interval);

    new ProgressBar.Line('#progress', {
      strokeWidth: 2,
      easing: 'easeInOut',
      duration: 2000,
      color: '#ef4139',
      trailColor: '#222',
      trailWidth: 1,
      svgStyle: {
        width: '215px',
        right: 0,
        float: 'right'
      },
      text: {
        style: {
          color: '#222',
          top: '15px',
          position: 'absolute',
          right: '25px',
          transform: null
        },
        autoStyleContainer: false
      },
      from: {
        color: '#FFEA82'
      },
      to: {
        color: '#ED6A5A'
      },
      step: (state, bar) => bar.setText(Math.round(bar.value() * 100) + ' %')
    }).animate(currentCoursesCount / allCoursesCount);
  }
}, 500);
