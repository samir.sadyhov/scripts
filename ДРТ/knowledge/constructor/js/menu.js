if($('.menu__icon').length) {

  $('.menu__icon').off().on('click', e => {
    e.preventDefault();
    $('.mobile-menu').toggleClass('_active');
    $('body').toggleClass('_lock');
  });

  $('.mobile-menu .menuItem').off().on('click', e => {
    $('.mobile-menu').removeClass('_active');
    $('body').removeClass('_lock');
  });
  
}
