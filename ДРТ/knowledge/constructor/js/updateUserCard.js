this.updateUserCard = () => {
  const {userCardUUID} = Cons.getAppStore();

  rest.synergyGet(`api/asforms/data/${userCardUUID}`, res => {
    const userCard = res.data.filter(item => item.type !== 'label');

    Cons.setAppStore({
      userCard,
      mainCoursesTable: convertTable(userCard.find(item => item.id === 'hcm_form_userCard_competenceTable')?.data),
      otherCoursesTable: convertTable(userCard.find(item => item.id === 'hcm_form_userCard_competenceTable_other')?.data),
      booksTable: convertTable(userCard.find(item => item.id === 'hcm_form_userCard_books')?.data)
    });

    Cons.hideLoader();
  }, err => console.error(err));
}
