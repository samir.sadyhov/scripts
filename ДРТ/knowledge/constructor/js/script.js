const getAllCoursesCount = () => {
    const params = Qs.stringify({
        registryCode: 'hcm2_registry_competence',
        field: 'hcm_form_compitience_status',
        condition: 'EQUALS',
        key: '2' // Статус готовности курса - Разработан
    })

    rest.synergyGet(`api/registry/data_ext?${params}`,
        res => fireFn.CHANGE_LABEL({compCode: 'allCoursesCount', text: localizedText(String(res.recordsCount))}),
        err => console.error(err)
    )
}

const getMyCoursesCount = () => {
    const {mainCoursesTable, otherCoursesTable} = Cons.getAppStore()
    const requiredCourses = Object.keys(mainCoursesTable).length
    const optionalCourses = Object.keys(otherCoursesTable).length
    const sum = requiredCourses + optionalCourses

    fireFn.CHANGE_LABEL({compCode: 'myCoursesCount', text: localizedText(String(sum))})
}

const getBooksCount = () => {
    const {booksTable} = Cons.getAppStore()
    const booksCount = Object.keys(booksTable).length

    fireFn.CHANGE_LABEL({compCode: 'booksCount', text: localizedText(String(booksCount))})
}

const setCoursesCount = () => {
    const waitingForUserCard = setInterval(() => {
        if (Cons.getAppStore().userCard) {
            getAllCoursesCount()
            getMyCoursesCount()
            getBooksCount()

            clearInterval(waitingForUserCard)
        }
    }, 500)
}

pageHandler('all_courses', () => {
    $('.uk-accordion').remove()

    $('.kp').parent().prop({
        hidden: false
    })

    setCoursesCount()
})

pageHandler('courses_list', () => {
    setCoursesCount()
})

pageHandler('profile', () => {
    setCoursesCount()
})

pageHandler('my_courses', () => {
    setCoursesCount()
})

pageHandler('library', () => {
    setCoursesCount()
})

const addGoBackListener = pageCode => {
  const {prevPage, currentGroup} = Cons.getAppStore();
  if(!Cons.getAppStore()[`addGoBackListener_${pageCode}`]) {
    addListener('button_click', 'goBack', () => {
      const pageParams = prevPage === 'courses_list' ? [{
        pageParamName: 'uuid',
        value: currentGroup
      }] : [];

      updateUserCard();
      fireFn.GOTO_PAGE({compCode: 'goBack', pageCode: prevPage, pageParams: pageParams});
    });

    let s = {};
    s[`addGoBackListener_${pageCode}`] = true;
    Cons.setAppStore(s);
  }
}

const addMenuClickListener = courseUUID => {
    const wait = setInterval(() => {
        if (Cons.getAppStore().currentCourse || courseUUID) {
            clearInterval(wait)

            const {currentCourse} = Cons.getAppStore()
            const value = courseUUID ?? currentCourse

            const buttonsArr = ['gotoCoursePage', 'gotoEducation', 'gotoTesting']
            const pagesArr = ['course_page', 'education', 'testing']

            buttonsArr.forEach((buttonCode, index) => {
                $(`#${buttonCode}`).click(() => {
                    fireFn.GOTO_PAGE({
                        compCode: 'top',
                        pageCode: pagesArr[index],
                        pageParams: [{
                            pageParamName: 'uuid',
                            value: value
                        }]
                    })
                })
            })
        }
    }, 500)
}

pageHandler('course_page', () => {
    addGoBackListener('course_page')
    addMenuClickListener()
})

pageHandler('testing', () => {
    addGoBackListener('testing')
    addMenuClickListener()
})

pageHandler('education', () => {
    addGoBackListener('education')
    addMenuClickListener()
})

if(!Cons.getAppStore().value_changed_locale_selector) {
  addListener('value_changed_locale_selector', 'localeSelector', e => {
    const locale = e.value;
    Cons.setAppStore({locale: locale});
    AS.OPTIONS.locale = locale;
    localStorage.locale = locale;
  });
  Cons.setAppStore({value_changed_locale_selector: true});
}

this.changeViewStore = (prop, newValue) => {
    const store = Cons.getAppStore();
    const settings = JSON.parse(localStorage.getItem('k_settings'));

    Cons.setAppStore({
        ...store,
        settings: {
            views: {
                ...store.settings.views,
                [prop]: newValue
            }
        }
    });

    localStorage.setItem('k_settings', JSON.stringify({
        views: {
            ...settings.views,
            [prop]: newValue
        }
    }))
};

setInterval(() => {
    this.state = Cons.getAppStore()
    this.app = Cons.getCurrentApp()
    this.page = getCurrentPage()
    this.comp = compCode => getCompByCode(compCode)
}, 2000)
