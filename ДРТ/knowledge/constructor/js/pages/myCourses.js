const getDataByDocumentId = (docId, handler) => {
    rest.synergyGet(`api/formPlayer/getAsfDataUUID?documentID=${docId}`,
        dataUUID => {
            rest.synergyGet(`api/asforms/data/get?dataUUID=${dataUUID}`,
                res => handler(res[0].data.filter(item => item.type !== 'label'), dataUUID),
                err => console.error(err)
            )
        },
        err => console.error(err)
    )
}

const compStat = (finDate) => {
    if (finDate) {
        let finishDate = finDate.split(" ")

        finishDate = new Date(finishDate[0], finishDate[1] - 1, finishDate[2])

        const currentDay = new Date()
        let remaining = finishDate - currentDay // миллисекунды до даты

        if (remaining <= 0) {
            return 0
        } else {
            remaining /= 1000 // секунды до даты
            remaining /= 60   // минуты до даты
            remaining /= 60   // часы до даты
            remaining /= 24   // дни до даты

            return Math.round(remaining)
        }
    }
}

const getCourseData = (courseItem, option = '', handler) => {
    const competence = courseItem[`hcm_form_userCard_competenceName${option}`]
    const status = courseItem[`hcm_form_userCard_status${option}`].value
    const finishDate = courseItem[`hcm_form_userCard_finishDate${option}`]?.value
    const competenceKey = competence.key
    const name_ru = competence.value

    getDataByDocumentId(competenceKey, (competenceData, dataUUID) => {
        const skillId = competenceData.find(item => item.id === 'hcm_form_compitience_pasport').key
        const uuid = String(dataUUID)

        getDataByDocumentId(skillId, skillData => {
            const code = skillData.find(item => item.id === 'hcm_form_skill_code').value
            const name_kz = skillData.find(item => item.id === 'hcm_form_skill_name_kz')?.value

            handler({
                uuid,
                name_ru,
                name_kz: (name_kz ? (`${code}-${name_kz}`) : undefined),
                status,
                finishDate,
            })
        })
    })
}

const drawListItem = (area, data) => {
    const {locale} = Cons.getAppStore()

    const {uuid, name_ru, name_kz, status, finishDate} = data
    const courseName = locale === 'ru' ? name_ru : (name_kz ?? name_ru)
    const localizedStatus = t(status)
    const date = !finishDate ? `` : `${t('До')} - ${finishDate}`
    const fullString = date ? `${courseName} (${localizedStatus} | ${date})` : `${courseName} (${localizedStatus})`

    const item = jQuery('<div>')
        .attr({
            uuid,
            class: 'listItem'
        })
        .html(fullString)
        .click(() => {
            fireFn.GOTO_PAGE({
                compCode: 'middle',
                pageCode: 'course_page',
                pageParams: [{
                    pageParamName: 'uuid',
                    value: uuid
                }]
            })
        })

    $(area).append(item)
}

const setCourseStatusBar = (props) => {
    const {uuid, status, description, color, keyframe} = props

    const interval = setInterval(() => {
        const parentEl = $('#rightPanel span').filter(function () {
            return this.innerText === uuid
        })

        if (!parentEl.length) {
            return
        }

        const parentElement = parentEl
            .parent()
            .parent()
            .filter((a, b) => !$(b).is(':hidden'))

        const element = parentElement[0]

        const statusDescription = jQuery('<span>').css({color: color}).html(description)

        parentElement.parent().find('.desc')[0].append($(statusDescription)[0])

        new ProgressBar.Line(element, {
            strokeWidth: 2,
            easing: 'easeOut',
            duration: 4000,
            color: color,
            trailColor: '#222',
            trailWidth: 1,
            text: {
                style: {
                    marginTop: '15px',
                },
                autoStyleContainer: true
            },
            step: (state, bar) => {
                bar.setText(status)
            }
        }).animate(1 - (keyframe / 100))

        clearInterval(interval)
    }, 500)
}

const detectAndSetCourseStatus = (item) => {
    const {finishDate, status, uuid} = item
    const [failColor, successColor] = ['#db3f3a', '#6ccb6c']

    let finishCourseDate, formatFinishDate
    let [message, description, color, keyframe] = ['', '', '', 100]

    /**
     * @keyframe может быть 100 (статус-бар не окрашен), 0 (окрашен полностью), n (значение, равное количеству оставшихся дней)
     */

    if (finishDate) {
        const date = finishDate.split(".")

        let finDay = parseInt(date[0])
        let finMonth = parseInt(date[1])
        const finYear = parseInt(date[2])

        if (finDay < 10) {
            finDay = '0' + finDay
        }

        if (finMonth < 10) {
            finMonth = '0' + finMonth
        }

        finishCourseDate = finYear + " " + finMonth + " " + finDay
        formatFinishDate = finDay + "." + finMonth + "." + finYear
    }

    const state = compStat(finishCourseDate) //СКОЛЬКО ДНЕЙ ОСТАЛОСЬ

    if (status === "Обучение") {
        if (state === 0) {
            message = t('Срок обучения истек')
            color = failColor
            keyframe = 0 // Полоса статус-бара полностью окрашена

            if (!isNaN(formatFinishDate[0] + formatFinishDate[1])) {
                description = `${t('Обучение')} - ${t('До')} ${formatFinishDate} `
            } else {
                description = t('Обучение')
            }
        } else {
            if (state !== undefined) {
                message = t('Обучение')
                color = successColor

                if (!isNaN(formatFinishDate[0] + formatFinishDate[1])) {
                    description += `${t('До')} ${formatFinishDate} `
                }

                if (!isNaN(state)) {
                    description += `${t('Осталось дней:')} ${state}`
                    keyframe = state
                }
            }
        }
    } else if (status === "Без статуса") {
        message = t('Без статуса')
        color = successColor

        if (state === 0) {
            if (state !== undefined) {
                if (!isNaN(formatFinishDate[0] + formatFinishDate[1])) {
                    description = `${t("До")} ${formatFinishDate} `
                }
            }
        }
    } else if (status === "Обучение завершено" || status === "Подтверждено 360") {
        message = t('Обучение завершено')
        color = successColor
        keyframe = 0 // Полоса статус-бара полностью окрашена
    }

    setCourseStatusBar({
        uuid,
        status: message,
        description,
        color,
        keyframe
    })
}

const extractCoursesData = ({mainCoursesTable, otherCoursesTable, mainHandler, otherHandler}) => {
    for (const i in mainCoursesTable) {
        if (mainCoursesTable.hasOwnProperty(i)) {
            const courseItem = mainCoursesTable[i]

            if (!courseItem) {
                continue
            }

            getCourseData(courseItem, undefined, courseData => {
                mainHandler(courseData)
            })
        }
    }

    for (const i in otherCoursesTable) {
        if (otherCoursesTable.hasOwnProperty(i)) {
            const courseItem = otherCoursesTable[i]

            if (!otherCoursesTable[i]) {
                continue
            }

            getCourseData(courseItem, '_other', courseData => {
                otherHandler(courseData)
            })
        }
    }
}

const drawMyCourses = () => {
    const {settings, mainCoursesTable, otherCoursesTable} = Cons.getAppStore()
    const {os} = settings.views

    if (os) {
        fireFn.SHOW('mainPanel')

        const container = jQuery('<ul>')
            .attr({
                'uk-accordion': true
            })
            .css({
                width: '80%'
            })

        const containerElementMain = jQuery('<li>').attr({
            class: 'main'
        })
        const containerElementOther = jQuery('<li>').attr({
            class: 'other'
        })

        const titleMain = jQuery('<a>').attr({
            class: 'uk-accordion-title'
        }).html(t('Обязательные:'))
        const titleOther = jQuery('<a>').attr({
            class: 'uk-accordion-title'
        }).html(t('Дополнительные:'))

        const contentMain = jQuery('<div>').attr({
            class: 'uk-accordion-content main-content'
        })
        const contentOther = jQuery('<div>').attr({
            class: 'uk-accordion-content other-content'
        })

        containerElementMain.append(titleMain, contentMain)
        containerElementOther.append(titleOther, contentOther)

        container.append(containerElementMain, containerElementOther)

        $('#mainPanel').append(container)

        extractCoursesData({
            mainCoursesTable,
            otherCoursesTable,
            mainHandler: courseData => drawListItem('.main-content', courseData),
            otherHandler: courseData => drawListItem('.other-content', courseData)
        })
    } else {
        const [mainCoursesList, otherCoursesList] = [[], []]

        Cons.showLoader()

        extractCoursesData({
            mainCoursesTable,
            otherCoursesTable,
            mainHandler: courseData => mainCoursesList.push(courseData),
            otherHandler: courseData => otherCoursesList.push(courseData)
        })

        const mainInterval = setInterval(() => {
            if (mainCoursesList.length === Object.keys(mainCoursesTable).length) {
                clearInterval(mainInterval)

                const filteredMainCoursesList = []

                for (const item in mainCoursesTable) {
                    if (mainCoursesTable.hasOwnProperty(item)) {
                        const courseName = mainCoursesTable[item]['hcm_form_userCard_competenceName'].value

                        filteredMainCoursesList.push(mainCoursesList.find(item => item.name_ru === courseName))
                    }
                }

                fireFn.CHANGE_REPEATER_CUSTOM_SOURCE({compCode: 'mainCoursesRepeater', data: filteredMainCoursesList})

                mainCoursesList.length && mainCoursesList.forEach((item, index) => {
                    detectAndSetCourseStatus(item)

                    index === (mainCoursesList.length - 1) && Cons.hideLoader()
                })
            }
        }, 1000)

        const otherInterval = setInterval(() => {
            if (otherCoursesList.length === Object.keys(otherCoursesTable).length) {
                clearInterval(otherInterval)

                const filteredOtherCoursesList = []

                for (const item in otherCoursesTable) {
                    if (otherCoursesTable.hasOwnProperty(item)) {
                        const courseName = otherCoursesTable[item]['hcm_form_userCard_competenceName_other'].value

                        filteredOtherCoursesList.push(otherCoursesList.find(item => item.name_ru === courseName))
                    }
                }

                fireFn.CHANGE_REPEATER_CUSTOM_SOURCE({compCode: 'otherCoursesRepeater', data: filteredOtherCoursesList})

                otherCoursesList.length && otherCoursesList.forEach(item => {
                    detectAndSetCourseStatus(item)
                })
            }
        }, 1000)

        Object.keys(mainCoursesTable).length && fireFn.SHOW('mainCoursesPanel')
        Object.keys(otherCoursesTable).length && fireFn.SHOW('otherCoursesPanel')
    }
}

pageHandler('my_courses', () => {
    if (!Cons.getAppStore().my_courses_changed_locale_listener) {
        addListener('value_changed_locale_selector', 'localeSelector', () => {
            $('#mainPanel').empty();
            drawMyCourses();
        });
        Cons.setAppStore({my_courses_changed_locale_listener: true});
    }

    $('.uk-accordion').remove()

    Cons.setAppStore({
        prevPage: 'my_courses'
    })

    $('.kp').parent().prop({
        hidden: true
    })

    const wait = setInterval(() => {
        if (Cons.getAppStore().mainCoursesTable && Cons.getAppStore().otherCoursesTable) {
            clearInterval(wait)

            drawMyCourses()
        }
    }, 1000)

    $('.os').off('click')
    $('.os').click(e => {
        const newValue = e.target.checked

        fireFn.HIDE('mainCoursesPanel')
        fireFn.HIDE('otherCoursesPanel')
        $('#mainPanel').empty()

        changeViewStore('os', newValue)

        $('.kp').prop('disabled', $('.os').is(':checked'))

        drawMyCourses()
    })
})
