const ENUM = {
    LESSON_REPEATER: {
        ru: 'lessonsRepeater_ru',
        kk: 'lessonsRepeater_kz'
    },
    LOCALES: {
        RU: 'ru',
        KK: 'kk'
    }
}

const getFile = (fileId, handler) => {
    const {login, password} = Cons.getAppStore().creds
    const auth = btoa(unescape(encodeURIComponent(`${login}:${atob(password)}`)))

    const xhr = new XMLHttpRequest()
    xhr.open('GET', location.origin + AS.OPTIONS.coreUrl + 'rest/api/storage/pdf/get?inline=true&identifier=' + fileId, true)
    xhr.setRequestHeader("Authorization", "Basic " + auth)
    xhr.responseType = 'blob'
    xhr.onload = function (e) {
        if (this.status === 200) {
            const blob = window.URL.createObjectURL(this.response)

            handler(blob)
        } else {
            showMessage('Ошибка открытия файла. Обратитесь к менеджеру обучения!', 'error')
        }
    }
    xhr.send()
}

const fillTheLessonMaterials = ({content, handler}) => {
    const {value, header, name, type} = content

    switch (type) {
        case 'text': {
            name && handler({
                label: header,
                buttonText: name,
                icon: ''
            })

            break
        }
        case 'link': {
            return handler({
                label: header,
                buttonText: name,
                icon: 'link',
                link: value
            })
        }
        case 'file': {
            value && handler({
                label: header,
                buttonText: (name ? name : t('Ссылка на документ')),
                icon: 'file',
                identifier: value
            })

            break
        }
    }
}

const getDataFromLessonArray = ({lessonData, locale}) => {
    // передали массив с json для конкретного урока
    // если есть ютуб видео - отображать его, если нет - отображать файл
    // проверяем все поля: в них может содержаться текст, ссылка или файл

    const postfix = locale === 'ru' ? '' : '_kz'

    const getField = field => lessonData[`${field}${postfix}`]

    const VIDEO_LINK = getField(`itsm_form_competence_videolink`)
    const VIDEO_FILE = getField(`itsm_form_competence_videofile`)
    const TASK_NAME = getField(`itsm_form_competence_taskname`)
    const MAIN = getField(`itsm_form_competence_main`)
    const MATERIAL_FILE = getField(`itsm_form_competence_materialfile`)
    const MATERIAL_LINK = getField(`itsm_form_competence_materiallink`)
    const MATERIAL_FILE_1 = getField(`itsm_form_competence_material1file`)
    const MATERIAL_LINK_1 = getField(`itsm_form_competence_material1link`)
    const TEST_LINK = getField(`itsm_form_competence_testlink`)
    const TEST_FILE = getField(`itsm_form_competence_testfile`)
    const PRACTICAL_TASK_FILE = getField(`itsm_form_competence_practicaltaskfile`)
    const PRACTICAL_TASK = getField(`itsm_form_competence_practicaltask`)
    const STANDARDS = getField(`itsm_form_competence_standards`)

    const descriptionArr = []

    const emptyObj = {
        name: '',
        value: '',
        header: '',
        type: ''
    }

    const baseStructure = {
        taskName: TASK_NAME.value,
        taskMain: MAIN.value ?? '',
        video: VIDEO_LINK ? (
            VIDEO_LINK.value ? {
                name: VIDEO_LINK.key.split(';')[0],
                value: VIDEO_LINK.value,
                header: localizedText('Видео урока', 'Видео урока', 'Сабақ бейнесі', ''),
                type: 'link'
            } : {
                name: VIDEO_FILE.value,
                value: VIDEO_FILE.key,
                header: localizedText('Видео урока', 'Видео урока', 'Сабақ бейнесі', ''),
                type: 'file'
            }
        ) : emptyObj,
    };

    const getLinkName = field => {
        const split = field.key.split(';')

        return split.length === 2 ? split[0] : field.value
    }

    const t = phrase => {
        const dict = {
            'Учебный материал': {
                ru: 'Учебный материал',
                kk: 'Оқу материалы'
            },
            'Дополнительный материал': {
                ru: 'Дополнительный материал',
                kk: 'Қосымша материал'
            },
            'Тестирование для самоконтроля': {
                ru: 'Тестирование для самоконтроля',
                kk: 'Өзін-өзі бақылауға арналған тестілеу'
            },
            'Практическое задание': {
                ru: 'Практическое задание',
                kk: 'Практикалық тапсырма'
            },
            'Нормативы, подтверждающие навык': {
                ru: 'Нормативы, подтверждающие навык',
                kk: 'Нормативы, подтверждающие навык'
            }
        }

        return dict[phrase][locale]
    }

    const structure = {
        material: MATERIAL_LINK ? (
            MATERIAL_LINK.value ? {
                name: getLinkName(MATERIAL_LINK),
                value: MATERIAL_LINK.value,
                header: t('Учебный материал'),
                type: 'link'
            } : {
                name: MATERIAL_FILE.value,
                value: MATERIAL_FILE.key,
                header: t('Учебный материал'),
                type: 'file'
            }
        ) : emptyObj,
        material_1: MATERIAL_LINK_1 ? (
            MATERIAL_LINK_1.value ? {
                name: getLinkName(MATERIAL_LINK_1),
                value: MATERIAL_LINK_1.value,
                header: t('Дополнительный материал'),
                type: 'link'
            } : {
                name: MATERIAL_FILE_1.value,
                value: MATERIAL_FILE_1.key,
                header: t('Дополнительный материал'),
                type: 'file'
            }
        ) : emptyObj,
        test: TEST_LINK ? (
            TEST_LINK.value ? {
                name: getLinkName(TEST_LINK),
                value: TEST_LINK.value,
                header: t('Тестирование для самоконтроля'),
                type: 'link'
            } : {
                name: TEST_FILE.value,
                value: TEST_FILE.key,
                header: t('Тестирование для самоконтроля'),
                type: 'file'
            }
        ) : emptyObj,
        practicalTaskFile: PRACTICAL_TASK_FILE ? (
            PRACTICAL_TASK_FILE.value ? {
                name: PRACTICAL_TASK_FILE.value,
                value: PRACTICAL_TASK_FILE.key,
                header: t('Практическое задание'),
                type: 'file'
            } : null
        ) : emptyObj,
        practicalTask: {
            value: PRACTICAL_TASK.value ?? '',
            header: t('Практическое задание'),
            type: 'text'
        },
        standards: {
            value: STANDARDS.value ?? '',
            header: t('Нормативы, подтверждающие навык'),
            type: 'text'
        }
    }

    fireFn.HIDE('video')
    fireFn.CHANGE_LABEL({compCode: 'lessonName', text: localizedText(baseStructure.taskName)})
    fireFn.CHANGE_LABEL({compCode: 'lessonContent', text: localizedText(baseStructure.taskMain)})

    if (baseStructure.video.value) {
        switch (baseStructure.video.type) {
            case "link": {
                fireFn.VIDEO_CHANGE_URL({compCode: 'video', value: baseStructure.video.value})
                fireFn.SHOW('video')

                break
            }
            case "file": {
                fireFn.VIDEO_CHANGE_STORE_ID({value: baseStructure.video.value, compCode: 'video'})
                fireFn.SHOW('video')

                break
            }
        }
    }

    for (const prop in structure) {
        if (structure.hasOwnProperty(prop)) {
            const item = structure[prop]

            item?.type && fillTheLessonMaterials({
                content: item,
                handler: info => descriptionArr.push(info)
            })
        }
    }

    descriptionArr.length && fireFn.SHOW('repeaterPanel')

    fireFn.CHANGE_REPEATER_CUSTOM_SOURCE({compCode: 'descriptionRepeater', data: descriptionArr})

    setTimeout(() => {
        descriptionArr.forEach(item => {
            $(`button:contains('${item.buttonText}'):visible`).click(() => {
                showMessage('Загрузка. Файл откроется автоматически', 'warn')

                const fileId = item.identifier
                const link = item.link

                fileId && getFile(fileId, (blob) => {
                    open(blob, '_blank')
                })

                link && open(link, '_blank')
            })
        })
    }, 500)

    Cons.hideLoader()
}

const trimValue = (value) => {
    if (typeof value === 'string') {
        return value.trim()
    }

    return value
} ;

const showLesson = ({lessonName, courseContent, locale}) => {
    for (const prop in courseContent) {
        if (courseContent.hasOwnProperty(prop)) {
            const lesson = courseContent[prop];

            const taskName_ru = trimValue(lesson['itsm_form_competence_taskname']?.value);
            const taskName_kz = trimValue(lesson['itsm_form_competence_taskname_kz']?.value);

            const isRequiredLesson = (taskName_ru === lessonName) || (taskName_kz === lessonName);

            if (isRequiredLesson) {
                getDataFromLessonArray({
                    lessonData: lesson,
                    locale
                });

                break
            }
        }
    }
};

const setClickHandlerOnButton = (handler) => {
    setTimeout(() => {
        $('#lessonsList button:visible')
            .each(function (index) {
                $(this).click(() => {
                    fireFn.VIDEO_CHANGE_STORE_ID({value: null, compCode: 'video'})

                    const lessonName = this.innerText;

                    handler(lessonName)
                });

                index === 0 && $(this).click()
            })
    }, 1000)
};

const setButtons = ({uuid, locale, courseContent}) => {
    fireFn.CHANGE_REPEATER_DATA_ID({compCode: ENUM.LESSON_REPEATER[locale], uuid: uuid});

    setClickHandlerOnButton(lessonName => showLesson({lessonName, courseContent, locale}))
};

const setLessonsList = (courseContent) => {
    const TASK_NAME_KZ = 'itsm_form_competence_taskname_kz';

    const [{paramValues}, {locale}] = [getCurrentPage(), Cons.getAppStore()];

    const courseHasKazakhContent = !!courseContent[1][TASK_NAME_KZ]?.value;
    const uuid = paramValues.find(param => param.pageParamName === 'uuid').value;

    if (locale === 'ru' || (locale !== 'ru' && !courseHasKazakhContent)) {
        return setButtons({uuid, locale: ENUM.LOCALES.RU, courseContent})
    } else {
        return setButtons({uuid, locale: ENUM.LOCALES.KK, courseContent})
    }
};

pageHandler('education', () => {
    Cons.showLoader();

    const {currentCourseInfo} = Cons.getAppStore();
    const courseData = currentCourseInfo
        .find(i => i.id === 'hcm_form_competence_tableCourseContent').data
        .filter(i => i.type !== 'label');
    const structuredCourseData = convertTable(courseData);
    const contentIsNotEmpty = !!Object.keys(structuredCourseData).length;

    contentIsNotEmpty && setLessonsList(structuredCourseData)
});
