const getFile = (fileId, handler) => {
    const {login, password} = Cons.getAppStore().creds
    const auth = btoa(unescape(encodeURIComponent(`${login}:${atob(password)}`)))

    const xhr = new XMLHttpRequest()
    xhr.open('GET', location.origin + AS.OPTIONS.coreUrl + 'rest/api/storage/pdf/get?inline=true&identifier=' + fileId, true)
    xhr.setRequestHeader("Authorization", "Basic " + auth);
    xhr.responseType = 'blob'
    xhr.onload = function (e) {
        if (this.status === 200) {
            const blob = window.URL.createObjectURL(this.response)

            handler(blob)
        } else if (this.status === 404) {
            showMessage('Файл не найден!', 'error')
        }
    }
    xhr.send()
}

const setClickHandler = (dataArr) => {
    setTimeout(() => {
        dataArr.forEach(item => {
            $(`button:contains(${item.name}):visible`).click(() => {
                const fileId = item.fileID
                const link = item.link

                if (fileId) {
                    showMessage('Загрузка. Файл откроется автоматически', 'warn')

                    getFile(fileId, (blob) => {
                        open(blob, '_blank')
                    })
                }

                link && open(link, '_blank')
            })
        })
    }, 500)
}

const getAndDrawBooksList = () => {
    const {booksTable} = Cons.getAppStore()
    const keys = Object.keys(booksTable)
    const dataArr = []

    if (!keys.length) {
        return showMessage('Библиотека пуста', 'warn')
    }

    for (const id in booksTable) {
        if (booksTable.hasOwnProperty(id)) {
            const book = booksTable[id]
            const bookNum = book['hcm_form_position_bookNum'].value
            const bookFile = book['hcm_form_position_bookFile']
            const bookLink = book['hcm_form_position_bookLink']

            if (!bookNum) {
                continue
            }

            bookFile.value && dataArr.push({
                name: bookFile.value,
                fileID: bookFile.key,
                icon: 'file'
            })

            bookLink.value && dataArr.push({
                name: bookLink.textValue,
                link: bookLink.value,
                icon: 'link'
            })
        }
    }

    dataArr.length && fireFn.SHOW('books')

    fireFn.CHANGE_REPEATER_CUSTOM_SOURCE({compCode: 'books', data: dataArr})

    setClickHandler(dataArr)
}

pageHandler('library', () => {
    getAndDrawBooksList()
})
