const emailRegex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

const emptyValidator = input => {
  if(input.val() !== '') {
    input.removeClass('uk-form-danger');
    return true;
  } else {
    input.addClass('uk-form-danger');
    return false;
  }
}

//валидация введенного email
const emailValidator = input => {
  if(emptyValidator(input)) {
    if(emailRegex.test(input.val())) {
      input.removeClass('uk-form-danger');
      return true;
    } else {
      input.addClass('uk-form-danger');
      return false;
    }
  }
  return false;
}

const generatePassword = () => {
  let charSet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-+_';
  return Array.apply(null, Array(6)).map(() => charSet.charAt(Math.random() * charSet.length)).join('');
}

const searchUser = param => {
  return new Promise((resolve, reject) => {
    try {
      rest.synergyGet(`api/filecabinet/user/checkExistence?${jQuery.param(param)}`, res => resolve(res.result == "true"), err => reject(err));
    } catch (e) {
      reject('Произошла ошибка при поиске пользователя');
    }
  });
}

const changeCredentials = (login, password) => {
  return new Promise((resolve, reject) => {
    rest.synergyPost("api/filecabinet/user/changeCredentials", {
      actionCode: 'CHANGE_PASSWORD',
      login: login,
      password: password,
      passwordConfirm: password
    }, "application/x-www-form-urlencoded; charset=UTF-8", res => {
      resolve(res);
    }, err => {
      reject(err);
    });
  });
}

const sendNotification = body => {
  return new Promise((resolve, reject) => {
    try {
      fetch(`${window.origin}/Synergy/rest/api/notifications/send`, {
        method: 'POST',
        headers: {
          'Authorization': "Basic " + btoa(Cons.creds.login + ":" + Cons.creds.password),
          'Content-Type': 'application/json;charset=utf-8'
        },
        body: JSON.stringify(body)
      }).then(response => response.json()).then(resolve).catch(reject);
    } catch (e) {
      reject('Произошла ошибка при смене пароля');
    }
  });
}

pageHandler('recovery_page', () => {
  let ie = $('#input_login');
  ie.off();
  ie.on('input', () => {
    ie.val(ie.val().replace(/[^A-Za-z0-9_.+-\@]/g,''));
    ie.removeClass('uk-form-danger');
  });

  if(Cons.getAppStore().button_recovery_pass_listener) return;
  Cons.setAppStore({button_recovery_pass_listener: true});

  addListener('button_click', 'button_recovery', (e) => {
    if(!emptyValidator(ie)) {
      showMessage(localizedText('Поле e-mail не заполнено', 'Поле e-mail не заполнено', 'Электрондық пошта өрісі толтырылмаған', 'Поле e-mail не заполнено'), 'error');
      return;
    }
    if(!emailValidator(ie)) {
      showMessage(localizedText('Введено не корректное значение.', 'Введено не корректное значение.', 'Қате мән енгізілді.', 'Введено не корректное значение.'), 'error');
      return;
    }

    try {
      Cons.showLoader();
      let newPassword = generatePassword();
      let email = ie.val();

      searchUser({login: email, mail: email}).then(result => {
        if(!result) throw new Error(`Пользователь с email "${email}" не найден в системе.`);

        return {errorCode: 0}; //changeCredentials(ie.val(), newPassword);
      }).then(res => {
        if(res.errorCode && res.errorCode != 0) throw new Error(res.errorMessage);
        return sendNotification({
          header: 'Сброс пароля',
          message: `Ваш пароль был сброшен.<br>Ваш новый пароль для входа <b>${newPassword}</b><br><br>Для перехода в систему перейдите по <a href="${window.location.origin}/kw">ссылке</a>`,
          emails: [email]
        });
      }).then(res => {
        if(res.errorCode && res.errorCode != 0) throw new Error(res.errorMessage);
        Cons.hideLoader();
        showMessage(localizedText('Новый пароль был отправлен вам на почту', 'Новый пароль был отправлен вам на почту', 'Новый пароль был отправлен вам на почту', 'Новый пароль был отправлен вам на почту'), 'error');
        fire({type: "goto_page", pageCode: "auth", pageParams: []}, "button_recovery");
      }).catch(error => {
        Cons.hideLoader();
        showMessage(error.message, 'error');
      });

    } catch (err) {
      console.log(err.message);
      Cons.hideLoader();
      showMessage(localizedText('Произошла ошибка при сбросе пароля', 'Произошла ошибка при сбросе пароля', 'Құпия сөзді қалпына келтіру кезінде қате пайда болды', 'Произошла ошибка при сбросе пароля'), 'error');
    }
  });

});
