const reqRegistry = ["hcm_skills", "hcm2_registry_competence", "hcm_registry_courseGroups", "hcm_registry_edu_instruction", "hcm_registry_trainingProfile", "hcm_registry_userCards", "hcm_registry_idp"];
const savedSettings = localStorage.k_settings;

if (!savedSettings) {
  const k_settings = {views: {kp: false, os: false}};
  localStorage.setItem('k_settings', JSON.stringify(k_settings));
  Cons.setAppStore({settings: k_settings});
} else {
  Cons.setAppStore({settings: JSON.parse(savedSettings)});
}

const fetchUserFullName = () => {
  let {lastname, firstname, patronymic} = AS.OPTIONS.currentUser;
  firstname = ' ' + firstname.charAt(0) + '.';
  patronymic = patronymic != '' ? ' ' + patronymic.charAt(0) + '.' : '';
  return lastname + firstname + patronymic;
}

const authContinue = userCardUUID => {
  Cons.setAppStore({userCardUUID: userCardUUID});

  // 2 - получаем данные из карточки пользователя
  updateUserCard();

  // 3 - проверяем наличие необходимых реестров для работы
  rest.synergyGet('api/registry/list', res => {
    const arr = [];

    reqRegistry.forEach(registry => {
      arr.push(res.find(item => item.regGroupName === 'LMS').consistOf.some(item => item.registryCode === registry));
    });

    if(arr.some(item => !item)) {
      Cons.hideLoader();
      Cookies.remove('lms-web');
      return showMessage(localizedText('В приложении недостаточно данных для запуска академии, обратитесь к менеджеру обучения!'), 'error');
    }

    Cons.hideLoader();
    fireFn.GOTO_PAGE({compCode: 'root-panel', pageCode: 'my_courses'});
  }, err => {
    Cons.hideLoader();
    Cookies.remove('lms-web');
    showMessage(localizedText('Ошибка авторизации', 'Ошибка авторизации', 'Ошибка авторизации'), 'error');
    console.error(err);
  });
}

const searchProfileCard = () => {
  const params = Qs.stringify({
    registryCode: 'hcm_registry_trainingProfile',
    field: 'hcm_form_position_choice',
    condition: 'CONTAINS',
    key: AS.OPTIONS.currentUser.positions[0].positionID,
    loadData: true,
    countInPart: 1,
    fields: 'hcm_form_profile_name'
  });
  return new Promise((resolve, reject) => {
    rest.synergyGet(`api/registry/data_ext?${params}`, res => {
      if(res.recordsCount == 0) reject('Не найдено профиля обучения');
      else resolve(res.result[0]);
    }, err => reject('Ошибка поиска профиля обучения'));
  });
}

const getUserCardAsfData = positionCard => {
  let asfData = [];
  let tableBlockIndex = 1;
  let competenceTable = {id: 'hcm_form_userCard_competenceTable', type: 'appendable_table', data: []};
  let booksTable = {id: 'hcm_form_userCard_books', type: 'appendable_table', data: []};
  let userCardAll = {id: 'hcm_form_userCard_all', type: 'numericinput', key: '0', value: '0'};

  asfData.push(competenceTable);
  asfData.push(booksTable);
  asfData.push(userCardAll);
  asfData.push({id: 'hcm_form_userCard_current', type: 'numericinput', key: '0', value: '0'});
  asfData.push({id: 'hcm_form_userCard_user', type: 'entity', key: AS.OPTIONS.currentUser.userid, value: fetchUserFullName()});
  asfData.push({
    id: 'hcm_form_userCard_department',
    type: 'entity',
    key: AS.OPTIONS.currentUser.positions[0].departmentID,
    value: AS.OPTIONS.currentUser.positions[0].departmentName
  });
  asfData.push({
    id: 'hcm_form_userCard_reglink',
    type: 'reglink',
    key: positionCard.documentID,
    value: positionCard.fieldValue.hcm_form_profile_name || 'Профиль обучения'
  });

  return new Promise(resolve => {
    rest.synergyGet(`api/asforms/data/${positionCard.dataUUID}`, posAsfData => {
      let positionBooks = posAsfData.data.find(x => x.id == 'hcm_form_position_books');
      let positionCompetence = posAsfData.data.find(x => x.id == 'hcm_form_position_competence_name');

      if(positionCompetence && positionCompetence.hasOwnProperty('key')) {
        let keys = positionCompetence.key.split(';');
        let values = positionCompetence.value.split(';');

        keys.forEach((docID, i) => {
          competenceTable.data.push({id: `hcm_form_userCard_competenceName-b${tableBlockIndex}`, type: 'reglink', key: docID, valueID: docID, value: values[i]});
          competenceTable.data.push({id: `hcm_form_userCard_status-b${tableBlockIndex}`, type: 'listbox', key: '0', value: 'Без статуса'});
          competenceTable.data.push({id: `hcm_form_userCard_finishDate-b${tableBlockIndex}`, type: 'date'});
          competenceTable.data.push({id: `hcm_form_userCard_result-b${tableBlockIndex}`, type: 'reglink'});
          tableBlockIndex++;
        });

        userCardAll.key = String(tableBlockIndex - 1);
        userCardAll.value = String(tableBlockIndex - 1);
      }

      if(positionBooks && positionBooks.hasOwnProperty('data')) booksTable.data = positionBooks.data;
      resolve(asfData);
    }, err => resolve(asfData));
  });
}

const createUserCard = positionCard => {
  return new Promise((resolve, reject) => {
    getUserCardAsfData(positionCard).then(asfData => {
      AS.FORMS.ApiUtils.simpleAsyncPost('rest/api/registry/create_doc_rcc', res => {
        res.errorCode != 0 ? reject(res.errorMessage) : resolve(res);
      }, null, JSON.stringify({
        registryCode: 'hcm_registry_userCards',
        data: asfData,
        sendToActivation: true
      }), "application/json; charset=utf-8", err => {
        console.log('create_doc_rcc error', err.responseJSON.errorMessage);
        reject('Ошибка создания карточки пользователя');
      });
    });
  });
}

const authMe = () => {
  const locale = $('.localeSelector').val();
  Cons.setAppStore({locale});

  if (AS.OPTIONS.currentUser.userid === '1') {
    Cookies.remove('lms-web');
    return showMessage(localizedText('Вход в систему недоступен!', 'Вход в систему недоступен!', 'Жүйеге кіру мүмкін емес!'), 'error');
  }

  // 1 - проверяем наличие карточки пользователя
  const params = Qs.stringify({
    loadData: true,
    registryCode: 'hcm_registry_userCards',
    field: 'hcm_form_userCard_user',
    condition: 'CONTAINS',
    key: AS.OPTIONS.currentUser.userid
  });

  Cons.showLoader();
  rest.synergyGet(`api/registry/data_ext?${params}`, res => {
    if (!res.recordsCount) {
      Cons.hideLoader();
      UIkit.modal.confirm('Отсутствует профиль Академии, создать профиль и продолжить?').then(() => {
        Cons.showLoader();

        searchProfileCard()
        .then(createUserCard)
        .then(userCard => authContinue(userCard.dataID))
        .catch(err => {
          Cons.hideLoader();
          Cookies.remove('lms-web');
          showMessage(localizedText('Ошибка при создании профиля', 'Ошибка при создании профиля', 'Ошибка при создании профиля'), 'error');
          console.log('%c' + err, 'color: red;');
        });

      }, () => {
        Cookies.remove('lms-web');
        return;
      });
    } else {
      authContinue(res.result[0].dataUUID);
    }
  }, err => {
    Cons.hideLoader();
    Cookies.remove('lms-web');
    showMessage(localizedText('Ошибка авторизации', 'Ошибка авторизации', 'Ошибка авторизации'), 'error');
    console.log('%c' + err, 'color: red;');
  });
}

pageHandler('auth', () => {
  const lmsWeb = Cookies.get("lms-web");
  const storedLocale = localStorage.locale || 'ru';

  fireFn.CHANGE_LOCALE_SELECTOR_VALUE({compCode: 'localeSelector', value: storedLocale});

  if(lmsWeb) {
    const basicParams = btoa(encodeURIComponent(decodeURIComponent(unescape(atob(lmsWeb)))));
    const authParams = decodeURIComponent(atob(basicParams)).split(':');
    Cons.login({login: authParams[0], password: authParams[1]});
  }

  if(!Cons.getAppStore().auth_page_listener) {
    addListener('auth_success', 'auth', authed => {
      const {login, password} = authed.creds;
      AS.apiAuth.setCredentials(login, password);
      AS.OPTIONS.currentUser = authed.data.person;
      Cons.setAppStore({userData: authed.data.person});
      Cons.setAppStore({creds: {login, password: btoa(password)}});
      Cookies.set('lms-web', btoa(escape(encodeURIComponent(login + ":" + password))), {expires: 5});
      authMe();
    });

    Cons.setAppStore({auth_page_listener: true});
  }
});
