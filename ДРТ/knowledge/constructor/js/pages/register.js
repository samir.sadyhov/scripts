const emailRegex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

//если поле не заполнено - выделить его как невалидное
const emptyValidator = input => {
  if(input.val() !== '') {
    input.removeClass('uk-form-danger');
    return true;
  } else {
    input.addClass('uk-form-danger');
    return false;
  }
}

//валидация введенного email
const emailValidator = input => {
  if(emptyValidator(input)) {
    if(emailRegex.test(input.val())) {
      input.removeClass('uk-form-danger');
      return true;
    } else {
      input.addClass('uk-form-danger');
      return false;
    }
  }
  return false;
}

const toTranslit = text => {
  return text.replace(/([а-яё])|([\s_-])|([^a-z\d])/gi,
    function (all, ch, space, words, i) {
      if (space || words) return space ? '_' : '';
      var code = ch.charCodeAt(0),
      index = code == 1025 || code == 1105 ? 0 :
      code > 1071 ? code - 1071 : code - 1039,
      t = ['yo', 'a', 'b', 'v', 'g', 'd', 'e', 'zh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o',
      'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'shch', '', 'y', '', 'e', 'yu', 'ya'];
      return t[index];
    });
}

const positionsAppoint = (positionID, userID) => {
  return new Promise(async resolve => {
    try {
      rest.synergyGet(`api/positions/appoint?positionID=${positionID}&userID=${userID}`, res => {
        if(res.errorCode == 0) {
          resolve(res);
        } else {
          console.log(res);
          resolve(null);
        }
      });
    } catch (e) {
      console.log(e);
      resolve(null);
    }
  });
}

const addUser = userData => {
  return new Promise(async resolve => {
    try {
      let data = {
        login: userData.email,
        password: userData.password,
        email: userData.email,
        lastname: userData.lastname,
        firstname: userData.firstname,
        pointersCode: toTranslit(`${userData.lastname}_${userData.firstname}`),
        isConfigurator: false,
        isAdmin: false,
        isChancellery: false
      };
      if(userData.patronymic) data.patronymic = userData.patronymic;

      rest.synergyPost('api/filecabinet/user/save', data, "application/x-www-form-urlencoded; charset=UTF-8", function (response) {
        if(response.errorCode == 0) {
          resolve(response);
        } else {
          console.log(response);
          resolve(null);
        }
      }, function (err) {
          console.error(err);
          resolve(null);
      });
    } catch (e) {
      console.log(e);
      resolve(null);
    }
  });
}

const addUserGroup = userID => {
  return new Promise(async resolve => {
    try {
      rest.synergyGet(`api/storage/groups/add_user?groupCode=${groupCode}&userID=${userID}`, res => resolve(res));
    } catch (e) {
      console.log(e);
      resolve(null);
    }
  });
}

const createDocRCC = (registryCode, asfData, sendToActivation = false) => {
  return new Promise(async resolve => {
    try {
      let settings = {
        url: `${window.location.origin}/Synergy/rest/api/registry/create_doc_rcc`,
        method: "POST",
        headers: {
          "Authorization": "Basic " + btoa(Cons.creds.login + ":" + Cons.creds.password),
          "Content-Type": "application/json; charset=utf-8"
        },
        data: JSON.stringify({
          registryCode: registryCode,
          data: asfData,
          sendToActivation: sendToActivation
        }),
      };
      $.ajax(settings).done(response => {
        if(response.errorCode == 0) {
          resolve(response);
        } else {
          console.log(response);
          resolve(null);
        }
      }).fail(err => {
        console.error(err);
        resolve(null);
      });
    } catch (e) {
      console.log(e);
      resolve(null);
    }
  });
}

const searchUser = param => {
  return new Promise((resolve, reject) => {
    try {
      rest.synergyGet(`api/filecabinet/user/checkExistence?${jQuery.param(param)}`, res => resolve(res.result == "true"));
    } catch (e) {
      reject(e);
    }
  });
}

const checkRows = () => {
  if(!emptyValidator($('#input_lastname'))) {
    showMessage(localizedText('Заполните фамилию', 'Заполните фамилию', 'Заполните фамилию'), 'error');
    return false;
  }

  if(!emptyValidator($('#input_firstname'))) {
    showMessage(localizedText('Заполните имя', 'Заполните имя', 'Заполните имя'), 'error');
    return false;
  }

  if(!emailValidator($('#input_login'))) {
    showMessage(localizedText('Заполните поле Логин (e-mail)', 'Заполните поле Логин (e-mail)', 'Заполните поле Логин (e-mail)'), 'error');
    return false;
  }

  if($('#input_password').val().length < 3) {
    $('#input_password').addClass('uk-form-danger');
    showMessage(localizedText('Минимальная длина пароля должна составлять 3 символа', 'Минимальная длина пароля должна составлять 3 символа', 'Минимальная длина пароля должна составлять 3 символа'), 'error');
    return false;
  }

  return true;
}

let regData = {
  lastname: '',
  firstname: '',
  login: '',
  password: ''
};

const registration = () => {
  console.log('||| registration |||', regData);

  searchUser({login: regData.login}).then(result => {
    if(result) throw new Error(`Пользователь с email "${regData.login}" найден в системе, дальнейшая регистрация не возможна`);
    console.log('registration continue...');
  }).catch(error => {
    showMessage(error.message, 'error');
  });
}

const initListeners = () => {
  let mailInput = $('#input_login');
  mailInput.off().on('input', e => {
    mailInput.val(mailInput.val().replace(/[^a-zA-Z\d.@\-_]/g,''));
    regData.login = mailInput.val();
    mailInput.removeClass('uk-form-danger');
  });

  $('#input_password').on('input', e => {
    $('#input_password').removeClass('uk-form-danger');
    regData.password = $('#input_password').val();
  });
  $('#input_lastname').on('input', e => {
    $('#input_lastname').removeClass('uk-form-danger');
    regData.lastname = $('#input_lastname').val();
  });
  $('#input_firstname').on('input', e => {
    $('#input_firstname').removeClass('uk-form-danger');
    regData.firstname = $('#input_firstname').val();
  });
}

pageHandler('registration', () => {
  initListeners();

  if(Cons.getAppStore().registration_page_listener) return;
  Cons.setAppStore({registration_page_listener: true});

  addListener("button_click", "button_registration", e => {
    if(!checkRows()) return;
    registration();
  });
});
