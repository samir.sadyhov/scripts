pageHandler('profile', () => {
    setTimeout(() => {
        fire({
            type: 'set_disabled',
            disabled: true
        }, 'buttonToProfile')
    }, 10)

    const {
        userCard,
        userData,
        mainCoursesTable
    } = Cons.getAppStore()

    const profileFieldsArr = [
        "hcm_form_userCard_reglink",
        "hcm_form_userCard_department",
        "hcm_form_userCard_manager",
        "hcm_form_userCard_office",
        "hcm_form_userCard_current",
        "hcm_form_userCard_all",
        "hcm_form_userCard_certified"
    ]

    const neededFields = userCard.filter(item => {
        return profileFieldsArr.includes(item.id)
    }).map(item => {
        return new Object({
            field: item.id,
            value: item.value
        })
    })

    const tmp = Object.values(mainCoursesTable);

    const certificateStatus = {
        current: tmp.filter(x => x.hcm_form_userCard_status.key == '2').length || 0,
        all: tmp.length,
        status: neededFields.find(item => item.field === 'hcm_form_userCard_certified').key
    }

    const COUNT_RU = `Пройдено курсов: ${certificateStatus.current} из ${certificateStatus.all}`
    const COUNT_KZ = `Курстар аяқталды: ${certificateStatus.all}-ден ${certificateStatus.current}`

    const STATUS_RU_SUCCESS = `${COUNT_RU} (пользователь сертифицирован)`
    const STATUS_RU_FAIL = `${COUNT_RU} (пользователь еще не сертифицирован)`
    const STATUS_KZ_SUCCESS = `${COUNT_KZ} (пайдаланушы сертификатталған)`
    const STATUS_KZ_FAIL = `${COUNT_KZ} (пайдаланушы әлі сертификатталмаған)`

    const STATUS_RU = certificateStatus.status ? STATUS_RU_SUCCESS : STATUS_RU_FAIL
    const STATUS_KZ = certificateStatus.status ? STATUS_KZ_SUCCESS : STATUS_KZ_FAIL

    setTimeout(() => {
        fireFn.CHANGE_LABEL({
            compCode: 'certificationStatus',
            text: localizedText(STATUS_RU, STATUS_RU, STATUS_KZ)
        })
    }, 10)

    const userProfileFields = [{
        field: 'user_field',
        value: `${userData.lastname} ${userData.firstname}`
    }, {
        field: 'title_field',
        value: neededFields.find(item => item.field === 'hcm_form_userCard_reglink').value
    }, {
        field: 'department_field',
        value: neededFields.find(item => item.field === 'hcm_form_userCard_department').value
    }, {
        field: 'office_field',
        value: neededFields.find(item => item.field === 'hcm_form_userCard_office').value
    }, {
        field: 'manager_field',
        value: neededFields.find(item => item.field === 'hcm_form_userCard_manager').value
    }]

    userProfileFields.map(item => fireFn.CHANGE_INPUT_VALUE({compCode: item.field, value: item.value}))
})

addListener('button_click', 'logout', () => {
    Cookies.remove('lms-web')
})
