const getDataByDocumentId = (docId, handler) => {
    rest.synergyGet(`api/formPlayer/getAsfDataUUID?documentID=${docId}`,
        dataUUID => {
            rest.synergyGet(`api/asforms/data/get?dataUUID=${dataUUID}`,
                res => handler(res[0].data.filter(item => item.type !== 'label')),
                err => console.error(err)
            )
        },
        err => console.error(err)
    )
}

const drawAllGroups = ({locale}) => {
    const {groupsInfo, settings} = Cons.getAppStore()
    const {os} = settings.views

    if (os) {
        fireFn.SHOW('groupsList')

        if ($('#groupsList').children().length === 1) {
            return
        }

        const container = jQuery('<ul>')
            .attr({
                'uk-accordion': true
            })
            .css({
                width: '80%'
            })

        groupsInfo.forEach(item => {
            const {coursesCount, name, name_kz, description, description_kz, courses} = item
            const [groupName, groupDescription] = [
                (locale === 'ru' ? name : (name_kz ?? name)),
                (locale === 'ru' ? description : (description_kz ?? description))
            ]

            const fullTitle = `${groupName} (${groupDescription}) - ${coursesCount}`

            const groupContainer = jQuery('<li>')
                .attr({
                    class: 'groupContainer'
                })
                .css({
                    padding: 10,
                    backgroundColor: 'rgba(248,248,255,0.6)',
                    borderRadius: 10
                })

            const title = jQuery('<a>').attr({
                class: 'uk-accordion-title'
            }).html(fullTitle)

            const content = jQuery('<div>').attr({
                class: 'uk-accordion-content content'
            })

            courses.forEach(item => {
                const {uuid, name, name_kz} = item
                const courseName = (locale === 'ru' ? name : (name_kz ?? name))

                const listItem = jQuery('<div>')
                    .attr({
                        uuid,
                        class: 'listItem'
                    })
                    .html(courseName)
                    .click(() => {
                        fireFn.GOTO_PAGE({
                            compCode: 'groupsList',
                            pageCode: 'course_page',
                            pageParams: [{
                                pageParamName: 'uuid',
                                value: uuid
                            }]
                        })
                    })

                $(content).append(listItem)
            })

            groupContainer.append(title, content)
            container.append(groupContainer)
        })

        $('#groupsList').append(container)
    } else {
        fireFn.SHOW('groupCards')

        const groups = []

        groupsInfo.forEach(item => {
            const {uuid, image, coursesCount, name, name_kz, description, description_kz} = item

            groups.push({
                groupName: (locale === 'ru' ? name : (name_kz ?? name)),
                groupDescription: (locale === 'ru' ? description : (description_kz ?? description)),
                groupCourseCount: coursesCount,
                uuid: uuid,
                groupImage: image
            })
        })

        fireFn.CHANGE_REPEATER_CUSTOM_SOURCE({compCode: 'allGroupsRepeater', data: groups})
    }

    Cons.hideLoader()
}

const getAllCourses = () => {
    try {
        const params = Qs.stringify({
            registryCode: 'hcm_registry_courseGroups',
            filterCode: 'access_to_course'
        });

        rest.synergyGet(`api/registry/data_ext?${params}`,
            res => {
                const groupsPromiseArr = []

                res.result.forEach(item => {
                    groupsPromiseArr.push(new Promise(resolve => {
                        const group = {}
                        const {dataUUID, fieldValue} = item

                        rest.synergyGet(`api/asforms/data/get?dataUUID=${dataUUID}`,
                            res => {
                                const responseData = res[0].data
                                const courseGroupListUUID = responseData.find(item => item.id === 'hcm_form_courseGroup_listUUID').value

                                if (!courseGroupListUUID) {
                                    return resolve(null)
                                }

                                const coursesIds = JSON.parse(courseGroupListUUID)
                                const coursesPromiseArr = []

                                group.uuid = dataUUID
                                group.name = fieldValue.hcm_form_courseGroup_name
                                group.description = fieldValue.hcm_form_courseGroup_description
                                group.coursesCount = fieldValue.hcm_form_courseGroup_number
                                group.image = responseData.find(item => item.id === 'hcm_form_courseGroup_image').key
                                group.name_kz = responseData.find(item => item.id === 'hcm_form_courseGroup_name_kz')?.value
                                group.description_kz = responseData.find(item => item.id === 'hcm_form_courseGroup_description_kz')?.value

                                coursesIds.forEach(item => {
                                    coursesPromiseArr.push(new Promise(resolve => {
                                        rest.synergyGet(`api/asforms/data/get?dataUUID=${item}`,
                                            res => {
                                                const responseData = res[0].data
                                                const competenceIsCompleted = responseData.find(item => item.id === 'hcm_form_compitience_status').key === '2'

                                                if (!competenceIsCompleted) {
                                                    return resolve(null)
                                                }

                                                const course = {}

                                                course.uuid = item
                                                course.name = responseData.find(item => item.id === 'hcm_form_compitience_pasport').value
                                                course.key = responseData.find(item => item.id === 'hcm_form_compitience_pasport').key
                                                course.author = responseData.find(item => item.id === 'hcm_form_compitience_author').value
                                                course.image = responseData.find(item => item.id === 'hcm_form_compitience_image').key

                                                getDataByDocumentId(course.key, (dataArr) => {
                                                    const code = dataArr.find(item => item.id === 'hcm_form_skill_code').value
                                                    const name_kz = dataArr.find(item => item.id === 'hcm_form_skill_name_kz')?.value

                                                    course.name_kz = (name_kz ? (`${code}-${name_kz}`) : null)

                                                    resolve(course)
                                                })
                                            },
                                            err => console.error(err)
                                        )
                                    }))
                                })

                                Promise.all(coursesPromiseArr).then(courses => {
                                    group.courses = courses.filter(Boolean)

                                    resolve(group)
                                })
                            },
                            err => console.error(err)
                        )
                    }))
                })

                Promise.all(groupsPromiseArr).then(groups => {
                    Cons.setAppStore({
                        groupsInfo: groups.filter(Boolean)
                    })
                })
            },
            err => console.error(err)
        )
    } catch (e) {
        console.error('error in allCourses.js/getAllCourses()')
    }
}

const showAllCourses = (newLocale = null) => {
    Cons.showLoader()

    const {groupsInfo, locale} = Cons.getAppStore()
    const currentLocale = newLocale ?? locale

    groupsInfo?.length && drawAllGroups({locale: currentLocale})

    if (!groupsInfo?.length) {
        getAllCourses()

        const interval = setInterval(() => {
            const {groupsInfo} = Cons.getAppStore()

            if (groupsInfo?.length) {
                drawAllGroups({locale: currentLocale})

                clearInterval(interval)
            }
        }, 1000)
    }
}

pageHandler('all_courses', () => {
    Cons.setAppStore({
        prevPage: 'all_courses'
    })

    showAllCourses();

    if(!Cons.getAppStore().value_changed_locale_selector_all_courses) {
      addListener('value_changed_locale_selector', 'localeSelector', (e) => {
        $('#groupsList').empty();
        showAllCourses(e.value);
      });
      Cons.setAppStore({value_changed_locale_selector_all_courses: true});
    }

    $('.os').off('click')
    $('.os').click(e => {
        const newValue = e.target.checked

        fireFn.HIDE('allGroupsRepeater')
        $('#groupsList').empty()

        changeViewStore('os', newValue)

        $('.kp').prop('disabled', $('.os').is(':checked'))

        showAllCourses()
    })
})
