const SET_HIDDEN = 'set_hidden'

this.fireFn = {
    HIDE: compCode => {
        fire({
            type: SET_HIDDEN,
            hidden: true
        }, compCode)
    },
    SHOW: compCode => {
        fire({
            type: SET_HIDDEN,
            hidden: false
        }, compCode)
    },
    CHANGE_REPEATER_CUSTOM_SOURCE: ({compCode, data}) => {
        fire({
            type: 'change_repeater_custom_source',
            customSource: data
        }, compCode)
    },
    CHANGE_REPEATER_DATA_ID: ({compCode, uuid}) => {
        fire({
            type: 'change_repeater_data_id',
            dataId: uuid
        }, compCode)
    },
    GOTO_PAGE: ({compCode, pageCode, pageParams = []}) => {
        fire({
            type: 'goto_page',
            pageCode: pageCode,
            pageParams: pageParams
        }, compCode)
    },
    CHANGE_LOCALE_SELECTOR_VALUE: ({compCode, value}) => {
        fire({
            type: 'locale_selector_value_change',
            value: value
        }, compCode)
    },
    CHANGE_INPUT_VALUE: ({compCode, value}) => {
        fire({
            type: 'input_default_change',
            defaultValue: value
        }, compCode)
    },
    DISABLE: compCode => {
        fire({
            type: 'set_disabled',
            disabled: true
        }, compCode)
    },
    ACTIVATE: compCode => {
        fire({
            type: 'set_disabled',
            disabled: false
        }, compCode)
    },
    CHANGE_LABEL: ({compCode, text}) => {
        fire({
            type: 'change_label',
            text: text
        }, compCode)
    },
    SHOW_FORM: ({compCode, dataID}) => {
        fire({
            type: 'show_form_data',
            dataId: dataID
        }, compCode)
    },
    INPUT_HIGHLIGHT: ({compCode, isError}) => {
        fire({
            type: 'input_highlight',
            error: isError
        }, compCode)
    },
    VIDEO_CHANGE_URL: ({compCode, value}) => {
        fire({
            type: 'video_change_url',
            url: value
        }, compCode)
    },
    VIDEO_CHANGE_STORE_ID: ({compCode, value}) => {
        fire({
            type: 'video_change_store_id',
            identifier: value
        }, compCode)
    }
}
