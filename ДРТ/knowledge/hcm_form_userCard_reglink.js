const cmp = {
  name: 'hcm_form_userCard_competenceName',
  result: 'hcm_form_userCard_result',
  status: 'hcm_form_userCard_status',
  finishDate: 'hcm_form_userCard_finishDate',
  table: 'hcm_form_userCard_competenceTable',
  tableOther: 'hcm_form_userCard_competenceTable_other'
}

const createField = fieldData => {
  let field = {};
  for (let key in fieldData) field[key] = fieldData[key];
  return field;
}

const getValue = (data, cmp) => {
  data = data.data ? data.data : data;
  return data.find(x => x.id === cmp) || null;
}

const setValue = (asfData, cmpID, data) => {
  let field = getValue(asfData, cmpID);
  if(field) {
    for (let key in data) {
      if(key === 'id' || key === 'type') continue;
      field[key] = data[key];
    }
    return field;
  } else {
    asfData = asfData.data ? asfData.data : asfData;
    field = createField(data);
    field.id = cmpID;
    asfData.push(field);
    return field;
  }
}

const getTableBlockIndex = (data, cmp) => {
  let res = 0;
  data = data.data ? data.data : data;
  data.forEach(item => {
    if (item.id.slice(0, item.id.indexOf('-b')) === cmp) res++;
  });
  return res === 0 ? 1 : ++res;
}

const parseCompetenceTable = (table, other) => {
  let result = [];
  if(table.hasOwnProperty('data')){
    let tableLength = getTableBlockIndex(table, cmp.name + (other ? '_other' : ''));
    for(let i = 1; i < tableLength; i++) {
      if(getValue(table, cmp.name + (other ? '_other-b' : '-b') + i))
      result.push({
        name: getValue(table, cmp.name + (other ? '_other-b' : '-b') + i),
        result: getValue(table, cmp.result + (other ? '_other-b' : '-b') + i),
        status: getValue(table, cmp.status + (other ? '_other-b' : '-b') + i),
        finishDate: getValue(table, cmp.finishDate + (other ? '_other-b' : '-b') + i)
      });
    }
  }
  return result;
}

const getUserCompetence = () => {
  let skills = parseCompetenceTable(model.playerModel.getModelWithId(cmp.table).getAsfData()[0]);
  let otherSkills = parseCompetenceTable(model.playerModel.getModelWithId(cmp.tableOther).getAsfData()[0], true);
  return skills.concat(otherSkills);
}

const getPositionCompetence = asfData => {
  let result = null;
  let data = getValue(asfData, 'hcm_form_position_competence_name');
  if(data && data.hasOwnProperty('key')) {
    result = [];
    let keys = data.key.split(';');
    let values = data.value.split(';');
    keys.forEach((key, i) => result.push({key: key, value: values[i]}));
  }
  return result;
}

const getResultTable = (positionCompetence, userCompetence) => {
  let table = {id: cmp.table, type: 'appendable_table', data: []};
  let tableOther = {id: cmp.tableOther, type: 'appendable_table', data: []};
  let otherCompetence = [];
  let tbi = 1;

  userCompetence.forEach(item => {
    let pos = positionCompetence.filter(x => x.key == item.name.key);
    if(!pos.length) otherCompetence.push(item);
  });

  positionCompetence.forEach(item => {
    let competence = userCompetence.filter(x => x.name.key == item.key);
    if(competence && competence.length) {
      competence = competence[0];
      setValue(table, cmp.name + '-b' + tbi, competence.name);
      setValue(table, cmp.result + '-b' + tbi, competence.result);
      setValue(table, cmp.status + '-b' + tbi, competence.status);
      setValue(table, cmp.finishDate + '-b' + tbi, competence.finishDate);
    } else {
      setValue(table, cmp.name + '-b' + tbi, {
        type: 'reglink',
        value: item.value,
        key: item.key,
        valueID: item.key
      });
      setValue(table, cmp.result + '-b' + tbi, {type: 'reglink'});
      setValue(table, cmp.status + '-b' + tbi, {type: 'listbox', value: "Без статуса", key: "0"});
      setValue(table, cmp.finishDate + '-b' + tbi, {type: 'date'});
    }
    tbi++;
  });

  tbi=1;
  otherCompetence.forEach(item => {
    if(item.result.hasOwnProperty('key')) {
      setValue(tableOther, cmp.name + '_other-b' + tbi, item.name);
      setValue(tableOther, cmp.result + '_other-b' + tbi, item.result);
      setValue(tableOther, cmp.status + '_other-b' + tbi, item.status);
      setValue(tableOther, cmp.finishDate + '_other-b' + tbi, item.finishDate);
      tbi++;
    }
  });

  return {competenceTable: table, otherCompetenceTable: tableOther};
}

const changeUserCompetence = profileDocID => {
  if(!profileDocID) return;
  try {
    let userCompetence = getUserCompetence();
    let tableBooks;

    AS.FORMS.ApiUtils.getAsfDataUUID(profileDocID)
    .then(AS.FORMS.ApiUtils.loadAsfData)
    .then(positionAsfData => {
      tableBooks = getValue(positionAsfData, 'hcm_form_position_books');
      tableBooks.id = 'hcm_form_userCard_books';
      return getPositionCompetence(positionAsfData);
    })
    .then(positionCompetence => getResultTable(positionCompetence, userCompetence))
    .then(tables => {

      model.playerModel.getModelWithId(cmp.table).setAsfData(tables.competenceTable);
      model.playerModel.getModelWithId(cmp.tableOther).setAsfData(tables.otherCompetenceTable);
      model.playerModel.getModelWithId('hcm_form_userCard_books').setAsfData(tableBooks);

    }).catch(e => {
      console.log('ERROR: ' + e.message);
    });
  } catch (e) {
    console.log('ERROR: ' + e.message);
  }
}

if(editable) {
  setTimeout(() => {
    let selectedValue = model.getValue();
    model.on('valueChange', (_1, _2, value) => {
      if(selectedValue != value) {
        selectedValue = value;
        changeUserCompetence(value);
      }
    });
  }, 500);
}
