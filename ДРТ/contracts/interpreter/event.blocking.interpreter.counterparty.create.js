var result = true;
var message = 'ok';

let cabinetHost = 'http://192.168.4.143:8080/contracts/';

function valid_IIN_BIN(iin_bin) {
  let s = 0;
  for (let i = 0; i < 11; i++) s = s + (i + 1) * iin_bin[i];
  let k = s % 11;
  if (k === 10) {
    s = 0;
    for (let i = 0; i < 11; i++) {
      let t = (i + 3) % 11;
      if (t === 0) t = 11;
      s = s + t * iin_bin[i];
    }
    k = s % 11;
    if (k === 10) return false;
    return (k === Number(iin_bin.substring(11, 12)));
  }
  return (k === Number(iin_bin.substring(11, 12)));
}

function createNewUser(params) {
  let client = API.getHttpClient();
  let post = new org.apache.commons.httpclient.methods.PostMethod("http://127.0.0.1:8080/Synergy/rest/api/filecabinet/user/save");
  for(let key in params) post.addParameter(key, params[key]);
  post.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=utf-8");
  let resp = client.executeMethod(post);
  resp = JSON.parse(post.getResponseBodyAsString());
  post.releaseConnection();
  return resp;
}

function searchPosition(pointer_code){
  let res = API.httpGetMethod("rest/api/positions/search?pointer_code=" + pointer_code);
  return res.length ? res[0] : null;
}

function searchUserCode(pointer_code){
  let res = API.httpGetMethod("rest/api/filecabinet/user/checkExistence?code=" + pointer_code);
  if(res.errorCode == '0' && res.result == 'true') {
    return true;
  } else {
    return false;
  }
}

function searchUserLogin(login){
  let res = API.httpGetMethod("rest/api/filecabinet/user/checkExistence?login=" + login);
  if(res.errorCode == '0' && res.result == 'true') {
    return true;
  } else {
    return false;
  }
}

function getRandomInt(max) {
  return Math.floor(Math.random() * max);
}

function generatePassword() {
  let charSet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-+_!#$%^';
  return getRandomInt(9) + Array.apply(null, Array(6)).map(function() {
    return charSet.charAt(Math.random() * charSet.length);
  }).join('') + getRandomInt(9);
}

function getFullName(lastname, firstname, patronymic) {
  firstname = firstname ? ' ' + firstname.charAt(0) + '.' : '';
  patronymic = patronymic ? ' ' + patronymic.charAt(0) + '.' : '';
  return lastname + firstname + patronymic;
}

try {
  let currentFormData = API.getFormData(dataUUID);
  let iin = UTILS.getValue(currentFormData, 'workflow_form_client_iin');
  let textbox_email = UTILS.getValue(currentFormData, 'workflow_textbox_email');
  let lastname = UTILS.getValue(currentFormData, 'workflow_textbox_signer_lastname_nominative');
  let firstname = UTILS.getValue(currentFormData, 'workflow_textbox_signer_firstname_nominative');
  let patronymic = UTILS.getValue(currentFormData, 'workflow_textbox_signer_patronymic_nominative');

  if(!iin || !iin.hasOwnProperty('value')) throw new Error('Не указан ИИН');
  if(!valid_IIN_BIN(iin.value)) throw new Error('Введен некорректный ИИН');

  if(!lastname || !lastname.hasOwnProperty('value') || lastname.value == '') throw new Error('Не заполнена фамилия пользователя');
  if(!firstname || !firstname.hasOwnProperty('value') || firstname.value == '') throw new Error('Не заполнено имя пользователя');
  if(!textbox_email || !textbox_email.hasOwnProperty('value')) throw new Error('Не заполнен email пользователя');

  if(searchUserCode('IIN' + iin.value)) throw new Error('Пользователь с таким кодом (IIN'+iin.value+') уже существует в системе');
  if(searchUserLogin(textbox_email.value)) throw new Error('Пользователь с таким логином ('+textbox_email.value+') уже существует в системе');

  let positionID = searchPosition('counterparties');
  if(!positionID) throw new Error('Не найдена должность для назначения, код: "counterparties"');

  if(!patronymic || !patronymic.hasOwnProperty('value') || patronymic.value == '') {
    patronymic = null;
  } else {
    patronymic = patronymic.value;
  }

  let userPassword = generatePassword();

  //создание юзера
  let user = createNewUser({
    lastname: lastname.value,
    firstname: firstname.value,
    patronymic: patronymic || '',
    login: textbox_email.value,
    password: userPassword,
    pointersCode: 'IIN' + iin.value,
    hasAccess: true,
    hasPrivateFolder: true,
    isConfigurator: false,
    isAdmin: false,
    isChancellery: false
  });

  if(user.errorCode == '13') throw new Error(user.errorMessage);
  message = user.errorMessage;

  //Добавление юзера в группу
  let apiRes = API.httpGetMethod('rest/api/storage/groups/add_user?groupCode=workflow_group_counterparties&userID=' + user.userID);
  message += '\n\nДобавление в группу: ' + apiRes.message;

  //Назначение на должность
  apiRes = API.httpGetMethod('rest/api/positions/appoint?positionID=' + positionID + '&userID=' + user.userID);
  message += '\n\nНазначение на должность: ' + apiRes.errorMessage;

  let mergeAsfData = [];
  mergeAsfData.push({
    id: 'workflow_form_user',
    type: 'entity',
    value: getFullName(lastname.value, firstname.value, patronymic),
    key: user.userID
  });

  mergeAsfData.push({
    id: 'workflow_textbox_email_prev',
    type: 'textbox',
    value: textbox_email.value
  });

  //сохранение данных на форме
  API.mergeFormData({
    uuid: dataUUID,
    data: mergeAsfData
  });

  let emailMessage = 'Сіз үшін "Arta Synergy Workflow" жүйесінде тіркелу жасалды. Жүйеге өту үшін келесі сілтеме арқылы өтуге болады (<a href="' + cabinetHost + '">' + cabinetHost + '</a>).';
  emailMessage += '<br>Сіздің логиніңіз ' + textbox_email.value + '<br>Құпия сөзіңіз ' + userPassword;
  emailMessage += '<br><br>Для вас была создана учетная запись в системе "Arta Synergy Workflow". Для перехода в систему можете перейти по следующей ссылке <a href="' + cabinetHost + '">' + cabinetHost + '</a>.';
  emailMessage += '<br>Ваш логин ' + textbox_email.value + '<br>Ваш пароль ' + userPassword;

  // отправка письма
  apiRes = API.sendNotification({
    header: 'Arta Synergy Workflow жүйесінде тіркелу туралы хабарлама / Уведомление о создание учетной записи в системе Arta Synergy Workflow',
    message: emailMessage,
    emails: [textbox_email.value]
  });

  if(apiRes.errorCode != 0) {
    message += '\n\nПроизошла ошибка отправки уведомления';
  } else {
    message += '\n\nУведомление успешно отправлено';
  }

} catch (err) {
  result = false;
  message = err.message;
}
