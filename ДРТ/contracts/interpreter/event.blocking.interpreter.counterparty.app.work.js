var result = true;
var message = 'ok';

function modifyDoc(uuid){
  return API.httpGetMethod("rest/api/registry/modify_doc?dataUUID=" + uuid);
}

try {
  let currentFormData = API.getFormData(dataUUID);
  let appAsfData = [];
  let matching = [];

  let contract_client = UTILS.getValue(currentFormData, 'workflow_form_contract_client');

  if(!contract_client || !contract_client.hasOwnProperty('key')) throw new Error('Не указан профль контрагента');

  UTILS.setValue(appAsfData, 'workflow_form_my_doc_clientLink', contract_client);

  matching.push({from: 'workflow_form_contract_regnumber', to: 'workflow_form_my_doc_num'}); //№ договора
  matching.push({from: 'workflow_form_contract_regdate', to: 'workflow_form_my_doc_date'}); //Дата договора
  matching.push({from: 'workflow_form_contract_finish_date', to: 'workflow_form_my_doc_finish_date'}); //Срок действия договора
  // matching.push({from: '', to: 'workflow_form_my_doc_subject'}); //Предмет договора

  appAsfData.push({
    id: "workflow_form_my_doc_dataUUID",
    type: "textbox",
    value: dataUUID
  });

  appAsfData.push({
    id: "workflow_form_my_doc_status",
    type: "listbox",
    key: '3',
    value: 'В работу'
  });

  appAsfData.push({
    id: "workflow_form_my_doc_isnew",
    type: "listbox",
    key: '1',
    value: 'Да'
  });

  matching.forEach(function(id) {
    let fromData = UTILS.getValue(currentFormData, id.from);
    if(fromData) UTILS.setValue(appAsfData, id.to, fromData);
  });

  let clientAsfData = API.getFormData(API.getAsfDataId(contract_client.key));
  let textbox_bin = UTILS.getValue(clientAsfData, 'workflow_textbox_bin');
  let user = UTILS.getValue(clientAsfData, 'workflow_form_user');

  UTILS.setValue(appAsfData, 'workflow_form_my_doc_clientLink', contract_client);

  if(textbox_bin && textbox_bin.hasOwnProperty('value')) {
    UTILS.setValue(appAsfData, 'workflow_form_my_doc_clientBIN', textbox_bin);
  }

  if(user && user.hasOwnProperty('key')) {
    UTILS.setValue(appAsfData, 'workflow_form_my_doc_user', user);
  }

  let urlSearch = 'rest/api/registry/data_ext?registryCode=workflow_registry_my_docs&loadData=false';
  urlSearch += '&field=workflow_form_my_doc_dataUUID&condition=TEXT_EQUALS&value=' + dataUUID;

  let res = API.httpGetMethod(urlSearch);

  if((res.hasOwnProperty('errorCode') && res.errorCode != '0') || res.count == 0) {
    let createDocResult = API.httpPostMethod("rest/api/registry/create_doc_rcc", {
      registryCode: 'workflow_registry_my_docs',
      data: appAsfData,
      sendToActivation: true
    }, "application/json; charset=utf-8");

    if(createDocResult.errorCode != 0) throw new Error("Не удалось создать документ.\n" + createDocResult.errorMessage);

    message = "Запись создана\ndocumentID: " + createDocResult.documentID;
  } else {
    API.mergeFormData({uuid: res.data[0].dataUUID, data: appAsfData});
    modifyDoc(res.data[0].dataUUID);

    message = "Запись обновлена\ndocumentID: " + res.data[0].documentID;
  }

} catch (err) {
  result = false;
  message = err.message;
}
