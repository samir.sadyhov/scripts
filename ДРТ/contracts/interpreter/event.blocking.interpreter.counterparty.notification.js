var result = true;
var message = 'ok';

let cabinetHost = 'http://192.168.4.143:8080/contracts/';

function getEmailMsg(status, uuid){
  switch (status) {
    case '2':
      return {
        header: 'Arta Synergy Workflow жүйесінде құжатқа қол қою қажеттілігі туралы хабарлама / Уведомление о необходимости подписания документа в системе Arta Synergy Workflow',
        body: 'Қол қою үшін жаңа құжат алдыңыз. Жүйеге кіру үшін келесі сілтеме (<a href="' + cabinetHost + '?dataUUID=' + uuid + '">' + cabinetHost + '</a>) арқылы өтуге болады<br><br>Вам поступил новый документ для подписания. Для перехода в систему можете перейти по следующей ссылке (<a href="' + cabinetHost + '?dataUUID=' + uuid + '">' + cabinetHost + '</a>).'
      }
      break;
    case '3':
      return {
        header: 'Arta Synergy Workflow жүйесіндегі жаңа құжат туралы хабарландыру / Уведомление о новом документе в системе Arta Synergy Workflow',
        body: 'Сіз өңдеу үшін жаңа құжат алдыңыз. Жүйеге кіру үшін келесі сілтеме (<a href="' + cabinetHost + '?dataUUID=' + uuid + '">' + cabinetHost + '</a>) арқылы өтуге болады<br><br>Вам поступил новый документ для редактирования. Для перехода в систему можете перейти по следующей ссылке (<a href="' + cabinetHost + '?dataUUID=' + uuid + '">' + cabinetHost + '</a>)'
      }
      break;
    case '4':
      return {
        header: 'Arta Synergy Workflow жүйесіндегі құжатпен танысу қажеттілігі туралы хабарлама / Уведомление о необходимости ознакомления с документом в системе Arta Synergy Workflow',
        body: 'Қарау үшін жаңа құжат алдыңыз. Жүйеге кіру үшін келесі сілтеме (<a href="' + cabinetHost + '?dataUUID=' + uuid + '">' + cabinetHost + '</a>) арқылы өтуге болады<br><br>Вам поступил новый документ для ознакомления. Для перехода в систему можете перейти по следующей ссылке (<a href="' + cabinetHost + '?dataUUID=' + uuid + '">' + cabinetHost + '</a>)'
      }
      break;
    default:
      return null;
  }
}

try {
  let currentFormData = API.getFormData(dataUUID);
  let clientLink = UTILS.getValue(currentFormData, 'workflow_form_my_doc_clientLink');
  let contractDataUUID = UTILS.getValue(currentFormData, 'workflow_form_my_doc_dataUUID');
  let docStatus = UTILS.getValue(currentFormData, 'workflow_form_my_doc_status');

  if(!docStatus || !docStatus.hasOwnProperty('key')) throw new Error('Уведомление не отправлено, статус документа не определен');

  if(!clientLink || !clientLink.hasOwnProperty('key')) throw new Error('Не указан профль контрагента');

  if(!contractDataUUID || !contractDataUUID.hasOwnProperty('value') || contractDataUUID.value == '') {
    throw new Error('Не найдена ссылка на договор');
  } else {
    contractDataUUID = contractDataUUID.value;
  }

  let msg = getEmailMsg(docStatus.key, contractDataUUID);
  if(!msg) throw new Error('Уведомление не отправлено, статус документа ' + docStatus.value);

  let clientAsfData = API.getFormData(API.getAsfDataId(clientLink.key));
  let textbox_email = UTILS.getValue(clientAsfData, 'workflow_textbox_email');
  let textbox_email_prev = UTILS.getValue(clientAsfData, 'workflow_textbox_email_prev');

  if(!textbox_email || !textbox_email.hasOwnProperty('value')) throw new Error('Не заполнен email пользователя в карточке контрагента');

  let userEmail = textbox_email.value;
  if(textbox_email_prev && textbox_email_prev.hasOwnProperty('value') && textbox_email_prev.value != '') {
    if(textbox_email.value != textbox_email_prev.value) userEmail = textbox_email_prev.value;
  }

  // отправка письма
  let apiRes = API.sendNotification({
    header: msg.header,
    message: msg.body,
    emails: [userEmail]
  });

  if(apiRes.errorCode != 0) {
    throw new Error('Произошла ошибка отправки уведомления');
  } else {
    message = 'Уведомление успешно отправлено';
  }

} catch (err) {
  message = err.message;
}
