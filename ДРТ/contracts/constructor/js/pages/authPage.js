const addLog = async data => {
  return new Promise(async resolve => {
    AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/logger/log", resolve, 'json', JSON.stringify(data), "application/json; charset=UTF-8", resolve);
  });
}

const getUrlParameter = param => {
  const url = new URLSearchParams(window.location.search);
  return url.has(param) ? url.get(param) : null;
}

const getAuthParam = () => {
  const {defaultUser, defaultUserPassword} = Cons.getCurrentApp();
  return "Basic " + btoa(unescape(encodeURIComponent(`${defaultUser}:${defaultUserPassword}`)));
}

const searchCounterparty = async userID => {
  let param = `registryCode=workflow_registry_clients`;
  param += `&field=workflow_form_user&condition=TEXT_EQUALS&key=${userID}`;
  param += `&registryRecordStatus=STATE_SUCCESSFUL&loadData=false`;

  return new Promise(async resolve => {
    try {
      const url = `../Synergy/rest/api/registry/data_ext?${param}`;
      const response = await fetch(url, {method: 'GET', headers: {"Authorization": getAuthParam()}});

      if(!response.ok) throw new Error(`HTTP: ${response.status}, message: ${response.statusText} [ ${response.text()} ]`);
      resolve(response.json());
    } catch (err) {
      console.log(`ERROR [ searchCounterparty ]: ${err.message}`);
      resolve(null);
    }
  });
}

pageHandler('authPage', () => {
  if (!Cons.getAppStore().auth_page_listener) {
    addListener('auth_success', 'buttonLogin', async authed => {
      const {login, password} = authed.creds;

      const searchResult = await searchCounterparty(authed.data.person.userid);

      if(searchResult && searchResult.count != 0) {
        Cons.creds.login = login;
        Cons.creds.password = password;
        AS.apiAuth.setCredentials(login, password);
        AS.OPTIONS.login = login;
        AS.OPTIONS.password = password;
        AS.OPTIONS.currentUser = authed.data.person;

        Cons.setAppStore({userGroups: authed.data.groups});

        const resultLog = await addLog({
          "providerId": "AI_SEC",
          "eventId": "2005",
          "userId": AS.OPTIONS.currentUser.userid,
          "objects": [AS.OPTIONS.login, "Contracts"]
        });

        const systemSettings = await appAPI.getSystemSettings();
        Cons.setAppStore({systemSettings});

        const dataUUID = getUrlParameter('dataUUID');
        if(dataUUID) {
          Cons.setAppStore({dataUUID});
          fire({type: 'goto_page', pageCode: 'formPlayerPage'}, 'root-panel');
        } else {
          Cons.setAppStore({dataUUID: null});
          fire({type: 'goto_page', pageCode: 'mainPage'}, 'root-panel');
        }
      } else {
        const msg = {
          ru: 'Пользователь не является контрагентом',
          kk: 'Пайдаланушы контрагент емес',
          en: 'The user is not a counterparty'
        };

        UIkit.modal.alert(msg[AS.OPTIONS.locale]).then(function () {
          Cons.setAppStore({dataUUID: null});
          Cons.logout();
        });
      }

    });

    Cons.setAppStore({auth_page_listener: true});
  }
});
