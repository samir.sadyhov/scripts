const menuItems = [];

menuItems.push({
  id: 'menu_item_filter_all',
  name: {
    ru: 'Все',
    kk: 'Бәрі',
    en: 'All'
  },
  icon: 'list_alt',
  filterCode: 'workflow_registry_my_docs_filter_all'
});

menuItems.push({
  id: 'menu_item_filter_unsigned',
  name: {
    ru: 'На подпись',
    kk: 'Қол қоюға',
    en: 'For signature'
  },
  icon: 'pending_actions',
  filterCode: 'workflow_registry_my_docs_filter_unsigned'
});

menuItems.push({
  id: 'menu_item_filter_work',
  name: {
    ru: 'В работу',
    kk: 'Жұмысқа оралу',
    en: 'To work'
  },
  icon: 'work',
  filterCode: 'workflow_registry_my_docs_filter_work'
});

menuItems.push({
  id: 'menu_item_filter_forinfo',
  name: {
    ru: 'Ознакомление',
    kk: 'Ақпарат үшін',
    en: 'Familiarization'
  },
  icon: 'info_outline',
  filterCode: 'workflow_registry_my_docs_filter_forinfo'
});

menuItems.push({
  id: 'menu_item_filter_signed',
  name: {
    ru: 'Подписан',
    kk: 'Қол қойды',
    en: 'Signed'
  },
  icon: 'done_all',
  filterCode: 'workflow_registry_my_docs_filter_signed'
});

menuItems.push({
  id: 'menu_item_filter_declined',
  name: {
    ru: 'Отклонен',
    kk: 'Қабылданбады',
    en: 'Rejected'
  },
  icon: 'remove_done',
  filterCode: 'workflow_registry_my_docs_filter_declined'
});

menuItems.push({
  id: 'menu_item_filter_completed',
  name: {
    ru: 'Завершенные',
    kk: 'Аяқталған',
    en: 'Completed'
  },
  icon: 'check_circle_outline',
  filterCode: 'workflow_registry_my_docs_filter_completed'
});

const getCountRow = async (filterCode, key) => {
  let param = `?registryCode=workflow_registry_my_docs&filterCode=${filterCode}`;

  if(key) param += `&field=workflow_form_my_doc_isnew&condition=TEXT_EQUALS&key=${key}`;

  return new Promise(resolve => {
    rest.synergyGet(`api/registry/count_data${param}`, resolve, err => {
      console.log(`KanbanBoard ERROR [ getCountRow ]: ${JSON.stringify(err)}`);
      resolve(null);
    });
  });
}

const renderCountRow = async (filterCode, menuCount) => {
  const {recordsCount: allRow} = await getCountRow(filterCode);
  const {recordsCount: newRow} = await getCountRow(filterCode, '1');

  menuCount.removeClass('isnew');

  if(Number(allRow) == 0) {
    menuCount.hide().text('');
  } else if(Number(newRow) > 0) {
    menuCount.show().text(`${newRow}/${allRow}`).addClass('isnew');
  } else {
    menuCount.show().text(allRow);
  }
}

pageHandler('mainPage', () => {
  history.pushState(null, null, '/contracts');

  const eventParam = {
    registryCode: 'workflow_registry_my_docs',
    filterCode: 'workflow_registry_my_docs_filter_all',
    fieldIsNew: {
      code: 'workflow_form_my_doc_isnew',
      key: '1'
    }
  };

  //инициация кастомного компонента записей реестра
  $('#customRegistryList').trigger({
    type: 'renderNewTable',
    eventParam: eventParam
  });

  const updateData = () => {
    $('#searchInput').val(null);
    $('#customRegistryList').trigger({type: 'updateTableBody'});

    //обновляем счетчики
    menuItems.forEach(async (item, i) => {
      const {id, filterCode} = item;
      const menuCount = $(`#${id} .menu_count`);
      await renderCountRow(filterCode, menuCount);
    });
  }

  if (!Cons.getAppStore().mainPage_listener) {
    //кнопка обновить
    addListener('button_click', 'buttonRefreshRegList', e => {
      updateData();
    });

    addListener('change_locale', 'root-panel', e => {
      $('#customRegistryList').trigger({
        type: 'renderNewTable',
        eventParam: eventParam
      });
    });

    //кнопка выгрузки в excel
    addListener('button_click', 'buttonGetXLS', e => {
      $('#customRegistryList').trigger({type: 'getXLS'});
    });

    //клик по записи реестра
    addListener('registry_item_click', 'customRegistryList', async event => {
      Cons.showLoader();
      const asfData = await appAPI.loadAsfData(event.dataUUID);
      const contractDataUUID = asfData.data.find(x => x.id == 'workflow_form_my_doc_dataUUID')?.value || null;
      Cons.hideLoader();

      Cons.setAppStore({dataUUID: contractDataUUID});
      fire({type: 'goto_page', pageCode: 'formPlayerPage'}, 'root-panel');
    });

    let delay = 60000;
    let timerId = setTimeout(function request() {
      updateData();
      timerId = setTimeout(request, delay);
    }, delay);

    Cons.setAppStore({mainPage_listener: true});
  }

  $('#searchInput').off().on('keyup', e => {
    if (e.keyCode === 13) {
      e.preventDefault();
      $('#customRegistryList').trigger({
        type: 'searchInRegistry',
        eventParam: {
          searchString: $('#searchInput').val()
        }
      });
    }
  });

  // контейнер для пунктов меню
  const menuContainer = $('#panel-nav');
  menuContainer.addClass('filter_menu_container');
  menuContainer.empty();

  // рисуем менюшку с фильтрами
  menuItems.forEach((item, i) => {
    const {id, name, icon, filterCode} = item;

    const menuItem = $('<div>', {class: 'menu_item', id: id});
    const menuIcon = getSvgIcon(icon);
    const menuName = $('<span>', {class: 'menu_name'});
    const menuCount = $('<span>', {class: 'menu_count'});

    if(i == 0) menuItem.addClass('selected');

    menuName.text(name[AS.OPTIONS.locale]);
    menuItem.append($('<div>').append(menuIcon, menuName), menuCount);
    menuItem.attr('uk-tooltip', `title: ${name[AS.OPTIONS.locale]}; pos: right; duration: 300;`);

    addListener('change_locale', 'root-panel', e => {
      menuName.text(name[AS.OPTIONS.locale]);
      menuItem.attr('uk-tooltip', `title: ${name[AS.OPTIONS.locale]}; pos: right; duration: 300;`);
    });

    menuItem.on('click', e => {
      $('#panel-nav .menu_item').removeClass('selected');
      menuItem.addClass('selected');

      $('#searchInput').val(null);
      $('#customRegistryList').trigger({
        type: 'updateTableBody',
        eventParam: {filterCode}
      });
    });

    menuContainer.append(menuItem);

    renderCountRow(filterCode, menuCount);
  });

});
