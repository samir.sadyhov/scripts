const getUserWork = (processes, userID) => {
  const result = [];
  function search(p) {
    p.forEach(function(process) {
      if (!process.finished &&
        process.actionID != "" &&
        (process.responsibleUserID == userID || process.authorID == userID)
      ) {
        result.push(process);
      }
      if (process.subProcesses.length > 0) search(process.subProcesses);
    });
  }
  search(processes);
  return result;
}

const filterAgrProcesses = processes => {
  const procCode = ['APPROVAL_ITEM', 'AGREEMENT_ITEM', 'ACQUAINTANCE_ITEM'];
  const result = [];
  function search(p) {
    p.forEach(function(process) {
      if (!process.finished && procCode.includes(process.typeID)) {
        result.push(process);
      }
      if (process.subProcesses.length > 0) search(process.subProcesses);
    });
  }
  search(processes);
  return result;
}

const canEdit = async player => {
  const {docActions, process, docInfo, workInfo} = player;
  const acqAgrProc = filterAgrProcesses(process);

  if(!docInfo) return false;
  if(docInfo.registered == "true") return false;

  if(workInfo) {
    const {actionID, has_subprocesses, parent_process} = workInfo;
    const procCode = ['approval-single', 'agreement-single', 'acquaintance-single'];
    if(procCode.includes(parent_process)) return false;

    if(has_subprocesses == "true") {
      const subworks = await appAPI.getSubworks(actionID);
      const filtered = subworks.filter(x => procCode.includes(x.parent_process));
      if(filtered.length) return false;
    }
  }

  return true;
}

const checkSavedForm = (formPlayer, handler) => {
  if(formPlayer.model.hasChanges) {
    UIkit.modal.confirm(i18n.tr('Документ был изменен. Сохранить произведенные изменения?'),
    {labels: {ok: i18n.tr('Да'), cancel: i18n.tr('Отмена')}})
    .then(() => {
      if(!formPlayer.model.isValid()) {
        showMessage(i18n.tr("Заполните обязательные поля!"), 'warn');
      } else {
        Cons.showLoader();
        const asfData = formPlayer.model.getAsfData();
        const data = {
          data: '"data" : ' + JSON.stringify(asfData.data),
          form: asfData.form,
          uuid: asfData.uuid
        };
        AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/asforms/form/multipartdata", res => {
          Cons.hideLoader();
          showMessage(`<svg width="35" height="35" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><polyline fill="none" stroke="#32d296" stroke-width="1.1" points="4,10 8,15 17,4"></polyline></svg> ${i18n.tr("Успешно")}`, 'success');
          handler();
        }, "text", data, "application/x-www-form-urlencoded; charset=UTF-8", err => {
          Cons.hideLoader();
          handler();
          showMessage(i18n.tr("Нет прав на редактирование файла"), 'error');
        });
      }
    }, handler);
  } else {
    handler();
  }
}

const saveFormData = (formPlayer, handler) => {
  if(!formPlayer.model.isValid()) {
    showMessage(i18n.tr("Заполните обязательные поля!"), 'warn');
  } else {
    Cons.showLoader();
    try {
      const asfData = formPlayer.model.getAsfData();
      const data = {
        data: '"data" : ' + JSON.stringify(asfData.data),
        form: asfData.form,
        uuid: asfData.uuid
      };
      AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/asforms/form/multipartdata", res => {
        Cons.hideLoader();
        showMessage(`<svg width="35" height="35" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><polyline fill="none" stroke="#32d296" stroke-width="1.1" points="4,10 8,15 17,4"></polyline></svg> ${i18n.tr("Успешно")}`, 'success');
        formPlayer.model.hasChanges = false;
        if(handler) handler();
      }, "text", data, "application/x-www-form-urlencoded; charset=UTF-8", err => {
        Cons.hideLoader();
        showMessage(i18n.tr("Нет прав на редактирование файла"), 'error');
      });
    } catch (error) {
      Cons.hideLoader();
      showMessage(i18n.tr("Ошибка сохранения данных по форме"), 'error');
      console.log(error);
    }
  }
}

const closeDocument = () => {
  history.pushState(null, null, '/contracts');
  Cons.setAppStore({dataUUID: null});
  fire({type: 'goto_page', pageCode: 'mainPage'}, 'root-panel');
}

const setStatusSignMyDoc = async (signal, dataUUID) => {
  let param = `registryCode=workflow_registry_my_docs&loadData=false`;
  param += `&field=workflow_form_my_doc_dataUUID&condition=TEXT_EQUALS&value=${dataUUID}`;
  const searchResult = await searchInRegistry(param);

  if(!searchResult || searchResult.count == 0) return false;

  const status = {
    id: 'workflow_form_my_doc_status',
    type: "listbox",
    value: 'Отклонен',
    key: '0'
  }

  if(signal == 'got_agree') {
    status.value = 'Подписан';
    status.key = '1';
  }

  const mergeResult = await appAPI.mergeFormData({
    uuid: searchResult.data[0].dataUUID,
    data: [status]
  });

  if(!mergeResult) return false;

  return true;
}

const setStatusMyDoc = async (status, dataUUID) => {
  let param = `registryCode=workflow_registry_my_docs&loadData=false`;
  param += `&field=workflow_form_my_doc_dataUUID&condition=TEXT_EQUALS&value=${dataUUID}`;
  const searchResult = await searchInRegistry(param);

  if(!searchResult || searchResult.count == 0) return false;

  const mergeResult = await appAPI.mergeFormData({
    uuid: searchResult.data[0].dataUUID,
    data: [{
      id: 'workflow_form_my_doc_status',
      type: "listbox",
      value: status.value,
      key: status.key
    }]
  });

  if(!mergeResult) return false;

  return true;
}

const setProgressWork = async (actionID, progress, formPlayer, finishStatus, dataUUID) => {
  if(progress == 100) {
    const msgConfirm = i18n.tr("Вы действительно хотите завершить данную работу? Все дочерние работы так же будут завершены.");
    const msgSuccess = i18n.tr("Работа завершена");

    UIkit.modal.confirm(msgConfirm, {labels: {ok: i18n.tr('Да'), cancel: i18n.tr('Отмена')}}).then(async () => {
      const resultFinish = await appAPI.setProgressWork(progress, actionID);
      if(resultFinish) {
        await setStatusMyDoc(finishStatus, dataUUID);
        showMessage(msgSuccess, 'success');
        closeDocument();
      } else {
        showMessage(i18n.tr('Произошла ошибка при завершении работы'), 'error');
      }
    }, () => {
      return;
    });
  } else {
    const resultFinish = await appAPI.setProgressWork(progress, actionID);
    if(resultFinish) {
      await setStatusMyDoc(finishStatus, dataUUID);
      closeDocument();
    } else {
      showMessage(i18n.tr('Произошла ошибка при изменении прогресса работы'), 'error');
    }
  }
}

const finishWork = async (formPlayer, workInfo) => {
  try {
    if(formPlayer.model.getErrors().length) throw new Error('Заполните обязательные поля');

    checkSavedForm(formPlayer, async () => {
      const msgConfirm = i18n.tr('Вы действительно хотите завершить работу?');
      const {actionID, author, completionForm, documentID, dataUUID} = workInfo;
      const childDocuments = await appAPI.getChildDocuments(documentID);
      workInfo.childDocuments = childDocuments;
      const finishStatus = {key: '5', value: 'Завершенные'};

      if(AS.OPTIONS.currentUser.userid == author.id) {
        UIkit.modal.confirm(msgConfirm, {labels: {ok: i18n.tr('Да'), cancel: i18n.tr('Отмена')}})
        .then(() => {
          _WORK.finishWork(actionID, formPlayer, async resultFinish => {
            await setStatusMyDoc(finishStatus, dataUUID);
          });
        }, null);
      } else {
        if(completionForm) {
          const {CompletionFormType, completionFormName, CompletionFormInfo} = completionForm;

          switch (CompletionFormType) {
            case "FORM": _WORK.finishForm(actionID, CompletionFormInfo, formPlayer, async resultFinish => {
              await setStatusMyDoc(finishStatus, dataUUID);
            }); break;
            case "COMMENT": _WORK.finishComment(actionID, formPlayer, async resultFinish => {
              await setStatusMyDoc(finishStatus, dataUUID);
            }); break;
            case "FILE": _WORK.finishFile(actionID, formPlayer, async resultFinish => {
              await setStatusMyDoc(finishStatus, dataUUID);
            }); break;
            case "DOCUMENT": _WORK.finishDocument(actionID, childDocuments, formPlayer, async resultFinish => {
              await setStatusMyDoc(finishStatus, dataUUID);
            }); break;
            case "NOTHING":
              UIkit.modal.confirm(msgConfirm, {labels: {ok: i18n.tr('Да'), cancel: i18n.tr('Отмена')}})
              .then(() => {
                _WORK.finishWork(actionID, formPlayer, async resultFinish => {
                  await setStatusMyDoc(finishStatus, dataUUID);
                });
              }, null);
              break;
          }

        } else {
          UIkit.modal.confirm(msgConfirm, {labels: {ok: i18n.tr('Да'), cancel: i18n.tr('Отмена')}})
          .then(() => {
            setProgressWork(actionID, 100, formPlayer, finishStatus, dataUUID);
          }, null);
        }
      }
    });

  } catch (error) {
    Cons.hideLoader();
    showMessage("Произошла ошибка при завершении работы", 'error');
    console.log(error);
  }
}

const searchInRegistry = async param => {
  return new Promise(resolve => {
    rest.synergyGet(`api/registry/data_ext?${param}`, resolve, err => {
      console.log(`ERROR [ searchInRegistry ]: ${JSON.stringify(err)}`);
      resolve(null);
    });
  });
}

const getCounterparty = async iin => {
  let param = `registryCode=workflow_registry_clients`;
  param += `&field=workflow_form_client_iin&condition=TEXT_EQUALS&value=${iin}`;
  param += `&fields=workflow_form_client_person`;
  param += `&fields=workflow_form_user`;
  param += `&fields=workflow_form_client_iin`;
  param += `&registryRecordStatus=STATE_SUCCESSFUL`
  const searchResult = await searchInRegistry(param);

  if(!searchResult || searchResult.recordsCount == 0) return null;

  return searchResult.result[0];
}

const finishProcess = async workInfo => {

  const {actionID, procInstID, name, parent_process, documentID, dataUUID} = workInfo;
  const procInfo = await appAPI.getProcessInfo(actionID);
  const {raw_data, buttons, demandSign} = procInfo;

  const dialog = await UTILS.getDialogFinishProcess(name, buttons, parent_process, async (signal, comment) => {

    try {
      let succesMsg = 'Документ успешно подписан ЭЦП';

      const finishProcessBody = {
        procInstID, signal, comment,
        rawdata: raw_data
      };

      NCALayer.sign('SIGN', documentID, async result => {
        Cons.showLoader();

        try {
          const iin = result.SERIALNUMBER.slice(3);

          const counterparty = await getCounterparty(iin);

          if(!counterparty) throw new Error('ИИН на ЭЦП не соответсвует ИИН авторизованного пользователя');

          if(counterparty.fieldKey?.workflow_form_user != AS.OPTIONS.currentUser.userid) throw new Error('Произошла ошибка при проверки контрагента');

          finishProcessBody.signdata = result.signedData;
          finishProcessBody.certificate = result.certificate;
          finishProcessBody.certID = result.certID;
          finishProcessBody.addSignature = true;

          if(signal != 'got_agree') succesMsg = 'Документ успешно отклонен';

          const resultFinish = await appAPI.finishProcess(finishProcessBody);
          if(!resultFinish) throw new Error('Произошла ошибка при подписании документа');

          await setStatusSignMyDoc(signal, dataUUID);

          Cons.hideLoader();
          showMessage(i18n.tr(succesMsg), 'success');

          closeDocument();

          UIkit.modal(dialog).hide();
        } catch (e) {
          Cons.hideLoader();
          UIkit.modal(dialog).hide();
          showMessage(i18n.tr(e.message), 'error');
        }

      });

    } catch (err) {
      Cons.hideLoader();
      UIkit.modal(dialog).hide();
      showMessage(i18n.tr(err.message), 'error');
    }
  });

  UIkit.modal(dialog).show();
  dialog.on('hidden', () => {
    dialog.remove();
  });
}

const renderButtons = async player => {
  const {userid} = AS.OPTIONS.currentUser;
  const {workInfo} = player;

  if(workInfo) {
    const {parent_process, user, author, completionFormID, completionForm, name, finished, dataUUID} = workInfo;
    const finishStatus = {key: '5', value: 'Завершенные'};

    if(finished) return;

    if(userid == user.id) {
      switch (parent_process) {
        case 'null':
        case 'assignment-single': //Завершить
          $('#buttonWorkASSIGNMENT').removeClass('uk-hidden');
          $('#buttonWorkASSIGNMENT').off().on('click', e => {
            e.preventDefault();
            e.target.blur();
            finishWork(player, workInfo);
          });
          break;
        case 'acquaintance-single': //Ознакомиться
          $('#buttonWorkACQUAINTANCE').removeClass('uk-hidden');
          $('#buttonWorkACQUAINTANCE').off().on('click', e => {
            e.preventDefault();
            e.target.blur();
            _WORK.finishProcess(workInfo, player, async resultFinish => {
              await setStatusMyDoc(finishStatus, dataUUID);
            });
          });
          break;
        case 'approval-single': //Утвердить
          $('#buttonSign').removeClass('uk-hidden');
          $('#buttonSign').off().on('click', e => {
            e.preventDefault();
            e.target.blur();
            finishProcess(workInfo);
          });
          break;
        case 'agreement-single': //Согласовать
          $('#buttonWorkAGREEMENT').removeClass('uk-hidden');
          $('#buttonWorkAGREEMENT').off().on('click', e => {
            e.preventDefault();
            e.target.blur();
            finishProcess(workInfo);
          });
          break;

      }
    } else if(userid == author.id) {
      if(completionForm && completionForm.hasOwnProperty('is_result_free') && !completionForm.is_result_free) {
        $('#buttonWorkASSIGNMENT').removeClass('uk-hidden');
        $('#buttonWorkASSIGNMENT').off().on('click', e => {
          e.preventDefault();
          e.target.blur();
          _WORK.acceptWork(player, workInfo, async resultFinish => {
            await setStatusMyDoc(finishStatus, dataUUID);
          });
        });
      } else {
        $('#buttonWorkASSIGNMENT').removeClass('uk-hidden');
        $('#buttonWorkASSIGNMENT').off().on('click', e => {
          e.preventDefault();
          e.target.blur();
          finishWork(player, workInfo);
        });
      }
    }

    //проверка доступна ли форма для редактирования
    const can_edit = await canEdit(player);
    if(can_edit) {
      let formEditable = false;

      //кнопка редактировать
      $('#buttonEditForm').removeClass('uk-hidden');
      $('#buttonEditForm').off().on('click', e => {
        e.preventDefault();
        e.target.blur();
        formEditable = !formEditable;
        fire({type: 'set_form_editable', editable: formEditable}, player.code);
        if(formEditable) {
          UTILS.show('buttonSaveForm');
          fire({
            type: 'button_change_text',
            text: localizedText("Просмотр","Просмотр","Просмотр","Просмотр"),
          }, 'buttonEditForm');
        } else {
          UTILS.hide('buttonSaveForm');
          fire({
            type: 'button_change_text',
            text: localizedText("Редактировать","Редактировать","Редактировать","Редактировать"),
          }, 'buttonEditForm');
        }
      });

      //кнопка сохранить
      $('#buttonSaveForm').off().on('click', e => {
        e.preventDefault();
        e.target.blur();
        saveFormData(player, () => {
          formEditable = false;
          fire({type: 'set_form_editable', editable: formEditable}, player.code);
          UTILS.hide('buttonSaveForm');
          fire({
            type: 'button_change_text',
            text: localizedText("Редактировать","Редактировать","Редактировать","Редактировать"),
          }, 'buttonEditForm');
        });
      });
    }

  }
}

const initWorkActions = async dataUUID => {
  Cons.showLoader();

  try {
    const player = Cons.getCompByCode('formPlayerContract');

    player.dataUUID = dataUUID;
    player.documentID = await appAPI.getDocumentIdentifier(player.dataUUID);
    player.docInfo = await appAPI.getDocumentInfo(player.documentID);
    player.docActions = await AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/docflow/document_actions?documentID=${player.documentID}&locale=${AS.OPTIONS.locale}`);
    player.process = await AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/workflow/get_execution_process?documentID=${player.documentID}&locale=${AS.OPTIONS.locale}`);

    const processes = getUserWork(player.process, AS.OPTIONS.currentUser.userid);
    if(processes.length) {
      const {actionID} = processes[processes.length - 1];
      const workInfo = await appAPI.getWorkInfo(actionID);

      workInfo.dataUUID = player.dataUUID;
      workInfo.documentID = player.documentID;

      if(workInfo.completionFormID && workInfo.completionFormID != "") {
        workInfo.completionForm = await appAPI.getCompletionForm(workInfo.completionFormID);
      }

      player.workInfo = workInfo;

      renderButtons(player);
    }

    Cons.hideLoader();
  } catch (e) {
    Cons.hideLoader();
    showMessage(e.message, 'error');
  }
}

const changeStatusIsNew = async dataUUID => {
  let param = `registryCode=workflow_registry_my_docs&loadData=true`;
  param += `&field=workflow_form_my_doc_dataUUID&condition=TEXT_EQUALS&value=${dataUUID}`;
  param += `&fields=workflow_form_my_doc_isnew&countInPart=1`;
  const searchResult = await searchInRegistry(param);

  if(!searchResult || searchResult.recordsCount == 0) return false;

  const row = searchResult.result[0];

  if(row?.fieldKey?.workflow_form_my_doc_isnew == '1') {
    await appAPI.mergeFormData({
      uuid: row.dataUUID,
      data: [{
        id: 'workflow_form_my_doc_isnew',
        type: "listbox",
        value: "Нет",
        key: "2"
      }]
    });
  }
}

pageHandler("formPlayerPage", () => {
  const {dataUUID} = Cons.getAppStore();

  if(dataUUID) {
    fire({type: 'show_form_data', dataId: dataUUID}, 'formPlayerContract');
    history.pushState(null, null, `/contracts/?dataUUID=${dataUUID}`);

    initWorkActions(dataUUID);

    changeStatusIsNew(dataUUID);

    $('#buttonPrintForm').off().on('click', e => {
      e.preventDefault();
      e.target.blur();

      const player = Cons.getCompByCode('formPlayerContract');
      if(player.model.hasPrintable) {
        window.open(`../Synergy/rest/asforms/template/print/form?format=pdf&dataUUID=${dataUUID}`);
      } else {
        UTILS.printForm(player.view.container[0]);
      }
    });

  } else {
    UTILS.hide('formPlayerContract');
  }
});
