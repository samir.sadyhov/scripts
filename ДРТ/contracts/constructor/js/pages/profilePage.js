const searchInRegistry = async param => {
  return new Promise(resolve => {
    rest.synergyGet(`api/registry/data_ext?${param}`, resolve, err => {
      console.log(`ERROR [ searchInRegistry ]: ${JSON.stringify(err)}`);
      resolve(null);
    });
  });
}

const searchCounterparty = async userid => {
  let param = `registryCode=workflow_registry_clients`;
  param += `&field=workflow_form_user&condition=TEXT_EQUALS&key=${userid}`;
  param += `&registryRecordStatus=STATE_SUCCESSFUL&loadData=false`;

  return searchInRegistry(param);
}

const blockFields = player => {
  const fields = [
    'workflow_form_client_id',
    'workflow_form_client_manager',
    'workflow_form_client_iin',
    'workflow_textbox_bin',
    'workflow_form_user'
  ];

  fields.forEach(id => {
    const fieldView = player.view.getViewWithId(id);
    if(fieldView) fieldView.setEnabled(false);
  });
}

const initPage = async () => {
  Cons.showLoader();
  try {

    const searchResult = await searchCounterparty(AS.OPTIONS.currentUser.userid);
    if(!searchResult || searchResult.count != 1) throw new Error('Произошла ошибка при открытии карточки профиля');

    const {dataUUID} = searchResult.data[0];
    const player = Cons.getCompByCode('formPlayerProfile');

    fire({type: 'show_form_data', dataId: dataUUID}, 'formPlayerProfile');

    player.model.on('dataLoad', () => {
      blockFields(player);
    });

    $('#buttonSave').removeClass('uk-hidden');

    Cons.hideLoader();
  } catch (err) {
    Cons.hideLoader();
    showMessage(i18n.tr(err.message), 'error');
  }

}

pageHandler("profilePage", () => {
  initPage();

  $('#buttonSave').off().on('click', async e => {
    e.preventDefault();
    e.target.blur();

    Cons.showLoader();
    try {
      const player = Cons.getCompByCode('formPlayerProfile');
      if(!player.model.asfDataId) throw new Error('Произошла ошибка получения данных');
      if(!player.model.isValid()) throw new Error('Заполните обязательные поля');

      const asfData = player.model.getAsfData();
      const mergeResult = await appAPI.mergeFormData({uuid: asfData.uuid, data: asfData.data});

      if(!mergeResult) throw new Error('Произошла ошибка сохранения данных');

      const currentEmail = player.model.getModelWithId('workflow_textbox_email');
      const prevEmail = player.model.getModelWithId('workflow_textbox_email_prev');

      if(currentEmail.getValue() != prevEmail.getValue()) {
        const urlCheck = `${window.location.origin}/contracts/?uid=${AS.OPTIONS.currentUser.userid}`;

        await appAPI.sendNotification({
          header: `Подтверждение смены email на ${window.location.href}`,
          message: `На портале договоров <a href="${window.location.href}">${window.location.href}</a> Вы изменили email. Для подтверждения нового email перейдите по ссылке:<br><a href="${urlCheck}">${urlCheck}</a>`,
          emails: [currentEmail.getValue()]
        });

        Cons.hideLoader();

        UIkit.modal.alert(`На указанную Вами почту отправлено письмо для подтверждения e-mail. При подтверждении Ваш логин будет изменен на ${currentEmail.getValue()}`);
      }

      Cons.hideLoader();
    } catch (err) {
      Cons.hideLoader();
      showMessage(i18n.tr(err.message), 'error');
    }

  });

});
