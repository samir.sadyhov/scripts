const getUrlParameter = sParam => {
  let sPageURL = window.location.search.substring(1), sURLVariables = sPageURL.split('&'), sParameterName;
  for (let i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split('=');
    if (sParameterName[0] === sParam)
    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
  }
}

const getAuthParam = () => {
  const {defaultUser, defaultUserPassword} = Cons.getCurrentApp();
  return "Basic " + btoa(unescape(encodeURIComponent(`${defaultUser}:${defaultUserPassword}`)));
}

const searchCounterparty = async userID => {
  let param = `registryCode=workflow_registry_clients`;
  param += `&field=workflow_form_user&condition=TEXT_EQUALS&key=${userID}`;
  param += `&registryRecordStatus=STATE_SUCCESSFUL&loadData=false`;

  return new Promise(async resolve => {
    try {
      const url = `../Synergy/rest/api/registry/data_ext?${param}`;
      const response = await fetch(url, {method: 'GET', headers: {"Authorization": getAuthParam()}});

      if(!response.ok) throw new Error(`HTTP: ${response.status}, message: ${response.statusText} [ ${response.text()} ]`);
      resolve(response.json());
    } catch (err) {
      console.log(`ERROR [ searchCounterparty ]: ${err.message}`);
      resolve(value);
    }
  });
}

const loadAsfData = async dataUUID => {
  return new Promise(async resolve => {
    try {
      const url = `../Synergy/rest/api/asforms/data/${dataUUID}`;
      const response = await fetch(url, {method: 'GET', headers: {"Authorization": getAuthParam()}});
      if(!response.ok) throw new Error(`HTTP: ${response.status}, message: ${response.statusText} [ ${response.text()} ]`);
      resolve(response.json());
    } catch (err) {
      console.log(`ERROR [ loadAsfData ]: ${err.message}`);
      resolve(value);
    }
  });
}

const changeLogin = async body => {
  return new Promise(async resolve => {
    try {
      const url = `../Synergy/rest/api/filecabinet/user/save`;
      const headers = new Headers();
      headers.append("Authorization", getAuthParam());
      headers.append("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
      const response = await fetch(url, {method: 'POST', headers, body});
      if(!response.ok) throw new Error(`HTTP: ${response.status}, message: ${response.statusText} [ ${response.text()} ]`);
      resolve(response.json());
    } catch (err) {
      console.log(`ERROR [ changeLogin ]: ${err.message}`);
      resolve(null);
    }
  });
}

const mergeFormData = async data => {
  return new Promise(async resolve => {
    try {
      const url = `../Synergy/rest/api/asforms/data/merge`;
      const response = await fetch(url, {
        method: 'POST',
        headers: {"Authorization": getAuthParam(), "Content-Type": "application/json; charset=UTF-8"},
        body: JSON.stringify(data)
      });
      if(!response.ok) throw new Error(`HTTP: ${response.status}, message: ${response.statusText} [ ${response.text()} ]`);
      resolve(response.json());
    } catch (err) {
      console.log(`ERROR [ mergeFormData ]: ${JSON.stringify(err)}`);
      resolve(null);
    }
  });
}

const changeEmailInit = async userID => {
  Cons.showLoader();
  try {

    const searchResult = await searchCounterparty(userID);
    if(!searchResult || searchResult.count != 1) throw new Error('Произошла ошибка поиска карточки контрагента');

    const asfData = await loadAsfData(searchResult.data[0].dataUUID);

    const email = asfData.data.find(x => x.id == 'workflow_textbox_email');
    const iin = asfData.data.find(x => x.id == 'workflow_form_client_iin');
    const lastname = asfData.data.find(x => x.id == 'workflow_textbox_signer_lastname_nominative');
    const firstname = asfData.data.find(x => x.id == 'workflow_textbox_signer_firstname_nominative');

    const urlencoded = new URLSearchParams();
    urlencoded.append("userID", userID);
    urlencoded.append("lastname", lastname.value);
    urlencoded.append("firstname", firstname.value);
    urlencoded.append("pointersCode", 'IIN' + iin.value);
    urlencoded.append("login", email.value);

    const saveResutl = await changeLogin(urlencoded);

    await mergeFormData({
      uuid: asfData.uuid,
      data: [{
        id: 'workflow_textbox_email_prev',
        type: 'textbox',
        value: email.value
      }]
    });

    Cons.hideLoader();

    $('#label-1 span')
    .css('letter-spacing', '1px')
    .text(`Ваш логин изменен на ${email.value}, Вы будете перенаправлены на страницу авторизации через 5 секунд.`);
    setTimeout(() => {
      history.pushState(null, null, '/contracts');
      Cons.setAppStore({dataUUID: null});
      Cons.logout();
      fire({type: 'goto_page', pageCode: 'authPage'}, 'root-panel');
    }, 5000);

  } catch (err) {
    Cons.hideLoader();
    showMessage(i18n.tr(err.message), 'error');
    history.pushState(null, null, '/contracts');
    Cons.setAppStore({dataUUID: null});
    Cons.logout();
    fire({type: 'goto_page', pageCode: 'authPage'}, 'root-panel');
  }
}

pageHandler('start_page', () => {
  const uid = getUrlParameter('uid');

  if(uid) {
    changeEmailInit(uid);
  } else {
    fire({type: 'goto_page', pageCode: 'authPage'}, 'root-panel');
  }
});
