//Иконка приложения
if(!$('link[rel="icon"]').length) $('head').append('<link rel="icon" href="../constructorFiles/contracts/logo.svg">');

const getFullName = person => {
  const {firstname, lastname, patronymic} = person;
  const fio = lastname.substr(0, 1).toUpperCase() + lastname.substr(1) + ' ' + firstname.substr(0, 1).toUpperCase() + '.';
  return patronymic ? fio + ' ' + patronymic.substr(0, 1).toUpperCase() + '.' : fio;
}

const setProfileData = () => {
  if(!$('#profile').length) return;

  const photoURL = `../Synergy/load?userid=${AS.OPTIONS.currentUser.userid}`;
  $('#profileName span').text(getFullName(AS.OPTIONS.currentUser));
  $('#profileImage').attr('src', photoURL);
}

const addMenuButtonIcon = () => {
  const buttons = $('.mb-icon');

  if(!buttons.length) return;

  for(let i = 0; i < buttons.length; i++) {
    const button = $(buttons[i]);
    const buttonID = button.attr('id');
    const {comp} = getCompByCode(buttonID);
    const classList = button.attr('class').split(/\s+/);
    const icon = classList.find(x => x.substr(0,5) == 'icon-').substr(5);

    button.empty().append(
      getSvgIcon(icon),
      `<span class="button-name">${comp.text.translations[AS.OPTIONS.locale]}</span>`
    );
    button.attr('uk-tooltip', `title: ${comp.text.translations[AS.OPTIONS.locale]}; pos: right; duration: 300;`);

    addListener('change_locale', 'root-panel', e => {
      button.empty().append(
        getSvgIcon(icon),
        `<span class="button-name">${comp.text.translations[AS.OPTIONS.locale]}</span>`
      );
      button.attr('uk-tooltip', `title: ${comp.text.translations[AS.OPTIONS.locale]}; pos: right; duration: 300;`);
    });
  }

}

const leftPanelInit = () => {
  const leftPanel = $('#leftPanel');
  const {appInIframe, userGroups = []} = Cons.getAppStore();

  if(!leftPanel.length) return;

  // свернуть/развернуть левую панель
  let leftPanelVisible = true;
  $('#buttonHideShowLeftPanel').off().on('click', function(e) {
    e.preventDefault();
    e.target.blur();

    if(leftPanelVisible) {
      leftPanel.css({
        'width': '50px'
      });
      $('#panelContent, #panelAppActions').css({
        'width': 'calc(100% - 50px)',
        'left': '50px'
      });
      $(this).addClass('pressed');
      $(this).find('span').attr('uk-icon', 'icon: chevron-double-right');
      $('.menu-content').addClass('collapse');
      $('#profile').hide();
    } else {
      leftPanel.css({
        'width': '300px'
      });
      $('#panelContent, #panelAppActions').css({
        'width': 'calc(100% - 300px)',
        'left': '300px'
      });
      $(this).removeClass('pressed');
      $(this).find('span').attr('uk-icon', 'icon: chevron-double-left');
      $('.menu-content').removeClass('collapse');
      $('#profile').fadeIn(1000);
    }
    leftPanelVisible = !leftPanelVisible;
  });
}

function getCookie(name) {
  let matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setCookie(name, value, options = {}) {

  options = {
    path: '/',
    ...options
  };

  if (options.expires instanceof Date) {
    options.expires = options.expires.toUTCString();
  }

  let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);

  for (let optionKey in options) {
    updatedCookie += "; " + optionKey;
    let optionValue = options[optionKey];
    if (optionValue !== true) {
      updatedCookie += "=" + optionValue;
    }
  }

  document.cookie = updatedCookie;
}

function deleteCookie(name) {
  setCookie(name, "", {
    'max-age': -1
  });
}

if($('#buttonLogout').length) {
  $('#buttonLogout').off().on('click', e => {
    deleteCookie(`constructor_creds_for_customers`);
    deleteCookie(`constructor_image_api_key`);
    Cons.logout();
  });
}

addMenuButtonIcon();
leftPanelInit();
setProfileData();

jQuery(document).mouseup(function(event){
	setTimeout(function(){
		AS.FORMS.popupPanel.hide();
	}, 0);
});

jQuery(document).click(function(event){
	AS.FORMS.popupPanel.hide();
});
