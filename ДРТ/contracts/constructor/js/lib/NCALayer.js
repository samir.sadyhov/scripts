/*
Получение информации по ключу авторизации
NCALayer.sign('AUTH', null, result => {
  console.log(result)
});

получение информации по ключу и данных для подписания при согласовании/утверждении или просто подписания документа
NCALayer.sign('SIGN','documentID', result => {
  console.log(result)
});
*/

this.NCALayer = {
  connector: {
    callback: null,
    heartbeat_interval: null
  },

  SOCKET_URL: 'wss://127.0.0.1:13579/',
  METHOD_GET_KEYS: 'getKeys',
  METHOD_GET_SUBJECT_DN: 'getSubjectDN',
  METHOD_GET_NOT_BEFORE: 'getNotBefore',
  METHOD_GET_NOT_AFTER: 'getNotAfter',
  METHOD_SIGN_CMS: 'createCMSSignature',
  CALLING_TIMEOUT: 100,
  METHOD_BROWSE_KEY_STORE: 'browseKeyStore',
  storageName: "PKCS12",
  missed_heartbeats_limit_max: 50,
  missed_heartbeats_limit_min: 30,
  missed_heartbeats_limit: this.missed_heartbeats_limit_min,
  webSocket: null,
  heartbeat_msg: '--heartbeat--',
  missed_heartbeats: 0,

  keysConstType: null,
  documentID: null,

  storagePath: "",
  password: "",
  key: "",
  info: {},

  successHandler: null,

  sign: function(keysConstType = 'AUTH', documentID = null, handler) {
    // keysConstType 'AUTH' || 'SIGN'
    this.keysConstType = keysConstType;
    this.documentID = documentID;
    this.storagePath = "";
    this.password = "";
    this.key = "";
    this.info = {};

    if(handler && typeof handler == 'function') {
      this.successHandler = handler;
    } else {
      this.successHandler = null;
    }

    if(!this.webSocket) this.connector.initNCALayerSocket();

    if(this.webSocket) this.connector.openChooseCertDialog();
  },

  getSynergySignData: async function() {
    const {dataForSign, certificate, signedData, CN} = this.info;

    const verificationkey = await AS.FORMS.ApiUtils.simpleAsyncPost("rest/sign/verificationkey", null, "text", {
      uuid: AS.OPTIONS.currentUser.userid,
      pemCer: certificate,
      edsInfo: CN
    });

    if(verificationkey === null || verificationkey.indexOf("::::") === -1)
    throw new Error('Произошла ошибка проверки ключа.');
    if(verificationkey === "CERT REVOKED") throw new Error('Сертификат отозван.');
    if(verificationkey === "CERT END") throw new Error('Сертификат просрочен.');

    const certID = verificationkey.split('::::')[0];
    const alg = verificationkey.split('::::')[1];

    return {
      documentID: this.documentID,
      rawdata: dataForSign,
      signdata: signedData,
      certificate,
      certID,
      alg
    }
  }

}

NCALayer.connector.initNCALayerSocket = function () {
  if(!NCALayer.webSocket) NCALayer.webSocket = new WebSocket(NCALayer.SOCKET_URL);
  NCALayer.webSocket.onopen = webSocketOnOpen.bind(this);
  NCALayer.webSocket.onclose = webSocketOnClose.bind(this);
  NCALayer.webSocket.onmessage = webSocketOnMessage.bind(this);
}

const generateGuid = prefix => prefix+'-'+Math.random().toString(36).substring(2, 15)+Math.random().toString(36).substring(2, 15);

const createInputText = (label, placeholder = '', disabled = false) => {
  const id = generateGuid('nc');
  const container = $('<div>', {class: 'uk-margin-small'});
  const fc = $('<div>', {class: 'uk-form-controls'});
  const input = $(`<input type="text" id="${id}" class="uk-input" placeholder="${placeholder}">`);

  if(disabled) input.prop('disabled', true);

  fc.append(input);
  container.append(`<label class="uk-form-label uk-text-bold" for="${id}">${label}</label>`).append(fc);

  return {container, input};
}

const createInputPassword = (label = '') => {
  const id = generateGuid('nc');
  const container = $('<div>', {class: 'uk-margin-small'});
  const fc = $('<div>', {class: 'uk-form-controls'});

  const inline = $('<div>', {
    class: 'uk-inline',
    style: 'width: 100%;'
  });
  const icon = $(`<span>`, {
    class: 'uk-form-icon uk-form-icon-flip material-icons',
    style: 'display: flex; pointer-events: all; cursor: pointer;'
  })
  const input = $(`<input>`, {type: "password", id, class: "uk-input"});

  let hidePass = true;
  icon.on('click', e => {
    if(hidePass) {
      input.attr('type', 'text');
      icon.text('visibility_off');
    } else {
      input.attr('type', 'password');
      icon.text('visibility');
    }
    hidePass = !hidePass;
  });

  icon.text('visibility');
  inline.append(icon, input);
  fc.append(inline);
  container.append(`<label class="uk-form-label uk-text-bold" for="${id}">${label}</label>`, fc);
  return {container, input};
}

const getPasswordModalDialog = (body, callBack) => {
  const dialog = $('<div class="uk-flex-top" uk-modal>');
  const md = $('<div class="uk-modal-dialog uk-margin-auto-vertical" style="max-width: 600px !important; overflow: hidden;">');
  const modalBody = $('<div class="uk-modal-body" uk-overflow-auto>');
  const footer = $('<div class="uk-modal-footer uk-text-right">');
  const buttonApply = $(`<button class="uk-button uk-button-default" type="button">${i18n.tr("Подтвердить")}</button>`);

  dialog.css({'z-index': '10000'});

  buttonApply.on('click', e => {
    e.preventDefault();
    e.target.blur();
    callBack();
  });

  modalBody.append(body);
  footer.append(buttonApply);
  md.append(
    `<div class="uk-modal-header"><h3>Введите пароль от сертификата</h3></div>`,
    modalBody,
    footer
  );
  dialog.append(md);

  return dialog;
}

const SignInformationFunctions = {
  getKeyPath: function (result) {
    if(result.getResult() && result.getErrorCode() === "NONE") {

      const dialogContent = $('<div>');
      const {
        container: pathContainer,
        input: pathInput
      } = createInputText(i18n.tr('Путь к хранилищу эцп'), '-------------------', true);

      const {
        container: passwordContainer,
        input: passwordInput
      } = createInputPassword(i18n.tr('Введите пароль от эцп'));

      dialogContent.append(pathContainer, passwordContainer);
      pathInput.val(result.getResult() + "");

      const dialog = getPasswordModalDialog(dialogContent, () => {
        NCALayer.storagePath = result.getResult();
        NCALayer.password = passwordInput.val();
        NCALayer.connector.loadKeys();
        UIkit.modal(dialog).hide();
      });

      // Событие Для инпута при нажатии ENTER
      passwordInput.keyup(function (event) {
        if(event.keyCode === 13) {
          NCALayer.storagePath = result.getResult();
          NCALayer.password = passwordInput.val();
          NCALayer.connector.loadKeys();
          UIkit.modal(dialog).hide();
        }
      });

      UIkit.modal(dialog).show();
      dialog.on('hidden', () => {
        dialog.remove();
      });

    } else if(result.getErrorCode() !== "NONE") {
      NCALayer.connector.openChooseCertDialog();
    } else {
      this.sendNotification(202, "Отмена действия");
    }
  },

  loadKeysBack: function(result) {
    if(result.getResult() && result.getResult() !== -1) {
      NCALayer.key = result.getResult().split("|")[3];
      NCALayer.connector.getKeySubjectDN();
    } else if(result.getErrorCode() === "LOAD_KEYSTORE_ERROR") {
      this.sendNotification(205, "Ошибка получения эцп по сохраненому пути");
    } else if(result.getErrorCode() === "EMPTY_KEY_LIST") {
      this.sendNotification(205, "Неверный вид сертификата");
    } else {
      this.sendNotification(204, "Неправильный пароль");
    }
  },

  getKeySubjectDNBack: function(result) {
    if(result.getResult()) {
      const keyInfo = result.getResult().split(",");
      NCALayer.info = {};
      keyInfo.forEach(item => {
        const keyValue = item.split('=');
        NCALayer.info[keyValue[0]] = keyValue[1];
      });
      NCALayer.connector.loadExpireBefore();
    } else {
      this.sendNotification(202, "Ошибка получения базовой информации из эцп");
    }
  },

  loadExpireBeforeBack: function(result) {
    if(result.getResult()) {
      NCALayer.info['expireBeforeBack'] = result.getResult();
      NCALayer.connector.loadExpireAfter();
    } else {
      this.sendNotification(202, "Ошибка получения даты начала эцп");
    }
  },

  loadExpireAfterBack: function(result) {
    if(result.getResult()) {
      NCALayer.info['expireAfterBack'] = result.getResult();

      if(NCALayer.documentID) {
        NCALayer.connector.signDocument();
      } else {
        NCALayer.password = "";
        if(NCALayer.successHandler) NCALayer.successHandler(NCALayer.info);
      }

    } else {
      this.sendNotification(202, "Ошибка получения даты окончания эцп");
    }
  },

  signDocumentSynergy: async function(result) {
    if(result?.code == 200) {
      NCALayer.info = {...NCALayer.info, ...result.getResponseObject()}
      if(NCALayer.successHandler) {
        const synergyData = await NCALayer.getSynergySignData();
        NCALayer.successHandler({info: NCALayer.info, synergyData});
      }
    } else {
      this.sendNotification(202, "Ошибка подписания документа");
      console.log('signDocumentSynergy ERROR', {result, keyInfo: NCALayer.info});
    }
  },

  sendNotification: function(int, text) {
    console.log(`NCALayer notifications: [${int}] - ${text}`);
    if([202, 204, 205].includes(int)) showMessage(i18n.tr(text), "error");
  }
}

function setMissedHeartbeatsLimitToMax() {
  NCALayer.missed_heartbeats_limit = NCALayer.missed_heartbeats_limit_max;
}

function setMissedHeartbeatsLimitToMin() {
  NCALayer.missed_heartbeats_limit = NCALayer.missed_heartbeats_limit_min;
}

function customSend(method, args, callback) {
  if(callback) NCALayer.connector.callback = callback;
  setMissedHeartbeatsLimitToMax();
  try {
    getNCALayerSocket().send(JSON.stringify({method, args}));
  } catch (err) {
    SignInformationFunctions.sendNotification(209, "Ошибка открытия соединения " + method);
  }
}

function getNCALayerSocket() {
  if (NCALayer.webSocket === null || NCALayer.webSocket === undefined || NCALayer.webSocket.readyState === 3 || NCALayer.webSocket.readyState === 2) {
    NCALayer.connector.initNCALayerSocket();
  }
  return NCALayer.webSocket;
}

const openDialogError = function () {
  const cofirmMsg = "NCALayer-ге қосылғанда қате шықты. NCALayer-ды қайта қосып, ОК-ді басыңыз\nОшибка при подключении к NCALayer. Запустите NCALayer и нажмите ОК";
  UIkit.modal.confirm(cofirmMsg, {labels: {ok: i18n.tr('ОК'), cancel: i18n.tr('Отмена')}}).then(() => {
    location.reload();
  }, () => null);
}

function pingLayer() {
  try {
    NCALayer.missed_heartbeats++;
    if (NCALayer.missed_heartbeats > NCALayer.missed_heartbeats_limit) throw new Error("exceeded timeLimit");
    getNCALayerSocket().send(NCALayer.heartbeat_msg);
  } catch (e) {
    clearInterval(NCALayer.connector.heartbeat_interval);
    NCALayer.connector.heartbeat_interval = null;
    SignInformationFunctions.sendNotification(301, "Превышено время ожидания. Попробуйте снова");
    getNCALayerSocket().close();
  }
}

function webSocketOnOpen(event) {
  SignInformationFunctions.sendNotification(200, "Соединение открыто");
}

function webSocketOnClose(event) {
  const {wasClean, code, reason} = event
  if(wasClean) {
    console.log("Соединение закрыто успешно");
  } else {
    openDialogError();
  }
  console.log(`[webSocketOnClose] - Code: ${code} Reason: ${reason}`);
}

function webSocketOnMessage(event) {
  if(event.data === NCALayer.heartbeat_msg) {
    NCALayer.missed_heartbeats = 0;
    return;
  }

  const data = JSON.parse(event.data);

  if(data) {
    const rw = {
      ...data,

      getResult: function () {
        return this?.result;
      },
      getMessage: function () {
        return this?.message;
      },
      getResponseObject: function () {
        return this?.responseObject;
      },
      getCode: function () {
        return this?.code;
      },
      getSecondResult: function () {
        return this?.secondResult;
      },
      getErrorCode: function () {
        return this?.errorCode;
      }
    };

    if(NCALayer.connector.callback != null) {
      SignInformationFunctions[NCALayer.connector.callback](rw);
      NCALayer.connector.callback = null;
    }
  }

  setMissedHeartbeatsLimitToMin();
}

const getKeyInfo = function (storageName, callBack) {
  const param = {
    "module": "kz.arta.synergy.signmodule",
    "method": "getKeyInfo",
    "args": [storageName]
  }
  NCALayer.connector.callback = callBack;
  getNCALayerSocket().send(JSON.stringify(param));
}

const getKeyInfoCall = function () {
  getKeyInfo(NCALayer.storageName, "getKeyInfoBack");
}

const getKeyInfoBack = function (result) {
  if(result['code'] === "500") {
    SignInformationFunctions.sendNotification(202, "Отмена действия");
  } else if(result['code'] === "200") {
    console.log('getKeyInfoBack', result);
  }
}

NCALayer.connector.getKeyInfoCallButton = function () {
  getKeyInfoCall();
}

NCALayer.connector.openChooseCertDialog = function () {
  const args = [NCALayer.storageName, 'P12', "~"];
  setTimeout(customSend, NCALayer.CALLING_TIMEOUT, NCALayer.METHOD_BROWSE_KEY_STORE, args, "getKeyPath");
}

NCALayer.connector.loadKeys = function () {
  const args = [NCALayer.storageName, NCALayer.storagePath, NCALayer.password, NCALayer.keysConstType];
  setTimeout(customSend, NCALayer.CALLING_TIMEOUT, NCALayer.METHOD_GET_KEYS, args, "loadKeysBack");
}

NCALayer.connector.getKeySubjectDN = function () {
  const args = [NCALayer.storageName, NCALayer.storagePath, NCALayer.key, NCALayer.password];
  setTimeout(customSend, NCALayer.CALLING_TIMEOUT, NCALayer.METHOD_GET_SUBJECT_DN, args, "getKeySubjectDNBack");
}

NCALayer.connector.loadExpireBefore = function () {
  const args = [NCALayer.storageName, NCALayer.storagePath, NCALayer.key, NCALayer.password];
  setTimeout(customSend, NCALayer.CALLING_TIMEOUT, NCALayer.METHOD_GET_NOT_BEFORE, args, "loadExpireBeforeBack");
}

NCALayer.connector.loadExpireAfter = function () {
  const args = [NCALayer.storageName, NCALayer.storagePath, NCALayer.key, NCALayer.password];
  setTimeout(customSend, NCALayer.CALLING_TIMEOUT, NCALayer.METHOD_GET_NOT_AFTER, args, "loadExpireAfterBack");
}

NCALayer.connector.signDocument = async function () {
  const { rawdata } = await AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/docflow/doc/document_info?documentID=${NCALayer.documentID}`);

  const param = {
    "module": "kz.arta.synergy.signmodule",
    "method": "signDocument",
    "args": ["", "SIGN", rawdata]
  }

  setTimeout(() => {
    NCALayer.connector.callback = "signDocumentSynergy";
    try {
      getNCALayerSocket().send(JSON.stringify(param));
    } catch (err) {
      console.log('signDocument error', err);
      SignInformationFunctions.sendNotification(210, "Ошибка подписания документа");
    }
  });
}

NCALayer.connector.webSocketOnClose = function () {
  try {
    getNCALayerSocket().close();
  } catch (error) {
    alert("Ошибка: обратитесь к разработчику системы - " + error);
  }
}
