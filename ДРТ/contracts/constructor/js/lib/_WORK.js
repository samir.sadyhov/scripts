const createSelectComponent = (label, items, multiple = false) => {
  const container = $('<div>', {class: 'uk-margin-small'});
  const fc = $('<div>', {class: 'uk-form-controls'});
  const select = $('<select class="uk-select" style="min-width: 100px;">');

  if(multiple) select.attr('multiple', 'multiple');
  if(items) items.sort((a,b) => a.value - b.value)
  .forEach(item => select.append(`<option value="${item.value}" title="${item.label}">${item.label}</option>`));

  fc.append(select);
  container.append(`<label class="uk-form-label uk-text-bold">${label}</label>`).append(fc);

  return {container, select};
}

const getCommentForDialog = async () => {
  const commentText = i18n.tr("Комментарий");
  const container = $('<div>', {class: 'uk-margin'});
  const label = $(`<label class="uk-form-label" for="textarea-comment-text">${commentText}</label>`);
  const textarea = $('<textarea class="uk-textarea" rows="5" id="textarea-comment-text"></textarea>');
  container.append(label).append(textarea);
  return {container, textarea};
}

const getFileForDialog = () => {
  const container = $('<div class="uk-margin" uk-margin style="width: 100%">');
  const formCustom = $('<div uk-form-custom="target: true" style="width: 100%;">');
  const inputFile = $('<input type="file">');
  formCustom.append(inputFile)
  .append(`<input class="uk-input uk-form-width-medium" type="text" style="font-size: 14px; color: #333; background: #fff; min-width: 245px; width: 100%;" placeholder="Прикрепить результат" disabled>`);
  container.append(formCustom);
  return {container, inputFile};
}

const getDocumentForDialog = childDocuments => {
  const container = $('<div class="uk-margin" uk-margin style="width: 100%">');

  const values = childDocuments.map(x => {
    return {label: x.name, value: x.documentID};
  });
  const {container: docsContainer, select} = createSelectComponent(i18n.tr("Выберите документ"), values);

  container.append(docsContainer);
  return {container, select};
}

const closeDocument = () => {
  history.pushState(null, null, '/contracts');
  Cons.setAppStore({dataUUID: null});
  fire({type: 'goto_page', pageCode: 'mainPage'}, 'root-panel');
}

this._WORK = {
  finishWork: async function(workID, formPlayer, successHandler) {
    Cons.showLoader();
    try {
      const resultFinish = await appAPI.finishWork({
          workID: workID,
          completionForm: "NOTHING"
      });

      if(!resultFinish) throw new Error('Произошла ошибка при завершении работы');

      if(successHandler && typeof successHandler == 'function') successHandler(resultFinish);

      Cons.hideLoader();
      showMessage('Работа завершена', 'success');
      closeDocument();

    } catch (err) {
      Cons.hideLoader();
      showMessage(err.message, 'error');
    }
  },

  acceptWork: async function(formPlayer, workInfo, successHandler) {
    Cons.showLoader();
    try {
      const {actionID, completionResultID} = workInfo;
      const acceptResult = await appAPI.acceptResultWork(completionResultID, actionID);

      if(!acceptResult) throw new Error('Произошла ошибка при завершении работы');

      if(successHandler && typeof successHandler == 'function') successHandler(acceptResult);

      Cons.hideLoader();
      showMessage('Работа завершена', 'success');
      closeDocument();

    } catch (err) {
      Cons.hideLoader();
      showMessage(err.message, 'error');
    }
  },

  finishForm: async function(workID, CompletionFormInfo, formPlayer, successHandler) {
    const {formCode, formID} = CompletionFormInfo;
    const formResult = await appAPI.getFormForResult(formCode, workID);
    const player = UTILS.getSynergyPlayer(formResult.dataUUID, true);

    const dialogTitle = i18n.tr("Форма завершения");
    const dialogButton = i18n.tr("Сохранить");
    const msgSuccess = i18n.tr("Работа завершена");

    const dialog = await UTILS.getModalDialog(dialogTitle, player.view.container, dialogButton, async () => {
      Cons.showLoader();
      try {
        if (!player.model.isValid()) throw new Error(i18n.tr('Заполните обязательные поля'));

        player.saveFormData(async saveResult => {
          const resultFinish = await appAPI.finishWork({
              workID: workID,
              completionForm: "FORM",
              type: 'work',
              file_identifier: formResult.file_identifier
          });

          if(!resultFinish) throw new Error(i18n.tr('Произошла ошибка при завершении работы'));
          if(successHandler && typeof successHandler == 'function') successHandler(resultFinish);

          Cons.hideLoader();
          showMessage(msgSuccess, 'success');
          UIkit.modal(dialog).hide();
          player.destroy();
          closeDocument();
        });

      } catch (err) {
        Cons.hideLoader();
        UIkit.modal(dialog).hide();
        player.destroy();
        showMessage(err.message, 'error');
      }
    });

    UIkit.modal(dialog).show();
    dialog.on('hidden', () => {
      dialog.remove();
    });
  },

  finishComment: async function(workID, formPlayer, successHandler) {
    const body = await getCommentForDialog();

    const dialogTitle = i18n.tr("Комментарий");
    const dialogButton = i18n.tr("Сохранить");
    const msgSuccess = i18n.tr("Работа завершена");

    const dialog = await UTILS.getModalDialog(dialogTitle, body.container, dialogButton, async () => {
      Cons.showLoader();
      try {
        const resultFinish = await appAPI.finishWork({
            workID: workID,
            completionForm: "COMMENT",
            comment: body.textarea.val()
        });

        if(!resultFinish) throw new Error('Произошла ошибка при завершении работы');
        if(successHandler && typeof successHandler == 'function') successHandler(resultFinish);

        Cons.hideLoader();
        showMessage(msgSuccess, 'success');
        UIkit.modal(dialog).hide();
        closeDocument();

      } catch (err) {
        Cons.hideLoader();
        UIkit.modal(dialog).hide();
        showMessage(err.message, 'error');
      }
    });

    UIkit.modal(dialog).show();
    dialog.on('hidden', () => {
      dialog.remove();
    });
  },

  finishFile: async function(workID, formPlayer, successHandler) {
    const body = getFileForDialog();
    const {container, inputFile} = body;

    const dialogTitle = i18n.tr("Файл");
    const dialogButton = i18n.tr("Сохранить");
    const msgSuccess = i18n.tr("Работа завершена");

    const dialog = await UTILS.getModalDialog(dialogTitle, container, dialogButton, async () => {
      Cons.showLoader();
      try {

        if(!inputFile[0].files.length) throw new Error('Необходимо прикрепить файл');

        const sourceFile = inputFile[0].files[0];
        const fileReader = new FileReader();

        if(sourceFile.size > 20971520) throw new Error('Размер файла не должен превышать 20 МБ');

        const filePath = await appAPI.startUpload();
        if(!filePath) throw new Error('Ошибка создания временного файла');

        fileReader.onload = async function (e) {

          const b64encoded = btoa(new Uint8Array(e.target.result).reduce((data, byte) => data + String.fromCharCode(byte), ''));
          const data = new FormData();
          data.append('body', b64encoded);

          const uploadResult = await appAPI.uploadPart(filePath.file, data);
          if(!uploadResult) throw new Error('Ошибка загрузки файла');
          if(uploadResult.errorCode != '0') throw new Error(uploadResult.errorMessage);

          const resultFinish = await appAPI.finishWork({
              workID: workID,
              completionForm: "FILE",
              type: 'file',
              file_name: sourceFile.name,
              file_path: filePath.file
          });

          if(!resultFinish) throw new Error('Произошла ошибка при завершении работы');
          if(successHandler && typeof successHandler == 'function') successHandler(resultFinish);

          Cons.hideLoader();
          showMessage(msgSuccess, 'success');
          UIkit.modal(dialog).hide();
          closeDocument();

        }
        fileReader.readAsArrayBuffer(sourceFile);

      } catch (err) {
        Cons.hideLoader();
        showMessage(i18n.tr(err.message), 'error');
      }
    });

    UIkit.modal(dialog).show();
    dialog.on('hidden', () => {
      dialog.remove();
    });
  },

  finishProcess: async function(workInfo, formPlayer, successHandler) {
    const {systemSettings} = Cons.getAppStore();
    const {needCert, enableEDS} = systemSettings;

    const {actionID, procInstID, name, parent_process, documentID} = workInfo;
    const procInfo = await appAPI.getProcessInfo(actionID);
    const {raw_data, demandSign, buttons} = procInfo;

    const dialog = await UTILS.getDialogFinishProcess(name, buttons, parent_process, async (signal, comment) => {
      Cons.showLoader();
      try {
        let succesMsg = 'Процесс завершен';

        const finishProcessBody = {
          procInstID, signal, comment,
          rawdata: raw_data
        };

        if(enableEDS && needCert && demandSign == "true") {
          NCALayer.sign('SIGN', documentID, async result => {
            Cons.showLoader();

            try {
              finishProcessBody.signdata = result.signedData;
              finishProcessBody.certificate = result.certificate;
              finishProcessBody.certID = result.certID;
              finishProcessBody.addSignature = true;
              succesMsg = 'Документ успешно подписан ЭЦП';

              const resultFinish = await appAPI.finishProcess(finishProcessBody);
              if(!resultFinish) throw new Error('Произошла ошибка при подписании документа');
              if(successHandler && typeof successHandler == 'function') successHandler(resultFinish);

              Cons.hideLoader();
              showMessage(i18n.tr(succesMsg), 'success');
              UIkit.modal(dialog).hide();
              closeDocument();
            } catch (e) {
              Cons.hideLoader();
              UIkit.modal(dialog).hide();
              showMessage(i18n.tr(e.message), 'error');
            }

          });
        } else {
          Cons.showLoader();

          const resultFinish = await appAPI.finishProcess(finishProcessBody);
          if(!resultFinish) throw new Error('Произошла ошибка при завершении процесса');
          if(successHandler && typeof successHandler == 'function') successHandler(resultFinish);

          Cons.hideLoader();
          showMessage(i18n.tr(succesMsg), 'success');
          UIkit.modal(dialog).hide();
          closeDocument();
        }

      } catch (err) {
        Cons.hideLoader();
        UIkit.modal(dialog).hide();
        showMessage(i18n.tr(err.message), 'error');
      }
    });

    UIkit.modal(dialog).show();
    dialog.on('hidden', () => {
      dialog.remove();
    });
  },

  finishDocument: async function(workID, childDocuments, formPlayer, successHandler) {
    try {
      if(!childDocuments || !Array.isArray(childDocuments)) {
        throw new Error(i18n.tr('Не удалось получить список дочерних документов'));
      }

      const body = await getDocumentForDialog(childDocuments);

      const dialogTitle = i18n.tr("Результат завершения");
      const dialogButton = i18n.tr("Готово");
      const msgSuccess = i18n.tr("Работа завершена");

      const dialog = await UTILS.getModalDialog(dialogTitle, body.container, dialogButton, async () => {
        Cons.showLoader();
        try {
          const child = childDocuments.find(x => x.documentID == body.select.val());

          const resultFinish = await appAPI.finishWork({
              workID: workID,
              completionForm: "DOCUMENT",
              documentID: child.documentID,
              document_title: child.name
          });

          if(!resultFinish) throw new Error(i18n.tr('Произошла ошибка при завершении работы'));
          if(successHandler && typeof successHandler == 'function') successHandler(resultFinish);

          Cons.hideLoader();
          showMessage(msgSuccess, 'success');
          UIkit.modal(dialog).hide();
          closeDocument();

        } catch (err) {
          Cons.hideLoader();
          UIkit.modal(dialog).hide();
          showMessage(err.message, 'error');
        }
      });

      UIkit.modal(dialog).show();
      dialog.on('hidden', () => {
        dialog.remove();
      });
    } catch (e) {
      Cons.hideLoader();
      showMessage(e.message, 'error');
    }
  }

}
