/*
model.dictionaryCode = 'location'; - код справочника
model.columnTitle = 'code_title'; - наименование
model.columnValue = 'code_value'; - значение
model.asfProperty.required = true; - признаяк обязательности заполнения

model.defaultValue = '0'; - дефолтное значение
*/

let select = $('<select class="asf-textBox uk-select">').css({"background-color": "#fff"});
let label = $(view.container).children(".asf-label");

view.container.append(select);

model.getAsfData = function(blockNumber){
  if(model.getValue()) {
    var result = AS.FORMS.ASFDataUtils.getBaseAsfData(model.asfProperty, blockNumber, model.getValue().value , model.getValue().key);
    result.valueID = model.getValue().key;
    return result;
  } else {
    return AS.FORMS.ASFDataUtils.getBaseAsfData(model.asfProperty, blockNumber);
  }
};

model.setAsfData = function(asfData){
  if(!asfData || !asfData.value) {
    return;
  }
  var value = {key: asfData.key, value: asfData.value};
  model.setValue(value);
};

model.getSpecialErrors = function() {
  if(model.asfProperty.required && !model.getValue()) {
    return {id : model.asfProperty.id, errorCode : AS.FORMS.INPUT_ERROR_TYPE.emptyValue};
  }
};

let valueUpdate = null;
view.updateValueFromModel = function () {
  if (model.getValue()) {
    valueUpdate = model.getValue();
    view.container.removeClass('asf-invalidInput');
    view.unmarkInvalid();
    label.text(valueUpdate.value);
    select.val(valueUpdate.key);
  } else if (valueUpdate) {
    model.setAsfData({key: valueUpdate.key, value: valueUpdate.value});
    view.container.removeClass('asf-invalidInput');
    view.unmarkInvalid();
    label.text(valueUpdate.value);
    select.val(valueUpdate.key);
  } else {
    label.text("");
    select.val("0");
  }
};

model.on(AS.FORMS.EVENT_TYPE.valueChange, function () {
  view.updateValueFromModel();
});

model.on(AS.FORMS.EVENT_TYPE.dataLoad, function () {
  view.updateValueFromModel();
});

view.markInvalid = function(){
  select.css({
    "background-color": "#fff3f3",
    "border": "1px solid #ecacad"
  });
  label.css({
    "background-color": "#fff3f3",
    "border": "1px solid #ecacad"
  });
};

view.unmarkInvalid = function(){
  select.css({
    "background-color": "#ffffff",
    "border": "1px solid #d6d6d6"
  });
  label.css({
    "background-color": "#ffffff",
    "border": ""
  });
};

const loadDictionary = code => {
  return new Promise(resolve => {
    try {
      AS.FORMS.ApiUtils.loadDictionary(code, AS.OPTIONS.locale, resolve);
    } catch (e) {
      console.log(e.message);
      resolve(null);
    }
  });
}

const fillSelect = items => {
  let emptyValue = AS.OPTIONS.locale == 'kk' ? 'Таңдалған жоқ' : 'Не выбрано';
  select.empty();
  items.forEach(item => select.append(`<option value="${item.value}">${item.title}</option>`));
  setTimeout(() => {
    if(model.hasOwnProperty('defaultValue') && model.defaultValue && !model.getValue()) {
      select.val(model.defaultValue).trigger('change');
    }
  }, 300);
  view.updateValueFromModel();
}

const initSelectDictionary = () => {
  loadDictionary(model.dictionaryCode).then(res => {
    let columnTitle = res.columns.find(x => x.code == model.columnTitle).columnID;
    let columnValue = res.columns.find(x => x.code == model.columnValue).columnID;
    let items = [];
    res.items.forEach(item => {
      let title = item.values.find(x => x.columnID == columnTitle).value;
      let value = item.values.find(x => x.columnID == columnValue).value;
      items.push({title, value});
    });
    items.sort((a,b) => a.value - b.value);
    fillSelect(items);
  });
}

if(editable) {
  select.show();
  label.hide();
} else {
  select.hide();
  label.show();
}

select.on('change', el => {
  let key = $(el.currentTarget).val();
  let value = $(el.currentTarget).find('option:selected').text();
  model.setAsfData({key: key, value: value});
});

initSelectDictionary();
