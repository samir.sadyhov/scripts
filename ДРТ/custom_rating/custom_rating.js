/*
Настройки на компоненте на форме:
model.dictionaryCode = 'experience_dict_assessment';  //код справочника
model.titleCode = 'name'; //код столбца с наименованием
model.valueCode = 'code'; //код столбца со сзначением
model.listCmpID = 'cjm_assessment'; //выпадающий список на форме с тем же справочником
*/

if(!model.dictionaryCode) return;

let dictCode = model.dictionaryCode;
let title = model.titleCode || 'title';
let value = model.valueCode || 'value';
let listModel = model.playerModel.getModelWithId(model.listCmpID);

if(!listModel) return;

function getDictionaryData(cb) {
  jQuery.when(AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/dictionary/get_by_code?dictionaryCode=${dictCode}`))
  .then(res => {
    if(res.errorCode) cb([]);
    cb(filteredDict(res));
  }).fail(error => {
    console.error(error);
    cb([]);
  });
}

function filteredDict(data) {
  let result = [];
  let titleID = data.columns.filter(column => column.code === title)[0].columnID;
  let valueID = data.columns.filter(column => column.code === value)[0].columnID;
  data.items.forEach(item => {
    let t, v;
    item.values.forEach(val => {
      if (val.columnID === titleID) {
        t = val.value
      } else if (val.columnID === valueID) {
        v = val.value
      }
    })
    result.push({
      title: t,
      value: v
    })
  })
  return result.sort((a, b) => b.value - a.value);
}

function changeStar() {
  listModel.setValue($(this).val().toString())
}

let rating = $('<div class="rating">');
view.container.append(rating);

getDictionaryData(data => {
  data.forEach(item => {
    if(item.value == listModel.getValue()) {
      item.checked = true;
    }
    rating.append($(`<input type="radio" ${item.checked ? "checked" : ""} id="star${item.value}" name="rating" value="${item.value}" />
    <label for="star${item.value}" title="${item.title}"></label>`)
    .on('change', changeStar))
  })
});
