const setFinishkDate = value => {
  if(!value) return;
  let finishDate = model.playerModel.getModelWithId('cjm_finishdate');
  if(!finishDate) return;
  if(value[0] == '3') {
    finishDate.setValue(AS.FORMS.DateUtils.formatDate(new Date(), AS.FORMS.DateUtils.DATE_FORMAT_FULL));
  } else {
    finishDate.setValue(null);
  }

}

if(editable) {
  if(model.customChange) return;
  model.on('valueChange', (_1, _2, value) => setFinishkDate(value));
  model.customChange = true;
}
