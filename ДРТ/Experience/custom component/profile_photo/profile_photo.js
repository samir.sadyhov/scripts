let container = $('<div>', {class: 'img-container'});
let photo = $('<img>', {class: 'photo'});
let formUser = model.playerModel.getModelWithId('cjm_user').getValue();
let photoURL = '';
let profile = localStorage.getItem('profile-photo');

if(formUser.length > 0) {
  photoURL = `${window.location.origin}/Synergy/load?userid=${formUser[0].personID}`;
}

if(profile && formUser.length > 0) {
  profile = JSON.parse(profile);
  if(profile.userID == formUser[0].personID) photoURL = profile.photo;
}

container.append(photo);
view.container.append(container);
photo.attr('src', photoURL);
