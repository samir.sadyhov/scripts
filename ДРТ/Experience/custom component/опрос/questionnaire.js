const getUrlParameter = sParam => {
  let sPageURL = window.location.search.substring(1), sURLVariables = sPageURL.split('&'), sParameterName;
  for (let i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split('=');
    if (sParameterName[0] === sParam)
    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
  }
}

const generateGuid = prefix => prefix+'-'+Math.random().toString(36).substring(2, 15)+Math.random().toString(36).substring(2, 15);

const searchInRegistry = async params => {
  return new Promise((resolve, reject) => {
    AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/data_ext?${$.param(params)}`, resolve, null, null, reject);
  });
}

const getAsfDatas = async uuids => {
  return new Promise((resolve, reject) => {
    AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/asforms/data/get?${uuids}`, resolve, null, null, reject);
  });
}

const getValue = (asfData, cmp) => asfData.data.find(x => x.id == cmp);

const parseAsfValue = asfDataValue => {
  if(!asfDataValue) return null;
  const {value = '', key = ''} = asfDataValue;
  return key ? {value, key} : {value};
}

const parseAsfTable = asfTable => {
  if(!asfTable.hasOwnProperty('data')) return [];
  const data = asfTable.data.filter(x => x.type != 'label');
  if(!data.length) return [];

  const result = [];
  const ids = data.map(x => x.id.slice(0, x.id.indexOf('-b'))).uniq();
  let tbi =  data.slice(-1)[0].id;
  tbi = Number(tbi.slice(tbi.indexOf('-b') + 2));

  for(let i = 1; i <= tbi; i++) {
    const item = {};
    ids.forEach(key => {
      const parseValue = parseAsfValue(getValue(asfTable, `${key}-b${i}`));
      if(parseValue) item[key] = parseValue;
    });
    result.push(item);
  }

  return result;
}

const parseAnswerData = asfData => {
  const {uuid, documentID} = asfData;
  const result = {uuid, documentID};
  result.num = Number(getValue(asfData, 'experience_form_question_num')?.value) || 0;
  result.question_ru = getValue(asfData, 'experience_form_question_text')?.value || '';
  result.question_kk = getValue(asfData, 'experience_form_question_text_kk')?.value || '';
  result.type = Number(getValue(asfData, 'experience_form_question_type')?.value) || null;
  result.statistic = Number(getValue(asfData, 'experience_form_question_statistic')?.value)  || 0;
  result.threshold = Number(getValue(asfData, 'experience_form_question_threshold')?.value) || 0;

  result.service = parseAsfValue(getValue(asfData, 'experience_form_question_service'));
  result.stage = parseAsfValue(getValue(asfData, 'experience_form_question_stage'));

  result.answers = parseAsfTable(getValue(asfData, 'experience_form_question_answersTable'));
  result.answers.sort((a,b) => a.answer_num?.value - b.answer_num?.value);
  return result;
}

const dataContainer = $('<div>', {class: 'questionnaire_container'});
view.container.append(dataContainer);

const render = {
  setJsonAnswer: function(){
    const json = parseAsfTable(this.table.getAsfData()[0]);
    json.forEach(item => {
      const {key} = item.question_reglink;
      const data = model.data.find(x => x.documentID == key);
      if(data) {
        item.service = data.service;
        item.stage = data.stage;
        item.statistic = data.statistic;
        item.coef = data.coef;
        item.question = data.question_ru;
        const answer = data.answers.find(x => x.answer_num.value == data.answer_num);
        if(answer) {
          item.answer_value = answer?.answer_ru?.value;
          item.answer_key = answer?.answer_num?.value;
        }
      }
    });
    model.playerModel.getModelWithId('experience_form_feedback_answersTableJSON').setValue(JSON.stringify(json));
  },

  radio: function(item){
    const {tableBlockIndex, tableID, answers, threshold, statistic} = item;
    const id = generateGuid('radio');
    const container = $('<div>');
    const fc = $('<div>', {class: 'uk-form-controls', style: "display: flex; flex-direction: column;"});
    const textarea = $(`<textarea class="uk-textarea" rows="2" placeholder="Комментарий" style="display: none;"></textarea>`);

    container.append(fc, textarea);

    textarea.on('change input', e => {
      model.playerModel.getModelWithId('user_comment', tableID, tableBlockIndex).setValue(textarea.val());
      this.setJsonAnswer();
    });

    answers.forEach(x => {
      const {answer_key, answer_num, answer_kz, answer_ru, coef, explanation} = x;
      let optionName = ' ' + answer_num?.value;
      if(AS.OPTIONS.locale == 'kk') {
        optionName += ' - ' + answer_kz?.value || '';
      } else {
        optionName += ' - ' + answer_ru?.value || '';
      }
      const label = $('<label>');
      const radio = $(`<input class="uk-radio" type="radio" name="${id}">`);

      label.append(radio, optionName);
      fc.append(label);

      radio.on('change', e => {
        if(statistic == 1 || statistic == 2) {
          const coefSelect = Number(coef?.key) || 0;
          if(coefSelect <= threshold) {
            textarea.fadeIn(100);
            model.playerModel.getModelWithId('answer_type', tableID, tableBlockIndex).setValue('2');
          } else {
            textarea.fadeOut(100, () => textarea.val('').trigger('change'));
            model.playerModel.getModelWithId('answer_type', tableID, tableBlockIndex).setValue('1');
          }
        }

        item.answer_num = answer_num?.value;
        item.coef = coef?.key;

        model.playerModel.getModelWithId('answer', tableID, tableBlockIndex).setValue(optionName.trim());
        model.playerModel.getModelWithId('explanation', tableID, tableBlockIndex).setValue(explanation.key);
        this.setJsonAnswer();
      });

    });
    return container;
  },

  checkbox: function(item){
    const {tableBlockIndex, tableID, answers} = item;
    const id = generateGuid('radio');
    const fc = $('<div>', {class: 'uk-form-controls', style: "display: flex; flex-direction: column;"});
    let values = [];

    answers.forEach(x => {
      const {answer_key, answer_num, answer_kz, answer_ru} = x;
      let optionName = ' ' + answer_num?.value;
      if(AS.OPTIONS.locale == 'kk') {
        optionName += ' - ' + answer_kz?.value || '';
      } else {
        optionName += ' - ' + answer_ru?.value || '';
      }
      const label = $('<label>');
      const checkbox = $(`<input class="uk-checkbox" type="checkbox"" name="${id}" value="${optionName.trim()}">`);
      label.append(checkbox, optionName);
      fc.append(label);

      checkbox.on('change', e => {
        const {value} = e.target;
        const i = values.indexOf(value);
        if(e.target.checked) {
          if(i == -1) values.push(value);
        } else {
          if(i > -1) values.splice(i, 1);
        }

        const finishValue = values.uniq().sort().join(';\n');
        model.playerModel.getModelWithId('answer', tableID, tableBlockIndex).setValue(finishValue);
        this.setJsonAnswer(item);
      });
    });
    return fc;
  },

  textarea: function(item){
    const {tableBlockIndex, tableID} = item;
    const textarea = $(`<textarea class="uk-textarea" rows="2"></textarea>`);
    textarea.on('change input', e => {
      model.playerModel.getModelWithId('answer', tableID, tableBlockIndex).setValue(textarea.val());
      this.setJsonAnswer();
    });
    return textarea;
  },

  init: function(table){
    this.table = table;

    model.data.forEach(item => {
      const {documentID, question_ru, question_kk, type} = item;
      const {locale} = AS.OPTIONS;
      const questionsContainer = $('<div>', {class: 'questions_container'});
      const answerContainer = $('<div>', {class: 'answer_container'});

      const {tableBlockIndex} = this.table.createRow();
      item.tableBlockIndex = tableBlockIndex;
      item.tableID = this.table.asfProperty.id;

      const questionReglink = model.playerModel.getModelWithId('question_reglink', item.tableID, tableBlockIndex);

      if(questionReglink) {
        questionReglink.setValue(documentID);

        if(locale == 'kk') {
          questionsContainer.append(`<h3>${question_kk}</h3>`);
        } else {
          questionsContainer.append(`<h3>${question_ru}</h3>`);
        }

        switch (type) {
          case 1: answerContainer.append(this.textarea(item)); break;
          case 2: answerContainer.append(this.checkbox(item)); break;
          case 3: answerContainer.append(this.radio(item)); break;
        }

        questionsContainer.append(answerContainer);
        dataContainer.append(questionsContainer);
      }

    });
  }
}

const initComponent = async () => {
  const table = model.playerModel.getModelWithId('experience_form_feedback_answersTable');

  if(table.modelBlocks.length) return;

  model.data = [];

  const searchParam = {
    registryCode: 'experience_registry_questions',
    field: 'experience_form_question_status',
    condition: 'TEXT_EQUALS',
    value: '1',
    loadData:  false
  };

  const stage = getUrlParameter('stage') || null;
  if(stage) {
    const dict = await AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/dictionary/get_by_code?dictionaryCode=experience_dict_stages&locale=${AS.OPTIONS.locale}`);
    const dictItem = dict.items.find(x => x.values[2]?.value == stage);
    if(dictItem) {
      searchParam.field1 = 'experience_form_question_stage';
      searchParam.condition1 = 'CONTAINS';
      searchParam.key1 = stage;

      const serviceModel = model.playerModel.getModelWithId('experience_form_question_service');
      const stageModel = model.playerModel.getModelWithId('experience_form_question_stage');

      if(serviceModel) serviceModel.setValue(dictItem.values[0].value);
      if(serviceModel) stageModel.setValue(dictItem.values[2].value);
    }
  }

  const searchResult = await searchInRegistry(searchParam);
  if(searchResult && searchResult.count > 0) {
    const uuids = 'dataUUID=' + searchResult.data.map(x => x.dataUUID).join('&dataUUID=');
    const asfDatas = await getAsfDatas(uuids);

    for(let i = 0; i < asfDatas.length; i++) {
      const asfData = asfDatas[i];
      asfData.documentID = await AS.FORMS.ApiUtils.getDocumentIdentifier(asfData.uuid);
      model.data.push(parseAnswerData(asfData));
    }

    model.data.sort((a,b) => a.num - b.num);

    render.init(table);
  } else {
    console.log('Не найдено данных для формирования опроса');
  }
}

initComponent();

Array.prototype.uniq = function() {
  return this.filter((v, i, a) => i == a.indexOf(v));
}
