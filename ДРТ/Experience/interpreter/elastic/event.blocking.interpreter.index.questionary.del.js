var result = true;
var message = "ok";

let ELASTIC_HOST = '127.0.0.1:9200';
let INDEX_NAME = 'experience_registry_assessment_survey'
let INDEX_TYPE = 'assessment'

function checkElasticServer(){
  let client = new org.apache.commons.httpclient.HttpClient();
  let get = new org.apache.commons.httpclient.methods.GetMethod("http://" + ELASTIC_HOST);
  client.executeMethod(get);
  get.getResponseBodyAsString();
  get.releaseConnection();
}

function deleteIndex(elasticKey) {
  let client = new org.apache.commons.httpclient.HttpClient();
  let del = new org.apache.commons.httpclient.methods.DeleteMethod("http://"+ELASTIC_HOST+"/"+INDEX_NAME+"/"+INDEX_TYPE+"/" + encodeURIComponent(elasticKey));
  client.executeMethod(del);
  let resp = del.getResponseBodyAsString();
  del.releaseConnection();
  return resp;
}

try {
  checkElasticServer(); // В случае недоступности сервера вернет ошибку:
                        // "В соединении отказано (Connection refused)"
                        // дальнейшее выполнение БП будет невозможно

  let currentFormData = API.getFormData(dataUUID);
  let json = UTILS.getValue(currentFormData, 'experience_form_feedback_answersTableJSON');

  if(!json || !json.hasOwnProperty('value')) throw new Error('Не удалось получить данные для индекса');
  json = JSON.parse(json.value);

  for(let i = 0; i < json.length; i++) {
    let item = json[i];
    if(!item.hasOwnProperty('statistic') || item.statistic == 0) continue;

    let questionID = item.question_reglink.key || '';

    let resultAddIndex = deleteIndex("question-" + questionID + '-' + dataUUID);
    log.info(":::::: RESULT DELETE INDEX ::::::", resultAddIndex);
  }

} catch (e) {
  log.error('[ DELETE INDEX questionary ] ' + err.message);
  message = err.message;
}
