var result = true;
var message = "ok";

let ELASTIC_HOST = '127.0.0.1:9200';
let INDEX_NAME = 'experience_registry_assessment_survey'
let INDEX_TYPE = 'assessment'

function checkElasticServer(){
  let client = new org.apache.commons.httpclient.HttpClient();
  let get = new org.apache.commons.httpclient.methods.GetMethod("http://" + ELASTIC_HOST);
  client.executeMethod(get);
  get.getResponseBodyAsString();
  get.releaseConnection();
}

function putInIndex(data, elasticKey) {
  let client = new org.apache.commons.httpclient.HttpClient();
  let put = new org.apache.commons.httpclient.methods.PutMethod("http://"+ELASTIC_HOST+"/"+INDEX_NAME+"/"+INDEX_TYPE+"/" + encodeURIComponent(elasticKey));
  let body = new org.apache.commons.httpclient.methods.StringRequestEntity(JSON.stringify(data), "application/json", "UTF-8");
  put.setRequestEntity(body);
  client.executeMethod(put);
  let resp = put.getResponseBodyAsString();
  put.releaseConnection();
  return JSON.parse(resp);
}

function formatDate(date) {
  date = date.split(' ');
  return date[0] + 'T' + date[1] + '.000Z';
}

function parseDict(dict) {
	let result = [];
  let codeID = dict.columns.filter(function(x) {if(x.code == 'code') return x})[0].columnID;
  let nameID = dict.columns.filter(function(x) {if(x.code == 'name') return x})[0].columnID;
  let coordinatesID = dict.columns.filter(function(x) {if(x.code == 'coordinates') return x})[0].columnID;

  dict.items.forEach(function(item){
    result.push({
    	name: item.values.filter(function(x) {if(x.columnID == nameID) return x})[0].value,
      code: item.values.filter(function(x) {if(x.columnID == codeID) return x})[0].value,
      coordinates: item.values.filter(function(x) {if(x.columnID == coordinatesID) return x})[0].value
    });
  });

  return result;
}

function getIndexData(item, currentFormData, locationDict) {
  let indexData = {feedbackUUID: dataUUID};
  let cjm_feedback_number = UTILS.getValue(currentFormData, 'cjm_feedback_number');
  let cjm_location = UTILS.getValue(currentFormData, 'cjm_location');
  let cjm_date_assessment = UTILS.getValue(currentFormData, 'cjm_date_assessment');
  let cjm_feedback_resource = UTILS.getValue(currentFormData, 'cjm_feedback_resource');
  let cjm_subdivision_name = UTILS.getValue(currentFormData, 'cjm_subdivision_name');
  let cjm_subdivision_name1 = UTILS.getValue(currentFormData, 'cjm_subdivision_name1');
  let cjm_location_dev = UTILS.getValue(currentFormData, 'cjm_location_dev');
  let cjm_status = UTILS.getValue(currentFormData, 'cjm_status');

  let cjmDictLocation = locationDict.filter(function(x){if(x.code == cjm_location.key) return x})[0];

  indexData.feedback_number = cjm_feedback_number.value || '';

  indexData.questionID = item.question_reglink.key || '';
  indexData.question = item.question || '';

  indexData.location_code = cjmDictLocation.code;
  indexData.location_name = cjmDictLocation.name;
  indexData.location_coordinates = cjmDictLocation.coordinates;

  indexData.cjm_date_assessment_value = cjm_date_assessment.value;
  indexData.cjm_date_assessment_key = cjm_date_assessment.key;
  indexData.cjm_date_assessment_formated = formatDate(cjm_date_assessment.key);

  indexData.cjm_status_value = cjm_status.value;
  indexData.cjm_status_key = Number(cjm_status.key);

  indexData.cjm_feedback_resource_value = cjm_feedback_resource.value;
  indexData.cjm_feedback_resource_key = cjm_feedback_resource.key;

  indexData.cjm_subdivision_name_value = cjm_subdivision_name.value || '';
  indexData.cjm_subdivision_name_key = cjm_subdivision_name.key || '';

  indexData.cjm_subdivision_name1_value = cjm_subdivision_name1.value || '';
  indexData.cjm_subdivision_name1_key = cjm_subdivision_name1.key || '';

  indexData.cjm_location_dev_value = cjm_location_dev.value || '';
  indexData.cjm_location_dev_key = cjm_location_dev.key || '';

  indexData.answer_value = item.answer_value || '';
  indexData.answer_key = Number(item.answer_key) || 0;

  indexData.explanation_value = item.explanation.value || '';
  indexData.explanation_key = Number(item.explanation.key) || 0;

  indexData.answer_type_value = item.answer_type.value || '';
  indexData.answer_type_key = item.answer_type.key || '';

  indexData.service_value = item.service.value || '';
  indexData.service_key = item.service.key || '';

  indexData.stage_value = item.stage.value || '';
  indexData.stage_key = item.stage.key || '';

  indexData.coef = Number(item.coef) || 0;

  item.statistic == 1 ? indexData.nps = 0 : indexData.nps = 1;

  return indexData;
}

try {
  checkElasticServer(); // В случае недоступности сервера вернет ошибку:
                        // "В соединении отказано (Connection refused)"
                        // дальнейшее выполнение БП будет невозможно

  let currentFormData = API.getFormData(dataUUID);
  let json = UTILS.getValue(currentFormData, 'experience_form_feedback_answersTableJSON');

  if(!json || !json.hasOwnProperty('value')) throw new Error('Не удалось получить данные для индекса');

  json = JSON.parse(json.value);

  let locationDict = API.httpGetMethod('rest/api/dictionary/get_by_code?dictionaryCode=experience_dict_location&locale=ru');
  locationDict = parseDict(locationDict);

  for(let i = 0; i < json.length; i++) {
    let item = json[i];
    if(!item.hasOwnProperty('statistic') || item.statistic == 0) continue;

    let indexData = getIndexData(item, currentFormData, locationDict);
    log.info(":::::: INDEX DATA ::::::", indexData);
    let resultAddIndex = putInIndex(indexData, "question-" + indexData.questionID + '-' + dataUUID);
    log.info(":::::: RESULT ADD INDEX ::::::", resultAddIndex);
  }

} catch (err) {
  log.error('[ INDEX questionary ] ' + err.message);
  message = err.message;
}
