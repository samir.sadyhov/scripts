var result = true;
var message = "ok";

function parseValue(val) {
  return ('0' + val).slice(-2);
}

try {
  let servicesID = "c4e4efd1-10ae-472c-b418-f0381c18ef83"; //боевой "f39ebded-2284-4a55-8cea-94c6c73fd794"
  let searchDate = '2021-02-28 23:59:59';

  let urlSearch = 'rest/api/registry/data_ext?registryCode=experience_registry_assessment';
  urlSearch += '&loadData=false';
  urlSearch += '&field=cjm_date_assessment&condition=LESS_OR_EQUALS&key=' + encodeURIComponent(searchDate);
  urlSearch += '&field1=cjm_service_item&condition1=CONTAINS&key1=' + servicesID;
  urlSearch += '&field2=cjm_assessment&condition2=LESS_OR_EQUALS&value2=5';

  let resultSearch = API.httpGetMethod(urlSearch);

  if(result.count == 0) throw new Error('Не найдено записей');

  resultSearch.data.forEach(function(item) {
    log.info("REPLACE_ASSESSMENT item", item);
    let assessmentFormData = API.getFormData(item.dataUUID);
    let assessment = UTILS.getValue(assessmentFormData, 'cjm_assessment');
    let newKey = assessment.key * 2;

    let mergeData = [
      {
        "id": "cjm_assessment",
        "type": "radio",
        "value": parseValue(newKey),
        "key": String(newKey)
      }
    ];

    log.info("REPLACE_ASSESSMENT old", assessment);
    log.info("REPLACE_ASSESSMENT new", mergeData);
    log.info("REPLACE_ASSESSMENT merge", API.mergeFormData({uuid: item.dataUUID, data: mergeData}));
  });

} catch (err) {
  log.error("REPLACE_ASSESSMENT", err.message);
  message = err.message;
}
