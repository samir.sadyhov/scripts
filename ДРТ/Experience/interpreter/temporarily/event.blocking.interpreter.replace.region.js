var result = true;
var message = "ok";

try {
  let searchDate = '2021-02-28 23:59:59';

  let urlSearch = 'rest/api/registry/data_ext?registryCode=experience_registry_assessment';
  urlSearch += '&loadData=false';
  urlSearch += '&field=cjm_date_assessment&condition=LESS_OR_EQUALS&key=' + encodeURIComponent(searchDate);
  urlSearch += '&countInPart=2';

  let resultSearch = API.httpGetMethod(urlSearch);

  if(result.count == 0) throw new Error('Не найдено записей');

  resultSearch.data.forEach(function(item) {
    log.info("REPLACE_REGION item", item);

    let mergeData = [];
    mergeData.push({
      "id": "cjm_location",
      "type": "listbox",
      "key": "01",
      "value": "г. Нур-Султан"
    });
    mergeData.push({
      "id": "cjm_location1",
      "type": "listbox",
      "key": "01",
      "value": "г. Нур-Султан"
    });

    log.info("REPLACE_REGION data", mergeData);
    log.info("REPLACE_REGION merge", API.mergeFormData({uuid: item.dataUUID, data: mergeData}));
  });

} catch (err) {
  log.error("REPLACE_REGION", err.message);
  message = err.message;
}
