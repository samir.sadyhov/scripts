function customFormatDate(datetime){
  datetime = datetime.split(/\D/);
  return datetime[2] + '.' + datetime[1] + '.' + datetime[0] + ' ' + datetime[3] + ':' + datetime[4];
}

function setDepartment(formData){
  let subdivision_name = UTILS.getValue(formData, 'cjm_subdivision_name');
  if(subdivision_name && subdivision_name.hasOwnProperty('key')) {
    let locationFormData = API.getFormData(API.getAsfDataId(subdivision_name.key));
    let dep = UTILS.getValue(locationFormData, 'experience_form_subdivision_dev');
    if(dep) UTILS.setValue(formData, 'cjm_subdivision_dev', dep);
    dep = UTILS.getValue(locationFormData, 'experience_form_subdivision_location_dev');
    if(dep) UTILS.setValue(formData, 'cjm_location_dev', dep);
  }
}

var result = true;
var message = "ok";

try {
  let currentFormData = API.getFormData(dataUUID);
  let currentDate = UTILS.getCurrentDateParse();
  let haveComments = false;
  let reviewType = false;
  let table = UTILS.getValue(currentFormData, 'experience_form_feedback_answersTable');

  if(table && table.hasOwnProperty('data')) {
    table.data.forEach(function(item){
      if(item.type == 'textarea' && item.id.slice(0,12) == 'user_comment' && item.hasOwnProperty('value') && item.value.trim().length > 0) haveComments = true;

      // Тип отзыва отрицательный
      if(item.type == 'listbox' && item.id.slice(0,11) == 'answer_type' && item.hasOwnProperty('key') && item.key == '2') reviewType = true;
    });
  }

  //Установка даты отправки
  UTILS.setValue(currentFormData, 'cjm_date_assessment', {key: currentDate, value: customFormatDate(currentDate)});

  //установка признака наличия комментария
  if(haveComments) {
    UTILS.setValue(currentFormData, 'cjm_comment_sign', {
      type: 'radio',
      key: 'Да',
      value: '01'
    });
  } else {
    UTILS.setValue(currentFormData, 'cjm_comment_sign', {
      type: 'radio',
      key: 'Нет',
      value: '02'
    });
  }

  //смотрим тип отзыва и выставляем нужные статусы
  if(reviewType) {
    UTILS.setValue(currentFormData, 'cjm_type', {
      type: 'listbox',
      key: '2',
      value: 'Отрицательный'
    });
    UTILS.setValue(currentFormData, 'cjm_status', {
      type: 'listbox',
      key: '1',
      value: 'Новый'
    });
  } else {
    UTILS.setValue(currentFormData, 'cjm_status', {
      type: 'listbox',
      key: '3',
      value: 'Закрыт'
    });
    // устанавливает дату закрытия равную дате регистрации
    UTILS.setValue(currentFormData, 'cjm_finishdate', {key: currentDate, value: customFormatDate(currentDate)});
  }

  setDepartment(currentFormData);

  API.mergeFormData(currentFormData);
} catch (err) {
  log.error(err.message);
  message = err.message;
}
