var result = true;
var message = "ok";

try {
  let currentFormData = API.getFormData(dataUUID);
  let assessment = [];

  currentFormData.data.forEach(function(item) {
    if(item.type == 'radio' && item.id.slice(0,14) == 'cjm_assessment' && item.hasOwnProperty('key')) assessment.push(item);
  });

  assessment = assessment.filter(function(x){if(x.key < 4) return x;});
  assessment.forEach(function(item) {
    let assessmentLink = UTILS.getValue(currentFormData, 'cjm_assessment_copy' + item.id.slice(14));
    if(assessmentLink && assessmentLink.hasOwnProperty('key')) {
      let workAnswer = UTILS.getValue(currentFormData, 'cjm_work' + item.id.slice(14));
      if(workAnswer && workAnswer.hasOwnProperty('value')) {
        let data = [];
        let assessmentUuid = API.getAsfDataId(assessmentLink.key);
        data.push({id: 'cjm_work', type: 'textarea', value: workAnswer.value});
        UTILS.setValue(data, 'cjm_responsibleManager_copy', UTILS.getValue(currentFormData, 'cjm_responsibleManager'));
        API.mergeFormData({uuid: assessmentUuid, data: data});
      }
    }
  });

} catch (err) {
  log.error(err.message);
  message = err.message;
}
