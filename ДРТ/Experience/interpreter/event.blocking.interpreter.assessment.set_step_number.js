var result = true;
var message = "ok";

try {
  let currentFormData = API.getFormData(dataUUID);
  let service = UTILS.getValue(currentFormData, 'cjm_service_item');

  if(!service || !service.hasOwnProperty('key')) throw new Error('не выбран этап');

  let serviceFormData = API.getFormData(API.getAsfDataId(service.key));
  let stepNumber = UTILS.getValue(serviceFormData, 'experience_form_serviceStep_number');

  UTILS.setValue(currentFormData, 'cjm_serviceStep_number', stepNumber);

  API.mergeFormData(currentFormData);

} catch (err) {
  log.error(err.message);
  message = err.message;
}
