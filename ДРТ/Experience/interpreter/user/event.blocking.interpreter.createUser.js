function fetchUserFullName(lastname, firstname, patronymic) {
  firstname = firstname ? ' ' + firstname.charAt(0) + '.' : '';
  patronymic = patronymic ? ' ' + patronymic.charAt(0) + '.' : '';
  return lastname + firstname + patronymic;
}

function createNewUser(params) {
  let client = API.getHttpClient();
  let post = new org.apache.commons.httpclient.methods.PostMethod("http://127.0.0.1:8080/Synergy/rest/api/filecabinet/user/save");
  for(let key in params) post.addParameter(key, params[key]);
  post.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=utf-8");
  let resp = client.executeMethod(post);
  resp = JSON.parse(post.getResponseBodyAsString());
  post.releaseConnection();
  return resp;
}

function generatePassword() {
  let charSet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-+_';
  return Array.apply(null, Array(8)).map(function() {
    return charSet.charAt(Math.random() * charSet.length);
  }).join('');
}

function transliterate(text) {
  return text
    .replace(/\u0401/g, 'YO').replace(/\u0419/g, 'I').replace(/\u0426/g, 'TS')
    .replace(/\u0423/g, 'U').replace(/\u041A/g, 'K').replace(/\u0415/g, 'E')
    .replace(/\u041D/g, 'N').replace(/\u0413/g, 'G').replace(/\u0428/g, 'SH')
    .replace(/\u0429/g, 'SCH').replace(/\u0417/g, 'Z').replace(/\u0425/g, 'H')
    .replace(/\u042A/g, '').replace(/\u0451/g, 'yo').replace(/\u0439/g, 'i')
    .replace(/\u0446/g, 'ts').replace(/\u0443/g, 'u').replace(/\u043A/g, 'k')
    .replace(/\u0435/g, 'e').replace(/\u043D/g, 'n').replace(/\u0433/g, 'g')
    .replace(/\u0448/g, 'sh').replace(/\u0449/g, 'sch').replace(/\u0437/g, 'z')
    .replace(/\u0445/g, 'h').replace(/\u044A/g, "'").replace(/\u0424/g, 'F')
    .replace(/\u042B/g, 'I').replace(/\u0412/g, 'V').replace(/\u0410/g, 'a')
    .replace(/\u041F/g, 'P').replace(/\u0420/g, 'R').replace(/\u041E/g, 'O')
    .replace(/\u041B/g, 'L').replace(/\u0414/g, 'D').replace(/\u0416/g, 'ZH')
    .replace(/\u042D/g, 'E').replace(/\u0444/g, 'f').replace(/\u044B/g, 'i')
    .replace(/\u0432/g, 'v').replace(/\u0430/g, 'a').replace(/\u043F/g, 'p')
    .replace(/\u0440/g, 'r').replace(/\u043E/g, 'o').replace(/\u043B/g, 'l')
    .replace(/\u0434/g, 'd').replace(/\u0436/g, 'zh').replace(/\u044D/g, 'e')
    .replace(/\u042F/g, 'Ya').replace(/\u0427/g, 'CH').replace(/\u0421/g, 'S')
    .replace(/\u041C/g, 'M').replace(/\u0418/g, 'I').replace(/\u0422/g, 'T')
    .replace(/\u042C/g, "'").replace(/\u0411/g, 'B').replace(/\u042E/g, 'YU')
    .replace(/\u044F/g, 'ya').replace(/\u0447/g, 'ch').replace(/\u0441/g, 's')
    .replace(/\u043C/g, 'm').replace(/\u0438/g, 'i').replace(/\u0442/g, 't')
    .replace(/\u044C/g, "'").replace(/\u0431/g, 'b').replace(/\u044E/g, 'yu')
    .replace(/\u0020/g, '_');
}

var result = true;
var message = 'ok';

try {
  let currentFormData = API.getFormData(dataUUID);

  let lastname = UTILS.getValue(currentFormData, "cjm_user_lastname");
  let firstname = UTILS.getValue(currentFormData, "cjm_user_firstname");
  let patronymic = UTILS.getValue(currentFormData, "cjm_user_patronymic");
  let email = UTILS.getValue(currentFormData, "cjm_user_email");
  let position = UTILS.getValue(currentFormData, "cjm_user_position");
  let access = UTILS.getValue(currentFormData, "cjm_user_access");

  if(!lastname || !lastname.hasOwnProperty('value')) throw new Error('Не заполнена фамилия пользователя');
  if(!firstname || !firstname.hasOwnProperty('value')) throw new Error('Не заполнено имя пользователя');
  if(!email || !email.hasOwnProperty('value')) throw new Error('Не заполнен адрес эл. почты');
  if(!position || !position.hasOwnProperty('key')) throw new Error('Не выбрана должность');

  if(!patronymic || !patronymic.hasOwnProperty('value')) {
    patronymic = '';
  } else {
    patronymic = patronymic.value;
  }

  let accessValue = access.value;
  access = access.key == 1 ? true : false;

  let newPassword = generatePassword();
  let fullName = fetchUserFullName(lastname.value, firstname.value, patronymic);

  let user = createNewUser({
    lastname: lastname.value,
    firstname: firstname.value,
    patronymic: patronymic,
    login: email.value,
    password: newPassword,
    pointersCode: transliterate(lastname.value + ' ' + firstname.value),
    hasAccess: access,
    isAdmin: false,
    isChancellery: false,
    isConfigurator: false
  });

  if(user.errorCode == '13') throw new Error(user.errorMessage);
  message = user.errorMessage;

  API.mergeFormData({
    uuid: dataUUID,
    data: [{
      id: "cjm_user",
      type: "entity",
      key: user.userID,
      value: fullName
    }]
  });

  //Назначение на должность
  let apiRes = API.httpGetMethod('rest/api/positions/appoint?positionID=' + position.key + '&userID=' + user.userID);
  message += '\n|| Назначение на должность: ' + apiRes.errorMessage;

  // отправка письма
  apiRes = API.sendNotification({
    header: 'Уведомление о создании новой учетной записи в системе.',
    message: 'Для вас была создана учетная запись для входа в систему.<br>Ваш логин <b>' + email.value + '</b> <br>Ваш пароль <b>' + newPassword + '</b><br>После входа в систему необходимо сменить пароль.',
    emails: [email.value]
  });

  if(apiRes.errorCode != 0) {
    message += '\n|| Произошла ошибка отправки уведомления.';
  } else {
    message += '\n|| Уведомление успешно отправлено.';
  }

  message += '\n|| Доступ в систему: ' + accessValue;

} catch (err) {
  log.error(err, err.message);
  result = false;
  message = err.message;
}
