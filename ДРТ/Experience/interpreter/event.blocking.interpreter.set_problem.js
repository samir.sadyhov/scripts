var result = true;
var message = "ok";

try {
  let currentFormData = API.getFormData(dataUUID);
  let meaning = API.getDocMeaningContent(documentID);
  let table = UTILS.getValue(currentFormData, 'experience_form_problem_pdg_table_negative');

  if(!table || !table.hasOwnProperty('data')) throw new Error('не выбраны отзывы');

  let surveyAsfData = [];

  surveyAsfData.push({
    id: 'experience_form_question_problem_exist',
    type: 'check',
    keys: ["Да"],
    values: ["1"]
  });

  surveyAsfData.push({
    id: 'experience_form_question_problem_link',
    type: 'reglink',
    key: documentID,
    value: meaning,
    valueID: documentID
  })

  table.data.forEach(function(item){
    let surveyAsfDataUUID = API.getAsfDataId(item.key);
    API.mergeFormData({
      uuid: surveyAsfDataUUID,
      data: surveyAsfData
    });
  });

} catch (err) {
  log.error(err.message);
  message = err.message;
}
