var result = true;
var message = "ok";

try {
  let currentFormData = API.getFormData(dataUUID);

  let cjm_subdivision_name = UTILS.getValue(currentFormData, 'cjm_subdivision_name');
  let cjm_subdivision_dev = UTILS.getValue(currentFormData, 'cjm_subdivision_dev');

  if(cjm_subdivision_dev && cjm_subdivision_dev.hasOwnProperty('key') && cjm_subdivision_dev.key)
  throw new Error('поле подразделение в текущей записе уже заполнено, пропускаем запись');

  if(!cjm_subdivision_name) throw new Error('поле cjm_subdivision_name не заполнено');
  if(!cjm_subdivision_name.hasOwnProperty('key')) throw new Error('поле cjm_subdivision_name не заполнено');

  let depFormData = API.getFormData(API.getAsfDataId(cjm_subdivision_name.key));
  let department = UTILS.getValue(depFormData, 'experience_form_subdivision_dev');

  if(!department) throw new Error('не заполнена ссылка на подразделние в реестре подразделений');
  if(!department.hasOwnProperty('key')) throw new Error('не заполнена ссылка на подразделние в реестре подразделений');

  cjm_subdivision_dev = {
    id: 'cjm_subdivision_dev',
    type: "entity",
    key: department.key,
    value: department.value
  };

  API.mergeFormData({
    uuid: currentFormData.uuid,
    data: [cjm_subdivision_dev]
  });

} catch (err) {
  log.error(err.message);
  message = err.message;
}
