

function getColumnID(dict, code) {
	return dict.columns.filter(function(x){if(x.code == code) return x})[0].columnID;
}

function getDictValue(item, id){
	return item.values.filter(function(x){if(x.columnID == id) return x})[0].value;
}

function getCoordinates(formData){
  try {
    let dict = API.httpGetMethod('rest/api/dictionary/get_by_code?dictionaryCode=experience_dict_location');
    let cjm_location = UTILS.getValue(formData, 'cjm_location');
    let columnIDcode = getColumnID(dict, 'code');
    let columnIDcoordinates = getColumnID(dict, 'coordinates');
    let coordinates = null;
    dict.items.forEach(function(item){
      if(getDictValue(item, columnIDcode) == cjm_location.key) coordinates = getDictValue(item, columnIDcoordinates);
    });
    if(coordinates) {
      return {id: 'cjm_location_map', type: 'listbox', key: coordinates, value: cjm_location.value}
    } else {
      return null;
    }
  } catch (e) {
    return null;
  }
}

var result = true;
var message = "ok";

try {
  let currentFormData = API.getFormData(dataUUID);
  let assessmentData = [];
  let reviewType = false;
  let matching = [];
  let coordinates = getCoordinates(currentFormData);

  matching.push({from: 'cjm_name', to: 'cjm_name_psc'});
  matching.push({from: 'cjm_phone', to: 'cjm_phone_psc'});
  matching.push({from: 'cjm_subdivision_name', to: 'cjm_subdivision_name'});
  matching.push({from: 'cjm_service_name', to: 'cjm_service_name'});
  matching.push({from: 'cjm_date_assessment', to: 'cjm_date_assessment'});
  matching.push({from: 'cjm_feedback_resource', to: 'cjm_feedback_resource'});
  matching.push({from: 'cjm_subdivision_dev', to: 'cjm_subdivision_dev'});
  matching.push({from: 'cjm_location', to: 'cjm_location1'});

  currentFormData.data.forEach(function(item) {
    if(item.type == 'radio' && item.id.slice(0,14) == 'cjm_assessment' && item.hasOwnProperty('key')) assessmentData.push(item);
  });

  assessmentData.forEach(function(item) {
    let data = [];
    let qNumber = item.id.length == 15 ? item.id.slice(-1) : item.id.slice(-2);
    let question = UTILS.getValue(currentFormData, 'cjm_question' + qNumber);
    let comment = UTILS.getValue(currentFormData, 'cjm_assessment_comment' + qNumber);

    if(item && item.hasOwnProperty('value') && item.hasOwnProperty('key')) {
      data.push({
        id: 'cjm_assessment',
        type: item.type,
        value: item.value,
        key: item.key
      });
      if(item.key < 4) reviewType = true;
    }
    if(question && question.hasOwnProperty('value') && question.hasOwnProperty('key')) {
      data.push({
        id: 'cjm_service_item',
        type: 'reglink',
        value: question.value,
        key: question.key
      });
    }
    if(comment && comment.hasOwnProperty('value')) {
      data.push({
        id: 'cjm_comment',
        type: 'textarea',
        value: comment.value
      });
    }

    if(UTILS.getValue(currentFormData, 'cjm_feedback_number')) {
      data.push({
        id: "cjm_feedback_number_copy",
        type: "textbox",
        value: UTILS.getValue(currentFormData, 'cjm_feedback_number').value || ''
      });
    }

    if(coordinates) data.push(coordinates);

    data.push({
      type: 'reglink',
      id: 'cjm_feedback_resource_link',
      key: documentID,
      valueID: documentID,
      value: API.getDocMeaningContent(documentID)
    })

    matching.forEach(function(id) {
      let fromData = UTILS.getValue(currentFormData, id.from);
      if(fromData && fromData.hasOwnProperty('value')) {
        let field = {id: id.to};
        for(let key in fromData) {
          if(key === 'id' || key === 'username' || key === 'userID') continue;
          field[key] = fromData[key];
        }
        data.push(field);
      }
    });

    let newDoc = API.httpPostMethod("rest/api/registry/create_doc_rcc", {
      registryCode: 'experience_registry_assessment',
      sendToActivation: true,
      data: data
    }, "application/json; charset=utf-8");

    if(UTILS.getValue(currentFormData, 'cjm_assessment_copy' + qNumber)) {
      UTILS.setValue(currentFormData, 'cjm_assessment_copy' + qNumber, {
        type: 'reglink',
        key: newDoc.documentID,
        valueID: newDoc.documentID,
        value: API.getDocMeaningContent(newDoc.documentID)
      });
    }
  });

  //http://gitlab.lan.arta.kz/commerce/synergy-experience/-/issues/56
  if(reviewType) {
    UTILS.setValue(currentFormData, 'cjm_type', {
      type: 'listbox',
      key: '2',
      value: 'Отрицательный'
    });
    UTILS.setValue(currentFormData, 'cjm_status', {
      type: 'listbox',
      key: '1',
      value: 'Новый'
    });
  } else {
    UTILS.setValue(currentFormData, 'cjm_status', {
      type: 'listbox',
      key: '3',
      value: 'Закрыт'
    });
    // устанавливает дату закрытия равную дате регистрации
    UTILS.setValue(currentFormData, 'cjm_finishdate', UTILS.getValue(currentFormData, 'cjm_date_assessment'));
  }

  API.mergeFormData(currentFormData);

} catch (err) {
  log.error(err.message);
  message = err.message;
}
