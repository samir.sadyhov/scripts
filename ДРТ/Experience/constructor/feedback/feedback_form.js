const getUrlParameter = sParam => {
  let sPageURL = window.location.search.substring(1), sURLVariables = sPageURL.split('&'), sParameterName;
  for (let i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split('=');
    if (sParameterName[0] === sParam)
    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
  }
}

const formIsSubmitted = () => {
  const storage = JSON.parse(localStorage.getItem('send_feedback_form'));
  if(!storage) return false;
  const curLoc = getUrlParameter('location') || null;
  if(curLoc == storage.location) return (Number(storage.sendDate) - new Date().getTime()) > 0;
  return false;
}

$('#localeSelector>[value="en"]').hide();
$('#localeSelector>[value="kk"]').text('KZ');
$('#localeSelector>[value="ru"]').text('RU');

AS.OPTIONS.locale = $("#localeSelector").val();

pageHandler("start_page", () => {
  if(formIsSubmitted()) {
    fire({type: 'goto_page', pageCode: 'no_send_form'}, 'root-panel');
  } else {
    fire({type: 'goto_page', pageCode: 'feedback'}, 'root-panel');
  }
});

const hideFields = ['cjm_subdivisionLabel', 'cjm_subdivision_name', 'cjm_location', 'cjm_locationLabel'];
const formPlayerId = "asf-assessment";
const find = ["cjm_phone", "cjm_subdivision_name"];

pageHandler("feedback", () => {
  const fields = {};
  let asform = null;

  if(!Cons.getAppStore().feedback_listener) {
    addListener("loaded_form_data", formPlayerId, event => {
      asform = event;

      const loc = getUrlParameter('location');
      const resource = getUrlParameter('resource');

      if(loc) {
        let url = 'api/registry/data_ext?registryCode=experience_registry_subdivision&countInPart=1&pageNumber=0';
        url += '&fields=experience_form_subdivision_location&fields=experience_form_subdivision_name';
        url += `&field=experience_form_subdivision_index&condition=TEXT_EQUALS&value=${loc}`;
        rest.synergyGet(url, res => {
          if(res.recordsCount !== 0) {
            asform.model.getModelWithId('cjm_location').setValue(res.result[0].fieldKey['experience_form_subdivision_location']);
            asform.model.getModelWithId('cjm_subdivision_name').setAsfData({
              key: res.result[0].documentID,
              value: res.result[0].fieldValue['experience_form_subdivision_name']
            });
            asform.view.getViewWithId('cjm_subdivision_name').container.find('select').trigger('change');

            hideFields.forEach(cmp => {
              if(asform.view.getViewWithId(cmp)) asform.view.getViewWithId(cmp).container.parent().parent().hide();
            });
          }
        });
      }

      if(resource) asform.model.getModelWithId('cjm_feedback_resource').setValue(resource);

      let inputPhone = $('[data-asformid="textbox.input.cjm_phone"]');
      new IMask(inputPhone[0], {
        mask: '+{7}-000-000-00-00',
        lazy: false
      });

      inputPhone.on('keydown', e => {
        if ([35, 36, 37, 38, 39, 40].indexOf(e.keyCode) !== -1) {
          e.preventDefault();
          return;
        }
      });

    });

    addListener("button_click", "btn-send", () => {
      find.forEach(elem => {
        if(asform.model.getModelWithId(elem)) {
          fields[elem] = {};
          fields[elem].model = asform.model.getModelWithId(elem);
          fields[elem].view = asform.view.getViewWithId(elem);
          $($(fields[elem].view.container).find(".asf-textBox")).addClass("uk-input");
          $($(fields[elem].view.container).find(".asf-dropdown-input")).addClass("uk-select");

          let errorType = 'emptyValue';
          let errorMsg = "Пустое поле ввода";

          switch (elem) {
            case "cjm_phone":
              errorMsg = "Не верно заполнен телефон";
              errorType = 'wrongValue';
              break;
            case "cjm_subdivision_name":
              errorMsg = "Не выбран ЦОН";
              break;
          };

          fields[elem].model.getSpecialErrors = function() {
            if (!fields[elem].model.getValue() ||
            (fields[elem].model.asfProperty.type == "textbox" && fields[elem].model.getValue().indexOf('_') !== -1)) {
              if (!$(fields[elem].view.container).find("div.error").length) {
                $(fields[elem].view.container).append(`<div class='error' style='color:#f0506e;padding-left:10px;'>${errorMsg}</span>`);
              }
              return {
                id: fields[elem].model.asfProperty.id,
                errorCode: AS.FORMS.INPUT_ERROR_TYPE[errorType]
              }
            };
            $(fields[elem].view.container).find("div.error").remove();
            return 0;
          }

        }
      });

      if (!asform.model.getErrors().length) {
        fire({type: "set_disabled", disabled: true, cmpID: "btn-send"}, "btn-send");
        fire({
          type: "create_form_data",
          registryCode: "experience_registry_assessment_survey",
          activate: true,
          success: (id, docid) => {
            let sd = new Date();
            sd.setDate(sd.getDate() + 14);
            localStorage.setItem('send_feedback_form', JSON.stringify({
              sendDate: sd.getTime(),
              location: getUrlParameter('location') || null
            }));
          },
          error: (st, err) => {
            Cons.hideLoader();
            showMessage('Произошла ошибка при отправке отзыва, попробуйте позже', 'error');
            console.log("create_form_data failed",  err);
          }
        }, formPlayerId);
      } else {
        Cons.hideLoader();
        showMessage('Заполните обязательные поля', 'error');
      }
    });

    Cons.setAppStore({feedback_listener: true});
  }

  $("#localeSelector").off().on('change', function() {
    AS.OPTIONS.locale = this.value;
    fire({type: 'show_form', formCode: 'experience_form_assessment_survey'}, formPlayerId);
  });
});
