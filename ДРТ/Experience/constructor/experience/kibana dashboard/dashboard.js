const pageCode = Cons.getCurrentPage().code;
const dashboardName = {};

switch (pageCode) {
  case 'dashboards':
    dashboardName.PC = 'CJM';
    dashboardName.mobile = 'CJM-mobile';
    break;
  case 'analytics_ms':
    dashboardName.PC = 'MS';
    dashboardName.mobile = 'MS-mobile';
    break;
}

const formatDate = datetime => {
  return datetime.getFullYear() + '-' + ('0' + (datetime.getMonth() + 1)).slice(-2) + '-' + ('0' + datetime.getDate()).slice(-2);
}

let dashboardURL = window.location.origin.match(/((?:\d{1,3}[\.]){3}\d{1,3})/);
dashboardURL = dashboardURL ? '//' + dashboardURL[1] + ':5601' : window.location.origin;

if($(document).width() < 781) {
  dashboardURL += `/kibana/app/kibana#/dashboard/${dashboardName.mobile}?embed=true`;
} else {
  dashboardURL += `/kibana/app/kibana#/dashboard/${dashboardName.PC}?embed=true`;
}

let currentDate = new Date();
let refreshPeriodInSecond = 60; // 1 минута
let defaultPeriod = `time:(from:'${formatDate(new Date(currentDate.getFullYear(), 0))}T00:00:00.000',mode:absolute,to:'${formatDate(currentDate)}T23:59:59.999')`;
let defaultParam = `refreshInterval:(display:Off,pause:!f,value:${refreshPeriodInSecond*1000})`;
let newURL = `${dashboardURL}&_g=(${defaultParam},${defaultPeriod})`;

let container = $('.dashboard-container');
let iFrame = $(`<iframe id="dashboard-frame" src="${newURL}" width="100%" height="100%">`);

container.empty().append(iFrame);

let refreshButton = $(`<button class="uk-button uk-button-default refresh-dashboard-button" style="margin-left: 20px;">Сбросить</button>`);
let periodStart = $(`<input id="input_date_start" type="date" value="${formatDate(new Date(currentDate.getFullYear(), 0))}">`).addClass('uk-input uk-width-auto uk-margin-right');
let periodStop = $(`<input id="input_date_stop" type="date" value="${formatDate(currentDate)}">`).addClass('uk-input uk-width-auto');

$('#panel-dashboard-period').append(periodStart).append(periodStop).append(refreshButton);

const changePeriod = () => {
  let period = `time:(from:'${periodStart.val()}T00:00:00.000',mode:absolute,to:'${periodStop.val()}T23:59:59.999')`;
  let newURL = `${dashboardURL}&_g=(${defaultParam},${period})`;
  iFrame.attr('src', newURL);
}

periodStart.on('change', e => {
  changePeriod();
});

periodStop.on('change', e => {
  changePeriod();
});

refreshButton.on('click', e => {
  e.preventDefault();
  periodStart.val(formatDate(new Date(currentDate.getFullYear(), 0)));
  periodStop.val(formatDate(currentDate));
  changePeriod();
});

setTimeout(() => {
  let iFrameHead = $('#dashboard-frame').contents().find("head");
  let iFrameCSS = `
  <style type="text/css">
    .c3-axis-x .tick text {
      fill: #000 !important;
      font: 9px sans-serif;
      word-spacing: -2.3px;
    }
    .gridster li {
      border: 1px dashed #e5e5e5 !important;
      transition: 0.3s ease-out 0s !important;
    }
    .gridster li:hover {
      border: 1px solid #666 !important;
    }

    .gridster > li:nth-child(2) > dashboard-panel > div > visualize > div > div > div > div > div.output-value {
      color: green;
    }
    .gridster > li:nth-child(3) > dashboard-panel > div > visualize > div > div > div > div > div.output-value {
      color: blue;
    }
    .gridster > li:nth-child(4) > dashboard-panel > div > visualize > div > div > div > div > div.output-value {
      color: red;
    }
  </style>`;
  $(iFrameHead).append(iFrameCSS);
}, 1000);

setTimeout(() => {
  iFrame.attr('src', newURL);
}, 10000);
