const syfixViewComponent = 'Label';
const defaultParamDict = ['MORE', 'MORE_OR_EQUALS', 'LESS', 'LESS_OR_EQUALS', 'EQUALS', 'NOT_EQUALS', 'CONTAINS', 'NOT_CONTAINS', 'START', 'NOT_START', 'NOT_END', 'END', 'TEXT_NOT_EQUALS', 'TEXT_EQUALS'];
let filterWindow = $('.filter-window');
let filterClose = filterWindow.find('.filter-close');
let filterBody = filterWindow.find('.filter-body');
let btnFilter = filterWindow.find('#btn-filter');
let btnFilterDrop = filterWindow.find('#btn-filter-drop');
let tableParamBody = $('#rd-table-params').find('tbody');

let RD = {
  formData: null,
  paramSearch: {},

  paramFields: {
    registryRouteStatus: ['STATE_SUCCESSFUL', 'STATE_NOT_FINISHED', 'NO_ROUTE', 'STATE_UNSUCCESSFUL'],
    textarea: defaultParamDict,
    textbox: defaultParamDict,
    custom: defaultParamDict,
    formula: defaultParamDict,
    counter: defaultParamDict,
    projectlink: defaultParamDict,
    repeater: defaultParamDict,
    docnumber: defaultParamDict,
    personlink: defaultParamDict,
    link: defaultParamDict,
    htd: defaultParamDict,

    numericinput: ['MORE', 'MORE_OR_EQUALS', 'LESS', 'LESS_OR_EQUALS', 'EQUALS', 'NOT_EQUALS'],
    date: ['MORE', 'MORE_OR_EQUALS', 'LESS', 'LESS_OR_EQUALS', 'EQUALS', 'NOT_EQUALS'],
    entity: {
      users: ['CONTAINS', 'NOT_CONTAINS'],
      positions: ['CONTAINS', 'NOT_CONTAINS'],
      departments: ['CONTAINS', 'NOT_CONTAINS']
    },
    reglink: ['CONTAINS', 'NOT_CONTAINS'],
    listbox: ['EQUALS'],
    check: ['CONTAINS'],
    radio: ['EQUALS']
  },

  paramName: {
    STATE_SUCCESSFUL: 'Активная',
    STATE_NOT_FINISHED: 'В процессе',
    NO_ROUTE: 'Подготовка',
    STATE_UNSUCCESSFUL: 'Неуспешная',

    MORE: '>',
    MORE_OR_EQUALS: '>=',
    LESS: '<',
    LESS_OR_EQUALS: '<=',
    EQUALS: '=',
    NOT_EQUALS: '<>',

    CONTAINS: 'Содержит',
    NOT_CONTAINS: 'Не содержит',
    START: 'Начинается с',
    NOT_START: 'Не начинается с',
    NOT_END: 'Не заканчивается на',
    END: 'Заканчивается на',
    TEXT_NOT_EQUALS: 'Не совпадает',
    TEXT_EQUALS: 'Совпадает'
  },

  reset: function() {
    this.formData = null;
    this.paramSearch = {};
  }
};

const getUrlSearch = () => {
  let url = '&groupTerm=and';
  let count = 0;

  for(let key in RD.paramSearch) {
    if(RD.paramSearch[key].field == 'registryRecordStatus') {
      url += `&registryRecordStatus=${RD.paramSearch[key].value}`;
    } else {
      let gID = count > 0 ? count : '';
      switch (RD.paramSearch[key].type) {
        case 'listbox':
          RD.paramSearch[key].condition.indexOf('NOT') == -1 ? url += `&term${gID}=or` : url += `&term${gID}=and`;
          if(RD.paramSearch[key].hasOwnProperty('keys') && RD.paramSearch[key].keys) {
            RD.paramSearch[key].keys.forEach(fieldKey =>
              url += `&field${gID}=${RD.paramSearch[key].field}&condition${gID}=${RD.paramSearch[key].condition}&key${gID}=${fieldKey}`
            );
          } else {
            return null;
          }
          break;
        case 'radio':
        case 'check':
          RD.paramSearch[key].condition.indexOf('NOT') == -1 ? url += `&term${gID}=or` : url += `&term${gID}=and`;
          if(RD.paramSearch[key].hasOwnProperty('values') && RD.paramSearch[key].values) {
            RD.paramSearch[key].values.forEach(fieldValue =>
              url += `&field${gID}=${RD.paramSearch[key].field}&condition${gID}=${RD.paramSearch[key].condition}&value${gID}=${fieldValue}`
            );
          } else {
            return null;
          }
          break;
        case 'reglink':
          RD.paramSearch[key].condition.indexOf('NOT') == -1 ? url += `&term${gID}=or` : url += `&term${gID}=and`;
          if(RD.paramSearch[key].hasOwnProperty('selectedIds') && RD.paramSearch[key].selectedIds) {
            RD.paramSearch[key].selectedIds.forEach(docID =>
              url += `&field${gID}=${RD.paramSearch[key].field}&condition${gID}=${RD.paramSearch[key].condition}&key${gID}=${docID}`
            );
          } else {
            return null;
          }
          break;
        case 'users':
          RD.paramSearch[key].condition.indexOf('NOT') == -1 ? url += `&term${gID}=or` : url += `&term${gID}=and`;
          if(RD.paramSearch[key].hasOwnProperty('users') && RD.paramSearch[key].users) {
            RD.paramSearch[key].users.map(x => x.personID).forEach(userid =>
              url += `&field${gID}=${RD.paramSearch[key].field}&condition${gID}=${RD.paramSearch[key].condition}&key${gID}=${userid}`
            );
          } else {
            return null;
          }
          break;
        case 'positions':
          RD.paramSearch[key].condition.indexOf('NOT') == -1 ? url += `&term${gID}=or` : url += `&term${gID}=and`;
          if(RD.paramSearch[key].hasOwnProperty('positions') && RD.paramSearch[key].positions) {
            RD.paramSearch[key].positions.map(x => x.elementID).forEach(positionID =>
              url += `&field${gID}=${RD.paramSearch[key].field}&condition${gID}=${RD.paramSearch[key].condition}&key${gID}=${positionID}`
            );
          } else {
            return null;
          }
          break;
        case 'departments':
          RD.paramSearch[key].condition.indexOf('NOT') == -1 ? url += `&term${gID}=or` : url += `&term${gID}=and`;
          if(RD.paramSearch[key].hasOwnProperty('departments') && RD.paramSearch[key].departments) {
            RD.paramSearch[key].departments.map(x => x.departmentId).forEach(departmentId =>
              url += `&field${gID}=${RD.paramSearch[key].field}&condition${gID}=${RD.paramSearch[key].condition}&key${gID}=${departmentId}`
            );
          } else {
            return null;
          }
          break;
        default:
          url += `&field${gID}=${RD.paramSearch[key].field}&condition${gID}=${RD.paramSearch[key].condition}`;
          if(RD.paramSearch[key].hasOwnProperty('key')) {
            if(!RD.paramSearch[key].key || RD.paramSearch[key].key == "") {
              return null;
            } else {
              url += `&key${gID}=${RD.paramSearch[key].key}`;
            }
          } else {
            if(!RD.paramSearch[key].value || RD.paramSearch[key].value == "") {
              return null;
            } else {
              url += `&value${gID}=${RD.paramSearch[key].value}`;
            }
          }
      }
      count++;
    }
  }

  return url == '&groupTerm=and' ? null : url;
}

const createSelectComponent = (items, multiple) => {
  let select = $('<select class="uk-select" style="min-width: 100px;">');
  if(multiple) select.attr('multiple', 'multiple');
  if(items) items.sort((a,b) => a.value - b.value)
  .forEach(item => select.append(`<option value="${item.value}" title="${item.label}">${item.label}</option>`));
  return select;
};
const createInputText = () => $('<input type="text" class="uk-input" placeholder="Введите значение">');
const createInputNumber = () => $('<input type="number" class="uk-input" placeholder="Введите значение">');
const createInputDate = () => $('<input type="date" class="uk-input" style="max-width: 160px;"><input type="time" class="uk-input" style="padding-left: 10px;max-width: 100px;" value="00:00">');

const createBlockParam = param => {
  let items = RD.paramFields[param.type].map(x => ({label: RD.paramName[x], value: x}));
  let select = createSelectComponent(items);
  let input;
  switch (param.type) {
    case 'date': input = createInputDate(); break;
    case 'numericinput': input = createInputNumber(); break;
    default: input = createInputText(); break;
  }

  select.addClass('uk-width-auto').css({'padding-left': '10px', 'margin-right': '5px'});
  input.addClass('uk-width-expand').css('padding-left', '10px');

  return $('<div uk-grid style="margin: 0;"></div>').append(select).append(input);
};

const createRegistryParamBlock = (param, uuidRow, guid) => {
  let button = $('<a class="uk-form-icon uk-form-icon-flip" href="javascript:void(0);" uk-icon="icon: list"></a>');
  let input = $('<input class="uk-input" type="text" disabled>');
  let select = createSelectComponent(RD.paramFields.reglink.map(x => ({label: RD.paramName[x], value: x})));

  if(guid) {
    AS.FORMS.ApiUtils.simpleAsyncGet('rest/api/formPlayer/getDocMeaningContents?documentId=' + RD.paramSearch[uuidRow].selectedIds.join('&documentId='), content => {
      content = content.map(x => x.meaning).join('; ');
      input.val(content).attr('title', content);
    });
  } else {
    RD.paramSearch[uuidRow] = {field: param.id, type: param.type, condition: select.val()};
  }

  select.on('change', () => RD.paramSearch[uuidRow].condition = select.val());

  button.on('click', e => {
    Cons.showLoader();
    let selectedIds = RD.paramSearch[uuidRow].hasOwnProperty('selectedIds') ? RD.paramSearch[uuidRow].selectedIds : null;
    AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/info?registryID=${param.registryID}`, registryInfo => {
      Cons.hideLoader();
      //(registry, multi, selectedIds, handler)
      AS.SERVICES.showRegistryLinkDialog(registryInfo, true, selectedIds, docIDs => {
        RD.paramSearch[uuidRow].selectedIds = docIDs;
        let contentURL = 'rest/api/formPlayer/getDocMeaningContents?documentId=' + docIDs.join('&documentId=');
        AS.FORMS.ApiUtils.simpleAsyncGet(contentURL, content => {
          content = content.map(x => x.meaning).join('; ');
          input.val(content).attr('title', content);
        });
      });
    });
  });

  return $('<div class="uk-grid-small" uk-grid>')
  .append($('<div class="uk-width-1-4@s">').append(select))
  .append($('<div class="uk-inline uk-width-3-4@s">').append(button).append(input));
}

const createUserParamBlock = (param, uuidRow, guid) => {
  let button = $('<a class="uk-form-icon uk-form-icon-flip" href="javascript:void(0);" uk-icon="icon: users"></a>');
  let input = $('<input class="uk-input" type="text" disabled>');
  let select = createSelectComponent(RD.paramFields.entity.users.map(x => ({label: RD.paramName[x], value: x})));

  if(guid) {
    let userNames = RD.paramSearch[uuidRow].users.map(x => x.personName).join('; ');
    input.val(userNames).attr('title', userNames);
  } else {
    RD.paramSearch[uuidRow] = {field: param.id, type: param.entity, condition: select.val()};
  }

  select.on('change', () => RD.paramSearch[uuidRow].condition = select.val());

  button.on('click', e => {
    let values = RD.paramSearch[uuidRow].hasOwnProperty('users') ? RD.paramSearch[uuidRow].users : null;
    //(values, multiSelectable, isGroupSelectable, showWithoutPosition, filterPositionID, filterDepartmentID, locale, handler)
    AS.SERVICES.showUserChooserDialog(values, true, false, false, null, null, AS.OPTIONS.locale, users => {
      let userNames = users.map(x => x.personName).join('; ');
      input.val(userNames).attr('title', userNames);
      RD.paramSearch[uuidRow].users = users;
    });
  });

  return $('<div class="uk-grid-small" uk-grid>')
  .append($('<div class="uk-width-1-4@s">').append(select))
  .append($('<div class="uk-inline uk-width-3-4@s">').append(button).append(input));
}

const createDepParamBlock = (param, uuidRow, guid) => {
  let button = $('<a class="uk-form-icon uk-form-icon-flip" href="javascript:void(0);" uk-icon="icon: more"></a>');
  let input = $('<input class="uk-input" type="text" disabled>');
  let select = createSelectComponent(RD.paramFields.entity.departments.map(x => ({label: RD.paramName[x], value: x})));

  if(guid) {
    let depNames = RD.paramSearch[uuidRow].departments.map(x => x.departmentName).join('; ');
    input.val(depNames).attr('title', depNames);
  } else {
    RD.paramSearch[uuidRow] = {field: param.id, type: param.entity, condition: select.val()};
  }

  select.on('change', () => RD.paramSearch[uuidRow].condition = select.val());

  button.on('click', e => {
    let values = RD.paramSearch[uuidRow].hasOwnProperty('departments') ? RD.paramSearch[uuidRow].departments : null;
    //(values, multiSelectable, filterUserID, filterPositionID, filterDepartmentID, filterChildDepartmentID, locale, handler)
    AS.SERVICES.showDepartmentChooserDialog(values, true, null, null, null, null, AS.OPTIONS.locale, departments => {
      let depNames = departments.map(x => x.departmentName).join('; ');
      input.val(depNames).attr('title', depNames);
      RD.paramSearch[uuidRow].departments = departments;
    });
  });

  return $('<div class="uk-grid-small" uk-grid>')
  .append($('<div class="uk-width-1-4@s">').append(select))
  .append($('<div class="uk-inline uk-width-3-4@s">').append(button).append(input));
}

const createPositionParamBlock = (param, uuidRow, guid) => {
  let button = $('<a class="uk-form-icon uk-form-icon-flip" href="javascript:void(0);" uk-icon="icon: more"></a>');
  let input = $('<input class="uk-input" type="text" disabled>');
  let select = createSelectComponent(RD.paramFields.entity.positions.map(x => ({label: RD.paramName[x], value: x})));

  if(guid) {
    let posNames = RD.paramSearch[uuidRow].positions.map(x => x.elementName).join('; ');
    input.val(posNames).attr('title', posNames);
  } else {
    RD.paramSearch[uuidRow] = {field: param.id, type: param.entity, condition: select.val()};
  }

  select.on('change', () => RD.paramSearch[uuidRow].condition = select.val());

  button.on('click', e => {
    let values = RD.paramSearch[uuidRow].hasOwnProperty('positions') ? RD.paramSearch[uuidRow].positions : null;
    //(values, multiSelect, filterUserId, filterDepartmentId, showVacant, locale, handler)
    AS.SERVICES.showPositionChooserDialog(values, true, null, null, null, AS.OPTIONS.locale, positions => {
      let posNames = positions.map(x => x.elementName).join('; ');
      input.val(posNames).attr('title', posNames);
      RD.paramSearch[uuidRow].positions = positions;
    });
  });

  return $('<div class="uk-grid-small" uk-grid>')
  .append($('<div class="uk-width-1-4@s">').append(select))
  .append($('<div class="uk-inline uk-width-3-4@s">').append(button).append(input));
}

const generateGuid = () => 'R'+Math.random().toString(36).substring(2, 15)+Math.random().toString(36).substring(2, 15);

//Метод добавления параметра для поиска
const addParamRow = (param, guid) => {
  if(!param) return;

  let uuidRow = guid || generateGuid();
  let tr = $('<tr>');
  let tdFilter = $('<td>');
  let deleteRow = $('<a href="#" uk-icon="trash"></a>');

  deleteRow.on('click', e => {
    e.preventDefault();
    e.stopPropagation();
    tr.remove();
    delete RD.paramSearch[uuidRow];
  });

  switch(param.type) {
    case 'registryRouteStatus':
      let statusRow = createSelectComponent(RD.paramFields[param.type].map(x => ({label: RD.paramName[x], value: x})));
      if(guid) {
        statusRow.val(RD.paramSearch[uuidRow].value);
      } else {
        RD.paramSearch[uuidRow] = {field: 'registryRecordStatus', value: statusRow.val()};
      }
      statusRow.on('change', () => RD.paramSearch[uuidRow].value = statusRow.val());
      tdFilter.append(statusRow);
      break;
    case 'listbox':
      let dictValues = createSelectComponent(param.elements, true);
      if(guid) {
        dictValues.val(RD.paramSearch[uuidRow].keys);
      } else {
        RD.paramSearch[uuidRow] = {
          field: param.id,
          type: param.type,
          condition: RD.paramFields[param.type][0],
          keys: dictValues.val()
        };
      }
      dictValues.on('change', () => RD.paramSearch[uuidRow].keys = dictValues.val());
      tdFilter.append(dictValues);
      break;
    case 'radio':
    case 'check':
      let dictValuesR = createSelectComponent(param.elements, true);
      if(guid) {
        dictValuesR.val(RD.paramSearch[uuidRow].values);
      } else {
        RD.paramSearch[uuidRow] = {
          field: param.id,
          type: param.type,
          condition: RD.paramFields[param.type][0],
          values: dictValuesR.val()
        };
      }
      dictValuesR.on('change', () => RD.paramSearch[uuidRow].values = dictValuesR.val());
      tdFilter.append(dictValuesR);
      break;
    case 'date':
      let blockParamND = createBlockParam(param);
      let selectND = blockParamND.find('select');
      let inputDate = blockParamND.find('input[type="date"]');
      let inputTime = blockParamND.find('input[type="time"]');

      if(guid) {
        selectND.val(RD.paramSearch[uuidRow].condition);
        inputDate.val(RD.paramSearch[uuidRow].key.split(' ')[0]);
        inputTime.val(RD.paramSearch[uuidRow].key.split(' ')[1].substr(0,5));
      } else {
        RD.paramSearch[uuidRow] = {
          field: param.id,
          type: param.type,
          condition: selectND.val(),
          key: `${inputDate.val()} ${inputTime.val()}:00`
        }
      }

      selectND.on('change', () => RD.paramSearch[uuidRow].condition = selectND.val());
      inputDate.on('change', () => RD.paramSearch[uuidRow].key = `${inputDate.val()} ${inputTime.val()}:00`);
      inputTime.on('change', () => RD.paramSearch[uuidRow].key = `${inputDate.val()} ${inputTime.val()}:00`);
      tdFilter.append(blockParamND);
      break;
    case 'reglink':
      tdFilter.append(createRegistryParamBlock(param, uuidRow, guid));
      break;
    case 'entity':
      switch (param.entity) {
        case 'users': tdFilter.append(createUserParamBlock(param, uuidRow, guid)); break;
        case 'departments': tdFilter.append(createDepParamBlock(param, uuidRow, guid)); break;
        case 'positions': tdFilter.append(createPositionParamBlock(param, uuidRow, guid)); break;
        default: tdFilter.append('<div>Параметр в разработке</div>');
      }
      break;
    default:
      let blockParamText = createBlockParam(param);
      let inputText = blockParamText.find('input');
      let selectText = blockParamText.find('select');

      if(guid) {
        inputText.val(RD.paramSearch[uuidRow].value);
        selectText.val(RD.paramSearch[uuidRow].condition);
      } else {
        RD.paramSearch[uuidRow] = {
          field: param.id,
          type: param.type,
          condition: selectText.val(),
          value: inputText.val()
        };
      }

      inputText.on('change', () => RD.paramSearch[uuidRow].value = inputText.val());
      selectText.on('change', () => RD.paramSearch[uuidRow].condition = selectText.val());
      tdFilter.append(blockParamText);
  }

  tr.append(`<td class="uk-form-label uk-text-primary uk-width-small uk-text-truncate" title="${param.name}">${param.name}</td>`)
  .append(tdFilter).append($('<td title="Удалить условие" style="width: 30px; text-align: end;">').append(deleteRow));

  tableParamBody.append(tr);
}

//получение элементов справочника для компонента listbox
const getDictionaryItems = (el) => {
  if (el.hasOwnProperty('dataSource')) {
    rest.synergyGet(`api/dictionaries/${el.dataSource.dict}`, dict => {
      el.elements = [];
      for (let key in dict.items) {
        el.elements.push({
          label: dict.items[key][el.dataSource.key].value,
          value: dict.items[key][el.dataSource.value].value
        });
      }
    });
  }
}

//Действие при выборе параметра/условия
const selectParameter = (value, key) => {
  let param;
  if(value == 'registryRecordStatus') {
    param = {id: 'registryRecordStatus', type: 'registryRouteStatus', name: 'Статус записи реестра'};
  } else {
    param = RD.formData.component[value];
  }
  addParamRow(param, key);
}

//Инициализация справочника параметров/условий
const initParamList = () => {
  let paramList = $('#rd-form-param-list');
  RD.paramSearch = {};
  tableParamBody.empty();

  paramList.empty().off()
  .append('<option disabled selected value="">Добавить условие</option>')
  .on('change', () => {
    selectParameter(paramList.val());
    paramList.val("");
  });

  for(let key in RD.formData.label) {
    let x = RD.formData.label[key];
    paramList.append(`<option value="${key}" title="${x.label}">${x.label}</option>`);
  }
}

const getViewComponent = (component, componentsArrayList) => {
  let idxComponentLabel = component.id.lastIndexOf(syfixViewComponent);
  if (~idxComponentLabel) {
    let isComponentLabel = component.id.substr(idxComponentLabel) === syfixViewComponent;
    if (isComponentLabel) {
      let componentID = component.id.substr(0, idxComponentLabel);
      let filter = componentsArrayList.map(x => {
        let fComponent = x.filter(x => x.id === componentID).values().next().value || {};
        if (Object.keys(fComponent).length) {
          if(['listbox', 'check', 'radio'].indexOf(fComponent.type) != -1) getDictionaryItems(fComponent);
          if(fComponent.type == 'reglink') fComponent.registryID = fComponent.config.dateFormat;
          if(fComponent.type == 'entity') fComponent.entity = fComponent.config.entity;
          fComponent.name = component.label;
          return {
            label: {id: component.id, view: component},
            component: {id: fComponent.id, view: fComponent}
          };
        }
      });
      return filter;
    }
  }
}

const initFilter = () => {
  let formCode = Cons.getAppStore().formCode;
  let saveParam = sessionStorage.getItem(`filterParam_${Cons.getCurrentPage().code}`);
  RD.reset();

  if(saveParam) {
    saveParam = JSON.parse(saveParam);
    RD.formData = saveParam.formData;
    initParamList();
    RD.paramSearch = saveParam.paramSearch;
    for(key in RD.paramSearch) selectParameter(RD.paramSearch[key].field, key);
    let urlSearch = getUrlSearch();
    if(urlSearch) {
      $('#panel-search span').addClass('_filter');

      setTimeout(() => {
        $('.exp-table-container').trigger({
          type: 'filterRegistryRows',
          eventParam: {
            filterSearchUrl: urlSearch
          }
        });
      }, 500);
    }
  } else {
    AS.FORMS.ApiUtils.simpleAsyncGet('rest/api/asforms/form_ext?formCode=' + formCode)
    .then(response => {
      RD.formData = response.properties.reduce((res, x) => {
        if(x.type === 'table' && !x.config.appendRows) {
          x.properties.forEach(component => {
            (getViewComponent(component, [response.properties, x.properties]) || []).forEach(x => {
              if (x instanceof Object) {
                res.label[x.component.id] = x.label.view;
                res.component[x.component.id] = x.component.view;
              }
            });
          });
        } else {
          (getViewComponent(x, [response.properties]) || []).forEach(x => {
            if(x instanceof Object) {
              res.label[x.component.id] = x.label.view;
              res.component[x.component.id] = x.component.view;
            }
          });
        }
        return res;
      }, {
        label: {},
        component: {}
      });


      setTimeout(() => {
        sessionStorage.setItem(`filterParam_${Cons.getCurrentPage().code}`, JSON.stringify({
          formData: RD.formData,
          paramSearch: RD.paramSearch
        }));
      }, 500);

      initParamList();
    });
  }
}

const openFilterWindow = () => {
  filterWindow.fadeIn();
  $('body').addClass('lock');
}

const closeFilterWindow = () => {
  filterWindow.fadeOut();
  $('body').removeClass('lock');
}

const checkSearchParam = () => {
  let saveParam = sessionStorage.getItem(`filterParam_${Cons.getCurrentPage().code}`);
  // RD.reset();

  if(saveParam) {
    saveParam = JSON.parse(saveParam);
    RD.formData = saveParam.formData;
    initParamList();
    RD.paramSearch = saveParam.paramSearch;
    for(key in RD.paramSearch) selectParameter(RD.paramSearch[key].field, key);
    let urlSearch = getUrlSearch();
    if(urlSearch) {
      $('#panel-search span').addClass('_filter');

      setTimeout(() => {
        $('.exp-table-container').trigger({
          type: 'filterRegistryRows',
          eventParam: {
            filterSearchUrl: urlSearch
          }
        });
      }, 500);
    }
  }

}

addListener('init_filters', comp.code, event => {
  $('#panel-search span').attr('title', 'Расширенный поиск');
  $('#panel-search span').off().on('click', e => {
    openFilterWindow();
    initFilter();
  });

  checkSearchParam();

  filterClose.off().on('click', e => {
    closeFilterWindow();
  });

  btnFilter.off().on('click', e => {
    let urlSearch = getUrlSearch();
    e.preventDefault();
    e.target.blur();

    if(urlSearch) {
      sessionStorage.setItem(`filterParam_${Cons.getCurrentPage().code}`, JSON.stringify({
        formData: RD.formData,
        paramSearch: RD.paramSearch
      }));

      $('.exp-table-container').trigger({
        type: 'filterRegistryRows',
        eventParam: {
          filterSearchUrl: urlSearch
        }
      });

      $('#panel-search span').addClass('_filter');

      closeFilterWindow();
    } else {
      showMessage('Не все условия поиска заполнены','error');
      return;
    }
  });

  btnFilterDrop.off().on('click', e => {
    e.preventDefault();
    e.target.blur();

    RD.formData = null;
    RD.paramSearch = {};

    sessionStorage.removeItem(`filterParam_${Cons.getCurrentPage().code}`);

    initFilter();

    $('#panel-search span').removeClass('_filter');
    $('.exp-table-container').trigger({type: 'updateTableBody', eventParam: {filterSearchUrl: null}});
  });

  filterWindow.off().on('click', e => {
    if ($(e.target).closest('.filter-content').length == 0) closeFilterWindow();
	});
});
