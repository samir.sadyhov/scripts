!function(i){i.widget("ih.resizableColumns",{_create:function(){this._initResizable()},_initResizable:function(){let e,t,n,s=this.element;s.find("th").resizable({handles:{e:" .resizeHelper"},minWidth:10,create:function(e,t){let n=i(this).find(".columnLabel").width();n&&(n+=i(this).find(".ui-resizable-e").width(),i(this).resizable("option","minWidth",n))},start:function(i,h){let l=h.helper.index()+1;e=s.find("colgroup > col:nth-child("+l+")"),t=parseInt(e.get(0).style.width,10),n=h.size.width},resize:function(s,h){let l=h.size.width-n,d=t+l;e.width(d),i(this).css("height","auto")}})}})}(jQuery);

$.getScript("https://cdnjs.cloudflare.com/ajax/libs/alasql/0.6.1/alasql.min.js");

const subName = (name, num) => {
  if(!name) return '-';
  return name.length > num ? name.substring(0, num) + '..' : name;
}

let tableContainer = $('.exp-table-container');

const createSynergyPlayer = (dataUUID, editable = false) => {
  let player = AS.FORMS.createPlayer();
  if($(document).width() < 781) player.model.showView('mobile');
  player.view.setEditable(editable);
  player.showFormData(null, null, dataUUID);
  return new Promise(resolve => {
    AS.FORMS.ApiUtils.getDocumentIdentifier(dataUUID)
    .then(documentID => AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/docflow/doc/document_info?documentID=${documentID}`))
    .then(documentInfo => {
      player.documentID = documentInfo.documentID;
      player.registryID = documentInfo.registryID;
      player.registryName = documentInfo.registryName;
      player.formName = documentInfo.formName;
      player.formID = documentInfo.formID;
      player.formCode = documentInfo.formCode;
      player.documentInfo = documentInfo;
      resolve(player);
    })
  });
}

const activateDoc = dataUUID => {
  return AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/activate_doc?dataUUID=${dataUUID}`);
}

const deleteDoc = documentID => {
  return AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/delete_doc?documentID=${documentID}`);
}

const saveFormData = formPlayer => {
  return new Promise(resolve => {
    let asfData = formPlayer.model.getAsfData();

    if(formPlayer.formCode == 'experience_form_assessment_survey') {
      let manager = asfData.data.find(x => x.id == 'cjm_responsibleManager');
      if(manager) {
        manager.key = AS.OPTIONS.currentUser.userid;
        manager.value = `${AS.OPTIONS.currentUser.lastname} ${AS.OPTIONS.currentUser.firstname}`;
      }
    }

    let data = {
      data: '"data" : ' + JSON.stringify(asfData.data),
      form: asfData.form,
      uuid: asfData.uuid
    };
    AS.FORMS.ApiUtils.simpleAsyncPost('rest/api/asforms/form/multipartdata', res => {
      AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/modify_doc?dataUUID=${asfData.uuid}`)
      .then(res => {
        formPlayer.model.hasChanges = false;
        resolve(res);
      })
    }, "text", data, "application/x-www-form-urlencoded; charset=UTF-8", err => {
      showMessage(JSON.parse(err.responseText).errorMessage, 'error');
      resolve(null);
    });
  });
}

const checkSavedForm = (formPlayer, handler) => {
  if(formPlayer.model.hasChanges) {
    UIkit.modal.confirm('Документ был изменен. Сохранить произведенные изменения?').then(() => {
      if(!formPlayer.model.isValid()) {
        showMessage("Заполните обязательные поля!", "error");
      } else {
        Cons.showLoader();
        saveFormData(formPlayer).then(result => {
          Cons.hideLoader();
          showMessage('<svg width="35" height="35" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><polyline fill="none" stroke="#fff" stroke-width="1.1" points="4,10 8,15 17,4"></polyline></svg> Успешно', 'success');
          handler();
        });
      }
    }, handler);
  } else {
    handler();
  }
}

const printForm = content => {
  var css = '<link rel="stylesheet" href="synergyForms.css" type="text/css" />';
  var WinPrint = window.open();
  WinPrint.document.write('');
  WinPrint.document.write(css);
  WinPrint.document.write(content.innerHTML);
  WinPrint.document.write('');
  WinPrint.document.close();
  WinPrint.focus();
  WinPrint.print();
}

let clickOpenRow = false;
const openDocumentInWindow = (dataUUID, editable = false, activate = false) => {
  if(clickOpenRow) return;
  clickOpenRow = !clickOpenRow;
  setTimeout(() => {
    clickOpenRow = !clickOpenRow;
  }, 1000);

  let documentWindow = $('<div>', {class: 'exp-window-document'});
  let header = $('<div>', {class: 'exp-window-header'});
  let body = $('<div>', {class: 'exp-window-body'});
  let content = $('<div>', {class: 'exp-window-content'});
  let formPanel = $('<div>', {class: 'exp-window-form-panel'})
  let buttonsHeader = $('<div>', {class: 'exp-window-header-buttons'});
  let closeButton = $('<span class="exp-window-close-button" uk-icon="icon: close; ratio: 2"></span>');
  let panelActions = $('<div>', {class: 'exp-window-form-panel-actions'});
  let panelLogo = $('<div>', {class: 'exp-window-header-logo'});

  let printButton = $('<span class="exp-window-print-button" uk-icon="icon: print; ratio: 1.5"></span>');
  let editButton = $('<span class="exp-window-edit-button" uk-icon="icon: file-edit; ratio: 1.5"></span>');
  let viewButton = $('<span class="exp-window-view-button" uk-icon="icon: file-text; ratio: 1.5"></span>').hide();
  let saveButton = $('<button class="exp-window-save-button uk-button uk-button-primary">Сохранить</button>');

  panelActions.append(printButton).append(editButton).append(viewButton).append(saveButton);

  content.append(panelActions).append(formPanel);
  buttonsHeader.append(closeButton);
  header.append(panelLogo).append(buttonsHeader);
  body.append(content);
  documentWindow.append(header).append(body);

  if(registryTable.registryCode == "experience_registry_users") editable = true;
  if(registryTable.registryCode == "experience_registry_ms") {
    editButton.hide();
    saveButton.hide();
  }

  if(editable) {
    printButton.hide();
    if(registryTable.registryCode != "experience_registry_ms") editButton.show();
    if(registryTable.registryCode == "experience_registry_users") {
      viewButton.hide();
    } else {
      viewButton.show();
    }
  }

  createSynergyPlayer(dataUUID, editable).then(player => {
    formPanel.append(player.view.container);
    $('body').append(documentWindow).addClass('_lock');

    let img = $('<img>').attr('src', $('#logo').attr('src'));
    panelLogo.append(img).append(`<span>${player.formName || 'Experience'}</span>`);

    setTimeout(() => {
      player.model.hasChanges = false;
    }, 1000);

    closeButton.on('click', e => {
      checkSavedForm(player, () => {
        player.destroy();
        documentWindow.fadeOut({
          complete: function() {
            documentWindow.remove();
          }
        });
        $('body').removeClass('_lock');
      });
    });

    editButton.on('click', e => {
      e.preventDefault();
      e.target.blur();
      e.stopPropagation();
      player.view.setEditable(true);
      editButton.hide();
      viewButton.show();
      printButton.hide();
    });

    viewButton.on('click', e => {
      e.preventDefault();
      e.target.blur();
      e.stopPropagation();
      player.view.setEditable(false);
      editButton.show();
      viewButton.hide();
      printButton.show();
    });

    saveButton.on('click', e => {
      e.preventDefault();
      e.target.blur();
      e.stopPropagation();

      function complet(){
        Cons.hideLoader();
        showMessage('<svg width="35" height="35" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><polyline fill="none" stroke="#fff" stroke-width="1.1" points="4,10 8,15 17,4"></polyline></svg> Данные успешно сохранены', 'success');
        player.destroy();
        documentWindow.fadeOut({
          complete: function() {
            documentWindow.remove();
          }
        });
        $('body').removeClass('_lock');
      }

      if(!player.model.isValid()) {
        showMessage("Заполните обязательные поля!", "error");
      } else {
        Cons.showLoader();
        saveFormData(player).then(result => {
          if(activate) {
            activateDoc(dataUUID).then(res => complet());
          } else {
            complet();
          }
        });
      }

    });

    printButton.on('click', e => {
      e.preventDefault();
      e.target.blur();
      e.stopPropagation();
      printForm(player.view.container[0]);
    });

    if(registryTable.registryCode == "experience_registry_users") {
      let userActionContainer = $('<div>');
      let menuContainer = $('<div uk-dropdown="mode: click">');
      let nav = $('<ul class="uk-nav uk-dropdown-nav">');
      let itemAuth = $('<li class="action-menu-item"><a href="#">Параметры авторизации</a></li>');
      let itemParam = $('<li class="action-menu-item"><a href="#">Управление модулями</a></li>');
      let itemDeleted = $('<li class="action-menu-item"><a href="#">Удалить</a></li>');

      panelActions.prepend(userActionContainer);
      nav.append(itemAuth).append(itemParam).append(itemDeleted);
      menuContainer.append(nav);
      userActionContainer.append('<button class="exp-window-nav-button uk-button uk-button-default">Действие</button>').append(menuContainer);

      itemAuth.on('click', e => {
        e.preventDefault();
        e.target.blur();
      });

      itemParam.on('click', e => {
        e.preventDefault();
        e.target.blur();
      });

      itemDeleted.on('click', e => {
        e.preventDefault();
        e.target.blur();
      });
    }

  });

}

const createTableInProblem = dataUUID => {
  return new Promise(resolve => {
    AS.FORMS.ApiUtils.loadAsfData(dataUUID).then(asfData => {
      let table = asfData.data.find(x => x.id == 'experience_form_problem_pdg_table_negative');

      if(!table) {
        table = {
          id: "experience_form_problem_pdg_table_negative",
          type: "appendable_table",
          data: []
        };
        asfData.data.push(table);
      }
      if(!table.hasOwnProperty('data')) table.data = [];

      let currentData = table.data.map(x => x.key);
      let docIDs = registryTable.selectedItems.filter(id => currentData.indexOf(id) === -1);
      let tableBlockIndex = 1;

      if(table.data.length) {
        let id = table.data[table.data.length - 1].id;
        tableBlockIndex = Number(id.substring(id.indexOf('-b') + 2)) + 1;
      }

      let promises = docIDs.map(docID => AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/formPlayer/getDocMeaningContent?documentId=${docID}`, null, 'text'));

      Promise.all(promises).then(meaning => {
        docIDs.forEach((docID, i) => {
          table.data.push({
            id: "experience_form_problem_pdg_negative-b" + tableBlockIndex,
            type: "reglink",
            value: meaning[i],
            key: docID,
            valueID: docID
          });
          tableBlockIndex++;
        });
        resolve(asfData);
      });

    });
  });
}

const createProblem = () => {
  Cons.showLoader();
  AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/create_doc?registryCode=experience_registry_problems_pdg`)
  .then(res => {
    createTableInProblem(res.dataUUID).then(problemAsfData => {
      AS.FORMS.ApiUtils.saveAsfData(problemAsfData.data, problemAsfData.form, problemAsfData.uuid)
      .then(uuid => {
        openDocumentInWindow(uuid, true, true);
        Cons.hideLoader();
      });
    });
  });
}

const addToProblem = () => {
  AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/info?code=experience_registry_problems_pdg`, registryInfo => {
    if(!registryInfo.hasOwnProperty('registryCustomFilters')) registryInfo.registryCustomFilters = [];
    //(registry, multi, selectedIds, handler)
    AS.SERVICES.showRegistryLinkDialog(registryInfo, false, [], docID => {
      Cons.showLoader();
      AS.FORMS.ApiUtils.getAsfDataUUID(docID).then(dataUUID => {
        createTableInProblem(dataUUID).then(problemAsfData => {
          AS.FORMS.ApiUtils.saveAsfData(problemAsfData.data, problemAsfData.form, problemAsfData.uuid)
          .then(uuid => {
            openDocumentInWindow(uuid, true);
            Cons.hideLoader();
          });
        });
      });
    });
  });
}

const createMenuActions = () => {
  let menuContainer = $('<div uk-dropdown="mode: click">');
  let nav = $('<ul class="uk-nav uk-dropdown-nav">');
  let itemCreate = $('<li class="action-menu-item uk-disabled"><a href="#">Создать проблему</a></li>');
  let itemAdd = $('<li class="action-menu-item uk-disabled"><a href="#">Добавить в проблему</a></li>');

  nav.append(
    itemCreate,
    '<li class="uk-nav-divider"></li>',
    itemAdd
  );
  menuContainer.append(nav);
  $('#panel-service-action').append(menuContainer);

  itemCreate.on('click', e => {
    createProblem();
  });

  itemAdd.on('click', e => {
    addToProblem();
  });
}

let Paginator = {
  container: $('<div class="exp-pt-container">'),
  paginator: $('<div class="exp-pt-paginator">'),
  pContent: $('<div class="exp-pt-paginator-content">'),
  bPrevious: $('<button class="exp-pt-previous" disabled="disabled" title="Назад">'),
  bNext: $('<button class="exp-pt-next" disabled="disabled" title="Вперед">'),
  label: $('<label>'),
  input: $('<input type="text">'),
  countInPart: 0,
  rows: 0,
  currentPage: 1,
  pages: 0,

  init: function(){
    $('.exp-pt-container').remove();
    $('.exp-registry-container').after(this.container);
    this.pContent.append(this.label).append(this.input);
    this.paginator.append(this.bPrevious).append(this.pContent).append(this.bNext);
    this.container.append(this.paginator);
    this.reset();

    this.bNext.click(() => {
      this.currentPage++;
      this.update();
      registryTable.createBody();
    });

    this.bPrevious.click(() => {
      this.currentPage--;
      this.update();
      registryTable.createBody();
    });

    this.label.on('dblclick', e => {
      e.preventDefault();
      this.input.show();
      this.label.hide();

      this.input.on('blur', () => {
        this.label.show();
        this.input.hide();
      });

      this.input.val("" + this.currentPage);

      this.input.on('keypress', e => {
        if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) return false;
      });

      this.input.on('keydown', e => {
        if(e.which === 13) {
          let inputVal = +this.input.val();
          if(inputVal && inputVal != this.currentPage && inputVal <= this.pages && inputVal >= 1){
            this.currentPage = inputVal;
            this.update();
            registryTable.createBody();
          }
          this.label.show();
          this.input.hide();
          this.input.off();
        }
      });
      this.input.focus();
    });

  },

  update: function(){
    this.pages = Math.ceil(this.rows / this.countInPart),
    this.label.text(this.currentPage + ' / ' + this.pages);

    if(this.pages == 0) {
      this.bPrevious.attr('disabled', 'disabled');
      this.bNext.attr('disabled', 'disabled');
    } else {
      if(this.currentPage == 1) {
        this.bPrevious.attr('disabled', 'disabled');
        if (this.currentPage == this.pages) {
          this.bNext.attr('disabled', 'disabled');
        } else {
          this.bNext.removeAttr('disabled');
        }
      } else {
        this.bPrevious.removeAttr('disabled');
        if (this.currentPage == this.pages) {
          this.bNext.attr('disabled', 'disabled');
        } else {
          this.bNext.removeAttr('disabled');
        }
      }
    }
  },

  reset: function(){
    this.countInPart = 15;
    this.rows = 15;
    this.currentPage = 1;
    this.pages = 0;
  }
}

let registryTable = {
  registryInfo: null,
  registryCode: null,
  registryID: null,
  registryName: '',
  registryRights: [],
  filterCode: null,
  formCode: null,

  selectedItems: [],

  searchString: null,
  filterSearchUrl: null,

  pageCode: null,
  playerCode: null,

  allRights: [],

  sortCmpID: null,
  sortDesc: false,
  searchField: null,
  searchValue: null,
  heads: [],

  registryTable: null,
  colgroup: null,
  tHead: null,
  tBody: null,

  isDelete: false,

  getNextFieldNumber: function(url) {
    let p = url.substring(url.indexOf('?') + 1).split('&');
    p = p.map(x => {
      x = x.split('=');
      if(x[0].indexOf('field') !== -1 && x[0] !== 'fields') return x[0];
    }).filter(x => x).sort();
    if(p.length) return Number(p[p.length - 1].substring(5)) + 1;
    return '';
  },

  getUrl: function(all){
    let url = `api/registry/data_ext?registryCode=${this.registryCode}`;
    if(!all) url += `&pageNumber=${Paginator.currentPage - 1}&countInPart=${Paginator.countInPart}`;
    if(this.filterCode) url += `&filterCode=${this.filterCode}`;
    if(this.heads && this.heads.length > 0) this.heads.forEach(item => url += `&fields=${item.columnID}`);
    if(this.registryCode == 'experience_registry_assessment_survey') url += `&fields=experience_form_feedback_answersTableJSON`
    if(this.sortCmpID) url += `&sortCmpID=${this.sortCmpID}&sortDesc=${this.sortDesc}`;
    if(this.searchString) url += `&searchString=${this.searchString}`;
    if(this.filterSearchUrl) {
      url += this.filterSearchUrl;
      if(this.searchField && this.searchValue) {
        let next = this.getNextFieldNumber(this.filterSearchUrl);
        url += `&field${next}=${this.searchField}&condition${next}=CONTAINS&value${next}=${this.searchValue}`;
      }
    } else {
      if(this.searchField && this.searchValue) url += `&field=${this.searchField}&condition=CONTAINS&value=${this.searchValue}`;
    }
    return url;
  },

  removeRegistryRow: function(uuid, e) {
    e.preventDefault();
    e.target.blur();
    UIkit.modal.confirm('Вы действительно хотите удалить запись реестра?').then(() => {
      Cons.showLoader();
      try {
        rest.synergyGet(`api/registry/delete_doc?dataUUID=${uuid}`, res => {
          if(res.errorCode != '0') throw new Error(res.errorMessage);
          showMessage("Запись реестра удалена", "success");
          this.createBody();
          Cons.hideLoader();
        });
      } catch (err) {
        Cons.hideLoader();
        showMessage("Произошла ошибка при удалении записи реестра", "error");
        console.log(err);
      }
    }, () => null);
  },

  removeSelectedRow: function() {
    UIkit.modal.confirm('Вы действительно хотите удалить все выделенные записи реестра?').then(() => {
      Cons.showLoader();
      try {
        let promises = [];
        this.selectedItems.forEach(docID => promises.push(deleteDoc(docID)));

        Promise.all(promises).then(res => {
          showMessage("Все выделенные записи реестра удалены", "success");
          this.createBody();
          Cons.hideLoader();
        });

      } catch (err) {
        Cons.hideLoader();
        showMessage("Произошла ошибка при удалении записей реестра", "error");
        console.log(err);
      }
    }, () => null);
  },

  documentInfo: function(dataRow, e){
    const createRow = (label, value) => $(`<tr><td>${label}</td><td>${value}</td></tr>`);

    let dialog = $('<div class="uk-flex-top" uk-modal>');
    let md = $('<div class="uk-modal-dialog uk-margin-auto-vertical">');
    let modalBody = $('<div class="uk-modal-body" uk-overflow-auto>');

    dialog.append(md);
    md.append('<button class="uk-modal-close-default" type="button" uk-close></button>')
    .append('<div class="uk-modal-header"><h2 class="uk-modal-title">Свойства документа</h2></div>')
    .append(modalBody)
    .append('<div class="uk-modal-footer uk-text-right"><button class="uk-button uk-button-default uk-modal-close" type="button">Закрыть</button></div>');

    Cons.showLoader();
    try {
      rest.synergyGet(`api/docflow/doc/document_info?documentID=${dataRow.documentID}`, info => {
        let container = $('<div>');
        let table = $('<table>', {class: 'uk-table doc-info'});
        let body = $('<tbody>');
        let thead = $('<thead>').append('<tr><th>Параметр</th><th>Значение</th></tr>');

        body.append(createRow('Наименование реестра', info.registryName));
        body.append(createRow('Наименование формы', info.formName));
        body.append(createRow('Автор документа', info.author));
        body.append(createRow('Дата создания', info.createDate));
        body.append(createRow('documentID', info.documentID));
        body.append(createRow('asfDataID', info.asfDataID));

        table.append(thead).append(body);
        container.append(table);
        modalBody.append(container);
        Cons.hideLoader();
      });
    } catch (err) {
      Cons.hideLoader();
      modalBody.append('<p>Произошла ошибка получения данных по документу</p>');
      console.log(err);
    }

    e.preventDefault();
    e.target.blur();
    UIkit.modal(dialog).show();
    dialog.on('hidden', () => dialog.remove());
  },

  contextMenu: function(el, dataRow){
    this.isDelete = false;
    if(this.allRights.indexOf('rr_delete') !== -1 && dataRow.status != 'STATE_NOT_FINISHED') {
      this.isDelete = true;
    }
    el.on('contextmenu', event => {
      $('.exp-context-menu').remove();
      $('<div>', {class: 'exp-context-menu'})
      .css({
        "left": event.pageX + 'px',
        "top": event.pageY + 'px'
      })
      .appendTo('body')
      .append(
        $('<ul class="uk-nav-default uk-nav-parent-icon" uk-nav>')
        .append($(`<li ><a href="javascript:void(0);"><span class="uk-margin-small-right" uk-icon="icon: push"></span>Открыть</a></li>`)
          .on('click', e => {
            Cons.setAppStore({dataRow: dataRow});
            openDocumentInWindow(dataRow.dataUUID);
          })
        )
        .append($('<li><a href="javascript:void(0);"><span class="uk-margin-small-right" uk-icon="icon: info"></span>Информация</a></li>')
          .on('click', e => this.documentInfo(dataRow, e))
        )
        .append('<li class="uk-nav-divider"></li>')
        .append($(`<li ${this.isDelete ? '' : 'class="uk-disabled"'} ><a href="javascript:void(0);"><span class="uk-margin-small-right" uk-icon="icon: trash"></span>Удалить запись</a></li>`)
          .on('click', e => {
            this.isDelete ? this.removeRegistryRow(dataRow.dataUUID, e) : false;
          })
        )
      )
      .show('fast');
      return false;
    });
  },

  createRow: function(dataRow) {
    let tr = $('<tr>');

    if(this.registryCode == 'experience_registry_assessment_survey') {
      if(dataRow.fieldKey.hasOwnProperty('cjm_type') && dataRow.fieldKey.cjm_type == 2) tr.addClass('highlight');
      if(dataRow.fieldKey.hasOwnProperty('cjm_status') && dataRow.fieldKey.cjm_status == 1) tr.addClass('draw_attention');
    }

    if(['experience_registry_assessment_survey', 'experience_registry_users', 'experience_registry_questions'].includes(this.registryCode)) {
      let checkbox = $('<input/>').addClass('uk-checkbox').attr('type', 'checkbox');
      let tmpTd = $('<td>');
      if(this.selectedItems.indexOf(dataRow.documentID) !== -1) checkbox.prop('checked', true);
      tmpTd.append(`<span class="mobile-table-header">Выбор</span>`).append(checkbox);
      tr.append(tmpTd);

      checkbox.on('change', e => {
        if(e.target.checked) {
          if(this.selectedItems.indexOf(dataRow.documentID) === -1) {
            this.selectedItems.push(dataRow.documentID);
          }
        } else {
          let index = this.selectedItems.indexOf(dataRow.documentID);
          if(index !== -1) this.selectedItems.splice(index, 1);
        }
        if(this.selectedItems.length > 0) {
          $('.action-menu-item').removeClass('uk-disabled');
          fire({type: 'set_hidden', hidden: false}, 'button-actions-row');
          $('#panel-service-action').css('border-right', '1px solid #c4c4c4');
          if($('.action-menu-item.item-delete').length) {
            if(!this.isDelete) $('.action-menu-item.item-delete').addClass('uk-disabled');
          }
        } else {
          $('.action-menu-item').addClass('uk-disabled');
          fire({type: 'set_hidden', hidden: true}, 'button-actions-row');
          $('#panel-service-action').css('border-right', '1px solid transparent');
        }
      });
    }

    this.heads.forEach(item => {
      if(item.columnID == 'experience_form_feedback_answersTableJSON') {
        const customFields = dataRow.fieldValue?.experience_form_feedback_answersTableJSON;
        this.customHeads.forEach(head => {
          let td = $('<td>');
          if(customFields) {
            const json = JSON.parse(customFields);
            const row = json.find(x => x.question_reglink?.key == head.columnID);
            td.attr('title', row?.answer?.value || '');
            td.append(`<span class="mobile-table-header">${head.label}</span>`);
            td.append(`<span>${row?.answer?.value || ''}</span>`);
          }
          tr.append(td);
        });
      } else {
        let td = $('<td>');
        if (dataRow.fieldValue.hasOwnProperty(item.columnID)) {
          td.attr('title', dataRow.fieldValue[item.columnID]);
          td.append(`<span class="mobile-table-header">${item.label}</span>`);
          td.append(`<span>${dataRow.fieldValue[item.columnID]}</span>`);
        }
        tr.append(td);
      }
    });

    this.contextMenu(tr, dataRow);

    tr.on('click', e => {
      if($(e.target).is("input")) return;
      if(this.allRights.indexOf('rr_read') !== -1) {
        Cons.setAppStore({dataRow: dataRow});
        openDocumentInWindow(dataRow.dataUUID);
      } else {
        showMessage('У вас нет прав на просмотр этого документа', 'warning');
      }
    });

    return tr;
  },

  createBody: function() {
    rest.synergyGet(this.getUrl(), data => {
      this.tBody.empty();
      if(data.errorCode && data.errorCode != 0) {
        Paginator.rows = 0;
      } else {
        data.result.forEach(item => this.tBody.append(this.createRow(item)));
        Paginator.rows = data.recordsCount;
      }
      Paginator.update();
      tableContainer.scrollTop(0);
    });
  },

  resetHeaderLabel: function(){
    $('.exp-pt-header > label').each((i, el) => $(el).text(this.heads[i].label).css({"text-decoration": "none"}));
  },

  createHeader: async function() {
    Cons.showLoader();
    this.tHead.empty();
    this.colgroup.empty();

    let tr = $('<tr class="colHeaders">');

    if(['experience_registry_assessment_survey', 'experience_registry_users', 'experience_registry_questions'].includes(this.registryCode)) {
      this.colgroup.append(`<col style="width: 25px">`);
      let th = $('<th class="ui-resizable">');
      let checkbox = $('<input/>')
        .addClass('uk-checkbox')
        .attr('type', 'checkbox')
        .on('change', e => {
          this.tBody.find('td > [type="checkbox"]').each((k, x) => {
            x.checked = !!e.target.checked;
          });
          this.tBody.find('td > [type="checkbox"]').each((k, x) => {
            $(x).trigger('change');
          });
        });
      th.append(checkbox);
      tr.append(th);
    }

    let me = this;
    for(let i = 0; i < me.heads.length; i++) {
      const item = me.heads[i];

      if(item.columnID == 'experience_form_feedback_answersTableJSON') {
        const params = {
          registryCode: 'experience_registry_questions',
          field: 'experience_form_question_status',
          condition: 'TEXT_EQUALS',
          value: '1',
          fields: 'experience_form_question_text'
        };
        const searchResult = await AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/data_ext?${$.param(params)}`);
        me.customHeads = searchResult.result.map(x => {
          const label = x.fieldValue?.experience_form_question_text || '';
          const columnID = x.documentID;

          me.colgroup.append(`<col style="width: 80px">`);
          const tmp_th = $('<th class="exp-pt-header ui-resizable">');
          const tmp_label = $('<label class="columnLabel">').text(label);

          if(label.length > 25) {
            tmp_label.text(label.slice(0, 25) + '...');
          } else {
            tmp_label.text(label);
          }

          tmp_th.attr('title', label);
          tmp_th.append(tmp_label, `<div class="resizeHelper ui-resizable-handle ui-resizable-e">&nbsp;</div>`);
          tr.append(tmp_th);

          return {label, columnID};
        });

      } else {
        this.colgroup.append(`<col style="width: 80px">`);
        let tmp_th = $('<th class="exp-pt-header ui-resizable">');
        let tmp_label = $('<label class="columnLabel">').text(item.label);
        let tmp_imput = $('<input type="text" class="uk-input">').css({'height': 'auto'}).hide();

        tmp_th.attr('title', item.label);
        tmp_th.append(tmp_label).append(tmp_imput)
        .append(`<div class="resizeHelper ui-resizable-handle ui-resizable-e">&nbsp;</div>`);
        tr.append(tmp_th);

        let timeoutId;
        tmp_label.on("click", function (e) {
          timeoutId = setTimeout(() => {
            if(!timeoutId) return;
            me.tHead.find("th").not(tmp_th).find('label').removeClass("exp-pt-sorted-asc exp-pt-sorted-desc");
            if (tmp_label.hasClass("exp-pt-sorted-asc") || tmp_label.hasClass("exp-pt-sorted-desc")) {
              tmp_label.toggleClass("exp-pt-sorted-asc exp-pt-sorted-desc");
            } else {
              tmp_label.addClass("exp-pt-sorted-asc");
            }
            me.sortCmpID = item.columnID;
            me.sortDesc = tmp_label.hasClass("exp-pt-sorted-asc") ? false : true;
            me.createBody();
          }, 200);
        });

        tmp_label.on('dblclick', e => {
          clearTimeout(timeoutId);
          timeoutId = null;
          e.preventDefault();
          tmp_imput.show();
          tmp_label.hide();
          tmp_imput.on('blur', () => {
            tmp_label.show();
            tmp_imput.hide();
          });
          tmp_imput.on('keydown', e => {
            if(e.which === 13) {
              let inputVal = tmp_imput.val();
              this.resetHeaderLabel();
              if(inputVal && inputVal !== '') {
                tmp_label.text(inputVal).css({"text-decoration": "underline"});
                this.searchField = item.columnID;
                this.searchValue = inputVal;
              } else {
                tmp_label.text(item.label).css({"text-decoration": "none"});
                this.searchField = null;
                this.searchValue = null;
              }
              this.createBody();
              tmp_label.show();
              tmp_imput.hide();
              tmp_imput.off();
            }
          });
          tmp_imput.focus();
        });
      }

    }

    setTimeout(() => {
      this.tHead.append(tr);
      this.createBody();
      setTimeout(() => {
        this.registryTable.resizableColumns();
        Cons.hideLoader();
      }, 500);
    }, 100);
  },

  reset: function(){
    this.filterRights = [];
    this.allRights = [];
    this.filterName = null;
    this.filterCode = null;
    this.filterID = null;
    this.sortCmpID = null;
    this.sortDesc = false;
    this.searchField = null;
    this.searchValue = null;
    this.filterSearchUrl = null;
    this.selectedItems = [];

    this.registryTable = $('<table class="exp-table uk-table uk-table-small uk-table-divider uk-table-responsive">');
    this.colgroup = $('<colgroup>');
    this.tHead = $('<thead>');
    this.tBody = $('<tbody>');
    this.registryTable.append(this.colgroup).append(this.tHead).append(this.tBody);
    tableContainer.empty().append(this.registryTable);
    $('.action-menu-item').addClass('uk-disabled');
  },

  init: function(params){
    this.reset();
    rest.synergyGet(`api/registry/info?code=${params.registryCode}`, info => {
      let registryList = Cons.getAppStore().registryList;
      this.registryInfo = info;
      let heads = info.columns.filter(item => item.visible != '0');

      if(params.registryCode == 'experience_registry_assessment_survey') {
        heads.push({
          columnID: "experience_form_feedback_answersTableJSON",
          label: "json",
          order: 4
        });
      }

      heads.sort((a, b) => {
        if (a.order == 0) return 0;
        return a.order - b.order;
      });

      this.heads = heads.map(item => {
        return {label: item.label, columnID: item.columnID}
      });

      this.registryID = info.registryID;
      this.registryName = info.name;
      this.registryCode = params.registryCode;
      this.allRights = registryList.find(x => x.registryCode == this.registryCode).rights;
      this.formCode = info.formCode;
      Cons.setAppStore({formCode: info.formCode});

      if(params.filterCode) this.filterCode = params.filterCode;
      if(params.searchString) this.searchString = params.searchString;

      this.createHeader();
      Paginator.init();

      if(this.registryCode == 'experience_registry_assessment_survey') createMenuActions();

      if(['experience_registry_users', 'experience_registry_questions'].includes(this.registryCode)) {
        let menuContainer = $('<div uk-dropdown="mode: click">');
        let nav = $('<ul class="uk-nav uk-dropdown-nav">');
        let itemDeleted = $('<li class="action-menu-item item-delete uk-disabled"><a href="#">Удалить</a></li>');

        nav.append(itemDeleted);
        menuContainer.append(nav);
        $('#panel-service-action').append(menuContainer);

        itemDeleted.on('click', e => {
          e.preventDefault();
          e.target.blur();
          this.removeSelectedRow();
        });
      }

    });
  }
}

const formatDate = () => {
  let d = new Date();
  return ['0' + d.getDate(), '0' + (d.getMonth() + 1), '' + d.getFullYear()].map(x => x.slice(-2)).join('.');
}

const getSystemReport = () => {
  Cons.showLoader();
  AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/filters?registryCode=${registryTable.registryCode}&type=service`)
  .then(filters => {
    let filterID = filters.find(x => x.code === registryTable.filterCode);
    if(filterID) filterID = filterID.id;
    Cons.hideLoader();

    let url = `${window.location.origin}/Synergy/rest/reg/load/xls?r=${registryTable.registryID}`;
    url += `&l=ru&f=${filterID || ''}&s=&u=${AS.OPTIONS.currentUser.userid}&fn=${registryTable.registryName}_${formatDate()}`;
    window.open(url);
  });
}

tableContainer.on('setOpenRowSettings', e => {
  if(!e.hasOwnProperty('eventParam')) return;
  registryTable.pageCode = e.eventParam.pageCode;
  registryTable.playerCode = e.eventParam.playerCode;
});

tableContainer.on('renderNewTable', e => {
  if(!e.hasOwnProperty('eventParam')) return;
  registryTable.init(e.eventParam);
});

tableContainer.on('updateTableBody', e => {
  if(!registryTable.registryCode) return;
  if(Paginator.currentPage != 1) return;
  if(e.hasOwnProperty('eventParam')) {
    let param = e.eventParam;
    if(param.hasOwnProperty('filterCode')) {
      registryTable.filterCode = param.filterCode;
    }
    if(e.eventParam.hasOwnProperty('filterSearchUrl')) {
      registryTable.filterSearchUrl = null;
    }
  }
  Paginator.currentPage = 1;
  registryTable.createBody();
});

tableContainer.on('searchInRegistry', e => {
  if(!e.hasOwnProperty('eventParam')) return;
  if(e.eventParam.searchString && e.eventParam.searchString !== "") {
    registryTable.searchString = e.eventParam.searchString;
  } else {
    registryTable.searchString = null;
  }
  Paginator.currentPage = 1;
  registryTable.createBody();
});

tableContainer.on('filterRegistryRows', e => {
  if(!e.hasOwnProperty('eventParam')) return;
  if(e.eventParam.filterSearchUrl && e.eventParam.filterSearchUrl !== "") {
    registryTable.filterSearchUrl = e.eventParam.filterSearchUrl;
  } else {
    registryTable.filterSearchUrl = null;
    return;
  }
  Paginator.currentPage = 1;
  registryTable.createBody();
});

const getExcelAssessmentSurvey = () => {
  Cons.showLoader();

  try {

    let url = registryTable.getUrl(true);
    url += `&pageNumber=1&countInPart=1&loadData=false`;

    rest.synergyGet(url, part => {
      Cons.hideLoader();

      UIkit.modal.confirm(`Вы собираетесь выгрузить ${part.count} записей. Продолжить?`,
        {labels: {ok: i18n.tr('Да'), cancel: i18n.tr('Отмена')}})
      .then(() => {
        Cons.showLoader();

        rest.synergyGet(registryTable.getUrl(true), data => {
          const excelData = [];

          data.result.forEach(res => {
            const tmpValues = {};
            registryTable.heads.forEach(col => {
              if(col.columnID == "experience_form_feedback_answersTableJSON") {
                registryTable.customHeads.forEach(head => {
                  const customFields = res.fieldValue?.experience_form_feedback_answersTableJSON;
                  if(customFields) {
                    const json = JSON.parse(customFields);
                    const row = json.find(x => x.question_reglink?.key == head.columnID);
                    tmpValues[head.label] = row?.answer?.value || '';
                    tmpValues[`Комментарий - ${head.label}`] = row?.user_comment?.value || '';
                  }
                });
              } else {
                tmpValues[col.label] = res.fieldValue[col.columnID] || "";
              }
            });
            excelData.push(tmpValues);
          });

          const opts = {headers: true, column: {style:{Font:{Bold:"1"}}}};
          const result = alasql(`SELECT * INTO XLS("${registryTable.registryName}_${formatDate()}.xls",?) FROM ?`, [opts, excelData]);

          Cons.hideLoader();
        });
      }, () => null);
    });

  } catch (e) {
    console.log(e.message);
    Cons.hideLoader();
    showMessage('Произошла ошибка при выгрузке отчета', 'error');
  }
}

if($('#button-print-report').length) {
  $('#button-print-report').off().on('click', e => {
    e.preventDefault();
    e.target.blur();

    if(registryTable.registryCode == 'experience_registry_assessment_survey') {
      getExcelAssessmentSurvey();
    } else if(registryTable.searchString || registryTable.filterSearchUrl || registryTable.searchField) {
      Cons.showLoader();
      rest.synergyGet(registryTable.getUrl(true), data => {
        let excelData = [];
        data.result.forEach(res => {
          let tmpValues = {};
          registryTable.heads.forEach(col => tmpValues[col.label] = res.fieldValue[col.columnID] || "");
          excelData.push(tmpValues);
        });

        try {
          let opts = {headers: true, column: {style:{Font:{Bold:"1"}}}};
          let result = alasql(`SELECT * INTO XLS("${registryTable.registryName}_${formatDate()}.xls",?) FROM ?`, [opts, excelData]); //XLSXML ломается, скорее всего где-то косяк в данных
          Cons.hideLoader();
        } catch (e) {
          console.log(e.message);
          Cons.hideLoader();
          showMessage('Произошла ошибка при выгрузке отчета', 'error');
        }
      });
    } else {
      getSystemReport();
    }
  });
}

$(document).off()
.on('contextmenu', () => $('.exp-context-menu').remove())
.on('click', () => $('.exp-context-menu').remove())
.on('openDocumentInWindow', e => {
  if(e.hasOwnProperty('eventParam') && e.eventParam.dataUUID)
  openDocumentInWindow(e.eventParam.dataUUID);
});
//$(document).trigger({type: 'openDocumentInWindow', eventParam: {dataUUID: 3}})
