const getFullName = person => {
  const {firstname, lastname, patronymic} = person;
  const fio = lastname.substr(0, 1).toUpperCase() + lastname.substr(1) + ' ' + firstname.substr(0, 1).toUpperCase() + '.';
  return patronymic ? fio + ' ' + patronymic.substr(0, 1).toUpperCase() + '.' : fio;
}

const $exp = {
  userGroups: Cons.getAppStore().userGroups,

  logout: function() {
    AS.apiAuth.setCredentials(null, null);
    AS.OPTIONS.currentUser = {};
    Cons.logout();
    window.location.reload();
  },

  setProfileData: function() {
    let photoURL = `../Synergy/load?userid=${AS.OPTIONS.currentUser.userid}`;
    let profile = localStorage.getItem('profile-photo');
    if(profile) {
      profile = JSON.parse(profile);
      if(profile.userID == AS.OPTIONS.currentUser.userId) photoURL = profile.photo;
    }
    $('#profileName span').text(getFullName(AS.OPTIONS.currentUser));
    $('#profileImage').attr('src', photoURL);
    $('#profileName-menu span').text(getFullName(AS.OPTIONS.currentUser));
    $('#profileImage-menu').attr('src', photoURL);
  },

  emptyValidator: function(input) {
    if (input && input.text && input.text !== '') {
      fire({type: 'input_highlight', error: false}, input.code);
      return true;
    } else {
      fire({type: 'input_highlight', error: true}, input.code);
      return false;
    }
  }
};

window.$exp = $exp;

if($('.menu__icon').length) {
  let openMenu = false;
  let xDown = null;
  let yDown = null;

  const hideShowMenu = () => {
    if($('.filter-window').css('display') == 'block') return;
    if($('.exp-window-document').length) return;

    if(!openMenu) {
      openMenu = !openMenu;
      $('.menu-container').css({'display': 'flex'});
      setTimeout(() => {
        $('.menu__icon').addClass('_active');
        $('.menu-container').addClass('_open');
        $('body').addClass('lock');
      }, 0);
    } else {
      openMenu = !openMenu;
      $('.menu__icon').removeClass('_active');
      $('.menu-container').removeClass('_open');
      $('body').removeClass('lock');
      setTimeout(() => {
        $('.menu-container').css({'display': 'none'});
      }, 400);
    }
  }

  const handleTouchStart = evt => {
    xDown = evt.touches[0].clientX;
    yDown = evt.touches[0].clientY;
  }

  const handleTouchMove = evt => {
    if ( ! xDown || ! yDown ) return;
    let xUp = evt.touches[0].clientX;
    let yUp = evt.touches[0].clientY;
    let xDiff = xDown - xUp;
    let yDiff = yDown - yUp;

    if(Math.abs(xDiff) > Math.abs(yDiff)) {
      if (xDiff > 0) {
        //left swipe
        if(openMenu) hideShowMenu();
      } else {
        //right swipe
        if(!openMenu) hideShowMenu();
      }
    } else {
      if (yDiff > 0) {
        //up swipe
      } else {
        //down swipe
      }
    }
    xDown = null;
    yDown = null;
  }

  $('.menu__icon').off().on('click', e => {
    e.preventDefault();
    hideShowMenu();
  });

  $('.menu-container').off().on('click', e => {
    e.preventDefault();
    if($(e.target).hasClass('menu-container')) hideShowMenu();
    if($(e.target).hasClass('menu-button')) hideShowMenu();
  });

  $('#button-logout').off().on('click', e => {
    e.preventDefault();
    e.target.blur();
    $exp.logout();
  });

  document.addEventListener('touchstart', handleTouchStart, false);
  document.addEventListener('touchmove', handleTouchMove, false);
}

//Иконка приложения
if(!$('link[rel="icon"]').length) {
  $('head').append('<link rel="icon" href="data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgNDM4IDQzOCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPGNpcmNsZSBjeD0iNDAxIiBjeT0iMjE5IiByPSIzNyIgZmlsbD0icmVkIj48L2NpcmNsZT4KPGNpcmNsZSBjeD0iMTI4LjAwMDAwMDAwMDAwMDA2IiBjeT0iMzc2LjYxNjYyMzQ4ODc2Nzg2IiByPSIzNyIgZmlsbD0icmVkIj48L2NpcmNsZT4KPGNpcmNsZSBjeD0iMTI3Ljk5OTk5OTk5OTk5OTkxIiBjeT0iNjEuMzgzMzc2NTExMjMyMiIgcj0iMzciIGZpbGw9InJlZCI+PC9jaXJjbGU+CjxwYXRoIGQ9Ik0gMTgxLjE2MDA3MjI3MTE2Nzg1IDM5Ny4wMjI4NjMzMzM1NTI2NiBBIDE4MiAxODIgMCAwIDAgMzkyLjA5MjI4NTk2NTcxNzkgMjc1LjI0MTA5Mjk3NjI0MDQ1IiBmaWxsPSJub25lIiBzdHJva2U9InJlZCIgc3Ryb2tlLXdpZHRoPSIyNCI+PC9wYXRoPgo8cGF0aCBkPSJNIDgzLjc0NzY0MTc2MzExNDI1IDk3LjIxODIyOTY0MjY4NzggQSAxODIgMTgyIDAgMCAwIDgzLjc0NzY0MTc2MzExNDI3IDM0MC43ODE3NzAzNTczMTIyIiBmaWxsPSJub25lIiBzdHJva2U9InJlZCIgc3Ryb2tlLXdpZHRoPSIyNCI+PC9wYXRoPgo8cGF0aCBkPSJNIDM5Mi4wOTIyODU5NjU3MTc5IDE2Mi43NTg5MDcwMjM3NTk1NSBBIDE4MiAxODIgMCAwIDAgMTgxLjE2MDA3MjI3MTE2NzczIDQwLjk3NzEzNjY2NjQ0NzM5IiBmaWxsPSJub25lIiBzdHJva2U9InJlZCIgc3Ryb2tlLXdpZHRoPSIyNCI+PC9wYXRoPgo8bGluZSB4MT0iNDAxIiB5MT0iMjE5IiB4Mj0iMTI3Ljk5OTk5OTk5OTk5OTkxIiB5Mj0iNjEuMzgzMzc2NTExMjMyMiIgc3Ryb2tlPSJyZWQiIHN0cm9rZS13aWR0aD0iMjQiPjwvbGluZT4KPGxpbmUgeDE9IjQwMSIgeTE9IjIxOSIgeDI9IjEyOC4wMDAwMDAwMDAwMDAwNiIgeTI9IjM3Ni42MTY2MjM0ODg3Njc4NiIgc3Ryb2tlPSJyZWQiIHN0cm9rZS13aWR0aD0iMjQiPjwvbGluZT4KPGxpbmUgeDE9IjEyOC4wMDAwMDAwMDAwMDAwNiIgeTE9IjM3Ni42MTY2MjM0ODg3Njc4NiIgeDI9IjEyNy45OTk5OTk5OTk5OTk5MSIgeTI9IjYxLjM4MzM3NjUxMTIzMjIiIHN0cm9rZT0icmVkIiBzdHJva2Utd2lkdGg9IjI0Ij48L2xpbmU+CjxjaXJjbGUgY3g9IjIxOSIgY3k9IjIxOSIgcj0iNTQiIGZpbGw9InJlZCI+PC9jaXJjbGU+Cjwvc3ZnPgo=">')
}

if($('.button-analytics span').length) {
  $('.button-analytics span').empty().append(`<svg width="20" height="20" viewBox="0 0 20 20" fill="none" xmlns="http://www.w3.org/2000/svg">
<path d="M19.5475 17.3752H2.62484V0.452489C2.62484 0.202579 2.42222 0 2.17235 0C1.92249 0 1.71986 0.202579 1.71986 0.452489V17.3752H0.452489C0.202624 17.3752 0 17.5777 0 17.8276C0 18.0776 0.202624 18.2801 0.452489 18.2801H1.71986V19.5475C1.71986 19.7974 1.92249 20 2.17235 20C2.42222 20 2.62484 19.7974 2.62484 19.5475V18.2801H19.5475C19.7974 18.2801 20 18.0776 20 17.8276C20 17.5777 19.7974 17.3752 19.5475 17.3752Z" fill="#666666"/>
<path d="M5.06941 16.4702C5.91837 16.4702 6.609 15.7795 6.609 14.9306C6.609 14.5951 6.50081 14.2846 6.31796 14.0314L8.94099 10.8491C9.12194 10.924 9.32 10.9655 9.52769 10.9655C9.91873 10.9655 10.2759 10.8187 10.5478 10.5777L12.4938 11.764C12.4628 11.8855 12.4463 12.0127 12.4463 12.1437C12.4463 12.9927 13.137 13.6833 13.9859 13.6833C14.8348 13.6833 15.5255 12.9926 15.5255 12.1437C15.5255 11.6974 15.3344 11.295 15.03 11.0135L18.1922 4.42177C18.2743 4.43535 18.3584 4.44277 18.4442 4.44277C19.2932 4.44277 19.9838 3.75209 19.9838 2.90318C19.9838 2.05426 19.2931 1.36359 18.4442 1.36359C17.5953 1.36359 16.9046 2.05426 16.9046 2.90318C16.9046 3.34087 17.0885 3.73621 17.3828 4.0168L14.2145 10.6212C14.1399 10.6101 14.0636 10.6042 13.9859 10.6042C13.5949 10.6042 13.2377 10.751 12.9658 10.992L11.0198 9.80566C11.0508 9.68417 11.0673 9.55697 11.0673 9.42598C11.0673 8.57702 10.3766 7.88634 9.52769 7.88634C8.67877 7.88634 7.9881 8.57702 7.9881 9.42598C7.9881 9.73878 8.08212 10.0299 8.24303 10.2731L5.59647 13.484C5.43199 13.4238 5.25448 13.391 5.06941 13.391C4.22045 13.391 3.52982 14.0817 3.52982 14.9306C3.52982 15.7796 4.22045 16.4702 5.06941 16.4702ZM18.4443 2.26852C18.7942 2.26852 19.0789 2.55322 19.0789 2.90313C19.0789 3.25304 18.7942 3.53775 18.4443 3.53775C18.0943 3.53775 17.8096 3.25304 17.8096 2.90313C17.8096 2.55322 18.0943 2.26852 18.4443 2.26852ZM13.986 11.5091C14.3359 11.5091 14.6206 11.7938 14.6206 12.1437C14.6206 12.4936 14.3359 12.7783 13.986 12.7783C13.6361 12.7783 13.3514 12.4936 13.3514 12.1437C13.3514 11.7938 13.636 11.5091 13.986 11.5091ZM9.52773 8.79127C9.87764 8.79127 10.1623 9.07598 10.1623 9.42593C10.1623 9.77584 9.87764 10.0605 9.52773 10.0605C9.17782 10.0605 8.89312 9.77584 8.89312 9.42593C8.89307 9.07598 9.17778 8.79127 9.52773 8.79127ZM5.06941 14.2959C5.41932 14.2959 5.70402 14.5806 5.70402 14.9306C5.70402 15.2805 5.41932 15.5652 5.06941 15.5652C4.7195 15.5652 4.43479 15.2805 4.43479 14.9306C4.43479 14.5806 4.7195 14.2959 5.06941 14.2959Z" fill="#666666"/>
</svg>`);
}
