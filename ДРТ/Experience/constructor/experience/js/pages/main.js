let renderTable = false;

const getReviewTypeCheck = () => {
  let {n, p} = Cons.getAppStore().reviewChecked || {n: true, p: true};
  let container = $(`<div class="uk-margin uk-grid-small uk-child-width-auto uk-grid" style="font-family: 'Roboto', sans-serif;">`);
  let negativeCheck = $(`<label style="cursor: pointer;"><input class="uk-checkbox" type="checkbox" ${n ? 'checked' : ''}> Отрицательные</label>`);
  let positiveCheck = $(`<label style="cursor: pointer;"><input class="uk-checkbox" type="checkbox" ${p ? 'checked' : ''}> Положительные</label>`);
  container.append(negativeCheck).append(positiveCheck);
  $('#panel-actions').append(container);
  return {negative: negativeCheck.find('input'), positive: positiveCheck.find('input')};
}

const filterRowsReview = reviewCheck => {
  let regionBoss = $exp.userGroups.find(x => x.groupCode === "location");
  let {n, p} = Cons.getAppStore().reviewChecked || {n: true, p: true};
  let filterRows = Cons.getAppStore().filterRows;
  let filterCode = regionBoss ? 'assessment_survey_regoin' : 'assessment_survey_currentDev';

  if(n && p) {
    if(filterRows == 'all') filterCode = '';
  } else if(n && !p) {
    if(filterRows == 'all') {
      filterCode = 'assessment_survey_negative';
    } else {
      filterCode = regionBoss ? 'assessment_survey_region_negative' : 'assessment_survey_currentDev_negative';
    }
  } else if(!n && p) {
    if(filterRows == 'all') {
      filterCode = 'assessment_survey_positive';
    } else {
      filterCode = regionBoss ? 'assessment_survey_region_positive' : 'assessment_survey_currentDev_positive';
    }
  } else {
    filterCode = 'assessment_survey_empty';
  }

  Cons.setAppStore({filterCode: filterCode});

  if(!renderTable) {
    let eventParam = {
      registryCode: 'experience_registry_assessment_survey',
      filterCode: filterCode
    };
    Cons.setAppStore({registryCode: eventParam.registryCode});

    let searchValue = Cons.getAppStore().searchValueMain;
    if(searchValue && searchValue !== "") {
      $('#search-input').val(searchValue);
      eventParam.searchString = searchValue;
    }

    $('.exp-table-container').trigger({
      type: 'renderNewTable',
      eventParam: eventParam
    });

    renderTable = true;
  } else {
    $('.exp-table-container').trigger({
      type: 'updateTableBody',
      eventParam: {
        filterCode: filterCode
      }
    });
  }
}

pageHandler("main", () => {
  fire({type: 'init_filters'}, 'registry-filter');
  let reviewCheck = getReviewTypeCheck();
  let buttonFilterDep = $('#button-filter-dep');
  let buttonFilterAll = $('#button-filter-all');

  Cons.setAppStore({filterRows: 'dep'});
  $exp.setProfileData();

  reviewCheck.negative.on('change', e => {
    let checked = Cons.getAppStore().reviewChecked;
    let n = true;
    let p = true;

    if(checked) p = checked.p;
    n = e.target.checked;

    Cons.setAppStore({reviewChecked: {n, p}});
    filterRowsReview(reviewCheck);
  });
  reviewCheck.positive.on('change', e => {
    let checked = Cons.getAppStore().reviewChecked;
    let n = true;
    let p = true;

    if(checked) n = checked.n;
    p = e.target.checked;

    Cons.setAppStore({reviewChecked: {n, p}});
    filterRowsReview(reviewCheck);
  });

  buttonFilterDep.off().on('click', e => {
    e.preventDefault();
    buttonFilterDep.addClass("active");
    buttonFilterAll.removeClass("active");
    Cons.setAppStore({filterRows: 'dep'});
    filterRowsReview(reviewCheck);
  });
  buttonFilterAll.off().on('click', e => {
    e.preventDefault();
    buttonFilterAll.addClass("active");
    buttonFilterDep.removeClass("active");
    Cons.setAppStore({filterRows: 'all'});
    filterRowsReview(reviewCheck);
  });

  filterRowsReview(reviewCheck);

  let timerID = setInterval(() => {
    if(Cons.getCurrentPage().code !== 'main') {
      clearInterval(timerID);
      return;
    }
    $('.exp-table-container').trigger({type: 'updateTableBody'});
  }, 60000);

  $('#button-refresh').off().on('click', e => {
    e.preventDefault();
    e.target.blur();
    $('.exp-table-container').trigger({type: 'updateTableBody'});
  });

  $('#search-input').off().on('keyup', e => {
    if (e.keyCode === 13) {
      e.preventDefault();
      Cons.setAppStore({searchValueMain: $('#search-input').val()});
      $('.exp-table-container').trigger({
        type: 'searchInRegistry',
        eventParam: {
          searchString: $('#search-input').val()
        }
      });
    }
  });
});
