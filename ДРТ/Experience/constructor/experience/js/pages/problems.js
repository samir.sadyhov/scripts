pageHandler("problems", () => {
  fire({type: 'init_filters'}, 'registry-filter');
  $exp.setProfileData();

  let eventParam = {
    registryCode: 'experience_registry_problems_pdg'
  };
  Cons.setAppStore({registryCode: eventParam.registryCode});

  let searchValue = Cons.getAppStore().searchValueProblem;
  if(searchValue && searchValue !== "") {
    $('#search-input').val(searchValue);
    eventParam.searchString = searchValue;
  }

  $('.exp-table-container').trigger({
    type: 'renderNewTable',
    eventParam: eventParam
  });

  $('#button-refresh').off().on('click', e => {
    e.preventDefault();
    e.target.blur();
    $('.exp-table-container').trigger({type: 'updateTableBody'});
  });

  $('#search-input').off().on('keyup', e => {
    if (e.keyCode === 13) {
      e.preventDefault();
      Cons.setAppStore({searchValueProblem: $('#search-input').val()});
      $('.exp-table-container').trigger({
        type: 'searchInRegistry',
        eventParam: {
          searchString: $('#search-input').val()
        }
      });
    }
  });

  if (Cons.getAppStore().created_form_data_listener_problem) return;
  addListener('created_form_data', 'formPlayer-problem', e => {
    fire({type: 'set_hidden', hidden: true}, 'form-modal');
    showMessage('Запись успешно создана', 'success');
    $('.exp-table-container').trigger({type: 'updateTableBody'});
  });
  Cons.setAppStore({created_form_data_listener_problem: true});
});
