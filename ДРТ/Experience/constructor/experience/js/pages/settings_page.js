const changeCredentials = param => {
  return new Promise((resolve, reject) => {
    rest.synergyPost("api/filecabinet/user/changeCredentials", {
      actionCode: param.actionCode,
      login: param.newLogin,
      password: param.newPassword,
      passwordConfirm: param.confirmPassword
    }, "application/x-www-form-urlencoded; charset=UTF-8", res => {
      resolve(res);
    }, err => {
      reject(err);
    });
  });
}

const getNewAuthParams = () => {
  let newLogin = getCompByCode('new-login');
  let currentPassword = getCompByCode('current-password');
  let newPassword = getCompByCode('new-password');
  let confirmPassword = getCompByCode('confirm-password');

  if(!$exp.emptyValidator(currentPassword)) {
    showMessage(localizedText('Поле текущий пароль не заполнено', 'Поле текущий пароль не заполнено', 'Поле текущий пароль не заполнено', 'Поле текущий пароль не заполнено'), 'error');
    return;
  }

  if(currentPassword.text != AS.OPTIONS.password) {
    showMessage(localizedText('Введен не верный пароль', 'Введен не верный пароль', 'Введен не верный пароль', 'Введен не верный пароль'), 'error');
    fire({type: 'input_highlight', error: true}, currentPassword.code);
    return;
  } else {
    fire({type: 'input_highlight', error: false}, currentPassword.code);
  }

  if(!$exp.emptyValidator(newPassword)) {
    showMessage(localizedText('Поле новый пароль не заполнено', 'Поле новый пароль не заполнено', 'Поле новый пароль не заполнено', 'Поле новый пароль не заполнено'), 'error');
    return;
  }

  if(!$exp.emptyValidator(confirmPassword)) {
    showMessage(localizedText('Поле подтверждение пароля не заполнено', 'Поле подтверждение пароля не заполнено', 'Поле подтверждение пароля не заполнено', 'Поле подтверждение пароля не заполнено'), 'error');
    return;
  }

  if(newPassword.text != confirmPassword.text) {
    showMessage(localizedText('Пароли не совпадают', 'Пароли не совпадают', 'Пароли не совпадают', 'Пароли не совпадают'), 'error');
    fire({type: 'input_highlight', error: true}, newPassword.code);
    fire({type: 'input_highlight', error: true}, confirmPassword.code);
    return;
  } else {
    fire({type: 'input_highlight', error: false}, newPassword.code);
    fire({type: 'input_highlight', error: false}, confirmPassword.code);
  }

  return {
    newLogin: newLogin.text == AS.OPTIONS.login ? null : newLogin.text || null,
    currentPassword: currentPassword.text,
    newPassword: newPassword.text,
    confirmPassword: confirmPassword.text
  }
}

const changeAuthParam = () => {
  let param = getNewAuthParams();
  if(!param) return;

  if(param.newLogin) {
    param.actionCode = 'CHANGE_ALL';
  } else {
    param.actionCode = 'CHANGE_PASSWORD';
    param.newLogin = AS.OPTIONS.login;
  }

  Cons.showLoader();
  changeCredentials(param).then(res => {
    if(res.errorCode && res.errorCode != 0) throw new Error(res.errorMessage);
    Cons.hideLoader();
    showMessage(localizedText('Параметры авторизации изменены', 'Параметры авторизации изменены', 'Параметры авторизации изменены', 'Параметры авторизации изменены'), 'success');

    Cons.logout();
    Cons.login({login: param.newLogin, password: param.newPassword});
    AS.apiAuth.setCredentials(param.newLogin, param.newPassword);
    AS.OPTIONS.login = param.newLogin;
    AS.OPTIONS.password = param.newPassword;
    Cons.creds.login = param.newLogin;
    Cons.creds.password = param.newPassword;

  }).catch(err => {
    Cons.hideLoader();
    showMessage(err.message, 'error');
  });
}

const addInputSelectPhoto = () => {
  let inputFile = $(`<input id="image-file" type="file">`);
  let container = $(`<div>`);
  let ukFormCustom = $(`<div uk-form-custom="target: true" class="uk-inline"></div>`);

  ukFormCustom.append(inputFile)
  .append(`<input class="uk-input uk-form-width-medium" type="text" placeholder="Выберите фото">`)
  .append(`<span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: more" style="color: #1e87f0;"></span>`);

  container.append(ukFormCustom);
  $('#panel-select-file').append(container);
  return inputFile;
}

const uploadPhoto = file => {
  return new Promise((resolve, reject) => {
    let currentApp = Cons.getCurrentApp();
    let formData = new FormData();
    formData.append("uploadFormElement", file);

    $.ajax({
      url: AS.FORMS.ApiUtils.getFullUrl(`rest/api/person/photo/load?userid=${AS.OPTIONS.currentUser.userid}`),
      data: formData,
      cache: false,
      contentType: false,
      processData: false,
      headers: {
          "Authorization": "Basic " + btoa(unescape(encodeURIComponent(currentApp.defaultUser + ":" + currentApp.defaultUserPassword)))
      },
      type: 'POST',
      success: function(res){
        resolve(res);
      },
      error: function(err){
        reject(err);
      }
    });

  });
}

const fileFormat = ['png', 'jpeg', 'jpg'];

pageHandler("settings_page", () => {
  $exp.setProfileData();

  let isConfigurator = $exp.userGroups.find(x => x.groupCode === "experience_group_main");
  if(isConfigurator) {
    fire({type: 'set_hidden', hidden: false}, 'panel-settings-exp');
    fire({type: 'set_hidden', hidden: false}, 'panel-settings-org');
  }

  let inputFile = null;
  let file = null;
  let format = null;

  if(!Cons.getAppStore().modal_change_auth_listener) {
    addListener("set_hidden", "modal-change-auth", e => {
      if(e.hidden) return;
      fire({type: 'value_changed_input', value: AS.OPTIONS.login}, 'new-login');
    });
    Cons.setAppStore({modal_change_auth_listener: true});
  }

  addListener("set_hidden", "modal-change-photo", e => {
    if(e.hidden) return;

    let photoURL = `../Synergy/load?userid=${AS.OPTIONS.currentUser.userid}`;
    let profile = localStorage.getItem('profile-photo');
    if(profile) {
      profile = JSON.parse(profile);
      if(profile.userID == AS.OPTIONS.currentUser.userId) photoURL = profile.photo;
    }
    $('#photo-profile').attr('src', photoURL);

    inputFile = addInputSelectPhoto();

    inputFile.off().on('change', () => {
      file = inputFile[0].files[0];
      console.log(inputFile[0].files, file);
      format = file.name.substring(file.name.lastIndexOf(".")+1).toLowerCase();

      if(file.size > 16000000) {
        showMessage(localizedText('Размер файла не должен превышать 16 МБ', 'Размер файла не должен превышать 16 МБ', 'Размер файла не должен превышать 16 МБ', 'Размер файла не должен превышать 16 МБ'), 'error');
        return;
      }

      if(fileFormat.indexOf(format) == -1) {
        showMessage(localizedText('Неподдерживаемый формат изображения', 'Неподдерживаемый формат изображения', 'Неподдерживаемый формат изображения', 'Неподдерживаемый формат изображения'), 'error');
        return;
      }

      let reader = new FileReader();
      reader.onload = function (e) {
        $('#photo-profile').attr('src', e.target.result);
      }
      reader.readAsDataURL(file);
    });
  });

  if(!Cons.getAppStore().save_auth_param_listener) {
    addListener("button_click", "button-save-auth", e => {
      changeAuthParam();
    });
    Cons.setAppStore({save_auth_param_listener: true});
  }

  if(!Cons.getAppStore().button_save_photo_listener) {
    addListener("button_click", "button-save-photo", e => {
      if(!inputFile) return;
      file = inputFile[0].files[0];
      format = file.name.substring(file.name.lastIndexOf(".")+1).toLowerCase();

      if(file.size > 16000000) {
        showMessage(localizedText('Размер файла не должен превышать 16 МБ', 'Размер файла не должен превышать 16 МБ', 'Размер файла не должен превышать 16 МБ', 'Размер файла не должен превышать 16 МБ'), 'error');
        return;
      }

      if(fileFormat.indexOf(format) == -1) {
        showMessage(localizedText('Неподдерживаемый формат изображения', 'Неподдерживаемый формат изображения', 'Неподдерживаемый формат изображения', 'Неподдерживаемый формат изображения'), 'error');
        return;
      }

      Cons.showLoader();
      uploadPhoto(file).then(res => {
        Cons.hideLoader();
        showMessage(localizedText('Фото успешно загружено', 'Фото успешно загружено', 'Фото успешно загружено', 'Фото успешно загружено'), 'success');

        let reader = new FileReader();
        reader.onload = function (e) {
          $('#profileImage').attr('src', e.target.result);
          localStorage.setItem('profile-photo', JSON.stringify({
            userID: AS.OPTIONS.currentUser.userId,
            photo: e.target.result
          }));
        }
        reader.readAsDataURL(file);
      }).catch(err => {
        Cons.hideLoader();
        console.log(err);
        showMessage(localizedText('Ошибка при загрузке фото', 'Ошибка при загрузке фото', 'Ошибка при загрузке фото', 'Ошибка при загрузке фото'), 'error');
      });

    });
    Cons.setAppStore({button_save_photo_listener: true});
  }

});
