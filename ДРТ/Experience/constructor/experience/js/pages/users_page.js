pageHandler("users_page", () => {
  fire({type: 'init_filters'}, 'registry-filter');
  $exp.setProfileData();

  let eventParam = {
    registryCode: 'experience_registry_users'
  };
  Cons.setAppStore({registryCode: eventParam.registryCode});

  let searchValue = Cons.getAppStore().searchValueUsers;
  if(searchValue && searchValue !== "") {
    $('#search-input').val(searchValue);
    eventParam.searchString = searchValue;
  }

  $('.exp-table-container').trigger({
    type: 'renderNewTable',
    eventParam: eventParam
  });

  $('#button-refresh').off().on('click', e => {
    e.preventDefault();
    e.target.blur();
    $('.exp-table-container').trigger({type: 'updateTableBody'});
  });

  $('#search-input').off().on('keyup', e => {
    if (e.keyCode === 13) {
      e.preventDefault();
      Cons.setAppStore({searchValueUsers: $('#search-input').val()});
      $('.exp-table-container').trigger({
        type: 'searchInRegistry',
        eventParam: {
          searchString: $('#search-input').val()
        }
      });
    }
  });

  let asform = null;
  let formPlayerId = 'formPlayer-users';

  if (Cons.getAppStore().created_form_data_listener_users) return;

  addListener('loaded_form_data', formPlayerId, event => {
    asform = event;
    if(asform.view.getViewWithId('cjm_user_photo')) asform.view.getViewWithId('cjm_user_photo').container.parent().hide();
  });

  addListener("button_click", "create-button", function() {
    let valid = !asform.model.getErrors().length;
    if (valid) {
      fire({type: "set_disabled", disabled: true, cmpID: "create-button"}, "create-button");
      fire({
        type: "create_form_data",
        registryCode: "experience_registry_users",
        activate: true,
        success: (id, docid) => {
          console.log('success', id, docid);
        },
        error: (st, err) => {
          console.log("failed" + err.toString());
        }
      }, formPlayerId);
    } else {
      showMessage('Заполните обязательные поля', 'error');
    }
  });

  addListener('created_form_data', formPlayerId, e => {
    fire({type: 'set_hidden', hidden: true}, 'form-modal');
    showMessage('Запись успешно создана', 'success');
    $('.exp-table-container').trigger({type: 'updateTableBody'});
  });


  Cons.setAppStore({created_form_data_listener_users: true});
});
