pageHandler("mystery_shopper", () => {
  fire({type: 'init_filters'}, 'registry-filter');
  $exp.setProfileData();

  let eventParam = {
    registryCode: 'experience_registry_ms'
  };
  Cons.setAppStore({registryCode: eventParam.registryCode});

  let searchValue = Cons.getAppStore().searchValueMS;
  if(searchValue && searchValue !== "") {
    $('#search-input').val(searchValue);
    eventParam.searchString = searchValue;
  }

  $('.exp-table-container').trigger({
    type: 'renderNewTable',
    eventParam: eventParam
  });

  $('#button-refresh').off().on('click', e => {
    e.preventDefault();
    e.target.blur();
    $('.exp-table-container').trigger({type: 'updateTableBody'});
  });

  $('#search-input').off().on('keyup', e => {
    if (e.keyCode === 13) {
      e.preventDefault();
      Cons.setAppStore({searchValueMS: $('#search-input').val()});
      $('.exp-table-container').trigger({
        type: 'searchInRegistry',
        eventParam: {
          searchString: $('#search-input').val()
        }
      });
    }
  });

});
