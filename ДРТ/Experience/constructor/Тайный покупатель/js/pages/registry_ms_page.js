pageHandler("registry_ms_page", () => {
  $TP.setProfileData();

  let eventParam = {
    registryCode: 'experience_registry_ms',
    filterCode: 'my_notes'
  };
  Cons.setAppStore({registryCode: eventParam.registryCode});

  let searchValue = Cons.getAppStore().searchValue_registry_ms_page;
  if(searchValue && searchValue !== "") {
    $('#search-input').val(searchValue);
    eventParam.searchString = searchValue;
  }

  $('.exp-table-container').trigger({
    type: 'renderNewTable',
    eventParam: eventParam
  });

  fire({type: 'init_filters'}, 'registry-filter');

  $('#search-input').off().on('keyup', e => {
    if (e.keyCode === 13) {
      e.preventDefault();
      Cons.setAppStore({searchValue_registry_ms_page: $('#search-input').val()});
      $('.exp-table-container').trigger({
        type: 'searchInRegistry',
        eventParam: {
          searchString: $('#search-input').val()
        }
      });
    }
  });

  $('#button-refresh').off().on('click', e => {
    e.preventDefault();
    e.target.blur();
    $('.exp-table-container').trigger({type: 'updateTableBody'});
  });

  let asform;
  if(!Cons.getAppStore().form_modal_registry_ms_page) {

    addListener("set_hidden", "form-modal", e => {
      if(e.hidden) return;
      let closeModalButton = $('<span class="close-modal-button" uk-icon="icon: close; ratio: 2"></span>');
      $('.uk-modal-header').append(closeModalButton);
      closeModalButton.on('click', e => {
        fire({type: 'set_hidden', hidden: true}, 'form-modal');
      });
    });

    addListener('loaded_form_data', 'formPlayer-ms', e => {
      asform = e;
    });

    addListener("button_click", "button-create", function() {
      let valid = !asform.model.getErrors().length;
      if (valid) {
        fire({type: "set_disabled", disabled: true, cmpID: "button-create"}, "button-create");
        fire({
          type: "create_form_data",
          registryCode: 'experience_registry_ms',
          activate: true,
          success: (id, docid) => {
            fire({type: 'set_hidden', hidden: true}, 'form-modal');
            showMessage('Запись успешно создана', 'success');
            $('.exp-table-container').trigger({type: 'updateTableBody'});
          },
          error: (st, err) => {
            console.log("failed" + err.toString());
          }
        }, 'formPlayer-ms');
      } else {
        showMessage('Заполните обязательные поля', 'error');
      }
    });

    Cons.setAppStore({form_modal_registry_ms_page: true});
  }

});
