const emptyValidator = input => {
  if (input && input.text && input.text !== '') {
    fire({type: 'input_highlight', error: false}, input.code);
    return true;
  } else {
    fire({type: 'input_highlight', error: true}, input.code);
    return false;
  }
}

const changeCredentials = param => {
  return new Promise((resolve, reject) => {
    rest.synergyPost("api/filecabinet/user/changeCredentials", {
      actionCode: param.actionCode,
      login: param.newLogin,
      password: param.newPassword,
      passwordConfirm: param.confirmPassword
    }, "application/x-www-form-urlencoded; charset=UTF-8", res => {
      resolve(res);
    }, err => {
      reject(err);
    });
  });
}

const getNewAuthParams = () => {
  let newLogin = getCompByCode('new-login');
  let currentPassword = getCompByCode('current-password');
  let newPassword = getCompByCode('new-password');
  let confirmPassword = getCompByCode('confirm-password');

  if(!emptyValidator(currentPassword)) {
    showMessage(localizedText('Поле текущий пароль не заполнено', 'Поле текущий пароль не заполнено', 'Поле текущий пароль не заполнено', 'Поле текущий пароль не заполнено'), 'error');
    return;
  }

  if(currentPassword.text != AS.OPTIONS.password) {
    showMessage(localizedText('Введен не верный пароль', 'Введен не верный пароль', 'Введен не верный пароль', 'Введен не верный пароль'), 'error');
    fire({type: 'input_highlight', error: true}, currentPassword.code);
    return;
  } else {
    fire({type: 'input_highlight', error: false}, currentPassword.code);
  }

  if(!emptyValidator(newPassword)) {
    showMessage(localizedText('Поле новый пароль не заполнено', 'Поле новый пароль не заполнено', 'Поле новый пароль не заполнено', 'Поле новый пароль не заполнено'), 'error');
    return;
  }

  if(!emptyValidator(confirmPassword)) {
    showMessage(localizedText('Поле подтверждение пароля не заполнено', 'Поле подтверждение пароля не заполнено', 'Поле подтверждение пароля не заполнено', 'Поле подтверждение пароля не заполнено'), 'error');
    return;
  }

  if(newPassword.text != confirmPassword.text) {
    showMessage(localizedText('Пароли не совпадают', 'Пароли не совпадают', 'Пароли не совпадают', 'Пароли не совпадают'), 'error');
    fire({type: 'input_highlight', error: true}, newPassword.code);
    fire({type: 'input_highlight', error: true}, confirmPassword.code);
    return;
  } else {
    fire({type: 'input_highlight', error: false}, newPassword.code);
    fire({type: 'input_highlight', error: false}, confirmPassword.code);
  }

  return {
    newLogin: newLogin.text == AS.OPTIONS.login ? null : newLogin.text || null,
    currentPassword: currentPassword.text,
    newPassword: newPassword.text,
    confirmPassword: confirmPassword.text
  }
}

const changeAuthParam = () => {
  let param = getNewAuthParams();
  if(!param) return;

  if(param.newLogin) {
    param.actionCode = 'CHANGE_ALL';
  } else {
    param.actionCode = 'CHANGE_PASSWORD';
    param.newLogin = AS.OPTIONS.login;
  }

  Cons.showLoader();
  changeCredentials(param).then(res => {
    if(res.errorCode && res.errorCode != 0) throw new Error(res.errorMessage);
    Cons.hideLoader();
    showMessage(localizedText('Параметры авторизации изменены', 'Параметры авторизации изменены', 'Параметры авторизации изменены', 'Параметры авторизации изменены'), 'success');

    Cons.logout();
    Cons.login({login: param.newLogin, password: param.newPassword});
    AS.apiAuth.setCredentials(param.newLogin, param.newPassword);
    AS.OPTIONS.login = param.newLogin;
    AS.OPTIONS.password = param.newPassword;
    Cons.creds.login = param.newLogin;
    Cons.creds.password = param.newPassword;

  }).catch(err => {
    Cons.hideLoader();
    showMessage(err.message, 'error');
  });
}

pageHandler("personal_data_page", () => {
  $TP.setProfileData();

  if(!Cons.getAppStore().save_auth_param_listener) {
    addListener("button_click", "button-save-auth", e => {
      changeAuthParam();
    });
    Cons.setAppStore({save_auth_param_listener: true});
  }
});
