if (window.location.href.indexOf('Synergy') !== -1) return;

const setEnabled = event => {
  let cmpBlock = model.playerModel.getAsfData().data.filter(x => {
    if (
      x.type !== 'label' &&
      x.id !== 'cjm_status' &&
      x.id !== 'cjm_responsibleManager' &&
      x.id.slice(0,8) !== 'cjm_work'
    )
    return x;
  });

  cmpBlock.forEach(item => {
    let tmpView = view.playerView.getViewWithId(item.id);
    if(tmpView) {
      tmpView.setEnabled(false);
    }
  });

  setTimeout(() => {
    $('[data-asformid="custom.container.cjm_subdivision_name"] select').attr('disabled', true);
  }, 100);
}

if(editable) {
  if(model.customChange) return;
  setEnabled();
  model.customChange = true;
}
