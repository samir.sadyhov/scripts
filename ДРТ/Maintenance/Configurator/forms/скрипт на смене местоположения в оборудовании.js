const updateStatus = () => {
  if(!editable) return;

  const actionCheck = model.playerModel.getModelWithId('maintenance_form_equipment_actions');
  if(actionCheck) actionCheck.setValue(['1']);
}

model.on('valueChange', (_1, _2, value) => updateStatus());
