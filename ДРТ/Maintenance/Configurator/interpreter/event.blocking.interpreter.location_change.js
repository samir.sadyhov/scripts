var result = true;
var message = 'ok';

function customFormatDate(datetime){
  return ('0' + datetime.getDate()).slice(-2) + '.' +
  ('0' + (datetime.getMonth() + 1)).slice(-2) + '.' +
  datetime.getFullYear();
}

try {
  let currentFormData = API.getFormData(dataUUID);
  let actionCheck = UTILS.getValue(currentFormData, "maintenance_form_equipment_actions");

  if(!actionCheck || !actionCheck.hasOwnProperty('keys')) throw new Error('не найдено или не заполнено поле maintenance_form_equipment_actions');

  actionCheck = actionCheck.keys[0];

  if(actionCheck != '1') throw new Error('пропускаем, поле maintenance_form_equipment_actions = ' + actionCheck);

  let location = UTILS.getValue(currentFormData, "maintenance_form_equipment_location");
  let locationOld = UTILS.getValue(currentFormData, "maintenance_form_equipment_location_old");

  let historyData = [];
  let currentDate = new Date();

  //заполнение данных для истории
  UTILS.setValue(historyData, 'maintenance_form_location_history_from', locationOld || location);
  UTILS.setValue(historyData, 'maintenance_form_location_history_to', location);
  UTILS.setValue(historyData, 'maintenance_form_location_history_date', {
    type: 'date',
    key: UTILS.formatDate(currentDate, true),
    value: customFormatDate(currentDate)
  });
  UTILS.setValue(historyData, 'maintenance_form_location_history_equipment', {
    type: 'reglink',
    key: documentID,
    valueID: documentID,
    value: API.getDocMeaningContent(documentID)
  });

  //создание записи в реестре истории
  API.httpPostMethod("rest/api/registry/create_doc_rcc", {
    registryCode: 'maintenance_registry_location_history',
    sendToActivation: true,
    data: historyData
  }, "application/json; charset=utf-8");

  //сброс статуса
  UTILS.setValue(currentFormData, 'maintenance_form_equipment_actions', {
    "id": "maintenance_form_equipment_actions",
    "type": "check",
    "values": [null],
    "keys": [null]
  });

  //перезапись старого метоположения
  UTILS.setValue(currentFormData, 'maintenance_form_equipment_location_old', location);

  //сохранение текущей формы с новыми данными
  API.mergeFormData(currentFormData);

} catch (err) {
  message = err.message;
}
