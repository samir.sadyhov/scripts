/*
@param:
{
  type: "text/password", - может принимать только эти 2 значения, дефолтное значение text
  class: string, - не обязательно
  placeholder: string, - не обязательно
  label: string, - не обязательно

  icon: { - не обязательно, будет просто инпут
    type: "src/uikit/material", - тип иконки
    value: "", - название иконки если type uikit/material, путь к файлу если type src
    position: "left/right", - по умолчанию left
    clickHandler: function - будет выполнятся при клике на икноку, не обязательно (return icon event && input)
  }
}
*/

const generateGuid = prefix => prefix+'-'+Math.random().toString(36).substring(2, 15)+Math.random().toString(36).substring(2, 15);

this.CustomInput = class {
  constructor(param) {
    this.param = param;
    this.init();
  }

  get container(){
    return this.inputContainer;
  }

  get value(){
    return this.input.val();
  }

  set value(val){
    this.input.val(val);
  }

  addTranslateListener(){
    if($('.maintenance_locale_selector').length) {
      $('.maintenance_locale_selector').on('maintenance_change_locale', e => {
        if(this.label) this.inputLabel.text(i18n.tr(this.label));
        if(this.placeholder) this.input.attr('placeholder', i18n.tr(this.placeholder));
      });
    }
  }

  render(){
    this.inputContainer = $('<div>');
    this.input = $('<input>', {class: 'uk-input', type: 'text'});

    this.inputContainer.append(this.input);

    if(this.type && this.type == 'password') this.input.attr('type', this.type);
    if(this.placeholder) this.input.attr('placeholder', i18n.tr(this.placeholder));
    if(this.class) this.input.addClass(this.class);

    if(this.label) {
      const uuid = generateGuid('CI');
      this.inputLabel = $('<label>', {class: 'uk-form-label', for: uuid});
      this.inputLabel.text(i18n.tr(this.label));
      this.input.attr('id', uuid);
      this.inputContainer.prepend(this.inputLabel);
    }

    if(this.icon) {
      const span = $('<span>', {class: 'uk-form-icon'});

      this.inputContainer.addClass('uk-inline');
      this.inputContainer.prepend(span);

      switch (this.icon.type) {
        case 'uikit': {
          span.attr('uk-icon', `icon: ${this.icon.value}`);
          break;
        }
        case 'material': {
          span.addClass('material-icons');
          span.text(this.icon.value);
          break;
        }
        case 'src': {
          const img = $('<img>', {src: this.icon.value});
          span.append(img);
          break;
        }
      }

      if(this.icon.clickHandler && typeof this.icon.clickHandler == 'function') {
        span.on('click', e => {
          this.icon.clickHandler(e, this.input);
        });

        span.css({
          'cursor': 'pointer',
          'pointer-events': 'all'
        }).hover(
          function() {
            $( this ).css('filter', 'brightness(1.2)');
          }, function() {
            $( this ).css('filter', 'brightness(1)');
          }
        );
      }

      if(this.icon.position && this.icon.position == 'right') span.addClass('uk-form-icon-flip');

      if(this.label) span.css('top', '20px');
    }

    this.addTranslateListener();
  }

  init(){
    const keyTypes = ['type', 'class', 'placeholder', 'label', 'icon'];

    for(const key in this.param) {
      if(!keyTypes.includes(key)) continue;
      this[key] = this.param[key];
    }

    this.render();
  }
}
