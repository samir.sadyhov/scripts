this.RegistryColumns = class {
  constructor(registryInfo, successHandler) {
    this.registryInfo = registryInfo;
    this.successHandler = successHandler;

    this.init();
  }

  get columns(){
    return this.userColumns;
  }

  open(){
    UIkit.modal(this.dialog).show();
  }

  addListeners(){
    this.buttonApply.on('click', e => {

      this.userColumns = this.registryColumns.map(item => {
        const column = {...item};
        const checkbox = this.dialog.find(`input[columnid="${column.columnID}"]`);
        column.visible = checkbox.prop('checked') ? '1' : '0';
        return column;
      }).filter(item => item.visible == '1');

      localStorage.setItem(this.storageKey, JSON.stringify(this.userColumns));

      if(this.successHandler && typeof this.successHandler == 'function') this.successHandler(this.userColumns);

      UIkit.modal(this.dialog).hide();
    });
  }

  renderDialog(body){
    this.dialog = $('<div class="uk-flex-top" uk-modal>');
    const md = $('<div class="uk-modal-dialog uk-margin-auto-vertical">');
    const modalBody = $('<div class="uk-modal-body" uk-overflow-auto>');
    const footer = $('<div class="uk-modal-footer uk-text-right">');

    const buttonClose = $(`<button class="uk-button uk-button-default uk-modal-close" type="button">${i18n.tr("Отмена")}</button>`);
    this.buttonApply = $(`<button class="uk-button uk-button-primary" type="button">${i18n.tr("Применить")}</button>`);

    footer.css({
      'display': 'flex',
      'justify-content': 'end',
      'gap': '12px'
    });

    md.css('min-width', '800px');

    modalBody.append(body);
    footer.append(buttonClose, this.buttonApply);
    md.append(
      `<button class="uk-modal-close-default modal-close-custom" type="button" uk-close></button>`,
      `<div class="uk-modal-header"><h3>${i18n.tr("Настройка столбцов")}</h3></div>`,
      modalBody, footer
    );
    this.dialog.append(md);

    this.addListeners();
  }

  render() {
    const container = $('<div>', {class: "uk-column-1-2 uk-column-divider"});

    for(let i = 0; i < this.registryColumns.length; i++) {
      const {number, columnID, label, visible} = this.registryColumns[i];

      if(label == '') continue;

      const columnName = label == '' ? columnID : label;
      const checkbox = $('<input>', {class: "uk-checkbox", type: "checkbox", columnid: columnID});
      const checkboxLabel = $('<label>');

      checkboxLabel.css({
        'width': '100%',
        'display': 'block',
        'margin': '10px 0',
        'user-select': 'none'
      });

      if(this.userColumns) {
        const uc = this.userColumns.find(x => x.columnID == columnID);
        if(uc && uc.visible == '1') checkbox.prop('checked', true);
      } else {
        if(visible == '1') checkbox.prop('checked', true);
      }

      checkboxLabel.append(checkbox, ` ${columnName}`);
      container.append(checkboxLabel);
    }

    this.renderDialog(container);
  }

  init(){
    const {columns, code} = this.registryInfo;
    this.storageKey = `registry_columns_${code}_${AS.OPTIONS.currentUser.userid}`;
    this.userColumns = localStorage.getItem(this.storageKey);

    if(this.userColumns) this.userColumns = JSON.parse(this.userColumns);

    this.registryColumns = columns.sort((a, b) => {
      if (a.order == 0) return 0;
      return a.order - b.order;
    });

    this.render();
  }
}
