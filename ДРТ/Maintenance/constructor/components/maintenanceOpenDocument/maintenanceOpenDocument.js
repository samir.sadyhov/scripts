const getFileModels = formPlayer => {
  const result = [];
  formPlayer.model.models[0].modelBlocks[0].forEach(block => {
    if(block.asfProperty.type === "file") result.push(block);
    if(block.asfProperty.type === "table") {
      if(block.modelBlocks.length > 0) {
        block.modelBlocks.forEach(row => {
          row.forEach(tableBlock => {
            if(tableBlock.asfProperty.type === "file") result.push(tableBlock);
          });
        });
      }
    }
  });
  return result;
}

const initOpenFilesInForm = formPlayer => {
  const fileModels = getFileModels(formPlayer);

  fileModels.forEach(fileModel => {
    const props = fileModel.asfProperty;
    const tmpView = formPlayer.view.getViewWithId(props.id, props.ownerTableId, props.tableBlockIndex);

    //отключаем все стандартные события
    setTimeout(() => {
      formPlayer.view.container.find(`[data-asformid="file.filename.${props.id}"]`).off();
      fileModel.on('valueChange', () => {
        formPlayer.view.container.find(`[data-asformid="file.filename.${props.id}"]`).off();
        return null;
      });
    }, 100);

    //вешаем свое событие
    if(tmpView) {
      tmpView.container.off().on('click', () => {
        if(!fileModel.value) return;
        if(!fileModel.value.hasOwnProperty('identifier')) return;
        const file = new AttachmentFile(fileModel.value.name, fileModel.value.identifier);
        file.open();
      });
    }

  });
}

const renderDocumentWindow = (_doc, closeDocumentHandler) => {
  let {formPlayer, docActions, dataUUID, editable, canEdit} = _doc;

  const rootPanel = $('.maintenance-content-container');
  const container = $('<div>', {class: 'maintenance_document_window', data_uuid: dataUUID});
  const header = $('<div>', {class: 'maintenance_document_header'});
  const content = $('<div>', {class: 'maintenance_document_content'});

  const buttonSave = $('<span class="document_button_icon icon_save" style="display: none;"></span>');
  const buttonPrint = $('<span class="document_button_icon icon_print"></span>');
  const buttonEditable = $('<span class="document_button_icon icon_edit"></span>');
  const buttonClose = $('<span class="document_button_icon icon_close"></span>');
  const buttonActivate = $('<button>', {class: "uk-button uk-button-primary uk-button-small uk-hidden"});

  if(!canEdit) editable = false;

  formPlayer.showFormData(null, null, dataUUID);

  const run = docActions.find(x => x.operation == 'RUN');
  if(run) {
    buttonActivate.text(run.label);
    buttonActivate.removeClass('uk-hidden');
  }

  const setEditable = () => {
    formPlayer.view.setEditable(editable);
    if(editable) {
      buttonEditable.removeClass('icon_edit');
      buttonEditable.addClass('icon_view');
      buttonPrint.hide();
      buttonSave.show();
    } else {
      buttonEditable.removeClass('icon_view');
      buttonEditable.addClass('icon_edit');
      buttonPrint.show();
      if(formPlayer.model.hasChanges) {
        buttonSave.show();
      } else {
        buttonSave.hide();
      }
    }
  }

  const saveFormData = (succesHandler) => {
    Cons.showLoader();
    if(!formPlayer.model.isValid()) {
      showMessage(i18n.tr('Заполните обязательные поля'), 'error');
      Cons.hideLoader();
    } else {
      formPlayer.saveFormData(result => {
        showMessage(i18n.tr('Данные сохранены'), 'success');
        if(!run) appAPI.modifyDoc(formPlayer.model.asfDataId);
        Cons.hideLoader();
        formPlayer.model.hasChanges = false;
        setTimeout(() => {
          formPlayer.model.hasChanges = false;
        }, 1000);
        if(succesHandler) succesHandler();
      }, (errStatus, errMessage) => {
        console.log('ERROR saveFormData', {errStatus, errMessage});
        Cons.hideLoader();
      });
    }
  }

  const closeWindow = () => {
    container.remove();
    formPlayer.destroy();
    window.location.hash = '';
    if(closeDocumentHandler) closeDocumentHandler();
  }

  setEditable();

  buttonSave.on('click', e => {
    if(!formPlayer.model.hasChanges) return;
    saveFormData();
  });

  buttonActivate.on('click', e => {
    Cons.showLoader();
    if(!formPlayer.model.isValid()) {
      showMessage(i18n.tr('Заполните обязательные поля'), 'error');
      Cons.hideLoader();
    } else {
      formPlayer.saveFormData(async result => {
        await appAPI.activateDoc(formPlayer.model.asfDataId);
        showMessage(i18n.tr('Данные сохранены'), 'success');

        closeWindow();

        Cons.hideLoader();
      }, (errStatus, errMessage) => {
        console.log('ERROR saveFormData', {errStatus, errMessage});
        Cons.hideLoader();
      });
    }
  });

  buttonPrint.on('click', e => {
    if(formPlayer.model.hasPrintable) {
      window.open(`../Synergy/rest/asforms/template/print/form?format=pdf&dataUUID=${uuid}`);
    } else {
      UTILS.printForm(formPlayer.view.container[0]);
    }
  });

  if(canEdit) {
    buttonEditable.on('click', e => {
      editable = !editable;
      setEditable();
    });
  }

  buttonClose.on('click', e => {
    if(formPlayer.model.hasChanges) {
      UIkit.modal.confirm(i18n.tr('Документ был изменен. Сохранить произведенные изменения?'),
      {labels: {ok: i18n.tr('Да'), cancel: i18n.tr('Отмена')}})
      .then(() => {
        saveFormData(() => {
          closeWindow();
        });
      }, closeWindow);
    } else {
      closeWindow();
    }
  });

  const block1 = $('<div>');
  const block2 = $('<div>');

  block1.append(buttonSave, buttonPrint, buttonActivate);
  if(canEdit) {
    block2.append(buttonEditable, buttonClose);
  } else {
    block2.append(buttonClose);
  }

  header.append(block1, block2);
  content.append(formPlayer.view.container);
  container.append(header, content);
  rootPanel.append(container);

  formPlayer.model.on(AS.FORMS.EVENT_TYPE.formShow, (event, playerModel, playerView) => {
    setTimeout(() => {
      initOpenFilesInForm(formPlayer);
    }, 500);
  });
}

const filterAgrProcesses = processes => {
  const procCode = ['APPROVAL_ITEM', 'AGREEMENT_ITEM', 'ACQUAINTANCE_ITEM'];
  const result = [];
  function search(p) {
    p.forEach(function(process) {
      if (!process.finished && procCode.includes(process.typeID)) {
        result.push(process);
      }
      if (process.subProcesses.length > 0) search(process.subProcesses);
    });
  }
  search(processes);
  return result;
}

const getUserWork = (processes, userID) => {
  const result = [];
  function search(p) {
    p.forEach(function(process) {
      if (!process.finished &&
        process.actionID != "" &&
        (process.responsibleUserID == userID || process.authorID == userID)
      ) {
        result.push(process);
      }
      if (process.subProcesses.length > 0) search(process.subProcesses);
    });
  }
  search(processes);

  return result.sort((a, b) => UTILS.parseDateTime(b.started) - UTILS.parseDateTime(a.started));
}

const getResponsibleUserWork = processes => {
  const result = [];
  function search(p) {
    p.forEach(function(process) {
      if (!process.finished &&
        process.actionID != "" &&
        process.responsibleUserID == AS.OPTIONS.currentUser.userid
      ) {
        result.push(process);
      }
      if (process.subProcesses.length > 0) search(process.subProcesses);
    });
  }
  search(processes);

  return result.sort((a, b) => UTILS.parseDateTime(b.started) - UTILS.parseDateTime(a.started));
}

class DocumentParams {
  constructor(){
    this.#init([...arguments]);
  }

  #init(args){
    for(let x = 0; x < args.length; x++) {
      const arg = args[x];
      if(arg && arg instanceof Object && !Array.isArray(arg)) for(const key in arg) this[key] = arg[key];
    }
  }
}

const canEdit = async _doc => {
  const {process, docInfo, workInfo, registryInfo} = _doc;
  const acqAgrProc = filterAgrProcesses(process);

  if(!docInfo) return false;
  if(docInfo.registered == "true") return false;
  if(registryInfo.rr_edit != "Y") return false;

  if(workInfo) {
    const {actionID, has_subprocesses, parent_process} = workInfo;
    const procCode = ['approval-single', 'agreement-single', 'acquaintance-single'];
    if(procCode.includes(parent_process)) return false;

    if(has_subprocesses == "true") {
      const subworks = await appAPI.getSubworks(actionID);
      const filtered = subworks.filter(x => procCode.includes(x.parent_process));
      if(filtered.length) return false;
    }
  }

  return true;
}

const initParams = async _doc => {
  try {
    Cons.showLoader();

    _doc.docInfo = await appAPI.getDocumentInfo(_doc.documentID);
    _doc.dataUUID = _doc.docInfo?.asfDataID || null;
    _doc.process = await AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/workflow/get_execution_process?documentID=${_doc.documentID}&locale=${AS.OPTIONS.locale}`);

    _doc.processResponsible = getResponsibleUserWork(_doc.process);
    _doc.process = getUserWork(_doc.process, AS.OPTIONS.currentUser.userid);

    if(_doc.process.length) {
      let actionID;
      const currentUserWork = _doc.process.find(x => x.responsibleUserID == AS.OPTIONS.currentUser.userid);
      if(currentUserWork) {
        actionID = currentUserWork.actionID;
      } else {
        actionID = _doc.process[_doc.process.length - 1].actionID;
      }
      _doc.workInfo = await appAPI.getWorkInfo(actionID);
      if(_doc.workInfo) {
        _doc.workInfo.documentID = _doc.documentID;
        _doc.workInfo.dataUUID = _doc.dataUUID;
      }
    }

    _doc.docActions = await AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/docflow/document_actions?documentID=${_doc.documentID}&locale=${AS.OPTIONS.locale}`);

    _doc.registryInfo = await appAPI.getRegistryInfoByID(_doc.docInfo.registryID);

    const msgErrorRead = 'Вам запрещен доступ к этому документу';
    if(!_doc.registryInfo) throw new Error(msgErrorRead);
    if(_doc.registryInfo.hasOwnProperty('rights') && _doc.registryInfo.rights == "no") throw new Error(msgErrorRead);
    if(_doc.registryInfo.rr_read != "Y") throw new Error(msgErrorRead);

    _doc.registryCode = _doc.registryInfo.code;

    _doc.formPlayer = AS.FORMS.createPlayer();

    _doc.canEdit = await canEdit(_doc);

    Cons.hideLoader();
  } catch (err) {
    Cons.hideLoader();
    console.log(err);
    UIkit.notification.closeAll();
    showMessage(i18n.tr(err.message), 'error');
  }
}

this.maintenanceOpenDocument = async (documentID, closeDocumentHandler, editable = false) => {
  try {

    const _doc = new DocumentParams({type: 'document', documentID, editable});

    await initParams(_doc);

    renderDocumentWindow(_doc, closeDocumentHandler);
  } catch (err) {
    console.log(err);
    UIkit.notification.closeAll();
    showMessage(i18n.tr(err.message), 'error');
  }
}

$(document).mouseup(event => {
	setTimeout(() => {
		AS.FORMS.popupPanel.hide();
	}, 0);
}).click(event => {
	AS.FORMS.popupPanel.hide();
});
