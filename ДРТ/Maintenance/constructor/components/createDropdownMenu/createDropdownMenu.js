/*
@param:
el - контейнер куда будет вставлено меню
items - список (массив) пунктов меню
если в массиве будет слово 'divider' - то будет отрисован разделитель
{
  name: - наименование пункта меню
  handler: функция при клике на пункт менюб
  disabled: true/false - заблокирован ли пункт меню, по умолчанию false

  icon: {
    type: "src/uikit/material", - тип иконки
    value: "", - название иконки если type uikit/material, путь к файлу если type src
  }
}
*/

const getMenuItem = item => {
  const {name, icon, handler, disabled = false} = item;
  const li = $('<li>');
  const a = $('<a>');

  if(disabled) li.addClass('uk-disabled');

  const updateLink = () => {
    if(icon) {
      const {type, value} = icon;
      const span = $('<span>', {class: 'uk-margin-small-right'});

      switch (type) {
        case 'uikit': {
          span.attr('uk-icon', `icon: ${value}`);
          break;
        }
        case 'material': {
          span.addClass('material-icons');
          span.text(value);
          break;
        }
        case 'src': {
          const img = $('<img>', {src: value});
          span.append(img);
          break;
        }
      }

      a.append(span, i18n.tr(name));
    } else {
      a.append(i18n.tr(name));
    }
  }

  updateLink();

  li.append(a);

  li.on('click', e => {
    e.preventDefault();
    e.target.blur();
    if(handler && typeof handler == 'function') handler();
  });

  $('.maintenance_locale_selector').on('maintenance_change_locale', e => {
    updateLink();
  });

  return li;
}

this.createDropdownMenu = (el, items) => {
  const menuContainer = $('<div uk-dropdown="mode: click">');
  const nav = $('<ul class="uk-nav uk-dropdown-nav">');

  items.forEach(item => {
    if(item == 'divider') {
      nav.append('<li class="uk-nav-divider"></li>');
    } else {
      nav.append(getMenuItem(item));
    }
  });

  menuContainer.append(nav);
  el.append(menuContainer);
}
