const getUserInfo = () => {
  const {firstname, lastname, patronymic, mail, userid} = AS.OPTIONS.currentUser;
  const fio = lastname.substr(0, 1).toUpperCase() + lastname.substr(1) + ' ' + firstname.substr(0, 1).toUpperCase() + firstname.substr(1);
  const photoURL = `../Synergy/load?userid=${userid}`;

  return {fio, mail, photoURL};
}

const menu = {
  panelContainer: null,
  store: {
    menu_hide: false,
    selected: null,
    subSelected: null
  },

  renderTop: function(){
    const logo = $('<img>', {class: 'menu_app_logo'});
    logo.attr('src', this.appLogo);

    const title = $('<span>', {class: 'menu_app_title fonts'});
    title.text(this.appName[AS.OPTIONS.locale]);

    this.topContainer.append(logo, title);

    if($('.maintenance_locale_selector').length) {
      $('.maintenance_locale_selector').on('maintenance_change_locale', e => {
        title.text(this.appName[AS.OPTIONS.locale]);
      });
    }
  },

  adjustPosition: function(block){
    const windowHeight = $(window).height();
    const blockOffset = block.offset().top;
    const blockHeight = block.outerHeight();

    if (blockOffset + blockHeight > windowHeight) {
      const pos = windowHeight - ((blockHeight * 1.7) + blockOffset);
      block.css("transform", `translateY(${pos}px)`);
    }
  },

  getNavItem: function(item, setStore = false, isBadge = false){
    const {id, name, icon, handler, children = []} = item;
    const li = $('<li>', {id: id});
    const a = $('<a>');
    const itemName = $('<span>', {class: 'nav-item-name'});

    itemName.text(name[AS.OPTIONS.locale]);

    if(icon) {
      const {type, value} = icon;
      const span = $('<span>', {class: 'nav-item-icon'});

      switch (type) {
        case 'uikit': {
          span.attr('uk-icon', `icon: ${value}`);
          break;
        }
        case 'material': {
          span.addClass('material-icons');
          span.text(value);
          break;
        }
        case 'src': {
          const img = $('<img>', {src: value});
          span.append(img);
          break;
        }
      }

      a.append(span);
    }

    a.append(itemName);

    if(isBadge) {
      const badge = $('<span>', {class: 'nav-item-badge'});
      a.append(badge);
    }

    a.on('click', e => {
      e.preventDefault();

      if(setStore) {
        this.store.selected = id;
        if(children && children.length) {
          this.store.subSelected = children[0].id;
        } else {
          this.store.subSelected = null;
        }
        localStorage.setItem(this.storageKey, JSON.stringify(this.store));
      }

      if(handler && typeof handler == 'function') handler();
    });

    li.on('mouseenter', e => {
      if(!li.hasClass('active') || this.store.menu_hide) {
        li.addClass('is-over');
        const subNav = li.find('.sidebar_sub_nav');
        if(subNav && subNav.length) this.adjustPosition(subNav);
      }
    }).on('mouseleave', e => {
      li.removeClass('is-over');
      const subNav = li.find('.sidebar_sub_nav');
      if(subNav && subNav.length) subNav.css("transform", '');
    });

    if($('.maintenance_locale_selector').length) {
      $('.maintenance_locale_selector').on('maintenance_change_locale', e => {
        itemName.text(name[AS.OPTIONS.locale]);
      });
    }

    if(this.store.selected == id) li.addClass('active');

    li.append(a);

    return li;
  },

  getSubNavItem: function(item, parentID){
    const {id, name, handler} = item;
    const li = $('<li>');
    const a = $('<a>');
    const itemName = $('<span>', {class: 'nav-item-name'});

    a.on('click', e => {
      e.preventDefault();

      this.store.selected = parentID;
      this.store.subSelected = id;
      localStorage.setItem(this.storageKey, JSON.stringify(this.store));

      if(handler && typeof handler == 'function') handler();
    });

    itemName.text(name[AS.OPTIONS.locale]);

    if($('.maintenance_locale_selector').length) {
      $('.maintenance_locale_selector').on('maintenance_change_locale', e => {
        itemName.text(name[AS.OPTIONS.locale]);
      });
    }

    if(this.store.subSelected == id) {
      $(`#${parentID}`).addClass('active');
      li.addClass('active');
    }

    a.append(itemName);
    li.append(a);

    return li;
  },

  renderCenter: function(){
    const navContainerTop = $('<div>', {class: 'nav_container_top'});
    this.centerContainer.append(navContainerTop);

    const navTop = $('<ul>', {class: 'sidebar_nav'});
    navContainerTop.append(navTop);

    for(let i = 0; i < this.menuItems.length; i++) {
      const item = this.menuItems[i];
      if(!item.visible) continue;

      const menuItem = this.getNavItem(item, true, true);
      navTop.append(menuItem);

      if(item.hasOwnProperty('children') && item.children.length) {
        const subNav = $('<ul>', {class: 'sidebar_sub_nav'});
        menuItem.append(subNav);

        for(let j = 0; j < item.children.length; j++) {
          const subItem = item.children[j];
          const subMenuItem = this.getSubNavItem(subItem, item.id);

          subNav.append(subMenuItem);
        }
      }
    }

    if(this.itemLogout.visible || this.itemSettings.visible) {
      const navContainerBottom = $('<div>', {class: 'nav_container_bottom'});
      this.centerContainer.append(navContainerBottom);

      const navBottom = $('<ul>', {class: 'sidebar_nav'});
      navContainerBottom.append(navBottom);

      if(this.itemSettings.visible) navBottom.append(this.getNavItem(this.itemSettings, true, false));
      if(this.itemLogout.visible) navBottom.append(this.getNavItem(this.itemLogout, false, false));
    }

  },

  renderBottom: function(){
    const {fio, mail, photoURL} = getUserInfo();

    const photo = $('<img>', {class: 'menu_user_photo'});
    photo.attr('src', photoURL);

    const info = $('<div>', {class: 'menu_user_info_container'});
    const fioLabel = $('<span>', {class: 'user_fio fonts'});
    const mailLabel = $('<span>', {class: 'user_mail fonts'});

    fioLabel.text(fio);
    mailLabel.text(mail);

    info.append(fioLabel, mailLabel);

    this.bottomContainer.append(photo, info);
  },

  renderToggle: function(){
    const toggle = $(`<a>`, {class: 'menu_toggle_button', 'uk-icon': 'icon: chevron-left; ratio: 1.5;'});

    toggle.on('click', e => {
      e.preventDefault();
      $('.maintenance-content-container').toggleClass("menu_hide");
      this.store.menu_hide = !this.store.menu_hide;
      localStorage.setItem(this.storageKey, JSON.stringify(this.store));
    });

    this.panelContainer.append(toggle);
  },

  render: function(){
    this.sidebarContainer = $('<div>', {class: 'sidebar_container'});
    this.topContainer = $('<div>', {class: 'top_container'});
    this.centerContainer = $('<div>', {class: 'center_container'});
    this.bottomContainer = $('<div>', {class: 'bottom_container'});

    this.sidebarContainer.append(this.topContainer, this.centerContainer, this.bottomContainer);

    this.panelContainer.append(this.sidebarContainer);

    this.panelContainer.css('position', 'relative');

    this.renderTop();
    this.renderCenter();
    this.renderBottom();
    this.renderToggle();
  },

  init: function(panelContainer){
    this.panelContainer = panelContainer;

    for(const key in sidebarParam) this[key] = sidebarParam[key];

    const {code: appCode} = Cons.getCurrentApp();
    this.storageKey = `sidebar_${appCode}_${AS.OPTIONS.currentUser.userid}`;

    this.store = localStorage.getItem(this.storageKey);
    if(this.store) {
      this.store = JSON.parse(this.store);
    } else {
      const activeItem = this.menuItems.filter(x => x.visible)[0];

      this.store = {
        menu_hide: false,
        selected: null,
        subSelected: null
      };

      this.store.selected = activeItem.id;

      if(activeItem.hasOwnProperty('children') && activeItem.children.length) {
        const activeSubItem = activeItem.children.filter(x => x.visible);
        if(activeSubItem && activeSubItem.length) {
          this.store.subSelected = activeSubItem[0].id;
        }
      }

      localStorage.setItem(this.storageKey, JSON.stringify(this.store));
    }

    if(this.store && this.store.menu_hide) {
      $('.maintenance-content-container').addClass("menu_hide");
    }

    this.render();
  }
}

this.sidebar = panelContainer => {
  if(!panelContainer || !panelContainer.length) return;
  panelContainer.empty();
  menu.init(panelContainer);
}
