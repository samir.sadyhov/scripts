this.sidebarParam = {
  appName: {
    ru: 'Maintenance',
    kk: 'Maintenance',
    en: 'Maintenance'
  },
  appLogo: '../constructorFiles/maintenance/logo.svg',

  itemSettings: {
    visible: false,
    name: {
      ru: 'Настройки',
      kk: 'Анықтамалар',
      en: 'Directories'
    },
    icon: {
      type: 'src',
      value: '../constructorFiles/maintenance/menuIcon/params.svg'
    },
    handler: function(){
      fire({type: 'goto_page', pageCode: ''}, 'root-panel');
    }
  },

  itemLogout: {
    visible: true,
    name: {
      ru: 'Выход',
      kk: 'Шығу',
      en: 'Exit'
    },
    icon: {
      type: 'src',
      value: '../constructorFiles/maintenance/menuIcon/logout.svg'
    },
    handler: function(){
      Cons.logout();
    }
  },

  menuItems: []
}

sidebarParam.menuItems.push({
  visible: true,
  id: 'item_menu_1',
  name: {
    ru: 'Наряды',
    kk: 'Наряды',
    en: 'Outfits'
  },
  icon: {
    type: 'src',
    value: '../constructorFiles/maintenance/menuIcon/calendar_today.svg'
  },
  handler: function(){
    fire({type: 'goto_page', pageCode: 'outfits_page'}, 'root-panel');
  }
});

sidebarParam.menuItems.push({
  visible: false,
  id: 'item_menu_2',
  name: {
    ru: 'ППР',
    kk: 'ППР',
    en: 'ППР'
  },
  icon: {
    type: 'src',
    value: '../constructorFiles/maintenance/menuIcon/event_repeat.svg'
  },
  handler: function(){
    // fire({type: 'goto_page', pageCode: 'outfits_page'}, 'root-panel');
  }
});

sidebarParam.menuItems.push({
  visible: true,
  id: 'item_menu_3',
  name: {
    ru: 'Оборудование',
    kk: 'Жабдық',
    en: 'Equipment'
  },
  icon: {
    type: 'src',
    value: '../constructorFiles/maintenance/menuIcon/settings.svg'
  },
  handler: function(){
    fire({type: 'goto_page', pageCode: 'equipment_page'}, 'root-panel');
  }
});

sidebarParam.menuItems.push({
  visible: true,
  id: 'item_menu_4',
  name: {
    ru: 'Материальные запасы',
    kk: 'Материалдық қорлар',
    en: 'Material reserves'
  },
  icon: {
    type: 'src',
    value: '../constructorFiles/maintenance/menuIcon/settings_suggest.svg'
  },
  handler: function(){
    fire({type: 'goto_page', pageCode: 'material_stocks_page'}, 'root-panel');
  }
});

sidebarParam.menuItems.push({
  visible: true,
  id: 'item_menu_5',
  name: {
    ru: 'Журналы',
    kk: 'Журналдар',
    en: 'Registers'
  },
  icon: {
    type: 'src',
    value: '../constructorFiles/maintenance/menuIcon/access_time.svg'
  },
  children: [
    {
      visible: true,
      id: 'item_menu_5_sub_1',
      name: {
        ru: 'Журнал Наработки - показатели эксплуатации',
        kk: 'Журнал Наработки - показатели эксплуатации',
        en: 'Журнал Наработки - показатели эксплуатации'
      },
      handler: function(){
        fire({type: 'goto_page', pageCode: 'reg_operations_page'}, 'root-panel');
      }
    },
    {
      visible: true,
      id: 'item_menu_5_sub_2',
      name: {
        ru: 'Журнал Осмотры (дефектовка)',
        kk: 'Журнал Осмотры (дефектовка)',
        en: 'Журнал Осмотры (дефектовка)'
      },
      handler: function(){
        fire({type: 'goto_page', pageCode: 'reg_inspection_page'}, 'root-panel');
      }
    },
    {
      visible: true,
      id: 'item_menu_5_sub_3',
      name: {
        ru: 'Журнал Технического обслуживания',
        kk: 'Журнал Технического обслуживания',
        en: 'Журнал Технического обслуживания'
      },
      handler: function(){
        fire({type: 'goto_page', pageCode: 'reg_maintenance_page'}, 'root-panel');
      }
    },
    {
      visible: true,
      id: 'item_menu_5_sub_4',
      name: {
        ru: 'Журнал перемещения оборудования',
        kk: 'Журнал перемещения оборудования',
        en: 'Журнал перемещения оборудования'
      },
      handler: function(){
        fire({type: 'goto_page', pageCode: 'reg_equipment_page'}, 'root-panel');
      }
    },
    {
      visible: true,
      id: 'item_menu_5_sub_5',
      name: {
        ru: 'Журнал Актов выполненных работ',
        kk: 'Журнал Актов выполненных работ',
        en: 'Журнал Актов выполненных работ'
      },
      handler: function(){
        fire({type: 'goto_page', pageCode: 'reg_acts_page'}, 'root-panel');
      }
    }
  ]
});

sidebarParam.menuItems.push({
  visible: false,
  id: 'item_menu_6',
  name: {
    ru: 'Аналитика',
    kk: 'Аналитика',
    en: 'Analytics'
  },
  icon: {
    type: 'src',
    value: '../constructorFiles/maintenance/menuIcon/analytics.svg'
  },
  handler: function(){
    // fire({type: 'goto_page', pageCode: ''}, 'root-panel');
  }
});

sidebarParam.menuItems.push({
  visible: true,
  id: 'item_menu_7',
  name: {
    ru: 'Справочники',
    kk: 'Анықтамалар',
    en: 'Directories'
  },
  icon: {
    type: 'src',
    value: '../constructorFiles/maintenance/menuIcon/access_time.svg'
  },
  children: [
    {
      visible: true,
      id: 'item_menu_7_sub_1',
      name: {
        ru: 'Локация оборудования',
        kk: 'Локация оборудования',
        en: 'Локация оборудования'
      },
      handler: function(){
        fire({type: 'goto_page', pageCode: 'dict_location_page'}, 'root-panel');
      }
    },
    {
      visible: true,
      id: 'item_menu_7_sub_2',
      name: {
        ru: 'Каталог видов оборудования',
        kk: 'Каталог видов оборудования',
        en: 'Каталог видов оборудования'
      },
      handler: function(){
        fire({type: 'goto_page', pageCode: 'dict_category_page'}, 'root-panel');
      }
    },
    {
      visible: true,
      id: 'item_menu_7_sub_3',
      name: {
        ru: 'Реестр исполнителей',
        kk: 'Реестр исполнителей',
        en: 'Реестр исполнителей'
      },
      handler: function(){
        fire({type: 'goto_page', pageCode: 'dict_executors_page'}, 'root-panel');
      }
    },
    {
      visible: true,
      id: 'item_menu_7_sub_4',
      name: {
        ru: 'Виды дефектов оборудования',
        kk: 'Виды дефектов оборудования',
        en: 'Виды дефектов оборудования'
      },
      handler: function(){
        fire({type: 'goto_page', pageCode: 'dict_defect_types_page'}, 'root-panel');
      }
    },
    {
      visible: true,
      id: 'item_menu_7_sub_5',
      name: {
        ru: 'Виды квалификаций исполнителей',
        kk: 'Виды квалификаций исполнителей',
        en: 'Виды квалификаций исполнителей'
      },
      handler: function(){
        fire({type: 'goto_page', pageCode: 'dict_qualifications_page'}, 'root-panel');
      }
    }
  ]
});

sidebarParam.menuItems.push({
  visible: false,
  id: 'item_menu_8',
  name: {
    ru: 'Уведомления',
    kk: 'Хабарландырулар',
    en: 'Notifications'
  },
  icon: {
    type: 'src',
    value: '../constructorFiles/maintenance/menuIcon/notifications_none.svg'
  },
  handler: function(){
    // fire({type: 'goto_page', pageCode: ''}, 'root-panel');
  }
});
