const getUrlParam = (url, key) => {
  url = url.match(new RegExp(key + '=([^&=]+)'));
  return url ? url[1] : null;
}

const getDialog = (title, body) => {
  const dialog = $('<div class="uk-flex-top" uk-modal>');
  const md = $('<div>', {class: "uk-modal-dialog uk-margin-auto-vertical", style: "width: 800px;"});
  const modalBody = $('<div class="uk-modal-body" uk-overflow-auto>');

  modalBody.append(body);
  md.append(`<button class="uk-modal-close-default modal-close-custom" type="button" uk-close></button>`,
  `<div class="uk-modal-header"><h3>${title}</h3></div>`, modalBody);
  dialog.append(md);
  return dialog;
}

//Для открыти документов по клику на ссылку реестра
window.onhashchange = async function(event) {
  const documentID = getUrlParam(event.newURL, 'document_identifier');

  if(documentID) {
    Cons.showLoader();
    try {
      const docInfo = await appAPI.getDocumentInfo(documentID);
      const regInfo = await appAPI.getRegistryInfoByID(docInfo.registryID);

      if(!regInfo || regInfo.rr_read != "Y") throw new Error(i18n.tr('Нет прав на просмотр этого документа'));

      const player = UTILS.getSynergyPlayer(docInfo.asfDataID, false);
      const dialog = getDialog(docInfo.content, player.view.container);

      Cons.hideLoader();

      UIkit.modal(dialog).show();
      dialog.on('hidden', () => {
        window.location.hash = '';
        dialog.remove();
      });

    } catch (err) {
      showMessage(err.message, 'error');
      Cons.hideLoader();
    }
  }
}
