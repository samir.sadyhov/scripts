const updateTranslations = async () => {
  i18n.locale = AS.OPTIONS.locale;

  const systemMessages = await AS.FORMS.ApiUtils.simpleAsyncGet(`rest/system/messages?localeID=${AS.OPTIONS.locale}`);
  i18n.messages = systemMessages;

  translations.forEach(t => i18n.messages[t.ru] = t[AS.OPTIONS.locale]);

  let localeSelect = document.querySelector(".maintenance_locale_selector");
  if(localeSelect) {
    localeSelect.dispatchEvent(new Event("maintenance_change_locale", {bubbles: true}));
  }
}

if($('.maintenance_locale_selector').length) {
  $('.maintenance_locale_selector').off().on('change', e => {
    const locale = $('.maintenance_locale_selector').val();
    AS.OPTIONS.locale = locale;
    localStorage.locale = locale;
    updateTranslations();
  });
}

if(localStorage.locale && localStorage.locale != AS.OPTIONS.locale) {
  let localeSelect = document.querySelector(".maintenance_locale_selector");
  if(localeSelect) {
    localeSelect.value = localStorage.locale;
    localeSelect.dispatchEvent(new Event("change", {bubbles: true}));
  }
}
