this.translations = [];

translations.push({
  ru: 'Maintenance',
  kk: 'Maintenance',
  en: 'Maintenance'
});

translations.push({
  ru: 'введите логин',
  kk: 'логинді енгізіңіз',
  en: 'enter login'
});

translations.push({
  ru: 'введите пароль',
  kk: 'құпия сөзді енгізіңіз',
  en: 'enter password'
});

translations.push({
  ru: 'Не заполнен пароль',
  kk: 'Құпия сөз толтырылмаған',
  en: 'Password not filled in'
});

translations.push({
  ru: 'Не заполнено поле подтверждения пароля',
  kk: 'Құпия сөзді растау өрісі толтырылмаған',
  en: 'The password confirmation field is not filled in'
});

translations.push({
  ru: 'Пароли не совпадают',
  kk: 'Құпия сөздер сәйкес келмейді',
  en: 'The passwords do not match'
});

translations.push({
  ru: 'Ошибка регистрации',
  kk: 'Тіркеу қатесі',
  en: 'Registration error'
});

translations.push({
  ru: 'Регистрация прошла успешно',
  kk: 'Тіркеу сәтті өтті',
  en: 'Registration was successful'
});

translations.push({
  ru: 'Ошибка авторизации. Проверьте правильность введенных данных.',
  kk: 'Авторизация қатесі. Енгізілген деректердің дұрыстығын тексеріңіз.',
  en: 'Authorization error. Check the correctness of the entered data.'
});

translations.push({
  ru: 'Не заполнен логин',
  kk: 'Логин толтырылмаған',
  en: 'Login not filled in'
});

translations.push({
  ru: 'Ошибка авторизации. Выбран неверный ключ.',
  kk: 'Авторизация қатесі. Жарамсыз кілт таңдалды.',
  en: 'Authorization error. Incorrect key selected.'
});

translations.push({
  ru: 'Регистрация успешно завершена! Вы можете войти в систему с помощью ЭЦП или использовать логин и пароль. В качестве логина укажите ваш ИИН, а пароль — тот, который вы задали при регистрации.',
  kk: 'Тіркеу сәтті аяқталды! Жүйеге кіру үшін ЭЦҚ немесе логин мен құпиясөзді пайдалана аласыз. Логин ретінде ЖСН-іңізді енгізіңіз, ал құпиясөз – тіркелу кезінде көрсеткеніңіз.',
  en: 'Registration completed successfully! You can log in using an EDS or your login and password. Your login is your IIN, and your password is the one you\'ve set during registration.'
});

translations.push({
  ru: 'Не заполнен E-mail',
  kk: 'Электрондық пошта толтырылмаған',
  en: 'Email is not filled in'
});

translations.push({
  ru: 'Введен некорректный email адрес',
  kk: 'Қате электрондық пошта мекенжайы енгізілді',
  en: 'Incorrect email address entered'
});
