const {code: appCode} = Cons.getCurrentApp();
const iconName = 'logo.svg';

//Иконка приложения
if(!$('link[rel="icon"]').length) $('head').append(`<link rel="icon" href="../constructorFiles/${appCode}/${iconName}">`);

const getUserInfo = () => {
  const {firstname, lastname, patronymic, mail, userid} = AS.OPTIONS.currentUser;
  const fio = lastname.substr(0, 1).toUpperCase() + lastname.substr(1) + ' ' + firstname.substr(0, 1).toUpperCase() + firstname.substr(1);
  const photoURL = `../Synergy/load?userid=${userid}`;

  return {fio, mail, photoURL};
}

if($('#toggleMenuButton').length) {
  let menu_hide = false;
  if(localStorage.menu_hide && localStorage.menu_hide == 'true') {
    $('.maintenance-content-container').addClass("menu_hide");
    menu_hide = true;
  }

  $('#toggleMenuButton').on('click', e => {
    $('.maintenance-content-container').toggleClass("menu_hide");
    menu_hide = !menu_hide;
    localStorage.menu_hide = menu_hide;
  });
}

//перехватываем событие кнопок браузера назад/вперед
window.onpopstate = function(event) {
  window.history.pushState({page: 1}, "", "");
  return;
}
window.history.pushState({page: 1}, "", "");

window.location.hash = '';
