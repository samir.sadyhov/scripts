const passworRecovery = async body => {
  return new Promise(async resolve => {
    try {
      const url = `../Synergy/rest/api/password/recovery`;
      const response = await fetch(url, {
        method: 'POST',
        headers: {"Content-Type": "application/json; charset=UTF-8"},
        body: JSON.stringify(body)
      });
      resolve(response.json());
    } catch (err) {
      console.log(`ERROR [ passworRecovery ]: ${err.message}`);
      resolve({
        errorCode: 666,
        errorMessage: err.message
      });
    }
  });
}

const passworReset = async body => {
  return new Promise(async resolve => {
    try {
      const url = `../Synergy/rest/api/password/reset`;
      const response = await fetch(url, {
        method: 'POST',
        headers: {"Content-Type": "application/json; charset=UTF-8"},
        body: JSON.stringify(body)
      });
      resolve(response.json());
    } catch (err) {
      console.log(`ERROR [ passworReset ]: ${err.message}`);
      resolve({
        errorCode: 666,
        errorMessage: err.message
      });
    }
  });
}

const initInputs = () => {
  const inputLogin = new CustomInput({
    placeholder: 'введите логин',
    class: 'maintenance_input textInputLogin',
    type: 'text',

    icon: {
      type: "src",
      value: "../constructorFiles/maintenance/account_circle.svg",
      position: 'left'
    }
  });

  const inputCode = new CustomInput({
    placeholder: 'введите код',
    class: 'maintenance_input textInputCode',
    type: 'text',

    icon: {
      type: "src",
      value: "../constructorFiles/maintenance/123.svg",
      position: 'left'
    }
  });

  let typeInputPassword = true;
  const inputPassword = new CustomInput({
    placeholder: 'введите новый пароль',
    class: 'maintenance_input textInputNewPassword',
    type: 'password',

    icon: {
      type: "src",
      value: "../constructorFiles/maintenance/lock.svg",
      position: 'left',
      clickHandler: function(e, input) {
        input.attr('type', typeInputPassword ? 'text' : 'password');
        typeInputPassword = !typeInputPassword;
      }
    }
  });

  let typeInputPasswordConfirm = true;
  const inputPasswordConfirm = new CustomInput({
    placeholder: 'подтвердите новый пароль',
    class: 'maintenance_input textInputConfirmPassword',
    type: 'password',

    icon: {
      type: "src",
      value: "../constructorFiles/maintenance/lock.svg",
      position: 'left',
      clickHandler: function(e, input) {
        input.attr('type', typeInputPasswordConfirm ? 'text' : 'password');
        typeInputPasswordConfirm = !typeInputPasswordConfirm;
      }
    }
  });

  inputLogin.container.css('width', '100%');
  inputCode.container.css('width', '100%');
  inputPassword.container.css('width', '100%');
  inputPasswordConfirm.container.css('width', '100%');

  $('#panelInputs1').append(inputLogin.container);
  $('#panelInputs2').append(inputCode.container, inputPassword.container, inputPasswordConfirm.container);
}

pageHandler('recovery_password_page', () => {
  const timerID = setInterval(() => {
    if(window.hasOwnProperty('CustomInput')) {
      initInputs();
      clearInterval(timerID);
    }
  }, 1);

  if (!Cons.getAppStore().recovery_password_page_listener) {

    addListener('button_click', 'buttonSendLogin', async e => {
      try {
        Cons.showLoader();
        const result = await passworRecovery({
          login: $('.textInputLogin').val(),
          locale: AS.OPTIONS.locale
        });

        if(result.hasOwnProperty('errorCode') && result.errorCode != 0) throw new Error(result.errorMessage);

        Cons.hideLoader();
        showMessage(result.errorMessage, 'success');

        fire({type: 'set_hidden', hidden: true}, 'panelStep1');
        fire({type: 'set_hidden', hidden: false}, 'panelStep2');

      } catch (err) {
        Cons.hideLoader();
        console.log(err);
        UIkit.notification.closeAll();
        showMessage(i18n.tr(err.message), 'error');
      }
    });

    addListener('button_click', 'buttonSendNewPassword', async e => {
      try {
        Cons.showLoader();
        const result = await passworReset({
          code: $('.textInputCode').val(),
          newPassword: $('.textInputNewPassword').val(),
          confirmPassword: $('.textInputConfirmPassword').val(),
          locale: AS.OPTIONS.locale
        });

        if(result.hasOwnProperty('errorCode') && result.errorCode != 0) throw new Error(result.errorMessage);

        Cons.hideLoader();
        showMessage(result.errorMessage, 'success');

        setTimeout(() => {
          fire({type: 'goto_page', pageCode: 'auth_page'}, 'root-panel');
        }, 2000);

      } catch (err) {
        Cons.hideLoader();
        console.log(err);
        UIkit.notification.closeAll();
        showMessage(i18n.tr(err.message), 'error');
      }
    });

    Cons.setAppStore({recovery_password_page_listener: true});
  }
});
