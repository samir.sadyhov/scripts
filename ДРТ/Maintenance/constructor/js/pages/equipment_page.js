const getRegistryInfo = async () => {
  return new Promise(resolve => {
    $('#customRegistryList').trigger({type: 'getRegistryInfo', successHandler: function(registryInfo){
      resolve(registryInfo);
    }});
  });
}

const getRegistrySelectItems = async () => {
  return new Promise(resolve => {
    $('#customRegistryList').trigger({type: 'getSelectedItems', successHandler: function(selectedItems){
      resolve(selectedItems);
    }});
  });
}

const getDocMeaningContent = async documentId => {
  return new Promise(async resolve => {
    try {
      const {login, password} = Cons.creds;
      const auth = "Basic " + btoa(unescape(encodeURIComponent(`${login}:${password}`)));
      const url = `../Synergy/rest/api/formPlayer/getDocMeaningContent?documentId=${documentId}`;
      const response = await fetch(url, {method: 'GET', headers: {"Authorization": auth}});

      if(!response.ok) throw new Error(await response.text());
      resolve(response.text());
    } catch (err) {
      console.log(`ERROR [ getDocMeaningContent ]: ${err.message}`);
      resolve(null);
    }
  });
}

const deleteRows = async () => {
  const selectedItems = await getRegistrySelectItems();

  const confirmMsg = i18n.tr('Вы действительно хотите удалить выбранные записи? Действие безвозвратное!');

  UIkit.modal.confirm(confirmMsg, {labels: {ok: i18n.tr('Да'), cancel: i18n.tr('Отмена')}})
  .then(async () => {
    for(let i = 0; i < selectedItems.length; i++) {
      const docID = selectedItems[i];
      const delResult = await appAPI.deleteDocDocID(docID);
      if(delResult.errorCode == '0') {
        await appAPI.modifyDocDocID(docID);
        $('#customRegistryList').trigger({type: 'removeSelectItem', documentID: docID});
      }
    }
    $('#customRegistryList').trigger({type: 'updateTableBody'});
  }, () => null);
}

const changeOwner = async () => {
  const registryInfo = await appAPI.getRegistryInfo('maintenance_registry_executors');
  if(!registryInfo.hasOwnProperty('registryCustomFilters')) registryInfo.registryCustomFilters = [];

  //(registry, multi, selectedIds, handler)
  AS.SERVICES.showRegistryLinkDialog(registryInfo, false, null, async documentID => {
    try {
      Cons.showLoader();
      const meaning = await getDocMeaningContent(documentID[0]);
      const selectedItems = await getRegistrySelectItems($('#customRegistryList'));

      const promises = selectedItems.map(documentID => appAPI.getAsfDataUUID(documentID));
      const dataUUIDs = await Promise.all(promises);

      const data = [
        {
          id: 'maintenance_form_equipment_owner',
          type: 'reglink',
          key: documentID[0],
          valueID: documentID[0],
          value: meaning
        }
      ];

      const mergePromises = dataUUIDs.map(uuid => appAPI.mergeFormData({uuid, data}));
      const mergeResult = await Promise.all(mergePromises);

      Cons.hideLoader();

      $('#customRegistryList').trigger({type: 'updateTableBody'});
    } catch (err) {
      showMessage(err.message, 'error');
      Cons.hideLoader();
    }
  });
}

const writeOffEquipment = async () => {
  const confirmMsg = i18n.tr('Вы действительно хотите списать выбранное оборудование?');

  UIkit.modal.confirm(confirmMsg, {labels: {ok: i18n.tr('Да'), cancel: i18n.tr('Отмена')}})
  .then(async () => {
    try {
      Cons.showLoader();

      const selectedItems = await getRegistrySelectItems($('#customRegistryList'));
      const promises = selectedItems.map(documentID => appAPI.getAsfDataUUID(documentID));
      const dataUUIDs = await Promise.all(promises);

      const data = [
        {
          id: 'maintenance_form_equipment_status',
          type: 'listbox',
          key: '06',
          value: 'Списан'
        }
      ];

      const mergePromises = dataUUIDs.map(uuid => appAPI.mergeFormData({uuid, data}));
      const mergeResult = await Promise.all(mergePromises);

      Cons.hideLoader();

      $('#customRegistryList').trigger({type: 'updateTableBody'});
    } catch (err) {
      showMessage(err.message, 'error');
      Cons.hideLoader();
    }
  }, () => null);
}

const updateLocation = async () => {
  const registryInfo = await appAPI.getRegistryInfo('maintenance_registry_locations');
  if(!registryInfo.hasOwnProperty('registryCustomFilters')) registryInfo.registryCustomFilters = [];

  //(registry, multi, selectedIds, handler)
  AS.SERVICES.showRegistryLinkDialog(registryInfo, false, null, async documentID => {
    try {
      Cons.showLoader();
      const meaning = await getDocMeaningContent(documentID[0]);
      const selectedItems = await getRegistrySelectItems($('#customRegistryList'));

      const promises = selectedItems.map(documentID => appAPI.getAsfDataUUID(documentID));
      const dataUUIDs = await Promise.all(promises);

      const data = [
        {
          id: 'maintenance_form_equipment_location',
          type: 'reglink',
          key: documentID[0],
          valueID: documentID[0],
          value: meaning
        },
        {
          id: 'maintenance_form_equipment_actions',
          type: 'check',
          keys: ['1'],
          values: ['1']
        }
      ];

      const mergePromises = dataUUIDs.map(uuid => appAPI.mergeFormData({uuid, data}));
      const modifyPromises = dataUUIDs.map(uuid => appAPI.modifyDoc(uuid));

      const mergeResult = await Promise.all(mergePromises);
      const modifyResult = await Promise.all(modifyPromises);

      Cons.hideLoader();

      $('#customRegistryList').trigger({type: 'updateTableBody'});
    } catch (err) {
      showMessage(err.message, 'error');
      Cons.hideLoader();
    }
  });
}

const massActionItems = [
  {
    name: 'Назначить/сменить владельца',
    icon: {
      type: 'uikit',
      value: 'users'
    },
    handler: changeOwner
  },
  {
    name: 'Обновить местоположение',
    icon: {
      type: 'uikit',
      value: 'location'
    },
    handler: updateLocation
  },
  {
    name: 'Списать оборудование',
    icon: {
      type: 'uikit',
      value: 'minus-circle'
    },
    handler: writeOffEquipment
  },
  'divider',
  {
    name: 'Удалить',
    icon: {
      type: 'uikit',
      value: 'trash'
    },
    handler: deleteRows,
    disabled: false
  }
];

pageHandler('equipment_page', () => {
  //рисуем меню
  sidebar($('#leftPanel'));

  //грохаем окно открытого документа, если есть
  $('.maintenance_document_window').remove();

  //массовые действия
  createDropdownMenu($('#panelMassActions'), massActionItems);

  const registryCode = 'maintenance_registry_equipment';
  const formCode = 'maintenance_form_equipment';

  //инициализация компонента записей рееста
  const eventParam = {
    registryCode,
    colorColumn: {
      columnID: 'maintenance_form_equipment_status',
      dict: {
        code: 'maintenance_dict_asset_status',
        columnTitle: 'name',
        columnValue: 'value',
        columnColor: 'color'
      }
    },
    showStatusRow: false
  };

  const storageKey = `registry_columns_${registryCode}_${AS.OPTIONS.currentUser.userid}`;
  let userColumns = localStorage.getItem(storageKey);
  if(userColumns) {
    userColumns = JSON.parse(userColumns);
    eventParam.columns = userColumns;
  }

  //инициализация
  $('#customRegistryList').trigger({type: 'renderNewTable', eventParam});

  //открыть запись реестра
  $('#customRegistryList').on('registry_item_dblclick registry_item_contextmenu_open', row => {
    maintenanceOpenDocument(row.documentID, () => {
      $('#customRegistryList').trigger({type: 'updateTableBody'});
    });
  });

  if (!Cons.getAppStore().equipment_page_listener) {
    //выбор записей реестра
    addListener('registry_select_row', 'customRegistryList', e => {
      const {selectedItems} = e;
      if(selectedItems.length) {
        $('#panelMassActions').removeClass('uk-hidden');
      } else {
        $('#panelMassActions').addClass('uk-hidden');
      }
    });

    //создание записи
    addListener('button_click', 'buttonCreateRow', async e => {
      Cons.showLoader();

      try {
        const registryInfo = await getRegistryInfo();
        if(registryInfo.rr_create != "Y") throw new Error(i18n.tr('У вас нет прав на создание записи'));

        const doc = await appAPI.createDoc(registryInfo.code);
        if(!doc) throw new Error(i18n.tr('Произошла ошибка при создании данного типа документа'));

        $('#customRegistryList').trigger({type: 'updateTableBody'});

        Cons.hideLoader();

        maintenanceOpenDocument(doc.documentID, () => {
          $('#customRegistryList').trigger({type: 'updateTableBody'});
        }, true);

      } catch (err) {
        showMessage(err.message, 'error');
        Cons.hideLoader();
      }
    });

    //выгрузка в excel
    addListener('button_click', 'buttonDownloadXLS', e => {
      $('#customRegistryList').trigger({type: 'getXLS'});
    });

    //обновить список записей реестра
    addListener('button_click', 'buttonRefresh', e => {
      $('#customRegistryList').trigger({type: 'updateTableBody'});
    });

    //настройка столбцов реестра
    let columnsComponent = null;
    addListener('button_click', 'buttonRegistryColumns', async e => {
      if(!columnsComponent) {
        const registryInfo = await getRegistryInfo();

        columnsComponent = new RegistryColumns(registryInfo, columns => {
          $('#customRegistryList').trigger({type: 'setColumns', columns});
        });
      }

      columnsComponent.open();
    });

    Cons.setAppStore({equipment_page_listener: true});
  }

  //инициация компонента фильтрации записей реестра
  $('#registryFilterComponent').trigger({
    type: 'init_filters',
    eventParam: {
      formCode,
      filterButtonSelector: '#buttonRegistryFilter',
      registryComponent: 'customRegistryList'
    }
  });

  Cons.setAppStore({previousPageCode: 'equipment_page'});
  localStorage.setItem('previousPageCode', 'equipment_page');
});
