const addLog = async data => {
  return new Promise(async resolve => {
    AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/logger/log", resolve, 'json', JSON.stringify(data), "application/json; charset=UTF-8", resolve);
  });
}

const checkInputs = (inputLogin, inputPassword) => {
  const result = {errorCode: 0, errorMessage: ''};

  if(inputLogin.value == '') {
    result.errorCode = 666;
    result.errorMessage = i18n.tr('Не заполнен логин');
    inputLogin.input.addClass('highlight');
  } else {
    inputLogin.input.removeClass('highlight');
  }

  if(inputPassword.value == '') {
    result.errorCode = 666;
    if(result.errorMessage != '') result.errorMessage += '<br>';
    result.errorMessage += i18n.tr('Не заполнен пароль');
    inputPassword.input.addClass('highlight');
  } else {
    inputPassword.input.removeClass('highlight');
  }

  return result;
}

const autorizationCreds = (inputLogin, inputPassword) => {
  try {
    const checkResult = checkInputs(inputLogin, inputPassword);
    if(checkResult.errorCode != 0) throw new Error(checkResult.errorMessage);

    Cons.login({login: inputLogin.value, password: inputPassword.value});
  } catch (err) {
    console.log(err);
    UIkit.notification.closeAll();
    showMessage(err.message, 'error');
  }
}

const addListeners = (inputLogin, inputPassword) => {

  inputLogin.input.on('keyup', e => {
    if (e.keyCode === 13) {
      e.preventDefault();
      inputPassword.input.focus();
    }
  });

  inputPassword.input.on('keyup', e => {
    if (e.keyCode === 13) {
      e.preventDefault();
      autorizationCreds(inputLogin, inputPassword);
    }
  });

  inputLogin.input.on('change', e => {
    inputLogin.input.removeClass('highlight');
  });

  inputPassword.input.on('change', e => {
    inputPassword.input.removeClass('highlight');
  });

  $('#buttonLogin').off().on('click', e => {
    e.preventDefault();
    autorizationCreds(inputLogin, inputPassword);
  });
}

const initInputs = () => {
  const inputLogin = new CustomInput({
    placeholder: 'введите логин',
    class: 'maintenance_input textInputLogin',
    type: 'text',

    icon: {
      type: "src",
      value: "../constructorFiles/maintenance/account_circle.svg",
      position: 'left'
    }
  });

  let typeInputPassword = true;
  const inputPassword = new CustomInput({
    placeholder: 'введите пароль',
    class: 'maintenance_input textInputPassword',
    type: 'password',

    icon: {
      type: "src",
      value: "../constructorFiles/maintenance/lock.svg",
      position: 'left',
      clickHandler: function(e, input) {
        input.attr('type', typeInputPassword ? 'text' : 'password');
        typeInputPassword = !typeInputPassword;
      }
    }
  });

  inputLogin.container.css('width', '100%');
  inputPassword.container.css('width', '100%');

  $('#panelInputs').append(inputLogin.container, inputPassword.container);

  addListeners(inputLogin, inputPassword);
}

pageHandler('auth_page', () => {
  const timerID = setInterval(() => {
    if(window.hasOwnProperty('CustomInput')) {
      initInputs();
      clearInterval(timerID);
    }
  }, 1);

  if (!Cons.getAppStore().auth_page_listener) {

    addListener('auth_success', 'root-panel', async authed => {
      const {login, password} = authed.creds;

      Cons.creds.login = login;
      Cons.creds.password = password;
      AS.apiAuth.setCredentials(login, password);
      AS.OPTIONS.login = login;
      AS.OPTIONS.password = password;
      AS.OPTIONS.currentUser = authed.data.person;

      await addLog({
        "providerId": "AI_SEC",
        "eventId": "2005",
        "userId": AS.OPTIONS.currentUser.userid,
        "objects": [AS.OPTIONS.login, "Maintenance"]
      });

      const {previousPageCode = 'outfits_page'} = localStorage;
      fire({type: 'goto_page', pageCode: previousPageCode}, 'root-panel');
    });

    addListener('auth_failure', 'root-panel', e => {
      UIkit.notification.closeAll();
      showMessage(i18n.tr('Ошибка авторизации. Проверьте правильность введенных данных.'), 'error');
    });

    addListener('button_click', 'buttonSelectEDS', e => {
      NCALayer.authEDS(result => {
        try {
          if(result.hasOwnProperty('errorCode') && result.errorCode != 0) throw new Error(result.errorMessage);
          Cons.login({login: '$token', password: result.token});
        } catch (err) {
          console.log(err);
          UIkit.notification.closeAll();
          showMessage(i18n.tr(err.message), 'error');
        }
      });
    });

    Cons.setAppStore({auth_page_listener: true});
  }
});
