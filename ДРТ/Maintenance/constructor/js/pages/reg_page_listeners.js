const pages = [
  {
    pageCode: 'reg_operations_page',
    registryCode: 'maintenance_registry_operating_indicators',
    formCode: 'maintenance_form_operating_indicators'
  },
  {
    pageCode: 'reg_inspection_page',
    registryCode: 'maintenance_registry_defects',
    formCode: 'maintenance_form_defects'
  },
  {
    pageCode: 'reg_maintenance_page',
    registryCode: 'maintenance_registry_technical_history',
    formCode: 'maintenance_form_technical_history'
  },
  {
    pageCode: 'reg_equipment_page',
    registryCode: 'maintenance_registry_location_history',
    formCode: 'maintenance_form_location_history'
  },
  {
    pageCode: 'reg_acts_page',
    registryCode: 'maintenance_registry_completion_work',
    formCode: 'maintenance_form_completion_work'
  }
];

const getRegistryInfo = async () => {
  return new Promise(resolve => {
    $('#customRegistryList').trigger({type: 'getRegistryInfo', successHandler: function(registryInfo){
      resolve(registryInfo);
    }});
  });
}

const getRegistrySelectItems = async () => {
  return new Promise(resolve => {
    $('#customRegistryList').trigger({type: 'getSelectedItems', successHandler: function(selectedItems){
      resolve(selectedItems);
    }});
  });
}

const deleteRows = async () => {
  let selectedItems = await getRegistrySelectItems();

  const confirmMsg = i18n.tr('Вы действительно хотите удалить выбранные записи? Действие безвозвратное!');

  UIkit.modal.confirm(confirmMsg, {labels: {ok: i18n.tr('Да'), cancel: i18n.tr('Отмена')}})
  .then(async () => {
    for(let i = 0; i < selectedItems.length; i++) {
      const docID = selectedItems[i];
      const delResult = await appAPI.deleteDocDocID(docID);
      if(delResult.errorCode == '0') {
        await appAPI.modifyDocDocID(docID);
        $('#customRegistryList').trigger({type: 'removeSelectItem', documentID: docID});
      }
    }
    $('#customRegistryList').trigger({type: 'updateTableBody'});
  }, () => null);
}

const massActionItems = [
  {
    name: 'Удалить',
    icon: {
      type: 'uikit',
      value: 'trash'
    },
    handler: deleteRows
  }
];

pages.forEach(param => {
  const {pageCode, registryCode, formCode} = param;

  pageHandler(pageCode, () => {
    //рисуем меню
    sidebar($('#leftPanel'));

    //грохаем окно открытого документа, если есть
    $('.maintenance_document_window').remove();

    //массовые действия
    createDropdownMenu($('#panelMassActions'), massActionItems);

    //инициализация компонента записей рееста
    const eventParam = {
      registryCode,
      showStatusRow: false
    };

    const storageKey = `registry_columns_${registryCode}_${AS.OPTIONS.currentUser.userid}`;
    let userColumns = localStorage.getItem(storageKey);
    if(userColumns) {
      userColumns = JSON.parse(userColumns);
      eventParam.columns = userColumns;
    }

    //инициализация
    $('#customRegistryList').trigger({type: 'renderNewTable', eventParam});

    //открыть запись реестра
    $('#customRegistryList').on('registry_item_dblclick registry_item_contextmenu_open', row => {
      maintenanceOpenDocument(row.documentID, () => {
        $('#customRegistryList').trigger({type: 'updateTableBody'});
      });
    });

    if (!Cons.getAppStore()[`${pageCode}_listener`]) {
      //выбор записей реестра
      addListener('registry_select_row', 'customRegistryList', e => {
        const {selectedItems} = e;
        if(selectedItems.length) {
          $('#panelMassActions').removeClass('uk-hidden');
        } else {
          $('#panelMassActions').addClass('uk-hidden');
        }
      });

      //создание записи
      addListener('button_click', 'buttonCreateRow', async e => {
        Cons.showLoader();

        try {
          const registryInfo = await getRegistryInfo();
          if(registryInfo.rr_create != "Y") throw new Error(i18n.tr('У вас нет прав на создание записи'));

          const doc = await appAPI.createDoc(registryInfo.code);
          if(!doc) throw new Error(i18n.tr('Произошла ошибка при создании данного типа документа'));

          $('#customRegistryList').trigger({type: 'updateTableBody'});

          Cons.hideLoader();

          maintenanceOpenDocument(doc.documentID, () => {
            $('#customRegistryList').trigger({type: 'updateTableBody'});
          }, true);

        } catch (err) {
          showMessage(err.message, 'error');
          Cons.hideLoader();
        }
      });

      //выгрузка в excel
      addListener('button_click', 'buttonDownloadXLS', e => {
        $('#customRegistryList').trigger({type: 'getXLS'});
      });

      //обновить список записей реестра
      addListener('button_click', 'buttonRefresh', e => {
        $('#customRegistryList').trigger({type: 'updateTableBody'});
      });

      //настройка столбцов реестра
      let columnsComponent = null;
      addListener('button_click', 'buttonRegistryColumns', async e => {
        if(!columnsComponent) {
          const registryInfo = await getRegistryInfo();

          columnsComponent = new RegistryColumns(registryInfo, columns => {
            $('#customRegistryList').trigger({type: 'setColumns', columns});
          });
        }

        columnsComponent.open();
      });

      let s = {};
      s[`${pageCode}_listener`] = true;
      Cons.setAppStore(s);
    }

    //инициация компонента фильтрации записей реестра
    $('#registryFilterComponent').trigger({
      type: 'init_filters',
      eventParam: {
        formCode,
        filterButtonSelector: '#buttonRegistryFilter',
        registryComponent: 'customRegistryList'
      }
    });

    Cons.setAppStore({previousPageCode: pageCode});
    localStorage.setItem('previousPageCode', pageCode);
  });
});
