const cmp = {
  name: 'kw_form_userCard_competenceName',
  status: 'kw_form_userCard_status',
  finishDate: 'kw_form_userCard_finishDate',
  realFinishDate: 'kw_form_userCard_real_finishDate',
  aviableAttempts: 'kw_form_userCard_aviableAttempts',
  table: 'kw_form_userCard_competenceTable',
  tableOther: 'kw_form_userCard_competenceTable_other'
}

const createField = fieldData => {
  let field = {};
  for (let key in fieldData) field[key] = fieldData[key];
  return field;
}

const getValue = (data, cmp) => {
  data = data.data ? data.data : data;
  return data.find(x => x.id === cmp) || null;
}

const setValue = (asfData, cmpID, data) => {
  let field = getValue(asfData, cmpID);
  if(field) {
    for (let key in data) {
      if(key === 'id' || key === 'type') continue;
      field[key] = data[key];
    }
    return field;
  } else {
    asfData = asfData.data ? asfData.data : asfData;
    field = createField(data);
    field.id = cmpID;
    asfData.push(field);
    return field;
  }
}

const getTableBlockIndex = (data, cmp) => {
  let res = 0;
  data = data.data ? data.data : data;
  data.forEach(item => {
    if (item.id.slice(0, item.id.indexOf('-b')) === cmp) res++;
  });
  return res === 0 ? 1 : ++res;
}

const parseCompetenceTable = (table, other) => {
  let result = [];
  if(table.hasOwnProperty('data')){
    let tableLength = getTableBlockIndex(table, cmp.name + (other ? '_other' : ''));
    for(let i = 1; i < tableLength; i++) {
      if(getValue(table, cmp.name + (other ? '_other-b' : '-b') + i))
      result.push({
        name: getValue(table, cmp.name + (other ? '_other-b' : '-b') + i),
        status: getValue(table, cmp.status + (other ? '_other-b' : '-b') + i),
        finishDate: getValue(table, cmp.finishDate + (other ? '_other-b' : '-b') + i),
        realFinishDate: getValue(table, cmp.realFinishDate + (other ? '_other-b' : '-b') + i),
        aviableAttempts: getValue(table, cmp.aviableAttempts + (other ? '_other-b' : '-b') + i)
      });
    }
  }
  return result;
}

const getUserCompetence = () => {
  let skills = parseCompetenceTable(model.playerModel.getModelWithId(cmp.table).getAsfData()[0]);
  let otherSkills = parseCompetenceTable(model.playerModel.getModelWithId(cmp.tableOther).getAsfData()[0], true);
  return skills.concat(otherSkills);
}

const getPositionCompetence = asfData => {
  const result = [];
  const data = getValue(asfData, 'kw_form_position_competence_name');
  if(data && data.hasOwnProperty('key')) {
    const keys = data.key.split(';');
    const values = data.value.split(';');
    keys.forEach((key, i) => result.push({key: key, value: values[i]}));
  }
  return result;
}

const getResultTable = (positionCompetence, userCompetence) => {
  let competenceTable = {id: cmp.table, type: 'appendable_table', data: []};
  let otherCompetenceTable = {id: cmp.tableOther, type: 'appendable_table', data: []};
  let otherCompetence = [];
  let tbi = 1;

  userCompetence.forEach(item => {
    let pos = positionCompetence.filter(x => x.key == item.name.key);
    if(!pos.length) otherCompetence.push(item);
  });

  positionCompetence.forEach(item => {
    let competence = userCompetence.filter(x => x.name.key == item.key);
    if(competence && competence.length) {
      competence = competence[0];
      setValue(competenceTable, cmp.name + '-b' + tbi, competence.name);
      setValue(competenceTable, cmp.status + '-b' + tbi, competence.status);
      setValue(competenceTable, cmp.finishDate + '-b' + tbi, competence.finishDate);
      setValue(competenceTable, cmp.realFinishDate + '-b' + tbi, competence.realFinishDate);
      setValue(competenceTable, cmp.aviableAttempts + '-b' + tbi, competence.aviableAttempts);
    } else {
      setValue(competenceTable, cmp.name + '-b' + tbi, {
        type: 'reglink',
        value: item.value,
        key: item.key,
        valueID: item.key
      });
      setValue(competenceTable, cmp.status + '-b' + tbi, {type: 'listbox', value: "Без статуса", key: "0"});
      setValue(competenceTable, cmp.finishDate + '-b' + tbi, {type: 'date'});
      setValue(competenceTable, cmp.realFinishDate + '-b' + tbi, {type: 'date'});
      setValue(competenceTable, cmp.aviableAttempts + '-b' + tbi, {
        type: 'numericinput',
        value: '0',
        key: '0'
      });
    }
    tbi++;
  });

  tbi = 1;
  otherCompetence.forEach(item => {
    if(item.status.key != '0') {
      setValue(otherCompetenceTable, cmp.name + '_other-b' + tbi, item.name);
      setValue(otherCompetenceTable, cmp.status + '_other-b' + tbi, item.status);
      setValue(otherCompetenceTable, cmp.finishDate + '_other-b' + tbi, item.finishDate);
      setValue(otherCompetenceTable, cmp.realFinishDate + '_other-b' + tbi, item.realFinishDate);
      setValue(otherCompetenceTable, cmp.aviableAttempts + '_other-b' + tbi, item.aviableAttempts);
      tbi++;
    }
  });

  return {competenceTable, otherCompetenceTable};
}

const changeUserCompetence = profileDocID => {
  if(!profileDocID) return;
  try {
    let userCompetence = getUserCompetence();
    let tableBooks;

    AS.FORMS.ApiUtils.getAsfDataUUID(profileDocID)
    .then(AS.FORMS.ApiUtils.loadAsfData)
    .then(positionAsfData => {
      tableBooks = getValue(positionAsfData, 'kw_form_position_books');
      tableBooks.id = 'kw_form_userCard_books';
      return getPositionCompetence(positionAsfData);
    })
    .then(positionCompetence => getResultTable(positionCompetence, userCompetence))
    .then(tables => {
      model.playerModel.getModelWithId(cmp.table).setAsfData(tables.competenceTable);
      model.playerModel.getModelWithId(cmp.tableOther).setAsfData(tables.otherCompetenceTable);
      model.playerModel.getModelWithId('kw_form_userCard_books').setAsfData(tableBooks);
    });

  } catch (e) {
    console.log('ERROR: ' + e.message);
  }
}

if(editable) {
  setTimeout(() => {
    let selectedValue = model.getValue();
    model.on('valueChange', (_1, _2, value) => {
      if(selectedValue != value) {
        selectedValue = value;
        changeUserCompetence(value);
      }
    });
  }, 500);
}
