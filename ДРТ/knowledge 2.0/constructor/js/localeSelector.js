//доп функции для переводов
const localized = () => {
  const store = Cons.getAppStore();

  // этa фигня нужна для списка уроков
  if (store && store.localeClickerLessonList) {
    store.localeClickerLessonList();
  }

  // этa фигня нужна для статусов в моих курсах
  if (store && store.localeClickerMyCourseStatus) {
    store.localeClickerMyCourseStatus();
  }

  // этa фигня нужна для аккордионов
  if (store && store.localeClickerAccordion) {
    store.localeClickerAccordion();
  }
}

const changeLocale = locale => {
  let localeSelect = document.querySelector("#localeSelector");
  localeSelect.value = locale;
  localeSelect.dispatchEvent(new Event("change", {bubbles: true}));
}

const initCustomLocaleSwitch = () => {
  let buttonRU = $('#button_ru');
  let buttonKZ = $('#button_kz');
  if(!buttonRU.length || !buttonKZ.length) return;

  if(AS.OPTIONS.locale == 'ru') {
    buttonRU.addClass('select');
    buttonKZ.removeClass('select');
  } else {
    buttonKZ.addClass('select');
    buttonRU.removeClass('select');
  }

  changeLocale(AS.OPTIONS.locale);

  buttonRU.off().on('click', e => {
    e.preventDefault();
    e.target.blur();
    buttonRU.addClass('select');
    buttonKZ.removeClass('select');
    changeLocale("ru");
  });

  buttonKZ.off().on('click', e => {
    e.preventDefault();
    e.target.blur();
    buttonKZ.addClass('select');
    buttonRU.removeClass('select');
    changeLocale("kk");
  });
}

if($("#localeSelector").length) {
  if(localStorage.locale) {
    AS.OPTIONS.locale = localStorage.locale;
  } else {
    AS.OPTIONS.locale = $('#localeSelector').val();
  }

  initCustomLocaleSwitch();

  $("#localeSelector").off().on('change', function() {
    AS.OPTIONS.locale = this.value;
    localStorage.locale = this.value;
    localized();
  });
}
