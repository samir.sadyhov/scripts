const switchCoursePanel = tabId => {
  $('.kw-tabs-item').removeClass('_active');
  $(`#${tabId}`).addClass('_active');

  const _active = $('.course_panel._active');
  _active.removeClass('_active');
  setTimeout(() => {
    _active.css({'display': 'none'});
  }, 0);

  switch (tabId) {
    case 'switchCoursePage':
      $('.course_panel._info').css({'display': 'flex'});
      setTimeout(() => {
        $('.course_panel._info').addClass('_active');
      }, 0);
      break;
    case 'switchEducation':
      $('.course_panel._education').css({'display': 'flex'});
      setTimeout(() => {
        $('.course_panel._education').addClass('_active');
      }, 0);
      break;
    case 'switchTesting':
      $('.course_panel._testing').css({'display': 'flex'});
      setTimeout(() => {
        $('.course_panel._testing').addClass('_active');
      }, 0);
      break;
  }
}

const getCourseParam = () => {
  const {appInfo} = Cons.getAppStore();
  const {allCourses} = appInfo;
  const {additionalSkillsTable, requiredSkillsTable, requiredSkillsAsfTableData, additionalSkillsAsfTableData} = appInfo.profile;
  const myCourses = [...additionalSkillsTable, ...requiredSkillsTable];
  const courseDataUUID = $KW.getPageParamValue("dataUUID");
  const courseInfo = allCourses.find(x => x.dataUUID == courseDataUUID);
  courseInfo.myCourse = myCourses.find(x => x.dataUUID == courseInfo.dataUUID);

  return {courseDataUUID, myCourses, courseInfo, requiredSkillsAsfTableData, additionalSkillsAsfTableData};
}

const courseInfoCmp = ['kw_form_course_skill', 'kw_form_course_way', 'kw_form_course_note', 'kw_form_course_attribute'];

const parseCourseAsfData = (courseAsfData, courseInfo) => {
  courseInfoCmp.forEach(cmp => {
    courseInfo[cmp] = {ru: $KW.getValue(courseAsfData, cmp).value || '', kk: $KW.getValue(courseAsfData, `${cmp}_kz`).value || ''}
  });

  const {key, value} = $KW.getValue(courseAsfData, 'kw_form_course_author');
  courseInfo.author = value || '';
  courseInfo.authorID = key || '';
}

const parseCourseTasks = tasksAsfData => {
  return tasksAsfData.map(asfData => {
    const task = {};
    task.dataUUID = asfData.uuid;
    task.name = {
      ru: $KW.getValue(asfData, "kw_form_task_name").value || '',
      kk: $KW.getValue(asfData, "kw_form_task_name_kz").value || ''
    }
    task.description = {
      ru: $KW.getValue(asfData, "kw_form_task_main").value || '',
      kk: $KW.getValue(asfData, "kw_form_task_main_kz").value || ''
    }
    task.video_choice = {
      ru: $KW.getValue(asfData, "kw_form_task_video_choice").key || '',
      kk: $KW.getValue(asfData, "kw_form_task_video_choice_kz").key || ''
    }
    task.videolink = {
      ru: $KW.getValue(asfData, "kw_form_task_videolink").value || '',
      kk: $KW.getValue(asfData, "kw_form_task_videolink_kz").value || ''
    }
    task.videofile = {
      ru: $KW.getValue(asfData, "kw_form_task_videofile").key || '',
      kk: $KW.getValue(asfData, "kw_form_task_videofile_kz").key || ''
    }

    task.educational_material = $KW.parseAsfTable($KW.getValue(asfData, 'kw_form_task_educational_material_table'));
    task.additional_material = $KW.parseAsfTable($KW.getValue(asfData, 'kw_form_task_additional_material_table'));

    return task;
  });
}

const getCourseTasks = courseAsfData => {
  const tableCourseContent = $KW.getValue(courseAsfData, 'kw_form_course_tableContent');
  const docIds = tableCourseContent.data.filter(x => x.type == "reglink").map(x => x.key).filter(x => x);

  return new Promise(resolve => {
    Promise.all(docIds.map(x => $KW.getAsfDataUUID(x))).then(uuids => {
      return $KW.getAllAsfData(uuids);
    }).then(tasksAsfData => {
      tasksAsfData.forEach(taskAsfData => taskAsfData.data = taskAsfData.data.filter(cmp => cmp.type != 'label'));
      resolve(parseCourseTasks(tasksAsfData));
    });
  });
}

const getTableOtherAsfData = courseInfo => {
  const data = [];
  data.push({
    id: 'kw_form_userCard_competenceName_other',
    type: 'reglink',
    value: courseInfo.name,
    key: courseInfo.documentID,
    valueID: courseInfo.documentID
  });
  data.push({
    id: 'kw_form_userCard_status_other',
    type: 'listbox',
    key: '0',
    value: 'Без статуса'
  });
  data.push({
    id: 'kw_form_userCard_aviableAttempts_other',
    type: 'numericinput',
    key: '0',
    value: '0'
  });
  data.push({id: 'kw_form_userCard_finishDate_other', type: "date"});
  data.push({id: 'kw_form_userCard_real_finishDate_other', type: "date"});
  return data;
}

const addCourse = courseInfo => {
  const {userCardUUID} = Cons.getAppStore();
  const userCardTableOther = getTableOtherAsfData(courseInfo);

  Cons.showLoader();
  $KW.appendTable(userCardUUID, 'kw_form_userCard_competenceTable_other', userCardTableOther).then(res => {
    if(res) {
      $KW.getAppInfo().then(appInfo => {
        Cons.hideLoader();
        showMessage(localizedText('Курс добавлен', 'Курс добавлен', 'курс қосылды'), 'success');
        setProfileData();
        fire({type: 'set_hidden', hidden: true}, 'addButton');
        fire({type: 'set_hidden', hidden: false}, 'startButton');
      });
    } else {
      Cons.hideLoader();
      showMessage(localizedText('Ошибка добавления курса', 'Ошибка добавления курса', 'Курс қате қосылды'), 'error');
    }
  });
}

const startCourse = () => {
  const {userCardUUID} = Cons.getAppStore();
  const {courseDataUUID, myCourses, requiredSkillsAsfTableData, additionalSkillsAsfTableData} = getCourseParam();
  const courseInfo = myCourses.find(x => x.dataUUID == courseDataUUID);
  const tableAsfData = {requiredSkillsAsfTableData, additionalSkillsAsfTableData}[`${courseInfo.courseType}SkillsAsfTableData`];
  tableAsfData.data = tableAsfData.data.filter(x => x.type != "label");

  const inputDate = $('<input>', {class: 'uk-input', type: 'date', style: 'max-width: 200px;'});

  inputDate.on('change', () => {
    inputDate.removeClass('uk-form-danger');
  });

  const dialogText = {
    title: {
      ru: 'Дата завершения обучения',
      kk: 'Курсты аяқтау күні'
    },
    button: {
      ru: 'Сохранить',
      kk: 'Сақтау'
    }
  }

  const dialog = $KW.getModalDialog(dialogText.title[AS.OPTIONS.locale], inputDate, dialogText.button[AS.OPTIONS.locale], () => {
    if(inputDate.val() == '') {
      inputDate.addClass('uk-form-danger');
      showMessage(localizedText('Выберите дату', 'Выберите дату', 'Күнді таңдаңыз'), 'error');
    } else if (new Date() > new Date(inputDate.val())) {
      inputDate.addClass('uk-form-danger');
      showMessage(localizedText('Дата завершения не может быть меньше текущей даты', 'Дата завершения не может быть меньше текущей даты', 'Аяқталу күні ағымдағы күннен кем болмауы керек'), 'error');
    } else {
      const {documentID} = courseInfo;
      let tbi = tableAsfData.data.find(x => x.key == documentID);
      tbi = tbi.id.slice(tbi.id.indexOf('-b'));

      let finishDateField = tableAsfData.data.find(x => x.id == `kw_form_userCard_finishDate${courseInfo.courseType == 'additional' ? '_other' : ''}${tbi}`);
      let statusField = tableAsfData.data.find(x => x.id == `kw_form_userCard_status${courseInfo.courseType == 'additional' ? '_other' : ''}${tbi}`);
      let newFinishDate = `${inputDate.val()} 23:59:59`;

      finishDateField.key = newFinishDate;
      finishDateField.value = $KW.customFormatDate(newFinishDate);
      statusField.key = '1';
      statusField.value = 'Обучение';

      Cons.showLoader();
      $KW.mergeFormData(userCardUUID, [tableAsfData]).then(res => {
        if(res) {
          $KW.getAppInfo().then(appInfo => {
            Cons.hideLoader();
            setProfileData();
            UIkit.modal(dialog).hide();
            fire({type: 'set_hidden', hidden: true}, 'startButton');
            fire({type: 'set_hidden', hidden: false}, 'switchEducation');
            fire({type: 'set_hidden', hidden: false}, 'switchTesting');
          });
        } else {
          Cons.hideLoader();
          showMessage(localizedText('Ошибка начала курса', 'Ошибка начала курса', 'Курс қате басталды'), 'error');
        }
      });

    }
  });

  UIkit.modal(dialog).show();
  dialog.on('hidden', () => {
    dialog.remove();
  });
}

const initCoursePageListeners = courseInfo => {
  //Вешаем событие на кнопку добавления курса
  $('#addButton').off().on('click', e => {
    e.preventDefault();
    e.target.blur();
    addCourse(courseInfo);
  });

  //Вешаем событие на кнопку начать курс
  $('#startButton').off().on('click', e => {
    e.preventDefault();
    e.target.blur();
    startCourse();
  });

  //переключение табов
  $('.kw-tabs-item').off().on('click', e => {
    e.preventDefault();
    e.target.blur();
    if($(e.target).hasClass('_active')) return;
    switchCoursePanel(e.target.id)
  });

  //кнопка назад
  $('#goBack').off().on('click', e => {
    e.preventDefault();
    e.target.blur();
    const {backPageEvent} = Cons.getAppStore();
    if(backPageEvent) fire(backPageEvent, 'root-panel');
  });
}

pageHandler('course_page', () => {
  const {courseDataUUID, courseInfo} = getCourseParam();

  fire({type: 'image_change_store_id', identifier: courseInfo.img}, 'courseBackground');
  $KW.changeLabel('courseName', courseInfo.fullName, courseInfo.fullName);

  $KW.loadAsfData(courseDataUUID).then(courseAsfData => {
    Cons.setAppStore({courseAsfData: courseAsfData});
    parseCourseAsfData(courseAsfData, courseInfo);

    //заполняем поля с информацией о курсе
    courseInfoCmp.forEach(key => {
      const value = courseInfo[key];
      $KW.changeLabel(key, value.ru, value.kk);
    });
    $KW.changeLabel('courseAuthor', `Автор курса: ${courseInfo.author}`, `Курс авторлары: ${courseInfo.author}`);

    //показать кнопку "Добавить в мои курсы", если выбранный курс не найден в моих курсах
    if(!courseInfo.myCourse) {
      fire({type: 'set_hidden', hidden: false}, 'addButton');
    } else {
      //иначе проверяем, если курс не начат, отображаем кнопку "Начать курс"
      if(courseInfo.myCourse.curseStatusKey == '0') {
        fire({type: 'set_hidden', hidden: false}, 'startButton');
      } else {
        //если курс начат отображаем вкладки обучения и итогового задания
        fire({type: 'set_hidden', hidden: false}, 'switchEducation');
        fire({type: 'set_hidden', hidden: false}, 'switchTesting');
      }
    }

    return getCourseTasks(courseAsfData);
  }).then(tasks => {
    initEducation(tasks);
    initTesting();
  });

  initCoursePageListeners(courseInfo);

  $('.course_panel._active').css({'display': 'flex'});

  if (!Cons.getAppStore().course_page_modalStudyInterview_listener) {
    addListener('loaded_form_data', 'formPlayer-1', asform => {
      const {currentCourse} = Cons.getAppStore();
      const courseModel = asform.model.playerModel.getModelWithId('kw_form_studyInterview_course');
      if(courseModel) courseModel.setValue(currentCourse.documentID);
    });

    Cons.setAppStore({course_page_modalStudyInterview_listener: true});
  }
});
