const closeWindowListiner = e => {
  e.preventDefault();
  e.returnValue = '';
}

const openQuestionsWrongDialog = questionsWrong => {
  let answerIndex = 0;
  const allAnswers = questionsWrong.length;

  const body = $('<div>');
  const wrongAnswersContent = $('<div>');
  const correctAnswerContent = $('<div>');
  const footer = $('<div>');

  correctAnswerContent.css({
    "display": "flex",
    "flex-direction": "column",
    "gap": "10px"
  });

  footer.css({
    "display": "flex",
    "justify-content": "space-between",
    "align-items": "center",
    "margin-top": "10px",
    "padding-top": "5px",
    "border-top": "1px solid var(--red)"
  });

  const questionLabel = $('<span>', {style: "font-weight: bold;"});
  const wrongAnswersList = $('<div>');

  wrongAnswersList.css({
    "border": "1px solid var(--border)",
    "border-radius": "10px",
    "padding": "10px",
    "margin": "10px 0",
    "background": "rgba(0,0,0,0.02)"
  });

  wrongAnswersContent.append(questionLabel, wrongAnswersList);

  const correctAnswer = $('<span>');
  correctAnswerContent.append('<span style="font-weight: bold;">Правильный ответ</span>', correctAnswer);

  const answerCountLabel = $('<span>0 / 0</span>');
  const buttonContainer = $('<div>');
  const answerButtonNext = $('<button>', {class: 'uk-button margined uk-button-default kw-testing-button button__green'});
  const answerButtonBack = $('<button>', {class: 'uk-button margined uk-button-default kw-testing-button button__blue', style: 'display: none;'});

  answerButtonNext.text('Далее');
  answerButtonBack.text('Назад');

  buttonContainer.append(answerButtonBack, answerButtonNext);
  footer.append(answerCountLabel, buttonContainer);

  body.append(wrongAnswersContent, correctAnswerContent, footer);

  if((answerIndex + 1) == allAnswers) answerButtonNext.hide();

  const renderAnswer = () => {
    const {answerCorrect, name: question, selectAnswer, tableAnswers} = questionsWrong[answerIndex];
    const ca = tableAnswers.find(x => x.kw_form_testQuestion_number?.value == answerCorrect)
    const {kw_form_testQuestion_answer, kw_form_testQuestion_number} = ca;
    const answer_list = $('<ul>', {class: 'answer_list'});
    const correctAnswerNum = kw_form_testQuestion_number?.value || '';
    const correctAnswerText = kw_form_testQuestion_answer?.value || '';

    correctAnswer.text(correctAnswerNum + ' - ' + correctAnswerText);
    answerCountLabel.text(`${answerIndex + 1} / ${allAnswers}`);
    questionLabel.text(question[AS.OPTIONS.locale]);

    wrongAnswersList.empty().append(answer_list);

    for(let i=0; i < tableAnswers.length; i++) {
      const {kw_form_testQuestion_answer, kw_form_testQuestion_number} = tableAnswers[i];
      if(kw_form_testQuestion_number?.value == answerCorrect) continue;

      const answerText = kw_form_testQuestion_answer?.value || '';
      const answerNum = kw_form_testQuestion_number?.value || '';
      const answer_item = $(`<li>`, {class: 'answer_item no_hover'});
      const answer_itemA = $('<a>');
      if(kw_form_testQuestion_number?.value == selectAnswer) answer_itemA.css({
        'background': 'var(--red)',
        'color': '#fff'
      });
      answer_itemA.append(`<span class="answer_number">${answerNum}</span>`, `<span class="answer_text">${answerText}</span>`);
      answer_item.append(answer_itemA);
      answer_list.append(answer_item);
    }
  }

  renderAnswer();

  answerButtonNext.on('click', ev => {
    ev.preventDefault();
    ev.target.blur();
    if((answerIndex + 1) < allAnswers) {
      answerButtonBack.show();
      answerIndex++;
      renderAnswer();
    }

    if((answerIndex + 1) >= allAnswers) {
      answerButtonNext.hide();
    }
  });

  answerButtonBack.on('click', ev => {
    ev.preventDefault();
    ev.target.blur();
    if(answerIndex > 0) {
      answerIndex--;
      renderAnswer();
      answerButtonNext.show();
      if(answerIndex == 0) answerButtonBack.hide();
    } else {
      answerButtonBack.hide();
    }
  });

  const dialog = $KW.getModalDialog('Неправильные ответы', body);
  dialog.find('.uk-modal-dialog').css({'width': '50%'});
  UIkit.modal(dialog).show();
  dialog.on('hidden', () => dialog.remove());
}

class FinalTesting {
  constructor(testingData, questions) {
    this.data = testingData;
    this.questions = questions;
    this.randomAnswer = testingData['appTesting-randomAnswer'].key;
    this.randomQuesting = testingData['appTesting-randomQuesting'].key;
    this.useAttempts = testingData['appTesting-useAttempts'].key;
    this.aviableAttempts = testingData['appTesting-aviableAttempts'].key;
    this.timerEnable = testingData['appTesting-timerEnable'].key; //РЕЖИМ (0-нет, 1-да, 2-на весь тест)
    this.timerInterval = Number(testingData['appTesting-timerInterval']?.key) || 30; //УКАЗАННОЕ КОЛ-ВО СЕКУНД
    this.courseMinLevel = testingData['kw_form_course_minLevel']?.key || '0';
    this.showAnswers = testingData['appTesting-showAnswers'].key;

    this.buttonStart = $('#testingButtonStart');
    this.buttonBack = $('#testingButtonBack');
    this.buttonAnswer = $('#testingButtonAnswer');
    this.buttonFinish = $('#testingButtonFinish');
    this.repeatTestingButton = $('#repeatTestingButton');
    this.wrongAnswersButton = $('#wrongAnswersButton');

    this.timerSecondLabel = $('#timerSecondLabel span');

    this.currentQuestion = 1;
    this.timerID;
    this.sec = this.timerInterval;

    this.init();
  }

  start() {
    $('#finalTestingPanel').addClass('testing');
    $('.menu__icon').addClass('testing');
    $('.app_left_panel').addClass('testing');
    this.buttonStart.fadeOut();
    this.buttonAnswer.fadeIn();

    window.addEventListener('beforeunload', closeWindowListiner, false);

    if(this.timerEnable == '1') {
      fire({type: 'set_hidden', hidden: false}, 'panelTimer');
      this.divisor_for_minutes = this.sec % (60 * 60);
      this.minutes = Math.floor(this.divisor_for_minutes / 60);
      this.minutes = this.minutes < 10 ? '0' + this.minutes : this.minutes;
      this.divisor_for_seconds = this.divisor_for_minutes % 60;
      this.seconds = Math.ceil(this.divisor_for_seconds);
      this.seconds = this.seconds < 10 ? '0' + this.seconds : this.seconds;
      this.timerSecondLabel.text(this.minutes + ':' + this.seconds);

      this.sec = this.timerInterval;

      this.timerID = setInterval(() => {
        this.sec--;
        if(this.sec == 0) {
          this.currentQuestion++;

          if (this.currentQuestion > this.questions.length) {
            this.currentQuestion--;
            this.buttonAnswer.hide();
            this.buttonFinish.fadeIn();
            clearTimeout(this.timerID);
            this.finishTest();
          } else {
            this.renderQuestion();
            this.sec = this.timerInterval;
            this.divisor_for_minutes = this.sec % (60 * 60);
            this.minutes = Math.floor(this.divisor_for_minutes / 60);
            this.minutes = this.minutes < 10 ? '0' + this.minutes : this.minutes;
            this.divisor_for_seconds = this.divisor_for_minutes % 60;
            this.seconds = Math.ceil(this.divisor_for_seconds);
            this.seconds = this.seconds < 10 ? '0' + this.seconds : this.seconds;
            this.timerSecondLabel.text(this.minutes + ':' + this.seconds);
          }
        } else {
          this.divisor_for_minutes = this.sec % (60 * 60);
          this.minutes = Math.floor(this.divisor_for_minutes / 60);
          this.minutes = this.minutes < 10 ? '0' + this.minutes : this.minutes;
          this.divisor_for_seconds = this.divisor_for_minutes % 60;
          this.seconds = Math.ceil(this.divisor_for_seconds);
          this.seconds = this.seconds < 10 ? '0' + this.seconds : this.seconds;
          this.timerSecondLabel.text(this.minutes + ':' + this.seconds);
        }
      }, 1000);
    } else if (this.timerEnable == '2') {
      fire({type: 'set_hidden', hidden: false}, 'panelTimer');

      this.hours = this.sec <= 3600 ? 0 : ((this.sec - (this.sec % 3600)) / 3600);
      this.hours = this.hours < 10 ? '0' + this.hours : this.hours;
      this.divisor_for_minutes = this.sec % (60 * 60);
      this.minutes = Math.floor(this.divisor_for_minutes / 60);
      this.minutes = this.minutes < 10 ? '0' + this.minutes : this.minutes;
      this.divisor_for_seconds = this.divisor_for_minutes % 60;
      this.seconds = Math.ceil(this.divisor_for_seconds);
      this.seconds = this.seconds < 10 ? '0' + this.seconds : this.seconds;
      this.timerSecondLabel.text(this.hours + ':' + this.minutes + ':' + this.seconds);

      this.sec = this.timerInterval;

      this.timerID = setInterval(() => {
        this.sec--;
        if(this.sec == 0) {
          this.buttonAnswer.hide();
          this.buttonFinish.fadeIn();
          clearTimeout(this.timerID);
          this.finishTest();
        } else {
          this.divisor_for_minutes = this.sec % (60 * 60);
          this.minutes = Math.floor(this.divisor_for_minutes / 60);
          this.minutes = this.minutes < 10 ? '0' + this.minutes : this.minutes;
          this.divisor_for_seconds = this.divisor_for_minutes % 60;
          this.seconds = Math.ceil(this.divisor_for_seconds);
          this.seconds = this.seconds < 10 ? '0' + this.seconds : this.seconds;
          this.timerSecondLabel.text(this.hours + ':' + this.minutes + ':' + this.seconds);
        }
      }, 1000);
    }
  }

  renderAnswers(question) {
    const {tableAnswers, selectAnswer} = question;
    const prefix = AS.OPTIONS.locale == 'ru' ? '' : '_kz';
    const ul = $('<ul>', {class: 'answer_list'});

    if(this.randomAnswer == '1') {
      tableAnswers.shuffle();
    } else {
      tableAnswers.sort((a, b) => a.kw_form_testQuestion_number?.value - b.kw_form_testQuestion_number?.value);
    }

    tableAnswers.forEach(item => {
      const li = $('<li>', {class: 'answer_item'});
      const a = $('<a>');
      const answerNumber = $('<span>', {class: 'answer_number'});
      const answerText = $('<span>', {class: 'answer_text'});
      const numberAnswerValue = item[`kw_form_testQuestion_number`]?.value;
      const file = item[`kw_form_testQuestion_file${prefix}`];

      answerNumber.text(numberAnswerValue);
      answerText.text(item[`kw_form_testQuestion_answer${prefix}`]?.value);

      li.append(a);
      a.append(answerNumber).append(answerText);
      ul.append(li);

      if(file && file.hasOwnProperty('key') && file.key != '') {
        const img = $('<img>', {class: 'answer_img', src: `/constructor/rest/media?identifier=${file.key}`});

        img.off().on('click', e => {
          Cons.showLoader();
          $KW.getFile(file.key).then(blob => {
            Cons.hideLoader();
            open(blob, '_blank');
          }).catch(err => {
            Cons.hideLoader();
            showMessage(err, 'error');
          });
        });

        a.append(img);
      }

      if(selectAnswer && selectAnswer == numberAnswerValue) a.addClass('selected');

      a.on('click', e => {
        e.preventDefault();
        e.target.blur();
        ul.find('a').removeClass('selected');
        a.addClass('selected');
        question.selectAnswer = numberAnswerValue;
      });
    });

    $('#finalTestingAnswersPanel').empty().append(ul);
  }

  renderQuestion() {
    const question = this.questions[this.currentQuestion - 1];

    $KW.changeLabel('finalTestingQuestion', question.name.ru, question.name.kk);

    if(question.file[AS.OPTIONS.locale] != '') {
      fire({type: 'set_hidden', hidden: false}, 'finalTestingQuestionImage');
      fire({type: 'image_change_store_id', identifier: question.file[AS.OPTIONS.locale]}, 'finalTestingQuestionImage');

      $('#finalTestingQuestionImage').off().on('click', e => {
        Cons.showLoader();
        $KW.getFile(question.file[AS.OPTIONS.locale]).then(blob => {
          Cons.hideLoader();
          open(blob, '_blank');
        }).catch(err => {
          Cons.hideLoader();
          showMessage(err, 'error');
        });
      });

    } else {
      $('#finalTestingQuestionImage').off();
      fire({type: 'set_hidden', hidden: true}, 'finalTestingQuestionImage');
    }

    if(this.currentQuestion != 1) {
      if(this.timerEnable == '0') this.buttonBack.fadeIn();
    } else {
      this.buttonBack.hide();
    }

    fire({type: 'change_label', text: localizedText(String(this.currentQuestion))}, 'currentQuestion');

    this.renderAnswers(question);
  }

  isAllAnswers() {
    for(let i = 0; i < this.questions.length; i++) {
      if(!this.questions[i].hasOwnProperty('selectAnswer')) {
        return i;
        break;
      }
    }
    return false;
  }

  saveResult(success = false) {
    const {appInfo, currentCourse, userCardUUID} = Cons.getAppStore();
    const {additionalSkillsAsfTableData, requiredSkillsAsfTableData} = appInfo.profile;
    const additional = additionalSkillsAsfTableData.data.find(x => x.key == currentCourse.documentID);
    const tableAsfData = additional ? additionalSkillsAsfTableData : requiredSkillsAsfTableData;

    let tbi = tableAsfData.data.find(x => x.key == currentCourse.documentID);
    tbi = tbi.id.slice(tbi.id.indexOf('-b'));

    let finishDateField = tableAsfData.data.find(x => x.id == `kw_form_userCard_real_finishDate${additional ? '_other' : ''}${tbi}`);
    let aviableAttempts = tableAsfData.data.find(x => x.id == `kw_form_userCard_aviableAttempts${additional ? '_other' : ''}${tbi}`);

    let newFinishDate = getCurrentDateParse();
    finishDateField.key = newFinishDate;
    finishDateField.value = $KW.customFormatDate(newFinishDate);

    if(aviableAttempts && aviableAttempts.hasOwnProperty('key')) {
      let newValue = Number(aviableAttempts.key);
      newValue++;
      aviableAttempts.key = String(newValue);
      aviableAttempts.value = String(newValue);
    } else {
      aviableAttempts.key = '1';
      aviableAttempts.value = '1';
    }

    if(success) {
      let statusField = tableAsfData.data.find(x => x.id == `kw_form_userCard_status${additional ? '_other' : ''}${tbi}`);
      statusField.key = '2';
      statusField.value = 'Обучение завершено';
    }

    $KW.mergeFormData(userCardUUID, [tableAsfData]).then(res => {
      if(res) $KW.getAppInfo().then(appInfo => setProfileData());
    });
  }

  renderFinishResult() {
    let resultFinishText = '';
    let questionsTotalLabel = this.questions.length;
    let questionsMissedLabel = 0;
    let questionsCorrectLabel = 0;
    let questionsWrongLabel = 0;

    this.questionsWrong = [];

    this.questions.forEach(x => {
      if (!x.hasOwnProperty('selectAnswer')) {
        questionsMissedLabel++;
      } else {
        if (x.answerCorrect == x.selectAnswer) questionsCorrectLabel++;
        if (x.answerCorrect != x.selectAnswer) {
          questionsWrongLabel++;
          this.questionsWrong.push(x);
        }
      }
    });

    if (questionsCorrectLabel >= Number(this.courseMinLevel)) {
      resultFinishText = 'Тестирование пройдено';
      this.saveResult(true);
    } else {
      resultFinishText = 'Тестирование не пройдено';
      this.saveResult();
    }

    fire({type: 'set_hidden', hidden: true}, 'testingHelloLabel');
    fire({type: 'set_hidden', hidden: true}, 'finalTestingPanelTest');
    fire({type: 'set_hidden', hidden: false}, 'finalTestingPanelResult');
    $('#finalTestingPanelButtons').hide();

    if(this.showAnswers == '1') {
      fire({type: 'set_hidden', hidden: false}, 'wrongAnswersButton');
    } else {
      fire({type: 'set_hidden', hidden: true}, 'wrongAnswersButton');
    }

    $KW.changeLabel('finalTestingLabelResult', resultFinishText);
    $KW.changeLabel('questionsTotalLabel', String(questionsTotalLabel));
    $KW.changeLabel('questionsMissedLabel', String(questionsMissedLabel));
    $KW.changeLabel('questionsCorrectLabel', String(questionsCorrectLabel));
    $KW.changeLabel('questionsWrongLabel', String(questionsWrongLabel));
    $KW.changeLabel('thresholdLevelLabel', this.courseMinLevel);
  }

  finishTest() {

    const finish = () => {
      this.buttonBack.hide();
      $('#finalTestingPanel').removeClass('testing');
      $('.menu__icon').removeClass('testing');
      $('.app_left_panel').removeClass('testing');
      this.buttonStart.fadeIn();
      this.buttonFinish.hide();
      this.renderFinishResult();

      Cons.showLoader();
      try {
        createHistoryData({questions: this.questions}).then(res => {
          Cons.hideLoader();
          window.removeEventListener('beforeunload', closeWindowListiner, false);

          if(this.data.kw_form_course_studyInterview_choice?.value == '1') {
            const {currentCourse} = Cons.getAppStore();
            $KW.searchUserQuestionnaire(currentCourse.documentID).then(anketa => {
              if(!anketa) fire({type: 'set_hidden', hidden: false}, 'modalStudyInterview');
            });
          }

        });
      } catch (e) {
        console.log('createHistoryData ERROR', e);
        Cons.hideLoader();
      }

    }

    const noAnswer = this.isAllAnswers();

    if ((this.timerEnable == '0' && noAnswer) || (this.timerEnable == '2' && noAnswer && this.sec != 0)) {
      UIkit.modal.confirm(t('У вас есть пропущенные вопросы, вы действительно хотите завершить тест?')).then(() => {
        finish();
      }, () => {
        this.currentQuestion = noAnswer + 1;
        this.renderQuestion();
      });
    } else {
      finish();
    }
  }

  initListener() {
    $('#finalTestingPanelButtons').css('display', 'inline-flex');

    this.buttonStart.off().on('click', e => {
      e.preventDefault();
      e.target.blur();
      this.start();
    });

    this.buttonAnswer.off().on('click', e => {
      e.preventDefault();
      e.target.blur();

      this.currentQuestion++;

      if (this.currentQuestion > this.questions.length) {
        this.currentQuestion--;
        this.buttonAnswer.hide();
        this.buttonFinish.fadeIn();
      } else {
        this.renderQuestion();
      }

      if (this.timerEnable == '1') {
        this.sec = this.timerInterval;
        this.timerSecondLabel.text(this.sec);
      }
    });

    this.buttonBack.off().on('click', e => {
      e.preventDefault();
      e.target.blur();

      this.currentQuestion--;
      this.renderQuestion();
      if (this.currentQuestion == 1) {
        this.buttonBack.hide();
        this.buttonAnswer.fadeIn();
      }
    });

    this.buttonFinish.off().on('click', e => {
      e.preventDefault();
      e.target.blur();
      if (this.timerEnable == '1') clearTimeout(this.timerID);
      this.finishTest();
    });

    this.repeatTestingButton.off().on('click', e => {
      e.preventDefault();
      e.target.blur();
      fire({type: 'set_hidden', hidden: false}, 'testingHelloLabel');
      fire({type: 'set_hidden', hidden: false}, 'finalTestingPanelTest');
      fire({type: 'set_hidden', hidden: true}, 'finalTestingPanelResult');
      fire({type: 'set_hidden', hidden: true}, 'wrongAnswersButton');
      $('#finalTestingPanelButtons').css('display', 'inline-flex');

      this.questions.forEach(x => delete x.selectAnswer);
      this.currentQuestion = 1;
      this.renderQuestion();
    });

    this.wrongAnswersButton.off().on('click', e => {
      e.preventDefault();
      e.target.blur();
      openQuestionsWrongDialog(this.questionsWrong);
    });

  }

  init() {
    if(this.randomQuesting == '1') this.questions.shuffle();

    $KW.changeLabel('testingName', this.data['appTesting-name'].ru, this.data['appTesting-name'].kk);
    $KW.changeLabel('testingHelloLabel', this.data['appTesting-info'].ru, this.data['appTesting-info'].kk);
    $KW.buttonChangeText('testingButtonStart', this.data['appTesting-submitStart'].ru, this.data['appTesting-submitStart'].kk);
    $KW.buttonChangeText('testingButtonAnswer', this.data['appTesting-submitNext'].ru, this.data['appTesting-submitNext'].kk);
    $KW.buttonChangeText('testingButtonFinish', this.data['appTesting-submitEnd'].ru, this.data['appTesting-submitEnd'].kk);

    fire({type: 'change_label', text: localizedText(String(this.questions.length))}, 'allQuestion');

    this.buttonBack.hide();
    this.buttonAnswer.hide();
    this.buttonFinish.hide();

    this.renderQuestion();
    this.initListener();
  }
}

const testingFields = [
  'kw_form_course_resTest', //Вопросы,
  'kw_form_course_minLevel', //Пороговый уровень:
  'appTesting-randomAnswer', //Рандом ответов:
  'appTesting-randomQuesting', //Рандом вопросов:
  'appTesting-timerEnable', //Использовать таймер:
  'appTesting-timerInterval', //Кол-во секунд у таймера:
  'appTesting-useAttempts', //Ограничить кол-во попыток
  'appTesting-aviableAttempts', //Кол-во попыток:
  'appTesting-showAnswers', // Показывать список правильных ответов
  'appTesting-name',
  'appTesting-submitStart',
  'appTesting-submitNext',
  'appTesting-submitEnd',
  'appTesting-info',
  'kw_form_course_studyInterview_choice' //Заполнять  Анкету эффективности обучения после окончания курса
];

const finalTaskFields = [
  'kw_form_course_resTask', //Итоговое задание на проверку умения
  'kw_form_course_resInstruction', //Инструкция для выполнения (для обучающегося)
  'kw_form_course_resInstruction_choice',
  'kw_form_course_resLink',
  'kw_form_course_resFile',
  'kw_form_course_resAdditional', //Дополнительный материал
  'kw_form_course_studyInterview_choice' //Заполнять  Анкету эффективности обучения после окончания курса
];


const parseAsfData = (asfData, fields) => {
  const result = {};

  fields.forEach(cmp => {
    const field = $KW.getValue(asfData, cmp);
    if(field) {
      if(field.type == 'textbox' || field.type == 'textarea') {
        result[cmp] = {ru: field.value || '', kk: $KW.getValue(asfData, `${cmp}_kz`).value || ''};
      } else if (field.type == 'appendable_table') {
        result[cmp] = $KW.parseAsfTable(field);
      } else {
        const {key = null, value = null} = field;
        const field_kk = $KW.getValue(asfData, `${cmp}_kz`);

        result[cmp] = {key, value};

        if(field_kk) {
          result[field_kk.id] = {
            key: field_kk?.key || null,
            value: field_kk?.value || null
          }
        }

      }
    }
  });

  return result;
}

const parseQuestions = questionsAsfData => {
  return questionsAsfData.map(asfData => {
    const question = {};
    question.dataUUID = asfData.uuid;
    question.documentID = asfData.documentID;
    question.name = {
      ru: $KW.getValue(asfData, "kw_form_testQuestion_name")?.value || '',
      kk: $KW.getValue(asfData, "kw_form_testQuestion_name_kz")?.value || ''
    }
    question.file = {
      ru: $KW.getValue(asfData, "kw_form_testQuestion_questionFile")?.key || '',
      kk: $KW.getValue(asfData, "kw_form_testQuestion_questionFile_kz")?.key || ''
    }
    question.answerCorrect = $KW.getValue(asfData, "kw_form_testQuestion_answerCorrect")?.key || '';
    question.tableAnswers = $KW.parseAsfTable($KW.getValue(asfData, 'kw_form_testQuestion_tableAnswers'));

    return question;
  });
}

const getQuestions = docIds => {
  docIds = docIds.split(';');

  return new Promise(resolve => {
    Promise.all(docIds.map(x => $KW.getAsfDataUUID(x))).then(uuids => {
      return $KW.getAllAsfData(uuids);
    }).then(questionsAsfData => {
      questionsAsfData.forEach((questionsAsfData, i) => {
        questionsAsfData.data = questionsAsfData.data.filter(cmp => cmp.type != 'label');
        questionsAsfData.documentID = docIds[i];
      });
      resolve(parseQuestions(questionsAsfData));
    });
  });
}


const renderFinalTask = courseAsfData => {
  const finalTaskgData = parseAsfData(courseAsfData, finalTaskFields);
  const prefix = AS.OPTIONS.locale == 'ru' ? '' : '_kz';

  $KW.changeLabel('kw_form_course_resTask', finalTaskgData['kw_form_course_resTask']?.ru || '', finalTaskgData['kw_form_course_resTask']?.kk || '');

  if(finalTaskgData['kw_form_course_resInstruction'][AS.OPTIONS.locale] && finalTaskgData['kw_form_course_resInstruction'][AS.OPTIONS.locale] != '') {
    fire({type: 'set_hidden', hidden: true}, 'finalTestingQuestionImage');
    $KW.changeLabel('kw_form_course_resInstruction', finalTaskgData['kw_form_course_resInstruction']?.ru || '', finalTaskgData['kw_form_course_resInstruction']?.kk || '');
  }

  if(finalTaskgData[`kw_form_course_resInstruction_choice${prefix}`].key == '1') {
    if(finalTaskgData[`kw_form_course_resFile${prefix}`].key)
    $('#finalTaskFileLinkPanel').append($KW.getFileBlock(finalTaskgData[`kw_form_course_resFile${prefix}`], 'file'));
  } else {
    if(finalTaskgData[`kw_form_course_resLink${prefix}`].value)
    $('#finalTaskFileLinkPanel').append($KW.getFileBlock(finalTaskgData[`kw_form_course_resLink${prefix}`], 'link'));
  }

  if(finalTaskgData[`kw_form_course_resAdditional${prefix}`].key)
  $('#finalTaskFileLinkPanel').append($KW.getFileBlock(finalTaskgData[`kw_form_course_resAdditional${prefix}`], 'file'));

  const selectFileContainer = $('<div class="uk-margin" uk-margin style="width: 100%">');
  const formCustom = $('<div uk-form-custom="target: true">');
  const inputFile = $('<input type="file">');
  const submitButton = $('<button>', {
    class: "uk-button uk-button-default kw-button kw-button-transparent",
    disabled: true
  }).text(t('Завершить курс'));

  const onOffButton = () => {
    if(inputFile[0].files.length || $('#finalTaskResultLink').val().length) {
      submitButton.removeAttr("disabled");
    } else {
      submitButton.attr("disabled", true);
    }
  }

  inputFile.on('change', e => {
    onOffButton();
  });

  $('#finalTaskResultLink').on('input', e => {
    onOffButton();
  });

  formCustom.append(inputFile)
  .append(`<input class="uk-input uk-form-width-medium" type="text" style="font-size: 14px; color: #333; background: #fff; min-width: 245px;" placeholder="${t('Прикрепить результат')}" disabled>`);

  selectFileContainer.append(formCustom);

  $('#finalTaskButtonPanel').append(selectFileContainer).append(submitButton);

  submitButton.on('click', e => {
    e.preventDefault();
    e.target.blur();

    if(!inputFile[0].files.length && !$('#finalTaskResultLink').val().length) {
      showMessage(localizedText('Необходимо прикрепить файл или ссылку на результат', 'Необходимо прикрепить файл или ссылку на результат', 'Нәтижеге файлды немесе сілтемені тіркеу қажет'), 'error');
      return;
    }

    Cons.showLoader();
    try {
      createHistoryData({inputFile}).then(res => {
        Cons.hideLoader();

        if(finalTaskgData?.kw_form_course_studyInterview_choice?.value == '1') {
          const {currentCourse} = Cons.getAppStore();
          $KW.searchUserQuestionnaire(currentCourse.documentID).then(anketa => {
            if(!anketa) fire({type: 'set_hidden', hidden: false}, 'modalStudyInterview');
          });
        }

        $KW.changeLabel('statusFinalTaskLabel', 'Ваше задание находится на проверке');
        fire({type: 'set_hidden', hidden: false}, 'statusFinalTaskLabel');
        fire({type: 'set_hidden', hidden: true}, 'finalTaskButtonPanel');
        fire({type: 'set_hidden', hidden: true}, 'kw_form_course_resTask');
        fire({type: 'set_hidden', hidden: true}, 'finalTaskFileLinkPanel');

      });
    } catch (e) {
      console.log('createHistoryData ERROR', e);
      Cons.hideLoader();
    }

  });
}

const getCurrenUserFullName = () => {
  const {firstname, lastname, patronymic} = AS.OPTIONS.currentUser;
  const fio = lastname.substr(0, 1).toUpperCase() + lastname.substr(1) + ' ' + firstname.substr(0, 1).toUpperCase() + '.';
  return patronymic ? fio + ' ' + patronymic.substr(0, 1).toUpperCase() + '.' : fio;
}


const getCurrentDateParse = () => {
  const datetime = new Date();
  return datetime.getFullYear() + '-' +
    ('0' + (datetime.getMonth() + 1)).slice(-2) + '-' +
    ('0' + datetime.getDate()).slice(-2) + ' ' +
    ('0' + datetime.getHours()).slice(-2) + ':' +
    ('0' + datetime.getMinutes()).slice(-2) + ':' +
    ('0' + datetime.getSeconds()).slice(-2);
}

const createCourseHistoryTable = questions => {
  const table = {
    id: 'kw_form_course_history_table',
    type: 'appendable_table',
    data: []
  };
  let tableBlockIndex = 1;
  for(let i = 0; i < questions.length; i++) {
    const {documentID, name, answerCorrect, selectAnswer} = questions[i];
    if(answerCorrect == selectAnswer) continue;
    table.data.push({
      id: `kw_form_course_history_table_testQuestion-b${tableBlockIndex}`,
      type: 'reglink',
      key: documentID,
      valueID: documentID,
      value: name.ru
    });
    table.data.push({
      id: `kw_form_course_history_table_goodAnswer-b${tableBlockIndex}`,
      type: 'textbox',
      value: answerCorrect
    });
    table.data.push({
      id: `kw_form_course_history_table_userAnswer-b${tableBlockIndex}`,
      type: 'textbox',
      value: selectAnswer
    });
    tableBlockIndex++;
  }
  return table;
}

const createHistoryData = param => {
  const {appInfo, courseAsfData, currentCourse, userCardUUID} = Cons.getAppStore();
  const {profile} = appInfo;
  const {questions, inputFile} = param;
  const historyAsfData = [];

  return new Promise(resolve => {
    AS.FORMS.ApiUtils.getDocumentIdentifier(userCardUUID).then(userCardDocID => {
      AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/formPlayer/getDocMeaningContent?asfDataUUID=${userCardUUID}`, null, 'text')
      .then(userCardName => {
        let newFinishDate = getCurrentDateParse();

        historyAsfData.push({
          id: 'kw_form_course_history_userCard',
          type: 'reglink',
          value: userCardName,
          key: userCardDocID,
          valueID: userCardDocID
        });

        historyAsfData.push({
          id: 'kw_form_course_history_courseName',
          type: 'reglink',
          value: currentCourse.name,
          key: currentCourse.documentID,
          valueID: currentCourse.documentID
        });

        historyAsfData.push({
          id: 'kw_form_course_history_user',
          type: 'entity',
          value: getCurrenUserFullName(),
          key: AS.OPTIONS.currentUser.userid
        });

        historyAsfData.push({
          id: 'kw_form_course_history_manager',
          type: 'entity',
          value: profile.manager.value,
          key: profile.manager.key
        });

        historyAsfData.push({
          id: 'kw_form_course_history_finishDate',
          type: 'date',
          value: $KW.customFormatDate(newFinishDate),
          key: newFinishDate
        });

        historyAsfData.push({
          id: 'kw_form_course_history_resTask_choice',
          type: 'listbox',
          value: questions ? 'Итоговое тестирование' : 'Практическое задание',
          key: questions ? '1' : '0'
        });

        historyAsfData.push({
          id: 'kw_form_course_history_isapprove',
          type: 'listbox',
          value: questions ? 'подтверждено' : 'на проверке',
          key: questions ? '1' : '0'
        });

        if(questions) {
          let questionsMissedLabel = 0;
          let questionsCorrectLabel = 0;
          let questionsWrongLabel = 0;

          let minLevel = $KW.getValue(courseAsfData, 'kw_form_course_minLevel');
          if(!minLevel) {
            minLevel = {
              id: 'kw_form_course_history_minLevel',
              type: 'numericinput'
            }
          } else {
            minLevel.id = 'kw_form_course_history_minLevel';
          }

          questions.forEach(x => {
            if(!x.hasOwnProperty('selectAnswer')) {
              questionsMissedLabel++;
            } else {
              if(x.answerCorrect == x.selectAnswer) questionsCorrectLabel++;
              if(x.answerCorrect != x.selectAnswer) questionsWrongLabel++;
            }
          });

          historyAsfData.push(minLevel);
          historyAsfData.push({
            id: 'kw_form_course_history_goodAnswer',
            type: 'numericinput',
            value: String(questionsCorrectLabel),
            key: String(questionsCorrectLabel)
          });
          historyAsfData.push({
            id: 'kw_form_course_history_badAnswer',
            type: 'numericinput',
            value: String(questionsWrongLabel),
            key: String(questionsWrongLabel)
          });
          historyAsfData.push({
            id: 'kw_form_course_history_skipAnswer',
            type: 'numericinput',
            value: String(questionsMissedLabel),
            key: String(questionsMissedLabel)
          });

          historyAsfData.push(createCourseHistoryTable(questions));

        } else {
          historyAsfData.push({
            id: 'kw_form_course_history_result_link',
            type: 'link',
            value: $('#finalTaskResultLink').val(),
            key: $('#finalTaskResultLink').val() + '; false'
          });
        }

        $KW.createDocRCC('kw_registry_course_history', historyAsfData).then(res => {
          if(inputFile) {
            if(!inputFile[0].files.length) {
              AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/activate_doc?dataUUID=${res.dataID}`).then(activated => {
                resolve({error: 0, result: activated});
              });
            } else {
              if(inputFile[0].files[0].size > 20971520) {
                showMessage(localizedText('Размер файла не должен превышать 20 МБ', 'Размер файла не должен превышать 20 МБ', 'Файл өлшемі 20 МБ аспауы керек'), 'error');
                resolve({error: 1, result: 'Размер файла не должен превышать 20 МБ'});
              } else {
                $KW.uploadFile(inputFile[0].files[0], res.dataID, res.asfNodeID, 'kw_form_course_history_result_file').then(result => {
                  AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/activate_doc?dataUUID=${res.dataID}`).then(activated => {
                    resolve({error: 0, result: activated});
                  });
                });
              }
            }
          } else {
            AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/activate_doc?dataUUID=${res.dataID}`).then(activated => {
              resolve({error: 0, result: activated});
            });
          }
        }).catch(err => {
          resolve({error: 1, result: err});
        });

      });
    })

  });
}

this.initTesting = () => {
  const {appInfo, courseAsfData} = Cons.getAppStore();
  const {allCourses, profile} = appInfo;
  const currentCourse = allCourses.find(x => x.dataUUID == courseAsfData.uuid);

  Cons.setAppStore({currentCourse: currentCourse});

  $KW.searchCourseHistory(currentCourse.documentID).then(history => {
    Cons.setAppStore({courseHistory: history});

    const resTaskChoice = $KW.getValue(courseAsfData, 'kw_form_course_resTask_choice').key;
    if(resTaskChoice == '1') {
      const testingData = parseAsfData(courseAsfData, testingFields);
      getQuestions(testingData.kw_form_course_resTest.key).then(questions => {
        new FinalTesting(testingData, questions);
      });
      fire({type: 'set_hidden', hidden: true}, 'finalTaskPanel');
    } else {
      fire({type: 'set_hidden', hidden: true}, 'finalTestingPanel');
      if(history) {
        const isapprove = $KW.getValue(history, "kw_form_course_history_isapprove")?.key;

        switch (isapprove) {
          case '0':
            $KW.changeLabel('statusFinalTaskLabel', 'Ваше задание находится на проверке');
            fire({type: 'set_hidden', hidden: false}, 'statusFinalTaskLabel');
            fire({type: 'set_hidden', hidden: true}, 'finalTaskButtonPanel');
            fire({type: 'set_hidden', hidden: true}, 'kw_form_course_resTask');
            fire({type: 'set_hidden', hidden: true}, 'finalTaskFileLinkPanel');
            break;
          case '1':
            $KW.changeLabel('statusFinalTaskLabel', 'Задание успешно пройдено (подтверждено менеджером обучения)');
            fire({type: 'set_hidden', hidden: false}, 'statusFinalTaskLabel');
            fire({type: 'set_hidden', hidden: true}, 'finalTaskButtonPanel');
            fire({type: 'set_hidden', hidden: true}, 'kw_form_course_resTask');
            fire({type: 'set_hidden', hidden: true}, 'finalTaskFileLinkPanel');
            break;
          case '2':
            $KW.changeLabel('statusFinalTaskLabel', 'Задание не подтверждено менеджером обучения. Прикрепите новый результат выполнения и отправьте на проверку повторно');
            fire({type: 'set_hidden', hidden: false}, 'statusFinalTaskLabel');
            fire({type: 'set_hidden', hidden: false}, 'finalTaskButtonPanel');
            renderFinalTask(courseAsfData);
            break;
        }
      } else {
        renderFinalTask(courseAsfData);
      }

    }
  });
}
