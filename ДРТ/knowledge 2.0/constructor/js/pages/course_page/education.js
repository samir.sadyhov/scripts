const setVideo = task => {
  const videoChoice = task.video_choice[AS.OPTIONS.locale];

  switch (videoChoice) {
    case '0':
      const videolink = task.videolink[AS.OPTIONS.locale];
      if(videolink != '') {
        fire({type: 'set_hidden', hidden: false}, 'lessonVideo');
        fire({type: 'video_change_url', url: videolink}, 'lessonVideo');
      } else {
        fire({type: 'set_hidden', hidden: true}, 'lessonVideo');
      }
      break;
    case '1':
      const videofile = task.videofile[AS.OPTIONS.locale];
      if(videofile != '') {
        fire({type: 'set_hidden', hidden: false}, 'lessonVideo');
        fire({type: 'video_change_store_id', identifier: videofile}, 'lessonVideo');
      } else {
        fire({type: 'set_hidden', hidden: true}, 'lessonVideo');
      }
      break;
  }
}

const getAccBody = data => {
  const accBody = $('<div>');
  data.forEach(item => accBody.append($KW.getFileBlock(item, item.type)));
  return accBody;
}

const renderLesson = task => {
  const {educational_material, additional_material} = task;
  const prefix = AS.OPTIONS.locale == 'ru' ? '' : '_kz';
  const accordionData = [];

  const educationData = educational_material.map(item => {
    if(item[`kw_form_task_material_choice${prefix}`].key == '1') {
      const {key, value} = item[`kw_form_task_materialfile${prefix}`];
      return {key, value, type: 'file'};
    } else {
      const {key, value} = item[`kw_form_task_materiallink${prefix}`];
      return {key, value, type: 'link'};
    }
  });

  const additionalData = additional_material.map(item => {
    if(item[`kw_form_task_additional_material_choice${prefix}`].key == '1') {
      const {key, value} = item[`kw_form_task_additional_materialfile${prefix}`];
      return {key, value, type: 'file'};
    } else {
      const {key, value} = item[`kw_form_task_additional_materiallink${prefix}`];
      return {key, value, type: 'link'};
    }
  });

  if(educationData.length) accordionData.push({title: 'Учебный материал', body: getAccBody(educationData), active: true});
  if(additionalData.length) accordionData.push({title: 'Дополнительный материал', body: getAccBody(additionalData), active: true});

  $KW.createAccordion(accordionData, $('#materialDataPanel'));

  $KW.changeLabel('lessonName', task.name.ru, task.name.kk);
  $KW.changeLabel('lessonContent', task.description.ru, task.description.kk);

  setVideo(task);
}


this.initEducation = tasks => {
  const listContainer = $('#lessonsList');
  const select = $('<select>', {class: 'uk-select'});

  listContainer.empty().append(select);
  tasks.forEach(task => select.append(`<option value="${task.dataUUID}">${task.name[AS.OPTIONS.locale] || task.name.ru}</option>`));

  select.on('change', e => renderLesson(tasks.find(x => x.dataUUID == select.val())));

  renderLesson(tasks[0]);

  Cons.setAppStore({localeClickerLessonList: function(){
    if(Cons.getCurrentPage().code != "course_page") return;
    tasks.forEach(task => $(`#lessonsList select [value="${task.dataUUID}"]`).text(task.name[AS.OPTIONS.locale] || task.name.ru));
    setVideo(tasks.find(x => x.dataUUID == select.val()));
  }});
}
