const getAccBody = courses => {
  const accBody = $('<div>');

  courses.forEach(course => {
    const {name, dataUUID} = course;
    const item = $('<div>', {class: "accordion-body-item"}).text(name);

    item.on('click', e => {
      fire({
        type: 'goto_page',
        pageCode: 'course_page',
        pageParams: [
          {
            pageParamName: 'dataUUID',
            value: dataUUID
          }
        ]
      }, 'root-panel');
    });

    accBody.append(item);
  });

  return accBody;
}

pageHandler('courses_list', () => {
  const {appInfo} = Cons.getAppStore();
  const {groupCourses, allCourses} = appInfo;
  const groupDataUUID = $KW.getPageParamValue("dataUUID");
  const group = groupCourses.find(x => x.dataUUID == groupDataUUID);
  const groupCoursData = group.course.map(documentID => allCourses.find(x => x.documentID == documentID));
  const switchButton = $('#switchCoursePanel');
  const panelAccordion = $('#panel-accordion');
  const panelIterators = $('#panel-iterators');
  const accordionData = [{title: group.groupName, body: getAccBody(groupCoursData)}];
  let courses_display_type = localStorage.getItem('courses_display_type');

  $KW.changeLabel('labelGroupName', group.name.ru, group.name.kk);

  fire({type: 'change_repeater_custom_source', customSource: groupCoursData}, 'groupCoursesRepeater');
  $KW.createAccordion(accordionData, panelAccordion);

  panelAccordion.hide();
  panelIterators.hide();
  if(!courses_display_type || courses_display_type == 'tiles') {
    panelIterators.fadeIn();
  } else {
    panelAccordion.fadeIn();
  }

  switchButton.on('click', e => {
    courses_display_type = localStorage.getItem('courses_display_type');
    if(courses_display_type == 'tiles') {
      courses_display_type = 'accordion';
      panelIterators.fadeOut(200, () => panelAccordion.fadeIn(200));
    } else {
      courses_display_type = 'tiles';
      panelAccordion.fadeOut(200, () => panelIterators.fadeIn(200));
    }
  });

  Cons.setAppStore({backPageEvent: {
    type: 'goto_page',
    pageCode: 'courses_list',
    pageParams: [
      {
        pageParamName: 'dataUUID',
        value: groupDataUUID
      }
    ]
  }});

  //кнопка назад
  $('#goBack').off().on('click', e => {
    e.preventDefault();
    e.target.blur();
    fire({type: 'goto_page', pageCode: 'all_courses'}, 'root-panel');
  });
});
