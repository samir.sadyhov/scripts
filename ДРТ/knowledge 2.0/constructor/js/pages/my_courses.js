const getAccBody = skills => {
  const accBody = $('<div>');

  skills.forEach(skill => {
    const {fullName, curseStatus, expired, finishDate, dataUUID} = skill;
    let itemText = fullName;

    if(curseStatus == 'Обучение завершено') {
      itemText += ` (${curseStatus})`;
    } else if(expired == 'Срок обучения истек') {
      itemText += ` (${expired} | ${finishDate})`;
    } else if (curseStatus == 'Без статуса') {
      itemText += ` (${curseStatus})`;
    } else {
      itemText += ` (${curseStatus} | ${expired})`;
    }

    const item = $('<div>', {class: "accordion-body-item"}).text(itemText);

    item.on('click', e => {
      fire({
        type: 'goto_page',
        pageCode: 'course_page',
        pageParams: [
          {
            pageParamName: 'dataUUID',
            value: dataUUID
          }
        ]
      }, 'root-panel');
    });

    accBody.append(item);
  });

  return accBody;
}

pageHandler('my_courses', () => {
  const {appInfo} = Cons.getAppStore();
  const {allCourses} = appInfo;
  const {additionalSkillsTable, requiredSkillsTable} = appInfo.profile;
  const switchButton = $('#switchCoursePanel');
  const panelAccordion = $('#panel-accordion');
  const panelIterators = $('#panel-iterators');
  let courses_display_type = localStorage.getItem('courses_display_type');

  Cons.setAppStore({backPageEvent: {
    type: 'goto_page',
    pageCode: 'my_courses'
  }});

  const accordionData = [
    {
      title: `Обязательные - ${requiredSkillsTable.length}`,
      body: getAccBody(requiredSkillsTable)
    },
    {
      title: `Дополнительные - ${additionalSkillsTable.length}`,
      body: getAccBody(additionalSkillsTable)
    }
  ];

  fire({type: 'change_repeater_custom_source', customSource: requiredSkillsTable}, 'mainCoursesRepeater');
  fire({type: 'change_repeater_custom_source', customSource: additionalSkillsTable}, 'otherCoursesRepeater');
  $KW.createAccordion(accordionData, panelAccordion);

  panelAccordion.hide();
  panelIterators.hide();
  if(!courses_display_type || courses_display_type == 'tiles') {
    panelIterators.fadeIn();
  } else {
    panelAccordion.fadeIn();
  }

  switchButton.on('click', e => {
    courses_display_type = localStorage.getItem('courses_display_type');
    if(courses_display_type == 'tiles') {
      courses_display_type = 'accordion';
      panelIterators.fadeOut(200, () => panelAccordion.fadeIn(200));
    } else {
      courses_display_type = 'tiles';
      panelAccordion.fadeOut(200, () => panelIterators.fadeIn(200));
    }
  });

  Cons.setAppStore({localeClickerMyCourseStatus: function(){
    if(Cons.getCurrentPage().code != "my_courses") return;
    $('.kw-card-status').each((index, item) => {
      const text = t($(item).text());
      $(item).text(text);
    });
  }});
});
