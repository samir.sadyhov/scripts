pageHandler('profile', () => {
  const {appInfo} = Cons.getAppStore();
  const {profile} = appInfo;
  const {lastname, firstname, patronymic} = AS.OPTIONS.currentUser;

  const certificateStatus = {
    current: profile.requiredSkillsTable.filter(x => x.curseStatusKey == '2').length || 0,
    all: profile.requiredSkillsTable.length,
    status: profile.certified?.key || '0'
  }

  const COUNT_RU = `Пройдено курсов: ${certificateStatus.current} из ${certificateStatus.all}`;
  const COUNT_KZ = `Курстар аяқталды: ${certificateStatus.all}-ден ${certificateStatus.current}`;

  const STATUS_RU_SUCCESS = `${COUNT_RU} (пользователь сертифицирован)`;
  const STATUS_RU_FAIL = `${COUNT_RU} (пользователь еще не сертифицирован)`;
  const STATUS_KZ_SUCCESS = `${COUNT_KZ} (пайдаланушы сертификатталған)`;
  const STATUS_KZ_FAIL = `${COUNT_KZ} (пайдаланушы әлі сертификатталмаған)`;

  const STATUS_RU = certificateStatus.status != '0' ? STATUS_RU_SUCCESS : STATUS_RU_FAIL;
  const STATUS_KZ = certificateStatus.status != '0' ? STATUS_KZ_SUCCESS : STATUS_KZ_FAIL;

  const userProfileFields = [];
  userProfileFields.push({field: 'profileLastname', value: lastname});
  userProfileFields.push({field: 'profileFirstname', value: firstname});
  userProfileFields.push({field: 'profilePatronymic', value: patronymic});
  userProfileFields.push({field: 'profileOffice', value: profile.office?.value || ''});
  userProfileFields.push({field: 'profileDepartment', value: profile.department?.value || ''});
  userProfileFields.push({field: 'profileTraining', value: profile.trainingProfile?.value || ''});
  userProfileFields.push({field: 'profileManager', value: profile.manager?.value || ''});

  fire({
    type: 'image_change_url',
    url: `../Synergy/load?userid=${AS.OPTIONS.currentUser.userid}`
  }, 'profile-photo');

  $KW.changeLabel('certificationStatus', STATUS_RU, STATUS_KZ);

  userProfileFields.forEach(x => $KW.changeLabel(x.field, x.value, x.value));
});
