const groupCode = 'student';
const reqRegistry = ["kw_registry_course", "kw_registry_courseGroups", "kw_registry_trainingProfile", "kw_registry_userCards", "kw_registry_idp"];

const fetchUserFullName = () => {
  let {lastname, firstname, patronymic} = AS.OPTIONS.currentUser;
  firstname = ' ' + firstname.charAt(0) + '.';
  patronymic = patronymic != '' ? ' ' + patronymic.charAt(0) + '.' : '';
  return lastname + firstname + patronymic;
}

const searchProfileCard = () => {
  const params = $.param({
    registryCode: 'kw_registry_trainingProfile',
    field: 'kw_form_position_choice',
    condition: 'CONTAINS',
    key: AS.OPTIONS.currentUser.positions[0].positionID,
    loadData: true,
    countInPart: 1,
    fields: 'kw_form_profile_name'
  });
  return new Promise((resolve, reject) => {
    rest.synergyGet(`api/registry/data_ext?${params}`, res => {
      if(res.recordsCount == 0) reject('Не найдено профиля обучения');
      else resolve(res.result[0]);
    }, err => reject('Ошибка поиска профиля обучения'));
  });
}

const getUserCardAsfData = positionCard => {
  let asfData = [];
  let tableBlockIndex = 1;
  let competenceTable = {id: 'kw_form_userCard_competenceTable', type: 'appendable_table', data: []};
  let booksTable = {id: 'kw_form_userCard_books', type: 'appendable_table', data: []};
  let userCardAll = {id: 'kw_form_userCard_all', type: 'numericinput', key: '0', value: '0'};

  asfData.push(competenceTable);
  asfData.push(booksTable);
  asfData.push(userCardAll);
  asfData.push({id: 'kw_form_userCard_current', type: 'numericinput', key: '0', value: '0'});
  asfData.push({id: 'kw_form_userCard_user', type: 'entity', key: AS.OPTIONS.currentUser.userid, value: fetchUserFullName()});
  asfData.push({
    id: 'kw_form_userCard_department',
    type: 'entity',
    key: AS.OPTIONS.currentUser.positions[0].departmentID,
    value: AS.OPTIONS.currentUser.positions[0].departmentName
  });
  asfData.push({
    id: 'kw_form_userCard_reglink',
    type: 'reglink',
    key: positionCard.documentID,
    value: positionCard.fieldValue.kw_form_profile_name || 'Профиль обучения'
  });

  return new Promise(resolve => {
    rest.synergyGet(`api/asforms/data/${positionCard.dataUUID}`, posAsfData => {
      let positionBooks = posAsfData.data.find(x => x.id == 'kw_form_position_books');
      let positionCompetence = posAsfData.data.find(x => x.id == 'kw_form_position_competence_name');

      if(positionCompetence && positionCompetence.hasOwnProperty('key')) {
        let keys = positionCompetence.key.split(';');
        let values = positionCompetence.value.split(';');

        keys.forEach((docID, i) => {
          competenceTable.data.push({id: `kw_form_userCard_competenceName-b${tableBlockIndex}`, type: 'reglink', key: docID, valueID: docID, value: values[i]});
          competenceTable.data.push({id: `kw_form_userCard_status-b${tableBlockIndex}`, type: 'listbox', key: '0', value: 'Без статуса'});
          competenceTable.data.push({id: `kw_form_userCard_finishDate-b${tableBlockIndex}`, type: 'date'});
          competenceTable.data.push({id: `kw_form_userCard_real_finishDate-b${tableBlockIndex}`, type: 'date'});
          competenceTable.data.push({id: `kw_form_userCard_aviableAttempts-b${tableBlockIndex}`, type: 'numericinput', key: '0', value: '0'});
          tableBlockIndex++;
        });

        userCardAll.key = String(tableBlockIndex - 1);
        userCardAll.value = String(tableBlockIndex - 1);
      }

      if(positionBooks && positionBooks.hasOwnProperty('data')) booksTable.data = positionBooks.data;
      resolve(asfData);
    }, err => resolve(asfData));
  });
}

const createUserCard = positionCard => {
  return new Promise((resolve, reject) => {
    getUserCardAsfData(positionCard).then(asfData => {
      AS.FORMS.ApiUtils.simpleAsyncPost('rest/api/registry/create_doc_rcc', res => {
        res.errorCode != 0 ? reject(res.errorMessage) : resolve(res);
      }, null, JSON.stringify({
        registryCode: 'kw_registry_userCards',
        data: asfData,
        sendToActivation: true
      }), "application/json; charset=utf-8", err => {
        console.log('create_doc_rcc error', err.responseJSON.errorMessage);
        reject('Ошибка создания карточки пользователя');
      });
    });
  });
}

const addUserGroup = userID => {
  return new Promise(async resolve => {
    try {
      rest.synergyGet(`api/storage/groups/add_user?groupCode=${groupCode}&userID=${userID}`, res => resolve(res));
    } catch (e) {
      console.log(e);
      resolve(null);
    }
  });
}

const authContinue = userCardUUID => {
  rest.synergyGet('api/registry/list', res => {
    const arr = [];

    reqRegistry.forEach(registry => {
      arr.push(res.find(item => item.regGroupName === 'LMS').consistOf.some(item => item.registryCode === registry));
    });

    if(arr.some(item => !item)) {
      Cons.hideLoader();
      return showMessage(localizedText('В приложении недостаточно данных для запуска академии, обратитесь к менеджеру обучения!'), 'error');
    }

    Cons.setAppStore({userCardUUID: userCardUUID});

    $KW.getAppInfo().then(res => {
      Cons.hideLoader();
      if(!res) {
        showMessage(localizedText('Ошибка авторизации', 'Ошибка авторизации', 'Ошибка авторизации'), 'error');
      } else {
        fire({type: 'goto_page', pageCode: 'my_courses'}, 'button-auth');
      }
    });

  }, err => {
    Cons.hideLoader();
    showMessage(localizedText('Ошибка авторизации', 'Ошибка авторизации', 'Ошибка авторизации'), 'error');
    console.error(err);
  });
}

const authMe = () => {
  if (AS.OPTIONS.currentUser.userid === '1') {
    return showMessage(localizedText('Вход в систему недоступен!', 'Вход в систему недоступен!', 'Жүйеге кіру мүмкін емес!'), 'error');
  }

  // 1 - проверяем наличие карточки пользователя
  const params = $.param({
    loadData: true,
    registryCode: 'kw_registry_userCards',
    field: 'kw_form_userCard_user',
    condition: 'CONTAINS',
    key: AS.OPTIONS.currentUser.userid
  });

  Cons.showLoader();
  rest.synergyGet(`api/registry/data_ext?${params}`, res => {
    if (!res.recordsCount) {
      Cons.hideLoader();
      UIkit.modal.confirm('Отсутствует профиль Академии, создать профиль и продолжить?').then(() => {
        Cons.showLoader();

        //Поиск профиля обучения и создание карточки
        searchProfileCard()
        .then(createUserCard)
        .then(userCard => authContinue(userCard.dataID))
        .catch(err => {
          Cons.hideLoader();
          showMessage(localizedText('Ошибка при создании профиля', 'Ошибка при создании профиля', 'Ошибка при создании профиля'), 'error');
          console.log('%c' + err, 'color: red;');
        });

      }, () => {
        return;
      });
    } else {
      authContinue(res.result[0].dataUUID);
    }
  }, err => {
    Cons.hideLoader();
    showMessage(localizedText('Ошибка авторизации', 'Ошибка авторизации', 'Ошибка авторизации'), 'error');
    console.log('%c' + err, 'color: red;');
  });
}

pageHandler('auth_page', () => {
  if(!Cons.getAppStore().auth_page_listener) {
    addListener('auth_success', 'button-auth', authed => {
      const {login, password} = authed.creds;
      const userGroups = authed.data.groups;

      Cons.creds.login = login;
      Cons.creds.password = password;
      AS.apiAuth.setCredentials(login, password);
      AS.OPTIONS.login = login;
      AS.OPTIONS.password = password;
      AS.OPTIONS.currentUser = authed.data.person;

      Cons.setAppStore({userGroups: userGroups});

      if(userGroups.find(x => x.groupCode == groupCode)) {
        authMe();
      } else {
        addUserGroup(AS.OPTIONS.currentUser.userid).then(res => authMe());
      }

    });

    Cons.setAppStore({auth_page_listener: true});
  }
});
