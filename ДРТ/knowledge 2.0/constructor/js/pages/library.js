const getAccBody = (data, type = 'file') => {
  const accBody = $('<div>');
  data.forEach(item => accBody.append($KW.getFileBlock(item, type)));
  return accBody;
}

pageHandler('library', () => {
  const {appInfo} = Cons.getAppStore();
  const {files, links} = appInfo.profile.library;

  const accordionData = [];
  if(files.length) accordionData.push({title: `Файлы`, body: getAccBody(files, 'file'), active: true});
  if(links.length) accordionData.push({title: `Внешние ресурсы`, body: getAccBody(links, 'link')});

  $KW.createAccordion(accordionData, $('#panel-data'));
});
