const getAccBody = course => {
  const accBody = $('<div>');
  const {allCourses} = Cons.getAppStore().appInfo;

  course.forEach(documentID => {
    const courseInfo = allCourses.find(x => x.documentID == documentID);
    const {dataUUID, fullName} = courseInfo;
    const item = $('<div>', {class: "accordion-body-item"}).text(fullName);

    item.on('click', e => {
      fire({
        type: 'goto_page',
        pageCode: 'course_page',
        pageParams: [
          {
            pageParamName: 'dataUUID',
            value: dataUUID
          }
        ]
      }, 'root-panel');
    });

    accBody.append(item);
  });

  return accBody;
}

pageHandler('all_courses', () => {
  const {appInfo} = Cons.getAppStore();
  const {groupCourses} = appInfo;
  const switchButton = $('#switchCoursePanel');
  const panelAccordion = $('#panel-accordion');
  const panelIterators = $('#panel-iterators');
  const accordionData = [];
  let courses_display_type = localStorage.getItem('courses_display_type');

  groupCourses.forEach(item => {
    item.courseCount = String(item.course.length);
    item.groupName = item.name[AS.OPTIONS.locale];
    item.groupDescription = item.description[AS.OPTIONS.locale];

    accordionData.push({
      title: `${item.groupName} - ${item.courseCount}`,
      body: getAccBody(item.course)
    });
  });

  fire({type: 'change_repeater_custom_source', customSource: groupCourses}, 'allGroupsRepeater');
  $KW.createAccordion(accordionData, panelAccordion);

  panelAccordion.hide();
  panelIterators.hide();
  if(!courses_display_type || courses_display_type == 'tiles') {
    panelIterators.fadeIn();
  } else {
    panelAccordion.fadeIn();
  }

  switchButton.on('click', e => {
    courses_display_type = localStorage.getItem('courses_display_type');
    if(courses_display_type == 'tiles') {
      courses_display_type = 'accordion';
      panelIterators.fadeOut(200, () => panelAccordion.fadeIn(200));
    } else {
      courses_display_type = 'tiles';
      panelAccordion.fadeOut(200, () => panelIterators.fadeIn(200));
    }
  });
});
