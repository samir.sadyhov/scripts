if($('.menu__icon').length) {
  let openMenu = false;
  let xDown = null;
  let yDown = null;

  const hideShowMenu = () => {
    if(!openMenu) {
      openMenu = !openMenu;
      $('.menu__icon').addClass('_active');
      $('.app_left_panel').addClass('_active');
      $('.app_content').addClass('_lock');
    } else {
      openMenu = !openMenu;
      $('.menu__icon').removeClass('_active');
      $('.app_left_panel').removeClass('_active');
      $('.app_content').removeClass('_lock');
    }
  }

  const handleTouchStart = evt => {
    xDown = evt.touches[0].clientX;
    yDown = evt.touches[0].clientY;
  }

  const handleTouchMove = evt => {
    if ( ! xDown || ! yDown ) return;
    let xUp = evt.touches[0].clientX;
    let yUp = evt.touches[0].clientY;
    let xDiff = xDown - xUp;
    let yDiff = yDown - yUp;

    if(Math.abs(xDiff) > Math.abs(yDiff)) {
      if (xDiff > 0) {
        //left swipe
        if(openMenu) hideShowMenu();
      } else {
        //right swipe
        if(!openMenu) hideShowMenu();
      }
    }
    xDown = null;
    yDown = null;
  }

  $('.menu__icon').off().on('click', e => {
    e.preventDefault();
    hideShowMenu();
  });

  $('.app_left_panel').off().on('click', e => {
    e.preventDefault();
    if($(e.target).hasClass('uk-button')) hideShowMenu();
  });

  document.addEventListener('touchstart', handleTouchStart, false);
  document.addEventListener('touchmove', handleTouchMove, false);
}
