const searchInRegistry = params => {
  return new Promise((resolve, reject) => {
    rest.synergyGet(`api/registry/data_ext?${params}`, res => resolve(res), err => reject(err));
  });
}

const parseAllCourse = data => {
  return data.map(x => {
    const {dataUUID, documentID, fieldKey, fieldValue} = x;
    return {
      dataUUID,
      documentID,
      name: fieldValue.kw_form_course_pasport || '',
      code: fieldValue.kw_form_skill_code || '',
      fullName: (fieldValue.kw_form_skill_code || '') + '-' + (fieldValue.kw_form_course_pasport || ''),
      img: fieldKey ? (fieldKey.kw_form_course_image || null) : null
    }
  });
}

const parseGroupCourse = data => {
  const compare = list => {
    if(!list) return [];
    return $KW.appInfo.allCourses.map(x => {
      if (list.split(';').includes(x.documentID)) return x.documentID;
    }).filter(x => x != undefined);
  }
  return data.map(x => {
    const {dataUUID, documentID, fieldKey, fieldValue} = x;
    return {
      dataUUID,
      documentID,
      name: {
        ru: fieldValue.kw_form_courseGroup_name || '',
        kk: fieldValue.kw_form_courseGroup_name_kz || ''
      },
      description: {
        ru: fieldValue.kw_form_courseGroup_description || '',
        kk: fieldValue.kw_form_courseGroup_description_kz || ''
      },
      course: fieldKey ? compare(fieldKey.kw_form_courseGroup_list) : [],
      img: fieldKey ? (fieldKey.kw_form_courseGroup_image || null) : null
    }
  });
}

const getAllCourse = () => {
  let params = $.param({
    registryCode: 'kw_registry_course',
    field: 'kw_form_course_status',
    condition: 'TEXT_EQUALS',
    key: '2'
  });
  ['kw_form_course_pasport', 'kw_form_course_image', 'kw_form_skill_code'].forEach(x => params += `&fields=${x}`);

  return new Promise(resolve => {
    searchInRegistry(params).then(res => {
      res.recordsCount == 0 ? resolve(null) : resolve(parseAllCourse(res.result));
    }).catch(err => {
      resolve(null);
      console.log('getAllCourse ERROR', err);
    });
  });
}

const getGroupCourse = () => {
  let params = 'registryCode=kw_registry_courseGroups&filterCode=access_to_course';
  ['kw_form_courseGroup_name',
  'kw_form_courseGroup_name_kz',
  'kw_form_courseGroup_description',
  'kw_form_courseGroup_description_kz',
  'kw_form_courseGroup_list',
  'kw_form_courseGroup_image'
  ].forEach(x => params += `&fields=${x}`);

  return new Promise(resolve => {
    searchInRegistry(params).then(res => {
      res.recordsCount == 0 ? resolve(null) : resolve(parseGroupCourse(res.result));
    }).catch(err => {
      resolve(null);
      console.log('getGroupCourse ERROR', err);
    });
  });
}

const parseAsfValue = asfDataValue => {
  if(!asfDataValue) return null;
  const {value = '', key = ''} = asfDataValue;
  return key ? {value, key} : {value};
}

const parseAsfTable = asfTable => {
  try {
    const result = [];

    if(!asfTable.hasOwnProperty('data')) return result;

    const data = asfTable.data.filter(x => x.type != 'label');

    if(!data.length) return result;

    const ids = data.map(x => x.id.slice(0, x.id.indexOf('-b'))).uniq();
    let tbi =  data.slice(-1)[0].id;
    tbi = Number(tbi.slice(tbi.indexOf('-b') + 2));

    for(let i = 1; i <= tbi; i++) {
    	const item = {};
      ids.forEach(key => {
        const parseValue = parseAsfValue($KW.getValue(asfTable, `${key}-b${i}`));
        if(parseValue) {
          if(key == 'kw_form_userCard_competenceName_other' || key == 'kw_form_userCard_competenceName') {
            item.documentID = parseValue.key;
            item.name = parseValue.value;
            item.courseType = key.indexOf('_other') == -1 ? 'required' : 'additional';
          } else if (key == 'kw_form_userCard_finishDate_other' || key == 'kw_form_userCard_finishDate') {
            item.finishDate = parseValue.value ? `Обучение до ${parseValue.value}` : '';
            item.finishDateKey = parseValue?.key || '';
            item.expired = parseValue.key ? (new Date(parseValue.key).getTime() - new Date().getTime() < 0 ? t('Срок обучения истек') : '') : '';
          } else if (key == 'kw_form_userCard_status' || key == 'kw_form_userCard_status_other') {
            item.curseStatus = t(parseValue?.value || '');
            item.curseStatusKey = parseValue?.key || '';
            item[key] = parseValue;
          } else {
            item[key] = parseValue;
          }
        }
      });
      result.push(item);
    }

    return result;
  } catch (err) {
    console.log('ERROR parseAsfTable', err);
  }
}

this.$KW = {
  appInfo: {},

  getAppInfo: function(){
    const {userCardUUID} = Cons.getAppStore();
    return new Promise(resolve => {

      let library;

      this.loadAsfData(userCardUUID).then(userAsfData => {
        const requiredSkillsAsfTableData = $KW.getValue(userAsfData, 'kw_form_userCard_competenceTable');
        const additionalSkillsAsfTableData = $KW.getValue(userAsfData, 'kw_form_userCard_competenceTable_other');
        const requiredSkillsTable = parseAsfTable(requiredSkillsAsfTableData);
        const additionalSkillsTable = parseAsfTable(additionalSkillsAsfTableData);
        const booksTable = parseAsfTable($KW.getValue(userAsfData, 'kw_form_userCard_books'));

        const files = booksTable.map(x => {
          if(x.kw_form_position_bookFile.hasOwnProperty('key')) return x.kw_form_position_bookFile;
        }).filter(x => x);

        const links = booksTable.map(x => {
          if(x.kw_form_position_bookLink.hasOwnProperty('value') && x.kw_form_position_bookLink.value) return x.kw_form_position_bookLink;
        }).filter(x => x);

        library = {files, links};

        this.appInfo.profile = {
          trainingProfile: $KW.getValue(userAsfData, 'kw_form_userCard_reglink'),
          department: $KW.getValue(userAsfData, 'kw_form_userCard_department'),
          office: $KW.getValue(userAsfData, 'kw_form_userCard_office'),
          manager: $KW.getValue(userAsfData, 'kw_form_userCard_manager'),
          certified: $KW.getValue(userAsfData, 'kw_form_userCard_certified'),
          requiredSkillsAsfTableData, additionalSkillsAsfTableData,
          requiredSkillsTable, additionalSkillsTable, library
        };

        return getAllCourse();
      }).then(allCourses => {
        this.appInfo.allCourses = allCourses;

        this.appInfo.profile.requiredSkillsTable.forEach(item => {
          const courseInfo = allCourses.find(x => x.documentID == item.documentID);
          if(courseInfo) {
            item.courseImage = courseInfo.img;
            item.dataUUID = courseInfo.dataUUID;
            item.name = courseInfo.name;
            item.code = courseInfo.code;
            item.fullName = courseInfo.fullName;
          }
        });

        this.appInfo.profile.additionalSkillsTable.forEach(item => {
          const courseInfo = allCourses.find(x => x.documentID == item.documentID);
          if(courseInfo) {
            item.courseImage = courseInfo.img;
            item.dataUUID = courseInfo.dataUUID;
            item.name = courseInfo.name;
            item.code = courseInfo.code;
            item.fullName = courseInfo.fullName;
          }
        });

        return getGroupCourse();
      }).then(groupCourses => {
        if(!groupCourses) throw new Error('Не найдены группы курсов');

        this.appInfo.groupCourses = groupCourses.filter(x => x.course.length);

        const myRequiredCourse = this.appInfo.profile.requiredSkillsTable.length;
        const myAdditionalCourse = this.appInfo.profile.additionalSkillsTable.length;
        const myCourse = myRequiredCourse + myAdditionalCourse;
        const myFinishCourse = this.appInfo.profile.requiredSkillsTable.reduce((sum, x) => x.kw_form_userCard_status.key == '2' ? sum + 1 : 0, 0);

        this.appInfo.counts = {
          myCourse,
          myFinishCourse,
          allCourse: this.appInfo.groupCourses.reduce((sum, x) => sum + x.course.length, 0),
          books: library.files.length + library.links.length,
          percent: Math.round(myFinishCourse / myRequiredCourse * 100)
        }

        Cons.setAppStore({appInfo: this.appInfo});
        resolve(this.appInfo);
      }).catch(err => {
        console.log('ERROR getAppInfo', err);
        resolve(null);
      });

    });
  },

  createAccordion: function(accordionData, accordionContainer) {
    const toggleAcc = el => {
      el = el[0];
      el.classList.toggle("active");
      let panel = el.nextElementSibling;
      if (panel.style.maxHeight) panel.style.maxHeight = null;
      else panel.style.maxHeight = "100%";
    }

    accordionContainer.empty();

    const titleEl = [];

    accordionData.forEach(item => {
      const block = $('<div class="accordion-block">')
      const button = $(`<button class="accordion-button">${t(item.title)}</button>`);
      const body = $('<div class="accordion-body">');

      titleEl.push({el: button, text: item.title});

      accordionContainer.append(block);
      block.append(button).append(body);
      button.on('click', () => toggleAcc(button));

      body.append(item.body);

      if(item.hasOwnProperty('active') && item.active) body.addClass('active').css('max-height', '100%');

      body.append('<div class="accordion-footer"></div>');
    });

    Cons.setAppStore({localeClickerAccordion: function(){
      titleEl.forEach(item => {
        item.el.text(t(item.text));
      });
    }});
  },

  getPageParamValue: function(paramName) {
    const {paramValues, params, pageParams} = Cons.getCurrentPage();
    const paramID = params.find(x => x.name == paramName).id;
    return paramValues.find(x => x.pageParamID == paramID || x.pageParamName == paramName).value;
  },

  changeLabel: function(id, ru, kk) {
    if(kk) {
      fire({type: 'change_label', text: localizedText(ru, ru, kk, ru)}, id);
    } else {
      fire({type: 'change_label', text: localizedText(ru, ru, t(ru, 'kk'), ru)}, id);
    }
  },

  buttonChangeText: function(id, ru, kk) {
    if(kk) {
      fire({type: 'button_change_text', text: localizedText(ru, ru, kk, ru)}, id);
    } else {
      fire({type: 'button_change_text', text: localizedText(ru, ru, t(ru, 'kk'), ru)}, id);
    }
  },

  getModalDialog: function(title, body, buttonName, buttonAction) {
    let dialog = $('<div class="uk-flex-top" uk-modal>');
    let md = $('<div class="uk-modal-dialog uk-margin-auto-vertical">');
    let modalBody = $('<div class="uk-modal-body" uk-overflow-auto>');
    let footer = $('<div class="uk-modal-footer uk-text-right">');

    modalBody.append(body).append('<button class="uk-modal-close-default" type="button" uk-close></button>');
    footer.append('<button class="uk-button uk-button-default uk-modal-close kw-button kw-button-transparent" type="button">Закрыть</button>');
    dialog.append(md);
    md.append(`<div class="uk-modal-header"><h3>${title}</h3></div>`)
    .append(modalBody).append(footer);

    if(buttonName) {
      let actionButton = $('<button class="uk-button uk-button-default kw-button uk-margin-right" type="button">');
      actionButton.html(buttonName).on('click', buttonAction);
      footer.prepend(actionButton);
    }

    return dialog;
  },

  parseAsfTable: function(asfTable){
    return parseAsfTable(asfTable);
  },

  getValue: function(asfData, cmp) {
    return asfData.data.find(x => x.id == cmp);
  },

  loadAsfData: function(uuid) {
    return new Promise((resolve, reject) => {
      rest.synergyGet(`api/asforms/data/${uuid}`, res => resolve(res), err => reject(err));
    });
  },

  getAllAsfData: function(uuids) {
    let url = 'api/asforms/data/get?';
    uuids.forEach(uuid => url += `dataUUID=${uuid}&`);
    url = url.slice(0, -1);
    return new Promise((resolve, reject) => {
      rest.synergyGet(url, res => resolve(res), err => reject(err));
    });
  },

  getAsfDataUUID: function(documentID) {
    return new Promise((resolve, reject) => {
      rest.synergyGet(`api/formPlayer/getAsfDataUUID?documentID=${documentID}`, res => resolve(res), err => reject(err));
    });
  },

  getFile: function(fileId) {
    return new Promise((resolve, reject) => {
      const requestUrl = AS.FORMS.ApiUtils.getFullUrl(`rest/api/storage/pdf/get?inline=true&identifier=${fileId}`);
      const xhr = new XMLHttpRequest();
      xhr.open('GET', requestUrl, true);
      AS.FORMS.ApiUtils.addAuthHeader(xhr);
      xhr.responseType = 'blob';
      xhr.onload = function (e) {
        if (this.status === 200) {
          const blob = window.URL.createObjectURL(this.response);
          resolve(blob);
        } else if (this.status === 404) {
          reject('Файл не найден!');
        } else if (this.status === 403) {
          reject('Ошибка загрузки файла!');
        }
      }
      xhr.send();
    });
  },

  getFileBlock: function(item, type = 'file') {
    const fileContainer = $('<div>', {class: 'file_container'});
    const fileIcon = $('<span>', {class: 'file_icon'});
    const fileName = $('<div>', {class: 'file_name'});
    const fileFormat = item.value.substr(item.value.lastIndexOf('.') + 1);
    const linkName = item.key.substr(0, item.key.lastIndexOf(';'));

    fileContainer.on('click', e => {
      if(type == 'file') {
        Cons.showLoader();
        this.getFile(item.key).then(blob => {
          Cons.hideLoader();
          open(blob, '_blank');
        }).catch(err => {
          Cons.hideLoader();
          showMessage(err, 'error');
        });
      } else {
        open(item.value, '_blank');
      }
    });

    fileName.text(type == 'file' ? item.value : linkName);
    fileIcon.append(getSvgIcon(type == 'file' ? fileFormat : 'link'));
    fileContainer.append(fileIcon).append(fileName);

    return fileContainer;
  },

  appendTable: function(uuid, tableId, data) {
    return new Promise(resolve => {
      AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/asforms/data/append_table",
        res => res.errorCode != 0 ? resolve(null) : resolve(res),
        null, JSON.stringify({uuid, tableId, data}), "application/json; charset=utf-8",
        err => {
          console.log('appendTable ERROR:', err.responseJSON);
          resolve(null);
        }
      );
    });
  },

  mergeFormData: function(uuid,  data) {
    return new Promise(resolve => {
      AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/asforms/data/merge",
        res => res.errorCode != 0 ? resolve(null) : resolve(res),
        null, JSON.stringify({uuid, data}), "application/json; charset=utf-8",
        err => {
          console.log('mergeFormData ERROR:', err.responseJSON);
          resolve(null);
        }
      );
    });
  },

  customFormatDate: function(datetime) {
    datetime = datetime.split(/\D/);
    return datetime[2] + '.' + datetime[1] + '.' + datetime[0] + ' ' + datetime[3] + ':' + datetime[4];
  },

  createDocRCC: function(registryCode, asfData) {
    return new Promise((resolve, reject) => {
      AS.FORMS.ApiUtils.simpleAsyncPost('rest/api/registry/create_doc_rcc', res => {
        res.errorCode != 0 ? reject(res.errorMessage) : resolve(res);
      }, null, JSON.stringify({
        registryCode: registryCode,
        data: asfData,
        sendToActivation: false
      }), "application/json; charset=utf-8", err => {
        console.log('create_doc_rcc error', err.responseJSON.errorMessage);
        reject('Ошибка создания записи реестра');
      });
    });
  },

  uploadFile: function(file, dataUUID, asfNodeID, fileCmp) {
    return new Promise((resolve, reject) => {
      let formData = new FormData();
      formData.append(fileCmp, file);

      AS.FORMS.ApiUtils.uploadFile(asfNodeID, dataUUID, formData).then(elementID => {
        this.mergeFormData(dataUUID, [{
          id: fileCmp,
          type: 'file',
          key: elementID,
          value: file.name
        }]).then(res => resolve(res));
      });

    });
  },

  searchCourseHistory: function(courseDocID) {
    let params = 'registryCode=kw_registry_course_history';
    params += `&field=kw_form_course_history_user&condition=TEXT_EQUALS&key=${AS.OPTIONS.currentUser.userid}`;
    params += `&field=kw_form_course_history_courseName&condition=TEXT_EQUALS&key=${courseDocID}`;
    params += '&loadData=false&countInPart=1';

    return new Promise(resolve => {
      searchInRegistry(params).then(res => {
        if(res.count > 0) {
          this.loadAsfData(res.data[0].dataUUID).then(resolve);
        } else {
          resolve(null);
        }
      }).catch(err => {
        resolve(null);
        console.log('searchCourseHistory ERROR', err);
      });
    });
  },

  searchUserQuestionnaire: function(courseDocID) {
    let params = 'registryCode=kw_registry_studyInterview';
    params += `&field=kw_form_studyInterview_user&condition=TEXT_EQUALS&key=${AS.OPTIONS.currentUser.userid}`;
    params += `&field=kw_form_studyInterview_course&condition=TEXT_EQUALS&key=${courseDocID}`;
    params += '&loadData=false&countInPart=1';

    return new Promise(resolve => {
      searchInRegistry(params).then(res => {
        if(res.count > 0) {
          resolve(res.data[0]);
        } else {
          resolve(null);
        }
      }).catch(err => {
        resolve(null);
        console.log('searchUserQuestionnaire ERROR', err);
      });
    });
  }
}

Array.prototype.uniq = function() {
  return this.filter((v, i, a) => i == a.indexOf(v));
}

//перемешать элементы массива
Array.prototype.shuffle = function() {
  for (let i = this.length - 1; i > 0; i--) {
    const j = Math.floor(Math.random() * (i + 1));
    [this[i], this[j]] = [this[j], this[i]];
  }
  return this;
}
