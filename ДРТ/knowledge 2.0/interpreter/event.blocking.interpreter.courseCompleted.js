const statusValue = 'Обучение завершено';
const statusKey = '2';

var result = true;
var message = "ok";

function customFormatDate(datetime){
  datetime = datetime.split(/\D/);
  return datetime[2] + '.' + datetime[1] + '.' + datetime[0] + ' ' + datetime[3] + ':' + datetime[4];
}

try {

  let currentFormData = API.getFormData(dataUUID);
  let userCard = UTILS.getValue(currentFormData, 'kw_form_course_history_userCard');
  let courseName = UTILS.getValue(currentFormData, 'kw_form_course_history_courseName');
  let isapprove = UTILS.getValue(currentFormData, 'kw_form_course_history_isapprove');

  if(!userCard.hasOwnProperty('key')) throw new Error('не найдена карточка пользователя');

  if(isapprove.key == '1') {

    if(!courseName.hasOwnProperty('key')) throw new Error('не найдена ссылка на курс');

    let userCardFormData = API.getFormData(API.getAsfDataId(userCard.key));

    let competenceTable = UTILS.getValue(userCardFormData, 'kw_form_userCard_competenceTable');
    let competenceTableOther = UTILS.getValue(userCardFormData, 'kw_form_userCard_competenceTable_other');

    let rr;

    if(competenceTable.hasOwnProperty('data')) {
      competenceTable.data.forEach(function(x){
        if(x.hasOwnProperty('key') && x.key == courseName.key) rr = x;
      });
    }

    if(competenceTableOther.hasOwnProperty('data')) {
      competenceTableOther.data.forEach(function(x){
        if(x.hasOwnProperty('key') && x.key == courseName.key) rr = x;
      });
    }

    if(!rr) throw new Error('не найден курс в карточке пользователя');

    let tbi = rr.id.substr(rr.id.indexOf('-b'));
    let currentDate = UTILS.getCurrentDateParse();

    if(rr.id.indexOf('_other') != -1) {
      UTILS.setValue(competenceTableOther, 'kw_form_userCard_status_other' + tbi, {
        key: statusKey,
        value: statusValue
      });
      UTILS.setValue(competenceTableOther, 'kw_form_userCard_real_finishDate_other' + tbi, {key: currentDate, value: customFormatDate(currentDate)});
    } else {
      UTILS.setValue(competenceTable, 'kw_form_userCard_status' + tbi, {
        key: statusKey,
        value: statusValue
      });
      UTILS.setValue(competenceTable, 'kw_form_userCard_real_finishDate' + tbi, {key: currentDate, value: customFormatDate(currentDate)});
    }


    API.mergeFormData(userCardFormData);


  } else {
    message = 'Результат подтверждения менеджером: ' + isapprove.value + '. Статус курса не меняется';
  }

} catch (err) {
  log.error(err.message);
  result = false;
  message = err.message;
}
