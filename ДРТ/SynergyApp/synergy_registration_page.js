const createUserParam = {
  //права юзера в системе
  userParams: {
    hasAccess: true, //Доступ в систему
    accessMobile: true, //Доступ в мобильное приложение
    isAdmin: false, //Администратор
    isChancellery: false, //Сотрудник канцелярии
    isConfigurator: false //Разработчик Synergy
  },

  add: {
    groupCodes: ['vse'], //массив групп куда будет добавлен юзер (параметр не обязательный)
    positionCode: 'spetsialist' //код должности на которую будет назначен юзер (параметр не обязательный)
  }
};

const capitalizeFirstLetter = str => {
  if (typeof str !== 'string' || str.length === 0) return '';
  return str.charAt(0).toUpperCase() + str.slice(1).toLowerCase();
}

const createUser = async data => {
  return new Promise(async resolve => {
    try {
      const {defaultUser, defaultUserPassword} = Cons.getCurrentApp();
      const auth = "Basic " + btoa(unescape(encodeURIComponent(`${defaultUser}:${defaultUserPassword}`)));
      const url = `../Synergy/rest/api/filecabinet/user/save`;
      const response = await fetch(url, {
        method: 'POST',
        headers: {"Authorization": auth, "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"},
        body: data
      });
      resolve(response.json());
    } catch (err) {
      console.log(`ERROR [ createUser ]: ${err.message}`);
      resolve(null);
    }
  });
}

const searchPosition = async pointer_code => {
  return new Promise(async resolve => {
    try {
      const {defaultUser, defaultUserPassword} = Cons.getCurrentApp();
      const auth = "Basic " + btoa(unescape(encodeURIComponent(`${defaultUser}:${defaultUserPassword}`)));
      const url = `../Synergy/rest/api/positions/search?pointer_code=${pointer_code}&searchTypePC=exact&deleted=false`;
      const response = await fetch(url, {method: 'GET', headers: {"Authorization": auth}});

      if(!response.ok) throw new Error(await response.text());
      resolve(response.json());
    } catch (err) {
      console.log(`ERROR [ searchPosition ]: ${err.message}`);
      resolve(null);
    }
  });
}

const positionsAppoint = async (userID, positionID) => {
  return new Promise(async resolve => {
    try {
      const {defaultUser, defaultUserPassword} = Cons.getCurrentApp();
      const auth = "Basic " + btoa(unescape(encodeURIComponent(`${defaultUser}:${defaultUserPassword}`)));
      const url = `../Synergy/rest/api/positions/appoint?positionID=${positionID}&userID=${userID}`;
      const response = await fetch(url, {method: 'GET', headers: {"Authorization": auth}});

      if(!response.ok) throw new Error(await response.text());
      resolve(response.json());
    } catch (err) {
      console.log(`ERROR [ positionsAppoint ]: ${err.message}`);
      resolve(null);
    }
  });
}

const groupsAddUser = async (userID, groupCode) => {
  return new Promise(async resolve => {
    try {
      const {defaultUser, defaultUserPassword} = Cons.getCurrentApp();
      const auth = "Basic " + btoa(unescape(encodeURIComponent(`${defaultUser}:${defaultUserPassword}`)));
      const url = `../Synergy/rest/api/storage/groups/add_user?groupCode=${groupCode}&userID=${userID}`;
      const response = await fetch(url, {method: 'GET', headers: {"Authorization": auth}});

      if(!response.ok) throw new Error(await response.text());
      resolve(response.text());
    } catch (err) {
      console.log(`ERROR [ groupsAddUser ]: ${err.message}`);
      resolve(null);
    }
  });
}

pageHandler('synergy_registration_page', () => {
  if (!Cons.getAppStore().synergy_registration_page_listener) {

    addListener('button_click', 'buttonSelectEDS', e => {
      NCALayer.registerEDS(async result => {
        Cons.showLoader();
        try {
          if(result.hasOwnProperty('errorCode') && result.errorCode != 0) throw new Error(result.errorMessage);

          let {email, iin, name, patronymic, surname} = result;
          name = capitalizeFirstLetter(name);
          surname = capitalizeFirstLetter(surname);
          patronymic = capitalizeFirstLetter(patronymic);

          const body = new URLSearchParams();
          body.append("lastname", name);
          body.append("firstname", surname);
          body.append("patronymic", patronymic);
          body.append("iin", iin);
          body.append("email", email);
          body.append("pointersCode", `IIN${iin}`);

          for(const key in createUserParam.userParams) {
            body.append(key, createUserParam.userParams[key]);
          }

          const createUserResult = await createUser(body);
          if(!createUserResult) throw new Error(i18n.tr('Ошибка регистрации'));
          if(createUserResult.hasOwnProperty('errorCode') && createUserResult.errorCode != 0) {
            throw new Error(createUserResult.errorMessage);
          }

          const {userID} = createUserResult;

          for(const key in createUserParam.add) {
            switch (key) {
              case 'groupCodes': {
                for(let i = 0; i < createUserParam.add[key].length; i++) {
                  const groupCode = createUserParam.add[key][i];
                  await groupsAddUser(userID, groupCode);
                }
                break;
              }
              case 'positionCode': {
                const searchResult = await searchPosition(createUserParam.add[key]);
                if(searchResult && searchResult.length) await positionsAppoint(userID, searchResult[0]);
                break;
              }
            }
          }

          Cons.hideLoader();
          showMessage(i18n.tr('Регистрация прошла успешно'), 'success');
          setTimeout(() => {
            fire({type: 'goto_page', pageCode: 'synergy_auth_page'}, 'root-panel');
          }, 2000);
        } catch (err) {
          Cons.hideLoader();
          console.error(err);
          showMessage(err.message, "error");
        }
      });
    });

    Cons.setAppStore({synergy_registration_page_listener: true});
  }
});
