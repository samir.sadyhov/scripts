const synergyApps = [];

synergyApps.push({
  name: 'Потоки работ',
  icon: 'work', // https://fonts.google.com/icons?icon.style=Filled&icon.set=Material+Icons
  url: 'workflows' // урл приложения в конструкторе
});

const getNavButton = (name, icon) => {
  const button = $(`<a href="#" class="synergy_nav_item_button" title="${name}">`);
  button.append(
    `<span class="material-icons" style="margin-right: 5px;">${icon}</span>`,
    `<span class="synergy_nav_item_text">${name}</span>`
  );
  return button;
}

const openApp = url => {
  const {login, password} = Cons.creds;
  const g = btoa(unescape(encodeURIComponent(`${login}:${password}`)));
  const iFrame = $(`<iframe id="synergy-app-iframe" src="../${url}?app=${g}&locale=${AS.OPTIONS.locale}">`);
  iFrame.css({
    "width": "100%",
    "min-height": "100vh"
  });
  $('#synergyAppContent').empty().append(iFrame);
}

pageHandler('synergy_app_page', () => {
  const synergyNavIcon = $('<div>', {id: 'synergy-nav-icon'});
  $('#panelNavMenuButton').append(synergyNavIcon);
  synergyNavIcon.append('<span>', '<span>', '<span>');

  let menuHide = true;

  synergyNavIcon.on('click', e => {
    if(menuHide) {
      synergyNavIcon.addClass("open");
      $('#synergyAppNav').addClass("open");
      $('#synergyAppContent').addClass("open");
    } else {
      synergyNavIcon.removeClass("open");
      $('#synergyAppNav').removeClass("open");
      $('#synergyAppContent').removeClass("open");
    }
    menuHide = !menuHide;
  });

  synergyApps.forEach((item, i) => {
    const {name, icon, url} = item;
    const navButton = getNavButton(i18n.tr(name), icon);

    if(i == 0) openApp(url);

    navButton.off().on('click', e => {
      e.preventDefault();
      e.target.blur();
      synergyNavIcon.removeClass("open");
      $('#synergyAppNav').removeClass("open");
      $('#synergyAppContent').removeClass("open");
      menuHide = true;

      openApp(url);
    });

    $('#panelNavAppsButton').append(navButton);
  });

  const exitButton = getNavButton(i18n.tr('Выход'), 'logout');
  exitButton.off().on('click', e => {
    e.preventDefault();
    e.target.blur();
    Cons.logout();
  });

  const settingsButton = getNavButton(i18n.tr('Настройки'), 'tune');
  settingsButton.off().on('click', e => {
    e.preventDefault();
    e.target.blur();
    console.log('settingsButton');
  });
  $('#panelNavAddButton').append(settingsButton, exitButton);

});
