pageHandler('synergy_auth_page', () => {
  $('#textInputLogin').parent().find('span').css('color', '#fff');
  $('#textInputPassword').parent().find('span').css('color', '#fff');

  if (!Cons.getAppStore().auth_page_listener) {
    addListener('auth_success', 'buttonAppLogin', authed => {
      const {login, password} = authed.creds;

      Cons.creds.login = login;
      Cons.creds.password = password;
      AS.apiAuth.setCredentials(login, password);
      AS.OPTIONS.login = login;
      AS.OPTIONS.password = password;
      AS.OPTIONS.currentUser = authed.data.person;
    });

    Cons.setAppStore({auth_page_listener: true});
  }
});
