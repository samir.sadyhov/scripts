// тип аутентификации
/* Возможные параметры:
1 - по логину и паролю
2 - по эцп
3 - двухфакторная
4 - по логину или ЭЦП
*/
const TYPE_AUTH = 4;

const show = id => fire({type: 'set_hidden', hidden: false}, id);

const checkAuth = async (login, password) => {
  return new Promise(async resolve => {
    try {
      const auth = "Basic " + btoa(unescape(encodeURIComponent(`${login}:${password}`)));
      const url = `../Synergy/rest/api/person/auth`;
      const response = await fetch(url, {method: 'GET', headers: {"Authorization": auth}});

      if(!response.ok) {
        if(response.status == 401) {
          throw new Error('Ошибка авторизации. Проверьте правильность введенных данных.');
        } else {
          throw new Error(await response.text());
        }
      }

      resolve(response.json());
    } catch (err) {
      resolve({
        errorCode: 666,
        errorMessage: err.message
      });
    }
  });
}

const checkInputs = () => {
  const result = {errorCode: 0, errorMessage: ''};
  const textInputLogin = $('#textInputLogin');
  const textInputPassword = $('#textInputPassword');

  if(textInputLogin.val() == '') {
    result.errorCode = 666;
    result.errorMessage = i18n.tr('Не заполнен логин');
    textInputLogin.addClass('highlight');
  } else {
    textInputLogin.removeClass('highlight');
  }

  if(textInputPassword.val() == '') {
    result.errorCode = 666;
    if(result.errorMessage != '') result.errorMessage += '<br>';
    result.errorMessage += i18n.tr('Не заполнен пароль');
    textInputPassword.addClass('highlight');
  } else {
    textInputPassword.removeClass('highlight');
  }

  return result;
}

const autorization = async () => {
  try {
    const textInputLogin = $('#textInputLogin');
    const textInputPassword = $('#textInputPassword');
    const checkResult = checkInputs();

    if(checkResult.errorCode != 0) throw new Error(checkResult.errorMessage);

    const user = await checkAuth(textInputLogin.val(), textInputPassword.val());
    if(user.hasOwnProperty('errorCode')) throw new Error(user.errorMessage);

    NCALayer.authEDS(result => {
      try {
        if(result.hasOwnProperty('errorCode') && result.errorCode != 0) throw new Error(result.errorMessage);

        if(result.personInfo.person.userid != user.userid) throw new Error('Ошибка авторизации. Выбран не верный ключ.');

        AS.apiAuth.setCredentials('$token', result.token);
        AS.OPTIONS.login = '$token';
        AS.OPTIONS.password = result.token;
        Cons.login({login: '$token', password: result.token});
      } catch (err) {
        console.log(err);
        UIkit.notification.closeAll();
        showMessage(i18n.tr(err.message), 'error');
      }
    });

  } catch (e) {
    console.log(e);
    UIkit.notification.closeAll();
    showMessage(i18n.tr(e.message), 'error');
  }
}

pageHandler('synergy_auth_page', () => {

  // отображение нужных панелей в зависимости от выбранного типа аутентификации
  switch (TYPE_AUTH) {
    case 1: show('panelLoginPass'); break;
    case 2: show('panelEDSAuth'); break;
    case 3: show('panelLoginPass'); break;
    default: {
      show('panelLoginPass');
      show('panelButtons');
    }
  }

  if (!Cons.getAppStore().auth_page_listener) {

    addListener('auth_success', 'root-panel', authed => {
      const {login, password} = authed.creds;

      Cons.creds.login = login;
      Cons.creds.password = password;
      AS.apiAuth.setCredentials(login, password);
      AS.OPTIONS.login = login;
      AS.OPTIONS.password = password;
      AS.OPTIONS.currentUser = authed.data.person;
    });

    addListener('button_click', 'buttonSelectEDS', e => {
      NCALayer.authEDS(result => {
        if(result.hasOwnProperty('errorCode') && result.errorCode != 0) {
          showMessage(result.errorMessage, "error");
        } else {
          AS.apiAuth.setCredentials('$token', result.token);
          AS.OPTIONS.login = '$token';
          AS.OPTIONS.password = result.token;
          Cons.login({login: '$token', password: result.token});
        }
      });
    });

    addListener('value_changed_input', 'textInputPassword', e => {
      $('#textInputPassword').removeClass('highlight');
    });

    addListener('value_changed_input', 'textInputLogin', e => {
      $('#textInputLogin').removeClass('highlight');
    });

    addListener('text_input_key_down_enter', 'textInputLogin', e => {
      $('#textInputPassword').focus();
    });

    addListener('text_input_key_down_enter', 'textInputPassword', e => {
      if(TYPE_AUTH == 3) {
        autorization();
      } else {
        Cons.login({login: $('#textInputLogin').val(), password: $('#textInputPassword').val()});
      }
    });

    Cons.setAppStore({auth_page_listener: true});
  }

  $('#buttonAppLogin').on('click', e => {
    if(TYPE_AUTH == 3) {
      autorization();
    } else {
      Cons.login({login: $('#textInputLogin').val(), password: $('#textInputPassword').val()});
    }
  });

});
