const synergyApps = [];

synergyApps.push({
  name: 'Потоки работ',
  icon: 'work', // https://fonts.google.com/icons?icon.style=Filled&icon.set=Material+Icons
  url: 'workflows' // урл приложения в конструкторе
});

synergyApps.push({
  name: 'Реестры',
  icon: 'list',
  url: 'registry'
});

synergyApps.push({
  name: 'Документы',
  icon: 'menu_book',
  url: 'documents'
});

let openAppID;

function getCookie(name) {
  let matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setCookie(name, value, options = {}) {

  options = {
    path: '/',
    ...options
  };

  if (options.expires instanceof Date) {
    options.expires = options.expires.toUTCString();
  }

  let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);

  for (let optionKey in options) {
    updatedCookie += "; " + optionKey;
    let optionValue = options[optionKey];
    if (optionValue !== true) {
      updatedCookie += "=" + optionValue;
    }
  }

  document.cookie = updatedCookie;
}

function deleteCookie(name) {
  setCookie(name, "", {
    'max-age': -1
  });
}

const getNavButton = (name, icon) => {
  const button = $(`<a href="#" class="synergy_nav_item_button" title="${i18n.tr(name)}" defaultName="${name}">`);
  button.append(
    `<span class="material-icons" style="margin-right: 5px;">${icon}</span>`,
    `<span class="synergy_nav_item_text">${i18n.tr(name)}</span>`
  );
  return button;
}

const generateGuid = () => Math.random().toString(36).substring(2, 10);

const openApp = (name, url, navButton) => {
  deleteCookie(`constructor_creds_for_${url}`);
  const {login, password} = Cons.creds;
  const uuid = generateGuid();
  let str = `${uuid}${login.split('').reverse().join('')}`;
  str += `${uuid}${password.split('').reverse().join('')}`;
  const g = btoa(unescape(encodeURIComponent(str)));
  const iFrame = $(`<iframe id="synergy-app-iframe" src="../${url}?app=${g}&locale=${AS.OPTIONS.locale}">`);
  iFrame.css({
    "width": "100%",
    "min-height": "calc(100vh - 56px)"
  });
  $('.synergy_nav_item_button').removeClass('active');
  navButton.addClass('active');
  $('#synergyAppContent').empty().append(iFrame);
  fire({type: 'change_label', text: localizedText(name, name, name, name)}, 'appNameLabel');
  openAppID = url;
}

const updateTranslations = async () => {
  i18n.locale = AS.OPTIONS.locale;
  return new Promise(resolve => {
    rest.synergyGet(`system/messages?localeID=${AS.OPTIONS.locale}`,
      res => {
        i18n.messages = res;
        resolve(true);
      },
      err => {
        i18n.messages = null;
        resolve(true);
      }
    );
  });
}

pageHandler('synergy_app_page', () => {
  let currentApp = {};

  synergyApps.forEach((item, i) => {
    const {name, icon, url} = item;
    const navButton = getNavButton(name, icon);

    if(i == 0) {
      currentApp = item;
      openApp(i18n.tr(name), url, navButton);
    }

    navButton.off().on('click', e => {
      e.preventDefault();
      e.target.blur();
      if(openAppID == url) return;

      currentApp = item;
      openApp(i18n.tr(name), url, navButton);
    });

    $('#headerCenterPanel').append(navButton);
  });

  $('#localeSelector').off().on('change', async e => {
    const locale = $('#localeSelector').val();
    AS.OPTIONS.locale = locale;
    if(currentApp) {
      await updateTranslations();
      const iframe = $('#synergy-app-iframe');
      const appName = i18n.tr(currentApp.name);

      fire({type: 'change_label', text: localizedText(appName, appName, appName, appName)}, 'appNameLabel');

      let src = iframe.attr('src');
      src = src.substring(0, src.indexOf('locale'));
      src += `locale=${locale}`;

      iframe.attr('src', src);

      $('.synergy_nav_item_button').each((i, el) => {
        const appName = i18n.tr($(el).attr('defaultname'));
        $(el).attr('title', appName);
        $(el).find('.synergy_nav_item_text').text(appName);
      })
    }
  });

  if(!Cons.getAppStore().synergy_app_page_listener) {

    addListener('button_click', 'buttonLogOut', e => {
      synergyApps.forEach(item => {
        const {url} = item;
        deleteCookie(`constructor_creds_for_${url}`);
      });
      deleteCookie(`constructor_creds_for_SynergyApp`);
      deleteCookie(`constructor_image_api_key`);
      Cons.logout();
    });

    Cons.setAppStore({synergy_app_page_listener: true});
  }
});
