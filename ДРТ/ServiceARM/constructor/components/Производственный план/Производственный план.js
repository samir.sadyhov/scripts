const compContainer = $(`#${comp.code}`);
const container = compContainer.find('.prodplan_container');
const boardPanel = compContainer.find('.prodplan_board');
const infoPanel = compContainer.find('.prodplan_info');

compContainer.find('.jsx-parser').css('height', 'inherit');

const searchInRegistry = async param => {
  return new Promise(resolve => {
    rest.synergyGet(`api/registry/data_ext?${param}`, resolve, err => {
      console.log(`ProductionPlan ERROR [ searchInRegistry ]: ${JSON.stringify(err)}`);
      resolve(null);
    });
  });
}

const loadAsfData = async dataUUID => {
  return new Promise(resolve => {
    rest.synergyGet(`api/asforms/data/${dataUUID}?locale=${AS.OPTIONS.locale}`, resolve, err => {
      console.log(`ProductionPlan ERROR [ loadAsfData ]: ${JSON.stringify(err)}`);
      resolve(null);
    });
  });
}

const mergeFormData = async data => {
  return new Promise(async resolve => {
    try {
      const {login, password} = Cons.creds;
      const auth = "Basic " + btoa(unescape(encodeURIComponent(`${login}:${password}`)));
      const url = `../Synergy/rest/api/asforms/data/merge`;
      const response = await fetch(url, {
        method: 'POST',
        headers: {"Authorization": auth, "Content-Type": "application/json; charset=UTF-8"},
        body: JSON.stringify(data)
      });
      if(!response.ok) throw new Error(`HTTP: ${response.status}, message: ${response.statusText} [ ${response.text()} ]`);
      resolve(response.json());
    } catch (err) {
      console.log(`ERROR [ mergeFormData ]: ${JSON.stringify(err)}`);
      resolve(null);
    }
  });
}

const parseSettings = asfData => {
  if(!asfData || !asfData.hasOwnProperty('data')) return null;

  const result = {
    status: {
      plan: {code: ['0'], color: {good: '#71bc78', bad: '#ed760e'}},
      work: {code: ['1'], color: {good: '#b2ec5d', bad: '#d35339'}},
      finish: {code: ['2'], color: {good: '#57a639', bad: '#47d3bf'}}
    }
  };

  for(let i = 0; i < asfData.data.length; i++) {
    const item = asfData.data[i];
    if(item.type == 'label') continue;
    switch (item.id) {
      case 'service_form_prodPlanSettings_resource': result.resourceLinkCmp = item?.value || null; break;
      case 'service_form_prodPlanSettings_resourceGroup': result.resourceGroupCmp = item?.value || null; break;
      case 'service_form_prodPlanSettings_viewStatus': result.statusCmp = item?.value || null; break;

      case 'service_form_prodPlanSettings_prepared': result.status.plan.code = (item?.value || '').split(','); break;
      case 'service_form_prodPlanSettings_prepared_OK': result.status.plan.color.good = item?.value || '#71bc78'; break;
      case 'service_form_prodPlanSettings_prepared_notOK': result.status.plan.color.bad = item?.value || '#ed760e'; break;

      case 'service_form_prodPlanSettings_inWork': result.status.work.code = (item?.value || '').split(','); break;
      case 'service_form_prodPlanSettings_inWork_OK': result.status.work.color.good = item?.value || '#b2ec5d'; break;
      case 'service_form_prodPlanSettings_inWork_notOK': result.status.work.color.bad = item?.value || '#d35339'; break;

      case 'service_form_prodPlanSettings_done': result.status.finish.code = (item?.value || '').split(','); break;
      case 'service_form_prodPlanSettings_done_OK': result.status.finish.color.good = item?.value || '#57a639'; break;
      case 'service_form_prodPlanSettings_done_notOK': result.status.finish.color.bad = item?.value || '#57a639'; break;
    }
  }

  return result;
}

const getSettings = async () => {
  const res = await searchInRegistry($.param({
    registryCode: 'service_registry_prodPlanSettings',
    loadData: false,
    recordsCount: 1
  }));

  if(!res || res.count == 0) return null;

  const asfData = await loadAsfData(res.data[0].dataUUID);

  return parseSettings(asfData);
}

const getResource = async () => {
  let url = 'registryCode=service_registry_resources';
  url += '&fields=service_form_resources_name';
  url += '&fields=service_form_resources_group';

  const res = await searchInRegistry(url);

  if(!res || res.recordsCount == 0) return null;

  return res.result.map(x => {
    const {dataUUID, documentID, fieldKey, fieldValue} = x;
    return {
      dataUUID,
      documentID,
      group: {
        name: fieldValue?.service_form_resources_group || '',
        code: fieldKey?.service_form_resources_group || null
      },
      name: fieldValue?.service_form_resources_name || ''
    }
  });
}

const formatDate = datetime => {
  return datetime.getFullYear() + '-' +
    ('0' + (datetime.getMonth() + 1)).slice(-2) + '-' +
    ('0' + datetime.getDate()).slice(-2) + ' ' +
    ('0' + datetime.getHours()).slice(-2) + ':' +
    ('0' + datetime.getMinutes()).slice(-2) + ':' +
    ('0' + datetime.getSeconds()).slice(-2);
}

const customFormatDate = (datetime, time = true) => {
  datetime = datetime.split(/\D/);
  if(time) {
    return datetime[2] + '.' + datetime[1] + '.' + datetime[0] + ' ' + datetime[3] + ':' + datetime[4];
  } else {
    return datetime[2] + '.' + datetime[1] + '.' + datetime[0];
  }
}

const getMonthPeriod = date => {
  const startMonth = new Date(date.getFullYear(), date.getMonth() - 1, 1);
  const {monday: start} = getWeeStartStopDate(startMonth);
  const stop = new Date(date.getFullYear(), date.getMonth() + 2, 0, 23, 59, 59);

  return {start, stop};
}

const getWeekPeriod = date => {
  const {monday, sunday} = getWeeStartStopDate(date);
  const oldWeek = new Date(monday.getFullYear(), monday.getMonth(), monday.getDate() - 1);
  const nextWeek = new Date(sunday.getFullYear(), sunday.getMonth(), sunday.getDate() + 1);
  const {monday: start} = getWeeStartStopDate(oldWeek);
  const {sunday: stop} = getWeeStartStopDate(nextWeek);
  return {start, stop};
}

const getDayPeriod = date => {
  const start = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0);
  const stop = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 23, 59, 59);
  return {start, stop};
}

const getPeriod = (date, type) => {
  switch (type) {
    case 'month': return getMonthPeriod(date); break;
    case 'week': return getWeekPeriod(date); break;
    case 'day': return getDayPeriod(date); break;
    default: return getMonthPeriod(date);
  }
}

const getWeekNumber = date => {
  const now = date ? new Date(date) : new Date();
  now.setHours(0, 0, 0, 0);
  now.setDate(now.getDate() + 3 - (now.getDay() + 6) % 7);
  const week1 = new Date(now.getFullYear(), 0, 4);
  return 1 + Math.round(((now.getTime() - week1.getTime()) / 86400000 - 3 + (week1.getDay() + 6) % 7) / 7);
}

const getWeeStartStopDate = date => {
  const now = date ? new Date(date) : new Date();

  const monday = new Date(now);
  const day = monday.getDay() || 7;
  if(day !== 1) monday.setHours(-24 * (day - 1));

  const sunday = new Date(monday);
  sunday.setDate(sunday.getDate() - sunday.getDay() + 7);
  sunday.setHours(23,59,59,0);

  const week = getWeekNumber(now);

  return {week, monday, sunday};
}

const getPeriodInWeek = (week = 1, year = new Date().getFullYear()) => {
  const monday = new Date(year, 0, 7 * week);
  monday.setDate(monday.getDate() - (monday.getDay() || 7) + 1);

  const sunday = new Date(monday);
  sunday.setDate(sunday.getDate() - sunday.getDay() + 7);
  sunday.setHours(23,59,59,0);

  return {week, monday, sunday};
}

const ProductionPlan = {

  searchString: null,
  renderPeriodType: 'month',
  startPeriodDate: new Date(),

  formatWeekPeriod: function(weekPeriod) {
    const {week, monday, sunday} = weekPeriod;
    const start = `${('0' + monday.getDate()).slice(-2)}.${('0' + (monday.getMonth() + 1)).slice(-2)}`;
    const stop = `${('0' + sunday.getDate()).slice(-2)}.${('0' + (sunday.getMonth() + 1)).slice(-2)}`;
    return `${start} - ${stop}`;
  },

  getTableHeader: function(){
    const {start, stop} = this.period;
    const tHead = $('<thead>');
    const tr = $('<tr>');

    tr.append('<th>Ресурс / группа ресурсов</th>');

    if(this.renderPeriodType == 'month') {
      let weekNumber = getWeekNumber(start);
      let finishWeekNumber = getWeekNumber(stop);
      let year = start.getFullYear();

      while (true) {
        let weekPeriod = getPeriodInWeek(weekNumber, year);
        tr.append(`<th uk-tooltip="title: ${weekNumber} неделя; delay: 400">${this.formatWeekPeriod(weekPeriod)}</th>`);

        weekNumber++;
        if(weekNumber > 52) {
          weekNumber = 1;
          year = stop.getFullYear();
        }

        if(weekNumber == finishWeekNumber) {
          weekPeriod = getPeriodInWeek(weekNumber, year);
          tr.append(`<th uk-tooltip="title: ${weekNumber} неделя; delay: 400">${this.formatWeekPeriod(weekPeriod)}</th>`);
          break;
        }
      }
    } else if (this.renderPeriodType == 'week') {
      const tmpDate = new Date(start);
      const finishDate = new Date(stop.getFullYear(), stop.getMonth(), stop.getDate(), 0, 0, 0);
      while (true) {
        tr.append(`<th>${customFormatDate(formatDate(tmpDate), false)}</th>`);
        tmpDate.setDate(tmpDate.getDate() + 1);
        if(tmpDate.getTime() == finishDate.getTime()) {
          tr.append(`<th>${customFormatDate(formatDate(tmpDate), false)}</th>`);
          break;
        }
      }
    } else if (this.renderPeriodType == 'day') {
      const tmpDate = new Date(start);
      const finishDate = new Date(start.getFullYear(), start.getMonth(), start.getDate(), 23, 0, 0);
      while (true) {
        tr.append(`<th>${customFormatDate(formatDate(tmpDate))}</th>`);
        tmpDate.setHours(tmpDate.getHours() + 1);
        if(tmpDate.getTime() == finishDate.getTime()) {
          tr.append(`<th>${customFormatDate(formatDate(tmpDate))}</th>`);
          break;
        }
      }
    }

    tHead.append(tr);
    return tHead;
  },

  getAppPanel: function(app){
    const panel = $('<div>', {class: 'prodplan_app_container'});
    const titleField = this.appRegistryFields.find(x => x.type == 'title');
    const textFields = this.appRegistryFields.filter(x => x.type == 'text');
    const {dataUUID} = app;

    const cmpStartDate = this.cmpStartDate?.code;
    const cmpFinishDate = this.cmpFinishDate?.code;
    const cmpFactFinishDate = this.cmpFactFinishDate?.code;

    const bgColor = this.getStatusAppColor(app);

    panel.attr('data-uuid', dataUUID);
    panel.attr('draggable', true);

    panel.append(`<span class="prodplan_app_status" style="background: ${bgColor}"></span>`);

    panel.append(`<span class="prodplan_app_title">${titleField.title}: ${app[titleField.code].value}</span>`);

    textFields.forEach(item => {
      panel.append(`<span class="prodplan_app_text">${item.title}: ${app[item.code].value}</span>`);
    })

    panel.append(
      `<span class="prodplan_app_date">${this.cmpStartDate.title}: ${app[this.cmpStartDate.code].value}</span>`,
      `<span class="prodplan_app_date">${this.cmpFinishDate.title}: ${app[this.cmpFinishDate.code].value}</span>`
    );

    if(app[this.cmpFactFinishDate.code] && app[this.cmpFactFinishDate.code].hasOwnProperty('key')) {
      panel.append(
        `<span class="prodplan_app_date">${this.cmpFactFinishDate.title}: ${app[this.cmpFactFinishDate.code].value}</span>`
      );
    }

    panel.on('click', e => {
      openFormPlayer(dataUUID, null, async () => {
        Cons.showLoader();
        this.apps = await this.getApplications();
        await this.renderBoard();
        Cons.hideLoader();
      });
    }).on("dragstart", event => {
      event.originalEvent.dataTransfer.setData("dataUUID", dataUUID);
    });

    return panel;
  },

  renderPanelApps: function(filterApps){
    if(filterApps.length) {
      const appContentContainer = infoPanel.find('.prodplan_info_data');
      appContentContainer.empty();

      filterApps.forEach(app => {
        appContentContainer.append(this.getAppPanel(app));
      });

      container.addClass('open');
    } else {
      container.removeClass('open');
    }

    infoPanel.find('.uk-icon').on('click', e => {
      boardPanel.find('.td_block_data').removeClass('select');
      container.removeClass('open');
    });
  },

  getFilterApps: function(apps, groupCode, isGroup, resourceDocumentID, monday, sunday){
    const {resourceGroupCmp, resourceLinkCmp} = this.settings;

    const filterApps = [];
    let countApp = 0;

    apps.forEach(app => {
      const appStartDate = new Date(app[this.cmpStartDate.code].key).getTime();
      const appFinishDate = new Date(app[this.cmpFinishDate.code].key).getTime();
      const appResource = app[resourceLinkCmp];
      const appGroup = app[resourceGroupCmp];

      if(isGroup) {
        if((appStartDate < sunday.getTime() && monday.getTime() < appFinishDate) && appGroup?.key == groupCode) {
          filterApps.push(app);
          countApp++;
        }
      } else {
        if((appStartDate < sunday.getTime() && monday.getTime() < appFinishDate) && appGroup?.key == groupCode && appResource?.key == resourceDocumentID) {
          filterApps.push(app);
          countApp++;
        }
      }
    });
    return {filterApps, countApp};
  },

  getStatusAppColor: function(app){
    const currentDate = new Date();
    const {statusCmp, status} = this.settings;
    const appStatus = app[statusCmp];
    const appStartDate = new Date(app[this.cmpStartDate.code].key);
    const appFinishDate = new Date(app[this.cmpFinishDate.code].key);
    const appFactFinishDate = app[this.cmpFactFinishDate.code] && app[this.cmpFactFinishDate.code].hasOwnProperty('key') ? new Date(app[this.cmpFactFinishDate.code].key) : currentDate;

    if (status.work.code.includes(appStatus.key) && appFinishDate < currentDate) {
      return status.work.color.bad;
    } else if (status.plan.code.includes(appStatus.key) && appStartDate < currentDate) {
      return status.plan.color.bad;
    } else if (status.work.code.includes(appStatus.key) && appFinishDate > currentDate) {
      return status.work.color.good;
    } else if (status.plan.code.includes(appStatus.key) && appStartDate > currentDate) {
      return status.plan.color.good;
    } else if(status.finish.code.includes(appStatus.key) && appFactFinishDate > appFinishDate) {
      return status.finish.color.bad;
    } else if(status.finish.code.includes(appStatus.key) && appFactFinishDate <= appFinishDate) {
      return status.finish.color.good;
    } else {
      return '#fff';
    }
  },

  getStatusColor: function(apps){
    const currentDate = new Date();
    const {statusCmp, status} = this.settings;
    const s = [];

    for(let i = 0; i < apps.length; i++) {
      const app = apps[i];
      const appStatus = app[statusCmp];
      const appStartDate = new Date(app[this.cmpStartDate.code].key);
      const appFinishDate = new Date(app[this.cmpFinishDate.code].key);
      const appFactFinishDate = app[this.cmpFactFinishDate.code] && app[this.cmpFactFinishDate.code].hasOwnProperty('key') ? new Date(app[this.cmpFactFinishDate.code].key) : currentDate;

      if (status.work.code.includes(appStatus.key) && appFinishDate < currentDate) {
        s.push(1);
      } else if (status.plan.code.includes(appStatus.key) && appStartDate < currentDate) {
        s.push(2);
      } else if (status.work.code.includes(appStatus.key) && appFinishDate > currentDate) {
        s.push(3);
      } else if (status.plan.code.includes(appStatus.key) && appStartDate > currentDate) {
        s.push(4);
      } else if(status.finish.code.includes(appStatus.key) && appFactFinishDate > appFinishDate) {
        s.push(5);
      } else if(status.finish.code.includes(appStatus.key) && appFactFinishDate <= appFinishDate) {
        s.push(6);
      }
    }

    if(s.includes(1)) {
      return status.work.color.bad;
    } else if (s.includes(2)) {
      return status.plan.color.bad;
    } else if (s.includes(3)) {
      return status.work.color.good;
    } else if (s.includes(4)) {
      return status.plan.color.good;
    } else if (s.includes(5)) {
      return status.finish.color.bad;
    } else if (s.includes(6)) {
      return status.finish.color.good;
    } else {
      return "#fff";
    }

    return bgColor;
  },

  renderRowsData: function(param){
    const {tr, apps, groupCode, isGroup = false, resourceDocumentID} = param;
    const cmpStartDate = this.cmpStartDate?.code;
    const cmpFinishDate = this.cmpFinishDate?.code;
    const cmpFactFinishDate = this.cmpFactFinishDate?.code;
    const {resourceGroupCmp, resourceLinkCmp, status, statusCmp} = this.settings;
    const {start, stop} = this.period;
    const currentDate = new Date();

    function setAppData(td, monday, sunday, me){
      let {filterApps, countApp} = me.getFilterApps(apps, groupCode, isGroup, resourceDocumentID, monday, sunday);

      td.css({'background': me.getStatusColor(filterApps)});
      td.attr('start-date', formatDate(monday));
      td.attr('stop-date', formatDate(sunday));
      td.text(countApp);
      td.addClass('td_block_data');
      if(resourceDocumentID) td.attr('resourceDocumentID', resourceDocumentID);

      td.on('click', e => {
        boardPanel.find('.td_block_data').removeClass('select');
        td.addClass('select');
        me.renderPanelApps(filterApps);
      }).on('drop', async event => {
        event.preventDefault();
        if(!resourceDocumentID) return;

        const dataUUID = event.originalEvent.dataTransfer.getData("dataUUID");
        const app = me.apps.find(x => x.dataUUID == dataUUID);
        const appStatus = app[statusCmp];
        const resource = me.resource.find(x => x.documentID == resourceDocumentID);

        if(status.finish.code.includes(appStatus.key)) return;

        const newAppAsfData = [];
        const dStart = td.attr('start-date');
        const dStop = td.attr('stop-date');

        newAppAsfData.push({
          id: resourceLinkCmp,
          type: 'reglink',
          value: resource.name,
          key: resource.documentID,
          valueID: resource.documentID
        });

        newAppAsfData.push({
          id: resourceGroupCmp,
          type: 'listbox',
          value: resource.group.name,
          key: resource.group.code
        });

        newAppAsfData.push({
          id: cmpStartDate,
          type: 'date',
          value: customFormatDate(dStart),
          key: dStart
        });

        newAppAsfData.push({
          id: cmpFinishDate,
          type: 'date',
          value: customFormatDate(dStop),
          key: dStop
        });

        Cons.showLoader();

        const mergeResult = await mergeFormData({
          uuid: dataUUID,
          data: newAppAsfData
        });

        if(mergeResult) {
          me.apps = await me.getApplications();
          await me.renderBoard();
          Cons.hideLoader();
        } else {
          Cons.hideLoader();
          showMessage("Произошла ошибка при изменении данных", 'error');
        }

      }).on('dragover', event => {
        event.preventDefault();
      });
    }

    if(this.renderPeriodType == 'month') {

      let weekNumber = getWeekNumber(start);
      let finishWeekNumber = getWeekNumber(stop);
      let year = start.getFullYear();

      while (true) {
        let weekPeriod = getPeriodInWeek(weekNumber, year);
        let {monday, sunday} = weekPeriod;
        let td = $('<td>');
        setAppData(td, monday, sunday, this);
        tr.append(td);

        weekNumber++;
        if(weekNumber > 52) {
          weekNumber = 1;
          year = this.period.stop.getFullYear();
        }

        if(weekNumber == finishWeekNumber) {
          weekPeriod = getPeriodInWeek(weekNumber, year);
          monday = weekPeriod.monday;
          sunday = weekPeriod.sunday;
          let tdLast = $('<td>');
          setAppData(tdLast, monday, sunday, this);
          tr.append(tdLast);
          break;
        }
      }

    } else if (this.renderPeriodType == 'week') {

      const tmpStartDate = new Date(start);
      const tmpStopDate = new Date(tmpStartDate.getFullYear(), tmpStartDate.getMonth(), tmpStartDate.getDate(), 23, 59, 59);
      const finishDate = new Date(stop.getFullYear(), stop.getMonth(), stop.getDate(), 0, 0, 0);

      while (true) {
        const td = $('<td>');
        setAppData(td, tmpStartDate, tmpStopDate, this);
        tr.append(td);

        tmpStartDate.setDate(tmpStartDate.getDate() + 1);
        tmpStopDate.setDate(tmpStopDate.getDate() + 1);

        if(tmpStartDate.getTime() == finishDate.getTime()) {
          const tdLast = $('<td>');
          setAppData(tdLast, tmpStartDate, tmpStopDate, this);
          tr.append(tdLast);
          break;
        }
      }

    } else if (this.renderPeriodType == 'day') {

      const tmpStartDate = new Date(start);
      const tmpStopDate = new Date(tmpStartDate);
      const finishDate = new Date(start.getFullYear(), start.getMonth(), start.getDate(), 23, 0, 0);
      tmpStopDate.setHours(tmpStartDate.getHours() + 1);

      while (true) {
        const td = $('<td>');
        setAppData(td, tmpStartDate, tmpStopDate, this);
        tr.append(td);

        tmpStartDate.setHours(tmpStartDate.getHours() + 1);
        tmpStopDate.setHours(tmpStartDate.getHours() + 1);

        if(tmpStartDate.getTime() == finishDate.getTime()) {
          const td = $('<td>');
          setAppData(td, tmpStartDate, tmpStopDate, this);
          tr.append(td);
          break;
        }
      }
    }

  },

  renderRows: function(param){
    const {groupCode, groupName, tBody} = param;
    const {resourceGroupCmp} = this.settings;
    const groupApps = this.apps.filter(x => x[resourceGroupCmp].key == groupCode);
    const groupResource = this.resource.filter(x => x.group.code == groupCode);

    const trGroup = $('<tr>');
    tBody.append(trGroup);

    trGroup.append(`<td uk-tooltip="title: ${groupName}; delay: 400" class="td_group">${groupName}</td>`);

    this.renderRowsData({tr: trGroup, apps: groupApps, groupCode, isGroup: true});

    groupResource.forEach(resource => {
      const trResource = $('<tr>');
      tBody.append(trResource);
      trResource.append(`<td uk-tooltip="title: ${resource.name}; delay: 400">${resource.name}</td>`);
      this.renderRowsData({tr: trResource, apps: groupApps, groupCode, resourceDocumentID: resource.documentID});
    });
  },

  renderBoard: async function(){
    const table = $('<table>', {class: 'uk-table uk-table-small'});
    const tHead = this.getTableHeader();
    const tBody = $('<tbody>');

    table.append(tHead, tBody);
    boardPanel.empty().append(table);

    for(const groupCode in this.groups) {
      const groupName = this.groups[groupCode];
      this.renderRows({groupCode, groupName, tBody});
    }
  },

  parseAppsData: function(promisesResult) {
    const result = [];

    if(!promisesResult || !promisesResult.length) return result;

    for(let i = 0; i < promisesResult.length; i++) {
      const data = promisesResult[i];
      if(data.recordsCount == 0) continue;

      for(let j = 0; j < data.result.length; j++) {
        const {dataUUID, documentID, registryID, fieldKey, fieldValue} = data.result[j];
        const tmp = {dataUUID, documentID, registryID};
        for(let key in fieldValue) {
          tmp[key] = {value: fieldValue[key]}
          if(fieldKey.hasOwnProperty(key)) tmp[key].key = fieldKey[key];
        }
        result.push(tmp);
      }
    }

    return result;
  },

  getApplications: async function() {
    try {
      const d = this.renderPeriodType == 'month' ? this.startPeriodDate : this.period.start;
      const {start, stop} = getPeriod(d, 'month');

      const promises = this.appRegistryCodes.map(registryCode => {
        let url = `registryCode=${registryCode}`;
        url += `&field=${this.cmpStartDate.code}&condition=MORE_OR_EQUALS&key=${formatDate(start)}`;
        url += `&field1=${this.cmpStartDate.code}&condition1=LESS_OR_EQUALS&key1=${formatDate(stop)}`;
        if(this.searchString) url += `&searchString=${this.searchString}`;
        this.appRegistryFields.forEach(x => url += `&fields=${x.code}`);
        url += `&fields=${this.settings.resourceGroupCmp}`;
        url += `&fields=${this.settings.resourceLinkCmp}`;
        return searchInRegistry(url);
      });

      const searchResult = await Promise.all(promises);

      return this.parseAppsData(searchResult);
    } catch (e) {
      console.log(e);
    }
  },

  initListeners: function(){
    const buttonRefresh = container.find('#button_refresh');
    const buttonReset = container.find('#button_reset');
    const buttonDay = container.find('#button_day');
    const buttonWeek = container.find('#button_week');
    const buttonMonth = container.find('#button_month');
    const buttonPrev = container.find('#button_prev');
    const buttonNext = container.find('#button_next');
    const searchInput = container.find('#search_input');
    const searchButton = container.find('#search_button');

    function clear(){
      boardPanel.find('.td_block_data').removeClass('select');
      container.removeClass('open');
    }

    async function render(me){
      Cons.showLoader();
      me.startPeriodDate = new Date();
      me.period = getPeriod(me.startPeriodDate, me.renderPeriodType);
      me.apps = await me.getApplications();
      await me.renderBoard();
      Cons.hideLoader();
    }

    buttonRefresh.on('click', async e => {
      e.preventDefault();
      e.target.blur();
      Cons.showLoader();
      clear();
      this.apps = await this.getApplications();
      await this.renderBoard();
      Cons.hideLoader();
    });

    buttonReset.on('click', async e => {
      e.preventDefault();
      e.target.blur();

      clear();
      buttonDay.removeClass('select');
      buttonWeek.removeClass('select');
      buttonMonth.addClass('select');
      searchInput.val(null);
      this.searchString = null;
      this.renderPeriodType = 'month';
      render(this);
    });

    buttonMonth.on('click', async e => {
      e.preventDefault();
      e.target.blur();
      if(buttonMonth.hasClass('select')) return;

      clear();
      this.renderPeriodType = 'month';
      buttonDay.removeClass('select');
      buttonWeek.removeClass('select');
      buttonMonth.addClass('select');
      render(this);
    });

    buttonWeek.on('click', async e => {
      e.preventDefault();
      e.target.blur();
      if(buttonWeek.hasClass('select')) return;

      clear();
      this.renderPeriodType = 'week';
      buttonDay.removeClass('select');
      buttonWeek.addClass('select');
      buttonMonth.removeClass('select');
      render(this);
    });

    buttonDay.on('click', async e => {
      e.preventDefault();
      e.target.blur();
      if(buttonDay.hasClass('select')) return;

      clear();
      this.renderPeriodType = 'day';
      buttonDay.addClass('select');
      buttonWeek.removeClass('select');
      buttonMonth.removeClass('select');
      render(this);
    });

    searchInput.off().on('keyup', async e => {
      if (e.keyCode === 13) {
        e.preventDefault();
        Cons.showLoader();
        clear();
        this.searchString = searchInput.val() || null;
        this.apps = await this.getApplications();
        await this.renderBoard();
        Cons.hideLoader();
      }
    });

    searchButton.on('click', async e => {
      e.preventDefault();
      e.target.blur();
      Cons.showLoader();
      clear();
      this.searchString = searchInput.val() || null;
      this.apps = await this.getApplications();
      await this.renderBoard();
      Cons.hideLoader();
    });

    buttonNext.on('click', async e => {
      e.preventDefault();
      e.target.blur();
      Cons.showLoader();
      clear();

      switch (this.renderPeriodType) {
        case 'month':
          this.startPeriodDate.setMonth(this.startPeriodDate.getMonth() + 1);
          break;
        case 'week':
          const week = getWeeStartStopDate(this.startPeriodDate);
          week.sunday.setDate(week.sunday.getDate() + 1);
          this.startPeriodDate = new Date(week.sunday);
          break;
        case 'day':
          this.startPeriodDate.setDate(this.startPeriodDate.getDate() + 1);
          break;
      }

      this.period = getPeriod(this.startPeriodDate, this.renderPeriodType);
      this.apps = await this.getApplications();
      await this.renderBoard();

      Cons.hideLoader();
    });

    buttonPrev.on('click', async e => {
      e.preventDefault();
      e.target.blur();
      Cons.showLoader();
      clear();

      switch (this.renderPeriodType) {
        case 'month':
          this.startPeriodDate.setMonth(this.startPeriodDate.getMonth() - 1);
          break;
        case 'week':
          const week = getWeeStartStopDate(this.startPeriodDate);
          week.monday.setDate(week.monday.getDate() - 1);
          this.startPeriodDate = new Date(week.monday);
          break;
        case 'day':
          this.startPeriodDate.setDate(this.startPeriodDate.getDate() - 1);
          break;
      }

      this.period = getPeriod(this.startPeriodDate, this.renderPeriodType);
      this.apps = await this.getApplications();
      await this.renderBoard();

      Cons.hideLoader();
    });

  },

  init: async function(params) {
    Cons.showLoader();
    try {
      const {appRegistryCodes, appRegistryFields} = params;

      if(!appRegistryCodes) throw new Error(`Не передан параметр appRegistryCodes`);
      if(!appRegistryFields) throw new Error(`Не передан параметр appRegistryFields`);

      this.settings = await getSettings();
      if(!this.settings) throw new Error(`Не найдены настройки производственного плана`);

      this.resource = await getResource();
      if(!this.resource) throw new Error(`Не найдены записи в реестре ресурсов`);

      this.cmpStartDate = appRegistryFields.find(x => x.type == 'start_date');
      this.cmpFinishDate = appRegistryFields.find(x => x.type == 'finish_date');
      this.cmpFactFinishDate = appRegistryFields.find(x => x.type == 'finish_fact_date');

      if(!this.cmpStartDate) throw new Error(`Не передано поле с типом start_date`);
      if(!this.cmpFinishDate) throw new Error(`Не передано поле с типом finish_date`);
      if(!this.cmpFactFinishDate) throw new Error(`Не передано поле с типом finish_fact_date`);

      this.groups = {};

      this.resource.forEach(x => this.groups[x.group.code] = x.group.name);

      this.appRegistryCodes = appRegistryCodes;
      this.appRegistryFields = appRegistryFields;

      this.period = getPeriod(this.startPeriodDate, this.renderPeriodType);

      this.apps = await this.getApplications();

      await this.renderBoard();

      this.initListeners();

      Cons.hideLoader();
    } catch (err) {
      Cons.hideLoader();
      console.log('ERROR init Production Plan', err);
      showMessage(err.message, 'error');
    }
  }

}

compContainer.off()
.on('renderProductionPlan', e => {
  if(!e.hasOwnProperty('eventParam')) return;
  ProductionPlan.init(e.eventParam);
});
