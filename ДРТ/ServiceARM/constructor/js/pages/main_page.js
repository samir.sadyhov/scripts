pageHandler('main_page', () => {

  const eventParam = {
    appRegistryCodes: ['service_registry_resourceOrders'], //список кодов реестров
    appRegistryFields: [
      {title: 'Номер', code: 'service_form_resourceOrder_num', type: 'title'}, //заголовок, кликабельный
      {title: 'Статус', code: 'service_form_resourceOrder_status', type: 'text'}, //типа описание
      {title: 'Дата создания', code: 'service_form_resourceOrder_crDate', type: 'create_date'}, //дата создания
      {title: 'Дата начала', code: 'service_form_resourceOrder_start', type: 'start_date'}, //дата начала работ, обязательное поле, по нему ищутся записи в реестре
      {title: 'План', code: 'service_form_resourceOrder_finish', type: 'finish_date'}, //дата завершения, обязательное поле
      {title: 'Факт', code: 'service_form_resourceOrder_finish_fact', type: 'finish_fact_date'} //дата файктического завершения, обязательное поле
    ]
  };

  //инициация компонента "Производственный план"
  $('#productionPlan').trigger({
    type: 'renderProductionPlan',
    eventParam: eventParam
  });

});
