pageHandler('auth_page', () => {
  if (!Cons.getAppStore().auth_page_listener) {
    addListener('auth_success', 'buttonLogin', authed => {
      const {login, password} = authed.creds;

      Cons.creds.login = login;
      Cons.creds.password = password;
      AS.apiAuth.setCredentials(login, password);
      AS.OPTIONS.login = login;
      AS.OPTIONS.password = password;
      AS.OPTIONS.currentUser = authed.data.person;
    });

    Cons.setAppStore({auth_page_listener: true});
  }
});
