const getServerTime = async () => {
  let res = await fetch(`${window.origin}/server/api/time/getServerTime`);
  return await res.text();
}

const checkDate = value => {
  if(!AS.FORMS.DateUtils.parseDate(value)) return;
  getServerTime().then(serverTime => {
    if(value > serverTime) {
      model.setValue(null);
      view.markInvalid();
      model.trigger(AS.FORMS.EVENT_TYPE.markInvalid);
      alert(`Вы не можете указать дату больше текущей даты: ${serverTime}`);
    }
  });
}

if(editable) {
  if(model.customChange) return;
  model.on('valueChange', (_1, _2, value) => checkDate(value));
  model.customChange = true;
}
