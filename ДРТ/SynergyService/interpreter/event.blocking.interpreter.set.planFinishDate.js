var result = true;
var message = "ok";

function customFormatDate(datetime){
  datetime = datetime.split(/\D/);
  return datetime[2] + '.' + datetime[1] + '.' + datetime[0] + ' ' + datetime[3] + ':' + datetime[4];
}

try {

  let currentFormData = API.getFormData(dataUUID);

  let timemode = UTILS.getValue(currentFormData, 'service_form_order_timemode');
  let orderDate = UTILS.getValue(currentFormData, 'service_form_order_date');
  let duration = UTILS.getValue(currentFormData, 'service_form_order_duration');
  let planFinishDate = null;


  if(!timemode || !timemode.hasOwnProperty('key')) throw new Error('не выбран режим обслуживания');
  if(!orderDate || !orderDate.hasOwnProperty('key')) throw new Error('не заполнена дата заявки');

  if(!duration || !duration.hasOwnProperty('key')) {
    duration = 8 * 60;
  } else {
    duration = Math.round(duration.key * 60);
  }

  if(timemode.key == '1') {
    planFinishDate = API.getFinishDate(orderDate.key, duration);
    if(planFinishDate && planFinishDate.errorCode == '0') {
      planFinishDate = planFinishDate.data;
    } else {
      planFinishDate = null;
    }
  }

  if(timemode.key == '2') {
    planFinishDate = UTILS.parseDateTime(orderDate.key);
    planFinishDate.setMinutes(planFinishDate.getMinutes() + duration);
    planFinishDate = UTILS.formatDate(planFinishDate, true);
  }

  if(!planFinishDate) throw new Error('Ошибка рассчета планового срока исполнения');

  let asfData = [];
  asfData.push({
    "id": "service_form_order_planFinishDate",
    "type": "date",
    "value": customFormatDate(planFinishDate),
    "key": planFinishDate
  });

  let saveResult = API.mergeFormData({uuid: dataUUID, data: asfData});

  if(saveResult && saveResult.errorCode == '0') {
    message = 'Плановый срок исполнения: ' + planFinishDate;
  } else {
    message = 'Произошла ошибка сохранения';
  }

} catch (err) {
  log.error(err.message);
  message = err.message;
}
