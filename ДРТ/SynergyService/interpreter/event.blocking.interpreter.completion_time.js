function filteredProcesses(processes, formCode) {
  let filtered = [];

  function search(p) {
    p.forEach(function(process) {
      if (process.completionFormCode == formCode && process.finished) filtered.push(process);
      if (process.subProcesses.length > 0) search(process.subProcesses);
    });
  }
  search(processes);

  return filtered.sort(function(a, b) {
    return UTILS.parseDateTime(b.finished) - UTILS.parseDateTime(a.finished);
  }).map(function(item) {
    return {actionID: item.actionID, finished: item.finished};
  });
}

function customFormatDate(datetime){
  datetime = datetime.split(/\D/);
  return datetime[2] + '.' + datetime[1] + '.' + datetime[0] + ' ' + datetime[3] + ':' + datetime[4];
}

var result = true;
var message = "ok";

try {
  let formCodeCompletion = 'work_completion_form_orders'; // код формы завершения

  let processes = API.getProcesses(documentID);
  processes = filteredProcesses(processes, formCodeCompletion);

  let completionFormDataUUID = processes.map(function(item) {
    return API.getWorkCompletionData(item.actionID).result.dataUUID;
  }).filter(function(dataUUID){ return dataUUID != null})[0];

  if(!completionFormDataUUID) throw new Error('Не найдена форма завершения');

  let currentFormData = API.getFormData(dataUUID);
  let completionFormData = API.getFormData(completionFormDataUUID);

  let timemode = UTILS.getValue(currentFormData, 'service_form_order_timemode');
  let orderDate = UTILS.getValue(currentFormData, 'service_form_order_date');
  let planFinishDate = UTILS.getValue(currentFormData, 'service_form_order_planFinishDate');
  let factFinishDate = UTILS.getValue(completionFormData, 'service_form_completion_factFinishDate');
  let spenttime = null;
  let asfData = [];

  if(!factFinishDate || !factFinishDate.hasOwnProperty('key')) throw new Error('Не удалось получить фактическое время завершения с ФЗ');
  if(!timemode || !timemode.hasOwnProperty('key')) throw new Error('не выбран режим обслуживания');

  asfData.push({
    "id": "service_form_order_factFinishDate",
    "type": "date",
    "value": customFormatDate(factFinishDate.key),
    "key": factFinishDate.key
  });

  if(planFinishDate && planFinishDate.hasOwnProperty('key') && factFinishDate.key > planFinishDate.key) {
    asfData.push({
      "id": "service_form_order_overdue",
      "type": "listbox",
      "value": 'Да',
      "key": '1'
    });
  }

  if(timemode.key == '1') {
    spenttime = API.getWorkTime(orderDate.key, factFinishDate.key);
    if(spenttime && spenttime.errorCode == '0') {
      spenttime = Math.round((spenttime.data.milliseconds / (60 * 60 * 1000)) * 100) / 100;
    } else {
      spenttime = null;
    }
  }

  if(timemode.key == '2') {
    spenttime = new Date(factFinishDate.key) - new Date(orderDate.key);
    spenttime = Math.round((spenttime / (60 * 60 * 1000)) * 100) / 100;
  }

  if(spenttime) {
    asfData.push({
      "id": "service_form_order_spenttime",
      "type": "numericinput",
      "value": String(spenttime),
      "key": String(spenttime)
    });
  }

  let saveResult = API.mergeFormData({uuid: dataUUID, data: asfData});
  if(saveResult && saveResult.errorCode == '0') {
    message = 'Время, затраченное на исполнение: ' + spenttime;
  } else {
    message = 'Произошла ошибка сохранения';
  }

} catch (err) {
  log.error(err.message);
  message = err.message;
}
