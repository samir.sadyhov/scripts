var result = true;
var message = "ok";
let debug = false;

function customFormatDate(datetime){
  datetime = datetime.split(/\D/);
  return datetime[2] + '.' + datetime[1] + '.' + datetime[0] + ' ' + datetime[3] + ':' + datetime[4];
}

//суббота или воскресенье
function isDayOff(datetime) {
  if(typeof datetime == 'string') datetime = UTILS.parseDateTime(datetime);
  return (datetime.getDay() || 7) > 5 ? true : false;
}

function dateAddMonth(startPeriod, months) {
  let date = UTILS.parseDateTime(startPeriod);
  date.setMonth(date.getMonth() + (months || 1));
  return {
    start: startPeriod.split(' ')[0],
    stop: UTILS.formatDate(date, false)
  };
}

/*
  получение Нерабочих дней по календарю синержи без учета выходных
  @param calendar - календарь синержи
*/
function getHolidays(calendar) {
  let holidays = [];
  for (let date in calendar)
  if (calendar[date][0].type == 'holiday' && !isDayOff(date)) holidays.push(date);
  return holidays;
}

function parseAsfTable(table) {
  if(!table || !table.hasOwnProperty('data')) return null;

  let result = [];
  let hour_start = true;
  let tbi = 1;

  do {
    hour_start = UTILS.getValue(table, 'hour_start-b' + tbi);
    if(hour_start) {
      result.push({
        hour_start: Number(hour_start.value),
        minute_start: Number(UTILS.getValue(table, 'minute_start-b' + tbi).value),
        hour_finish: Number(UTILS.getValue(table, 'hour_finish-b' + tbi).value),
        minute_finish: Number(UTILS.getValue(table, 'minute_finish-b' + tbi).value)
      });
      tbi++;
    }
  } while (hour_start);

  return result;
}

function parseOperationMode(asfData){
  return {
    holidaysOn: UTILS.getValue(asfData, 'service_form_operation_mode_holidays_on').values[0] || '0',
    weekdays: parseAsfTable(UTILS.getValue(asfData, 'service_form_operation_mode_weekdays')),
    weekends: parseAsfTable(UTILS.getValue(asfData, 'service_form_operation_mode_weekends')),
    holidays: parseAsfTable(UTILS.getValue(asfData, 'service_form_operation_mode_holidays'))
  }
}

function searchOperationMode(id){
  let urlSearch = 'rest/api/registry/data_ext?registryCode=service_registry_operation_mode';
  urlSearch += '&field=service_form_operation_mode_number&condition=TEXT_EQUALS&value=' + id;
  urlSearch += '&countInPart=1&loadData=false';
  return API.httpGetMethod(urlSearch);
}

function getOperationMode(id){
  let searchResult = searchOperationMode(id);
  if(searchResult && searchResult.count > 0) {
    return parseOperationMode(API.getFormData(searchResult.data[0].dataUUID));
  } else {
    searchResult = searchOperationMode(1);
    if(searchResult && searchResult.count > 0) {
      return parseOperationMode(API.getFormData(searchResult.data[0].dataUUID));
    } else {
      return null;
    }
  }
}

function getStartWorkPeriod(operationMode, type) {
  return {
    h: operationMode[type][0].hour_start, //начало периода часы
    m: operationMode[type][0].minute_start //начало периода минуты
  }
}
function getStopWorkPeriod(operationMode, type) {
  return {
    h: operationMode[type][operationMode[type].length - 1].hour_finish, //часы
    m: operationMode[type][operationMode[type].length - 1].minute_finish //минуты
  }
}

function appendMinutes(startDate, minutes, timemode) {
  let operationMode = getOperationMode(timemode); //режим работы (данные с записи реестра)
  if(!operationMode) return null;

  let calendarPeriod = dateAddMonth(startDate); //берем период 1 месяц с переданной даты
  let synergyCalendar = API.getSynergyCalendar(calendarPeriod.start, calendarPeriod.stop); //загружаем календарь синержи
  let holidays = getHolidays(synergyCalendar); //праздничные отмеченные в календаре синержи
  let loop = true; //служебная переменная для цикла

  startDate = UTILS.parseDateTime(startDate);

  //полученние типа рабочего графика
  function getOperationModeType(nextDay) {
    let newDate = new Date(startDate);
    if(nextDay) newDate.setDate(newDate.getDate() + 1);
    if(isDayOff(newDate) && operationMode.weekends) { //если выходной и есть график на выходной
      //если праздничный и есть график на праздники
      if (holidays.indexOf(UTILS.formatDate(newDate, false)) != -1 && operationMode.holidaysOn == 1 && operationMode.holidays) {
        return 'holidays'; //праздничный
      } else {
        return 'weekends'; //выходной
      }
    } else if (holidays.indexOf(UTILS.formatDate(newDate, false)) != -1 && operationMode.holidaysOn == 1 && operationMode.holidays) {
      return 'holidays'; //праздничный
    } else {
      //если праздничный и есть график на праздники
      if (holidays.indexOf(UTILS.formatDate(newDate, false)) != -1 && operationMode.holidaysOn == 1 && operationMode.holidays) {
        return 'holidays'; //праздничный
      } else {
        return 'weekdays'; //обычный день
      }
    }
  }

  //проверяем праздничного/выходного с добавлением этого промежутка к дате
  function chekDay() {
    let operationModeType = getOperationModeType();
    if(!operationMode[operationModeType]) return;

    let pStart = getStartWorkPeriod(operationMode, operationModeType); //начало дня
    let m = startDate.getMinutes();

    //проверяем если день выходной и нет графика на выходные, то прибавляем день
    if(isDayOff(startDate) && !operationMode.weekends) {
      startDate.setDate(startDate.getDate() + 1);
      startDate.setHours(pStart.h, pStart.m);
      startDate.setMinutes(startDate.getMinutes() + m);
      return true;
    }

    //проверяем если праздничный и не стоит отметка о рабте в праздничный, то прибавляем день
    if(holidays.indexOf(UTILS.formatDate(startDate, false)) != -1 && operationMode.holidaysOn == 0) {
      startDate.setDate(startDate.getDate() + 1);
      startDate.setHours(pStart.h, pStart.m);
      startDate.setMinutes(startDate.getMinutes() + m);
      return true;
    }

    return false;
  }

  //проверка окончания рабочего времени, перевод на следующий день
  function chekDayEnd(addStartMinutes) {
    let operationModeType = getOperationModeType();
    if(!operationMode[operationModeType]) return;

    let h = startDate.getHours();
    let m = startDate.getMinutes();

    let pStart = getStartWorkPeriod(operationMode, operationModeType); //начало дня
    let pStop = getStopWorkPeriod(operationMode, operationModeType); //конец дня

    // для начала проверяем не выходит ли время за конец рабочего периода
    // если да то прибавляем день и проверяем на праздничные и выходные
    if(h > pStop.h || (h == pStop.h && m > pStop.m)) {
      operationModeType = getOperationModeType(true);
      pStart = getStartWorkPeriod(operationMode, operationModeType); //начало следующего дня

      startDate.setDate(startDate.getDate() + 1);
      startDate.setHours(pStart.h, pStart.m);
      if(addStartMinutes) startDate.setMinutes(startDate.getMinutes() + m);

      //проверяем на праздничные и выходные
      loop = true;
      do loop = chekDay(); while (loop);
    }
  }

  //проверка начала рабочего времени
  function chekDayStart() {
    let operationModeType = getOperationModeType();
    if(!operationMode[operationModeType]) return;

    let h = startDate.getHours();
    let m = startDate.getMinutes();

    let pStart = getStartWorkPeriod(operationMode, operationModeType); //начало дня

    // проверяем если время раньше начала периода
    // то устанавливаем соответствующее время
    if(h < pStart.h) startDate.setHours(pStart.h, pStart.m);
  }

  // проверка перерыва
  function isLunchBreak(){
    let operationModeType = getOperationModeType();
    if(!operationMode[operationModeType] || !operationMode[operationModeType].length) return false;

    let h = startDate.getHours();
    let m = startDate.getMinutes();
    let startDateMinutes = h * 60 + m;

    let lStart = null;
    let lStop = null;
    let lStartM = 0;
    let lStopM = 0;

    for(let i = 0; i < operationMode[operationModeType].length; i++) {
      lStart = operationMode[operationModeType][i];
      lStop = operationMode[operationModeType][i + 1] || null;
      if(lStop) {
        lStartM = lStart.hour_finish * 60 + lStart.minute_finish;
        lStopM = lStop.hour_start * 60 + lStop.minute_start;
      }
      if(startDateMinutes > lStartM && startDateMinutes < lStopM) break;
    }

    if(lStart && lStop) {
      let rm = lStopM - startDateMinutes;
      if(startDateMinutes > lStartM) {
        rm += (startDateMinutes - lStartM);
      }
      return {
        minutes: rm,
        p: lStop
      }
    } else {
      return false;
    }
  }

  // для начала проверяем текущий день, вдруг сегодня праздник, и на праздники нет графика (holidaysOn == 0)
  // значит дату начала ставить на начало по графику
  // проверяем в цикле пока не добавим все праздники
  while (holidays.indexOf(UTILS.formatDate(startDate, false)) != -1 && operationMode.holidaysOn == 0) {
    let operationModeType = getOperationModeType(true);
    let pStart = getStartWorkPeriod(operationMode, operationModeType);
    startDate.setDate(startDate.getDate() + 1);
    startDate.setHours(pStart.h, pStart.m);
  }

  //проверяем если день выходной и нет графика на выходные, то прибавляем день
  //проверяем в цикле пока не добавим все выходные
  while (isDayOff(startDate) && !operationMode.weekends) {
    let operationModeType = getOperationModeType(true);
    let pStart = getStartWorkPeriod(operationMode, operationModeType);
    startDate.setDate(startDate.getDate() + 1);
    startDate.setHours(pStart.h, pStart.m);
  }

  chekDayStart(); // проверяем начало рабочего времени
  chekDayEnd(); // проверяем закончился ли рабочий день

  // теперь проверяем не на обеде ли пришла заявка, если да то ставим начало периода после перерыва
  let lb = isLunchBreak();
  if(lb) startDate.setHours(lb.p.hour_start, lb.p.minute_start);

  // ну а теперь можно прибавить длительность (в минутах)
  do {
    startDate.setMinutes(startDate.getMinutes() + 1); //прибавляем минуту
    minutes -= 1; //и отнимаем минуты из общего количества

    lb = isLunchBreak(); //ищем перерыв
    // если есть то прибавляем разницу минут между текущей датой и датой окончания перерыва
    if(lb) startDate.setMinutes(startDate.getMinutes() + lb.minutes);

    chekDayEnd(true); //проверяем закончился ли рабочий день

  } while (minutes > 0);

  let finishDate = UTILS.formatDate(startDate, true);
  return {
    key: finishDate,
    value: customFormatDate(finishDate)
  };
}

try {
  let defaultDuration = 8; //длительность по умолчанию 8 часов

  let currentFormData = API.getFormData(dataUUID);
  let timemode = UTILS.getValue(currentFormData, 'service_form_order_timemode'); //режим работы
  let orderDate = UTILS.getValue(currentFormData, 'service_form_order_date'); //дата регистрации
  let duration = UTILS.getValue(currentFormData, 'service_form_order_duration'); //длительность выполнения
  let planFinishDate = null;

  if(!orderDate || !orderDate.hasOwnProperty('key')) throw new Error('не заполнена дата регистрации');

  if(!timemode || !timemode.hasOwnProperty('key')) {
    timemode = '1';
  } else {
    timemode = timemode.key;
  }

  if(!duration || !duration.hasOwnProperty('key') || duration.key.trim() == '') {
    duration = defaultDuration * 60;
  } else {
    duration = Math.round(duration.key * 60);
  }

  if(debug) log.info('дата регистрации: ' + orderDate.key + ' !! длительность: ' + duration + ' !! режим работы: ' + timemode);

  planFinishDate = appendMinutes(orderDate.key, duration, timemode);
  if(!planFinishDate) throw new Error('Ошибка рассчета планового срока исполнения');

  let asfData = [];
  asfData.push({
    "id": "service_form_order_planFinishDate",
    "type": "date",
    "value": planFinishDate.value,
    "key": planFinishDate.key
  });

  if(debug) {
    log.info(asfData);
  } else {
    let saveResult = API.mergeFormData({uuid: dataUUID, data: asfData});

    if(saveResult && saveResult.errorCode == '0') {
      message = 'Плановый срок исполнения: ' + planFinishDate.value;
    } else {
      message = 'Произошла ошибка сохранения';
    }
  }

} catch (err) {
  log.error(err.message);
  message = err.message;
}
