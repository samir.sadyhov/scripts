const searchUserCard = userID => {
  return new Promise(resolve => {
    try {
      const url = `rest/api/registry/data_ext?registryCode=service_registry_clients&loadData=false&field=service_form_ClientCard_user&condition=TEXT_EQUALS&key=${userID}`;
      AS.FORMS.ApiUtils.simpleAsyncGet(url)
      .then(res => res.count > 0 ? resolve(res.data[0]) : resolve(null));
    } catch (e) {
      resolve(null);
    }
  });
}

const setUserCard = user => {
  if(!user) return;

  searchUserCard(user[0].personID).then(res => {
    if(!res) throw new Error('Не найдена карточка клиента');
    let clientCardModel = model.playerModel.getModelWithId('service_form_order_client_card');
    if(clientCardModel) clientCardModel.setValue(res.documentID);
  }).catch(e => {
    console.error(e.message);
  });
}

if(editable) model.on('valueChange', (_1, _2, value) => setUserCard(value));
