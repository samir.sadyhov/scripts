//Менюшка
if($('.menu__icon').length) {
  let openMenu = false;

  const hideShowMenu = () => {
    if(!openMenu) {
      openMenu = !openMenu;
      $('.menu__icon').addClass('_active');
      $('.menu-container').addClass('_open');
      $('body').addClass('_lock');
    } else {
      openMenu = !openMenu;
      $('.menu__icon').removeClass('_active');
      $('.menu-container').removeClass('_open');
      $('body').removeClass('_lock');
    }
  }

  $('.menu__icon').off().on('click', e => {
    e.preventDefault();
    hideShowMenu();
  });

  $('.menu-container').off().on('click', e => {
    e.preventDefault();
    if($(e.target).hasClass('menu-container')) hideShowMenu();
    if($(e.target).hasClass('menu-button')) hideShowMenu();
  });
}

//Иконка приложения
if(!$('link[rel="icon"]').length) {
  $('head').append('<link rel="icon" href="data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgNDM4IDQzOCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPGNpcmNsZSBjeD0iNDAxIiBjeT0iMjE5IiByPSIzNyIgZmlsbD0icmVkIj48L2NpcmNsZT4KPGNpcmNsZSBjeD0iMTI4LjAwMDAwMDAwMDAwMDA2IiBjeT0iMzc2LjYxNjYyMzQ4ODc2Nzg2IiByPSIzNyIgZmlsbD0icmVkIj48L2NpcmNsZT4KPGNpcmNsZSBjeD0iMTI3Ljk5OTk5OTk5OTk5OTkxIiBjeT0iNjEuMzgzMzc2NTExMjMyMiIgcj0iMzciIGZpbGw9InJlZCI+PC9jaXJjbGU+CjxwYXRoIGQ9Ik0gMTgxLjE2MDA3MjI3MTE2Nzg1IDM5Ny4wMjI4NjMzMzM1NTI2NiBBIDE4MiAxODIgMCAwIDAgMzkyLjA5MjI4NTk2NTcxNzkgMjc1LjI0MTA5Mjk3NjI0MDQ1IiBmaWxsPSJub25lIiBzdHJva2U9InJlZCIgc3Ryb2tlLXdpZHRoPSIyNCI+PC9wYXRoPgo8cGF0aCBkPSJNIDgzLjc0NzY0MTc2MzExNDI1IDk3LjIxODIyOTY0MjY4NzggQSAxODIgMTgyIDAgMCAwIDgzLjc0NzY0MTc2MzExNDI3IDM0MC43ODE3NzAzNTczMTIyIiBmaWxsPSJub25lIiBzdHJva2U9InJlZCIgc3Ryb2tlLXdpZHRoPSIyNCI+PC9wYXRoPgo8cGF0aCBkPSJNIDM5Mi4wOTIyODU5NjU3MTc5IDE2Mi43NTg5MDcwMjM3NTk1NSBBIDE4MiAxODIgMCAwIDAgMTgxLjE2MDA3MjI3MTE2NzczIDQwLjk3NzEzNjY2NjQ0NzM5IiBmaWxsPSJub25lIiBzdHJva2U9InJlZCIgc3Ryb2tlLXdpZHRoPSIyNCI+PC9wYXRoPgo8bGluZSB4MT0iNDAxIiB5MT0iMjE5IiB4Mj0iMTI3Ljk5OTk5OTk5OTk5OTkxIiB5Mj0iNjEuMzgzMzc2NTExMjMyMiIgc3Ryb2tlPSJyZWQiIHN0cm9rZS13aWR0aD0iMjQiPjwvbGluZT4KPGxpbmUgeDE9IjQwMSIgeTE9IjIxOSIgeDI9IjEyOC4wMDAwMDAwMDAwMDAwNiIgeTI9IjM3Ni42MTY2MjM0ODg3Njc4NiIgc3Ryb2tlPSJyZWQiIHN0cm9rZS13aWR0aD0iMjQiPjwvbGluZT4KPGxpbmUgeDE9IjEyOC4wMDAwMDAwMDAwMDAwNiIgeTE9IjM3Ni42MTY2MjM0ODg3Njc4NiIgeDI9IjEyNy45OTk5OTk5OTk5OTk5MSIgeTI9IjYxLjM4MzM3NjUxMTIzMjIiIHN0cm9rZT0icmVkIiBzdHJva2Utd2lkdGg9IjI0Ij48L2xpbmU+CjxjaXJjbGUgY3g9IjIxOSIgY3k9IjIxOSIgcj0iNTQiIGZpbGw9InJlZCI+PC9jaXJjbGU+Cjwvc3ZnPgo=">')
}
