const createSynergyPlayer = (dataUUID, editable = false) => {
  let player = AS.FORMS.createPlayer();
  if($(document).width() < 781) player.model.showView('mobile');
  player.view.setEditable(editable);
  player.showFormData(null, null, dataUUID);
  return new Promise(resolve => {
    AS.FORMS.ApiUtils.getDocumentIdentifier(dataUUID)
    .then(documentID => AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/docflow/doc/document_info?documentID=${documentID}`))
    .then(documentInfo => {
      player.documentID = documentInfo.documentID;
      player.registryID = documentInfo.registryID;
      player.registryName = documentInfo.registryName;
      player.formName = documentInfo.formName;
      player.formID = documentInfo.formID;
      player.formCode = documentInfo.formCode;
      player.documentInfo = documentInfo;
      resolve(player);
    })
  });
}

const getModalDialog = (title, body, buttonName, buttonAction) => {
  let dialog = $('<div class="uk-flex-top" uk-modal>');
  let md = $('<div class="uk-modal-dialog uk-margin-auto-vertical">');
  let modalBody = $('<div class="uk-modal-body" uk-overflow-auto>');
  let footer = $('<div class="uk-modal-footer uk-text-right">');

  modalBody.append(body);
  footer.append('<button class="uk-button uk-button-default uk-modal-close" type="button">Закрыть</button>');
  dialog.append(md);
  md.append(`<div class="uk-modal-header"><h3>${title}</h3></div>`)
  .append(modalBody).append(footer);

  if(buttonName) {
    let actionButton = $('<button class="uk-button uk-button-primary uk-margin-left" type="button">');
    actionButton.html(buttonName).on('click', buttonAction);
    footer.append(actionButton);
  }

  return dialog;
}

const finishWork = (formPlayer, processes, formCode) => {
  try {
    if(formPlayer.model.getErrors().length) throw new Error('Заполните обязательные поля');

    let url = `rest/api/workflow/work/get_form_for_result?formCode=${formCode}&workID=${processes[0].actionID}`;
    AS.FORMS.ApiUtils.simpleAsyncGet(url).then(res => {
      if(res.errorCode && res.errorCode != '0') throw new Error(res.errorMessage);

      let player = AS.FORMS.createPlayer();
      player.view.setEditable(true);
      player.showFormData(null, null, res.dataUUID);

      let dialog = getModalDialog("Форма завершения", player.view.container, "Сохранить", () => {
        Cons.showLoader();
        try {
          if (!player.model.isValid()) throw new Error('Заполните обязательные поля');
          player.saveFormData(saveResult => {
            let finishWorkBody = {
                workID: processes[0].actionID,
                completionForm: "FORM",
                type: 'work',
                file_identifier: res.file_identifier
            };
            AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/workflow/work/set_result", result => {
              if(result.errorCode && result.errorCode != '0') throw new Error(result.errorMessage);
              Cons.hideLoader();
              UIkit.modal(dialog).hide();
              player.destroy();

              formPlayer.destroy();
              $('.exp-window-shadow').fadeOut({
                complete: function() {
                  $('.exp-window-shadow').remove();
                }
              });
              $('body').removeClass('_lock');

            }, null, finishWorkBody, "application/x-www-form-urlencoded; charset=UTF-8", err => {
              console.log(err);
            });

          });
        } catch (e) {
          Cons.hideLoader();
          showMessage(e.message, 'error');
        }
      });

      UIkit.modal(dialog).show();
      dialog.on('hidden', () => dialog.remove());
    });

  } catch (e) {
    showMessage(e.message, 'error');
    console.log(e);
  }
}

const getUserWork = processes => {
  let result = [];
  function search(p) {
    p.forEach(function(process) {
      if (process.responsibleUserID == AS.OPTIONS.currentUser.userid && !process.finished && process.code == 'approve') {
        result.push(process);
      }
      if (process.subProcesses.length > 0) search(process.subProcesses);
    });
  }
  search(processes);
  return result;
}

const setEnabled = player => {
  let cmpBlock = player.model.getAsfData().data.filter(x => x.type !== 'label');
  cmpBlock.forEach(item => {
    let tmpView = player.view.getViewWithId(item.id);
    if(tmpView) tmpView.setEnabled(false);
  });
}

const initButtonConfirm = (player, panelActions) => {
  let status = player.model.getModelWithId('service_form_order_status');
  if(status && status.getValue() && status.getValue()[0] == '2') {
    AS.FORMS.ApiUtils.simpleAsyncGet("rest/api/workflow/get_execution_process?documentID=" + player.documentID).then(processes => {
      let process = getUserWork(processes);
      if(!process.length) return;

      let confirmButton = $('<button class="exp-window-save-button uk-button uk-button-default">Подтвердить</button>');
      panelActions.append(confirmButton);
      panelActions.show();

      confirmButton.on('click', e => {
        e.preventDefault();
        e.target.blur();
        finishWork(player, process, 'service_form_approve');
      });

    });
  }
}

const openDocumentInWindow = (row, editable = false) => {
  let windowShadow = $('<div>', {class: 'exp-window-shadow'});
  let documentWindow = $('<div>', {class: 'exp-window-document'});
  let header = $('<div>', {class: 'exp-window-header'});
  let body = $('<div>', {class: 'exp-window-body'});
  let content = $('<div>', {class: 'exp-window-content'});
  let formPanel = $('<div>', {class: 'exp-window-form-panel'})
  let buttonsHeader = $('<div>', {class: 'exp-window-header-buttons'});
  let closeButton = $('<span class="exp-window-close-button" uk-icon="icon: close; ratio: 2"></span>');
  let panelActions = $('<div>', {class: 'exp-window-form-panel-actions'});
  let panelLogo = $('<div>', {class: 'exp-window-header-logo'});

  panelActions.hide();
  content.append(formPanel).append(panelActions);
  buttonsHeader.append(closeButton);
  header.append(panelLogo).append(buttonsHeader);
  body.append(content);
  documentWindow.append(header).append(body);
  windowShadow.append(documentWindow);

  createSynergyPlayer(row.dataUUID, editable).then(player => {
    panelLogo.append(`<saon>${player.formName}</span>`);
    formPanel.append(player.view.container);
    $('body').append(windowShadow).addClass('_lock');
    setTimeout(() => {
      player.model.hasChanges = false;
    }, 1000);

    closeButton.on('click', e => {
      player.destroy();
      windowShadow.fadeOut({
        complete: function() {
          windowShadow.remove();
        }
      });
      $('body').removeClass('_lock');
    });

    if(editable) setEnabled(player);
    initButtonConfirm(player, panelActions);
    Cons.hideLoader();
  });

}

Cons.setAppStore({openDocumentInWindow: openDocumentInWindow});
