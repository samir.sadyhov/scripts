const searchInRegistry = params => {
  return new Promise((resolve, reject) => {
    rest.synergyGet(`api/registry/data_ext?${params}`, res => resolve(res), err => reject(err));
  });
}

const getAllRegistries = () => {
  return new Promise(resolve => {
    rest.synergyGet(`api/registry/list`,
      res => resolve(res.find(x => x.regGroupName == 'SS. Реестры заказов по Услугам').consistOf || []),
      err => resolve([])
    );
  });
}

const getServiceRegistryGroups = () => {
  let params = $.param({registryCode: 'service_registry_service_group'});
  ['service_form_service_group_name',
  'service_form_service_group_services',
  'service_form_service_group_pic'].forEach(x => params += `&fields=${x}`);
  return new Promise(resolve => {
    searchInRegistry(params).then(res => {
      resolve(res.recordsCount > 0 ? res.result : []);
    }).catch(err => {
      resolve(null);
      console.log('getServiceRegistries ERROR', err);
    });
  });
}

const getServiceRegistries = () => {
  let params = $.param({registryCode: 'service_registry_service'});
  ['service_form_service_description',
  'service_form_service_formCode',
  'service_form_service_name',
  'service_form_service_registryCode',
  'file-v1biq0'].forEach(x => params += `&fields=${x}`);
  return new Promise(resolve => {
    searchInRegistry(params).then(res => {
      resolve(res.recordsCount > 0 ? res.result : []);
    }).catch(err => {
      resolve(null);
      console.log('getServiceRegistries ERROR', err);
    });
  });
}

const parseRegistries = (allReg, servicesReg) => {
  return servicesReg.map(x => {
    const {
      service_form_service_registryCode,
      service_form_service_formCode,
      service_form_service_description,
      service_form_service_name
    } = x.fieldValue;
    const img = x.fieldKey['file-v1biq0'] || null;
    const regInfo = allReg.find(r => r.registryCode == service_form_service_registryCode);
    if(regInfo && regInfo.rights.includes("rr_create")) {
      return {
        service_form_service_registryCode,
        service_form_service_formCode,
        service_form_service_description,
        service_form_service_name,
        img,
        rights: regInfo.rights,
        documentID: x.documentID
      }
    }
  }).filter(x => x != undefined);
}

const parseFilters = data => {
  const filters = [];
  const parse = data => {
  	data.forEach(filter => {
    	const {id, name, code} = filter;
    	filters.push({id, name, code});
    	if (filter.hasOwnProperty('children')) parse(filter.children);
  	});
  }
  parse(data);
  return filters;
}

const getRegistryFilters = registryCode => {
  return new Promise(resolve => {
    rest.synergyGet(`api/registry/filters?registryCode=${registryCode}`,
      res => resolve(parseFilters(res)),
      err => resolve([])
    );
  });
}

const parseServiceGroups = (data, appRegistryList) => {
  return data.map(x => {
    const services = x.fieldKey.service_form_service_group_services.split(';');
    const groupServices = services.map(documentID => appRegistryList.find(x => x.documentID == documentID)).filter(x => x);
    if(groupServices.length) {
      return {
        groupID: x.dataUUID,
        groupName: x.fieldValue.service_form_service_group_name,
        groupImage: x.fieldKey.service_form_service_group_pic,
        groupServices
      }
    }
  }).filter(x => x);
}

this.$service = {
  getAppInfo: function(){
    let allRegisties;
    let appRegistryList;
    let serviceGroups;

    return new Promise(resolve => {
      getAllRegistries().then(res => {
        allRegisties = res;
        return getServiceRegistries();
      }).then(res => {
        return parseRegistries(allRegisties, res);
      }).then(parseData => {
        appRegistryList = parseData;
        const promises = appRegistryList.map(x => getRegistryFilters(x.service_form_service_registryCode));
        return Promise.all(promises);
      }).then(promiseResult => {
        appRegistryList.forEach((x, i) => x.filters = promiseResult[i]);
        return getServiceRegistryGroups();
      }).then(res => {
        serviceGroups = parseServiceGroups(res, appRegistryList);
        Cons.setAppStore({appRegistryList: appRegistryList});
        Cons.setAppStore({serviceGroups: serviceGroups});
        resolve(appRegistryList);
      });
    });
  }
}
