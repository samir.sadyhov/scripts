let servicePanelHidden = true;
let filterPanelHidden = true;

const getFilterButton = filter => {
  return $('<button>', {class: 'uk-button uk-button-default fonts'})
  .text(filter.name)
  .on('click', e => {
    $('.filter_group_buttons button').removeClass('selected');
    $(e.target).addClass('selected');
    $('.filter_group_buttons').removeClass('__open');
    $('body').removeClass('_lock');
    filterPanelHidden = true;
    fire({type: 'registry_filter_change', filterCode: filter.code}, 'registry-1');
    changeFilterButtonText(filter.name);
  });
}

const initFilters = filters => {
  const buttonContainer = $('.filter_group_buttons').empty();
  filters.forEach(filter => buttonContainer.append(getFilterButton(filter)));
}

const changeFilterButtonText = text => {
  fire({type: 'button_change_text', text: localizedText(text,text,text,text)}, 'filter_button_menu');
}

pageHandler('my_orders', () => {
  const {appRegistryList} = Cons.getAppStore();

  const myServices = appRegistryList.map(x => {
    if(x.filters.find(filter => filter.code == "my_orders")) return x;
  }).filter(x => x != undefined);

  fire({
    type: 'change_repeater_custom_source',
    customSource: myServices
  }, 'panel-iterator');

  $('#panel-iterator').parent().off().on('click', e => {
    if(!$(e.target).hasClass('registry-list-button')) return;

    const registryCode = $(e.target).parent().parent().find('.registry-list-item-code').text();
    const regInfo = myServices.find(x => x.service_form_service_registryCode == registryCode);

    initFilters(regInfo.filters);

    $('.registry-list-item').removeClass('active');
    $(e.target).parent().parent().addClass('active');

    $('.app_left_panel').removeClass('__open');
    $('body').removeClass('_lock');
    servicePanelHidden = true;

    if(registryCode == 'Не найдено') {
      showMessage('Не найден код реестра', 'error');
    } else {
      $('.service-table').removeClass('hidden');
      fire({type: 'registry_code_change', code: registryCode}, 'registry-1');
      fire({type: 'registry_page_change', page: 0}, 'registry-1');
      fire({type: 'registry_filter_change', filterCode: regInfo.filters[0].code}, 'registry-1');
      $('.filter_group_buttons button').removeClass('selected');
      $('.filter_group_buttons button:nth-child(1)').addClass('selected');
      changeFilterButtonText(regInfo.filters[0].name);
    }
  });

  $('#services_button_menu, #close-panel-registry').off().on('click', e => {
    e.preventDefault();
    e.target.blur();
    if(servicePanelHidden) {
      $('.app_left_panel').addClass('__open');
      $('body').addClass('_lock');
    } else {
      $('.app_left_panel').removeClass('__open');
      $('body').removeClass('_lock');
    }
    servicePanelHidden = !servicePanelHidden;
  });

  $('#filter_button_menu').off().on('click', e => {
    e.preventDefault();
    e.target.blur();
    $('.filter_group_buttons').addClass('__open');
    $('body').addClass('_lock');
    filterPanelHidden = false;
  });

  if(!Cons.getAppStore().listeners_my_orders_page) {
    addListener('registry_item_click', 'registry-1', e => {
      Cons.showLoader();
      const {openDocumentInWindow} = Cons.getAppStore();
      openDocumentInWindow(e);
    });
    Cons.setAppStore({listeners_my_orders_page: true});
  }
});
