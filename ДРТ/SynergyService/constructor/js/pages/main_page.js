pageHandler('main_page', () => {
  const {serviceGroups, appRegistryList} = Cons.getAppStore();
  let servicePanelHidden = true;
  let asform;

  //группы услуг
  fire({type: 'change_repeater_custom_source', customSource: serviceGroups}, 'ServiceGroupRepeater');

  //обработка клика по группе
  $('#ServiceGroupRepeater').parent().off().on('click', e => {
    const el = $(e.target).closest('.service-group-item');
    if(el.hasClass('active')) return;

    const groupID = el.find('.service-group-id').text();
    const services = serviceGroups.find(x => x.groupID == groupID).groupServices;
    fire({type: 'set_hidden', hidden: false}, 'panel-service');
    fire({type: 'change_repeater_custom_source', customSource: services}, 'panel-service');
    $('.service-group-item').removeClass('active');
    el.addClass('active');

    $('.app_left_panel').removeClass('__open');
    $('body').removeClass('_lock');
    servicePanelHidden = true;
  });

  //обработка клика по услуге
  $('.app-iterator-container').off().on('click', e => {
    if(e.target.nodeName != 'IMG') return;

    const iteratorInfo = {
      formCode: $(e.target).parent().parent().find('.form-code').text(),
      registryCode: $(e.target).parent().parent().find('.registry-code').text()
    }
    Cons.setAppStore({iteratorInfo: iteratorInfo});

    if(iteratorInfo.formCode == 'Не найдено') {
      showMessage('Не найдена форма', 'error');
    } else {
      fire({type: 'set_hidden', hidden: false}, 'panel-modal');
    }
  });

  $('#servicesGroupButtonMenu, #close-panel-registry').off().on('click', e => {
    e.preventDefault();
    e.target.blur();
    if(servicePanelHidden) {
      $('.app_left_panel').addClass('__open');
      $('body').addClass('_lock');
    } else {
      $('.app_left_panel').removeClass('__open');
      $('body').removeClass('_lock');
    }
    servicePanelHidden = !servicePanelHidden;
  });

  if(!Cons.getAppStore().modal_form_main_page) {

    addListener("set_hidden", "panel-modal", e => {
      if(e.hidden) return;
      const {formCode, registryCode} = Cons.getAppStore().iteratorInfo;
      fire({type: 'show_form', formCode: formCode}, 'formPlayer-1');
      fire({type: 'show_form_view', viewCode: 'client_view'}, 'formPlayer-1');
    });

    addListener('loaded_form_data', 'formPlayer-1', e => {
      asform = e;
    });

    addListener("button_click", "button-sendOrder", function() {
      const {formCode, registryCode} = Cons.getAppStore().iteratorInfo;
      let valid = !asform.model.getErrors().length;
      if (valid) {
        fire({type: "set_disabled", disabled: true, cmpID: "button-sendOrder"}, "button-sendOrder");
        fire({
          type: "create_form_data",
          registryCode: registryCode,
          activate: true,
          success: (id, docid) => {
            fire({type: 'set_hidden', hidden: true}, 'panel-modal');
            showMessage('Заявка успешно отправлена', 'success');
          },
          error: (st, err) => {
            console.log("failed" + err.toString());
          }
        }, 'formPlayer-1');
      } else {
        showMessage('Заполните обязательные поля', 'error');
      }
    });

    Cons.setAppStore({modal_form_main_page: true});
  }
});
