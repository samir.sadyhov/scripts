const eventShow = {type: 'set_hidden', hidden: false};
const eventHide = {type: 'set_hidden', hidden: true};
const emailRegex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
const groupCode = 'service_client_group';
const positionCode = 'service_clients';

//если поле не заполнено - выделить его как невалидное
const emptyValidator = input => {
  if(input && input.text && input.text !== '') {
    fire({type: 'input_highlight', error: false}, input.code);
    return true;
  } else {
    fire({type: 'input_highlight', error: true}, input.code);
    return false;
  }
}

//валидация введенного email
const emailValidator = input => {
  if(emptyValidator(input)) {
    if (input && emailRegex.test(input.text)) {
      fire({type: 'input_highlight', error: false}, input.code);
      return true;
    } else {
      fire({type: 'input_highlight', error: true}, input.code);
      return false;
    }
  }
  return false;
}

const toTranslit = text => {
  return text.replace(/([а-яё])|([\s_-])|([^a-z\d])/gi,
    function (all, ch, space, words, i) {
      if (space || words) return space ? '_' : '';
      var code = ch.charCodeAt(0),
      index = code == 1025 || code == 1105 ? 0 :
      code > 1071 ? code - 1071 : code - 1039,
      t = ['yo', 'a', 'b', 'v', 'g', 'd', 'e', 'zh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o',
      'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'shch', '', 'y', '', 'e', 'yu', 'ya'];
      return t[index];
    });
}

const getTemplateEmail = (fio, login, password) => {
  const theme = 'Успешная регистрация';
  const subject = `Уважаемый(ая) ${fio}!<br><br>
  Ваши данные для входа в систему<br>
  <br>Логин: <b>${login}</b>
  <br>Пароль: <b>${password}</b>
  <br><br>Для входа в систему перейдите по <a href="${window.location.origin}/service">ссылке</a>`;
  return {theme, subject};
}

const serviceApi = {
  searchPosition: function(){
    return new Promise(async resolve => {
      try {
        let url = `api/positions/search?pointer_code=${positionCode}&deleted=false&recordsCount=1&searchTypePC=exact`;
        rest.synergyGet(url, res => {
          if(res.length > 0) {
            resolve(res[0]);
          } else {
            resolve(null);
          }
        });
      } catch (e) {
        console.log(e);
        resolve(null);
      }
    });
  },

  positionsAppoint: function(positionID, userID){
    return new Promise(async resolve => {
      try {
        rest.synergyGet(`api/positions/appoint?positionID=${positionID}&userID=${userID}`, res => {
          if(res.errorCode == 0) {
            resolve(res);
          } else {
            console.log(res);
            resolve(null);
          }
        });
      } catch (e) {
        console.log(e);
        resolve(null);
      }
    });
  },

  addUser: function(userData){
    return new Promise(async resolve => {
      try {
        let data = {
          login: userData.email,
          password: userData.password,
          email: userData.email,
          lastname: userData.lastname,
          firstname: userData.firstname,
          pointersCode: toTranslit(`${userData.lastname} ${userData.firstname}`),
          hasAccess: true,
          isConfigurator: false,
          isAdmin: false,
          isChancellery: false
        };
        if(userData.patronymic) data.patronymic = userData.patronymic;

        rest.synergyPost('api/filecabinet/user/save', data, "application/x-www-form-urlencoded; charset=UTF-8", function (response) {
          if(response.errorCode == 0) {
            resolve(response);
          } else {
            console.log(response);
            resolve(null);
          }
        }, function (err) {
            console.error(err);
            resolve(null);
        });
      } catch (e) {
        console.log(e);
        resolve(null);
      }
    });
  },

  addUserGroup: function(userID){
    return new Promise(async resolve => {
      try {
        rest.synergyGet(`api/storage/groups/add_user?groupCode=${groupCode}&userID=${userID}`, res => resolve(res));
      } catch (e) {
        console.log(e);
        resolve(null);
      }
    });
  },

  createDocRCC: function(registryCode, asfData, sendToActivation = false){
    return new Promise(async resolve => {
      try {
        let settings = {
          url: `${window.location.origin}/Synergy/rest/api/registry/create_doc_rcc`,
          method: "POST",
          headers: {
            "Authorization": "Basic " + btoa(Cons.creds.login + ":" + Cons.creds.password),
            "Content-Type": "application/json; charset=utf-8"
          },
          data: JSON.stringify({
            registryCode: registryCode,
            data: asfData,
            sendToActivation: sendToActivation
          }),
        };
        $.ajax(settings).done(response => {
          if(response.errorCode == 0) {
            resolve(response);
          } else {
            console.log(response);
            resolve(null);
          }
        }).fail(err => {
          console.error(err);
          resolve(null);
        });
      } catch (e) {
        console.log(e);
        resolve(null);
      }
    });
  },

  searchUser: function(param){
    return new Promise(async resolve => {
      try {
        rest.synergyGet(`api/filecabinet/user/checkExistence?${jQuery.param(param)}`, res => resolve(res.result == "true"));
      } catch (e) {
        console.log(e);
        resolve(null);
      }
    });
  },

  sendNotification: function(body){
    return new Promise(async resolve => {
      try {
        $.ajax({
          url: `${window.location.origin}/Synergy/rest/api/notifications/send`,
          method: "POST",
          headers: {
            "Authorization": "Basic " + btoa(Cons.creds.login + ":" + Cons.creds.password),
            "Content-Type": "application/json; charset=utf-8"
          },
          data: JSON.stringify(body),
        }).done(response => {
          if(response.errorCode == 0) {
            resolve(response);
          } else {
            console.log(response);
            resolve(null);
          }
        }).fail(err => {
          console.error(err);
          resolve(null);
        });
      } catch (e) {
        console.log(e);
        resolve(null);
      }
    });
  }
}

const registration = () => {
  let lname = getCompByCode('lastname-input');
  let fname = getCompByCode('firstname-input');
  let pname = getCompByCode('patronymic-input');
  let email = getCompByCode('mail-input');
  let phone = getCompByCode('phone-input');
  let password = getCompByCode('pass-input');
  let passwordConfrim = getCompByCode('pass-confirm-input');
  let {location} = Cons.getAppStore();
  let selectLocation = $('#location-select').val();
  let organization = getCompByCode('organization-input');

  let success = emptyValidator(fname) &
      emptyValidator(lname) &
      emptyValidator(password) &
      emptyValidator(passwordConfrim) &
      emailValidator(email);

  if(!success) {
    showMessage('Не все поля формы регистрации были заполнены', 'error');
    return;
  }

  if(password.text.length < 3) {
    fire({type: 'input_highlight', error: true}, password.code);
    showMessage('Минимальная длина пароля должна составлять 3 символа', 'error');
    return;
  } else {
    fire({type: 'input_highlight', error: false}, password.code);
  }

  if(password.text != passwordConfrim.text) {
    fire({type: 'input_highlight', error: true}, password.code);
    fire({type: 'input_highlight', error: true}, passwordConfrim.code);
    showMessage('Пароли не совпадают', 'error');
    return;
  } else {
    fire({type: 'input_highlight', error: false}, password.code);
    fire({type: 'input_highlight', error: false}, passwordConfrim.code);
  }

  serviceApi.searchUser({login: email.text}).then(resUser => {
    if(resUser) throw new Error('В системе уже имеется учетная запись с таким email.');
    return serviceApi.searchUser({code:  toTranslit(`${lname.text} ${fname.text}`)});
  }).then(resCode => {
    if(resCode) throw new Error('В системе уже имеется учетная запись с таким кодом.');
    let loc = {
      key: selectLocation,
      value: location.find(x => x.documentID == selectLocation).fieldValue.service_form_location_long
    }
    registrationContinue({lname, fname, pname, email, phone, password, loc, organization});
  }).catch(e => {
    showMessage(e.message, 'error');
    console.error('REGISTRATION ERROR: ' + e.message);
  });

}

const registrationContinue = data => {
  Cons.showLoader();

  let newData = {};
  newData.fio = [data.lname.text, data.fname.text, data.pname.text].join(' ').trim();

  //Поиск должности по коду
  serviceApi.searchPosition().then(res => {
    if(!res) throw new Error('Не найдена должность для назначения');
    newData.positionID = res;

    //создаем пользователя
    return serviceApi.addUser({
      lastname: data.lname.text,
      firstname: data.fname.text,
      patronymic: data.pname.text,
      email: data.email.text,
      password: data.password.text
    });

  }).then(newUser => {
    if(!newUser) throw new Error('Ошибка при создании пользователя');
    newData.userID = newUser.userID;

    //добавления пользователя в группу
    return serviceApi.addUserGroup(newData.userID);
  }).then(res => {

    //назначаем сотрудника на должность
    return serviceApi.positionsAppoint(newData.positionID, newData.userID);
  }).then(res => {
    if(!res) throw new Error('Ошибка при назначении сотрудника на должность');

    let asfData = [];
    asfData.push({id: "service_form_ClientCard_lastname", type: "textbox", value: data.lname.text});
    asfData.push({id: "service_form_ClientCard_name", type: "textbox", value: data.fname.text});
    asfData.push({id: "service_form_ClientCard_phone", type: "textbox", value: data.phone.text});
    asfData.push({id: "service_form_ClientCard_email", type: "textbox", value: data.email.text});
    asfData.push({id: "service_form_ClientCard_user", type: "entity", value: newData.fio, key: newData.userID});
    asfData.push({id: "service_form_ClientCard_location", type: "reglink", value: data.loc.value, valueID: data.loc.key, key: data.loc.key});

    if(data.organization.text) asfData.push({id: "service_form_ClientCard_organization", type: "textbox", value: data.organization.text});
    if(data.pname.text) asfData.push({id: "service_form_ClientCard_patronymic", type: "textbox", value: data.pname.text});

    //создание записи в реестре Клиенты
    return serviceApi.createDocRCC('service_registry_clients', asfData, true);

  }).then(res => {
    if(!res) throw new Error('Ошибка при создании карточки пользователя');

    let emlData = getTemplateEmail(newData.fio, data.email.text, data.password.text);

    return serviceApi.sendNotification({
      header: emlData.theme,
      message: emlData.subject,
      emails: [data.email.text]
    });

  }).then(res => {

    Cons.hideLoader();
    showMessage("Регистрация в системе прошла успешно", 'success', true);
    setTimeout(() => {
      fire({type: 'goto_page', pageCode: 'auth_page'}, 'button-reg');
    }, 2000);

  }).catch(e => {
    showMessage(e.message, 'error');
    console.error('REGISTRATION ERROR: ' + e.message);
    Cons.hideLoader();
  });

}

const renderLocationListBox = () => {
  rest.synergyGet(`api/registry/data_ext?registryCode=service_registry_location&fields=service_form_location_long`, res => {
    Cons.setAppStore({location: res.result});
    let container = $('<div>', {style: "width: calc(100% - 10px); margin: 5px;"});
    let select = $('<select>', {class: 'uk-select', id: 'location-select'});
    res.result.forEach(x => select.append(`<option value="${x.documentID}">${x.fieldValue.service_form_location_long}</option>`));
    container.append(`<label class="uk-form-label" for="location-select">Локация</label>`).append(select);
    $('#panel-reg-data').append(container);
  });
}

pageHandler('registration_page', () => {

  let mailInput = $('#mail-input');
  mailInput.off().on('input', e => {
    mailInput.val(mailInput.val().replace(/[^a-zA-Z\d.@\-_]/g,''));
  });

  let inputPhone = $('#phone-input');
  new IMask(inputPhone[0], {
    mask: '+{7}-000-000-00-00',
    lazy: false
  });

  inputPhone.on('keydown', e => {
    if ([35, 36, 37, 38, 39, 40].indexOf(e.keyCode) !== -1) {
      e.preventDefault();
      return;
    }
  });

  renderLocationListBox();

  if(!Cons.getAppStore().button_registration_listener) {

    addListener('button_click', 'button-reg', () => {
      registration();
    });

    Cons.setAppStore({button_registration_listener: true});
  }
});
