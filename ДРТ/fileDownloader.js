const getFile = async (identifier, type = 'blob') => {
  return new Promise(async resolve => {
    try {
      const {login, password} = AS.OPTIONS;
      const auth = "Basic " + btoa(unescape(encodeURIComponent(`${login}:${password}`)));
      const url = `../Synergy/rest/api/storage/file/get?identifier=${identifier}`;
      const response = await fetch(url, {method: 'GET', headers: {"Authorization": auth}});
      if(!response.ok) throw new Error(`HTTP: ${response.status}, message: ${response.statusText} [ ${response.text()} ]`);
      switch (type) {
        case 'json': resolve(response.json()); break;
        default: resolve(response.blob());
      }
    } catch (err) {
      console.log(`ERROR [ getFile ]: ${JSON.stringify(err)}`);
      resolve(false);
    }
  });
}

const getFileModels = () => {
  const result = [];
  model.playerModel.models[0].modelBlocks[0].forEach(block => {
    if(block.asfProperty.type === "file") result.push(block);
    if(block.asfProperty.type === "table") {
      if(block.modelBlocks.length > 0) {
        block.modelBlocks.forEach(row => {
          row.forEach(tableBlock => {
            if(tableBlock.asfProperty.type === "file") result.push(tableBlock);
          });
        });
      }
    }
  });
  return result;
}

const fileDownload = async (fullName, identifier) => {
  AS.SERVICES.showWaitWindow();
  const blob = await getFile(identifier);
  if(blob) {
    const fileURL = window.URL.createObjectURL(blob);
    const a = document.createElement("a");
    if(typeof a.download === 'undefined') {
      window.location = fileURL;
    } else {
      a.href = fileURL;
      a.download = fullName;
      a.click();
    }
  }
  AS.SERVICES.hideWaitWindow();
}

const initFileDownloader = () => {
  const fileModels = getFileModels();

  fileModels.forEach(fileModel => {
    const {id, ownerTableId, tableBlockIndex} = fileModel.asfProperty;
    const tmpView = view.playerView.getViewWithId(id, ownerTableId, tableBlockIndex);

    //отключаем все стандартные события
    setTimeout(() => {
      $(`[data-asformid="file.filename.${id}"]`).off();
      fileModel.on('valueChange', () => {
        $(`[data-asformid="file.filename.${id}"]`).off();
        return null;
      });
    }, 100);

    //вешаем свое событие
    if(tmpView) tmpView.container.on('click', () => {
      if(!fileModel.value) return;
      if(!fileModel.value.hasOwnProperty('identifier')) return;
      fileDownload(fileModel.value.name, fileModel.value.identifier);
    });

  });
}

initFileDownloader();
