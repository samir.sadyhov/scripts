let ISSUE114_NEW_STATUS = 1; // Зарегистрирован
let ISSUE114_IN_QUEUE = 2; // На очереди
let ISSUE114_IN_PROGRESS = 3; // В процессе
let ISSUE114_WAITING_USER = 4; // Ожидает ответа пользователя
let ISSUE114_WAITING_PROBLEM = 5; // Ожидает проблему
let ISSUE114_CLOSED = 6; // Закрыт
let ISSUE114_WAITING_USER_RATE = 7; // Ожидает подтверждения завершения
let ISSUE114_WAITING_EQUIPMENT = 8; // В ожидании выделения техники
let ISSUE114_WAITING_SUPPLY = 9; // Направлен внешнему поставщику
let ISSUE114_INFO_GIVEN = 10; // Информация предоставлена
let ISSUE114_WRONG_WAY = 11; // Неверно направлен
let ISSUE114_RE_OPEN = 12; // Направлен повторно
let ISSUE114_WAITING_MASS_INCIDENT = 13; //Ожидает закрытия массового инцидента
let ISSUE114_WAITING_APPROVAL = 91; // На согласовании
let ISSUE114_APPROVE = 15; // Согласовано
let ISSUE114_NOT_APPROVE = 16; // Не согласовано

// Коды груп которым будет доступна кнопка
let ISSUE114_ACCESS_GROUPS = ['itsm_group_reassign_access'];

// Коды статусов которым будет доступна кнопка
let ISSUE114_ACCESS_STATUS = [ISSUE114_IN_QUEUE, ISSUE114_IN_PROGRESS, ISSUE114_INFO_GIVEN];

// Наименование кнопки
let ISSUE114_BUTTON_NAME = 'Переназначить';

// Переменные для алгоритма
let ISSUE114_PLAYERTMP = false;

function ISSUE114_API_ITSM(method, callback, params, type, dataType, headers, async) {
  var query = $.ajax({
    url: window.location.origin + '/itsm/rest/' + method,
    async: (async ?async :false),
    type: (!type ? 'GET' : type),
    beforeSend: AS.FORMS.ApiUtils.addAuthHeader,
    data: params,
    headers: headers,
    dataType: (!dataType ? 'json' : dataType)
  });
  $.when(query).then(callback);
}

function ISSUE114_API(method, callback, params, type, dataType, headers, async) {
  var query = $.ajax({
    url: window.location.origin + '/Synergy/rest/api/' + method,
    async: (async ?async :false),
    type: (!type ? 'GET' : type),
    beforeSend: AS.FORMS.ApiUtils.addAuthHeader,
    data: params,
    headers: headers,
    dataType: (!dataType ? 'json' : dataType)
  });
  $.when(query).then(callback);
}

/**
 * Рекурсивная функция - ищет работу по документу с типом `ASSIGNMENT_ITEM` и не завершенная
 * @param   arr     Массив содержащий в себе `ход-выполнения документа`
 * @return  Массив с работой
 */
function ISSUE114_recursiveFilter(arr, currentUserId) {
  let matches = [];
  if (!Array.isArray(arr)) return matches;
  arr.forEach(x => {
    if (x.typeID == 'ASSIGNMENT_ITEM' && !x.finished && x.responsibleUserID == currentUserId) matches.push(x);
    if (x.subProcesses.length > 0) {
      let childResults = ISSUE114_recursiveFilter(x.subProcesses, currentUserId);
      if (childResults.length) matches = matches.concat(childResults);
    }
  });
  return matches;
}

AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  model.playerModel.on('dataLoad', function() {
    if (model.playerModel.formCode != "itsm_form_incident") return;
    if (model.formCode != 'itsm_form_incident' || ISSUE114_ACCESS_STATUS.indexOf(+model.playerModel.getModelWithId('itsm_form_incident_status').getValue()[0]) === -1) return;
    if ($('.custom-redirect-btn-brightgreen').length) return;

    let api = AS.FORMS.ApiUtils;

    $.when(api.simpleAsyncGet('rest/api/person/auth?getGroups=true'))
    .then(res => {
      let resFilter = res.groups.filter(i => ISSUE114_ACCESS_GROUPS.indexOf("" + i.name) !== -1);
      if (!resFilter.length) return;
      return api.getDocumentIdentifier(model.playerModel.asfDataId);
    })
    .then(documentID => {
        let currentDocModel = model.playerModel;
        let parent = $(`[document_id=${documentID}]`).parent().parent().parent();
        let newBtn = $(`<td colspan="3"><div class="custom-redirect-btn-brightgreen issue114-custom-button"><span>${ISSUE114_BUTTON_NAME}</span></div></td>`);
        let trBlock = $('<tr></tr>');

        parent.find('td > div.gwt-HTML').each(function(k, item) {
          if ($(item).width() == 9) $(item).remove();
        });

        newBtn.on('click', () => {
          if (currentDocModel.getErrors().length) {
            AS.SERVICES.showErrorMessage("Заполните обязательные поля");
            return;
          }
          AS.SERVICES.showWaitWindow();

          let workId;
          api.saveAsfData(currentDocModel.getAsfData().data, currentDocModel.formId, currentDocModel.asfDataId);
          $.when(api.simpleAsyncGet(`rest/api/workflow/get_execution_process?documentID=${documentID}`))
          .then(processes => {
            workId = ISSUE114_recursiveFilter(processes, currentDocModel.getModelWithId('itsm_form_incident_service_user').asfProperty.data.key);
            if (!workId.length) {
              AS.SERVICES.showErrorMessage("Не найден идентификатор маршрута работы");
              AS.SERVICES.hideWaitWindow();
              return;
            }
            return api.simpleAsyncGet(`rest/api/workflow/work/get_form_for_result?formCode=itsm_form_incident_wcf_new&workID=${workId[0].actionID}`);
          })
          .then(formWork => {
            ISSUE114_PLAYERTMP = {
              player: null,
              title: "Форма завершения инцидента",
              showMessage: function(message) {
                $("#message_text").html(message);
                $("#message").show();
              },
              hideMessage: function() {
                $("#message").hide();
              },
              clearPlayer: function() {
                if (ISSUE114_PLAYERTMP.player) ISSUE114_PLAYERTMP.player.destroy();
                ISSUE114_PLAYERTMP.player = null;
              },
              saveData: function(callback) {
                if (ISSUE114_PLAYERTMP.player.model.getErrors().length > 0) return AS.SERVICES.showErrorMessage("Заполните обязательные поля");
                AS.SERVICES.showWaitWindow();
                ISSUE114_PLAYERTMP.player.saveFormData(callback);
              },
              createPlayer: function() {
                ISSUE114_PLAYERTMP.clearPlayer();
                ISSUE114_PLAYERTMP.player = AS.FORMS.createPlayer();
                ISSUE114_PLAYERTMP.player.view.setEditable(true);
                ISSUE114_PLAYERTMP.player.showFormData(ISSUE114_PLAYERTMP.title, 0, formWork.dataUUID);
                AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
                  if (!ISSUE114_PLAYERTMP || !ISSUE114_PLAYERTMP.player) return;
                  model.disabledNewValueList = false;
                  model.on(AS.FORMS.EVENT_TYPE.dataLoad, function(event, model) {
                    AS.SERVICES.hideWaitWindow();
                    $('#form_player_btns_list').remove();
                    $('#form_player_div').append('<div id="form_player_btns_list"><hr class="form_player_bts_hr"></div>');
                    $('#form_player_btns_list').append('<div class="form_player_loader_dots"><div class="form_player_loader_dot"></div><div class="form_player_loader_dot"></div><div class="form_player_loader_dot"></div><div class="form_player_loader_dot"></div><div class="form_player_loader_dot"></div></div>');
                    let btn = $('<div class="form_player_btn_send">Готово</div>');
                    btn.hide();
                    btn.on('click', function() {
                      if (model.getErrors().length) {
                        AS.SERVICES.showErrorMessage("Заполните обязательные поля");
                        return;
                      }

                      ISSUE114_PLAYERTMP.saveData(function(resp) {
                        workId.forEach(item => {
                          try {
                            let urlMethod = `workflow/work/system/set_result?workId=${item.actionID}&file_identifier=${formWork.file_identifier}&dataUUID=${currentDocModel.asfDataId}`;
                            ISSUE114_API_ITSM(urlMethod, function(workResp) {
                              if (workResp.errorCode == '0') {
                                currentDocModel.getModelWithId('itsm_form_incident_status').setValue("" + ISSUE114_IN_QUEUE);
                                AS.FORMS.ApiUtils.saveAsfData(currentDocModel.getAsfData().data, currentDocModel.formId, currentDocModel.asfDataId);
                              } else {
                                console.log(workResp);
                              }
                              $('td > img[src*="close.png"]').click();
                              AS.SERVICES.hideWaitWindow();
                            }, {}, 'POST', 'json', {'Content-Type': 'application/json'}, true);
                          } catch (e) {
                            console.log(e);
                          }
                        });
                        $('#form_player_container').remove();
                        ISSUE114_PLAYERTMP = false;
                        AS.SERVICES.hideWaitWindow();
                      });
                    });
                    btn.appendTo('#form_player_btns_list');
                    if (view.getViewWithId('itsm_form_incident_wcf_status') != null) {
                      view.getViewWithId('itsm_form_incident_wcf_status').setEnabled(false);
                      model.getModelWithId('itsm_form_incident_wcf_status').setValue("" + ISSUE114_IN_PROGRESS);
                    }
                  });
                });
                ISSUE114_PLAYERTMP.player.view.appendTo($('#form_player_div'));
                ISSUE114_PLAYERTMP.player.model.on("valueChange", function() {
                  ISSUE114_PLAYERTMP.hideMessage();
                });
              }
            };

            var container = $('<div></div>', {id: 'form_player_container'});
            container.append($('<div></div>', {id: 'form_player_div'}));
            container.dialog({
              modal: true,
              width: 640,
              height: 340,
              resizable: false,
              close: function(event, ui) {
                ISSUE114_PLAYERTMP = false;
                $('#form_player_container').remove();
              },
              title: i18n.tr(ISSUE114_PLAYERTMP.title)
            });
            ISSUE114_PLAYERTMP.createPlayer();
          })
        });
        trBlock.append(newBtn);
        parent.append(trBlock);
        parent.append('<tr><td colspan="3"><div class="gwt-HTML" style="height: 6px;"></div></td></tr>');
    });
  });
});
