const fileDownload = identifier => window.open(`${window.location.origin}/itsm/rest/file?identifier=${identifier}`);

const getFileModels = () => {
  let result = [];
  model.playerModel.models[0].modelBlocks[0].forEach(block => {
    if(block.asfProperty.type === "file") result.push(block);
    if(block.asfProperty.type === "table") {
      if(block.modelBlocks.length > 0) {
        block.modelBlocks.forEach(row => {
          row.forEach(tableBlock => {
            if(tableBlock.asfProperty.type === "file") result.push(tableBlock);
          });
        });
      }
    }
  });
  return result;
};

const initFileDownloader = () => {
  let fileModels = getFileModels();

  fileModels.forEach(fileModel => {
    let props = fileModel.asfProperty;
    let tmpView = view.playerView.getViewWithId(props.id, props.ownerTableId, props.tableBlockIndex);

    //отключаем все стандартные события
    setTimeout(() => {
      $(`[data-asformid="file.filename.${props.id}"]`).off();
      fileModel.on('valueChange', () => {
        $(`[data-asformid="file.filename.${props.id}"]`).off();
        return null;
      });
    }, 100);

    //вешаем свое событие
    if(tmpView) tmpView.container.on('click', () => {
      if(!fileModel.value) return;
      if(!fileModel.value.hasOwnProperty('identifier')) return;
      fileDownload(fileModel.value.identifier, fileModel.value.name);
    });

  });
};

if(window.location.href.indexOf('itsm-arm') !== -1) initFileDownloader();
