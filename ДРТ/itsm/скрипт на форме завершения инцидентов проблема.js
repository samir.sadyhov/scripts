const checkProblem = docID => {
  if(!editable) return;
  if(!docID) return;
  AS.FORMS.ApiUtils.getAsfDataUUID(docID)
  .then(uuid => AS.FORMS.ApiUtils.loadAsfData(uuid))
  .then(asfData => {
    let problem_status = asfData.data.find(x => x.id === 'itsm_form_problem_status');
    if(problem_status.hasOwnProperty('key')) {
      if(problem_status.key == '5') {
        alert('Выбранная проблема имеет статус "Закрыто". Выберите другую проблему');
        model.setValue(null);
      }
    } else {
      alert('Выбранная проблема имеет статус "Закрыто". Выберите другую проблему');
      model.setValue(null);
    }
  });
}
model.on('valueChange', (_1, _2, value) => checkProblem(value));
