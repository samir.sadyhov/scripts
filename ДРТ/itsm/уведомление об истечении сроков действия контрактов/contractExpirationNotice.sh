#!/bin/bash

#тут есть логин/пароль и хост синержи
source /opt/synergy/jboss/standalone/configuration/itsm.properties

# вспомогательные настройки
logFile="/var/log/synergy/scripts.log"
scriptName=${0##*/}
tmpfile=$(mktemp)

registryCode="reestr_dlya_otpravki_uvedomlenii_po_srokam_deistviya_kontraktov"

function now_time () {
  date +"%Y-%m-%d %H:%M:%S"
}
function logging () {
  echo -e "`now_time` #$scriptName# [$1] $2" >> $logFile
}

#Метод частичного сохранения данных по форме
# $1 передается жисонка в соответствии с описанием апи data/merge
function createDocRCC () {
  curl --user $login:$password -XPOST "$address/rest/api/registry/create_doc_rcc" --data "$1" -H "Content-Type: application/json; charset=utf-8" --output $tmpfile --silent 2>/dev/null
}

newDoc='{"registryCode": "'$registryCode'", "sendToActivation": true, "wasOpened": true}'
logging INFO "$newDoc"

#создаем запись в реестре "Реестр для отправки уведомлений по срокам действия контрактов"
createDocRCC "$newDoc"
logging RESULT "`cat $tmpfile`"


echo '' >> $logFile
logging STATUS "Удаление временных файлов"
rm -rf $tmpfile
logging END "Завершение работы скрипта"
exit 0
