const oldRequests = 10;
let fillUserID = '';

const clearTable = (table, cb) => {
  for(let i = 0; i < table.modelBlocks.length; i++) table.removeRow(i);
  setTimeout(() => {
    table.modelBlocks.length > 0 ? clearTable(table, cb) : cb();
  }, 0);
};

const filTable = (url, tableModel) => {
  AS.FORMS.ApiUtils.simpleAsyncGet(url).then(res => res.data.forEach(x => {
    tableModel.createRow()[0].setValue(x.documentID);
    model.playerModel.hasChanges = false;
  }));
};

const setContacts = userID => {
  let tablePhones = model.playerModel.getModelWithId('crm_form_contact_phone');

  if(!userID) {
    model.playerModel.getModelWithId('itsm_form_incident_address').setValue(null);
    clearTable(tablePhones, () => null);
    return;
  }

  AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/personalrecord/forms/${userID}`)
  .then(res => {
    res = res.find(x => x.formCode = "itsm_form_user_card");
    return AS.FORMS.ApiUtils.loadAsfData(res["data-uuid"]);
  })
  .then(cardData => {
    let address = cardData.data.find(x => x.id === 'itsm_form_usercard_address');
    let phoneWork = cardData.data.find(x => x.id === 'itsm_form_usercard_phone_work');
    let phoneMobile = cardData.data.find(x => x.id === 'itsm_form_usercard_phone_mobile');

    model.playerModel.getModelWithId('itsm_form_incident_address').setValue(address.value);

    clearTable(tablePhones, () => {
      if(phoneWork.hasOwnProperty('value')) {
        let rowWork = tablePhones.createRow();
        rowWork[0].setValue('1');
        rowWork[1].setValue(phoneWork.value);
      }
      if(phoneMobile.hasOwnProperty('value')) {
        let rowMobile = tablePhones.createRow();
        rowMobile[0].setValue('2');
        rowMobile[1].setValue(phoneMobile.value);
      }
      model.playerModel.hasChanges = false;
    });
  });
}

const initFill = user => {
  let tableNotes = model.playerModel.getModelWithId('itsm_form_incident_author_notes');
  let tableCi = model.playerModel.getModelWithId('itsm_form_incident_ciTable');

  if(user.length === 0) {
    clearTable(tableNotes, () => model.playerModel.hasChanges = false);
    clearTable(tableCi, () => model.playerModel.hasChanges = false);
    fillUserID = '';
    setContacts();
    return;
  }

  if(fillUserID == user[0].personID) return;
  fillUserID = user[0].personID;

  let urlnotes = "rest/api/registry/data_ext?loadData=false&&registryCode=itsm_registry_incidents&field=itsm_form_incident_author&condition=TEXT_EQUALS&key=" + user[0].personID + "&countInPart=" + oldRequests;
  let urlCI = "rest/api/registry/data_ext?loadData=false&&registryCode=itsm_registry_ci&field=itsm_form_ci_owner&condition=TEXT_EQUALS&key=" + user[0].personID;

  if(tableNotes.modelBlocks.length > 0) {
    clearTable(tableNotes, () => filTable(urlnotes, tableNotes));
  } else {
    filTable(urlnotes, tableNotes);
  }

  if(tableCi.modelBlocks.length > 0) {
    clearTable(tableCi, () => filTable(urlCI, tableCi));
  } else {
    filTable(urlCI, tableCi);
  }

  setContacts(user[0].personID);
};

setTimeout(() => {
  initFill(model.getValue());
}, 100);
if(editable) model.on('valueChange', (_1, _2, user) => initFill(user));
