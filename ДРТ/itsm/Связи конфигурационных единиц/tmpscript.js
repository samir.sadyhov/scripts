//'9ba20dd9-1233-45a4-af86-dee7c69af8a2'

function getCI(documentID) {
  return AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/asforms/search/advanced", null, null, JSON.stringify({
    query: `WHERE ci.DATE = '${documentID}' AND code = 'itsm_form_ci'`,
    dynParams: [["ci.DATE"]],
    showDeleted: false,
    searchInRegistry: true
  }), 'application/json; charset=utf-8');
}

function getInfoCI(documentID) {
  return AS.FORMS.ApiUtils.getAsfDataUUID(documentID).then(dataUUID => AS.FORMS.ApiUtils.loadAsfData(dataUUID));
}

function getValue(data, cmpID) {
  data = data.data ? data.data : data;
  for(let i = 0; i < data.length; i++)
  if (data[i].id === cmpID) return data[i];
}

function getTableBlockIndex(tableData, cmp) {
  let res = 0;
  tableData = tableData.data ? tableData.data : tableData;
  tableData.forEach(item => {
    if (item.id.slice(0, item.id.indexOf('-b')) === cmp) res++;
  });
  return res === 0 ? 1 : ++res;
}

function searchR(r, id) {
  let result = false;
  for(let i = 0; i < r.length; i++) {
    if(r[i].id === id) result = true;
  }
  return result;
}

function getRelations(asfData){
  let result = [];
  let tableRelations = getValue(asfData, 'itsm_form_ci_relations');

  if(tableRelations && tableRelations.data) {
    tableRelations.data.forEach((row, index) => {
      if (row.id.slice(0, row.id.indexOf('-b')) === 'ci') {
        result.push({
          id: row.key,
          name: row.value,
          type: tableRelations.data[index-1].key,
          relations: []
        });
      }
    });
  }
  return result;
}

// function s(id){
//   if(searchR(result.relations, id)) return;  //
//   getInfoCI(id).then(asfData => {
//     if(id === result.id) {
//       result.name = getValue(asfData, 'itsm_form_ci_name').value;
//     }
//   });
// }
// s(documentID);

function getAllCI(documentID) {

}

function selectCI(items) {
  let infoCI = [];

  items.forEach(id => {
    getInfoCI(id).then(asfData => {
      infoCI.push({
        id: id,
        name: getValue(asfData, 'itsm_form_ci_name').value,
        parent: true,
        relations: getRelations(asfData)
      });
    });
  });

  return infoCI;
}
