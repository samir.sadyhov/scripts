model.on('valueChange', (m, t, value) => {
  if(!value) return;
  AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/userchooser/search?showAll=true&departmentID=${value[0].departmentId}`)
  .then(usersDep => {
    let usersList = [];
    usersDep.forEach(x => usersList.push({personID: x.userID, personName: x.name}));
    model.playerModel.getModelWithId('itsm_form_problem_wcf_responsible').setValue(usersList);
  });
});
