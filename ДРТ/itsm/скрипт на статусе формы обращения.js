// Статусы инцидента
let NEW_STATUS = 1; // Зарегистрирован
let IN_QUEUE = 2; // На очереди
let IN_PROGRESS = 3; // В процессе
let WAITING_USER = 4; // Ожидает ответа пользователя
let WAITING_PROBLEM = 5; // Ожидает решение проблемы
let CLOSED = 6; // Закрыто
let WAITING_USER_RATE = 7; // Ожидает оценки пользователя
let WAITING_EQUIPMENT = 8; // В ожидании выделения техники
let WAITING_SUPPLY = 9; // Направлен внешнему поставщику
let INFO_GIVEN = 10; // Информация предоставлена
let WRONG_WAY = 11; // Неверно направлен
let RE_OPEN = 12; // Направлен повторно
let WAITING_MASS_INCIDENT = 13; //Ожидает закрытия массового инцидента
let IN_AGREEMENT = 91; // На согласовании
let AGREED = 15; // Согласовано
let NOT_AGREED = 16; // Не согласовано

function hideFields(){
    let val = model.value[0];
    if(view.playerView.getViewWithId('itsm_form_incident_supply_date')) view.playerView.getViewWithId('itsm_form_incident_supply_date').setVisible(val==WAITING_SUPPLY);
    if(view.playerView.getViewWithId('itsm_form_incident_supply_date_label')) view.playerView.getViewWithId('itsm_form_incident_supply_date_label').setVisible(val==WAITING_SUPPLY);
    if(view.playerView.getViewWithId('itsm_form_incident_equipment_date')) view.playerView.getViewWithId('itsm_form_incident_equipment_date').setVisible(val==WAITING_EQUIPMENT);
    if(view.playerView.getViewWithId('itsm_form_incident_equipment_date_label')) view.playerView.getViewWithId('itsm_form_incident_equipment_date_label').setVisible(val==WAITING_EQUIPMENT);
    if(view.playerView.getViewWithId('itsm_form_incident_supplier')) view.playerView.getViewWithId('itsm_form_incident_supplier').setVisible(val==WAITING_SUPPLY);
    if(view.playerView.getViewWithId('itsm_form_incident_supplier_label')) view.playerView.getViewWithId('itsm_form_incident_supplier_label').setVisible(val==WAITING_SUPPLY);
    if(view.playerView.getViewWithId('itsm_form_incident_comment')) view.playerView.getViewWithId('itsm_form_incident_comment').setVisible(val==WRONG_WAY);
    if(view.playerView.getViewWithId('itsm_form_incident_comment_label')) view.playerView.getViewWithId('itsm_form_incident_comment_label').setVisible(val==WRONG_WAY);
    if(view.playerView.getViewWithId('itsm_form_incident_return_to_author')) view.playerView.getViewWithId('itsm_form_incident_return_to_author').setVisible(val==WAITING_USER);
    if(view.playerView.getViewWithId('itsm_form_incident_return_to_author_label')) view.playerView.getViewWithId('itsm_form_incident_return_to_author_label').setVisible(val==WAITING_USER);

    if(view.playerView.getViewWithId('itsm_form_incident_mark')) view.playerView.getViewWithId('itsm_form_incident_mark').setVisible(val==CLOSED);
    if(view.playerView.getViewWithId('itsm_form_incident_mark_label')) view.playerView.getViewWithId('itsm_form_incident_mark_label').setVisible(val==CLOSED);
    if(view.playerView.getViewWithId('itsm_form_incident_mark_comment')) view.playerView.getViewWithId('itsm_form_incident_mark_comment').setVisible(val==CLOSED || val==RE_OPEN);
    if(view.playerView.getViewWithId('itsm_form_incident_mark_comment_label')) view.playerView.getViewWithId('itsm_form_incident_mark_comment_label').setVisible(val==CLOSED || val==RE_OPEN);

    if(view.playerView.getViewWithId('itsm_form_incident_major_problemlink_label')) view.playerView.getViewWithId('itsm_form_incident_major_problemlink_label').setVisible(val==WAITING_PROBLEM);
    if(view.playerView.getViewWithId('itsm_form_incident_major_problemlink')) view.playerView.getViewWithId('itsm_form_incident_major_problemlink').setVisible(val==WAITING_PROBLEM);

    if(view.playerView.getViewWithId('itsm_form_incident_approval_comment')) view.playerView.getViewWithId('itsm_form_incident_approval_comment').setVisible(val==AGREED || val==NOT_AGREED);
    if(view.playerView.getViewWithId('itsm_form_incident_approval_comment_label')) view.playerView.getViewWithId('itsm_form_incident_approval_comment_label').setVisible(val==AGREED || val==NOT_AGREED);
    if(view.playerView.getViewWithId('itsm_form_incident_approval_users')) view.playerView.getViewWithId('itsm_form_incident_approval_users').setVisible(val==IN_AGREEMENT || val==AGREED || val==NOT_AGREED);
    if(view.playerView.getViewWithId('itsm_form_incident_approval_users_label')) view.playerView.getViewWithId('itsm_form_incident_approval_users_label').setVisible(val==IN_AGREEMENT || val==AGREED || val==NOT_AGREED);

    if(view.playerView.getViewWithId('itsm_form_incident_approval_description_label')) view.playerView.getViewWithId('itsm_form_incident_approval_description_label').setVisible(val==IN_AGREEMENT);
    if(view.playerView.getViewWithId('itsm_form_incident_approval_description')) view.playerView.getViewWithId('itsm_form_incident_approval_description').setVisible(val==IN_AGREEMENT);

    if(view.playerView.getViewWithId('mass_incident_label')) view.playerView.getViewWithId('mass_incident_label').setVisible(val==WAITING_MASS_INCIDENT);
    if(view.playerView.getViewWithId('itsm_form_incident_mass_reglink')) view.playerView.getViewWithId('itsm_form_incident_mass_reglink').setVisible(val==WAITING_MASS_INCIDENT);

    view.setEnabled(AS.OPTIONS.currentUser.userId==1 || AS.OPTIONS.currentUser.userId=='f7abd6d9-7b92-4da4-a183-d53feaee2296');

    if(window.location.href.indexOf('Synergy') !== -1) {
      let buttonProblem = $(`[document_id="${getDocID()}"]`).closest('tbody').find('[data-button="APPROVE"]:contains("В проблему")').closest('tr');
      if([IN_PROGRESS].indexOf(Number(val)) === -1) {
        buttonProblem.hide();
      } else {
        buttonProblem.show();
      }
    }
}

function getDocID() {
  return window.location.href.split("#")[1].split('&').filter(x => x.indexOf('document_identifier') !== -1)[0].split('=')[1];
}

hideFields();
model.on('valueChange', () => hideFields());
