// Статусы проблем
let NEW_STATUS = 1; // Зарегистрирована
let IN_QUEUE = 2; // На очереди
let IN_PROGRESS = 3; // Расследование
let WAITING_CHANGE = 4; // Ожидает изменений
let CLOSED = 5; // Закрыто

let fixDoubleChangeDist = false; // от дубликатов изменения справочника


function hideFields() {
  var val = model.value[0];
  view.playerView.getViewWithId('itsm_form_problem_wcf_responsible').setVisible(val==2);
  view.playerView.getViewWithId('itsm_form_problem_wcf_responsible_label').setVisible(val==2);
  view.playerView.getViewWithId('itsm_form_problem_wcf_responsibleDepartment').setVisible(val==2);
  view.playerView.getViewWithId('itsm_form_problem_wcf_responsibleDepartment_inprogress').setVisible(val==3);
  view.playerView.getViewWithId('itsm_form_problem_wcf_responsible_inprogress_label').setVisible(val==3);
  view.playerView.getViewWithId('itsm_form_problem_wcf_responsible_inprogress').setVisible(val==3);
  view.playerView.getViewWithId('itsm_form_problem_wcf_decisiondescription').setVisible(val==5);
  view.playerView.getViewWithId('itsm_form_problem_wcf_decisiondescription_label').setVisible(val==5);
  view.playerView.getViewWithId('itsm_form_problem_wcf_decisiontype').setVisible(val==5);
  view.playerView.getViewWithId('itsm_form_problem_wcf_decisiontype_label').setVisible(val==5);
  view.playerView.getViewWithId('itsm_form_problem_wcf_current_date_label').setVisible(val==5);
  view.playerView.getViewWithId('itsm_form_problem_wcf_current_date').setVisible(val==5);
}

function newValueList(list) {
  let curList = model.listElements.length > model.listCurrentElements.length ? model.listElements : model.listCurrentElements;
  let curVal = model.getValue()[0];
  model.listElements = [];
  model.listCurrentElements = [];
  curList = curList.filter(i => list.indexOf(+i.value) !== -1);
  model.listElements = curList;
  model.listCurrentElements = curList;
  if (list.indexOf(+curVal) === -1) model.setValue('');
}


function parseDateTime(datetime /*yyyy-mm-dd HH:MM:SS*/ ) {
  let date = datetime.split(' ')[0].split('-');
  let time = datetime.split(' ')[1].split(':');
  return new Date(date[0], date[1] - 1, date[2], time[0], time[1], time[2]);
}

function getListStateProgress(processes) {
  let result = [];
  function search(p) {
    p.forEach(item => {
      if (item.subProcesses.length > 0) {
        search(item.subProcesses);
      } else if (item.code && !item.finished && item.typeID == 'ASSIGNMENT_ITEM') {
        result.push(item);
      }
    });
  }
  search(processes);
  return result.sort((a, b) => parseDateTime(a.started) - parseDateTime(b.started))[result.length - 1];
};


model.playerModel.on('dataLoad', function() {
  hideFields();

  let api = AS.FORMS.ApiUtils;
  let documentID = null;

  api.getDocumentIdentifier(model.playerModel.asfDataId)
  .then(docID => {
    documentID = docID;
    return api.getAsfDataUUID(docID);
  })
  .then(uuid => api.loadAsfData(uuid))
  .then(asfData => {

    if (!fixDoubleChangeDist) {
      fixDoubleChangeDist = true;

      try {
        AS.SERVICES.showWaitWindow();

        api.simpleAsyncGet(`rest/api/workflow/get_execution_process?documentID=${documentID}`).then(responseCode => {
          responseCode = getListStateProgress(responseCode);

          if (responseCode === undefined) responseCode = {code: false};

          console.log('responseCode', responseCode);

          switch (responseCode.code) {

            // Статус 1 Зарегистрирован
            case 'problem_reg':
              newValueList([
                IN_QUEUE,
                CLOSED
              ]);
              break;

            // Статус 2 На очереди
            case 'problem_queue':
              newValueList([
                IN_PROGRESS,
                CLOSED
              ]);
              break;

            // Статус 3 В расследовании
            case 'problem_progress':
              newValueList([
                CLOSED
              ]);
              break;

            // Статус 4 Ожидает изменений
            case 'problem_change':
              newValueList([
                NEW_STATUS,
                IN_QUEUE,
                IN_PROGRESS,
                WAITING_CHANGE,
                CLOSED
              ]);
              break;
          }

          AS.SERVICES.hideWaitWindow();
        });
      } catch (err) {
        console.error(err, err.message);
        AS.SERVICES.hideWaitWindow();
      }
    }

  });
});

hideFields();
model.on('valueChange',hideFields);
