const checkMassIncident = docID => {
  if(!editable) return;
  if(!docID) return;
  AS.FORMS.ApiUtils.getAsfDataUUID(docID)
  .then(uuid => AS.FORMS.ApiUtils.loadAsfData(uuid))
  .then(asfData => {
    let incident_major = asfData.data.find(x => x.id === 'itsm_form_incident_major');
    if(incident_major.hasOwnProperty('values')) {
      if(incident_major.values[0] != '1') {
        alert('Данное обращение не является массовым инцидентом. Выберите другое обращение');
        model.setValue(null);
      }
    } else {
      alert('Данное обращение не является массовым инцидентом. Выберите другое обращение');
      model.setValue(null);
    }
  });
}
model.on('valueChange', (_1, _2, value) => checkMassIncident(value));
