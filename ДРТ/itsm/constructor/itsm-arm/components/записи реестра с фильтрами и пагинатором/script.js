!function(i){i.widget("ih.resizableColumns",{_create:function(){this._initResizable()},_initResizable:function(){let e,t,n,s=this.element;s.find("th").resizable({handles:{e:" .resizeHelper"},minWidth:10,create:function(e,t){let n=i(this).find(".columnLabel").width();n&&(n+=i(this).find(".ui-resizable-e").width(),i(this).resizable("option","minWidth",n))},start:function(i,h){let l=h.helper.index()+1;e=s.find("colgroup > col:nth-child("+l+")"),t=parseInt(e.get(0).style.width,10),n=h.size.width},resize:function(s,h){let l=h.size.width-n,d=t+l;e.width(d),i(this).css("height","auto")}})}})}(jQuery);

const registries = Cons.getAppStore().registries;
let tableContainer = $('.itsm-table-container');
let filterContainer = $('.itsm-filter-container');

tableContainer.empty();
filterContainer.empty();
$('.itsm-registry-container').parent().parent().css({"width": "100%"});

function subName(name, num){
  if(!name) return '-';
  return name.length > num ? name.substring(0, num) + '..' : name;
}

function getRegInfo(code) {
  let result;
  if(Cons.getCurrentPage().code == 'settings_page') {
    const registrySettings = Cons.getAppStore().registrySettings;
    if(registrySettings) {
      const reg = registrySettings.find(x => x.registryCode == code);
      result = {
        registryCode: reg.registryCode,
        rights: reg.rights,
        formName: reg.registryName
      }
    }
  } else {
    registries.forEach(item => {
      if (item.children) {
        item.children.forEach(child => {
          if (child.code == code) result = child;
        })
      } else {
        if (item.code == code) result = item;
      }
    });
  }
  return result;
}

const refreshRows = () => {
  let data = {
    filterCode: registryTable.filterCode,
    filterID: registryTable.filterID,
    filterRights: registryTable.filterRights,
    registryCode: registryTable.registryCode
  }
  registryTable.init(data);
  initRegistryFilters(data);
  itsmPaginator.init();
}

let itsmPaginator = {
  container: $('<div class="itsm-pt-container">'),
  paginator: $('<div class="itsm-pt-paginator">'),
  pContent: $('<div class="itsm-pt-paginator-content">'),
  bPrevious: $('<button class="itsm-pt-previous" disabled="disabled" title="Назад">'),
  bNext: $('<button class="itsm-pt-next" disabled="disabled" title="Вперед">'),
  label: $('<label>'),
  input: $('<input type="text">'),
  countInPart: 0,
  rows: 0,
  currentPage: 1,
  pages: 0,

  init: function(){
    $('.itsm-pt-container').remove();
    $('.itsm-registry-container').after(this.container);
    this.pContent.append(this.label).append(this.input);
    this.paginator.append(this.bPrevious).append(this.pContent).append(this.bNext);
    this.container.append(this.paginator);
    this.reset();

    this.bNext.click(() => {
      this.currentPage++;
      this.update();
      registryTable.createBody();
    });

    this.bPrevious.click(() => {
      this.currentPage--;
      this.update();
      registryTable.createBody();
    });

    this.label.on('dblclick', e => {
      e.preventDefault();
      this.input.show();
      this.label.hide();

      this.input.on('blur', () => {
        this.label.show();
        this.input.hide();
      });

      this.input.val("" + this.currentPage);

      this.input.on('keypress', e => {
        if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) return false;
      });

      this.input.on('keydown', e => {
        if(e.which === 13) {
          let inputVal = +this.input.val();
          if(inputVal && inputVal != this.currentPage && inputVal <= this.pages && inputVal >= 1){
            this.currentPage = inputVal;
            this.update();
            registryTable.createBody();
          }
          this.label.show();
          this.input.hide();
          this.input.off();
        }
      });
      this.input.focus();
    });

  },

  update: function(){
    this.pages = Math.ceil(this.rows / this.countInPart),
    this.label.text(this.currentPage + ' / ' + this.pages);

    if(this.pages == 0) {
      this.bPrevious.attr('disabled', 'disabled');
      this.bNext.attr('disabled', 'disabled');
    } else {
      if(this.currentPage == 1) {
        this.bPrevious.attr('disabled', 'disabled');
        if (this.currentPage == this.pages) {
          this.bNext.attr('disabled', 'disabled');
        } else {
          this.bNext.removeAttr('disabled');
        }
      } else {
        this.bPrevious.removeAttr('disabled');
        if (this.currentPage == this.pages) {
          this.bNext.attr('disabled', 'disabled');
        } else {
          this.bNext.removeAttr('disabled');
        }
      }
    }

    let cookiePages = cookie.getJson('pages');
    if(cookiePages.hasOwnProperty('param')) {
      cookiePages.param.currentPage = this.currentPage;
      cookie.setJson('pages', cookiePages);
    }

  },

  reset: function(){
    this.countInPart = 15;
    this.rows = 15;
    this.currentPage = 1;
    this.pages = 0;
  }

}

let registryTable = {
  registryName: '',
  registryCode: null,
  registryID: null,
  registryRights: [],

  formName: '',

  filterName: null,
  filterCode: null,
  filterID: null,
  filterRights: [],

  allRights: [],

  sortCmpID: null,
  sortDesc: false,
  searchField: null,
  searchValue: null,
  heads: [],
  selectedItems: [],

  registryTable: null,
  colgroup: null,
  tHead: null,
  tBody: null,

  goFormPage: function(uuid, code, name, editable) {

    let regInfo = getRegInfo(code);
    let event = {
      dataUUID: uuid,
      registryCode: code,
      registryName: name,
      editable: editable || false
    };
    if($(document).width() < 769) event.viewCode = 'mobile';

    if(regInfo && regInfo.hasOwnProperty('formName')) {
      event.formName = regInfo.formName;
      $('#root-panel').trigger({type: 'custom_open_document', eventParam: event});
      // fire(event, 'root-panel');
    } else {
      AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/info?code=${code}`).then(res => {
        event.registryName = res.name;
        return AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/asforms/form/${res.formId}`);
      }).then(res => {
        event.formName = res.nameru;
        $('#root-panel').trigger({type: 'custom_open_document', eventParam: event});
      });
    }
  },

  getUrl: function(){
    let url = `api/registry/data_ext?registryCode=${this.registryCode}`;
    url += `&pageNumber=${itsmPaginator.currentPage - 1}&countInPart=${itsmPaginator.countInPart}`;
    if(this.filterCode) url+=`&filterCode=${this.filterCode}`;
    if(this.heads && this.heads.length > 0) this.heads.forEach(item => url+=`&fields=${item.columnID}`);
    if(this.sortCmpID) url+=`&sortCmpID=${this.sortCmpID}&sortDesc=${this.sortDesc}`;
    if(this.searchField && this.searchValue) url+=`&field=${this.searchField}&condition=CONTAINS&value=${this.searchValue}`;
    return url;
  },

  documentInfo: function(dataRow, e){
    function createRow(label, value) {
      return $(`<tr><td>${label}</td><td>${value}</td></tr>`);
    }

    let dialog = $('<div class="uk-flex-top" uk-modal>');
    let md = $('<div class="uk-modal-dialog uk-margin-auto-vertical">');
    let modalBody = $('<div class="uk-modal-body" uk-overflow-auto>');
    dialog.append(md);
    md.append('<button class="uk-modal-close-default" type="button" uk-close></button>')
    .append('<div class="uk-modal-header"><h2 class="uk-modal-title">Свойства документа</h2></div>')
    .append(modalBody)
    .append('<div class="uk-modal-footer uk-text-right"><button class="uk-button uk-button-default uk-modal-close" type="button">Закрыть</button></div>');

    Cons.showLoader();
    try {
      rest.synergyGet(`api/docflow/doc/document_info?documentID=${dataRow.documentID}`, res => {
        let container = $('<div>', {class: 'itsm-table-container'});
        let table = $('<table>', {class: 'itsm-custom-table'});
        let body = $('<tbody>');
        let thead = $('<thead>').append('<tr><th>Параметр</th><th>Значение</th></tr>');

        body.append(createRow('Краткое содержание документа', res.name));
        body.append(createRow('Дата создания', res.createDate));
        body.append(createRow('Содержимое', res.content));
        body.append(createRow('Автор документа', res.author));
        body.append(createRow('Исполнитель', res.resUsers));
        body.append(createRow('Направлено', res.to));

        table.append(thead).append(body);
        container.append(table);
        modalBody.append(container);
        Cons.hideLoader();
      });
    } catch (err) {
      Cons.hideLoader();
      modalBody.append('<p>Произошла ошибка получения данных по документу</p>');
      console.log(error);
    }
    e.preventDefault();
    e.target.blur();
    UIkit.modal(dialog).show();
    dialog.on('hidden', () => dialog.remove());
  },

  removeRegistryRow: function(uuid, e) {
    e.preventDefault();
    e.target.blur();
    UIkit.modal.confirm('Вы действительно хотите удалить запись реестра?').then(() => {
      Cons.showLoader();
      try {
        rest.synergyGet(`api/registry/delete_doc?dataUUID=${uuid}`, res => {
          if(res.errorCode != '0') throw new Error(res.errorMessage);
          itsmShowMessage({message: "Запись реестра успешно удалена", title: "Удаление"});
          this.createBody();
          Cons.hideLoader();
        });
      } catch (err) {
        Cons.hideLoader();
        itsmShowMessage({message: "Произошла ошибка при удалении записи реестра", type: 'error'});
        console.log(error);
      }
    }, () => null);
  },

  contextMenu: function(el, dataRow){
    let isDelete = false;
    if(this.allRights.indexOf('rr_delete') !== -1 && dataRow.status != 'STATE_NOT_FINISHED') {
      isDelete = true;
    }
    el.on('contextmenu', event => {
      $('.itsm-context-menu').remove();
      $('<div>', {class: 'itsm-context-menu'})
      .css({
        "left": event.pageX + 'px',
        "top": event.pageY + 'px',
        "min-width": "200px"
      })
      .appendTo('body')
      .append(
        $('<ul class="uk-nav-default uk-nav-parent-icon" uk-nav>')
        .append($(`<li ><a href="javascript:void(0);"><span class="uk-margin-small-right" uk-icon="icon: push"></span>Открыть</a></li>`)
          .on('click', e => {
            Cons.setAppStore({dataRow: dataRow});
            this.goFormPage(dataRow.dataUUID, this.registryCode, this.registryName);
          })
        )
        .append($('<li><a href="javascript:void(0);"><span class="uk-margin-small-right" uk-icon="icon: info"></span>Информация</a></li>')
          .on('click', e => this.documentInfo(dataRow, e))
        )
        .append('<li class="uk-nav-divider"></li>')
        .append($(`<li ${isDelete ? '' : 'class="uk-disabled"'} ><a href="javascript:void(0);"><span class="uk-margin-small-right" uk-icon="icon: trash"></span>Удалить запись</a></li>`)
          .on('click', e => {
            isDelete ? this.removeRegistryRow(dataRow.dataUUID, e) : false;
          })
        )
      )
      .show('fast');
      return false;
    });
  },

  createRow: function(dataRow) {
    let tr = $('<tr>');

    if(this.registryCode == 'itsm_registry_incidents') {
      let checkbox = $('<input/>').addClass('uk-checkbox').attr('type', 'checkbox');
      if(this.selectedItems.indexOf(dataRow.documentID) !== -1) checkbox.prop('checked', true);
      tr.append($('<td>').addClass('uk-table-shrink').append(checkbox));

      checkbox.on('change', e => {
        if(e.target.checked) {
          if(this.selectedItems.indexOf(dataRow.documentID) === -1) {
            this.selectedItems.push(dataRow.documentID);
          }
          $('.itsm-menu-content-right>.itsm-mobile-registry-actions').show();
        } else {
          let index = this.selectedItems.indexOf(dataRow.documentID);
          if(index !== -1) this.selectedItems.splice(index, 1);
          $('.itsm-menu-content-right>.itsm-mobile-registry-actions').hide();
        }
        $('.itsm-massactions-container').trigger('additems');
      });
    }


    this.heads.forEach(item => {
      let td = $('<td>');
      if (dataRow.fieldValue.hasOwnProperty(item.columnID)) {
        td.text(dataRow.fieldValue[item.columnID]);
        td.attr('title', dataRow.fieldValue[item.columnID]);
      }
      tr.append(td);
    });

    this.contextMenu(tr, dataRow);

    tr.on('click', (e) => {
      this.tBody.find('tr').removeAttr('selected');
      tr.attr('selected', true);
      if($(document).width() < 960) {
        if($(e.target).hasClass("uk-checkbox")) return;
        if(this.allRights.indexOf('rr_read') !== -1) {
          Cons.setAppStore({dataRow: dataRow});
          this.goFormPage(dataRow.dataUUID, this.registryCode, this.registryName);
        } else {
          itsmShowMessage({message: 'У вас нет прав на просмотр этого документа', type: 'warning'});
        }
      }
    });
    tr.on('dblclick', () => {
      if(this.allRights.indexOf('rr_read') !== -1) {
        Cons.setAppStore({dataRow: dataRow});
        this.goFormPage(dataRow.dataUUID, this.registryCode, this.registryName);
      } else {
        itsmShowMessage({message: 'У вас нет прав на просмотр этого документа', type: 'warning'});
      }
    });

    return tr;
  },

  createBody: function() {
    Cons.showLoader();
    rest.synergyGet(this.getUrl(), data => {
      this.tBody.empty();
      data.result.forEach(item => this.tBody.append(this.createRow(item)));
      itsmPaginator.rows = data.recordsCount;
      itsmPaginator.update();
      $('.itsm-table-container').scrollTop(0);
      Cons.hideLoader();
    });
  },

  resetHeaderLabel: function(){
    $('.itsm-pt-header > label').each((i, el) => $(el).text(this.heads[i].label).css({"text-decoration": "none"}));
  },

  createHeader: function() {
    Cons.showLoader();
    this.tHead.empty();
    this.colgroup.empty();
    rest.synergyGet(`api/registry/info?code=${this.registryCode}`, info => {
      this.heads = info.columns.filter(item => item.visible != '0')
      .sort((a, b) => {
        if (a.order == 0) return 0;
        return a.order - b.order;
      })
      .map(item => {
        return {label: item.label, columnID: item.columnID}
      });

      let rInfo = getRegInfo(this.registryCode);

      this.registryID = info.registryID;
      this.registryName = info.name;
      this.formName = rInfo.formName;
      let tr = $('<tr class="colHeaders">');

      if(this.registryCode == 'itsm_registry_incidents') {
        this.colgroup.append(`<col style="width: 50px">`);
        let th = $('<th class="ui-resizable">');
        let checkbox = $('<input/>')
          .addClass('uk-checkbox')
          .attr('type', 'checkbox')
          .on('change', e => {
            this.tBody.find('td > [type="checkbox"]').each((k, x) => {
              x.checked = !!e.target.checked;
            });
            this.tBody.find('td > [type="checkbox"]').each((k, x) => {
              $(x).trigger('change');
            });
          });
        th.append(checkbox);
        tr.append(th);
      }

      let me = this;
      this.heads.forEach(item => {
        this.colgroup.append(`<col style="width: 100px">`);
        let tmp_th = $('<th class="itsm-pt-header ui-resizable">');
        let tmp_label = $('<label class="columnLabel">').text(item.label);
        let tmp_imput = $('<input type="text" class="uk-input">').css({'height': 'auto'}).hide();
        tmp_th.append(tmp_label).append(tmp_imput)
        .append(`<div class="resizeHelper ui-resizable-handle ui-resizable-e">&nbsp;</div>`);
        tr.append(tmp_th);

        let timeoutId;
        tmp_label.on("click", function (e) {
          timeoutId = setTimeout(() => {
            if(!timeoutId) return;
            me.tHead.find("th").not(tmp_th).find('label').removeClass("itsm-pt-sorted-asc itsm-pt-sorted-desc");
            if (tmp_label.hasClass("itsm-pt-sorted-asc") || tmp_label.hasClass("itsm-pt-sorted-desc")) {
              tmp_label.toggleClass("itsm-pt-sorted-asc itsm-pt-sorted-desc");
            } else {
              tmp_label.addClass("itsm-pt-sorted-asc");
            }
            me.sortCmpID = item.columnID;
            me.sortDesc = tmp_label.hasClass("itsm-pt-sorted-asc") ? false : true;
            me.createBody();
          }, 200);
        });

        tmp_label.on('dblclick', e => {
          clearTimeout(timeoutId);
          timeoutId = null;
          e.preventDefault();
          tmp_imput.show();
          tmp_label.hide();
          tmp_imput.on('blur', () => {
            tmp_label.show();
            tmp_imput.hide();
          });
          tmp_imput.on('keydown', e => {
            if(e.which === 13) {
              let inputVal = tmp_imput.val();
              this.resetHeaderLabel();
              if(inputVal && inputVal !== '') {
                tmp_label.text(inputVal).css({"text-decoration": "underline"});
                this.searchField = item.columnID;
                this.searchValue = inputVal;
              } else {
                tmp_label.text(item.label).css({"text-decoration": "none"});
                this.searchField = null;
                this.searchValue = null;
              }
              this.createBody();
              tmp_label.show();
              tmp_imput.hide();
              tmp_imput.off();
            }
          });
          tmp_imput.focus();
        });

      });
      this.tHead.empty();
      setTimeout(() => {
        this.tHead.append(tr);
        this.createBody();
        setTimeout(() => {
          this.registryTable.resizableColumns();
          Cons.hideLoader();
        }, 500);
      }, 100);
    });
  },

  massActionChange: function(item){
    Cons.showLoader();

    let regName = 'Операции над выборкой';
    let regCode = 'itsm_registry_incidents_operation';
    let asfData = [];

    asfData.push(UTILS.createField({
      "id": "itsm_form_incidents_operation_records",
      "type": "reglink",
      "key": this.selectedItems.join(';'),
      "valueID": this.selectedItems.join(';')
    }));
    asfData.push(UTILS.createField({
      "id": "itsm_form_incidents_operation_status",
      "type": "listbox",
      "value": item.status.value,
      "key": item.status.key
    }));

    let body = {"registryCode": regCode, "data": asfData};

    try {
      AS.FORMS.ApiUtils.simpleAsyncPost('rest/api/registry/create_doc_rcc', null, null, JSON.stringify(body), "application/json; charset=utf-8")
      .then(res => {
        if(res.errorCode != '0') throw new Error(res.errorMessage);
        Cons.hideLoader();
        Cons.setAppStore({dataRow: res});
        this.goFormPage(res.dataID, regCode, regName, true);
      });
    } catch (e) {
      itsmShowMessage({message: "Произошла ошибка при создании записи реестра", type: 'error'});
      console.log(e);
      Cons.hideLoader();
    }

  },

  massActionsInit: function(){
    if(this.registryCode !== 'itsm_registry_incidents') return;
    let container = $('.itsm-massactions-container');
    let massActionsMenu = $('.itsm-massactions-menu');
    let mobileMenuNav = $('<ul class="uk-nav-default uk-nav-parent-icon" uk-nav></ul>');

    massActionsMenu.empty();
    mobileMenuNav.append('<li class="uk-nav-header">Массовые действия</li>');

    setTimeout(() => {
      $('.itsm-mobile-registry-actions').empty().append(mobileMenuNav);
    }, 600);

    let items = [
      {title: 'На очередь', status: {value: 'На очередь', key: '2'}, icon: 'comments'},
      {title: 'Ожидает оценки пользователя', status: {value: 'Ожидает оценки пользователя', key: '7'}, icon: 'star'},
      {title: 'Ожидает закрытия массового инцидента', status: {value: 'Ожидает закрытия массового инцидента', key: '13'}, icon: 'future'},
      {title: 'Ожидает ответа пользователя', status: {value: 'Ожидает ответа пользователя', key: '4'}, icon: 'question'}
    ];

    items.forEach(item => {
      let row = $(`<li><a href="#"><span class="uk-margin-small-right" uk-icon="icon: ${item.icon}"></span>${item.title}</a></li>`);
      row.on('click', e => {
        this.massActionChange(item);
        e.preventDefault();
        e.target.blur();
      })
      massActionsMenu.append(row);
      row.clone(true).appendTo(mobileMenuNav);
    });
    mobileMenuNav.append('<li class="uk-nav-divider"></li>');

    container.on('additems', () => {
      if(this.selectedItems.length > 0) {
        container.css({display: 'flex'});
      } else {
        container.css({display: 'none'});
      }
    });
  },

  printReport: function(defaultName, requestUrl){
    Cons.showLoader();
    try {
      let xhr = new XMLHttpRequest();
      xhr.open('POST', requestUrl, true);
      xhr.setRequestHeader("Authorization", "Basic " + btoa(unescape(encodeURIComponent(AS.OPTIONS.login + ":" + AS.OPTIONS.password))));
      xhr.responseType = 'arraybuffer';
      xhr.onload = function () {
        if (this.status === 200) {
          let filename = defaultName;
          let type = xhr.getResponseHeader('Content-Type');
          let blob = new Blob([this.response], {type: type});
          if (typeof window.navigator.msSaveBlob !== 'undefined') {
            window.navigator.msSaveBlob(blob, filename);
          } else {
            let URL = window.URL || window.webkitURL;
            let downloadUrl = URL.createObjectURL(blob);
            if (filename) {
              let a = document.createElement("a");
              if (typeof a.download === 'undefined') {
                window.location = downloadUrl;
              } else {
                a.href = downloadUrl;
                a.download = filename;
                document.body.appendChild(a);
                a.click();
              }
            } else {
              window.location = downloadUrl;
            }
            setTimeout(function () {
              URL.revokeObjectURL(downloadUrl);
            }, 100);
          }
          Cons.hideLoader();
        } else {
          Cons.hideLoader();
          console.log(this.status, this);
          itsmShowMessage({
            message: "Произошла ошибка при формировании отчета",
            type: 'error'
          });
        }
      };
      xhr.send();
    } catch (e) {
      console.log(e);
      Cons.hideLoader();
      itsmShowMessage({
        message: "Произошла ошибка при формировании отчета",
        type: 'error'
      });
    }
  },

  listValues: {},
  getParamRow: function(param){
    let paramRow = $('<div class="uk-margin">');
    let label = $(`<label class="uk-form-label" for="${param.code}">${param.label}</label>`);
    let controls = $('<div class="uk-form-controls">');

    switch (param.type) {
      case "java.util.Date":
        let inputDate = $(`<input class="uk-input fonts" id="${param.code}" type="date">`);
        inputDate.val(new Date().toISOString().substring(0, 10));
        controls.append(inputDate);
        break;
      case "java.util.List":
        let p = param.code.split('.');
        let dictCode = p[0];
        let dictValue = p[1];
        let dictKey = p[2].split('_')[0];

        let scrollable = $(`<div id="${param.code}" class="uk-panel uk-panel-scrollable">`);
        let ul = $('<ul class="uk-list">');

        scrollable.append(ul);
        controls.append(scrollable);

        this.listValues[param.code] = [];
        rest.synergyGet(`api/dictionaries/${dictCode}`, dict => {
          let items = [];
          for (let key in dict.items) {
            items.push({value: dict.items[key][dictValue].value, key: dict.items[key][dictKey].value});
          }

          items = items.sort((a, b) => {
            a = a.value.toUpperCase();
            b = b.value.toUpperCase();
            if (a > b) return 1;
            if (a < b) return -1;
            return 0;
          });

          items.forEach(item => {
            let li = $('<li>');
            let checkbox = $('<input/>').addClass('uk-checkbox').attr('type', 'checkbox');
            let label = $('<label>')
            .addClass('fonts dict-menu-item')
            .append(checkbox)
            .append(`<sapn> ${item.value}</span>`);

            checkbox.on('change', e => {
              if(e.target.checked) {
                if(this.listValues[param.code].indexOf(item.key) === -1)
                this.listValues[param.code].push(item.key);
              } else {
                let index = this.listValues[param.code].indexOf(item.key);
                if(index !== -1) this.listValues[param.code].splice(index, 1);
              }
            });

            ul.append(li.append(label));
          });
        });
        break;
      default:
        if(param.code.substr(0, 8) == 'registry') {
          let paramRegCode = param.code.split('.')[1];
          let buttonReg = $('<a class="uk-form-icon uk-form-icon-flip" href="#" uk-icon="icon: more"></a>');
          let inputReg = $(`<input id="${param.code}" class="uk-input" type="text" style="background-color: #fff; color: #666;" disabled>`);
          let selectDocId = null;

          buttonReg.on('click', e => {
            e.preventDefault();
            e.target.blur();
            e.stopPropagation();

            Cons.showLoader();
            AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/info?code=${paramRegCode}`, registryInfo => {
              Cons.hideLoader();
              AS.SERVICES.showRegistryLinkDialog(registryInfo, false, selectDocId, documentId => {
                selectDocId = [documentId];
                AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/formPlayer/getDocMeaningContent?documentId=${documentId}`, content => {
                  inputReg.val(content).attr('documentId', documentId);
                }, 'text');
              });
            });
          });

          controls.append($('<div class="uk-inline uk-width-1-1">').append(buttonReg).append(inputReg));

        } else {
          switch (param.code) {
            case "departments_id":
              let selectDep = null;
              let button = $('<a class="uk-form-icon uk-form-icon-flip" href="#" uk-icon="icon: more"></a>');
              let input = $(`<input id="${param.code}" class="uk-input" type="text" style="background-color: #fff; color: #666;" disabled>`);

              button.on('click', e => {
                e.preventDefault();
                e.target.blur();
                e.stopPropagation();

                AS.SERVICES.showDepartmentChooserDialog(selectDep, true, null, null, null, null, AS.OPTIONS.locale, departments => {
                  selectDep = departments;
                  let depIDs = departments.map(x => x.departmentId).join(',');
                  let depNames = departments.map(x => x.departmentName).join('; ');
                  input.val(depNames).attr('title', depNames).attr('dep-ids', depIDs);
                });
              });

              controls.append($('<div class="uk-inline uk-width-1-1">').append(button).append(input));
              break;
            case "department_id":
              let button2 = $('<a class="uk-form-icon uk-form-icon-flip" href="#" uk-icon="icon: more"></a>');
              let input2 = $(`<input id="${param.code}" class="uk-input" type="text" style="background-color: #fff; color: #666;" disabled>`);

              button2.on('click', e => {
                e.preventDefault();
                e.target.blur();
                e.stopPropagation();

                AS.SERVICES.showDepartmentChooserDialog(null, false, null, null, null, null, AS.OPTIONS.locale, dep => {
                  input2.val(dep[0].departmentName).attr('title', dep[0].departmentName).attr('dep-ids', dep[0].departmentId);
                });
              });
              controls.append($('<div class="uk-inline uk-width-1-1">').append(button2).append(input2));
              break;
            default:
              controls.append(`<input class="uk-input fonts" id="${param.code}" type="text" placeholder="${param.label}">`);
          }
        }
    }

    paramRow.append(label).append(controls);
    return paramRow;
  },

  getReport: function(report){
    let requestUrl = `${window.location.origin}/Synergy/rest/api/report/do?reportID=${report.reportID}`;

    if(report.params && report.params.length > 0) {
      let dialog = $('<div uk-modal>');
      let md = $('<div>', {class: 'uk-modal-dialog'});
      let modalBody = $('<div class="uk-modal-body" uk-overflow-auto>');
      let footer = $('<div class="uk-modal-footer uk-text-right">')
      let button = $('<button class="uk-button uk-button-primary" type="button">Принять</button>');
      dialog.append(md);
      footer.append('<button class="uk-button uk-button-default uk-modal-close uk-margin-right" type="button">Отмена</button>');
      footer.append(button);
      md.append(`<div class="uk-modal-header"><h3>${report.nameru}</h3></div>`).append(modalBody).append(footer);

      report.params.forEach(param => {
        modalBody.append(this.getParamRow(param));
      });

      UIkit.modal(dialog).show();
      dialog.on('hidden', () => {
        dialog.remove();
      });
      button.on('click', () => {
        report.params.forEach(param => {
          let val = "";
          switch (param.type) {
            case "java.util.Date":
              val = $(`[id="${param.code}"]`).val();
              let time = " 00:00:00";
              if(!val) val = new Date().toISOString().substring(0, 10);
              if(param.code == "start") time = " 00:00:00";
              if(param.code == "stop") time = " 23:59:59";
              val += time;
              break;
            case "java.util.List":
              val = this.listValues[param.code].join(',');
              break;
            default:
              if(param.code.substr(0, 8) == 'registry') {
                val = $(`[id="${param.code}"]`).attr('documentId');
              } else {
                switch (param.code) {
                  case "department_id":
                  case "departments_id":
                    val = $(`[id="${param.code}"]`).attr('dep-ids') || "";
                    break;
                  default:
                    val = $(`[id="${param.code}"]`).val() || "";
                }
              }
          }
          requestUrl += `&${param.code}=${val}`;
        });
        this.printReport(report.defaultName, requestUrl);;
        UIkit.modal(dialog).hide();
      });

    } else {
      this.printReport(report.defaultName, requestUrl);
    }

  },

  getSystemReportExcel: function(){
    let url = `${window.location.origin}/Synergy/rest/reg/load/xls?r=${this.registryID}`;
    url += `&l=ru&f=${this.filterID || ''}&s=&u=${AS.OPTIONS.currentUser.userid}&fn=${this.filterName || this.registryName}`;
    window.open(url);
  },

  reportsInit: function(){
    rest.synergyGet(`api/report/list`, res => {
      let reportsMenu = $('.itsm-report-menu');
      reportsMenu.empty();

      let linkGetExcel = $('<li><a href="#">Выгрузка в Excel</a></li>');
      linkGetExcel.on('click', e => this.getSystemReportExcel());
      reportsMenu.append(linkGetExcel);
      reportsMenu.append('<li class="uk-nav-divider"></li>');

      res = res.filter(x => x.objectType == 65536 && x.objectCode == this.registryCode);
      res.forEach(report => {
        let item = $(`<li><a href="#">${report.nameru}</a></li>`);
        item.on('click', e => this.getReport(report));
        reportsMenu.append(item);
      });
    });
  },

  createDocument: function(){
    UIkit.offcanvas('.itsm-mobile-menu-right').hide();
    Cons.showLoader();
    try {
      AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/create_doc?registryCode=${this.registryCode}`)
      .then(res => {
        if(res.errorCode != '0') throw new Error(res.errorMessage);
        Cons.hideLoader();
        Cons.setAppStore({dataRow: res});
        this.goFormPage(res.dataUUID, this.registryCode, this.registryName, true);
      });
    } catch (error) {
      itsmShowMessage({message: "Произошла ошибка при создании записи реестра", type: 'error'});
      console.log(error);
      Cons.hideLoader();
    }
  },

  mobileMenuInit: function(){
    let nav = $('<ul class="uk-nav-default uk-nav-parent-icon" uk-nav></ul>');
    let buttonCreate = $(`<li><a href="#"><span class="uk-margin-small-right" uk-icon="icon: plus-circle"></span>Создать</a></li>`);
    let buttonRefresh = $(`<li><a href="#"><span class="uk-margin-small-right" uk-icon="icon: refresh"></span>Обновить</a></li>`);

    buttonCreate.on('click', e => {
      e.preventDefault();
      e.target.blur();
      UIkit.offcanvas('.itsm-mobile-menu-right').hide();
      this.createDocument();
    });

    buttonRefresh.on('click', e => {
      e.preventDefault();
      e.target.blur();
      UIkit.offcanvas('.itsm-mobile-menu-right').hide();
      refreshRows();
    });

    nav.append(buttonCreate).append(buttonRefresh).append('<li class="uk-nav-divider"></li>');

    setTimeout(() => {
      $('.itsm-mobile-buttons-container').empty().append(nav);
    }, 600);

    if(this.allRights.indexOf('rr_create') == -1) buttonCreate.hide();
  },

  reset: function(){
    this.registryID = null;
    this.registryName = '';
    this.registryRights = [];
    this.formName = '';
    this.filterRights = [];
    this.allRights = [];
    this.filterName = null;
    this.filterCode = null;
    this.filterID = null;
    this.sortCmpID = null;
    this.sortDesc = false;
    this.searchField = null;
    this.searchValue = null;
    this.selectedItems = [];

    this.registryTable = $('<table class="itsm-table uk-table uk-table-small uk-table-striped uk-table-responsive">');
    this.colgroup = $('<colgroup>');
    this.tHead = $('<thead>');
    this.tBody = $('<tbody>');
    this.registryTable.append(this.colgroup).append(this.tHead).append(this.tBody);
    tableContainer.empty().append(this.registryTable);

    $('.itsm-massactions-container').css({display: 'none'});
    $('.itsm-menu-content-right>.itsm-mobile-registry-actions').hide();
  },

  init: function(event){
    let info = getRegInfo(event.registryCode);
    this.reset();
    this.registryCode = event.registryCode;
    this.registryRights = info.rights;
    this.allRights = info.rights;
    this.formName = info.formName;

    if(event.filterCode) this.filterCode = event.filterCode;
    if(event.filterID) this.filterID = event.filterID;
    if(event.filterRights) {
      this.filterRights = event.filterRights;
      this.allRights = this.registryRights.concat(this.filterRights);
    }

    this.createHeader();
    this.massActionsInit();
    this.reportsInit();
    this.mobileMenuInit();
  }
}

function filterAction(filter){
  let registryRights = registryTable.registryRights;
  let registryCode = registryTable.registryCode;
  itsmPaginator.reset();
  registryTable.reset();
  registryTable.registryCode = registryCode;
  registryTable.registryRights = registryRights;
  registryTable.filterCode = filter.code;
  registryTable.filterID = filter.id;
  registryTable.filterName = filter.name;
  registryTable.filterRights = filter.rights;
  registryTable.allRights = registryTable.registryRights.concat(registryTable.filterRights);
  registryTable.createHeader();

  let cookiePages = cookie.getJson('pages');
  cookiePages.param.filterCode = filter.code;
  cookiePages.param.filterID = filter.id;
  cookiePages.param.filterRights = filter.rights;
  cookie.setJson('pages', cookiePages);
}


function initRegistryFilters(event) {
  filterContainer.empty();

  let regInfo = getRegInfo(registryTable.registryCode);
  if(!regInfo.hasOwnProperty('filters')) return;

  let mobileFilterMenu = $('<ul class="uk-nav-default" uk-nav>');
  let ukSlider = $('<div uk-slider="finite: true">').css({"width": "96%", "padding-left": "4%"});
  let sliderContainer = $('<div class="uk-slider-container">');
  let ul = $('<ul class="uk-slider-items">');


  mobileFilterMenu.append('<li class="uk-nav-header">Фильтры</li>');
  setTimeout(() => {
    $('.itsm-mobile-filter-container').empty().append(mobileFilterMenu);
    if(regInfo.filters.length === 0) {
      $('.itsm-mobile-filter-container').hide();
    } else {
      $('.itsm-mobile-filter-container').show();
    }
  }, 600);

  sliderContainer.append(ul)
  .append('<a class="uk-position-center-left uk-position-small uk-hidden-hover" href="#" uk-slidenav-previous uk-slider-item="previous"></a>')
  .append('<a class="uk-position-center-right uk-position-small uk-hidden-hover" href="#" uk-slidenav-next uk-slider-item="next"></a>');
  ukSlider.append(sliderContainer);
  filterContainer.append(ukSlider);

  regInfo.filters.forEach(item => {
    let li = $('<li>');
    let block = $('<div>').addClass('itsm-filter-button').attr('filter-code', item.code);
    let counter = $('<span>').addClass('uk-float-right uk-margin-small-right uk-badge');
    let label = $('<span>').addClass('uk-margin-small-right').text(subName(item.name, 15)).attr('title', item.name);

    let mobileMenuItem = $('<li class="itsm-filter-mobile-item">');
    let mobileCounter = $('<div>');
    let mobileBlock = $('<a href="javascript:void(0);">');

    block.on('click', e => {
      let cookiePages = cookie.getJson('pages');
      if(cookiePages && cookiePages.param.filterCode && cookiePages.param.filterCode == item.code) return;

      $('.itsm-filter-button').removeClass("itsm-filter-button-active");
      block.addClass("itsm-filter-button-active");
      $('.itsm-filter-mobile-item').removeClass("itsm-filter-mobile-active");
      mobileMenuItem.addClass("itsm-filter-mobile-active");
      filterAction(item);
    });
    block.append(label).append(counter);
    li.append(block);
    ul.append(li);

    mobileBlock.on('click', e => {
      UIkit.offcanvas('.itsm-mobile-menu-right').hide();
      $('.itsm-filter-button').removeClass("itsm-filter-button-active");
      block.addClass("itsm-filter-button-active");
      $('.itsm-filter-mobile-item').removeClass("itsm-filter-mobile-active");
      mobileMenuItem.addClass("itsm-filter-mobile-active");
      filterAction(item);
    });
    mobileBlock.append($(`
      <div class="uk-grid-small" uk-grid>
        <div class="uk-width-expand mobile-filter-name" uk-leader>${item.name}</div>
      </div>`).append(mobileCounter)
    );
    mobileMenuItem.append(mobileBlock);
    mobileFilterMenu.append(mobileMenuItem);

    if(event.filterCode && event.filterCode == item.code) {
      block.addClass("itsm-filter-button-active");
      mobileMenuItem.addClass("itsm-filter-mobile-active");
    }

    if(regInfo.hasOwnProperty('filter_ignore')) {
      if(regInfo.filter_ignore.indexOf(item.code) == -1) {
        rest.synergyGet(`api/registry/data_ext?registryCode=${item.registryCode}&filterCode=${item.code}&pageNumber=0&countInPart=1&loadData=false`, res => {
          counter.text(res.count);
          mobileCounter.text(res.count);
        });
      } else {
        counter.hide();
        mobileCounter.hide();
      }
    } else {
      rest.synergyGet(`api/registry/data_ext?registryCode=${item.registryCode}&filterCode=${item.code}&pageNumber=0&countInPart=1&loadData=false`, res => {
        counter.text(res.count);
        mobileCounter.text(res.count);
      });
    }


  });
  mobileFilterMenu.append('<li class="uk-nav-divider"></li>');
}

addListener('change_itsm_menu', 'panel-registry-list', event => {
  registryTable.init(event);
  initRegistryFilters(event);
  itsmPaginator.init();

  if(event.currentPage) {
    itsmPaginator.currentPage = event.currentPage;
    itsmPaginator.update();
  }
});

$('.itsm-refresh-button').off().on('click', e => {
  e.preventDefault();
  e.target.blur();
  refreshRows();
});

$(document).on('contextmenu', () => $('.itsm-context-menu').remove());
$(document).on('click', () => $('.itsm-context-menu').remove());
