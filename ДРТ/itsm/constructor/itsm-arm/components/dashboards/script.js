const infoDashboards = Cons.getAppStore().infoDashboards;
let ulNav = $('.itsm-menu-desktop .itsm-nav');
ulNav.empty();

function fullscreen(element) {
  if(element.requestFullScreen) {
    element.requestFullScreen();
  } else if(element.mozRequestFullScreen) {
    element.mozRequestFullScreen();
  } else if(element.webkitRequestFullScreen) {
    element.webkitRequestFullScreen();
  }
}

function dashboardInit(){
  const userGroups = Cons.getAppStore().itsmUser.data.groups;

  function checkUserGroup(groups) {
    if(!groups) return false;
    groups = groups.split(', ');
    for (let i = 0; i < userGroups.length; i++)
    if (groups.indexOf(userGroups[i].name) !== -1) return true;
    return false;
  }

  let dashboardUrl = '';
  let newDashboardUrl = '';
  let analyticsPanel = $('#analytics-panel');
  let periodPanel = $('#period-dashboard-panel');
  let periodContainer = $('<div class="uk-inline">');
  let datePeriod = $('<input class="uk-input uk-form-width-large" type="text"placeholder="выберите период...">');
  let periodCheckButton = $('<span id="period-check" uk-icon="check" title="Применить"></span>');
  let icon = 'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIGhlaWdodD0iMjQiIHZpZXdCb3g9IjAgMCAyNCAyNCIgd2lkdGg9IjI0Ij48cGF0aCBkPSJNOSAxN0g3di03aDJ2N3ptNCAwaC0yVjdoMnYxMHptNCAwaC0ydi00aDJ2NHptMi41IDIuMWgtMTVWNWgxNXYxNC4xem0wLTE2LjFoLTE1Yy0xLjEgMC0yIC45LTIgMnYxNGMwIDEuMS45IDIgMiAyaDE1YzEuMSAwIDItLjkgMi0yVjVjMC0xLjEtLjktMi0yLTJ6IiBmaWxsPSIjMUU4N0YwIi8+PHBhdGggZD0iTTAgMGgyNHYyNEgweiIgZmlsbD0ibm9uZSIvPjwvc3ZnPg==';

  periodContainer
  .append('<span class="uk-form-icon" uk-icon="icon: calendar"></span>')
  .append(datePeriod)
  .append(periodCheckButton);
  periodPanel.hide().append(periodContainer);

  let query = '&condition=CONTAINS&field=dasboard_visible&key=2&fields=dasboard_name&fields=dasboard_url&fields=dasboard_period&fields=dashboard_groups';
  rest.synergyGet('api/registry/data_ext?registryCode=custom_dashboard_registry' + query, res => {
    res.result.forEach(item => {
      if(checkUserGroup(item.fieldValue.dashboard_groups)) {
        let li = $('<li>');
        let a = $('<a>').addClass('itsm-nav-link uk-link-text');
        a.append(`<span class="itsm-nav-icon uk-margin-small-left" style="background: url(${icon}) no-repeat center;">`);
        a.append(`<span class="itsm-nav-link-text" title="${item.fieldValue.dasboard_name}">${item.fieldValue.dasboard_name}</span>`);
        li.append(a);
        ulNav.append(li);
        a.on('click', function() {
          if($(document).width() < 769) UIkit.offcanvas('.itsm-mobile-menu-left').hide();
          $('.itsm-nav-link').removeClass("itsm-nav-link-active");
          a.addClass("itsm-nav-link-active");
          dashboardUrl = item.fieldValue.dasboard_url;
          let iFrame = $(`<iframe id="dashboard-frame" src="${dashboardUrl}" width="100%" height="100%">`);
          analyticsPanel.empty().append(iFrame);
          datePeriod.val('');
          if(item.fieldKey.dasboard_period == '2') {
            periodPanel.show();
            analyticsPanel.css({'height': 'calc(100% - 50px)'});
          } else {
            periodPanel.hide();
            analyticsPanel.css({'height': '100%'});
          }
          if($('#button-fullscreen').length) {
            $('#button-fullscreen').removeClass('uk-hidden');
            $('#button-fullscreen').off().on('click', e => {
              e.preventDefault();
              e.target.blur();
              fullscreen(document.getElementById("app-panel"))
            })
          }
        });
      }
    });
  });

  datePeriod.daterangepicker({
    opens: 'right',
    autoUpdateInput: false,
    showDropdowns: true,
    locale: {
      format: "DD.MM.YYYY",
      separator: " - ",
      applyLabel: "Выбрать",
      cancelLabel: "Очистить",
      fromLabel: "From",
      toLabel: "To",
      customRangeLabel: "Custom",
      weekLabel: "Н",
      daysOfWeek: ["Вс","Пн","Вт","Ср","Чт","Пт","Сб"],
      monthNames: ["Январь","Февраль","Март","Апрель","май","Июнь","Июль","Август","Сентябрь","Октябрь","Ноябрь","Декабрь"],
      firstDay: 1
    }
  });
  datePeriod.on('apply.daterangepicker', function(ev, picker) {
    $(this).val(picker.startDate.format('DD.MM.YYYY') + ' - ' + picker.endDate.format('DD.MM.YYYY'));
    newDashboardUrl = `${dashboardUrl.split('?')[0]}?embed=true&_g=(time:(from:'${picker.startDate.format('YYYY-MM-DD')}',mode:absolute,to:'${picker.endDate.format('YYYY-MM-DD')}'))`;
  });
  datePeriod.on('cancel.daterangepicker', function(ev, picker) {
    $(this).val('');
    newDashboardUrl = dashboardUrl;
  });
  periodCheckButton.on('click', e => {
    $('#dashboard-frame').attr('src', newDashboardUrl);
  });

  periodCheckButton.css({
    'padding': '7px',
    'margin-left': '15px',
    'background': 'transparent',
    'cursor': 'pointer'
  }).hover(function() {
    $(this).css({
      'background': '#1E87F0',
      'color': '#fff'
    });
  }, function() {
    $(this).css({
      'background': 'transparent',
      'color': 'inherit'
    });
  });
}

if(infoDashboards && infoDashboards.rr_list && infoDashboards.rr_list == 'Y') {
  dashboardInit();
}

let pages = cookie.getJson('pages');
if(pages) {
	pages.currentPage = 'analytics';
	cookie.setJson('pages', pages);
}


// #period-check {
//     padding: 7px;
//     margin-left: 15px;
//     background: transparent;
//     border-radius: 3px;
//     cursor: pointer;
// }
//
// #period-check:hover {
//     background: #1E87F0;
//     color: #fff;
// }
