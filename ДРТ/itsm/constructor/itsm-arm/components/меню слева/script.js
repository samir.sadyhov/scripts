const registries = Cons.getAppStore().registries;
const currentPage = Cons.getCurrentPage();

let ulNav = $('.itsm-menu-desktop .itsm-nav');
ulNav.attr('uk-nav', '');
ulNav.empty();

const hidePanel = () => {
  ulNav.addClass('collapsed');
  $('#app-panel').addClass('collapsed');
  $('#left-panel').addClass('collapsed');
  $('.itsm-nav-link').addClass('collapsed');

  if($('.itsm-nav .uk-parent.uk-open .itsm-nav-link-active').length) {
    $('.itsm-nav .uk-parent.uk-open').addClass('itsm-nav-link-active');
  }

  $('.itsm-nav .uk-parent.uk-open .uk-nav-sub').attr('hidden', true);
  $('.itsm-nav .uk-parent.uk-open .uk-nav-sub').hide();
  $('.itsm-nav-link-text').hide();
}

const showPanel = () => {
  ulNav.removeClass('collapsed');
  $('#app-panel').removeClass('collapsed');
  $('#left-panel').removeClass('collapsed');
  $('.itsm-nav-link').removeClass('collapsed');
  $('.itsm-nav .uk-parent.uk-open').removeClass('itsm-nav-link-active');
  $('.itsm-nav .uk-parent.uk-open .uk-nav-sub').attr('hidden', null);
  $('.itsm-nav .uk-parent .uk-nav-sub').show();
  $('.itsm-nav-link-text').fadeIn(600);
}

const createButtonCollapse = () => {
  let panelStatus = 'show';
  let button = $('<div>', {class: 'collapse_button'});
  button.append('<span>Свернуть панель</span>');

  button.on('click', e => {
    if(panelStatus == 'show') {
      hidePanel();
      button.addClass('collapsed');
      panelStatus = 'hide';
    } else {
      showPanel();
      button.removeClass('collapsed');
      panelStatus = 'show';
    }
  });

  $('#left-panel').append(button);
}

const createContextMenu = (itemFilter, parent) => {
  let contextMenu = $('<ul class="itsm-nav uk-nav" uk-nav></ul>');
  let regCode = null;
  if($('.itsm-nav-link-active').length) {
    regCode = $('.itsm-nav-link-active').attr('registrycode');
  }
  itemFilter.forEach(children => {
    let subLi = $('<li>');
    let subA = $('<a>').addClass('itsm-nav-link uk-link-text');
    subA.append(`<span class="itsm-nav-link-text" title="${children.name}">${children.name}</span>`);
    subLi.append(subA);
    contextMenu.append(subLi);
    subA.attr('registrycode', children.code)
    .on('click', function() {

      cookie.setJson('pages', {
        currentPage: currentPage.code,
        code: currentPage.code,
        param: {
          registryCode: children.code,
          children: true
        }
      });

      $('.itsm-nav-link-active').removeClass("itsm-nav-link-active");
      $('.uk-open').removeClass("uk-open");
      $('[aria-hidden="false"]').attr('aria-hidden', true).attr('hidden', true);

      $(`[registrycode="${children.code}"]`).addClass("itsm-nav-link-active");
      parent.addClass('itsm-nav-link-active uk-open');
      parent.find('ul').attr('aria-hidden', false).hide();

      let event = {
        type: 'change_itsm_menu',
        registryCode: children.code
      };
      let filter = null;
      if(children.rights.length == 0 && children.filters.length > 0) filter = children.filters[0];
      if(filter) {
        event.filterCode = filter.code;
        event.filterID = filter.id;
        event.filterRights = filter.rights;
      }

      fire(event, comp.code);
      hideShowButtonCreate(children.code);

    });
  });
  if(regCode) contextMenu.find(`[registrycode="${regCode}"]`).addClass("itsm-nav-link-active");
  return contextMenu;
}

registries.forEach(item => {
  if (item.children) {
    let itemFilter = item.children.filter(x => x.rights.length > 0 || x.filters.length > 0);
    if(itemFilter && itemFilter.length > 0) {
      let li = $(`<li title="${item.name}">`);
      let a = $('<a>').addClass('itsm-nav-link uk-link-text');
      a.append(`<span class="itsm-nav-icon uk-margin-small-left" style="background: url(${item.icon}) no-repeat center;">`);
      a.append(`<span class="itsm-nav-link-text">${item.name}</span>`);
      li.append(a);
      ulNav.append(li);
      let subNav = $('<ul class="uk-nav-sub">');
      li.addClass('uk-parent');

      li.on('click', e => {
        if(!ulNav.hasClass('collapsed')) return;

        e.stopPropagation();
        $('.itsm-nav .uk-parent.uk-open .uk-nav-sub').attr('hidden', true);
        $('.itsm-nav .uk-parent.uk-open .uk-nav-sub').hide();

        $('.itsm-context-menu.left').remove();
        let contextMenu = createContextMenu(itemFilter, li);
        $('<div>', {class: 'itsm-context-menu left'})
        .css({
          "left": '50px',
          "top": (li.position().top + 100) + 'px',
          "min-width": "200px"
        })
        .appendTo('body')
        .append(contextMenu)
        .show('fast');
      });


      itemFilter.forEach(children => {
        let subLi = $('<li>');
        let subA = $('<a>').addClass('itsm-nav-link uk-link-text');
        subA.append(`<span class="itsm-nav-link-text" title="${children.name}">${children.name}</span>`);
        subLi.append(subA);
        subNav.append(subLi);
        subA.attr('registrycode', children.code)
        .on('click', function() {
          let pages = cookie.getJson('pages');
          if(pages && pages.param.registryCode !== children.code) {
            ulNav.find(`[registrycode="${pages.param.registryCode}"]`).attr('click', null);
          }
          if(subA.attr('click')) return;
          subA.attr('click', true);
          setTimeout(() => {
            subA.attr('click', null);
          }, 2000);

          if($(document).width() < 769) UIkit.offcanvas('.itsm-mobile-menu-left').hide();

          Cons.showLoader();
          cookie.setJson('pages', {
            currentPage: currentPage.code,
            code: currentPage.code,
            param: {
              registryCode: children.code,
              children: true
            }
          });

          $('.itsm-nav-link').removeClass("itsm-nav-link-active");
          $(`[registrycode="${children.code}"]`).addClass("itsm-nav-link-active");

          let event = {
            type: 'change_itsm_menu',
            registryCode: children.code
          };
          let filter = null;
          if(children.rights.length == 0 && children.filters.length > 0) filter = children.filters[0];
          if(filter) {
            event.filterCode = filter.code;
            event.filterID = filter.id;
            event.filterRights = filter.rights;
          }

          fire(event, comp.code);
          hideShowButtonCreate(children.code);

          setTimeout(() => {
            Cons.hideLoader();
          }, 500);
        });
        subA.on('dblclick', e => {
          e.preventDefault();
          return;
        });
      });

      li.append(subNav);
    }
  } else {
    if(item.rights.length > 0 || item.filters.length > 0) {
      let li = $(`<li title="${item.name}">`);
      let a = $('<a>').addClass('itsm-nav-link uk-link-text');
      a.append(`<span class="itsm-nav-icon uk-margin-small-left" style="background: url(${item.icon}) no-repeat center;">`);
      a.append(`<span class="itsm-nav-link-text">${item.name}</span>`);
      li.append(a);
      ulNav.append(li);

      a.attr('registrycode', item.code)
      .on('click', function() {
        if(ulNav.hasClass('collapsed')) {
          $('.itsm-nav .uk-parent.uk-open .uk-nav-sub').attr('hidden', true);
          $('.itsm-nav .uk-parent.uk-open .uk-nav-sub').hide();
        }

        let pages = cookie.getJson('pages');
        if(pages && pages.param.registryCode !== item.code) {
          ulNav.find(`[registrycode="${pages.param.registryCode}"]`).attr('click', null);
        }
        if(a.attr('click')) return;
        a.attr('click', true);
        setTimeout(() => {
          a.attr('click', null);
        }, 2000);

        if($(document).width() < 769) UIkit.offcanvas('.itsm-mobile-menu-left').hide();

        Cons.showLoader();

        cookie.setJson('pages', {
          currentPage: currentPage.code,
          code: currentPage.code,
          param: {
            registryCode: item.code,
            children: false
          }
        });

        $('.itsm-nav-link').removeClass("itsm-nav-link-active");
        $('.itsm-nav .uk-parent').removeClass("itsm-nav-link-active");
        $(`[registrycode="${item.code}"]`).addClass("itsm-nav-link-active");

        let event = {
          type: 'change_itsm_menu',
          registryCode: item.code
        };
        let filter = null;
        if(item.rights.length == 0 && item.filters.length > 0) filter = item.filters[0];
        if(filter) {
          event.filterCode = filter.code;
          event.filterID = filter.id;
          event.filterRights = filter.rights;
        }

        fire(event, comp.code);
        hideShowButtonCreate(item.code);

        setTimeout(() => {
          Cons.hideLoader();
        }, 500);
      });
      a.on('dblclick', e => {
        e.preventDefault();
        return;
      });
    }
  }
});

createButtonCollapse();
