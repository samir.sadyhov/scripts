const registries = Cons.getAppStore().registries;

let formPlayer = getCompByCode("formPlayer-1");
let dataRow = Cons.getAppStore().dataRow;
let itsmUser = Cons.getAppStore().itsmUser;
let pages = cookie.getJson('pages');

pages.currentPage = 'form_player_page';
cookie.setJson('pages', pages);

AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/formPlayer/getDocMeaningContent?documentId=${dataRow.documentID}`, null, 'text')
.then(value => dataRow.meaning = value);


/*
{
    "document_close_assignment": true/false,    завершение    assignment
    "document_close_approvement": true/false,   утверждение   approval
    "document_close_acq": true/false,           ознакомление  acquaintance
    "document_close_agreement": true/false      согласование  agreement
}
*/
let docflowSettings = Cons.getAppStore().docflowSettings;
if(!docflowSettings) {
  fetch(`${window.origin}/itsm/rest/config/docflow-settings`, {headers: {
    'Authorization': "Basic " + btoa(Cons.creds.login + ":" + Cons.creds.password)
  }})
  .then(res => res.json())
  .then(res => {
    if(!res.errorCode) {
      docflowSettings = res;
      Cons.setAppStore({docflowSettings: docflowSettings});
    }
  });
}

let buttonContainer = $('#form-buttons');
buttonContainer.css({'width': '100%'})
.addClass('uk-flex-inline uk-flex-wrap uk-flex-column uk-flex-wrap-middle');

let mobileMenuContent = $(".itsm-menu-content-left");
mobileMenuContent.empty();

function getParam(name) {
  let paramValues = getCurrentPage().paramValues;
  for (let i = 0; i < paramValues.length; i++)
    if (paramValues[i].pageParamName == name) return paramValues[i].value;
  return undefined;
}

function getRegInfo(code) {
  let result;
  registries.forEach(item => {
    if (item.children) {
      item.children.forEach(child => {
        if (child.code == code) result = child;
      })
    } else {
      if (item.code == code) result = item;
    }
  });
  return result;
}

function buttonSort() {
  function s(a, b) {
    let idA = $(a).attr("id");
    let idB = $(b).attr("id");
    if (idA > idB) return 1;
    if (idA < idB) return -1;
    return 0;
  }
  let buttons = buttonContainer.find('.itsm-action-button');
  let buttons2 = mobileMenuContent.find('.itsm-action-button');
  buttons.sort(s);
  buttons2.sort(s);
  $(buttons).appendTo(buttonContainer);
  $(buttons2).appendTo(mobileMenuContent);
}

function createButton(name, id, handler, greenButton){
  let button = $(`<button id="${id}" class="uk-button margined uk-button-default itsm-action-button fonts">`)
  .css({
    'width': 'calc(85% - 10px)',
    'white-space': 'nowrap',
    'text-overflow': 'ellipsis',
    'overflow': 'hidden',
    'padding': '0 5px',
  })
  .attr('title', name)
  .html(name)
  .on('click', e => {
    e.preventDefault();
    e.target.blur();
    handler();
  });
  if(greenButton) button.addClass('itsm-action-button-green');
  buttonContainer.append(button);
  button.clone(true).appendTo(mobileMenuContent);
  buttonSort();
}

function getModalDialog(title, body, buttonName, buttonAction) {
  let dialog = $('<div class="uk-flex-top" uk-modal>');
  let md = $('<div class="uk-modal-dialog uk-margin-auto-vertical">');
  let modalBody = $('<div class="uk-modal-body" uk-overflow-auto>');
  let footer = $('<div class="uk-modal-footer uk-text-right">');

  modalBody.css({'background': '#e1e7f3'}).append(body);
  footer.append('<button class="uk-button uk-button-default uk-modal-close" type="button">Закрыть</button>');
  dialog.append(md);
  md.append(`<div class="uk-modal-header"><h3>${title}</h3></div>`)
  .append(modalBody).append(footer);

  if(buttonName) {
    let actionButton = $('<button class="uk-button uk-button-primary uk-margin-left" type="button">');
    actionButton.html(buttonName).on('click', buttonAction);
    footer.append(actionButton);
  }

  return dialog;
}

function getDialogAgreement(title, buttons, handler) {
  let dialog = $('<div class="uk-flex-top" uk-modal>');
  let md = $('<div class="uk-modal-dialog uk-margin-auto-vertical">');
  let modalBody = $('<div class="uk-modal-body" uk-overflow-auto>');
  let footer = $('<div class="uk-modal-footer uk-text-right">');
  let commentInput = $('<textarea class="uk-textarea" rows="5" placeholder="Комментарий"></textarea>');

  if(title !== 'Ознакомление') {
    modalBody.append($('<div class="uk-margin">').append(commentInput));
  }

  dialog.append(md);
  md.append(`<div class="uk-modal-header"><h3>${title}</h3></div>`)
  .append(modalBody).append(footer);

  buttons.forEach(button => {
    let agreeButton = $('<button class="uk-button uk-button-default uk-margin-left" type="button">');
    agreeButton.html(button.label).on('click', () => handler(button.signal, commentInput.val()));
    footer.append(agreeButton)
  });

  return dialog;
}

function printForm() {
  var prtContent = document.getElementById('formPlayer-1');
  var prtCSS = '<link rel="stylesheet" href="synergyForms.css" type="text/css" />';
  var WinPrint = window.open();
  WinPrint.document.write('');
  WinPrint.document.write(prtCSS);
  WinPrint.document.write(prtContent.innerHTML);
  WinPrint.document.write('');
  WinPrint.document.close();
  WinPrint.focus();
  WinPrint.print();
}

function saveFormData(){
  if(!formPlayer.model.isValid()) {
    itsmShowMessage({message: "Заполните обязательные поля!", type: 'warning'});
  } else {
    Cons.showLoader();
    try {
      let asfData = formPlayer.model.getAsfData();
      $.when(AS.FORMS.ApiUtils.saveAsfData(asfData.data, asfData.form, asfData.uuid))
      .then(uuid => {
        Cons.hideLoader();
        itsmShowMessage({message: '<svg width="35" height="35" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><polyline fill="none" stroke="#fff" stroke-width="1.1" points="4,10 8,15 17,4"></polyline></svg> Успешно', type: 'success'});
        formPlayer.model.save = true;
        $('#save-button').hide();
        $('.mobile-button-save').hide();
      });
    } catch (error) {
      Cons.hideLoader();
      itsmShowMessage({message: "Ошибка сохранения данных по форме", type: 'error'});
      console.log(error);
    }
  }
}

const isRequiredFile = () => {
  return new Promise(async resolve => {
    try {
      let service = formPlayer.model.getModelWithId('itsm_form_incident_servicelink');
      if(!service || !service.getValue()) throw Error('Не выбран сервис');
      AS.FORMS.ApiUtils.getAsfDataUUID(service.getValue())
      .then(uuid => AS.FORMS.ApiUtils.loadAsfData(uuid))
      .then(serviceAsfData => {
        let fileReq = serviceAsfData.data.find(x => x.id === 'itsm_form_service_file_required');
        if(!fileReq || !fileReq.hasOwnProperty('values')) {
          resolve(false);
          return;
        }
        let files = null;
        let table = formPlayer.model.getModelWithId('itsm_form_incident_files');
        table.getBlockNumbers().forEach(block => {
          let fileModel = formPlayer.model.getModelWithId('file', 'itsm_form_incident_files', block);
          if(fileModel.getValue()) files = true;
        });
        resolve(fileReq.values[0] ? (files ? false : true) : false);
      });
    } catch (e) {
      resolve(false);
    }
  });
}

function activateDocument(regInfo) {
  if(!formPlayer.model.isValid()) {
    itsmShowMessage({message: "Заполните обязательные поля!", type: 'warning'});
    return;
  }

  Cons.showLoader();
  try {
    let asfData = formPlayer.model.getAsfData();
    $.when(AS.FORMS.ApiUtils.saveAsfData(asfData.data, asfData.form, asfData.uuid))
    .then(uuid => AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/activate_doc?dataUUID=${asfData.uuid}`))
    .then(result => {
      Cons.hideLoader();
      itsmShowMessage({message: '<svg width="35" height="35" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><polyline fill="none" stroke="#fff" stroke-width="1.1" points="4,10 8,15 17,4"></polyline></svg> Успешно', type: 'success'})

      //переход к списку записей реестра с выбраным ранее реестром
      if(regInfo && regInfo.closeAfterActivation) {
        fire({type: 'goto_page', pageCode: 'app', pageParams: [{pageParamName: 'registryCode', value: regInfo.code}]}, 'form-buttons');
      }

      buttonInit();
      $('#itsm-button1').hide();
      $('#save-button').hide();
      $('.mobile-button-save').hide();
      fire({type: 'button_click'}, 'show-button');
    });
  } catch (error) {
    Cons.hideLoader();
    itsmShowMessage({message: "Произошла ошибка при создании заявки, обратитесь к администратору", type: 'error'});
    console.log(error);
  }

  // isRequiredFile().then(res => {
  //   if(res) {
  //     $('[data-asformid="table.container.itsm_form_incident_files"]').css({
  //       "border": "1px solid red",
  //       "background": "rgba(255, 0, 0, 0.05)"
  //     });
  //     itsmShowMessage({message: "Заполните обязательные поля!", type: 'warning'});
  //     return;
  //   } else {
  //
  //   }
  // });
}

function checkSavedForm(handler){
  if(!formPlayer.model.save) {
    UIkit.modal.confirm('Документ был изменен. Сохранить произведенные изменения?')
    .then(() => {
      Cons.showLoader();
      let asfData = formPlayer.model.getAsfData();
      AS.FORMS.ApiUtils.saveAsfData(asfData.data, asfData.form, asfData.uuid).then(uuid => {
        Cons.hideLoader();
        itsmShowMessage({message: '<svg width="35" height="35" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><polyline fill="none" stroke="#fff" stroke-width="1.1" points="4,10 8,15 17,4"></polyline></svg> Успешно', type: 'success'});
        formPlayer.model.save = true;
        handler();
      });
    }, handler);
  } else {
    handler();
  }
}

function finishWork(processes, formCode){
  try {
    if(formPlayer.model.getErrors().length) throw new Error('Заполните обязательные поля');

    checkSavedForm(() => {
      let url = `api/workflow/work/get_form_for_result?formCode=${formCode}&workID=${processes[0].actionID}`;
      rest.synergyGet(url, res => {
        if(res.errorCode && res.errorCode != '0') throw new Error(res.errorMessage);

        let player = AS.FORMS.createPlayer();
        player.view.setEditable(true);
        player.showFormData(null, null, res.dataUUID);

        let dialog = getModalDialog("Форма завершения", player.view.container, "Сохранить", () => {
          Cons.showLoader();
          try {
            if (!player.model.isValid()) throw new Error('Заполните обязательные поля');
            player.saveFormData(saveResult => {
              let finishWorkBody = {
                  workID: processes[0].actionID,
                  completionForm: "FORM",
                  type: 'work',
                  file_identifier: res.file_identifier
              };
              rest.synergyPost("api/workflow/work/set_result", finishWorkBody, "application/x-www-form-urlencoded", result => {
                if(result.errorCode && result.errorCode != '0') throw new Error(result.errorMessage);
                Cons.hideLoader();
                UIkit.modal(dialog).hide();

                //переход к списку записей реестра с выбраным ранее реестром
                if(docflowSettings && docflowSettings.document_close_assignment) {
                  fire({type: 'goto_page', pageCode: 'app', pageParams: [{pageParamName: 'registryCode', value: getParam("registryCode")}]}, 'form-buttons');
                } else {
                  buttonInit();
                }

              });
            });
          } catch (e) {
            Cons.hideLoader();
            itsmShowMessage({message: e.message, type: 'error'});
          }
        });

        UIkit.modal(dialog).show();
        dialog.on('hidden', () => dialog.remove());
      }, err => {
        console.log(err);
        throw new Error('Произошла ошибка при завершении работы');
      });
    });
  } catch (e) {
    itsmShowMessage({message: e.message, type: 'error'});
    console.log(e);
  }
}

function finishWorkNotForm(process){
  try {
    if(formPlayer.model.getErrors().length) throw new Error('Заполните обязательные поля');
    checkSavedForm(() => {
      Cons.showLoader();
      let data = {workID: process[0].actionID, completionForm: "COMMENT", comment: "Работа завершена"};
      rest.synergyPost("api/workflow/work/set_result", data, "application/x-www-form-urlencoded; charset=utf-8", result => {
        if(result.errorCode && result.errorCode != '0') throw new Error(result.errorMessage);
        Cons.hideLoader();

        //переход к списку записей реестра с выбраным ранее реестром
        if(docflowSettings && docflowSettings.document_close_assignment) {
          fire({type: 'goto_page', pageCode: 'app', pageParams: [{pageParamName: 'registryCode', value: getParam("registryCode")}]}, 'form-buttons');
        } else {
          buttonInit();
        }

      });
    });
  } catch (e) {
    console.log(e);
    Cons.hideLoader();
  }
}

function agreementDoc(process, title){
  Cons.showLoader();
  try {
    rest.synergyGet("api/workflow/process_info?workID=" + process.actionID, res => {
      let dialog = getDialogAgreement(title, res.buttons, (signal, comment) => {
        Cons.showLoader();
        let data = {
          procInstID: process.itemID,
          signal: signal,
          rawdata: res.raw_data,
          comment: comment
        };
        rest.synergyPost("api/workflow/finish_process", data, "application/x-www-form-urlencoded; charset=utf-8", result => {
          if(result.errorCode && result.errorCode != '0') throw new Error("произошла ошибка при согласовании/ознакомлении");
          UIkit.modal(dialog).hide();
          Cons.hideLoader();

          let closeDoc = false;
          switch (process.typeID) {
            case 'AGREEMENT_ITEM':
              if(docflowSettings && docflowSettings.document_close_agreement) closeDoc = true;
              break;
            case 'ACQUAINTANCE_ITEM':
              if(docflowSettings && docflowSettings.document_close_acq) closeDoc = true;
              break;
            default: closeDoc = false;
          }
          //переход к списку записей реестра с выбраным ранее реестром
          if(closeDoc) {
            fire({type: 'goto_page', pageCode: 'app', pageParams: [{pageParamName: 'registryCode', value: getParam("registryCode")}]}, 'form-buttons');
          } else {
            buttonInit();
          }

        });
      });
      UIkit.modal(dialog).show();
      dialog.on('hidden', () => dialog.remove());
      Cons.hideLoader();
    });
  } catch (e) {
    console.log(e);
    Cons.hideLoader();
  }
}

function getCollateData(items){
  let data = [];
  items.forEach(id => {
    let fromData;
    if(id.from == 'CURRENT_DOCUMENTID') {
      fromData = {
        type: "reglink",
        key: dataRow.documentID,
        valueID: dataRow.documentID,
        value: dataRow.meaning || 'Документ'
      };
    } else {
      fromData = formPlayer.model.getModelWithId(id.from).getAsfData();
    }
    if(fromData) {
      let field = UTILS.createField(fromData);
      if(id.to.indexOf('.') == -1) {
        field.id = id.to;
        data.push(field);
      } else {
        let tmpTableData = UTILS.createField({
          id: id.to.split('.')[0],
          type: "appendable_table",
          key: "",
          data: []
        });
        field.id = id.to.split('.')[1] + '-b1';
        tmpTableData.data.push(field);
        data.push(tmpTableData);
      }
    }
  });
  return data;
}

function createEvent(e, documentID){
  try {
    let event = {"api_event": `event.docflow.document.${e}`, "documentId": documentID, "userId": AS.OPTIONS.currentUser.userid};
    let data = {
      "eventMsg": JSON.stringify(event),
      "eventName": "event.docflow.document"
    }
    AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/events/create", res => {
      if(res.errorCode != '0') throw new Error(res.errorMessage);
      let openDocs = Cons.getAppStore().openDocs || [];
      if(e == 'opened') {
        openDocs.push(documentID);
      } else {
        openDocs = openDocs.filter(v => v != documentID);
      }
      openDocs = openDocs.filter((v, i, a) => i == a.indexOf(v));
      Cons.setAppStore({openDocs: openDocs});
    }, null, data, "application/x-www-form-urlencoded; charset=UTF-8", err => {
      console.log(err);
    });
  } catch (e) {
    console.log(e);
  }
}

function createDocument(code, items, data){
  Cons.showLoader();
  let regInfo = getRegInfo(code);
  let asfData = getCollateData(items);
  data.forEach(field => asfData.push(field));
  try {
    AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/registry/create_doc_rcc", res => {
      if(res.errorCode != '0') throw new Error(res.errorMessage);
      Cons.hideLoader();
      Cons.setAppStore({dataRow: res});
      dataRow = res;
      fire({type: 'show_form_data', dataId: res.dataID}, 'formPlayer-1');
      fire({type: 'set_form_editable', editable: true}, 'formPlayer-1');

      fire({
        type: 'goto_page',
        pageCode: 'form_player_page',
        pageParams: [
          {pageParamName: 'dataUUID', value: res.dataUUID},
          {pageParamName: 'registryCode', value: code},
          {pageParamName: 'registryName', value: regInfo.name},
          {pageParamName: 'formName', value: regInfo.formName},
          {pageParamName: 'editable', value: true}
        ]
      }, comp.code);

      buttonInit();
    }, null, JSON.stringify({"registryCode": code, "data": asfData}), "application/json; charset=utf-8", err => {
      throw new Error(err.responseText);
    });
  } catch (e) {
    console.log(e);
    Cons.hideLoader();
    itsmShowMessage({message: "Произошла ошибка при создании документа", type: 'error'});
  }
}

const cutText = text => {
  if(text.length > 100) return text.slice(0, 100) + '...';
  return text;
}


let Incident = {

  reassign: function(){
    try {
      if(formPlayer.model.getErrors().length) throw new Error('Заполните обязательные поля');

      checkSavedForm(() => {
        rest.synergyGet(`api/workflow/get_execution_process?documentID=${dataRow.documentID}`, res => {
          let service_user = formPlayer.model.getModelWithId('itsm_form_incident_service_user').getAsfData();
          let processes = UTILS.getUserWork(res, service_user.key, 'ASSIGNMENT_ITEM');
          if(!processes.length) throw new Error('Не найден идентификатор маршрута работы');

          let workId = processes[0].actionID;
          let url = "api/workflow/work/get_form_for_result?formCode=itsm_form_incident_wcf_new&workID=" + workId;
          rest.synergyGet(url, formWork => {
            let player = AS.FORMS.createPlayer();
            player.view.setEditable(true);
            player.showFormData(null, null, formWork.dataUUID);
            player.model.on('dataLoad', () => {
              let wcf_status = player.model.getModelWithId('itsm_form_incident_wcf_status');
              if(wcf_status) {
                wcf_status.setValue("3");
                if(player.view.getViewWithId('itsm_form_incident_wcf_status'))
                player.view.getViewWithId('itsm_form_incident_wcf_status').setEnabled(false);
              }
            });
            let dialog = getModalDialog("Форма завершения инцидента", player.view.container, "Готово", () => {
              Cons.showLoader();
              try {
                if (player.model.getErrors().length) throw new Error('Заполните обязательные поля');
                player.saveFormData(saveResult => {
                  let finishWorkBody = {
                      workID: workId,
                      completionForm: "FORM",
                      type: 'work',
                      file_identifier: formWork.file_identifier
                  };
                  rest.synergyPost("api/workflow/work/set_result", finishWorkBody, "application/x-www-form-urlencoded", result => {
                    if(result.errorCode && result.errorCode != '0') throw new Error(result.errorMessage);
                    formPlayer.model.getModelWithId('itsm_form_incident_status').setValue("2");
                    AS.FORMS.ApiUtils.saveAsfData(formPlayer.model.getAsfData().data, formPlayer.model.formId, formPlayer.model.asfDataId)
                    .then(uid => {
                      Cons.hideLoader();
                      UIkit.modal(dialog).hide();

                      //переход к списку записей реестра с выбраным ранее реестром
                      if(docflowSettings && docflowSettings.document_close_assignment) {
                        fire({type: 'goto_page', pageCode: 'app', pageParams: [{pageParamName: 'registryCode', value: 'itsm_registry_incidents'}]}, 'form-buttons');
                      } else {
                        buttonInit();
                      }

                    });
                  }, err => {
                    console.log(err);
                    Cons.hideLoader();
                    itsmShowMessage({message: 'Произошла ошибка при завершении работы', type: 'error'});
                    UIkit.modal(dialog).hide();
                  });
                });
              } catch (e) {
                Cons.hideLoader();
                itsmShowMessage({message: e.message, type: 'error'});
                UIkit.modal(dialog).hide();
              }
            });
            UIkit.modal(dialog).show();
            dialog.on('hidden', () => dialog.remove());
          }, err => console.log(err));
        }, err => console.log(err));
      });
    } catch (e) {
      itsmShowMessage({message: e.message, type: 'error'});
      console.log(e);
    }
  },

  inProblem: function(){
    let m = [];
    m.push({from: 'itsm_form_incident_priority', to: 'itsm_form_problem_priority'});
    m.push({from: 'itsm_form_incident_userscount', to: 'itsm_form_problem_userscount'});
    m.push({from: 'itsm_form_incident_influence', to: 'itsm_form_problem_influence'});
    m.push({from: 'itsm_form_incident_confitemlink', to: 'itsm_form_problem_confitemlink'});
    m.push({from: 'itsm_form_incident_urgency', to: 'itsm_form_problem_urgency'});
    m.push({from: 'itsm_form_incident_category', to: 'itsm_form_problem_category'});
    m.push({from: 'itsm_form_incident_description', to: 'itsm_form_problem_description'});
    m.push({from: 'itsm_form_incident_servicelink', to: 'itsm_form_problem_servicelink'});
    m.push({from: 'CURRENT_DOCUMENTID', to: 'itsm_form_problem_parent_incident'});
    checkSavedForm(() => {
      createDocument('itsm_registry_problems', m);
    });
  },

  inRFC: function(){
    let m = [];
    m.push({from: 'itsm_form_incident_servicelink', to: 'itsm_form_rfc_servicelink'}); //Услуга
    m.push({from: 'itsm_form_incident_confitemlink', to: 'itsm_form_rfc_ci'}); //Конфигурационные единицы
    m.push({from: 'itsm_form_incident_theme', to: 'itsm_form_rfc_theme'}); //Тема
    m.push({from: 'itsm_form_incident_description', to: 'itsm_form_rfc_description'}); //Описание
    m.push({from: 'CURRENT_DOCUMENTID', to: 'itsm_form_rfc_incidentlink'}); //Ссылка на обращение
    checkSavedForm(() => {
      createDocument('itsm_registry_rfc', m, [{id: 'itsm_form_rfc_source', type: 'listbox', key: '2', value: 'Обращение'}]);
    });
  },

  showHistory: function(){
    function processFilter(processes) {
      let result = [];
      function search(p) {
        p.forEach(function(process) {
          if (process.comment.indexOf('Форма завершения инцидента') != -1) result.push(process);
          if (process.subProcesses.length > 0) search(process.subProcesses);
        });
      }
      search(processes);
      return result.sort((a,b) => new Date(a.started) - new Date(b.started)).filter(x => x.actionID);
    }

    Cons.showLoader();

    let dialog = $('<div class="uk-modal-container" uk-modal>');
    let md = $('<div>', {class: 'uk-modal-dialog'});
    let modalBody = $('<div class="uk-modal-body" uk-overflow-auto>');
    dialog.append(md);
    md.append(`<div class="uk-modal-header"><h2 class="uk-modal-title">История инцидента ${formPlayer.model.getModelWithId('itsm_form_incident_id').getTextValue()}</h2></div>`)
      .append(modalBody)
      .append('<div class="uk-modal-footer uk-text-right"><button class="uk-button uk-button-default uk-modal-close" type="button">Закрыть</button></div>');

    let oldDateHeader = formPlayer.model.getModelWithId('itsm_form_incident_regdate').getValue();
    let oldDateHN = new Date(""+oldDateHeader).toLocaleString('ru', {year: 'numeric', month: 'long', day: 'numeric'});
    let newDateHeader = oldDateHN.replace(/(?:([А-Я]{3}).+$)/i,'$1, ' + oldDateHeader.split(' ')[1].substr(0,5));

    let tableData = [];
    tableData.push({
      'Статус': 'Зарегистрирован',
      'Автор': formPlayer.model.getModelWithId('itsm_form_incident_author').getTextValue(),
      'Дата': newDateHeader,
      'Пользователь':  formPlayer.model.getModelWithId('itsm_form_incident_author').getTextValue(),
      'Подразделение': formPlayer.model.getModelWithId('itsm_form_incident_authorDepartment').getTextValue(),
      'Информация': ''
    });

    let autoAssign = formPlayer.model.getModelWithId('itsm_form_incident_auto_assignment');
    if(autoAssign && autoAssign.getValue() && autoAssign.getValue()[0] == '1') {
      tableData.push({
        'Статус': 'На очереди',
        'Автор': formPlayer.model.getModelWithId('itsm_form_incident_author').getTextValue(),
        'Дата': newDateHeader,
        'Пользователь':  formPlayer.model.getModelWithId('itsm_form_incident_responsible').getTextValue(),
        'Подразделение': formPlayer.model.getModelWithId('itsm_form_incident_responsibleDepartment').getTextValue(),
        'Информация': 'Направлено: ' + formPlayer.model.getModelWithId('itsm_form_incident_responsible').getTextValue()
      });
    }

    let historyTable = UTILS.createTable(tableData);
    historyTable.addClass('uk-table uk-table-striped uk-table-responsive')
    let tbody = historyTable.find('tbody');

    $.when(AS.FORMS.ApiUtils.getDocumentIdentifier(formPlayer.model.asfDataId))
    .then(documentID => AS.FORMS.ApiUtils.simpleAsyncGet('rest/api/workflow/get_execution_process?documentID=' + documentID))
    .then(processes => {
      processes = processFilter(processes);
      processes.forEach(process => {
        let tr = $('<tr>');
        tbody.append(tr);

        AS.FORMS.ApiUtils.simpleAsyncGet('rest/api/workflow/work/get_completion_data?workID=' + process.actionID)
        .then(work => AS.FORMS.ApiUtils.loadAsfData(work.result.dataUUID))
        .then(asfData => {
          let status = UTILS.getValue(asfData, 'itsm_form_incident_wcf_status');
          let department = UTILS.getValue(asfData, 'itsm_form_incident_wcf_responsibleDepartment').value || '';
          let department_inprogress = UTILS.getValue(asfData, 'itsm_form_incident_wcf_responsibleDepartment_inprogress').value || '';
          let user = UTILS.getValue(asfData, 'itsm_form_incident_wcf_responsible').value || '';
          let user_inprogress = UTILS.getValue(asfData, 'itsm_form_incident_wcf_responsible_inprogress').value || '';
          let info = '';

          let username = ([6,10,12].indexOf(+status.key) !== -1 ? 'Системный пользователь' : (status.key == 3 ? user_inprogress : user ? user : user_inprogress));
          department = status.key == 3 ? department_inprogress : department ? department : department_inprogress;

          switch (+status.key) {
            case 2:
            case 3:
              info = 'Направлено: ' + username + ',<br>' + department; break;
            case 4: info = 'Запрос: ' + UTILS.getValue(asfData, 'itsm_form_incident_wcf_return_to_author').value; break;
            case 11: info = 'Комментарий: ' + UTILS.getValue(asfData, 'itsm_form_incident_wcf_comment').value; break;
            case 7:
              info = 'Код решения: ' + UTILS.getValue(asfData, 'itsm_form_incident_wcf_decisiontype').value; + '<br>Описание: ' + UTILS.getValue(asfData, 'itsm_form_incident_wcf_decisiondescription').value;
              break;
            case 9:
              //Дата поставки: ' + UTILS.getValue(asfData, 'itsm_form_incident_wcf_supply_date').value + '<br>
              info = 'Поставщик:  ' + UTILS.getValue(asfData, 'itsm_form_incident_wcf_supplier').value;
              break;
            case 8: info = 'Дата: ' + UTILS.getValue(asfData, 'itsm_form_incident_wcf_equipment_date').value; break;
            case 10:
              info = 'Инициатор: ' + formPlayer.model.getModelWithId('itsm_form_incident_author').getTextValue();
              department = "" + formPlayer.model.getModelWithId('itsm_form_incident_authorDepartment').getTextValue();
              break;
            case 12:
              info = 'Инициатор: ' + username;
              department = "" + formPlayer.model.getModelWithId('itsm_form_incident_authorDepartment').getTextValue();
              break;
            case 6:
              info = 'Инициатор: ' + username;
              department = "" + formPlayer.model.getModelWithId('itsm_form_incident_authorDepartment').getTextValue();
              break;
            case 14: info = 'Описание: ' + UTILS.getValue(asfData, 'itsm_form_incident_wcf_approval_description').value; break;
            case 15:
            case 16:
              info = 'Комментарий: ' + UTILS.getValue(asfData, 'itsm_form_incident_wcf_approval_comment').value;
              break;
          }

          let oldDateN = new Date(""+process.finished).toLocaleString('ru', {year: 'numeric', month: 'long', day: 'numeric'});
          let newDate = oldDateN.replace(/(?:([А-Я]{3}).+$)/i,'$1, ' + process.finished.split(' ')[1].substr(0,5));
          let row = {
            'Статус': status.value,
            'Автор': UTILS.getValue(asfData, 'itsm_form_incident_wcf_current_user').value,
            'Дата': newDate,
            'Пользователь':  username,
            'Подразделение': department,
            'Информация': info
          };
          for (let key in row) tr.append($('<td>').html(row[key]));
        });
      });
    });

    modalBody.append(historyTable);
    UIkit.modal(dialog).show();
    dialog.on('shown', () => Cons.hideLoader());
    dialog.on('hidden', () => dialog.remove());
  },

  init: function(){
    //инициализация кнопки "Завершить"
    rest.synergyGet("api/workflow/get_execution_process?documentID=" + dataRow.documentID, res => {
      let processes = UTILS.getUserWork(res, AS.OPTIONS.currentUser.userid, 'ASSIGNMENT_ITEM');
      if(processes.length > 0) {
        createButton('Завершить', 'itsm-button2', () => {
          finishWork(processes, 'itsm_form_incident_wcf_new');
        }, true);
        $('#work-name').show()
        .text(cutText(processes[0].name))
        .attr('title', processes[0].name);
      }
    }, err => {
      console.log(err);
    });
    let statusIncident = formPlayer.model.getModelWithId('itsm_form_incident_status');

    rest.synergyGet("api/person/auth?getGroups=true", res => {
      let resGroup = res.groups.find(i => i.groupCode == 'itsm_group_reassign_access');
      let rfcGroup = res.groups.find(i => i.groupCode == 'itsm_group_button_rfc');
      if(statusIncident) {
        statusIncident = statusIncident.getValue();
        if(resGroup && ['2', '3', '10'].indexOf(statusIncident[0]) !== -1) {
          createButton('Переназначить', 'itsm-button3', this.reassign);
        }
        if(Cons.getAppStore().statusesInProblem.indexOf(statusIncident[0]) !== -1) {
          createButton('В проблему', 'itsm-button5', this.inProblem);
        }
        if(rfcGroup && ['1', '10', '2', '3'].indexOf(statusIncident[0]) !== -1) {
          createButton('Запрос на изменение', 'itsm-button6', this.inRFC);
        }
      }
    }, err => console.log(err));

    createButton('История', 'itsm-button4', this.showHistory);
    createEvent('opened', dataRow.documentID);
  }
}


let Problem = {
  changeRequest: function(){
    let m = [];
    m.push({from: 'itsm_form_problem_parent_incident', to: 'itsm_form_rfc_incidentes.incedent'});
    m.push({from: 'itsm_form_problem_servicelink', to: 'itsm_form_rfc_servicelink'});
    m.push({from: 'itsm_form_problem_confitemlink', to: 'itsm_form_rfc_ci'});
    m.push({from: 'CURRENT_DOCUMENTID', to: 'itsm_form_rfc_problemlink'});
    checkSavedForm(() => {
      createDocument('itsm_registry_rfc', m);
    });
  },

  init: function(){
    //инициализация кнопки "Завершить"
    rest.synergyGet("api/workflow/get_execution_process?documentID=" + dataRow.documentID, res => {
      let processes = UTILS.getUserWork(res, AS.OPTIONS.currentUser.userid, 'ASSIGNMENT_ITEM');
      if(processes.length > 0) {
        createButton('Завершить', 'itsm-button2', () => {
          finishWork(processes, 'itsm_form_problem_wcf');
        }, true);
        $('#work-name').show()
        .text(cutText(processes[0].name))
        .attr('title', processes[0].name);
      }
    }, err => {
      console.log(err);
    });
    createButton('Запрос на изменение', 'itsm-button3', this.changeRequest);
  }
}

//База знаний
let Knowledgebase = {
  init: function(){
    //инициализация кнопки "Согласования"
    rest.synergyGet("api/workflow/get_execution_process?documentID=" + dataRow.documentID, res => {
      let processes = UTILS.getUserWork(res, AS.OPTIONS.currentUser.userid, 'AGREEMENT_ITEM');
      if(processes.length > 0) {
        createButton('Согласовать', 'itsm-button2', () => {
          agreementDoc(processes[0], "Согласование");
        }, true);
        $('#button-edit').hide();
        $('#work-name').show()
        .text(`Прошу согласовать - ${cutText(dataRow.meaning)}`)
        .attr('title', `Прошу согласовать - ${dataRow.meaning}`);
      }
    });
  }

}

//Права доступа: Заявки [itsm_registry_access_orders]
let AccessOrders = {
  init: function(){
    rest.synergyGet("api/workflow/get_execution_process?documentID=" + dataRow.documentID, res => {
      let processes1 = UTILS.getUserWork(res, AS.OPTIONS.currentUser.userid, 'ASSIGNMENT_ITEM');
      let processes2 = UTILS.getUserWork(res, AS.OPTIONS.currentUser.userid, 'AGREEMENT_ITEM');
      //инициализация кнопки "Завершить"
      if(processes1.length > 0) {
        createButton('Завершить', 'itsm-button2', () => {
          finishWorkNotForm(processes1);
        }, true);
        $('#work-name').show()
        .text(cutText(processes1[0].name))
        .attr('title', processes1[0].name);
      }
      //инициализация кнопки "Согласования"
      if(processes2.length > 0) {
        createButton('Согласовать', 'itsm-button3', () => {
          agreementDoc(processes2[0], "Согласование");
        }, true);
        $('#button-edit').hide();
        $('#work-name').show()
        .text(`Прошу согласовать - ${cutText(dataRow.meaning)}`)
        .attr('title', `Прошу согласовать - ${dataRow.meaning}`);
      }
    });
  }
}

//"Запросы на изменения RFC"(itsm_registry_rfc)
let RFC = {
  inChanges: function(){
    let m = [];
    m.push({from: 'itsm_form_rfc_author', to: 'itsm_form_change_rfc_author'});
    m.push({from: 'itsm_form_rfc_system_admin', to: 'itsm_form_cr_system_admin'});
    m.push({from: 'itsm_form_rfc_problemlink', to: 'itsm_form_change_problemlink'});
    m.push({from: 'itsm_form_rfc_servicelink', to: 'itsm_form_change_servicelink'});
    m.push({from: 'itsm_form_rfc_description', to: 'itsm_form_change_description'});
    m.push({from: 'itsm_form_rfc_ci', to: 'itsm_form_change_ci'});
    m.push({from: 'CURRENT_DOCUMENTID', to: 'itsm_form_change_rfclink'});
    checkSavedForm(() => {
      createDocument('itsm_registry_changes', m);
    });
  },

  init: function(){
    rest.synergyGet("api/workflow/get_execution_process?documentID=" + dataRow.documentID, res => {
      //инициализация кнопки "Завершить"
      let assignProc = UTILS.getUserWork(res, AS.OPTIONS.currentUser.userid, 'ASSIGNMENT_ITEM');
      if(assignProc.length > 0) {
        if(assignProc[0].code === 'itsm_route_rfc_done') {
          createButton('Завершить', 'itsm-button2', () => {
            finishWorkNotForm(assignProc);
          }, true);
        } else {
          createButton('Завершить', 'itsm-button2', () => {
            finishWork(assignProc, 'itsm_form_rfc_wcf');
          }, true);
        }
        $('#work-name').show()
        .text(cutText(assignProc[0].name))
        .attr('title', assignProc[0].name);
      }

      //инициализация кнопки "Согласовать"
      let agreeProc = UTILS.getUserWork(res, AS.OPTIONS.currentUser.userid, 'AGREEMENT_ITEM');
      if(agreeProc.length > 0) {
        createButton('Согласовать', 'itsm-button3', () => {
          agreementDoc(agreeProc[0], 'Согласование');
        }, true);
        $('#button-edit').hide();
        $('#work-name').show()
        .text(`Прошу согласовать - ${cutText(dataRow.meaning)}`)
        .attr('title', `Прошу согласовать - ${dataRow.meaning}`);
      }

      //инициализация кнопки "Ознакомиться"
      let acquaProc = UTILS.getUserWork(res, AS.OPTIONS.currentUser.userid, 'ACQUAINTANCE_ITEM');
      if(acquaProc.length > 0) {
        createButton('Ознакомиться', 'itsm-button4', () => {
          agreementDoc(acquaProc[0], 'Ознакомление');
        }, true);
        $('#button-edit').hide();
        $('#work-name').show()
        .text(`Ознакомиться - ${cutText(dataRow.meaning)}`)
        .attr('title', `Ознакомиться - ${dataRow.meaning}`);
      }
    });

    createButton('В изменение', 'itsm-button5', this.inChanges);
  }
}

//"Изменения CR"(itsm_registry_changes)
let Changes = {
  init: function(){
    rest.synergyGet("api/workflow/get_execution_process?documentID=" + dataRow.documentID, res => {
      //инициализация кнопки "Завершить"
      let assignProc = UTILS.getUserWork(res, AS.OPTIONS.currentUser.userid, 'ASSIGNMENT_ITEM');
      if(assignProc.length > 0) {
        createButton('Завершить', 'itsm-button2', () => {
          finishWork(assignProc, 'itsm_form_cr_wcf');
        }, true);
        $('#work-name').show()
        .text(cutText(assignProc[0].name))
        .attr('title', assignProc[0].name);
      }
      //инициализация кнопки "Ознакомиться"
      let acquaProc = UTILS.getUserWork(res, AS.OPTIONS.currentUser.userid, 'ACQUAINTANCE_ITEM');
      if(acquaProc.length > 0) {
        createButton('Ознакомиться', 'itsm-button3', () => {
          agreementDoc(acquaProc[0], 'Ознакомление');
        }, true);
        $('#button-edit').hide();
        $('#work-name').show()
        .text(`Ознакомиться - ${cutText(dataRow.meaning)}`)
        .attr('title', `Ознакомиться - ${dataRow.meaning}`);
      }
    });
  }
}

function mobileRightMenuInit(){
  let mobileButtonContainer = $('.itsm-mobile-buttons-container');
  let mobileMenu = $('<ul class="uk-nav-default" uk-nav>');
  mobileButtonContainer.empty();
  mobileButtonContainer.append(mobileMenu);

  let saveItem = $('<li class="mobile-button-save"><a href="javascript:void(0);"><span class="uk-margin-small-right" uk-icon="icon: pull"></span>Сохранить</a></li>');
  let editItem = $('<li><a href="javascript:void(0);"><span class="uk-margin-small-right" uk-icon="icon: file-edit"></span>Редактировать</a></li>');
  let viewItem = $('<li><a href="javascript:void(0);"><span class="uk-margin-small-right" uk-icon="icon: file-text"></span>Просмотр</a></li>');
  let printItem = $('<li><a href="javascript:void(0);"><span class="uk-margin-small-right" uk-icon="icon: print"></span>Печать</a></li>');

  mobileMenu
  .append(editItem)
  .append(viewItem)
  .append('<li class="uk-nav-divider"></li>')
  .append(saveItem)
  .append('<li class="uk-nav-divider mobile-button-save"></li>')
  .append(printItem);

  $('.mobile-button-save').hide();
  saveItem.on('click', e => {
    UIkit.offcanvas('.itsm-mobile-menu-right').hide();
    e.preventDefault();
    e.target.blur();
    saveFormData();
  });

  printItem.on('click', e => {
    UIkit.offcanvas('.itsm-mobile-menu-right').hide();
    e.preventDefault();
    e.target.blur();
    printForm();
  });

  editItem.on('click', e => {
    UIkit.offcanvas('.itsm-mobile-menu-right').hide();
    e.preventDefault();
    e.target.blur();
    editItem.hide();
    viewItem.show();
    fire({type: 'set_form_editable', editable: true}, 'formPlayer-1');
  });

  viewItem.on('click', e => {
    UIkit.offcanvas('.itsm-mobile-menu-right').hide();
    e.preventDefault();
    e.target.blur();
    editItem.show();
    viewItem.hide();
    fire({type: 'set_form_editable', editable: false}, 'formPlayer-1');
  });

  if(formPlayer.view.editable) {
    editItem.hide();
    viewItem.show();
  } else {
    editItem.show();
    viewItem.hide();
  }

}

function buttonInit() {
  buttonContainer.empty();
  $('#work-name').empty().attr('title', null).hide();
  let registryCode = getParam("registryCode");
  let regInfo = getRegInfo(registryCode);
  rest.synergyGet("api/docflow/document_actions?documentID=" + dataRow.documentID, res => {
    res = res.filter(x => x.operation == 'RUN')[0];
    // Если запись не отправлена, создаем кнопку с активацией документа
    if(res) {
      if(!regInfo) {
        //в случае если нет информации по реестру
        createButton(res.label, 'itsm-button1', () => activateDocument(regInfo), true);
      } else if(regInfo.rights.indexOf('rr_create') !== -1) {
        //Если есть права на создание
        createButton(res.label, 'itsm-button1', () => activateDocument(regInfo), true);
      }
      if(registryCode == "itsm_registry_incidents") createEvent('opened', dataRow.documentID);
    } else {
      // иначе инициализируем кнопки
      switch (registryCode) {
        case "itsm_registry_incidents":  Incident.init(); break;
        case "itsm_registry_problems":  Problem.init(); break;
        case "itsm_registry_knowledgebase":  Knowledgebase.init(); break;
        case "itsm_registry_access_orders":  AccessOrders.init(); break;
        case "itsm_registry_rfc":  RFC.init(); break;
        case "itsm_registry_changes":  Changes.init(); break;
        default:
      }
    }
    //Если нет прав на редактирование
    if(regInfo && regInfo.rights.indexOf('rr_edit') == -1) {
      $('#button-edit').hide();
    }
  }, err => {
    console.log(err);
  });

  $('#save-button').off().hide();
  $('#save-button').on('click', e => {
    e.preventDefault();
    e.target.blur();
    saveFormData();
  });
  formPlayer.model.save = true;

  $('#button-print-form').off();
  $('#button-print-form').on('click', printForm);

  if(formPlayer.view.editable) {
    fire({type: 'button_click'}, 'button-edit');
  }

  if($(document).width() > 768) {
    $('#form-panel').height($(document).height() - 200);
  } else {
    $('#form-panel').height($(document).height() - 100);
  }

  mobileRightMenuInit();
}

function closeWindow(event){
  pages = cookie.getJson('pages');
  if(!pages) return;
  let openDocs = Cons.getAppStore().openDocs || [];
  openDocs.forEach(doc => createEvent('closed', doc));

  if($(event.target.activeElement)[0].id == 'button-logout') return;

  let cookieUserExpires = 30; //30 минут сохранять сессию пользовател
  if(itsmUser) {
    cookie.setJson('itsm-arm-user', itsmUser, {expires: cookieUserExpires*60});
  }
  pages.currentPage = 'app';
  cookie.setJson('pages', pages);

  if(formPlayer.model.save) return;
  event.preventDefault();
  event.returnValue = '';
}

setTimeout(buttonInit, 500);

let viewCode = getParam('viewCode');
if(viewCode) fire({type: 'show_form_view', viewCode: viewCode}, 'formPlayer-1');

const setCustomColorLabel = () => {
  let asfData = formPlayer.model.getAsfData();
  if(asfData && asfData.hasOwnProperty('data')) {
    asfData.data.forEach(item => {
      if(item.type == 'label') {
        let tmpModel = formPlayer.model.getModelWithId(item.id);
        if(tmpModel && tmpModel.hasOwnProperty('color')) {
          $('[data-asformid="label.label.'+item.id+'"]').css("color", tmpModel.color);
        }
      }
    });
  }
}

addListener('changed_form_data', 'formPlayer-1', e => {
  if(e.view.editable) {
    formPlayer.model.save = false;
    $('#save-button').show();
    $('.mobile-button-save').show();
  }
});

addListener('loaded_form_data', 'formPlayer-1', e => {
  setCustomColorLabel();
});

addListener('set_form_editable', 'formPlayer-1', e => {
  setCustomColorLabel();
});

//Динамическое изменение размера окна проигрывателя форм
$(window).resize(() => {
  if($(document).width() > 768) {
    $('#form-panel').height($(document).height() - 200);
  } else {
    $('#form-panel').height($(document).height() - 100);
  }
});
window.addEventListener('beforeunload', closeWindow, false);
