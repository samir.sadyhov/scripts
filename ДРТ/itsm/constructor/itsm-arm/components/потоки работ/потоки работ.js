const getParentFilter = () => AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/workflow/works/filters/${AS.OPTIONS.currentUser.userid}`);

const getDefaultIcon = () => $('<span uk-icon="icon: folder; ratio: 1.2;" class="itsm-nav-icon uk-margin-small-left" style="height: auto; color: #1E87F0;">');

const getProgressBar = value => `<div class="work-progress-container"><div class="work-progress" style="width: ${value}%;">${value}%</div></div>`;

const initWorkFilters = () => {
  let menuContainer = $('#work-filter');
  let mobileMenuContainer = $('.itsm-mobile-menu-left .itsm-menu-content-left');
  let nav = $(`<ul class="uk-nav uk-nav-parent-icon itsm-nav" uk-nav>`);
  let navMobile = $(`<ul class="uk-nav uk-nav-parent-icon itsm-nav" uk-nav>`);

  menuContainer.empty().append(nav);
  mobileMenuContainer.empty().append(navMobile);

  getParentFilter().then(parents => {
    parents.forEach(parent => {
      let menuItem = $('<li>');
      let menuItemMobile = $('<li>');
      let a = $('<a>').addClass('itsm-nav-link uk-link-text').attr('filtercode', parent.filterType);
      let aMobile = $('<a>').addClass('itsm-nav-link uk-link-text').attr('filtercode', parent.filterType);

      a.append(getDefaultIcon()).append(`<span class="itsm-nav-link-text">${parent.name}</span>`);
      aMobile.append(getDefaultIcon()).append(`<span class="itsm-nav-link-text">${parent.name}</span>`);
      menuItem.append(a);
      menuItemMobile.append(aMobile);
      nav.append(menuItem);
      navMobile.append(menuItemMobile);

      let allWork = workTable.counters.find(x => x.key == `${parent.filterType}_null_null`).value;

      if(parent.filterType == 'OWN_WORKS') {
        let newWork = workTable.counters.find(x => x.key == `${parent.filterType}_null_null_`).value;
        a.append(`<span class="uk-badge" style="margin-right: 5px;">${newWork}/${allWork}</span>`);
      } else {
        a.append(`<span class="uk-badge" style="margin-right: 5px;">${allWork}</span>`);
      }

      if(parent.filterType == workTable.filterType) {
        a.addClass('itsm-nav-link-active');
        aMobile.addClass('itsm-nav-link-active');
      }

      function changeItem(){
        $('.itsm-nav-link').removeClass("itsm-nav-link-active");
        a.addClass("itsm-nav-link-active");
        aMobile.addClass("itsm-nav-link-active");
        itsmPaginator.reset();
        workTable.filterType = parent.filterType;
        workTable.search = null;
        $('#itsm-search-input').val(null);
        workTable.render();
      }

      a.on('click', e => {
        e.preventDefault();
        e.target.blur();
        if(a.hasClass('itsm-nav-link-active')) return;
        changeItem();
      });

      aMobile.on('click', e => {
        e.preventDefault();
        e.target.blur();
        if(aMobile.hasClass('itsm-nav-link-active')) return;
        UIkit.offcanvas('.itsm-mobile-menu-left').hide();
        changeItem();
      });

    });
  });
}

const initPeriodFilter = () => {
  if(!$('#work-period').is(':empty')) return;

  const options = [
    {title: 'В работе', value: '0'},
    {title: 'Прошедший квартал', value: '9'},
    {title: 'Прошедший месяц', value: '8'},
    {title: 'Прошедшая неделя', value: '7'},
    {title: 'Сегодня', value: '6'},
    {title: 'Будущая неделя', value: '2'},
    {title: 'Будущий месяц', value: '3'},
    {title: 'Будущий квартал', value: '4'}
  ];

  let label = $('<label class="uk-form-label uk-margin-small-left uk-text-bold uk-text-background" for="work-period-filter">Период работ</label>')
  let select = $('<select class="uk-select" id="work-period-filter" style="min-width: 100px;">');
  options.forEach(item => select.append(`<option value="${item.value}" title="${item.title}">${item.title}</option>`));

  select.on('change', e => {
    e.preventDefault();
    e.target.blur();
    if(select.val() == '0') {
      workTable.periodType = null;
    } else {
      workTable.periodType = select.val();
    }
    workTable.render();
  });

  $('#work-period').empty().append(label).append(select);
}

let tableContainer = $('.itsm-table-container');

let itsmPaginator = {
  container: $('<div class="itsm-pt-container">'),
  paginator: $('<div class="itsm-pt-paginator">'),
  pContent: $('<div class="itsm-pt-paginator-content">'),
  bPrevious: $('<button class="itsm-pt-previous" disabled="disabled" title="Назад">'),
  bNext: $('<button class="itsm-pt-next" disabled="disabled" title="Вперед">'),
  label: $('<label>'),
  input: $('<input type="text">'),
  countInPart: 15,
  rows: 0,
  currentPage: 1,
  pages: 0,

  init: function(){
    $('.itsm-pt-container').remove();
    $('.itsm-work-container').after(this.container);
    this.pContent.append(this.label).append(this.input);
    this.paginator.append(this.bPrevious).append(this.pContent).append(this.bNext);
    this.container.append(this.paginator);
    this.reset();

    this.bNext.click(() => {
      this.currentPage++;
      this.update();
      workTable.createBody();
    });

    this.bPrevious.click(() => {
      this.currentPage--;
      this.update();
      workTable.createBody();
    });

    this.label.on('dblclick', e => {
      e.preventDefault();
      this.input.show();
      this.label.hide();

      this.input.on('blur', () => {
        this.label.show();
        this.input.hide();
      });

      this.input.val("" + this.currentPage);

      this.input.on('keypress', e => {
        if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) return false;
      });

      this.input.on('keydown', e => {
        if(e.which === 13) {
          let inputVal = +this.input.val();
          if(inputVal && inputVal != this.currentPage && inputVal <= this.pages && inputVal >= 1){
            this.currentPage = inputVal;
            this.update();
            workTable.createBody();
          }
          this.label.show();
          this.input.hide();
          this.input.off();
        }
      });
      this.input.focus();
    });

  },

  update: function(){
    this.pages = Math.ceil(this.rows / this.countInPart),
    this.label.text(this.currentPage + ' / ' + this.pages);

    if(this.pages == 0) {
      this.bPrevious.attr('disabled', 'disabled');
      this.bNext.attr('disabled', 'disabled');
    } else {
      if(this.currentPage == 1) {
        this.bPrevious.attr('disabled', 'disabled');
        if (this.currentPage == this.pages) {
          this.bNext.attr('disabled', 'disabled');
        } else {
          this.bNext.removeAttr('disabled');
        }
      } else {
        this.bPrevious.removeAttr('disabled');
        if (this.currentPage == this.pages) {
          this.bNext.attr('disabled', 'disabled');
        } else {
          this.bNext.removeAttr('disabled');
        }
      }
    }
  },

  reset: function(){
    this.countInPart = 15;
    this.rows = 15;
    this.currentPage = 1;
    this.pages = 0;
  }
}

let workTable = {
  filterType: 'OWN_WORKS',
  search: null,
  counters: null,
  periodType: null,

  isAscSort: true,
  sortColumn: null,

  table: null,
  tHead: null,
  tBody: null,

  getUrl: function(){
    let url = `api/workflow/works/list_ext?userID=${AS.OPTIONS.currentUser.userid}&filterType=${this.filterType}`;
    url += `&startRecord=${itsmPaginator.currentPage - 1}&recordsCount=${itsmPaginator.countInPart}`;
    if(this.search) url += `&search=${this.search}`;
    if(this.periodType) url += `&hasExtSearchParams=true&periodType=${this.periodType}`;
    if(this.sortColumn) url += `&sortColumn=${this.sortColumn}&isAscSort=${this.isAscSort}`;
    return url;
  },

  sortable: function(th) {
    this.tHead.find('th').not(th).removeClass('sortable sortable__down sortable__up');
    if (th.hasClass("sortable__down") || th.hasClass("sortable__up")) {
      th.toggleClass("sortable__down sortable__up");
    } else {
      th.addClass("sortable sortable__down");
    }
    this.sortColumn = th.attr('columnid');
    this.isAscSort = th.hasClass("sortable__down") ? true : false;
    this.createBody();
  },

  createHeader: function() {
    this.tHead.empty();
    this.isAscSort = true;
    this.sortColumn = null;

    let tr = $('<tr>');
    tr.append('<th columnid="name" class="uk-table-expand">Название</th>');
    if(this.filterType == 'OWN_WORKS') {
      tr.append('<th columnid="author" class="uk-width-small">Автор</th>');
    } else {
      tr.append('<th columnid="responsible" class="uk-width-small">Ответственный</th>');
    }
    tr.append('<th columnid="left" class="uk-width-small">Осталось</th>');
    tr.append('<th columnid="percent" class="uk-width-small">Прогресс</th>');

    tr.on('click', e => this.sortable($(e.target)));

    this.tHead.append(tr);
  },

  openWork: function(actionID) {
    let params = {actionID: actionID};
    Cons.showLoader();
    try {
      AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/workflow/work/${actionID}/document`).then(doc => {
        if(doc && doc.documentID) {
          params.documentID = doc.documentID;
          return AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/docflow/doc/document_info?documentID=${params.documentID}`);
        }
      }).then(docInfo => {
        params.docInfo = docInfo;
        return AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/info?registryID=${params.docInfo.registryID}`);
      }).then(regInfo => {
        Cons.hideLoader();

        let event = {
          dataUUID: params.docInfo.asfDataID,
          registryCode: regInfo.code,
          registryName: regInfo.name,
          formName: params.docInfo.formName,
          editable: false
        };

        if(event.dataUUID) {
          $('#root-panel').trigger({type: 'custom_open_document', eventParam: event});
          AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/workflow/work/set_seen?workID=${actionID}`);
        } else {
          itsmShowMessage({message: "Ошибка при открытии работы", type: 'error'});
        }

      });
    } catch (e) {
      Cons.hideLoader();
      console.log(e);
    }
  },

  createRow: function(item) {
    let tr = $('<tr>');

    if(item.is_new && item.is_new == "true") tr.addClass('new-work');

    tr.append(`<td title="${item.name}">${item.name}</td>`);
    if(this.filterType == 'OWN_WORKS') {
      tr.append(`<td title="${item.author.name}">${item.author.name}</td>`);
    } else {
      tr.append(`<td title="${item.user.name}">${item.user.name}</td>`);
    }
    tr.append(`<td>${item.remained_label}</td>`);
    tr.append(`<td>${getProgressBar(item.percent)}</td>`);

    tr.on('click', () => {
      this.tBody.find('tr').removeAttr('selected');
      tr.attr('selected', true);
      if($(document).width() < 960) this.openWork(item.actionID);
    });
    tr.on('dblclick', () => {
      this.openWork(item.actionID);
    });

    return tr;
  },

  createBody: function(rowsCount) {
    Cons.showLoader();
    rest.synergyGet(this.getUrl(), res => {
      this.tBody.empty();
      res.data.forEach(item => this.tBody.append(this.createRow(item)));
      if(this.search || this.periodType) {
        itsmPaginator.rows = res.dataCount;
      } else {
        itsmPaginator.rows = this.counters.find(x => x.key == `${this.filterType}_null_null`).value;
      }
      itsmPaginator.update();
      $('.itsm-table-container').scrollTop(0);
      Cons.hideLoader();
    });
  },

  render: function() {
    this.table = $('<table class="itsm-table uk-table uk-table-small uk-table-striped uk-table-responsive">');
    this.tHead = $('<thead>');
    this.tBody = $('<tbody>');
    this.table.append(this.tHead).append(this.tBody);
    tableContainer.empty().append(this.table);
    this.createHeader();
    this.createBody();
  }

};

const refreshAllData = () => {
  AS.FORMS.ApiUtils.simpleAsyncGet('rest/api/workflow/get_counters').then(res => {
    workTable.counters = res;
    workTable.search = null;
    $('#itsm-search-input').val(null);
    initWorkFilters();
    initPeriodFilter();
    workTable.render();
    itsmPaginator.init();
  });
}

refreshAllData();

$('.itsm-refresh-button').off().on('click', e => {
  e.preventDefault();
  e.target.blur();
  refreshAllData();
});

$('#itsm-search-icon').off().on('click', e => {
  e.preventDefault();
  e.target.blur();
  e.stopPropagation();
  workTable.search = $('#itsm-search-input').val() || null;
  workTable.createBody();
});

$('#itsm-search-input').off().on('keyup', e => {
  if(e.keyCode === 13) {
    e.preventDefault();
    e.target.blur();
    workTable.search = $('#itsm-search-input').val() || null;
    workTable.createBody();
  }
});
