const registries = Cons.getAppStore().registries;
const currentPage = Cons.getCurrentPage();

let cardContainer = $('#itsm-card-container')
cardContainer.attr('uk-grid', 'uk-grid').empty();

setTimeout(() => {
  $('.itsm-mobile-menu-registry').hide();
  $('#divider1').hide();
}, 550);

const createCard = item => {
  let create = false;
  let filter = null;

  if(item.children) {
    if(item.children.filter(x => x.rights.length > 0 || x.filters.length > 0).length > 0) create = true;
    for(let i = 0; i < item.children.length; i++) {
      let x = item.children[i];
      if(x.rights.length > 0 && x.filters.length > 0) {
         filter = x.filters[0];
         break;
      }
    }
  } else {
    if(item.rights.length > 0 || item.filters.length > 0) create = true;
    if(item.rights.length == 0 && item.filters.length > 0) filter = item.filters[0];
  }

  if(!create) return null;

  let card = $('<div>').css({cursor: 'pointer'}).attr('title', item.name);
  let data = $('<div class="uk-card uk-card-default uk-card-hover uk-card-body">');
  let body = $('<div class="uk-grid-small uk-flex-middle" uk-grid>');

  body.append(`<div class="uk-width-auto" style="position: absolute;"><img width="25" height="25" src="${item.icon}"></div>`);
  body.append(`<h3 class="itsm-card-title uk-card-title uk-margin-remove-bottom">${item.name}</h3>`);
  data.append(body);
  card.append(data);

  card.on('click', () => {
    $('.itsm-mobile-menu-registry').show();
    if($(document).width() < 769) $('.mobile-button-right').show();
    $('#divider1').show();
    let tmRegCode = item.children ? item.children[0].code : item.code;
    let pageData = {
      currentPage: 'app',
      code: 'app',
      param: {
        registryCode: tmRegCode,
        children: item.children ? true : false
      }
    };

    if(filter) {
      pageData.param.filterCode = filter.code;
      pageData.param.filterID = filter.id;
      pageData.param.filterRights = filter.rights;
    }

    cookie.setJson('pages', pageData);
    fire({
      type: 'goto_page',
      pageCode: 'app',
      pageParams: [{pageParamName: 'registryCode', value: tmRegCode}]
    }, 'logoImg1');

  });

  return card;
}

registries.forEach(item => cardContainer.append(createCard(item)));


function r(){
  if($(document).width() < 640) {
    setTimeout(() => {
      if($(document).width() < 640) cardContainer.height($(document).height() - 100);
    }, 500);
  }
}
$(window).resize(() => r());
r();
