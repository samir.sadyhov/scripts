/*********************************************************************************************************************
	JavaScript | canvas arrows v.1.0 | updated: 25.10.2013 | author: michael verhov | http://michael.verhov.com | License: GNU GPL

	какая-то херня, с синержи данная библиотека херово совместима, не пойму почему, пришлось немного костылей вставить
**********************************************************************************************************************/
(function(g,c){var d=function(o,p){if(g===this){return new d(o,p)}this.options={base:{canvasZIndex:0,alertErrors:true,putToContainer:true},arrow:{connectionType:"rectangleAuto",arrowType:"arrow",arrowSize:9},render:{lineWidth:2,strokeStyle:"#2D6CA2"}};this.CanvasStorage=[[],[],[]];if(typeof o!=="string"){this.trowException("common parent must be specified")}let commonParentResult=document.querySelectorAll(o);if(commonParentResult.length===0){this.trowException("common parent not found")}for(let i=0;i<commonParentResult.length;i++){this.CanvasStorage[0][i]=commonParentResult[i]}this.CanvasStorage[0].length=commonParentResult.length;if(p!==c){if(p.base!==c){k(this.options.base,p.base)}if(p.render!==c){k(this.options.render,p.render)}if(p.arrow!==c){k(this.options.arrow,p.arrow)}}this.CanvasStorage[0][0].style.position="relative";let canvas=document.createElement("canvas");canvas.innerHTML="";canvas.style.position="absolute";canvas.style.left="0px";canvas.style.top="0px";canvas.width=this.CanvasStorage[0][0].scrollWidth;canvas.height=this.CanvasStorage[0][0].scrollHeight;if(this.options.canvasId!==c){canvas.id=this.options.canvasId}if(this.options.canvasClass!==c){canvas.className=this.options.canvasClass}this.CanvasStorage[0][0].insertBefore(canvas,this.CanvasStorage[0][0].firstChild);this.CanvasStorage[1].push(canvas);return this};function k(p,o){if(p&&o){for(let name in o){if(o[name]!==c){p[name]=o[name]}}}return p}function n(o,p){let canv=o.getBoundingClientRect();let box=p.getBoundingClientRect();return{top:box.top-canv.top,left:box.left-canv.left,width:p.offsetWidth,height:p.offsetHeight}}function l(o){return o*(Math.PI/180)}function h(o){return o*(180/Math.PI)}function b(o,p){let x=0;let y=0;switch(p){case"top":x=o.left+(o.width/2);y=o.top;break;case"right":x=o.left+o.width;y=o.top+(o.height/2);break;case"bottom":x=o.left+(o.width/2);y=o.top+o.height;break;case"left":x=o.left;y=o.top+(o.height/2);break;default:x=o.left+(o.width/2);y=o.top+o.height;break}return{x:x,y:y}}function f(o){return{x:o.left+o.width/2,y:o.top+(o.height/2)}}function e(o,q,p){let x;let y;let rAngle=Math.acos(Math.sqrt(Math.pow(o.left+o.width-q.x,2))/Math.sqrt(Math.pow(o.left+o.width-q.x,2)+Math.pow(o.top-q.y,2)));if(p>=2*Math.PI-rAngle||p<rAngle){x=o.left+o.width;y=q.y+Math.tan(p)*(o.left+o.width-q.x)}else{if(p>=rAngle&&p<Math.PI-rAngle){x=q.x-((o.top-q.y)/Math.tan(p));y=o.top+o.height}else{if(p>=Math.PI-rAngle&&p<Math.PI+rAngle){x=o.left;y=q.y-Math.tan(p)*(o.left+o.width-q.x)}else{if(p>=Math.PI+rAngle){x=q.x+((o.top-q.y)/Math.tan(p));y=o.top}}}}return{x:x,y:y}}function a(o,q,p){return{x:q.x+(o.width/2)*Math.cos(p),y:q.y+(o.height/2)*Math.sin(p)}}function m(o,r,q,p){let headlen=p.arrowSize;let angle=Math.atan2(q.y-r.y,q.x-r.x);o.beginPath();o.moveTo(r.x,r.y);o.lineTo(q.x,q.y);switch(p.arrowType){case"arrow":o.moveTo(q.x-headlen*Math.cos(angle-Math.PI/6),q.y-headlen*Math.sin(angle-Math.PI/6));o.lineTo(q.x,q.y);o.lineTo(q.x-headlen*Math.cos(angle+Math.PI/6),q.y-headlen*Math.sin(angle+Math.PI/6));break;case"line":break;case"double-headed":o.moveTo(r.x+headlen*Math.cos(angle-Math.PI/6),r.y+headlen*Math.sin(angle-Math.PI/6));o.lineTo(r.x,r.y);o.lineTo(r.x+headlen*Math.cos(angle+Math.PI/6),r.y+headlen*Math.sin(angle+Math.PI/6));o.moveTo(q.x-headlen*Math.cos(angle-Math.PI/6),q.y-headlen*Math.sin(angle-Math.PI/6));o.lineTo(q.x,q.y);o.lineTo(q.x-headlen*Math.cos(angle+Math.PI/6),q.y-headlen*Math.sin(angle+Math.PI/6));break;default:break}o.stroke()}function j(q,s,r,p,o){let context=q.getContext("2d");let arrowOpt={};let dot1=n(q,s);let dot2=n(q,r);let c1=f(dot1);let c2=f(dot2);k(arrowOpt,p.arrow);k(context,p.render);if(o!==c){if(o.render!==c){k(context,o.render)}if(o.arrow!==c){k(arrowOpt,o.arrow)}}switch(arrowOpt.connectionType){case"rectangleAuto":dot1=e(dot1,c1,Math.atan2(c1.y-c2.y,c1.x-c2.x)+Math.PI);dot2=e(dot2,c2,Math.atan2(c2.y-c1.y,c2.x-c1.x)+Math.PI);break;case"center":dot1=f(dot1);dot2=f(dot2);break;case"ellipseAuto":dot1=a(dot1,c1,Math.atan2(c2.y-c1.y,c2.x-c1.x));dot2=a(dot2,c2,Math.atan2(c1.y-c2.y,c1.x-c2.x));break;case"side":dot1=b(dot1,arrowOpt.sideFrom);dot2=b(dot2,arrowOpt.sideTo);break;case"rectangleAngle":dot1=e(dot1,f(dot1),l(arrowOpt.angleFrom));dot2=e(dot2,f(dot2),l(arrowOpt.angleTo));break;case"ellipseAngle":dot1=a(dot1,f(dot1),l(arrowOpt.angleFrom));dot2=a(dot2,f(dot2),l(arrowOpt.angleTo));break;default:break}m(context,dot1,dot2,arrowOpt)}d.fn=d.prototype={trowException:function(o){if(this.options.base.alertErrors===true){console.log("CanvasArrows error: "+o)}throw new Error(o)},arrow:function(q,p,o){let fromChildrens=this.CanvasStorage[0][0].querySelectorAll(q);let toChildrens=this.CanvasStorage[0][0].querySelectorAll(p);for(let fi=0;fi<fromChildrens.length;fi++){for(let ti=0;ti<toChildrens.length;ti++){j(this.CanvasStorage[1][0],fromChildrens[fi],toChildrens[ti],this.options,o)}if(this.options.base.putToContainer===true){this.CanvasStorage[2].push([q,p,o])}}return this},arrows:function(o){for(let i=0;i<o.length;i++){this.arrow(o[i][0],o[i][1],o[i][2])}return this},clear:function(){let canvas=this.CanvasStorage[1][0];let context=canvas.getContext("2d");context.clearRect(0,0,canvas.width,canvas.height);return this},draw:function(){let putToContainer=this.options.base.putToContainer;this.options.base.putToContainer=false;for(let iArrow in this.CanvasStorage[2]){this.arrow(this.CanvasStorage[2][iArrow][0],this.CanvasStorage[2][iArrow][1],this.CanvasStorage[2][iArrow][2])}this.options.base.putToContainer=putToContainer;return this},redraw:function(){return this.clear().draw()},updateOptions:function(o){if(o.base!==c){k(this.options.base,o.base)}if(o.render!==c){k(this.options.render,o.render)}if(o.arrow!==c){k(this.options.arrow,o.arrow)}return this}};g.$cArrows=d})(window);

/***********************************************************************************
Связи конфигурационных единиц | http://gitlab.lan.arta.kz/commerce/sm/issues/201
http://gitlab.lan.arta.kz/commerce/sm/issues/269
***********************************************************************************/
class ItsmCiRelations {
  constructor() {
		this.limit = 50; // лимит обрабатываемых данных (максимальное кол-во отрисованых блоков)

    this._idsArrow = [];
    this.isAppShow = false;
    this.isAppCreated = false;
		this.selectedID = null;
		this.documentID = null;
		this.data = {};
		this.registryCiItems = null;
		this.categoriesIcon = null;

		this.form = {
			createField: (fieldData) => {
		    let field = {};
		    for (let key in fieldData) field[key] = fieldData[key];
		    return field;
		  },
		  getValue: (data, cmpID) => {
		    data = data.data ? data.data : data;
		    for(let i = 0; i < data.length; i++)
		    if (data[i].id === cmpID) return data[i];
		  },
		  setValue: (asfData, cmpID, data) => {
		    let field = this.form.getValue(asfData, cmpID);
		    for (let key in data) {
		      if(key === 'id' || key === 'type') continue;
		      field[key] = data[key];
		    }
		    return field;
		  },
		  getTableBlockIndex: (data, cmp) => {
		    let res = 0;
		    data = data.data ? data.data : data;
		    data.forEach(item => {
		      if (item.id.slice(0, item.id.indexOf('-b')) === cmp) res++;
		    });
		    return res === 0 ? 1 : ++res;
		  }
		}

    this._create = {
      separator: () => $('<div class="itsm-ci-separator">'),

			registryChooser: () => {
				let api = AS.FORMS.ApiUtils;
				let asfContainer = $('<div id="itsm-ci-registrychooser" class="asf-container">');
				let tagContainer = $('<div class="ns-tagContainer" style="width: calc(100% - 30px);">')
				.append('<div class="itsm-ci-chooser-placeholder">Все конфигурационные единицы</div>');
				let button = $('<button class="asf-browseButton itsm-ci-button" title="Выбор...">');

				AS.SERVICES.showWaitWindow();
				api.simpleAsyncGet('rest/api/registry/info?code=itsm_registry_ci', reg => {
					button.on('click', () => AS.SERVICES.showRegisterLinkDialog(reg, documentId => {
						api.getDocMeaningContentByDocumentId(documentId).then(text => {
							let tagItem = $('<div class="ns-tagItem" style="width: calc(100% - 2px);">');
							let input = $('<input type="text" class="ns-tagItemInput" readonly="readonly" style="width: calc(100% - 14px);">');
							let delButton = $('<div class="ns-tagDeleteImage" style="display: inline-block;">')
							.on('click', () => {
								tagContainer.empty().append('<div class="itsm-ci-chooser-placeholder">Все конфигурационные единицы</div>');
								this.documentID = null;
								this.buildTree();
								$('.itsm-ci-description').empty()
								.append('<div class="value" style="font-weight: bold;">Не выбрана конфигурационная единица</div>')
							});
							input.val(text).attr("title", text);
							tagItem.append(input).append(delButton);
							tagContainer.empty().append(tagItem);
							this.documentID = documentId;
							this.buildTree();
						});
					}));
					AS.SERVICES.hideWaitWindow();
				});
				asfContainer.append(tagContainer).append(button);
				return asfContainer;
			},

			editInfoButton: (infoCI) => {
				return $('<div class="itsm-ci-button-edit">').text('Редактировать запись ...')
				.on('click', () => {
					this.showModalPlayer(infoCI);
				});
			},

			saveButton: () => {
				return $('<div class="itsm-ci-button itsm-ci-button-save" title="Сохранить координаты">')
				.hide()
				.on('click', () => {
					this.saveCoordinates();
				});
			},

			row: (label, value) => {
			  return $(`<tr>
			    <td class="label">${label}</td>
			    <td style="vertical-align: top;">:</td>
			    <td class="value">${value}</td>
			    </tr>`);
			},

			itemBlock: (option) => {
				let create = this._create;
				let me = this;
				let block = $(`<li id="CI${option.infoCI.id}" documentid="${option.infoCI.id}" class="itsm-ci-item">`)
				.attr("title", option.infoCI.name)
				.css({"top": option.top+"px", "left": option.left+"px"})
				.on('click', function() {
					me.selectedID = option.infoCI.id;
					$('.itsm-ci-item').css({"box-shadow": "1px 1px 5px #777"});
			    $(this).css({"box-shadow": "1px 1px 10px #0C59A3"});
			    create.description(option.infoCI);
			  })
				.on('dblclick', () => {
					me.selectedID = option.infoCI.id
					me.showModalPlayer(option.infoCI);
				});

				function subName(name){
					if(!name) return '-';
					return name.length > 15 ? name.substring(0, 15) + '..' : name;
				}

				let icon = $('<div class="icon">');
				let label = $('<div class="label">').text(subName(option.infoCI.name));

				let customIcon = this.form.getValue(option.infoCI.data, 'itsm_form_ci_icon');
				let categoryIcon = this.form.getValue(option.infoCI.data, 'itsm_form_ci_category');

				if(customIcon && customIcon.key) {
					icon.css({'background': `url(/constructor/rest/image?imageID=${customIcon.key}) no-repeat center / cover`});
				} else if (categoryIcon && categoryIcon.key && categoryIcon.key != ' ') {
					if(this.categoriesIcon && this.categoriesIcon[categoryIcon.key] && this.categoriesIcon[categoryIcon.key].indexOf('base64') != -1) {
						icon.css({'background': `url(${this.categoriesIcon[categoryIcon.key]}) no-repeat center / cover`});
					}
				}

				block.append(icon).append(label);
				return block;
			},

			description: (infoCI) => {
			  if(infoCI && infoCI.data) {
					let create = this._create;
			    let basicInfo = $('<table>')
			    .append(create.row("Наименование", this.form.getValue(infoCI.data, "itsm_form_ci_name").value  || '-'))
			    .append(create.row("Идентификатор", this.form.getValue(infoCI.data, "itsm_form_ci_id").value || '-'))
			    .append(create.row("Описание", this.form.getValue(infoCI.data, "itsm_form_ci_description").value || '-'))
			    .append(create.row("Владелец", this.form.getValue(infoCI.data, "itsm_form_ci_owner").value || '-'))
			    .append(create.row("Тип", this.form.getValue(infoCI.data, "itsm_form_ci_type").value || '-'))
			    .append(create.row("Версия", this.form.getValue(infoCI.data, "itsm_form_ci_version").value || '-'));

			    let addInfo = $('<table>')
			    .append(create.row("Серийный номер", this.form.getValue(infoCI.data, "itsm_form_ci_serialnumber").value || '-'))
			    .append(create.row("Номер лицензии", this.form.getValue(infoCI.data, "itsm_form_ci_licensenumber").value || '-'))
			    .append(create.row("Физическое расположение", this.form.getValue(infoCI.data, "itsm_form_ci_physical").value || '-'));

			    $('.itsm-ci-description').empty()
			    .append(basicInfo)
			    .append(create.separator())
			    .append(addInfo)
					.append(create.separator())
					.append(create.editInfoButton(infoCI));
			  }
			}
    }
  }

	createDocument(asfData) {
		let data = {
			registryCode: "coordinates_for_drawing",
			data: asfData
		};
		return AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/registry/create_doc_rcc", null, null, JSON.stringify(data), "application/json; charset=utf8");
	}

	searchCoordinates(cb) {
		let apiParam = {
	    registryCode: 'coordinates_for_drawing',
	    field: 'ci_parent',
	    condition: 'TEXT_EQUALS',
	    value: this.documentID || 'all_items'
	  }
		AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/data_ext?${jQuery.param(apiParam)}`, res => {
			if(res.recordsCount == 0) cb(null);
			else cb(res.result[0].dataUUID);
	  });
	}

	saveCoordinates() {
		AS.SERVICES.showWaitWindow();

		let form = this.form;
		let me = this;
		let blocks = $('.itsm-ci-item').map((i, item) => {
			let block = $(item);
			return {
				documentID: block.attr('documentid'),
				left: block.position().left,
				top: block.position().top
			}
		});

		let asfData = [];
		asfData.push(form.createField({
			id: "ci_parent",
			type: "textbox",
			value: this.documentID || "all_items"
		}));
		let tableMaps = form.createField({
			id: "ci_table",
			type: "appendable_table",
			key: "",
			data: []
		});
		asfData.push(tableMaps);
		blocks.each((i, block) => {
			tableMaps.data.push(form.createField({
	      id: "ci_reglink-b" + form.getTableBlockIndex(tableMaps, "ci_reglink"),
	      type: "reglink",
	      key: block.documentID,
	      valueID: block.documentID
	    }));
			tableMaps.data.push(form.createField({
				id: "ci_left_pos-b" + form.getTableBlockIndex(tableMaps, "ci_left_pos"),
				type: "numericinput",
				value: block.left,
				key: block.left
			}));
			tableMaps.data.push(form.createField({
				id: "ci_top_pos-b" + form.getTableBlockIndex(tableMaps, "ci_top_pos"),
				type: "numericinput",
				value: block.top,
				key: block.top
			}));
		});
		me.searchCoordinates(uuid => {
			if(uuid) {
				AS.FORMS.ApiUtils.loadAsfData(uuid).then(data => {
					form.setValue(data, 'ci_table', tableMaps);
					return AS.FORMS.ApiUtils.saveAsfData(data.data, data.form, data.uuid);
				}).then(() => {
					AS.SERVICES.hideWaitWindow();
					$('.itsm-ci-button-save').hide();
					itsmShowMessage({message: "Координаты сохранены", type: 'success'});
				});
			} else {
				me.createDocument(asfData)
				.then(result => {
					AS.SERVICES.hideWaitWindow();
					$('.itsm-ci-button-save').hide();
					itsmShowMessage({message: "Координаты сохранены", type: 'success'});
				});
			}
		});
	}

	parseCoordinates(data){
		let ciTable = this.form.getValue(data, 'ci_table');
		let maps = {};

		for(let i = 0; i < ciTable.data.length; i+=3) {
			maps[ciTable.data[i].key] = {
				left: ciTable.data[i+1].key,
				top: ciTable.data[i+2].key
			}
		}
		return maps;
	}

	getAsfDataCI(documentID, cb) {
		if(this.data[documentID]) {
			cb(this.data[documentID]);
			return;
		}
		AS.FORMS.ApiUtils.getAsfDataUUID(documentID).then(dataUUID => {
			AS.FORMS.ApiUtils.loadAsfData(dataUUID).then(asfData => {
				this.data[documentID] = asfData;
				cb(asfData);
			});
		});
	}

	getCiItems(cb) {
		if(this.registryCiItems) {
			cb(this.registryCiItems);
			return;
		}
		let apiParam = {
	    registryCode: 'itsm_registry_ci',
	    field: 'itsm_form_ci_status',
	    condition: 'CONTAINS',
	    key: '1',
	    fields: 'itsm_form_ci_name',
			pageNumber: 0,
			countInPart: this.limit
	  }
	  AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/data_ext?${jQuery.param(apiParam)}`, res => {
			this.registryCiItems = res;
			cb(this.registryCiItems);
	  });
	}

	getRelations(asfData){
	  let result = [];
	  let tableRelations = this.form.getValue(asfData, 'itsm_form_ci_relations');
	  if(tableRelations && tableRelations.data) {
	    tableRelations.data.forEach((row, index) => {
	      if (row.id.slice(0, row.id.indexOf('-b')) === 'ci') {
					if(row.key) {
						result.push({
		          id: row.key,
		          name: row.value,
		          type: tableRelations.data[index-1].key, //1 - дочерний для; 2 - связан с
		          relations: []
		        });
					}
	      }
	    });
	  }
	  return result;
	}

	getAllRelations(asfData, parentID) {
	  let relations = this.getRelations(asfData).filter(r => r.id !== parentID);
	  if (relations.length === 0) return [];

		let me = this;
		let localLimit = 0;

		me._idsArrow = [];

		relations.forEach(r => {
			me._idsArrow.push("CI" + parentID + "||CI" + r.id);
		});

		function searchIdRelation(rel, id){
			let result = false;
			rel.forEach(item => {
				if(me._idsArrow.indexOf("CI" + id + "||CI" + item.id) !== -1) result = true;
			});
			return result;
		}

	  function rec(rel, parentID) {
			if(localLimit > me.limit) return; //проверял, пока вызывается меньше установленного лимита
	    rel.forEach((item, i) => {
	      me.getAsfDataCI(item.id, asfData => {
					localLimit++;
	        item.data = asfData;
	        let newRelations = me.getRelations(asfData).filter(r => r.id !== parentID);
	        if(!searchIdRelation(newRelations, item.id)) {
						newRelations.forEach(r => {
							me._idsArrow.push("CI" + item.id + "||CI" + r.id);
						});
	          rel[i].relations = newRelations;
	        }
	        if(rel[i].relations.length > 0) rec(rel[i].relations, rel[i].id);
	      });
	    });
	    return rel;
	  }
	  rec(relations, parentID);

	  return relations;
	}


	getInformationOnSelectedItems(ids) {
	  let result = [];
		let me = this;
	  ids.forEach(id => {
	    me.getAsfDataCI(id, asfData => {
	      result.push({
	        id: id,
	        name: me.form.getValue(asfData, 'itsm_form_ci_name').value,
	        data: asfData,
	        relations: me.getAllRelations(asfData, id)
	      });
	    });
	  });
	  return result;
	}

	buildTree() {
		if(!this.documentID) {
			this.drawAllItems();
		} else {
			let infoCI = this.getInformationOnSelectedItems([this.documentID]);
		  $('.itsm-ci-description').empty();
			$('.itsm-ci-button-save').hide();
		  setTimeout(() => {
		    this._create.description(infoCI[0]);
		    this.drawTree(infoCI);
		  }, 1000);
		}
	}

	drawTree(infoCI) {
		let me = this;
	  let elTree = $('.itsm-ci-tree').empty();
	  let common_parent = $('<ul id="itsm-common-parent">')
	  .css({
	    "padding": 0,
	    "height": $('.itsm-ci-content').height() - 42 + "px",
			"position": "initial",
			"overflow": "auto"
	  });

		let startTopPos = 20;
		let position = {[startTopPos]: [elTree.width()/2-65]};
		let blocks = [];
		let create = this._create;

		function createNewItems(relations, parentID, parentLeft, maps) {
			if(blocks.length >= me.limit) return;

			let length = relations.length;
			if(length === 0) return;
			let leftPos = length > 1 ? parentLeft - length * 140 / 2 + 65 : parentLeft;

			if(maps) {
				relations.forEach((item, i) => {
					if(blocks.indexOf(item.id) == -1) {
						blocks.push(item.id);
						let tmpLeft = maps[item.id] ? maps[item.id].left : leftPos;
						let tmpTop = maps[item.id] ? maps[item.id].top : startTopPos;
						common_parent.append(create.itemBlock({
				      infoCI: item,
				      left: tmpLeft,
				      top: tmpTop
				    }));
						createNewItems(item.relations, item.id, tmpLeft || leftPos, maps);
					}
				});

			} else {

				startTopPos += 70;
				position[startTopPos] = [];
				relations.forEach((item, i) => {
					if(blocks.indexOf(item.id) == -1) {
						position[startTopPos].push(leftPos);
						blocks.push(item.id);
						common_parent.append(create.itemBlock({
				      infoCI: item,
				      left: leftPos,
				      top: startTopPos
				    }));
						createNewItems(item.relations, item.id, leftPos);
						leftPos += 140;
					}
				});

			}

		}

		me.searchCoordinates(uuid => {
			if(uuid) {
				AS.FORMS.ApiUtils.loadAsfData(uuid).then(data => {
					let maps = me.parseCoordinates(data);
					common_parent.append(create.itemBlock({
						infoCI: infoCI[0],
						top: maps[infoCI[0].id].top,
						left: maps[infoCI[0].id].left
					}).css({
						"box-shadow": "1px 1px 10px #0C59A3"
					}));
					blocks.push(infoCI[0].id);
					createNewItems(infoCI[0].relations, infoCI[0].id, maps[infoCI[0].id].top, maps);
					elTree.append(common_parent);
				  me.arrowsDrawerInit();

				});
			} else {
				common_parent.append(create.itemBlock({
					infoCI: infoCI[0],
					top: startTopPos,
					left: position[startTopPos][0]
				}).css({
					"box-shadow": "1px 1px 10px #0C59A3"
				}));
				blocks.push(infoCI[0].id);
				createNewItems(infoCI[0].relations, infoCI[0].id, position[startTopPos][0]);

				elTree.append(common_parent);
			  me.arrowsDrawerInit();
			}
		});

	}

	arrowsDrawerInit(){
		let options = {
	    render: {
	      strokeStyle: '#0C59A3',
	      lineWidth: 2
	    },
			arrow: {
				connectionType: 'rectangleAuto'
			}
	  }
		let arrows = this._idsArrow.map(ids => {
			ids = ids.split('||');
			return [`#${ids[0]}`, `#${ids[1]}`]
		});

		let cArrow = $cArrows('#itsm-common-parent', options);
		cArrow.arrows(arrows);

	  $('.itsm-ci-item').draggable({
	    containment: '#itsm-common-parent',
	    scroll: false,
	    drag: function(event, ui) {
	      cArrow.redraw();
				$('.itsm-ci-button-save').show();
	    }
	  });
	}

	showModalPlayer(infoCI) {
		let me = this;
		let modal = $('<div class="itsm-ci-modal-player">');

		let player = AS.FORMS.createPlayer();
	  player.view.setEditable(true);
		player.showFormData(null, null, infoCI.data.uuid);
		player.view.appendTo(modal);

	  $("body").append(modal);
	  modal.dialog({
	      modal: true,
	      width: 1000,
	      height: 600,
	      resizable: false,
				dialogClass: "itsm-ci-dialog",
	      title: infoCI.name || '-',
	      show: {
	        effect: 'fade',
	        duration: 500
	      },
	      hide: {
	        effect: 'fade',
	        duration: 500
	      },
	      buttons: [
	        {
	          text: i18n.tr("Сохранить"),
	          click: function() {
							if(!player.model.isValid()) {
								itsmShowMessage({message: "Введите все обязательные поля", type: 'warning'});
								return;
							}
							AS.SERVICES.showWaitWindow();
	            AS.FORMS.ApiUtils.saveAsfData(player.model.getAsfData().data, player.model.formId, player.model.asfDataId)
							.then(asfDataId => {
								AS.SERVICES.hideWaitWindow();
								itsmShowMessage({message: "Данные сохранены", type: 'success'});
								me.data[infoCI.id] = player.model.getAsfData();
								me.buildTree();
								$(this).dialog("close");
							});
	          }
	        },
	        {
	          text: i18n.tr("Закрыть"),
	          click: function() {
							// if(confirm("Закрыть без сохранения?"))
							$(this).dialog("close");
	          }
	        }
	      ],
	      close: function() {
					player.destroy();
	        $(this).remove();
	      }
	  });
	}

	drawAllItems(){
		let me = this;
		AS.SERVICES.showWaitWindow();
		me.getCiItems(items => {
			let ids = items.result.filter(x => x.fieldValue.itsm_form_ci_name).map(item => item.documentID);
			let infoCI = me.getInformationOnSelectedItems(ids);

			setTimeout(() => {
				me._idsArrow = [];
				infoCI.forEach(parent => {
					parent.relations.forEach(child => {
						me._idsArrow.push("CI" + parent.id + "||CI" + child.id);
					});
				});

				let elTree = $('.itsm-ci-tree');
			  let common_parent = $('<ul id="itsm-common-parent">')
			  .css({
			    "padding": 0,
			    "height": $('.itsm-ci-content').height() - 42 + "px",
					"position": "initial",
					"overflow": "auto"
			  });

				let leftPos = 60;
				let topPos = 20;
				let c = 1;
				let pl = true;
				function drawBlock(info){
					common_parent.append(me._create.itemBlock({
						infoCI: info,
						left: leftPos,
						top: topPos
					}));
					c-=-1;
					leftPos-=-200;
					if(c>4) {
						c=1;
						!pl ? leftPos=60 : leftPos=160;
						pl=!pl;
						topPos-=-140;
					}
				}

				me.searchCoordinates(uuid => {
					if(uuid) {
						AS.FORMS.ApiUtils.loadAsfData(uuid).then(data => {
							let maps = me.parseCoordinates(data);

							infoCI.forEach(item => {
								if(maps[item.id]) {
									common_parent.append(me._create.itemBlock({
										infoCI: item,
										left: maps[item.id].left,
										top: maps[item.id].top
									}));
								} else {
									drawBlock(item);
								}
							});
							elTree.empty().append(common_parent);
						  me.arrowsDrawerInit();
							AS.SERVICES.hideWaitWindow();
						});
					} else {
						for(let i = 0; i < infoCI.length; i-=-1){
							drawBlock(infoCI[i]);
						}
						elTree.empty().append(common_parent);
					  me.arrowsDrawerInit();
						AS.SERVICES.hideWaitWindow();
					}
				});

		  }, 1500);
		});
	}


  init(panel) {
		this.panel = panel;

    if (this.isAppCreated) {
			console.log("приложение уже создано");
			this.elContainer.show();
		} else {
			console.log("Создаем новое приложение");

			AS.FORMS.ApiUtils.simpleAsyncGet('rest/api/dictionaries/itsm_dict_ci_categories')
			.then(res => {
				this.categoriesIcon = {};
				for(let key in res.items) {
					this.categoriesIcon[res.items[key].itsm_dict_ci_categories_value.value] = res.items[key].itsm_dict_ci_categories_icon.value;
				}
			});

			let create = this._create;

			this.elContainer = $('<div id="itsm-ci" class="itsm-ci-container">');
		  let elHeader = $('<div class="itsm-ci-header">');
		  let elContent = $('<div class="itsm-ci-content">');
		  let elTree = $('<div class="itsm-ci-tree">');
		  let elInfo = $('<div class="itsm-ci-info">')
		  .append('<div class="itsm-ci-info-label" style="display: none;">Информация о конфигурационной еденице</div>');
		  let elDescription = $('<div class="itsm-ci-description">').height(panel.height() - 120).hide();
		  let buttonInfo = $('<div class="itsm-ci-button itsm-ci-button-open-info">');
			let sep1 = create.separator().hide();

			elDescription.append($('<div class="value" style="font-weight: bold;">Не выбрана конфигурационная единица</div>'));
		  elInfo.append(buttonInfo).append(sep1).append(elDescription);
		  elContent.append(elTree).append(elInfo);
		  elHeader.append('<div style="position: absolute; left: 10px; font-weight: bold; width: 140px; font-family: Open Sans; font-size: 14px; color: #404040;">Фильтрация по КЕ:</div>')
			.append(create.registryChooser())
			.append(create.saveButton());

		  this.elContainer.append(elHeader).append(elContent);

		  buttonInfo.data('open', false);
		  buttonInfo.on('click', () => {
		    if(!buttonInfo.data('open')){
		      buttonInfo.css('background-image', 'url(../Synergy/light/images/buttons/dark.gray/forward.png)');
		      buttonInfo.data('open', true);
		      // elTree.css('width', 'calc(100% - 316px)');
		      elInfo.css('width', '304px');
		      $('.itsm-ci-info-label').show();
		      sep1.show();
		      elDescription.show();
		    } else {
		      buttonInfo.css('background-image', 'url(../Synergy/light/images/buttons/dark.gray/back.png)');
		      buttonInfo.data('open', false);
		      // elTree.css('width', 'calc(100% - 52px)');
		      elInfo.css('width', '50px');
		      $('.itsm-ci-info-label').hide();
		      sep1.hide();
		      elDescription.hide();
		    }
		  });

			this.elContainer.on('contextmenu', event => {
		    event = event || window.event;
		    event.preventDefault ? event.preventDefault() : event.returnValue = false;
		  });

			this.drawAllItems();
	    this.isAppCreated = true;
		}

		panel.hide();
		panel.after(this.elContainer);
  }
}

(function(){
	let ciRelations = new ItsmCiRelations();
	let panel = $("#ci-panel");

	if(panel.length > 0) {
		if(!ciRelations.isAppShow) {
			ciRelations.init(panel);
			ciRelations.isAppShow = true;
		}
	} else if (panel.length == 0) {
		if(ciRelations.isAppCreated) {
			ciRelations.isAppShow = false;
			ciRelations.elContainer.hide();
		}
	}

	$(window).resize(() => {
		if(!ciRelations.isAppShow) return;
		if($('.itsm-ci-content').height() === panel.height()) return;
		$('.itsm-ci-content').height(panel.height());
		$('.itsm-ci-info').height(panel.height());
		$('.itsm-ci-description').height(panel.height() - 120);
	});

})();

let pages = cookie.getJson('pages');
if(pages) {
	pages.currentPage = 'ItsmCiRelations';
	cookie.setJson('pages', pages);
}
