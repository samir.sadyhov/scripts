const mobileMenuItems = [
  {
    name: {ru: 'Главная', kk: 'Главная'},
    icon: 'home',
    pageCode: 'reg_card'
  },
  {
    name: {ru: 'Задачи', kk: 'Задачи'},
    icon: 'calendar',
    pageCode: 'workflow_page'
  },
  {
    name: {ru: 'Аналитика', kk: 'Аналитика'},
    icon: 'gitter',
    pageCode: 'analytics'
  },
  {
    name: {ru: 'Карта связей', kk: 'Карта связей'},
    icon: 'link',
    pageCode: 'ItsmCiRelations'
  },
  {
    name: {ru: 'Отчеты', kk: 'Отчеты'},
    icon: 'settings',
    pageCode: 'report_designer'
  },
  {
    name: {ru: 'Настройки', kk: 'Настройки'},
    icon: 'cog',
    pageCode: 'settings_page'
  }
];

const getMenuItem = param => {
  let li = $('<li>').css({'margin-bottom': '10px'});
  let menuItem = $(`<a href="#"><span class="uk-margin-small-right" uk-icon="icon: ${param.icon}"></span>${param.name[AS.OPTIONS.locale]}</a>`);
  menuItem.on('click', e => {
    e.preventDefault();
    e.target.blur();
    UIkit.offcanvas($('.itsm-mobile-menu-left')).hide();
    UIkit.offcanvas($('.itsm-mobile-menu-right')).hide();
    fire({type: 'goto_page', pageCode: param.pageCode}, 'root-panel');
  });
  return li.append(menuItem);
}

function createEvent(e, documentID){
  try {
    let event = {"api_event": `event.docflow.document.${e}`, "documentId": documentID, "userId": AS.OPTIONS.currentUser.userid};
    let data = {"eventMsg": JSON.stringify(event), "eventName": "event.docflow.document"}
    AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/events/create", res => {
      if(res.errorCode != '0') throw new Error(res.errorMessage);
      let openDocs = Cons.getAppStore().openDocs || [];
      if(e == 'opened') openDocs.push(documentID); else openDocs = openDocs.filter(v => v != documentID);
      openDocs = openDocs.filter((v, i, a) => i == a.indexOf(v));
      Cons.setAppStore({openDocs: openDocs});
    }, null, data, "application/x-www-form-urlencoded; charset=UTF-8", err => console.log(err));
  } catch (e) {
    console.log(e);
  }
}

function closeWindow(){return null;}

function logout(){
  UIkit.offcanvas('.itsm-mobile-menu-left').hide();
  window.removeEventListener('beforeunload', closeWindow, false);

  let openDocs = Cons.getAppStore().openDocs || [];
  openDocs.forEach(doc => createEvent('closed', doc));

  AS.apiAuth.setCredentials(null, null);
  AS.OPTIONS.currentUser = {};
  cookie.delete('pages');
  cookie.delete('itsm-arm-user');
  Cons.logout();
}

const renderMenu = () => {
  const currentPage = Cons.getCurrentPage();

  $('body>.itsm-mobile-button').remove();
  $('body>.itsm-mobile-menu').remove();

  let buttonLeft = $(`<a href="" class="itsm-mobile-button mobile-button-left" uk-icon="menu"></a>`);
  let buttonRight = $(`<a href="" class="itsm-mobile-button mobile-button-right" uk-icon="more-vertical"></a>`).hide();
  let menuLeft = $(`<div class="itsm-mobile-menu itsm-mobile-menu-left" uk-offcanvas="overlay: true">`);
  let menuRight = $(`<div class="itsm-mobile-menu itsm-mobile-menu-right" uk-offcanvas="flip: true; overlay: true">`);
  let menuLeftContainer = $('<div>', {class: 'uk-offcanvas-bar'});
  let profileContent = $('<div>');
  let menuLeftContent = $('<div>', {class: 'itsm-menu-content-left'});
  let menuButtons = $('<ul class="itsm-menu-buttons uk-nav-default uk-nav-parent-icon" uk-nav></ul>');
  let exitItem = $('<li>');
  let exitClick = $(`<a href="#" id="itsm-menu-exit"><span class="uk-margin-small-right" uk-icon="icon: sign-out"></span>Выход</a>`);

  mobileMenuItems.forEach(item => {
    if(item.pageCode == 'settings_page') {
      if(AS.OPTIONS.currentUser.groups.find(x => x.groupCode == 'itsm_group_settings')) menuButtons.append(getMenuItem(item));
    } else {
      menuButtons.append(getMenuItem(item));
    }
  });

  exitClick.on('click', e => {
    e.preventDefault();
    e.target.blur();
    logout();
  });

  exitItem.append(exitClick);
  menuButtons.append(exitItem);

  profileContent
  .append(`<img id="mobile-profile-icon" src="${window.location.origin}/Synergy/load?userid=${AS.OPTIONS.currentUser.userid}">`)
  .append(`<span id="mobile-profile-name" class="uk-text-middle">${AS.OPTIONS.currentUser.name}</span>`);

  menuLeftContainer.append(profileContent).append('<hr>').append(menuLeftContent).append('<hr id="divider1">').append(menuButtons);
  menuLeft.append(menuLeftContainer);

  menuRight.append(`<div class="uk-offcanvas-bar">
    <div class="itsm-menu-content-right">
      <div class="itsm-mobile-buttons-container"></div>
      <div class="itsm-mobile-filter-container"></div>
      <div class="itsm-mobile-registry-actions"></div>
    </div>
  </div>`);

  $('body').append(menuLeft).append(menuRight).append(buttonLeft).append(buttonRight);

  buttonLeft.on('click', e => {
    e.preventDefault();
    e.target.blur();
    UIkit.offcanvas($('.itsm-mobile-menu-left')).show();
  });

  buttonRight.on('click', e => {
    e.preventDefault();
    e.target.blur();
    UIkit.offcanvas($('.itsm-mobile-menu-right')).show();
  });

  setTimeout(() => {
    switch (currentPage.code) {
      case "app":
      case "settings_page":
        if($('.itsm-menu-desktop .itsm-nav').length) $('.itsm-menu-desktop .itsm-nav').clone(true).addClass("itsm-mobile-menu-registry").appendTo(menuLeftContent);
        if($(document).width() < 768) buttonRight.show();
        break;
      case "analytics":
        setTimeout(() => {
          if($('.itsm-menu-desktop .itsm-nav').length) $('.itsm-menu-desktop .itsm-nav').clone(true).addClass("itsm-mobile-menu-analytics").appendTo(menuLeftContent);
        }, 300)
        break;
      case "ItsmCiRelations":
      case "report_designer": $('#divider1').remove(); break;
    }
  }, 100);
}

setTimeout(() => {renderMenu()}, 500);
