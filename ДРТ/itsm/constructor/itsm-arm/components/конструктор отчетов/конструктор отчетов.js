$.getScript("https://cdnjs.cloudflare.com/ajax/libs/alasql/0.6.1/alasql.min.js");

let defaultParamDict = ['MORE', 'MORE_OR_EQUALS', 'LESS', 'LESS_OR_EQUALS', 'EQUALS', 'NOT_EQUALS', 'CONTAINS', 'NOT_CONTAINS', 'START', 'NOT_START', 'NOT_END', 'END', 'TEXT_NOT_EQUALS', 'TEXT_EQUALS'];

let RD = {
  registryInfo: null,
  registryCode: null,
  registryID: null,
  registryName: null,
  registryRights: [],
  registryColumns: [],

  filterName: null,
  filterCode: null,
  filterID: null,
  filterRights: [],

  forms: {},

  paramSearch: {},

  paramFields: {
    registryRouteStatus: ['STATE_SUCCESSFUL', 'STATE_NOT_FINISHED', 'NO_ROUTE', 'STATE_UNSUCCESSFUL'],
    textarea: defaultParamDict,
    textbox: defaultParamDict,
    custom: defaultParamDict,
    formula: defaultParamDict,
    counter: defaultParamDict,
    projectlink: defaultParamDict,
    repeater: defaultParamDict,
    docnumber: defaultParamDict,
    personlink: defaultParamDict,
    link: defaultParamDict,
    htd: defaultParamDict,

    numericinput: ['MORE', 'MORE_OR_EQUALS', 'LESS', 'LESS_OR_EQUALS', 'EQUALS', 'NOT_EQUALS'],
    date: ['MORE', 'MORE_OR_EQUALS', 'LESS', 'LESS_OR_EQUALS', 'EQUALS', 'NOT_EQUALS'],
    entity: {
      users: ['CONTAINS', 'NOT_CONTAINS'],
      positions: ['CONTAINS', 'NOT_CONTAINS'],
      departments: ['CONTAINS', 'NOT_CONTAINS']
    },
    reglink: ['CONTAINS', 'NOT_CONTAINS'],
    listbox: ['EQUALS'],
    check: ['CONTAINS'],
    radio: ['EQUALS']
  },

  paramName: {
    STATE_SUCCESSFUL: 'Активная',
    STATE_NOT_FINISHED: 'В процессе',
    NO_ROUTE: 'Подготовка',
    STATE_UNSUCCESSFUL: 'Неуспешная',

    MORE: '>',
    MORE_OR_EQUALS: '>=',
    LESS: '<',
    LESS_OR_EQUALS: '<=',
    EQUALS: '=',
    NOT_EQUALS: '<>',

    CONTAINS: 'Содержит',
    NOT_CONTAINS: 'Не содержит',
    START: 'Начинается с',
    NOT_START: 'Не начинается с',
    NOT_END: 'Не заканчивается на',
    END: 'Заканчивается на',
    TEXT_NOT_EQUALS: 'Не совпадает',
    TEXT_EQUALS: 'Совпадает'
  }
};

class CustomSelect {
  constructor(param) {
    this.items = param.items;
    this.placeholder = param.placeholder || 'Выбор...';
    this.search = param.search || false;
    this.selectItem = null;
    this.status = 'hide';
    this.init();
  }

  createSelectContainer() {
    this.selectContainer = $('<div>', {class: 'custom-select'});
    this.selected = $('<div>', {class: 'select-selected'}).text(this.placeholder);
    this.selectItems = $('<div>', {class: 'select-items select-hide'});
    this.selectContainer.append(this.selected).append(this.selectItems);

    this.selected.on('click', e => this.status === 'hide' ? this.show() : this.hide());
    this.selectContainer.on('close-select', e => this.hide());

    if(this.search) {
      let me = this;
      this.searchInput = $('<input>', {class: 'select-search-input select-hide', placeholder: 'Поиск...'});
      this.selectContainer.append(this.searchInput);
      this.searchInput.keyup(function(){
        $.each(me.selectItems.find("div"), function() {
          if($(this).text().toLowerCase().indexOf(me.searchInput.val().toLowerCase()) === -1) $(this).hide();
          else $(this).show();
        });
      });
    }
  }

  createItem(item) {
    let menuItem = $(`<div data-value="${item.value}" title="${item.title}">${item.title}</div>`);
    menuItem.on('click', e => {
      this.hide();
      this.selected.text(item.title);
      this.selected.attr('title', item.title);
      this.selectItem = item;
      this.selectContainer.trigger({
      	type: 'selected',
        eventParam: this.value
      });
    });
    this.selectItems.append(menuItem);
  }

  createItems() {
    this.items.forEach(x => this.createItem(x));
  }

  get container() {
    return this.selectContainer;
  }
  get value() {
    return this.selectItem;
  }

  set value(value) {
    let findVal = this.items.find(x => x.value == value);
    if (!findVal) {
    	this.selectItem = null;
      this.selected.text(this.placeholder);
    }
    this.selectItem = findVal;
  }

  hide() {
    this.status = 'hide';
    this.selected.removeClass('select-arrow-active');
    this.selectItems.addClass('select-hide');
    if(this.search) this.searchInput.addClass('select-hide');
  }

  show() {
    this.status = 'open';
    this.selected.addClass('select-arrow-active');
    this.selectItems.removeClass('select-hide');
    if(this.search) {
      this.searchInput.removeClass('select-hide').val('');
      $.each(this.selectItems.find("div"), function() {
        $(this).show();
      });
    }
  }

  init() {
    this.createSelectContainer();
    this.createItems();
  }
}

let selectedColumns = {};
let leftMenu = $('.rd-app-left');
let content = $('.rd-app-content');
let buttonRigth = $('#rd-button-menu-rigth');
let buttonLeft = $('#rd-button-menu-left');
let applyFilterButton = $('#rd-filter-button');
let applyCustomParamButton = $('#rd-button-apply-params');
let downloadXLSButton = $('#rd-download-button');
let tableContainer = $('.rd-table-container');
let tableParamBody = $('#rd-table-params').find('tbody');
let templateReportButton = $('#rd-button-select-report-template');
let templateSaveButton = $('#rd-button-save-report-template');
let templateDeleteButton = $('#rd-button-delete-report-template');
let templateRefreshButton = $('#rd-button-refresh-report-template');
let templateNameInput = $('#rd-template-name');

tableContainer.empty();
$('body>#rd-filter').remove();

buttonRigth.on('click', e => {
  buttonLeft.show();
  buttonRigth.hide();
  let width = $(document).width();

  if(width > 1024) {
    width = width / 2;
    if(width > 700) width = 700;
  } else {
    content.hide();
  }

  leftMenu.width(width);
  content.width($(document).width() - width - 10);
  $('.rd-report-settings').fadeIn(600);
  $('#rd-param-label').fadeIn(600);
});

buttonLeft.on('click', e => {
  buttonRigth.show();
  buttonLeft.hide();
  leftMenu.width(60);
  content.width($(document).width() - 70);
  $('.rd-report-settings').hide();
  $('#rd-param-label').hide();
  setTimeout(() => {
    content.show();
  }, 400);
});

const getUrlSearch = countInPart => {
  let url = `api/registry/data_ext?registryCode=${RD.registryCode}`;
  if(countInPart) url += `&countInPart=${countInPart}`;
  if(RD.registryColumns.length > 0) RD.registryColumns.forEach(item => url+=`&fields=${item.columnID}`);
  if(RD.filterCode) {
    url+=`&filterCode=${RD.filterCode}`;
  } else {
    let count = 0;
    url += '&groupTerm=and';

    for(let key in RD.paramSearch) {
      if(RD.paramSearch[key].field == 'registryRecordStatus') {
        url += `&registryRecordStatus=${RD.paramSearch[key].value}`;
      } else {
        let gID = count > 0 ? count : '';
        switch (RD.paramSearch[key].type) {
          case 'listbox':
            RD.paramSearch[key].condition.indexOf('NOT') == -1 ? url += `&term${gID}=or` : url += `&term${gID}=and`;
            if(RD.paramSearch[key].hasOwnProperty('keys') && RD.paramSearch[key].keys) {
              RD.paramSearch[key].keys.forEach(fieldKey =>
                url += `&field${gID}=${RD.paramSearch[key].field}&condition${gID}=${RD.paramSearch[key].condition}&key${gID}=${fieldKey}`
              );
            } else {
              $(`[uuid-row="${key}"]`).addClass('rd-param-empty');
              return null;
            }
            break;
          case 'radio':
          case 'check':
            RD.paramSearch[key].condition.indexOf('NOT') == -1 ? url += `&term${gID}=or` : url += `&term${gID}=and`;
            if(RD.paramSearch[key].hasOwnProperty('values') && RD.paramSearch[key].values) {
              RD.paramSearch[key].values.forEach(fieldValue =>
                url += `&field${gID}=${RD.paramSearch[key].field}&condition${gID}=${RD.paramSearch[key].condition}&value${gID}=${fieldValue}`
              );
            } else {
              $(`[uuid-row="${key}"]`).addClass('rd-param-empty');
              return null;
            }
            break;
          case 'reglink':
            RD.paramSearch[key].condition.indexOf('NOT') == -1 ? url += `&term${gID}=or` : url += `&term${gID}=and`;
            if(RD.paramSearch[key].hasOwnProperty('selectedIds') && RD.paramSearch[key].selectedIds) {
              RD.paramSearch[key].selectedIds.forEach(docID =>
                url += `&field${gID}=${RD.paramSearch[key].field}&condition${gID}=${RD.paramSearch[key].condition}&key${gID}=${docID}`
              );
            } else {
              $(`[uuid-row="${key}"]`).addClass('rd-param-empty');
              return null;
            }
            break;
          case 'users':
            RD.paramSearch[key].condition.indexOf('NOT') == -1 ? url += `&term${gID}=or` : url += `&term${gID}=and`;
            if(RD.paramSearch[key].hasOwnProperty('users') && RD.paramSearch[key].users) {
              RD.paramSearch[key].users.map(x => x.personID).forEach(userid =>
                url += `&field${gID}=${RD.paramSearch[key].field}&condition${gID}=${RD.paramSearch[key].condition}&key${gID}=${userid}`
              );
            } else {
              $(`[uuid-row="${key}"]`).addClass('rd-param-empty');
              return null;
            }
            break;
          case 'positions':
            RD.paramSearch[key].condition.indexOf('NOT') == -1 ? url += `&term${gID}=or` : url += `&term${gID}=and`;
            if(RD.paramSearch[key].hasOwnProperty('positions') && RD.paramSearch[key].positions) {
              RD.paramSearch[key].positions.map(x => x.elementID).forEach(positionID =>
                url += `&field${gID}=${RD.paramSearch[key].field}&condition${gID}=${RD.paramSearch[key].condition}&key${gID}=${positionID}`
              );
            } else {
              $(`[uuid-row="${key}"]`).addClass('rd-param-empty');
              return null;
            }
            break;
          case 'departments':
            RD.paramSearch[key].condition.indexOf('NOT') == -1 ? url += `&term${gID}=or` : url += `&term${gID}=and`;
            if(RD.paramSearch[key].hasOwnProperty('departments') && RD.paramSearch[key].departments) {
              RD.paramSearch[key].departments.map(x => x.departmentId).forEach(departmentId =>
                url += `&field${gID}=${RD.paramSearch[key].field}&condition${gID}=${RD.paramSearch[key].condition}&key${gID}=${departmentId}`
              );
            } else {
              $(`[uuid-row="${key}"]`).addClass('rd-param-empty');
              return null;
            }
            break;
          default:
            url += `&field${gID}=${RD.paramSearch[key].field}&condition${gID}=${RD.paramSearch[key].condition}`;
            if(RD.paramSearch[key].hasOwnProperty('key')) {
              if(RD.paramSearch[key].type == "date" && !AS.FORMS.DateUtils.parseDate(RD.paramSearch[key].key)) {
                $(`[uuid-row="${key}"]`).addClass('rd-param-empty');
                return null;
              }
              if(!RD.paramSearch[key].key || RD.paramSearch[key].key == "") {
                $(`[uuid-row="${key}"]`).addClass('rd-param-empty');
                return null;
              } else {
                url += `&key${gID}=${RD.paramSearch[key].key}`;
              }
            } else {
              if(!RD.paramSearch[key].value || RD.paramSearch[key].value == "") {
                $(`[uuid-row="${key}"]`).addClass('rd-param-empty');
                return null;
              } else {
                url += `&value${gID}=${RD.paramSearch[key].value}`;
              }
            }
        }
        count++;
      }
    }

  }
  return url;
};

//Создание блока в таблице, по результату апи
const createRow = fieldValue => {
  let tr = $('<tr>');
  RD.registryColumns.forEach(x => {
    let td = $('<td>');
    if (fieldValue.hasOwnProperty(x.columnID)) {
      td.text(fieldValue[x.columnID]);
      td.attr('title', fieldValue[x.columnID]);
    }
    tr.append(td);
  });
  return tr;
};

//Создание таблицы результатов, по результату апи
const createTable = data => {
  let table = $('<table class="uk-table uk-table-divider uk-table-responsive uk-table-hover">');
  let thead = $('<thead>');
  let body = $('<tbody>');
  let trHead = $('<tr>');

  RD.registryColumns.forEach(x =>
    trHead.append($('<th class="uk-text-top">').text(x.label))
  );
  data.result.forEach(row =>
    body.append(createRow(row.fieldValue))
  );

  thead.append(trHead);
  table.append(thead).append(body);
  tableContainer.empty().append(table);
  return table;
};


//Метод фильтрации списка реестров по правам
const filteredRegistry = list => {
  let result = [];
  const s = data => data.forEach(x => {
    if (x.consistOf && x.consistOf.length > 0) s(x.consistOf);
    else if (x.rights.indexOf('rr_read') !== -1) result.push(x);
  });
  s(list);
  return result.sort((a, b) => {
    a = a.registryName.toLowerCase();
    b = b.registryName.toLowerCase();
    return a > b ? 1 : a < b ? -1 : 0;
  });
};

//Событие при выборе фильтра
const selectFilter = (filter, menuItem) => {
  if(RD.filterCode == filter.code) return;
  applyFilterButton.show();
  RD.filterName = filter.name;
  RD.filterCode = filter.code;
  RD.filterID = filter.id;
  RD.filterRights = filter.rights;
  $('.rd-filter-item').removeClass("rd-filter-item-active");
  menuItem.addClass("rd-filter-item-active");
};

//Метод инициализации фильтров реестров
const initRegistryFilter = () => {
  RD.filterName = null;
  RD.filterCode = null;
  RD.filterID = null;
  RD.filterRights = [];

  rest.synergyGet(`api/registry/filters?registryCode=${RD.registryCode}&type=service&getIcon=false`, filters => {
    filters = filters.filter(x => x.code);
    let filterNav = $('.rd-filter-nav');
    filterNav.empty();

    if(filters.length === 0) {
      $('#rd-filter-menu-button').hide();
      $('.rd-button-container').hide();
      downloadXLSButton.hide();
      r();
      return;
    }

    $('#rd-filter-menu-button').fadeIn(600);
    $('.rd-button-container').css('display', 'flex');

    let filterMenu = $('<ul class="uk-nav-default" uk-nav>');
    filterNav.append(filterMenu);

    filters.forEach(filter => {
      let menuItem = $('<li class="rd-filter-item">');
      let counter = $('<div>');
      let block = $('<a href="javascript:void(0);">');

      block.on('click', e => selectFilter(filter, menuItem));

      block.append($(`
        <div class="uk-grid-small" uk-grid>
          <div class="uk-width-expand" uk-leader>${filter.name}</div>
        </div>`).append(counter)
      );

      menuItem.append(block);
      filterMenu.append(menuItem);

      rest.synergyGet(`api/registry/data_ext?registryCode=${RD.registryCode}&filterCode=${filter.code}&pageNumber=0&countInPart=1&loadData=false`, res => counter.text(res.count));
    });
    r();
  });
};


const createSelectComponent = (items, multiple) => {
  let select = $('<select class="uk-select" style="min-width: 100px;">');
  if(multiple) select.attr('multiple', 'multiple');
  items.sort((a,b) => a.value - b.value)
  .forEach(item => select.append(`<option value="${item.value}" title="${item.label}">${item.label}</option>`));
  return select;
};
const createInputText = () => $('<input type="text" class="uk-input" placeholder="Введите значение">');
const createInputNumber = () => $('<input type="number" class="uk-input" placeholder="Введите значение">');
const createInputDate = () => $('<input type="date" class="uk-input" style="max-width: 160px;"><input type="time" class="uk-input" style="padding-left: 10px;max-width: 100px;" value="00:00">');

const createBlockParam = param => {
  let items = RD.paramFields[param.type].map(x => ({label: RD.paramName[x], value: x}));
  let select = createSelectComponent(items);
  let input;
  switch (param.type) {
    case 'date': input = createInputDate(); break;
    case 'numericinput': input = createInputNumber(); break;
    default: input = createInputText(); break;
  }

  select.addClass('uk-width-auto').css({'padding-left': '10px', 'margin-right': '5px'});
  input.addClass('uk-width-expand').css('padding-left', '10px');

  return $('<div uk-grid style="margin: 0;"></div>').append(select).append(input);
};

const createRegistryParamBlock = (param, uuidRow, guid) => {
  let button = $('<a class="uk-form-icon uk-form-icon-flip" href="javascript:void(0);" uk-icon="icon: list"></a>');
  let input = $('<input class="uk-input" type="text" disabled>');
  let select = createSelectComponent(RD.paramFields.reglink.map(x => ({label: RD.paramName[x], value: x})));

  if(guid) {
    AS.FORMS.ApiUtils.simpleAsyncGet('rest/api/formPlayer/getDocMeaningContents?documentId=' + RD.paramSearch[uuidRow].selectedIds.join('&documentId='), content => {
      content = content.map(x => x.meaning).join('; ');
      input.val(content).attr('title', content);
    });
  } else {
    RD.paramSearch[uuidRow] = {field: param.id, type: param.type, condition: select.val()};
  }

  select.on('change', () => RD.paramSearch[uuidRow].condition = select.val());

  button.on('click', e => {
    Cons.showLoader();
    let selectedIds = RD.paramSearch[uuidRow].hasOwnProperty('selectedIds') ? RD.paramSearch[uuidRow].selectedIds : null;
    AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/info?registryID=${param.registryID}`, registryInfo => {
      Cons.hideLoader();
      //(registry, multi, selectedIds, handler)
      AS.SERVICES.showRegistryLinkDialog(registryInfo, true, selectedIds, docIDs => {
        RD.paramSearch[uuidRow].selectedIds = docIDs;
        let contentURL = 'rest/api/formPlayer/getDocMeaningContents?documentId=' + docIDs.join('&documentId=');
        AS.FORMS.ApiUtils.simpleAsyncGet(contentURL, content => {
          content = content.map(x => x.meaning).join('; ');
          input.val(content).attr('title', content);
          $(`[uuid-row="${uuidRow}"]`).removeClass('rd-param-empty');
        });
      });
    });
  });

  return $('<div class="uk-grid-small" uk-grid>')
  .append($('<div class="uk-width-1-4@s">').append(select))
  .append($('<div class="uk-inline uk-width-3-4@s">').append(button).append(input));
}

const createUserParamBlock = (param, uuidRow, guid) => {
  let button = $('<a class="uk-form-icon uk-form-icon-flip" href="javascript:void(0);" uk-icon="icon: users"></a>');
  let input = $('<input class="uk-input" type="text" disabled>');
  let select = createSelectComponent(RD.paramFields.entity.users.map(x => ({label: RD.paramName[x], value: x})));

  if(guid) {
    let userNames = RD.paramSearch[uuidRow].users.map(x => x.personName).join('; ');
    input.val(userNames).attr('title', userNames);
  } else {
    RD.paramSearch[uuidRow] = {field: param.id, type: param.entity, condition: select.val()};
  }

  select.on('change', () => RD.paramSearch[uuidRow].condition = select.val());

  button.on('click', e => {
    let values = RD.paramSearch[uuidRow].hasOwnProperty('users') ? RD.paramSearch[uuidRow].users : null;
    //(values, multiSelectable, isGroupSelectable, showWithoutPosition, filterPositionID, filterDepartmentID, locale, handler)
    AS.SERVICES.showUserChooserDialog(values, true, false, false, null, null, AS.OPTIONS.locale, users => {
      let userNames = users.map(x => x.personName).join('; ');
      input.val(userNames).attr('title', userNames);
      RD.paramSearch[uuidRow].users = users;
      $(`[uuid-row="${uuidRow}"]`).removeClass('rd-param-empty');
    });
  });

  return $('<div class="uk-grid-small" uk-grid>')
  .append($('<div class="uk-width-1-4@s">').append(select))
  .append($('<div class="uk-inline uk-width-3-4@s">').append(button).append(input));
}

const createDepParamBlock = (param, uuidRow, guid) => {
  let button = $('<a class="uk-form-icon uk-form-icon-flip" href="javascript:void(0);" uk-icon="icon: more"></a>');
  let input = $('<input class="uk-input" type="text" disabled>');
  let select = createSelectComponent(RD.paramFields.entity.departments.map(x => ({label: RD.paramName[x], value: x})));

  if(guid) {
    let depNames = RD.paramSearch[uuidRow].departments.map(x => x.departmentName).join('; ');
    input.val(depNames).attr('title', depNames);
  } else {
    RD.paramSearch[uuidRow] = {field: param.id, type: param.entity, condition: select.val()};
  }

  select.on('change', () => RD.paramSearch[uuidRow].condition = select.val());

  button.on('click', e => {
    let values = RD.paramSearch[uuidRow].hasOwnProperty('departments') ? RD.paramSearch[uuidRow].departments : null;
    //(values, multiSelectable, filterUserID, filterPositionID, filterDepartmentID, filterChildDepartmentID, locale, handler)
    AS.SERVICES.showDepartmentChooserDialog(values, true, null, null, null, null, AS.OPTIONS.locale, departments => {
      let depNames = departments.map(x => x.departmentName).join('; ');
      input.val(depNames).attr('title', depNames);
      RD.paramSearch[uuidRow].departments = departments;
      $(`[uuid-row="${uuidRow}"]`).removeClass('rd-param-empty');
    });
  });

  return $('<div class="uk-grid-small" uk-grid>')
  .append($('<div class="uk-width-1-4@s">').append(select))
  .append($('<div class="uk-inline uk-width-3-4@s">').append(button).append(input));
}

const createPositionParamBlock = (param, uuidRow, guid) => {
  let button = $('<a class="uk-form-icon uk-form-icon-flip" href="javascript:void(0);" uk-icon="icon: more"></a>');
  let input = $('<input class="uk-input" type="text" disabled>');
  let select = createSelectComponent(RD.paramFields.entity.positions.map(x => ({label: RD.paramName[x], value: x})));

  if(guid) {
    let posNames = RD.paramSearch[uuidRow].positions.map(x => x.elementName).join('; ');
    input.val(posNames).attr('title', posNames);
  } else {
    RD.paramSearch[uuidRow] = {field: param.id, type: param.entity, condition: select.val()};
  }

  select.on('change', () => RD.paramSearch[uuidRow].condition = select.val());

  button.on('click', e => {
    let values = RD.paramSearch[uuidRow].hasOwnProperty('positions') ? RD.paramSearch[uuidRow].positions : null;
    //(values, multiSelect, filterUserId, filterDepartmentId, showVacant, locale, handler)
    AS.SERVICES.showPositionChooserDialog(values, true, null, null, null, AS.OPTIONS.locale, positions => {
      let posNames = positions.map(x => x.elementName).join('; ');
      input.val(posNames).attr('title', posNames);
      RD.paramSearch[uuidRow].positions = positions;
      $(`[uuid-row="${uuidRow}"]`).removeClass('rd-param-empty');
    });
  });

  return $('<div class="uk-grid-small" uk-grid>')
  .append($('<div class="uk-width-1-4@s">').append(select))
  .append($('<div class="uk-inline uk-width-3-4@s">').append(button).append(input));
}

const getModalFullDialog = (title, body, buttonName, buttonAction) => {
  let dialog = $('<div class="uk-modal-full" uk-modal style="background: #fff;">');
  let md = $('<div class="uk-modal-dialog">');
  let modalBody = $('<div class="uk-modal-body" style="padding: 10px;" uk-overflow-auto>');
  let footer = $('<div class="uk-modal-footer uk-text-right">');

  modalBody.append(body);
  dialog.append(md);
  md.append('<button class="uk-modal-close-default" type="button" style="color: #fff;" uk-close></button>')
  .append(`<div class="uk-modal-header"><h3>${title}</h3></div>`)
  .append(modalBody).append(footer);

  modalBody.css('max-height', dialog.height() - 120);

  if (buttonName) {
    let actionButton = $('<button class="uk-button uk-button-primary uk-margin-left" type="button">');
    actionButton.html(buttonName).on('click', e => buttonAction());
    footer.append(actionButton);
  }

  return dialog;
};

const generateGuid = () => 'R'+Math.random().toString(36).substring(2, 15)+Math.random().toString(36).substring(2, 15);

//Метод добавления параметра для поиска
const addParamRow = (param, guid) => {
  if(!param) return;

  let uuidRow = guid || generateGuid();
  let tr = $('<tr>').attr('uuid-row', uuidRow);
  let tdFilter = $('<td>');
  let deleteRow = $('<a href="javascript:void(0);" uk-icon="trash"></a>');

  deleteRow.on('click', e => {
    tr.remove();
    delete RD.paramSearch[uuidRow];
    if (Object.keys(RD.paramSearch).length == 0) {
      templateSaveButton.attr('disabled', true);
      templateRefreshButton.attr('disabled', true);
      templateNameInput.attr('disabled', true).attr('placeholder', null);
    }
  });

  switch(param.type) {
    case 'registryRouteStatus':
      let statusRow = createSelectComponent(RD.paramFields[param.type].map(x => ({label: RD.paramName[x], value: x})));
      if(guid) {
        statusRow.val(RD.paramSearch[uuidRow].value);
      } else {
        RD.paramSearch[uuidRow] = {field: 'registryRecordStatus', value: statusRow.val()};
      }
      statusRow.on('change', () => RD.paramSearch[uuidRow].value = statusRow.val());
      tdFilter.append(statusRow);
      break;
    case 'listbox':
      let dictValues = createSelectComponent(param.elements, true);
      if(guid) {
        dictValues.val(RD.paramSearch[uuidRow].keys);
      } else {
        RD.paramSearch[uuidRow] = {
          field: param.id,
          type: param.type,
          condition: RD.paramFields[param.type][0],
          keys: dictValues.val()
        };
      }
      dictValues.on('change', () => {
        RD.paramSearch[uuidRow].keys = dictValues.val();
        tr.removeClass('rd-param-empty');
      });
      tdFilter.append(dictValues);
      break;
    case 'radio':
    case 'check':
      let dictValuesR = createSelectComponent(param.elements, true);
      if(guid) {
        dictValuesR.val(RD.paramSearch[uuidRow].values);
      } else {
        RD.paramSearch[uuidRow] = {
          field: param.id,
          type: param.type,
          condition: RD.paramFields[param.type][0],
          values: dictValuesR.val()
        };
      }
      dictValuesR.on('change', () => {
        RD.paramSearch[uuidRow].values = dictValuesR.val();
        tr.removeClass('rd-param-empty');
      });
      tdFilter.append(dictValuesR);
      break;
    case 'date':
      let blockParamND = createBlockParam(param);
      let selectND = blockParamND.find('select');
      let inputDate = blockParamND.find('input[type="date"]');
      let inputTime = blockParamND.find('input[type="time"]');

      if(guid) {
        selectND.val(RD.paramSearch[uuidRow].condition);
        inputDate.val(RD.paramSearch[uuidRow].key.split(' ')[0]);
        inputTime.val(RD.paramSearch[uuidRow].key.split(' ')[1].substr(0,5));
      } else {
        RD.paramSearch[uuidRow] = {
          field: param.id,
          type: param.type,
          condition: selectND.val(),
          key: `${inputDate.val()} ${inputTime.val()}:00`
        }
      }

      selectND.on('change', () => RD.paramSearch[uuidRow].condition = selectND.val());
      inputDate.on('change', () => {
        RD.paramSearch[uuidRow].key = `${inputDate.val()} ${inputTime.val()}:00`;
        tr.removeClass('rd-param-empty');
      });
      inputTime.on('change', () => {
        RD.paramSearch[uuidRow].key = `${inputDate.val()} ${inputTime.val()}:00`;
        tr.removeClass('rd-param-empty');
      });
      tdFilter.append(blockParamND);
      break;
    case 'reglink':
      tdFilter.append(createRegistryParamBlock(param, uuidRow, guid));
      break;
    case 'entity':
      switch (param.entity) {
        case 'users': tdFilter.append(createUserParamBlock(param, uuidRow, guid)); break;
        case 'departments': tdFilter.append(createDepParamBlock(param, uuidRow, guid)); break;
        case 'positions': tdFilter.append(createPositionParamBlock(param, uuidRow, guid)); break;
        default: tdFilter.append('<div>Параметр в разработке</div>');
      }
      break;
    default:
      let blockParamText = createBlockParam(param);
      let inputText = blockParamText.find('input');
      let selectText = blockParamText.find('select');

      if(guid) {
        inputText.val(RD.paramSearch[uuidRow].value);
        selectText.val(RD.paramSearch[uuidRow].condition);
      } else {
        RD.paramSearch[uuidRow] = {
          field: param.id,
          type: param.type,
          condition: selectText.val(),
          value: inputText.val()
        };
      }

      inputText.on('change', () => {
        RD.paramSearch[uuidRow].value = inputText.val();
        tr.removeClass('rd-param-empty');
      });
      selectText.on('change', () => RD.paramSearch[uuidRow].condition = selectText.val());
      tdFilter.append(blockParamText);
  }

  tr.append(`<td class="uk-form-label uk-text-primary uk-width-small uk-text-truncate" title="${param.name}" style="max-width: 120px;">${param.name}</td>`)
  .append(tdFilter).append($('<td title="Удалить условие" style="width: 30px; text-align: end;">').append(deleteRow));

  tableParamBody.append(tr);
};


//Действие при выборе параметра/условия
const selectParameter = (value, key) => {
  let param;
  if(value == 'registryRecordStatus') {
    param = {id: 'registryRecordStatus', type: 'registryRouteStatus', name: 'Статус записи реестра'};
  } else {
    param = RD.forms[RD.registryInfo.formId].rows.filter(x => x.id == value)[0];
  }
  addParamRow(param, key);
};


//Инициализация справочника параметров/условий
const initParamList = () => {
  let paramListContainer = $('#rd-form-param-list');
  let items = RD.forms[RD.registryInfo.formId].rows.map(x => {
    return {title: x.name, value: x.id};
  });

  items.push({title: 'Статус записи реестра', value: 'registryRecordStatus'});

  tableParamBody.empty();
  RD.paramSearch = {};

  let newSelect = new CustomSelect({
    items: items,
    placeholder: 'Добавить условие',
    search: true
  });

  paramListContainer.empty().append(newSelect.container);

  newSelect.container.on('selected', e => {
    $('#rd-filter-menu-button').hide();
    $('#rd-filter-name').hide().text("");
    downloadXLSButton.hide();
    tableContainer.empty();
    RD.filterName = null;
    RD.filterCode = null;
    RD.filterID = null;
    RD.filterRights = [];

    selectParameter(e.eventParam.value);

    templateSaveButton.attr('disabled', false);
    templateRefreshButton.attr('disabled', false);

    if(templateNameInput.attr('datauuid')) {
      templateNameInput.attr('disabled', true).attr('placeholder', null);
    } else {
      templateNameInput.attr('disabled', false).attr('placeholder', 'введите наименование шаблона...');
    }

    newSelect.value = null;
  });
};

//получение элементов справочника для компонента listbox
const getDictionaryItems = (item, el) => {
  if(item.hasOwnProperty('elements')) {
    el.elements = item.elements;
  } else if (item.hasOwnProperty('dataSource')) {
    rest.synergyGet(`api/dictionaries/${item.dataSource.dict}`, dict => {
      el.elements = [];
      for (let key in dict.items) {
        el.elements.push({
          label: dict.items[key][item.dataSource.key].value,
          value: dict.items[key][item.dataSource.value].value
        });
      }
    });
  }
};

//Проверка типа доступных компонентов
const chekType = type => ['label', 'signlist', 'resolutionlist', 'processlist', 'doclink', 'filelink', 'image', 'file'].indexOf(type) < 0;

//получить имя компонента из настроек реестра
const getCmpName = cmpID => {
  let label = RD.registryInfo.columns.filter(x => x.columnID == cmpID);
  if(label.length == 0) return cmpID;
  return label[0].label == "" ? cmpID : label[0].label;
}

//Метод получает компоненты формы
const getFormComponents = formInfo => {
  let result = [];
  function s(prop){
    if(!prop) return;
    prop.forEach(x => {
      if(chekType(x.type)) {
        if(x.type == 'table') {
          if(!x.config.hasOwnProperty('appendRows')) s(x.properties);
        } else {
          let tmpItem = {id: x.id, type: x.type};
          tmpItem.name = getCmpName(x.id);
          if(['listbox', 'check', 'radio'].indexOf(x.type) != -1) getDictionaryItems(x, tmpItem);
          if(x.type == 'reglink') tmpItem.registryID = x.config.dateFormat;
          if(x.type == 'entity') tmpItem.entity = x.config.entity;
          result.push(tmpItem);
        }
      }
    });
  }
  if(formInfo.hasOwnProperty('properties')) s(formInfo.properties);
  return result;
};

//Получение данных по форме реестра
const getFormData = () => {
  if(RD.forms.hasOwnProperty(RD.registryInfo.formId)) {initParamList(); return};
  rest.synergyGet(`api/asforms/form/${RD.registryInfo.formId}`, info => {
    RD.forms[RD.registryInfo.formId] = info;
    RD.forms[RD.registryInfo.formId].rows = getFormComponents(info);
    initParamList();
  });
};

//Метод выбора реестра из списка
const selectRegistry = (registryList, val) => {
  let reg = registryList.filter(x => x.registryCode == val)[0];
  RD.registryCode = reg.registryCode;
  RD.registryName = reg.registryName;
  RD.registryRights = reg.rights;

  tableContainer.empty();
  downloadXLSButton.hide();
  $('#rd-filter-name').hide().text("");

  rest.synergyGet(`api/registry/info?code=${RD.registryCode}`, info => {
    RD.registryID = info.registryID;
    RD.registryInfo = info;

    RD.registryColumns = info.columns
    .filter(item => item.visible != '0')
    .sort((a, b) => a.order == 0 ? 0 : a.order - b.order)
    .map(item => ({label: item.label, columnID: item.columnID, order: item.order}));

    selectedColumns = {};
    RD.registryColumns.forEach(col => selectedColumns[col.columnID] = col);

    templateReportButton.attr('disabled', false);
    templateSaveButton.attr('disabled', true);
    templateDeleteButton.attr('disabled', true);
    templateRefreshButton.attr('disabled', true);

    templateNameInput.val('')
    .attr('disabled', true)
    .attr('placeholder', null)
    .attr('datauuid', null);

    $('#rd-params-block').fadeIn(600);
    $('.rd-template-block').fadeIn(600);

    getFormData();
    initRegistryFilter();
  });
};

//Применить фильтр
applyFilterButton.on('click', e => {
  applyFilterButton.hide();
  Cons.showLoader();

  rest.synergyGet(getUrlSearch(15), data => {
    Cons.hideLoader();
    UIkit.offcanvas('#rd-filter').hide();
    createTable(data);
    downloadXLSButton.fadeIn(600);
    $('#rd-filter-name').fadeIn(600).text(RD.filterName);
  });

});

const parseVal = val => {
  let l = ['запись', 'записи', 'записей'];
  let k = val % 100;
  if(k > 20) k = val % 10;
  let i = (!k || val > 5 && val < 21 || k > 4) ? 2 : ((k == 1) ? 0 : 1);
  return `Найдено ${val} ${l[i]}`;
}

//Применить пользовательские условия
applyCustomParamButton.on('click', e => {
  $('.rd-button-container').css('display', 'flex');
  $('#rd-filter-menu-button').hide();

  Cons.showLoader();

  let url = getUrlSearch(10);
  if(url) {
    rest.synergyGet(url, data => {
      Cons.hideLoader();
      createTable(data);
      downloadXLSButton.fadeIn(600);
      $('#rd-filter-name').fadeIn(600).text(parseVal(data.recordsCount));
    });
  } else {
    Cons.hideLoader();
    itsmShowMessage({message: "Необходимо заполнить обязательное поле", type: 'error'});
    return;
  }
});

//Кнопка скачивания ексельки
downloadXLSButton.on('click', e => {
  //Если выбран фильтр скачиваем стандартым апи synergy
  if(RD.filterID) {
    let url = `${window.location.origin}/Synergy/rest/reg/load/xls?r=${RD.registryID}`;
    url += `&l=ru&f=${RD.filterID || ''}&s=&u=${AS.OPTIONS.currentUser.userid}&fn=${RD.filterName || RD.registryName}`;
    window.open(url);
  } else { //Выгрузка по кастомным параметрам
    Cons.showLoader();
    rest.synergyGet(getUrlSearch(), data => {
      let excelData = [];

      data.result.forEach(res => {
        let tmpValues = {};
        RD.registryColumns.forEach(col => tmpValues[col.label] = res.fieldValue[col.columnID] || "");
        excelData.push(tmpValues);
      });

      try {
        let opts = {headers: true, column: {style:{Font:{Bold:"1"}}}};
        let result = alasql(`SELECT * INTO XLS("${RD.registryName}.xls",?) FROM ?`, [opts, excelData]); //XLSXML ломается, скорее всего где-то косяк в данных
        Cons.hideLoader();
      } catch (e) {
        console.log(e.message);
        Cons.hideLoader();
        itsmShowMessage({message: "Произошла ошибка при выгрузке отчета", type: 'error'});
      }

    });
  }
});


//Выбор списка столбцов для отчета
$('#rd-button-select-col').on('click', e => {
  let table = $('<table class="uk-table uk-table-small uk-table-divider uk-table-striped rd-table-columns">');
  let thead = $('<thead>');
  let body = $('<tbody>');
  let trHead = $('<tr>');

  ['№ п/п', 'Отображать', 'Название поля', 'Идентификатор']
  .forEach((x,i) => trHead.append($(`<th class="uk-text-top ${i < 2 ? 'uk-width-small' : ''}">`).text(x)));

  RD.forms[RD.registryInfo.formId].rows.forEach(row => {
    let tr = $('<tr>');
    let check = $('<input class="uk-checkbox" type="checkbox">');
    let inputName = $('<input class="uk-input uk-form-small" type="text">');
    let inputOrder = $('<input class="uk-input uk-form-small" type="number" style="max-width: 100px;">')
    tr.append($('<td>').append(inputOrder));
    tr.append($('<td>').append(check));
    tr.append($('<td>').append(inputName));
    tr.append(`<td>${row.id}</td>`);

    if(selectedColumns.hasOwnProperty(row.id)) {
    	check.prop('checked', true);
      if(selectedColumns[row.id].label != row.id)
      inputName.val(selectedColumns[row.id].label);
      inputOrder.val(Number(selectedColumns[row.id].order));
    }

    if(row.id != row.name) inputName.val(row.name);

    check.on('click', e => {
      if (check.prop('checked')) {
        selectedColumns[row.id] = {
          label: inputName.val() != '' ? inputName.val() : row.name,
          columnID: row.id,
          order: Number(inputOrder.val()) || 0
        }
      } else {
      	delete selectedColumns[row.id];
      }
    });
    inputName.on('input', e => {
      if (!check.prop('checked')) return;
      selectedColumns[row.id] = {
          label: inputName.val() != '' ? inputName.val() : row.name,
          columnID: row.id,
          order: Number(inputOrder.val()) || 0
        }
    });
    inputOrder.on('input', e => {
      if (!check.prop('checked')) return;
      selectedColumns[row.id] = {
          label: inputName.val() != '' ? inputName.val() : row.name,
          columnID: row.id,
          order: Number(inputOrder.val()) || 0
        }
    });

    body.append(tr);
  });


  thead.append(trHead);
  table.append(thead).append(body);

  let dialog = getModalFullDialog("Поля для отчета", table, "Применить", () => {
  	if(Object.keys(selectedColumns).length > 0) {
    	RD.registryColumns = [];
    	for (let key in selectedColumns) RD.registryColumns.push(selectedColumns[key]);
    	RD.registryColumns.sort((a,b) => a.order - b.order);
    } else {
      RD.registryColumns = RD.registryInfo.columns
      .filter(item => item.visible != '0')
      .sort((a, b) => a.order == 0 ? 0 : a.order - b.order)
      .map(item => ({label: item.label, columnID: item.columnID}));
    }

    if (Object.keys(RD.paramSearch).length > 0) {
      templateSaveButton.attr('disabled', false);
      templateRefreshButton.attr('disabled', false);

      if(templateNameInput.attr('datauuid')) {
        templateNameInput.attr('disabled', true).attr('placeholder', null);
      } else {
        templateNameInput.attr('disabled', false).attr('placeholder', 'введите наименование шаблона...');
      }
    }

    UIkit.modal(dialog).hide();
  });
  UIkit.modal(dialog).show();
  dialog.on('hidden', () => {
  	if(Object.keys(selectedColumns).length == 0)
    RD.registryColumns = RD.registryInfo.columns
    .filter(item => item.visible != '0')
    .sort((a, b) => a.order == 0 ? 0 : a.order - b.order)
    .map(item => ({label: item.label, columnID: item.columnID}));
  	dialog.remove();
  });

});

//Метод сохранения шаблона отчета
const saveTemplateReport = () => {
  let url = getUrlSearch();
  if(!url) {
    itsmShowMessage({message: "Необходимо заполнить обязательное поле", type: 'error'});
    return;
  }

  let templateData = {
    registryColumns: RD.registryColumns,
    filterCode: RD.filterCode,
    paramSearch: RD.paramSearch
  };

  if(!templateNameInput.attr('datauuid')) {
    if(templateNameInput.val() !== '') {
      Cons.showLoader();

      let name = templateNameInput.val();
      let url = 'api/registry/data_ext?registryCode=rd_registry_report_template';
      url += `&field=rd_form_userid&value=${AS.OPTIONS.currentUser.userid}&condition=TEXT_EQUALS`;
      url += `&field1=rd_form_registry_code&value1=${RD.registryCode}&condition1=TEXT_EQUALS`;
      url += `&field2=rd_form_name&value2=${name}&condition2=TEXT_EQUALS`;
      url += `&loadData=false&pageNumber=0&countInPart=1`;

      //поиск шаблонов совпадающих по параметрам
      rest.synergyGet(url, res => {
        if(res.count > 0) {
          UIkit.modal.alert(`Шаблон с именем "${name}" уже существует, повторите попытку сохранения с другим именем`);
        } else {
          let asfData = [];
          asfData.push({"id": "rd_form_name", "type": "textbox", "value": name});
          asfData.push({"id": "rd_form_userid", "type": "textbox", "value": AS.OPTIONS.currentUser.userid});
          asfData.push({"id": "rd_form_registry_code", "type": "textbox", "value": RD.registryCode});
          asfData.push({"id": "rd_form_params", "type": "textarea", "value": JSON.stringify(templateData)});

          let body = {"registryCode": "rd_registry_report_template", "data": asfData};

          AS.FORMS.ApiUtils.simpleAsyncPost('rest/api/registry/create_doc_rcc', null, null, JSON.stringify(body), "application/json; charset=utf-8")
          .then(res => {
            Cons.hideLoader();
            if(res.errorCode != '0') {
              UIkit.modal.alert('Ошибка сохранения шаблона');
            } else {
              UIkit.modal.alert('Шаблон успешно сохранен');
              templateSaveButton.attr('disabled', true);
              templateDeleteButton.attr('disabled', false);
              templateNameInput
              .attr('disabled', true)
              .attr('placeholder', null)
              .attr('datauuid', res.dataID);
            }
          });
        }
      });
    } else {
      templateNameInput.addClass('empty');
      UIkit.modal.alert('Необходимо заполнить обязательное поле наименование шаблона');
    }

  } else {
    Cons.showLoader();

    let asfData = [];
    asfData.push({"id": "rd_form_params", "type": "textarea", "value": JSON.stringify(templateData)});
    let body = {"uuid": templateNameInput.attr('datauuid'), "data": asfData};

    AS.FORMS.ApiUtils.simpleAsyncPost('rest/api/asforms/data/merge', null, null, JSON.stringify(body), "application/json; charset=utf-8")
    .then(res => {
      Cons.hideLoader();
      if(res.errorCode != '0') {
        UIkit.modal.alert('Ошибка сохранения шаблона');
      } else {
        templateSaveButton.attr('disabled', true);
        UIkit.modal.alert('Шаблон успешно сохранен');
      }
    });
  }

}

//Метод удаления шаблона
const deleteTemplateReport = () => {
  try {
    Cons.showLoader();
    let uuid = $('#rd-template-name').attr('datauuid');
    rest.synergyGet(`api/registry/delete_doc?dataUUID=${uuid}`, res => {
      Cons.hideLoader();
      $('#rd-registry-list').trigger('change');
      UIkit.modal.alert('Шаблон удален');
    });
  } catch (e) {
    Cons.hideLoader();
    UIkit.modal.alert('Ошибка удаления шаблона');
  }
}

//Инициализация обработки шаблона
const reportTemplateInit = template => {
  let p = JSON.parse(template.params);
  RD.registryColumns = p.registryColumns;
  RD.filterCode = p.filterCode;
  RD.paramSearch = p.paramSearch;

  selectedColumns = {};
  RD.registryColumns.forEach(col => selectedColumns[col.columnID] = col);

  templateDeleteButton.attr('disabled', false);
  templateRefreshButton.attr('disabled', false);
  templateSaveButton.attr('disabled', true);
  templateNameInput
  .val(template.value)
  .removeClass('empty')
  .attr('disabled', true)
  .attr('datauuid', template.key);

  tableParamBody.empty();
  for(key in p.paramSearch) selectParameter(p.paramSearch[key].field, key);
  applyCustomParamButton.click();
}

//Модальное окно выбора шаблонов
const openModalTemplates = items => {
  let container = $('<div>');
  let table = $('<table class="template-report-table uk-table uk-table-divider uk-table-small uk-table-hover uk-table-responsive">');
  let tBody = $('<tbody>');
  let dialog = $('<div class="uk-flex-top" uk-modal>');
  let md = $('<div class="uk-modal-dialog uk-margin-auto-vertical">');
  let modalBody = $('<div class="uk-modal-body template-modal">');
  let footer = $('<div class="uk-modal-footer uk-text-right">');
  let actionButton = $('<button class="uk-button uk-button-primary uk-button-small uk-margin-left" type="button" disabled>');
  let searchBlock = $('<div class="uk-inline" style="width: 100%;">');
  let searchInput = $('<input class="uk-input" type="text" placeholder="поиск...">');

  searchBlock.append('<span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: search"></span>').append(searchInput);
  container.append(searchBlock).append($('<div class="uk-overflow-auto" style="max-height: 250px; margin-top: 20px;">').append(table));
  table.append(tBody);
  modalBody.append(container);
  footer.append(actionButton);
  dialog.append(md);
  md.append('<button class="uk-modal-close-default" type="button" style="color: #fff;" uk-close></button>');
  md.append('<div class="uk-modal-header"><h3>Сохраненные шаблоны</h3></div>');
  md.append(modalBody).append(footer);

  items.forEach(item => {
    let tr = $('<tr>').attr('key', item.key);
    tr.append($('<td>').text(item.value));
    tBody.append(tr);

    tr.on('click', () => {
      tBody.find('tr').removeClass('selected');
      tr.addClass('selected');
      actionButton.attr('disabled', false);
    });
  });

  searchInput.keyup(function() {
    tBody.find('tr').removeClass('selected');
    actionButton.attr('disabled', true);
    $.each(tBody.find("tr"), function() {
      if($(this).text().toLowerCase().indexOf(searchInput.val().toLowerCase()) === -1) $(this).hide();
      else $(this).show();
    });
  });

  actionButton.html('Выбрать').on('click', e => {
    try {
      reportTemplateInit(items.find(x => x.key === tBody.find('.selected').attr('key')));
      UIkit.modal(dialog).hide();
    } catch (e) {
      console.log(e);
    }
  });

  Cons.hideLoader();
  UIkit.modal(dialog).show();
  dialog.on('hidden', () => dialog.remove());
}

//Кнопка выбора шаблона отчета
templateReportButton.on('click', e => {
  e.preventDefault();
  e.target.blur();
  Cons.showLoader();

  let url = 'api/registry/data_ext?registryCode=rd_registry_report_template';
  url += `&field=rd_form_userid&value=${AS.OPTIONS.currentUser.userid}&condition=TEXT_EQUALS`;
  url += `&field1=rd_form_registry_code&value1=${RD.registryCode}&condition1=TEXT_EQUALS`;
  url += `&fields=rd_form_name&fields=rd_form_params`;

  //поиск сохраненных шаблонов
  rest.synergyGet(url, res => {
    //парсинг найденных шаблонов
    let items = res.result
    .filter(x => x.fieldValue.hasOwnProperty('rd_form_name'))
    .map(x => ({key: x.dataUUID, value: x.fieldValue['rd_form_name'], params: x.fieldValue['rd_form_params']}))
    .sort((a, b) => {
      a = a.value.toLowerCase();
      b = b.value.toLowerCase();
      return a > b ? 1 : a < b ? -1 : 0;
    });
    openModalTemplates(items);
  });

});

//Кнопка сохранения шаблона отчета
templateSaveButton.on('click', e => {
  e.preventDefault();
  e.target.blur();
  saveTemplateReport();
});

//Кнопка удаления шаблона отчета
templateDeleteButton.on('click', e => {
  e.preventDefault();
  e.target.blur();
  let name = $('#rd-template-name').val();
  UIkit.modal.confirm(`Вы действительно хотите удалить шаблон отчета ${name}?`).then(() => {
    deleteTemplateReport();
  }, () => null);
});

//Обновление, очистка филтров
templateRefreshButton.on('click', e => {
  e.preventDefault();
  e.target.blur();
  $('#rd-registry-list').trigger('change');
});

templateNameInput.keyup(() => {
  templateNameInput.removeClass('empty');
});

//Метод инициализации реестров из настроек АРМ
const initRegUserList = () => {
  const registries = Cons.getAppStore().registries;
  let userRegistryList = [];

  registries.forEach(x => {
    if(x.children) x.children.forEach(c => userRegistryList.push({
      registryName: c.name,
      registryCode: c.code,
      rights: c.rights
    }));
    else userRegistryList.push({
      registryName: x.name,
      registryCode: x.code,
      rights: x.rights
    });
  });

  userRegistryList.sort((a, b) => {
    a = a.registryName.toLowerCase();
    b = b.registryName.toLowerCase();
    return a > b ? 1 : a < b ? -1 : 0;
  });

  let select = $('#rd-registry-list');
  select.on('change', e => {
    selectRegistry(userRegistryList, select.val());
  });

  userRegistryList.forEach(item => {
    select.append(`<option value="${item.registryCode}" title="${item.registryName}">${item.registryName}</option>`)
  });
}

//Метод инициализации реестров доступных по правам
const initRegUserListAll = () => {
  rest.synergyGet("api/registry/list", registryList => {
    registryList = filteredRegistry(registryList);

    let select = $('#rd-registry-list');
    select.on('change', e => {
      selectRegistry(registryList, select.val());
    });

    registryList.forEach(item => {
      select.append(`<option value="${item.registryCode}" title="${item.registryName}">${item.registryName}</option>`)
    });
  });
}

//Инициализация списка реестров
// initRegUserListAll();
initRegUserList();

const closeAllSelect = (e) => {
  if ($(e.target).closest('.custom-select').length == 0) $('.custom-select').trigger('close-select');
}
document.addEventListener("click", closeAllSelect);

function r(){
  if(!$('.rd-button-container').length) return;
  let bch = $('.rd-button-container')[0].style.display || 'none';
  tableContainer.height($('.rd-app-content').height() - (bch != 'none' ? 65 : 5));
  if($(document).width() > 768) {
    $('.rd-table-params-container').height($('.rd-app-left').height() - 355);
  } else {
    $('.rd-table-params-container').height($('.rd-app-left').height() - 410);
  }
}
$(window).resize(() => r());
r();
