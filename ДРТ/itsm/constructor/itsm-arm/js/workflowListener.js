const getParentFilter = () => {
  return AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/workflow/works/filters/${AS.OPTIONS.currentUser.userid}`);
}

const getChildrenFilter = filterType => {
  return AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/workflow/works/filters/${AS.OPTIONS.currentUser.userid}?filterType=${filterType}`);
}

const getDefaultIcon = () => $('<span uk-icon="icon: folder; ratio: 1.2;" class="itsm-nav-icon uk-margin-small-left" style="height: auto; color: #1E87F0;">');

const initWorkFilters = () => {
  let menuContainer = $('#work-filter');
  let nav = $(`<ul class="uk-nav uk-nav-parent-icon itsm-nav" uk-nav>`);
  let counters;

  menuContainer.empty().append(nav);

  AS.FORMS.ApiUtils.simpleAsyncGet('rest/api/workflow/get_counters').then(res => {
    counters = res;

    getParentFilter().then(parents => {
      parents.forEach(parent => {
        let menuItem = $('<li>');
        let a = $('<a>').addClass('itsm-nav-link uk-link-text');

        a.append(getDefaultIcon()).append(`<span class="itsm-nav-link-text">${parent.name}</span>`);
        menuItem.append(a);
        nav.append(menuItem);

        let allWork = counters.find(x => x.key == `${parent.filterType}_null_null`).value;

        if(parent.filterType == 'OWN_WORKS') {
          let newWork = counters.find(x => x.key == `${parent.filterType}_null_null_`).value;
          a.append(`<span class="uk-badge" style="margin-right: 5px;">${newWork}/${allWork}</span>`);
          a.addClass('itsm-nav-link-active');
        } else {
          a.append(`<span class="uk-badge" style="margin-right: 5px;">${allWork}</span>`);
        }

        a.on('click', e => {
          e.preventDefault();
          e.target.blur();
          if(a.hasClass('itsm-nav-link-active')) return;

          $('.itsm-nav-link').removeClass("itsm-nav-link-active");
          a.addClass("itsm-nav-link-active");
          fire({type: 'worklist_filter_change', filterCode: parent.filterType}, 'workList-1');
        });

        // ДОЧЕРНИИ ФИЛЬТРЫ
        // if(parent.hasChildren && parent.hasChildren == "true") {
        //   getChildrenFilter(parent.filterType).then(childrenFilter => {
        //
        //     let childMenu = $('<ul>', {class: 'uk-nav-sub'});
        //     menuItem.append(childMenu);
        //     menuItem.addClass('uk-parent');
        //
        //     childrenFilter.forEach(child => {
        //       let childLi = $('<li>');
        //       let childA = $('<a>').addClass('itsm-nav-link uk-link-text');
        //       let childCount = 0;
        //
        //       childA.on('click', e => {
        //         e.preventDefault();
        //         e.target.blur();
        //         if(childA.hasClass('itsm-nav-link-active')) return;
        //
        //         $('.itsm-nav-link').removeClass("itsm-nav-link-active");
        //         childA.addClass("itsm-nav-link-active");
        //
        //         if(parent.filterType == 'OWN_WORKS') {
        //           fire({type: 'worklist_filter_code_change', userFilterCode: child.filterID}, 'workList-1');
        //         } else {
        //           fire({type: 'worklist_filter_code_change', userFilterCode: child.objectID}, 'workList-1');
        //         }
        //       });
        //
        //       if(parent.filterType == 'OWN_WORKS') {
        //         childCount = counters.find(x => x.key == `${parent.filterType}_${child.filterID}_null`).value;
        //       } else {
        //         childCount = counters.find(x => x.key == `${parent.filterType}_null_${child.objectID}`).value;
        //       }
        //
        //       childMenu.append(childLi);
        //       childLi.append(childA);
        //       childA.append(`<span class="itsm-nav-link-text">${child.name}</span>`);
        //       childA.append(`<span class="uk-badge" style="margin-right: 5px;">${childCount}</span>`);
        //     });
        //   });
        // }

      });
    });

  });
}

const initPeriodFilter = () => {
  const options = [
    {title: 'В работе', value: 'inProgress'},
    {title: 'Прошедший квартал', value: 'lastQuarter'},
    {title: 'Прошедший месяц', value: 'lastMonth'},
    {title: 'Прошедшая неделя', value: 'lastWeek'},
    {title: 'Сегодня', value: 'today'},
    {title: 'Будущая неделя', value: 'nextWeek'},
    {title: 'Будущий месяц', value: 'nextMonth'},
    {title: 'Будущий квартал', value: 'nextQuarter'}
  ];

  let label = $('<label class="uk-form-label uk-margin-left" for="work-period-filter">Период работ</label>')
  let select = $('<select class="uk-select" id="work-period-filter" style="min-width: 100px;">');
  options.forEach(item => select.append(`<option value="${item.value}" title="${item.title}">${item.title}</option>`));

  select.on('change', e => {
    e.preventDefault();
    e.target.blur();
    fire({type: 'worklist_period_change', period: select.val()}, 'workList-1');
  });

  $('#work-period').append(label).append(select);
}

pageHandler('workflow_page', () => {
  let pages = cookie.getJson('pages');
  if(pages) {
  	pages.currentPage = 'workflow_page';
  	cookie.setJson('pages', pages);
  }

  initWorkFilters();
  initPeriodFilter();

  if(!Cons.getAppStore().workAddListener) {
    Cons.setAppStore({ workAddListener: true });
    addListener('worklist_item_click', 'workList-1', e => {
      let params = {actionID: e.actionID};
      Cons.showLoader();
      try {
        AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/workflow/work/${e.actionID}/document`).then(doc => {
          if(doc && doc.documentID) {
            params.documentID = doc.documentID;
            return AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/docflow/doc/document_info?documentID=${params.documentID}`);
          }
        }).then(docInfo => {
          params.docInfo = docInfo;
          return AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/info?registryID=${params.docInfo.registryID}`);
        }).then(regInfo => {
          Cons.hideLoader();

          let event = {
            dataUUID: params.docInfo.asfDataID,
            registryCode: regInfo.code,
            registryName: regInfo.name,
            formName: params.docInfo.formName,
            editable: false
          };

          if(event.dataUUID) {
            $('#root-panel').trigger({type: 'custom_open_document', eventParam: event});
          } else {
            itsmShowMessage({message: "Ошибка при открытии работы", type: 'error'});
          }

        });
      } catch (e) {
        Cons.hideLoader();
        console.log(e);
      }
    });
  }

  if(!$('#workList-1 table').hasClass('uk-table-responsive')) $('#workList-1 table').addClass('uk-table-responsive');
});
