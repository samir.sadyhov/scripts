const getRegInfo = code => {
  const registries = Cons.getAppStore().registries;
  let result;
  registries.forEach(item => {
    if (item.children) {
      item.children.forEach(child => {
        if (child.code == code) result = child;
      })
    } else {
      if (item.code == code) result = item;
    }
  });
  return result;
}

const filterAgrProcesses = processes => {
  let result = [];
  function search(p) {
    p.forEach(function(process) {
      if (!process.finished && (process.typeID == 'AGREEMENT_ITEM' || process.typeID == 'ACQUAINTANCE_ITEM')) {
        result.push(process);
      }
      if (process.subProcesses.length > 0) search(process.subProcesses);
    });
  }
  search(processes);
  return result;
}

const createEvent = (e, documentID) => {
  try {
    let event = {"api_event": `event.docflow.document.${e}`, "documentId": documentID, "userId": AS.OPTIONS.currentUser.userid};
    let data = {
      "eventMsg": JSON.stringify(event),
      "eventName": "event.docflow.document"
    }
    AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/events/create", res => {
      if(res.errorCode != '0') throw new Error(res.errorMessage);
      let openDocs = Cons.getAppStore().openDocs || [];
      if(e == 'opened') {
        openDocs.push(documentID);
      } else {
        openDocs = openDocs.filter(v => v != documentID);
      }
      openDocs = openDocs.filter((v, i, a) => i == a.indexOf(v));
      Cons.setAppStore({openDocs: openDocs});
    }, null, data, "application/x-www-form-urlencoded; charset=UTF-8", err => {
      console.log(err);
    });
  } catch (e) {
    console.log(e);
  }
}

const setRating = (playerModel, rating, handler) => {
  if(!rating) {
    handler();
  } else if(playerModel.formCode !== 'itsm_form_knowledgebase') {
    handler();
  } else {
    let rating = "itsm_form_knowledgebase_rating";
    let ratingsCount = "itsm_form_knowledgebase_ratingsCount";
    let div = $('<div>').css("border", "1px solid #afafaf");
    let starsPanel = $("<div>");
    let stars = [];
    let userRating = 1;

    function createStar(stars, index, onclick) {
      let starPanel = $("<span>").html("☆")
      .css({'font-size': '40px', 'color': '#1e87f0', 'cursor': 'pointer'});
      if (index == 0) starPanel.html("★");
      starPanel.hover(function() {
        for (let i = 0; i < 5; i++) stars[i].css('color', '#1e87f0"').html("☆");
        for (let i = 0; i <= index; i++) stars[i].css('color', '#1e87f0"').html("★");
      });
      starPanel.on('click', e => {
        onclick(index);
      });
      return starPanel;
    }

    function ratingClicked(index) {
      Cons.showLoader();
      userRating = index + 1;
      let modelRating = playerModel.getModelWithId(rating);
      let modelRatingsCount = playerModel.getModelWithId(ratingsCount);

      let globalRating = modelRating.getValue() * 1;
      let usersCount = modelRatingsCount.getValue() * 1;
      let newRating = (globalRating * usersCount + userRating) / (usersCount + 1);

      modelRating.setValue(newRating + "");
      modelRatingsCount.setValue((usersCount + 1) + "");
      playerModel.hasChanges = false;

      let data = {
        data: '"data" : ' + JSON.stringify(playerModel.getAsfData().data),
        form: playerModel.formId,
        uuid: playerModel.asfDataId
      };

      AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/asforms/form/multipartdata", res => {
        Cons.hideLoader();
        div.dialog("destroy");
        handler();
      }, "text", data, "application/x-www-form-urlencoded; charset=UTF-8", err => {
        Cons.hideLoader();
        div.dialog("destroy");
        handler();
        itsmShowMessage({message: "Нет прав на редактирование файла", type: 'error'});
      });
    }

    for (let i = 0; i < 5; i++) {
      let star = createStar(stars, i, ratingClicked);
      stars.push(star);
      starsPanel.append(star);
    }

    div.append(starsPanel);
    div.dialog({
      title: i18n.tr('Оцените полезность данной статьи:'),
      width: 300,
      height: 110,
      modal: true,
      closeOnEscape: true,
      close: function() {
        handler();
      }
    });
  }
}

const checkSavedForm = (formPlayer, handler) => {
  if(formPlayer.model.hasChanges) {
    UIkit.modal.confirm('Документ был изменен. Сохранить произведенные изменения?').then(() => {
      if(!formPlayer.model.isValid()) {
        itsmShowMessage({message: "Заполните обязательные поля!", type: 'warning'});
      } else {
        Cons.showLoader();
        let asfData = formPlayer.model.getAsfData();
        let data = {
          data: '"data" : ' + JSON.stringify(asfData.data),
          form: asfData.form,
          uuid: asfData.uuid
        };
        AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/asforms/form/multipartdata", res => {
          Cons.hideLoader();
          itsmShowMessage({message: '<svg width="35" height="35" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><polyline fill="none" stroke="#fff" stroke-width="1.1" points="4,10 8,15 17,4"></polyline></svg> Успешно', type: 'success'});
          handler();
        }, "text", data, "application/x-www-form-urlencoded; charset=UTF-8", err => {
          Cons.hideLoader();
          handler();
          itsmShowMessage({message: "Нет прав на редактирование файла", type: 'error'});
        });
      }
    }, handler);
  } else {
    handler();
  }
}

const closeDocument = (formPlayer, rating) => {
  setRating(formPlayer.model, rating, () => {
    let openDocsWindow = Cons.getAppStore().openDocsWindow.filter(x => x.dataUUID !== formPlayer.dataUUID);
    Cons.setAppStore({openDocsWindow: openDocsWindow});
    formPlayer.panels.documentWindow.remove();
    formPlayer.panels.documentPanel.remove();
    formPlayer.destroy();
    if(formPlayer.registryCode == 'itsm_registry_incidents') createEvent('closed', formPlayer.documentID);
  });
}

const buttonSort = (formPlayer) => {
  function s(a, b) {
    let idA = $(a).attr("id");
    let idB = $(b).attr("id");
    if (idA > idB) return 1;
    if (idA < idB) return -1;
    return 0;
  }
  let mobileMenuContent = formPlayer.panels.mobileMenu.find('.arm-mobile-two-panel');
  let buttons = formPlayer.panels.panelButtons.find('.itsm-action-button');
  buttons.sort(s);
  $(buttons).appendTo(formPlayer.panels.panelButtons);
  let buttons2 = mobileMenuContent.find('.itsm-action-button');
  buttons2.sort(s);
  $(buttons2).appendTo(mobileMenuContent);
}

const createButton = (formPlayer, name, id, handler, greenButton) => {
  let mobileMenu = formPlayer.panels.mobileMenu;
  let mobileMenuContent = mobileMenu.find('.arm-mobile-two-panel');
  let button = $(`<button id="${id}" class="uk-button margined uk-button-default itsm-action-button fonts">`)
  .css({
    'width': 'calc(85% - 10px)',
    'white-space': 'nowrap',
    'text-overflow': 'ellipsis',
    'overflow': 'hidden',
    'padding': '0 5px',
  })
  .attr('title', name)
  .html(name)
  .on('click', e => {
    e.preventDefault();
    e.target.blur();
    mobileMenu.trigger('hideShowMenu');
    handler();
  });
  if(greenButton) button.addClass('itsm-action-button-green');
  formPlayer.panels.panelButtons.append(button);
  button.clone(true).appendTo(mobileMenuContent);
  buttonSort(formPlayer);
}

/*{
    "document_close_assignment": true/false,    завершение    assignment
    "document_close_approvement": true/false,   утверждение   approval
    "document_close_acq": true/false,           ознакомление  acquaintance
    "document_close_agreement": true/false      согласование  agreement
}*/
const getDocflowSettings = () => {
  return new Promise(resolve => {
    let docflowSettings = Cons.getAppStore().docflowSettings;
    if(docflowSettings) {
      resolve(docflowSettings);
    } else {
      fetch(`${window.origin}/itsm/rest/config/docflow-settings`, {headers: {
        'Authorization': "Basic " + btoa(AS.OPTIONS.login + ":" + AS.OPTIONS.password)
      }})
      .then(res => res.json())
      .then(res => {
        if(!res.errorCode) {
          Cons.setAppStore({docflowSettings: res});
          resolve(res);
        } else {
          resolve(null);
        }
      });
    }
  });
}

const saveFormData = (formPlayer, handler) => {
  if(!formPlayer.model.isValid()) {
    itsmShowMessage({message: "Заполните обязательные поля!", type: 'warning'});
  } else {
    Cons.showLoader();
    try {
      let asfData = formPlayer.model.getAsfData();
      let data = {
        data: '"data" : ' + JSON.stringify(asfData.data),
        form: asfData.form,
        uuid: asfData.uuid
      };
      AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/asforms/form/multipartdata", res => {
        Cons.hideLoader();
        itsmShowMessage({message: '<svg width="35" height="35" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><polyline fill="none" stroke="#fff" stroke-width="1.1" points="4,10 8,15 17,4"></polyline></svg> Успешно', type: 'success'});
        formPlayer.model.hasChanges = false;
        if(handler) handler();
      }, "text", data, "application/x-www-form-urlencoded; charset=UTF-8", err => {
        Cons.hideLoader();
        itsmShowMessage({message: "Нет прав на редактирование файла", type: 'error'});
      });
    } catch (error) {
      Cons.hideLoader();
      itsmShowMessage({message: "Ошибка сохранения данных по форме", type: 'error'});
      console.log(error);
    }
  }
}

const getModalDialog = (title, body, buttonName, buttonAction) => {
  let dialog = $('<div class="uk-flex-top" uk-modal>');
  let md = $('<div class="uk-modal-dialog uk-margin-auto-vertical">');
  let modalBody = $('<div class="uk-modal-body" uk-overflow-auto>');
  let footer = $('<div class="uk-modal-footer uk-text-right">');

  modalBody.css({'background': '#e1e7f3'}).append(body);
  footer.append('<button class="uk-button uk-button-default uk-modal-close" type="button">Закрыть</button>');
  dialog.append(md);
  md.append(`<div class="uk-modal-header"><h3>${title}</h3></div>`)
  .append(modalBody).append(footer);

  if(buttonName) {
    let actionButton = $('<button class="uk-button uk-button-primary uk-margin-left" type="button">');
    actionButton.html(buttonName).on('click', buttonAction);
    footer.append(actionButton);
  }

  return dialog;
}

const getDialogAgreement = (title, buttons, handler) => {
  let dialog = $('<div class="uk-flex-top" uk-modal>');
  let md = $('<div class="uk-modal-dialog uk-margin-auto-vertical">');
  let modalBody = $('<div class="uk-modal-body" uk-overflow-auto>');
  let footer = $('<div class="uk-modal-footer uk-text-right">');
  let commentInput = $('<textarea class="uk-textarea" rows="5" placeholder="Комментарий"></textarea>');

  if(title !== 'Ознакомление') {
    modalBody.append($('<div class="uk-margin">').append(commentInput));
  }

  dialog.append(md);
  md.append(`<div class="uk-modal-header"><h3>${title}</h3></div>`)
  .append(modalBody).append(footer);

  buttons.forEach(button => {
    let agreeButton = $('<button class="uk-button uk-button-default uk-margin-left" type="button">');
    agreeButton.html(button.label).on('click', () => handler(button.signal, commentInput.val()));
    footer.append(agreeButton)
  });

  return dialog;
}

const printForm = content => {
  var css = '<link rel="stylesheet" href="synergyForms.css" type="text/css" />';
  var WinPrint = window.open();
  WinPrint.document.write('');
  WinPrint.document.write(css);
  WinPrint.document.write(content.innerHTML);
  WinPrint.document.write('');
  WinPrint.document.close();
  WinPrint.focus();
  WinPrint.print();
}

const cutText = text => {
  if(text.length > 100) return text.slice(0, 100) + '...';
  return text;
}

const closeAfterActivation = code => {
  return new Promise(resolve => {
    try {
      fetch(`${window.origin}/itsm/rest/registry/closeAfterActivation?registryCode=${code}`)
      .then(res => res.json())
      .then(res => res.errorCode == 0 ? resolve(res.result) : resolve(null));
    } catch (e) {
      console.log(e);
      resolve(null);
    }
  });
}

const activateDocument = formPlayer => {
  if(!formPlayer.model.isValid()) {
    itsmShowMessage({message: "Заполните обязательные поля!", type: 'warning'});
    return;
  }

  Cons.showLoader();
  try {
    let asfData = formPlayer.model.getAsfData();
    let regInfo = getRegInfo(formPlayer.registryCode);
    $.when(AS.FORMS.ApiUtils.saveAsfData(asfData.data, asfData.form, asfData.uuid))
    .then(uuid => AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/activate_doc?dataUUID=${asfData.uuid}`))
    .then(result => {
      Cons.hideLoader();
      itsmShowMessage({message: '<svg width="35" height="35" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><polyline fill="none" stroke="#fff" stroke-width="1.1" points="4,10 8,15 17,4"></polyline></svg> Успешно', type: 'success'});

      //закрыть окно после активации
      if(!regInfo) {
        closeAfterActivation(formPlayer.registryCode).then(res => {
          if(res) {
            closeDocument(formPlayer);
            $('.itsm-refresh-button').click();
          } else {
            formPlayer.editReadFormButton.trigger('click');
          }
        })
      } else {
        if(regInfo.closeAfterActivation) {
          closeDocument(formPlayer);
          $('.itsm-refresh-button').click();
        } else {
          formPlayer.editReadFormButton.trigger('click');
        }
      }
    });
  } catch (error) {
    Cons.hideLoader();
    itsmShowMessage({message: "Произошла ошибка при создании заявки, обратитесь к администратору", type: 'error'});
    console.log(error);
  }
}

const finishWork = (formPlayer, processes, formCode) => {
  try {
    if(formPlayer.model.getErrors().length) throw new Error('Заполните обязательные поля');

    checkSavedForm(formPlayer, () => {
      let url = `rest/api/workflow/work/get_form_for_result?formCode=${formCode}&workID=${processes[0].actionID}`;
      AS.FORMS.ApiUtils.simpleAsyncGet(url).then(res => {
        if(res.errorCode && res.errorCode != '0') throw new Error(res.errorMessage);

        let player = AS.FORMS.createPlayer();
        player.view.setEditable(true);
        player.showFormData(null, null, res.dataUUID);

        let dialog = getModalDialog("Форма завершения", player.view.container, "Сохранить", () => {
          Cons.showLoader();
          try {
            if (!player.model.isValid()) throw new Error('Заполните обязательные поля');
            player.saveFormData(saveResult => {
              let finishWorkBody = {
                  workID: processes[0].actionID,
                  completionForm: "FORM",
                  type: 'work',
                  file_identifier: res.file_identifier
              };
              AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/workflow/work/set_result", result => {
                if(result.errorCode && result.errorCode != '0') throw new Error(result.errorMessage);
                Cons.hideLoader();
                UIkit.modal(dialog).hide();
                player.destroy();

                //закрыть окно после завершения работы
                getDocflowSettings().then(docflowSettings => {
                  if(docflowSettings && docflowSettings.document_close_assignment) {
                    closeDocument(formPlayer);
                    $('.itsm-refresh-button').click();
                  } else {
                    formPlayer.buttonClass.refresh();
                  }
                });
              }, null, finishWorkBody, "application/x-www-form-urlencoded; charset=UTF-8", err => {
                console.log(err);
              });

            });
          } catch (e) {
            Cons.hideLoader();
            itsmShowMessage({message: e.message, type: 'error'});
          }
        });

        UIkit.modal(dialog).show();
        dialog.on('hidden', () => {
          dialog.remove();
          formPlayer.panels.formPanel.removeClass('_lock');
        });
      });
    });
  } catch (e) {
    itsmShowMessage({message: e.message, type: 'error'});
    console.log(e);
  }
}

const finishWorkNotForm = (formPlayer, process, comment = "Работа завершена") => {
  try {
    if(formPlayer.model.getErrors().length) throw new Error('Заполните обязательные поля');
    checkSavedForm(formPlayer, () => {
      Cons.showLoader();
      let data = {workID: process[0].actionID, completionForm: "COMMENT", comment: comment};
      AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/workflow/work/set_result", result => {
        if(result.errorCode && result.errorCode != '0') throw new Error(result.errorMessage);
        Cons.hideLoader();

        //закрыть окно после завершения работы
        getDocflowSettings().then(docflowSettings => {
          if(docflowSettings && docflowSettings.document_close_assignment) {
            closeDocument(formPlayer);
            $('.itsm-refresh-button').click();
          } else {
            formPlayer.buttonClass.refresh();
          }
        });
      }, null, data, "application/x-www-form-urlencoded; charset=UTF-8", err => {
        console.log(err);
      });

    });
  } catch (e) {
    console.log(e);
    Cons.hideLoader();
  }
}

const agreementDoc = (formPlayer, process, title) => {
  Cons.showLoader();
  try {
    AS.FORMS.ApiUtils.simpleAsyncGet("rest/api/workflow/process_info?workID=" + process.actionID).then(res => {
      let dialog = getDialogAgreement(title, res.buttons, (signal, comment) => {
        Cons.showLoader();
        let data = {
          procInstID: process.itemID,
          signal: signal,
          rawdata: res.raw_data,
          comment: comment
        };
        AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/workflow/finish_process", result => {
          if(result.errorCode && result.errorCode != '0') throw new Error("произошла ошибка при согласовании/ознакомлении");
          UIkit.modal(dialog).hide();
          Cons.hideLoader();

          getDocflowSettings().then(docflowSettings => {
            switch (process.typeID) {
              case 'AGREEMENT_ITEM':
                if(docflowSettings && docflowSettings.document_close_agreement) {
                  closeDocument(formPlayer);
                  $('.itsm-refresh-button').click();
                }
                break;
              case 'ACQUAINTANCE_ITEM':
                if(docflowSettings && docflowSettings.document_close_acq) {
                  closeDocument(formPlayer);
                  $('.itsm-refresh-button').click();
                }
                break;
              default: formPlayer.buttonClass.refresh();;
            }
          });

        }, null, data, "application/x-www-form-urlencoded; charset=UTF-8", err => {
          console.log(err);
        });
      });
      UIkit.modal(dialog).show();
      dialog.on('hidden', () => {
        dialog.remove();
        formPlayer.panels.formPanel.removeClass('_lock');
      });
      Cons.hideLoader();
    });
  } catch (e) {
    console.log(e);
    Cons.hideLoader();
  }
}

const getCollateData = (formPlayer, items) => {
  let data = [];
  let currentFormData = formPlayer.model.getAsfData();
  items.forEach(id => {
    let fromData;
    if(id.from == 'CURRENT_DOCUMENTID') {
      fromData = {
        type: "reglink",
        key: formPlayer.documentID,
        valueID: formPlayer.documentID,
        value: formPlayer.documentName
      };
    } else {
      fromData = UTILS.getValue(currentFormData, id.from);
    }
    if(fromData) {
      let field = UTILS.createField(fromData);
      if(field.type == 'appendable_table' && field.hasOwnProperty('data') && field.data.length) {
        field.data = field.data.filter(x => x.type !== 'label');
      }
      if(id.to.indexOf('.') == -1) {
        field.id = id.to;
        data.push(field);
      } else {
        let tmpTableData = UTILS.createField({
          id: id.to.split('.')[0],
          type: "appendable_table",
          key: "",
          data: []
        });
        field.id = id.to.split('.')[1] + '-b1';
        tmpTableData.data.push(field);
        data.push(tmpTableData);
      }
    }
  });
  return data;
}

const createDocument = (formPlayer, code, items, data) => {
  Cons.showLoader();
  let regInfo = getRegInfo(code);
  let asfData = getCollateData(formPlayer, items);
  if(data) data.forEach(field => asfData.push(field));
  try {
    AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/registry/create_doc_rcc", res => {
      if(res.errorCode != '0') throw new Error(res.errorMessage);
      Cons.hideLoader();
      let event = {
        type: 'custom_open_document',
        dataUUID: res.dataID,
        registryCode: code,
        editable: true
      };
      if($(document).width() < 769) event.viewCode = 'mobile';

      if(regInfo && regInfo.hasOwnProperty('formName')) {
        event.formName = regInfo.formName;
        event.registryName = regInfo.name;
        fire(event, 'root-panel');
      } else {
        AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/info?code=${code}`).then(res => {
          event.registryName = res.name;
          return AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/asforms/form/${res.formId}`);
        }).then(res => {
          event.formName = res.nameru;
          fire(event, 'root-panel');
        });
      }

    }, null, JSON.stringify({"registryCode": code, "data": asfData}), "application/json; charset=utf-8", err => {
      console.log(err.responseText);
      Cons.hideLoader();
      itsmShowMessage({message: "Произошла ошибка при создании документа", type: 'error'});
    });
  } catch (e) {
    console.log(e);
    Cons.hideLoader();
    itsmShowMessage({message: "Произошла ошибка при создании документа", type: 'error'});
  }
}

class Incident {
  constructor(player, process) {
    this.formPlayer = player;
    this.dataUUID = player.dataUUID;
    this.workName = player.panels.workName;
    this.panelButtons = player.panels.panelButtons;
    this.process = process;
    this.groups = [];

    this.init();
  }

  reassign(){
    try {
      if(this.formPlayer.model.getErrors().length) throw new Error('Заполните обязательные поля');

      checkSavedForm(this.formPlayer, () => {
        let service_user = this.formPlayer.model.getModelWithId('itsm_form_incident_service_user').getAsfData();
        let processes = UTILS.getUserWork(this.process, service_user.key, 'ASSIGNMENT_ITEM');
        if(!processes.length) throw new Error('Не найден идентификатор маршрута работы');

        let workId = processes[0].actionID;
        let url = `rest/api/workflow/work/get_form_for_result?formCode=itsm_form_incident_wcf_new&workID=${workId}`;
        AS.FORMS.ApiUtils.simpleAsyncGet(url).then(formWork => {
          let player = AS.FORMS.createPlayer();
          player.view.setEditable(true);
          player.showFormData(null, null, formWork.dataUUID);
          player.model.disabledNewValueList = false;
          player.model.on('dataLoad', () => {
            player.model.disabledNewValueList = false;
            let wcf_status = player.model.getModelWithId('itsm_form_incident_wcf_status');
            if(wcf_status) {
              wcf_status.setValue("3");
              if(player.view.getViewWithId('itsm_form_incident_wcf_status'))
              player.view.getViewWithId('itsm_form_incident_wcf_status').setEnabled(false);
            }
          });
          let dialog = getModalDialog("Форма завершения инцидента", player.view.container, "Готово", () => {
            Cons.showLoader();
            try {
              if (player.model.getErrors().length) throw new Error('Заполните обязательные поля');
              player.saveFormData(saveResult => {

                let urlMethod = `/itsm/rest/workflow/work/system/set_result?workId=${workId}&file_identifier=${formWork.file_identifier}&dataUUID=${this.dataUUID}`;
                let request = $.ajax({
                  url: window.location.origin + urlMethod,
                  type: 'POST',
                  beforeSend: AS.FORMS.ApiUtils.addAuthHeader,
                  headers: {'Content-Type': 'application/json'},
                  dataType: 'json'
                });
                request.done(result => {
                  this.formPlayer.model.getModelWithId('itsm_form_incident_status').setValue("2");
                  AS.FORMS.ApiUtils.saveAsfData(this.formPlayer.model.getAsfData().data, this.formPlayer.model.formId, this.dataUUID)
                  .then(uid => {
                    Cons.hideLoader();
                    UIkit.modal(dialog).hide();

                    //закрыть окно после завершения работы
                    getDocflowSettings().then(docflowSettings => {
                      if(docflowSettings && docflowSettings.document_close_assignment) {
                        closeDocument(this.formPlayer);
                      } else {
                        this.formPlayer.buttonClass.refresh();
                      }
                    });

                  });
                });
                request.fail((jqXHR, textStatus ) => {
                  console.log(jqXHR);
                  Cons.hideLoader();
                  itsmShowMessage({message: textStatus, type: 'error'});
                  UIkit.modal(dialog).hide();
                });
              });
            } catch (e) {
              Cons.hideLoader();
              itsmShowMessage({message: e.message, type: 'error'});
              UIkit.modal(dialog).hide();
            }
          });
          UIkit.modal(dialog).show();
          dialog.on('hidden', () => {
            dialog.remove();
            this.formPlayer.panels.formPanel.removeClass('_lock');
          });
        });
      });
    } catch (e) {
      itsmShowMessage({message: e.message, type: 'error'});
      console.log(e);
    }
  }

  inProblem(){
    let m = [];
    m.push({from: 'itsm_form_incident_priority', to: 'itsm_form_problem_priority'});
    m.push({from: 'itsm_form_incident_userscount', to: 'itsm_form_problem_userscount'});
    m.push({from: 'itsm_form_incident_influence', to: 'itsm_form_problem_influence'});
    m.push({from: 'itsm_form_incident_confitemlink', to: 'itsm_form_problem_confitemlink'});
    m.push({from: 'itsm_form_incident_urgency', to: 'itsm_form_problem_urgency'});
    m.push({from: 'itsm_form_incident_category', to: 'itsm_form_problem_category'});
    m.push({from: 'itsm_form_incident_description', to: 'itsm_form_problem_description'});
    m.push({from: 'itsm_form_incident_servicelink', to: 'itsm_form_problem_servicelink'});
    m.push({from: 'CURRENT_DOCUMENTID', to: 'itsm_form_problem_parent_incident'});
    checkSavedForm(this.formPlayer, () => {
      createDocument(this.formPlayer, 'itsm_registry_problems', m);
    });
  }

  inRFC(){
    let m = [];
    m.push({from: 'itsm_form_incident_servicelink', to: 'itsm_form_rfc_servicelink'}); //Услуга
    m.push({from: 'itsm_form_incident_confitemlink', to: 'itsm_form_rfc_ci'}); //Конфигурационные единицы
    m.push({from: 'itsm_form_incident_theme', to: 'itsm_form_rfc_theme'}); //Тема
    m.push({from: 'itsm_form_incident_description', to: 'itsm_form_rfc_description'}); //Описание
    m.push({from: 'CURRENT_DOCUMENTID', to: 'itsm_form_rfc_incidentlink'}); //Ссылка на обращение
    checkSavedForm(this.formPlayer, () => {
      createDocument(this.formPlayer, 'itsm_registry_rfc', m, [{id: 'itsm_form_rfc_source', type: 'listbox', key: '2', value: 'Обращение'}]);
    });
  }

  showHistory(){
    function processFilter(processes) {
      let result = [];
      function search(p) {
        p.forEach(function(process) {
          if (process.comment.indexOf('Форма завершения инцидента') != -1) result.push(process);
          if (process.subProcesses.length > 0) search(process.subProcesses);
        });
      }
      search(processes);
      return result.sort((a,b) => new Date(a.started) - new Date(b.started)).filter(x => x.actionID);
    }

    Cons.showLoader();
    let me = this;

    let dialog = $('<div class="uk-modal-container" uk-modal>');
    let md = $('<div>', {class: 'uk-modal-dialog'});
    let modalBody = $('<div class="uk-modal-body" uk-overflow-auto>');
    dialog.append(md);
    md.append(`<div class="uk-modal-header"><h2 class="uk-modal-title">История инцидента ${me.formPlayer.model.getModelWithId('itsm_form_incident_id').getTextValue()}</h2></div>`)
      .append(modalBody)
      .append('<div class="uk-modal-footer uk-text-right"><button class="uk-button uk-button-default uk-modal-close" type="button">Закрыть</button></div>');

    let oldDateHeader = me.formPlayer.model.getModelWithId('itsm_form_incident_regdate').getValue();
    let oldDateHN = new Date(""+oldDateHeader).toLocaleString('ru', {year: 'numeric', month: 'long', day: 'numeric'});
    let newDateHeader = oldDateHN.replace(/(?:([А-Я]{3}).+$)/i,'$1, ' + oldDateHeader.split(' ')[1].substr(0,5));

    let tableData = [];
    tableData.push({
      'Статус': 'Зарегистрирован',
      'Автор': me.formPlayer.model.getModelWithId('itsm_form_incident_author').getTextValue(),
      'Дата': newDateHeader,
      'Пользователь':  me.formPlayer.model.getModelWithId('itsm_form_incident_author').getTextValue(),
      'Подразделение': me.formPlayer.model.getModelWithId('itsm_form_incident_authorDepartment').getTextValue(),
      'Информация': ''
    });

    let autoAssign = me.formPlayer.model.getModelWithId('itsm_form_incident_auto_assignment');
    if(autoAssign && autoAssign.getValue() && autoAssign.getValue()[0] == '1') {
      tableData.push({
        'Статус': 'На очереди',
        'Автор': me.formPlayer.model.getModelWithId('itsm_form_incident_author').getTextValue(),
        'Дата': newDateHeader,
        'Пользователь':  me.formPlayer.model.getModelWithId('itsm_form_incident_responsible').getTextValue(),
        'Подразделение': me.formPlayer.model.getModelWithId('itsm_form_incident_responsibleDepartment').getTextValue(),
        'Информация': 'Направлено: ' + me.formPlayer.model.getModelWithId('itsm_form_incident_responsible').getTextValue()
      });
    }

    let historyTable = UTILS.createTable(tableData);
    historyTable.addClass('uk-table uk-table-striped uk-table-responsive')
    let tbody = historyTable.find('tbody');
    let processes = processFilter(this.process);

    processes.forEach(proc => {
      let tr = $('<tr>');
      tbody.append(tr);

      AS.FORMS.ApiUtils.simpleAsyncGet('rest/api/workflow/work/get_completion_data?workID=' + proc.actionID)
      .then(work => AS.FORMS.ApiUtils.loadAsfData(work.result.dataUUID))
      .then(asfData => {
        let status = UTILS.getValue(asfData, 'itsm_form_incident_wcf_status');
        let department = UTILS.getValue(asfData, 'itsm_form_incident_wcf_responsibleDepartment').value || '';
        let department_inprogress = UTILS.getValue(asfData, 'itsm_form_incident_wcf_responsibleDepartment_inprogress').value || '';
        let user = UTILS.getValue(asfData, 'itsm_form_incident_wcf_responsible').value || '';
        let user_inprogress = UTILS.getValue(asfData, 'itsm_form_incident_wcf_responsible_inprogress').value || '';
        let info = '';

        let username = ([6,10,12].indexOf(+status.key) !== -1 ? 'Системный пользователь' : (status.key == 3 ? user_inprogress : user ? user : user_inprogress));
        if([91, 15, 16].includes(Number(status.key))) {
          username = UTILS.getValue(asfData, 'itsm_form_incident_wcf_approval_users').value || '';
        }
        department = status.key == 3 ? department_inprogress : department ? department : department_inprogress;

        switch (+status.key) {
          case 2:
          case 3:
            info = 'Направлено: ' + username + ',<br>' + department; break;
          case 4: info = 'Запрос: ' + UTILS.getValue(asfData, 'itsm_form_incident_wcf_return_to_author').value; break;
          case 11: info = 'Комментарий: ' + UTILS.getValue(asfData, 'itsm_form_incident_wcf_comment').value; break;
          case 7:
            info = 'Код решения: ' + UTILS.getValue(asfData, 'itsm_form_incident_wcf_decisiontype').value; + '<br>Описание: ' + UTILS.getValue(asfData, 'itsm_form_incident_wcf_decisiondescription').value;
            break;
          case 9:
            info = 'Поставщик:  ' + UTILS.getValue(asfData, 'itsm_form_incident_wcf_supplier').value;
            break;
          case 8: info = 'Дата: ' + UTILS.getValue(asfData, 'itsm_form_incident_wcf_equipment_date').value; break;
          case 10:
            info = 'Инициатор: ' + me.formPlayer.model.getModelWithId('itsm_form_incident_author').getTextValue();
            department = "" + me.formPlayer.model.getModelWithId('itsm_form_incident_authorDepartment').getTextValue();
            break;
          case 12:
            info = 'Инициатор: ' + username;
            department = "" + me.formPlayer.model.getModelWithId('itsm_form_incident_authorDepartment').getTextValue();
            break;
          case 6:
            info = 'Инициатор: ' + username;
            department = "" + me.formPlayer.model.getModelWithId('itsm_form_incident_authorDepartment').getTextValue();
            break;
          case 91: info = 'Описание: ' + UTILS.getValue(asfData, 'itsm_form_incident_wcf_approval_description').value; break;
          case 15:
          case 16:
            info = 'Комментарий: ' + UTILS.getValue(asfData, 'itsm_form_incident_wcf_approval_comment').value;
            break;
        }

        let oldDateN = new Date(""+proc.finished).toLocaleString('ru', {year: 'numeric', month: 'long', day: 'numeric'});
        let newDate = oldDateN.replace(/(?:([А-Я]{3}).+$)/i,'$1, ' + proc.finished.split(' ')[1].substr(0,5));
        let row = {
          'Статус': status.value,
          'Автор': UTILS.getValue(asfData, 'itsm_form_incident_wcf_current_user').value,
          'Дата': newDate,
          'Пользователь':  username,
          'Подразделение': department,
          'Информация': info
        };
        for (let key in row) tr.append($('<td>').html(row[key]));
      });
    });

    modalBody.append(historyTable);
    UIkit.modal(dialog).show();
    dialog.on('shown', () => Cons.hideLoader());
    dialog.on('hidden', () => {
      dialog.remove();
      this.formPlayer.panels.formPanel.removeClass('_lock');
    });
  }

  renderButtons(){
    let statusIncident = this.formPlayer.model.getModelWithId('itsm_form_incident_status');
    let resGroup = this.groups.find(i => i.groupCode == 'itsm_group_reassign_access');
    let rfcGroup = this.groups.find(i => i.groupCode == 'itsm_group_button_rfc');
    if(statusIncident) {
      statusIncident = statusIncident.getValue();
      if(resGroup && ['2', '3', '10'].indexOf(statusIncident[0]) !== -1) {
        createButton(this.formPlayer, 'Переназначить', 'itsm-button3', () => this.reassign());
      }
      if(Cons.getAppStore().statusesInProblem.indexOf(statusIncident[0]) !== -1) {
        createButton(this.formPlayer, 'В проблему', 'itsm-button5', () => this.inProblem());
      }
      if(rfcGroup && ['1', '10', '2', '3'].indexOf(statusIncident[0]) !== -1) {
        createButton(this.formPlayer, 'Запрос на изменение', 'itsm-button6', () => this.inRFC());
      }
    }

    //инициализация кнопки "Завершить"
    let processes = UTILS.getUserWork(this.process, AS.OPTIONS.currentUser.userid, 'ASSIGNMENT_ITEM');
    if(processes.length > 0) {
      createButton(this.formPlayer, 'Завершить', 'itsm-button2', () => {
        finishWork(this.formPlayer, processes, 'itsm_form_incident_wcf_new');
      }, true);
      this.workName.show()
      .text(cutText(processes[0].name))
      .attr('title', processes[0].name);
    }

    //инициализация кнопки "История"
    createButton(this.formPlayer, 'История', 'itsm-button4', () => this.showHistory());
  }

  init(refresh){
    if(refresh) {
      this.renderButtons();
    } else {
      AS.FORMS.ApiUtils.simpleAsyncGet("rest/api/person/auth?getGroups=true").then(res => {
        this.groups = res.groups;
        this.renderButtons();
        setTimeout(() => {
          this.formPlayer.model.hasChanges = false;
        }, 1000);
      });
    }
  }

}

class Problem {
  constructor(player, process) {
    this.formPlayer = player;
    this.workName = player.panels.workName;
    this.process = process;

    this.init();
  }

  changeRequest(){
    let m = [];
    m.push({from: 'itsm_form_problem_parent_incident', to: 'itsm_form_rfc_incidentes.incedent'});
    m.push({from: 'itsm_form_problem_servicelink', to: 'itsm_form_rfc_servicelink'});
    m.push({from: 'itsm_form_problem_confitemlink', to: 'itsm_form_rfc_ci'});
    m.push({from: 'CURRENT_DOCUMENTID', to: 'itsm_form_rfc_problemlink'});
    checkSavedForm(this.formPlayer, () => {
      createDocument(this.formPlayer, 'itsm_registry_rfc', m);
    });
  }

  renderButtons(){
    let processes = UTILS.getUserWork(this.process, AS.OPTIONS.currentUser.userid, 'ASSIGNMENT_ITEM');
    if(processes.length > 0) {
      createButton(this.formPlayer, 'Завершить', 'itsm-button2', () => {
        finishWork(this.formPlayer, processes, 'itsm_form_problem_wcf');
      }, true);
      this.workName.show()
      .text(cutText(processes[0].name))
      .attr('title', processes[0].name);
    }
    createButton(this.formPlayer,'Запрос на изменение', 'itsm-button3', () => this.changeRequest());
  }

  init(refresh){
    if(refresh) {
      this.renderButtons();
    } else {
      this.renderButtons();
      setTimeout(() => {
        this.formPlayer.model.hasChanges = false;
      }, 1000);
    }
  }
}

class Knowledgebase {
  constructor(player, process) {
    this.formPlayer = player;
    this.workName = player.panels.workName;
    this.meaning = player.meaning;
    this.process = process;

    this.init();
  }

  renderButtons(){
    let processes = UTILS.getUserWork(this.process, AS.OPTIONS.currentUser.userid, 'AGREEMENT_ITEM');
    if(processes.length > 0) {
      createButton(this.formPlayer, 'Согласование', 'itsm-button2', () => {
        agreementDoc(this.formPlayer, processes[0], 'Согласование');
      }, true);
      this.workName.show()
      .text(`Прошу согласовать - ${cutText(this.meaning)}`)
      .attr('title', `Прошу согласовать - ${this.meaning}`);
    }
  }

  init(refresh){
    if(refresh) {
      this.renderButtons();
    } else {
      this.renderButtons();
      setTimeout(() => {
        this.formPlayer.model.hasChanges = false;
      }, 1000);
    }
  }
}

class AccessOrders {
  constructor(player, process) {
    this.formPlayer = player;
    this.workName = player.panels.workName;
    this.meaning = player.meaning;
    this.process = process;

    this.init();
  }

  renderButtons(){
    let processes1 = UTILS.getUserWork(this.process, AS.OPTIONS.currentUser.userid, 'ASSIGNMENT_ITEM');
    let processes2 = UTILS.getUserWork(this.process, AS.OPTIONS.currentUser.userid, 'AGREEMENT_ITEM');
    //инициализация кнопки "Завершить"
    if(processes1.length > 0) {
      createButton(this.formPlayer, 'Завершить', 'itsm-button2', () => {
        finishWorkNotForm(this.formPlayer, processes1);
      }, true);
      this.workName.show()
      .text(cutText(processes1[0].name))
      .attr('title', processes1[0].name);
    }
    //инициализация кнопки "Согласования"
    if(processes2.length > 0) {
      createButton(this.formPlayer, 'Согласовать', 'itsm-button3', () => {
        agreementDoc(this.formPlayer, processes2[0], "Согласование");
      }, true);
      this.workName.show()
      .text(`Прошу согласовать - ${cutText(this.meaning)}`)
      .attr('title', `Прошу согласовать - ${this.meaning}`);
    }
  }

  init(refresh){
    if(refresh) {
      this.renderButtons();
    } else {
      this.renderButtons();
      setTimeout(() => {
        this.formPlayer.model.hasChanges = false;
      }, 1000);
    }
  }
}

class RFC {
  constructor(player, process) {
    this.formPlayer = player;
    this.workName = player.panels.workName;
    this.process = process;
    this.meaning = player.meaning;

    this.init();
  }

  inChanges(){
    let m = [];
    m.push({from: 'itsm_form_rfc_author', to: 'itsm_form_change_rfc_author'});
    m.push({from: 'itsm_form_rfc_system_admin', to: 'itsm_form_cr_system_admin'});
    m.push({from: 'itsm_form_rfc_problemlink', to: 'itsm_form_change_problemlink'});
    m.push({from: 'itsm_form_rfc_servicelink', to: 'itsm_form_change_servicelink'});
    m.push({from: 'itsm_form_rfc_description', to: 'itsm_form_change_description'});
    m.push({from: 'itsm_form_rfc_ci', to: 'itsm_form_change_ci'});
    m.push({from: 'itsm_form_rfc_incidents', to: 'itsm_form_change_incidents'});
    m.push({from: 'itsm_form_rfc_problems', to: 'itsm_form_change_problems'});
    m.push({from: 'CURRENT_DOCUMENTID', to: 'itsm_form_change_rfclink'});
    checkSavedForm(this.formPlayer, () => {
      createDocument(this.formPlayer, 'itsm_registry_changes', m);
    });
  }

  renderButtons(){
    //инициализация кнопки "Завершить"
    let assignProc = UTILS.getUserWork(this.process, AS.OPTIONS.currentUser.userid, 'ASSIGNMENT_ITEM');
    if(assignProc.length > 0) {
      if(assignProc[0].code === 'itsm_route_rfc_done') {
        createButton(this.formPlayer, 'Завершить', 'itsm-button2', () => {
          finishWorkNotForm(this.formPlayer, assignProc);
        }, true);
      } else {
        createButton(this.formPlayer, 'Завершить', 'itsm-button2', () => {
          finishWork(this.formPlayer, assignProc, 'itsm_form_rfc_wcf');
        }, true);
      }
      this.workName.show()
      .text(cutText(assignProc[0].name))
      .attr('title', assignProc[0].name);
    }

    //инициализация кнопки "Согласовать"
    let agreeProc = UTILS.getUserWork(this.process, AS.OPTIONS.currentUser.userid, 'AGREEMENT_ITEM');
    if(agreeProc.length > 0) {
      createButton(this.formPlayer, 'Согласовать', 'itsm-button3', () => {
        agreementDoc(this.formPlayer, agreeProc[0], 'Согласование');
      }, true);
      this.workName.show()
      .text(`Прошу согласовать - ${cutText(this.meaning)}`)
      .attr('title', `Прошу согласовать - ${this.meaning}`);
    }

    //инициализация кнопки "Ознакомиться"
    let acquaProc = UTILS.getUserWork(this.process, AS.OPTIONS.currentUser.userid, 'ACQUAINTANCE_ITEM');
    if(acquaProc.length > 0) {
      createButton(this.formPlayer, 'Ознакомиться', 'itsm-button4', () => {
        agreementDoc(this.formPlayer, acquaProc[0], 'Ознакомление');
      }, true);
      this.workName.show()
      .text(`Ознакомиться - ${cutText(this.meaning)}`)
      .attr('title', `Ознакомиться - ${this.meaning}`);
    }

    createButton(this.formPlayer, 'В изменение', 'itsm-button5', () => this.inChanges());
  }

  init(refresh){
    if(refresh) {
      this.renderButtons();
    } else {
      this.renderButtons();
      setTimeout(() => {
        this.formPlayer.model.hasChanges = false;
      }, 1000);
    }
  }
}

class Changes {
  constructor(player, process) {
    this.formPlayer = player;
    this.workName = player.panels.workName;
    this.meaning = player.meaning;
    this.process = process;

    this.init();
  }

  renderButtons(){
    //инициализация кнопки "Завершить"
    let assignProc = UTILS.getUserWork(this.process, AS.OPTIONS.currentUser.userid, 'ASSIGNMENT_ITEM');
    if(assignProc.length > 0) {
      createButton(this.formPlayer, 'Завершить', 'itsm-button2', () => {
        finishWork(this.formPlayer, assignProc, 'itsm_form_cr_wcf');
      }, true);
      this.workName.show()
      .text(cutText(assignProc[0].name))
      .attr('title', assignProc[0].name);
    }
    //инициализация кнопки "Ознакомиться"
    let acquaProc = UTILS.getUserWork(this.process, AS.OPTIONS.currentUser.userid, 'ACQUAINTANCE_ITEM');
    if(acquaProc.length > 0) {
      createButton(this.formPlayer, 'Ознакомиться', 'itsm-button3', () => {
        agreementDoc(this.formPlayer, acquaProc[0], 'Ознакомление');
      }, true);
      this.workName.show()
      .text(`Ознакомиться - ${cutText(this.meaning)}`)
      .attr('title', `Ознакомиться - ${this.meaning}`);
    }
  }

  init(refresh){
    if(refresh) {
      this.renderButtons();
    } else {
      this.renderButtons();
      setTimeout(() => {
        this.formPlayer.model.hasChanges = false;
      }, 1000);
    }
  }
}

class ButtonsActions {
  constructor(player) {
    this.player = player;
    this.documentID = player.documentID;
    this.panelButtons = player.panels.panelButtons;
    this.editReadFormButton = player.editReadFormButton;
    this.workName = player.panels.workName;
    this.registryCode = player.registryCode;
    this.regInfo = null;
    this.actionButtons = null;

    this.init();
  }

  refresh(init){
    this.panelButtons.empty();
    this.workName.empty().attr('title', null).hide();

    AS.FORMS.ApiUtils.simpleAsyncGet("rest/api/docflow/document_actions?documentID=" + this.player.documentID).then(res => {
      res = res.filter(x => x.operation == 'RUN')[0];
      // Если запись не отправлена, создаем кнопку с активацией документа
      if(res) {
        if(!this.regInfo) {
          //в случае если нет информации по реестру
          createButton(this.player, res.label, 'itsm-button1', () => activateDocument(this.player), true);
        } else if(this.regInfo.rights.indexOf('rr_create') !== -1) {
          //Если есть права на создание
          createButton(this.player, res.label, 'itsm-button1', () => activateDocument(this.player), true);
        }
        if(this.registryCode == "itsm_registry_incidents") createEvent('opened', this.player.documentID);
      } else {
        // иначе инициализируем кнопки
        AS.FORMS.ApiUtils.simpleAsyncGet("rest/api/workflow/get_execution_process?documentID=" + this.player.documentID).then(process => {
          let acqAgrProc = filterAgrProcesses(process);
          if(acqAgrProc.length > 0) {
            this.player.saveButton.hide();
            this.player.saveButtonMobile.hide();
            this.player.editReadFormButton.hide();
            this.player.editReadFormButtonMobile.hide();
            UIkit.modal.alert('Документ заблокирован, т.к. находится на согласовании/ознакомлении');
          }

          switch (this.registryCode) {
            case "itsm_registry_incidents":
              if(init) {
                this.actionButtons = new Incident(this.player, process);
                createEvent('opened', this.player.documentID);
              } else {
                this.actionButtons.init(true);
              }
              break;
            case "itsm_registry_problems":
              if(init) this.actionButtons = new Problem(this.player, process);
              else this.actionButtons.init(true);
              break;
            case "itsm_registry_knowledgebase":
              if(init) this.actionButtons = new Knowledgebase(this.player, process);
              else this.actionButtons.init(true);
              break;
            case "itsm_registry_access_orders":
              if(init) this.actionButtons = new AccessOrders(this.player, process);
              else this.actionButtons.init(true);
              break;
            case "itsm_registry_rfc":
              if(init) this.actionButtons = new RFC(this.player, process);
              else this.actionButtons.init(true);
              break;
            case "itsm_registry_changes":
              if(init) this.actionButtons = new Changes(this.player, process);
              else this.actionButtons.init(true);
              break;
          }
        });
      }
    });
  }

  init(){
    this.regInfo = getRegInfo(this.registryCode);
    //Если нет прав на редактирование
    if(this.regInfo && this.regInfo.rights.indexOf('rr_edit') == -1) {
      this.editReadFormButton.hide();
    }
    this.player.buttonClass = this;
    this.refresh(true);
  }
}

class ARMDocumentButtons {
  constructor(player) {
    this.player = player;
    this.dataUUID = player.model.asfDataId;
    this.editable = player.view.editable;
    this.workName = player.panels.workName;
    this.panelButtons = player.panels.panelButtons;
    this.panelActions = player.panels.panelActions;
    this.mobileMenu = player.panels.mobileMenu;
    this.formPanel = player.panels.formPanel;
    this.editReadFormButton = null;
    this.saveButton = null;
    this.printButton = null;

    this.init();
  }

  changcheButtons(){
    if(this.editable) {
      this.editReadFormButton.addClass('show-button').removeClass('button-edit').attr('title', 'Режим просмотра');
      this.editReadFormButtonMobile.addClass('show-button').removeClass('button-edit');
      this.printButton.hide();
      this.printButtonMobile.hide();
    } else {
      this.editReadFormButton.addClass('button-edit').removeClass('show-button').attr('title', 'Режим редактирования');
      this.editReadFormButtonMobile.addClass('button-edit').removeClass('show-button');
      this.printButton.show();
      this.printButtonMobile.show();
    }
  }

  renderPanelActions(){
    let PAHeader = $('<div>', {class: 'arm-formname'}).text(this.player.formName);
    let panelBodyRight = $('<div>', {class: 'arm-right-panel-actions'});
    let oneMobilePanel = $('<div>', {class: 'arm-mobile-one-panel'});
    this.menuIcon = $('<div>', {class: 'menu__icon'}).append('<span>');

    this.saveButton = $('<button class="uk-button margined uk-button-default save-button" title="Сохранить">Сохранить</button>');
    this.printButton = $('<button class="uk-button margined uk-button-default button-print-form" title="Печатное представление">');
    this.editReadFormButton = $('<button class="uk-button margined uk-button-default">');

    this.saveButtonMobile = $('<button class="uk-button margined uk-button-default save-button-mobile">');
    this.printButtonMobile = $('<button class="uk-button margined uk-button-default button-print-form mobile">');
    this.editReadFormButtonMobile = $('<button class="uk-button margined uk-button-default mobile">');

    oneMobilePanel.append(this.printButtonMobile).append(this.editReadFormButtonMobile).append(this.saveButtonMobile);
    panelBodyRight.append(this.printButton).append(this.editReadFormButton).append(this.saveButton);
    this.panelActions.append(this.menuIcon).append(PAHeader).append(panelBodyRight);
    this.mobileMenu.append(oneMobilePanel).append($('<div>', {class: 'arm-mobile-two-panel'}));

    this.saveButtonMobile.append(`<svg viewBox="0 0 32 32">
    <style type="text/css">
    .st0{fill:none;stroke:#acbad8;stroke-width:2;stroke-linecap:round;stroke-linejoin:round;stroke-miterlimit:10;}
    </style>
    <rect class="st0" height="11" width="14" x="9" y="17"></rect>
    <line class="st0" x1="18" x2="18" y1="7" y2="9"></line>
    <path class="st0" d="M21,4v6c0,1.1-0.9,2-2,2h-8c-1.1,0-2-0.9-2-2l0-6H4v24h24V9l-5-5H9"></path>
    </svg>`);

    this.player.editReadFormButton = this.editReadFormButton;
    this.player.editReadFormButtonMobile = this.editReadFormButtonMobile;
    this.player.saveButton = this.saveButton;
    this.player.saveButtonMobile = this.saveButtonMobile;
    this.changcheButtons();
  }

  hideShowMobileMenu(){
    this.menuIcon.toggleClass('_active');
    this.mobileMenu.toggleClass('_active');
    this.formPanel.toggleClass('_lock');
  }

  editRead(e){
    e.preventDefault();
    e.target.blur();
    this.editable = !this.editable;
    this.player.view.setEditable(this.editable);
    this.changcheButtons();
  }

  save(e){
    e.preventDefault();
    e.target.blur();
    saveFormData(this.player);
  }

  print(e){
    e.preventDefault();
    e.target.blur();
    printForm(this.player.view.container[0]);
  }

  addListeners(){
    this.editReadFormButton.on('click', e => {
      this.editRead(e);
    });
    this.editReadFormButtonMobile.on('click', e => {
      this.editRead(e);
      this.hideShowMobileMenu();
    });

    this.saveButton.on('click', e => {
      this.save(e);
    });
    this.saveButtonMobile.on('click', e => {
      this.save(e);
      this.hideShowMobileMenu();
    });

    this.printButton.on('click', e => {
      this.print(e);
    });
    this.printButtonMobile.on('click', e => {
      this.print(e);
      this.hideShowMobileMenu();
    });

    this.menuIcon.on('click', e => {
      this.hideShowMobileMenu();
    });

    this.mobileMenu.on('hideShowMenu', e => {
      this.hideShowMobileMenu();
    });
  }

  init(){
    this.renderPanelActions();
    this.addListeners();
    this.workName.text(this.player.documentName);
    new ButtonsActions(this.player);
  }
}

class ARMDocument {
  constructor(panel, param) {
    this.documentName = param.meaning || param.formName;
    this.meaning = param.meaning;
    this.formName = param.formName;
    this.dataUUID = param.dataUUID;
    this.editable = param.editable || false;
    this.viewCode = param.viewCode || false;
    this.registryCode = param.registryCode;

    this.panel = panel;
    this.window = null;
    this.state = null;
    this.player = null;

    this.init();
  }

  initSynergyPlayer(handler){
    this.player = AS.FORMS.createPlayer();
    this.player.view.setEditable(this.editable);
    this.player.showFormData(null, null, this.dataUUID);
    if(this.viewCode) this.player.model.showView(this.viewCode);

    this.player.registryCode = this.registryCode;
    this.player.formName = this.formName;
    this.player.documentName = this.documentName;
    this.player.meaning = this.meaning;
    this.player.dataUUID = this.dataUUID;

    AS.FORMS.ApiUtils.getDocumentIdentifier(this.player.dataUUID).then(documentID => {
      this.player.documentID = documentID;
      handler(documentID);
    });
  }

  renderWindow(){
    this.window = $('<div>', {class: 'arm-window-document'}).attr('data-uuid', this.dataUUID);
    let header = $('<div>', {class: 'arm-header uk-modal-header'});
    let body = $('<div>', {class: 'arm-body'});
    let leftPanel = $('<div>', {class: 'arm-left-panel'});
    let content = $('<div>', {class: 'arm-content'});
    let formPanel = $('<div>', {class: 'arm-form-panel'})
    let buttonsHeader = $('<div>', {class: 'arm-header-buttons'});
    let minButton = $('<span class="arm-min-button uk-margin-small-right" uk-icon="chevron-down"></span>');
    let closeButton = $('<span class="arm-close-button" uk-icon="close"></span>');
    let panelActions = $('<div>', {class: 'arm-form-panel-actions'});
    let panelButtons = $('<div>', {class: 'arm-form-panel-buttons'});
    let workName = $('<div>', {class: 'arm-work-name'});
    let mobileMenu = $('<div>', {class: 'arm-menu-mobile'});

    this.player.panels = {
      workName,
      panelButtons,
      panelActions,
      mobileMenu,
      formPanel,
      documentWindow: this.window,
      documentPanel: this.panel
    };

    formPanel.append(this.player.view.container)
    leftPanel.append(workName).append(panelButtons);
    content.append(panelActions).append(formPanel);
    buttonsHeader.append(minButton).append(closeButton);
    header.append(`<h3 style="overflow: hidden; white-space: nowrap; width: calc(100% - 30px);">${this.documentName}</h3>`).append(buttonsHeader);
    body.append(leftPanel).append(content);
    this.window.append(header).append(body).append(mobileMenu);
    $('body').append(this.window);

    minButton.on('click', e => this.hideShowDocument());
    closeButton.on('click', e => {
      checkSavedForm(this.player, () => {
        closeDocument(this.player, true);
      });
    });

    new ARMDocumentButtons(this.player);
  }

  openDocument(){
    this.window.fadeIn(200);
    this.state = 'open';
    $('.arm-window-document').attr('arm-active-window', null);
    this.window.attr('arm-active-window', true).attr('state', this.state);
    $('.footer-document').removeClass('arm-active-panel');
    this.panel.addClass('arm-active-panel');
  }

  hideShowDocument(){
    if(!this.window.attr('arm-active-window')) {
      this.openDocument();
    } else {
      if(this.state == 'open') {
        this.state = 'hide';
        this.panel.removeClass('arm-active-panel');
        this.window.fadeOut(200).attr('arm-active-window', null).attr('state', this.state);

        let aw = $('.arm-window-document[state="open"]');
        if(aw.length) {
          aw = $(aw[0]);
          aw.attr('arm-active-window', true);
          let uuid = aw.attr('data-uuid');
          $(`.footer-document[data-uuid="${uuid}"]`).addClass('arm-active-panel');
        }
      } else {
        this.openDocument();
      }
    }
  }

  init(){
    this.initSynergyPlayer(documentID => {
      if(!this.window) this.renderWindow();
      this.openDocument();

      this.panel.on('click', e => {
        this.hideShowDocument();
      });

      this.window.on('show-document', e => {
        this.hideShowDocument();
      });

      this.window.on('hide-window', e => {
        this.window.hide();
        this.state = 'hide';
        this.window.attr('arm-active-window', null).attr('state', this.state);
        this.panel.removeClass('arm-active-panel');
      });
    });
  }
}

class ARMFooter {
  constructor() {
    this.footer = null;
    this.init();
  }

  renderDocToFooter(param){
    AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/formPlayer/getDocMeaningContent?asfDataUUID=${param.dataUUID}`, null, 'text')
    .then(meaning => {
      let panel = $('<div>', {class: 'footer-document'})
      .attr('data-uuid', param.dataUUID)
      .attr('title', meaning || param.formName)
      .html(`<span uk-icon="icon: file-text"></span> ${meaning || param.formName}`);

      this.footer.append(panel);
      param.meaning = meaning;
      new ARMDocument(panel, param);
    });
  }

  openDocument(param){
    let openDocsWindow = Cons.getAppStore().openDocsWindow || [];
    let doc = openDocsWindow.find(x => x.dataUUID === param.dataUUID);
    if(!doc) {
      this.renderDocToFooter(param);
      openDocsWindow.push(param);
      Cons.setAppStore({openDocsWindow: openDocsWindow});
    } else {
      $(`.arm-window-document[data-uuid="${param.dataUUID}"]`).trigger('show-document');
    }
  }

  initListener(){
    if(Cons.getAppStore().open_document_listener) return;

    if(Cons.getCurrentPage().code == 'app') {
      addListener('custom_open_document', 'root-panel', event => this.openDocument(event));
      Cons.setAppStore({open_document_listener: true});
    };

    $('#root-panel').off().on('custom_open_document', e => {
      if(e.hasOwnProperty('eventParam')) this.openDocument(e.eventParam);
    });
  }

  renderPanel() {
    if(Cons.getCurrentPage().code == 'auth_page') return;

    if($('.footer-panel').length) {
      this.footer = $('.footer-panel');
    } else {
      this.footer = $('<div>', {class: 'footer-panel'});
      let homeButton = $('<span class="uk-margin-small-right arm-home-button" uk-icon="icon: home; ratio: 1.5"></span>');

      homeButton.on('click', e => $('.arm-window-document').trigger('hide-window'));
      this.footer.append(homeButton);
    }

    $('#root-panel').append(this.footer);
  }

  init(){
    this.renderPanel();
    this.initListener();
  }
}

new ARMFooter();
