const getDefaultIcon = () => $('<span uk-icon="icon: folder; ratio: 1.2;" class="itsm-nav-icon uk-margin-small-left" style="height: auto; color: #1E87F0;">');

const getRegistrySettings = () => {
  return new Promise(resolve => {
    AS.FORMS.ApiUtils.simpleAsyncGet('rest/api/registry/list').then(res => {
      const itsmGroup = res.find(x => x.regGroupName == "ITSM").consistOf;
      resolve(itsmGroup.find(x => x.regGroupName == "Настройки").consistOf);
    })
  });
}

const hidePanel = () => {
  $('.itsm-menu-desktop .itsm-nav').addClass('collapsed');
  $('#app-panel').addClass('collapsed');
  $('#left-panel').addClass('collapsed');
  $('.itsm-nav-link').addClass('collapsed');

  if($('.itsm-nav .uk-parent.uk-open .itsm-nav-link-active').length) {
    $('.itsm-nav .uk-parent.uk-open').addClass('itsm-nav-link-active');
  }

  $('.itsm-nav .uk-parent.uk-open .uk-nav-sub').attr('hidden', true);
  $('.itsm-nav .uk-parent.uk-open .uk-nav-sub').hide();
  $('.itsm-nav-link-text').hide();
}

const showPanel = () => {
  $('.itsm-menu-desktop .itsm-nav').removeClass('collapsed');
  $('#app-panel').removeClass('collapsed');
  $('#left-panel').removeClass('collapsed');
  $('.itsm-nav-link').removeClass('collapsed');
  $('.itsm-nav .uk-parent.uk-open').removeClass('itsm-nav-link-active');
  $('.itsm-nav .uk-parent.uk-open .uk-nav-sub').attr('hidden', null);
  $('.itsm-nav .uk-parent .uk-nav-sub').show();
  $('.itsm-nav-link-text').fadeIn(600);
}

const createButtonCollapse = () => {
  let panelStatus = 'show';
  let button = $('<div>', {class: 'collapse_button'});
  button.append('<span>Свернуть панель</span>');

  button.on('click', e => {
    if(panelStatus == 'show') {
      hidePanel();
      button.addClass('collapsed');
      panelStatus = 'hide';
    } else {
      showPanel();
      button.removeClass('collapsed');
      panelStatus = 'show';
    }
  });

  $('#left-panel').append(button);
}

pageHandler('settings_page', () => {
  let pages = cookie.getJson('pages');
  if(pages) {
  	pages.currentPage = 'settings_page';
  	cookie.setJson('pages', pages);
  }

  const reglistPanel = $('#panel-registry-list');
  const ulNav = $('<ul>', {class: 'uk-nav uk-nav-parent-icon itsm-nav'});
  ulNav.attr('uk-nav', '');
  reglistPanel.append(ulNav);

  let registrySettings;

  getRegistrySettings().then(registries => {
    Cons.setAppStore({registrySettings: registries});
    registrySettings = registries;
    registries.forEach(item => {
      let li = $(`<li title="${item.registryName}">`);
      let a = $('<a>').addClass('itsm-nav-link uk-link-text');
      a.append(getDefaultIcon());
      a.append(`<span class="itsm-nav-link-text">${item.registryName}</span>`);
      a.attr('registrycode', item.registryCode);
      li.append(a);
      ulNav.append(li);

      a.on('click', function() {
        if(ulNav.hasClass('collapsed')) {
          $('.itsm-nav .uk-parent.uk-open .uk-nav-sub').attr('hidden', true);
          $('.itsm-nav .uk-parent.uk-open .uk-nav-sub').hide();
        }

        if(a.attr('click')) return;
        a.attr('click', true);
        setTimeout(() => {
          a.attr('click', null);
        }, 2000);

        if($(document).width() < 769) UIkit.offcanvas('.itsm-mobile-menu-left').hide();

        Cons.showLoader();

        $('.itsm-nav-link').removeClass("itsm-nav-link-active");
        $('.itsm-nav .uk-parent').removeClass("itsm-nav-link-active");
        $(`[registrycode="${item.registryCode}"]`).addClass("itsm-nav-link-active");

        let event = {
          type: 'change_itsm_menu',
          registryCode: item.registryCode
        };
        fire(event, 'panel-registry-list');

        setTimeout(() => {
          Cons.hideLoader();
        }, 500);

        //Проверяем права на создание, для кнопки "Создать"
        if(item.rights.indexOf('rr_create') === -1) {
          fire({type: 'set_hidden', hidden: true}, 'button-create');
        } else {
          fire({type: 'set_hidden', hidden: false}, 'button-create');
        }
      });

      a.on('dblclick', e => {
        e.preventDefault();
        return;
      });
    });
  });

  $('#button-create').off();
  $('#button-create').on('click', () => {
    Cons.showLoader();
    try {
      let registryCode =  $('.itsm-nav-link-active').attr('registrycode');
      let regInfo = registrySettings.find(x => x.registryCode == registryCode);
      AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/create_doc?registryCode=${registryCode}`)
      .then(res => {
        if(res.errorCode != '0') throw new Error(res.errorMessage);
        Cons.hideLoader();
        Cons.setAppStore({dataRow: res});
        let eventOpenDoc = {
          dataUUID: res.dataUUID,
          registryCode: registryCode,
          registryName: regInfo.registryName,
          formName: regInfo.registryName,
          editable: true
        };
        $('#root-panel').trigger({type: 'custom_open_document', eventParam: eventOpenDoc});
      });
    } catch (error) {
      itsmShowMessage({message: "Произошла ошибка при создании записи реестра", type: 'error'});
      console.log(error);
      Cons.hideLoader();
    }
  });

  createButtonCollapse();
});
