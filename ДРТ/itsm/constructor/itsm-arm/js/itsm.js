/*
 * param: {
 * message: "msg text",
 * type: ['success', 'error', 'warning'] || default: primary,
 * title: "title text" || default '';
 * }
*/
window.itsmShowMessage = function(param) {
  let options = {
    message: `<legend class="uk-legend">${param.title || ''}</legend>${param.message}`,
    status: param.type || 'primary',
    pos: 'bottom-right',
    timeout: 5000
  };
  if(param.type && param.type == 'error') options.status = 'danger';
  UIkit.notification(options);
}

Array.prototype.uniq = function() {
  return this.filter(function(v, i, a){ return i == a.indexOf(v) });
}

let cookie = {
  get: function(name) {
    let matches = document.cookie.match(new RegExp(
      "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
    ));
    return matches ? decodeURIComponent(matches[1]) : undefined;
  },
  getJson: function(name) {
    let c = this.get(name);
    return c ? JSON.parse(c) : undefined;
  },
  set: function(name, value, options) {
    options = options || {};
    let expires = options.expires;
    if (typeof expires == "number" && expires) {
      let d = new Date();
      d.setTime(d.getTime() + expires * 1000);
      expires = options.expires = d;
    }
    if (expires && expires.toUTCString) {
      options.expires = expires.toUTCString();
    }
    value = encodeURIComponent(value);
    let updatedCookie = name + "=" + value;
    for (let propName in options) {
      updatedCookie += "; " + propName;
      if (options[propName] !== true) updatedCookie += "=" + options[propName];
    }
    document.cookie = updatedCookie;
  },
  setJson: function(name, value, options) {
    this.set(name, JSON.stringify(value), options);
  },
  delete: function(name) {
    document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
  }
};

let UTILS = {
  createField: function(fieldData) {
    let field = {};
    for (let key in fieldData) field[key] = fieldData[key];
    return field;
  },
  getValue: function(data, cmpID) {
    data = data.data ? data.data : data;
    for(let i = 0; i < data.length; i++)
    if (data[i].id === cmpID) return data[i];
    return null;
  },
  setValue: function(asfData, cmpID, data) {
    let field = this.getValue(asfData, cmpID);
    if(field) {
      for (let key in data) {
        if(key === 'id' || key === 'type') continue;
        field[key] = data[key];
      }
      return field;
    } else {
      asfData = asfData.data ? asfData.data : asfData;
      field = this.createField(data);
      field.id = cmpID;
      asfData.push(field);
      return field;
    }
  },
  createTable: function(data) {
  	let container = $('<div>');
    let table = $('<table>', {class: 'itsm-custom-table'});
    let thead = $('<thead>');
    let body = $('<tbody>');
    let tr = $('<tr>');

    for (let key in data[0])
    tr.append($('<th>').html(key));

    data.forEach(row => {
      let tr = $('<tr>');
      for (let key in row)
      tr.append($('<td>').html(row[key]));
      body.append(tr);
    });

    thead.append(tr);
    table.append(thead).append(body);
    container.append(table);
    return container;
  }
};

UTILS.getUserWork = function(processes, userID, typeID) {
  let result = [];
  function search(p) {
    p.forEach(function(process) {
      if (process.responsibleUserID == userID && !process.finished && process.typeID == typeID) {
        result.push(process);
      }
      if (process.subProcesses.length > 0) search(process.subProcesses);
    });
  }
  search(processes);
  return result;
};

const cookieUserExpires = 30; //30 минут сохранять сессию пользовател

AS.SERVICES.showWaitWindow = Cons.showLoader;
AS.SERVICES.hideWaitWindow = Cons.hideLoader;
AS.SERVICES.showErrorMessage = str => itsmShowMessage({message: str, type: 'error'});
window.showErrorMessage = str => itsmShowMessage({message: str, type: 'error'});
window.showMessage = str => itsmShowMessage({message: str});
window.UTILS = UTILS;
window.cookie = cookie;

const closeAfterActivation = async (code) => {
  let res = await fetch(`${window.origin}/itsm/rest/registry/closeAfterActivation?registryCode=${code}`);
  res = await res.json();
  if(res.errorCode === 0) return res.result;
  return false;
}

const getRegInfo = code => {
  let result;
  let rr = Cons.getAppStore().registries;
  rr.forEach(item => {
    if (item.children) {
      item.children.forEach(child => {
        if (child.code == code) result = child;
      })
    } else {
      if (item.code == code) result = item;
    }
  });
  return result;
}

const concatFilterRights = filters => filters.map(x => x.rights).reduce((r, x) => r.concat(x)).uniq();
const getRegistryFilters = async (registryCode) => {
  return new Promise(async resolve => {
    try {
      AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/filters?registryCode=${registryCode}&type=service&getIcon=false`, res => {
        if(res.errorCode && res.errorCode != 0) {
          resolve([]);
        } else {
          resolve(res);
        }
      }, null, null, e => {
        resolve([]);
      });
    } catch (e) {
      console.log('getRegistryFilters error', e);
      resolve([]);
    }
  })
}
const searchRegistryRights = async (list, code) => {
  let registryRights = [];
  let concatRights = [];
  function s(data) {
    data.forEach(item => {
      if (item.consistOf && item.consistOf.length > 0) {
        s(item.consistOf);
      } else if (item.registryCode == code) {
        registryRights = item.rights;
      }
    });
  }
  s(list);
  let filters = await getRegistryFilters(code);
  filters = filters.filter(item => item.code)
  .map(item => {
    return {name: item.name, id: item.id, code: item.code, rights: item.rights, registryCode: code}
  });
  if(filters.length > 0) concatRights = concatFilterRights(filters);
  registryRights = registryRights.uniq();
  concatRights = registryRights.concat(concatRights).uniq();
  return {registryRights, filters, concatRights};
}

const getTableBlockIndex = tableData => {
  let id = tableData[tableData.length - 1].id;
  return Number(id.slice(id.indexOf('-b') + 2));
}

const parseRegTableResult = regList => {
  let children = regList.filter(x => x.number.indexOf('.') !== -1);
  let parent = regList.filter(x => x.number.indexOf('.') === -1);
  parent.forEach(p => {
    if(p.children) p.children = children.filter(x => p.number == x.number.slice(0, x.number.indexOf('.')));
  });
  return parent;
}

const parseRegTable = table => {
  let reg = []
  let rows = [
    {id: 'itsm_form_arm_settings_rg_number', name: 'number'},
    {id: 'itsm_form_arm_settings_rg_name', name: 'name'},
    {id: 'itsm_form_arm_settings_rg_parent', name: 'children'},
    {id: 'itsm_form_arm_settings_rg_code', name: 'code'},
    {id: 'itsm_form_arm_settings_form_name', name: 'formName'},
    {id: 'itsm_form_arm_settings_rg_icon', name: 'icon'},
    {id: 'itsm_form_arm_settings_filter_ignore', name: 'filter_ignore'}
  ];
  for(let i = 1; i <= getTableBlockIndex(table); i++) {
    let param = {};
    rows.forEach(row => {
      let tmp = table.find(x => x.id == `${row.id}-b${i}`);
      if(row.id === 'itsm_form_arm_settings_filter_ignore') {
        if(tmp.value) param[row.name] = tmp.value.split(',');
      } else {
        if(tmp.type == 'check') {
          if(tmp.values[0]) param[row.name] = [];
        } else {
          if(tmp.value) param[row.name] = tmp.value;
        }
      }
    });
    reg.push(param);
  }
  return parseRegTableResult(reg);
}

const getArmSettings = cb => {
  try {
    let tmpReg = Cons.getAppStore().registries;
    if(tmpReg && tmpReg.length) {
      cb();
    } else {
      Cons.showLoader();
      const api = AS.FORMS.ApiUtils;
      api.simpleAsyncGet('rest/api/registry/data_ext?registryCode=itsm_registry_arm_settings&pageNumber=0&countInPart=1&loadData=false&sortDesc=true')
      .then(settings => {
        if(settings.count == 0) {
          console.log('Не найдено настроек для АРМ');
          Cons.hideLoader();
        } else {
          api.loadAsfData(settings.data[0].dataUUID).then(settings => {
            const table = settings.data.find(x => x.id == 'itsm_arm_table_registry').data;
            const statusesInProblem = UTILS.getValue(settings, 'itsm_form_arm_settings_status');
            const settingsWork = UTILS.getValue(settings, 'itsm_form_arm_settings_work');
            let img = UTILS.getValue(settings, 'itsm_form_arm_settings_logo');
            let registries = parseRegTable(table);

            let workflow = true;
            if(settingsWork.key == 0) workflow = false;
            Cons.setAppStore({workflow: workflow});

            api.simpleAsyncGet('rest/api/registry/info?code=custom_dashboard_registry').then(infoDashboards => {
              Cons.setAppStore({infoDashboards: infoDashboards});
            });

            if(img && img.hasOwnProperty('key')) {
              fetch(`${window.origin}/itsm/rest/image?identifier=${img.key}`)
              .then(res => res.blob())
              .then(img => {
                img = URL.createObjectURL(img);
                Cons.setAppStore({logoImg: img});
              });
            }

            if(statusesInProblem && statusesInProblem.hasOwnProperty('values')) {
              Cons.setAppStore({statusesInProblem: statusesInProblem.values});
            } else {
              Cons.setAppStore({statusesInProblem: []});
            }

            api.simpleAsyncGet('rest/api/registry/list').then(userRegistryList => {
              registries = registries.filter(async (item, index) => {
                if(item.children) {
                  item.children = item.children.filter(async (child, indexChild) => {
                    let {registryRights, filters, concatRights} = await searchRegistryRights(userRegistryList, child.code);
                    child.rights = registryRights || [];
                    child.filters = filters;
                    if(concatRights.length > 0 && concatRights.indexOf('rr_list') !== -1) {
                      child.closeAfterActivation = await closeAfterActivation(child.code)
                      return child;
                    } else {
                      item.children.splice(indexChild, 1);
                    }
                  });
                  if(item.children.length > 0) {
                    return item;
                  } else {
                    registries.splice(index, 1);
                  }
                } else {
                  let {registryRights, filters, concatRights} = await searchRegistryRights(userRegistryList, item.code);
                  item.rights = registryRights;
                  item.filters = filters;
                  if(concatRights.length > 0 && concatRights.indexOf('rr_list') !== -1) {
                    item.closeAfterActivation = await closeAfterActivation(item.code);
                    return item;
                  } else {
                    registries.splice(index, 1);
                  }
                }
              });
              setTimeout(() => {
                Cons.setAppStore({registries: registries});
                Cons.hideLoader();
                cb();
              }, 500);
            });
          });
        }
      });
    }
  } catch (e) {
    console.log('getArmSettings error', e);
    Cons.hideLoader();
  }
}

const getFullName = person => {
  const {firstname, lastname, patronymic} = person;
  const fio = lastname.substr(0, 1).toUpperCase() + lastname.substr(1) + ' ' + firstname.substr(0, 1).toUpperCase() + '.';
  return patronymic ? fio + ' ' + patronymic.substr(0, 1).toUpperCase() + '.' : fio;
}

const createEvent = (e, documentID) => {
  try {
    let event = {"api_event": `event.docflow.document.${e}`, "documentId": documentID, "userId": AS.OPTIONS.currentUser.userid};
    let data = {
      "eventMsg": JSON.stringify(event),
      "eventName": "event.docflow.document"
    }
    AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/events/create", res => {
      if(res.errorCode != '0') throw new Error(res.errorMessage);
      if(e == 'opened') {
        let openDocs = Cons.getAppStore().openDocs;
        if(openDocs && openDocs.length > 0) {
          openDocs.push(documentID);
        } else {
          openDocs = [documentID]
        }
        Cons.setAppStore({openDocs: openDocs});
      }
    }, null, data, "application/x-www-form-urlencoded; charset=UTF-8", err => {
      console.log(err);
    });
  } catch (e) {
    console.log(e);
  }
}

window.hideShowButtonCreate = regCode => {
  let regInfo = getRegInfo(regCode);
  //Проверяем права на создание, для кнопки "Создать"
  if(regInfo.rights.indexOf('rr_create') === -1) {
    $('#button-create').hide();
  } else {
    $('#button-create').show();
  }
}

let itsmUser = cookie.getJson('itsm-arm-user');
if(itsmUser && itsmUser.data.person && itsmUser.data.person.access_token) {

  AS.apiAuth.setCredentials('$key', itsmUser.data.person.access_token);
  cookie.setJson('itsm-arm-user', itsmUser, {expires: cookieUserExpires*60});
  Cons.setAppStore({itsmUser: itsmUser});
  AS.OPTIONS.login = '$key';
  AS.OPTIONS.password = itsmUser.data.person.access_token;

  getArmSettings(() => {
    if($('#button-auth').length) {
      Cons.login(itsmUser.creds);
      AS.OPTIONS.currentUser = itsmUser.data.person;
    }
  });
}

pageHandler('auth_page', async () => {
  if(Cons.getAppStore().auth_page_listener) return;

  addListener('auth_success', 'button-auth', async (authed) => {
    authed.data.person.name = getFullName(authed.data.person);
    const { login, password } = authed.creds;
    AS.OPTIONS.currentUser = authed.data.person;
    AS.OPTIONS.currentUser.groups = authed.data.groups;
    AS.apiAuth.setCredentials(login, password);

    if(!itsmUser) {
      cookie.delete('pages');
      AS.FORMS.ApiUtils.simpleAsyncGet('rest/api/person/generate_auth_key?moduleID=itsm_arm')
      .then(access_token => {
        authed.data.person.access_token = access_token.key;
        authed.creds.login = '$key';
        authed.creds.password = access_token.key;
        AS.apiAuth.setCredentials(authed.creds.login, authed.creds.password);
        AS.OPTIONS.login = '$key';
        AS.OPTIONS.password = access_token.key;
        Cons.setAppStore({itsmUser: authed});
        cookie.setJson('itsm-arm-user', authed, {expires: cookieUserExpires*60});
        itsmShowMessage({message: "Добро пожаловать на рабочее место сотрудника ITSM!", title: "Вход в систему"});
      });
    }

    getArmSettings(() => {
      if(!itsmUser) {
        fire({type: 'goto_page', pageCode: 'reg_card'}, 'button-auth');
      } else {
        cookie.getJson('pages') ? pageBack() : fire({type: 'goto_page', pageCode: 'reg_card'}, 'button-auth');
      }
    });
  });

  addListener('auth_failure', 'button-auth', e => {
    console.log('auth_failure', e);
    AS.OPTIONS.currentUser = {};
    AS.apiAuth.setCredentials(null, null);
    itsmShowMessage({
      message: "Неверно введен логин и/или пароль",
      title: "Вход в систему",
      type: 'error'
    });
  });

  Cons.setAppStore({auth_page_listener: true});
});

//Логотип
if(Cons.getAppStore().logoImg) $('#logoImg1').attr('src', Cons.getAppStore().logoImg);

$('.logoImg1').off().on('click', e => {
  window.removeEventListener('beforeunload', closeWindow, false);
  fire({type: 'goto_page', pageCode: 'reg_card'}, 'logoImg1');
});

const initButtonsHeader = () => {
  //кнопка Аналитика
  $('#button-analytics').hide();
  const infoDashboards = Cons.getAppStore().infoDashboards;
  if(infoDashboards && infoDashboards.rr_list && infoDashboards.rr_list == 'Y') {
    $('#button-analytics').show();
  } else {
    $('#button-analytics').hide();
  }

  //кнопка Карта связей
  $('#button-ci').hide();
  AS.FORMS.ApiUtils.simpleAsyncGet('rest/api/person/check_ext_module?moduleCode=svyazi_konfiguratsionnyh_edinits')
  .then(res => res ? $('#button-ci').show() : $('#button-ci').hide());

  if(Cons.getAppStore().workflow) fire({type: 'set_hidden', hidden: false}, 'button-work');

  if(AS.OPTIONS.currentUser.groups.find(x => x.groupCode == 'itsm_group_settings')) fire({type: 'set_hidden', hidden: false}, 'button-settings');
};

pageHandler('report_designer', () => {
  let pages = cookie.getJson('pages');
  if(pages) {
  	pages.currentPage = 'report_designer';
  	cookie.setJson('pages', pages);
  }
});

pageHandler('reg_card', () => {
  let pages = cookie.getJson('pages');
  if(pages) {
  	pages.currentPage = 'reg_card';
  	cookie.setJson('pages', pages);
  } else {
    cookie.setJson('pages', {
      currentPage: 'reg_card',
      code: 'reg_card'
    });
  }

  setTimeout(() => {
    $('.itsm-mobile-menu-registry').hide();
    $('#divider1').hide();
  }, 550);

  initButtonsHeader();
});

pageHandler('app', () => {
  let pages = cookie.getJson('pages');
  if(pages) {
  	pages.currentPage = 'app';
  	cookie.setJson('pages', pages);

    let param = pages.param;
    let link = $(`[registrycode="${param.registryCode}"]`);
    link.addClass("itsm-nav-link-active");
    if(param.children) {
      link.closest('ul').parent().addClass('uk-open');
    }
    if($("#panel-registry-list")) {
      let event = {
        type: 'change_itsm_menu',
        registryCode: param.registryCode,
        currentPage: param.currentPage,
        filterCode: param.filterCode,
        filterID: param.filterID,
        filterRights: param.filterRights
      };
      fire(event, "panel-registry-list");
    }

    setTimeout(() => {
      $('.itsm-mobile-menu-registry').show();
      $('#divider1').show();
      if($(document).width() < 769) $('.mobile-button-right').show();
    }, 1000);

    //Проверяем права на создание, для кнопки "Создать"
    hideShowButtonCreate(param.registryCode);
  }

  $('#button-create').off();
  $('#button-create').on('click', () => {
    Cons.showLoader();
    try {
      let registryCode =  $('.itsm-nav-link-active').attr('registrycode');
      let regInfo = getRegInfo(registryCode);
      AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/create_doc?registryCode=${registryCode}`)
      .then(res => {
        if(res.errorCode != '0') throw new Error(res.errorMessage);
        Cons.hideLoader();
        Cons.setAppStore({dataRow: res});
        let eventOpenDoc = {
          type: 'custom_open_document',
          dataUUID: res.dataUUID,
          registryCode: registryCode,
          registryName: regInfo.name,
          formName: regInfo.formName,
          editable: true
        };
        fire(eventOpenDoc, 'root-panel');
      });
    } catch (error) {
      itsmShowMessage({message: "Произошла ошибка при создании записи реестра", type: 'error'});
      console.log(error);
      Cons.hideLoader();
    }
  });

  initButtonsHeader();
});

function closeWindow(event){
  let itsmUser = cookie.getJson('itsm-arm-user') || Cons.getAppStore().itsmUser;;
  if(itsmUser) {
    cookie.setJson('itsm-arm-user', itsmUser, {expires: cookieUserExpires*60});
  }
}

function logOut(){
  window.removeEventListener('beforeunload', closeWindow, false);

  let openDocs = Cons.getAppStore().openDocs || [];
  openDocs.forEach(doc => createEvent('closed', doc));

  Cons.setAppStore({registries: null});
  Cons.setAppStore({openDocsWindow: null});
  $('.footer-panel .footer-document').remove();

  AS.apiAuth.setCredentials(null, null);
  AS.OPTIONS.currentUser = {};
  cookie.delete('pages');
  cookie.delete('itsm-arm-user');
  Cons.logout();
}

$('#button-logout').off().on('click', e => {
  let openDocsWindow = Cons.getAppStore().openDocsWindow;
  if(openDocsWindow && openDocsWindow.length) {
    UIkit.modal.confirm('Все несохраненные изменения будут потеряны. Продолжить?').then(() => {
      logOut();
    }, () => null);
  } else {
    logOut();
  }
});


const getUrlParam = (url, key) => {
  url = url.match(new RegExp(key + '=([^&=]+)'));
  return url ? url[1] : null;
}

var oldHash = "";
function pageBack(){
  let pages = cookie.getJson('pages');
  if(!pages) return;

  function goAppPage(){
    window.removeEventListener('beforeunload', closeWindow, false);
    fire({type: 'goto_page', pageCode: 'app'}, 'logoImg1');
  }

  if(pages.code == "app") {
    if(window.location.hash != "" || oldHash != "") return;
    if(window.location.hash == "" && oldHash == "") {
      let openDocs = Cons.getAppStore().openDocs || [];
      openDocs.forEach(doc => createEvent('closed', doc));
    }
    if(pages.currentPage == 'reg_card' || pages.currentPage == 'app') {
      fire({type: 'goto_page', pageCode: 'reg_card'}, 'logoImg1');
    } else {
      goAppPage();
    }
  } else {
    fire({type: 'goto_page', pageCode: 'reg_card'}, 'logoImg1');
  }
}


$('#button-back').off().on('click', e => {
  pageBack();
});

window.history.pushState({page: 1}, "", "");
window.onpopstate = function(event) {
  window.history.pushState({page: 1}, "", "");
  pageBack();
  return;
}

//Для открыти документов по клику на ссылку реестра
window.onhashchange = function(event) {
  let documentID = getUrlParam(event.newURL, 'document_identifier');
  if(documentID) {
    Cons.showLoader();
    try {
      let documentInfo;
      AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/docflow/doc/document_info?documentID=${documentID}`)
      .then(docInfo => {
        if(docInfo && docInfo.registryID) {
          documentInfo = docInfo;
          return AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/info?registryID=${docInfo.registryID}`);
        }
        return null;
      })
      .then(regInfo => {
        Cons.hideLoader();
        if(regInfo && regInfo.rr_read == "Y") {
          let player = AS.FORMS.createPlayer();
          player.view.setEditable(false);
          player.showFormData(null, null, documentInfo.asfDataID);

          let dialog = $('<div class="uk-modal-container uk-flex-top" uk-modal>');
          let md = $('<div class="uk-modal-dialog uk-margin-auto-vertical">');
          let modalBody = $('<div class="uk-modal-body" uk-overflow-auto>');
          let footer = $('<div class="uk-modal-footer uk-text-right">');

          modalBody.css({'background': '#e1e7f3'});
          md.append('<button class="uk-modal-close-default" type="button" uk-close></button>')
          footer.append('<button class="uk-button uk-button-primary uk-modal-close" type="button">Закрыть</button>');
          dialog.append(md);
          md.append(`<div class="uk-modal-header"><h3>${documentInfo.registryName} - ${documentInfo.content}</h3></div>`)
          .append(modalBody).append(footer);

          modalBody.append(player.view.container);

          UIkit.modal(dialog).show();
          dialog.on('hidden', () => {
            dialog.remove();
            oldHash = window.location.hash;
            window.location.hash = '';
            setTimeout(() => {oldHash = ''}, 1000);
          });

        } else {
          itsmShowMessage({message: "Вам запрещен доступ к этому документу", type: 'error'});
          oldHash = window.location.hash;
          window.location.hash = '';
          setTimeout(() => {oldHash = ''}, 1000);
        }
      });
    } catch (e) {
      Cons.hideLoader();
      console.log(e);
      itsmShowMessage({message: "Произошла ошибка при открытии документа", type: 'error'});
      oldHash = window.location.hash;
      window.location.hash = '';
      setTimeout(() => {oldHash = ''}, 1000);
    }
  }
}

//Иконка приложения
if(!$('link[rel="icon"]').length) {
  $('head').append('<link rel="icon" href="data:image/svg+xml;base64,PHN2ZyB2aWV3Qm94PSIwIDAgNDM4IDQzOCIgeG1sbnM9Imh0dHA6Ly93d3cudzMub3JnLzIwMDAvc3ZnIj4KPGNpcmNsZSBjeD0iNDAxIiBjeT0iMjE5IiByPSIzNyIgZmlsbD0icmVkIj48L2NpcmNsZT4KPGNpcmNsZSBjeD0iMTI4LjAwMDAwMDAwMDAwMDA2IiBjeT0iMzc2LjYxNjYyMzQ4ODc2Nzg2IiByPSIzNyIgZmlsbD0icmVkIj48L2NpcmNsZT4KPGNpcmNsZSBjeD0iMTI3Ljk5OTk5OTk5OTk5OTkxIiBjeT0iNjEuMzgzMzc2NTExMjMyMiIgcj0iMzciIGZpbGw9InJlZCI+PC9jaXJjbGU+CjxwYXRoIGQ9Ik0gMTgxLjE2MDA3MjI3MTE2Nzg1IDM5Ny4wMjI4NjMzMzM1NTI2NiBBIDE4MiAxODIgMCAwIDAgMzkyLjA5MjI4NTk2NTcxNzkgMjc1LjI0MTA5Mjk3NjI0MDQ1IiBmaWxsPSJub25lIiBzdHJva2U9InJlZCIgc3Ryb2tlLXdpZHRoPSIyNCI+PC9wYXRoPgo8cGF0aCBkPSJNIDgzLjc0NzY0MTc2MzExNDI1IDk3LjIxODIyOTY0MjY4NzggQSAxODIgMTgyIDAgMCAwIDgzLjc0NzY0MTc2MzExNDI3IDM0MC43ODE3NzAzNTczMTIyIiBmaWxsPSJub25lIiBzdHJva2U9InJlZCIgc3Ryb2tlLXdpZHRoPSIyNCI+PC9wYXRoPgo8cGF0aCBkPSJNIDM5Mi4wOTIyODU5NjU3MTc5IDE2Mi43NTg5MDcwMjM3NTk1NSBBIDE4MiAxODIgMCAwIDAgMTgxLjE2MDA3MjI3MTE2NzczIDQwLjk3NzEzNjY2NjQ0NzM5IiBmaWxsPSJub25lIiBzdHJva2U9InJlZCIgc3Ryb2tlLXdpZHRoPSIyNCI+PC9wYXRoPgo8bGluZSB4MT0iNDAxIiB5MT0iMjE5IiB4Mj0iMTI3Ljk5OTk5OTk5OTk5OTkxIiB5Mj0iNjEuMzgzMzc2NTExMjMyMiIgc3Ryb2tlPSJyZWQiIHN0cm9rZS13aWR0aD0iMjQiPjwvbGluZT4KPGxpbmUgeDE9IjQwMSIgeTE9IjIxOSIgeDI9IjEyOC4wMDAwMDAwMDAwMDAwNiIgeTI9IjM3Ni42MTY2MjM0ODg3Njc4NiIgc3Ryb2tlPSJyZWQiIHN0cm9rZS13aWR0aD0iMjQiPjwvbGluZT4KPGxpbmUgeDE9IjEyOC4wMDAwMDAwMDAwMDAwNiIgeTE9IjM3Ni42MTY2MjM0ODg3Njc4NiIgeDI9IjEyNy45OTk5OTk5OTk5OTk5MSIgeTI9IjYxLjM4MzM3NjUxMTIzMjIiIHN0cm9rZT0icmVkIiBzdHJva2Utd2lkdGg9IjI0Ij48L2xpbmU+CjxjaXJjbGUgY3g9IjIxOSIgY3k9IjIxOSIgcj0iNTQiIGZpbGw9InJlZCI+PC9jaXJjbGU+Cjwvc3ZnPgo=">')
}


function r(){
  const currentPage = Cons.getCurrentPage().code;
  switch (currentPage) {
    case "app":
      if($(document).width() > 768) {
        $('#registry_rows').height($(document).height() - 140);
        $('.itsm-table-container').height($(document).height() - 290);
        $('.mobile-button-right').hide();
      } else {
        $('.mobile-button-right').show();
        $('#registry_rows').height($(document).height() - 40);
        $('.itsm-table-container').height($(document).height() - 160);
        if($(document).width() < 640) {
          setTimeout(() => {
            if($(document).width() < 640) $('#itsm-card-container').height($(document).height() - 100);
          }, 500);
        }
      }
      break;
    case "workflow_page":
      if($(document).width() > 768) {
        $('.itsm-table-container').height($(document).height() - 280);
      } else {
        $('.itsm-table-container').height($(document).height() - 160);
      }
      break;
  }
}

$(window).resize(() => r());
r();
