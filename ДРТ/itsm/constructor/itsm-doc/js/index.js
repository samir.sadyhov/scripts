//Иконка приложения
if(!$('link[rel="icon"]').length) $('head').append('<link rel="icon" href="../constructorFiles/itsm-doc/logo.svg">');

const authUser = async () => {
  return new Promise(async resolve => {
    rest.synergyGet(`api/person/auth`, resolve, err => {
      console.log(`ERROR [ authUser ]: ${JSON.stringify(err)}`);
      resolve(null);
    });
  });
}

const pageHandlerFunction = async () => {
  try {
    const url = new URLSearchParams(window.location.search);

    if(!url.has('task')) throw new Error('Не верный URL');

    const task = url.get('task');
    Cons.setAppStore({task});

    const user = await authUser();
    Cons.setAppStore({user});

    fire({type: 'goto_page', pageCode: 'incidentPage'}, 'root-panel');
  } catch (err) {
    Cons.setAppStore({task: null});
    showMessage(i18n.tr(err.message), 'error');
  }
}

pageHandler('mainPage', () => {
  const {login, password} = Cons.creds;
  AS.apiAuth.setCredentials(login, password);

  pageHandlerFunction();
});
