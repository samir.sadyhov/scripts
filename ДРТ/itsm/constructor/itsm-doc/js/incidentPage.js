const fields = [
  {name: 'Номер', id: 'itsm_form_incident_id'},
  {name: 'Статус', id: 'itsm_form_incident_status'},
  {name: 'Услуга', id: 'itsm_form_incident_servicelink'},
  {name: 'Внешний поставщик', id: 'itsm_form_incident_supplier'},
  {name: 'Дата ожидаемого решения', id: 'itsm_form_incident_supply_date'},
  {name: 'Тема', id: 'itsm_form_incident_theme'},
  {name: 'Описание', id: 'itsm_form_incident_description'}
];

const getPosition = e => {
  let x = 0;
  let y = 0;

  if (e.pageX || e.pageY) {
    x = e.pageX;
    y = e.pageY;
  } else if (e.clientX || e.clientY) {
    x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
    y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
  }

  return {x, y};
}

const positionMenu = (contextMenu, e) => {
  const clickCoords = getPosition(e);
  const clickCoordsX = clickCoords.x;
  const clickCoordsY = clickCoords.y;
  const menuWidth = contextMenu.width() + 4;
  const menuHeight = 380;
  const windowWidth = window.innerWidth;
  const windowHeight = window.innerHeight;

  let left = 0;
  let top = 0;

  if ( (windowWidth - clickCoordsX) < menuWidth ) {
    left = windowWidth - menuWidth;
  } else {
    left = clickCoordsX;
  }

  if ( (windowHeight - clickCoordsY) < menuHeight ) {
    top = windowHeight - menuHeight;
  } else {
    top = clickCoordsY;
  }

  contextMenu.css({
    "left": left + "px",
    "top": top + "px"
  });
}

const getContextMenuItem = (name, icon, handler, disabled = false) => {
  const li = $(`<li>`);
  const a = $('<a>');
  const span = $(`<span class="uk-margin-small-right" uk-icon="icon: ${icon}"></span>`);

  if(disabled) li.addClass('uk-disabled');

  a.append(span, i18n.tr(name));
  li.append(a);

  li.on('click', e => {
    e.preventDefault();
    e.target.blur();
    if(handler) handler();
  });

  return li;
}

const contextMenu = (el, file) => {
  el.on('contextmenu', event => {
    const {value, key} = file;
    $('.attachment-context-menu').remove();

    const contextMenu = $('<div>', {class: 'attachment-context-menu'});
    const nav = $('<ul class="uk-nav-default uk-nav-parent-icon" uk-nav>');

    contextMenu.append(nav);

    nav.append(getContextMenuItem('Открыть', 'file-text', () => {
      const file = new AttachmentFile(value, key);
      file.open();
    }));

    nav.append(getContextMenuItem('Скачать', 'download', () => {
      UTILS.fileDownload(value, key);
    }));

    $('body').append(contextMenu);
    positionMenu(contextMenu, event);

    contextMenu.show('fast');
    return false;
  });
}

const parseIncident = asfData => {
  const result = {dataUUID: asfData.uuid};

  fields.forEach(field => result[field.id] = UTILS.getValue(asfData, field.id));

  const itsm_form_incident_files = UTILS.getValue(asfData, 'itsm_form_incident_files');
  result.files = UTILS.parseAsfTable(itsm_form_incident_files);

  return result;
}

const renderIcidentInfo = incident => {
  const table = $('<table>', {class: 'uk-table uk-table-striped uk-table-responsive'});
  const tbody = $('<tbody>');

  fields.forEach(field => {
    const {name, id} = field;
    const tr = $('<tr>');
    tr.append(
      `<td class="uk-width-1-5 uk-text-bold">${name}</td>`,
      `<td>${incident[id]?.value || ''}</td>`
    );
    tbody.append(tr);
  });

  table.append(tbody);
  $(`#panelIncidentInfoContent`).empty().append(table);
}

const renderIcidentFiles = incident => {
  try {
    const { files } = incident;
    const table = $('<table>', {class: 'uk-table uk-table-divider uk-table-responsive'});
    const tbody = $('<tbody>');

    files.forEach(item => {
      const {value, key} = item.file;
      const format = value.substr(value.lastIndexOf('.') + 1);
      const icon = getSvgIcon(format);
      const tr = $('<tr>');
      const td = $('<td>');

      td.on('click', e => {
        e.preventDefault();
        e.target.blur();
        const file = new AttachmentFile(value, key);
        file.open();
      });

      td.append(icon, value);
      tr.append(td);
      contextMenu(tr, item.file);
      tbody.append(tr);
    });

    table.append(tbody);
    $(`#panelIncidentFilesContent`).empty().append(table);
  } catch (e) {
    console.log(`ERROR [ renderIcidentFiles ]: ${e.message}`);
  }
}

const createSelectFileMultiple = buttonName => {
  const container = $('<div>', {class: 'uk-margin-small'});
  const fc = $('<div uk-form-custom="target: true" style="width: 100%;" class="uk-inline">');
  const inputFile = $('<input type="file" multiple>');

  fc.append(inputFile, `<button class="uk-button uk-button-default" type="button" tabindex="-1">${buttonName}</button>`);
  container.append(fc);

  return {container, inputFile};
}

const getModalDialog = () => {
  const dialog = $('<div class="uk-flex-top" uk-modal="bg-close: false; esc-close: false;">');
  const md = $('<div>', {class: "uk-modal-dialog uk-margin-auto-vertical"});
  const modalBody = $('<div class="uk-modal-body" uk-overflow-auto>');
  const footer = $('<div>', {class: "uk-modal-footer", style: "gap: 10px; display: flex; justify-content: flex-end;"});
  const buttonFinish = $(`<button class="uk-button uk-button-primary" type="button">${i18n.tr("Завершить")}</button>`);
  const commentInput = $(`<textarea class="uk-textarea" rows="5" placeholder="${i18n.tr("Описание решения")}"></textarea>`);
  const {container: filesContainer, inputFile} = createSelectFileMultiple(i18n.tr("Файлы"));
  const listFiles = $('<ul>', {class: 'uk-list uk-list-divider'});

  modalBody.append(commentInput, filesContainer, listFiles);
  footer.append(buttonFinish, `<button class="uk-button uk-button-default uk-modal-close" type="button">${i18n.tr("Закрыть")}</button>`);
  md.append(`<button class="uk-modal-close-default modal-close-custom" type="button" uk-close></button>`,
  `<div class="uk-modal-header"><h3>${i18n.tr("Завершить обращение")}</h3></div>`, modalBody, footer);
  dialog.append(md);

  return {dialog, buttonFinish, commentInput, inputFile, listFiles};
}

const renderListFiles = (files, listFiles) => {
  listFiles.empty();
  for(let i = 0; i < files.length; i++) {
    const file = files[i];
    const li = $(`<li class="uk-position-relative" index-file="${i}" title="${file.name}">${i+1}. ${file.name}</li>`);
    const delButton = $(`<a href="#" uk-icon="icon: trash" class="uk-position-center-right uk-hidden-hover"></a>`);
    li.append(delButton);
    listFiles.append(li);

    delButton.on('click', e => {
      files.splice(i, 1);
      li.remove();
      renderListFiles(files, listFiles);
    });
  }
}

const getUserWork = (processes, userID) => {
  const result = [];

  const search = p => {
    p.forEach(process => {
      if (!process.finished && process.actionID != "" && process.responsibleUserID == userID) {
        result.push(process);
      }
      if (process.subProcesses.length > 0) search(process.subProcesses);
    });
  }
  search(processes);

  return result.length ? result[0] : null;
}

const openModalFinishWork = documentID => {
  const {dialog, buttonFinish, commentInput, inputFile, listFiles} = getModalDialog();
  const { user } = Cons.getAppStore();
  let files = [];

  inputFile.on('change', e => {
    files = [...inputFile[0].files];
    renderListFiles(files, listFiles);
  });

  commentInput.on('keyup', e => {
    if(commentInput.val().trim() != '') commentInput.removeClass('uk-form-danger');
  });

  buttonFinish.on('click', async e => {
    Cons.showLoader();
    try {

      const comment = commentInput.val();
      if(!comment || comment.trim() == '') {
        commentInput.addClass('uk-form-danger');
        throw new Error('Заполните обязательное поле "Описание решения"');
      }

      const processes = await AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/workflow/get_execution_process?documentID=${documentID}&locale=${AS.OPTIONS.locale}`);
      const process = getUserWork(processes, user.userid);
      if(!process || !process.completionFormID) throw new Error('Произошла ошибка при завершении обращения');

      const completionForm = await appAPI.getCompletionForm(process.completionFormID);
      const {CompletionFormType, completionFormName, CompletionFormInfo} = completionForm;
      const {formCode, formID} = CompletionFormInfo;
      const formResult = await appAPI.getFormForResult(formCode, process.actionID);
      const {nodeUUID} = await appAPI.loadAsfData(formResult.dataUUID);

      const asfData = [];
      asfData.push({
        id: 'itsm_form_incident_wcf_current_date',
        type: 'date',
        value: AS.FORMS.DateUtils.formatDate(new Date(), '${dd}.${mm}.${yyyy} ${HH}:${MM}'),
        key: AS.FORMS.DateUtils.formatDate(new Date(), AS.FORMS.DateUtils.DATE_FORMAT_FULL)
      });
      asfData.push({id: 'itsm_form_incident_wcf_status', type: 'listbox', value: 'Ожидает оценки пользователя', key: '7'});
      asfData.push({id: 'itsm_form_incident_wcf_decisiontype', type: 'listbox', value: 'Разрешен', key: '2'});
      asfData.push({id: 'itsm_form_incident_wcf_decisiondescription', type: 'textarea', value: comment});

      if(files.length) {
        const table = {id: 'itsm_form_incident_wcf_new_decision_files', type: 'appendable_table', data: []};
        for(let i = 0; i < files.length; i++) {
          const file = files[i];
          const formData = new FormData();
          formData.append('file', file);

          const elementID = await AS.FORMS.ApiUtils.uploadFile(nodeUUID, formResult.dataUUID, formData);
          table.data.push({id: `file-b${i + 1}`, type: 'file', value: file.name, key: elementID});
        }
        asfData.push(table);
      }

      const resultMerge = await appAPI.mergeFormData({uuid: formResult.dataUUID, data: asfData});

      const resultFinish = await appAPI.finishWork({
          workID: process.actionID,
          completionForm: "FORM",
          type: 'work',
          file_identifier: formResult.file_identifier
      });

      if(!resultFinish) throw new Error(i18n.tr('Произошла ошибка при завершении работы'));

      Cons.hideLoader();
      showMessage('Обращение завершено', 'success');
      UIkit.modal(dialog).hide();
      UTILS.hide('buttonFinish');
    } catch (err) {
      Cons.hideLoader();
      showMessage(err.message, 'error');
    }
  });

  UIkit.modal(dialog).show();
  dialog.on('hidden', () => dialog.remove());
}

const pageHandlerFunction = async () => {
  Cons.showLoader();
  try {
    const { task: documentID } = Cons.getAppStore();
    const docInfo = await appAPI.getDocumentInfo(documentID);

    if(!docInfo || (docInfo.hasOwnProperty('errorCode') && docInfo.errorCode != 0)) throw new Error('Не верный URL');
    if(docInfo.formCode != "itsm_form_incident") throw new Error(`Ошибка доступа`);

    const asfData = await appAPI.loadAsfData(docInfo.asfDataID);
    const incident = parseIncident(asfData);

    if(incident?.itsm_form_incident_status?.key != '9') throw new Error(`Ошибка доступа`);

    const label = `Обращение № ${incident.itsm_form_incident_id?.value || ''}`;
    UTILS.changeLabel('labelTaskNumber', label, label);

    renderIcidentInfo(incident);
    renderIcidentFiles(incident);

    $('#buttonFinish').on('click', e => {
      e.preventDefault();
      e.target.blur();
      openModalFinishWork(documentID);
    });

    Cons.hideLoader();
  } catch (err) {
    Cons.hideLoader();
    UTILS.hide('panelIncidentFiles');
    UTILS.hide('panel-2');
    UTILS.show('panelError');
    UTILS.changeLabel('labelErrorMessage', err.message, err.message);
  }
}

pageHandler('incidentPage', () => {
  pageHandlerFunction();
});

$(document).off()
.on('contextmenu', () => $('.attachment-context-menu').remove())
.on('click', () => $('.attachment-context-menu').remove());
