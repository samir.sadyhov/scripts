#!/bin/bash

# кол-во часов, прошедших с даты открытия документа, по истечению которых нужно удалить документ из базы
HOURS=5

# mySQL настройки
mysqlUser="root"
mysqlPass="root"
mysqlDB="synergy"
mysqlHost="localhost"

# вспомогательные настройки
logFile="/var/log/synergy/scripts.log"
scriptName=${0##*/}
tmpsql=$(mktemp)
tmpsql2=$(mktemp)

function now_time () {
  date +"%Y-%m-%d %H:%M:%S"
}
function logging () {
  echo -e "`now_time` #$scriptName# [$1] $2" >> $logFile
}

# $1 mysql query
# $2 output result to tmp file
function executeQuery () {
  mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use $mysqlDB; set names utf8; $1;" > $2 2>/dev/null
}

echo '' >> $logFile
logging START "Начало работы скрипта"
logging INFO "Поиск открытых более $HOURS часов документов"

executeQuery "SELECT documentID FROM opened_documents
WHERE TIMEDIFF(NOW(), DATE_FORMAT(FROM_UNIXTIME(date/1000), '%Y-%m-%d %T')) > TIME($HOURS * 10000)" $tmpsql

if [ -s $tmpsql ];
then
  countStr=`cat $tmpsql | wc -l`
  for i in `seq 2 $countStr`;
  do
    documentID=`cat $tmpsql | sed -n $i'p' | cut -f1`
    echo '' >> $logFile
    logging INFO "Удаление documentID: $documentID"
    executeQuery "DELETE FROM opened_documents WHERE documentID = '$documentID'" $tmpsql2
    logging RESULT "`cat $tmpsql2`"
  done
else
  logging INFO "нет новых данных"
fi

echo '' >> $logFile
logging STATUS "Удаление временных файлов"
rm -rf $tmpsql
rm -rf $tmpsql2
logging END "Завершение работы скрипта"
exit 0
