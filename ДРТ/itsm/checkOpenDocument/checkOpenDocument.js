const getParams = str => str.split('$').filter(s => s.indexOf('{') !== -1).map(s => s.substring(s.indexOf('{') + 1, s.indexOf('}')));

const isopenDoc = async (login, password, documentID) => {
	return new Promise(async resolve => {
		try {
			const auth = "Basic " + btoa(unescape(encodeURIComponent(`${login}:${password}`)));
			const url = `${window.location.origin}/itsm/rest/document/isopen?documentID=${documentID}`;
			const response = await fetch(url, {method: 'GET', headers: {"Authorization": auth}});

			if(!response.ok) throw new Error(`HTTP: ${response.status}, message: ${response.statusText} [ ${response.text()} ]`);
			resolve(response.json());
		} catch (err) {
			console.log(`ERROR [ isopenDoc ]: ${err.message}`);
			resolve(null);
		}
	});
}

const docRemove = async (login, password, documentID) => {
	return new Promise(async resolve => {
		try {
			const auth = "Basic " + btoa(unescape(encodeURIComponent(`${login}:${password}`)));
			const url = `${window.location.origin}/itsm/rest/document/remove/${documentID}`;
			const headers = new Headers();
			headers.append("Authorization", auth);
			headers.append("Content-Type", "application/json; charset=utf-8");
			const response = await fetch(url, {method: 'POST', headers});
			resolve(response.json());
		} catch (err) {
			console.log(`ERROR [ docRemove ]: ${err.message}`);
			resolve(null);
		}
	});
}

const checkOpenDocument = async () => {
	try {
		const statusValue = model.playerModel.getModelWithId('itsm_form_incident_status').getValue()[0];

		if(!model.checkStatuses.includes(statusValue)) throw new Error(`со статусом ${statusValue} документ не проверяется`);

		const currentUserID = AS.OPTIONS.currentUser.userId || AS.OPTIONS.currentUser.userid;
		let userPass = AS.OPTIONS.password;

		if (window.location.href.indexOf('Synergy') !== -1) {
			if($CURRENT_USER && $CURRENT_USER.sso_hash) userPass = $CURRENT_USER.sso_hash;
		}

		const documentID = await AS.FORMS.ApiUtils.getDocumentIdentifier(model.playerModel.asfDataId);

		const doc = await isopenDoc(AS.OPTIONS.login, userPass, documentID);
		if(!doc.data) throw new Error(`не найдено открытого обращения другими пользователями`);

		if(doc.data.userID != currentUserID) {
			const user = await AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/userchooser/getUserInfo?userID=${doc.data.userID}`);

			let message = model.msg;
			const params = getParams(message);
			params.forEach(id => {
				message = message.replace('${'+id+'}', id == "open_user" ? user[0].personName : model.playerModel.getModelWithId(id).getValue());
			});
			alert(message);

		} else {
			$(window).on("beforeunload", async () => {
				await docRemove(AS.OPTIONS.login, userPass, documentID);
				$(window).off("beforeunload");
			});
		}

	} catch (e) {
		// console.error(e.message);
	}
}

checkOpenDocument();
