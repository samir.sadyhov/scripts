#!/bin/bash

#тут есть логин/пароль и хост синержи
source /opt/synergy/jboss/standalone/configuration/itsm.properties

#настройки mysql
mysqlUser="root"
mysqlPass="root"
mysqlHost="127.0.0.1"

# вспомогательные настройки
logFile="/var/log/synergy/scripts.log"
scriptName=${0##*/}
tmpfile=$(mktemp)
tmpsql=$(mktemp)

#Код реестра Базы знаний
registryCode="itsm_registry_knowledgebase"

#Срок проверки статей
termyear=1    # год
termmonth=2   # месяц
termdays=10    # день


function now_time () {
  date +"%Y-%m-%d %H:%M:%S"
}
function earlier_now_time () {
 date -d 'now -5 min' +'%Y-%m-%d %H:%M:%S'
}
function logging () {
  echo -e "`now_time` #$scriptName# [$1] $2" >> $logFile
}

# $1 mysql query
# $2 output result to tmp file
function executeQuery () {
  mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8; $1;" > $2 2>/dev/null
}

#Метод частичного сохранения данных по форме
# $1 передается жисонка в соответствии с описанием апи data/merge
function mergeAsfData () {
  curl --user $login:$password -XPOST "$address/rest/api/asforms/data/merge" --data "$1" -H "Content-Type: application/json; charset=utf-8" --output $tmpfile --silent 2>/dev/null
}

#Запускает событие "Изменение" записи реестра (документа)
# $1 dataUUID
function modifyDoc () {
  curl --user $login:$password --request GET "$address/rest/api/registry/modify_doc?dataUUID=$1" --output $tmpfile --silent 2>/dev/null
}

term=0
let "term = ($termyear * 365) + ($termmonth * 30) + $termdays"

echo '' >> $logFile
logging START "Начало работы скрипта"
logging INFO "Поиск статей в базе знаний для актуализации"

executeQuery "SELECT rg.asfDataID, rg.documentID, a.cmp_key checkdate, DATEDIFF(NOW(), a.cmp_key) days
FROM (SELECT asfDataID, documentID, registryID FROM registry_documents WHERE deleted IS NULL
AND registryID = (SELECT registryID FROM registries WHERE code = '$registryCode')) rg
JOIN (SELECT uuid, cmp_key FROM asf_data_index WHERE cmp_id = 'itsm_form_knowledgebase_checkdate') a ON a.uuid = rg.asfDataID
JOIN (SELECT uuid, cmp_key FROM asf_data_index WHERE cmp_id = 'itsm_form_knowledgebase_status') b ON b.uuid = rg.asfDataID AND b.cmp_key = 1
WHERE DATEDIFF(NOW(), a.cmp_key) >= $term" $tmpsql

if [ -s $tmpsql ];
then
  countStr=`cat $tmpsql | wc -l`

  for i in `seq 2 $countStr`;
  do
    asfDataID=`cat $tmpsql | sed -n $i'p' | cut -f1`
    documentID=`cat $tmpsql | sed -n $i'p' | cut -f2`

    echo '' >> $logFile
    logging INFO "asfDataID: $asfDataID :::: documentID: $documentID"

    mergeData='{"uuid": "'$asfDataID'", "data": [{"id": "itsm_form_knowledgebase_rout_modify", "type": "check", "values": ["1"], "keys": ["запуск маршрута изменения"]}]}'

    mergeAsfData "$mergeData"
    logging RESULT "сохранение данных\n`cat $tmpfile`"

    modifyDoc $asfDataID
    logging RESULT "запуск маршрута на изменение\n`cat $tmpfile`"
  done

else
  logging INFO "нет новых данных"
fi


echo '' >> $logFile
logging STATUS "Удаление временных файлов"
rm -rf $tmpsql
rm -rf $tmpfile
logging END "Завершение работы скрипта"
exit 0
