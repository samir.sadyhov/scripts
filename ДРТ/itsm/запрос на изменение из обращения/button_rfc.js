const getUserGroups = () => {
  return new Promise(resolve => {
    try {
      if(AS.OPTIONS.currentUser.hasOwnProperty('groups')) {
        resolve(AS.OPTIONS.currentUser.groups);
      } else {
        AS.FORMS.ApiUtils.simpleAsyncGet('rest/api/person/auth?getGroups=true', res => {
          AS.OPTIONS.currentUser.groups = res.groups;
          resolve(AS.OPTIONS.currentUser.groups);
        });
      }
    } catch (e) {
      console.log(e);
      resolve([]);
    }
  });
}

const getIncStatus = () => model.playerModel.getModelWithId('itsm_form_incident_status').getValue()[0];

const createDocument = (registryCode, data) => {
  return new Promise(resolve => {
    let body = {registryCode, data};
    try {
      AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/registry/create_doc_rcc", res => {
        if(res.errorCode != '0') throw new Error(res.errorMessage);
        resolve(res);
      }, null, JSON.stringify(body), "application/json; charset=utf-8", err => {
        throw new Error(err.responseText);
      });
    } catch (e) {
      console.log(e.message);
      resolve(null);
    }
  });
}

const createButton = () => {
  $('.custom-rfc-button').remove();
  let documentID;
  AS.FORMS.ApiUtils.getDocumentIdentifier(model.playerModel.asfDataId)
  .then(res => {
    documentID = res;
    return AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/formPlayer/getDocMeaningContent?documentId=${documentID}`, null, 'text');
  }).then(meaning => {
    let parent = $(`[document_id=${documentID}]`).parent().parent().parent();
    let buttonRFC = $('<div class="GCFCEAPDHS GCFCEAPDPJ" style="width: 100%;">').append('<span class="GCFCEAPDLO">Запрос на изменение</span>');

    parent.find('td > div.gwt-HTML').each(function(k, item) {
      if($(item).height() == 6) $(item).remove();
    });

    buttonRFC.on('click', e => {
      e.preventDefault();
      e.target.blur();

      if(!model.playerModel.isValid()) {
        AS.SERVICES.showErrorMessage('Заполните обязательные поля');
        return;
      }

      AS.SERVICES.showWaitWindow();

      let asfData = [];
      let matching = [];
      matching.push({from: 'itsm_form_incident_servicelink', to: 'itsm_form_rfc_servicelink'}); //Услуга
      matching.push({from: 'itsm_form_incident_confitemlink', to: 'itsm_form_rfc_ci'}); //Конфигурационные единицы
      matching.push({from: 'itsm_form_incident_theme', to: 'itsm_form_rfc_theme'}); //Тема
      matching.push({from: 'itsm_form_incident_description', to: 'itsm_form_rfc_description'}); //Описание

      matching.forEach(id => {
        let fromData = model.playerModel.getModelWithId(id.from).getAsfData();
        if(fromData) {
          let field = {}
          for(let key in fromData) field[key] = fromData[key];
          field.id = id.to;
          asfData.push(field);
        }
      });

      asfData.push({id: 'itsm_form_rfc_source', type: 'listbox', key: '2', value: 'Обращение'});
      asfData.push({
        id: 'itsm_form_rfc_incidentlink',
        type: 'reglink',
        key: documentID,
        valueID: documentID,
        value: meaning || 'Обращение'
      });

      console.log(asfData);

      createDocument('itsm_registry_rfc', asfData).then(newDoc => {
        // $('.custom-rfc-button').remove();
        // $('td > img[src*="edit.png"]').click();
        // $('td > img[src*="close.png"]').click();
        // setTimeout(() => {
        //   $('body > div.gwt-DialogBox > div > table > tbody > tr.background-gray > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td:nth-child(3) > table > tbody > tr > td.buttonCenterOldGray').click();
        // }, 100);
        console.log(newDoc);
        AS.SERVICES.hideWaitWindow();
        window.location.hash = `#submodule=common&action=open_document&document_identifier=${newDoc.documentID}`;
      });
    });

    let tr = $(`<tr colspan="3" class="custom-rfc-button">`);
    tr.append($('<td align="left" style="vertical-align: top;">').append(buttonRFC));
    parent.append(tr);
    parent.append('<tr><td colspan="3"><div class="gwt-HTML" style="height: 6px;"></div></td></tr>');

    model.buttonRFC = true;
  });
}

const initButtonRFC = () => {
  if(window.location.href.indexOf('Synergy') === -1) return;
  getUserGroups().then(groups => {
    const rfcGroup = groups.find(i => i.groupCode == 'itsm_group_button_rfc');
    if(rfcGroup && ['1', '2', '3', '10'].indexOf(getIncStatus()) !== -1 && !model.buttonRFC) createButton();
  });
}

initButtonRFC();
