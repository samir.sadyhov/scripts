const setStyle = () => {
  //border
  $('[data-asformid="label.container.label-4yt3dy_copy2"]').parent().parent().css({
    "border-top": "1px solid #DEE5F0",
    "border-left": "1px solid #DEE5F0",
    "border-right": "1px solid #DEE5F0"
  });
  ["label-6fw5ng", "label-h23th2", "label-h23th2_copy1", "label-h23th2_copy2", "label-h23th2_copy3"]
  .forEach(labelID => {
    $(`[data-asformid="label.container.${labelID}"]`).parent().parent().css({
      "border-left": "1px solid #DEE5F0",
      "border-right": "1px solid #DEE5F0"
    });
  });

  if(editable) {
    //если режим редактирования
    $('[data-asformid="label.container.label-h23th2_copy4"]').parent().parent().css({
      "border-left": "1px solid #DEE5F0",
      "border-right": "1px solid #DEE5F0"
    });
    $('.asf-tableRemoveButton').parent().parent().css({
      "border-bottom": "1px solid #DEE5F0",
      "border-left": "1px solid #DEE5F0",
      "border-right": "1px solid #DEE5F0"
    });
  } else {
    //если режим просмотра
    $('[data-asformid="label.container.label-h23th2_copy4"]').parent().parent().css({
      "border-bottom": "1px solid #DEE5F0",
      "border-left": "1px solid #DEE5F0",
      "border-right": "1px solid #DEE5F0"
    });
  }
}

setStyle();

if(editable) {
  let tableSla = model.playerModel.getModelWithId('itsm_form_service_sla');
  tableSla.on('tableRowAdd', function(evt, modelTab, modelRow) {
    setStyle();
  });
}
