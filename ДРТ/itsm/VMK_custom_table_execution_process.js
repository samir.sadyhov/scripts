/**
 * Issue 122
 */
let ISSUE122_CAPTIONSTATUS = 'Зарегистрирован';
let ISSUE122_CAPTIONUSER = 'Системный пользователь';

let ISSUE122_NEW_STATUS = 1;         // Зарегистрирован
let ISSUE122_IN_QUEUE = 2;           // На очереди
let ISSUE122_IN_PROGRESS = 3;        // В процессе
let ISSUE122_WAITING_USER = 4;       // Ожидает ответа пользователя
let ISSUE122_WAITING_PROBLEM = 5;    // Ожидает проблему
let ISSUE122_CLOSED = 6;             // Закрыт
let ISSUE122_WAITING_USER_RATE = 7;  // Ожидает подтверждения завершения
let ISSUE122_WAITING_EQUIPMENT = 8;  // В ожидании выделения техники
let ISSUE122_WAITING_SUPPLY = 9;     // Направлен внешнему поставщику
let ISSUE122_INFO_GIVEN = 10;        // Информация предоставлена
let ISSUE122_WRONG_WAY = 11;         // Неверно направлен
let ISSUE122_RE_OPEN = 12;           // Направлен повторно
let ISSUE122_WAITING_MASS_INCIDENT = 13; //Ожидает закрытия массового инцидента
let ISSUE122_WAITING_APPROVAL = 91;  // На согласовании
let ISSUE122_APPROVE = 15;           // Согласовано
let ISSUE122_NOT_APPROVE = 16;       // Не согласовано

/**
 * Статусы при которых будет от ISSUE122_CAPTIONUSER (Системного пользователя)
 */
let ISSUE122_ACCESS_STATUS_SU = [
  ISSUE122_CLOSED,
  ISSUE122_INFO_GIVEN,
  ISSUE122_RE_OPEN
];

let TIMER_ISSUE122_FORMCODE = false;
let TIMER_ISSUE122_TIMER = false;
let ISSUE122_FORMMODEL = false;
let RKK_ISSUE122_PARENT = false;
let IMG_ISSUE122_CLOSE = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAfklEQVR42mNgoDZITk4WosiAtLS0M6mpqb1Amp9cAy4B8X8gfpWenp5MiQEwfB6IbSgxAIaXArEcJQaA8Ddg+NQVFhZykmsADG8MDQ1lJseA5wQDFocBP0FRm5WVxUNOGGwEJi4VcmLhKtC5HuSkg8NA5+bjDCRCAG8UDUoAAIw8kVdwMG+3AAAAAElFTkSuQmCC`;
let IMG_ISSUE122_OPEN = `data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAAjUlEQVR42mNgGD6gsLCQMy0t7TAQXyICn0lOThbCMCQ1NTUfKPmfEAaq68XqitDQUGaggqsEDHgFxPw4vZKenu6BzwCgfDLB8AAq3IjDgPNEBSgwgFSAin9iMcCG6FgBBRSa5qUkRWtWVhYPUNNzqOZvQCxHctoABRg02urITmCgAAUlMrINAKWNwZ2HAAhGkVd3k7/tAAAAAElFTkSuQmCC`;

const TD_ISSUE122_TABLE = text => `<td class="white-cell light-gray-border padding4 black-text" style="cursor:default;vertical-align:top;">${text}</td>`
const TD_ISSUE122_TABLE2 = text => `<td class="gray-scroll-table-header gray-scroll-table-header-text light-gray-border" style="cursor:default;">${text}</td>`

function ISSUE122_API(method, callback, params, type) {
  var query = $.ajax({
    url: window.location.origin + '/Synergy/rest/api/' + method,
    async: false,
    type: (!type ? 'GET' : type),
    beforeSend: AS.FORMS.ApiUtils.addAuthHeader,
    data: params,
    dataType: 'json'
  });
  $.when(query).then(callback);
}

function processFilter(processes) {
  let result = [];

  function search(p) {
    p.forEach(function(process) {
      if (process.comment.indexOf('Форма завершения инцидента') != -1) result.push(process);
      if (process.subProcesses.length > 0) search(process.subProcesses);
    });
  }
  search(processes);
  return result.sort((a, b) => new Date(a.started) - new Date(b.started)).filter(x => x.actionID);
}

//костыль, шоб не появлялось на работах и проектах документов
window.onhashchange = function() {
  ISSUE122_FORMMODEL = false;
  TIMER_ISSUE122_FORMCODE = false;
  RKK_ISSUE122_PARENT = false;
}

AS.FORMS.bus.on('formShow', function(event, model, view) {
  TIMER_ISSUE122_FORMCODE = model.formCode;

  model.playerModel.on('dataLoad', function() {
    ISSUE122_FORMMODEL = model.playerModel;
  });

  if (!TIMER_ISSUE122_TIMER) {
    setInterval(function() {
      let rkk = $('.gwt-DisclosurePanel').filter((k, i) => $(i).text().toLowerCase().indexOf('ркк') !== -1);
      if (rkk.length && TIMER_ISSUE122_FORMCODE == 'itsm_form_incident' && !$('.custom-table-VMK-issue122-root').length) {
        RKK_ISSUE122_PARENT = $(rkk[0]).closest('tbody');

        $(rkk[0]).closest('tr').hide(); // HIDE TABLE "РКК"

        RKK_ISSUE122_PARENT.prepend(`<tr>
                    <td align="left" style="vertical-align: top;">
                        <table cellspacing="0" cellpadding="0" class="gwt-DisclosurePanel custom-table-VMK-issue122-table gwt-DisclosurePanel-open" style="width: 100%;">
                            <tbody>
                                <tr>
                                    <td align="left" style="vertical-align: top;">
                                        <a href="javascript:void(0);" class="header custom-table-VMK-issue122-page" style="display: block;">
                                            <table>
                                                <tbody>
                                                    <tr>
                                                        <td align="center" style="width: 16px;"><img class="custom-table-VMK-issue122-img" src="${IMG_ISSUE122_OPEN}" width="16" height="16" class="gwt-Image"></td>
                                                        <td class="custom-table-VMK-issue122-title">История инцидента ${ISSUE122_FORMMODEL.getModelWithId('itsm_form_incident_id').getTextValue()}</td>
                                                    </tr>
                                                </tbody>
                                            </table>
                                        </a>
                                    </td>
                                </tr>
                                <tr class="custom-table-VMK-issue122-root"></tr>
                            </tbody>
                        </table>
                    </td>
                </tr>`);

        $('.custom-table-VMK-issue122-page').on('click', function() {
          if (!$(this).data('VMK-issue122-page')) {
            $('.custom-table-VMK-issue122-root').hide();
            $('.custom-table-VMK-issue122-img').attr('src', IMG_ISSUE122_CLOSE);
            $('.custom-table-VMK-issue122-table').removeClass('gwt-DisclosurePanel-open');
            $('.custom-table-VMK-issue122-table').addClass('gwt-DisclosurePanel-closed');
            $(this).data('VMK-issue122-page', true);
          } else {
            $('.custom-table-VMK-issue122-root').show();
            $('.custom-table-VMK-issue122-img').attr('src', IMG_ISSUE122_OPEN);
            $('.custom-table-VMK-issue122-table').addClass('gwt-DisclosurePanel-open');
            $('.custom-table-VMK-issue122-table').removeClass('gwt-DisclosurePanel-closed');
            $(this).data('VMK-issue122-page', false);
          }
        });

        $('.custom-table-VMK-issue122-root').append('<div class="custom-table-VMK-issue122-loadData">Получение данных</div>');
        $('.custom-table-VMK-issue122-root').append('<table class="light-gray-border custom-table-VMK-issue122" cellspacing="0" cellpadding="0" style="width:100%;"></table>');
        $('.custom-table-VMK-issue122').append(`<colgroup><col width="280px"><col width="150px"><col><col width="280px"><col width="280px"></colgroup>`);
        $('.custom-table-VMK-issue122').append(`<tbody>
            <tr>
              ${TD_ISSUE122_TABLE2('Статус')}
              ${TD_ISSUE122_TABLE2('Автор')}
              ${TD_ISSUE122_TABLE2('Дата')}
              ${TD_ISSUE122_TABLE2('Пользователь')}
              ${TD_ISSUE122_TABLE2('Подразделение')}
              ${TD_ISSUE122_TABLE2('Информация')}
            </tr>
          </tbody>`);

        let oldDateHeader = ISSUE122_FORMMODEL.getModelWithId('itsm_form_incident_regdate').getValue();
        let oldDateHN = new Date("" + oldDateHeader).toLocaleString('ru', {
          year: 'numeric',
          month: 'long',
          day: 'numeric'
        });
        let newDateHeader = oldDateHN.replace(/(?:([А-Я]{3}).+$)/i, '$1, ' + oldDateHeader.split(' ')[1].substr(0, 5));

        let tbody = $('.custom-table-VMK-issue122 tbody');

        tbody.append(`<tr>
          ${TD_ISSUE122_TABLE(ISSUE122_CAPTIONSTATUS)}
          ${TD_ISSUE122_TABLE(ISSUE122_FORMMODEL.getModelWithId('itsm_form_incident_author').getTextValue())}
          ${TD_ISSUE122_TABLE(newDateHeader)}
          ${TD_ISSUE122_TABLE(ISSUE122_FORMMODEL.getModelWithId('itsm_form_incident_author').getTextValue())}
          ${TD_ISSUE122_TABLE(ISSUE122_FORMMODEL.getModelWithId('itsm_form_incident_authorDepartment').getTextValue())}
          ${TD_ISSUE122_TABLE(' - ')}
          </tr>`);


        let autoAssign = ISSUE122_FORMMODEL.getModelWithId('itsm_form_incident_auto_assignment');
        if (autoAssign && autoAssign.getValue() && autoAssign.getValue()[0] == '1') {
          tbody.append(`<tr>
            ${TD_ISSUE122_TABLE('На очереди')}
            ${TD_ISSUE122_TABLE(ISSUE122_FORMMODEL.getModelWithId('itsm_form_incident_author').getTextValue())}
            ${TD_ISSUE122_TABLE(newDateHeader)}
            ${TD_ISSUE122_TABLE(ISSUE122_FORMMODEL.getModelWithId('itsm_form_incident_responsible').getTextValue())}
            ${TD_ISSUE122_TABLE(ISSUE122_FORMMODEL.getModelWithId('itsm_form_incident_responsibleDepartment').getTextValue())}
            ${TD_ISSUE122_TABLE('Направлено: ') + ISSUE122_FORMMODEL.getModelWithId('itsm_form_incident_responsible').getTextValue()}
            </tr>`);
        }

        $.when(AS.FORMS.ApiUtils.getDocumentIdentifier(ISSUE122_FORMMODEL.asfDataId))
          .then(documentID => AS.FORMS.ApiUtils.simpleAsyncGet('rest/api/workflow/get_execution_process?documentID=' + documentID))
          .then(processes => {
            processes = processFilter(processes);
            processes.forEach(function(workItem) {
              let tr = $('<tr>')
              tbody.append(tr);

              AS.FORMS.ApiUtils.simpleAsyncGet('rest/api/workflow/work/get_completion_data?workID=' + workItem.actionID)
                .then(work => AS.FORMS.ApiUtils.loadAsfData(work.result.dataUUID))
                .then(response => {
                  let isCheckFilter = false;
                  response.data.forEach(function(itemCheckForm) {
                    if (itemCheckForm.id == 'itsm_form_incident_wcf_status') {
                      isCheckFilter = true;
                      return;
                    }
                  });

                  if (!isCheckFilter) return;

                  let tmpData = {};

                  response.data.forEach(function(item) {
                    if (item.id == 'itsm_form_incident_wcf_status') {
                      tmpData.status = (item.key ? item : {key: -1, value: ''});
                    } else if (item.id == 'itsm_form_incident_wcf_responsible') {
                      tmpData.user = (item.value ? item.value : '');
                    } else if (item.id == 'itsm_form_incident_wcf_responsible_inprogress') {
                      tmpData.user_inprogress = (item.value ? item.value : '');
                    } else if (item.id == 'itsm_form_incident_wcf_responsibleDepartment') {
                      tmpData.department = (item.value ? item.value : '');
                    } else if (item.id == 'itsm_form_incident_wcf_responsibleDepartment_inprogress') {
                      tmpData.department_inprogress = (item.value ? item.value : '');
                    } else if (item.id == 'itsm_form_incident_wcf_comment') {
                      tmpData.comment = (item.value ? item.value : '');
                    } else if (item.id == 'itsm_form_incident_wcf_equipment_date') {
                      tmpData.equipment_date = (item.value ? item.value : '');
                    } else if (item.id == 'itsm_form_incident_wcf_decisiondescription') {
                      tmpData.decisiondescription = (item.value ? item.value : '');
                    } else if (item.id == 'itsm_form_incident_wcf_decisiontype') {
                      tmpData.decisiontype = (item.value ? item.value : '');
                    } else if (item.id == 'itsm_form_incident_wcf_supply_date') {
                      tmpData.supply_date = (item.value ? item.value : '');
                    } else if (item.id == 'itsm_form_incident_wcf_supplier') {
                      tmpData.supplier = (item.value ? item.value : '');
                    } else if (item.id == 'itsm_form_incident_wcf_return_to_author') {
                      tmpData.return_to_author = (item.value ? item.value : '');
                    } else if (item.id == 'itsm_form_incident_wcf_current_user') {
                      tmpData.current_user = (item.value ? item.value : '');
                    } else if (item.id == 'itsm_form_incident_wcf_approval_description') {
                      tmpData.approval_description = (item.value ? item.value : '');
                    } else if (item.id == 'itsm_form_incident_wcf_approval_comment') {
                      tmpData.approval_comment = (item.value ? item.value : '');
                    } else if (item.id == 'itsm_form_incident_wcf_approval_users') {
                      tmpData.approval_users = (item.value ? item.value : '');
                    }
                  });

                  let username = ([ISSUE122_CLOSED, ISSUE122_INFO_GIVEN, ISSUE122_RE_OPEN].indexOf(+tmpData.status.key) !== -1 ? ISSUE122_CAPTIONUSER : (tmpData.status.key == ISSUE122_IN_PROGRESS ? tmpData.user_inprogress : tmpData.user ? tmpData.user : tmpData.user_inprogress));
                  if([ISSUE122_WAITING_APPROVAL, ISSUE122_APPROVE, ISSUE122_NOT_APPROVE].includes(Number(tmpData.status.key))) {
                    username = tmpData.approval_users;
                  }
                  let info = '';
                  let department = tmpData.status.key == ISSUE122_IN_PROGRESS ? tmpData.department_inprogress : tmpData.department ? tmpData.department : tmpData.department_inprogress;

                  switch (+tmpData.status.key) {
                    case ISSUE122_IN_QUEUE:
                    case ISSUE122_IN_PROGRESS: info = 'Направлено: ' + username + ',<br>' + tmpData.department; break;
                    case ISSUE122_WAITING_USER: info = 'Запрос: ' + tmpData.return_to_author; break;
                    case ISSUE122_WRONG_WAY: info = 'Комментарий: ' + tmpData.comment; break;
                    case ISSUE122_WAITING_USER_RATE: info = 'Код решения: ' + tmpData.decisiontype + '<br>Описание: ' + tmpData.decisiondescription; break;
                    case ISSUE122_WAITING_SUPPLY: info = 'Поставщик:  ' + tmpData.supplier; break;
                    case ISSUE122_WAITING_EQUIPMENT: info = 'Дата: ' + tmpData.equipment_date; break;
                    case ISSUE122_WAITING_APPROVAL: info = 'Описание: ' + tmpData.approval_description; break;
                    case ISSUE122_APPROVE:
                    case ISSUE122_NOT_APPROVE: info = 'Комментарий: ' + tmpData.approval_comment; break;
                    case ISSUE122_INFO_GIVEN:
                      info = 'Инициатор: ' + ISSUE122_FORMMODEL.getModelWithId('itsm_form_incident_author').getTextValue();
                      department = "" + ISSUE122_FORMMODEL.getModelWithId('itsm_form_incident_authorDepartment').getTextValue();
                      break;
                    case ISSUE122_RE_OPEN:
                      info = 'Инициатор: ' + username;
                      department = "" + ISSUE122_FORMMODEL.getModelWithId('itsm_form_incident_authorDepartment').getTextValue();
                      break;
                    case ISSUE122_CLOSED:
                      info = 'Инициатор: ' + username;
                      department = "" + ISSUE122_FORMMODEL.getModelWithId('itsm_form_incident_authorDepartment').getTextValue();
                      break;
                  }

                  let oldDate = workItem.finished;
                  let oldDateN = new Date("" + oldDate).toLocaleString('ru', {
                    year: 'numeric',
                    month: 'long',
                    day: 'numeric'
                  });
                  let newDate = oldDateN.replace(/(?:([А-Я]{3}).+$)/i, '$1, ' + oldDate.split(' ')[1].substr(0, 5));
                  let row = {
                    'Статус': tmpData.status.value,
                    'Автор': tmpData.current_user,
                    'Дата': newDate,
                    'Пользователь': username,
                    'Подразделение': department,
                    'Информация': info
                  };
                  for (let key in row) {
                    let td = $('<td>')
                      .addClass('white-cell light-gray-border padding4 black-text')
                      .css({'cursor': 'default', 'vertical-align': 'top'})
                      .html(row[key]);
                    tr.append(td);
                  }
                });
            });
            $('.custom-table-VMK-issue122-loadData').remove();
            $('.custom-table-VMK-issue122').show();
          });

      } else if (rkk.length && TIMER_ISSUE122_FORMCODE == 'itsm_form_incident') {
        $(rkk[0]).closest('tr').hide(); // HIDE TABLE "РКК"
      }
    }, 100);

    TIMER_ISSUE122_TIMER = true;
  }
});
