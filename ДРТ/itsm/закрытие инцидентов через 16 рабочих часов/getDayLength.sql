USE `synergy`;
DROP function IF EXISTS `getDayLength`;

DELIMITER $$
USE `synergy`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `getDayLength`(check_date DATETIME)
  RETURNS INT
	DETERMINISTIC
BEGIN
  DECLARE isCustomDays INT;
  DECLARE hours INT;

  SELECT COUNT(*) INTO isCustomDays FROM custom_work_days cwd WHERE cwd.date = DATE(check_date);

  IF isCustomDays > 0 THEN
    SELECT IFNULL(SUM(HOUR(TIMEDIFF(ctp.finish_time, ctp.start_time))),0) INTO hours
    FROM custom_work_days cwd
    LEFT JOIN custom_time_phases ctp ON ctp.dayID = cwd.dayID
    WHERE DATE(cwd.date) = DATE(check_date);
  ELSE
    SELECT IFNULL(SUM(HOUR(TIMEDIFF(tp.finish_time, tp.start_time))),0) INTO hours
    FROM calendars c
    LEFT JOIN time_phases tp ON tp.calendarID = c.calendarID
    LEFT JOIN work_days wd ON wd.number = tp.day_number
    WHERE tp.day_number = DAYOFWEEK(check_date) - 1
    AND wd.isWorking;
  END IF;

  RETURN hours;
END$$
DELIMITER ;
