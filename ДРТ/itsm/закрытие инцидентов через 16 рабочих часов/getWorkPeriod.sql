USE `synergy`;
DROP function IF EXISTS `getWorkPeriod`;

DELIMITER $$
USE `synergy`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `getWorkPeriod`(check_date DATETIME) RETURNS char(255) CHARSET utf8
    DETERMINISTIC
BEGIN
  DECLARE result char(255);
  DECLARE numberDay TINYINT;

  SET numberDay = DAYOFWEEK(check_date) - 1;
  IF numberDay = 0 THEN SET numberDay = 1; END IF;

  SELECT CONCAT(MIN(ctp.start_time), '-', MAX(ctp.finish_time)) INTO result
  FROM custom_work_days cwd
  LEFT JOIN custom_time_phases ctp ON ctp.dayID = cwd.dayID
  WHERE DATE(cwd.date) = DATE(check_date);

  IF result IS NULL THEN
    SELECT CONCAT(MIN(start_time), '-', MAX(finish_time)) INTO result
    FROM time_phases
    WHERE day_number = numberDay;
  END IF;

  IF result IS NULL THEN
    SELECT CONCAT(MIN(start_time), '-', MAX(finish_time)) INTO result
    FROM time_phases
    WHERE day_number = 1;
  END IF;

  RETURN result;
END$$

DELIMITER ;
