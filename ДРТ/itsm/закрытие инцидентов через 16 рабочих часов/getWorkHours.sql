USE `synergy`;
DROP function IF EXISTS `getWorkHours`;

DELIMITER $$
USE `synergy`$$
CREATE DEFINER=`root`@`localhost` FUNCTION `getWorkHours`(startDate DATETIME, finishDate DATETIME)
  RETURNS INT
  DETERMINISTIC
BEGIN
  DECLARE hours INT;
  DECLARE tmpHour INT;

  DECLARE period TEXT;
  DECLARE timeS TEXT;
  DECLARE timeF TEXT;

  IF DATE(finishDate) = DATE(startDate) THEN
    SET period = getWorkPeriod(startDate);
    SET timeS = TIME(startDate);
    SET timeF = TIME(finishDate);

    IF HOUR(timeS) < HOUR(SUBSTRING_INDEX(period, '-', 1)) THEN SET timeS = SUBSTRING_INDEX(period, '-', 1); END IF;
    IF HOUR(timeF) > HOUR(SUBSTRING_INDEX(period, '-', -1)) THEN SET timeF = SUBSTRING_INDEX(period, '-', -1); END IF;

    SET hours = HOUR(TIMEDIFF(CONCAT(DATE(finishDate), ' ', timeF), CONCAT(DATE(startDate), ' ', timeS)));
    IF hours > getDayLength(startDate) THEN SET hours = getDayLength(startDate); END IF;
  ELSE
		SELECT IFNULL(SUM(getDayLength(time_period.selected_date)),0) INTO hours
    FROM (SELECT v.* FROM
      (SELECT ADDDATE('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date
      FROM
      (SELECT 0 i UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t0,
      (SELECT 0 i UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t1,
      (SELECT 0 i UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t2,
      (SELECT 0 i UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t3,
      (SELECT 0 i UNION SELECT 1 UNION SELECT 2 UNION SELECT 3 UNION SELECT 4 UNION SELECT 5 UNION SELECT 6 UNION SELECT 7 UNION SELECT 8 UNION SELECT 9) t4
    ) v
    WHERE v.selected_date BETWEEN DATE(startDate) + INTERVAL 1 DAY AND DATE(finishDate) - INTERVAL 1 DAY) AS time_period;

    SET period = getWorkPeriod(startDate);
    SET timeS = TIME(startDate);
    SET timeF = SUBSTRING_INDEX(period, '-', -1);
    IF HOUR(timeS) < HOUR(SUBSTRING_INDEX(period, '-', 1)) THEN SET timeS = SUBSTRING_INDEX(period, '-', 1); END IF;
    SET tmpHour = HOUR(TIMEDIFF(CONCAT(DATE(startDate), ' ', timeF), CONCAT(DATE(startDate), ' ', timeS)));
    IF tmpHour > getDayLength(startDate) THEN SET tmpHour = getDayLength(startDate); END IF;
    SET hours = hours + tmpHour;

    SET period = getWorkPeriod(finishDate);
    SET timeS = SUBSTRING_INDEX(period, '-', 1);
    SET timeF = TIME(finishDate);
    IF HOUR(timeF) > HOUR(SUBSTRING_INDEX(period, '-', -1)) THEN SET timeF = SUBSTRING_INDEX(period, '-', -1); END IF;
    SET tmpHour = HOUR(TIMEDIFF(CONCAT(DATE(finishDate), ' ', timeF), CONCAT(DATE(finishDate), ' ', timeS)));
    IF tmpHour > getDayLength(finishDate) THEN SET tmpHour = getDayLength(finishDate); END IF;
    SET hours = hours + tmpHour;

  END IF;

  RETURN hours;
END$$

DELIMITER ;
