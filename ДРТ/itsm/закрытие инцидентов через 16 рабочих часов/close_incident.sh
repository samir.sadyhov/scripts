#!/bin/bash

#тут есть логин/пароль и хост синержи
source /opt/synergy/jboss/standalone/configuration/itsm.properties

#настройки mysql
mysqlUser="root"
mysqlPass="root"
mysqlHost="127.0.0.1"

# вспомогательные настройки
logFile="/var/log/synergy/scripts.log"
scriptName=${0##*/}
tmpfile=$(mktemp)
tmpsql=$(mktemp)
tmpsql2=$(mktemp)

registryCode="itsm_registry_incidents"
comment="Закрыто по истечению времени на ожидание ответа пользователя"


function now_time () {
  date +"%Y-%m-%d %H:%M:%S"
}
function now_time_formated () {
  date +"%d.%m.%Y %H:%M"
}
function earlier_now_time () {
 date -d 'now -5 min' +'%Y-%m-%d %H:%M:%S'
}
function logging () {
  echo -e "`now_time` #$scriptName# [$1] $2" >> $logFile
}

# $1 mysql query
# $2 output result to tmp file
function executeQuery () {
  mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8; $1;" > $2 2>/dev/null
}

#Метод частичного сохранения данных по форме
# $1 передается жисонка в соответствии с описанием апи data/merge
function mergeAsfData () {
  curl --user $login:$password -XPOST "$address/rest/api/asforms/data/merge" --data "$1" -H "Content-Type: application/json; charset=utf-8" --output $tmpfile --silent 2>/dev/null
}

# $1 id работы (actionid)
function getFormForResult () {
  curl --user $login:$password --request GET "$address/rest/api/workflow/work/get_form_for_result?formCode=itsm_form_incident_wcf_new&workID=$1" --output $tmpfile --silent 2>/dev/null
}

# Завершить работу
# $1 id работы (actionid)
# $2 file_identifier
function setWorkComplet () {
  curl --user $login:$password -XPOST --url "$address/rest/api/workflow/work/set_result" -H "Content-Type: application/x-www-form-urlencoded; charset=UTF-8" --data "workID=$1" --data "completionForm=FORM" --data "file_identifier=$2" --output $tmpfile --silent 2>/dev/null
}


echo '' >> $logFile
logging START "Начало работы скрипта"
logging INFO "Поиск заявок в статусе 'ожидает ответа пользователя' больше 16 часов"

#все заявки больше 16 часов в статусе ожидания
executeQuery "SELECT rg.asfDataID, rg.documentID,
IFNULL((SELECT cmp_key FROM asf_data_index WHERE uuid = rg.asfDataID AND cmp_id = 'itsm_form_incident_solvingLate'), 0) solvingLate
FROM (SELECT asfDataID, documentID, registryID FROM registry_documents WHERE deleted IS NULL AND active = 'P'
AND registryID = (SELECT registryID FROM registries WHERE code = '$registryCode')) rg
LEFT JOIN asf_data ad ON ad.uuid = rg.asfDataID
LEFT JOIN (SELECT uuid, cmp_key FROM asf_data_index WHERE cmp_id = 'itsm_form_incident_status') a ON a.uuid = rg.asfDataID
WHERE a.cmp_key = 4 AND getWorkHours(ad.modified, NOW()) > 16" $tmpsql

if [ -s $tmpsql ];
then
  countStr=`cat $tmpsql | wc -l`

  for i in `seq 2 $countStr`;
  do
    asfDataID=`cat $tmpsql | sed -n $i'p' | cut -f1`
    documentID=`cat $tmpsql | sed -n $i'p' | cut -f2`
    solvingLate=`cat $tmpsql | sed -n $i'p' | cut -f3`

    echo '' >> $logFile
    logging INFO "asfDataID: $asfDataID :::: documentID: $documentID"

    data='{"id": "itsm_form_incident_decisiontype", "type": "listbox", "key": "5", "value": "Не разрешен"}'
    data+=',{"id": "itsm_form_incident_cause", "type": "listbox", "key": "0", "value": "-"}'
    data+=',{"id": "itsm_form_incident_failCategory", "type": "listbox", "key": "0", "value": "-"}'
    data+=',{"id": "itsm_form_incident_failType", "type": "listbox", "key": "0", "value": "-"}'
    data+=',{"id": "itsm_form_incident_decisiondescription", "type": "textarea", "value": "'$comment'"}'
    data+=',{"id": "itsm_form_incident_confirm_date", "type": "date", "key": "'`now_time`'", "value": "'`now_time_formated`'"}'

    if [ "$solvingLate" == "0" ]; then
      data+=',{"id": "itsm_form_incident_responsible_dateFinish", "type": "date", "key": "'`now_time`'", "value": "'`now_time_formated`'"}'
    fi

    #жисонка для мержа
    mergeData='{"uuid": "'$asfDataID'", "data": ['$data']}'

    logging INFO "$mergeData"

    #мержим данные
    mergeAsfData "$mergeData"
    logging RESULT "`cat $tmpfile`"

    #работы которые надо завершить
    executeQuery "SELECT a.actionID
    FROM registry_processes rp
    LEFT JOIN processes p ON p.topProcInstID = rp.procInstID AND p.object_type = 4
    LEFT JOIN actions a ON a.topActionID = p.objectID
    WHERE rp.asfDataID = '$asfDataID'
    AND p.finished IS NULL AND a.finished IS NULL
    AND a.userID = (SELECT userid FROM users WHERE login = '$login')" $tmpsql2

    if [ -s $tmpsql2 ];
    then

      #берем одну работу, остальные завершаться паходу
      actionID=`cat $tmpsql2 | sed -n '2p' | cut -f1`

      echo '' >> $logFile
      logging INFO "Завершаем работу [actionID: $actionID]"
      #берем форму завершения
      getFormForResult $actionID

      file_identifier=`cat $tmpfile | jq '.file_identifier' | sed 's/"//g'`
      uuid=`cat $tmpfile | jq '.dataUUID' | sed 's/"//g'`

      logging RESULT "file_identifier: $file_identifier ::: dataUUID: $uuid"

      data2='{"id": "itsm_form_incident_wcf_status", "type": "listbox", "key": "6", "value": "Закрыто"}'
      data2+=',{"id": "itsm_form_incident_wcf_close_attribute", "type": "listbox", "key": "3", "value": "обращение закрыто, потому что автор не ответил на запрос доп.инфо"}'
      data2+=',{"id": "itsm_form_incident_wcf_decisiontype", "type": "listbox", "key": "5", "value": "Не разрешен"}'
      data2+=',{"id": "itsm_form_incident_wcf_cause", "type": "listbox", "key": "0", "value": "-"}'
      data2+=',{"id": "itsm_form_incident_wcf_failCategory", "type": "listbox", "key": "0", "value": "-"}'
      data2+=',{"id": "itsm_form_incident_wcf_failType", "type": "listbox", "key": "0", "value": "-"}'
      data2+=',{"id": "itsm_form_incident_wcf_decisiondescription", "type": "textarea", "value": "'$comment'"}'
      data2+=',{"id": "itsm_form_incident_wcf_current_date", "type": "date", "key": "'`now_time`'", "value": "'`now_time`'"}'
      mergeData2='{"uuid": "'$uuid'", "data": ['$data2']}'

      echo '' >> $logFile
      mergeAsfData "$mergeData2"
      logging RESULT "результат сохранения формы завершения\n`cat $tmpfile`"

      setWorkComplet $actionID $file_identifier
      logging RESULT "результат завершения работы\n`cat $tmpfile`"

    else
      logging INFO "нет работ для завершения"
    fi

  done
else
  logging INFO "нет новых данных"
fi


echo '' >> $logFile
logging STATUS "Удаление временных файлов"
rm -rf $tmpsql
rm -rf $tmpsql2
rm -rf $tmpfile
logging END "Завершение работы скрипта"
exit 0
