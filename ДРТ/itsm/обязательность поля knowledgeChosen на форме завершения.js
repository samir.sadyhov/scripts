model.getSpecialErrors = function() {
  let status = model.playerModel.getModelWithId('itsm_form_incident_wcf_status').value[0];
  let statusClose = model.playerModel.getModelWithId('itsm_form_incident_wcf_decisiontype').value[0];
  if(status == '7' && statusClose == '4') {
    let checked = $('[data-asformid="custom.container.itsm_form_incident_knowledgeChosen"] .asf-checkBox:checked');
    if(checked.length === 0) return {id: model.asfProperty.id, errorCode: AS.FORMS.INPUT_ERROR_TYPE.emptyValue};
  }
};
