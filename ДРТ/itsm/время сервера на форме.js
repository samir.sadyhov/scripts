const getCurrentDate = async () => {
  try {
    return new Promise(async resolve => {
      const response = await fetch(`${window.location.origin}/itsm/rest/config/currentDate`);
      if(!response.ok) throw new Error(`HTTP: ${response.status}, message: ${response.statusText} [ ${response.text()} ]`);
      resolve(response.text());
    });
  } catch (err) {
    console.log(`ERROR [ getCurrentDate ]: ${JSON.stringify(err)}`);
    resolve(null);
  }
}

const setCurrentDate = async () => {
  const curDate = await getCurrentDate();
  if(curDate) model.setValue(curDate);
}

const initInterval = () => {
  const oldTimerID = sessionStorage.getItem('timerID');
  if(oldTimerID) {
    clearInterval(oldTimerID);
    sessionStorage.removeItem('timerID');
  }

  if(model) setCurrentDate();

  const timerID = setInterval(async () => {
    if(model) {
      setCurrentDate();
    } else {
      clearInterval(timerID);
      sessionStorage.removeItem('timerID');
    }
  }, 10000);

  sessionStorage.setItem('timerID', timerID);
}

if(editable) initInterval();
