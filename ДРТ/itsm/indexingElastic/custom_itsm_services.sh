#!/bin/bash

source $(dirname $(readlink -e $0))/utils.class
scriptName=${0##*/}
tmpsql=$(mktemp)

indexName='custom_itsm_services'
indexType='cs'

echo '' >> $logFile
logging $scriptName INFO "Собираем данные для индекса $indexName"

mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8;
SELECT rg.asfDataID, rg.documentID, DATE(rg.created) created, DATE(ad.modified) modified,
IFNULL(SUM(a.cmp_data),0) sum_sla_check

FROM registries r
LEFT JOIN registry_documents rg ON rg.registryID = r.registryID
LEFT JOIN asf_data ad ON ad.uuid = rg.asfDataID
LEFT JOIN asf_data_index a ON a.uuid = rg.asfDataID AND a.cmp_id LIKE 'sla_check-b%'

WHERE r.code = 'itsm_registry_services'
AND rg.deleted IS NULL
AND DATE(ad.modified) = DATE(NOW())
GROUP BY rg.asfDataID;" > $tmpsql 2>/dev/null


if [ -s $tmpsql ];
then
  # Количество строк в файле tmpsql
  countStr=`cat $tmpsql | wc -l`

  for i in `seq 2 $countStr`;
  do
    asfDataID=`cat $tmpsql | sed -n $i'p' | cut -f1`
    documentID=`cat $tmpsql | sed -n $i'p' | cut -f2`
    created=`cat $tmpsql | sed -n $i'p' | cut -f3`
    modified=`cat $tmpsql | sed -n $i'p' | cut -f4`

    sum_sla_check=`cat $tmpsql | sed -n $i'p' | cut -f5`

    echo '' >> $logFile
    logging $scriptName INFO "asfDataID: $asfDataID"

    iData='{"asfDataID": "'$asfDataID'", "documentID": "'$documentID'",'
    iData+='"created": "'$created'", "modified": "'$modified'", "sum_sla_check": '$sum_sla_check'}'
    logging $scriptName RESULT "iData: $iData"

    elasticKey=$indexName"-"$asfDataID
    logging $scriptName RESULT "elasticKey: $elasticKey"

    logging $scriptName INFO "Обновление индекса"
    logging $scriptName INFO "$elasticHost/$indexName/$indexType/$elasticKey"
    addIndexData "$elasticHost/$indexName/$indexType/$elasticKey" "$iData"
    logging $scriptName RESULT "`cat $tmpfile`"

  done
else
  logging $scriptName INFO "нет новых данных"
fi

echo '' >> $logFile
logging $scriptName STATUS "Удаление временных файлов"
rm -rf $tmpsql
rm -rf $tmpfile
logging $scriptName END "Завершение работы скрипта"
exit 0
