#!/bin/bash

source $(dirname $(readlink -e $0))/utils.class
scriptName=${0##*/}
tmpsql=$(mktemp)

indexName='custom_itsm_problem'
indexType='cp'

echo '' >> $logFile
logging $scriptName INFO "Собираем данные для индекса $indexName"

mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8;
SELECT rg.asfDataID, rg.documentID, DATE(ad.modified) modified,
a.cmp_data regnumber, DATE(b.cmp_key) regdate,
c.cmp_data statusValue, c.cmp_key statusKey,
DATE(d.cmp_key) finishDate,

IF(c.cmp_key = 5,
IF(DATEDIFF(IFNULL(d.cmp_key, ad.modified), b.cmp_key) >= 90, 1, 0),
IF(DATEDIFF(NOW(), b.cmp_key) >= 90, 1, 0)) expired,

IFNULL((CASE
WHEN e.cmp_data <= 5 THEN 1
WHEN e.cmp_data > 5 AND e.cmp_data <= 10 THEN 0.8
WHEN e.cmp_data > 10 AND e.cmp_data <= 20 THEN 0.5
WHEN e.cmp_data > 20 THEN 0.2
END) +
(IFNULL(f.cmp_key, 0) * 1) +
IF(g.cmp_key = 3, 1, 0) +
(IFNULL(h.cmp_key, 0) * 1),0) Pr

FROM registries r
LEFT JOIN registry_documents rg ON rg.registryID = r.registryID
LEFT JOIN asf_data ad ON ad.uuid = rg.asfDataID
LEFT JOIN asf_data_index a ON a.uuid = rg.asfDataID AND a.cmp_id = 'itsm_form_problem_id'
LEFT JOIN asf_data_index b ON b.uuid = rg.asfDataID AND b.cmp_id = 'itsm_form_problem_regdate'
LEFT JOIN asf_data_index c ON c.uuid = rg.asfDataID AND c.cmp_id = 'itsm_form_problem_status'
LEFT JOIN asf_data_index d ON d.uuid = rg.asfDataID AND d.cmp_id = 'itsm_form_problem_finish_date'

LEFT JOIN asf_data_index e ON e.uuid = rg.asfDataID AND e.cmp_id = 'incidentLinkCount'
LEFT JOIN asf_data_index f ON f.uuid = rg.asfDataID AND f.cmp_id = 'itsm_form_problem_decision_Assessment'
LEFT JOIN asf_data_index g ON g.uuid = rg.asfDataID AND g.cmp_id = 'itsm_form_problem_decisiontype'
LEFT JOIN asf_data_index h ON h.uuid = rg.asfDataID AND h.cmp_id = 'itsm_form_problem_decision_Assessment_change'

WHERE r.code = 'itsm_registry_problems'
AND rg.deleted IS NULL
AND (DATE(ad.modified) = DATE(NOW()) OR c.cmp_key != 5);" > $tmpsql 2>/dev/null


if [ -s $tmpsql ];
then
  # Количество строк в файле tmpsql
  countStr=`cat $tmpsql | wc -l`

  for i in `seq 2 $countStr`;
  do
    asfDataID=`cat $tmpsql | sed -n $i'p' | cut -f1`
    documentID=`cat $tmpsql | sed -n $i'p' | cut -f2`
    modified=`cat $tmpsql | sed -n $i'p' | cut -f3`

    regnumber=`cat $tmpsql | sed -n $i'p' | cut -f4`
    regdate=`cat $tmpsql | sed -n $i'p' | cut -f5`
    statusValue=`cat $tmpsql | sed -n $i'p' | cut -f6`
    statusKey=`cat $tmpsql | sed -n $i'p' | cut -f7`
    finishDate=`cat $tmpsql | sed -n $i'p' | cut -f8`
    expired=`cat $tmpsql | sed -n $i'p' | cut -f9`
    pr=`cat $tmpsql | sed -n $i'p' | cut -f10`

    echo '' >> $logFile
    logging $scriptName INFO "asfDataID: $asfDataID"

    iData='{"asfDataID": "'$asfDataID'", "documentID": "'$documentID'", "modified": "'$modified'",'
    iData+='"regnumber": "'$regnumber'", "regdate": "'$regdate'",'
    iData+='"statusValue": "'$statusValue'", "statusKey": '$statusKey','
    iData+='"finishDate": "'$finishDate'", "expired": '$expired', "pr": '$pr'}'
    logging $scriptName RESULT "iData: $iData"

    elasticKey=$indexName"-"$asfDataID
    logging $scriptName RESULT "elasticKey: $elasticKey"

    logging $scriptName INFO "Обновление индекса"
    logging $scriptName INFO "$elasticHost/$indexName/$indexType/$elasticKey"
    addIndexData "$elasticHost/$indexName/$indexType/$elasticKey" "$iData"
    logging $scriptName RESULT "`cat $tmpfile`"

  done
else
  logging $scriptName INFO "нет новых данных"
fi

echo '' >> $logFile
logging $scriptName STATUS "Удаление временных файлов"
rm -rf $tmpsql
rm -rf $tmpfile
logging $scriptName END "Завершение работы скрипта"
exit 0
