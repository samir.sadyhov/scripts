#!/bin/bash

source $(dirname $(readlink -e $0))/utils.class
scriptName=${0##*/}
tmpsql=$(mktemp)

indexName='custom_itsm_incident'
indexType='ci'

echo '' >> $logFile
logging $scriptName START "Начало работы скрипта"
logging $scriptName INFO "Собираем данные для индекса $indexName"

mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8;
SELECT rg.asfDataID, rg.documentID, DATE(rg.created) created, DATE(ad.modified) modified,
DATE(a.cmp_key) regDate, b.cmp_data regnumber, c.cmp_data statusValue, c.cmp_key statusKey,
IFNULL(d.cmp_data, '') count_wrongway,
IFNULL(e.cmp_key, '') inqueue_date,
IFNULL(f.cmp_key, '') confirm_date,
g.cmp_data decisiontypeValue, g.cmp_key decisiontypeKey,

IF(d.cmp_data != 0, 1, 0) p1_K, IF(e.cmp_key IS NOT NULL AND e.cmp_key != 'undefined', 1, 0) p1_O,

IF(UNIX_TIMESTAMP(IFNULL(e.cmp_key, 0)) - UNIX_TIMESTAMP(a.cmp_key) > 0,
ROUND((UNIX_TIMESTAMP(IFNULL(e.cmp_key, 0)) - UNIX_TIMESTAMP(a.cmp_key)) / 60, 0), 0) p2_minutes,

IF(UNIX_TIMESTAMP(IFNULL(f.cmp_key, 0)) - UNIX_TIMESTAMP(a.cmp_key) > 0,
ROUND((UNIX_TIMESTAMP(IFNULL(f.cmp_key, 0)) - UNIX_TIMESTAMP(a.cmp_key)) / 60 / 60, 0), 0) p3_hours,

if(g.cmp_key = 4, 1, 0) p6_bz,

IF(UNIX_TIMESTAMP(f.cmp_key) > UNIX_TIMESTAMP(h.cmp_key), 1, 0)  p9_U,

IFNULL(i.cmp_key, '') type_value, IFNULL(i.cmp_data, 0) type_key,
IFNULL(j.cmp_data, '') department_name, IFNULL(j.cmp_key, '') departmentID

FROM registries r
LEFT JOIN registry_documents rg ON rg.registryID = r.registryID
LEFT JOIN asf_data ad ON ad.uuid = rg.asfDataID
LEFT JOIN asf_data_index a ON a.uuid = rg.asfDataID AND a.cmp_id = 'itsm_form_incident_regdate'
LEFT JOIN asf_data_index b ON b.uuid = rg.asfDataID AND b.cmp_id = 'itsm_form_incident_id'
LEFT JOIN asf_data_index c ON c.uuid = rg.asfDataID AND c.cmp_id = 'itsm_form_incident_status'

LEFT JOIN asf_data_index d ON d.uuid = rg.asfDataID AND d.cmp_id = 'itsm_form_incident_count_wrongway'
LEFT JOIN asf_data_index e ON e.uuid = rg.asfDataID AND e.cmp_id = 'itsm_form_incident_inqueue_date'
LEFT JOIN asf_data_index f ON f.uuid = rg.asfDataID AND f.cmp_id = 'itsm_form_incident_confirm_date'

LEFT JOIN asf_data_index g ON g.uuid = rg.asfDataID AND g.cmp_id = 'itsm_form_incident_decisiontype'
LEFT JOIN asf_data_index h ON h.uuid = rg.asfDataID AND h.cmp_id = 'itsm_form_incident_responsible_dateFinish'

LEFT JOIN asf_data_index i ON i.uuid = rg.asfDataID AND i.cmp_id = 'itsm_form_incident_type'
LEFT JOIN asf_data_index j ON j.uuid = rg.asfDataID AND j.cmp_id = 'itsm_form_incident_responsibleDepartment'

WHERE r.code = 'itsm_registry_incidents'
AND rg.deleted IS NULL;" > $tmpsql 2>/dev/null


if [ -s $tmpsql ];
then
  # Количество строк в файле tmpsql
  countStr=`cat $tmpsql | wc -l`

  for i in `seq 2 $countStr`;
  do
    asfDataID=`cat $tmpsql | sed -n $i'p' | cut -f1`
    documentID=`cat $tmpsql | sed -n $i'p' | cut -f2`
    created=`cat $tmpsql | sed -n $i'p' | cut -f3`
    modified=`cat $tmpsql | sed -n $i'p' | cut -f4`

    regDate=`cat $tmpsql | sed -n $i'p' | cut -f5`
    regnumber=`cat $tmpsql | sed -n $i'p' | cut -f6`
    statusValue=`cat $tmpsql | sed -n $i'p' | cut -f7`
    statusKey=`cat $tmpsql | sed -n $i'p' | cut -f8`
    count_wrongway=`cat $tmpsql | sed -n $i'p' | cut -f9`
    inqueue_date=`cat $tmpsql | sed -n $i'p' | cut -f10`
    confirm_date=`cat $tmpsql | sed -n $i'p' | cut -f11`
    decisiontypeValue=`cat $tmpsql | sed -n $i'p' | cut -f12`
    decisiontypeKey=`cat $tmpsql | sed -n $i'p' | cut -f13`

    p1_K=`cat $tmpsql | sed -n $i'p' | cut -f14`
    p1_O=`cat $tmpsql | sed -n $i'p' | cut -f15`
    p2_minutes=`cat $tmpsql | sed -n $i'p' | cut -f16`
    p3_hours=`cat $tmpsql | sed -n $i'p' | cut -f17`
    p6_bz=`cat $tmpsql | sed -n $i'p' | cut -f18`
    p9_U=`cat $tmpsql | sed -n $i'p' | cut -f19`

    type_value=`cat $tmpsql | sed -n $i'p' | cut -f20`
    type_key=`cat $tmpsql | sed -n $i'p' | cut -f21`
    department_name=`cat $tmpsql | sed -n $i'p' | cut -f22`
    department_name=$(parseStr "$department_name")
    departmentID=`cat $tmpsql | sed -n $i'p' | cut -f23`

    echo '' >> $logFile
    logging $scriptName INFO "asfDataID: $asfDataID"

    iData='{"asfDataID": "'$asfDataID'", "documentID": "'$documentID'",'
    iData+='"created": "'$created'", "modified": "'$modified'", "regDate": "'$regDate'",'
    iData+='"regnumber": "'$regnumber'", "statusValue": "'$statusValue'", "statusKey": '$statusKey','
    iData+='"count_wrongway": "'$count_wrongway'", "inqueue_date": "'$inqueue_date'",'
    iData+='"confirm_date": "'$confirm_date'", "decisiontypeValue": "'$decisiontypeValue'", "decisiontypeKey": "'$decisiontypeKey'",'
    iData+='"p1_K": '$p1_K', "p1_O": '$p1_O','
    iData+='"p2_minutes": '$p2_minutes', "p3_hours": '$p3_hours','
    iData+='"p6_bz": '$p6_bz', "p9_U": '$p9_U','
    iData+='"type_value": "'$type_value'", "type_key": '$type_key','
    iData+='"department_name": "'$department_name'", "departmentID": "'$departmentID'"}'
    logging $scriptName RESULT "iData: $iData"

    elasticKey=$indexName"-"$asfDataID
    logging $scriptName RESULT "elasticKey: $elasticKey"

    logging $scriptName INFO "Обновление индекса"
    logging $scriptName INFO "$elasticHost/$indexName/$indexType/$elasticKey"
    addIndexData "$elasticHost/$indexName/$indexType/$elasticKey" "$iData"
    logging $scriptName RESULT "`cat $tmpfile`"

  done
else
  logging $scriptName INFO "нет новых данных"
fi


indexName='custom_itsm_services'
indexType='cs'

echo '' >> $logFile
logging $scriptName INFO "Собираем данные для индекса $indexName"

mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8;
SELECT rg.asfDataID, rg.documentID, DATE(rg.created) created, DATE(ad.modified) modified,
IFNULL(SUM(a.cmp_data),0) sum_sla_check

FROM registries r
LEFT JOIN registry_documents rg ON rg.registryID = r.registryID
LEFT JOIN asf_data ad ON ad.uuid = rg.asfDataID
LEFT JOIN asf_data_index a ON a.uuid = rg.asfDataID AND a.cmp_id LIKE 'sla_check-b%'

WHERE r.code = 'itsm_registry_services'
AND rg.deleted IS NULL
GROUP BY rg.asfDataID;" > $tmpsql 2>/dev/null


if [ -s $tmpsql ];
then
  # Количество строк в файле tmpsql
  countStr=`cat $tmpsql | wc -l`

  for i in `seq 2 $countStr`;
  do
    asfDataID=`cat $tmpsql | sed -n $i'p' | cut -f1`
    documentID=`cat $tmpsql | sed -n $i'p' | cut -f2`
    created=`cat $tmpsql | sed -n $i'p' | cut -f3`
    modified=`cat $tmpsql | sed -n $i'p' | cut -f4`

    sum_sla_check=`cat $tmpsql | sed -n $i'p' | cut -f5`

    echo '' >> $logFile
    logging $scriptName INFO "asfDataID: $asfDataID"

    iData='{"asfDataID": "'$asfDataID'", "documentID": "'$documentID'",'
    iData+='"created": "'$created'", "modified": "'$modified'", "sum_sla_check": '$sum_sla_check'}'
    logging $scriptName RESULT "iData: $iData"

    elasticKey=$indexName"-"$asfDataID
    logging $scriptName RESULT "elasticKey: $elasticKey"

    logging $scriptName INFO "Обновление индекса"
    logging $scriptName INFO "$elasticHost/$indexName/$indexType/$elasticKey"
    addIndexData "$elasticHost/$indexName/$indexType/$elasticKey" "$iData"
    logging $scriptName RESULT "`cat $tmpfile`"

  done
else
  logging $scriptName INFO "нет новых данных"
fi


indexName='custom_itsm_problem'
indexType='cp'

echo '' >> $logFile
logging $scriptName INFO "Собираем данные для индекса $indexName"

mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8;
SELECT rg.asfDataID, rg.documentID, DATE(ad.modified) modified,
a.cmp_data regnumber, DATE(b.cmp_key) regdate,
c.cmp_data statusValue, c.cmp_key statusKey,
DATE(d.cmp_key) finishDate,

IF(c.cmp_key = 5,
IF(DATEDIFF(IFNULL(d.cmp_key, ad.modified), b.cmp_key) >= 90, 1, 0),
IF(DATEDIFF(NOW(), b.cmp_key) >= 90, 1, 0)) expired,

IFNULL((CASE
WHEN e.cmp_data <= 5 THEN 1
WHEN e.cmp_data > 5 AND e.cmp_data <= 10 THEN 0.8
WHEN e.cmp_data > 10 AND e.cmp_data <= 20 THEN 0.5
WHEN e.cmp_data > 20 THEN 0.2
END) +
(IFNULL(f.cmp_key, 0) * 1) +
IF(g.cmp_key = 3, 1, 0) +
(IFNULL(h.cmp_key, 0) * 1),0) Pr

FROM registries r
LEFT JOIN registry_documents rg ON rg.registryID = r.registryID
LEFT JOIN asf_data ad ON ad.uuid = rg.asfDataID
LEFT JOIN asf_data_index a ON a.uuid = rg.asfDataID AND a.cmp_id = 'itsm_form_problem_id'
LEFT JOIN asf_data_index b ON b.uuid = rg.asfDataID AND b.cmp_id = 'itsm_form_problem_regdate'
LEFT JOIN asf_data_index c ON c.uuid = rg.asfDataID AND c.cmp_id = 'itsm_form_problem_status'
LEFT JOIN asf_data_index d ON d.uuid = rg.asfDataID AND d.cmp_id = 'itsm_form_problem_finish_date'

LEFT JOIN asf_data_index e ON e.uuid = rg.asfDataID AND e.cmp_id = 'incidentLinkCount'
LEFT JOIN asf_data_index f ON f.uuid = rg.asfDataID AND f.cmp_id = 'itsm_form_problem_decision_Assessment'
LEFT JOIN asf_data_index g ON g.uuid = rg.asfDataID AND g.cmp_id = 'itsm_form_problem_decisiontype'
LEFT JOIN asf_data_index h ON h.uuid = rg.asfDataID AND h.cmp_id = 'itsm_form_problem_decision_Assessment_change'

WHERE r.code = 'itsm_registry_problems'
AND rg.deleted IS NULL;" > $tmpsql 2>/dev/null


if [ -s $tmpsql ];
then
  # Количество строк в файле tmpsql
  countStr=`cat $tmpsql | wc -l`

  for i in `seq 2 $countStr`;
  do
    asfDataID=`cat $tmpsql | sed -n $i'p' | cut -f1`
    documentID=`cat $tmpsql | sed -n $i'p' | cut -f2`
    modified=`cat $tmpsql | sed -n $i'p' | cut -f3`

    regnumber=`cat $tmpsql | sed -n $i'p' | cut -f4`
    regdate=`cat $tmpsql | sed -n $i'p' | cut -f5`
    statusValue=`cat $tmpsql | sed -n $i'p' | cut -f6`
    statusKey=`cat $tmpsql | sed -n $i'p' | cut -f7`
    finishDate=`cat $tmpsql | sed -n $i'p' | cut -f8`
    expired=`cat $tmpsql | sed -n $i'p' | cut -f9`
    pr=`cat $tmpsql | sed -n $i'p' | cut -f10`

    echo '' >> $logFile
    logging $scriptName INFO "asfDataID: $asfDataID"

    iData='{"asfDataID": "'$asfDataID'", "documentID": "'$documentID'", "modified": "'$modified'",'
    iData+='"regnumber": "'$regnumber'", "regdate": "'$regdate'",'
    iData+='"statusValue": "'$statusValue'", "statusKey": '$statusKey','
    iData+='"finishDate": "'$finishDate'", "expired": '$expired', "pr": '$pr'}'
    logging $scriptName RESULT "iData: $iData"

    elasticKey=$indexName"-"$asfDataID
    logging $scriptName RESULT "elasticKey: $elasticKey"

    logging $scriptName INFO "Обновление индекса"
    logging $scriptName INFO "$elasticHost/$indexName/$indexType/$elasticKey"
    addIndexData "$elasticHost/$indexName/$indexType/$elasticKey" "$iData"
    logging $scriptName RESULT "`cat $tmpfile`"

  done
else
  logging $scriptName INFO "нет новых данных"
fi


echo '' >> $logFile
logging $scriptName STATUS "Удаление временных файлов"
rm -rf $tmpsql
rm -rf $tmpfile
logging $scriptName END "Завершение работы скрипта"
exit 0
