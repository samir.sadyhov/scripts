var result = true;
var message = "ok";

function createDepartment(params) {
  let client = API.getHttpClient();
  let post = new org.apache.commons.httpclient.methods.PostMethod("http://127.0.0.1:8080/Synergy/rest/api/departments/save?locale=ru");
  for(let key in params) post.addParameter(key, params[key]);
  post.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=utf-8");
  let resp = client.executeMethod(post);
  resp = JSON.parse(post.getResponseBodyAsString());
  post.releaseConnection();
  return resp;
}

function searchDepartment(parentDepID, name){
  let result = false;
  let allDeps = API.httpGetMethod('rest/api/departments/content?onlyDepartments=true&departmentID=' + parentDepID);
  allDeps.departments.forEach(function(x){if(x.departmentName == name) result = true});
  return result;
}

function saveResult(comment, status){
  API.mergeFormData({uuid: dataUUID, data: [
    {id: 'str_form_depart_result', type: 'radio', key: status.value, value: status.key},
    {id: 'str_form_depart_comment', type: 'textbox', value: comment}
  ]});
}

try {
  let currentFormData = API.getFormData(dataUUID);
  let dep_name = UTILS.getValue(currentFormData, 'str_form_depart_name');
  let dep_parent = UTILS.getValue(currentFormData, 'str_form_depart_parent');
  let dep_code = UTILS.getValue(currentFormData, 'str_form_depart_code');
  let dep_head = UTILS.getValue(currentFormData, 'str_form_depart_head_position');

  if(!dep_parent || !dep_parent.hasOwnProperty('key')) throw new Error('Не выбрано Родительское подразделение');
  if(!dep_name || !dep_name.hasOwnProperty('value') || dep_name.value == '') throw new Error('Не заполнено Наименование подразделения');
  if(!dep_head || !dep_head.hasOwnProperty('value') || dep_head.value == '') throw new Error('Не заполнено Название должности руководителя');

  if(searchDepartment(dep_parent.key, dep_name.value)) throw new Error('В родительском подразделении \'' + dep_parent.value + '\' уже существует подразделение с названием \'' + dep_name.value + '\'');

  let createResult = createDepartment({
    nameEn: dep_name.value,
    nameKz: dep_name.value,
    nameRu: dep_name.value,
    parentDepartmentID: dep_parent.key,
    pointersCode: dep_code.value,
    positionNameEn: dep_head.value,
    positionNameKz: dep_head.value,
    positionNameRu: dep_head.value
  });

  if(createResult.errorCode != 0) throw new Error(createResult.errorMessage);

  message = "Подразделение создано успешно";
  saveResult(message, {key: '1', value: 'Успешно'});

} catch (err) {
  log.error(err.message);
  message = err.message;
  saveResult(err.message, {key: '0', value: 'Неуспешно'});
}
