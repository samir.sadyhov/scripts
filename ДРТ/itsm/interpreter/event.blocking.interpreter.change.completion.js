let API = {
  getHttpClient: function(){
    let client = new org.apache.commons.httpclient.HttpClient();
    let creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(login, password);
    client.getParams().setAuthenticationPreemptive(true);
    client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
    return client;
  },
  httpGetMethod: function(methods, type) {
    let client = this.getHttpClient();
    let get = new org.apache.commons.httpclient.methods.GetMethod("http://127.0.0.1:8080/Synergy/" + methods);
    get.setRequestHeader("Content-type", "application/json");
    client.executeMethod(get);
    let resp = get.getResponseBodyAsString();
    get.releaseConnection();
    return type == 'text' ? resp : JSON.parse(resp);
  },
  httpPostMethod: function(methods, params) {
    let client = this.getHttpClient();
    let post = new org.apache.commons.httpclient.methods.PostMethod("http://127.0.0.1:8080/Synergy/" + methods);
    for(let key in params) post.addParameter(key, params[key]);
    post.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=utf-8");
    let resp = client.executeMethod(post);
    post.releaseConnection();
    return resp;
  },
  getFormData: function(asfDataId) {
    return this.httpGetMethod("rest/api/asforms/data/" + asfDataId)
  },
  saveFormData: function(formID, asfDataId, asfData) {
    return this.httpPostMethod("rest/api/asforms/form/multipartdata", {
      form: formID,
      uuid: asfDataId,
      data: "\"data\":" + JSON.stringify(asfData)
    });
  }
};

API.getProcesses = function(documentID) {
  return this.httpGetMethod("rest/api/workflow/get_execution_process?documentID=" + documentID);
};
API.getWorkCompletionData = function(workID) {
  return this.httpGetMethod("rest/api/workflow/work/get_completion_data?workID=" + workID);
};

API.getFormDescription = function(formID) {
  return this.httpGetMethod("rest/api/asforms/form/" + formID + "?isMobile=false");
};

let log = {
  parse: function(args) {
    let result = [];
    for(let x in args) result.push(JSON.stringify(args[x], null, 4));
    return documentID + '\n' + result.join('\n');
  },
  info: function() {
    console.info(this.parse(arguments));
  },
  error: function() {
    console.error(this.parse(arguments));
  }
}

let UTILS = {
  createField: function(fieldData) {
    let field = {};
    for (let key in fieldData) field[key] = fieldData[key];
    return field;
  },
  getValue: function(data, cmpID) {
    data = data.data ? data.data : data;
    for(let i = 0; i < data.length; i++)
    if(data[i].id === cmpID) return data[i];
  },
  setValue: function(asfData, cmpID, data) {
    let field = this.getValue(asfData, cmpID);
    for(let key in data) {
      if(key === 'id' || key === 'type') continue;
      field[key] = data[key];
    }
    return field;
  }
};

UTILS.getTableBlockIndex = function(data, cmp) {
  let res = 0;
  data = data.data ? data.data : data;
  data.forEach(function(item) {
    if (item.id.slice(0, item.id.indexOf('-b')) === cmp) res++;
  });
  return res === 0 ? 1 : ++res;
};

UTILS.parseDateTime = function(datetime /*yyyy-mm-dd HH:MM:SS*/) {
  let date = datetime.split(' ')[0].split('-');
  let time = datetime.split(' ')[1].split(':');
  return new Date(date[0], date[1]-1, date[2], time[0], time[1], time[2]);
};

UTILS.getActionIds = function(processes, formCode) {
  let result = [];

  function search(p) {
    p.forEach(function(process) {
      if (process.completionFormCode == formCode && process.finished) {
        result.push(process);
      }
      if (process.subProcesses.length > 0) search(process.subProcesses);
    });
  }
  search(processes);

  return result.sort(function(a, b) {
    return UTILS.parseDateTime(b.finished) - UTILS.parseDateTime(a.finished);
  }).map(function(item) {
    return {actionID: item.actionID, finished: item.finished};
  });
};

UTILS.getCmpType = function(formDescription, cmpID) {
  let result = null;
  for (let i = 0; i < formDescription.properties.length; i++) {
    if (formDescription.properties[i].id === cmpID) {
      result = formDescription.properties[i].type;
      break;
    }
    if (formDescription.properties[i].type === "table") {
      result = this.getCmpType(formDescription.properties[i], cmpID) || result;
    }
  }
  return result;
};

var result = true;
var message = "ok";

try {

  let formCodeCompletion = 'itsm_change_completion_new';
  let matching = [];
  matching.push({from: 'itsm_form_change_wcf_status', to: 'itsm_form_change_status'}); //статус
  matching.push({from: 'itsm_form_change_wcf_design_description', to: 'table_itsm_form_design.itsm_form_change_design_description'}); //по проектированию: описание
  matching.push({from: 'itsm_form_change_wcf_design_file', to: 'table_itsm_form_design.itsm_form_change_design_file'}); //по проектированию: файл
  matching.push({from: 'itsm_form_change_wcf_development_description', to: 'table_itsm_form_development.itsm_form_change_development_description'}); //по разработке: описание
  matching.push({from: 'itsm_form_change_wcf_development_file', to: 'table_itsm_form_development.itsm_form_change_development_file'}); //по разработке: файл
  matching.push({from: 'itsm_form_change_wcf_testing_description', to: 'table_itsm_form_testing.itsm_form_change_testing_description'}); //по тестированию: описание
  matching.push({from: 'itsm_form_change_wcf_testing_file', to: 'table_itsm_form_testing.itsm_form_change_testing_file'}); //по тестированию: файл
  matching.push({from: 'itsm_form_change_wcf_return_to_system_admin', to: 'itsm_form_change_return_to_system_admin'}); //запрос на доработку
  matching.push({from: 'itsm_form_change_wcf_close_code', to: 'itsm_form_change_close_code'}); //код закрытия
  matching.push({from: 'itsm_form_change_wcf_close_description', to: 'itsm_form_change_close_description'}); //описание
  matching.push({from: 'itsm_form_change_wcf_effect', to: 'itsm_form_change_effect'}); //оценка изменения

  let processes = API.getProcesses(documentID);
  let actions = UTILS.getActionIds(processes, formCodeCompletion);

  let completionFormDataUUID = actions.map(function(item) {
    return API.getWorkCompletionData(item.actionID).result.dataUUID;
  }).filter(function(dataUUID){ return dataUUID != null})[0];

  if(!completionFormDataUUID) throw new Error('Не найдена форма завершения');

  let completionFormData = API.getFormData(completionFormDataUUID);
  let currentFormData = API.getFormData(dataUUID);
  let currentFormDescription = API.getFormDescription(currentFormData.form);

  matching.forEach(function(id) {
    let fromData = UTILS.getValue(completionFormData, id.from);

    if(fromData && fromData.value) {
      if(id.to.indexOf('.') == -1) {
         UTILS.setValue(currentFormData, id.to, fromData);
      } else {
        let tableID = id.to.split('.')[0];
        let cmpID = id.to.split('.')[1];
        let tableData = UTILS.getValue(currentFormData, tableID);

        let field = UTILS.createField({
          id: cmpID + "-b" + UTILS.getTableBlockIndex(tableData, cmpID),
          type: UTILS.getCmpType(currentFormDescription, cmpID)
        });

        for(let key in fromData) {
          if(key === 'id' || key === 'type') continue;
          field[key] = fromData[key];
        }

        tableData.data.push(field);
      }
    }
  });

  API.saveFormData(currentFormData.form, currentFormData.uuid, currentFormData.data);

} catch (err) {
  log.error(err);
  message = err.message;
}
