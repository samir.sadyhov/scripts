let API = {
  getHttpClient: function(){
    let client = new org.apache.commons.httpclient.HttpClient();
    let creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(login, password);
    client.getParams().setAuthenticationPreemptive(true);
    client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
    return client;
  },
  httpGetMethod: function(methods, type) {
    let client = this.getHttpClient();
    let get = new org.apache.commons.httpclient.methods.GetMethod("http://127.0.0.1:8080/Synergy/" + methods);
    get.setRequestHeader("Content-type", "application/json");
    client.executeMethod(get);
    let resp = get.getResponseBodyAsString();
    get.releaseConnection();
    return type == 'text' ? resp : JSON.parse(resp);
  },
  httpPostMethod: function(methods, params, contentType) {
    let client = this.getHttpClient();
    let post = new org.apache.commons.httpclient.methods.PostMethod("http://127.0.0.1:8080/Synergy/" + methods);
    if(contentType) post.setRequestBody(JSON.stringify(params));
    else for(let key in params) post.addParameter(key, params[key]);
    post.setRequestHeader("Content-type", contentType || "application/x-www-form-urlencoded; charset=utf-8");
    let resp = client.executeMethod(post);
    post.releaseConnection();
    return resp;
  },
  getFormData: function(asfDataId) {
    return this.httpGetMethod("rest/api/asforms/data/" + asfDataId)
  },
  saveFormData: function(formID, asfDataId, asfData) {
    return this.httpPostMethod("rest/api/asforms/form/multipartdata", {
      form: formID,
      uuid: asfDataId,
      data: "\"data\":" + JSON.stringify(asfData)
    });
  },
  getAsfDataId: function(documentID) {
    return this.httpGetMethod("rest/api/formPlayer/getAsfDataUUID?documentID=" + documentID, 'text');
  },
  getFormForResult: function(formCode, workID) {
    return this.httpGetMethod("rest/api/workflow/work/get_form_for_result?formCode=" + formCode + "&workID=" + workID);
  },
  finishWork: function(actionID, file_identifier) {
    return this.httpPostMethod("rest/api/workflow/work/set_result", {
        workID: actionID,
        completionForm: "FORM",
        type: 'work',
        file_identifier: file_identifier
    });
  },
  getProcesses: function(documentID) {
    return this.httpGetMethod("rest/api/workflow/get_execution_process?documentID=" + documentID);
  }
};

let log = {
  parse: function(args) {
    let result = [];
    for (let x in args) result.push(JSON.stringify(args[x], null, 4));
    return documentID + '\n' + result.join('\n');
  },
  info: function() {
    console.info(this.parse(arguments));
  },
  error: function() {
    console.error(this.parse(arguments));
  }
}

let UTILS = {
  createField: function(fieldData) {
    let field = {};
    for (let key in fieldData) field[key] = fieldData[key];
    return field;
  },
  getValue: function(data, cmpID) {
    data = data.data ? data.data : data;
    for(let i = 0; i < data.length; i++)
    if (data[i].id === cmpID) return data[i];
  },
  setValue: function(asfData, cmpID, data) {
    let field = this.getValue(asfData, cmpID);
    for (let key in data) {
      if(key === 'id' || key === 'type') continue;
      field[key] = data[key];
    }
    return field;
  },
  getTableBlockIndex: function(data, cmp) {
    let res = 0;
    data = data.data ? data.data : data;
    data.forEach(function(item) {
      if (item.id.slice(0, item.id.indexOf('-b')) === cmp) res++;
    });
    return res === 0 ? 1 : ++res;
  },
  getActionIds: function(processes) {
    let result = [];
    function search(p) {
      p.forEach(function(process) {
        if (process.code == "problem_change" && !process.finished) {
          result.push(process.actionID);
        }
        if (process.subProcesses.length > 0) search(process.subProcesses);
      });
    }
    search(processes);
    return result;
  }
};

var result = true;
var message = "ok";

try {

  let currentFormData = API.getFormData(dataUUID);
  let problemDocID = UTILS.getValue(currentFormData, "itsm_form_rfc_problemlink");

  if(!problemDocID) throw new Error("Ссылка на проблему пустая");
  if(!problemDocID.key) throw new Error("Ссылка на проблему пустая");

  let ststusRFC = UTILS.getValue(currentFormData, "itsm_form_rfc_status");

  let problemFormData = API.getFormData(API.getAsfDataId(problemDocID.key));
  let problemTableData = UTILS.getValue(problemFormData, "itsm_form_problem_changeRequests");

  if(!problemTableData) {
    problemTableData = UTILS.createField({
      id: "itsm_form_problem_changeRequests",
      type: "appendable_table",
      key: "",
      data: []
    });
    problemFormData.data.push(problemTableData);
  }

  problemTableData.data.push(UTILS.createField({
    id: "itsm_form_problem_changeRequestLink-b" + UTILS.getTableBlockIndex(problemTableData, "itsm_form_problem_changeRequestLink"),
    type: "reglink",
    key: documentID,
    valueID: documentID
  }));

  let statusField= UTILS.createField({
    id: "itsm_form_problem_changeRequestStatus-b" + UTILS.getTableBlockIndex(problemTableData, "itsm_form_problem_changeRequestStatus"),
    type: "check",
    values: [],
    keys: []
  });

  let oldStatusProblem = UTILS.getValue(problemFormData, "itsm_form_problem_oldStatus");
  let oldStatusKey = oldStatusProblem.key;
  let oldStatusValue = oldStatusProblem.value;

  switch (ststusRFC.key) {
    case "4":
    statusField.values.push("2");
    statusField.keys.push("Принят");
    break;

    case "5":
    statusField.values.push("1");
    statusField.keys.push("Отклонен");
    //Возвращаем предыдущий статус
    UTILS.setValue(problemFormData, "itsm_form_problem_status", oldStatusProblem);
    //Сохраняем текущий статус как старый
    UTILS.setValue(problemFormData, "itsm_form_problem_oldStatus", {key: "4", value: "Ожидает изменений"});
    break;
  }

  problemTableData.data.push(statusField);

  API.saveFormData(problemFormData.form, problemFormData.uuid, problemFormData.data);

  if(ststusRFC.key == "5") { //если отклонен завершаем работу системного юзера в проблемах
    let problemProcesses = API.getProcesses(problemDocID.key);
    let actions = UTILS.getActionIds(problemProcesses);

    if(actions.length === 0) throw new Error('Не найдено работ для завершения');

    let resultFormWork = API.getFormForResult("itsm_form_problem_wcf", actions[0]);
    let resultFormWorkData = API.getFormData(resultFormWork.dataUUID);
    UTILS.setValue(resultFormWorkData, "itsm_form_problem_wcf_status", {key: oldStatusKey, value: oldStatusValue});
    API.saveFormData(resultFormWorkData.form, resultFormWorkData.uuid, resultFormWorkData.data);
    API.finishWork(actions[0], resultFormWork.file_identifier);
  }

} catch (err) {
  log.error(err.message);
  message = err.message;
}
