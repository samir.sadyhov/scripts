var result = true;
var message = "ok";

function getDateDiff(start, stop) {
  let client = API.getHttpClient();
  let get = new org.apache.commons.httpclient.methods.GetMethod("http://127.0.0.1:8080/itsm/rest/dateDiff?start=" + start + "&stop=" + stop);
  client.executeMethod(get);
  let resp = get.getResponseBodyAsString();
  get.releaseConnection();
  return resp;
}

function customFormatDate(datetime){
  datetime = datetime.split(/\D/);
  return datetime[2] + '.' + datetime[1] + '.' + datetime[0] + ' ' + datetime[3] + ':' + datetime[4];
}

try {
  let currentFormData = API.getFormData(dataUUID);
  let tableID = 'itsm_form_incident_table_exec';
  let tableData = UTILS.getValue(currentFormData, tableID);
  let matching = [];

  matching.push({from: 'itsm_form_incident_status', to: 'status'});
  matching.push({from: 'itsm_form_incident_prev_status_time', to: 'time'});
  matching.push({from: 'itsm_form_incident_servicelink', to: 'servicelink'});
  matching.push({from: 'itsm_form_incident_priority', to: 'priority'});
  matching.push({from: 'itsm_form_incident_responsible', to: 'responsible'});
  matching.push({from: 'itsm_form_incident_responsibleDepartment', to: 'responsibleDepartment'});

  if(!tableData) {
    tableData = {"id": tableID, "type": "appendable_table", "data": []}
    currentFormData.data.push(tableData);
  }

  if(!tableData.hasOwnProperty('data')) tableData.data = [];

  let tableBlockIndex = UTILS.getTableBlockIndex(tableData, matching[0].to);

  if(tableBlockIndex == '1') {
    let currentDate = UTILS.getCurrentDateParse();
    UTILS.setValue(tableData, "status-b" + tableBlockIndex, UTILS.getValue(currentFormData, 'itsm_form_incident_status'));
    UTILS.setValue(tableData, "time-b" + tableBlockIndex, {type: "date", key: currentDate, value: customFormatDate(currentDate)});
  } else {
    matching.forEach(function(id){
      let fromData = UTILS.getValue(currentFormData, id.from);
      let idTo = id.to + "-b" + tableBlockIndex;
      if(fromData) UTILS.setValue(tableData, idTo, fromData);
    });

    UTILS.setValue(tableData, "dateFinish-b" + (tableBlockIndex - 1), UTILS.getValue(currentFormData, 'itsm_form_incident_responsible_dateFinish'));

    let oldTime = UTILS.getValue(tableData, "time-b" + (tableBlockIndex - 1));
    let newTime = UTILS.getValue(tableData, "time-b" + tableBlockIndex);

    if(newTime.hasOwnProperty('key')) {
      oldTime = UTILS.parseDateTime(oldTime.key).getTime();
      newTime = UTILS.parseDateTime(newTime.key).getTime();

      let hours = getDateDiff(oldTime, newTime) / 3600;
      hours = Math.floor(hours * 100) / 100;

      UTILS.setValue(tableData, 'duration-b' + (tableBlockIndex - 1), {"type": "numericinput", "key": String(hours), "value": String(hours)});
    }
  }

  API.saveFormData(currentFormData);

} catch (err) {
  log.error(err.message);
  message = err.message;
}
