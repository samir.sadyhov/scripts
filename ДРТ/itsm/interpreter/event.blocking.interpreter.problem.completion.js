// Код справочника содержащий в себе статусы инцидента
let DICT_PROBLEM_STATUS = 'itsm_dict_problemstatus';

// Колонка по которой будем искать значение со справочника для смены статуса
let DICT_PROBLEM_STATUS_COL = 'itsm_dict_problemstatus_value';

// Колонка в которой хранится значение (Например: На очереди) от колонки `DICT_INCIDENT_STATUS_COL`
let DICT_PROBLEM_TYPE_COL = 'itsm_dict_problemstatus_type';

// Статусы инцидента
let NEW_STATUS = 1; // Зарегистрирован
let IN_QUEUE = 2; // На очереди
let IN_PROGRESS = 3; // В расследовании
let WAITING_CHANGE = 4; // Ожидает изменение
let CLOSED = 5; // Закрыт


// Массив для смены статуса по условию
let ArrayChangeStatus = [
  IN_QUEUE,
  IN_PROGRESS,
  WAITING_CHANGE,
  CLOSED,
];

let jSynergy = new __classSynergy();
/**
 * Функция ищет строку в справочнике по значению в коде колонки
 * @param   code            Код справочника
 * @param   colCode         Код колонки по которой будем искать значение
 * @param   value           Значение поиска
 * @param   locale          Локаль (перевод) - по умолчанию ru
 * @return  Объект строки из справочника
 */
function getDictStatus(code, colCode, value, locale) {
  let dictData = jSynergy.server.api('dictionary/get_by_code?dictionaryCode=' + code + (locale ? '&locale=' + locale : ''));
  let colIDs = {};
  let res = {};

  if (!dictData) return 'Не удалось загрузить справочник';
  dictData.columns.forEach(function(item) {
    colIDs[item.columnID] = item.code;
  });

  if (!colIDs) return 'Данной колонки `' + colCode + '` - не существует в справочнике';
  dictData.items.forEach(function(itemArr) {
    itemArr.values.forEach(function(item) {
      if (~Object.keys(colIDs).indexOf("" + item.columnID) && item.value == value) {
        itemArr.values.forEach(function(item) {
          res[colIDs[item.columnID]] = item.value;
        });
        return;
      }
    });
  });

  return res;
}

/**
 * Устанавливаем соединение с нашим контейнером *Обязательно*
 * @param   host        Адресс Synergy, если false то это 127.0.0.1
 * @param   login       Логин пользователя под кем подключаемся
 * @param   password    Пароль пользователя под кем подключаемся
 */
jSynergy.setConnection(false, login, password);

function recursiveFilter(arr) {
  var matches = [];
  if (!Array.isArray(arr)) return matches;

  arr.forEach(function(x) {
    if (x.typeID == 'ASSIGNMENT_ITEM' && !!x.finished) matches.push(x);

    if (x.subProcesses.length > 0) {
      var childResults = recursiveFilter(x.subProcesses);
      if (childResults.length) matches = matches.concat(childResults);
    }
  });

  return matches;
}

try {
  jSynergy.server.load(dataUUID);

  let processes = jSynergy.server.api('workflow/get_execution_process?documentID=' + documentID);

  if (!Array.isArray(processes)) throw new Error('get_execution_process не сработал');

  processes = recursiveFilter(processes);

  if (!processes || !processes[0]) throw new Error('Работа не найдена');

  /**
   * Фича от проблемы в Интерпретаторе
   * Если написать так `new Date('2019-02-18 03:58:32')` то в ответ получаем `Invalid Date` - Скорее всего БАГ.
   * Почему цикл - переменная `processes` может содержать более одной работы с датами.
   */
  processes.forEach(function(processe) {
    var finished = processe.finished.split(/\D/);
    var started = processe.started.split(/\D/);
    processe.finished = new Date(finished[0], finished[1], finished[2], finished[3], finished[4], finished[5]);
    processe.started = new Date(started[0], started[1], started[2], started[3], started[4], started[5]);
  });

  processes = processes.sort(function(a, b) {
    return b.finished - a.finished;
  });

  var lastRouteItemID = processes[0].routeItemID;
  processes = processes.filter(function(x) {
    return x.routeItemID === lastRouteItemID;
  });

  var cform_check = false;
  var responseComment = null;
  processes.forEach(function(work) {
    work = jSynergy.server.api('workflow/work/get_completion_data?workID=' + work.actionID);
    if (work && work.result && work.result.dataUUID) {
      cform_check = true;
      processes = work;
      responseComment = work.result.comment;
      return;
    }
  });

  if (!cform_check || !processes) throw new Error('Предыдущий этап завершен без формы завершения');

  /**
   * Чтоб не создавать лишнии переменные заюзаем уже лишнюю переменную `processes` в качестве работы с формой `processes.result.dataUUID`
   */
  jSynergy.server.load(processes.result.dataUUID);

  processes = {
    'itsm_form_problem_wcf_responsible': jSynergy.server.getValue('itsm_form_problem_wcf_responsible'),
    'itsm_form_problem_wcf_responsibleDepartment': jSynergy.server.getValue('itsm_form_problem_wcf_responsibleDepartment'),
    'itsm_form_problem_wcf_decisiontype': jSynergy.server.getValue('itsm_form_problem_wcf_decisiontype'),
    'itsm_form_problem_wcf_decisiondescription': jSynergy.server.getValue('itsm_form_problem_wcf_decisiondescription'),
    'itsm_form_problem_wcf_current_date': jSynergy.server.getValue('itsm_form_problem_wcf_current_date'), //#229
    'itsm_form_problem_wcf_makenew_knowledge': jSynergy.server.getValue('itsm_form_problem_wcf_makenew_knowledge'), //#236.1
    'itsm_form_problem_wcf_responsible_inprogress': jSynergy.server.getValue('itsm_form_problem_wcf_responsible_inprogress'), //#305
    'itsm_form_problem_wcf_responsibleDepartment_inprogress': jSynergy.server.getValue('itsm_form_problem_wcf_responsibleDepartment_inprogress') //#305
  };

  var wcf_status = +jSynergy.server.getValue('itsm_form_problem_wcf_status', 'key') || 0;

  jSynergy.server.selectForm(dataUUID);

  //Меняем статус если `wcf_status` есть в массиве
  if (~ArrayChangeStatus.indexOf(wcf_status)) {
    var dictWcf = getDictStatus(DICT_PROBLEM_STATUS, DICT_PROBLEM_STATUS_COL, "" + wcf_status);
    console.info("setStatus: " + wcf_status + " " + dictWcf[DICT_PROBLEM_TYPE_COL]);
    jSynergy.server.setValue('itsm_form_problem_status', {
      key: "" + wcf_status,
      value: "" + dictWcf[DICT_PROBLEM_TYPE_COL]
    });
  }

  switch (wcf_status) {
    case IN_QUEUE:
      if (processes.itsm_form_problem_wcf_responsible && processes.itsm_form_problem_wcf_responsible.key) {
        jSynergy.server.setValue('itsm_form_problem_responsible', {
          key: "" + processes.itsm_form_problem_wcf_responsible.key,
          value: "" + processes.itsm_form_problem_wcf_responsible.value
        });
      }
      if (processes.itsm_form_problem_wcf_responsibleDepartment && processes.itsm_form_problem_wcf_responsibleDepartment.key) {
        jSynergy.server.setValue('itsm_form_problem_responsibleDepartment', {
          key: "" + processes.itsm_form_problem_wcf_responsibleDepartment.key,
          value: "" + processes.itsm_form_problem_wcf_responsibleDepartment.value
        });
      }
      break;
    case IN_PROGRESS:
      //#305
      jSynergy.server.setValue('itsm_form_problem_responsible', {
        key: processes.itsm_form_problem_wcf_responsible_inprogress.key,
        value: processes.itsm_form_problem_wcf_responsible_inprogress.value
      });
      jSynergy.server.setValue('itsm_form_problem_responsibleDepartment', {
        key: processes.itsm_form_problem_wcf_responsibleDepartment_inprogress.key,
        value: processes.itsm_form_problem_wcf_responsibleDepartment_inprogress.value
      });
      break;
    case CLOSED:
      jSynergy.server.setValue('itsm_form_problem_decisiontype', {
        key: "" + processes.itsm_form_problem_wcf_decisiontype.key,
        value: "" + processes.itsm_form_problem_wcf_decisiontype.value
      });
      jSynergy.server.setValue('itsm_form_problem_decisiondescription', {
        key: "" + processes.itsm_form_problem_wcf_decisiondescription.key,
        value: "" + processes.itsm_form_problem_wcf_decisiondescription.value
      });
      //#229
      jSynergy.server.setValue('itsm_form_problem_finish_date', {
        key: "" + processes.itsm_form_problem_wcf_current_date.key,
        value: "" + processes.itsm_form_problem_wcf_current_date.value
      });
      //#236.1
      jSynergy.server.setValue('itsm_form_problem_makenew_knowledge', {
        keys: processes.itsm_form_problem_wcf_makenew_knowledge.keys,
        values: processes.itsm_form_problem_wcf_makenew_knowledge.values
      });
      break;
  }

  jSynergy.server.save();

  result = true;
  message = JSON.stringify(responseComment);
} catch (err) {
  result = false;
  message = err.message;
  console.error(err.message);
}
