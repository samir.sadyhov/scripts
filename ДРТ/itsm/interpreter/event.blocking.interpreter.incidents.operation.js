// Статусы инцидента
let NEW_STATUS = 1; // Зарегистрирован
let IN_QUEUE = 2; // На очереди
let IN_PROGRESS = 3; // В процессе
let WAITING_USER = 4; // Ожидает ответа пользователя
let WAITING_PROBLEM = 5; // Ожидает проблему
let CLOSED = 6; // Закрыт
let WAITING_USER_RATE = 7; // Ожидает подтверждения завершения
let WAITING_EQUIPMENT = 8; // В ожидании выделения техники
let WAITING_SUPPLY = 9; // Направлен внешнему поставщику
let INFO_GIVEN = 10; // Информация предоставлена
let WRONG_WAY = 11; // Неверно направлен
let RE_OPEN = 12; // Направлен повторно
let WAITING_MASS_INCIDENT = 13; //Ожидает закрытия массового инцидента
let IN_AGREEMENT = 91; // На согласовании
let AGREED = 15; // Согласовано
let NOT_AGREED = 16; // Не согласовано

/**
 * Рекурсивная функция ищет actionID работы
 */
function recursiveFilter(arr, currentUserId) {
  var matches = [];
  if (!Array.isArray(arr)) return matches;

  arr.forEach(function(x) {
    if (x.typeID == 'ASSIGNMENT_ITEM' && !x.finished && x.responsibleUserID == currentUserId) matches.push(x);

    if (x.subProcesses.length > 0) {
      var childResults = recursiveFilter(x.subProcesses, currentUserId);
      if (childResults.length) matches = matches.concat(childResults);
    }
  });

  return matches;
}

/**
 * Создаем класс jSynergy
 */
let jSynergy = new __classSynergy();
jSynergy.setConnection(false, login, password);


try {
  jSynergy.server.load(dataUUID);

  let registryDocList = [];
  let operation_records = jSynergy.server.getValue('itsm_form_incidents_operation_records');
  let operation_status = jSynergy.server.getValue('itsm_form_incidents_operation_status');
  let operation_responsible = jSynergy.server.getValue('itsm_form_incidents_operation_responsible');
  let operation_responsibleDepartment = jSynergy.server.getValue('itsm_form_incidents_operation_responsibleDepartment');
  let operation_type = jSynergy.server.getValue('itsm_form_incidents_operation_type');
  let operation_servicelink = jSynergy.server.getValue('itsm_form_incidents_operation_servicelink');
  let operation_decisiontype = jSynergy.server.getValue('itsm_form_incidents_operation_decisiontype');
  let operation_decisiondescription = jSynergy.server.getValue('itsm_form_incidents_operation_decisiondescription');
  let operation_massIncident = jSynergy.server.getValue('itsm_form_incidents_operation_mass_incident');
  let system_user = jSynergy.server.getValue('itsm_form_incidents_operation_system_user');
  let operation_request = jSynergy.server.getValue('itsm_form_incidents_operation_request');

  let doesntHaveWork = [];

  if (operation_records.key) {
    registryDocList = operation_records.key.split(';');
    registryDocList.forEach(function(item) {
      /**
       * Убиваем работу х1
       */
      let processes = recursiveFilter(
        jSynergy.server.api('workflow/get_execution_process?documentID=' + item),
        system_user.key
      );

      if (!processes || !processes[0]) {
        doesntHaveWork.push(item);
        return;
      }
      processes = processes[0].actionID;
      let formWork = jSynergy.server.api("rest/api/workflow/work/get_form_for_result?formCode=itsm_form_incident_wcf_new&workID=" + processes);

      jSynergy.server.load(formWork.dataUUID)

      jSynergy.server.setValue('itsm_form_incident_wcf_status', {
        key: '' + operation_status.key,
        value: '' + operation_status.value
      });

      switch (+operation_status.key) {
        case IN_QUEUE:
          if (operation_responsible && operation_responsible.value && operation_responsible.key) {
            jSynergy.server.setValue('itsm_form_incident_wcf_responsible', {
              key: '' + operation_responsible.key,
              value: '' + operation_responsible.value
            });
          }

          if (operation_responsibleDepartment && operation_responsibleDepartment.value && operation_responsibleDepartment.key) {
            jSynergy.server.setValue('itsm_form_incident_wcf_responsibleDepartment', {
              key: '' + operation_responsibleDepartment.key,
              value: '' + operation_responsibleDepartment.value
            });
          }

          if (operation_type && operation_type.value && operation_type.key) {
            jSynergy.server.setValue('itsm_form_incident_wcf_type', {
              key: '' + operation_type.key,
              value: '' + operation_type.value
            });
          }

          if (operation_servicelink && operation_servicelink.value && operation_servicelink.key) {
            jSynergy.server.setValue('itsm_form_incident_wcf_servicelink', {
              key: '' + operation_servicelink.key,
              valueID: "" + operation_servicelink.key,
              value: '' + operation_servicelink.value
            });
          }
          break;
        case WAITING_MASS_INCIDENT:
          if (operation_responsible && operation_responsible.value && operation_responsible.key) {
            jSynergy.server.setValue('itsm_form_incident_wcf_responsible', {
              key: '' + operation_responsible.key,
              value: '' + operation_responsible.value
            });
          }
          if (operation_responsibleDepartment && operation_responsibleDepartment.value && operation_responsibleDepartment.key) {
            jSynergy.server.setValue('itsm_form_incident_wcf_responsibleDepartment', {
              key: '' + operation_responsibleDepartment.key,
              value: '' + operation_responsibleDepartment.value
            });
          }
          if (operation_type && operation_type.value && operation_type.key) {
            jSynergy.server.setValue('itsm_form_incident_wcf_type', {
              key: '' + operation_type.key,
              value: '' + operation_type.value
            });
          }
          if (operation_servicelink && operation_servicelink.value && operation_servicelink.key) {
            jSynergy.server.setValue('itsm_form_incident_wcf_servicelink', {
              key: '' + operation_servicelink.key,
              valueID: "" + operation_servicelink.key,
              value: '' + operation_servicelink.value
            });
          }
          if (operation_massIncident && operation_massIncident.value && operation_massIncident.key) {
            jSynergy.server.setValue('itsm_form_incident_wcf_mass_reglink', {
              key: '' + operation_massIncident.key,
              valueID: "" + operation_massIncident.key,
              value: '' + operation_massIncident.value
            });
          }
          break;
        case WAITING_USER:
          if (operation_type && operation_type.value && operation_type.key) {
            jSynergy.server.setValue('itsm_form_incident_wcf_type', {
              key: '' + operation_type.key,
              value: '' + operation_type.value
            });
          }
          if (operation_servicelink && operation_servicelink.value && operation_servicelink.key) {
            jSynergy.server.setValue('itsm_form_incident_wcf_servicelink', {
              key: '' + operation_servicelink.key,
              valueID: "" + operation_servicelink.key,
              value: '' + operation_servicelink.value
            });
          }
          if (operation_request && operation_request.value) {
            jSynergy.server.setValue('itsm_form_incident_wcf_return_to_author', {value: '' + operation_request.value});
          }
          break;
        case WAITING_USER_RATE:
          if (operation_decisiontype && operation_decisiontype.value && operation_decisiontype.key) {
            jSynergy.server.setValue('itsm_form_incident_wcf_decisiontype', {
              key: '' + operation_decisiontype.key,
              value: '' + operation_decisiontype.value
            });
          }

          if (operation_decisiondescription && operation_decisiondescription.value) {
            jSynergy.server.setValue('itsm_form_incident_wcf_decisiondescription', operation_decisiondescription.value || '');
          }
          break;
      }

      jSynergy.server.setValue('isView', {
        values: ['1']
      });

      let data = {
        workID: "" + processes,
        completionForm: 'FORM',
        type: 'work',
        file_identifier: formWork.file_identifier
      }

      jSynergy.server.api('workflow/work/set_result', {
        type: 'POST',
        data: data
      });

      jSynergy.server.save();
    });
  }

  result = true;
  if (doesntHaveWork.length > 0) {
    message = "Для работ " + JSON.stringify(doesntHaveWork) + " нет созданных работ";
  } else {
    message = 'Успешно по всем обращениям';
  }
} catch (err) {
  message = err.message;
  result = false;
}
