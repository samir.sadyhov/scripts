let API = {
  getHttpClient: function(){
    let client = new org.apache.commons.httpclient.HttpClient();
    let creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(login, password);
    client.getParams().setAuthenticationPreemptive(true);
    client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
    return client;
  },
  httpGetMethod: function(methods, type) {
    let client = this.getHttpClient();
    let get = new org.apache.commons.httpclient.methods.GetMethod("http://127.0.0.1:8080/Synergy/" + methods);
    get.setRequestHeader("Content-type", "application/json");
    client.executeMethod(get);
    let resp = get.getResponseBodyAsString();
    get.releaseConnection();
    return type == 'text' ? resp : JSON.parse(resp);
  },
  httpPostMethod: function(methods, params, contentType) {
    let client = this.getHttpClient();
    let post = new org.apache.commons.httpclient.methods.PostMethod("http://127.0.0.1:8080/Synergy/" + methods);
    if(contentType) post.setRequestBody(JSON.stringify(params));
    else for(let key in params) post.addParameter(key, params[key]);
    post.setRequestHeader("Content-type", contentType || "application/x-www-form-urlencoded; charset=utf-8");
    let resp = client.executeMethod(post);
    post.releaseConnection();
    return resp;
  },
  getFormData: function(asfDataId) {
    return this.httpGetMethod("rest/api/asforms/data/" + asfDataId)
  },
  saveFormData: function(formID, asfDataId, asfData) {
    return this.httpPostMethod("rest/api/asforms/form/multipartdata", {
      form: formID,
      uuid: asfDataId,
      data: "\"data\":" + JSON.stringify(asfData)
    });
  },
  getProcesses: function(documentID) {
    return this.httpGetMethod("rest/api/workflow/get_execution_process?documentID=" + documentID);
  },
  getFormForResult: function(formCode, workID) {
    return this.httpGetMethod("rest/api/workflow/work/get_form_for_result?formCode=" + formCode + "&workID=" + workID);
  },
  finishWork: function(actionID, file_identifier) {
    return this.httpPostMethod("rest/api/workflow/work/set_result", {
        workID: actionID,
        completionForm: "FORM",
        type: 'work',
        file_identifier: file_identifier
    });
  },
  getAsfDataId: function(documentID) {
    return this.httpGetMethod("rest/api/formPlayer/getAsfDataUUID?documentID=" + documentID, 'text');
  }
};

let log = {
  parse: function(args) {
    let result = [];
    for (let x in args) result.push(JSON.stringify(args[x], null, 4));
    return documentID + '\n' + result.join('\n');
  },
  info: function() {
    console.info(this.parse(arguments));
  },
  error: function() {
    console.error(this.parse(arguments));
  }
}

let UTILS = {
  getValue: function(data, cmpID) {
    data = data.data ? data.data : data;
    for(let i = 0; i < data.length; i++)
    if (data[i].id === cmpID) return data[i];
  },
  setValue: function(asfData, cmpID, data) {
    let field = this.getValue(asfData, cmpID);
    for (let key in data) {
      if(key === 'id' || key === 'type') continue;
      field[key] = data[key];
    }
    return field;
  },
  getActionIds: function(processes) {
    let result = [];
    function search(p) {
      p.forEach(function(process) {
        if (process.code == "problem_change" && !process.finished) {
          result.push(process.actionID);
        }
        if (process.subProcesses.length > 0) search(process.subProcesses);
      });
    }
    search(processes);
    return result;
  }
};


var result = true;
var message = 'ok';

try {

  let currentFormData = API.getFormData(dataUUID);
  let problemDocID = UTILS.getValue(currentFormData, 'itsm_form_change_problemlink');

  if(!problemDocID) throw new Error('Не найдена ссылка на проблему');
  if(!problemDocID.key) throw new Error('Не найдена ссылка на проблему');

  let problemFormData = API.getFormData(API.getAsfDataId(problemDocID.key));

  let problemStatus = UTILS.getValue(problemFormData, 'itsm_form_problem_status');

  if(problemStatus.key != "4") throw new Error('изменение статуса не требуется');

  let oldStatusProblem = UTILS.getValue(problemFormData, "itsm_form_problem_oldStatus");
  let oldStatusKey = oldStatusProblem.key;
  let oldStatusValue = oldStatusProblem.value;

  //Возвращаем предыдущий статус
  UTILS.setValue(problemFormData, "itsm_form_problem_status", oldStatusProblem);
  //Сохраняем текущий статус как старый
  UTILS.setValue(problemFormData, "itsm_form_problem_oldStatus", {key: "4", value: "Ожидает изменений"});

  API.saveFormData(problemFormData.form, problemFormData.uuid, problemFormData.data);

  let problemProcesses = API.getProcesses(problemDocID.key);
  let actions = UTILS.getActionIds(problemProcesses);

  if(actions.length === 0) throw new Error('Не найдено работ для завершения');

  let resultFormWork = API.getFormForResult("itsm_form_problem_wcf", actions[0]);
  let resultFormWorkData = API.getFormData(resultFormWork.dataUUID);
  UTILS.setValue(resultFormWorkData, "itsm_form_problem_wcf_status", {key: oldStatusKey, value: oldStatusValue});
  API.saveFormData(resultFormWorkData.form, resultFormWorkData.uuid, resultFormWorkData.data);
  API.finishWork(actions[0], resultFormWork.file_identifier);

  API.finishWork(actions[0], "Работа завершена автоматически");

} catch (err) {
  log.error(err.message);
  message = err.message;
}
