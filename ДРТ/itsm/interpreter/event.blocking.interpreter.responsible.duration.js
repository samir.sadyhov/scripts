// Создаем класс jSynergy
let jSynergy = new __classSynergy();
jSynergy.setConnection(false, login, password);

try {
  jSynergy.server.load(dataUUID);

  let priority = jSynergy.server.getValue('itsm_form_incident_priority');

  let author = jSynergy.server.getValue('itsm_form_incident_author');
  author = jSynergy.server.api('filecabinet/user/' + author.key + '?getGroups=true');

  let serviceLink = jSynergy.server.getValue('itsm_form_incident_servicelink');

  if (!serviceLink) throw new Error('Услуга не выбрана');

  let serviceDataUUID = jSynergy.server.ApiUtils.getDataUUID(serviceLink.key);
  jSynergy.server.load(serviceDataUUID);
  let serviceSLA = jSynergy.server.converTable('itsm_form_service_sla');
  if (!serviceSLA.length) throw new Error('У услуги время не определено на разрешение запроса');

  let responsibleDuration = [];
  serviceSLA.forEach(function(serviceGroup) {
    if (serviceGroup.hasOwnProperty('group') && serviceGroup.group.hasOwnProperty('key')) {
      let keys = serviceGroup.group.key.split(";")
      keys.forEach(function(key) {
        author.groups.forEach(function(authorGroup) {
          if (key.substring(2) === authorGroup.groupID) {
            switch (new String(priority.key).toLocaleLowerCase()) {
              case "1":
                responsibleDuration.push(serviceGroup.on_critical_time.value);
                break;
              case "2":
                responsibleDuration.push(serviceGroup.on_high_time.value);
                break;
              case "3":
                responsibleDuration.push(serviceGroup.on_medium_time.value);
                break;
              case "4":
                responsibleDuration.push(serviceGroup.on_low_time.value);
                break;
              case "5":
                responsibleDuration.push(serviceGroup.on_verylow_time.value);
                break;
            }
          }
        })
      })
    }
  });

  if (!responsibleDuration.length) { // если инициатора нет ни в одной группе
    serviceSLA.forEach(function(serviceGroup) {
      responsibleDuration.push(serviceGroup.on_medium_time.value);
    });
  }

  let lowDuration = null;
  responsibleDuration.forEach(function(x) {
    if ((!lowDuration) || (lowDuration && lowDuration instanceof Object && +lowDuration > +x)) lowDuration = x;
  });

  jSynergy.server.selectForm(dataUUID);
  jSynergy.server.setValue("itsm_form_incident_responsible_duration", {
    value: "" + (lowDuration || 8) + "ч"
  });
  jSynergy.server.save();

  result = true;
  message = 'OK';
} catch (err) {
  result = true;
  message = err.message;
}
