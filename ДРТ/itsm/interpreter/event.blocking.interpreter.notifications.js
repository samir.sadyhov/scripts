var result = true;
var message = "";
var addressMsg = "";

function parseSettings(settings, registryCode) {
  let tbi = settings.data.filter(function(x){
    if(x.id.substring(0, x.id.indexOf("-b")) == "registry_code" && x.value == registryCode) return x;
  })[0];
  if(!tbi) return null;
  tbi = tbi.id.substring(tbi.id.indexOf("-b"));

  return {
    addressee: UTILS.getValue(settings, 'addressee' + tbi).value || '',
    theme: UTILS.getValue(settings, 'theme' + tbi).value || '',
    subject: UTILS.getValue(settings, 'text' + tbi).value || ''
  }
}

function parseAddress(data, addressee) {
  let userIDs = [];
  addressee.split(',').forEach(function(cmp){
    let tmpComp = UTILS.getValue(data, cmp);
    if(tmpComp && tmpComp.hasOwnProperty('key')) {
      tmpComp.key.split(';').forEach(function(item){
        if(item.substring(0,1) == 'g') {
          let users = API.httpGetMethod('rest/api/userchooser/search?showAll=true&groupID=' + item.substring(2));
          users.forEach(function(user){
            userIDs.push(user.userID);
          });
        } else {
          userIDs.push(item);
        }
      });
    } else {
      addressMsg += "Поле адресата с ID " + cmp + " пустое или не найдено на форме;\n";
    }
  });
  return userIDs.uniq();
}

function getPublicAddress(){
  let client = new org.apache.commons.httpclient.HttpClient();
  let get = new org.apache.commons.httpclient.methods.GetMethod("http://127.0.0.1:8080/itsm/rest/config/public-address");
  client.executeMethod(get);
  let resp = get.getResponseBodyAsString();
  get.releaseConnection();
  return resp.replace("Synergy", "");
}

try {
  let settings = API.httpGetMethod("rest/api/registry/data_ext?registryCode=itsm_registry_notification_settings&countInPart=1&loadData=false");
  if(settings.count == 0) throw new Error('Не найдены настройки уведомлений');

  settings = API.getFormData(settings.data[0].dataUUID);
  settings = UTILS.getValue(settings, "itsm_form_notification_settings_table");
  if(!settings || !settings.hasOwnProperty("data")) throw new Error('Не найдены настройки уведомлений');

  let registryCode = API.getDocumentInfo(documentID);
  registryCode = API.httpGetMethod("rest/api/registry/info?registryID=" + registryCode.registryID).code;

  settings = parseSettings(settings, registryCode);
  if(!settings) throw new Error('Не найдены настройки уведомлений для реестра с кодом ' + registryCode);
  if(!settings.addressee || settings.addressee.value == '') throw new Error('Пустое поле кому');

  let currentFormData = API.getFormData(dataUUID);

  UTILS.getParams(settings.theme).forEach(function(id) {
    settings.theme = settings.theme.replace('${'+id+'}', UTILS.getValue(currentFormData, id).value || '');
  });
  UTILS.getParams(settings.subject).forEach(function(id) {
    settings.subject = settings.subject.replace('${'+id+'}', UTILS.getValue(currentFormData, id).value || '');
  });

  settings.subject += '<br><br>Для просмотра документа данной работы перейдите по <a href="' + getPublicAddress() + 'Synergy/Synergy.html?locale=ru#submodule=common&action=open_document&document_identifier=' + documentID + '">ссылке</a>';

  settings.users = parseAddress(currentFormData, settings.addressee);

  if(settings.users.length == 0) throw new Error('Не найдены пользователи для отправки уведомления');

  if(settings.users.length > 100) {
    message = "Выбрано более 100 адресатов, устанавливается ограничение 100\n\n";
    settings.users = settings.users.slice(0, 101);
  };

  API.sendNotification({
    header: settings.theme,
    message: settings.subject,
    users: settings.users
  });

  message += "Уведомления отправлены для " + settings.users + " пользователей";

} catch (err) {
  log.error(err.message);
  message = err.message;
  if(addressMsg != "") message += "\n" + addressMsg;
}
