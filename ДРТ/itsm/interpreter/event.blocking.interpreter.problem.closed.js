var result = true;
var message = "ok";

try {

  let currentFormData = API.getFormData(dataUUID);
  let checkBox = UTILS.getValue(currentFormData, "itsm_form_problem_makenew_knowledge");

  if(!checkBox) throw new Error("Чекбокс 'Создать запись в базе знаний' не отмечен");
  if(checkBox.values.indexOf('1') === -1) throw new Error("Чекбокс 'Создать запись в базе знаний' не отмечен");

  let matching = [];
  matching.push({from: 'itsm_form_problem_description', to: 'itsm_form_knowledgebase_description'}); //Подробное описание
  matching.push({from: 'itsm_form_problem_responsiblemanager', to: 'itsm_form_knowledgebase_manager'}); //Ответственный менеджер
  matching.push({from: 'itsm_form_problem_decisiontype', to: 'itsm_form_knowledgebase_decisiontype'}); //Код закрытия
  matching.push({from: 'itsm_form_problem_decisiondescription', to: 'itsm_form_knowledgebase_decisiondescription'}); //Описание решения

  let currentFormDescription = API.getFormDescription(currentFormData.form);
  let knowledgeAsfData = [];

  matching.forEach(function(id) {
    let fromData = UTILS.getValue(currentFormData, id.from);

    if(fromData && fromData.value) {
      let field = UTILS.createField({
        id: id.to,
        type: UTILS.getCmpType(currentFormDescription, id.from)
      });

      for(let key in fromData) {
        if(key === 'id' || key === 'type') continue;
        field[key] = fromData[key];
      }

      if(field.id == 'itsm_form_knowledgebase_decisiontype' && field.key == '4') field.key = '5'

      knowledgeAsfData.push(field);
    }
  });

  knowledgeAsfData.push(UTILS.createField({
    id: "itsm_form_knowledgebase_source",
    type: "listbox",
    value: "Проблема",
    key: "2"
  }));

  let table4 = UTILS.createField({
    id: "itsm_form_knowledgebase_table4",
    type: "appendable_table",
    key: "",
    data: []
  });

  table4.data.push(UTILS.createField({
    id: "itsm_form_knowledgebase_problemlink-b1",
    type: "reglink",
    key: documentID,
    valueID: documentID
  }));

  knowledgeAsfData.push(table4);

  let newKnowledgeDoc = API.createDocRCC("itsm_registry_knowledgebase", knowledgeAsfData);
  if(newKnowledgeDoc.errorCode != 0) throw new Error("Ошибка создания записи в Базе знаний");

  newKnowledgeDoc = API.activateDoc(newKnowledgeDoc.documentID);
  if(newKnowledgeDoc.errorCode != 0) throw new Error("Ошибка активации записи в Базе знаний");

  log.info("Активация записи в Базе знаний", newKnowledgeDoc);

  message = "Запись создана\ndocumentID: " + newKnowledgeDoc.documentID;

} catch (err) {
  log.error(err.message);
  message = err.message;
}
