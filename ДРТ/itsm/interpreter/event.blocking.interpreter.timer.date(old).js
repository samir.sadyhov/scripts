let API = {
  getHttpClient: function(){
    let client = new org.apache.commons.httpclient.HttpClient();
    let creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(login, password);
    client.getParams().setAuthenticationPreemptive(true);
    client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
    return client;
  },
  httpGetMethod: function(methods, type) {
    let client = this.getHttpClient();
    let get = new org.apache.commons.httpclient.methods.GetMethod("http://127.0.0.1:8080/Synergy/" + methods);
    get.setRequestHeader("Content-type", "application/json");
    client.executeMethod(get);
    let resp = get.getResponseBodyAsString();
    get.releaseConnection();
    return type == 'text' ? resp : JSON.parse(resp);
  },
  httpPostMethod: function(methods, params, contentType) {
    let client = this.getHttpClient();
    let post = new org.apache.commons.httpclient.methods.PostMethod("http://127.0.0.1:8080/Synergy/" + methods);
    if(contentType) post.setRequestBody(JSON.stringify(params));
    else for(let key in params) post.addParameter(key, params[key]);
    post.setRequestHeader("Content-type", contentType || "application/x-www-form-urlencoded; charset=utf-8");
    let resp = client.executeMethod(post);
    post.releaseConnection();
    return resp;
  },
  getFormData: function(asfDataId) {
    return this.httpGetMethod("rest/api/asforms/data/" + asfDataId)
  },
  getSynergyCalendar: function(start, finish) {
    return this.httpGetMethod("rest/api/settings/calendar?date_start=" + start + "&date_finish=" + finish);
  },
  saveFormData: function(formID, asfDataId, asfData) {
    return this.httpPostMethod("rest/api/asforms/form/multipartdata", {
      form: formID,
      uuid: asfDataId,
      data: "\"data\":" + JSON.stringify(asfData)
    });
  }
};

let log = {
  parse: function(args) {
    let result = [];
    for (let x in args) result.push(JSON.stringify(args[x], null, 4));
    return documentID + '\n' + result.join('\n');
  },
  info: function() {
    console.info(this.parse(arguments));
  },
  error: function() {
    console.error(this.parse(arguments));
  }
}

let UTILS = {
  getValue: function(data, cmpID) {
    data = data.data ? data.data : data;
    for(let i = 0; i < data.length; i++)
    if (data[i].id === cmpID) return data[i];
  },
  setValue: function(asfData, cmpID, data) {
    let field = this.getValue(asfData, cmpID);
    for (let key in data) {
      if(key === 'id' || key === 'type') continue;
      field[key] = data[key];
    }
    return field;
  }
};


function formatDate(datetime, time) {
  let result = datetime.getFullYear() + '-' +
    ('0' + (datetime.getMonth() + 1)).slice(-2) + '-' +
    ('0' + datetime.getDate()).slice(-2) + ' ' +
    ('0' + datetime.getHours()).slice(-2) + ':' +
    ('0' + datetime.getMinutes()).slice(-2) + ':' +
    ('0' + datetime.getSeconds()).slice(-2);
  return time ? result : result.substring(0, result.indexOf(' '));
}

function getStartTimeDay(calendar, date) {
  date = date.split(' ')[0];
  if (!calendar[date]) return null;
  if (calendar[date][0].type == "holiday") return null;
  return calendar[date][0].start.substring(0, 2) * 1;
}

function getFinishTimeDay(calendar, date) {
  date = date.split(' ')[0];
  if (!calendar[date]) return null;
  if (calendar[date][0].type == "holiday") return null;
  return calendar[date][calendar[date].length - 1].finish.substring(0, 2) * 1;
}

function isHoliday(calendar, date) {
  date = date.split(' ')[0];
  if (!calendar[date]) return false;
  return calendar[date][0].type == "holiday" || false;
}

function getPeriod() {
  let f = new Date();
  f.setMonth(f.getMonth() + 3);
  return {
    start: formatDate(new Date()),
    finish: formatDate(f)
  };
}

function addWorkHours(calendar, date, hours) {
  let tmpDate = new Date(date);
  let tmpDateFormated = formatDate(tmpDate, true);
  let startHour = getStartTimeDay(calendar, tmpDateFormated);
  let finishHour = getFinishTimeDay(calendar, tmpDateFormated);

  function update() {
    tmpDate.setDate(tmpDate.getDate() + 1);
    tmpDateFormated = formatDate(tmpDate, true);
    if (isHoliday(calendar, tmpDateFormated)) update();

    startHour = getStartTimeDay(calendar, tmpDateFormated);
    finishHour = getFinishTimeDay(calendar, tmpDateFormated);
    tmpDate.setHours(startHour);
  }

  if (isHoliday(calendar, tmpDateFormated)) update();
  if (tmpDate.getHours() < startHour) tmpDate.setHours(startHour);
  if (tmpDate.getHours() > finishHour) update();

  for (let i = 0; i < hours; i++) {
    tmpDate.setHours(tmpDate.getHours() + 1);
    if (tmpDate.getHours() > finishHour) update();
  }
  return tmpDate;
}

let result = true;
let message = "ok";

try {

  let period = getPeriod();
  let synergyCalendar = API.getSynergyCalendar(period.start, period.finish);
  let newDate = addWorkHours(synergyCalendar, new Date(), 16);
  newDate = formatDate(newDate, true);

  let currentFormData = API.getFormData(dataUUID);
  UTILS.setValue(currentFormData, "itsm_form_incident_timerDate", {value: newDate, key: newDate});
  API.saveFormData(currentFormData.form, currentFormData.uuid, currentFormData.data);

  message = "Дата таймера: " + newDate;

} catch (err) {
  log.error(err.message);
  message = err.message;
}
