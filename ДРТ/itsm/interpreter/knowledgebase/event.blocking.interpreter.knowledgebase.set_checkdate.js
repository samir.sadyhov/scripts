var result = true;
var message = "ok";

function customFormatDate(datetime){
  datetime = datetime.split(/\D/);
  return datetime[2] + '.' + datetime[1] + '.' + datetime[0] + ' ' + datetime[3] + ':' + datetime[4];
}

try {
  let currentFormData = API.getFormData(dataUUID);
  let currDate = UTILS.getCurrentDateParse();

  UTILS.setValue(currentFormData, 'itsm_form_knowledgebase_checkdate', {key: currDate, value: customFormatDate(currDate), type: "date"});
  UTILS.setValue(currentFormData, 'itsm_form_knowledgebase_status', {key: '1', value: 'В использовании', type: 'listbox'});

  message = customFormatDate(currDate);

  API.mergeFormData(currentFormData);

} catch (err) {
  log.error(err.message);
  message = err.message;
}
