var result = true;
var message = "ok";

function getActionIds(processes, formCode) {
  let result = [];

  function search(p) {
    p.forEach(function(process) {
      if (process.completionFormCode == formCode && process.finished) {
        result.push(process);
      }
      if (process.subProcesses.length > 0) search(process.subProcesses);
    });
  }
  search(processes);

  return result.sort(function(a, b) {
    return UTILS.parseDateTime(b.finished) - UTILS.parseDateTime(a.finished);
  }).map(function(item) {
    return {actionID: item.actionID, finished: item.finished};
  });
}

function customFormatDate(datetime){
  datetime = datetime.split(/\D/);
  return datetime[2] + '.' + datetime[1] + '.' + datetime[0] + ' ' + datetime[3] + ':' + datetime[4];
}

try {
  let processes = API.getProcesses(documentID);
  let actions = getActionIds(processes, 'itsm_knowledgebase_completion_form');

  let completionFormDataUUID = actions.map(function(item) {
    return API.getWorkCompletionData(item.actionID).result.dataUUID;
  }).filter(function(dataUUID){ return dataUUID != null})[0];

  if(!completionFormDataUUID) throw new Error('Не найдена форма завершения');

  let matching = [];
  matching.push({from: 'itsm_form_knowledgebase_wcf_status', to: 'itsm_form_knowledgebase_status'});
  matching.push({from: 'itsm_form_knowledgebase_wcf_checkdate', to: 'itsm_form_knowledgebase_checkdate'});

  let completionFormData = API.getFormData(completionFormDataUUID);
  let currentFormData = API.getFormData(dataUUID);


  matching.forEach(function(id) {
    let fromData = UTILS.getValue(completionFormData, id.from);
    if(fromData) UTILS.setValue(currentFormData, id.to, fromData);
  });

  UTILS.setValue(currentFormData, 'itsm_form_knowledgebase_rout_modify', {type: "check", values: [null], keys: [null]});

  API.mergeFormData(currentFormData);

} catch (err) {
  log.error(err.message);
  message = err.message;
}
