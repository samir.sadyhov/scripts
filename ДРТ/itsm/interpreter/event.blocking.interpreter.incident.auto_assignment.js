function customFormatDate(datetime){
  datetime = datetime.split(/\D/);
  return datetime[2] + '.' + datetime[1] + '.' + datetime[0] + ' г. ' + datetime[3] + ':' + datetime[4];
}

function addHours(h) {
  let client = new org.apache.commons.httpclient.HttpClient();
  let get = new org.apache.commons.httpclient.methods.GetMethod("http://127.0.0.1:8080/itsm/rest/addHours?hours=" + h);
  client.executeMethod(get);
  let res = String(get.getResponseBodyAsString());
  get.releaseConnection();
  return res;
}

var result = true;
var message = "ok";

try {
  let currentFormData = API.getFormData(dataUUID);
  let servicelink = UTILS.getValue(currentFormData, 'itsm_form_incident_servicelink');
  if(!servicelink || !servicelink.hasOwnProperty('key')) throw new Error('Не найдено ссылки на Cервис');

  let serviceData = API.getFormData(API.getAsfDataId(servicelink.key));
  let auto_assign = UTILS.getValue(serviceData, 'itsm_form_service_auto_assign');
  if(!auto_assign || !auto_assign.hasOwnProperty('values')) throw new Error('Автоматическое назначение не включено');
  if(!auto_assign.values[0]) throw new Error('Автоматическое назначение не включено');

  let group_users = UTILS.getValue(serviceData, 'itsm_form_service_support_group_users');
  if(!group_users || !group_users.hasOwnProperty('key')) throw new Error('Не найдено исполнителей');

  UTILS.setValue(currentFormData, 'itsm_form_incident_responsible', group_users);
  UTILS.setValue(currentFormData, 'itsm_form_incident_status', {key: '2', value: 'На очереди'});
  UTILS.setValue(currentFormData, 'itsm_form_incident_auto_assignment', {key: '1', value: 'да'});

  let department = UTILS.getValue(serviceData, 'itsm_form_service_support_department');
  if(department && department.hasOwnProperty('key')) UTILS.setValue(currentFormData, 'itsm_form_incident_responsibleDepartment', department);

  //Время реакции -------------------------------------------------------------------------------------------------------------
  let timeSpend = 0;
  let duration = 0;
  let firstLineTime = UTILS.getValue(currentFormData, 'itsm_form_incident_firstLineTime');
  let managerDuration = UTILS.getValue(currentFormData, 'itsm_form_incident_manager_duration');

  if(firstLineTime && firstLineTime.hasOwnProperty('key')) timeSpend = Number(firstLineTime.key) || 0;
  if(managerDuration && managerDuration.hasOwnProperty('value')) duration = Number(managerDuration.value.replace(/\D+/g,""));

  let newDate = addHours(duration - timeSpend);

  UTILS.setValue(currentFormData, 'itsm_form_incident_manager_dateFinish', {key: newDate, value: customFormatDate(newDate)});
  //Время реакции -------------------------------------------------------------------------------------------------------------

  //Время перехода на следующий статус
  UTILS.setValue(currentFormData, 'itsm_form_incident_prev_status_time', {key: UTILS.getCurrentDateParse(), value: customFormatDate(UTILS.getCurrentDateParse())});

  message = 'Обращение назначено на ' + group_users.value;

  API.mergeFormData(currentFormData);

} catch (err) {
  log.error(err.message);
  message = err.message;
}
