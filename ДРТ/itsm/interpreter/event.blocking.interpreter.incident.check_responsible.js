var result = true;
var message = "ok";

function processesFilter(processes) {
  let result = [];
  function search(p) {
    p.forEach(function(process) {
      if (process.typeID == 'ASSIGNMENT_ITEM' && process.finished) result.push(process);
      if (process.subProcesses.length > 0) search(process.subProcesses);
    });
  }
  search(processes);
  return result.sort(function(a, b) {
    return UTILS.parseDateTime(b.finished) - UTILS.parseDateTime(a.finished);
  });
};

try {
  let currentFormData = API.getFormData(dataUUID);

  let responsible = UTILS.getValue(currentFormData, "itsm_form_incident_responsible");
  let responsiblemanager = UTILS.getValue(currentFormData, "itsm_form_incident_responsiblemanager");

  let processes = processesFilter(API.getProcesses(documentID));

  let completionFormDataUUID = processes.map(function(x) {
    return API.getWorkCompletionData(x.actionID).result.dataUUID;
  }).filter(function(uuid){ return uuid != null})[0];

  if(!completionFormDataUUID) throw new Error('Не найдена форма завершения');

  let completionFormData = API.getFormData(completionFormDataUUID);
  let currentUser = UTILS.getValue(completionFormData, "itsm_form_incident_wcf_current_user");
  let currentUserDep = UTILS.getValue(completionFormData, "itsm_form_incident_wcf_current_user_Department");

  let saveAsfData = [];
  let messageResult = '';

  // Если поле исполнитель пустое, то нужно скопировать из последней ФЗ инцидента
  if(!responsible || !responsible.hasOwnProperty('key') || responsible.key == '') {
    UTILS.setValue(saveAsfData, 'itsm_form_incident_responsible', currentUser);
    UTILS.setValue(saveAsfData, 'itsm_form_incident_responsibleDepartment', currentUserDep);

    messageResult = 'Исполнитель: ' + currentUser.value;
  }

  //если вдруг грохнули оператора
  if(!responsiblemanager || !responsiblemanager.hasOwnProperty('key') || responsiblemanager.key == '') {
    UTILS.setValue(saveAsfData, 'itsm_form_incident_responsiblemanager', currentUser);

    if(messageResult.length) messageResult += '; ';

    messageResult += 'Оператор: ' + currentUser.value;
  }

  if(saveAsfData.length) {
    API.mergeFormData({
      uuid: dataUUID,
      data: saveAsfData
    });
  } else {
    messageResult = 'Изменений не было';
  }

  message = messageResult;

} catch (err) {
  message = err.message;
}
