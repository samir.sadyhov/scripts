var result = true;
var message = "ok";
try {
  let client = new org.apache.commons.httpclient.HttpClient();
  let get = new org.apache.commons.httpclient.methods.GetMethod("http://127.0.0.1:8080/itsm/rest/addHours?hours=" + 16);
  client.executeMethod(get);
  let newDate = String(get.getResponseBodyAsString());
  get.releaseConnection();

  let form = platform.getFormsManager().getFormData(dataUUID);
  form.load();
  form.setValue("itsm_form_incident_timerDate", newDate);
  form.save();

  message = "Дата таймера: " + newDate;
} catch (err) {
  console.error(err.message);
  message = err.message;
}
