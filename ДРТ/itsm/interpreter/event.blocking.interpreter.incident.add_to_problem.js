var result = true;
var message = "ok";

try {
  let currentFormData = API.getFormData(dataUUID);
  let major_problemlink = UTILS.getValue(currentFormData, 'itsm_form_incident_major_problemlink');

  if(!major_problemlink || !major_problemlink.hasOwnProperty('key')) throw new Error('Не найдено ссылки на проблему');

  let problemData = API.getFormData(API.getAsfDataId(major_problemlink.key));
  let incidentlink = UTILS.getValue(problemData, 'itsm_form_problem_incidentlink');
  let problem_id = UTILS.getValue(problemData, 'itsm_form_problem_id');

  if(!incidentlink) {
    incidentlink = {
      id: "itsm_form_problem_incidentlink",
      type: "reglink",
      value: API.getDocMeaningContent(documentID),
      key: documentID,
      valueID: documentID
    }
  } else if (!incidentlink.hasOwnProperty('key')) {
    incidentlink.value = API.getDocMeaningContent(documentID);
    incidentlink.key = documentID;
    incidentlink.valueID = documentID;
  } else {
    if(incidentlink.key.indexOf(documentID) != -1) throw new Error('Обращение уже было связано с проблемой ' + problem_id.value);
    incidentlink.value += ';' + API.getDocMeaningContent(documentID);
    incidentlink.key += ';' + documentID;
    incidentlink.valueID += ';' + documentID;
  }

  log.info(API.mergeFormData(problemData));
  message = "Обращение связано с проблемой " + problem_id.value;

} catch (err) {
  log.error(err.message);
  message = err.message;
}
