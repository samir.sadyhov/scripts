var result = true;
var message = "ok";

let LOGGER = {
  text: '',
  set: function(text, type){
    this.text += '<br><b>' + UTILS.getCurrentDateParse() + '</b> [' + (type ? type : 'INFO') + '] ' + text;
  },
  get: function(){
    return this.text;
  }
}

function getDocs(){
  let client = API.getHttpClient();
  let get = new org.apache.commons.httpclient.methods.GetMethod("http://127.0.0.1:8080/itsm/rest/asset/documentsNotify");
  get.setRequestHeader("Content-type", "application/json");
  client.executeMethod(get);
  let resp = get.getResponseBodyAsString();
  get.releaseConnection();
  return JSON.parse(resp);
}

function getPublicAddress(){
  let client = new org.apache.commons.httpclient.HttpClient();
  let get = new org.apache.commons.httpclient.methods.GetMethod("http://127.0.0.1:8080/itsm/rest/config/public-address");
  client.executeMethod(get);
  let resp = get.getResponseBodyAsString();
  get.releaseConnection();
  return resp.replace("Synergy", "");
}

function saveResultScript(){
  let currentFormData = API.getFormData(dataUUID);
  UTILS.setValue(currentFormData, "date_start_script", {value: UTILS.getCurrentDateParse(), key: UTILS.getCurrentDateParse()});
  UTILS.setValue(currentFormData, "log", {value: LOGGER.get()});
  API.mergeFormData(currentFormData);
}

try {
  LOGGER.set('<b>Запуск скрипта</b>', 'START');

  let THEME_CMP_ID = 'itsm_form_settings_assets_contract_notification_theme';
  let SUBJECT_CMP_ID = 'itsm_form_settings_assets_contract_notification_subject';
  let SETTINGS = API.httpGetMethod('rest/api/registry/data_ext?registryCode=itsm_registry_settings&fields=' + THEME_CMP_ID + '&fields=' + SUBJECT_CMP_ID);

  if(SETTINGS.recordsCount == 0) throw new Error('Не найдены настройки');
  SETTINGS = SETTINGS.result[0].fieldValue;

  if(!SETTINGS[THEME_CMP_ID] || !SETTINGS[SUBJECT_CMP_ID]) throw new Error('Не найдены настройки для формирования сообщения');

  let docs = getDocs();
  if(!docs || docs.errorCode != 0 || docs.data.length == 0) throw new Error('Не найдены документы для уведомлений');

  LOGGER.set('Список документов:<br>' + JSON.stringify(docs.data, null, 4));

  docs.data.forEach(function(item) {
    let ids = item.userIDs.split(';');
    let names = item.userNames.split(', ');
    let users = ids.map(function(id, i) {
      return {userid: id, name: names[i]}
    });

    users.forEach(function(user) {
      let theme = SETTINGS[THEME_CMP_ID].replace('${itsm_form_asset_id}', item.asset_id || '');

      let subject = SETTINGS[SUBJECT_CMP_ID]
      .replace('${USER_FIO}', user.name || '')
      .replace('${itsm_form_asset_name}', item.asset_name || '')
      .replace('${DOC_TYPE}', item.typeDocument || '')
      .replace('${DATE_FINISHED}', item.dateFinished || '');

      subject += '<br><br>Ссылка на документ: <a href="' + getPublicAddress() + "itsm/rest/file?identifier=" + item.fileID + '">' + item.fileName + '</a>';

      LOGGER.set("Тема: " + theme);
      LOGGER.set("Тело письма:<br>" + subject + "<br>");

      let resultSend = API.sendNotification({
        header: theme,
        message: subject,
        users: [user.userid]
      });

      if(resultSend && resultSend.errorCode == 0) {
        LOGGER.set(resultSend.errorMessage + "<br>");
      } else {
        LOGGER.set("Ошибка отправки письма:<br>" + JSON.stringify(resultSend, null, 4) + "<br>", 'ERROR');
      }

    });
  });

  LOGGER.set('<b>Завершение работы скрипта</b>', 'FINISH');

  saveResultScript();

} catch (err) {
  LOGGER.set(err.message, 'ERROR');
  saveResultScript();
  log.error(err);
  message = err.message;
}
