let API = {
  getHttpClient: function(){
    let client = new org.apache.commons.httpclient.HttpClient();
    let creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(login, password);
    client.getParams().setAuthenticationPreemptive(true);
    client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
    return client;
  },
  httpGetMethod: function(methods, type) {
    let client = this.getHttpClient();
    let get = new org.apache.commons.httpclient.methods.GetMethod("http://127.0.0.1:8080/Synergy/" + methods);
    get.setRequestHeader("Content-type", "application/json");
    client.executeMethod(get);
    let resp = get.getResponseBodyAsString();
    get.releaseConnection();
    return type == 'text' ? resp : JSON.parse(resp);
  },
  httpPostMethod: function(methods, params, contentType) {
    let client = this.getHttpClient();
    let post = new org.apache.commons.httpclient.methods.PostMethod("http://127.0.0.1:8080/Synergy/" + methods);
    if(contentType) post.setRequestBody(JSON.stringify(params));
    else for(let key in params) post.addParameter(key, params[key]);
    post.setRequestHeader("Content-type", contentType || "application/x-www-form-urlencoded; charset=utf-8");
    let resp = client.executeMethod(post);
    post.releaseConnection();
    return resp;
  },
  getFormData: function(asfDataId) {
    return this.httpGetMethod("rest/api/asforms/data/" + asfDataId);
  },
  putInIndex: function(data, elasticKey) {
    let client = this.getHttpClient();
    let put = new org.apache.commons.httpclient.methods.PutMethod("http://localhost:9200/itsm_incident_tagcloud/tagcloud/" + encodeURIComponent(elasticKey));
    let body = new org.apache.commons.httpclient.methods.StringRequestEntity(JSON.stringify(data), "application/json", "UTF-8");
    put.setRequestEntity(body);
    client.executeMethod(put);
    let resp = put.getResponseBodyAsString();
    put.releaseConnection();
    return JSON.parse(resp);
  },
  searchSynonyms: function(str){
    let body = {
      query: "WHERE itsm_form_synonyms_additional LIKE '%" + str + "%' AND code = 'itsm_form_synonyms'",
      parameters: [],
      dynParams: [["itsm_form_synonyms_additional"]],
      startRecord: 0,
      recordsCount: 1,
      showDeleted: false,
      searchInRegistry: true
    };
    let client = this.getHttpClient();
    let post = new org.apache.commons.httpclient.methods.PostMethod("http://127.0.0.1:8080/Synergy/rest/api/asforms/search/advanced");
    post.setRequestHeader("Content-type", "application/json; charset=utf-8");
    post.setRequestBody(JSON.stringify(body));
    client.executeMethod(post);
    let resp = post.getResponseBodyAsString();
    post.releaseConnection();
    return JSON.parse(resp);
  }
};

let log = {
  parse: function(args) {
    let result = [];
    for (let x in args) result.push(JSON.stringify(args[x], null, 4));
    return documentID + '\n' + result.join('\n');
  },
  info: function() {
    console.info(this.parse(arguments));
  },
  error: function() {
    console.error(this.parse(arguments));
  }
}

let UTILS = {
  getValue: function(data, cmpID) {
    data = data.data ? data.data : data;
    for(let i = 0; i < data.length; i++)
    if (data[i].id === cmpID) return data[i];
  },
  setValue: function(asfData, cmpID, data) {
    let field = this.getValue(asfData, cmpID);
    for (let key in data) {
      if(key === 'id' || key === 'type') continue;
      field[key] = data[key];
    }
    return field;
  }
};

//выпиливыние из массива повторяющихся елементов
Array.prototype.uniq = function() {
  return this.filter(function(v, i, a){ return i == a.indexOf(v) });
}
//выпиливаем лишнии символы и цифры, фильтруем слова больше 2-х символов, оставляем уникальные uniq()
String.prototype.parsing = function() {
  return this.toString().replace(/[\n\/]/g, ' ').split(" ")
  .map(function(str) { return str.replace(/[^a-zA-ZА-Яа-яЁё]/gi, '').toUpperCase() })
  .filter(function(str) { if (str.length > 2) return str })
  .uniq();
}

var result = true;
var message = "ok";

try {
  let currentFormData = API.getFormData(dataUUID);

  let theme = (UTILS.getValue(currentFormData, "itsm_form_incident_theme").value || '').parsing();
  let description = (UTILS.getValue(currentFormData, "itsm_form_incident_description").value || '').parsing();

  //Получаем слова исключения
  let exceptions = API.httpGetMethod("rest/api/registry/data_ext?registryCode=itsm_registry_exception&fields=itsm_form_exception_word")
  .result.map(function(item) {
    if(item.fieldValue.itsm_form_exception_word && item.fieldValue.itsm_form_exception_word != '') {
      return item.fieldValue.itsm_form_exception_word.toUpperCase();
    }
  });

  //Выпиливаем слова исключения
  exceptions.forEach(function(exception) {
    if (theme.indexOf(exception) !== -1) theme.splice(theme.indexOf(exception), 1);
    if (description.indexOf(exception) !== -1) description.splice(description.indexOf(exception), 1);
  });

  //выпиливаем "синонимы"
  theme.forEach(function(str, i) {
    let searchResult = API.searchSynonyms(str);
    if(searchResult.length > 0)
    theme[i] = UTILS.getValue(API.getFormData(searchResult[0].dataUUID), "itsm_form_synonyms_main").value.toUpperCase();
  });
  description.forEach(function(str, i) {
    let searchResult = API.searchSynonyms(str);
    if(searchResult.length > 0)
    description[i] = UTILS.getValue(API.getFormData(searchResult[0].dataUUID), "itsm_form_synonyms_main").value.toUpperCase();
  });

  theme = theme.uniq();
  description = description.uniq();

  let indexData = {theme: theme, description: description, deleted: 1};
  log.info(":::::: INDEX DATA ::::::", indexData);
  let resultAddIndex = API.putInIndex(indexData, "tagcloud-" + dataUUID);
  log.info(":::::: RESULT ADD INDEX ::::::", resultAddIndex);

} catch (err) {
  log.error(err.message);
  message = err.message;
}
