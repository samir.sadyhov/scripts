var result = true;
var message = "ok";

function processesFilter(processes, userID) {
  let result = [];
  function search(p) {
    p.forEach(function(x) {
      if (x.typeID == 'ASSIGNMENT_ITEM' && !x.finished && x.responsibleUserID == userID) result.push(x);
      if (x.subProcesses.length > 0) search(x.subProcesses);
    });
  }
  search(processes);
  return result;
}

try {
  let currentFormData = API.getFormData(dataUUID);
  let incidentChild = UTILS.getValue(currentFormData, 'itsm_form_incident_child');
  let currentUser = API.httpGetMethod("rest/api/person/auth");

  if(!incidentChild || !incidentChild.hasOwnProperty('key')) throw new Error('Не найдено ссылок на дочернии инциденты');

  let docs = incidentChild.key.split(';');
  if(docs.length == 0) throw new Error('Не найдено ссылок на дочернии инциденты');

  docs.forEach(function(docID) {
    let processes = processesFilter(API.getProcesses(docID), currentUser.userid);

    if(processes.length != 0) {
      let actionID = processes[0].actionID;
      let resultFormWork = API.getFormForResult("itsm_form_incident_wcf_new", actionID);
      let resultFormWorkData = API.getFormData(resultFormWork.dataUUID);
      UTILS.setValue(resultFormWorkData, "itsm_form_incident_wcf_status", {key: '6', value: 'Закрыто'});
      UTILS.setValue(resultFormWorkData, "itsm_form_incident_wcf_mark", {key: '0', value: '-'});
      UTILS.setValue(resultFormWorkData, "itsm_form_incident_wcf_mark_comment", {value: 'Автоматически закрыто в связи с закрытием родительского массового инцидента'});
      UTILS.setValue(resultFormWorkData, "itsm_form_incident_wcf_decisiontype", {key: '8', value: 'Разрешен (массовый инцидент)'});

      API.saveFormData(resultFormWorkData);

      let incidentFormData = API.getFormData(API.getAsfDataId(docID));
      UTILS.setValue(incidentFormData, "itsm_form_incident_decisiontype", {key: '8', value: 'Разрешен (массовый инцидент)'});
      UTILS.setValue(incidentFormData, "itsm_form_incident_decisiondescription", UTILS.getValue(currentFormData, 'itsm_form_incident_decisiondescription'));
      API.saveFormData(incidentFormData);

      API.finishWork(actionID, resultFormWork.file_identifier);
    }

  });

} catch (err) {
  log.error(err.message);
  message = err.message;
}
