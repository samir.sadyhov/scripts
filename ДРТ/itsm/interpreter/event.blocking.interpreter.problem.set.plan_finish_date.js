var result = true;
var message = "ok";

function addHours(startDate, hours){
  let client = new org.apache.commons.httpclient.HttpClient();
  let get = new org.apache.commons.httpclient.methods.GetMethod("http://127.0.0.1:8080/itsm/rest/addHours?startDate="+startDate+"&hours="+hours);
  client.executeMethod(get);
  let result = String(get.getResponseBodyAsString());
  get.releaseConnection();
  return result;
}

function customFormatDate(datetime){
  datetime = datetime.split(/\D/);
  return datetime[2] + '.' + datetime[1] + '.' + datetime[0] + ' ' + datetime[3] + ':' + datetime[4];
}

try {

  let defaultHours = 40;
  let newDate = null;
  let currentFormData = API.getFormData(dataUUID);
  let servicelink = UTILS.getValue(currentFormData, 'itsm_form_problem_servicelink');
  let problem_priority = UTILS.getValue(currentFormData, 'itsm_form_problem_priority');
  let regdate = UTILS.getValue(currentFormData, 'itsm_form_problem_regdate');

  if(servicelink && servicelink.hasOwnProperty('key')) {
    let priority = {
      1: "on_critical_time", //Критичный
      2: "on_high_time", //Высокий
      3: "on_medium_time", //Средний
      4: "on_low_time", //Низкий
      5: "on_verylow_time" //Очень низкий
    };
    let serviceData = API.getFormData(API.getAsfDataId(servicelink.key));
    newDate = UTILS.getValue(serviceData, priority[problem_priority.key]);

    if(newDate && newDate.hasOwnProperty('key')) {
      newDate = Number(newDate.key) || defaultHours;
      if(newDate === 0) newDate = defaultHours;
    } else {
      newDate = defaultHours;
    }
  } else {
    newDate = defaultHours;
  }

  newDate = addHours(new Date(regdate.key).getTime(), newDate);
  message = "Плановая дата завершения: " + newDate;
  UTILS.setValue(currentFormData, 'itsm_form_problem_plan_finish_date', {value: customFormatDate(newDate), key: newDate});
  API.saveFormData(currentFormData);

} catch (err) {
  log.error(err);
  message = err.message;
}
