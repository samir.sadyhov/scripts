var result = true;
var message = "ok";

function processesFilter(processes, userID) {
  let result = [];
  function search(p) {
    p.forEach(function(x) {
      if (x.typeID == 'ASSIGNMENT_ITEM' && !x.finished && x.responsibleUserID == userID) result.push(x);
      if (x.subProcesses.length > 0) search(x.subProcesses);
    });
  }
  search(processes);
  return result;
}

try {
  let currentFormData = API.getFormData(dataUUID);
  let incidentlink = UTILS.getValue(currentFormData, 'itsm_form_problem_incidentlink');
  let currentUser = API.httpGetMethod("rest/api/person/auth");

  if(!incidentlink || !incidentlink.hasOwnProperty('key')) throw new Error('Не найдено связанных инцидентов');

  let incidents = incidentlink.key.split(';');

  incidents.forEach(function(docID){
    let incidentData = API.getFormData(API.getAsfDataId(docID));
    let incidentStatus = UTILS.getValue(incidentData, 'itsm_form_incident_status');

    if(incidentStatus && incidentStatus.hasOwnProperty('key') && incidentStatus.key == '5') {
      log.info('incident documentID: ' + docID + ' || Статус: ' + incidentStatus.value);
      let processes = API.getProcesses(docID);
      processes = processesFilter(processes, currentUser.userid);
      if(processes.length != 0) {
        let responsibleDepartment = UTILS.getValue(incidentData, 'itsm_form_incident_responsibleDepartment');
        let responsibleUser = UTILS.getValue(incidentData, 'itsm_form_incident_responsible');

        let actionID = processes[0].actionID;
        let resultFormWork = API.getFormForResult("itsm_form_incident_wcf_new", actionID);
        let resultFormWorkData = API.getFormData(resultFormWork.dataUUID);
        UTILS.setValue(resultFormWorkData, "itsm_form_incident_wcf_status", {key: '3', value: 'В процессе'});

        if(responsibleDepartment && responsibleDepartment.hasOwnProperty('key'))
        UTILS.setValue(resultFormWorkData, "itsm_form_incident_wcf_responsibleDepartment", {
          key: responsibleDepartment.key,
          value: responsibleDepartment.value
        });

        if(responsibleUser && responsibleUser.hasOwnProperty('key'))
        UTILS.setValue(resultFormWorkData, "itsm_form_incident_wcf_responsible", {
          key: responsibleUser.key,
          value: responsibleUser.value
        });

        API.saveFormData(resultFormWorkData);
        API.finishWork(actionID, resultFormWork.file_identifier);
      }
    }

  });

} catch (err) {
  log.error(err.message);
  message = err.message;
}
