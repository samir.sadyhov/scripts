var result = true;
var message = "ok";

function parseTable(table, cmp) {
  if(!table || !table.hasOwnProperty('data')) return [];
  return table.data
  .map(function(x){
    if(x.id.substring(0, x.id.indexOf('-b')) == cmp && x.hasOwnProperty('key')) return x.key;
  })
  .filter(function(x){if(x) return x})
  .uniq();
}

function processesFilter(processes, userID) {
  let result = [];
  function search(p) {
    p.forEach(function(x) {
      if (x.typeID == 'ASSIGNMENT_ITEM' && !x.finished && x.responsibleUserID == userID) result.push(x);
      if (x.subProcesses.length > 0) search(x.subProcesses);
    });
  }
  search(processes);
  return result;
}

function customFormatDate(datetime){
  datetime = datetime.split(/\D/);
  return datetime[2] + '.' + datetime[1] + '.' + datetime[0] + ' ' + datetime[3] + ':' + datetime[4];
}

try {

  let currentFormData = API.getFormData(dataUUID);
  let closeCode = UTILS.getValue(currentFormData, 'itsm_form_change_close_code');

  if(closeCode && closeCode.hasOwnProperty('key') && closeCode.key == '1') {
    let problems = parseTable(UTILS.getValue(currentFormData, 'itsm_form_change_problems'), 'problem');
    let incidents = parseTable(UTILS.getValue(currentFormData, 'itsm_form_change_incidents'), 'incident');
    let serviceUser = UTILS.getValue(currentFormData, 'itsm_form_cr_service_user');
    let changeId = UTILS.getValue(currentFormData, 'itsm_form_change_id');
    let closeDescription = UTILS.getValue(currentFormData, 'itsm_form_change_close_description');
    let finishComment = 'Решено в изменении ' + (changeId.value || "") + " " + (closeDescription.value || "");

    problems.forEach(function(docID) {
      let problemFormData = API.getFormData(API.getAsfDataId(docID));
      let problemStatus = UTILS.getValue(problemFormData, 'itsm_form_problem_status');
      if(problemStatus && problemStatus.hasOwnProperty('key') && problemStatus.key != '5') {
        let processes = processesFilter(API.getProcesses(docID), serviceUser.key);
        if(processes.length != 0) {
          let actionID = processes[0].actionID;
          let resultFormWork = API.getFormForResult('itsm_form_problem_wcf', actionID);
          let resultFormWorkData = API.getFormData(resultFormWork.dataUUID);
          let currentDate = UTILS.getCurrentDateParse();

          UTILS.setValue(resultFormWorkData, "itsm_form_problem_wcf_status", {key: '5', value: 'Закрыта'});
          UTILS.setValue(resultFormWorkData, "itsm_form_problem_wcf_current_date", {key: currentDate, value: customFormatDate(currentDate)});
          UTILS.setValue(resultFormWorkData, "itsm_form_problem_wcf_decisiontype", {key: '2', value: 'Разрешен'});
          UTILS.setValue(resultFormWorkData, "itsm_form_problem_wcf_decisiondescription", {value: finishComment.trim()});

          API.saveFormData(resultFormWorkData);
          API.finishWork(actionID, resultFormWork.file_identifier);
        }
      }
    });

    incidents.forEach(function(docID) {
      let incidentFormData = API.getFormData(API.getAsfDataId(docID));
      let incidentStatus = UTILS.getValue(incidentFormData, 'itsm_form_incident_status');
      if(incidentStatus && incidentStatus.hasOwnProperty('key') && incidentStatus.key != '6' && incidentStatus.key != '7') {
        let processes = processesFilter(API.getProcesses(docID), serviceUser.key);
        if(processes.length != 0) {
          let actionID = processes[0].actionID;
          let resultFormWork = API.getFormForResult('itsm_form_incident_wcf_new', actionID);
          let resultFormWorkData = API.getFormData(resultFormWork.dataUUID);
          let currentDate = UTILS.getCurrentDateParse();

          UTILS.setValue(resultFormWorkData, "itsm_form_incident_wcf_status", {key: '7', value: 'Ожидает оценки пользователя'});
          UTILS.setValue(resultFormWorkData, "itsm_form_incident_wcf_current_date", {key: currentDate, value: customFormatDate(currentDate)});
          UTILS.setValue(resultFormWorkData, "itsm_form_incident_wcf_decisiontype", {key: '2', value: 'Разрешен'});
          UTILS.setValue(resultFormWorkData, "itsm_form_incident_wcf_decisiondescription", {value: finishComment.trim()});

          API.saveFormData(resultFormWorkData);
          API.finishWork(actionID, resultFormWork.file_identifier);
        }
      }
    });

  }

} catch (err) {
  log.error(err.message);
  message = err.message;
}
