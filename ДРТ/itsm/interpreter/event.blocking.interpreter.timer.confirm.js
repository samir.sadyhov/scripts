function api(method, options) {
  var host = 'http://127.0.0.1:8080/Synergy/rest/api/';
  if (!options) options = {};
  if (!options.type) options.type = 'GET';
  var client = new org.apache.commons.httpclient.HttpClient();
  var creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(login, password);
  client.getParams().setAuthenticationPreemptive(true);
  client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
  switch (options.type) {
    case 'GET':
      var api = new org.apache.commons.httpclient.methods.GetMethod(host + method);
      api.setRequestHeader('Content-type', 'application/json');
      client.executeMethod(api);
      var result = api.getResponseBodyAsString();
      break;
    case 'POST':
      var api = new org.apache.commons.httpclient.methods.PostMethod(host + method);
      if (options && options.data)
        for (var p in options.data) api.addParameter(p, options.data[p]);
      api.setRequestHeader('Content-type', 'application/x-www-form-urlencoded; charset=utf-8');
      var result = client.executeMethod(api);
      break;
    default:
  }
  api.releaseConnection();
  return options.dataType && options.dataType == 'text' ? '' + result : JSON.parse(result);
}

function loadForm(uuid) {
  var form = api('asforms/data/' + uuid);
  form = {
    dataUUID: form.uuid,
    oldDataUUID: form.oldUuid,
    documentID: api('formPlayer/documentIdentifier?dataUUID=' + uuid, {
      dataType: 'text'
    }),
    documentChange: form.modified,
    formVersion: form.version,
    fullData: form,
    nodeUUID: form.nodeUUID,
    formUUID: form.form,
    data: form.data
  }
  return form;
}

function formSave(form) {
  api('asforms/data/save', {
    type: 'POST',
    data: {
      uuid: form.dataUUID,
      data: '"data":' + JSON.stringify(form.data)
    }
  });
  return 'OK';
}

function setValue() {
  var field;
  for (i = 0; i < arguments[0].length; i++) {
    if (arguments[0][i].id == arguments[1]) {
      field = arguments[0][i];
      break;
    }
  }
  if (!field) return;
  if (!!arguments[2]) field[arguments[2]] = arguments[3];
  else field = arguments[3];
}

function Exception(message) {
  this.toString = function() {
    return 'Confirm error: ' + message;
  };
}

try {
  var form = loadForm(dataUUID);
  setValue(form.data, 'itsm_form_incident_status', 'key', '6');
  setValue(form.data, 'itsm_form_incident_status', 'value', 'Закрыто');

  var work = api('workflow/get_execution_process?documentID=' + form.documentID).filter(function(i) {
    return i.code == 'incident_code_wait_confirm' && !i.finished
  });
  if (!work || !work[0]) {
    throw new Exception('Не найдена работа для завершения');
  }

  //создаем форму завершения
  var formWork = api("workflow/work/get_form_for_result?formCode=itsm_form_incident_wcf_new&workID=" + work[0].actionID);
  var completion_form = loadForm(formWork.dataUUID);

  setValue(completion_form.data, 'itsm_form_incident_wcf_status', 'key', '6');
  setValue(completion_form.data, 'itsm_form_incident_wcf_status', 'value', 'Закрыто');
  setValue(completion_form.data, 'itsm_form_incident_wcf_close_attribute', 'key', '2');
  setValue(completion_form.data, 'itsm_form_incident_wcf_close_attribute', 'value', 'обращение закрыто по истечению времени на подтверждение');

  setValue(completion_form.data, 'itsm_form_incident_wcf_mark_comment', 'value', 'Закрыто по таймеру.')

  var saveRes = formSave(completion_form);

  if (!!saveRes && saveRes == 'OK') {
    var req = api('workflow/work/set_result', {
      type: "POST",
      data: {
        workID: work[0].actionID,
        completionForm: 'FORM',
        type: 'work',
        file_identifier: formWork.file_identifier,
      },
    });
    if (req != 200) {
      throw new Exception('Не удалось завершить работы автоматически');
    }


    message = formSave(form);
    result = true;
  } else {
    throw new Exception('Форма завершения не может быть сохранена!');
  }


} catch (err) {
  message = err.toString();
  result = false;
}
