var result = true;
var message = "ok";

function createPosition(params) {
  let client = API.getHttpClient();
  let post = new org.apache.commons.httpclient.methods.PostMethod("http://127.0.0.1:8080/Synergy/rest/api/positions/save?locale=ru");
  for(let key in params) post.addParameter(key, params[key]);
  post.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=utf-8");
  let resp = client.executeMethod(post);
  resp = JSON.parse(post.getResponseBodyAsString());
  post.releaseConnection();
  return resp;
}

function searchPosition(departmentID, name){
  let result = false;
  let res = API.httpGetMethod('rest/api/departments/content?onlyPosition=true&departmentID=' + departmentID);
  res.positions.forEach(function(x){if(x.positionName == name) result = true});
  return result;
}

function saveResult(comment, status){
  API.mergeFormData({uuid: dataUUID, data: [
    {id: 'str_form_position_result', type: 'radio', key: status.value, value: status.key},
    {id: 'str_form_position_comment', type: 'textbox', value: comment}
  ]});
}

try {
  let currentFormData = API.getFormData(dataUUID);
  let pos_name = UTILS.getValue(currentFormData, 'str_form_position_name');
  let pos_dep = UTILS.getValue(currentFormData, 'str_form_position_department');
  let pos_code = UTILS.getValue(currentFormData, 'str_form_position_code');

  if(!pos_name || !pos_name.hasOwnProperty('value') || pos_name.value == '') throw new Error('Не заполнено Наименование должности');
  if(!pos_dep || !pos_dep.hasOwnProperty('key')) throw new Error('Не выбрано Подразделение');

  if(searchPosition(pos_dep.key, pos_name.value)) throw new Error('В подразделении \'' + pos_dep.value + '\' уже существует должность с названием \'' + pos_name.value + '\'');

  let createResult = createPosition({
    departmentID: pos_dep.key,
    nameEn: pos_name.value,
    nameKz: pos_name.value,
    nameRu: pos_name.value,
    pointersCode: pos_code.value
  });

  if(createResult.errorCode != 0) throw new Error(createResult.errorMessage);

  message = "Должность создана успешно";
  saveResult(message, {key: '1', value: 'Успешно'});

} catch (err) {
  log.error(err.message);
  message = err.message;
  saveResult(err.message, {key: '0', value: 'Неуспешно'});
}
