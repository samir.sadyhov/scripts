let API = {
  getHttpClient: function(){
    let client = new org.apache.commons.httpclient.HttpClient();
    let creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(login, password);
    client.getParams().setAuthenticationPreemptive(true);
    client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
    return client;
  },
  httpGetMethod: function(methods, type) {
    let client = this.getHttpClient();
    let get = new org.apache.commons.httpclient.methods.GetMethod("http://127.0.0.1:8080/Synergy/" + methods);
    get.setRequestHeader("Content-type", "application/json");
    client.executeMethod(get);
    let resp = get.getResponseBodyAsString();
    get.releaseConnection();
    return type == 'text' ? resp : JSON.parse(resp);
  },
  httpPostMethod: function(methods, params, contentType) {
    let client = this.getHttpClient();
    let post = new org.apache.commons.httpclient.methods.PostMethod("http://127.0.0.1:8080/Synergy/" + methods);
    if(contentType) post.setRequestBody(JSON.stringify(params));
    else for(let key in params) post.addParameter(key, params[key]);
    post.setRequestHeader("Content-type", contentType || "application/x-www-form-urlencoded; charset=utf-8");
    let resp = client.executeMethod(post);
    post.releaseConnection();
    return resp;
  },
  getFormData: function(asfDataId) {
    return this.httpGetMethod("rest/api/asforms/data/" + asfDataId)
  },
  saveFormData: function(formID, asfDataId, asfData) {
    return this.httpPostMethod("rest/api/asforms/form/multipartdata", {
      form: formID,
      uuid: asfDataId,
      data: "\"data\":" + JSON.stringify(asfData)
    });
  },
  getAsfDataId: function(documentID) {
    return this.httpGetMethod("rest/api/formPlayer/getAsfDataUUID?documentID=" + documentID, 'text');
  }
};

let log = {
  parse: function(args) {
    let result = [];
    for (let x in args) result.push(JSON.stringify(args[x], null, 4));
    return documentID + '\n' + result.join('\n');
  },
  info: function() {
    console.info(this.parse(arguments));
  },
  error: function() {
    console.error(this.parse(arguments));
  }
}

let UTILS = {
  createField: function(fieldData) {
    let field = {};
    for (let key in fieldData) field[key] = fieldData[key];
    return field;
  },
  getValue: function(data, cmpID) {
    data = data.data ? data.data : data;
    for(let i = 0; i < data.length; i++)
    if (data[i].id === cmpID) return data[i];
  },
  setValue: function(asfData, cmpID, data) {
    let field = this.getValue(asfData, cmpID);
    for (let key in data) {
      if(key === 'id' || key === 'type') continue;
      field[key] = data[key];
    }
    return field;
  },
  getTableBlockIndex: function(data, cmp) {
    let res = 0;
    data = data.data ? data.data : data;
    data.forEach(function(item) {
      if (item.id.slice(0, item.id.indexOf('-b')) === cmp) res++;
    });
    return res === 0 ? 1 : ++res;
  }
};

var result = true;
var message = "ok";

try {

  let currentFormData = API.getFormData(dataUUID);
  let documentIDs = UTILS.getValue(currentFormData, "itsm_form_change_ci");

  if(!documentIDs) throw new Error("Не выбраны конфигурационные единицы");
  if(!documentIDs.key) throw new Error("Не выбраны конфигурационные единицы");

  documentIDs = documentIDs.key.split(';');
  documentIDs.forEach(function(docID) {
    let keFormData = API.getFormData(API.getAsfDataId(docID));
    let keTableData = UTILS.getValue(keFormData, "itsm_form_ci_table6");

    if(!keTableData) {
      keTableData = UTILS.createField({
        id: "itsm_form_ci_table6",
        type: "appendable_table",
        key: "",
        data: []
      });
      keFormData.data.push(keTableData);
    }

    let tableBlockIndex = UTILS.getTableBlockIndex(keTableData, "itsm_form_ci_editdate");

    keTableData.data.push(UTILS.createField({
      id: "itsm_form_ci_editdate-b" + tableBlockIndex,
      type: "date",
      key: UTILS.getValue(currentFormData, "itsm_form_change_releasedate").key
    }));

    keTableData.data.push(UTILS.createField({
      id: "itsm_form_ci_edit_description-b" + tableBlockIndex,
      type: "date",
      value: UTILS.getValue(currentFormData, "itsm_form_change_description").value
    }));

    keTableData.data.push(UTILS.createField({
      id: "itsm_form_ci_change-b" + tableBlockIndex,
      type: "reglink",
      key: documentID,
      valueID: documentID
    }));

    API.saveFormData(keFormData.form, keFormData.uuid, keFormData.data);
  });

} catch (err) {
  log.error(err.message);
  message = err.message;
}
