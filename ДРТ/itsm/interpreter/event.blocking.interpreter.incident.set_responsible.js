var result = true;
var message = "ok";

function getUsersDepartment(departmentID) {
  return API.httpGetMethod("rest/api/userchooser/search?showAll=true&departmentID=" + departmentID);
}

try {
  let currentFormData = API.getFormData(dataUUID);
  let department = UTILS.getValue(currentFormData, "itsm_form_incident_responsibleDepartment");

  if(!department || !department.hasOwnProperty('key')) throw new Error('Не заполнено поле Подразделение в блоке Исполнение обращения');

  let employees = getUsersDepartment(department.key);
  employees = employees.slice(0, 50);

  let newResponsible = {
    id: 'itsm_form_incident_responsible',
    type: 'entity',
    key: employees.map(function(item){
      return item.userID;
    }).join(';'),
    value: employees.map(function(item){
      return item.name;
    }).join(', ')
  };

  API.mergeFormData({
    uuid: dataUUID,
    data: [newResponsible]
  });

} catch (err) {
  message = err.message;
}
