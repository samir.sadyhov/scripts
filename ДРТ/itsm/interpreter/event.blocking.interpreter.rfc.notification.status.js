var result = true;
var message = "ok";

try {

  let url = 'rest/api/registry/data_ext?registryCode=itsm_registry_settings&countInPart=1';
  url += '&fields=itsm_form_settings_rfc_author_notification_theme';
  url += '&fields=itsm_form_settings_rfc_author_notification_subject';
  url += '&fields=itsm_form_settings_rfc_users_notification_theme';
  url += '&fields=itsm_form_settings_rfc_users_notification_subject';

  let SETTINGS = API.httpGetMethod(url);

  if(SETTINGS.recordsCount == 0) throw new Error('Не найдены настройки');
  SETTINGS = SETTINGS.result[0].fieldValue;

  let currentFormData = API.getFormData(dataUUID);

  // Уведомление автора об изменении статуса запроса на изменение
  let themeAuthor = SETTINGS['itsm_form_settings_rfc_author_notification_theme'];
  let subjectAuthor = SETTINGS['itsm_form_settings_rfc_author_notification_subject'];
  if(themeAuthor && subjectAuthor) {
    let author = UTILS.getValue(currentFormData, 'itsm_form_rfc_author');
    if(author && author.hasOwnProperty('key')) {
      UTILS.getParams(subjectAuthor).forEach(function(id) {
        let tmp = UTILS.getValue(currentFormData, id);
        if(tmp && tmp.hasOwnProperty('value')) subjectAuthor = subjectAuthor.replace('${'+id+'}', tmp.value || '');
      });
      UTILS.getParams(themeAuthor).forEach(function(id) {
        let tmp = UTILS.getValue(currentFormData, id);
        if(tmp && tmp.hasOwnProperty('value')) themeAuthor = themeAuthor.replace('${'+id+'}', tmp.value || '');
      });
      API.sendNotification({
        header: themeAuthor,
        message: subjectAuthor,
        users: [author.key]
      });
    }
  }

  // Уведомление пользователей, ответственных за поддержку услуги, об изменении статуса запроса на изменение"
  let servicelink = UTILS.getValue(currentFormData, 'itsm_form_rfc_servicelink');
  if(servicelink && servicelink.hasOwnProperty('key')) {
    let serviceData = API.getFormData(API.getAsfDataId(servicelink.key));
    let users = UTILS.getValue(serviceData, 'itsm_form_service_support_group_users');

    if(users && users.hasOwnProperty('key')) {
      let userIDs = users.key.split(';');
      let userNames = users.value.split(', ');
      let themeUsers = SETTINGS['itsm_form_settings_rfc_users_notification_theme'];
      let subjectUsers = SETTINGS['itsm_form_settings_rfc_users_notification_subject'];

      UTILS.getParams(themeUsers).forEach(function(id) {
        let tmp = UTILS.getValue(currentFormData, id);
        if(tmp && tmp.hasOwnProperty('value')) themeUsers = themeUsers.replace('${'+id+'}', tmp.value || '');
      });

      userIDs.forEach(function(userid, i){
        let tmpSubject = subjectUsers;
        UTILS.getParams(subjectUsers).forEach(function(id) {
          let replaceValue = '';
          let tmp = UTILS.getValue(currentFormData, id);
          if(id == 'itsm_form_service_support_group_users') {
            replaceValue = userNames[i];
          } else {
            if(tmp && tmp.hasOwnProperty('value')) replaceValue = tmp.value;
          }
          tmpSubject = tmpSubject.replace('${'+id+'}', replaceValue);
        });
        API.sendNotification({
          header: themeUsers,
          message: tmpSubject,
          users: [userid]
        });
      });

    }
  }

} catch (err) {
  log.error(err);
  message = err.message;
}
