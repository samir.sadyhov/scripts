var result = true;
var message = "ok";

function getPublicAddress(){
  let client = new org.apache.commons.httpclient.HttpClient();
  let get = new org.apache.commons.httpclient.methods.GetMethod("http://127.0.0.1:8080/itsm/rest/config/public-address");
  client.executeMethod(get);
  let resp = get.getResponseBodyAsString();
  get.releaseConnection();
  return resp.replace("Synergy", "");
}

try {
  let THEME_CMP_ID = 'itsm_form_settings_external_provider_get_incident_theme';
  let SUBJECT_CMP_ID = 'itsm_form_settings_external_provider_get_incident_text';

  let SETTINGS = API.httpGetMethod('rest/api/registry/data_ext?registryCode=itsm_registry_settings&fields=' + THEME_CMP_ID + '&fields=' + SUBJECT_CMP_ID);

  if(SETTINGS.recordsCount == 0) throw new Error('Не найдены настройки');

  let mailTheme = SETTINGS.result[0].fieldValue[THEME_CMP_ID];
  let mailSubject = SETTINGS.result[0].fieldValue[SUBJECT_CMP_ID];

  if(!mailTheme && !mailSubject) throw new Error('Не найдены настройки для формирования сообщения');

  let currentFormData = API.getFormData(dataUUID);
  let supplier_email = UTILS.getValue(currentFormData, 'itsm_form_incident_supplier_email');

  UTILS.getParams(mailTheme).forEach(function(id) {
    mailTheme = mailTheme.replace('${'+id+'}', UTILS.getValue(currentFormData, id).value || '');
  });
  UTILS.getParams(mailSubject).forEach(function(id) {
    mailSubject = mailSubject.replace('${'+id+'}', UTILS.getValue(currentFormData, id).value || '');
  });

  if(!supplier_email || !supplier_email.hasOwnProperty('value') || supplier_email.value == '') {
    throw new Error('Не найден email получателя');
  }

  mailSubject += '<br><br><a href="' + getPublicAddress() + 'itsm-doc/?task=' + documentID + '"><b>Ссылка для ввода решения / Шешімді енгізу сілтемесі</b></a>';

  // отправка письма
  let sendResult = API.sendNotification({
    header: mailTheme,
    message: mailSubject,
    emails: [supplier_email.value]
  });

  if(sendResult.errorCode == 0) {
    message = 'Уведомление успешно отправлено';
  } else {
    throw new Error('Произошла ошибка отправки уведомления');
  }

} catch (err) {
  message = err.message;
}
