// Код справочника содержащий в себе статусы инцидента
let DICT_INCIDENT_STATUS = 'itsm_dict_incidentstatus';

// Колонка по которой будем искать значение со справочника для смены статуса
let DICT_INCIDENT_STATUS_COL = 'itsm_dict_incidentstatus_value';

// Колонка в которой хранится значение (Например: На очереди) от колонки `DICT_INCIDENT_STATUS_COL`
let DICT_INCIDENT_TYPE_COL = 'itsm_dict_incidentstatus_type';

// Статусы инцидента
let NEW_STATUS = 1; // Зарегистрирован
let IN_QUEUE = 2; // На очереди
let IN_PROGRESS = 3; // В процессе
let WAITING_USER = 4; // Ожидает ответа пользователя
let WAITING_PROBLEM = 5; // Ожидает проблему
let CLOSED = 6; // Закрыт
let WAITING_USER_RATE = 7; // Ожидает подтверждения завершения
let WAITING_EQUIPMENT = 8; // В ожидании выделения техники
let WAITING_SUPPLY = 9; // Направлен внешнему поставщику
let INFO_GIVEN = 10; // Информация предоставлена
let WRONG_WAY = 11; // Неверно направлен
let RE_OPEN = 12; // Направлен повторно
let WAITING_MASS_INCIDENT = 13; //Ожидает закрытия массового инцидента
let IN_AGREEMENT = 91; // На согласовании
let AGREED = 15; // Согласовано
let NOT_AGREED = 16; // Не согласовано

let ArrayChangeStatus = [
  IN_QUEUE,
  IN_PROGRESS,
  WAITING_USER,
  CLOSED,
  WAITING_USER_RATE,
  WAITING_EQUIPMENT,
  WAITING_SUPPLY,
  INFO_GIVEN,
  WRONG_WAY,
  RE_OPEN,
  WAITING_PROBLEM,
  WAITING_MASS_INCIDENT,
  IN_AGREEMENT,
  AGREED,
  NOT_AGREED
];

let jSynergy = new __classSynergy();

/* Обрезает корневой путь до последней директории */
function basename(path) {
  return decodeURIComponent((p = (path = new String(path)).substring((~(i = path.lastIndexOf('/')) ? i + 1 : 0), path.length)).substring((~(i = p.lastIndexOf('=')) ? i + 1 : 0), p.length)).replace(/\+/g, ' ');
}

/* Обрезает путь без рассширения файла */
function basenameNoExt(path) {
  return (i = basename(path).split('.')).splice(0, i.length - 1).join('.');
}

/**
 * Функция ищет строку в справочнике по значению в коде колонки
 * @param   code            Код справочника
 * @param   colCode         Код колонки по которой будем искать значение
 * @param   value           Значение поиска
 * @param   locale          Локаль (перевод) - по умолчанию ru
 * @return  Объект строки из справочника
 */
function getDictStatus(code, colCode, value, locale) {
  let dictData = jSynergy.server.api('dictionary/get_by_code?dictionaryCode=' + code + (locale ? '&locale=' + locale : ''));
  let colIDs = {};
  let res = {};

  if (!dictData) return 'Не удалось загрузить справочник';
  dictData.columns.forEach(function(item) {
    colIDs[item.columnID] = item.code;
  });

  if (!colIDs) return 'Данной колонки `' + colCode + '` - не существует в справочнике';
  dictData.items.forEach(function(itemArr) {
    itemArr.values.forEach(function(item) {
      if (~Object.keys(colIDs).indexOf("" + item.columnID) && item.value == value) {
        itemArr.values.forEach(function(item) {
          res[colIDs[item.columnID]] = item.value;
        });
        return;
      }
    });
  });

  return res;
}

jSynergy.setConnection(false, login, password);

/**
 * Рекурсивная функция - ищет работу по документу с типом `ASSIGNMENT_ITEM` и не завершенная
 * @param   arr     Массив содержащий в себе `ход-выполнения документа`
 * @return  Массив с работой
 */
function recursiveFilter(arr) {
  var matches = [];
  if (!Array.isArray(arr)) return matches;

  arr.forEach(function(x) {
    if (x.typeID == 'ASSIGNMENT_ITEM' && !!x.finished) matches.push(x);

    if (x.subProcesses.length > 0) {
      var childResults = recursiveFilter(x.subProcesses);
      if (childResults.length) matches = matches.concat(childResults);
    }
  });

  return matches;
}


function parseDate(datetime) {
  return datetime.getFullYear() + '-' +
    ('0' + (datetime.getMonth() + 1)).slice(-2) + '-' +
    ('0' + datetime.getDate()).slice(-2) + ' ' +
    ('0' + datetime.getHours()).slice(-2) + ':' +
    ('0' + datetime.getMinutes()).slice(-2) + ':' +
    ('0' + datetime.getSeconds()).slice(-2);
}

function customFormatDate(datetime /*yyyy-mm-dd HH:MM:SS*/){
  datetime = datetime.split(/\D/);
  return datetime[2] + '.' + datetime[1] + '.' + datetime[0] + ' ' + datetime[3] + ':' + datetime[4];
}

try {
  jSynergy.server.load(dataUUID);

  let usedFormCompletions = [];
  let tempUsed = jSynergy.server.getValue("itsm_form_incident_used_completion_forms");
  tempUsed = tempUsed.hasOwnProperty('value') ? tempUsed.value.split(';') : [];
  usedFormCompletions = tempUsed;

  let processes = jSynergy.server.api('workflow/get_execution_process?documentID=' + documentID);

  if (!Array.isArray(processes)) throw new Error('get_execution_process не сработал');

  processes = recursiveFilter(processes);

  if (!processes || !processes[0]) throw new Error('Работа не найдена');

  /**
   * Фича от проблемы в Интерпретаторе
   * Если написать так `new Date('2019-02-18 03:58:32')` то в ответ получаем `Invalid Date` - Скорее всего БАГ.
   * Почему цикл - переменная `processes` может содержать более одной работы с датами.
   */
  processes.forEach(function(processe) {
    var finished = processe.finished.split(/\D/);
    var started = processe.started.split(/\D/);
    processe.finished = new Date(finished[0], finished[1] - 1, finished[2], finished[3], finished[4], finished[5]);
    processe.started = new Date(started[0], started[1] - 1, started[2], started[3], started[4], started[5]);
  });

  // Сортируем даты по убыванию
  processes = processes.sort(function(a, b) {
    return b.finished - a.finished;
  });

  // Фильтруем последниe параллельные процессы
  var lastRouteItemID = processes[0].routeItemID;
  processes = processes.filter(function(x) {
    return x.routeItemID === lastRouteItemID;
  });

  // Завершаем работы с формой завершения
  var cform_check = false;
  var responseComment = null;
  let procFinish = null;
  processes.forEach(function(proc) {
    let work = jSynergy.server.api('workflow/work/get_completion_data?workID=' + proc.actionID);
    if (work && work.result && work.result.dataUUID && usedFormCompletions.indexOf(work.result.fileUUID) === -1) {
      usedFormCompletions.push(work.result.fileUUID);
      procFinish = proc;
      cform_check = true;
      processes = work;
      responseComment = basenameNoExt(work.result.file_name);
      return;
    }
  });

  if (!cform_check || !processes) throw new Error('Предыдущий этап завершен без формы завершения');

  jSynergy.server.load(processes.result.dataUUID);

  processes = {
    'itsm_form_incident_wcf_status': jSynergy.server.getValue('itsm_form_incident_wcf_status'),
    'itsm_form_incident_wcf_responsible': jSynergy.server.getValue('itsm_form_incident_wcf_responsible'),
    'itsm_form_incident_wcf_close_attribute': jSynergy.server.getValue('itsm_form_incident_wcf_close_attribute'),
    'itsm_form_incident_wcf_responsibleDepartment': jSynergy.server.getValue('itsm_form_incident_wcf_responsibleDepartment'),
    'itsm_form_incident_wcf_responsible_inprogress': jSynergy.server.getValue('itsm_form_incident_wcf_responsible_inprogress'),
    'itsm_form_incident_wcf_responsibleDepartment_inprogress': jSynergy.server.getValue('itsm_form_incident_wcf_responsibleDepartment_inprogress'),
    'itsm_form_incident_wcf_current_user': jSynergy.server.getValue('itsm_form_incident_wcf_current_user'),
    'itsm_form_incident_wcf_current_user_Department': jSynergy.server.getValue('itsm_form_incident_wcf_current_user_Department'),
    'itsm_form_incident_wcf_return_to_author': jSynergy.server.getValue('itsm_form_incident_wcf_return_to_author'),
    'itsm_form_incident_wcf_servicelink': jSynergy.server.getValue('itsm_form_incident_wcf_servicelink'),
    'itsm_form_incident_wcf_type': jSynergy.server.getValue('itsm_form_incident_wcf_type'),
    'itsm_form_incident_wcf_decisiontype': jSynergy.server.getValue('itsm_form_incident_wcf_decisiontype'),
    'itsm_form_incident_wcf_decisiondescription': jSynergy.server.getValue('itsm_form_incident_wcf_decisiondescription'),
    'itsm_form_incident_wcf_cause': jSynergy.server.getValue('itsm_form_incident_wcf_cause'),
    'itsm_form_incident_wcf_failCategory': jSynergy.server.getValue('itsm_form_incident_wcf_failCategory'),
    'itsm_form_incident_wcf_failType': jSynergy.server.getValue('itsm_form_incident_wcf_failType'),
    'itsm_form_incident_wcf_makenew_knowledge': jSynergy.server.getValue('itsm_form_incident_wcf_makenew_knowledge'),
    'itsm_form_incident_wcf_current_date': jSynergy.server.getValue('itsm_form_incident_wcf_current_date'),
    'itsm_form_incident_wcf_equipment_date': jSynergy.server.getValue('itsm_form_incident_wcf_equipment_date'),
    'itsm_form_incident_wcf_supplier': jSynergy.server.getValue('itsm_form_incident_wcf_supplier'),
    'itsm_form_incident_wcf_comment': jSynergy.server.getValue('itsm_form_incident_wcf_comment'),
    'itsm_form_incident_wcf_new_decision_files': jSynergy.server.converTable('itsm_form_incident_wcf_new_decision_files'),
    'itsm_form_incident_wcf_mark_files': jSynergy.server.converTable('itsm_form_incident_wcf_mark_files'),
    'itsm_form_incident_wcf_mark_comment': jSynergy.server.getValue('itsm_form_incident_wcf_mark_comment'),
    'itsm_form_incident_wcf_mark': jSynergy.server.getValue('itsm_form_incident_wcf_mark'),
    'itsm_form_incident_wcf_supplier_email': jSynergy.server.getValue('itsm_form_incident_wcf_supplier_email'), //#237
    'itsm_form_incident_wcf_mass_reglink': jSynergy.server.getValue('itsm_form_incident_wcf_mass_reglink'),
    'itsm_form_incident_wcf_major_problemlink': jSynergy.server.getValue('itsm_form_incident_wcf_major_problemlink'),
    'itsm_form_incident_wcf_approval_description': jSynergy.server.getValue('itsm_form_incident_wcf_approval_description'),
    'itsm_form_incident_wcf_approval_users': jSynergy.server.getValue('itsm_form_incident_wcf_approval_users'),
    'itsm_form_incident_wcf_approval_comment': jSynergy.server.getValue('itsm_form_incident_wcf_approval_comment'),
    'itsm_form_incident_knowledge': jSynergy.server.getValue('itsm_form_incident_knowledge')
  };

  var wcf_status = +jSynergy.server.getValue('itsm_form_incident_wcf_status', 'key') || 0;
  jSynergy.server.selectForm(dataUUID);
  jSynergy.server.setValue('itsm_form_incident_used_completion_forms', usedFormCompletions.join(';'))

  //копирование статуса в поле "Статус обращения, до отправки на согласование", который был на форме до указания "На согласовании".
  if(wcf_status == IN_AGREEMENT) {
    let currentStatus = jSynergy.server.getValue('itsm_form_incident_status');
    if(currentStatus.key != IN_AGREEMENT) {
      jSynergy.server.setValue('itsm_form_incident_approval_status', {key: currentStatus.key, value: currentStatus.value});
    }
  }

  if (~ArrayChangeStatus.indexOf(wcf_status)) {
    let dictWcf = getDictStatus(DICT_INCIDENT_STATUS, DICT_INCIDENT_STATUS_COL, "" + wcf_status);
    jSynergy.server.setValue('itsm_form_incident_status', {
      key: "" + wcf_status,
      value: "" + dictWcf[DICT_INCIDENT_TYPE_COL]
    });
  }

  jSynergy.server.setValue('itsm_form_incident_supplier_email', "" + processes.itsm_form_incident_wcf_supplier_email.value); //#237

  let currentDateTime = parseDate(new Date());

  switch (wcf_status) {
    case IN_QUEUE:
      jSynergy.server.setValue('itsm_form_incident_responsible', {
        key: "" + processes.itsm_form_incident_wcf_responsible.key,
        value: "" + processes.itsm_form_incident_wcf_responsible.value
      });
      jSynergy.server.setValue('itsm_form_incident_responsibleDepartment', {
        key: "" + processes.itsm_form_incident_wcf_responsibleDepartment.key,
        value: "" + processes.itsm_form_incident_wcf_responsibleDepartment.value
      });
      if (processes.itsm_form_incident_wcf_servicelink && processes.itsm_form_incident_wcf_servicelink.value && processes.itsm_form_incident_wcf_servicelink.key) {
        jSynergy.server.setValue('itsm_form_incident_servicelink', {
          key: "" + processes.itsm_form_incident_wcf_servicelink.key,
          valueID: "" + processes.itsm_form_incident_wcf_servicelink.key,
          value: "" + processes.itsm_form_incident_wcf_servicelink.value
        });
      }
      if (processes.itsm_form_incident_wcf_type && processes.itsm_form_incident_wcf_type.value && processes.itsm_form_incident_wcf_type.key) {
        jSynergy.server.setValue('itsm_form_incident_type', {
          key: "" + processes.itsm_form_incident_wcf_type.key,
          value: "" + processes.itsm_form_incident_wcf_type.value
        });
      }
      jSynergy.server.setValue('itsm_form_incident_inqueue_operator', {
        key: "" + processes.itsm_form_incident_wcf_current_user.key,
        value: "" + processes.itsm_form_incident_wcf_current_user.value
      });
      jSynergy.server.setValue('itsm_form_incident_inqueue_operator_Department', {
        key: "" + processes.itsm_form_incident_wcf_current_user_Department.key,
        value: "" + processes.itsm_form_incident_wcf_current_user_Department.value
      });
      jSynergy.server.setValue('itsm_form_incident_inqueue_date', {key: currentDateTime, value: customFormatDate(currentDateTime)});
      break;

    case IN_PROGRESS:
      jSynergy.server.setValue('itsm_form_incident_inprogress_operator', {
        key: "" + processes.itsm_form_incident_wcf_current_user.key,
        value: "" + processes.itsm_form_incident_wcf_current_user.value
      });
      jSynergy.server.setValue('itsm_form_incident_inprogress_operator_Department', {
        key: "" + processes.itsm_form_incident_wcf_current_user_Department.key,
        value: "" + processes.itsm_form_incident_wcf_current_user_Department.value
      });
      jSynergy.server.setValue('itsm_form_incident_inprogress_date', {key: currentDateTime, value: customFormatDate(currentDateTime)});
      jSynergy.server.setValue('itsm_form_incident_responsible', {
        key: "" + processes.itsm_form_incident_wcf_responsible_inprogress.key,
        value: "" + processes.itsm_form_incident_wcf_responsible_inprogress.value
      });
      jSynergy.server.setValue('itsm_form_incident_responsibleDepartment', {
        key: "" + processes.itsm_form_incident_wcf_responsibleDepartment_inprogress.key,
        value: "" + processes.itsm_form_incident_wcf_responsibleDepartment_inprogress.value
      });
      break;

    case WAITING_USER:
      jSynergy.server.setValue('itsm_form_incident_return_to_author', {
        key: "" + processes.itsm_form_incident_wcf_return_to_author.key,
        value: "" + processes.itsm_form_incident_wcf_return_to_author.value
      });
      if (processes.itsm_form_incident_wcf_servicelink && processes.itsm_form_incident_wcf_servicelink.value && processes.itsm_form_incident_wcf_servicelink.key) {
        jSynergy.server.setValue('itsm_form_incident_servicelink', {
          key: "" + processes.itsm_form_incident_wcf_servicelink.key,
          valueID: "" + processes.itsm_form_incident_wcf_servicelink.key,
          value: "" + processes.itsm_form_incident_wcf_servicelink.value
        });
      }
      if (processes.itsm_form_incident_wcf_type && processes.itsm_form_incident_wcf_type.value && processes.itsm_form_incident_wcf_type.key) {
        jSynergy.server.setValue('itsm_form_incident_type', {
          key: "" + processes.itsm_form_incident_wcf_type.key,
          value: "" + processes.itsm_form_incident_wcf_type.value
        });
      }
      break;

    case WAITING_USER_RATE:
      jSynergy.server.setValue('itsm_form_incident_decisiontype', {
        key: "" + processes.itsm_form_incident_wcf_decisiontype.key,
        value: "" + processes.itsm_form_incident_wcf_decisiontype.value
      });
      jSynergy.server.setValue('itsm_form_incident_decisiondescription', {
        key: "" + processes.itsm_form_incident_wcf_decisiondescription.key,
        value: "" + processes.itsm_form_incident_wcf_decisiondescription.value
      });
      jSynergy.server.setValue('itsm_form_incident_cause', {
        key: "" + processes.itsm_form_incident_wcf_cause.key,
        value: "" + processes.itsm_form_incident_wcf_cause.value
      });
      jSynergy.server.setValue('itsm_form_incident_failCategory', {
        key: "" + processes.itsm_form_incident_wcf_failCategory.key,
        value: "" + processes.itsm_form_incident_wcf_failCategory.value
      });
      jSynergy.server.setValue('itsm_form_incident_failType', {
        key: "" + processes.itsm_form_incident_wcf_failType.key,
        value: "" + processes.itsm_form_incident_wcf_failType.value
      });
      jSynergy.server.setValue('itsm_form_incident_makenew_knowledge', {
        keys: processes.itsm_form_incident_wcf_makenew_knowledge.keys,
        values: processes.itsm_form_incident_wcf_makenew_knowledge.values
      });
      jSynergy.server.setValue('itsm_form_incident_knowledge', {
        data: processes.itsm_form_incident_knowledge.data
      });

      if (processes.itsm_form_incident_wcf_new_decision_files.length) {
        var tbl = jSynergy.server.converTable('itsm_form_incident_decision_files');
        var files = [];
        var emptyRows = [];
        tbl.forEach(function(item) {
          if (item['file'].key && !~files.indexOf(item['file'].key)) files.push("" + item['file'].key);
        });
        processes.itsm_form_incident_wcf_new_decision_files.forEach(function(item) {
          if (!~files.indexOf(item['file'].key)) {
            jSynergy.server.addRowTable('itsm_form_incident_decision_files');
            jSynergy.server.setValue('file', 'itsm_form_incident_decision_files', jSynergy.server.getRowsCount('itsm_form_incident_decision_files'), {
              key: item['file'].key,
              value: item['file'].value
            });
          }
        });
      }
      jSynergy.server.setValue('itsm_form_incident_confirm_date', {key: currentDateTime, value: customFormatDate(currentDateTime)});
      jSynergy.server.setValue('itsm_form_incident_confirm_responible', {
        key: "" + processes.itsm_form_incident_wcf_current_user.key,
        value: "" + processes.itsm_form_incident_wcf_current_user.value
      });
      jSynergy.server.setValue('itsm_form_incident_confirm_responible_Department', {
        key: "" + processes.itsm_form_incident_wcf_current_user_Department.key,
        value: "" + processes.itsm_form_incident_wcf_current_user_Department.value
      });
      if (processes.itsm_form_incident_wcf_servicelink && processes.itsm_form_incident_wcf_servicelink.value && processes.itsm_form_incident_wcf_servicelink.key) {
        jSynergy.server.setValue('itsm_form_incident_servicelink', {
          key: "" + processes.itsm_form_incident_wcf_servicelink.key,
          valueID: "" + processes.itsm_form_incident_wcf_servicelink.key,
          value: "" + processes.itsm_form_incident_wcf_servicelink.value
        });
      }
      if (processes.itsm_form_incident_wcf_type && processes.itsm_form_incident_wcf_type.value && processes.itsm_form_incident_wcf_type.key) {
        jSynergy.server.setValue('itsm_form_incident_type', {
          key: "" + processes.itsm_form_incident_wcf_type.key,
          value: "" + processes.itsm_form_incident_wcf_type.value
        });
      }
      break;

    case WAITING_EQUIPMENT:
      jSynergy.server.setValue('itsm_form_incident_equipment_date', {
        key: "" + processes.itsm_form_incident_wcf_equipment_date.key,
        value: "" + processes.itsm_form_incident_wcf_equipment_date.value
      });
      break;

    case WAITING_SUPPLY:
      jSynergy.server.setValue('itsm_form_incident_supplier', {
        key: "" + processes.itsm_form_incident_wcf_supplier.key,
        value: "" + processes.itsm_form_incident_wcf_supplier.value
      });
      break;

    case WRONG_WAY:
      jSynergy.server.setValue('itsm_form_incident_comment', "" + processes.itsm_form_incident_wcf_comment.value);
      break;

    case CLOSED:
      jSynergy.server.setValue('itsm_form_incident_mark', {
        key: "" + processes.itsm_form_incident_wcf_mark.key,
        value: "" + processes.itsm_form_incident_wcf_mark.value
      });

      if(processes.itsm_form_incident_wcf_mark_comment.value && processes.itsm_form_incident_wcf_mark_comment.value != '' && processes.itsm_form_incident_wcf_mark_comment.value != 'undefined') {
        jSynergy.server.setValue('itsm_form_incident_mark_comment', {
          value: "" + processes.itsm_form_incident_wcf_mark_comment.value
        });
      }

      jSynergy.server.setValue('itsm_form_incident_close_attribute', {
        key: "" + processes.itsm_form_incident_wcf_close_attribute.key,
        value: "" + processes.itsm_form_incident_wcf_close_attribute.value
      });
      if (processes.itsm_form_incident_wcf_mark_files.length) {
        var tbl = jSynergy.server.converTable('itsm_form_incident_files');
        var files = [];
        var emptyRows = [];
        tbl.forEach(function(item) {
          if (item['file'].key && !~files.indexOf(item['file'].key)) files.push("" + item['file'].key);
        });
        processes.itsm_form_incident_wcf_mark_files.forEach(function(item) {
          if (!~files.indexOf(item['file'].key)) {
            jSynergy.server.addRowTable('itsm_form_incident_files');
            jSynergy.server.setValue('file', 'itsm_form_incident_files', jSynergy.server.getRowsCount('itsm_form_incident_files'), {
              key: item['file'].key,
              value: item['file'].value
            });
          }
        });
      }
      break;
    case RE_OPEN:
      jSynergy.server.setValue('itsm_form_incident_mark', {
        key: "" + processes.itsm_form_incident_wcf_mark.key,
        value: "" + processes.itsm_form_incident_wcf_mark.value
      });

      if(processes.itsm_form_incident_wcf_mark_comment.value && processes.itsm_form_incident_wcf_mark_comment.value != '' && processes.itsm_form_incident_wcf_mark_comment.value != 'undefined') {
        jSynergy.server.setValue('itsm_form_incident_mark_comment', {
          value: "" + processes.itsm_form_incident_wcf_mark_comment.value
        });
      }

      if (processes.itsm_form_incident_wcf_mark_files.length) {
        var tbl = jSynergy.server.converTable('itsm_form_incident_files');
        var files = [];
        var emptyRows = [];
        tbl.forEach(function(item) {
          if (item['file'].key && !~files.indexOf(item['file'].key)) files.push("" + item['file'].key);
        });
        processes.itsm_form_incident_wcf_mark_files.forEach(function(item) {
          if (!~files.indexOf(item['file'].key)) {
            jSynergy.server.addRowTable('itsm_form_incident_files');
            jSynergy.server.setValue('file', 'itsm_form_incident_files', jSynergy.server.getRowsCount('itsm_form_incident_files'), {
              key: item['file'].key,
              value: item['file'].value
            });
          }
        });
      }
      break;
    case WAITING_MASS_INCIDENT:
      //#353
      if(processes.itsm_form_incident_wcf_mass_reglink.hasOwnProperty('key') && processes.itsm_form_incident_wcf_mass_reglink.hasOwnProperty('value')) {
        jSynergy.server.setValue('itsm_form_incident_mass_reglink', {
          key: "" + processes.itsm_form_incident_wcf_mass_reglink.key,
          valueID: "" + processes.itsm_form_incident_wcf_mass_reglink.key,
          value: "" + processes.itsm_form_incident_wcf_mass_reglink.value
        });
      }
      if (processes.itsm_form_incident_wcf_type && processes.itsm_form_incident_wcf_type.value && processes.itsm_form_incident_wcf_type.key) {
        jSynergy.server.setValue('itsm_form_incident_type', {
          key: "" + processes.itsm_form_incident_wcf_type.key,
          value: "" + processes.itsm_form_incident_wcf_type.value
        });
      }
      if (processes.itsm_form_incident_wcf_servicelink && processes.itsm_form_incident_wcf_servicelink.value && processes.itsm_form_incident_wcf_servicelink.key) {
        jSynergy.server.setValue('itsm_form_incident_servicelink', {
          key: "" + processes.itsm_form_incident_wcf_servicelink.key,
          valueID: "" + processes.itsm_form_incident_wcf_servicelink.key,
          value: "" + processes.itsm_form_incident_wcf_servicelink.value
        });
      }
      break;
    case WAITING_PROBLEM:
      if(processes.itsm_form_incident_wcf_major_problemlink.hasOwnProperty('key') && processes.itsm_form_incident_wcf_major_problemlink.hasOwnProperty('value')) {
        jSynergy.server.setValue('itsm_form_incident_major_problemlink', {
          key: "" + processes.itsm_form_incident_wcf_major_problemlink.key,
          valueID: "" + processes.itsm_form_incident_wcf_major_problemlink.key,
          value: "" + processes.itsm_form_incident_wcf_major_problemlink.value
        });
      }
      break;
    case IN_AGREEMENT:
      if(processes.itsm_form_incident_wcf_approval_users.hasOwnProperty('key') && processes.itsm_form_incident_wcf_approval_users.hasOwnProperty('value')) {
        jSynergy.server.setValue('itsm_form_incident_approval_users', {
          key: "" + processes.itsm_form_incident_wcf_approval_users.key,
          value: "" + processes.itsm_form_incident_wcf_approval_users.value
        });
      }
      if (processes.itsm_form_incident_wcf_servicelink && processes.itsm_form_incident_wcf_servicelink.value && processes.itsm_form_incident_wcf_servicelink.key) {
        jSynergy.server.setValue('itsm_form_incident_servicelink', {
          key: "" + processes.itsm_form_incident_wcf_servicelink.key,
          valueID: "" + processes.itsm_form_incident_wcf_servicelink.key,
          value: "" + processes.itsm_form_incident_wcf_servicelink.value
        });
      }
      if (processes.itsm_form_incident_wcf_type && processes.itsm_form_incident_wcf_type.value && processes.itsm_form_incident_wcf_type.key) {
        jSynergy.server.setValue('itsm_form_incident_type', {
          key: "" + processes.itsm_form_incident_wcf_type.key,
          value: "" + processes.itsm_form_incident_wcf_type.value
        });
      }
      jSynergy.server.setValue('itsm_form_incident_approval_description', "" + processes.itsm_form_incident_wcf_approval_description.value);
      break;
    case AGREED:
    case NOT_AGREED:
      jSynergy.server.setValue('itsm_form_incident_approval_comment', "" + processes.itsm_form_incident_wcf_approval_comment.value);

      jSynergy.server.addRowTable('itsm_form_incident_agreement_table');
      var tableBlockIndex = jSynergy.server.getRowsCount('itsm_form_incident_agreement_table');
      jSynergy.server.setValue('agreement_date', 'itsm_form_incident_agreement_table', tableBlockIndex, {
        key: parseDate(procFinish.finished),
        value: customFormatDate(parseDate(procFinish.finished))
      });

      jSynergy.server.setValue('agreement_status', 'itsm_form_incident_agreement_table', tableBlockIndex, {
        key: processes.itsm_form_incident_wcf_status.key,
        value: processes.itsm_form_incident_wcf_status.value
      });

      jSynergy.server.setValue('agreement_user', 'itsm_form_incident_agreement_table', tableBlockIndex, {
        key: procFinish.responsibleUserID,
        value: procFinish.responsibleUserName
      });
      jSynergy.server.setValue('agreement_comment', 'itsm_form_incident_agreement_table', tableBlockIndex, {
        value: processes.itsm_form_incident_wcf_approval_comment.value
      });
      break;
  }

  //259 1. Для каждого статуса (case) в БП complition - добавить копирование (последний шаг в этом тикете(?)):
  jSynergy.server.setValue('itsm_form_incident_prev_status_time', {key: currentDateTime, value: customFormatDate(currentDateTime)});

  //461
  jSynergy.server.setValue('itsm_form_incident_priority_change', {key: 'Нет', value: '0'});

  jSynergy.server.save();
  result = true;
  message = JSON.stringify(responseComment);

} catch (err) {
  result = false;
  message = err.message;
}
