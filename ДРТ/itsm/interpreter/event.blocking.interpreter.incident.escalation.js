var result = true;
var message = "ok";

let LOGGER = {
  text: '',
  set: function(text, type){
    this.text += '<br><b>' + UTILS.getCurrentDateParse() + '</b> [' + (type ? type : 'INFO') + '] ' + text;
  },
  get: function(){
    return this.text;
  }
}

function saveResultScript(){
  let currentFormData = API.getFormData(dataUUID);
  UTILS.setValue(currentFormData, "date_start_script", {value: UTILS.getCurrentDateParse(), key: UTILS.getCurrentDateParse()});
  UTILS.setValue(currentFormData, "log", {value: LOGGER.get()});
  API.mergeFormData(currentFormData);
}

function parseItem(item) {
  return {
    dataUUID: item.dataUUID,
    responsibleDepartment: {
      key: item.fieldKey.itsm_form_incident_responsibleDepartment,
      value: item.fieldValue.itsm_form_incident_responsibleDepartment
    },
    responsible: {
      key: item.fieldKey.itsm_form_incident_responsible,
      value: item.fieldValue.itsm_form_incident_responsible
    },
    number: item.fieldValue.itsm_form_incident_id
  }
}

function parseResult(res, levels) {
  res.forEach(function(x) {
    if (levels.hasOwnProperty(x.fieldKey.itsm_form_incident_escalationLevel)) {
      levels[x.fieldKey.itsm_form_incident_escalationLevel].count++;
      levels[x.fieldKey.itsm_form_incident_escalationLevel].data.push(parseItem(x));
    } else {
      levels[x.fieldKey.itsm_form_incident_escalationLevel] = {
        count: 1,
        data: [parseItem(x)]
      };
    }
  });
}

function getCountApp(levels) {
  let result = 0;
  for (let key in levels) {
    result += levels[key].count;
  }
  return result;
}

function searchManager(level, departmentID) {
  let managers = [];

  for(let i = 0; i < level + 1; i++) {
    let tmpDep;
    let tmpUser;
    let info = API.httpGetMethod('rest/api/departments/get?departmentID=' + departmentID);
    if(info.parentDepartmentID) tmpDep = info.parentDepartmentID;
    if(info.manager.managerID) tmpUser = info.manager.managerID;

    if(level > 0) {
      info = API.httpGetMethod('rest/api/departments/get?departmentID=' + tmpDep);
      if(info.parentDepartmentID) tmpDep = info.parentDepartmentID;
      if(info.manager.managerID) tmpUser = info.manager.managerID;
    }

    if(tmpUser) managers.push(tmpUser);
  }

  return managers.uniq();
}

try {
  LOGGER.set('<b>Запуск скрипта</b>', 'START');

  let overdueApp = {};
  let stopPeriod = UTILS.getCurrentDateParse();
  let startPeriod = new Date();
  startPeriod.setMonth(startPeriod.getMonth() - 1);
  startPeriod = UTILS.formatDate(startPeriod, true);

  LOGGER.set('Период с <b>' +  startPeriod + '</b> по <b>' + stopPeriod + '</b>');

  let url = 'rest/api/registry/data_ext?registryCode=itsm_registry_incidents&registryRecordStatus=STATE_NOT_FINISHED';
  url += '&field=itsm_form_incident_regdate&condition=MORE_OR_EQUALS&key=' + encodeURIComponent(startPeriod);
  url += '&field1=itsm_form_incident_regdate&condition1=LESS_OR_EQUALS&key1=' + encodeURIComponent(stopPeriod);
  url += '&field2=itsm_form_incident_solvingLate&condition2=EQUALS&key2=1';
  url += '&field3=itsm_form_incident_status&condition3=NOT_EQUALS&key3=6';
  url += '&field4=itsm_form_incident_status&condition4=NOT_EQUALS&key4=7';
  url += '&fields=itsm_form_incident_responsibleDepartment&fields=itsm_form_incident_id&fields=itsm_form_incident_responsible&fields=itsm_form_incident_escalationLevel';

  let res = API.httpGetMethod(url);
  if(res.recordsCount > 0) parseResult(res.result, overdueApp);

  LOGGER.set('Поиск просроченных обращений 2 линия, найдено: <b>' +  res.recordsCount + '</b>');

  url = 'rest/api/registry/data_ext?registryCode=itsm_registry_incidents&registryRecordStatus=STATE_NOT_FINISHED&groupTerm=and';
  url += '&term=and&field=itsm_form_incident_regdate&condition=MORE_OR_EQUALS&key=' + encodeURIComponent(startPeriod);
  url += '&field=itsm_form_incident_regdate&condition=LESS_OR_EQUALS&key=' + encodeURIComponent(stopPeriod);
  url += '&term1=and&field1=itsm_form_incident_solvingLate&condition1=EQUALS&key1=0';
  url += '&term2=or&field2=itsm_form_incident_status&condition2=EQUALS&key2=2';
  url += '&field2=itsm_form_incident_status&condition2=EQUALS&key2=3';
  url += '&field2=itsm_form_incident_status&condition2=EQUALS&key2=8';
  url += '&field2=itsm_form_incident_status&condition2=EQUALS&key2=9';
  url += '&field2=itsm_form_incident_status&condition2=EQUALS&key2=12';
  url += '&term3=and&field3=itsm_form_incident_regdate&condition3=LESS_OR_EQUALS&key3=' + encodeURIComponent(stopPeriod);
  url += '&fields=itsm_form_incident_responsibleDepartment&fields=itsm_form_incident_id&fields=itsm_form_incident_responsible&fields=itsm_form_incident_escalationLevel';

  res = API.httpGetMethod(url);
  if(res.recordsCount > 0) parseResult(res.result, overdueApp);

  LOGGER.set('Поиск остальных просроченных обращений, найдено: <b>' +  res.recordsCount + '</b>');

  LOGGER.set('Всего найдено просроченных обращений: <b>' + getCountApp(overdueApp) + '</b><br><hr><i>Результат по уровням эскалации:</i><br>' + JSON.stringify(overdueApp, null, '&nbsp') + '<br><hr><br>');

  LOGGER.set('<b>Запуск обработки найденных обращений</b>');

  for(let level in overdueApp) {
    LOGGER.set('Уровнь эскалации: ' + level + '<br>');

    let departments = [];

    let tmpDeps = overdueApp[level].data.map(function(x){
      return x.responsibleDepartment.key;
    }).uniq();

    tmpDeps.forEach(function(id){
      let tmpInfo = overdueApp[level].data.filter(function(x){
        if(x.responsibleDepartment.key == id) return x;
      })[0];
      departments.push(tmpInfo.responsibleDepartment);
    });

    departments.forEach(function(department){
      let manager = searchManager(level, department.key);
      if(manager.length > 0) {
        let theme = 'ITSM-уведомление о нарушении SLA по группе исполнителей ' + department.value;
        let subject = 'По группе исполнителей '  + department.value + ' просрочены более ' + level + ' дней следующие обращения: <br>';
        let nextLevel = Number(level) || 0;
        nextLevel++;

        overdueApp[level].data.forEach(function(item){
          subject += '<br>' + item.number + ' - исполнитель/и ' + item.responsible.value;
          API.mergeFormData({
            uuid: item.dataUUID,
            data: [{id: 'itsm_form_incident_escalationLevel', type: 'numericinput', value: String(nextLevel), key: String(nextLevel)}]
          });
        });

        subject += '<br><br>Это автоматическое уведомление - пожалуйста, не отвечайте на него.';

        let resultSend = API.sendNotification({
          header: theme,
          message: subject,
          users: manager
        });

        if(resultSend && resultSend.errorCode == 0) {
          LOGGER.set('Подразделение: ' + department.value + ' || ' + resultSend.errorMessage);
        } else {
          LOGGER.set('Подразделение: ' + department.value + ' || Ошибка отправки письма:<br>' + JSON.stringify(resultSend, null, '&nbsp') + "<br>", 'ERROR');
        }

      } else {
        LOGGER.set('Подразделение: ' + department.value + ' || Не найден руководитель');
      }
    });
  }

  LOGGER.set('<b>Завершение работы скрипта</b>', 'FINISH');
  saveResultScript();

} catch (err) {
  LOGGER.set(err.message, 'ERROR');
  saveResultScript();
  log.error(err.message);
  message = err.message;
}
