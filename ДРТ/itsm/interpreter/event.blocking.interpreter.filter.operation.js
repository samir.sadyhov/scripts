// Статусы инцидента
let NEW_STATUS = 1; // Зарегистрирован
let IN_QUEUE = 2; // На очереди
let IN_PROGRESS = 3; // В процессе
let WAITING_USER = 4; // Ожидает ответа пользователя
let WAITING_PROBLEM = 5; // Ожидает проблему
let CLOSED = 6; // Закрыт
let WAITING_USER_RATE = 7; // Ожидает подтверждения завершения
let WAITING_EQUIPMENT = 8; // В ожидании выделения техники
let WAITING_SUPPLY = 9; // Направлен внешнему поставщику
let INFO_GIVEN = 10; // Информация предоставлена
let WRONG_WAY = 11; // Неверно направлен
let RE_OPEN = 12; // Направлен повторно
let WAITING_MASS_INCIDENT = 13; //Ожидает закрытия массового инцидента
let WAITING_APPROVAL = 91; // На согласовании
let APPROVE = 15; // Согласовано
let NOT_APPROVE = 16; // Не согласовано


//Рекурсивная функция ищет actionID работы
function getLastWorkId(processes) {
  let workId = null;
  processes.some(function(process) {
    if (process.subProcesses) workId = getLastWorkId(process.subProcesses);
    if (!workId && !process.finished && process.actionID) workId = process.actionID;
    return workId;
  });
  return workId;
}

//Рекурсивная функция ищет actionID работы
function recursiveFilter(arr, currentUserId) {
  var matches = [];
  if (!Array.isArray(arr)) return matches;

  arr.forEach(function(x) {
    if (x.typeID == 'ASSIGNMENT_ITEM' && !x.finished && x.responsibleUserID == currentUserId) matches.push(x);

    if (x.subProcesses.length > 0) {
      var childResults = recursiveFilter(x.subProcesses, currentUserId);
      if (childResults.length) matches = matches.concat(childResults);
    }
  });

  return matches;
}

//Создаем класс jSynergy
let jSynergy = new __classSynergy();

jSynergy.setConnection(false, login, password);

try {
  jSynergy.server.load(dataUUID);

  let operationObject = {
    'filterID': jSynergy.server.getValue('itsm_form_incident_filter_operation_filterID'),
    'actions': jSynergy.server.getValue('itsm_form_incident_filter_operation_actions'),
    'status': jSynergy.server.getValue('itsm_form_incident_filter_operation_status'),
    'decisiontype': jSynergy.server.getValue('itsm_form_incident_filter_operation_decisiontype'),
    'decisiondescription': jSynergy.server.getValue('itsm_form_incident_filter_operation_decisiondescription'),
    'to_author': jSynergy.server.getValue('itsm_form_incident_filter_operation_return_to_author'),
    'system_user': jSynergy.server.getValue('itsm_form_incident_filter_operation_system_user'),
  };
  if (!operationObject.filterID && !operationObject.filterID.value) throw new Error('Не найден фильтр "itsm_form_incident_filter_operation_filterID"');

  let data = jSynergy.server.api('registry/data_ext?registryCode=itsm_registry_incidents&filterID=' + operationObject.filterID.value);
  if (!data || !data.result) throw new Error('Данные по фильтру "' + operationObject.filterID.value + '" не найдены.');

  data.result.forEach(function(item) {
    console.error("CHECK doc id " + item.documentID);
    if (operationObject.actions.key && operationObject.actions.key != 0) {
      switch (+operationObject.actions.key) {
        case NEW_STATUS:
          let workID = recursiveFilter(
            jSynergy.server.api('workflow/get_execution_process?documentID=' + item.documentID),
            operationObject.system_user.key
          );
          if (!workID || !workID[0]) {
            console.error("CHECK Doesn't have workID " + item.documentID);
            return;
          }
          workID = workID[0].actionID;
          let formWork = jSynergy.server.api("rest/api/workflow/work/get_form_for_result?formCode=itsm_form_incident_wcf_new&workID=" + workID);
          jSynergy.server.load(formWork.dataUUID)

          jSynergy.server.setValue('itsm_form_incident_wcf_status', {
            key: "" + operationObject.status.key,
            value: "" + operationObject.status.value
          });

          switch (+operationObject.status.key) {
            case WAITING_USER:
              if (operationObject.to_author.value) jSynergy.server.setValue('itsm_form_incident_wcf_return_to_author', operationObject.to_author.value);
              break;
            case CLOSED:
              if (operationObject.decisiontype.value && operationObject.decisiontype.key) {
                jSynergy.server.setValue('itsm_form_incident_wcf_decisiontype', {
                  key: "" + operationObject.decisiontype.key,
                  value: "" + operationObject.decisiontype.value
                });
              }
              if (operationObject.decisiondescription.value) jSynergy.server.setValue('itsm_form_incident_wcf_decisiondescription', operationObject.decisiondescription.value);
              break;
          }

          jSynergy.server.save();
          jSynergy.setConnection(false, "itsm_service_user", "1");
          jSynergy.server.api('workflow/work/set_result', {
            type: 'POST',
            data: {
              workID: workID,
              completionForm: 'FORM',
              type: 'work',
              file_identifier: formWork.file_identifier
            }
          });
          break;
        case IN_QUEUE:
        case IN_PROGRESS:
          if (item.status != 'STATE_NOT_FINISHED') {
            jSynergy.server.api('registry/activate_doc?documentID=' + item.documentID);
          } else {
            jSynergy.server.api('docflow/doc/stop_route?documentID=' + item.documentID, {
              type: 'POST'
            });
          }
          break;
      }
    }
  });
  result = true;
  message = "" + JSON.stringify(operationObject.decisiontype);
} catch (err) {
  result = false;
  message = err.message;
}
