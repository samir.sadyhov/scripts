/**
 * Создаем класс jSynergy
 */
let jSynergy = new __classSynergy();
jSynergy.setConnection(false, login, password);

let currentUserId = jSynergy.server.api('person/auth').userid;

/**
 * Рекурсивная функция - ищет работу по документу с типом `ASSIGNMENT_ITEM` и не завершенная
 * @param   arr     Массив содержащий в себе `ход-выполнения документа`
 * @return  Массив с работой
 */
function recursiveFilter(arr) {
  var matches = [];
  if (!Array.isArray(arr)) return matches;

  arr.forEach(function(x) {
    if (x.typeID == 'ASSIGNMENT_ITEM' && !x.finished && x.responsibleUserID == currentUserId) matches.push(x);

    if (x.subProcesses.length > 0) {
      var childResults = recursiveFilter(x.subProcesses);
      if (childResults.length) matches = matches.concat(childResults);
    }
  });

  return matches;
}

try {
  jSynergy.server.load(dataUUID);

  let commentFormCompletion = '';

  let formData = {
    'itsm_form_problem_parent_incident': jSynergy.server.getValue('itsm_form_problem_parent_incident'),
    'itsm_form_problem_oldIncidentStatus': jSynergy.server.getValue('itsm_form_problem_oldIncidentStatus'),
    'itsm_form_problem_decisiontype': jSynergy.server.getValue('itsm_form_problem_decisiontype'),
    'itsm_form_problem_decisiondescription': jSynergy.server.getValue('itsm_form_problem_decisiondescription'),
    'itsm_form_problem_stop_incident': jSynergy.server.getValue('itsm_form_problem_stop_incident'),
    'itsm_form_problem_responsible': jSynergy.server.getValue('itsm_form_problem_responsible'),
    'itsm_form_problem_finish_date': jSynergy.server.getValue('itsm_form_problem_finish_date')  //#229 п.2
  };

  let incidentID = jSynergy.server.ApiUtils.getDataUUID(formData.itsm_form_problem_parent_incident.key);

  jSynergy.server.load(incidentID);

  /**
   * Ищем ID строки в дин.таблице
   */
  let converTbl = jSynergy.server.converTable('itsm_form_incident_table10');
  let idx = 0;
  converTbl.forEach(function(item) {
    if (item.itsm_form_incident_problemlink.key && item.itsm_form_incident_problemlink.key == documentID) {
      idx = +(item.itsm_form_incident_problemlink.id.substring(item.itsm_form_incident_problemlink.id.lastIndexOf('-b') + 2));
      return;
    }
  });
  // - - - - - - - - - - - - - -

  jSynergy.server.setValue('itsm_form_incident_status', {
    key: "" + formData.itsm_form_problem_oldIncidentStatus.key,
    value: "" + formData.itsm_form_problem_oldIncidentStatus.value
  });

  jSynergy.server.setValue('itsm_form_incident_problem_decisiontype', 'itsm_form_incident_table10', idx, {
    key: "" + formData.itsm_form_problem_decisiontype.key,
    value: "" + formData.itsm_form_problem_decisiontype.value
  });

  //#229 п.2
  jSynergy.server.setValue('itsm_form_incident_problem_finish_date', 'itsm_form_incident_table10', idx, {
    key: "" + formData.itsm_form_problem_finish_date.key,
    value: "" + formData.itsm_form_problem_finish_date.value
  });

  jSynergy.server.setValue('itsm_form_incident_problem_decisiondescription', 'itsm_form_incident_table10', idx, "" + formData.itsm_form_problem_decisiondescription.value);

  /**
   * Если вкл. чекбокс "Присотановить исполнение обращения"
   */
  if (formData.itsm_form_problem_stop_incident.keys && formData.itsm_form_problem_stop_incident.keys.length) {
    // Ищем работу от SU
    let processes = jSynergy.server.api('rest/api/workflow/get_execution_process?documentID=' + formData.itsm_form_problem_parent_incident.key);
    processes = recursiveFilter(processes);
    // Если нашли то...
    if (processes.length) {

      processes.forEach(function(process) {
        // Создаем ФЗ для проблемы
        let formWork = jSynergy.server.api('workflow/work/get_form_for_result?formCode=itsm_form_incident_wcf_new&workID=' + process.actionID);
        commentFormCompletion = formWork.comment;

        // Подгружаем форму в процесс обработки
        jSynergy.server.load(formWork.dataUUID);

        // Устанавливаем старый статус инцидента
        jSynergy.server.setValue('itsm_form_incident_wcf_status', {
          key: "" + formData.itsm_form_problem_oldIncidentStatus.key,
          value: "" + formData.itsm_form_problem_oldIncidentStatus.value
        });

        // Сохраняем ФЗ
        jSynergy.server.save();

        // Переключаемся на инцидент
        jSynergy.server.selectForm(incidentID);
        // Сохраняем инцидент
        jSynergy.server.save();

        // Убиваем работу
        let resultComlete = jSynergy.server.api('workflow/work/set_result', {
          type: 'POST',
          data: {
            workID: process.actionID,
            completionForm: 'FORM',
            type: 'work',
            file_identifier: formWork.file_identifier
          },
          headers: {
            'Content-Type': 'application/x-www-form-urlencoded'
          }
        });

      });

    } else {
      // Переключаемся на инцидент
      jSynergy.server.selectForm(incidentID);
      // Сохраняем инцидент
      jSynergy.server.save();
    }
  } else {
    // Переключаемся на инцидент
    jSynergy.server.selectForm(incidentID);
    // Сохраняем инцидент
    jSynergy.server.save();
  }

  result = true;
  message = commentFormCompletion;
} catch (e) {
  result = false;
  message = e.message;
}
