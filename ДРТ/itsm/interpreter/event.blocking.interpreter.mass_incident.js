var result = true;
var message = "ok";

try {
  let currentFormData = API.getFormData(dataUUID);
  let massIncident = UTILS.getValue(currentFormData, 'itsm_form_incident_mass_reglink');

  if(!massIncident || !massIncident.hasOwnProperty('key')) throw new Error('Не найдено ссылки на массовый инцидент');

  let massIncidentData = API.getFormData(API.getAsfDataId(massIncident.key));
  let incidentChild = UTILS.getValue(massIncidentData, 'itsm_form_incident_child');

  if(!incidentChild.hasOwnProperty('key')) {
    incidentChild.value = API.getDocMeaningContent(documentID);
    incidentChild.key = documentID;
    incidentChild.valueID = documentID;
  } else {
    if(incidentChild.key.indexOf(documentID) != -1) throw new Error('Данный инцидент ранее был записан в поле дочернии инциденты');
    incidentChild.value += ';' + API.getDocMeaningContent(documentID);
    incidentChild.key += ';' + documentID;
    incidentChild.valueID += ';' + documentID;
  }

  log.info(API.mergeFormData(massIncidentData));
  message = "Обращение было записано в поле дочернии инциденты";

} catch (err) {
  log.error(err.message);
  message = err.message;
}
