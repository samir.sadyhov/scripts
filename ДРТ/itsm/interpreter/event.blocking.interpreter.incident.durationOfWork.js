// Статусы инцидента
let NEW_STATUS = 1; // Зарегистрирован
let IN_QUEUE = 2; // На очереди
let IN_PROGRESS = 3; // В процессе
let WAITING_USER = 4; // Ожидает ответа пользователя
let WAITING_PROBLEM = 5; // Ожидает проблему
let CLOSED = 6; // Закрыт
let WAITING_USER_RATE = 7; // Ожидает подтверждения завершения
let WAITING_EQUIPMENT = 8; // В ожидании выделения техники
let WAITING_SUPPLY = 9; // Направлен внешнему поставщику
let INFO_GIVEN = 10; // Информация предоставлена
let WRONG_WAY = 11; // Неверно направлен
let RE_OPEN = 12; // Направлен повторно
let WAITING_MASS_INCIDENT = 13; //Ожидает закрытия массового инцидента
let WAITING_APPROVAL = 91; // На согласовании
let APPROVE = 14; // Согласовано
let NOT_APPROVE = 15; // Не согласовано

var result = true;
var message = "ok";

function getDateDiff(start, stop) {
  let client = API.getHttpClient();
  let get = new org.apache.commons.httpclient.methods.GetMethod("http://127.0.0.1:8080/itsm/rest/dateDiff?start=" + start + "&stop=" + stop);
  get.setRequestHeader("Content-type", "application/json");
  client.executeMethod(get);
  let resp = get.getResponseBodyAsString();
  get.releaseConnection();
  return JSON.parse(resp);
}

function processesFilter(processes) {
  let result = [];
  function search(p) {
    p.forEach(function(process) {
      if (process.typeID == 'ASSIGNMENT_ITEM' && process.finished) result.push(process);
      if (process.subProcesses.length > 0) search(process.subProcesses);
    });
  }
  search(processes);
  return result.sort(function(a, b) {
    return UTILS.parseDateTime(b.finished) - UTILS.parseDateTime(a.finished);
  });
};

function setFirstLineReaction(currentFormData, completionFormData) {
  let firstLineReaction = UTILS.getValue(currentFormData, "itsm_form_incident_FirstLineReaction");
  if(firstLineReaction) {
    firstLineReaction = Number(firstLineReaction.key) || 0;
  } else {
    firstLineReaction = 0;
  }
  if(firstLineReaction !== 0) return;

  let wcf_current_date = UTILS.parseDateTime(UTILS.getValue(completionFormData, "itsm_form_incident_wcf_current_date").key);
  let prev_status_time = UTILS.parseDateTime(UTILS.getValue(currentFormData, "itsm_form_incident_prev_status_time").key);

  firstLineReaction = (wcf_current_date.getTime() - prev_status_time.getTime()) / 3600000;
  firstLineReaction = Math.floor(firstLineReaction * 60 * 100) / 100;

  UTILS.setValue(currentFormData, "itsm_form_incident_FirstLineReaction", {value: String(firstLineReaction), key: String(firstLineReaction)});
}

function setSecondLineReaction(currentFormData, completionFormData, currentFormStatus) {
  let secondLineReaction = UTILS.getValue(currentFormData, "itsm_form_incident_SecondLineReaction");
  let secondLineDep = UTILS.getValue(currentFormData, "itsm_form_incident_SecondLineDep");
  let wcfUserDepartment = UTILS.getValue(completionFormData, "itsm_form_incident_wcf_current_user_Department");

  if(secondLineReaction) {
    secondLineReaction = Number(secondLineReaction.key) || 0;
  } else {
    secondLineReaction = 0;
  }

  if(currentFormStatus.key != IN_QUEUE) return;
  if(!wcfUserDepartment || !wcfUserDepartment.hasOwnProperty('key')) return;

  if(secondLineDep || secondLineDep.hasOwnProperty('key')) {
    if(secondLineReaction > 0 && secondLineDep.key == wcfUserDepartment.key) return;
  }

  let wcf_current_date = UTILS.parseDateTime(UTILS.getValue(completionFormData, "itsm_form_incident_wcf_current_date").key);
  let prev_status_time = UTILS.parseDateTime(UTILS.getValue(currentFormData, "itsm_form_incident_prev_status_time").key);

  secondLineReaction = getDateDiff(prev_status_time.getTime(), wcf_current_date.getTime()) / 3600;
  secondLineReaction = Math.floor(secondLineReaction * 60 * 100) / 100;

  UTILS.setValue(currentFormData, "itsm_form_incident_SecondLineReaction", {value: String(secondLineReaction), key: String(secondLineReaction)});
  UTILS.setValue(currentFormData, "itsm_form_incident_SecondLineDep", wcfUserDepartment);
}

try {
  let processes = processesFilter(API.getProcesses(documentID));

  let completionFormDataUUID = processes.map(function(x) {
    return API.getWorkCompletionData(x.actionID).result.dataUUID;
  }).filter(function(uuid){ return uuid != null})[0];

  if(!completionFormDataUUID) throw new Error('Не найдена форма завершения');

  let completionFormData = API.getFormData(completionFormDataUUID);
  let currentFormData = API.getFormData(dataUUID);
  let completionFormStatus = UTILS.getValue(completionFormData, "itsm_form_incident_wcf_status");
  let currentFormStatus = UTILS.getValue(currentFormData, "itsm_form_incident_status");

  let firstLineStatus = [IN_QUEUE, WAITING_USER, WAITING_USER_RATE, WAITING_PROBLEM];
  let secondLineStatus = [WAITING_EQUIPMENT, WAITING_SUPPLY, WAITING_USER, WRONG_WAY, WAITING_USER_RATE, WAITING_PROBLEM, IN_PROGRESS];
  let thirdLineStatus = [IN_PROGRESS, WAITING_SUPPLY, WAITING_USER_RATE, WAITING_PROBLEM];

  let firstLineTime = Number(UTILS.getValue(currentFormData, "itsm_form_incident_firstLineTime").key) || 0;
  let secondLineTime = Number(UTILS.getValue(currentFormData, "itsm_form_incident_secondLineTime").key) || 0;
  let thirdLineTime = Number(UTILS.getValue(currentFormData, "itsm_form_incident_thirdLineTime").key) || 0;

  switch (Number(currentFormStatus.key)) {

    // 2. подсчет времени по  1-й линии
    case NEW_STATUS:
      if(firstLineStatus.indexOf(Number(completionFormStatus.key)) !== -1) {
        let wcf_current_date = UTILS.parseDateTime(UTILS.getValue(completionFormData, "itsm_form_incident_wcf_current_date").key);
        let incident_regdate = UTILS.parseDateTime(UTILS.getValue(currentFormData, "itsm_form_incident_regdate").key);
        let hours = getDateDiff(incident_regdate.getTime(), wcf_current_date.getTime()) / 3600;
        firstLineTime += Math.floor(hours * 100) / 100;
        let minutes = Math.floor(firstLineTime * 60 * 100) / 100;
        UTILS.setValue(currentFormData, "itsm_form_incident_firstLineTime", {value: String(firstLineTime), key: String(firstLineTime)});
        UTILS.setValue(currentFormData, "itsm_form_incident_reactionTimeMinutes2", {value: String(minutes), key: String(minutes)});
      }
      setFirstLineReaction(currentFormData, completionFormData);
      break;
    case INFO_GIVEN:
    case WRONG_WAY:
      if(firstLineStatus.indexOf(Number(completionFormStatus.key)) !== -1) {
        let wcf_current_date = UTILS.parseDateTime(UTILS.getValue(completionFormData, "itsm_form_incident_wcf_current_date").key);
        let prev_status_time = UTILS.parseDateTime(UTILS.getValue(currentFormData, "itsm_form_incident_prev_status_time").key);
        let hours = getDateDiff(prev_status_time.getTime(), wcf_current_date.getTime()) / 3600;
        firstLineTime += Math.floor(hours * 100) / 100;
        let minutes = Math.floor(firstLineTime * 60 * 100) / 100;
        UTILS.setValue(currentFormData, "itsm_form_incident_firstLineTime", {value: String(firstLineTime), key: String(firstLineTime)});
        UTILS.setValue(currentFormData, "itsm_form_incident_reactionTimeMinutes2", {value: String(minutes), key: String(minutes)});
      }
      setFirstLineReaction(currentFormData, completionFormData);
      break;

    // 3. подсчет времени 2-й линии
    case IN_QUEUE:
    case IN_PROGRESS:
    case RE_OPEN:
      if(secondLineStatus.indexOf(Number(completionFormStatus.key)) !== -1) {
        let wcf_current_date = UTILS.parseDateTime(UTILS.getValue(completionFormData, "itsm_form_incident_wcf_current_date").key);
        let prev_status_time = UTILS.parseDateTime(UTILS.getValue(currentFormData, "itsm_form_incident_prev_status_time").key);
        let hours = getDateDiff(prev_status_time.getTime(), wcf_current_date.getTime()) / 3600;
        secondLineTime += Math.floor(hours * 100) / 100;
        UTILS.setValue(currentFormData, "itsm_form_incident_secondLineTime", {value: String(secondLineTime), key: String(secondLineTime)});
      }
      setSecondLineReaction(currentFormData, completionFormData, currentFormStatus);
      break;

    // 4. подсчет времени 3-й линии (вн.поставщиков)
    case WAITING_SUPPLY:
      if(thirdLineStatus.indexOf(Number(completionFormStatus.key)) !== -1) {
        let wcf_current_date = UTILS.parseDateTime(UTILS.getValue(completionFormData, "itsm_form_incident_wcf_current_date").key);
        let prev_status_time = UTILS.parseDateTime(UTILS.getValue(currentFormData, "itsm_form_incident_prev_status_time").key);
        let hours = getDateDiff(prev_status_time.getTime(), wcf_current_date.getTime()) / 3600;
        thirdLineTime += Math.floor(hours * 100) / 100;
        UTILS.setValue(currentFormData, "itsm_form_incident_thirdLineTime", {value: String(thirdLineTime), key: String(thirdLineTime)});
      }
      break;
  }

  // 1. Для каждого статуса (case)  - добавить копирование:
  UTILS.setValue(currentFormData, "itsm_form_incident_prev_status", UTILS.getValue(currentFormData, "itsm_form_incident_status"));

  // 5. Проверка просроченности
  let manager_duration = UTILS.getValue(currentFormData, "itsm_form_incident_manager_duration");
  let responsible_duration = UTILS.getValue(currentFormData, "itsm_form_incident_responsible_duration");
  let supplier_duration = UTILS.getValue(currentFormData, "itsm_form_incident_supplier_duration");

  if(firstLineTime > parseFloat(manager_duration.value)) {
    UTILS.setValue(currentFormData, "itsm_form_incident_reactionLate", {key: '1', value: 'Да'});
  } else {
    UTILS.setValue(currentFormData, "itsm_form_incident_reactionLate", {key: '0', value: 'Нет'});
  }
  if(secondLineTime > parseFloat(responsible_duration.value)) {
    UTILS.setValue(currentFormData, "itsm_form_incident_solvingLate", {key: '1', value: 'Да'});
  } else {
    UTILS.setValue(currentFormData, "itsm_form_incident_solvingLate", {key: '0', value: 'Нет'});
  }
  if(thirdLineTime > parseFloat(supplier_duration.key)) {
    UTILS.setValue(currentFormData, "itsm_form_incident_extSupplierLate", {key: '1', value: 'Да'});
  } else {
    UTILS.setValue(currentFormData, "itsm_form_incident_extSupplierLate", {key: '0', value: 'Нет'});
  }

  // 6. Общее время решения
  let fullTime = firstLineTime + secondLineTime + thirdLineTime;
  fullTime = Math.floor(fullTime * 100) / 100;
  UTILS.setValue(currentFormData, "itsm_form_incident_FullTime", {value: String(fullTime), key: String(fullTime)});

  API.saveFormData(currentFormData);

} catch (err) {
  log.error(err.message);
  message = err.message;
}
