var result = true;
var message = "ok";

try {

  let THEME_CMP_ID = 'itsm_form_settings_problem_notification_theme';
  let SUBJECT_AUTHOR = 'itsm_form_settings_problem_notification_subject_author';
  let SUBJECT_USERS = 'itsm_form_settings_problem_notification_subject_users';

  let SETTINGS = API.httpGetMethod('rest/api/registry/data_ext?registryCode=itsm_registry_settings&fields=' + THEME_CMP_ID + '&fields=' + SUBJECT_AUTHOR + '&fields=' + SUBJECT_USERS);

  if(SETTINGS.recordsCount == 0) throw new Error('Не найдены настройки');

  let theme = SETTINGS.result[0].fieldValue[THEME_CMP_ID];
  let subjectAuthor = SETTINGS.result[0].fieldValue[SUBJECT_AUTHOR];
  let subjectUsers = SETTINGS.result[0].fieldValue[SUBJECT_USERS];

  if(!theme || !subjectAuthor || !subjectUsers) throw new Error('Не найдены настройки для формирования сообщения');

  let currentFormData = API.getFormData(dataUUID);

  UTILS.getParams(theme).forEach(function(id) {
    theme = theme.replace('${'+id+'}', UTILS.getValue(currentFormData, id).value || '');
  });

  let author = UTILS.getValue(currentFormData, 'itsm_form_problem_author');
  let users = UTILS.getValue(currentFormData, 'itsm_form_problem_notification_users');

  if(author && author.hasOwnProperty('key')) {
    UTILS.getParams(subjectAuthor).forEach(function(id) {
      subjectAuthor = subjectAuthor.replace('${'+id+'}', UTILS.getValue(currentFormData, id).value || '');
    });
    API.sendNotification({
      header: theme,
      message: subjectAuthor,
      users: [author.key]
    });
  }

  if(users && users.hasOwnProperty('key')) {
    let userIDs = users.key.split(';');
    let userNames = users.value.split(', ');
    userIDs.forEach(function(userid, i){
      let tmpSubject = subjectUsers;
      UTILS.getParams(subjectUsers).forEach(function(id) {
        let replaceValue = UTILS.getValue(currentFormData, id).value || '';
        if(id == 'itsm_form_problem_notification_users') replaceValue = userNames[i];
        tmpSubject = tmpSubject.replace('${'+id+'}', replaceValue);
      });
      API.sendNotification({
        header: theme,
        message: tmpSubject,
        users: [userid]
      });
    });
  }

} catch (err) {
  log.error(err);
  message = err.message;
}
