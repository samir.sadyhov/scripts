function addWorkHours(hours) {
  let client = new org.apache.commons.httpclient.HttpClient();
  let get = new org.apache.commons.httpclient.methods.GetMethod("http://127.0.0.1:8080/itsm/rest/addHours?hours=" + hours);
  client.executeMethod(get);
  let newDate = String(get.getResponseBodyAsString());
  get.releaseConnection();
  return newDate;
}

var result = true;
var message = "ok";

try {
  let duration = '8';

  let priorityDict = {
    1: 'itsm_form_supplier_critical', //Критичный
    2: 'itsm_form_supplier_high', //Высокий
    3: 'itsm_form_supplier_medium', //Средний
    4: 'itsm_form_supplier_low', //Низкий
    5: 'itsm_form_supplier_very_low' //Очень низкий
  }

  let currentFormData = API.getFormData(dataUUID);

  let servicelink = UTILS.getValue(currentFormData, 'itsm_form_incident_servicelink');
  let priority = UTILS.getValue(currentFormData, 'itsm_form_incident_priority');
  let supplier = UTILS.getValue(currentFormData, 'itsm_form_incident_supplier');

  if(servicelink && servicelink.hasOwnProperty('key')) {
    if(supplier && supplier.hasOwnProperty('key')) {
      let supplierFormData = API.getFormData(API.getAsfDataId(supplier.key));
      let slaTbale = UTILS.getValue(supplierFormData, 'itsm_form_supplier_sla');
      if(slaTbale && slaTbale.hasOwnProperty('data')) {
        let block = slaTbale.data.filter(function(item){ if(item.key == servicelink.key) return item});
        if(block.length > 0) {
          block = block[0];
          let tableBlockIndex = block.id.slice(block.id.indexOf('-b') + 2);
          let durationSLA = UTILS.getValue(slaTbale, priorityDict[priority.key] + '-b' + tableBlockIndex);
          if(durationSLA && durationSLA.hasOwnProperty('key')) duration = durationSLA.key;
        } else {
          message = "Не найдено совпадений в таблице из карточки внешнего поставщика, устанавливаем продолжительность в 8 часов";
        }
      } else {
        message = "Не заполнена таблица в карточке внешнего поставщика, устанавливаем продолжительность в 8 часов";
      }
    } else {
      message = "Не выбран Внешний поставщик, устанавливаем продолжительность в 8 часов";
    }
  } else {
    message = "Не выбран сервис, устанавливаем продолжительность в 8 часов";
  }

  let dd = addWorkHours(duration);
  let supplier_duration = UTILS.setValue(currentFormData, 'itsm_form_incident_supplier_duration', {key: duration, value: duration});
  let supply_date = UTILS.setValue(currentFormData, 'itsm_form_incident_supply_date', {key: dd, value: dd});

  log.info(supply_date, API.mergeFormData(currentFormData));

} catch (err) {
  log.error(err);
  message = err.message;
}
