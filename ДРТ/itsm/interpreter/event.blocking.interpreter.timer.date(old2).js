let API = {
  getHttpClient: function(){
    let client = new org.apache.commons.httpclient.HttpClient();
    let creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(login, password);
    client.getParams().setAuthenticationPreemptive(true);
    client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
    return client;
  },
  httpGetMethod: function(methods, type) {
    let client = this.getHttpClient();
    let get = new org.apache.commons.httpclient.methods.GetMethod("http://127.0.0.1:8080/Synergy/" + methods);
    get.setRequestHeader("Content-type", "application/json");
    client.executeMethod(get);
    let resp = get.getResponseBodyAsString();
    get.releaseConnection();
    return type == 'text' ? resp : JSON.parse(resp);
  },
  httpPostMethod: function(methods, params, contentType) {
    let client = this.getHttpClient();
    let post = new org.apache.commons.httpclient.methods.PostMethod("http://127.0.0.1:8080/Synergy/" + methods);
    if(contentType) post.setRequestBody(JSON.stringify(params));
    else for(let key in params) post.addParameter(key, params[key]);
    post.setRequestHeader("Content-type", contentType || "application/x-www-form-urlencoded; charset=utf-8");
    let resp = client.executeMethod(post);
    post.releaseConnection();
    return resp;
  },
  getFormData: function(asfDataId) {
    return this.httpGetMethod("rest/api/asforms/data/" + asfDataId)
  },
  addWorkHours: function(hours) {
    let client = this.getHttpClient();
    let get = new org.apache.commons.httpclient.methods.GetMethod("http://127.0.0.1:8080/itsm/rest/addHours?hours=" + hours);
    client.executeMethod(get);
    let resp = get.getResponseBodyAsString();
    get.releaseConnection();
    return String(resp);
  },
  saveFormData: function(formID, asfDataId, asfData) {
    return this.httpPostMethod("rest/api/asforms/form/multipartdata", {
      form: formID,
      uuid: asfDataId,
      data: "\"data\":" + JSON.stringify(asfData)
    });
  }
};

let log = {
  parse: function(args) {
    let result = [];
    for (let x in args) result.push(JSON.stringify(args[x], null, 4));
    return documentID + '\n' + result.join('\n');
  },
  info: function() {
    console.info(this.parse(arguments));
  },
  error: function() {
    console.error(this.parse(arguments));
  }
}

let UTILS = {
  getValue: function(data, cmpID) {
    data = data.data ? data.data : data;
    for(let i = 0; i < data.length; i++)
    if (data[i].id === cmpID) return data[i];
  },
  setValue: function(asfData, cmpID, data) {
    let field = this.getValue(asfData, cmpID);
    for (let key in data) {
      if(key === 'id' || key === 'type') continue;
      field[key] = data[key];
    }
    return field;
  }
};

let result = true;
let message = "ok";

try {

  let newDate = API.addWorkHours(16);

  let currentFormData = API.getFormData(dataUUID);
  UTILS.setValue(currentFormData, "itsm_form_incident_timerDate", {value: newDate, key: newDate});
  API.saveFormData(currentFormData.form, currentFormData.uuid, currentFormData.data);

  message = "Дата таймера: " + newDate;

} catch (err) {
  log.error(err.message);
  message = err.message;
}
