// Статусы инцидента
let NEW_STATUS = 1; // Зарегистрирован
let IN_QUEUE = 2; // На очереди
let IN_PROGRESS = 3; // В процессе
let WAITING_USER = 4; // Ожидает ответа пользователя
let WAITING_PROBLEM = 5; // Ожидает решение проблемы
let CLOSED = 6; // Закрыто
let WAITING_USER_RATE = 7; // Ожидает оценки пользователя
let WAITING_EQUIPMENT = 8; // В ожидании выделения техники
let WAITING_SUPPLY = 9; // Направлен внешнему поставщику
let INFO_GIVEN = 10; // Информация предоставлена
let WRONG_WAY = 11; // Неверно направлен
let RE_OPEN = 12; // Направлен повторно
let WAITING_MASS_INCIDENT = 13; //Ожидает закрытия массового инцидента
let IN_AGREEMENT = 91; // На согласовании
let AGREED = 15; // Согласовано
let NOT_AGREED = 16; // Не согласовано

let fixDoubleChangeDist = false; // от дубликатов изменения справочника

let responsibleDepartment = model.playerModel.getModelWithId('itsm_form_incident_wcf_current_user_Department');
let responsibleDepartment_Dublicate = model.playerModel.getModelWithId('itsm_form_incident_wcf_responsibleDepartment');

function allUsersListDep() {
  responsibleDepartment_Dublicate.on('valueChange', function() {
    if (responsibleDepartment_Dublicate.getValue()) {
      AS.FORMS.ApiUtils.simpleAsyncGet('rest/api/userchooser/search?showAll=true&departmentID=' + responsibleDepartment_Dublicate.getValue()[0].departmentId, function(usersDep) {
        let usersList = [];
        usersDep.forEach(function(item) {
          usersList.push({
            personID: "" + item.userID,
            personName: "" + item.name
          });
        });
        model.playerModel.getModelWithId('itsm_form_incident_wcf_responsible').setValue(usersList);
      });
    }
  });
}

function hideFields() {
  let val = model.value[0];
  view.playerView.getViewWithId('itsm_form_incident_wcf_type_label').setVisible(false);
  view.playerView.getViewWithId('itsm_form_incident_wcf_type').setVisible(false);
  view.playerView.getViewWithId('itsm_form_incident_wcf_servicelink_label').setVisible(false);
  view.playerView.getViewWithId('itsm_form_incident_wcf_servicelink').setVisible(false);
  view.playerView.getViewWithId('itsm_form_incident_wcf_current_user').setVisible(false);
  view.playerView.getViewWithId('itsm_form_incident_wcf_current_user_label').setVisible(false);
  view.playerView.getViewWithId('itsm_form_incident_wcf_current_user_Department').setVisible(false);

  view.playerView.getViewWithId('itsm_form_incident_wcf_responsible').setVisible(val == IN_QUEUE);
  view.playerView.getViewWithId('itsm_form_incident_wcf_responsible_label').setVisible(val == IN_QUEUE);
  view.playerView.getViewWithId('itsm_form_incident_wcf_responsibleDepartment').setVisible(val == IN_QUEUE);

  view.playerView.getViewWithId('itsm_form_incident_wcf_responsible_inprogress').setVisible(val == IN_PROGRESS || val == RE_OPEN);
  view.playerView.getViewWithId('itsm_form_incident_wcf_responsible_label_inprogress').setVisible(val == IN_PROGRESS || val == RE_OPEN);
  view.playerView.getViewWithId('itsm_form_incident_wcf_responsibleDepartment_inprogress').setVisible(val == IN_PROGRESS || val == RE_OPEN);
  view.playerView.getViewWithId('itsm_form_incident_wcf_return_to_author').setVisible(val == WAITING_USER);
  view.playerView.getViewWithId('itsm_form_incident_wcf_return_to_author_label').setVisible(val == WAITING_USER);

  view.playerView.getViewWithId('itsm_form_incident_wcf_decisiondescription').setVisible(val == WAITING_USER_RATE);
  view.playerView.getViewWithId('itsm_form_incident_wcf_decisiondescription_label').setVisible(val == WAITING_USER_RATE);
  view.playerView.getViewWithId('itsm_form_incident_wcf_decisiontype').setVisible(val == WAITING_USER_RATE);
  view.playerView.getViewWithId('itsm_form_incident_wcf_decisiontype_label').setVisible(val == WAITING_USER_RATE);
  view.playerView.getViewWithId('itsm_form_incident_wcf_cause').setVisible(val == WAITING_USER_RATE);
  view.playerView.getViewWithId('itsm_form_incident_wcf_cause_label').setVisible(val == WAITING_USER_RATE);
  view.playerView.getViewWithId('itsm_form_incident_wcf_failCategory').setVisible(val == WAITING_USER_RATE);
  view.playerView.getViewWithId('itsm_form_incident_wcf_failCategory_label').setVisible(val == WAITING_USER_RATE);
  view.playerView.getViewWithId('itsm_form_incident_wcf_failType').setVisible(val == WAITING_USER_RATE);
  view.playerView.getViewWithId('itsm_form_incident_wcf_failType_label').setVisible(val == WAITING_USER_RATE);
  view.playerView.getViewWithId('itsm_form_incident_wcf_makenew_knowledge').setVisible(val == WAITING_USER_RATE);
  view.playerView.getViewWithId('itsm_form_incident_wcf_current_date_label').setVisible(val == WAITING_USER_RATE);
  view.playerView.getViewWithId('itsm_form_incident_wcf_current_date').setVisible(val == WAITING_USER_RATE);
  view.playerView.getViewWithId('itsm_form_incident_wcf_new_decision_files').setVisible(val == WAITING_USER_RATE);
  view.playerView.getViewWithId('itsm_form_incident_wcf_new_decision_files_label').setVisible(val == WAITING_USER_RATE);

  view.playerView.getViewWithId('itsm_form_incident_wcf_equipment_date').setVisible(val == WAITING_EQUIPMENT);
  view.playerView.getViewWithId('itsm_form_incident_wcf_equipment_date_label').setVisible(val == WAITING_EQUIPMENT);

  view.playerView.getViewWithId('itsm_form_incident_wcf_supplier').setVisible(val == WAITING_SUPPLY);
  view.playerView.getViewWithId('itsm_form_incident_wcf_supplier_label').setVisible(val == WAITING_SUPPLY);

  view.playerView.getViewWithId('itsm_form_incident_wcf_comment').setVisible(val == WRONG_WAY);
  view.playerView.getViewWithId('itsm_form_incident_wcf_comment_label').setVisible(val == WRONG_WAY);

  view.playerView.getViewWithId('itsm_form_incident_wcf_mark').setVisible(val == CLOSED);
  view.playerView.getViewWithId('itsm_form_incident_wcf_mark_label').setVisible(val == CLOSED);
  view.playerView.getViewWithId('itsm_form_incident_wcf_mark_comment').setVisible(val == CLOSED);
  view.playerView.getViewWithId('itsm_form_incident_wcf_mark_comment_label').setVisible(val == CLOSED);
  view.playerView.getViewWithId('itsm_form_incident_wcf_mark_files').setVisible(val == CLOSED);
  view.playerView.getViewWithId('itsm_form_incident_wcf_mark_files_label').setVisible(val == CLOSED);

  view.playerView.getViewWithId('itsm_form_incident_wcf_mass_label').setVisible(val == WAITING_MASS_INCIDENT);
  view.playerView.getViewWithId('itsm_form_incident_wcf_mass_reglink').setVisible(val == WAITING_MASS_INCIDENT);

  view.playerView.getViewWithId('itsm_form_incident_wcf_major_problemlink_label').setVisible(val == WAITING_PROBLEM);
  view.playerView.getViewWithId('itsm_form_incident_wcf_major_problemlink').setVisible(val == WAITING_PROBLEM);

  view.playerView.getViewWithId('itsm_form_incident_wcf_approval_description_label').setVisible(val == IN_AGREEMENT);
  view.playerView.getViewWithId('itsm_form_incident_wcf_approval_description').setVisible(val == IN_AGREEMENT);
  view.playerView.getViewWithId('itsm_form_incident_wcf_approval_users_label').setVisible(val == IN_AGREEMENT);
  view.playerView.getViewWithId('itsm_form_incident_wcf_approval_users').setVisible(val == IN_AGREEMENT);

  view.playerView.getViewWithId('itsm_form_incident_wcf_approval_comment_label').setVisible(val == AGREED || val == NOT_AGREED);
  view.playerView.getViewWithId('itsm_form_incident_wcf_approval_comment').setVisible(val == AGREED || val == NOT_AGREED);
}

function newValueList(list) {
  model.playerModel.getModelWithId('isView').setValue(['1']);
  if (model.playerModel.hasOwnProperty('disabledNewValueList') && model.playerModel.disabledNewValueList === false) return;
  let curList = model.listElements.length > model.listCurrentElements.length ? model.listElements : model.listCurrentElements;
  let curVal = model.getValue()[0];
  model.listElements = [];
  model.listCurrentElements = [];
  curList = curList.filter(i => list.indexOf(+i.value) !== -1);
  model.listElements = curList;
  model.listCurrentElements = curList;
  if (list.indexOf(+curVal) === -1) model.setValue('');
}


function parseDateTime(datetime /*yyyy-mm-dd HH:MM:SS*/ ) {
  let date = datetime.split(' ')[0].split('-');
  let time = datetime.split(' ')[1].split(':');
  return new Date(date[0], date[1] - 1, date[2], time[0], time[1], time[2]);
}

function getListStateProgress(processes) {
  let result = [];
  function search(p) {
    p.forEach(item => {
      if (item.subProcesses.length > 0) {
        search(item.subProcesses);
      } else if (item.code && !item.finished && item.typeID == 'ASSIGNMENT_ITEM') {
        result.push(item);
      }
    });
  }
  search(processes);
  return result.sort((a, b) => parseDateTime(a.started) - parseDateTime(b.started))[result.length - 1];
};

function showRequestInput(data) {
  data.forEach(item => {
    switch (item.id) {
      case 'itsm_form_incident_servicelink':
        if (!item.key) {
          model.playerModel.getModelWithId('itsm_form_incident_wcf_servicelink').getSpecialErrors = function() {
            if (!model.playerModel.getModelWithId('itsm_form_incident_wcf_servicelink').getValue())
              return {
                id: model.asfProperty.id,
                errorCode: AS.FORMS.INPUT_ERROR_TYPE.wrongValue
              };
          };
          view.playerView.getViewWithId('itsm_form_incident_wcf_servicelink').setVisible(true);
          view.playerView.getViewWithId('itsm_form_incident_wcf_servicelink_label').setVisible(true);
        }
        break;
      case 'itsm_form_incident_type':
        if (!item.key && !item.value) {
          model.playerModel.getModelWithId('itsm_form_incident_wcf_type').getSpecialErrors = function() {
            if (!model.playerModel.getModelWithId('itsm_form_incident_wcf_type').getValue())
              return {
                id: model.asfProperty.id,
                errorCode: AS.FORMS.INPUT_ERROR_TYPE.wrongValue
              };
          };
          view.playerView.getViewWithId('itsm_form_incident_wcf_type').setVisible(true);
          view.playerView.getViewWithId('itsm_form_incident_wcf_type_label').setVisible(true);
        }
        break;
    }
  });
  if (!model.isHandleShowRequestInput) {
    model.isHandleShowRequestInput = true;
    model.on('valueChange', function() {
      showRequestInput(data);
    });
  }
}

model.playerModel.on('dataLoad', function() {
  hideFields();

  let api = AS.FORMS.ApiUtils;
  let documentID = null;

  api.getDocumentIdentifier(model.playerModel.asfDataId)
  .then(docID => {
    documentID = docID;
    return api.getAsfDataUUID(docID);
  })
  .then(uuid => api.loadAsfData(uuid))
  .then(asfData => {
    showRequestInput(asfData.data);

    let knowledgeTable = model.playerModel.getModelWithId('itsm_form_incident_knowledge');
    if(knowledgeTable) {
      knowledgeTable.setAsfData(asfData.data.find(x => x.id === 'itsm_form_incident_knowledge'));
      $('[data-asformid="table.addRowButton.itsm_form_incident_knowledge"]').hide();
      $('[data-asformid="table.removeRowButton.itsm_form_incident_knowledge"]').hide();
    }

    if($('.form_player_btn_send').length) {
      $('.form_player_loader_dot').each(function(k, item) {
        $(item).hide(800, function() {
          $(this).remove();
          if($('.form_player_loader_dot').length <= 0) {
            $('.form_player_btn_send').show();
            $('.form_player_loader_dots').remove();
            $('.form_player_btn_send').animate({opacity: 1}, 400);
          }
        });
      });
    }

    if (!fixDoubleChangeDist) {
      fixDoubleChangeDist = true;

      try {
        //не понятно зачем эта проверка, с ней ломается скрипт когда в этом поле нет значения
        // if (model.playerModel.getModelWithId('isView').getValue().length) return;
        AS.SERVICES.showWaitWindow();

        api.simpleAsyncGet(`rest/api/workflow/get_execution_process?documentID=${documentID}`).then(responseCode => {
          responseCode = getListStateProgress(responseCode);

          if (responseCode === undefined) responseCode = {code: false};

          switch (responseCode.code) {

            // Статус 1 Зарегистрирован
            case 'incident_code_new':
              newValueList([
                IN_QUEUE,
                WAITING_USER,
                WAITING_USER_RATE,
                WAITING_MASS_INCIDENT,
                IN_AGREEMENT
              ]);
              break;

              //  Статус 2 На очереди
            case 'incident_queue':
              newValueList([
                IN_PROGRESS,
                WAITING_USER,
                WAITING_USER_RATE,
                WRONG_WAY,
                WAITING_MASS_INCIDENT,
                IN_AGREEMENT
              ]);
              break;

              // Статус 3 В процессе
            case 'incident_progress':
              newValueList([
                WAITING_EQUIPMENT,
                WAITING_SUPPLY,
                WAITING_USER_RATE,
                WAITING_USER,
                WRONG_WAY,
                WAITING_MASS_INCIDENT,
                WAITING_PROBLEM,
                IN_AGREEMENT
              ]);
              break;

              //  Статус 8 В ожидании выделения техники
            case 'incident_code_equipment':
              newValueList([
                IN_PROGRESS,
                WAITING_EQUIPMENT,
                WAITING_USER_RATE
              ]);
              break;

              //  Статус 9 Направлен внешнему поставщику
            case 'incident_code_supply':
              newValueList([
                IN_PROGRESS,
                WAITING_SUPPLY
              ]);
              break;

              //  Статус 4 Ожидает ответа пользователя
            case 'incident_code_wait_info':
              newValueList([
                INFO_GIVEN,
                CLOSED
              ]);
              break;

              //  Статус 7 Ожидает оценки инициатора
            case 'incident_code_wait_confirm':
              newValueList([
                CLOSED,
                RE_OPEN
              ]);
              break;

            //Статус Ожидает закрытия массового инцидента
            case 'incident_mass':
              newValueList([
                NEW_STATUS,
                IN_QUEUE,
                IN_PROGRESS,
                CLOSED
              ]);
              break;

            //Статус Ожидает решения проблемы
            case 'incident_problem':
              newValueList([
                IN_PROGRESS,
                CLOSED
              ]);
              break;

            //Статус На согласовании
            case 'incident_code_approval':
              newValueList([
                AGREED,
                NOT_AGREED
              ]);
              break;
          }

          if(model.getValue()) allUsersListDep();
          AS.SERVICES.hideWaitWindow();
        });
      } catch (err) {
        console.error(err, err.message);
        AS.SERVICES.hideWaitWindow();
      }
    }

  });
});

model.on('valueChange', hideFields);
hideFields();
