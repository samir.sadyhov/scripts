let implement_start = model.playerModel.getModelWithId('itsm_form_change_implement_start');
let implement_finish = model.playerModel.getModelWithId('itsm_form_change_implement_finish');
let servicelink = model.playerModel.getModelWithId('itsm_form_change_servicelink');
let ci = model.playerModel.getModelWithId('itsm_form_change_ci');
let currentChangeID = model.playerModel.getModelWithId('itsm_form_change_id').getValue();

const searchChanges = param => {
  return new Promise(resolve => {
    try {
      let url = `rest/api/registry/data_ext?registryCode=itsm_registry_changes&groupTerm=and&term=and&fields=itsm_form_change_id${param}`;
      AS.FORMS.ApiUtils.simpleAsyncGet(url, res => {
        if(res.recordsCount > 0) resolve(res.result);
        resolve(null);
      }, null, null, err => {
        console.log(`ERROR: ${err.responseJSON.errorMessage}`);
        resolve(null);
      });
    } catch (e) {
      console.log(`ERROR: ${e.message}`);
      resolve(null);
    }
  });
}

const showModalMsg = msg => {
  let msgBg = $('<div>', {class: 'itsm-msg-shadow'});
  let container = $('<div>', {class: 'itsm-msg-container'});
  let body = $('<div>', {class: 'itsm-msg-body'});
  let buttonPanel = $('<div>', {class: 'itsm-msg-buttons'});
  let buttonClose = $('<div>', {class: 'itsm-button'}).text('Закрыть');

  buttonPanel.append(buttonClose);
  body.append(msg);
  container.append(body).append(buttonPanel);
  msgBg.append(container);
  $('body').append(msgBg);

  setTimeout(() => {
    msgBg.addClass('open');
  }, 200);

  buttonClose.on('click', e => {
    msgBg.removeClass('open');
    setTimeout(() => {
       msgBg.remove();
    }, 500);
  });
}

const checkChanges = () => {
  let message = '';
  let defaultParam = '';
  let param = '';

  defaultParam += `&field1=itsm_form_change_implement_start&condition1=LESS_OR_EQUALS&key1=${implement_finish.getValue()}`;
  defaultParam += `&field1=itsm_form_change_implement_finish&condition1=MORE_OR_EQUALS&key1=${implement_start.getValue()}`;

  param = `${defaultParam}&field=itsm_form_change_servicelink&condition=TEXT_EQUALS&key=${servicelink.getValue()}`;
  searchChanges(param).then(res => {
    if(res) {
      let filteredData = res.filter(x => x.fieldValue.itsm_form_change_id !== currentChangeID);
      if(filteredData.length) {
        message += `По данной услуге ${servicelink.getAsfData().value} в это же время запланировано внедрение изменения`;
        filteredData.forEach(item => message += ` ${item.fieldValue.itsm_form_change_id}`);
      }
    }

    param = `${defaultParam}&field=itsm_form_change_ci&condition=TEXT_EQUALS&key=${ci.getValue()}`;
    return searchChanges(param);
  }).then(res => {
    if(res) {
      let filteredData = res.filter(x => x.fieldValue.itsm_form_change_id !== currentChangeID);
      if(filteredData.length) {
        if(message.length > 0) message += '<br><br>';
        message += `По выбранному КЕ ${ci.getAsfData().value} в это же время запланировано внедрение изменения`;
        filteredData.forEach(item => message += ` ${item.fieldValue.itsm_form_change_id}`);
      }
    }

    return message;
  }).then(message => {
    if(message.length == 0) message = 'Конфликтов не обнаружено';

    showModalMsg(message);
  }).catch(err => {
    console.log(`ERROR: ${err.message}`);
  });
}

const createButton = () => {
  let button = $('<button>', {class: 'itsm-button'})
  .text("Проверить")
  .on('click', e => {
    e.preventDefault();
    e.target.blur();
    checkChanges();
  });
  view.container.append(button);
}

createButton();
