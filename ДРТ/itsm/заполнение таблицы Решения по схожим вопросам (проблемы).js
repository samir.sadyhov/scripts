const max_kb_records = 10;
const incFormDivKBTable = "itsm_form_problem_knowledge";
const knowledgeLink = "itsm_form_problem_knowledgeLink";
const knowledgeCode = "itsm_form_problem_knowledgeCode";
const knowledgeDecision = "itsm_form_problem_knowledgeDecision";
const knowledgebase_source = "2"; // 1 - инциденты, 2 - проблемы

function setKnowledgeBaseData(){
  let serviceID = model.getValue();
  let tableModel = model.playerModel.getModelWithId(incFormDivKBTable);

  if(!serviceID || serviceID.isEmpty()) {
    for(let numb in tableModel.getBlockNumbers()) tableModel.removeRow(numb);
    return;
  };

  let params = {
    registryCode: 'itsm_registry_knowledgebase',
    field: 'itsm_form_knowledgebase_servicelink',
    condition: 'TEXT_EQUALS',
    key: serviceID,
    field1: 'itsm_form_knowledgebase_source',
    condition1: 'CONTAINS',
    key1: knowledgebase_source,
    field2: 'itsm_form_knowledgebase_status',
    condition2: 'CONTAINS',
    key2: '1',
    countInPart: max_kb_records,
    sortCmpID: 'itsm_form_knowledgebase_rating',
    sortDesc: false,
    registryRecordStatus: 'STATE_SUCCESSFUL'
  };

  AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/data_ext?${jQuery.param(params)}`, result => {
    let tableView = view.playerView.getViewWithId(incFormDivKBTable);

    for(let numb in tableModel.getBlockNumbers()) tableModel.removeRow(numb);

    if(tableView.getRowsCount() > max_kb_records) return;

    result.result.forEach(item => {
      let tableBlock = tableModel.createRow();
      let row = tableView.getRowsCount() - 1;

      if(row > max_kb_records) return;

      if(!!model.playerModel.getModelWithId(knowledgeLink, incFormDivKBTable, tableBlock.tableBlockIndex)){
        model.playerModel.getModelWithId(knowledgeLink, incFormDivKBTable, tableBlock.tableBlockIndex).setValue(item.documentID);
        model.playerModel.getModelWithId(knowledgeCode, incFormDivKBTable, tableBlock.tableBlockIndex).setValue(item.fieldValue.itsm_form_knowledgebase_id);
        model.playerModel.getModelWithId(knowledgeDecision, incFormDivKBTable, tableBlock.tableBlockIndex).setValue(item.fieldValue.itsm_form_knowledgebase_decisiondescription);
      }
    });
  });

};

model.on('valueChange', setKnowledgeBaseData);
