/* Настройки на компоненте
model.accordion = [];
model.accordion.push({titleID: '', open: true, blocks: []});

@param titleID - ID компонента на форме с типом label для заголовка блока

@param open - true/false, открытый или закрытый блок

@param blocks - массив, перечисление ID компонентов которые будут входить в данный блок.
если в блоке необходимо расположить несколько компонентов по горизонтале, надо передать массив этих компонентов, например:
blocks: ['cmp1', [{header: null, container: 'table1'}, {header: 'label-table2', container: 'table2'}]]
компоненты 'table1', 'table2' будут расположены горизонтально под компонентом 'cmp1'
параметр header это заголовок компонента (действиет для компонетов по горизонтали)
*/

let parentContainer = view.playerView.container;
let accordionContainer = $('<div class="custom-accordion">');
$(view.container).append(accordionContainer);

let userID = AS.OPTIONS.currentUser.userId || AS.OPTIONS.currentUser.userid;
let sessionKey = `accordion-form-data-${model.playerModel.asfDataId}-${userID}`;
let accordionData = [];
if(model.hasOwnProperty('accordion')) accordionData = model.accordion;
if(sessionStorage.getItem(sessionKey)) {
  accordionData = JSON.parse(sessionStorage.getItem(sessionKey));
}

const toggleAcc = (el, block) => {
  el = el[0];
  el.classList.toggle("active");
  block.open = $(el).hasClass('active');
  sessionStorage.setItem(sessionKey, JSON.stringify(accordionData));
  let panel = el.nextElementSibling;
  if (panel.style.maxHeight) panel.style.maxHeight = null;
  else panel.style.maxHeight = "initial";
}

accordionData.forEach(item => {
  let block = $('<div class="accordion-block">')
  let title = $(parentContainer).find(`[data-asformid="label.label.${item.titleID}"]`);
  let button = $(`<button class="accordion-button">${title.text()}</button>`);
  let body = $('<div class="accordion-body">');

  title.parent().parent().parent().hide();
  accordionContainer.append(block);
  block.append(button).append(body);
  button.on('click', () => toggleAcc(button, item));

  item.blocks.forEach(block => {
    if(Array.isArray(block)) {
      if($(document).width() > 768) {
        let tmpTable = $('<table style="margin-top: 20px;">');
        let tmpBody = $('<tbody>');
        let tmpTr = $('<tr>');

        block.forEach((id, i) => {
          let tmpTd = $('<td>').css({"vertical-align":"top"});
          let cc = $(parentContainer).find(`[data-asformid $= ".container.${id.container}"]`);
          cc.parent().parent().hide();
          if(id.header) {
            let hh = $(parentContainer).find(`[data-asformid $= ".container.${id.header}"]`);
            hh.parent().parent().hide();
            tmpTd.append(hh.detach());
          }
          tmpTd.append(cc.detach());
          tmpTr.append(tmpTd);
          if(i !== block.length - 1) tmpTr.append('<td style="width: 20px;">');
        });

        tmpBody.append(tmpTr);
        tmpTable.append(tmpBody);
        body.append(tmpTable);
      } else {
        block.forEach((id, i) => {
          let cc = $(parentContainer).find(`[data-asformid $= ".container.${id.container}"]`);
          cc.parent().parent().hide();
          if(id.header) {
            let hh = $(parentContainer).find(`[data-asformid $= ".container.${id.header}"]`);
            hh.parent().parent().hide();
            body.append(hh.detach());
          }
          body.append(cc.detach());
        });
      }
    } else {
      let cc = $(parentContainer).find(`[data-asformid $= ".container.${block}"]`);
      cc.parent().parent().hide();
      body.append(cc.detach());
    }
  });

  body.append('<div class="accordion-footer"></div>');

  if(item.open) toggleAcc(button, item);
});
