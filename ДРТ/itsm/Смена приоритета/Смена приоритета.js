// Статусы инцидента
let NEW_STATUS = 1; // Зарегистрирован
let IN_QUEUE = 2; // На очереди
let IN_PROGRESS = 3; // В процессе
let WAITING_USER = 4; // Ожидает ответа пользователя
let WAITING_PROBLEM = 5; // Ожидает проблему
let CLOSED = 6; // Закрыт
let WAITING_USER_RATE = 7; // Ожидает подтверждения завершения
let WAITING_EQUIPMENT = 8; // В ожидании выделения техники
let WAITING_SUPPLY = 9; // Направлен внешнему поставщику
let INFO_GIVEN = 10; // Информация предоставлена
let WRONG_WAY = 11; // Неверно направлен
let RE_OPEN = 12; // Направлен повторно
let WAITING_MASS_INCIDENT = 13; //Ожидает закрытия массового инцидента
let WAITING_APPROVAL = 91; // На согласовании
let APPROVE = 14; // Согласовано
let NOT_APPROVE = 15; // Не согласовано

const parseDateTime = datetime => {
  datetime = datetime.split(/\D/);
  return new Date(datetime[0], datetime[1] - 1, datetime[2], datetime[3] || 0, datetime[4] || 0, datetime[5] || 0);
}

const getDateDiff = (start, stop) => {
  return new Promise(resolve => {
    $.ajax({
      url: `${window.location.origin}/itsm/rest/dateDiff?start=${start}&stop=${stop}`,
      method: "GET",
      headers: {
        "Authorization": "Basic " + btoa(AS.OPTIONS.login + ":" + AS.OPTIONS.password),
        "Content-Type": "application/json; charset=utf-8"
      }
    }).done(response => {
      resolve(response);
    }).fail(err => {
      resolve(err.responseJSON);
    });
  });
}

const createSynergyPlayer = (dataUUID) => {
  let player = AS.FORMS.createPlayer();
  player.view.setEditable(true);
  player.showFormByCode('itsm_form_incident_priority_change');
  return player;
}

const getUserGroups = () => {
  let userID = AS.OPTIONS.currentUser.userId || AS.OPTIONS.currentUser.userid;
  return new Promise(resolve => {
    AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/filecabinet/user/${userID}?getGroups=true`)
      .then(res => {
        if (res.errorCode && res.errorCode != 0) {
          resolve(null);
        } else {
          resolve(res.groups);
        }
      });
  });
}

const getResponsibleDuration = priority => {
  let priorityDict = {
    1: 'on_critical_time', //Критичный
    2: 'on_high_time', //Высокий
    3: 'on_medium_time', //Средний
    4: 'on_low_time', //Низкий
    5: 'on_verylow_time' //Очень низкий
  }

  let slaTable;
  let responsibleDuration = [];
  let servicelink = model.playerModel.getModelWithId('itsm_form_incident_servicelink').getAsfData();
  let author = model.playerModel.getModelWithId('itsm_form_incident_author').getAsfData();

  return new Promise(resolve => {
    if (servicelink && servicelink.hasOwnProperty('key')) {
      AS.FORMS.ApiUtils.getAsfDataUUID(servicelink.key)
        .then(uuid => AS.FORMS.ApiUtils.loadAsfData(uuid))
        .then(serviceAsfData => {
          slaTable = serviceAsfData.data.find(x => x.id == 'itsm_form_service_sla');
          return AS.FORMS.ApiUtils.simpleAsyncGet('rest/api/filecabinet/user/' + author.key + '?getGroups=true');
        })
        .then(author => {
          author = author;
          let tableGroups = slaTable.data.filter(x => x.id.substr(0, 5) == 'group');

          tableGroups.forEach(t => {
            if (t && t.hasOwnProperty('key') && t.key && t.key != '') {
              let blockIndex = t.id.substring(5);
              t = t.key.split(';');
              t.forEach(tableGroupID => {
                tableGroupID = tableGroupID.substring(2);
                author.groups.forEach(authorGroup => {
                  if (tableGroupID === authorGroup.groupID) {
                    let val = slaTable.data.find(x => x.id == (priorityDict[priority] + blockIndex));
                    if (!responsibleDuration.find(x => x.id == val.id)) responsibleDuration.push(val);
                  }
                });
              });
            }
          });

          if (responsibleDuration.length) {
            resolve(responsibleDuration[0].key + 'ч');
          } else {
            resolve('8ч');
          }
        });
    } else {
      resolve('8ч');
    }
  });

}


const durationOfWork = (responsible_duration) => {

  let currentStatus = model.playerModel.getModelWithId('itsm_form_incident_status').getAsfData();
  let firstLineTime = Number(model.playerModel.getModelWithId("itsm_form_incident_firstLineTime").getAsfData().key) || 0;
  let secondLineTime = Number(model.playerModel.getModelWithId("itsm_form_incident_secondLineTime").getAsfData().key) || 0;
  let thirdLineTime = Number(model.playerModel.getModelWithId("itsm_form_incident_thirdLineTime").getAsfData().key) || 0;
  let incident_regdate = parseDateTime(model.playerModel.getModelWithId("itsm_form_incident_regdate").getAsfData().key);
  let prev_status_time = parseDateTime(model.playerModel.getModelWithId("itsm_form_incident_prev_status_time").getAsfData().key);
  let manager_duration = model.playerModel.getModelWithId('itsm_form_incident_manager_duration').getAsfData();
  let supplier_duration = model.playerModel.getModelWithId('itsm_form_incident_supplier_duration').getAsfData();

  let wcf_current_date = new Date();
  let fullTime = 0;
  let minutes = 0;
  let data = [];

  return new Promise(resolve => {
    switch (Number(currentStatus.key)) {

      // подсчет времени по  1-й линии
      case NEW_STATUS:
        getDateDiff(incident_regdate.getTime(), wcf_current_date.getTime()).then(hours => {
          hours = hours / 3600;
          firstLineTime += Math.floor(hours * 100) / 100;
          minutes = Math.floor(firstLineTime * 60 * 100) / 100;
          fullTime = firstLineTime + secondLineTime + thirdLineTime;
          fullTime = Math.floor(fullTime * 100) / 100;

          data.push({
            id: 'itsm_form_incident_firstLineTime',
            type: 'numericinput',
            value: String(firstLineTime),
            key: String(firstLineTime)
          });
          data.push({
            id: 'itsm_form_incident_reactionTimeMinutes2',
            type: 'numericinput',
            value: String(minutes),
            key: String(minutes)
          });
          data.push({
            id: 'itsm_form_incident_FullTime',
            type: 'numericinput',
            value: String(fullTime),
            key: String(fullTime)
          });

          if (firstLineTime > parseFloat(manager_duration.value)) {
            data.push({
              id: 'itsm_form_incident_reactionLate',
              type: 'listbox',
              value: 'Да',
              key: '1'
            });
          } else {
            data.push({
              id: 'itsm_form_incident_reactionLate',
              type: 'listbox',
              value: 'Нет',
              key: '0'
            });
          }

          resolve(data);
        });
        break;
      case INFO_GIVEN:
      case WRONG_WAY:
        getDateDiff(prev_status_time.getTime(), wcf_current_date.getTime()).then(hours => {
          hours = hours / 3600;
          firstLineTime += Math.floor(hours * 100) / 100;
          minutes = Math.floor(firstLineTime * 60 * 100) / 100;
          fullTime = firstLineTime + secondLineTime + thirdLineTime;
          fullTime = Math.floor(fullTime * 100) / 100;

          data.push({
            id: 'itsm_form_incident_firstLineTime',
            type: 'numericinput',
            value: String(firstLineTime),
            key: String(firstLineTime)
          });
          data.push({
            id: 'itsm_form_incident_reactionTimeMinutes2',
            type: 'numericinput',
            value: String(minutes),
            key: String(minutes)
          });
          data.push({
            id: 'itsm_form_incident_FullTime',
            type: 'numericinput',
            value: String(fullTime),
            key: String(fullTime)
          });

          if (firstLineTime > parseFloat(manager_duration.value)) {
            data.push({
              id: 'itsm_form_incident_reactionLate',
              type: 'listbox',
              value: 'Да',
              key: '1'
            });
          } else {
            data.push({
              id: 'itsm_form_incident_reactionLate',
              type: 'listbox',
              value: 'Нет',
              key: '0'
            });
          }

          resolve(data);
        });
        break;

        // подсчет времени 2-й линии
      case IN_QUEUE:
      case IN_PROGRESS:
      case RE_OPEN:
        getDateDiff(prev_status_time.getTime(), wcf_current_date.getTime()).then(hours => {
          hours = hours / 3600;
          secondLineTime += Math.floor(hours * 100) / 100;
          fullTime = firstLineTime + secondLineTime + thirdLineTime;
          fullTime = Math.floor(fullTime * 100) / 100;

          data.push({
            id: 'itsm_form_incident_secondLineTime',
            type: 'numericinput',
            value: String(secondLineTime),
            key: String(secondLineTime)
          });
          data.push({
            id: 'itsm_form_incident_FullTime',
            type: 'numericinput',
            value: String(fullTime),
            key: String(fullTime)
          });

          if (secondLineTime > parseFloat(responsible_duration)) {
            data.push({
              id: 'itsm_form_incident_solvingLate',
              type: 'listbox',
              value: 'Да',
              key: '1'
            });
          } else {
            data.push({
              id: 'itsm_form_incident_solvingLate',
              type: 'listbox',
              value: 'Нет',
              key: '0'
            });
          }

          resolve(data);
        });
        break;

        // подсчет времени 3-й линии (вн.поставщиков)
      case WAITING_SUPPLY:
        getDateDiff(prev_status_time.getTime(), wcf_current_date.getTime()).then(hours => {
          hours = hours / 3600;
          thirdLineTime += Math.floor(hours * 100) / 100;
          fullTime = firstLineTime + secondLineTime + thirdLineTime;
          fullTime = Math.floor(fullTime * 100) / 100;

          data.push({
            id: 'itsm_form_incident_thirdLineTime',
            type: 'numericinput',
            value: String(thirdLineTime),
            key: String(thirdLineTime)
          });
          data.push({
            id: 'itsm_form_incident_FullTime',
            type: 'numericinput',
            value: String(fullTime),
            key: String(fullTime)
          });

          if (thirdLineTime > parseFloat(supplier_duration.value)) {
            data.push({
              id: 'itsm_form_incident_extSupplierLate',
              type: 'listbox',
              value: 'Да',
              key: '1'
            });
          } else {
            data.push({
              id: 'itsm_form_incident_extSupplierLate',
              type: 'listbox',
              value: 'Нет',
              key: '0'
            });
          }

          resolve(data);
        });
        break;
      default:
        resolve(data);
    }

  });
}

const mergeFormData = player => {
  let priorityChangeCause = player.model.getModelWithId('itsm_form_incident_priority_change_cause').getAsfData();
  let newPriority = player.model.getModelWithId('itsm_form_incident_priority_change_criticalness').getAsfData();
  let body = {
    uuid: model.playerModel.asfDataId,
    data: [{
        id: 'itsm_form_incident_priority_change',
        type: 'radio',
        value: '1',
        key: 'Да'
      },
      {
        id: 'itsm_form_incident_priority',
        type: 'listbox',
        value: newPriority.value,
        key: newPriority.key
      },
      {
        id: 'itsm_form_incident_change_cause',
        type: 'textarea',
        value: priorityChangeCause.value || ''
      }
    ]
  };

  return new Promise(resolve => {
    getResponsibleDuration(newPriority.key).then(duration => {
      body.data.push({
        id: 'itsm_form_incident_responsible_duration',
        type: 'textarea',
        value: duration
      });

      durationOfWork(duration).then(result => {
        if (result) result.forEach(item => body.data.push(item));
        AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/asforms/data/merge", res => {
          resolve(res);
        }, null, JSON.stringify(body), "application/json; charset=utf-8", err => {
          resolve(err.responseJSON);
        });
      });

    });
  });
}

const stopRoute = documentID => {
  return new Promise(resolve => {
    $.ajax({
      url: `${window.location.origin}/itsm/rest/docflow/stop_route?documentID=${documentID}`,
      method: "POST",
      headers: {
        "Authorization": "Basic " + btoa(AS.OPTIONS.login + ":" + AS.OPTIONS.password),
        "Content-Type": "application/json; charset=utf-8"
      }
    }).done(response => {
      resolve(response);
    }).fail(err => {
      resolve(err.responseJSON);
    });
  });
}

const activateDocument = () => {
  return AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/activate_doc?dataUUID=${model.playerModel.asfDataId}`);
}

const closeWindowInARM = () => {
  $(`.arm-window-document[data-uuid="${model.playerModel.asfDataId}"] .arm-close-button`).click();
}

const closeWindowInSynergy = () => {
  $('td > img[src*="edit.png"]').click();
  $('td > img[src*="close.png"]').click();
}

const closeWindow = () => {
  if(window.location.href.indexOf('Synergy') !== -1) {
    closeWindowInSynergy();
  } else if (window.location.href.indexOf('itsm-arm') !== -1) {
    closeWindowInARM();
  }
}

const changePriority = (player, cb) => {
  if(!player.model.isValid()) {
    AS.SERVICES.showErrorMessage('Заполните обязательные поля!');
    cb(false);
  } else {
    AS.SERVICES.showWaitWindow();

    mergeFormData(player)
    .then(res => {
      if(res.errorCode != 0) throw new Error("Ошибка сохранения данных<br><br>" + res.errorMessage);
      return AS.FORMS.ApiUtils.getDocumentIdentifier(model.playerModel.asfDataId);
    })
    .then(docID => stopRoute(docID))
    .then(res => {
      if(res.errorCode != 0) throw new Error("Ошибка прерывания маршрута<br><br>" + res.errorMessage);
      return activateDocument();
    })
    .then(res => {
      AS.SERVICES.hideWaitWindow();
      model.playerModel.hasChanges = false;
      closeWindow();
      window.showMessage('Приоритет успешно изменен');
      cb(true);
    })
    .catch(err => {
      console.error(err);
      AS.SERVICES.hideWaitWindow();
      AS.SERVICES.showErrorMessage(err.message);
    });

  }
}

const showDialog = () => {
  let dialog = $('<div>');
  let player = createSynergyPlayer();

  if(window.location.href.indexOf('Synergy') === -1) dialog.css({'background-color': '#E1E7F3'});

  dialog.append(player.view.container);

  dialog.dialog({
    modal: true,
    width: 500,
    height: 300,
    title: 'Смена приоритета',
    show: {
      effect: 'fade',
      duration: 300
    },
    hide: {
      effect: 'fade',
      duration: 300
    },
    buttons: {
      'Сохранить': function() {
        changePriority(player, res => {
          if(res) $(this).dialog("close");
        });
      },
      'Отмена': function() {
        $(this).dialog("close");
      }
    },
    close: function() {
      $(this).remove();
    }
  });
}

const createButton = () => {
  let button = $('<button>', {class: 'itsm-button'})
  .text("Смена приоритета")
  .on('click', e => {
    e.preventDefault();
    e.target.blur();
    showDialog();
  });
  view.container.append($('<div style="justify-content: center; display: flex;">').append(button));
}

const initChangePriority = () => {
  getUserGroups().then(groups => {
    let group = groups.find(x => x.groupCode === 'itsm_group_priority_change');
    let currentStatus = model.playerModel.getModelWithId('itsm_form_incident_status').getValue()[0];
    if(group && ['2', '3', '12'].indexOf(currentStatus) !== -1) createButton();
  });
}

initChangePriority();
