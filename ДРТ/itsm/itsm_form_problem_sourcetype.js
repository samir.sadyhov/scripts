const checkSourceType = value => {
  if(!value && !value.length) {
    value = '0';
  } else {
    value = value[0];
  }

  switch (value) {
    case '1':
      ['itsm_form_problem_parent_incident', 'itsm_form_problem_stop_incident'].forEach(cmp => {
        let tmp = view.playerView.getViewWithId(cmp);
        if(tmp) {
          tmp.setEnabled(true);
          tmp.setVisible(true);
        }
      });
      if(view.playerView.getViewWithId('itsm_form_problem_parent_change'))
      view.playerView.getViewWithId('itsm_form_problem_parent_change').setVisible(false);
      break;
    case '2':
      ['itsm_form_problem_parent_incident', 'itsm_form_problem_stop_incident'].forEach(cmp => {
        let tmp = view.playerView.getViewWithId(cmp);
        if(tmp) {
          tmp.setEnabled(false);
          tmp.setVisible(false);
        }
      });
      let parentChange = view.playerView.getViewWithId('itsm_form_problem_parent_change');
      if(parentChange) {
        parentChange.setVisible(true);
        parentChange.setEnabled(true);
      }
      model.playerModel.getModelWithId('itsm_form_problem_stop_incident').setValue(null);
      break;
    case '0':
      ['itsm_form_problem_parent_incident', 'itsm_form_problem_stop_incident', 'itsm_form_problem_parent_change'].forEach(cmp => {
        let tmp = view.playerView.getViewWithId(cmp);
        if(tmp) {
          tmp.setEnabled(false);
          tmp.setVisible(false);
        }
      });
      model.playerModel.getModelWithId('itsm_form_problem_stop_incident').setValue(null);
      break;
  }
}

checkSourceType(model.getValue());
model.on('valueChange', (_1, _2, value) => checkSourceType(value));
