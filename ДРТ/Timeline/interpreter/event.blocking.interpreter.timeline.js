var result = true;
var message = "ok";

function processesFilterNotFinished(processes) {
  let result = [];
  function search(p) {
    p.forEach(function(x) {
      if (x.typeID == 'ASSIGNMENT_ITEM' && !x.finished) result.push(x);
      if (x.subProcesses.length > 0) search(x.subProcesses);
    });
  }
  search(processes);
  return result;
}

function processesFilterFinished(processes) {
  let result = [];
  function search(p) {
    p.forEach(function(x) {
      if (x.typeID == 'ASSIGNMENT_ITEM' && x.finished) result.push(x);
      if (x.subProcesses.length > 0) search(x.subProcesses);
    });
  }
  search(processes);
  return result;
}

try {
  let currentFormData = API.getFormData(dataUUID);
  let status = UTILS.getValue(currentFormData, "status");

  if(!status) throw new Error('не найдено поле status на форме');
  if(status.key == '0') throw new Error('не выбрано значение в поле статуса');

  let documentInfo = API.getDocumentInfo(documentID);
  let registryInfo = API.httpGetMethod("rest/api/registry/info?registryID=" + documentInfo.registryID);

  let searchResult = API.httpGetMethod('rest/api/registry/data_ext?registryCode=registry_step_guide&countInPart=1&loadData=false&field=textbox_registry_code&condition=TEXT_EQUALS&value=' + registryInfo.code);

  if(searchResult.count == 0) throw new Error('не найдено настрок для реестра: ' + registryInfo.code);

  let formSettings = API.getFormData(searchResult.data[0].dataUUID);
  let tableSettings = UTILS.getValue(formSettings, "table_steps");
  if(!tableSettings) throw new Error('не найдена таблица table_steps на форме настроек');

  let settings = UTILS.parseAsfTable(tableSettings);
  if(settings.length == 0) throw new Error('не заполнена таблица table_steps на форме настроек');

  settings.sort(function(a,b){
    return a.textbox_step_number.value - b.textbox_step_number.value;
  });

  let parseTableTimeline = UTILS.parseAsfTable(UTILS.getValue(currentFormData, 'table_timeline'));
  let processes = API.getProcesses(documentID);
  let finishedProcess = processesFilterFinished(processes);
  let notFinishedProcess = processesFilterNotFinished(processes);
  let table_timeline = {
    id: 'table_timeline',
    type: 'appendable_table',
    data: []
  }

  finishedProcess.sort(function(a,b){
    return new Date(b.finished) - new Date(a.finished);
  });

  notFinishedProcess.sort(function(a,b){
    return new Date(b.started) - new Date(a.started);
  });

  // если таблица table_timeline на форме пустая
  if(!parseTableTimeline.length) {
    //если нет завершенных этапов
    if(!finishedProcess.length) {
      let step_number1 = settings.filter(function(x){
        if(x.textbox_step_number.value == '1') return x;
      })[0];

      table_timeline.data.push({
        id: 'textbox_step_number-b1',
        type: 'textbox',
        value: '1'
      });

      table_timeline.data.push({
        id: 'listbox_step_name-b1',
        type: 'listbox',
        value: step_number1.listbox_step_name.value,
        key: step_number1.listbox_step_name.key
      });

      //если есть активные работы, заполняем дату начала и ответственного
      if(notFinishedProcess.length) {
        table_timeline.data.push({
          id: 'entity_responsible-b1',
          type: 'entity',
          value: notFinishedProcess[0].responsibleUserName,
          key: notFinishedProcess[0].responsibleUserID
        });

        table_timeline.data.push({
          id: 'date_stage_start_date-b1',
          type: 'entity',
          value: notFinishedProcess[0].started,
          key: notFinishedProcess[0].started
        });
      }

    } else {
      //если есть завершенные этапы
      let step_number1 = settings.filter(function(x){
        if(x.textbox_step_number.value == '1') return x;
      })[0];
      let step_number2 = settings.filter(function(x){
        if(x.textbox_step_number.value == '2') return x;
      })[0];

      table_timeline.data.push({
        id: 'textbox_step_number-b1',
        type: 'textbox',
        value: '1'
      });

      table_timeline.data.push({
        id: 'listbox_step_name-b1',
        type: 'listbox',
        value: step_number1.listbox_step_name.value,
        key: step_number1.listbox_step_name.key
      });

      table_timeline.data.push({
        id: 'entity_responsible-b1',
        type: 'entity',
        value: finishedProcess[0].responsibleUserName,
        key: finishedProcess[0].responsibleUserID
      });

      table_timeline.data.push({
        id: 'date_stage_start_date-b1',
        type: 'entity',
        value: finishedProcess[0].started,
        key: finishedProcess[0].started
      });

      table_timeline.data.push({
        id: 'date_stage_end_date-b1',
        type: 'entity',
        value: finishedProcess[0].finished,
        key: finishedProcess[0].finished
      });

      table_timeline.data.push({
        id: 'entity_completed_user-b1',
        type: 'entity',
        value: finishedProcess[0].finishedUser,
        key: finishedProcess[0].finishedUserID
      });

      table_timeline.data.push({
        id: 'textbox_step_number-b2',
        type: 'textbox',
        value: '2'
      });

      table_timeline.data.push({
        id: 'listbox_step_name-b2',
        type: 'listbox',
        value: step_number2.listbox_step_name.value,
        key: step_number2.listbox_step_name.key
      });

      //если есть активные работы, заполняем дату начала и ответственного
      if(notFinishedProcess.length) {
        table_timeline.data.push({
          id: 'entity_responsible-b2',
          type: 'entity',
          value: notFinishedProcess[0].responsibleUserName,
          key: notFinishedProcess[0].responsibleUserID
        });

        table_timeline.data.push({
          id: 'date_stage_start_date-b2',
          type: 'entity',
          value: notFinishedProcess[0].started,
          key: notFinishedProcess[0].started
        });
      }
    }
  } else {
    //если в таблице table_timeline уже есть данные
    //то перебираем каждую строку и формируем новую таблицу
    for(let i = 0; i < parseTableTimeline.length; i++) {

      let item = parseTableTimeline[i]; //блок таблицы
      let tableBlockIndex = i + 1;

      //ищем текущий блок в настройках
      let statusStep = settings.filter(function(x){
        if(x.listbox_step_name.key == status.key) return x;
      })[0];

      //это для следующего этапа который будет в работе
      let nextStep = settings.filter(function(x){
        if(x.textbox_step_number.value == String(Number(statusStep.textbox_step_number.value) + 1)) return x;
      });

      //если текущий статус в блоке таблицы совпадает со статусом на форме
      if(item.listbox_step_name.key == status.key) {
        table_timeline.data.push({
          id: 'textbox_step_number-b' + tableBlockIndex,
          type: 'textbox',
          value: item.textbox_step_number.value
        });

        table_timeline.data.push({
          id: 'listbox_step_name-b' + tableBlockIndex,
          type: 'listbox',
          value: item.listbox_step_name.value,
          key: item.listbox_step_name.key
        });

        if(finishedProcess.length) {
          table_timeline.data.push({
            id: 'entity_responsible-b' + tableBlockIndex,
            type: 'entity',
            value: finishedProcess[0].responsibleUserName,
            key: finishedProcess[0].responsibleUserID
          });

          table_timeline.data.push({
            id: 'date_stage_start_date-b' + tableBlockIndex,
            type: 'entity',
            value: finishedProcess[0].started,
            key: finishedProcess[0].started
          });

          table_timeline.data.push({
            id: 'date_stage_end_date-b' + tableBlockIndex,
            type: 'entity',
            value: finishedProcess[0].finished,
            key: finishedProcess[0].finished
          });

          table_timeline.data.push({
            id: 'entity_completed_user-b' + tableBlockIndex,
            type: 'entity',
            value: finishedProcess[0].finishedUser,
            key: finishedProcess[0].finishedUserID
          });
        } else {
          table_timeline.data.push({
            id: 'entity_responsible-b' + tableBlockIndex,
            type: 'entity',
            value: item.entity_responsible.value,
            key: item.entity_responsible.key
          });

          table_timeline.data.push({
            id: 'date_stage_start_date-b' + tableBlockIndex,
            type: 'entity',
            value: item.date_stage_start_date.value,
            key: item.date_stage_start_date.key
          });

          table_timeline.data.push({
            id: 'date_stage_end_date-b' + tableBlockIndex,
            type: 'entity',
            value: item.date_stage_end_date.value,
            key: item.date_stage_end_date.key
          });

          table_timeline.data.push({
            id: 'entity_completed_user-b' + tableBlockIndex,
            type: 'entity',
            value: item.entity_completed_user.value,
            key: item.entity_completed_user.key
          });
        }

        //если есть следующий этап
        if(nextStep && nextStep.length) {
          table_timeline.data.push({
            id: 'textbox_step_number-b' + (tableBlockIndex + 1),
            type: 'textbox',
            value: nextStep[0].textbox_step_number.value
          });

          table_timeline.data.push({
            id: 'listbox_step_name-b' + (tableBlockIndex + 1),
            type: 'listbox',
            value: nextStep[0].listbox_step_name.value,
            key: nextStep[0].listbox_step_name.key
          });
        }

        //заполнили данные и выходим из цикла
        break;
      } else {
        //если текущий статус в блоке таблицы не совпадает со статусом на форме
        //то просто сохраняем то что уже было заполнено
        table_timeline.data.push({
          id: 'textbox_step_number-b' + tableBlockIndex,
          type: 'textbox',
          value: item.textbox_step_number.value
        });

        table_timeline.data.push({
          id: 'listbox_step_name-b' + tableBlockIndex,
          type: 'listbox',
          value: item.listbox_step_name.value,
          key: item.listbox_step_name.key
        });

        table_timeline.data.push({
          id: 'entity_responsible-b' + tableBlockIndex,
          type: 'entity',
          value: item.entity_responsible.value,
          key: item.entity_responsible.key
        });

        table_timeline.data.push({
          id: 'date_stage_start_date-b' + tableBlockIndex,
          type: 'entity',
          value: item.date_stage_start_date.value,
          key: item.date_stage_start_date.key
        });

        table_timeline.data.push({
          id: 'date_stage_end_date-b' + tableBlockIndex,
          type: 'entity',
          value: item.date_stage_end_date.value,
          key: item.date_stage_end_date.key
        });

        table_timeline.data.push({
          id: 'entity_completed_user-b' + tableBlockIndex,
          type: 'entity',
          value: item.entity_completed_user.value,
          key: item.entity_completed_user.key
        });
      }

    }
  }

  API.mergeFormData({
    uuid: dataUUID,
    data: [table_timeline]
  });

} catch (err) {
  message = err.message;
}
