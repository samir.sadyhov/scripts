const searchInRegistry = async params => await AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/data_ext?${params}`);

const getValue = (data, cmpID) => {
  data = data.data ? data.data : data;
  return data.find(x => x.id == cmpID);
}

const parseAsfValue = asfDataValue => {
  if(!asfDataValue) return null;
  const {type, value = '', key = ''} = asfDataValue;
  return key ? {type, value, key} : {type, value};
}

const parseAsfTable = asfTable => {
  const result = [];
  try {
    if(!asfTable.hasOwnProperty('data')) return result;

    const data = asfTable.data.filter(x => x.type != 'label');
    if(!data.length) return result;

    const ids = data.map(x => x.id.slice(0, x.id.indexOf('-b'))).uniq();
    let tbi =  data.slice(-1)[0].id;
    tbi = Number(tbi.slice(tbi.indexOf('-b') + 2));

    for(let i = 1; i <= tbi; i++) {
    	const item = {};
      ids.forEach(key => {
        const parseValue = parseAsfValue(getValue(asfTable, `${key}-b${i}`));
        if(parseValue) item[key] = parseValue;
      });
      result.push(item);
    }
    return result;
  } catch (err) {
    console.log('ERROR parseAsfTable', err);
    return result;
  }
}

const getCoords = elem => {
  const box = $(elem)[0].getBoundingClientRect();
  const formOffset = view.playerView.container.offset();
  return {
    top: box.bottom - formOffset.top + (box.height / 2),
    left: box.left - formOffset.left - box.width
  };
}

class DictStagesWork {
  async parseDictionary(dictionary){
    const {items} = dictionary;
    this.items = [];

    for(let key in items) {
      const {id, step_name} = items[key];
      const {translations} = step_name;
      const data = {id: id.value, name: {}};
      for(let localeKey in translations) {
        data.name[localeKey] = translations[localeKey];
      }
      this.items.push(data);
    }
  }

  async getDictionary(code, columns = true, items = true){
    const url = `rest/api/dictionaries/${code}?getColumns=${columns}&getItems=${items}&locale=${AS.OPTIONS.locale}`;
    return await AS.FORMS.ApiUtils.simpleAsyncGet(url);
  }

  async init(){
    const dictionary = await this.getDictionary('directory_steps', false);
    this.parseDictionary(dictionary);
  }

  getList = () => this.items;

  getItem = id => this.items.find(x => x.id == id);
}

class Timeline {
  constructor(parentContainer) {
    this.parentContainer = parentContainer;
    this.timelineContainer = parentContainer.find('.timeline_container');
    this.timelineTitle = parentContainer.find('.timeline_title');
    this.timelineContent = parentContainer.find('.timeline_content');
    this.init();
  }

  getStep(item){
    const step = $('<div>', {class: 'timeline_step'});
    const circle = $('<div>', {class: 'timeline_step_circle'});
    const line = $('<div>', {class: 'timeline_step_line'});
    step.append(circle, line);

    if(item) {
      const {listbox_step_name, textbox_step_number} = item;
      const data = this.data.find(x => x.textbox_step_number.value == textbox_step_number.value);

      if(data) {
        const {date_stage_end_date, date_stage_start_date, entity_completed_user, entity_responsible} = data;
        if(date_stage_end_date.hasOwnProperty('key')) {
          step.addClass('finished');
        } else {
          step.addClass('active');
        }
      }
    }

    return step;
  }

  createMessage(elem, data) {
    $('.timeline_message').fadeOut(300, function(){
      $(this).remove();
    });

    const coords = getCoords(elem);
    const messageBox = $('<div>', {class: 'timeline_message'});
    const content = $('<div>', {class: 'timeline_message_content'});
    const closeButton = $('<button>', {class: 'timeline_message_button_close'});

    messageBox.css({
      'left': `${coords.left}px`,
      'top': `${coords.top}px`
    });

    data.forEach(item => {
      const {title, value} = item;
      const msg = $('<div>', {class: 'timeline_message_item'});
      msg.append(
        `<span class="timeline_message_item_title">${title}</span>`,
        `<span> - </span>`,
        `<span class="timeline_message_item_value">${value}</span>`
      )
      content.append(msg);
    });

    closeButton.text(i18n.tr('Понятно'));

    closeButton.on('click', e => {
      messageBox.fadeOut(300, function(){
        $(this).remove();
      });
    });

    messageBox.append(content, closeButton);
    view.playerView.container.append(messageBox);
  }

  initStepListener(step, item, title){
    const circle = step.find('.timeline_step_circle');

    if(item) {
      const {listbox_step_name, textbox_step_number} = item;
      const data = this.data.find(x => x.textbox_step_number.value == textbox_step_number.value);

      if(data) {
        circle.attr('title', data.listbox_step_name.value);

        circle.on('click', e => {
          const msgData = [];
          msgData.push({title: 'Наименование этапа', value: data.listbox_step_name.value});
          msgData.push({title: 'Ответственный', value: data.entity_responsible?.value || ''});
          msgData.push({title: 'Дата начала этапа', value: data.date_stage_start_date?.value || ''});
          if(data.date_stage_end_date.value) msgData.push({title: 'Дата окончания этапа', value: data.date_stage_end_date?.value || ''});
          if(data.entity_completed_user.value) msgData.push({title: ' Завершил этап', value: data.entity_completed_user?.value || ''});

          this.createMessage(circle, msgData);
        });

      } else {
        circle.attr('title', listbox_step_name.value);
      }

    } else {
      if(title) circle.attr('title', title);
    }

    return;
  }

  render(){
    try {
      const startStep = this.getStep();
      const finishStep = this.getStep();

      startStep.addClass('first');
      finishStep.addClass('last');

      this.initStepListener(startStep, null, i18n.tr('Начало маршрута'));
      this.initStepListener(finishStep, null, i18n.tr('Конец маршрута'));

      if(this.documentInfo.inProgress == 'true' || this.documentInfo.activate == "true") {
        if(this.data.length) {
          startStep.addClass('finished');
        } else {
          startStep.addClass('active');
        }
      } else {
        const firstSettings = this.settings[0];
        const firstData = this.data[0];
        if(firstData) {
          if(firstData.listbox_step_name.key == firstSettings.listbox_step_name.key
            && firstData.date_stage_start_date.hasOwnProperty('key')
            && firstData.date_stage_start_date.key != '') {
            startStep.addClass('finished');
          }
        }
      }

      const lastSettings = this.settings[this.settings.length - 1];
      const lastData = this.data[this.data.length - 1];

      if(lastData) {
        if(lastData.listbox_step_name.key == lastSettings.listbox_step_name.key
          && lastData.date_stage_end_date.hasOwnProperty('key')
          && lastData.date_stage_end_date.key != '') {
          finishStep.addClass('finished');
        }
      }

      this.timelineContent.append(startStep); // Начало маршрута

      // все остальные этапы
      this.settings.forEach(item => {
        const step = this.getStep(item);
        this.timelineContent.append(step);
        this.initStepListener(step, item);
      });

      this.timelineContent.append(finishStep); // Конец маршрута

    } catch (err) {
      console.log(`ERROR Timeline render`, err);
    }
  }

  getTimelineData(){
    const table = model.playerModel.getModelWithId('table_timeline');
    return parseAsfTable(table.getAsfData()[0]);
  }

  parseSettings(asfData){
    const table = getValue(asfData, "table_steps");
    return parseAsfTable(table);
  }

  async getSettings(){
    const searchParam = $.param({
      registryCode: 'registry_step_guide',
      field: 'textbox_registry_code',
      condition: 'TEXT_EQUALS',
      value: this.registryCode,
      loadData: false,
      countInPart: 1
    });
    const searchResult = await searchInRegistry(searchParam);

    if(searchResult.count == 0) return null;

    const {dataUUID} = searchResult.data[0];
    const asfData = await AS.FORMS.ApiUtils.loadAsfData(dataUUID);
    const settings = this.parseSettings(asfData);
    settings.sort((a,b) => a.textbox_step_number.value - b.textbox_step_number.value);

    return settings;
  }

  async getCurrentRegistryCode(){
    const {ApiUtils} = AS.FORMS;
    const {asfDataId} = model.playerModel;
    const documentID = await ApiUtils.getDocumentIdentifier(asfDataId);
    this.documentInfo = await ApiUtils.simpleAsyncGet(`rest/api/docflow/doc/document_info?documentID=${documentID}&locale=${AS.OPTIONS.locale}`);
    const {registryID} = this.documentInfo;

    if(registryID) {
      const {code = null} = await ApiUtils.simpleAsyncGet(`rest/api/registry/info?registryID=${registryID}&locale=${AS.OPTIONS.locale}`);
      return code;
    } else {
      return null;
    }
  }

  async init(){
    AS.SERVICES.showWaitWindow();
    $('.timeline_message').remove();

    try {
      const {name = {}} = model;

      this.timelineTitle.text(name[AS.OPTIONS.locale] || 'Этапы проведения работ');

      this.dictionary = new DictStagesWork();
      await this.dictionary.init();

      this.registryCode = await this.getCurrentRegistryCode();

      if(!this.registryCode) throw new Error('Ошибка получения кода реестра');

      this.settings = await this.getSettings();

      if(!this.settings) throw new Error('Ошибка получения настроек Timeline');

      this.data = this.getTimelineData();

      this.render();

      AS.SERVICES.hideWaitWindow();
    } catch (err) {
      AS.SERVICES.hideWaitWindow();
      AS.SERVICES.showErrorMessage(err.message);
      this.parentContainer.fadeOut();
      console.log(`ERROR Timeline init`, err);
    }
  }
}

//выпиливыние из массива повторяющихся елементов
Array.prototype.uniq = function() {
  return this.filter(function(v, i, a){ return i == a.indexOf(v) });
}

new Timeline(view.container);
