#!/bin/bash

source $(dirname $(readlink -e $0))/stopprocesses.conf

# вспомогательные настройки
scriptName=${0##*/}
tmpsql=$(mktemp)
tmpfile=$(mktemp)

function now_time () {
  date +"%Y-%m-%d %H:%M:%S"
}

function logging () {
  echo -e "`now_time` #$scriptName# [$1] $2" >> $logFile
}

# $1 mysql query
# $2 output result to tmp file
function executeQuery () {
  mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8; $1;" > $2 2>/dev/null
}

# прервать процесс
# $1 documentID
function stopRoute () {
  curl --user $synergyUser:$synergyPass -XPOST --url "$synergyURL/rest/api/docflow/doc/stop_route?documentID=$1&procInstID=$2"  --output $tmpfile --silent 2>/dev/null
}

echo '' >> $logFile
logging START "Начало работы скрипта"

logging INFO "periodStart: $periodStart :::: periodStop: $periodStop"
logging INFO "limitRow: $limitRow"

executeQuery "SELECT a.actionID, of.folderID, p.procInstID
FROM actions a
LEFT JOIN processes p ON p.objectID = a.actionID
LEFT JOIN object_folders of ON of.objectID = p.topProcInstId
WHERE a.finished IS NULL
AND a.deleted IS NULL
AND DATE(a.start_date) BETWEEN DATE('$periodStart') AND DATE('$periodStop')
ORDER BY a.start_date
LIMIT $limitRow" $tmpsql

if [ -s $tmpsql ];
then
  countStr=`cat $tmpsql | wc -l`
  for i in `seq 2 $countStr`;
  do
    actionID=`cat $tmpsql | sed -n $i'p' | cut -f1`
    documentID=`cat $tmpsql | sed -n $i'p' | cut -f2`
    procInstID=`cat $tmpsql | sed -n $i'p' | cut -f3`

    echo '' >> $logFile
    logging INFO "actionID: $actionID || documentID: $documentID || procInstID: $procInstID"

    stopRoute $documentID $procInstID
    logging RESULT "результат прерывания маршрута\n`cat $tmpfile`"
  done
else
  logging INFO "нет найдено работ за выбранный период"
fi

echo '' >> $logFile
logging STATUS "Удаление временных файлов"
rm -rf $tmpsql
rm -rf $tmpfile
logging END "Завершение работы скрипта"
exit 0
