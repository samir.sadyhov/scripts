const getReleases = async () => {
  return new Promise(resolve => {
    const {releases} = Cons.getAppStore();
    if(releases) {
      resolve(releases);
    } else {
      try {
        let url = `api/registry/data_ext?registryCode=releases_registry&filterCode=announcements`;
        rest.synergyGet(url, res => {
          Cons.setAppStore({releases: res});
          resolve(res);
        }, err => {
          resolve(null);
          console.log('ERROR getReleases', err);
        });
      } catch (e) {
        resolve(null);
        console.log('ERROR getReleases', e.message);
      }
    }
  });
}

const getReleaseBlock = (name, ver) => {
  const container = $('<div>', {class: 'release_block'});
  const version = $('<div>', {class: 'release_version'});
  const title = $('<div>', {class: 'release_title'});

  version.text(ver);
  title.text(name);

  container.append(version, title);

  return container;
}

const addBlockListener = (block, dataUUID) =>  {
  block.on('click', e => {
    e.preventDefault();
    e.target.blur();
    Cons.setAppStore({releaseID: dataUUID});
    fire({type: 'goto_page', pageCode: 'view_page'}, 'root-panel');
  });
}

const renderReleases = async data => {
  if(!data || !data.length) return;

  const container = $('#panelContent');

  container.empty();

  for(let i = 0; i < data.length; i++) {
    const {dataUUID, documentID, fieldKey, fieldValue} = data[i];
    const block = getReleaseBlock(fieldValue?.releases_form_name, fieldValue?.releases_form_version);
    addBlockListener(block, dataUUID);
    container.append(block);
  }
}

const initReleases = async () => {
  Cons.showLoader();
  try {
    const releases = await getReleases();
    if(!releases) throw new Error('Произошла ошибка получения записей реестра релизов');
    renderReleases(releases.result);
    Cons.hideLoader();
  } catch (e) {
    Cons.hideLoader();
    showMessage(e.message, 'error');
  }
}

pageHandler('announcement_page', () => {
  initReleases();
});
