const getArchive = async () => {
  return new Promise(resolve => {
    const {archive} = Cons.getAppStore();
    if(archive) {
      resolve(archive);
    } else {
      try {
        let url = `api/registry/data_ext?registryCode=releases_registry&filterCode=archive&sortCmpID=releases_form_date`;
        rest.synergyGet(url, res => {
          Cons.setAppStore({archive: res});
          resolve(res);
        }, err => {
          resolve(null);
          console.log('ERROR getArchive', err);
        });
      } catch (e) {
        resolve(null);
        console.log('ERROR getArchive', e.message);
      }
    }
  });
}

const getOpenButton = () => {
  const button = $('<span>', {class: 'archive_open_button'});
  button.append(`<svg xmlns="http://www.w3.org/2000/svg" height="32px" viewBox="0 0 24 24" width="32px" fill="#666"><path d="M0 0h24v24H0z" fill="none"/><path d="M19 19H5V5h7V3H5c-1.11 0-2 .9-2 2v14c0 1.1.89 2 2 2h14c1.1 0 2-.9 2-2v-7h-2v7zM14 3v2h3.59l-9.83 9.83 1.41 1.41L19 6.41V10h2V3h-7z"/></svg>`);
  return button;
}

const getArchiveTable = data => {
  const table = $('<table>', {class: 'uk-table uk-table-divider uk-table-justify archive_table'});
  const tbody = $('<tbody>');
  table.append(tbody);

  for(let i = 0; i < data.length; i++) {
    const {dataUUID, releases_form_title} = data[i];
    const tr = $('<tr>');
    const button = getOpenButton();
    const tdOpen = $('<td style="width: 40px;">');

    button.on('click', e => {
      e.preventDefault();
      e.target.blur();
      Cons.setAppStore({releaseID: dataUUID});
      fire({type: 'goto_page', pageCode: 'view_page'}, 'root-panel');
    });

    tdOpen.append(button);

    tr.append(
      `<td style="padding-left: 30px;">${releases_form_title}</td>`,
      tdOpen
    );

    tbody.append(tr);
  }

  return table;
}

const renderArchive = archive => {
  const container = $('#panelContent');
  container.empty();

  for(let key in archive) {
    const data = archive[key];
    if(!data || !data.length) continue;

    const block = $('<div>', {class: 'archive_block'});
    const title = $('<div>', {class: 'archive_title'});
    const table = getArchiveTable(data);

    title.text(`Анонсы релизов на ${key}`);

    block.append(title, table);

    container.append(block);
  }
}

const parseArchiveData = data => {
  const archive = {};
  const dates = data.result.map(x => x.fieldValue.releases_form_date).filter((v, i, a) => i == a.indexOf(v));

  for(let i = 0; i < dates.length; i++) {
    archive[dates[i]] = [];
  }

  for(let i = 0; i < data.result.length; i++) {
    const {dataUUID, documentID, fieldKey, fieldValue} = data.result[i];
    archive[fieldValue.releases_form_date].push({dataUUID, ...fieldValue})
  }

  return archive;
}

const initArchive = async () => {
  Cons.showLoader();
  try {
    const archiveData = await getArchive();
    if(!archiveData) throw new Error('Произошла ошибка получения записей реестра релизов');
    const archive = parseArchiveData(archiveData);
    renderArchive(archive);
    Cons.hideLoader();
  } catch (e) {
    Cons.hideLoader();
    showMessage(e.message, 'error');
  }
}

pageHandler('archive_page', () => {
  initArchive();
});
