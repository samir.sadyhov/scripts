pageHandler('view_page', () => {
  const {releaseID} = Cons.getAppStore();

  if(releaseID) {
    fire({type: 'show_form_data', dataId: releaseID}, 'formPlayerRelease');
  }
});
