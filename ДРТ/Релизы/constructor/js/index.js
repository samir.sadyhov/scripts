AS.SERVICES.showWaitWindow = Cons.showLoader;
AS.SERVICES.hideWaitWindow = Cons.hideLoader;
AS.SERVICES.showErrorMessage = str => showMessage(str, 'error');

//Иконка приложения
if(!$('link[rel="icon"]').length) $('head').append('<link rel="icon" href="../constructorFiles/releases_v2/logo.svg">');
