const getUrlParameter = sParam => {
  let sPageURL = window.location.search.substring(1), sURLVariables = sPageURL.split('&'), sParameterName;
  for (let i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split('=');
    if (sParameterName[0].toLowerCase() === sParam.toLowerCase())
    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
  }
}

const pageHandlerFunction = async () => {
  try {
    const documentID = getUrlParameter('documentID');
    const type = getUrlParameter('type') || null;

    if(!documentID) throw new Error('Не верный URL');

    Cons.setAppStore({documentID, type});

    fire({type: 'goto_page', pageCode: 'docInfoPage'}, 'root-panel');
  } catch (err) {
    Cons.setAppStore({dataUUID: null, type: null});
    showMessage(i18n.tr(err.message), 'error');
  }
}

pageHandler('startPage', () => {
  pageHandlerFunction();
});
