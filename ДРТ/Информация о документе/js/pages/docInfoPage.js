const renderDocInfo = (docInfo, rcc) => {
  const {number, regDate, name} = docInfo;
  const container = $('#panelDocInfo #docInfo');

  const t = {
    label: {
      ru: 'Данные о документе',
      kk: 'Құжат мәліметтері',
      en: 'Document Information'
    },
    number: {
      ru: '№ регистрации',
      kk: 'Тіркеу нөмірі',
      en: 'Registration No.'
    },
    date: {
      ru: 'Дата регистрации',
      kk: 'Тіркеу күні',
      en: 'Registration date'
    },
    name: {
      ru: 'Краткое содержание',
      kk: 'Қысқаша мазмұны',
      en: 'Summary'
    }
  };

  const label = $('<span>', {style: 'font-weight: bold;'});
  label.text(t.label[AS.OPTIONS.locale]);

  const parseRegDate = AS.FORMS.DateUtils.formatDate(AS.FORMS.DateUtils.parseDate(regDate), '${dd}.${mm}.${yyyy}');

  container.empty()
  .append(
    label,
    `<div>${t.name[AS.OPTIONS.locale]}: ${name}</div>`,
    `<div>${t.number[AS.OPTIONS.locale]}: ${number}</div>`,
    `<div>${t.date[AS.OPTIONS.locale]}: ${parseRegDate}</div>`
  );
}

const getTr = (label, value) => {
  const tr = $('<tr>');
  tr.append(
    `<td>${label}</td>`,
    `<td>${value}</td>`
  );
  return tr;
}

const getSignIcon = () => {
  return `<svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="48px" viewBox="0 0 24 24" width="48px" fill="#ef2d35"><g><rect fill="none" height="24" width="24"/></g><g><path d="M23,12l-2.44-2.79l0.34-3.69l-3.61-0.82L15.4,1.5L12,2.96L8.6,1.5L6.71,4.69L3.1,5.5L3.44,9.2L1,12l2.44,2.79l-0.34,3.7 l3.61,0.82L8.6,22.5l3.4-1.47l3.4,1.46l1.89-3.19l3.61-0.82l-0.34-3.69L23,12z M10.09,16.72l-3.8-3.81l1.48-1.48l2.32,2.33 l5.85-5.87l1.48,1.48L10.09,16.72z"/></g></svg>`;
}

const signLabels = {
  date: {
    ru: 'Дата подписания',
    kk: 'Қол қойылған күні',
    en: 'Signing date'
  },
  nameOgrInfo: {
    ru: 'Наименование',
    kk: 'Аты',
    en: 'Name'
  },
  bin: {
    ru: 'БИН',
    kk: 'БИН',
    en: 'BIN'
  },
  userName: {
    ru: 'ФИО подписанта',
    kk: 'Қол қоюшының толық аты-жөні',
    en: 'Full name of the signatory'
  },
  fff: {
    ru: 'ФИО на ЭЦП',
    kk: 'ЭСҚ-да толық аты-жөні',
    en: 'Full name on the EDS'
  },
  iin: {
    ru: 'ИИН',
    kk: 'ЖСН',
    en: 'IIN'
  },
  keyReleaseDate: {
    ru: 'Дата выдачи ЭЦП',
    kk: 'ЭСҚ берілген күні',
    en: 'EDS issue date'
  },
  keyExpiryDate: {
    ru: 'Дата окончания',
    kk: 'Мерзімнің өту күні',
    en: 'End date'
  },
  email: {
    ru: 'E-mail',
    kk: 'E-mail',
    en: 'E-mail'
  },
  actionResult: {
    ru: 'Действие',
    kk: 'Әрекет',
    en: 'Action'
  }

};

const renderSignList = signList => {
  const container = $('#panelDocInfo #signInfo');
  const t = {
    label: {
      ru: 'Данные о подписях',
      kk: 'Қолтаңба мәліметтері',
      en: 'Signature data'
    }
  };

  const label = $('<span>', {style: 'font-weight: bold;'});
  label.text(t.label[AS.OPTIONS.locale]);

  const table = $('<table>', {class: 'table_sign_info'});
  const rowspan = Object.keys(signLabels).length + 1;

  signList.forEach(item => {
    table.append(`<tr><th rowspan="${rowspan}">${getSignIcon()}</th></tr>`);
    for(const key in signLabels) {
      table.append(getTr(signLabels[key][AS.OPTIONS.locale], item[key] || ''))
    }
    table.append(`<tr><th colspan="3" class="divider"></th></tr>`);
  });

  container.empty().append(label, table);
}

const updateURL = type => {
  const {documentID} = Cons.getAppStore();
  const newURL = `${window.location.origin}${window.location.pathname}?documentid=${documentID}&type=${type}`;
  history.replaceState(null, null, newURL);
}

const changeTypeView = type => {
  if(!type) return;

  Cons.setAppStore({type});
  updateURL(type);

  switch (type) {
    case '1':
      $('#buttonInfoDoc').removeClass('active');
      $('#buttonViewDoc').addClass('active');

      $("#panelDocInfo").fadeOut(200, function() {
        $("#formPlayerDoc").fadeIn(100);
      });
      break;
    case '2':
      $('#buttonViewDoc').removeClass('active');
      $('#buttonInfoDoc').addClass('active');

      $("#formPlayerDoc").fadeOut(200, function() {
        $("#panelDocInfo").fadeIn(100);
      });
      break;
  }
}

const docInfoPageHandler = async () => {
  Cons.showLoader();
  try {
    const {documentID, type} = Cons.getAppStore();

    changeTypeView(type);

    const docInfo = await appAPI.getDocumentInfo(documentID);
    const rcc = await appAPI.getDocumentRCC(documentID);
    const signList = await appAPI.getSignList(documentID);

    Cons.hideLoader();

    if(docInfo.asfDataID) {
      fire({type: 'show_form_data', dataId: docInfo.asfDataID}, 'formPlayerDoc');
      if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
        fire({type: 'show_form_view', viewCode: 'mobile'}, 'formPlayerDoc');
      }
    }

    renderDocInfo(docInfo, rcc);
    renderSignList(signList);

  } catch (err) {
    Cons.hideLoader();
    showMessage(i18n.tr(err.message), 'error');
  }
}

const initLocaleSelector = () => {
  $("#localeSelector").off().on('change', function() {
    AS.OPTIONS.locale = this.value;
    fire({type: 'change_locale', locale: AS.OPTIONS.locale});
    docInfoPageHandler();
  });
}

pageHandler('docInfoPage', () => {
  initLocaleSelector();
  docInfoPageHandler();

  $('#buttonPrintForm').off().on('click', e => {
    const player = Cons.getCompByCode('formPlayerDoc');
    if(player.model.hasPrintable) {
      window.open(`../Synergy/rest/asforms/template/print/form?format=pdf&dataUUID=${player.dataId}`);
    } else {
      UTILS.printForm(player.view.container[0]);
    }
  }).on('focus', e => {
    e.target.blur();
  });

  $('#buttonViewDoc').off().on('click', e => {
    changeTypeView('1');
  }).on('focus', e => {
    e.target.blur();
  });

  $('#buttonInfoDoc').off().on('click', e => {
    changeTypeView('2');
  }).on('focus', e => {
    e.target.blur();
  });

});
