const createUserParam = {
  //права юзера в системе
  userParams: {
    hasAccess: true, //Доступ в систему
    accessMobile: true, //Доступ в мобильное приложение
    isAdmin: false, //Администратор
    isChancellery: false, //Сотрудник канцелярии
    isConfigurator: false //Разработчик Synergy
  },

  add: {
    groupCodes: ['vse'], //массив групп куда будет добавлен юзер (параметр не обязательный)
    positionCode: 'spetsialist' //код должности на которую будет назначен юзер (параметр не обязательный)
  }
};

const capitalizeFirstLetter = str => {
  if (typeof str !== 'string' || str.length === 0) return '';
  return str.charAt(0).toUpperCase() + str.slice(1).toLowerCase();
}

const getAuth = () => {
  const {defaultUser, defaultUserPassword} = Cons.getCurrentApp();
  return "Basic " + btoa(unescape(encodeURIComponent(`${defaultUser}:${defaultUserPassword}`)));
}

const createUser = async data => {
  return new Promise(async resolve => {
    try {
      const url = `../Synergy/rest/api/filecabinet/user/save`;
      const response = await fetch(url, {
        method: 'POST',
        headers: {"Authorization": getAuth(), "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"},
        body: data
      });
      resolve(response.json());
    } catch (err) {
      console.log(`ERROR [ createUser ]: ${err.message}`);
      resolve(null);
    }
  });
}

const sendNotification = async body => {
  return new Promise(async resolve => {
    try {
      const url = `../Synergy/rest/api/notifications/send`;
      const response = await fetch(url, {
        method: 'POST',
        headers: {"Authorization": getAuth(), "Content-Type": "application/json; charset=UTF-8"},
        body: JSON.stringify(body)
      });

      if(!response.ok) {
        let rt = await response.text();
        rt = JSON.parse(rt);
        throw new Error(`http: ${response.status}, message: ${response.statusText}, errorMessage: ${rt.errorMessage}`);
      }
      resolve(response.json());
    } catch (err) {
      console.log(`ERROR [ sendNotification ]: ${err.message}`);
      resolve(null);
    }
  });
}

const searchPosition = async pointer_code => {
  return new Promise(async resolve => {
    try {
      const url = `../Synergy/rest/api/positions/search?pointer_code=${pointer_code}&searchTypePC=exact&deleted=false`;
      const response = await fetch(url, {method: 'GET', headers: {"Authorization": getAuth()}});

      if(!response.ok) throw new Error(await response.text());
      resolve(response.json());
    } catch (err) {
      console.log(`ERROR [ searchPosition ]: ${err.message}`);
      resolve(null);
    }
  });
}

const positionsAppoint = async (userID, positionID) => {
  return new Promise(async resolve => {
    try {
      const url = `../Synergy/rest/api/positions/appoint?positionID=${positionID}&userID=${userID}`;
      const response = await fetch(url, {method: 'GET', headers: {"Authorization": getAuth()}});

      if(!response.ok) throw new Error(await response.text());
      resolve(response.json());
    } catch (err) {
      console.log(`ERROR [ positionsAppoint ]: ${err.message}`);
      resolve(null);
    }
  });
}

const groupsAddUser = async (userID, groupCode) => {
  return new Promise(async resolve => {
    try {
      const url = `../Synergy/rest/api/storage/groups/add_user?groupCode=${groupCode}&userID=${userID}`;
      const response = await fetch(url, {method: 'GET', headers: {"Authorization": getAuth()}});

      if(!response.ok) throw new Error(await response.text());
      resolve(response.text());
    } catch (err) {
      console.log(`ERROR [ groupsAddUser ]: ${err.message}`);
      resolve(null);
    }
  });
}

const checkInputs = () => {
  const emailRegex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  let newPassword;
  let inputEmail;

  try {
    if($('#textInputEmail').val() == '') {
      $('#textInputEmail').addClass('highlight');
      throw new Error('Не заполнен E-mail');
    } else {
      $('#textInputEmail').removeClass('highlight');
    }

    if(!emailRegex.test($('#textInputEmail').val())) {
      $('#textInputEmail').addClass('highlight');
      throw new Error('Введен некорректный email адрес');
    } else {
      $('#textInputEmail').removeClass('highlight');
    }

    if($('#textInputPassword').val() == '') {
      $('#textInputPassword').addClass('highlight');
      throw new Error('Не заполнен пароль');
    } else {
      $('#textInputPassword').removeClass('highlight');
    }

    if($('#textInputConfirmPassword').val() == '') {
      $('#textInputConfirmPassword').addClass('highlight');
      throw new Error('Не заполнено поле подтверждения пароля');
    } else {
      $('#textInputConfirmPassword').removeClass('highlight');
    }

    if($('#textInputPassword').val() != $('#textInputConfirmPassword').val()) {
      throw new Error('Пароли не совпадают');
    }

    newPassword = $('#textInputPassword').val();
    inputEmail = $('#textInputEmail').val();

    return {newPassword, inputEmail};
  } catch (err) {
    console.error(err);
    UIkit.notification.closeAll();
    showMessage(i18n.tr(err.message), "error");
    return null;
  }
}

pageHandler('registration_page', () => {
  if (!Cons.getAppStore().registration_page_listener) {

    addListener('value_changed_input', 'textInputPassword', e => {
      $('#textInputPassword').removeClass('highlight');
    });

    addListener('value_changed_input', 'textInputConfirmPassword', e => {
      $('#textInputConfirmPassword').removeClass('highlight');
    });

    addListener('button_click', 'buttonSelectEDS', e => {
      const inputData = checkInputs();

      if(inputData) {
        const {newPassword, inputEmail} = inputData;

        NCALayer.registerEDS(async result => {
          Cons.showLoader();
          try {
            if(result.hasOwnProperty('errorCode') && result.errorCode != 0) throw new Error(result.errorMessage);

            let {email, iin, name, patronymic, surname} = result;
            name = capitalizeFirstLetter(name);
            surname = capitalizeFirstLetter(surname);
            patronymic = capitalizeFirstLetter(patronymic);

            const body = new URLSearchParams();
            body.append("lastname", name);
            body.append("firstname", surname);
            body.append("patronymic", patronymic);
            body.append("iin", iin);
            body.append("email", email || inputEmail);
            body.append("pointersCode", `IIN${iin}`);
            body.append("login", iin);
            body.append("password", newPassword);

            for(const key in createUserParam.userParams) {
              body.append(key, createUserParam.userParams[key]);
            }

            const createUserResult = await createUser(body);
            if(!createUserResult) throw new Error('Ошибка регистрации');
            if(createUserResult.hasOwnProperty('errorCode') && createUserResult.errorCode != 0) {
              throw new Error(createUserResult.errorMessage);
            }

            const {userID} = createUserResult;

            for(const key in createUserParam.add) {
              switch (key) {
                case 'groupCodes': {
                  for(let i = 0; i < createUserParam.add[key].length; i++) {
                    const groupCode = createUserParam.add[key][i];
                    await groupsAddUser(userID, groupCode);
                  }
                  break;
                }
                case 'positionCode': {
                  const searchResult = await searchPosition(createUserParam.add[key]);
                  if(searchResult && searchResult.length) await positionsAppoint(userID, searchResult[0]);
                  break;
                }
              }
            }

            await sendNotification({
              header: `Вы зарегистрировались на портале`,
              message: `Поздравляем. Вы зарегистрировались на портале. <br><br><b>Данные для входа:</b><br>логин: ${iin}<br>пароль: ${$('#textInputPassword').val()}`,
              emails: [email || inputEmail]
            });

            Cons.hideLoader();
            showMessage(i18n.tr('Регистрация успешно завершена! Вы можете войти в систему с помощью ЭЦП или использовать логин и пароль. В качестве логина укажите ваш ИИН, а пароль — тот, который вы задали при регистрации.'), 'success');
            setTimeout(() => {
              fire({type: 'goto_page', pageCode: 'auth_page'}, 'root-panel');
            }, 2000);
          } catch (err) {
            Cons.hideLoader();
            console.error(err);
            UIkit.notification.closeAll();
            showMessage(i18n.tr(err.message), "error");
          }
        });
      }

    });

    Cons.setAppStore({registration_page_listener: true});
  }
});
