const translations = [];

translations.push({
  ru: 'Не заполнен пароль',
  kk: 'Құпия сөз толтырылмаған',
  en: 'Password not filled in'
});

translations.push({
  ru: 'Не заполнено поле подтверждения пароля',
  kk: 'Құпия сөзді растау өрісі толтырылмаған',
  en: 'The password confirmation field is not filled in'
});

translations.push({
  ru: 'Пароли не совпадают',
  kk: 'Құпия сөздер сәйкес келмейді',
  en: 'The passwords do not match'
});

translations.push({
  ru: 'Ошибка регистрации',
  kk: 'Тіркеу қатесі',
  en: 'Registration error'
});

translations.push({
  ru: 'Регистрация прошла успешно',
  kk: 'Тіркеу сәтті өтті',
  en: 'Registration was successful'
});

translations.push({
  ru: 'Ошибка авторизации. Проверьте правильность введенных данных.',
  kk: 'Авторизация қатесі. Енгізілген деректердің дұрыстығын тексеріңіз.',
  en: 'Authorization error. Check the correctness of the entered data.'
});

translations.push({
  ru: 'Не заполнен логин',
  kk: 'Логин толтырылмаған',
  en: 'Login not filled in'
});

translations.push({
  ru: 'Ошибка авторизации. Выбран неверный ключ.',
  kk: 'Авторизация қатесі. Жарамсыз кілт таңдалды.',
  en: 'Authorization error. Incorrect key selected.'
});

translations.push({
  ru: 'Регистрация успешно завершена! Вы можете войти в систему с помощью ЭЦП или использовать логин и пароль. В качестве логина укажите ваш ИИН, а пароль — тот, который вы задали при регистрации.',
  kk: 'Тіркеу сәтті аяқталды! Жүйеге кіру үшін ЭЦҚ немесе логин мен құпиясөзді пайдалана аласыз. Логин ретінде ЖСН-іңізді енгізіңіз, ал құпиясөз – тіркелу кезінде көрсеткеніңіз.',
  en: 'Registration completed successfully! You can log in using an EDS or your login and password. Your login is your IIN, and your password is the one you\'ve set during registration.'
});

translations.push({
  ru: 'Не заполнен E-mail',
  kk: 'Электрондық пошта толтырылмаған',
  en: 'Email is not filled in'
});

translations.push({
  ru: 'Введен некорректный email адрес',
  kk: 'Қате электрондық пошта мекенжайы енгізілді',
  en: 'Incorrect email address entered'
});

const updateTranslations = async () => {
  i18n.locale = AS.OPTIONS.locale;

  const systemMessages = await AS.FORMS.ApiUtils.simpleAsyncGet(`rest/system/messages?localeID=${AS.OPTIONS.locale}`);
  i18n.messages = systemMessages;

  translations.forEach(t => i18n.messages[t.ru] = t[AS.OPTIONS.locale]);
}

if($('#localeSelector-1').length) {
  $('#localeSelector-1').off().on('change', e => {
    const locale = $('#localeSelector-1').val();
    AS.OPTIONS.locale = locale;
    localStorage.locale = locale;
    updateTranslations();
  });
}

if(localStorage.locale && localStorage.locale != AS.OPTIONS.locale) {
  let localeSelect = document.querySelector("#localeSelector-1");
  if(localeSelect) {
    localeSelect.value = localStorage.locale;
    localeSelect.dispatchEvent(new Event("change", {bubbles: true}));
  }
}
