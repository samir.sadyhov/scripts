let UTILS = {
  createField: function(fieldData) {
    let field = {};
    for (let key in fieldData) field[key] = fieldData[key];
    return field;
  },
  getValue: function(data, cmpID) {
    data = data.data ? data.data : data;
    for(let i = 0; i < data.length; i++)
    if (data[i].id === cmpID) return data[i];
    return null;
  },
  setValue: function(asfData, cmpID, data) {
    let field = this.getValue(asfData, cmpID);
    if(field) {
      for (let key in data) {
        if(key === 'id' || key === 'type') continue;
        field[key] = data[key];
      }
      return field;
    } else {
      asfData = asfData.data ? asfData.data : asfData;
      field = this.createField(data);
      field.id = cmpID;
      asfData.push(field);
      return field;
    }
  }
};

function createTable(data) {
	let container = $('<div>', {class: 'table-container'});
  let table = $('<table>', {class: 'search-result'});
  let thead = $('<thead>');
  let body = $('<tbody>');
  let trHead = $('<tr>');

  for (let key in data[0]) {
    trHead.append($('<th>').text(key))
  }

  data.forEach(row => {
    let tr = $('<tr>');
    for (let key in row) {
      tr.append($('<td>').text(row[key]))
    }
    body.append(tr);
  });

  thead.append(trHead);
  table.append(thead).append(body);
  container.append(table);
  return container;
}

function showDialog(table, data) {
  let modal = $('<div>').append(table);
  $("body").append(modal);
  modal.dialog({
    modal: true,
    width: 800,
    height: 400,
    title: "Список найденых записей - " + data.length,
    buttons: [{
        text: "Закрыть",
        click: function() {
          $(this).dialog("close");
        }
      },
      {
        text: "Заменить значения",
        click: function() {
          if(confirm("Для замены данных может потребоваться много времени, продолжить?")) {
            replaceData(table, data);
          }
        }
      }
    ],
    close: function() {
      $(this).remove();
    }
  });
}

function replaceData(table, data) {
  data.forEach(item => {
    AS.SERVICES.showWaitWindow();
    try {
      let serviceLink;
      AS.FORMS.ApiUtils.loadAsfData(item.dataUUID)
      .then(asfData => {
        let serviceLink = UTILS.getValue(asfData, 'itsm_form_incident_servicelink');
        AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/formPlayer/getDocMeaningContents?documentId=${serviceLink.key}`)
        .then(info => {
          delete serviceLink.keys;
          serviceLink.valueID = serviceLink.key;
          serviceLink.value = info[0].meaning;

          let data = {
            data: '"data" : ' + JSON.stringify(asfData.data),
            form: asfData.form,
            uuid: asfData.uuid
          };

          AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/asforms/form/multipartdata", res => {
            table.find(`td:contains("${item.documentID}")`).parent().css("background-color", "#c8ffc8");
            console.log('Данные сохранены', res);
            AS.SERVICES.hideWaitWindow();
          }, "text", data, "application/x-www-form-urlencoded; charset=UTF-8", err => {
            let errMsg = JSON.parse(err.responseText).errorMessage;
            table.find(`td:contains("${item.documentID}")`).parent()
            .attr('title', errMsg)
            .css("background-color", "#ff9e9f");
            console.log('ошибка сохраненения данных', err);
            AS.SERVICES.hideWaitWindow();
          });

        });
      });
    } catch (e) {
      console.log('error', e, item);
      AS.SERVICES.hideWaitWindow();
    }
  });
}


let button = $('<button>', {class: 'custom-button'})
.text('починить registryChooser')
.on('click', () => {
  try {
    AS.SERVICES.showWaitWindow();
    let msg = $('<div id="custom-msg" style="font-size: 34px;font-weight: bold;color: red;background-color: rgba(0, 0, 0, 0.9);position: fixed;top: 10px;width: 100%;height: 10%;display: grid;align-content: center;">Подождите, идет поиск данных...</div>');
    $('#mngmnt_wait_div table td').append(msg);

    let body = {
    	"query": "where code = 'itsm_form_incident' AND itsm_form_incident_servicelink.DATE IS NOT NULL AND itsm_form_incident_servicelink.DATE != '' AND itsm_form_incident_servicelink.TEXT = ''",
    	"parameters": [ "itsm_form_incident_servicelink.DATE", "itsm_form_incident_servicelink.TEXT" ],
    	"showDeleted": false,
    	"searchInRegistry": true
      // ,"recordsCount": 2
    }

    AS.FORMS.ApiUtils.simpleAsyncPost('rest/api/asforms/search/advanced', null, null, JSON.stringify(body), "application/json; charset=utf-8")
    .then(res => {
      AS.SERVICES.hideWaitWindow();
      msg.remove();
      if(res.length > 0) {
        let table = createTable(res);
        showDialog(table, res);
      } else {
        alert("записей для изменения не найдено");
      }
    });

  } catch (e) {
    AS.SERVICES.hideWaitWindow();
    alert('Произошла какая-то неведомая ошибка');
    console.log('::: ERROR :::', e);
  }
});

view.container.append(button);
