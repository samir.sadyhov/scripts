/*
model.registryCode = 'experience_registry_services'; - код реестра
model.fieldTitle = 'experience_form_service_name'; - какое поле отображать в списке
model.asfProperty.required = true; - признаяк обязательности заполнения

model.customFilter = {
   field: 'experience_form_serviceStep_service',
   condition: 'CONTAINS',
   key: true,
   searchCmpId: 'custom-y50qvr'
}

model.matching = []; //Сопоставление из выбранной записи в текущую
model.matching.push({from: 'experience_form_serviceStep_number', to: 'cjm_servise_stepNum'})

model.defaultValue = 'documentID'; - дефолтное значение
*/

let select = $('<select class="asf-textBox uk-select">').css({"background-color": "#fff"});
let label = $(view.container).children(".asf-label");

view.container.append(select);

model.getAsfData = function(blockNumber){
  if(model.getValue()) {
    var result = AS.FORMS.ASFDataUtils.getBaseAsfData(model.asfProperty, blockNumber, model.getValue().value , model.getValue().key);
    result.valueID = model.getValue().key;
    return result;
  } else {
    return AS.FORMS.ASFDataUtils.getBaseAsfData(model.asfProperty, blockNumber);
  }
};

model.setAsfData = function(asfData){
  if(!asfData || !asfData.value) {
    return;
  }
  var value = {key: asfData.key, value: asfData.value};
  model.setValue(value);
};

model.getSpecialErrors = function() {
  if(model.asfProperty.required && !model.getValue()) {
    return {id : model.asfProperty.id, errorCode : AS.FORMS.INPUT_ERROR_TYPE.emptyValue};
  }
};

let valueUpdate = null;
view.updateValueFromModel = function () {
  if (model.getValue()) {
    valueUpdate = model.getValue();
    view.container.removeClass('asf-invalidInput');
    view.unmarkInvalid();
    label.text(valueUpdate.value);
    select.val(valueUpdate.key);
  } else if (valueUpdate) {
    model.setAsfData({key: valueUpdate.key, value: valueUpdate.value});
    view.container.removeClass('asf-invalidInput');
    view.unmarkInvalid();
    label.text(valueUpdate.value);
    select.val(valueUpdate.key);
  } else {
    label.text("");
    select.val("0");
  }
};

model.on(AS.FORMS.EVENT_TYPE.valueChange, function () {
  view.updateValueFromModel();
});

model.on(AS.FORMS.EVENT_TYPE.dataLoad, function () {
  view.updateValueFromModel();
});

view.markInvalid = function(){
  select.css({
    "background-color": "#fff3f3",
    "border": "1px solid #ecacad"
  });
  label.css({
    "background-color": "#fff3f3",
    "border": "1px solid #ecacad"
  });
};

view.unmarkInvalid = function(){
  select.css({
    "background-color": "#ffffff",
    "border": "1px solid #d6d6d6"
  });
  label.css({
    "background-color": "#ffffff",
    "border": ""
  });
};

const setField = (from, modelTo) => {
  if(from) {
    switch (from.type) {
      case 'textbox':
      case 'textarea':
        if(modelTo && from.hasOwnProperty('value') && from.value) {
          modelTo.setValue(from.value);
        } else {
          if(modelTo) modelTo.setValue(null);
        }
        break;
      case 'check':
        if(modelTo && from.hasOwnProperty('values')) {
          modelTo.setValue(from.values);
        } else {
          if(modelTo) modelTo.setValue(null);
        }
        break;
      case 'entity':
        if(modelTo && from.hasOwnProperty('key') && from.key && from.key !== 'null') {
          switch (modelTo.asfProperty.config.entity) {
            case "users":
              modelTo.setValue({
                personID: from.key,
                personName: from.value
              });
              break;
            case "departments":
              modelTo.setValue({
                departmentId: from.key,
                departmentName: from.value
              });
              break;
            case "positions":
              modelTo.setValue({
                elementID: from.key,
                elementName: from.value
              });
              break;
          }
        } else {
          if(modelTo) modelTo.setValue(null);
        }
        break;
      default:
        if(modelTo && from.hasOwnProperty('key') && from.key) {
          modelTo.setValue(from.key);
        } else {
          if(modelTo) modelTo.setValue(null);
        }
    }
  } else {
    if(modelTo) modelTo.setValue(null);
  }
}

const clearFields = () => {
  model.matching.forEach(id => {
    let tmpModel = model.playerModel.getModelWithId(id.to, model.asfProperty.ownerTableId, model.asfProperty.tableBlockIndex);
    if(tmpModel) tmpModel.setValue(null);
  });
}

const matchingInit = documentID => {
  if(!editable) return;
  if(!documentID) {
    clearFields();
    return;
  }

  AS.SERVICES.showWaitWindow();
  try {
    AS.FORMS.ApiUtils.getAsfDataUUID(documentID)
    .then(uuid => AS.FORMS.ApiUtils.loadAsfData(uuid))
    .then(asfData => {
      model.matching.forEach(item => {
        let from = asfData.data.find(x => x.id === item.from);
        let modelTo = model.playerModel.getModelWithId(item.to, model.asfProperty.ownerTableId, model.asfProperty.tableBlockIndex);
        setField(from, modelTo);
      });
      AS.SERVICES.hideWaitWindow();
    });
  } catch (e) {
    AS.SERVICES.hideWaitWindow();
  }
}

const fillSelect = items => {
  let emptyValue = AS.OPTIONS.locale == 'kk' ? 'Таңдалған жоқ' : 'Не выбрано';
  select.empty();
  select.append(`<option value="0" disabled selected>${emptyValue}</option>`);
  items.forEach(item => select.append(`<option value="${item.value}">${item.title}</option>`));
  setTimeout(() => {
    if(model.hasOwnProperty('defaultValue') && model.defaultValue && !model.getValue()) {
      select.val(model.defaultValue).trigger('change');
    }
  }, 300);
  view.updateValueFromModel();
}

const initSelectRegistry = () => {
  let items = [];
  let url = `rest/api/registry/data_ext?registryCode=${model.registryCode}&fields=${model.fieldTitle}&locale=${AS.OPTIONS.locale}`;

  if(model.customFilter) {
    let searchModel = model.playerModel.getModelWithId(model.customFilter.searchCmpId);
    if(searchModel && searchModel.getValue()) {
      url = `${url}&condition=${model.customFilter.condition}&field=${model.customFilter.field}&${model.customFilter.key ? 'key' : 'value'}=${model.customFilter.key ? searchModel.getAsfData().key : searchModel.getAsfData().value}`;
    }
  }

  AS.FORMS.ApiUtils.simpleAsyncGet(url).then(res => {
    if(res.errorCode) return;
    res.result.forEach(item => {
      items.push({title: item.fieldValue[model.fieldTitle], value: item.documentID})
    });
    fillSelect(items);
  });
}

if(editable) {
  select.show();
  label.hide();
} else {
  select.hide();
  label.show();
}

if(model.customFilter) {
  let searchModel = model.playerModel.getModelWithId(model.customFilter.searchCmpId, model.asfProperty.ownerTableId, model.asfProperty.tableBlockIndex);
  searchModel.on('valueChange', () => {
    valueUpdate = null;
    model.setValue(null);
    initSelectRegistry();
  });
}

select.on('change', el => {
  let key = $(el.currentTarget).val();
  let value = $(el.currentTarget).find('option:selected').text();
  model.setAsfData({key: key, value: value});
  if(model.matching && model.matching.length > 0) matchingInit(key);
});

initSelectRegistry();
