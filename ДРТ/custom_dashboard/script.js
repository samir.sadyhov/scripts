new Vue({
  el: '#custom_dashboard',
  data: function() {
    return {
      appShow: false,
      date_period: "",
      options: [],
      optionurl: "",
      urlDashboard: "",
      showPeriod: false,
      h: "800px"
    }
  },
  mounted() {
    setInterval(() => {
      let tsdFrame = $("iframe[src*='/Synergy/resources/wait/wait.white.gif#custom_dashboards']");
      if (tsdFrame.length > 0) {
        this.h = (tsdFrame.height() - 60) + 'px';
        if(!this.appShow) {
            tsdFrame.after($('#custom_dashboard'));
            tsdFrame.hide();
            this.appShow = true;
        }
      } else if (tsdFrame.length == 0) {
        this.appShow = false;
      }
      if(this.options.length === 0) this.getDashboards();
    }, 1000);
  },
  methods: {
    setPeriod() {
        this.urlDashboard = `${this.optionurl.split('||')[1].split('?')[0]}?embed=true&_g=(time:(from:'${this.date_period[0]}',mode:absolute,to:'${this.date_period[1]}'))`
    },
    getDashboards() {
      let me = this;
      let query = 'registryCode=custom_dashboard_registry&condition=CONTAINS&field=dasboard_visible&key=2&fields=dasboard_name&fields=dasboard_url&fields=dasboard_period';
      $.when(AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/data_ext?${query}`)).then((response) => {
          if (response.recordsCount < 1) return;
          me.options = [];
          response.result.forEach(item => {
             me.options.push({
               value: `${item.fieldKey.dasboard_period === '2'}||${item.fieldValue.dasboard_url}`,
               label: item.fieldValue.dasboard_name
             });
          });
      }).fail((e) => console.log(e));
    }
  },
  watch: {
    optionurl() {
      this.urlDashboard = this.optionurl.split('||')[1];
      this.showPeriod = this.optionurl.split('||')[0] === 'true' ? true : false;
      this.date_period = "";
    }
  }
})
