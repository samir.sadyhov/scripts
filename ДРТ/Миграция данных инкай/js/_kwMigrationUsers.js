const matchingBooks = [
  {from: 'hcm_form_position_bookNum', to: 'kw_form_position_bookNum'},
  {from: 'hcm_form_position_bookFile', to: 'kw_form_position_bookFile'},
  {from: 'hcm_form_position_bookLink', to: 'kw_form_position_bookLink'}
];

const fetchUserFullName = user => {
  let {lastname, firstname, patronymic} = user;
  firstname = ` ${firstname.charAt(0)}.`;
  patronymic = patronymic != '' ? ` ${patronymic.charAt(0)}.` : '';
  return lastname + firstname + patronymic;
}

const toTranslit = text => {
  return text.replace(/([а-яё])|([\s_-])|([^a-z\d])/gi, (all, ch, space, words, i) => {
    if (space || words) return space ? '_' : '';
    const code = ch.charCodeAt(0);
    const index = code == 1025 || code == 1105 ? 0 : code > 1071 ? code - 1071 : code - 1039;
    const t = ['yo', 'a', 'b', 'v', 'g', 'd', 'e', 'zh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o',
    'p', 'r', 's', 't', 'u', 'f', 'kh', 'c', 'ch', 'sh', 'shch', '', 'y', '', 'e', 'yu', 'ya'];
    return t[index];
  });
}

this._kwMigrationUsers = {
  errors: false,

  groupCode: 'student',

  positionCode: 'obuchauschiesya',
  positionID: null,
  positionName: 'Обучающиеся',

  department: {
    departmentID: '1',
    departmentName: 'ROOT'
  },

  getCountsData: async function(){
    const trainingProfile = await kwApi.countRegistryData(this.servers.sourceServer, 'registryCode=hcm_registry_trainingProfile');
    const userCards = await kwApi.countRegistryData(this.servers.sourceServer, 'registryCode=hcm_registry_userCards');

    return {trainingProfile, userCards};
  },

  searchProfile: async function(profileName) {
    const params = $.param({
      registryCode: 'kw_registry_trainingProfile',
      field: 'kw_form_profile_name',
      condition: 'TEXT_EQUALS',
      value: profileName
    });
    return await kwApi.countRegistryData(this.servers.receiverServer, params);
  },

  searchCourse: async function(courseCode) {
    const params = $.param({
      registryCode: 'kw_registry_course',
      field: 'kw_form_skill_code',
      condition: 'TEXT_EQUALS',
      value: courseCode,
      fields: 'kw_form_course_pasport',
      loadData: true
    });
    return await kwApi.searchInRegistry(this.servers.receiverServer, params);
  },

  getProfileAsfData: async function(profileAsfData, profileName, newProfileDataUUID) {
    const asfData = [];
    const positionNumber = UTILS.getValue(profileAsfData, 'hcm_form_position_number');
    const competence = UTILS.getValue(profileAsfData, 'hcm_form_position_competence_name');
    const tableBooks = UTILS.parseAsfTable(UTILS.getValue(profileAsfData, 'hcm_form_position_books'));
    const newTableBooks = {
      id: 'kw_form_position_books',
      type: 'appendable_table',
      data: []
    };

    if(positionNumber) UTILS.setValue(asfData, 'kw_form_position_number', positionNumber);

    asfData.push({id: 'kw_form_profile_name', type: 'textbox', value: profileName?.value});
    asfData.push({id: 'kw_form_position_choice', type: 'entity', value: this.positionName, key: this.positionID});

    if(competence && competence.hasOwnProperty('key')) {
      const courseDocumentIDs = competence.key.split(';');
      const docs = [];

      for(let i = 0; i < courseDocumentIDs.length; i++) {
        const courseDocumentID = courseDocumentIDs[i];
        const courseDataUUID = await kwApi.getAsfDataUUID(this.servers.sourceServer, courseDocumentID);
        const courseAsfData = await kwApi.loadAsfData(this.servers.sourceServer, courseDataUUID);
        const pasportLink = UTILS.getValue(courseAsfData, "hcm_form_compitience_pasport");
        const pasportDataUUID = await kwApi.getAsfDataUUID(this.servers.sourceServer, pasportLink.key);
        const pasportAsfData = await kwApi.loadAsfData(this.servers.sourceServer, pasportDataUUID);
        const courseCode = UTILS.getValue(pasportAsfData, 'hcm_form_skill_code')?.value;

        if(courseCode) {
          const searchCourseResult = await this.searchCourse(courseCode);
          if(searchCourseResult && searchCourseResult.recordsCount > 0) {
            const res = searchCourseResult.result[0];
            docs.push({
              documentID: res.documentID,
              meaning: res.fieldValue?.kw_form_course_pasport || `${courseCode}`
            });
          } else {
            UTILS.setLog(`[ K.${i+1} ] Курс с кодом <b>${courseCode}</b> не найден на сервере приемнике и не будет добавлен к профилю`, 'info');
          }
        }
      }

      if(docs.length) {
        const courselistValue = docs.map(x => x.meaning).join(';');
        const courselistKey = docs.map(x => x.documentID).join(';');
        asfData.push({
          id: 'kw_form_position_competence_name',
          type: 'reglink',
          value: courselistValue,
          key: courselistKey,
          valueID: courselistKey
        });
      }
    }

    for(let i = 0; i < tableBooks.length; i++) {
      const item = tableBooks[i];

      for(let j = 0; j < matchingBooks.length; j++) {
        const id = matchingBooks[j];
        if(!item.hasOwnProperty(id.from)) continue;

        const fromData = item[id.from];
        if(!fromData) continue;

        if(fromData.type == 'file' && fromData.hasOwnProperty('key')) {
          const fileName = fromData.value;
          const fileID = fromData.key;
          const resultUpload = await kwApi.uploadFile(fileID, fileName, newProfileDataUUID);

          if(!resultUpload) {
            this.errors = true;
            UTILS.setLog(`[ B.${i+1} <b>${fileName}</b> ] Не удалось загрузить файл`, 'error');
          } else {
            UTILS.setLog(`[ B.${i+1} <b>${fileName}</b> ] Файл загружен`, 'info');
            UTILS.setValue(newTableBooks, `${id.to}-b${i+1}`, {
              type: `file`,
              value: fileName,
              key: resultUpload.fileId
            });
          }
        } else {
          UTILS.setValue(newTableBooks, `${id.to}-b${i+1}`, fromData);
        }
      }

    }

    asfData.push(newTableBooks);

    return asfData;
  },

  migrationProfiles: async function(profiles, resolve) {
    UTILS.setLog(`<b>Миграция реестра "Профиль обучения"</b>`, 'info');

    for(let i = 0; i < profiles.count; i++) {

      if(Cons.getAppStore().isStopMigration) {
        resolve(`STOP Миграция прервана вручную`);
        break;
      }

      const profileAsfData = await kwApi.loadAsfData(this.servers.sourceServer, profiles.data[i].dataUUID);
      const profileName = UTILS.getValue(profileAsfData, "hcm_form_profile_name");
      const resSearchProfile = await this.searchProfile(profileName?.value);

      if(Number(resSearchProfile.recordsCount) > 0) {
        UTILS.setLog(`#P.${i + 1}: Профиль <b>${profileName?.value}</b> уже создан на сервере приемнике. Пропускаем`, 'info');
        UTILS.setProgres(profiles.count, i+1);
        continue;
      }

      UTILS.setLog(`[ P.${i+1} ] Создание записи в реестре "Профиль обучения" на сервере приемнике`, 'info');
      const resultCreateDoc = await kwApi.createDoc(this.servers.receiverServer, 'kw_registry_trainingProfile');

      if(resultCreateDoc && resultCreateDoc.errorCode == "0") {
        const {documentID, dataUUID} = resultCreateDoc;

        UTILS.setLog(`[ P.${i+1} ] Создана запись для профиля на сервере приемнике [dataUUID: ${dataUUID}, documentID: ${documentID}]`, 'success');
        UTILS.setLog(`[ P.${i+1} ] Формирование JSON для сохранения данных профиля на сервере приемнике`, 'info');
        const newProfileAsfData = await this.getProfileAsfData(profileAsfData, profileName, dataUUID);

        const resultMergeFormData = await kwApi.mergeFormData(this.servers.receiverServer, dataUUID, newProfileAsfData);
        if(!resultMergeFormData) {
          this.errors = true;
          UTILS.setLog(`[ P.${i+1} ] Ошибка сохранения данных`, 'error');
        } else {
          await kwApi.activateDoc(this.servers.receiverServer, dataUUID);
          UTILS.setLog(`[ P.${i+1} ] Профиль сохранен`, 'success');
        }
        UTILS.setProgres(profiles.count, i+1);

      } else {
        this.errors = true;
        UTILS.setProgres(profiles.count, i+1);
        UTILS.setLog(`[ P.${i+1} ] ${resultCreateDoc.errorMessage}`, 'error');
      }
    }

    if(!Cons.getAppStore().isStopMigration) UTILS.setLog(`<b>Миграция реестра "Профиль обучения" завершен</b>`, 'success');
  },

  getUserCardAsfData: async function(userFullName, resultCreateUser, profile) {
    const userCardAsfData = [];

    userCardAsfData.push({
      id: 'kw_form_userCard_user',
      type: 'entity',
      key: resultCreateUser.userID,
      value: userFullName
    });

    const resSearchProfile = await kwApi.searchInRegistry(this.servers.receiverServer, $.param({
      registryCode: 'kw_registry_trainingProfile',
      field: 'kw_form_profile_name',
      condition: 'TEXT_EQUALS',
      value: profile?.value,
      loadData: false
    }));

    if(resSearchProfile && resSearchProfile.count > 0) {
      userCardAsfData.push({
        id: 'kw_form_userCard_reglink',
        type: 'reglink',
        value: profile?.value,
        key: resSearchProfile.data[0].documentID,
        valueID: resSearchProfile.data[0].documentID
      });
    } else {
      userCardAsfData.push({
        id: 'kw_form_userCard_reglink',
        type: 'reglink'
      });
    }

    userCardAsfData.push({
      id: 'kw_form_userCard_department',
      type: 'entity',
      key: this.department.departmentID,
      value: this.department.departmentName
    });

    const manager = this.servers.receiverServer.user;

    userCardAsfData.push({
      id: 'kw_form_userCard_manager',
      type: 'entity',
      key: manager.userid,
      value: fetchUserFullName(manager)
    });

    return userCardAsfData;
  },

  migrationUsers: async function(userCards, resolve) {
    if(!Cons.getAppStore().isStopMigration) UTILS.setLog(`<b>Миграция пользователей и карточек пользователей</b>`, 'info');

    for(let i = 0; i < userCards.count; i++) {

      if(Cons.getAppStore().isStopMigration) {
        resolve(`STOP Миграция прервана вручную`);
        break;
      }

      const cardAsfData = await kwApi.loadAsfData(this.servers.sourceServer, userCards.data[i].dataUUID);
      const user = UTILS.getValue(cardAsfData, "hcm_form_userCard_user");
      const profile = UTILS.getValue(cardAsfData, "hcm_form_userCard_reglink");

      if(!user || !user.hasOwnProperty('key')) continue;
      if(!profile || !profile.hasOwnProperty('key')) continue;

      const userInfo = await kwApi.getUserInfo(this.servers.sourceServer, user.key);

      if(!userInfo) continue;

      const {lastname, firstname, patronymic} = userInfo;

      const userFullName = fetchUserFullName(userInfo);
      const userPointerCode = toTranslit(`${lastname} ${firstname}${patronymic && patronymic != '' ? ' ' + patronymic : ''}`);
      const resultSearchUser = await kwApi.searchUser(this.servers.receiverServer, userPointerCode);

      if(resultSearchUser.result == 'true') {
        UTILS.setLog(`[ П.${i+1} ] Пользователь ${userFullName} с кодом: <b>${userPointerCode}</b> уже есть на сервере приемнике, пропускаем`, 'info');
        UTILS.setProgres(userCards.count, i+1);
        continue;
      }

      const resultCreateUser = await kwApi.createUser(this.servers.receiverServer, {
          login: userFullName,
          lastname, firstname, patronymic,
          password: '1',
          email: '',
          pointersCode: userPointerCode,
          isConfigurator: false,
          isAdmin: false,
          isChancellery: false
      });

      if(resultCreateUser.errorCode != '0') {
        this.errors = true;
        UTILS.setLog(`[ П.${i+1} ] Ошибка создания пользователя: <b>${resultCreateUser.errorMessage}</b>`, 'error');
        UTILS.setProgres(userCards.count, i+1);
        continue;
      } else {
        UTILS.setLog(`[ П.${i+1} ] ${userFullName} <b>${resultCreateUser.errorMessage}</b>`, 'success');
      }

      await kwApi.addUserGroup(this.servers.receiverServer, resultCreateUser.userID, this.groupCode);

      const resultPositionsAppoint = await kwApi.positionsAppoint(this.servers.receiverServer, this.positionID, resultCreateUser.userID);

      if(!resultPositionsAppoint || resultPositionsAppoint.errorCode != 0) {
        this.errors = true;
        UTILS.setLog(`[ П.${i+1} ] Ошибка назначения пользователя ${userFullName} на должность: <b>${resultPositionsAppoint.errorMessage}</b>`, 'error');
        UTILS.setProgres(userCards.count, i+1);
        continue;
      } else {
        UTILS.setLog(`[ П.${i+1} ] Пользователь ${userFullName} назначен на должность: <b>${this.positionName}</b>`, 'success');
      }

      const userCardAsfData = await this.getUserCardAsfData(userFullName, resultCreateUser, profile);
      const resultCreateDoc = await kwApi.createDocRCC(this.servers.receiverServer, 'kw_registry_userCards', userCardAsfData, true);

      if(resultCreateDoc && resultCreateDoc.errorCode == 0) {
        const {documentID, dataID} = resultCreateDoc;
        UTILS.setLog(`[ П.${i+1} ] Создана карточка пользователя ${userFullName} на сервере приемнике [dataUUID: ${dataID}, documentID: ${documentID}]`, 'success');
      } else {
        this.errors = true;
        UTILS.setLog(`[ П.${i+1} ] Ошибка при создании карточки пользователя ${userFullName}`, 'error');
      }

      UTILS.setProgres(userCards.count, i+1);
    }
  },

  start: async function(){
    return new Promise(async resolve => {
      try {
        const {servers} = Cons.getAppStore();
        this.servers = servers;

        UTILS.setLog(`Запуск миграции`, 'start');

        const rootDepartment = await kwApi.getDepartmentsContent(this.servers.receiverServer, '1');

        if(rootDepartment && rootDepartment.departments.length > 0) {
          this.department = rootDepartment.departments[0];
        }

        UTILS.setLog(`Поиск должности для назначения по коду: <b>${this.positionCode}</b>`, 'info');
        const searchPositionResult = await kwApi.searchPosition(this.servers.receiverServer, this.positionCode);

        if(searchPositionResult && searchPositionResult.length > 0) {
          this.positionID = searchPositionResult[0];
        } else {
          UTILS.setLog(`Должность не найдена, создаем новую должность: <b>${this.positionName}</b>`, 'info');
          const createPositionResult = await kwApi.createPosition(this.servers.receiverServer, {
              departmentID: this.department.departmentID,
              pointersCode: this.positionCode,
              nameRu: this.positionName,
              nameKz: this.positionName,
              nameEn: this.positionName
          });

          if(!createPositionResult) throw new Error('Ошибка создания должности на сервере приемнике');
          if(createPositionResult.errorCode != '0') throw new Error(createPositionResult.errorMessage);

          this.positionID = createPositionResult.positionID;
        }

        UTILS.setLog(`positionID должности для назначения: <b>${this.positionID}</b>`, 'info');

        const counts = await this.getCountsData();

        if(!counts.trainingProfile || counts.trainingProfile.recordsCount == '0') throw new Error('Не найдено записей в реестре <b>"Профиль обучения"</b>');
        if(!counts.userCards || counts.userCards.recordsCount == '0') throw new Error('Не найдено записей в реестре <b>"Карточка пользователя"</b>');

        UTILS.setLog(`Всего записей в реестре "Профиль обучения": <b>${counts.trainingProfile.recordsCount}</b>`, 'info');
        UTILS.setLog(`Всего записей в реестре "Карточка пользователя": <b>${counts.userCards.recordsCount}</b>`, 'info');

        const profiles = await kwApi.searchInRegistry(this.servers.sourceServer, `registryCode=hcm_registry_trainingProfile&loadData=false`);

        if(!profiles || profiles.count == 0) throw new Error('Ошибка получения данных с реестра <b>"Профиль обучения"</b>');

        await this.migrationProfiles(profiles, resolve);

        const userCards = await kwApi.searchInRegistry(this.servers.sourceServer, `registryCode=hcm_registry_userCards&loadData=false`);
        if(!userCards || userCards.count == 0) throw new Error('Ошибка получения данных с реестра <b>"Карточка пользователя"</b>');

        await this.migrationUsers(userCards, resolve);

        resolve(`Миграция прошла ${this.errors ? 'с ошибками' : 'успешно'}`);
      } catch (err) {
        console.log(err);
        resolve(`ERROR: ${err.message}`);
      }
    });
  }
}
