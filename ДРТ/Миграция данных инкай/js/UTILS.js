this.UTILS = {
  createField: function(fieldData) {
    const field = {};
    for (let key in fieldData) field[key] = fieldData[key];
    return field;
  },
  getValue: function(data, cmpID) {
    data = data.data ? data.data : data;
    return data.find(x => x.id == cmpID);
  },
  setValue: function(asfData, cmpID, data) {
    let field = this.getValue(asfData, cmpID);
    if(field) {
      for (let key in data) {
        if(key === 'id' || key === 'type') continue;
        field[key] = data[key];
      }
      return field;
    } else {
      asfData = asfData.data ? asfData.data : asfData;
      field = this.createField(data);
      field.id = cmpID;
      asfData.push(field);
      return field;
    }
  },
  getTableBlockIndex: function(data, cmp) {
    let res = 0;
    data = data.data ? data.data : data;
    data.forEach(item => {
      if (item.id.slice(0, item.id.indexOf('-b')) === cmp) res++;
    });
    return res === 0 ? 1 : ++res;
  },
  getCurrentDate: function() {
    const datetime = new Date();
    return datetime.getFullYear() + '-' +
      ('0' + (datetime.getMonth() + 1)).slice(-2) + '-' +
      ('0' + datetime.getDate()).slice(-2) + ' ' +
      ('0' + datetime.getHours()).slice(-2) + ':' +
      ('0' + datetime.getMinutes()).slice(-2) + ':' +
      ('0' + datetime.getSeconds()).slice(-2);
  },
  setLog: function(msg, type) {
    const text = `<b>${this.getCurrentDate()}</b> [${type.toUpperCase()}] - ${msg}`;
    $('#panelLog').append(`<p class="log-${type.toLowerCase()}">${text}</p>\n`);
  },
  saveLog: function() {
    const a = document.createElement("a");
    const blob = new Blob([$('#panelLog').text()], {type: "octet/stream"});
    const url = window.URL.createObjectURL(blob);
    a.href = url;
    a.download = `kw_migration_${this.getCurrentDate()}.log`;
    a.click();
    window.URL.revokeObjectURL(url);
  }
}

UTILS.setProgres = async (countAllRows, finishRows) => {
  let percent = 100 * finishRows / countAllRows;
  if(percent > 99) percent = 100;
  fire({type: 'change_label', text: localizedText(`${percent}%`, `${percent}%`, `${percent}%`)}, 'progressLabel');
  $(`#panprogressBar`).css({'width': `${percent}%`});
}

UTILS.parseAsfValue = asfDataValue => {
  if(!asfDataValue) return null;
  const {type, value = '', key = ''} = asfDataValue;
  return key ? {type, value, key} : {type, value};
}

UTILS.parseAsfTable = asfTable => {
  try {
    const result = [];

    if(!asfTable.hasOwnProperty('data')) return result;

    const data = asfTable.data.filter(x => x.type != 'label');

    if(!data.length) return result;

    const ids = data.map(x => x.id.slice(0, x.id.indexOf('-b'))).uniq();
    let tbi =  data.slice(-1)[0].id;
    tbi = Number(tbi.slice(tbi.indexOf('-b') + 2));

    for(let i = 1; i <= tbi; i++) {
    	const item = {};
      ids.forEach(key => {
        const parseValue = UTILS.parseAsfValue(UTILS.getValue(asfTable, `${key}-b${i}`));
        if(parseValue) item[key] = parseValue;
      });
      result.push(item);
    }

    return result;
  } catch (err) {
    console.log('ERROR parseAsfTable', err);
  }
}

//выпиливыние из массива повторяющихся елементов
Array.prototype.uniq = function() {
  return this.filter(function(v, i, a){ return i == a.indexOf(v) });
}
