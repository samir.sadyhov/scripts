const matchingGroup = [
  {from: 'hcm_form_courseGroup_name', to: 'kw_form_courseGroup_name'},
  {from: 'hcm_form_courseGroup_name_kz', to: 'kw_form_courseGroup_name_kz'},
  {from: 'hcm_form_courseGroup_description', to: 'kw_form_courseGroup_description'},
  {from: 'hcm_form_courseGroup_description_kz', to: 'kw_form_courseGroup_description_kz'}
];

const matchingSkills = [
  {from: 'hcm_form_skill_code', to: 'kw_form_course_code'},
  {from: 'hcm_form_skill_name', to: 'kw_form_course_pasport'},
  {from: 'hcm_form_skill_name_kz', to: 'kw_form_course_pasport_kz'},
  {from: 'hcm_form_skill_description', to: 'kw_form_course_skill'},
  {from: 'hcm_form_skill_description_kz', to: 'kw_form_course_skill_kz'},
  {from: 'hcm_form_skill_attribute', to: 'kw_form_course_attribute'},
  {from: 'hcm_form_skill_attribute_kz', to: 'kw_form_course_attribute_kz'},
  {from: 'hcm_form_skill_way', to: 'kw_form_course_way'},
  {from: 'hcm_form_skill_way_kz', to: 'kw_form_course_way_kz'},
  {from: 'hcm_form_skill_author', to: 'kw_form_course_author'},
  {from: 'hcm_form_skill_note', to: 'kw_form_course_note'},
  {from: 'hcm_form_skill_note_kz', to: 'kw_form_course_note_kz'}
];

const matchingCourse = [
  {from: 'hcm_form_compitience_status', to: 'kw_form_course_status'},
  {from: 'hcm_form_compitience_finishdate', to: 'kw_form_course_finishdate'}
];

const matchingTestGroups = [
  {from: 'hcm_form_testGroups_minLevel', to: 'kw_form_course_minLevel'},
  {from: 'appTesting-randomAnswer', to: 'appTesting-randomAnswer'},
  {from: 'appTesting-randomQuesting', to: 'appTesting-randomQuesting'},
  {from: 'appTesting-timerEnable', to: 'appTesting-timerEnable'},
  {from: 'appTesting-useAttempts', to: 'appTesting-useAttempts'},
  {from: 'appTesting-name', to: 'appTesting-name'},
  {from: 'appTesting-name_kz', to: 'appTesting-name_kz'},
  {from: 'appTesting-submitStart', to: 'appTesting-submitStart'},
  {from: 'appTesting-submitStart_kz', to: 'appTesting-submitStart_kz'},
  {from: 'appTesting-submitNext', to: 'appTesting-submitNext'},
  {from: 'appTesting-submitNext_kz', to: 'appTesting-submitNext_kz'},
  {from: 'appTesting-submitEnd', to: 'appTesting-submitEnd'},
  {from: 'appTesting-submitEnd_kz', to: 'appTesting-submitEnd_kz'},
  {from: 'appTesting-info', to: 'appTesting-info'},
  {from: 'appTesting-info_kz', to: 'appTesting-info_kz'}
];

const matchingQuestions = [
  {from: 'hcm_form_testQuestion_name', to: 'kw_form_testQuestion_name'},
  {from: 'hcm_form_testQuestion_name_kz', to: 'kw_form_testQuestion_name_kz'},
  {from: 'hcm_form_testQuestion_questionFile', to: 'kw_form_testQuestion_questionFile'},
  {from: 'hcm_form_testQuestion_questionFile_kz', to: 'kw_form_testQuestion_questionFile_kz'},
  {from: 'hcm_form_testQuestion_answerCorrect', to: 'kw_form_testQuestion_answerCorrect'}
];

const matchingAnswers = [
  {from: 'hcm_form_testQuestion_number', to: 'kw_form_testQuestion_number'},
  {from: 'hcm_form_testQuestion_answer', to: 'kw_form_testQuestion_answer'},
  {from: 'hcm_form_testQuestion_answer_kz', to: 'kw_form_testQuestion_answer_kz'},
  {from: 'hcm_form_testQuestion_file', to: 'kw_form_testQuestion_file'},
  {from: 'hcm_form_testQuestion_file_kz', to: 'kw_form_testQuestion_file_kz'}
];

const matchingLessons = [
  {from: 'itsm_form_competence_taskname', to: 'kw_form_task_name'},
  {from: 'itsm_form_competence_taskname_kz', to: 'kw_form_task_name_kz'},
  {from: 'itsm_form_competence_main', to: 'kw_form_task_main'},
  {from: 'itsm_form_competence_main_kz', to: 'kw_form_task_main_kz'},
  {from: 'itsm_form_competence_standards', to: 'kw_form_task_standards'},
  {from: 'itsm_form_competence_standards_kz', to: 'kw_form_task_standards_kz'},

  {from: 'itsm_form_competence_videolink', to: 'kw_form_task_videolink', choice: 'kw_form_task_video_choice'},
  {from: 'itsm_form_competence_videolink_kz', to: 'kw_form_task_videolink_kz', choice: 'kw_form_task_video_choice_kz'},
  {from: 'itsm_form_competence_videofile', to: 'kw_form_task_videofile', choice: 'kw_form_task_video_choice'},
  {from: 'itsm_form_competence_videofile_kz', to: 'kw_form_task_videofile_kz', choice: 'kw_form_task_video_choice_kz'}
];

this._kwMigration = {
  errors: false,

  department: {
    departmentID: '1',
    departmentName: 'ROOT'
  },

  testServers: async function(param){
    const sourceServer = await kwApi.testServer(param.sourceServer);
    const receiverServer = await kwApi.testServer(param.receiverServer);
    return {sourceServer, receiverServer}
  },

  getCountsData: async function(){
    const groupsCourse = await kwApi.countRegistryData(this.servers.sourceServer, 'registryCode=hcm_registry_courseGroups');
    const courses = await kwApi.countRegistryData(this.servers.sourceServer, 'registryCode=hcm2_registry_competence');

    return {groupsCourse, courses};
  },

  getAllCourseGroups: async function() {
    return await kwApi.searchInRegistry(this.servers.sourceServer, `registryCode=hcm_registry_courseGroups&loadData=false`);
  },

  searchCourseGroup: async function(groupeName) {
    const params = $.param({
      registryCode: 'kw_registry_courseGroups',
      field: 'kw_form_courseGroup_name',
      condition: 'TEXT_EQUALS',
      value: groupeName
    });
    return await kwApi.countRegistryData(this.servers.receiverServer, params);
  },

  searchCourse: async function(courseCode) {
    const params = $.param({
      registryCode: 'kw_registry_course',
      field: 'kw_form_skill_code',
      condition: 'TEXT_EQUALS',
      value: courseCode,
      fields: 'kw_form_course_pasport',
      loadData: true
    });
    return await kwApi.searchInRegistry(this.servers.receiverServer, params);
  },

  searchTestGroup: async function(courseDocumentID) {
    const params = $.param({
      registryCode: 'hcm_registry_testGroups',
      field: 'hcm_form_testGroups_course',
      condition: 'TEXT_EQUALS',
      key: courseDocumentID,
      loadData: false
    });
    return await kwApi.searchInRegistry(this.servers.sourceServer, params);
  },

  getQuestionsAsfData: async function(questionAsfData, courseCode, newQuestionDataUUID){
    const asfData = [];
    const tableAnswers = {
      id: 'kw_form_testQuestion_tableAnswers',
      type: 'appendable_table',
      data: []
    };

    if(courseCode) {
      asfData.push({
        id: 'kw_form_testQuestion_theme',
        type: 'textbox',
        value: courseCode
      });
    }

    for(let i = 0; i < matchingQuestions.length; i++){
      const id = matchingQuestions[i];
      const fromData = UTILS.getValue(questionAsfData, id.from);

      if(!fromData) continue;
      if(fromData.type == 'entity') continue;

      if(fromData.type == 'file') {

        if(!fromData.hasOwnProperty('key')) continue;

        const fileName = fromData.value;
        const fileID = fromData.key;
        const resultUpload = await kwApi.uploadFile(fileID, fileName, newQuestionDataUUID);

        if(!resultUpload) {
          this.errors = true;
          UTILS.setLog(`[ Q.${id.to} ] Формулировка вопроса: Не удалось загрузить файл`, 'error');
        } else {
          UTILS.setLog(`[ Q.${id.to} ] Формулировка вопроса: Файл загружен`, 'info');
          UTILS.setValue(asfData, id.to, {
            type: 'file',
            value: fileName,
            key: resultUpload.fileId
          });
        }

      } else {
        UTILS.setValue(asfData, id.to, fromData);
      }
    }

    asfData.push(tableAnswers);

    const formTableAnswers = UTILS.getValue(questionAsfData, 'hcm_form_testQuestion_tableAnswers');
    const tbi = UTILS.getTableBlockIndex(formTableAnswers, "hcm_form_testQuestion_answer");

    for(let i = 0; i < tbi; i++) {
      for(let j = 0; j < matchingAnswers.length; j++){
        const id = matchingAnswers[j];
        const fromData = UTILS.getValue(formTableAnswers, `${id.from}-b${i+1}`);
        if(!fromData) continue;
        if(fromData.type == 'entity') continue;

        if(fromData.type == 'file') {

          if(!fromData.hasOwnProperty('key')) continue;

          const fileName = fromData.value;
          const fileID = fromData.key;
          const resultUpload = await kwApi.uploadFile(fileID, fileName, newQuestionDataUUID);

          if(!resultUpload) {
            this.errors = true;
            UTILS.setLog(`[ A.${id.to}-b${i+1} ] Вариант ответа: Не удалось загрузить файл`, 'error');
          } else {
            UTILS.setLog(`[ A.${id.to}-b${i+1} ] Вариант ответа: Файл загружен`, 'info');
            UTILS.setValue(tableAnswers, `${id.to}-b${i+1}`, {
              type: 'file',
              value: fileName,
              key: resultUpload.fileId
            });
          }

        } else {
          UTILS.setValue(tableAnswers, `${id.to}-b${i+1}`, fromData);
        }
      }
    }

    return asfData;
  },

  createQuestions: async function(asfData, questions, courseCode){
    const result = [];
    const meanings = questions.value.split(', ');

    for(let i = 0; i < questions.keys.length; i++) {
      const meaning = meanings[i];
      const questionDocID = questions.keys[i];
      const questionDataUUID = await kwApi.getAsfDataUUID(this.servers.sourceServer, questionDocID);
      UTILS.setLog(`[ Q.${i+1} ] Загрузка тестового вопроса с сервера источника [dataUUID: ${questionDataUUID}, documentID: ${questionDocID}]`, 'info');

      const questionAsfData = await kwApi.loadAsfData(this.servers.sourceServer, questionDataUUID);

      UTILS.setLog(`[ Q.${i+1} ] Создание записи в реестре "Тестовые вопросы" на сервере приемнике`, 'info');
      const resultCreateDoc = await kwApi.createDoc(this.servers.receiverServer, 'kw_registry_testQuestions');

      if(resultCreateDoc && resultCreateDoc.errorCode == "0") {
        const {documentID, dataUUID} = resultCreateDoc;

        UTILS.setLog(`[ Q.${i+1} ] Тестовый вопрос создан на сервере приемнике [dataUUID: ${dataUUID}, documentID: ${documentID}]`, 'success');
        UTILS.setLog(`[ Q.${i+1} ] Формирование JSON для создания тестового вопроса на сервере приемнике`, 'info');
        const newQuestionAsfData = await this.getQuestionsAsfData(questionAsfData, courseCode, dataUUID);

        const resultMergeFormData = await kwApi.mergeFormData(this.servers.receiverServer, dataUUID, newQuestionAsfData);
        if(!resultMergeFormData) {
          this.errors = true;
          UTILS.setLog(`[ Q.${i+1} ] Ошибка сохранения данных по тестовому вопросу`, 'error');
        } else {
          UTILS.setLog(`[ Q.${i+1} ] Данные по тестовому вопросу сохранены`, 'success');
          result.push({documentID, meaning});
        }

      } else {
        this.errors = true;
        UTILS.setLog(`[ Q.${i+1} ] ${resultCreateDoc.errorMessage}`, 'error');
      }
    }

    return result;
  },

  getLessonAsfDataTable: async function(newLessonDataUUID, item, tableID, choice, link, file) {
    const table = {
      id: tableID,
      type: 'appendable_table',
      data: []
    };

    if(item.hasOwnProperty(link.from)) {
      const fromData = item[link.from];
      if(fromData && fromData.key != "; false") {
        UTILS.setValue(table, `${choice}-b1`, {
          type: 'listbox',
          value: 'Ссылка',
          key: '0'
        });
        UTILS.setValue(table, `${link.to}-b1`, fromData);
      }
    }

    if(item.hasOwnProperty(`${link.from}_kz`)) {
      const fromData = item[`${link.from}_kz`];
      if(fromData && fromData.key != "; false") {
        UTILS.setValue(table, `${choice}_kz-b1`, {
          type: 'listbox',
          value: 'Ссылка',
          key: '0'
        });
        UTILS.setValue(table, `${link.to}_kz-b1`, fromData);
      }
    }

    if(item.hasOwnProperty(file.from)) {
      const fromData = item[file.from];
      if(fromData && fromData.hasOwnProperty('key')) {
        const fileName = fromData.value;
        const fileID = fromData.key;
        const resultUpload = await kwApi.uploadFile(fileID, fileName, newLessonDataUUID);

        if(!resultUpload) {
          this.errors = true;
          UTILS.setLog(`[ L.${tableID}.${file.to} ] Не удалось загрузить файл`, 'error');
        } else {
          UTILS.setLog(`[ L.${tableID}.${file.to} ] Файл загружен`, 'info');
          UTILS.setValue(table, `${file.to}-b1`, {
            type: 'file',
            value: fileName,
            key: resultUpload.fileId
          });
          UTILS.setValue(table, `${choice}-b1`, {
            type: 'listbox',
            value: 'Файл',
            key: '1'
          });
        }

      }
    }

    if(item.hasOwnProperty(`${file.from}_kz`)) {
      const fromData = item[`${file.from}_kz`];
      if(fromData && fromData.hasOwnProperty('key')) {
        const fileName = fromData.value;
        const fileID = fromData.key;
        const resultUpload = await kwApi.uploadFile(fileID, fileName, newLessonDataUUID);

        if(!resultUpload) {
          this.errors = true;
          UTILS.setLog(`[ L.${tableID}.${file.to}_kz ] Не удалось загрузить файл`, 'error');
        } else {
          UTILS.setLog(`[ L.${tableID}.${file.to}_kz ] Файл загружен`, 'info');
          UTILS.setValue(table, `${file.to}_kz-b1`, {
            type: 'file',
            value: fileName,
            key: resultUpload.fileId
          });
          UTILS.setValue(table, `${choice}_kz-b1`, {
            type: 'listbox',
            value: 'Файл',
            key: '1'
          });
        }

      }
    }

    return table;
  },

  getLessonAsfData: async function(item, newLessonDataUUID) {
    const asfData = [];

    for(let i = 0; i < matchingLessons.length; i++) {
      const id = matchingLessons[i];
      if(!item.hasOwnProperty(id.from)) continue;

      const fromData = item[id.from];
      if(!fromData) continue;

      if(fromData.type == 'file' && fromData.hasOwnProperty('key')) {
        const fileName = fromData.value;
        const fileID = fromData.key;
        const resultUpload = await kwApi.uploadFile(fileID, fileName, newLessonDataUUID);

        if(!resultUpload) {
          this.errors = true;
          UTILS.setLog(`[ L.${id.to} ] Видео урока: Не удалось загрузить файл`, 'error');
        } else {
          UTILS.setLog(`[ L.${id.to} ] Видео урока: Файл загружен`, 'info');
          UTILS.setValue(asfData, id.to, {
            type: 'file',
            value: fileName,
            key: resultUpload.fileId
          });
          UTILS.setValue(asfData, id.choice, {
            type: 'listbox',
            value: 'Файл',
            key: '1'
          });
        }

      } else if (fromData.type == 'link' && fromData.key != "; false") {
        UTILS.setValue(asfData, id.choice, {
          type: 'listbox',
          value: 'Ссылка',
          key: '0'
        });
      } else {
        UTILS.setValue(asfData, id.to, fromData);
      }
    }

    const educationalTable = await this.getLessonAsfDataTable(newLessonDataUUID, item,
      'kw_form_task_educational_material_table',
      'kw_form_task_material_choice',
      {
        from: 'itsm_form_competence_materiallink',
        to: 'kw_form_task_materiallink'
      },
      {
        from: 'itsm_form_competence_materialfile',
        to: 'kw_form_task_materialfile'
      }
    );

    const additionalTable = await this.getLessonAsfDataTable(newLessonDataUUID, item,
      'kw_form_task_additional_material_table',
      'kw_form_task_additional_material_choice',
      {
        from: 'itsm_form_competence_material1link',
        to: 'kw_form_task_additional_materiallink'
      },
      {
        from: 'itsm_form_competence_material1file',
        to: 'kw_form_task_additional_materialfile'
      }
    );

    asfData.push(educationalTable);
    asfData.push(additionalTable);

    return asfData;
  },

  createLessons: async function(tableCourseContent){
    const result = [];

    for(let i = 0; i < tableCourseContent.length; i++) {
      const item = tableCourseContent[i];
      const meaning = item?.itsm_form_competence_taskname?.value || `Урок ${i+1}`;

      UTILS.setLog(`[ L.${i+1} ] Создание записи в реестре "Уроки" на сервере приемнике`, 'info');
      const resultCreateDoc = await kwApi.createDoc(this.servers.receiverServer, 'kw_registry_tasks');

      if(resultCreateDoc && resultCreateDoc.errorCode == "0") {
        const {documentID, dataUUID} = resultCreateDoc;

        UTILS.setLog(`[ L.${i+1} ] Создана запись в реестре "Уроки" на сервере приемнике [dataUUID: ${dataUUID}, documentID: ${documentID}]`, 'success');
        UTILS.setLog(`[ L.${i+1} ] Формирование JSON для сохранения урока на сервере приемнике`, 'info');
        const lessonAsfData = await this.getLessonAsfData(item, dataUUID);

        const resultMergeFormData = await kwApi.mergeFormData(this.servers.receiverServer, dataUUID, lessonAsfData);
        if(!resultMergeFormData) {
          this.errors = true;
          UTILS.setLog(`[ L.${i+1} ] Ошибка сохранения данных по уроку`, 'error');
        } else {
          UTILS.setLog(`[ L.${i+1} ] Данные по уроку сохранены`, 'success');
          result.push({documentID, meaning});
        }

      } else {
        this.errors = true;
        UTILS.setLog(`[ L.${i+1} ] ${resultCreateDoc.errorMessage}`, 'error');
      }

    }

    return result;
  },

  getCourseAsfData: async function(courseDocumentID, courseAsfData, pasportAsfData, courseCode) {
    const asfData = [];

    if(courseCode) {
      asfData.push({
        id: 'kw_form_skill_code',
        type: 'textbox',
        value: courseCode
      });
    }

    asfData.push({
      id: 'kw_form_course_department',
      type: 'entity',
      value: this.department.departmentName,
      key: this.department.departmentID
    });

    asfData.push({
      id: 'kw_form_course_resTask_choice',
      type: 'listbox',
      key: '1',
      value: 'Итоговое тестирование'
    });

    matchingCourse.forEach(id => {
      const fromData = UTILS.getValue(courseAsfData, id.from);
      if(fromData && fromData.type != 'entity') UTILS.setValue(asfData, id.to, fromData);
    });

    matchingSkills.forEach(id => {
      const fromData = UTILS.getValue(pasportAsfData, id.from);
      if(fromData && fromData.type != 'entity') UTILS.setValue(asfData, id.to, fromData);
    });

    const tableCourseContent = UTILS.parseAsfTable(UTILS.getValue(courseAsfData, 'hcm_form_competence_tableCourseContent'));
    const lessons = await this.createLessons(tableCourseContent);
    const tableLessons = {
      id: 'kw_form_course_tableContent',
      type: 'appendable_table',
      data: []
    };

    lessons.forEach((lesson, i) => {
      const {documentID, meaning} = lesson;
      tableLessons.data.push({
        id: `kw_form_course_taskname-b${i+1}`,
        type: 'reglink',
        value: meaning,
        valueID: documentID,
        key: documentID
      });
    });

    asfData.push(tableLessons);

    const tg = await this.searchTestGroup(courseDocumentID);
    if(tg && tg.count > 0) {

      UTILS.setLog(`Загрузка данных группы тестовых вопросов с сервера источника [dataUUID: ${tg.data[0].dataUUID}, documentID: ${tg.data[0].documentID}]`, 'info');

      const testGroupsAsfData = await kwApi.loadAsfData(this.servers.sourceServer, tg.data[0].dataUUID);
      const testGroups_questions = UTILS.getValue(testGroupsAsfData, 'hcm_form_testGroups_questions');

      matchingTestGroups.forEach(id => {
        const fromData = UTILS.getValue(testGroupsAsfData, id.from);
        if(fromData && fromData.type != 'entity') UTILS.setValue(asfData, id.to, fromData);
      });

      if(testGroups_questions && testGroups_questions.hasOwnProperty('keys')) {
        const newQuestions = await this.createQuestions(asfData, testGroups_questions, courseCode);

        const value = newQuestions.map(x => x.meaning).join(';');
        const key = newQuestions.map(x => x.documentID).join(';');

        asfData.push({
          id: 'kw_form_course_resTest',
          type: 'reglink',
          value, key,
          valueID: key
        });

      } else {
        UTILS.setLog(`Не найдено тестовых вопросов ${JSON.stringify(testGroups_questions, null, 4)}`, 'warn');
      }
    }

    return asfData;
  },

  createCourse: async function(courses) {
    const result = [];
    for(let i = 0; i < courses.keys.length; i++) {
      const courseDocumentID = courses.keys[i];
      const courseDataUUID = await kwApi.getAsfDataUUID(this.servers.sourceServer, courseDocumentID);

      UTILS.setLog(`[ K.${i+1} ] Загрузка данных по курсу с сервера источника [dataUUID: ${courseDataUUID}, documentID: ${courseDocumentID}]`, 'info');

      const courseAsfData = await kwApi.loadAsfData(this.servers.sourceServer, courseDataUUID);
      const courseImage = UTILS.getValue(courseAsfData, "hcm_form_compitience_image");
      const pasportLink = UTILS.getValue(courseAsfData, "hcm_form_compitience_pasport");
      const pasportDataUUID = await kwApi.getAsfDataUUID(this.servers.sourceServer, pasportLink.key);

      UTILS.setLog(`[ K.${i+1} ] Загрузка данных "паспорт умения" с сервера источника [dataUUID: ${pasportDataUUID}, documentID: ${pasportLink?.key}]`, 'info');
      const pasportAsfData = await kwApi.loadAsfData(this.servers.sourceServer, pasportDataUUID);

      const courseCode = UTILS.getValue(pasportAsfData, 'hcm_form_skill_code')?.value;

      if(courseCode) {
        UTILS.setLog(`[ K.${i+1} ] Поиск курса по коду <b>${courseCode}</b> на сервере приемнике`, 'info');

        const searchCourseResult = await this.searchCourse(courseCode);
        if(searchCourseResult && searchCourseResult.recordsCount > 0) {
          const res = searchCourseResult.result[0];
          UTILS.setLog(`[ K.${i+1} ] Курс с кодом <b>${courseCode}</b> найден на сервере приемнике [dataUUID: ${res.dataUUID}, documentID: ${res.documentID}], добавляем к группе`, 'info');
          result.push({
            documentID: res.documentID,
            meaning: res.fieldValue?.kw_form_course_pasport || `Курс ${courseCode}`
          });
          continue
        } else {
          UTILS.setLog(`[ K.${i+1} ] Курс с кодом <b>${courseCode}</b> не найден на сервере приемнике`, 'info');
        }
      }

      UTILS.setLog(`[ K.${i+1} ] Формирование JSON для создания курса на сервере приемнике`, 'info');

      const newCourseAsfData = await this.getCourseAsfData(courseDocumentID, courseAsfData, pasportAsfData, courseCode);

      const resultCreateDoc = await kwApi.createDocRCC(this.servers.receiverServer, 'kw_registry_course', newCourseAsfData, true);
      if(resultCreateDoc && resultCreateDoc.errorCode == 0) {
        const {documentID, dataID} = resultCreateDoc;
        UTILS.setLog(`[ K.${i+1} ] Курс создан на сервере приемнике [dataUUID: ${dataID}, documentID: ${documentID}]`, 'success');
        result.push({
          documentID,
          meaning: UTILS.getValue(newCourseAsfData, 'kw_form_course_pasport')?.value || `Kурс ${i+1}`
        });

        if(courseImage && courseImage.hasOwnProperty('key')) {
          const fileName = courseImage.value;
          const fileID = courseImage.key;
          const resultUpload = await kwApi.uploadFile(fileID, fileName, dataID);

          if(!resultUpload) {
            this.errors = true;
            UTILS.setLog(`[ K.${i+1} ] Не удалось загрузить изображение курса`, 'error');
          } else {
            const resultMergeFormData = await kwApi.mergeFormData(this.servers.receiverServer, dataID, [{
              id: 'kw_form_course_image',
              type: 'file',
              value: fileName,
              key: resultUpload.fileId
            }]);
            if(!resultMergeFormData) {
              this.errors = true;
              UTILS.setLog(`[ K.${i+1} ] Не удалось загрузить изображение курса`, 'error');
            } else {
              UTILS.setLog(`[ K.${i+1} ] Изображение курса успешно добавлено`, 'success');
            }
          }
        }

      } else {
        this.errors = true;
        UTILS.setLog(`[ K.${i+1} ] Ошибка при создании курса`, 'error');
      }

    }
    return result;
  },

  start: async function(){
    return new Promise(async resolve => {
      try {
        const {servers} = Cons.getAppStore();
        this.servers = servers;

        UTILS.setLog(`ЗАПУСК МИГРАЦИИ`, 'start');

        const counts = await this.getCountsData();

        if(!counts.groupsCourse || counts.groupsCourse.recordsCount == '0') throw new Error('Не найдено групп курсов');
        if(!counts.courses || counts.courses.recordsCount == '0') throw new Error('Не найдено курсов');

        UTILS.setLog(`Всего групп курсов: <b>${counts.groupsCourse.recordsCount}</b>`, 'info');
        UTILS.setLog(`Всего курсов: <b>${counts.courses.recordsCount}</b>`, 'info');

        const groups = await this.getAllCourseGroups();
        const rootDepartment = await kwApi.getDepartmentsContent(servers.receiverServer, '1');

        if(rootDepartment && rootDepartment.departments.length > 0) {
          this.department = rootDepartment.departments[0];
        }

        if(!groups || groups.count == 0) throw new Error('Ошибка получения данных по группам курсов');

        for(let i = 0; i < groups.count; i++) {

          if(Cons.getAppStore().isStopMigration) {
            resolve(`STOP Миграция прервана вручную`);
            break;
          }

          const {dataUUID, documentID} = groups.data[i];
          const groupAsfData = await kwApi.loadAsfData(servers.sourceServer, dataUUID);
          const groupName = UTILS.getValue(groupAsfData, "hcm_form_courseGroup_name");
          const courses = UTILS.getValue(groupAsfData, "hcm_form_courseGroup_list");
          const groupImage = UTILS.getValue(groupAsfData, "hcm_form_courseGroup_image");
          const resSearchGroup = await this.searchCourseGroup(groupName?.value);
          const newGroupAsfData = [];

          if(Number(resSearchGroup.recordsCount) > 0) {
            UTILS.setLog(`#G.${i + 1}: Группа <b>${groupName?.value}</b> уже создана на сервере приемнике. Пропускаем`, 'warn');
            UTILS.setProgres(groups.count, i+1);
            continue;
          }

          if(!courses || !courses.hasOwnProperty('keys') || courses.keys.length == 0) {
            UTILS.setLog(`#G.${i + 1}: Группа <b>${groupName?.value || 'НЕ НАЙДЕНО ИМЯ ГРУППЫ'}</b> не содержит курсов. Пропускаем`, 'warn');
            UTILS.setLog(`#G.${i + 1}: ${JSON.stringify(courses, null, 4)}`, 'info');
            UTILS.setProgres(groups.count, i+1);
            continue;
          }

          UTILS.setLog(`#G.${i + 1}: Формирование данных по группе <b>${groupName?.value || 'НЕ НАЙДЕНО ИМЯ ГРУППЫ'}</b> [dataUUID: ${dataUUID}, documentID: ${documentID}]`, 'info');

          newGroupAsfData.push({
            id: 'kw_form_courseGroup_department',
            type: 'entity',
            value: this.department.departmentName,
            key: this.department.departmentID
          });

          matchingGroup.forEach(id => {
            const fromData = UTILS.getValue(groupAsfData, id.from);
            if(fromData && fromData.type != 'entity') UTILS.setValue(newGroupAsfData, id.to, fromData);
          });

          const newCourses = await this.createCourse(courses);
          const courseGroup_listValue = newCourses.map(x => x.meaning).join(';');
          const courseGroup_listKey = newCourses.map(x => x.documentID).join(';');

          newGroupAsfData.push({
            id: 'kw_form_courseGroup_list',
            type: 'reglink',
            value: courseGroup_listValue,
            key: courseGroup_listKey,
            valueID: courseGroup_listKey
          });

          const resultCreateDoc = await kwApi.createDocRCC(servers.receiverServer, 'kw_registry_courseGroups', newGroupAsfData, true);
          if(resultCreateDoc && resultCreateDoc.errorCode == 0) {
            const {documentID, dataID} = resultCreateDoc;
            UTILS.setLog(`#G.${i + 1}: Группа курсов создан на сервере приемнике [dataUUID: ${dataID}, documentID: ${documentID}]`, 'success');

            if(groupImage && groupImage.hasOwnProperty('key')) {
              const fileName = groupImage.value;
              const fileID = groupImage.key;
              const resultUpload = await kwApi.uploadFile(fileID, fileName, dataID);

              if(!resultUpload) {
                this.errors = true;
                UTILS.setLog(`#G.${i + 1}: Не удалось загрузить изображение группы`, 'error');
              } else {
                const resultMergeFormData = await kwApi.mergeFormData(servers.receiverServer, dataID, [{
                  id: 'kw_form_courseGroup_image',
                  type: 'file',
                  value: fileName,
                  key: resultUpload.fileId
                }]);
                if(!resultMergeFormData) {
                  this.errors = true;
                  UTILS.setLog(`#G.${i + 1}: Не удалось загрузить изображение группы`, 'error');
                } else {
                  UTILS.setLog(`#G.${i + 1}: Изображение группы успешно добавлено`, 'success');
                }
              }
            }

          } else {
            this.errors = true;
            UTILS.setLog(`#G.${i + 1}: Ошибка при создании группы курсов`, 'error');
          }

          UTILS.setProgres(groups.count, i+1);
        }

        resolve(`Миграция прошла ${this.errors ? 'с ошибками' : 'успешно'}`);
      } catch (err) {
        console.log(err);
        resolve(`ERROR: ${err.message}`);
      }
    });
  }

}
