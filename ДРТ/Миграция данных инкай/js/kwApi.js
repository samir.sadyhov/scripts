const fileDownload = async (serverParam, id) => {
  const {addres, login, password} = serverParam;
  const url = `${addres}/Synergy/rest/api/storage/file/get?identifier=${id}`;
  return new Promise(async resolve => {
    try {
      const response = await fetch(url, {headers: {"Authorization": "Basic " + btoa(`${login}:${password}`)}});
      if(!response.ok) throw new Error(`HTTP: ${response.status}, message: ${response.statusText} [ ${await response.text()} ]`);
      resolve(await response.arrayBuffer());
    } catch (e) {
      console.log(`ERROR [ fileDownload ]: ${e.message}`);
      resolve(false);
    }
  });
}

const startUpload = async serverParam => {
  const {addres, login, password} = serverParam;
  return await kwApi.httpGetMethod({
    url: `${addres}/Synergy/rest/api/storage/start_upload`,
    name: 'startUpload',
    login, password
  });
}

const uploadPart = async (serverParam, filePath, data) => {
  //filePath - result api/storage/start_upload
  const {addres, login, password} = serverParam;

  return new Promise(async resolve => {
    try {
      $.ajax({
        url: `${addres}/Synergy/rest/api/storage/upload_part?file=${encodeURIComponent(filePath)}`,
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        headers: {
          "Authorization": "Basic " + btoa(`${login}:${password}`)
        }
      }).done(response => {
        resolve(response);
      }).fail((jqXHR, textStatus) => {
        console.log(`ERROR uploadPart`, textStatus, jqXHR);
        resolve(false);
      });
    } catch (e) {
      console.log(`ERROR [ uploadPart ]: ${e.message}`);
      resolve(false);
    }
  });
}

const addFileInForm = async (serverParam, asfDataId, fileName, filePath) => {
  const {addres, login, password} = serverParam;

  return new Promise(async resolve => {
    try {
      let url = `${addres}/Synergy/rest/api/storage/asffile/addFile`;
      url += `?dataUUID=${asfDataId}&fileName=${fileName}&filePath=${filePath}`;
      const method = 'POST';
      const headers = new Headers();
      headers.append("Authorization", "Basic " + btoa(`${login}:${password}`));
      headers.append("Content-Type", "application/json; charset=utf-8");
      const response = await fetch(url, {method, headers});
      if(!response.ok) throw new Error(`HTTP: ${response.status}, message: ${response.statusText} [ ${await response.text()} ]`);
      resolve(await response.json());
    } catch (e) {
      console.log(`ERROR [ addFileInForm ]: ${e.message}`, e);
      resolve(false);
    }
  });
}

this.kwApi = {
  getFetchBody: function(data, isDataString) {
    if(isDataString) return JSON.stringify(data);
    const urlencoded = new URLSearchParams();
    for(const key in data) urlencoded.append(key, data[key]);
    return urlencoded;
  },

  httpGetMethod: async function(param){
    const {url, login, password, name} = param;
    return new Promise(async resolve => {
      try {
        const response = await fetch(url, {headers: {"Authorization": "Basic " + btoa(`${login}:${password}`)}});
        if(!response.ok) throw new Error(`HTTP: ${response.status}, message: ${response.statusText} [ ${await response.text()} ]`);
        resolve(await response.json());
      } catch (e) {
        console.log(`ERROR [ ${name} ]: ${e.message}`);
        resolve(false);
      }
    });
  },

  httpPostMethod: async function(param){
    const {url, login, password, name, data, contentType, isDataString = true} = param;
    return new Promise(async resolve => {
      try {
        const method = 'POST';
        const headers = new Headers();
        const body = this.getFetchBody(data, isDataString);
        headers.append("Authorization", "Basic " + btoa(`${login}:${password}`));
        headers.append("Content-Type", contentType || "application/x-www-form-urlencoded; charset=utf-8");
        const response = await fetch(url, {method, headers, body});
        if(!response.ok) throw new Error(`HTTP: ${response.status}, message: ${response.statusText} [ ${await response.text()} ]`);
        resolve(await response.json());
      } catch (e) {
        console.log(`ERROR [ ${name} ]: ${e.message}`);
        resolve(false);
      }
    });
  },

  testServer: async function(serverParam) {
    const {addres, login, password} = serverParam;
    return await this.httpGetMethod({
      url: `${addres}/Synergy/rest/api/person/auth`,
      name: 'testServer',
      login, password
    });
  },

  createDoc: async function(serverParam, registryCode) {
    const {addres, login, password} = serverParam;
    return await this.httpGetMethod({
      url: `${addres}/Synergy/rest/api/registry/create_doc?registryCode=${registryCode}`,
      name: 'createDoc',
      login, password
    });
  },

  createDocRCC: async function(serverParam, registryCode, data, sendToActivation = false) {
    const {addres, login, password} = serverParam;
    return await this.httpPostMethod({
      url: `${addres}/Synergy/rest/api/registry/create_doc_rcc`,
      login, password,
      data: {registryCode, data, sendToActivation},
      contentType: 'application/json; charset=utf-8',
      name: 'createDocRCC'
    });
  },

  activateDoc: async function(serverParam, dataUUID) {
    const {addres, login, password} = serverParam;
    return await this.httpGetMethod({
      url: `${addres}/Synergy/rest/api/registry/activate_doc?dataUUID=${dataUUID}`,
      name: 'activateDoc',
      login, password
    });
  },

  getDepartmentInfo: async function(serverParam, departmentID) {
    const {addres, login, password} = serverParam;
    return await this.httpGetMethod({
      url: `${addres}/Synergy/rest/api/departments/get?departmentID=${departmentID}`,
      name: 'getDepartmentInfo',
      login, password
    });
  },

  getDepartmentsContent: async function(serverParam, departmentID) {
    const {addres, login, password} = serverParam;
    return await this.httpGetMethod({
      url: `${addres}/Synergy/rest/api/departments/content?departmentID=${departmentID}`,
      name: 'getDepartmentsContent',
      login, password
    });
  },

  searchPosition: async function(serverParam, pointer_code) {
    const {addres, login, password} = serverParam;
    return await this.httpGetMethod({
      url: `${addres}/Synergy/rest/api/positions/search?pointer_code=${pointer_code}`,
      name: 'searchPosition',
      login, password
    });
  },

  createPosition: async function(serverParam, positionData) {
    const {addres, login, password} = serverParam;
    return await this.httpPostMethod({
      url: `${addres}/Synergy/rest/api/positions/save?locale=ru`,
      login, password,
      data: positionData,
      isDataString: false,
      name: 'createPosition'
    });
  },

  positionsAppoint: async function(serverParam, positionID, userID) {
    const {addres, login, password} = serverParam;
    return await this.httpGetMethod({
      url: `${addres}/Synergy/rest/api/positions/appoint?positionID=${positionID}&userID=${userID}`,
      name: 'positionsAppoint',
      login, password
    });
  },

  getUserInfo: async function(serverParam, userID) {
    const {addres, login, password} = serverParam;
    return await this.httpGetMethod({
      url: `${addres}/Synergy/rest/api/filecabinet/user/${userID}`,
      name: 'getUserInfo',
      login, password
    });
  },

  searchUser: async function(serverParam, pointersCode) {
    const {addres, login, password} = serverParam;
    return await this.httpGetMethod({
      url: `${addres}/Synergy/rest/api/filecabinet/user/checkExistence?code=${pointersCode}`,
      name: 'searchUser',
      login, password
    });
  },

  createUser: async function(serverParam, userData) {
    const {addres, login, password} = serverParam;
    return await this.httpPostMethod({
      url: `${addres}/Synergy/rest/api/filecabinet/user/save`,
      login, password,
      data: userData,
      isDataString: false,
      name: 'createUser'
    });
  },

  addUserGroup: async function(serverParam, userID, groupCode) {
    const {addres, login, password} = serverParam;
    return await this.httpGetMethod({
      url: `${addres}/Synergy/rest/api/storage/groups/add_user?groupCode=${groupCode}&userID=${userID}`,
      name: 'addUserGroup',
      login, password
    });
  },

  searchInRegistry: async function(serverParam, urlParam) {
    const {addres, login, password} = serverParam;
    return await this.httpGetMethod({
      url: `${addres}/Synergy/rest/api/registry/data_ext?${urlParam}`,
      name: 'searchInRegistry',
      login, password
    });
  },

  countRegistryData: async function(serverParam, urlParam) {
    const {addres, login, password} = serverParam;
    return await this.httpGetMethod({
      url: `${addres}/Synergy/rest/api/registry/count_data?${urlParam}`,
      name: 'countRegistryData',
      login, password
    });
  },

  loadAsfData: async function(serverParam, dataUUID) {
    const {addres, login, password} = serverParam;
    return await this.httpGetMethod({
      url: `${addres}/Synergy/rest/api/asforms/data/${dataUUID}`,
      name: 'loadAsfData',
      login, password
    });
  },

  mergeFormData: async function(serverParam, uuid, data) {
    const {addres, login, password} = serverParam;
    return await this.httpPostMethod({
      url: `${addres}/Synergy/rest/api/asforms/data/merge`,
      login, password,
      data: {uuid, data},
      contentType: 'application/json; charset=utf-8',
      name: 'mergeFormData'
    });
  },

  getAsfDataUUID: async function(serverParam, documentID) {
    const {addres, login, password} = serverParam;
    return await this.httpGetMethod({
      url: `${addres}/Synergy/rest/api/formPlayer/getAsfDataUUID?documentID=${documentID}`,
      name: 'getAsfDataUUID',
      login, password
    });
  },

  uploadFile: async function(id, fileName, asfDataId) {
    const {servers} = Cons.getAppStore();
    const {sourceServer, receiverServer} = servers;

    return new Promise(async resolve => {
      try {
        const sourceFile = await fileDownload(sourceServer, id);
        if(!sourceFile) throw new Error('Не удалось скачать файл с сервера источника');

        const filePath = await startUpload(receiverServer);
        if(!filePath) throw new Error('Ошибка создания временного файла на сервере приемнике');

        const b64encoded = btoa(new Uint8Array(sourceFile).reduce((data, byte) => data + String.fromCharCode(byte), ''));
        const data = new FormData();
        data.append('body', b64encoded);

        const uploadResult = await uploadPart(receiverServer, filePath.file, data);
        if(!uploadResult) throw new Error('Ошибка загрузки файла на сервер приемник');

        const addFile = await addFileInForm(receiverServer, asfDataId, fileName, filePath.file);
        if(!addFile) throw new Error('Ошибка прикрипления файла к форме на сервере приемнике');

        resolve(addFile);
      } catch (e) {
        console.log(`ERROR uploadFile: ${e.message}`, e);
        resolve(false);
      }
    });
  }
}
