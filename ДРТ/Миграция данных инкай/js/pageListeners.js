const eventShow = {type: 'set_hidden', hidden: false};
const eventHide = {type: 'set_hidden', hidden: true};

const inputs = [
  'sourceServerAddres',
  'sourceServerLogin',
  'sourceServerPassword',
  'receiverServerAddres',
  'receiverServerLogin',
  'receiverServerPassword'
];

const parseServerAddres = addres => addres.substr(-1) == '/' ? addres.substr(0, addres.length - 1) : addres;

const fillFields = () => {
  const {servers} = Cons.getAppStore();
  if(!servers) return;

  const {sourceServer, receiverServer} = servers;

  if(sourceServer, receiverServer) {
    $('#sourceServerAddres').val(sourceServer.addres);
    $('#sourceServerLogin').val(sourceServer.login);
    $('#sourceServerPassword').val(sourceServer.password);

    $('#receiverServerAddres').val(receiverServer.addres);
    $('#receiverServerLogin').val(receiverServer.login);
    $('#receiverServerPassword').val(receiverServer.password);
  }
}

const initPagesListiners = async () => {
  const scrollIntervalID = Cons.getAppStore();
  if(scrollIntervalID) clearInterval(scrollIntervalID);

  fillFields();

  $('#buttonTestServers').off().on('click', async e => {
    e.preventDefault();
    e.target.blur();

    const servers = {
      sourceServer: {
        addres: parseServerAddres($('#sourceServerAddres').val()),
        login: $('#sourceServerLogin').val(),
        password: $('#sourceServerPassword').val()
      },
      receiverServer: {
        addres: parseServerAddres($('#receiverServerAddres').val()),
        login: $('#receiverServerLogin').val(),
        password: $('#receiverServerPassword').val()
      }
    }

    const test = await _kwMigration.testServers(servers);

    fire(eventShow, 'sourceServerStatusLabel');
    fire(eventShow, 'receiverServerStatusLabel');

    if(!test.sourceServer) {
      servers.sourceServer.isAvailable = false;
      $('#sourceServerStatusLabel span').text('(не доступен)').css('color', 'red');
    } else {
      servers.sourceServer.isAvailable = true;
      servers.sourceServer.user = test.sourceServer;
      $('#sourceServerStatusLabel span').text('(доступен)').css('color', 'green');
    }

    if(!test.receiverServer) {
      servers.receiverServer.isAvailable = false;
      $('#receiverServerStatusLabel span').text('(не доступен)').css('color', 'red');
    } else {
      servers.receiverServer.isAvailable = true;
      servers.receiverServer.user = test.receiverServer;
      $('#receiverServerStatusLabel span').text('(доступен)').css('color', 'green');
    }

    if(test.receiverServer && test.sourceServer) {
      fire(eventHide, 'buttonTestServers');
      fire(eventShow, 'buttonStartMigration');
      fire(eventShow, 'buttonEditSettings');
      inputs.forEach(id => fire({type: 'set_disabled', disabled: true}, id));
    }

    Cons.setAppStore({servers: servers});
  });

  $('#buttonEditSettings').off().on('click', e => {
    e.preventDefault();
    e.target.blur();

    fire(eventShow, 'buttonTestServers');
    fire(eventHide, 'buttonStartMigration');
    fire(eventHide, 'buttonEditSettings');
    fire(eventHide, 'sourceServerStatusLabel');
    fire(eventHide, 'receiverServerStatusLabel');
    inputs.forEach(id => {
      fire({type: 'set_disabled', disabled: false}, id);
    });

    Cons.setAppStore({servers: null});
  });

  $('#buttonStopMigration').off().on('click', async e => {
    e.preventDefault();
    e.target.blur();
    Cons.setAppStore({isStopMigration: true});
  });

  $('#buttonSaveLog').off().on('click', e => {
    e.preventDefault();
    e.target.blur();
    UTILS.saveLog();
  });
}

pageHandler('main_page', async () => {

  initPagesListiners();

  $('#buttonStartMigration').off().on('click', async e => {
    e.preventDefault();
    e.target.blur();

    $('#panelLog').empty();

    fire({type: 'set_disabled', disabled: true}, 'buttonStartMigration');
    fire(eventHide, 'buttonEditSettings');
    fire(eventShow, 'buttonStopMigration');

    const scrollIntervalID = setInterval(() => {
      const panelLog = document.getElementById('panelLog');
      panelLog.scrollTop = panelLog.scrollHeight;
    }, 500);

    Cons.setAppStore({scrollIntervalID: scrollIntervalID});

    Cons.setAppStore({isStopMigration: false});

    const resultMigration = await _kwMigration.start();

    UTILS.setLog(`Миграция завершена с результатом: <b>${resultMigration}</b>`, 'finish');

    clearInterval(scrollIntervalID);
    Cons.setAppStore({scrollIntervalID: null});

    fire({type: 'set_disabled', disabled: false}, 'buttonStartMigration');
    fire(eventShow, 'buttonEditSettings');
    fire(eventHide, 'buttonStopMigration');
  });

});


pageHandler('users_page', async () => {

  initPagesListiners();

  $('#buttonStartMigration').off().on('click', async e => {
    e.preventDefault();
    e.target.blur();

    $('#panelLog').empty();

    fire({type: 'set_disabled', disabled: true}, 'buttonStartMigration');
    fire(eventHide, 'buttonEditSettings');
    fire(eventShow, 'buttonStopMigration');

    const scrollIntervalID = setInterval(() => {
      const panelLog = document.getElementById('panelLog');
      panelLog.scrollTop = panelLog.scrollHeight;
    }, 500);

    Cons.setAppStore({scrollIntervalID: scrollIntervalID});

    Cons.setAppStore({isStopMigration: false});

    const resultMigration = await _kwMigrationUsers.start();

    UTILS.setLog(`Миграция завершена с результатом: <b>${resultMigration}</b>`, 'finish');

    clearInterval(scrollIntervalID);
    Cons.setAppStore({scrollIntervalID: null});

    fire({type: 'set_disabled', disabled: false}, 'buttonStartMigration');
    fire(eventShow, 'buttonEditSettings');
    fire(eventHide, 'buttonStopMigration');
  });

});
