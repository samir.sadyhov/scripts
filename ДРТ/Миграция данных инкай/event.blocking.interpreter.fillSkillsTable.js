var result = true;
var message = "ok";

const cmp = {
  name: 'kw_form_userCard_competenceName',
  status: 'kw_form_userCard_status',
  finishDate: 'kw_form_userCard_finishDate',
  realFinishDate: 'kw_form_userCard_real_finishDate',
  aviableAttempts: 'kw_form_userCard_aviableAttempts',
  table: 'kw_form_userCard_competenceTable',
  tableOther: 'kw_form_userCard_competenceTable_other'
}

function getPositionCompetence(asfData) {
  let result = null;
  let data = UTILS.getValue(asfData, 'kw_form_position_competence_name');
  if(data && data.hasOwnProperty('key')) {
    result = [];
    let keys = data.key.split(';');
    let values = data.value.split(';');
    keys.forEach(function(key, i){
      result.push({key: key, value: values[i]});
    });
  }
  return result;
}

function parseCompetenceTable(table, other){
  let result = [];
  if(table.hasOwnProperty('data')){
    let tableLength = UTILS.getTableBlockIndex(table, cmp.name + (other ? '_other' : ''));
    for(let i = 1; i < tableLength; i++) {
      if(UTILS.getValue(table, cmp.name + (other ? '_other-b' : '-b') + i))
      result.push({
        name: UTILS.getValue(table, cmp.name + (other ? '_other-b' : '-b') + i),
        status: UTILS.getValue(table, cmp.status + (other ? '_other-b' : '-b') + i),
        finishDate: UTILS.getValue(table, cmp.finishDate + (other ? '_other-b' : '-b') + i),
        realFinishDate: UTILS.getValue(table, cmp.realFinishDate + (other ? '_other-b' : '-b') + i),
        aviableAttempts: UTILS.getValue(table, cmp.aviableAttempts + (other ? '_other-b' : '-b') + i)
      });
    }
  }
  return result;
}

function getUserCompetence(asfData) {
  let skills = parseCompetenceTable(UTILS.getValue(asfData, cmp.table));
  let otherSkills = parseCompetenceTable(UTILS.getValue(asfData, cmp.tableOther), true);
  return skills.concat(otherSkills);
}

function getResultTable(positionCompetence, userCompetence) {
  let table = {id: cmp.table, type: 'appendable_table', data: []};
  let tableOther = {id: cmp.tableOther, type: 'appendable_table', data: []};
  let otherCompetence = [];
  let tbi = 1;

  userCompetence.forEach(function(item){
    let pos = positionCompetence.filter(function(x){if(x.key == item.name.key) return x});
    if(!pos.length) otherCompetence.push(item);
  });

  positionCompetence.forEach(function(item){
    let competence = userCompetence.filter(function(x){if(x.name.key == item.key) return x});
    if(competence && competence.length) {
      competence = competence[0];
      UTILS.setValue(table, cmp.name + '-b' + tbi, competence.name);
      UTILS.setValue(table, cmp.status + '-b' + tbi, competence.status);
      UTILS.setValue(table, cmp.finishDate + '-b' + tbi, competence.finishDate);
      UTILS.setValue(table, cmp.realFinishDate + '-b' + tbi, competence.realFinishDate);
      UTILS.setValue(table, cmp.aviableAttempts + '-b' + tbi, competence.aviableAttempts);
    } else {
      UTILS.setValue(table, cmp.name + '-b' + tbi, {
        type: 'reglink',
        value: item.value,
        key: item.key,
        valueID: item.key
      });
      UTILS.setValue(table, cmp.status + '-b' + tbi, {type: 'listbox', value: "Без статуса", key: "0"});
      UTILS.setValue(table, cmp.finishDate + '-b' + tbi, {type: 'date'});
      UTILS.setValue(table, cmp.realFinishDate + '-b' + tbi, {type: 'date'});
      UTILS.setValue(table, cmp.aviableAttempts + '-b' + tbi, {
        type: 'numericinput',
        value: '0',
        key: '0'
      });
    }
    tbi++;
  });

  tbi=1;
  otherCompetence.forEach(function(item){
    if(item.result.hasOwnProperty('key')) {
      UTILS.setValue(tableOther, cmp.name + '_other-b' + tbi, item.name);
      UTILS.setValue(tableOther, cmp.status + '_other-b' + tbi, item.status);
      UTILS.setValue(tableOther, cmp.finishDate + '_other-b' + tbi, item.finishDate);
      UTILS.setValue(tableOther, cmp.realFinishDate + '_other-b' + tbi, item.realFinishDate);
      UTILS.setValue(tableOther, cmp.aviableAttempts + '_other-b' + tbi, item.aviableAttempts);
      tbi++;
    }
  });

  return {competenceTable: table, otherCompetenceTable: tableOther};
}

try {
  let currentFormData = API.getFormData(dataUUID);
  let profile = UTILS.getValue(currentFormData, 'kw_form_userCard_reglink');

  if(!profile || !profile.hasOwnProperty('key')) throw new Error('не выбран профиль обучения');

  let profileAsfData = API.getFormData(API.getAsfDataId(profile.key));

  let positionCompetence = getPositionCompetence(profileAsfData);
  if(!positionCompetence) throw new Error('не выбраны умения');

  let tableBooks = UTILS.getValue(profileAsfData, 'kw_form_position_books');
  tableBooks.id = 'kw_form_userCard_books';

  let userCompetence = getUserCompetence(currentFormData);
  let tables = getResultTable(positionCompetence, userCompetence);
  let allCourse = UTILS.getTableBlockIndex(tables.competenceTable, cmp.name) - 1;

  API.mergeFormData({
    uuid: dataUUID,
    data: [
      tables.competenceTable,
      tables.otherCompetenceTable,
      tableBooks,
      {
        id: 'kw_form_userCard_all',
        type: 'numericinput',
        key: String(allCourse),
        value: String(allCourse)
      }
    ]
  });

} catch (err) {
  log.error(err.message);
  message = err.message;
}
