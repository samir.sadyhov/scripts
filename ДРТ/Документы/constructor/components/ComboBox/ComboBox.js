this.ComboBox = class {
  constructor(
    values = [],
    inputClass = "",
    buttonClass = ""
  ) {
    this.values = values;
    this.inputClass = `asf-dropdown-input ${inputClass}`;
    this.buttonClass = `asf-dropdown-button ${buttonClass}`;

    this.init();
  }

  get container() {
    return this.compContainer;
  }

  get value() {
    return this.selectedValue;
  }

  set value(value) {
    this.setSelectedValue(value);
  }

  set disabled(disabled){
    this.dropdownBox.attr('disabled', disabled);
    this.button.attr('disabled', disabled);
  }

  setSelectedValue(newSelectedValue) {
    this.initialSelect = newSelectedValue;
    this.selectedValue = null;
    this.dropdownBox.val("");
    this.values.forEach(value => {
      value.selected = value.value === newSelectedValue;
      if(value.value === newSelectedValue) {
        this.dropdownBox.val(value.title);
        this.dropdownBox.attr('title', value.title);
        this.selectedValue = newSelectedValue;
      }
    });
  }

  showDropDown(filterValue){
    if (AS.SERVICES.isShownDropDown(this.compContainer)) {
      AS.SERVICES.closeDropDown(this.compContainer);
      return;
    }

    const values = this.values.filter(x => x.title.indexOf(filterValue) > -1);

    AS.SERVICES.showDropDown(values, this.compContainer, null, selectedValue => {
      this.setSelectedValue(selectedValue);
      this.compContainer.trigger({
        type: 'selected',
        eventParam: selectedValue
      });
    });
  }

  addListener(){
    this.dropdownBox.on("input", event => {
      this.showDropDown(this.dropdownBox.val());
    });

    this.dropdownBox.on("blur", event => {
      this.setSelectedValue(this.initialSelect);
    });

    this.button.on('click', event => {
      event.stopPropagation();
      event.stopImmediatePropagation();
      event.preventDefault();
      this.showDropDown("", event);
    });

    this.dropdownBox.on('click', event => {
      event.stopPropagation();
      event.stopImmediatePropagation();
      event.preventDefault();
      this.showDropDown("", event);
    });
  }

  render(){
    this.compContainer = $('<div>', {class: "asf-InlineBlock", style: "vertical-align:top"});
    this.dropdownBox =  $('<input>', {type : 'text', class : this.inputClass});
    this.button = $("<button>", {class: this.buttonClass});

    this.dropdownBox.prop("readonly", true);

    this.compContainer.append(this.dropdownBox);
    this.compContainer.append(this.button);

    this.setSelectedValue(this.initialSelect);
  }

  init(){
    this.selectedValue = null;
    this.initialSelect = null;

    if(this.values.length) this.initialSelect = this.values[0].value;

    this.render();
    this.addListener();
  }
}
