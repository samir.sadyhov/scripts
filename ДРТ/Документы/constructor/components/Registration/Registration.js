const getDoctypes = registerID => {
  return new Promise(resolve => {
    rest.synergyGet(`api/docflow/doctypes?locale=${AS.OPTIONS.locale}&description=full&registerID=${registerID}`, resolve, err => {
      console.log(`ERROR [ getDoctypes ]: ${JSON.stringify(err)}`);
      resolve(null);
    });
  });
}

const getDocFileInfo = docFileID => {
  return new Promise(resolve => {
    rest.synergyGet(`api/docflow/docFileInfo?fileID=${docFileID}&locale=${AS.OPTIONS.locale}`, resolve, err => {
      console.log(`ERROR [ getDocFileInfo ]: ${JSON.stringify(err)}`);
      resolve(null);
    });
  });
}

const saveDocument = async body => {
  return new Promise(async resolve => {
    try {
      const {login, password} = Cons.creds;
      const auth = "Basic " + btoa(unescape(encodeURIComponent(`${login}:${password}`)));
      const response = await fetch(`../Synergy/rest/api/docflow/doc/chancellery/save`, {
        method: 'POST',
        headers: {"Authorization": auth, "Content-Type": "application/json; charset=UTF-8"},
        body: JSON.stringify(body)
      });
      resolve(response.json());
    } catch (err) {
      console.log(`ERROR [ saveDocument ]: ${err.message}`);
      resolve(null);
    }
  });
}

const rejectDocument = async (documentID, comment) => {
  return new Promise(async resolve => {
    try {
      const {login, password} = Cons.creds;
      const auth = "Basic " + btoa(unescape(encodeURIComponent(`${login}:${password}`)));
      const urlencoded = new URLSearchParams();
      urlencoded.append("documentID", documentID);
      urlencoded.append("comment", comment);

      const response = await fetch(`../Synergy/rest/api/docflow/doc/reject`, {
        method: 'POST',
        headers: {"Authorization": auth, "Content-Type": "application/x-www-form-urlencoded; charset=UTF-8"},
        body: urlencoded
      });

      resolve(response.json());
    } catch (err) {
      console.log(`ERROR [ rejectDocument ]: ${err.message}`);
      resolve(null);
    }
  });
}

const documentRegistration = async documentID => {
  return new Promise(async resolve => {
    try {
      const {login, password} = Cons.creds;
      const auth = "Basic " + btoa(unescape(encodeURIComponent(`${login}:${password}`)));
      const response = await fetch(`../Synergy/rest/api/docflow/documentRegistration/${documentID}`, {
        method: 'POST',
        headers: {"Authorization": auth, "Content-Type": "application/json"}
      });
      if(!response.ok) throw new Error(`HTTP: ${response.status}, message: ${response.statusText} [ ${response.text()} ]`);
      resolve(response.json());
    } catch (err) {
      console.log(`ERROR [ documentRegistration ]: ${err.message}`);
      resolve(null);
    }
  });
}

/* права на журнал
{
  "canRead": true, - чтение
  "canWrite": true, - запись
  "canRegister": true, - регистрация
  "canRefuse": true, - отклонение
  "canEdit": true, - редактирование
  "canReserve": true, - резервирование
  "canEditRegisteredCard": true, - Редактирование РКК (зарег)
  "canEditRegisteredInbox": true, - Редактирование файлов (зарег)
  "canEditRegisteredRoute": true - Изменение маршрута (зарег)
}
*/

const rccFields = {
  1: {title: 'Кр. содерж.', code: 'subject', required: true},
  2: {title: 'Тип документа', code: 'doc_type', required: true},
  3: {title: 'Номер', code: 'number', required: false},
  4: {title: 'Дата регистрации', code: 'reg_date', required: true},
  5: {title: 'Длительность/Завершение', code: 'duration', required: true},
  6: {title: 'Корреспондент (орг)', code: 'correspondent_org', required: false},
  7: {title: 'Корреспондент', code: 'correspondent', required: false},
  8: {title: 'Автор', code: 'author', required: false},
  9: {title: 'Зарегистрировал', code: 'reg_user', required: false},
  10: {title: 'Является контрольным', code: 'control', required: false},
  11: {title: 'Номер исх.', code: 'base_number', required: false},
  12: {title: 'Дата исх.', code: 'base_date', required: false},
  13: {title: 'Списать в', code: 'doc_file', required: false},
  14: {title: 'Основание', code: 'base', required: false},

  in_docs: [1, 3, 4, 2, 5, 6, 7, 9, 10, 11, 12, 14, 13],
  out_docs: [1, 2, 3, 4, 6, 7, 8, 9, 10, 14, 13],
  internal_docs: [1, 3, 4, 2, 5, 8, 9, 10, 14, 13],

  get: function(docTypeInfo) {
    if(!docTypeInfo) return [this['1'], this['2'], this['4'], this['5']];
    const {baseRegisterID, fields} = docTypeInfo;
    if(fields && fields.length) {
      return fields.sort((a,b) => a.number - b.number).map(x => {
        if(x.number != 0 && x.selected) return this[x.number];
      }).filter(x => x);
    } else {
      return this[baseRegisterID].map(x => this[x]);
    }
  }
}

const convertToBase64 = async file => {
  return new Promise(resolve => {
    const reader = new FileReader();
    reader.onload = () => resolve(btoa(new Uint8Array(reader.result).reduce((data, byte) => data + String.fromCharCode(byte), '')));
    reader.readAsArrayBuffer(file);
  });
}

this.Registration = class {
  constructor(isNew = false, registerInfo, rccData){
    this.isNew = isNew;
    this.registerInfo = registerInfo;
    this.rccData = rccData;
    this.signList = null;

    this.files = [];
    this.fileIndex = 0;

    this.attachments = [];
    this.work_files = [];

    this.dataToSave = {};

    this.isChanges = false;

    this.isReservedNumber = false;
    this.reservedID = null;

    this.init();
  }

  checkRCCData(){
    const result = {errorCode: 0, errorMessage: ''};
    const errors = new Set();
    const fields = rccFields.get(this.docTypeInfo).filter(x => x.required);

    for(let i = 0; i < fields.length; i++) {
      const {code, title} = fields[i];
      if(code == 'control') continue;
      if(code == 'duration' && this.docTypeInfo.input_method_term == "DURATION") {
        if(this.dataToSave[code] == 0) {
          errors.add(i18n.tr(`${title} не может быть 0`));
          result.errorCode++;
        }
      } else if(!this.dataToSave[code] || this.dataToSave[code] == "") {
        errors.add(i18n.tr(`Не заполнено поле ${title}`));
        result.errorCode++;
      }
    }

    if(this.formPlayer && !this.formPlayer.model.isValid()) {
      errors.add(i18n.tr(`Проверьте правильность заполнения дополнительных полей карточки`));
      result.errorCode++;
    }

    if(result.errorCode != 0) result.errorMessage = [...errors].join('<br>');

    return result;
  }

  getSaveBody(){
    const body = {};

    if(this.selectItem && this.selectItem.hasOwnProperty('documentID')) {
      body.documentID = this.selectItem.documentID;
    }

    for(const code in this.dataToSave) {
      switch (code) {
        case 'control': body.control = this.dataToSave.control; break;
        case 'doc_file': {
          if(this.dataToSave[code] && this.dataToSave[code] != "") {
            const {doc_file, doc_file_code} = this.dataToSave[code];
            if(doc_file) body['doc_file'] = doc_file;
            if(doc_file_code) body['doc_file_code'] = doc_file_code;
          }
          break;
        }
        case 'duration': {
          if(this.docTypeInfo.input_method_term == "FINISH_DATE") {
            body['finish_date'] = this.dataToSave[code];
          } else {
            body['duration'] = Number(this.dataToSave[code]);
          }
          break;
        }
        default: if(this.dataToSave[code] != "") body[code] = this.dataToSave[code];
      }
    }

    if(!body.hasOwnProperty('duration')) body.duration = 1;

    if(this.reservedID) body['reservedID'] = this.reservedID;

    let formula = this.registerInfo.register.formula;
    body['numberTemplate'] = this.registerInfo.register.formula;
    if(this.isReservedNumber) formula = this.isReservedNumber;

    if(formula.includes('*')) {
      const inputChars = [];

      $('#docNumberContainer input').each((i, el) => {
        inputChars.push($(el).val());
      });

      body['number'] = formula.split('*').map((x, i) => {
        if(inputChars[i]) {
          return x + inputChars[i];
        } else {
          return x;
        }
      }).join('');
    } else {
      body['number'] = formula;
    }

    if(this.isReservedNumber) body['numberTemplate'] = body['number'];

    if(this.formPlayer) {
      body.formCode = this.formPlayer.model.formCode;
      body.data = this.formPlayer.model.getAsfData().data.filter(x => x.type != 'label');
    }

    return body;
  }

  async loadFilesToDoc(documentID){
    let path = "ase:attachmentContainer";
    if(this.selectItem && this.selectItem.isRegistyDocuments == "true") path = "ase:workContainer";

    for(let i = 0; i < this.files.length; i++) {
      const {file} = this.files[i];

      try {
        const b64encoded = await convertToBase64(file);
        const formData = new FormData();
        formData.append('body', b64encoded);

        const filePath = await appAPI.startUpload();
        if(filePath.errorCode != 0) throw new Error(filePath.errorMessage);

        const uploadResult = await appAPI.uploadPart(filePath.file, formData);
        if(uploadResult.errorCode != 0) throw new Error(uploadResult.errorMessage);

        const addFileResult = await appAPI.addFileToDocument(documentID, file.name, filePath.file, path);
        if(addFileResult.errorCode != 0) throw new Error(addFileResult.errorMessage);
      } catch (e) {
        console.log('ERROR load file', e.message);
      }
    }
  }

  addButtonListener(){

    // ОТКЛОНЕНИЕ
    if(!this.isNew && this.rights.canRefuse && this.buttonReject) {
      this.buttonReject.on('click', async e => {
        const dialogReject = $('<div>');
        const buttonContainer = $('<div>');
        const rejectButton = $('<button>', {class: "uk-button uk-button-small uk-button-primary", style: "border-radius: 3px;"});
        const buttonCancel = $('<button>', {class: "uk-button uk-button-small uk-button-default", style: "border-radius: 3px;"});
        const rejectComment = $('<textarea>', {class: "uk-textarea", style: "height: calc(100% - 50px); resize: none;"});

        rejectButton.text(i18n.tr("Отклонить"));
        buttonCancel.text(i18n.tr("Отмена"));

        dialogReject.append(rejectComment, buttonContainer);

        buttonContainer.append(rejectButton, buttonCancel);

        buttonContainer.css({
          'display': 'flex',
          'gap': '10px',
          'flex-direction': 'row',
          'justify-content': 'end',
          'align-items': 'center',
          'border-top': '1px solid #e5e5e5',
          'height': '50px'
        });

        rejectComment.on('input', e => {
          rejectComment.css({
            "background": "#fff",
            "border-color": "#e5e5e5"
          });
        });

        rejectButton.on('click', e => {
          e.preventDefault();
          e.target.blur();
          if(rejectComment.val() == "") {
            rejectComment.css({
              "background": "rgba(255, 0, 0, 0.1)",
              "border-color": "rgba(255, 0, 0, 0.5)"
            });
          } else {
            Cons.showLoader();
            rejectDocument(this.selectItem.documentID, rejectComment.val()).then(rejectResult => {
              Cons.hideLoader();
              if(rejectResult.errorCode != 0) {
                showMessage(rejectResult.errorMessage, 'error');
              } else {
                showMessage(i18n.tr('Регистрация отклонена'), 'success');
                fire({type: 'updateDocumentList'}, 'customDocumentList');
                dialogReject.dialog("close");
                this.dialog.dialog("close");
              }
            });
          }
        });

        buttonCancel.on('click', e => {
          e.preventDefault();
          e.target.blur();
          dialogReject.dialog("close");
        });

        dialogReject.dialog({
          modal: true,
          width: 400,
          height: 300,
          title: i18n.tr('Причина отказа'),
          show: {effect: 'fade', duration: 300},
          hide: {effect: 'fade', duration: 300},
          close: function() {
            $(this).remove();
          }
        });
      });
    }

    // СОХРАНЕНИЕ
    if(this.buttonSave && this.rights.canWrite) {
      this.buttonSave.on('click', async e => {
        if(!this.isChanges && (this.formPlayer && !this.formPlayer.model.hasChanges)) {
          showMessage(i18n.tr('Нет изменений для сохранения'), 'info');
          return;
        }

        Cons.showLoader();
        try {
          const body = this.getSaveBody();
          const resultSave = await saveDocument(body);

          if(!resultSave || resultSave.errorCode != 0) throw new Error(resultSave.errorMessage);

          if(this.files) await this.loadFilesToDoc(resultSave.documentID);

          this.dialog.dialog("close");
          showMessage(i18n.tr('Документ сохранен'), 'success');
          fire({type: 'updateDocumentList'}, 'customDocumentList');

          Cons.hideLoader();
        } catch (err) {
          UIkit.notification.closeAll();
          showMessage(i18n.tr(err.message), 'error');
          Cons.hideLoader();
        }
      });
    }


    //РЕГИСТРАЦИЯ
    if(this.rights.canRegister && this.buttonRegister) {
      this.buttonRegister.on('click', async e => {

        try {
          const rccErrors = this.checkRCCData();
          if(rccErrors.errorCode != 0) throw new Error(rccErrors.errorMessage);

          const regDoc = async () => {
            try {
              Cons.showLoader();

              let documentID;

              if(this.selectItem) documentID = this.selectItem.documentID;

              if(this.isChanges) {
                const body = this.getSaveBody();
                const resultSave = await saveDocument(body);

                if(!resultSave || resultSave.errorCode != 0) throw new Error(resultSave.errorMessage);

                if(this.files) await this.loadFilesToDoc(resultSave.documentID);

                documentID = resultSave.documentID;
              }

              const resultReg = await documentRegistration(documentID);
              if(!resultReg || (resultReg.hasOwnProperty('errorCode') && resultReg.errorCode != 0)) throw new Error('Ошибка регистрации');

              this.dialog.dialog("close");
              showMessage(i18n.tr('Документ зарегистрирован'), 'success');
              fire({type: 'updateDocumentList'}, 'customDocumentList');

              Cons.hideLoader();
            } catch (err) {
              UIkit.notification.closeAll();
              showMessage(i18n.tr(err.message), 'error');
              Cons.hideLoader();
            }
          }

          const docFiles = [...this.files, ...this.attachments, ...this.work_files];
          if(!docFiles.length) {
            if(confirm(i18n.tr('Вы не приложили ни одного файла. Продолжить регистрацию?'))) regDoc();
          } else {
            regDoc();
          }

        } catch (err) {
          UIkit.notification.closeAll();
          showMessage(i18n.tr(err.message), 'error');
          Cons.hideLoader();
        }
      });
    }
  }

  renderRccComponents(field, container, panelContent){
    const {title, code, required} = field;

    if(!this.dataToSave.hasOwnProperty(code)) this.dataToSave[code] = '';

    const getTable = (id, col, row) => {
      let table = container.find(`#${id}`);
      if(table.length) return table;

      table = $('<table>', {class: 'rcc_table_block', id: id});
      const tbody = $('<tbody>', {id: `tbody-${id}`});

      for(let i = 0; i < row; i++) {
        const tr = $('<tr>');
        for(let j = 0; j < col; j++) tr.append('<td>');
        tbody.append(tr);
      }

      table.append(tbody);
      container.append(table);
      return table;
    }

    switch (code) {
      case 'subject': {
        const {container: docNameContainer, textarea: docNameInput} = Components.createInputTextArea(i18n.tr(title));
        docNameContainer.addClass('rcc_field');

        if(this.dataToSave.hasOwnProperty(code) && this.dataToSave[code] != '') {
          docNameInput.val(this.dataToSave[code]);
        } else if(this.rccData) {
          docNameInput.val(this.rccData.content);
          this.dataToSave[code] = this.rccData.content;
        }

        if(required) docNameContainer.addClass('required');

        docNameInput.on('change', e => {
          this.dataToSave[code] = docNameInput.val();
          this.isChanges = true;
        });

        container.append(docNameContainer);
        break;
      }

      case 'doc_type': {
        const tableBlock1 = getTable('tableBlock1', 2, 2);
        const td = tableBlock1.find('tbody#tbody-tableBlock1>tr:nth-child(2)>td:nth-child(1)');

        const docTypeItems = this.doctypes.map(x => ({label: x.name, value: x.code}));
        const {container: docTypeContainer, select: docTypeSelect} = Components.createSelectComponent(i18n.tr(title), docTypeItems);
        docTypeContainer.addClass('rcc_field');

        if(this.dataToSave.hasOwnProperty(code) && this.dataToSave[code] != '') {
          docTypeSelect.val(this.dataToSave[code]);
        } else if(this.rccData) {
          docTypeSelect.val(this.rccData.docTypeCode);
          this.dataToSave[code] = this.rccData.docTypeCode;
        } else if(this.docTypeInfo) {
          docTypeSelect.val(this.docTypeInfo.code);
          this.dataToSave[code] = this.docTypeInfo.code;
        }

        if(required) docTypeContainer.addClass('required');

        docTypeSelect.on('change', e => {
          this.dataToSave[code] = docTypeSelect.val();
          this.isChanges = true;
          const docTypeInfo = this.doctypes.find(x => x.code == this.dataToSave[code]);
          this.renderRccData(panelContent, docTypeInfo);
        });

        td.append(docTypeContainer);
        break;
      }

      case 'number': {
        const tableBlock1 = getTable('tableBlock1', 2, 2);
        const td = tableBlock1.find('tbody#tbody-tableBlock1>tr:nth-child(1)>td:nth-child(1)');

        const docNumberContainer = $('<div>', {class: 'uk-margin-small rcc_field'});
        const fc = $('<div>', {class: 'uk-form-controls'});
        const selectButton = $('<a class="uk-form-icon uk-form-icon-flip" href="#" uk-icon="icon: more"></a>');
        const resultBlock = $('<div>', {id: 'docNumberContainer', class: 'base_documents_container'});

        if(required) docNumberContainer.addClass('required');

        fc.append($('<div class="uk-inline uk-width-expand">').append(selectButton, resultBlock));
        docNumberContainer.append(`<label class="uk-form-label uk-text-bold">${i18n.tr(title)}</label>`).append(fc);

        const renderDataToFormula = formula => {

          if(formula.includes('*')) {
            const chars = formula.split('');
            const ftable = $('<table>', {style: "border-spacing: 0;"});
            const fbody = $('<tbody>');
            const ftr = $('<tr>');

            for(let i = 0; i < chars.length; i++) {
              const chr = chars[i];
              const ftd = $('<td>');

              if(chr == '*') {
                const chrInput = $('<input type="text" maxlength="1" style="border: 1px solid #c4c4c4; width: 15px;">');
                ftd.append(chrInput);

                chrInput.on('change', e => {
                  this.isChanges = true;
                });
              } else {
                ftd.text(chr);
                ftd.css('white-space', 'nowrap');
              }

              ftr.append(ftd);
            }

            ftable.append(fbody);
            fbody.append(ftr);
            resultBlock.empty().append(ftable);

          } else {
            resultBlock.empty().append(`<span>${formula}</span>`);
            this.dataToSave[code] = formula;
          }
        }

        if(this.isNew) {
          renderDataToFormula(this.registerInfo.register.formula);
        } else if (this.selectItem.number != "") {
          if(this.selectItem.registered == "true") {
            resultBlock.empty().append(`<span>${this.selectItem.number}</span>`);
            selectButton.css('pointer-events', 'none');
          } else if (this.registerInfo.register.formula.includes('*')) {
            renderDataToFormula(this.registerInfo.register.formula);
          } else {
            resultBlock.empty().append(`<span>${this.selectItem.number}</span>`);
          }
        } else if(this.rccData) {
          if(this.rccData.registered) {
            resultBlock.empty().append(`<span>${this.rccData.number}</span>`);
            selectButton.css('pointer-events', 'none');
          } else if (this.rccData.userMask) {
            resultBlock.empty().append(`<span>${this.rccData.userMask}</span>`);
          } else {
            renderDataToFormula(this.registerInfo.register.formula);
          }
        }

        let selectNumber;
        selectButton.on('click', e => {
          e.preventDefault();
          reservedNumber(this.registerInfo, selectNumber, (result, reservedID) => {
            selectNumber = result;
            if(reservedID) this.reservedID = reservedID;
            if(!result || result == 'AUTO') {
              renderDataToFormula(this.registerInfo.register.formula);
              this.isReservedNumber = false;
              this.reservedID = null;
            } else {
              renderDataToFormula(result);
              this.isReservedNumber = result;
            }
          });
        });

        td.append(docNumberContainer);
        break;
      }

      case 'reg_date': {
        const tableBlock1 = getTable('tableBlock1', 2, 2);
        const td = tableBlock1.find('tbody#tbody-tableBlock1>tr:nth-child(1)>td:nth-child(2)');

        let regDate = '';
        if(this.dataToSave.hasOwnProperty(code) && this.dataToSave[code] != '') {
          regDate = this.dataToSave[code];
        } else if(this.selectItem && this.selectItem.regDate && this.selectItem.regDate != '') {
          regDate = this.selectItem.regDate.split(' ')[0];
          this.dataToSave[code] = regDate;
        }

        const {container: docRegDateContainer, dateInput: docRegDateInput} = Components.createDateInput(i18n.tr(title), regDate);

        docRegDateContainer.addClass('rcc_field');

        if(required) docRegDateContainer.addClass('required');

        docRegDateInput.on('change', e => {
          this.dataToSave[code] = docRegDateInput.val();
          this.isChanges = true;
        });

        td.append(docRegDateContainer);
        break;
      }

      case 'duration': {
        const tableBlock1 = getTable('tableBlock1', 2, 2);
        const td = tableBlock1.find('tbody#tbody-tableBlock1>tr:nth-child(2)>td:nth-child(2)');

        let docTypeInfo = this.doctypes.find(x => x.code == this.dataToSave['doc_type']);
        if(!docTypeInfo) docTypeInfo = this.doctypes[0];
        const {input_method_term, calc_method_term, duration} = docTypeInfo;

        if(input_method_term == "FINISH_DATE") {
          let finishDate = '';
          if(this.dataToSave.hasOwnProperty(code) && AS.FORMS.DateUtils.validateDateString(this.dataToSave[code])) {
            finishDate = this.dataToSave[code];
          } else if(this.rccData) {
            finishDate = this.rccData.finishDate;
            this.dataToSave[code] = this.rccData.finishDate;
          }

          const {container: docFinishDateContainer, dateInput: docFinishDateInput} = Components.createDateInput(i18n.tr('Завершение'), finishDate);

          docFinishDateContainer.addClass('rcc_field');

          if(required) docFinishDateContainer.addClass('required');

          docFinishDateInput.on('change', e => {
            this.dataToSave[code] = docFinishDateInput.val();
            this.isChanges = true;
          });

          td.append(docFinishDateContainer);
        } else {
          let label = 'Длительность (раб.дн)';
          if(calc_method_term == "CALENDAR_DAYS") label = 'Длительность (кал.дн)';

          const {container: docDurationContainer, input: docDurationInput} = Components.createInputNumber(i18n.tr(label));
          docDurationContainer.addClass('rcc_field');
          docDurationInput.css({'min-width': '100px'});

          if(this.dataToSave.hasOwnProperty(code) && this.dataToSave[code] != '' && !isNaN(Number(this.dataToSave[code]))) {
            docDurationInput.val(this.dataToSave[code]);
          } else if (this.rccData) {
            this.dataToSave[code] = String(this.rccData.length);
            docDurationInput.val(this.dataToSave[code]);
          } else {
            this.dataToSave[code] = String(duration);
            docDurationInput.val(this.dataToSave[code]);
          }

          if(required) docDurationContainer.addClass('required');

          docDurationInput.on('change', e => {
            this.dataToSave[code] = docDurationInput.val();
            this.isChanges = true;
          });

          td.append(docDurationContainer);
        }

        break;
      }

      case 'correspondent_org': {
        const {container: correspondentOrgContainer, input: correspondentOrgInput} = Components.createInputText(i18n.tr(title));
        correspondentOrgContainer.addClass('rcc_field');

        if(this.dataToSave.hasOwnProperty(code) && this.dataToSave[code] != '') {
          correspondentOrgInput.val(this.dataToSave[code]);
        } else if(this.selectItem) {
          correspondentOrgInput.val(this.selectItem.correspondentOrg);
          this.dataToSave[code] = this.selectItem.correspondentOrg;
        }

        if(required) correspondentOrgContainer.addClass('required');

        correspondentOrgInput.on('change', e => {
          this.dataToSave[code] = correspondentOrgInput.val();
          this.isChanges = true;
        });

        container.append(correspondentOrgContainer);
        break;
      }

      case 'correspondent': {
        const {container: correspondentContainer, input: correspondentInput} = Components.createInputText(i18n.tr(title));
        correspondentContainer.addClass('rcc_field');

        if(this.dataToSave.hasOwnProperty(code) && this.dataToSave[code] != '') {
          correspondentInput.val(this.dataToSave[code]);
        } else if(this.selectItem) {
          correspondentInput.val(this.selectItem.correspondent);
          this.dataToSave[code] = this.selectItem.correspondent;
        }

        if(required) correspondentContainer.addClass('required');

        correspondentInput.on('change', e => {
          this.dataToSave[code] = correspondentInput.val();
          this.isChanges = true;
        });

        container.append(correspondentContainer);
        break;
      }

      case 'author': {
        if(!this.isNew && this.selectItem.author != '') {
          const id = UTILS.generateGuid('comp');
          const labelContainer = $('<div>', {class: 'uk-margin-small rcc_field'});
          const fc = $('<div>', {class: 'uk-form-controls'});
          fc.append(`<span id="${id}" class="uk-form-label uk-text-normal">${this.selectItem.author}</span>`);
          labelContainer.append(`<label class="uk-form-label uk-text-bold" for="${id}">${i18n.tr(title)}</label>`).append(fc);

          container.append(labelContainer);
        }
        break;
      }

      case 'reg_user': {
        if(!this.isNew && this.selectItem.regUser != '') {
          const id = UTILS.generateGuid('comp');
          const labelContainer = $('<div>', {class: 'uk-margin-small rcc_field'});
          const fc = $('<div>', {class: 'uk-form-controls'});
          fc.append(`<span id="${id}" class="uk-form-label uk-text-normal">${this.selectItem.regUser}</span>`);
          labelContainer.append(`<label class="uk-form-label uk-text-bold" for="${id}">${i18n.tr(title)}</label>`).append(fc);

          container.append(labelContainer);
        }
        break;
      }

      case 'control': {
        const {container: isControlContainer, input: isControl} = Components.createCheckbox(i18n.tr(title));
        isControlContainer.addClass('rcc_field');

        if(this.dataToSave[code]) {
          isControl.prop('checked', true);
        } else if(!this.isNew && this.rccData && this.rccData.controlled) {
          isControl.prop('checked', true);
          this.dataToSave[code] = true;
        } else {
          this.dataToSave[code] = false;
        }

        if(required) isControlContainer.addClass('required');

        isControl.on('change', e => {
          this.dataToSave[code] = e.target.checked;
          this.isChanges = true;
        });

        container.append(isControlContainer);

        break;
      }

      case 'base_number': {
        const tableBlock2 = getTable('tableBlock2', 2, 1);
        const td = tableBlock2.find('tbody#tbody-tableBlock2>tr:nth-child(1)>td:nth-child(1)');

        const {container: docBaseNumberContainer, input: docBaseNumberInput} = Components.createInputText(i18n.tr(title));
        docBaseNumberContainer.addClass('rcc_field');

        if(this.dataToSave.hasOwnProperty(code) && this.dataToSave[code] != '') {
          docBaseNumberInput.val(this.dataToSave[code]);
        }

        if(required) docBaseNumberContainer.addClass('required');

        docBaseNumberInput.on('change', e => {
          this.dataToSave[code] = docBaseNumberInput.val();
          this.isChanges = true;
        });

        td.append(docBaseNumberContainer);
        break;
      }

      case 'base_date': {
        const tableBlock2 = getTable('tableBlock2', 2, 1);
        const td = tableBlock2.find('tbody#tbody-tableBlock2>tr:nth-child(1)>td:nth-child(2)');

        let baseRegDate = '';
        if(this.dataToSave.hasOwnProperty(code) && this.dataToSave[code] != '') {
          baseRegDate = this.dataToSave[code];
        }

        const {container: docBaseRegDateContainer, dateInput: docBaseRegDateInput} = Components.createDateInput(i18n.tr(title), baseRegDate);

        docBaseRegDateContainer.addClass('rcc_field');

        if(required) docBaseRegDateContainer.addClass('required');

        docBaseRegDateInput.on('change', e => {
          this.dataToSave[code] = docBaseRegDateInput.val();
          this.isChanges = true;
        });

        td.append(docBaseRegDateContainer);
        break;
      }

      case 'doc_file': {
        const docFileContainer = $('<div>', {class: 'uk-margin-small rcc_field'});
        const fc = $('<div>', {class: 'uk-form-controls'});
        const selectButton = $('<a class="uk-form-icon uk-form-icon-flip" href="javascript:void(0);" uk-icon="icon: more" style="right: 40px;"></a>');
        const clearButton = $('<a class="uk-form-icon uk-form-icon-flip" href="javascript:void(0);" uk-icon="icon: trash"></a>');
        const resultBlock = $('<div>', {class: 'doc_file_documents_container'});
        const labelDocFileInfo = $('<span>', {class: 'fonts', style: "margin-left: 10px;"});

        if(required) docFileContainer.addClass('required');

        fc.append($('<div class="uk-inline uk-width-expand" style="max-width: 400px;">').append(clearButton, selectButton, resultBlock), labelDocFileInfo);
        docFileContainer.append(`<label class="uk-form-label uk-text-bold">${i18n.tr(title)}</label>`).append(fc);
        container.append(docFileContainer);

        const fillDocFileInfo = docFileID => {
          const msgInfo = {
            ru: 'Ср. хр. в годах: ${value}',
            kk: '${value} жылдарда сақтау мерзімі',
            en: 'Expiry years: ${value}'
          }
          getDocFileInfo(docFileID).then(info => {
            if(info && info.expiry) {
              labelDocFileInfo.text(msgInfo[AS.OPTIONS.locale].replaceAll('${value}', info.expiry));
            } else {
              labelDocFileInfo.empty();
            }
          });
        }

        if(this.dataToSave.hasOwnProperty(code) && this.dataToSave[code].doc_file) {
          resultBlock.text(this.dataToSave[code].doc_file_name);
          fillDocFileInfo(this.dataToSave[code].doc_file);
        } else if (!this.isNew && this.rccData && this.rccData.doc_file) {
          const {doc_file, doc_file_code, doc_file_name} = this.rccData;
          this.dataToSave[code] = {doc_file, doc_file_code, doc_file_name};
          resultBlock.text(doc_file_name);
          fillDocFileInfo(doc_file);
        }

        selectButton.on('click', e => {
          showTreeAffairsDialog(result => {
            resultBlock.text(result.selectItem.text);
            this.dataToSave[code] = {
              doc_file: result.selectItem.id,
              doc_file_code: null,
              doc_file_name: result.selectItem.text
            };
            fillDocFileInfo(result.selectItem.id);
            this.isChanges = true;
          });
        });

        clearButton.on('click', e => {
          resultBlock.empty();
          this.dataToSave[code] = null;
          labelDocFileInfo.empty();
          this.isChanges = true;
        });

        break;
      }

      case 'base': {
        const baseContainer = $('<div>', {class: 'uk-margin-small rcc_field'});
        const fc = $('<div>', {class: 'uk-form-controls'});
        const selectButton = $('<a class="uk-form-icon uk-form-icon-flip" href="javascript:void(0);" uk-icon="icon: more"></a>');
        const resultBlock = $('<div>', {class: 'base_documents_container'});
        let values = [];

        if(required) baseContainer.addClass('required');

        fc.append($('<div class="uk-inline uk-width-expand">').append(selectButton, resultBlock));
        baseContainer.append(`<label class="uk-form-label uk-text-bold">${i18n.tr(title)}</label>`).append(fc);
        container.append(baseContainer);

        const getDocumentBlock = doc => {
          const meaning = doc.number != '' ? `${doc.number} - ${doc.name == '' ? doc.content : doc.name}` : doc.name == '' ? doc.content : doc.name;
          const documentBlock = $('<div>', {class: 'base_document_block'});
          const deleteBlockButton = $('<a class="uk-icon" href="javascript:void(0);" uk-icon="icon: close"></a>');

          documentBlock.append(`<span>${meaning}</span>`, deleteBlockButton);

          documentBlock.on('dblclick', e => {
            if($(e.target).is(".uk-icon")) return;
            const eventParam = {
              type: 'document',
              documentID: doc.documentID,
              editable: false
            };
            $('#root-panel').trigger({type: 'custom_open_document', eventParam});
          });

          deleteBlockButton.on('click', e => {
            const msgConfirm = i18n.tr('Вы действительно хотите удалить тег {0}?').replace('{0}', meaning);
            if(confirm(msgConfirm)) {
              values = values.filter(x => x.documentID != doc.documentID);
              this.dataToSave[code] = values.map(x => x.documentID);
              documentBlock.remove();
              this.isChanges = true;
            }
          });

          return documentBlock;
        }

        if(!this.isNew && this.rccData && this.rccData.bases.length) {
          this.dataToSave[code] = [...this.rccData.bases];
        }

        if(this.dataToSave.hasOwnProperty(code) && Array.isArray(this.dataToSave[code])) {
          this.dataToSave[code].forEach(async x => {
            const docInfo = await appAPI.getDocumentInfo(x);
            resultBlock.append(getDocumentBlock(docInfo));
            values.push(docInfo);
          });
        }

        selectButton.on('click', e => {
          showRegisterLinkDialog(doc => {
            if(values.find(x => x.documentID == doc.documentID)) {
              showMessage(i18n.tr('Выделенный документ уже выбран в качестве основания. Выберите другой документ.'), 'error');
            } else {
              resultBlock.append(getDocumentBlock(doc));
              values.push(doc);
              this.dataToSave[code] = values.map(x => x.documentID);
              this.isChanges = true;
            }
          });
        });

        break;
      }
    }
  }

  getFilesTable(){
    const table = $('<table cellspacing="4" cellpadding="0" style="table-layout: fixed; width: 100%; font-size: 14px;"></table>');
    const tBody = $('<tbody>');
    table.append(tBody);
    return {table, tBody};
  }

  getRowTableFile(type, data){
    const tr = $('<tr>');

    const tdName = $(`<td align="left" style="vertical-align: middle;"></td>`);
    const tdAction1 = $(`<td align="right" width="70px" style="vertical-align: middle;"></td>`);
    const tdAction2 = $(`<td align="right" width="70px" style="vertical-align: middle;"></td>`);
    const tdAction3 = $(`<td align="right" width="70px" style="vertical-align: middle;"></td>`);

    tr.append(tdName, tdAction1, tdAction2, tdAction3);

    switch (type) {
      case 'attach':
      case 'work': {
        const {name, uuid} = data;
        tdName.append(`<span>${name}</span>`);
        const openButton = $(`<a class="uk-link-text" href="#">${i18n.tr('Открыть')}</a>`);
        const downloadButton = $(`<a class="uk-link-text" href="#">${i18n.tr('Скачать')}</a>`);
        const deleteButton = $(`<a class="uk-link-text" href="#">${i18n.tr('Удалить')}</a>`);

        openButton.on('click', e => {
          e.preventDefault();
          e.target.blur();
          const file = new AttachmentFile(name, uuid);
          file.open();
        });

        downloadButton.on('click', e => {
          e.preventDefault();
          e.target.blur();
          Cons.showLoader();
          UTILS.fileDownload(name, uuid);
          Cons.hideLoader();
        });

        deleteButton.on('click', e => {
          const msgConfirm = i18n.tr('Вы действительно хотите удалить файл?');
          if(confirm(msgConfirm)) {
            appAPI.deleteFile(uuid).then(res => {
              if(res && res.errorCode == 0) {
                tr.remove();
                this.isChanges = true;
              } else {
                showMessage(res.errorMessage, 'error');
              }
            });
          }
        });

        if(this.selectItem && this.selectItem.isRegistyDocuments == "true"){
          if(type == 'work') {
            tdAction1.append(openButton);
            tdAction2.append(downloadButton);
            tdAction3.append(deleteButton);
          } else {
            tdAction2.append(openButton);
            tdAction3.append(downloadButton);
          }
        } else {
          if(this.rccData.registered) {
            tdAction2.append(openButton);
            tdAction3.append(downloadButton);
          } else {
            tdAction1.append(openButton);
            tdAction2.append(downloadButton);
            tdAction3.append(deleteButton);
          }
        }
        break;
      }
      case 'new_file': {
        const {id, file} = data;
        const {name, size} = file;
        const deleteButton = $(`<a class="uk-link-text" href="#">${i18n.tr('Удалить')}</a>`);

        tdName.append(`<span>${name} - (${Math.round(size / 1024 * 100) / 100} ${i18n.tr('КБ')})</span>`);

        deleteButton.on('click', e => {
          const msgConfirm = i18n.tr('Вы действительно хотите удалить загрузку?');
          if(confirm(msgConfirm)) {
            this.files = this.files.filter(x => x.id != id);
            tr.remove();
          }
        });

        tdAction3.append(deleteButton);
        break;
      }
    }

    return tr;
  }

  renderRccFiles(container) {
    const {container: inputFileContainer, inputFile} = Components.createSelectFileMultiple(i18n.tr('Выбрать файлы'));
    const accordionItems = [];
    const attachContainer = $('<div>');
    const workContainer = $('<div>');

    const {table: attachTable, tBody: attachTbody} = this.getFilesTable();
    const {table: workTable, tBody: workTbody} = this.getFilesTable();

    attachContainer.append(attachTable);
    workContainer.append(workTable);

    this.attachments.forEach(item => attachTbody.append(this.getRowTableFile('attach', item)));
    this.work_files.forEach(item => workTbody.append(this.getRowTableFile('work', item)));

    accordionItems.push({
      title: i18n.tr("Приложения"),
      content: attachContainer,
      open: true
    });

    accordionItems.push({
      title: i18n.tr("Прочие"),
      content: workContainer,
      open: true
    });

    inputFile.on('change', event => {
      for(let i = 0; i <= event.target.files.length; i++){
        if(!event.target.files[i]) break;

        const file = event.target.files[i];
        const chekFile = this.files.find(x => x.file.name == file.name && x.file.size == file.size);
        if(chekFile) continue;

        const tmp = {id: this.fileIndex, file: file};
        this.files.push(tmp);
        this.fileIndex++;

        if(this.selectItem && this.selectItem.isRegistyDocuments == "true") {
          workTbody.append(this.getRowTableFile('new_file', tmp));
        } else {
          attachTbody.append(this.getRowTableFile('new_file', tmp));
        }

        this.isChanges = true;
      }
    });

    const accordion = Components.createAccordion(accordionItems); //, 'rcc-accordion'

    accordion.find('.uk-accordion-title').css({
      'border-bottom': '1px dashed #c4c4c4',
      'padding': '10px 0',
      'font-size': '16px'
    });

    container.append(inputFileContainer, accordion);
  }

  async renderRccData(panelContent, docTypeInfo){
    panelContent.empty();

    const {baseRegisterID} = docTypeInfo;
    const fields = rccFields.get(docTypeInfo);

    const changeTab = (button, content) => {
      panelContent.find('.registration_tabcontent').removeClass('active');
      panelContent.find('.registration_tablinks').removeClass('active');

      button.addClass('active');
      content.addClass('active');
    }

    const tab = $('<div>', {class: 'registration_tab'});

    const tablinksRCC = $(`<button class="registration_tablinks active">${i18n.tr('Карточка')}</button>`);
    const tablinksFiles = $(`<button class="registration_tablinks">${i18n.tr('Файлы')}</button>`);

    const tabContentRCC = $('<div>', {id: 'tab_rcc_content', class: 'registration_tabcontent uk-form-horizontal active'});
    const tabContentFiles = $('<div>', {id: 'tab_files_content', class: 'registration_tabcontent'});

    for(let i = 0; i < fields.length; i++) this.renderRccComponents(fields[i], tabContentRCC, panelContent);

    this.renderRccFiles(tabContentFiles);

    tab.append(tablinksRCC, tablinksFiles);
    panelContent.append(tab, tabContentRCC, tabContentFiles);

    tablinksRCC.on('click', e => {
      changeTab(tablinksRCC, tabContentRCC);
    });
    tablinksFiles.on('click', e => {
      changeTab(tablinksFiles, tabContentFiles);
    });

    if(baseRegisterID != 'out_docs') {
      const tabName = i18n.tr('Шаг 3: Исполнители и маршрут').split(': ')[1];
      const tabContentUser = $('<div>', {id: 'tab_user_content', class: 'registration_tabcontent uk-form-horizontal'});
      const tablinksUser = $(`<button class="registration_tablinks">${tabName}</button>`);

      //user block
      if(!this.dataToSave.hasOwnProperty('user')) this.dataToSave['user'] = '';

      const userBlockContainer = $('<div>', {class: 'uk-margin-small rcc_field'});
      const fc = $('<div>', {class: 'uk-form-controls'});
      const selectUserButton = $('<a class="uk-form-icon uk-form-icon-flip" href="javascript:void(0);" uk-icon="icon: users"></a>');
      const inputResult = $('<input class="uk-input" type="text" style="background: #fff;" disabled>');
      let values = [];

      if(this.dataToSave['user'] != '') {
        values = await AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/userchooser/getUserInfo?userID=${this.dataToSave['user']}&locale=${AS.OPTIONS.locale}`);
      } else if(!this.isNew && this.rccData && this.rccData.executorID != '') {
        values = await AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/userchooser/getUserInfo?userID=${this.rccData.executorID}&locale=${AS.OPTIONS.locale}`);
      }

      if(values.length) {
        let userNames = values.map(x => x.personName).join('; ');
        inputResult.val(userNames).attr('title', userNames);
        this.dataToSave['user'] = values[0].personID;
      }

      selectUserButton.on('click', e => {
        //(values, multiSelectable, isGroupSelectable, showWithoutPosition, filterPositionID, filterDepartmentID, locale, handler)
        AS.SERVICES.showUserChooserDialog(values, false, false, false, null, null, AS.OPTIONS.locale, users => {
          let userNames = users.map(x => x.personName).join('; ');
          inputResult.val(userNames).attr('title', userNames);
          values = users;
          this.dataToSave['user'] = values[0].personID;
          this.isChanges = true;
        });
      });

      fc.append($('<div class="uk-inline uk-width-expand">').append(selectUserButton, inputResult));
      userBlockContainer.append(`<label class="uk-form-label uk-text-bold">${i18n.tr('Определить исполнителя')}</label>`).append(fc);
      tabContentUser.append(userBlockContainer);

      tab.append(tablinksUser);
      panelContent.append(tabContentUser);

      tablinksUser.on('click', e => {
        changeTab(tablinksUser, tabContentUser);
      });
    }

    if(!this.isNew && this.signList && this.signList.length) {
      const signButton = $(`<a class="uk-link-text registration_sign_button" href="javascript:void(0);">${i18n.tr('Подписи')}</a>`);

      signButton.on('click', e => {
        this.showSignList();
      });

      panelContent.append(signButton);
    }

    if(docTypeInfo.formID) {
      this.formPlayer = AS.FORMS.createPlayer();
      this.formPlayer.view.setEditable(true);

      if(this.rccData && this.rccData.dataId) {
        this.formPlayer.showFormData(null, null, this.rccData.dataId, null);
      } else {
        const formInfo = await appAPI.getAsfDefinition(docTypeInfo.formID);
        this.formPlayer.showFormByCode(formInfo.code);
      }

      tabContentRCC.append(this.formPlayer.view.container);
    } else {
      this.formPlayer = null;
    }
  }

  showSignList(){
    const dialog = $('<div>', {class: 'signlist_dialog_container'});
    const panelContent = $('<div>', {class: 'signlist_panel_content'});
    const panelButtons = $('<div>', {class: 'signlist_panel_buttons'});
    const buttonClose = $(`<button class="uk-button uk-button-default uk-button-small">${i18n.tr('Закрыть')}</button>`);

    const headers = [i18n.tr('Ф.И.О.'), i18n.tr('Должность'), i18n.tr('Дата')];
    const rows = [];

    this.signList.forEach(item => {
      const tmp = [];
      tmp.push(item.userName);
      tmp.push(item.userPosition);
      tmp.push(AS.FORMS.DateUtils.formatDate(AS.FORMS.DateUtils.parseDate(item.date), "${dd}.${mm}.${yy}"));
      rows.push(tmp);
    })

    const {table} = Components.createTable({headers, rows}, ['sign_table', 'uk-table-small']);

    panelButtons.append(buttonClose);
    panelContent.append(table);
    dialog.append(panelContent, panelButtons);

    buttonClose.on('click', e => {
      dialog.dialog("close");
    });

    dialog.dialog({
      modal: true,
      width: 700,
      height: 500,
      title: i18n.tr('Подписи документов'),
      show: {effect: 'fade', duration: 300},
      hide: {effect: 'fade', duration: 300},
      close: function() {
        $(this).remove();
      }
    });
  }

  openRegistrationDialog(){
    try {
      this.docTypeInfo;
      if(this.isNew) {
        this.docTypeInfo = this.doctypes[0];
      } else if (this.rccData) {
        this.docTypeInfo = this.doctypes.find(x => x.typeID == this.rccData.docTypeId);
      }

      if(!this.docTypeInfo) throw new Error('Не найден тип документа');

      const panelContent = $('<div>', {class: 'uk-modal-body registration_panel_content'});
      const panelButtons = $('<div>', {class: 'uk-modal-footer registration_panel_buttons'});

      if(!this.isNew && this.rights.canRefuse) {
        this.buttonReject = $(`<button class="uk-button uk-button-default uk-button-small" type="button">${i18n.tr("Отклонить")}</button>`);
        panelButtons.append(this.buttonReject);
      }

      if(this.rights.canWrite) {
        this.buttonSave = $(`<button class="uk-button uk-button-default uk-button-small" type="button">${i18n.tr("Сохранить")}</button>`);
        panelButtons.append(this.buttonSave);
      }

      if(this.rights.canRegister) {
        this.buttonRegister = $(`<button class="uk-button uk-button-primary uk-button-small" type="button">${i18n.tr("Зарегистрировать")}</button>`);
        panelButtons.append(this.buttonRegister);
      }

      this.renderRccData(panelContent, this.docTypeInfo);

      this.dialog = $('<div>', {class: 'registration-modal'});
      this.dialog.append(panelContent, panelButtons);

      this.dialog.dialog({
        modal: true,
        width: 900,
        height: 600,
        title: i18n.tr('Регистрация'),
        show: {effect: 'fade', duration: 300},
        hide: {effect: 'fade', duration: 300},
        close: function() {
          $(this).remove();
        }
      });

      this.addButtonListener();

    } catch (err) {
      console.log('ERROR openRegistrationDialog', err);
      showMessage(err.message, 'error');
    }
  }

  checkRights(){
    for(const key in this.rights) {
      if(!key.includes('can')) continue;
      if(this.rights[key]) {
        return true;
        break;
      }
    }
    return false;
  }

  async init(){
    Cons.showLoader();
    try {
      const {filterType, objectID, selectItem} = DocumentListGetFilterParams();

      this.filterType = filterType;
      this.registerID = objectID;
      this.selectItem = selectItem;

      if(this.isNew) this.selectItem = null;

      if(this.filterType !== "REGISTER_FILTER") throw new Error(`filterType = ${this.filterType}`);

      if(!this.registerID) {
        if(this.isNew) throw new Error(`Не передан ID журнала [registerID: ${this.registerID}]`);
        if(this.selectItem) {
          if(!this.rccData) this.rccData = await appAPI.getDocumentRCC(this.selectItem.documentID);
          this.registerID = this.rccData.registerID;
          this.signList = await appAPI.getSignList(this.selectItem.documentID);
        }
      }

      if(!this.registerInfo) {
        this.registerInfo = await appAPI.getRegisterInfo(this.registerID);
        if(!this.registerInfo) throw new Error(`Не удалось получить информацию по журналу`);
      }
      this.rights = this.registerInfo.rights.find(x => x.userID == AS.OPTIONS.currentUser.userid);

      if(!this.checkRights()) throw new Error(`Нет прав`);


      this.doctypes = await getDoctypes(this.registerID);
      if(!this.doctypes) throw new Error(`Не удалось получить список доступных типов документов`);

      if(!this.isNew && this.selectItem) {
        if(!this.rccData) this.rccData = await appAPI.getDocumentRCC(this.selectItem.documentID);
        if(!this.signList) this.signList = await appAPI.getSignList(this.selectItem.documentID);

        const res = await appAPI.getDocumentAttachments(this.selectItem.documentID);
        if(res) {
          this.attachments = res.attachments;
          this.work_files = res.work_files;
        }
      }

      if(this.isNew) this.isChanges = true;

      if(this.selectItem && this.selectItem.hasOwnProperty('reservedID') && this.selectItem.reservedID != "null") {
        this.reservedID = this.selectItem.reservedID;
      }

      Cons.hideLoader();

      this.openRegistrationDialog();
    } catch (e) {
      Cons.hideLoader();
      console.log(`ERROR Registration: ${e.message}`);
    }
  }
}
