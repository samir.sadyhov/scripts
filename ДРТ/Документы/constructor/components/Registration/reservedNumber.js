let selectResult, reservedList, selectReservedID;

const getReservedList = registerID => {
  return new Promise(resolve => {
    rest.synergyGet(`api/docflow/reserved/list?registerID=${registerID}&locale=${AS.OPTIONS.locale}`, resolve, err => {
      console.log(`ERROR [ getReservedList ]: ${JSON.stringify(err)}`);
      resolve(null);
    });
  });
}

const saveReserveNumber = async body => {
  return new Promise(async resolve => {
    try {
      const {login, password} = Cons.creds;
      const auth = "Basic " + btoa(unescape(encodeURIComponent(`${login}:${password}`)));
      const response = await fetch(`../Synergy/rest/api/docflow/reserved/save`, {
        method: 'POST',
        headers: {"Authorization": auth, "Content-Type": "application/json; charset=UTF-8"},
        body: JSON.stringify(body)
      });
      resolve(response.json());
    } catch (err) {
      console.log(`ERROR [ saveReserveNumber ]: ${err.message}`);
      resolve(null);
    }
  });
}

const openDialogReservedNumber = param => {
  const {edit = false, registerInfo, tBody, item} = param;
  const regexp = /\{[^}]+\}/g;

  const dialogTitle = edit ? i18n.tr('Редактирование резерва') : i18n.tr('Добавление номера в резерв');
  const applyButtonTitle = edit ? i18n.tr('Сохранить') : i18n.tr('Зарезервировать');

  const dialog = $('<div>');
  const panelContent = $('<div>', {style: "height: calc(100% - 50px);"});
  const panelButtons = $('<div>');
  const buttonApply = $(`<button class="uk-button uk-button-primary uk-button-small" type="button">${applyButtonTitle}</button>`);

  const panelCounter = $('<div>');
  const buttonPrev = $(`<a href="#" class="uk-icon-button disabled-link" uk-icon="chevron-left"></a>`);
  const buttonNext = $(`<a href="#" class="uk-icon-button" uk-icon="chevron-right"></a>`);
  const counterLabel = $('<span>');

  const reservComment = $(`<textarea class="uk-textarea" rows="6" placeholder="${i18n.tr('Комментарий к резервируемому номеру')}"></textarea>`);

  const label = $('<span>', {class: 'uk-text-bold', style: 'user-select: none;'});
  label.text(i18n.tr('Выберите номер'));

  panelCounter.css({
    'display': 'flex',
    'justify-content': 'space-between',
    'align-items': 'center',
    'padding': '10px 0'
  });

  panelButtons.css({
    'display': 'flex',
    'gap': '10px',
    'flex-direction': 'row',
    'justify-content': 'center',
    'align-items': 'center',
    'border-top': '1px solid #e5e5e5',
    'height': '50px'
  });

  if(edit) {
    panelCounter.append(counterLabel);
    panelContent.append(panelCounter, reservComment);
    panelCounter.css({'justify-content': 'center'});
    reservComment.text(item.comment);
    counterLabel.text(item.reservedNumber);
  } else {
    panelCounter.append(buttonPrev, counterLabel, buttonNext);
    panelContent.append(label, panelCounter, reservComment);
    counterLabel.text(registerInfo.register.formula.replace(regexp, reservedList.nextCounter));
  }

  panelButtons.append(buttonApply);
  dialog.append(panelContent, panelButtons);

  buttonApply.on('click', async e => {
    e.preventDefault();
    e.target.blur();
    Cons.showLoader();

    try {
      const findReserv = reservedList.reservedNumber.find(x => x.reservedNumber == counterLabel.text());
      if(!edit && findReserv) throw new Error(i18n.tr('Данный номер уже зарегистрирован.'));

      const saveBody = {};
      if(edit) {
        saveBody.reservedID = item.reservedID;
        selectReservedID = item.reservedID;
      } else {
        saveBody.registerID = registerInfo.identifier;
        saveBody.reservedNumber = counterLabel.text();
      }
      saveBody.comment = reservComment.val();

      const resultSave = await saveReserveNumber(saveBody);
      if(resultSave.errorCode != 0) throw new Error(resultSave.errorMessage);

      reservedList = await getReservedList(registerInfo.identifier);
      createBody(tBody, registerInfo);

      Cons.hideLoader();
      dialog.dialog("close");
    } catch (err) {
      Cons.hideLoader();
      showMessage(err.message, 'error');
    }

  });

  if(!edit) {
    let selectNumber = reservedList.nextCounter;

    buttonPrev.on('click', e => {
      e.preventDefault();
      e.target.blur();
      if(!isNaN(Number(selectNumber))) {
        if(selectNumber <= reservedList.nextCounter) {
          buttonPrev.addClass('disabled-link');
        } else {
          selectNumber--;
          counterLabel.text(registerInfo.register.formula.replace(regexp, selectNumber));
          if(selectNumber <= reservedList.nextCounter) buttonPrev.addClass('disabled-link');
        }
      }
    });

    buttonNext.on('click', e => {
      e.preventDefault();
      e.target.blur();
      if(!isNaN(Number(selectNumber))) {
        selectNumber++;
        counterLabel.text(registerInfo.register.formula.replace(regexp, selectNumber));
        buttonPrev.removeClass('disabled-link');
      }
    });
  }

  dialog.dialog({
    modal: true,
    width: 400,
    height: 310,
    title: dialogTitle,
    resizable: false,
    show: {effect: 'fade', duration: 300},
    hide: {effect: 'fade', duration: 300},
    close: function() {
      $(this).remove();
    }
  });
}

const createBody = (tBody, registerInfo) => {
  tBody.empty();

  const {reservedNumber} = reservedList;
  const trAuto = $('<tr>');
  const trR = $('<tr>');

  trAuto.append(`<td colspan="4">${i18n.tr('Автоматический генерируемый номер')}</td>`);
  trR.append(`<td colspan="4">${i18n.tr('Резерв')}</td>`);

  tBody.append(trAuto, trR);

  if(selectResult == 'AUTO') trAuto.attr('selected', true);

  trAuto.on('click', e => {
    tBody.find('tr').removeAttr('selected');
    trAuto.attr('selected', true);
    selectResult = 'AUTO';
  });

  trR.on('click', e => {
    tBody.find('tr').removeAttr('selected');
    trR.attr('selected', true);
    selectResult = null;
  });

  reservedNumber.forEach(item => {
    const {author, comment, reservedDate, reservedID, reservedNumber} = item;
    const trData = $('<tr>');
    trData.append(`<td>${reservedNumber || ''}</td>`);
    trData.append(`<td>${author || ''}</td>`);
    trData.append(`<td>${reservedDate || ''}</td>`);
    trData.append(`<td>${comment || ''}</td>`);

    if(selectResult == reservedNumber) {
      tBody.find('tr').removeAttr('selected');
      trData.attr('selected', true);
    }

    tBody.append(trData);

    trData.on('click', e => {
      tBody.find('tr').removeAttr('selected');
      trData.attr('selected', true);
      selectResult = reservedNumber;
      selectReservedID = reservedID;
    });

    new ContextMenu(trData, null, [{
      name: i18n.tr('Редактировать комментарий'),
      icon: 'file-edit',
      handler: function(){
        openDialogReservedNumber({edit: true, tBody, item, registerInfo});
      }
    }]);

  });
}

const createHeader = tHead => {
  const tr = $('<tr>');

  tr.append(`<th class="uk-table-expand">${i18n.tr('Номер')}</th>`);
  tr.append(`<th class="uk-table-expand">${i18n.tr('Изменил')}</th>`);
  tr.append(`<th class="uk-table-expand">${i18n.tr('Дата')}</th>`);
  tr.append(`<th class="uk-table-expand">${i18n.tr('Комментарий')}</th>`);

  tHead.empty().append(tr);
}

const createTable = (registerInfo) => {
  const table = $('<table class="registration_table_reserv uk-table uk-table-small uk-table-responsive">');
  const colgroup = $('<colgroup>');
  const tHead = $('<thead>');
  const tBody = $('<tbody>');

  createHeader(tHead);
  createBody(tBody, registerInfo);

  table.append(colgroup, tHead, tBody);

  return {table, tBody};
}

const openDialogNumber = (registerInfo, callback) => {
  const dialogNumber = $('<div>');
  const panelContent = $('<div>', {style: "height: calc(100% - 110px); overflow: auto; margin-top: 10px;"});
  const panelButtons = $('<div>');
  const buttonReserved = $(`<button class="uk-button uk-button-default uk-button-small" type="button">${i18n.tr("Зарезервировать номер")}</button>`);
  const buttonApply = $(`<button class="uk-button uk-button-primary uk-button-small" type="button">${i18n.tr("Готово")}</button>`);
  const {table, tBody} = createTable(registerInfo);

  panelContent.append(table);
  panelButtons.append(buttonApply);
  dialogNumber.append(buttonReserved, panelContent, panelButtons);

  panelButtons.css({
    'display': 'flex',
    'gap': '10px',
    'flex-direction': 'row',
    'justify-content': 'center',
    'align-items': 'center',
    'border-top': '1px solid #e5e5e5',
    'height': '50px'
  });

  buttonReserved.on('click', e => {
    e.preventDefault();
    e.target.blur();
    openDialogReservedNumber({registerInfo, tBody});
  });

  buttonApply.on('click', e => {
    e.preventDefault();
    e.target.blur();
    dialogNumber.dialog("close");
    callback(selectResult, selectReservedID);
  });

  dialogNumber.dialog({
    modal: true,
    width: 670,
    height: 500,
    title: i18n.tr('Номер'),
    resizable: false,
    show: {effect: 'fade', duration: 300},
    hide: {effect: 'fade', duration: 300},
    close: function() {
      $(this).remove();
    }
  });
}

this.reservedNumber = async (registerInfo, defaultNumber, callback) => {
  selectResult = defaultNumber || 'AUTO';
  const {identifier, formula} = registerInfo.register;
  reservedList = await getReservedList(identifier);

  openDialogNumber(registerInfo, callback);
}
