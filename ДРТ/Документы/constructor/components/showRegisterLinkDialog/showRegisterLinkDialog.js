const Paginator = {
  countInPart: 0,
  rows: 0,
  currentPage: 1,
  pages: 0,

  update: function(){
    this.pages = Math.ceil(this.rows / this.countInPart),
    this.label.text(this.currentPage + ' / ' + this.pages);

    if(this.pages == 0) {
      this.bPrevious.attr('disabled', 'disabled');
      this.bNext.attr('disabled', 'disabled');
    } else {
      if(this.currentPage == 1) {
        this.bPrevious.attr('disabled', 'disabled');
        if (this.currentPage == this.pages) {
          this.bNext.attr('disabled', 'disabled');
        } else {
          this.bNext.removeAttr('disabled');
        }
      } else {
        this.bPrevious.removeAttr('disabled');
        if (this.currentPage == this.pages) {
          this.bNext.attr('disabled', 'disabled');
        } else {
          this.bNext.removeAttr('disabled');
        }
      }
    }
  },

  addListeners: function(){
    this.bNext.click(() => {
      this.currentPage++;
      this.update();
      Documents.createBody();
    });

    this.bPrevious.click(() => {
      this.currentPage--;
      this.update();
      Documents.createBody();
    });

    this.label.on('dblclick', e => {
      e.preventDefault();
      this.input.show();
      this.label.hide();

      this.input.on('blur', () => {
        this.label.show();
        this.input.hide();
      });

      this.input.val("" + this.currentPage);

      this.input.on('keypress', e => {
        if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) return false;
      });

      this.input.on('keydown', e => {
        if(e.which === 13) {
          let inputVal = +this.input.val();
          if(inputVal && inputVal != this.currentPage && inputVal <= this.pages && inputVal >= 1){
            this.currentPage = inputVal;
            this.update();
            Documents.createBody();
          }
          this.label.show();
          this.input.hide();
          this.input.off();
        }
      });
      this.input.focus();
    });
  },

  render: function(){
    this.container = $('<div>', {class: "pt-container"});
    this.paginator = $('<div>', {class: "pt-paginator"});
    this.pContent = $('<div>', {class: "pt-paginator-content"});
    this.bPrevious = $(`<button class="pt-previous" disabled="disabled" title="${i18n.tr('Назад')}">`);
    this.bNext = $(`<button class="pt-next" disabled="disabled" title="${i18n.tr('Вперед')}">`);
    this.label = $('<label>');
    this.input = $('<input type="text">');

    this.paginatorContainer.empty().append(this.container);

    this.pContent.append(this.label).append(this.input);
    this.paginator.append(this.bPrevious).append(this.pContent).append(this.bNext);
    this.container.append(this.paginator);
  },

  init: function(rows, countInPart, paginatorContainer){
    this.paginatorContainer = paginatorContainer;
    this.currentPage = 1;
    this.rows = rows;
    this.countInPart = countInPart;
    this.pages = Math.ceil(this.rows / this.countInPart);

    this.render();
    this.addListeners();
  }
};

const Documents = {
  filterType: null,
  objectID: null,
  filterID: null,
  selectItem: null,

  createTable: function(){
    this.table = $('<table class="document-table-list uk-table uk-table-small">');
    this.colgroup = $('<colgroup>');
    this.tHead = $('<thead>');
    this.tBody = $('<tbody>');
    this.table.append(this.colgroup, this.tHead, this.tBody);

    this.colgroup.append(`<col style="width: 80px">`, `<col style="width: 300px">`, `<col style="width: 100px">`);

    this.createHeader();
  },

  createHeader: function(){
    const tr = $('<tr>');

    tr.append(`<th columnid="number" uk-tooltip="${i18n.tr('Номер')}" style="width: 80px;">${i18n.tr('Номер')}</th>`);
    tr.append(`<th columnid="name" uk-tooltip="${i18n.tr('Название')}" style="width: 300px;">${i18n.tr('Название')}</th>`);
    tr.append(`<th columnid="regDate" uk-tooltip="${i18n.tr('Дата регистрации')}" style="width: 100px;">${i18n.tr('Дата регистрации')}</th>`);
    tr.append(`<th columnid="correspondent" uk-tooltip="${i18n.tr('Корреспондент')}">${i18n.tr('Корреспондент')}</th>`);
    tr.append(`<th columnid="resUser" uk-tooltip="${i18n.tr('Исполнитель')}">${i18n.tr('Исполнитель')}</th>`);
    tr.append(`<th columnid="regUser" uk-tooltip="${i18n.tr('Зарегистрировал')}">${i18n.tr('Зарегистрировал')}</th>`);

    this.tHead.append(tr);
  },

  createTableRow: function(item){
    const tr = $('<tr>');

    item.number != '' ? tr.append(`<td uk-tooltip="${item.number}">${item.number}</td>`) : tr.append(`<td>`);
    item.name != '' ? tr.append(`<td uk-tooltip="${item.name}">${item.name}</td>`) : tr.append(`<td>`);

    const regDate = AS.FORMS.DateUtils.formatDate(AS.FORMS.DateUtils.parseDate(item.regDate), "${dd}.${mm}.${yyyy}");
    if(regDate && regDate != '') {
      tr.append(`<td uk-tooltip="${regDate}">${regDate}</td>`);
    } else {
      tr.append(`<td>`);
    }

    item.correspondentOrg != '' ? tr.append(`<td uk-tooltip="${item.correspondentOrg}">${item.correspondentOrg}</td>`) : tr.append(`<td>`);
    item.user != '' ? tr.append(`<td uk-tooltip="${item.user}">${item.user}</td>`) : tr.append(`<td>`);
    item.regUser != '' ? tr.append(`<td uk-tooltip="${item.regUser}">${item.regUser}</td>`) : tr.append(`<td>`);

    tr.on('click', e => {
      this.tBody.find('tr').removeAttr('selected');
      tr.attr('selected', true);
      this.selectItem = item;
    });

    if(item.registered == "true") tr.addClass('registered');
    if(item.statusID == "IN_PROGRESS" && item.hasResolution == "true") tr.addClass('resolution');
    if(item.statusID == "IN_PROGRESS" && item.isExpired == "true") tr.addClass('isexpired');

    this.tBody.append(tr);
  },

  getDocuments: async function(params){
    return new Promise(async resolve => {
      rest.synergyGet(`api/docflow/doc/documents?locale=${AS.OPTIONS.locale}${params ? '&'+params : ''}`, resolve, err => {
        console.log(`ERROR [ getDocuments ]: ${JSON.stringify(err)}`);
        resolve(null);
      });
    });
  },

  createBody: async function(){
    try {
      Cons.showLoader();

      this.selectItem = null;
      this.panelContent.empty();
      this.tBody.empty();
      this.panelContent.append(this.table);

      const params = new URLSearchParams();
      if(this.filterType) params.append("filterType", this.filterType);
      if(this.objectID) params.append("objectID", this.objectID);
      if(this.filterID) params.append("filterID", this.filterID);
      params.append("countInPart", Paginator.countInPart);
      params.append("pageNumber", Paginator.currentPage - 1);

      this.listData = await this.getDocuments(params.toString());

      for(let i = 0; i < this.listData.documents.length; i++) {
        const item = this.listData.documents[i];
        this.createTableRow(item);
      }

      Paginator.update();
      this.panelContent.scrollTop(0);

      Cons.hideLoader();
    } catch (err) {
      Cons.hideLoader();
      console.log('ERROR Documents createBody', err);
      showMessage(err.message, 'error');
    }
  },

  init: function(params, panelContent, paginatorContainer){
    try {
      const {filterType = null, objectID = null, filterID = null, count} = params;
      const totalCount = Number(count);
      this.filterType = filterType;
      this.objectID = objectID;
      this.filterID = filterID;
      this.panelContent = panelContent;

      this.createTable();

      Paginator.init(totalCount, 30, paginatorContainer);

      this.createBody();
    } catch (err) {
      console.log('ERROR Documents init', err);
      showMessage(err.message, 'error');
    }
  }
}

const getDocumentFilters = async () => {
  let result = [];

  const userAvailableFilters = await appAPI.getDocumentsFilters();

  for(let i = 0; i < userAvailableFilters.length; i++) {
    const {filterType} = userAvailableFilters[i];
    if(filterType == 'REGISTER_FILTER') {
      let REGISTER_FILTER = await appAPI.getDocumentsFilters(`filterType=${filterType}`);
      REGISTER_FILTER = REGISTER_FILTER.filter(x => x.hasOwnProperty('objectID')).map((x, index) => ({id: `RF-${index}`, ...x}));
      result = [...result, ...REGISTER_FILTER];
    } else if (filterType == 'ALL_USER_DOCUMENTS') {
      let ALL_USER_DOCUMENTS = await appAPI.getDocumentsFilters(`filterType=${filterType}`);
      ALL_USER_DOCUMENTS = ALL_USER_DOCUMENTS.filter(x => !x.hasOwnProperty('filterID')).map((x, index) => ({id: `UD-${index}`, ...x}));
      result = [...result, ...ALL_USER_DOCUMENTS];
    }
  }

  return result;
}

this.showRegisterLinkDialog = succesHandler => {
  try {
    Cons.showLoader();

    getDocumentFilters().then(documentFilters => {
      const dialog = $('<div>', {class: 'doc_chooser_dialog_container'});
      const panelFilter = $('<div>', {class: 'doc_chooser_panel_filter'});
      const panelContent = $('<div>', {class: 'doc_chooser_panel_content'});
      const panelButtons = $('<div>', {class: 'doc_chooser_panel_buttons'});
      const buttonApply = $(`<button class="uk-button uk-button-primary doc_chooser_dialog_button">${i18n.tr('Выбрать')}</button>`);
      const paginatorContainer = $('<div>', {class: 'doc_chooser_paginator'});

      const selectItems = documentFilters.map(x => ({title: x.name, value: x.id}));
      const selectRegister = new CustomSelect({items: selectItems, placeholder: 'Выберите фильтр', search: true});

      selectRegister.container.css({
        'width': 'calc(100% - 200px)',
        'border-radius': '6px'
      });

      selectRegister.container.find('.select-search-input').css('border-radius', '6px');

      selectRegister.container.on('selected', e => {
        const selectFilter = documentFilters.find(x => x.id == e.eventParam.value);
        Documents.init(selectFilter, panelContent, paginatorContainer);
      });

      panelFilter.append(selectRegister.container, paginatorContainer);

      panelButtons.append(buttonApply);
      dialog.append(panelFilter, panelContent, panelButtons);

      buttonApply.on('click', e => {
        e.preventDefault();
        e.target.blur();

        if(!Documents.selectItem) return;

        dialog.dialog("close");
        if(succesHandler) succesHandler(Documents.selectItem);
      });

      dialog.dialog({
        modal: true,
        width: 900,
        height: 600,
        title: i18n.tr('Выберите элемент'),
        show: {effect: 'fade', duration: 300},
        hide: {effect: 'fade', duration: 300},
        close: function() {
          $(this).remove();
        }
      });

      Cons.hideLoader();
    });
  } catch (err) {
    Cons.hideLoader();
    console.log(`ERROR showRegisterLinkDialog: ${err.message}`);
  }
}
