this.DocumentsFilter = class {
  constructor(treeItem, event, successHandler){
    this.treeItem = treeItem;
    this.event = event;
    this.selectedItems = {};
    this.sotingID = 0;
    this.successHandler = successHandler;

    this.init();
  }

  getSorting(){
    const result = [];

    for(const key in this.sortingData) {
      const {selectCondition, selectSortType} = this.sortingData[key];
      result.push({
        columnCode: selectCondition.value,
        reverse: selectSortType.value
      });
    }

    return result;
  }

  getConditions(){
    const result = [];
    let number = 0;

    for(const column_code in this.selectedItems) {
      switch (column_code) {
        case 'subject':
        case 'number':
        case 'base_number':
        case 'correspondent':
        case 'correspondent_org': {
          const {inputContains, inputNotContain} = this.selectedItems[column_code];
          if(!inputContains.values.length && !inputNotContain.values.length) return false;
          const condition_values = [];
          let cNumber = 0;

          if(inputContains.values) {
            inputContains.values.forEach(value => {
              condition_values.push({number: cNumber, value});
              cNumber++;
            });
          }

          condition_values.push({number: cNumber, value: ""});
          cNumber++;

          if(inputNotContain.values) {
            inputNotContain.values.forEach(value => {
              condition_values.push({number: cNumber, value});
              cNumber++;
            });
          }

          result.push({number, column_code, condition_values});
          number++;

          break;
        }
        case 'author':
        case 'reg_user':
        case 'user': {
          if(this.selectedItems[column_code].container.attr('user-ids') == '') return false;
          const condition_values = [];
          let cNumber = 0;

          this.selectedItems[column_code].container.attr('user-ids').split(';').forEach(value => {
            condition_values.push({number: cNumber, value});
            cNumber++;
          });

          result.push({number, column_code, condition_values});
          break;
        }
        case 'control':
        case 'defective':
        case 'active':
        case 'hidden': {
          result.push({
            number,
            column_code,
            condition_values: [{number: 0, value: this.selectedItems[column_code].val()}]
          });
          break;
        }
        case 'reg_date':
        case 'base_date': {
          const {select, dateInputStart, dateInputStop} = this.selectedItems[column_code];
          const condition_values = [];
          if(select.val() == '1') {
            if(dateInputStart.val() == '' || dateInputStop.val() == '') return false;
            condition_values.push({number: 0, value: '1'});
            condition_values.push({number: 1, value: dateInputStart.val()});
            condition_values.push({number: 2, value: dateInputStop.val()});
          } else {
            condition_values.push({number: 0, value: select.val()});
          }
          result.push({number, column_code, condition_values});
          break;
        }
        case 'doc_type':
        case 'status': {
          if(!this.selectedItems[column_code].value.length) return false;
          const condition_values = [];
          let cNumber = 0;

          this.selectedItems[column_code].value.forEach(item => {
            condition_values.push({number: cNumber, value: item.value});
            cNumber++;
          });

          result.push({number, column_code, condition_values});
          break;
        }
        case 'duration': {
          const {durationInputStart, durationInputStop} = this.selectedItems[column_code];
          if(durationInputStart.val() == '' || durationInputStop.val() == '') return false;
          if(durationInputStart.val() < 1 || durationInputStop.val() < 1) return false;

          const condition_values = [];
          condition_values.push({number: 0, value: durationInputStart.val()});
          condition_values.push({number: 1, value: durationInputStop.val()});

          result.push({number, column_code, condition_values});
          break;
        }
      }
    }

    return result;
  }

  getURLBody(){
    try {
      if(!this.inputFilterName || this.inputFilterName.val().trim() == '') throw new Error('Введите название');

      const conditions = this.getConditions();
      if(!conditions) throw new Error('Введите правильно все условия. Пустые условия не допускаются.');

      const sorting = this.getSorting();

      const body = {
        name: this.inputFilterName.val(),
        filterType: this.filterType,
        conditions, sorting,
        columns_default_settings: false,
        default_sorting: false,
        parentFilterID: null,
        parentObjectID: null
      };

      if(this.filterType == 'REGISTER_FILTER' && this.objectID) {
        body.default_sorting = this.sortSettingsDefault.prop('checked');
      }

      if(this.event == 'edit') {
        if(this.filterID) body.filterID = this.filterID;
      } else {
        if(this.filterID) body.parentFilterID = this.filterID;
        if(this.objectID) body.parentObjectID = this.objectID;
      }

      return body;
    } catch (err) {
      showMessage(i18n.tr(err.message), 'error');
    }
  }

  async saveFilter() {
    try {
      const body = this.getURLBody();
      if(!body) return;

      Cons.showLoader();
      const saveResult = await appAPI.documentFilterSave(body);
      if(!saveResult) throw new Error(i18n.tr('Ошибка сохранения фильтра'));

      this.dialog.dialog("close");
      this.successHandler(saveResult, this.treeItem.id, this.event);

      Cons.hideLoader();
    } catch (err) {
      Cons.hideLoader();
      showMessage(err.message, 'error');
    }
  }

  async addFilterItem(condition){
    const listItems = this.selectChanges.listItems;
    const {column_code, condition_values = []} = condition;
    const {title} = listItems.find(x => x.value == column_code);

    this.selectChanges.container.find('.select-items').find(`[data-value="${column_code}"]`).hide();
    if(!this.selectedItems.hasOwnProperty(column_code)) this.selectedItems[column_code] = null;

    switch (column_code) {
      case 'subject':
      case 'number':
      case 'base_number':
      case 'correspondent':
      case 'correspondent_org': {
        const {table, buttonDelete, inputContains, inputNotContain} = docFilterComponents.getInputsComponent(title, condition_values);
        this.panelFiltersData.append(table);

        this.selectedItems[column_code] = {inputContains, inputNotContain};

        buttonDelete.on('click', e => {
          e.preventDefault();
          table.remove();
          this.selectChanges.container.find('.select-items').find(`[data-value="${column_code}"]`).show();
          delete this.selectedItems[column_code];
        });

        break;
      }
      case 'author':
      case 'reg_user':
      case 'user': {
        const {table, buttonDelete, inputUsers} = await docFilterComponents.getUserChooserComponent(title, column_code, condition_values);
        this.panelFiltersData.append(table);

        this.selectedItems[column_code] = inputUsers;

        buttonDelete.on('click', e => {
          e.preventDefault();
          table.remove();
          this.selectChanges.container.find('.select-items').find(`[data-value="${column_code}"]`).show();
          delete this.selectedItems[column_code];
        });
        break;
      }
      case 'control':
      case 'defective':
      case 'active':
      case 'hidden': {

        const items = {
          control: [
            {title: "Только контрольные", value: "1"},
            {title: "Без контрольных", value: "2"}
          ],
          defective: [
            {title: "Все, включая бракованные", value: "1"},
            {title: "Только бракованные", value: "2"}
          ],
          active: [
            {title: "Только в процессе", value: "true"},
            {title: "Без документов в процессе", value: "false"}
          ],
          hidden: [
            {title: "Все, включая скрытые", value: "0"},
            {title: "Только скрытые", value: "1"}
          ]
        };

        const {table, buttonDelete, select} = docFilterComponents.getSelectComponent(title, column_code, condition_values, items[column_code]);
        this.panelFiltersData.append(table);

        this.selectedItems[column_code] = select;

        buttonDelete.on('click', e => {
          e.preventDefault();
          table.remove();
          this.selectChanges.container.find('.select-items').find(`[data-value="${column_code}"]`).show();
          delete this.selectedItems[column_code];
        });
        break;
      }

      case 'reg_date':
      case 'base_date': {
        const {table, buttonDelete, select, dateInputStart, dateInputStop} = docFilterComponents.getInputDateComponent(title, column_code, condition_values);
        this.panelFiltersData.append(table);

        this.selectedItems[column_code] = {select, dateInputStart, dateInputStop};

        buttonDelete.on('click', e => {
          e.preventDefault();
          table.remove();
          this.selectChanges.container.find('.select-items').find(`[data-value="${column_code}"]`).show();
          delete this.selectedItems[column_code];
        });
        break;
      }

      case 'doc_type':
      case 'status': {
        let items;
        if(column_code == 'doc_type') {
          items = this.doctypes.map(x => ({title: x.name, value: x.typeID}));
        } else {
          items = [
            {title: "Черновик", value: "5"},
            {title: "На регистрации", value: "0"},
            {title: "На исполнении", value: "1"},
            {title: "Просроченные", value: "4"},
            {title: "Завершен", value: "2"},
            {title: "Отклонён", value: "6"}
          ];
        }

        const {table, buttonDelete, select} = docFilterComponents.getCustomSelectMultipleComponent(title, column_code, condition_values, items);
        this.panelFiltersData.append(table);

        this.selectedItems[column_code] = select;

        buttonDelete.on('click', e => {
          e.preventDefault();
          table.remove();
          this.selectChanges.container.find('.select-items').find(`[data-value="${column_code}"]`).show();
          delete this.selectedItems[column_code];
        });

        break;
      }

      case 'duration': {
        const {table, buttonDelete, durationInputStart, durationInputStop} = docFilterComponents.getInputDurationComponent(title, column_code, condition_values);
        this.panelFiltersData.append(table);

        this.selectedItems[column_code] = {durationInputStart, durationInputStop};

        buttonDelete.on('click', e => {
          e.preventDefault();
          table.remove();
          this.selectChanges.container.find('.select-items').find(`[data-value="${column_code}"]`).show();
          delete this.selectedItems[column_code];
        });

        break;
      }
    }
  }

  addSortingItem(item, index = 0, sortingAll = 0){
    const defaulValues = {};
    const rowUUID = this.sotingID;

    if(item) {
      const {columnCode, reverse} = item;
      defaulValues.condition = columnCode;
      defaulValues.sortType = reverse;
    }

    const {table, buttonDelete, selectCondition, selectSortType, selectAdd} = docFilterComponents.getSortingComponent(this.sortingItems, defaulValues);

    if(item && (index + 1) < sortingAll) selectAdd.value = 'addSort';

    if(this.filterType == 'REGISTER_FILTER' && this.objectID) {
      selectCondition.disabled = this.sortSettingsDefault.prop('checked');
      selectSortType.disabled = this.sortSettingsDefault.prop('checked');
      selectAdd.disabled = this.sortSettingsDefault.prop('checked');
    }

    if(!Object.keys(this.sortingData).length) buttonDelete.hide();

    this.panelSorting.append(table);

    this.sortingData[rowUUID] = {selectCondition, selectSortType, selectAdd};

    buttonDelete.on('click', e => {
      e.preventDefault();
      table.remove();
      delete this.sortingData[rowUUID];
    });

    selectAdd.container.on('selected', e => {
      if(e.eventParam == 'addSort') {
        for(const key in this.sortingData) {
          this.sortingData[key].selectAdd.value = 'addSort';
        }
        this.addSortingItem();
      }
    });

    this.sotingID++;
  }

  renderFilterItems(){
    const {conditions = [], sorting = []} = this.filterInfo;

    if(!sorting.length) {
      sorting.push({
        columnCode: "reg_date",
        reverse: true
      });
    }

    conditions.forEach(item => this.addFilterItem(item));
    sorting.forEach((item, i) => this.addSortingItem(item, i, sorting.length));
  }

  addListeners(){
    this.buttonSaveFilter.on('click', e => {
      e.preventDefault();
      this.saveFilter();
    });

    this.selectChanges.container.on('selected', e => {
      const {value} = e.eventParam;
      if(value != 'empty') {
        this.addFilterItem({column_code: value});
        this.selectChanges.value = 'empty';
      }
    });

    if(this.filterType == 'REGISTER_FILTER' && this.objectID) {
      this.sortSettingsDefault.on('change', e => {
        for(const key in this.sortingData) {
          this.sortingData[key].selectCondition.disabled = e.target.checked;
          this.sortingData[key].selectSortType.disabled = e.target.checked;
          this.sortingData[key].selectAdd.disabled = e.target.checked;
        }
      });
    }
  }

  getSelectChanges(){
    const items = [
      {title: "Добавить условие", value: "empty"},
      {title: "Краткое содержание", value: "subject"},
      {title: "Номер", value: "number"},
      {title: "Тип документа", value: "doc_type"},
      {title: "Статус", value: "status"},
      {title: "Дата регистрации", value: "reg_date"},
      {title: "Номер исх.", value: "base_number"},
      {title: "Дата исх.", value: "base_date"},
      {title: "Корреспондент", value: "correspondent"},
      {title: "Корреспондент (орг)", value: "correspondent_org"},
      {title: "Автор", value: "author"},
      {title: "Зарегистрировал", value: "reg_user"},
      {title: "Исполнитель", value: "user"},
      {title: "Длительность (раб.дн)", value: "duration"},
      {title: "Контрольные", value: "control"},
      {title: "Бракованные", value: "defective"},
      {title: "В процессе", value: "active"}
    ];

    if(["ALL_USER_DOCUMENTS", "USER_OWN_DOCUMENTS", "USER_RECEIVED_DOCUMENTS", "USER_SENT_DOCUMENTS"].includes(this.filterType)) {
      items.push({title: "Скрытые", value: "hidden"});
    }

    const select = new CustomSelect({items, defaultValue: 'empty'});

    select.container.css({
      'width': 'calc(100% - 150px)',
      'border-radius': '3px'
    });

    select.container.find('.select-search-input').css('border-radius', '3px');

    return select;
  }

  openDialog(){
    const panelContent = $('<div>', {class: 'documents_filter_panel_content'});
    const panelButtons = $('<div>', {class: 'documents_filter_panel_buttons'});

    this.buttonSaveFilter = $(`<button class="uk-button uk-button-primary uk-button-small" type="button">${i18n.tr("Сохранить")}</button>`);

    this.panelFilters = $('<div>', {class: 'panel_filters'});
    this.panelSorting = $('<div>', {class: 'panel_sorting'});

    this.panelFiltersAction = $('<div>', {class: 'panel_filters_action'});
    this.panelFiltersData = $('<div>', {class: 'panel_filters_data'});

    this.selectChanges = this.getSelectChanges();
    this.inputFilterName = $('<input>', {id: 'document_filter_name', class: 'uk-input', type: 'text', style: 'border-radius: 3px;'});

    this.panelFiltersAction.append(this.selectChanges.container, this.inputFilterName);
    this.panelFilters.append(this.panelFiltersAction, this.panelFiltersData);

    if(this.event == 'create') {
      panelContent.append(this.panelFilters, this.panelSorting);
    } else {
      if (this.filterID) {
        panelContent.append(this.panelFilters, this.panelSorting);
      } else {
        this.panelSorting.addClass('single');
        panelContent.append(this.panelSorting);
      }
    }

    if(this.filterType == 'REGISTER_FILTER' && this.objectID) {
      const sortSettingsContainer = $('<div>', {style: "display: flex; gap: 5px; align-items: center; margin-bottom: 10px;"});
      const sortlabel = $('<label>', {class: 'uk-form-label', for: 'comp_sdi'});
      this.sortSettingsDefault = $('<input>', {type: 'checkbox', class: 'uk-checkbox', id: 'comp_sdi'});

      if(this.event == 'create') {
        this.sortSettingsDefault.prop('checked', true);
      } else {
        this.sortSettingsDefault.prop('checked', this.filterInfo.default_sorting);
      }

      sortlabel.text(i18n.tr("Использовать настройки по умолчанию"));
      sortSettingsContainer.append(this.sortSettingsDefault, sortlabel);

      this.panelSorting.append(sortSettingsContainer);
    }

    this.panelSorting.append(`<span class="fonts uk-text-bold">${i18n.tr('Сортировать по полю')}</span>`);

    panelButtons.append(this.buttonSaveFilter);

    this.dialog = $('<div>', {class: 'documents_filter_modal'});
    this.dialog.append(panelContent, panelButtons);

    this.dialog.dialog({
      modal: true,
      width: 650,
      height: 750,
      title: this.event == 'edit' ? i18n.tr('Редактирование фильтра') : i18n.tr('Создание фильтра'),
      show: {effect: 'fade', duration: 300},
      hide: {effect: 'fade', duration: 300},
      open: () => {
        if(this.event == 'edit') {
          this.inputFilterName.val(this.filterInfo.name);
          this.renderFilterItems();
        } else {
          this.addSortingItem();
        }
      },
      close: function() {
        $(this).remove();
      }
    });

    this.addListeners();
  }

  async init(){
    Cons.showLoader();

    try {
      const {filterType, filterID, objectID} = this.treeItem;

      this.filterType = filterType;
      this.filterID = filterID;
      this.objectID = objectID;

      const params = new URLSearchParams();
      if(this.filterType) params.append("filterType", this.filterType);
      if(this.objectID) params.append("objectID", this.objectID);
      if(this.filterID) params.append("filterID", this.filterID);

      this.sortingData = {};

      this.sortingItems = [
        {title: "Краткое содержание", value: "subject"},
        {title: "Номер", value: "number"},
        {title: "Тип документа", value: "doc_type"},
        {title: "Статус", value: "status"},
        {title: "Дата регистрации", value: "reg_date"},
        {title: "Автор", value: "author"},
        {title: "Зарегистрировал", value: "reg_user"},
        {title: "Исполнитель", value: "user"},
        {title: "Длительность", value: "duration"},
      ];

      if(["ALL_USER_DOCUMENTS", "USER_OWN_DOCUMENTS", "USER_RECEIVED_DOCUMENTS", "USER_SENT_DOCUMENTS"].includes(this.filterType)) {
        this.sortingItems.push({title: "Дата создания", value: "createDate"});
        this.sortingItems.push({title: "Дата получения/отправки", value: "lastdate"});
      }

      this.filterInfo = await appAPI.documentFilterInfo(params.toString());
      this.doctypes = await appAPI.getDoctypes();

      Cons.hideLoader();
      this.openDialog();
    } catch (e) {
      Cons.hideLoader();
      console.log(`ERROR DocumentsFilter: ${e.message}`);
    }
  }
}
