const getInputDate = d => $(`<input type="date" class="uk-input" style="max-width: 130px;" value="${d || ''}">`);

const createSelectComponent = (items, defaultValue = null, multiple = false) => {
  const container = $('<div>', {class: 'uk-form-controls'});
  const select = $('<select class="uk-select" style="min-width: 100px;">');

  if(multiple) select.attr('multiple', 'multiple');
  if(items) items.sort((a,b) => a.value - b.value)
  .forEach(item => {
    const option = $(`<option value="${item.value}" title="${item.title}">${item.title}</option>`);
    if(defaultValue == item.value) option.prop('selected', true);
    if(item.hasOwnProperty('disabled')) option.prop('disabled', true);
    if(item.hasOwnProperty('selected')) option.prop('selected', true);

    select.append(option);
  });

  container.append(select);

  return {container, select};
}

const getFullName = person => {
  const {firstname, lastname, patronymic} = person;
  const fio = lastname.substr(0, 1).toUpperCase() + lastname.substr(1) + ' ' + firstname.substr(0, 1).toUpperCase() + '.';
  return patronymic ? fio + ' ' + patronymic.substr(0, 1).toUpperCase() + '.' : fio;
}

const getInputsComponent = (title, condition_values) => {
  const valueContains = [], valueNotContains = []; let isNotContains = false;

  for(let i = 0; i < condition_values.length; i++) {
    const {value} = condition_values[i];
    if(value == "") {
      isNotContains = true;
      continue;
    }
    isNotContains ? valueNotContains.push(value) : valueContains.push(value);
  }

  const table = $('<table>', {style: "width: 100%;"});
  const tBody = $('<tbody>');
  const tr1 = $('<tr>'), tr2 = $('<tr>'), tr3 = $('<tr>');

  const buttonDelete =  $('<a href="" uk-icon="trash"></a>');
  tr1.append(
    $('<td>', {colspan: "2"}).append(
      $('<div>', {style: "display: flex; justify-content: space-between;"}).append(
        `<span class="uk-text-bold">${i18n.tr(title)}</span>`,
        buttonDelete
      )
    )
  );

  const inputContains = new InputTag({items: valueContains});
  const inputNotContain = new InputTag({items: valueNotContains});

  inputContains.container.css('max-width', '410px');
  inputNotContain.container.css('max-width', '410px');

  tr2.append(`<td>${i18n.tr('Содержит')}</td>`, $('<td>').append(inputContains.container));
  tr3.append(`<td>${i18n.tr('Не содержит')}</td>`, $('<td>').append(inputNotContain.container));

  tBody.append(tr1, tr2, tr3);
  table.append(`<colgroup><col style="width: 140px;"></colgroup>`, tBody);

  return {table, buttonDelete, inputContains, inputNotContain};
}

const getUserChooserComponent = async (title, column_code, condition_values) => {
  const table = $('<table>', {style: "width: 100%;"});
  const tBody = $('<tbody>');
  const colgroup = $('<colgroup>');
  const buttonDelete =  $('<a href="" uk-icon="trash"></a>');
  const tr = $('<tr>');
  const tdUser = $('<td>');

  let selectUsers = [];
  for(let i = 0; i < condition_values.length; i++) {
    const userInfo = await AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/userchooser/getUserInfo?userID=${condition_values[i].value}&locale=${AS.OPTIONS.locale}`);
    selectUsers.push(userInfo[0]);
  }

  const defaultValues = selectUsers.map(x => getFullName(x));
  const userIDs = condition_values.map(x => x.value).join(';');
  const buttonUsers = $('<a class="uk-form-icon uk-form-icon-flip" href="#" uk-icon="icon: users"></a>');

  const inputUsers = new InputTag({
    items: defaultValues,
    manualInput: false,
    placeholder: ' ',
    tagDeleteHandler: async (text) => {
      const userIDs = inputUsers.container.attr('user-ids').split(';');
      const userNames = inputUsers.container.attr('user-names').split(';');
      const index = userNames.findIndex(x => x == text);
      userIDs.splice(index, 1);
      userNames.splice(index, 1);
      inputUsers.container.attr('user-ids', userIDs.join(';')).attr('user-names', userNames.join(';'));
      selectUsers = [];
      for(let i = 0; i < userIDs.length; i++) {
        const userInfo = await AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/userchooser/getUserInfo?userID=${userIDs[i]}&locale=${AS.OPTIONS.locale}`);
        selectUsers.push(userInfo[0]);
      }
    }
  });

  inputUsers.container.attr('id', `select-user-${column_code}`).attr('user-ids', userIDs).attr('user-names', defaultValues.join(';'));
  inputUsers.container.find('.custom_tag_container').css('width', 'calc(100% - 40px)');

  buttonUsers.on('click', e => {
    e.preventDefault();
    e.target.blur();
    e.stopPropagation();

    AS.SERVICES.showUserChooserDialog(selectUsers, true, null, null, null, null, AS.OPTIONS.locale, users => {
      selectUsers = users;
      let userIDs = users.map(x => x.personID).join(';');
      let userNames = selectUsers.map(x => getFullName(x));
      inputUsers.container.attr('user-ids', userIDs).attr('user-names', userNames.join(';'));
      inputUsers.values = userNames;
    });
  });

  tdUser.append($('<div class="uk-inline uk-width-1-1">').append(buttonUsers).append(inputUsers.container));

  tr.append(`<td class="uk-text-bold">${i18n.tr(title)}</td>`, tdUser, $('<td style="text-align: end;">').append(buttonDelete));
  colgroup.append('<col style="width: 140px;">', '<col>', '<col style="width: 40px;">');
  tBody.append(tr);
  table.append(colgroup, tBody);

  return {table, buttonDelete, inputUsers};
}

const getSelectComponent = (title, column_code, condition_values, items) => {
  const table = $('<table>', {style: "width: 100%;"});
  const tBody = $('<tbody>');
  const colgroup = $('<colgroup>');
  const buttonDelete =  $('<a href="" uk-icon="trash"></a>');
  const tr = $('<tr>');
  const tdSelect = $('<td>');

  let defaultValue = items[0].value;
  if(condition_values && condition_values.length) {
    defaultValue = condition_values[0].value;
  }

  const {container: selectContainer, select} = createSelectComponent(items, defaultValue);

  tdSelect.append(selectContainer);
  tr.append(
    `<td>${i18n.tr("Отображать")}</td>`,
    tdSelect,
    $('<td style="text-align: end;">').append(buttonDelete)
  );
  colgroup.append('<col style="width: 140px;">', '<col>', '<col style="width: 40px;">');
  tBody.append(`<tr><td class="uk-text-bold" colspan="3">${i18n.tr(title)}</td></tr>`, tr);
  table.append(colgroup, tBody);

  return {table, buttonDelete, select};
}

const getInputDateComponent = (title, column_code, condition_values) => {
  const table = $('<table>', {style: "width: 100%;"});
  const tBody = $('<tbody>');
  const colgroup = $('<colgroup>');
  const buttonDelete =  $('<a href="" uk-icon="trash"></a>');
  const tr = $('<tr>');
  const tdSelect = $('<td>');

  const items = [
    {title: "Произвольный", value: "1"},
    {title: "Неделя", value: "7"},
    {title: "Месяц", value: "8"},
    {title: "Квартал", value: "9"}
  ];

  let defaultValue = items[0].value;
  if(condition_values && condition_values.length) {
    defaultValue = condition_values[0].value;
  }

  const {container: selectContainer, select} = createSelectComponent(items, defaultValue);

  let dateStart = null, dateStop = null;
  if(defaultValue == '1' && condition_values && condition_values.length) {
    dateStart = condition_values[1].value;
    dateStop = condition_values[2].value;
  }

  const dateInputStart = getInputDate(dateStart);
  const dateInputStop = getInputDate(dateStop);

  select.on('change', e => {
    const value = select.val();
    if(value == '1') {
      dateInputStart.prop('disabled', false);
      dateInputStop.prop('disabled', false);
      dateInputStart.val(null);
      dateInputStop.val(null);
    } else {
      dateInputStart.prop('disabled', true);
      dateInputStop.prop('disabled', true);
    }
  });

  tdSelect.append(selectContainer);
  tr.append(
    `<td class="uk-text-bold">${i18n.tr(title)}</td>`,
    tdSelect,
    $('<td>').append(dateInputStart),
    `<td>-</td>`,
    $('<td>').append(dateInputStop),
    $('<td style="text-align: end;">').append(buttonDelete)
  );
  colgroup.append('<col style="width: 140px;">', '<col>', '<col>', '<col>', '<col>', '<col style="width: 40px;">');
  tBody.append(tr);
  table.append(colgroup, tBody);

  return {table, buttonDelete, select, dateInputStart, dateInputStop};
}

const getInputDurationComponent = (title, column_code, condition_values) => {
  const table = $('<table>', {style: "width: 100%;"});
  const tBody = $('<tbody>');
  const colgroup = $('<colgroup>');
  const buttonDelete =  $('<a href="" uk-icon="trash"></a>');
  const tr = $('<tr>');

  const durationInputStart = $(`<input type="number" class="uk-input">`);
  const durationInputStop = $(`<input type="number" class="uk-input">`);

  if(condition_values && condition_values.length) {
    durationInputStart.val(condition_values[0].value);
    durationInputStop.val(condition_values[1].value);
  }

  tr.append(
    `<td class="uk-text-bold">${i18n.tr("Длительность")}</td>`,
    $('<td>').append(durationInputStart),
    `<td style="text-align: center;">-</td>`,
    $('<td>').append(durationInputStop),
    $('<td style="text-align: end;">').append(buttonDelete)
  );
  colgroup.append('<col style="width: 140px;">', '<col>', '<col style="width: 40px;">', '<col>', '<col style="width: 40px;">');
  tBody.append(tr);
  table.append(colgroup, tBody);

  return {table, buttonDelete, durationInputStart, durationInputStop};
}

const getCustomSelectMultipleComponent = (title, column_code, condition_values, items) => {
  const table = $('<table>', {style: "width: 100%;"});
  const tBody = $('<tbody>');
  const colgroup = $('<colgroup>');
  const buttonDelete =  $('<a href="" uk-icon="trash"></a>');
  const tr = $('<tr>');
  const tdSelect = $('<td>');

  let defaultValue = null;
  if(condition_values && condition_values.length) {
    defaultValue = condition_values.map(x => x.value);
  }

  const select = new CustomSelect({items, multiple: true, defaultValue});

  tdSelect.append(select.container);
  tr.append(
    `<td class="uk-text-bold">${i18n.tr(title)}</td>`,
    tdSelect,
    $('<td style="text-align: end;">').append(buttonDelete)
  );
  colgroup.append('<col style="width: 140px;">', '<col>', '<col style="width: 40px;">');
  tBody.append(tr);
  table.append(colgroup, tBody);

  return {table, buttonDelete, select};
}

const getSortingComponent = (items, defaulValues = {}, addItemHandler) => {
  const table = $('<table>', {style: "width: 100%;"});
  const tBody = $('<tbody>');
  const colgroup = $('<colgroup>');
  const buttonDelete =  $('<a href="" uk-icon="trash"></a>');
  const tr = $('<tr>');

  const selectCondition = new ComboBox(items, "df-component-input", "df-component-button");
  if(defaulValues.hasOwnProperty('condition')) selectCondition.value = defaulValues.condition;

  const selectSortType = new ComboBox([
    {title: i18n.tr("По возрастанию"), value: false},
    {title: i18n.tr("По убыванию"), value: true}
  ], "df-component-input", "df-component-button");
  if(defaulValues.hasOwnProperty('sortType')) selectSortType.value = defaulValues.sortType;

  const selectAdd = new ComboBox([
    {title: "", value: "empty"},
    {title: i18n.tr("Затем по"), value: "addSort"}
  ], "df-component-input", "df-component-button");

  tr.append(
    $('<td>').append(selectCondition.container),
    $('<td>').append(selectSortType.container),
    $('<td>').append(selectAdd.container),
    $('<td>').append(buttonDelete)
  );
  colgroup.append('<col>', '<col>', '<col>', '<col style="width: 40px;">');
  tBody.append(tr);
  table.append(colgroup, tBody);

  return {table, buttonDelete, selectCondition, selectSortType, selectAdd};
}

this.docFilterComponents = {
  getInputsComponent,
  getUserChooserComponent,
  getSelectComponent,
  getInputDateComponent,
  getInputDurationComponent,
  getCustomSelectMultipleComponent,
  getSortingComponent
}
