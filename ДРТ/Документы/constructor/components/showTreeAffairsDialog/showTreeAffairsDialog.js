const parseTreeData = item => {
  item.id = item.objectID;
  item.text = item.name;
  item.hasChildren == "true" ? item.hasChildren = true : item.hasChildren = false;
  item.children = [];

  return item;
}

const getTreeData = async () => {
  let result = await appAPI.getDocumentsFilters('filterType=DOCFILE_FILTER');
  result = result.filter(x => !x.filterID);

  const f = async r => {
    r = r.filter(x => !x.filterID);

    for(let i = 0; i < r.length; i++) {
      const item = parseTreeData(r[i]);
      const {filterType, objectID, filterID, hasChildren} = item;
      if(filterID) continue;

      if(hasChildren) {
        const params = new URLSearchParams();
        if(filterType) params.append("filterType", filterType);
        if(objectID) params.append("objectID", objectID);
        if(filterID) params.append("filterID", filterID);

        item.children = await appAPI.getDocumentsFilters(params.toString());
        await f(item.children);
      }
    }
  }

  await f(result);

  return result;
}

this.showTreeAffairsDialog = succesHandler => {
  try {
    Cons.showLoader();

    getTreeData().then(treeData => {
      const customTree = new TreeChooser(treeData, 'Дела', 400, 500, 'Принять', 'Отмена');
      customTree.treeContainer.addClass('tree_docfile_filter_icon');
      customTree.showDialog();

      customTree.dialogContainer.on(AS.FORMS.BasicChooserEvent.applyClicked, e => {
        if(succesHandler) succesHandler({treeData, selectItem: e.eventParam});
      });

      Cons.hideLoader();
    });
  } catch (err) {
    Cons.hideLoader();
    console.log(`ERROR showTreeAffairsDialog: ${err.message}`);
  }
}
