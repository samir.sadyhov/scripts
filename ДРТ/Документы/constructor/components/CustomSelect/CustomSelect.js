this.CustomSelect = class {
  constructor(param) {
    this.items = param.items;
    this.placeholder = i18n.tr(param.placeholder || 'Выбор...');
    this.search = param.search || false;
    this.defaultValue = param.defaultValue || null;
    this.multiple = param.multiple || false;
    this.selectItem = null;
    this.status = 'hide';
    this.init();
  }

  createSelectContainer() {
    this.selectContainer = $('<div>', {class: 'custom-select-container'});
    this.selected = $('<div>', {class: 'select-selected'});
    this.selectItems = $('<div>', {class: 'select-items select-hide'});
    this.selectContainer.append(this.selected, this.selectItems);

    if(this.multiple) {
      this.selectContainer.addClass('multiple');
    } else {
      this.selected.text(this.placeholder);
    }

    this.selected.on('click', e => {
      if(this.multiple && !$(e.target).is("div")) return;
      this.status === 'hide' ? this.show() : this.hide()
    });
    this.selectContainer.on('close-select', e => this.hide());

    if(this.search) {
      const me = this;
      this.searchInput = $('<input>', {class: 'select-search-input select-hide', placeholder: i18n.tr('Поиск...')});
      this.selectContainer.append(this.searchInput);
      this.searchInput.keyup(function(){
        $.each(me.selectItems.find("div"), function() {
          if($(this).text().toLowerCase().indexOf(me.searchInput.val().toLowerCase()) === -1) $(this).hide();
          else $(this).show();
        });
      });
    }
  }

  renderTagItem(item, menuItem){
    item.tag = $('<div>', {class: 'tag_block'});
    const deleteTagButton = $('<a class="uk-icon" href="javascript:void(0);" uk-icon="icon: close"></a>');

    item.tag.append(`<span>${i18n.tr(item.title)}</span>`, deleteTagButton);
    this.selected.append(item.tag);
    menuItem.addClass('selected');

    deleteTagButton.on('click', e => {
      const msgConfirm = i18n.tr('Вы действительно хотите удалить тег {0}?').replace('{0}', i18n.tr(item.title));
      if(confirm(msgConfirm)) {
        item.tag.remove();
        item.selected = false;
        menuItem.removeClass('selected');
      }
    });
  }

  createItems() {
    this.items.forEach(item => {
      const menuItem = $(`<div data-value="${item.value}" title="${i18n.tr(item.title)}">${i18n.tr(item.title)}</div>`);

      if(this.multiple && item.selected) this.renderTagItem(item, menuItem);

      menuItem.on('click', e => {
        if(this.multiple) {
          item.selected = !item.selected;
          if(item.selected) {
            this.renderTagItem(item, menuItem);
          } else {
            menuItem.removeClass('selected');
            item.tag.remove();
          }
        } else {
          this.hide();
          this.selected.text(i18n.tr(item.title));
          this.selected.attr('title', i18n.tr(item.title));
          this.selectItem = item;

          this.selectContainer.trigger({
          	type: 'selected',
            eventParam: this.value
          });
        }
      });

      this.selectItems.append(menuItem);
    });
  }

  get listItems() {
    return this.items;
  }

  get container() {
    return this.selectContainer;
  }

  get value() {
    if(this.multiple) {
      return this.items.filter(x => x.selected);
    } else {
      return this.selectItem;
    }
  }

  set value(value) {
    if(this.multiple) return;

    const findVal = this.items.find(x => x.value == value);
    if (findVal) {
      this.selected.text(i18n.tr(findVal.title));
      this.selected.attr('title', i18n.tr(findVal.title));
      this.selectItem = findVal;
    } else {
      this.selectItem = null;
      this.selected.text(this.placeholder);
    }
  }

  hide() {
    this.status = 'hide';
    this.selected.removeClass('select-arrow-active');
    this.selectItems.addClass('select-hide');
    if(this.search) this.searchInput.addClass('select-hide');
  }

  show() {
    $('.custom-select-container').trigger('close-select');

    this.status = 'open';
    this.selected.addClass('select-arrow-active');
    this.selectItems.removeClass('select-hide');
    if(this.search) {
      this.searchInput.removeClass('select-hide').val('');
      $.each(this.selectItems.find("div"), function() {
        $(this).show();
      });
    }
  }

  init() {
    if(this.multiple) {
      this.items.map(x => {
        if(this.defaultValue && this.defaultValue.length) {
          x.selected = !!this.defaultValue.find(v => v == x.value);
        } else {
          x.selected = false;
        }
      });
    }

    this.createSelectContainer();
    this.createItems();

    if(!this.multiple && this.defaultValue) {
      const findVal = this.items.find(x => x.value == this.defaultValue);
      if (findVal) {
        this.selected.text(i18n.tr(findVal.title));
        this.selected.attr('title', i18n.tr(findVal.title));
        this.selectItem = findVal;
      }
    }
  }
}

const closeAllSelect = (e) => {
  if ($(e.target).closest('.custom-select-container').length == 0) $('.custom-select-container').trigger('close-select');
}
document.addEventListener("click", closeAllSelect);
