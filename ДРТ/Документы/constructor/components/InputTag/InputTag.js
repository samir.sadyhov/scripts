this.InputTag = class {
  #enterValues = [];
  #placeholder;
  #defaultItems;
  #inputContainer;
  #tagContainer;
  #placeholderContainer;
  #input;

  constructor(param) {
    this.#placeholder = i18n.tr(param.placeholder || "Введите значение и нажмите Enter");
    this.#defaultItems = param.items || [];
    this.manualInput = param.hasOwnProperty('manualInput') ? param.manualInput : true;
    this.tagDeleteHandler = param.tagDeleteHandler || null;

    this.#init();
  }

  get values(){
    return this.#enterValues;
  }

  set values(arr){
    this.#enterValues = [];
    this.#tagContainer.empty();
    arr.forEach(text => this.#createTag(text));
  }

  get container(){
    return this.#inputContainer;
  }

  #createTag(text){
    this.#enterValues.push(text);
    const tag = $('<div>', {class: 'tag_block'});
    const deleteTagButton = $('<a class="uk-icon" href="javascript:void(0);" uk-icon="icon: close"></a>');

    tag.append(`<span>${text}</span>`, deleteTagButton);
    this.#tagContainer.append(tag);

    this.#tagContainer.css('display', 'flex');
    this.#placeholderContainer.hide();
    if(this.manualInput) this.#input.blur();

    deleteTagButton.on('click', e => {
      const msgConfirm = i18n.tr('Вы действительно хотите удалить тег {0}?').replace('{0}', text);
      if(confirm(msgConfirm)) {
        this.#enterValues.splice(this.#enterValues.length - 1, 1);
        tag.remove();
        if(!this.#enterValues.length) {
          this.#placeholderContainer.show();
          this.#tagContainer.css('display', 'none');
          if(this.manualInput) this.#input.blur();
        }
        if(this.tagDeleteHandler && typeof this.tagDeleteHandler == 'function') this.tagDeleteHandler(text);
      }
    });
  }

  #init(){
    this.#inputContainer = $('<div>', {class: 'custom_input_tag_container'});
    this.#input = $('<input>', {class: 'custom_input', type: 'text', style: 'display: none;'});
    this.#tagContainer = $('<div>', {class: 'custom_tag_container', style: 'display: none;'});
    this.#placeholderContainer = $('<div>', {class: 'custom_placeholder'});

    this.#placeholderContainer.text(this.#placeholder);
    this.#inputContainer.append(this.#placeholderContainer, this.#tagContainer, this.#input);

    this.#inputContainer.on('click', e => {
      this.#placeholderContainer.hide();
      if(this.manualInput) {
        this.#input.show();
        this.#input.focus();
      }
    });

    if(this.manualInput) {
      this.#input.on('blur', e => {
        this.#input.hide();
        if(!this.#enterValues.length) {
          this.#placeholderContainer.show();
          this.#tagContainer.css('display', 'none');
        }
      }).on('keyup', e => {
        if (e.keyCode === 13) {
          e.preventDefault();
          this.#createTag(this.#input.val());
          this.#input.val(null);
          this.#input.hide();
        }
      });
    }

    if(this.#defaultItems && this.#defaultItems.length) {
      this.#defaultItems.forEach(text => this.#createTag(text));
    }
  }
}
