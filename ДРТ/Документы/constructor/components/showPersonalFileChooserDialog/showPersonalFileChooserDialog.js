const parseTreeData = item => {
  item.id = item.folderID;
  item.text = item.name;
  item.hasChildren = !!item.includedFolders.length;
  item.children = item.includedFolders;

  return item;
}


const getTreeData = async () => {
  const result = await appAPI.getPersonnelFiles();

  const f = r => {
    for(let i = 0; i < r.length; i++) {
      const item = parseTreeData(r[i]);
      if(item.hasChildren) f(item.children);
    }
  }

  f(result);

  return result;
}

this.showPersonalFileChooserDialog = succesHandler => {
  try {
    Cons.showLoader();

    getTreeData().then(treeData => {
      const customTree = new TreeChooser(treeData, 'Выбор файла', 600, 400, 'Готово');
      customTree.treeContainer.addClass('tree_personal_file_icon');
      customTree.showDialog();

      customTree.dialogContainer.on(AS.FORMS.BasicChooserEvent.applyClicked, e => {
        if(succesHandler) succesHandler({treeData, selectItem: e.eventParam});
      });

      Cons.hideLoader();
    });
  } catch (err) {
    Cons.hideLoader();
    console.log(`ERROR showPersonalFileChooserDialog: ${err.message}`);
  }
}
