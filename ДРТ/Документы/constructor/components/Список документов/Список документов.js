const compContainer = $(`#${comp.code}`);
const componentContainer = compContainer.find(`.documents-component-container`);
const listContainer = componentContainer.find(`.documents-list-container`);
const paginatorContainer = componentContainer.find(`.documents-paginator-container`);
const detailsContainer = compContainer.find(`.documents-details-container`);

const Paginator = {
  countInPart: 0,
  rows: 0,
  currentPage: 1,
  pages: 0,

  update: function(){
    this.pages = Math.ceil(this.rows / this.countInPart),
    this.label.text(this.currentPage + ' / ' + this.pages);

    if(this.pages == 0) {
      this.bPrevious.attr('disabled', 'disabled');
      this.bNext.attr('disabled', 'disabled');
    } else {
      if(this.currentPage == 1) {
        this.bPrevious.attr('disabled', 'disabled');
        if (this.currentPage == this.pages) {
          this.bNext.attr('disabled', 'disabled');
        } else {
          this.bNext.removeAttr('disabled');
        }
      } else {
        this.bPrevious.removeAttr('disabled');
        if (this.currentPage == this.pages) {
          this.bNext.attr('disabled', 'disabled');
        } else {
          this.bNext.removeAttr('disabled');
        }
      }
    }
  },

  addListeners: function(){
    this.bNext.click(() => {
      this.currentPage++;
      this.update();
      Documents.createBody();
    });

    this.bPrevious.click(() => {
      this.currentPage--;
      this.update();
      Documents.createBody();
    });

    this.label.on('dblclick', e => {
      e.preventDefault();
      this.input.show();
      this.label.hide();

      this.input.on('blur', () => {
        this.label.show();
        this.input.hide();
      });

      this.input.val("" + this.currentPage);

      this.input.on('keypress', e => {
        if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) return false;
      });

      this.input.on('keydown', e => {
        if(e.which === 13) {
          let inputVal = +this.input.val();
          if(inputVal && inputVal != this.currentPage && inputVal <= this.pages && inputVal >= 1){
            this.currentPage = inputVal;
            this.update();
            Documents.createBody();
          }
          this.label.show();
          this.input.hide();
          this.input.off();
        }
      });
      this.input.focus();
    });
  },

  render: function(){
    this.container = $('<div>', {class: "pt-container"});
    this.paginator = $('<div>', {class: "pt-paginator"});
    this.pContent = $('<div>', {class: "pt-paginator-content"});
    this.bPrevious = $(`<button class="pt-previous" disabled="disabled" title="${i18n.tr('Назад')}">`);
    this.bNext = $(`<button class="pt-next" disabled="disabled" title="${i18n.tr('Вперед')}">`);
    this.label = $('<label>');
    this.input = $('<input type="text">');

    paginatorContainer.empty().append(this.container);

    this.pContent.append(this.label).append(this.input);
    this.paginator.append(this.bPrevious).append(this.pContent).append(this.bNext);
    this.container.append(this.paginator);
  },

  init: function(rows, countInPart){
    this.currentPage = 1;
    this.rows = rows;
    this.countInPart = countInPart;
    this.pages = Math.ceil(this.rows / this.countInPart);

    this.render();
    this.addListeners();
  }
};

const getDocuments = async params => {
  return new Promise(async resolve => {
    rest.synergyGet(`api/docflow/doc/documents?locale=${AS.OPTIONS.locale}${params ? '&'+params : ''}`, resolve, err => {
      console.log(`ERROR [ getDocuments ]: ${JSON.stringify(err)}`);
      resolve(null);
    });
  });
}

const getDocumentsXLSX = async params => {
  return new Promise(async resolve => {
    try {
      const {login, password} = Cons.creds;
      const auth = "Basic " + btoa(unescape(encodeURIComponent(`${login}:${password}`)));
      const url = `../Synergy/rest/api/docflow/documents/load?locale=${AS.OPTIONS.locale}${params ? '&'+params : ''}`;
      const response = await fetch(url, {method: 'GET', headers: {"Authorization": auth}});
      if(!response.ok) throw new Error(`HTTP: ${response.status}, message: ${response.statusText} [ ${response.text()} ]`);
      resolve(response.blob());
    } catch (err) {
      console.log(`ERROR [ getDocumentsXLSX ]: ${err.message}`);
      resolve(false);
    }
  });
}

const registerIcons = {
  resolution: `<svg xmlns="http://www.w3.org/2000/svg" height="18px" viewBox="0 0 24 24" width="20px" fill="#5f6368"><path d="M0 0h24v24H0z" fill="none"/><path d="M4 16v6h16v-6c0-1.1-.9-2-2-2H6c-1.1 0-2 .9-2 2zm14 2H6v-2h12v2zM12 2C9.24 2 7 4.24 7 7l5 7 5-7c0-2.76-2.24-5-5-5zm0 9L9 7c0-1.66 1.34-3 3-3s3 1.34 3 3l-3 4z"/></svg>`,

  attachments: `<svg xmlns="http://www.w3.org/2000/svg" height="18px" viewBox="0 0 24 24" width="20px" fill="#5f6368"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M16.5 6v11.5c0 2.21-1.79 4-4 4s-4-1.79-4-4V5c0-1.38 1.12-2.5 2.5-2.5s2.5 1.12 2.5 2.5v10.5c0 .55-.45 1-1 1s-1-.45-1-1V6H10v9.5c0 1.38 1.12 2.5 2.5 2.5s2.5-1.12 2.5-2.5V5c0-2.21-1.79-4-4-4S7 2.79 7 5v12.5c0 3.04 2.46 5.5 5.5 5.5s5.5-2.46 5.5-5.5V6h-1.5z"/></svg>`,

  control: `<svg xmlns="http://www.w3.org/2000/svg" height="18px" viewBox="0 0 24 24" width="20px" fill="#5f6368"><path d="M0 0h24v24H0z" fill="none"/><circle cx="12" cy="19" r="2"/><path d="M10 3h4v12h-4z"/></svg>`,

  in_progress: `<svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="18px" viewBox="0 0 24 24" width="20px" fill="#5f6368"><g><rect fill="none" height="24" width="24"/></g><g><g><g><path d="M11,5.08V2C6,2.5,2,6.81,2,12s4,9.5,9,10v-3.08c-3-0.48-6-3.4-6-6.92S8,5.56,11,5.08z M18.97,11H22c-0.47-5-4-8.53-9-9 v3.08C16,5.51,18.54,8,18.97,11z M13,18.92V22c5-0.47,8.53-4,9-9h-3.03C18.54,16,16,18.49,13,18.92z"/></g></g></g></svg>`,

  defective: `<svg xmlns="http://www.w3.org/2000/svg" height="18px" viewBox="0 0 24 24" width="20px" fill="#5f6368"><path d="M0 0h24v24H0z" fill="none"/><path d="M0 0h24v24H0zm0 0h24v24H0zm21 19c0 1.1-.9 2-2 2H5c-1.1 0-2-.9-2-2V5c0-1.1.9-2 2-2h14c1.1 0 2 .9 2 2" fill="none"/><path d="M21 5v6.59l-3-3.01-4 4.01-4-4-4 4-3-3.01V5c0-1.1.9-2 2-2h14c1.1 0 2 .9 2 2zm-3 6.42l3 3.01V19c0 1.1-.9 2-2 2H5c-1.1 0-2-.9-2-2v-6.58l3 2.99 4-4 4 4 4-3.99z"/></svg>`,

  registry: `<svg xmlns="http://www.w3.org/2000/svg" height="18px" viewBox="0 0 24 24" width="20px" fill="#5f6368"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M8 16h8v2H8zm0-4h8v2H8zm6-10H6c-1.1 0-2 .9-2 2v16c0 1.1.89 2 1.99 2H18c1.1 0 2-.9 2-2V8l-6-6zm4 18H6V4h7v5h5v11z"/></svg>`,

  default: `<svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24px" viewBox="0 0 24 24" width="24px" fill="#5f6368"><g><rect fill="none" height="24" width="24"/></g><g><g><g><path d="M19,19h2v2h-2V19z M19,17h2v-2h-2V17z M3,13h2v-2H3V13z M3,17h2v-2H3V17z M3,9h2V7H3V9z M3,5h2V3H3V5z M7,5h2V3H7V5z M15,21h2v-2h-2V21z M11,21h2v-2h-2V21z M15,21h2v-2h-2V21z M7,21h2v-2H7V21z M3,21h2v-2H3V21z M21,8c0-2.76-2.24-5-5-5h-5v2h5 c1.65,0,3,1.35,3,3v5h2V8z"/></g></g></g></svg>`,


  get: function(name) {
    if(!this.hasOwnProperty(name)) return this.default;
    return this[name];
  }
}

const getFullName = person => {
  const {firstname, lastname, patronymic} = person;
  const fio = lastname.substr(0, 1).toUpperCase() + lastname.substr(1) + ' ' + firstname.substr(0, 1).toUpperCase() + '.';
  return patronymic ? fio + ' ' + patronymic.substr(0, 1).toUpperCase() + '.' : fio;
}

const getUserChooserComponent = () => {
  let selectUsers = [];

  const fc = $('<div>', {class: 'uk-form-controls'});
  const buttonUsers = $('<a class="uk-form-icon uk-form-icon-flip" href="#" uk-icon="icon: users"></a>');

  const inputUsers = new InputTag({
    manualInput: false,
    placeholder: ' ',
    tagDeleteHandler: async (text) => {
      const userIDs = inputUsers.container.attr('user-ids').split(';');
      const userNames = inputUsers.container.attr('user-names').split(';');
      const index = userNames.findIndex(x => x == text);
      userIDs.splice(index, 1);
      userNames.splice(index, 1);
      inputUsers.container.attr('user-ids', userIDs.join(';')).attr('user-names', userNames.join(';'));
      selectUsers = [];
      for(let i = 0; i < userIDs.length; i++) {
        const userInfo = await AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/userchooser/getUserInfo?userID=${userIDs[i]}&locale=${AS.OPTIONS.locale}`);
        selectUsers.push(userInfo[0]);
      }
    }
  });

  inputUsers.container.find('.custom_tag_container').css('width', 'calc(100% - 40px)');

  buttonUsers.on('click', e => {
    e.preventDefault();
    e.target.blur();
    e.stopPropagation();

    AS.SERVICES.showUserChooserDialog(selectUsers, true, null, null, null, null, AS.OPTIONS.locale, users => {
      selectUsers = users;
      const userIDs = users.map(x => x.personID).join(';');
      const userNames = users.map(x => getFullName(x));
      inputUsers.container.attr('user-ids', userIDs).attr('user-names', userNames.join(';'));
      inputUsers.values = userNames;
    });
  });

  fc.append($('<div class="uk-inline uk-width-expand">').append(buttonUsers, inputUsers.container));

  return $('<div>', {class: 'uk-margin-small'}).append(`<label class="uk-form-label uk-text-bold">Пользователь</label>`).append(fc);
}

const copyToPersonalFile = async item => {
  let selectUserFile;

  const dialog = $('<div>');
  const panelContent = $('<div>', {style: "height: calc(100% - 50px);"});
  const panelButtons = $('<div>');
  const buttonSend = $(`<button class="uk-button uk-button-primary uk-button-small" type="button">${i18n.tr("Отправить")}</button>`);
  const buttonCancel = $(`<button class="uk-button uk-button-default uk-button-small" type="button">${i18n.tr("Отмена")}</button>`);

  const userChooserComponent = getUserChooserComponent();

  const userFileContainer = $('<div>', {class: 'uk-margin-small'});
  const fc = $('<div>', {class: 'uk-form-controls'});
  const userFileSelectButton = $('<a class="uk-form-icon uk-form-icon-flip" href="#" uk-icon="icon: more"></a>');
  const userFileResultBlock = $('<div>', {class: 'doc_file_documents_container'});

  fc.append($('<div class="uk-inline uk-width-expand">').append(userFileSelectButton, userFileResultBlock));
  userFileContainer.append(`<label class="uk-form-label uk-text-bold">${i18n.tr('Личное дело')}</label>`).append(fc);

  panelContent.append(userChooserComponent, userFileContainer);

  panelButtons.append(buttonSend, buttonCancel);
  dialog.append(panelContent, panelButtons);

  userFileResultBlock.css({
    "border": "1px solid #e5e5e5",
    "height": "40px",
    "background": "#fff",
    "padding": "0px 45px 0px 5px",
    "display": "flex",
    "gap": "5px",
    "align-items": "center",
    "overflow": "auto",
    "flex-wrap": "wrap"
  });

  panelButtons.css({
    'display': 'flex',
    'gap': '10px',
    'flex-direction': 'row',
    'justify-content': 'center',
    'align-items': 'center',
    'border-top': '1px solid #e5e5e5',
    'height': '50px'
  });

  userFileSelectButton.on('click', e => {
    showPersonalFileChooserDialog(result => {
      userFileResultBlock.text(result.selectItem.text);
      selectUserFile = result.selectItem;
    });
  });

  buttonCancel.on('click', e => {
    e.preventDefault();
    e.target.blur();
    dialog.dialog("close");
  });

  buttonSend.on('click', async e => {
    e.preventDefault();
    e.target.blur();
    Cons.showLoader();
    try {
      if(!userChooserComponent.find('.custom_input_tag_container').attr('user-ids')) throw new Error(i18n.tr('Выберите пользователя'));

      if(!selectUserFile) throw new Error(i18n.tr('Выберите личное дело'));

      const userIDs = userChooserComponent.find('.custom_input_tag_container').attr('user-ids').split(';');

      const apiResult = await appAPI.copyToPersonalFile({
        documentID: item.documentID,
        userIDs: userIDs,
        folderID: selectUserFile.folderID
      });

      if(apiResult.hasOwnProperty('errorCode') && apiResult.errorCode != 0) throw new Error(i18n.tr(apiResult.errorMessage));

      Cons.hideLoader();

      showMessage(apiResult.errorMessage, 'success');

      dialog.dialog("close");

    } catch (err) {
      showMessage(err.message, 'error');
      Cons.hideLoader();
    }
  });

  dialog.dialog({
    modal: true,
    width: 500,
    height: 400,
    title: i18n.tr('Копировать в личное дело'),
    show: {effect: 'fade', duration: 300},
    hide: {effect: 'fade', duration: 300},
    close: function() {
      $(this).remove();
    }
  });
}

const Documents = {
  filterType: null,
  objectID: null,
  filterID: null,
  typeViewData: 'list',
  showDetails: false,
  buttonDetails: null,
  selectItem: null,
  searchString: null,

  checkRightRegistration: async function(item, event) {
    Cons.showLoader();
    try {
      let registerID;
      let rccData;

      if(!this.objectID) {
        rccData = await appAPI.getDocumentRCC(item.documentID);
        registerID = rccData.registerID;
      } else {
        registerID = this.objectID;
      }

      const registerInfo = await appAPI.getRegisterInfo(registerID);

      Cons.hideLoader();

      const rights = registerInfo.rights.find(x => x.userID == AS.OPTIONS.currentUser.userid);

      if(rights && rights.canRead && (rights.canRegister || rights.canRefuse || (rights.canWrite && rights.canEdit))) {
        new Registration(false, registerInfo, rccData);
      } else {
        const eventParam = {...item};
        eventParam.filterType = this.filterType;
        eventParam.objectID = this.objectID;
        eventParam.filterID = this.filterID;
        fire({ type: event, ...eventParam }, comp.code);
        compContainer.trigger({type: event, eventParam});
      }
    } catch (e) {
      Cons.hideLoader();
      console.log(`ERROR checkRightRegistration: ${e.message}`);
    }
  },

  openDocument: function(item, event) {
    if(event == "document_item_click") {
      if(this.selectItem && this.selectItem.documentID == item.documentID) return;

      this.selectItem = item;
      if(this.showDetails) this.showDetailsDocument();
    }

    if(this.filterType == "REGISTER_FILTER" && item.statusID == "NOT_STARTED") {
      if(event == "document_item_dbl_click") this.checkRightRegistration(item, event);
    } else {
      const eventParam = {...item};
      eventParam.filterType = this.filterType;
      eventParam.objectID = this.objectID;
      eventParam.filterID = this.filterID;
      fire({ type: event, ...eventParam }, comp.code);
      compContainer.trigger({type: event, eventParam});
    }

    this.selectItem = item;
  },

  addRemoveFromControl: async function(item, controlled) {
    try {
      Cons.showLoader();
      const rccData = await appAPI.getDocumentRCC(item.documentID);
      const body = {};
      body.documentID = item.documentID;
      body.subject = item.name;
      body.doc_type = rccData.docTypeCode;
      body.control = controlled;
      body.duration = rccData.length;
      body.numberTemplate = rccData.userMask || rccData.number;

      const resultSave = await appAPI.saveDocument(body);

      if(!resultSave || resultSave.errorCode != 0) throw new Error(resultSave.errorMessage);

      this.createBody();

      Cons.hideLoader();
    } catch (err) {
      showMessage(err.message, 'error');
      Cons.hideLoader();
    }
  },

  documentHide: async function(item, hidden) {
    try {
      Cons.showLoader();

      const apiResult = await appAPI.documentHide(this.filterType, item.documentID, hidden);

      if(!apiResult || apiResult.errorCode != 0) throw new Error(apiResult.errorMessage);

      this.createBody();

      Cons.hideLoader();
    } catch (err) {
      showMessage(err.message, 'error');
      Cons.hideLoader();
    }
  },

  documentStopRout: async function(documentID, procInstID = null) {
    try {
      Cons.showLoader();

      const apiResult = await appAPI.stopRoute(documentID, procInstID);

      if(!apiResult || apiResult.errorCode != 0) throw new Error(apiResult.errorMessage);

      this.createBody();

      Cons.hideLoader();
    } catch (err) {
      showMessage(err.message, 'error');
      Cons.hideLoader();
    }
  },

  documentSetSeen: async function(item, seen) {
    try {
      Cons.showLoader();

      const apiResult = await appAPI.documentSetSeen(this.filterType, item.documentID, seen);

      if(!apiResult || apiResult.errorCode != 0) throw new Error(apiResult.errorMessage);

      this.createBody();

      Cons.hideLoader();
    } catch (err) {
      showMessage(err.message, 'error');
      Cons.hideLoader();
    }
  },

  markAsDefective: async function(item){
    const markAsDefectiveDialog = $('<div>');
    const panelContent = $('<div>', {style: "height: calc(100% - 50px);"});
    const panelButtons = $('<div>');

    const buttonSave = $(`<button class="uk-button uk-button-primary uk-button-small" type="button">${i18n.tr("Сохранить")}</button>`);
    const buttonCancel = $(`<button class="uk-button uk-button-default uk-button-small" type="button">${i18n.tr("Отмена")}</button>`);
    const defectiveComment = $('<textarea>', {class: "uk-textarea", style: "height: calc(100% - 50px); resize: none; margin-bottom: 15px;"});
    const closeWorks = $('<input class="uk-checkbox" type="checkbox">');

    panelButtons.append(buttonSave, buttonCancel);
    panelContent.append(defectiveComment, $('<label>').append(closeWorks, ' ' + i18n.tr('Завершить все работы по документу?')));

    markAsDefectiveDialog.append(panelContent, panelButtons);

    panelButtons.css({
      'display': 'flex',
      'gap': '10px',
      'flex-direction': 'row',
      'justify-content': 'end',
      'align-items': 'center',
      'border-top': '1px solid #e5e5e5',
      'height': '50px'
    });

    buttonCancel.on('click', e => {
      e.preventDefault();
      e.target.blur();
      markAsDefectiveDialog.dialog("close");
    });

    defectiveComment.on('input', e => {
      defectiveComment.css({
        "background": "#fff",
        "border-color": "#e5e5e5"
      });
    });

    buttonSave.on('click', async e => {
      e.preventDefault();
      e.target.blur();
      try {
        if(defectiveComment.val() == "") {
          defectiveComment.css({
            "background": "rgba(255, 0, 0, 0.1)",
            "border-color": "rgba(255, 0, 0, 0.5)"
          });
          throw new Error('Введите комментарий');
        }

        Cons.showLoader();
        const body = {};
        body.documentIDs = [item.documentID];
        body.closeWorks = closeWorks.prop('checked');
        body.comment = defectiveComment.val();

        const resultSave = await appAPI.markDefective(body);
        if(!resultSave || resultSave.errorCode != 0) throw new Error(resultSave.errorMessage);

        markAsDefectiveDialog.dialog("close");
        this.createBody();

        Cons.hideLoader();
      } catch (err) {
        showMessage(err.message, 'error');
        Cons.hideLoader();
      }
    });

    markAsDefectiveDialog.dialog({
      modal: true,
      width: 400,
      height: 300,
      title: i18n.tr('Пометить как бракованный'),
      show: {effect: 'fade', duration: 300},
      hide: {effect: 'fade', duration: 300},
      close: function() {
        $(this).remove();
      }
    });
  },

  writeOff: function(item){
    showTreeAffairsDialog(async result => {
      try {
        Cons.showLoader();
        const rccData = await appAPI.getDocumentRCC(item.documentID);
        const body = {};
        body.documentID = item.documentID;
        body.subject = item.name;
        body.doc_type = rccData.docTypeCode;
        body.duration = rccData.length;
        body.doc_file = result.selectItem.id;
        body.numberTemplate = rccData.userMask || rccData.number;

        const resultSave = await appAPI.saveDocument(body);

        if(!resultSave || resultSave.errorCode != 0) throw new Error(resultSave.errorMessage);

        this.createBody();

        Cons.hideLoader();
      } catch (err) {
        showMessage(err.message, 'error');
        Cons.hideLoader();
      }
    });
  },

  getContextMenuItems: async function(item){
    const menuItems = await appAPI.getContextMenuItems(this.filterType, item.documentID);
    const items = [];
    const me = this;

    if(!menuItems) return items;
    if(menuItems.hasOwnProperty('errorCode') && menuItems.errorCode != 0) {
      console.warn(`getContextMenuItems error: ${menuItems.errorMessage}`, {item, filterType: me.filterType});
    }

    for(let i = 0; i < menuItems.length; i++) {
      const {name, show, code, processes} = menuItems[i];
      if(["change_route", "export"].includes(code)) continue;
      if(!show) continue;

      switch (code) {
        case "open_as_document": {
          items.push({
            name: name,
            icon: 'file-text',
            handler: function(){
              const eventParam = {...item};
              eventParam.filterType = me.filterType;
              eventParam.objectID = me.objectID;
              eventParam.filterID = me.filterID;
              fire({ type: "document_item_dbl_click", ...eventParam }, comp.code);
              compContainer.trigger({type: "document_item_dbl_click", eventParam});
            }
          });
          break;
        }
        case "add_to_control": {
          items.push({
            name: name,
            icon: 'pull',
            handler: function(){
              me.addRemoveFromControl(item, true);
            }
          });
          break;
        }
        case "remove_from_control": {
          items.push({
            name: name,
            icon: 'push',
            handler: function(){
              me.addRemoveFromControl(item, false);
            }
          });
          break;
        }
        case "mark_as_defective": {
          items.push({
            name: name,
            icon: 'ban',
            handler: function(){
              me.markAsDefective(item);
            }
          });
          break;
        }
        case "mark_as_read": {
          items.push({
            name: name,
            icon: 'file-text',
            handler: function(){
              me.documentSetSeen(item, true);
            }
          });
          break;
        }
        case "mark_as_unread": {
          items.push({
            name: name,
            icon: 'hashtag',
            handler: function(){
              me.documentSetSeen(item, false);
            }
          });
          break;
        }
        case "change": {
          items.push({
            name: name,
            icon: 'file-edit',
            handler: function(){
              me.checkRightRegistration(item, "document_item_dbl_click");
            }
          });
          break;
        }
        case "write_off": {
          items.push({
            name: name,
            icon: 'folder',
            handler: function(){
              me.writeOff(item);
            }
          });
          break;
        }
        case "copy_to_personal_file": {
          items.push({
            name: name,
            icon: 'copy',
            handler: function(){
              copyToPersonalFile(item);
            }
          });
          break;
        }
        case "hide": {
          items.push({
            name: name,
            icon: 'ban',
            handler: function(){
              me.documentHide(item, true);
            }
          });
          break;
        }
        case "interrupt_route": {
          items.push({
            name: name,
            icon: 'minus-circle',
            handler: function(){
              if(processes && processes.length) {
                me.documentStopRout(item.documentID, processes[0].objectID);
              } else {
                me.documentStopRout(item.documentID);
              }
            }
          });
          break;
        }
      }
    }

    return items;
  },

  createListBlock: function(item) {
    const regDate = AS.FORMS.DateUtils.formatDate(AS.FORMS.DateUtils.parseDate(item.regDate), "${dd}.${mm}.${yyyy}");
    const createDate = AS.FORMS.DateUtils.formatDate(AS.FORMS.DateUtils.parseDate(item.createDate), "${dd} ${monthed} ${HH}:${MM}");
    const statusName = this.getStatusName(item);
    const statusColor = this.getStatusColor(item);

    const block = $('<div>', {class: 'document_block_container'});
    const blockLeft = $('<div>', {class: 'document_block_left'});
    const blockRight = $('<div>', {class: 'document_block_right'});

    //левая часть информации
    const bl1 = $('<div>'), bl2 = $('<div>'), bl3 = $('<div>'), bl4 = $('<div>');

    bl1.append(registerIcons.get('registry'));
    if(["ALL_USER_DOCUMENTS", "USER_OWN_DOCUMENTS", "USER_RECEIVED_DOCUMENTS", "USER_SENT_DOCUMENTS"].includes(this.filterType)) {
      if(item.inProgress == 'true' || item.statusID == "NOT_STARTED") bl1.append(registerIcons.get('in_progress'));
    } else {
      if(item.inProgress == 'true') bl1.append(registerIcons.get('in_progress'));
    }

    if(item.number != "") {
      if(["in_docs", "out_docs"].includes(item.docTypeID)) {
        if(item.correspondentOrg != "") {
          bl1.append(`<span>${item.number} - ${item.correspondentOrg}</span>`);
        } else if (item.correspondent != "") {
          bl1.append(`<span>${item.number} - ${item.correspondent}</span>`);
        } else {
          bl1.append(`<span>${item.number}</span>`);
        }
      } else {
        if(item.author != "") {
          bl1.append(`<span>${item.number} - ${item.author}</span>`);
        } else {
          bl1.append(`<span>${item.number}</span>`);
        }
      }
    }

    switch (this.filterType) {
      case "USER_RECEIVED_DOCUMENTS": {
        if(item.from != "") bl1.append(`<span style="color: #929292;">${i18n.tr('От')}:</span>`, `<span>${item.from}</span>`);
        break;
      }
      case "USER_OWN_DOCUMENTS": {
        if(item.to != "") bl1.append(`<span style="color: #929292;">${i18n.tr('Кому')}:</span>`, `<span>${item.to}</span>`);
        break;
      }
      case "USER_SENT_DOCUMENTS": {
        if(item.to != "") bl1.append(`<span style="color: #333333;">${i18n.tr('Кому')}:</span>`, `<span>${item.to}</span>`);
        break;
      }
    }

    bl2.append(`<span>${item.name}</span>`);
    if(item.content) bl2.append(`<span style="color: #929292;"> - ${item.content}</span>`);
    if(item.registered == "true" && item.regUser != "") bl3.append(`<span>${i18n.tr('Зарегистрировал')}: ${item.regUser}</span>`);
    if(item.resUsers != "") bl4.append(`<span>${i18n.tr('На исполнении')}: ${item.resUsers}</span>`);


    //правая часть информации
    const br1 = $('<div>'), br2 = $('<div>'), br3 = $('<div>'), br4 = $('<div>');

    if(["ALL_USER_DOCUMENTS", "USER_OWN_DOCUMENTS", "USER_RECEIVED_DOCUMENTS", "USER_SENT_DOCUMENTS"].includes(this.filterType) && item.registered == "false") {
      if(item.to != '') {
        br1.append(`<span>${i18n.tr('Отправлено')}: ${createDate}</span>`);
      } else if (item.from != '') {
        br1.append(`<span>${i18n.tr('Получено')}: ${createDate}</span>`);
      } else if (this.filterType == "USER_OWN_DOCUMENTS") {
        br1.append(`<span>${i18n.tr('Создано')}: ${createDate}</span>`);
      }
    } else {
      if(item.registered == "true" && item.regDate != "") br1.append(`<span>${i18n.tr('Зарегистрировано')}: ${regDate}</span>`);
      if(item.user != "") br3.append(`<span>${i18n.tr('Отписано')}: ${item.user}</span>`);
      br4.append(`<span>${i18n.tr('Статус')}:</span>`, `<span style="color: ${statusColor}">${statusName}</span>`);
    }

    if(item.hasResolution == 'true') br2.append(registerIcons.get('resolution'));
    if(item.controlled == 'true') br2.append(registerIcons.get('control'));
    if(item.defective == 'true') br2.append(registerIcons.get('defective'));
    if(item.hasAttachments == 'true') br2.append(registerIcons.get('attachments'));

    blockLeft.append(bl1, bl2, bl3, bl4);
    blockRight.append(br1, br2, br3, br4);
    block.append(blockLeft, blockRight);

    if(item.isNew == 'true') {
      bl2.css('font-weight', 'bold');
      block.addClass('isNew');
    }

    let timeoutId;

    block.on('click', e => {
      timeoutId = setTimeout(() => {
        if(!timeoutId) return;
        listContainer.find('.document_block_container').removeAttr('selected');
        block.attr('selected', true);
        this.openDocument(item, "document_item_click");
      }, 200);
    });

    block.on('dblclick', e => {
      if($(e.target).is("input")) return;
      clearTimeout(timeoutId);
      timeoutId = null;
      listContainer.find('.document_block_container').removeAttr('selected');
      block.attr('selected', true);
      this.openDocument(item, "document_item_dbl_click");
    });

    if((this.filterType == "REGISTER_FILTER" && this.objectID) || ["ALL_USER_DOCUMENTS", "USER_OWN_DOCUMENTS", "USER_RECEIVED_DOCUMENTS", "USER_SENT_DOCUMENTS"].includes(this.filterType)) {
      new ContextMenu(block, () => {
        listContainer.find('.document_block_container').removeAttr('selected');
        block.attr('selected', true);
        this.selectItem = item;
        if(this.showDetails) this.showDetailsDocument();
      }, null, () => {
        return this.getContextMenuItems(item);
      });
    }

    listContainer.append(block);
  },

  createTable: function(){
    this.table = $('<table class="document-table-list uk-table uk-table-small uk-table-responsive">');
    this.colgroup = $('<colgroup>');
    this.tHead = $('<thead>');
    this.tBody = $('<tbody>');
    this.table.append(this.colgroup, this.tHead, this.tBody);

    this.createColgroup();
    this.createHeader();
  },

  createColgroup: function(){
    this.colgroup.empty();

    if(this.filterInfo && this.filterInfo.hasOwnProperty('displayColumns') && this.filterInfo.displayColumns.length) {
      const displayColumns = this.filterInfo.displayColumns.filter(x => x.show).sort((a, b) => a.order - b.order);

      displayColumns.forEach(item => {
        const {columnType} = item;
        if(['registry', 'resolution', 'control', 'in_progress', 'defective', 'attachments'].includes(columnType)) {
          this.colgroup.append(`<col style="width: 40px">`);
        }
      });
    } else {
      this.colgroup.append(`<col style="width: 40px">`, `<col style="width: 40px">`, `<col style="width: 40px">`, `<col style="width: 40px">`, `<col style="width: 40px">`);
    }
  },

  createHeader: function(){
    const tr = $('<tr>');

    if(this.filterInfo && this.filterInfo.hasOwnProperty('displayColumns') && this.filterInfo.displayColumns.length) {
      const displayColumns = this.filterInfo.displayColumns.filter(x => x.show).sort((a, b) => a.order - b.order);

      displayColumns.forEach(item => {
        const {columnType, name} = item;
        const th = $('<th>', {class: 'uk-table-expand', columnid: columnType, 'uk-tooltip': name});
        if(['registry', 'resolution', 'control', 'in_progress', 'defective', 'attachments'].includes(columnType)) {
          th.append(registerIcons.get(columnType));
          th.css({
            'width': '40px',
            'padding': '0 10px'
          });
        } else {
          th.text(name);
        }
        tr.append(th);
      });
    } else {
      tr.append(`<th columnid="resolution" uk-tooltip="${i18n.tr('Резолюция')}" class="uk-table-expand" style="width: 40px; padding: 0 10px;">${registerIcons.get('resolution')}</th>`);
      tr.append(`<th columnid="controlled" uk-tooltip="${i18n.tr('Контрольный')}" class="uk-table-expand" style="width: 40px; padding: 0 10px;">${registerIcons.get('control')}</th>`);
      tr.append(`<th columnid="inProgress" uk-tooltip="${i18n.tr('В прогрессе')}" class="uk-table-expand" style="width: 40px; padding: 0 10px;">${registerIcons.get('in_progress')}</th>`);
      tr.append(`<th columnid="defective" uk-tooltip="${i18n.tr('Бракованный')}" class="uk-table-expand" style="width: 40px; padding: 0 10px;">${registerIcons.get('defective')}</th>`);
      tr.append(`<th columnid="attachments" uk-tooltip="${i18n.tr('Вложения')}" class="uk-table-expand" style="width: 40px; padding: 0 10px;">${registerIcons.get('attachments')}</th>`);
      tr.append(`<th columnid="number" uk-tooltip="${i18n.tr('Номер')}" class="uk-table-expand">${i18n.tr('Номер')}</th>`);
      tr.append(`<th columnid="regDate" uk-tooltip="${i18n.tr('Дата регистрации')}" class="uk-table-expand">${i18n.tr('Дата регистрации')}</th>`);
      tr.append(`<th columnid="name" uk-tooltip="${i18n.tr('Краткое содержание')}" class="uk-table-expand">${i18n.tr('Краткое содержание')}</th>`);
      if(!["ALL_USER_DOCUMENTS", "USER_OWN_DOCUMENTS", "USER_RECEIVED_DOCUMENTS", "USER_SENT_DOCUMENTS"].includes(this.filterType)) {
        tr.append(`<th columnid="statusID" uk-tooltip="${i18n.tr('Статус')}" class="uk-table-expand">${i18n.tr('Статус')}</th>`);
      }
      tr.append(`<th columnid="author" uk-tooltip="${i18n.tr('Автор')}" class="uk-table-expand">${i18n.tr('Автор')}</th>`);
      tr.append(`<th columnid="correspondent" uk-tooltip="${i18n.tr('Корреспондент')}" class="uk-table-expand">${i18n.tr('Корреспондент')}</th>`);
      tr.append(`<th columnid="correspondentOrg" uk-tooltip="${i18n.tr('Корреспондент (орг)')}" class="uk-table-expand">${i18n.tr('Корреспондент (орг)')}</th>`);
      tr.append(`<th columnid="regUser" uk-tooltip="${i18n.tr('Зарегистрировал')}" class="uk-table-expand">${i18n.tr('Зарегистрировал')}</th>`);
    }

    this.tHead.empty().append(tr);
  },

  getStatusName: function(item) {
    const {statusID, isExpired, isSoonExpired} = item;

    switch (statusID) {
      case 'NOT_STARTED': return i18n.tr('На регистрации'); break;
      case 'IN_PROGRESS': {
        if(isExpired == "true") {
          if(this.typeViewData == 'list') {
            return i18n.tr('Есть просроченные');
          } else {
            return i18n.tr('Просрочен');
          }
        } else {
          return i18n.tr('В процессе');
        }
        break;
      }
      case 'FINISHED': return i18n.tr('Завершен'); break;
      case 'DRAFT': return i18n.tr('Отклонен'); break;

      default: return statusID;
    }
  },

  getStatusColor: function(item) {
    const {statusID, isExpired, isSoonExpired} = item;

    switch (statusID) {
      case 'NOT_STARTED': return '#00a2e5'; break;
      case 'IN_PROGRESS': {
        if(isExpired == "true") {
          if(this.typeViewData == 'list') {
            return '#ad0101';
          } else {
            return '#c23b3b';
          }
        } else {
          return '#00a2e5';
        }
        break;
      }
      case 'FINISHED': return '#729b28'; break;
      case 'DRAFT': return '#222222'; break;

      default: return '#222222';
    }
  },

  createTableRow: function(item){
    const tr = $('<tr>');

    if(this.filterInfo && this.filterInfo.hasOwnProperty('displayColumns') && this.filterInfo.displayColumns.length) {
      const displayColumns = this.filterInfo.displayColumns.filter(x => x.show).sort((a, b) => a.order - b.order);

      displayColumns.forEach(x => {
        const {columnType} = x;
        const td = $('<td>');

        switch (columnType) {
          case 'registry': if(item.isRegistyDocuments == 'true') td.append(registerIcons.get(columnType)); break;
          case 'resolution': if(item.hasResolution == 'true') td.append(registerIcons.get(columnType)); break;
          case 'control': if(item.controlled == 'true') td.append(registerIcons.get(columnType)); break;
          case 'in_progress': {
            if(item.inProgress == 'true') {
              if(["ALL_USER_DOCUMENTS", "USER_OWN_DOCUMENTS", "USER_RECEIVED_DOCUMENTS", "USER_SENT_DOCUMENTS"].includes(this.filterType)) {
                if(item.statusID == "NOT_STARTED") td.append(registerIcons.get(columnType));
              } else {
                td.append(registerIcons.get(columnType));
              }
            }
            break;
          }
          case 'defective': if(item.defective == 'true') td.append(registerIcons.get(columnType)); break;
          case 'attachments': if(item.hasAttachments == 'true') td.append(registerIcons.get(columnType)); break;
          case 'number': {
            if(item.number != '') {
              td.text(item.number);
              td.css({'font-weight': 'bold'});
              td.attr('uk-tooltip', `title: ${item.number}`);
            }
            break;
          }
          case 'reg_date': {
            if(item.regDate != '') {
              const regDate = AS.FORMS.DateUtils.formatDate(AS.FORMS.DateUtils.parseDate(item.regDate), "${dd}.${mm}.${yyyy}");
              td.text(regDate);
              td.attr('uk-tooltip', `title: ${regDate}`);
            }
            break;
          }
          case 'subject': {
            if(item.name != '') {
              td.text(item.name);
              td.attr('uk-tooltip', `title: ${item.name}`);
            }
            break;
          }
          case 'status': {
            const statusName = this.getStatusName(item);
            const statusColor = this.getStatusColor(item);
            td.text(statusName);
            td.attr('uk-tooltip', `title: ${statusName}`);
            td.css({'color': statusColor});
            break;
          }
          case 'correspondent_org': {
            if(item.correspondentOrg != '') {
              td.text(item.correspondentOrg);
              td.attr('uk-tooltip', `title: ${item.correspondentOrg}`);
            }
            break;
          }
          case 'reg_user': {
            if(item.regUser != '') {
              td.text(item.regUser);
              td.attr('uk-tooltip', `title: ${item.regUser}`);
            }
            break;
          }
          case 'last_activity': {
            if(item.lastAction != '') {
              td.text(item.lastAction);
              td.attr('uk-tooltip', `title: ${item.lastAction}`);
            }
            break;
          }
          case 'signed_off': {
            if(item.resUsers != '') {
              td.text(item.resUsers);
              td.attr('uk-tooltip', `title: ${item.resUsers}`);
            }
            break;
          }
          default: {
            if(item[columnType] != '') {
              td.text(item[columnType]);
              td.attr('uk-tooltip', `title: ${item[columnType]}`);
            }
          }
        }

        tr.append(td);
      });
    } else {
      tr.append(`<td>${item.hasResolution == 'true' ? registerIcons.get('resolution') : ''}</td>`);
      tr.append(`<td>${item.controlled == 'true' ? registerIcons.get('control') : ''}</td>`);

      if(["ALL_USER_DOCUMENTS", "USER_OWN_DOCUMENTS", "USER_RECEIVED_DOCUMENTS", "USER_SENT_DOCUMENTS"].includes(this.filterType)) {
        tr.append(`<td>${item.inProgress == 'true' || item.statusID == "NOT_STARTED" ? registerIcons.get('in_progress') : ''}</td>`);
      } else {
        tr.append(`<td>${item.inProgress == 'true' ? registerIcons.get('in_progress') : ''}</td>`);
      }

      tr.append(`<td>${item.defective == 'true' ? registerIcons.get('defective') : ''}</td>`);
      tr.append(`<td>${item.hasAttachments == 'true' ? registerIcons.get('attachments') : ''}</td>`);
      tr.append(`<td ${item.number != '' ? 'uk-tooltip="title:' + item.number + '"': ''}; style="font-weight: bold;">${item.number || ''}</td>`);

      const dateFormat = "${dd}.${mm}.${yyyy}";
      const regDate = AS.FORMS.DateUtils.formatDate(AS.FORMS.DateUtils.parseDate(item.regDate), dateFormat);
      tr.append(`<td ${item.regDate != '' ? 'uk-tooltip="title:' + item.regDate + ';"': ''}>${item.regDate || ''}</td>`);

      tr.append(`<td ${item.name != '' ? 'uk-tooltip="title:' + item.name + ';"': ''}>${item.name || ''}</td>`);

      if(!["ALL_USER_DOCUMENTS", "USER_OWN_DOCUMENTS", "USER_RECEIVED_DOCUMENTS", "USER_SENT_DOCUMENTS"].includes(this.filterType)) {
        const statusName = this.getStatusName(item);
        const statusColor = this.getStatusColor(item);
        tr.append(`<td uk-tooltip="title: ${statusName};" style="color: ${statusColor}">${statusName}</td>`);
      }

      tr.append(`<td ${item.author != '' ? 'uk-tooltip="title:' + item.author + ';"': ''}>${item.author || ''}</td>`);
      tr.append(`<td ${item.correspondent != '' ? 'uk-tooltip="title:' + item.correspondent + ';"': ''}>${item.correspondent || ''}</td>`);
      tr.append(`<td ${item.correspondentOrg != '' ? 'uk-tooltip="title:' + item.correspondentOrg + ';"': ''}>${item.correspondentOrg || ''}</td>`);
      tr.append(`<td ${item.regUser != '' ? 'uk-tooltip="title:' + item.regUser + ';"': ''}>${item.regUser || ''}</td>`);
    }

    if(item.isExpired == "true" && item.statusID == 'IN_PROGRESS') tr.addClass('isExpired');
    if(item.isNew == "true") tr.addClass('isNew');

    tr.on('click', e => {
      if($(e.target).is("input")) return;
      timeoutId = setTimeout(() => {
        if(!timeoutId) return;
        this.tBody.find('tr').removeAttr('selected');
        tr.attr('selected', true);
        this.openDocument(item, "document_item_click");
      }, 200);
    });

    tr.on('dblclick', e => {
      clearTimeout(timeoutId);
      timeoutId = null;
      this.tBody.find('tr').removeAttr('selected');
      tr.attr('selected', true);
      this.openDocument(item, "document_item_dbl_click");
    });

    if((this.filterType == "REGISTER_FILTER" && this.objectID) || ["ALL_USER_DOCUMENTS", "USER_OWN_DOCUMENTS", "USER_RECEIVED_DOCUMENTS", "USER_SENT_DOCUMENTS"].includes(this.filterType)) {
      new ContextMenu(tr, () => {
        this.tBody.find('tr').removeAttr('selected');
        tr.attr('selected', true);
        this.selectItem = item;
        if(this.showDetails) this.showDetailsDocument();
      }, null, () => {
        return this.getContextMenuItems(item);
      });
    }

    this.tBody.append(tr);
  },

  renderData: function(item){
    listContainer.empty();
    this.tBody.empty();

    if (this.typeViewData == 'table') listContainer.append(this.table);

    for(let i = 0; i < this.listData.documents.length; i++) {
      const item = this.listData.documents[i];
      if(item.hidden == "true") continue;

      if(this.typeViewData == 'list') {
        this.createListBlock(item);
      } else if (this.typeViewData == 'table') {
        this.createTableRow(item);
      }
    }
  },

  createBody: async function(){
    try {
      Cons.showLoader();

      this.selectItem = null;

      if(this.buttonDetails) {
        this.showDetails = false;
        this.hideDetailsDocument();
      }

      const params = new URLSearchParams();
      if(this.filterType) params.append("filterType", this.filterType);
      if(this.objectID) params.append("objectID", this.objectID);
      if(this.filterID) params.append("filterID", this.filterID);
      if(this.searchString) params.append("searchString", this.searchString);
      params.append("countInPart", Paginator.countInPart);
      params.append("pageNumber", Paginator.currentPage - 1);

      this.listData = await getDocuments(params.toString());

      this.renderData();

      Paginator.rows = this.listData.totalCount;
      this.totalCount = this.listData.totalCount;

      Paginator.update();
      listContainer.scrollTop(0);

      Cons.hideLoader();
    } catch (err) {
      Cons.hideLoader();
      console.log('ERROR Documents createBody', err);
      showMessage(err.message, 'error');
    }
  },

  initButtonDetails: function(buttonDetailsID){
    this.buttonDetails = $(`#${buttonDetailsID}`);
    this.showDetails = false;
    this.buttonDetails.removeClass('pressed');

    this.buttonDetails.off().on('click', e => {
      e.preventDefault();
      e.target.blur();
      this.showDetails = !this.showDetails;

      if(this.showDetails) {
        this.showDetailsDocument();
      } else {
        this.hideDetailsDocument();
      }
    });
  },

  showDetailsDocument: function(){
    detailsContainer.empty();

    if(this.selectItem) {
      const rccContainer = $('<div>', {class: 'documents-details-rcc'});
      const attachmentContainer = $('<div>', {class: 'documents-details-files'});

      new _RCC(this.selectItem, rccContainer, true);
      new DocumentAttachments(this.selectItem.documentID, attachmentContainer, true);

      attachmentContainer.on('file_dbl_click file_context_open', async e => {
        const {name, uuid} = e.eventParam;
        const file = new AttachmentFile(name, uuid);
        file.open();
      });

      detailsContainer.append(rccContainer, attachmentContainer);
    } else {
      detailsContainer.append(`<div class="documents-details-empty">${i18n.tr("Выберите элемент")}</div>`);
    }

    if(this.buttonDetails) this.buttonDetails.addClass('pressed');

    componentContainer.css({
      'min-height': 'calc(100vh - 391px)'
    });
    detailsContainer.css({
      'height': '300px',
      'border-top': '1px solid #c4c4c4'
    });
  },

  hideDetailsDocument: function(){
    detailsContainer.empty();
    if(this.buttonDetails) this.buttonDetails.removeClass('pressed');

    componentContainer.css({
      'min-height': 'calc(100vh - 90px)'
    });
    detailsContainer.css({
      'height': '0px',
      'border-top': 'none'
    });
  },

  changeDisplayColumns: function(){
    try {
      if(!this.filterInfo) throw new Error(i18n.tr('Не найдено информации по фильтру'));

      const displayColumns = this.filterInfo.displayColumns.sort((a, b) => a.order - b.order);
      const dialog = $('<div>');
      const panelContent = $('<div>', {style: "height: calc(100% - 50px); padding: 10px;"});
      const panelButtons = $('<div>');
      const buttonSave = $(`<button class="uk-button uk-button-primary uk-button-small" type="button">${i18n.tr("Сохранить")}</button>`);
      const buttonCancel = $(`<button class="uk-button uk-button-default uk-button-small" type="button">${i18n.tr("Отмена")}</button>`);

      const panelColumns = $('<div>', {class: 'uk-column-1-2 uk-column-divider', style: 'height: 100%;'});

      displayColumns.forEach(item => {
        const label = $('<label>', {class: 'custom_checkbox_container'});
        const checkbox = $('<input>', {type: 'checkbox'});
        const checkmark = $('<span>', {class: 'checkmark'});

        if(item.show) checkbox.prop('checked', true);

        checkbox.on('change', e => {
          item.show = e.target.checked;
        });

        label.append(item.name, checkbox, checkmark);
        panelColumns.append(label);
      });

      panelContent.append(panelColumns);
      panelButtons.append(buttonSave, buttonCancel);
      dialog.append(panelContent, panelButtons);

      panelButtons.css({
        'display': 'flex',
        'gap': '10px',
        'flex-direction': 'row',
        'justify-content': 'center',
        'align-items': 'center',
        'border-top': '1px solid #e5e5e5',
        'height': '50px'
      });

      buttonCancel.on('click', e => {
        e.preventDefault();
        e.target.blur();
        dialog.dialog("close");
      });

      buttonSave.on('click', async e => {
        e.preventDefault();
        e.target.blur();
        Cons.showLoader();
        try {

          if(this.filterInfo.default_sorting == null) this.filterInfo.default_sorting = false;
          if(this.filterInfo.columns_default_settings == null) this.filterInfo.columns_default_settings = false;

          const saveResult = await appAPI.documentFilterSave(this.filterInfo);
          if(!saveResult) throw new Error(i18n.tr('Ошибка сохранения фильтра'));

          this.createColgroup();
          this.createHeader();
          this.renderData();

          Cons.hideLoader();
          dialog.dialog("close");
        } catch (err) {
          showMessage(err.message, 'error');
          Cons.hideLoader();
        }
      });

      dialog.dialog({
        modal: true,
        width: 600,
        height: 500,
        title: i18n.tr('Скрыть/отобразить столбцы'),
        show: {effect: 'fade', duration: 300},
        hide: {effect: 'fade', duration: 300},
        close: function() {
          $(this).remove();
        }
      });

    } catch (err) {
      console.log('ERROR changeDisplayColumns', err);
      showMessage(err.message, 'error');
    }
  },

  init: async function(params, typeViewData = 'list', buttonDetails){
    try {
      const {name, filterType = null, objectID = null, filterID = null, count, searchString = null} = params;
      this.itemName = name;
      this.totalCount = Number(count);
      this.filterType = filterType;
      this.objectID = objectID;
      this.filterID = filterID;
      this.typeViewData = typeViewData;
      this.searchString = searchString;

      const searchParams = new URLSearchParams();
      if(this.filterType) searchParams.append("filterType", this.filterType);
      if(this.objectID) searchParams.append("objectID", this.objectID);
      if(this.filterID) searchParams.append("filterID", this.filterID);
      this.filterInfo = await appAPI.documentFilterInfo(searchParams.toString());

      if(this.filterInfo && this.filterInfo.hasOwnProperty('errorCode') && this.filterInfo.errorCode != 0) this.filterInfo = null;

      if(buttonDetails) {
        this.initButtonDetails(buttonDetails);
      } else {
        this.buttonDetails = null;
      }

      this.createTable();

      Paginator.init(this.totalCount, 30);

      this.createBody();
    } catch (err) {
      console.log('ERROR Documents init', err);
      showMessage(err.message, 'error');
    }
  }
}

addListener('initDocumentList', comp.code, eventParam => {
  const {item, typeViewData, buttonDetails, searchString = null} = eventParam;
  item.searchString = searchString;
  Documents.init(item, typeViewData, buttonDetails);
});

addListener('updateDocumentList', comp.code, eventParam => {
  const {searchString = null} = eventParam;
  if(searchString && searchString != '') Paginator.currentPage = 1;
  Documents.searchString = searchString;
  Documents.createBody();
});

addListener('changeViewDocumentList', comp.code, eventParam => {
  Documents.typeViewData = eventParam.typeViewData;
  Documents.renderData();
  Paginator.update();
});

addListener('searchDocument', comp.code, eventParam => {
  const {searchString = null} = eventParam;
  if(searchString && searchString != '') Paginator.currentPage = 1;
  Documents.searchString = searchString;
  Documents.createBody();
});

addListener('changeDisplayColumns', comp.code, eventParam => {
  Documents.changeDisplayColumns();
});

addListener('uploadDataToFile', comp.code, eventParam => {
  const confirmMsg = i18n.tr('Вы собираетесь выгрузить ${number} записей документов. Продолжить?').replace('${number}', Documents.totalCount);

  UIkit.modal.confirm(confirmMsg, {labels: {ok: i18n.tr('Да'), cancel: i18n.tr('Отмена')}})
  .then(async () => {
    try {
      Cons.showLoader();

      const params = new URLSearchParams();
      if(Documents.filterType) params.append("filterType", Documents.filterType);
      if(Documents.objectID) params.append("objectID", Documents.objectID);
      if(Documents.filterID) params.append("filterID", Documents.filterID);
      if(Documents.searchString) params.append("searchString", Documents.searchString);
      params.append("fileName", Documents.itemName);

      const blob = await getDocumentsXLSX(params.toString());
      Cons.hideLoader();
      if(!blob) throw new Error(i18n.tr('Произошла ошибка при выгрузке записей'));

      const fileURL = window.URL.createObjectURL(blob);
      const a = document.createElement("a");
      if(typeof a.download === 'undefined') {
        window.location = fileURL;
      } else {
        a.href = fileURL;
        a.download = `${Documents.itemName}.xlsx`;
        a.click();
      }

    } catch (e) {
      console.log(e);
      Cons.hideLoader();
      showMessage(e.message, 'error');
    }

  }, () => null);
});


window.DocumentListGetFilterParams = () => {
  const {filterType, objectID, filterID, selectItem} = Documents;
  return {filterType, objectID, filterID, selectItem};
}
