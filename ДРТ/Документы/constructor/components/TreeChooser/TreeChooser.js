/*********Пример использования

https://www.jstree.com/docs/json/
const treeData = [
  { "id" : "ajson1", "parent" : "#", "text" : "Simple root node" },
  { "id" : "ajson2", "parent" : "#", "text" : "Root node 2" },
  { "id" : "ajson3", "parent" : "ajson2", "text" : "Child 1" },
  { "id" : "ajson4", "parent" : "ajson2", "text" : "Child 2" },
];

const customTree = new TreeChooser(treeData, 'Дела', 500, 600, 'Принять', 'Отмена');
customTree.showDialog();
customTree.treeContainer.addClass('tree_docfile_filter_icon');
customTree.dialogContainer.on(AS.FORMS.BasicChooserEvent.applyClicked, e => {
  console.log(e.eventParam);
});

*/

$.getScript("https://cdnjs.cloudflare.com/ajax/libs/jstree/3.3.16/jstree.min.js");

if(!$('link[res="jstreecss"]').length) {
  $('body').append('<link rel="stylesheet" res="jstreecss" href="https://cdnjs.cloudflare.com/ajax/libs/jstree/3.3.16/themes/default/style.min.css" integrity="sha512-A5OJVuNqxRragmJeYTW19bnw9M2WyxoshScX/rGTgZYj5hRXuqwZ+1AVn2d6wYTZPzPXxDeAGlae0XwTQdXjQA==" crossorigin="anonymous" referrerpolicy="no-referrer" />');
}

this.TreeChooser = class {
  constructor(treeData, dialogTitle, dialogWidth, dialogHeight, selectButtonName, isButtonCancel){
    this.treeData = treeData;
    this.dialogTitle = dialogTitle;
    this.dialogWidth = dialogWidth;
    this.dialogHeight = dialogHeight;
    this.selectButtonName = selectButtonName;
    this.isButtonCancel = isButtonCancel;
    this.selectedValue = null;

    this.init();
  }

  showDialog() {
    const instance = this;
    this.dialogContainer.dialog({
      modal: true,
      width: instance.dialogWidth,
      height: instance.dialogHeight,
      title: i18n.tr(instance.dialogTitle),
      show: {effect: 'fade', duration: 300},
      hide: {effect: 'fade', duration: 300},
      close: function() {
        $(this).remove();
      }
    });
  }

  addListener(){
    this.treeContainer.on("select_node.jstree", (evt, data) => {
      this.selectedValue = data.node.original;
      this.selectButton.prop('disabled', false);
    });

    this.selectButton.on('click', e => {
      const eventParam = {...this.selectedValue};
      this.dialogContainer.dialog( "destroy" );
      this.dialogContainer.detach();
      this.dialogContainer.trigger({type: AS.FORMS.BasicChooserEvent.applyClicked, eventParam});
    });

    if(this.isButtonCancel) {
      this.buttonCancel.on('click', e => {
        this.dialogContainer.dialog( "destroy" );
        this.dialogContainer.detach();
      });
    }
  }

  init(){
    const instance = this;

    this.dialogContainer = $('<div>');

    this.treeContainer = $('<div>', {
        style: "overflow: auto; width: " + (this.dialogWidth - 20) + "px; height: " + (this.dialogHeight - 100) + "px;"
    });

    this.buttonContainer = $('<div>');
    this.buttonContainer.css({
      'display': 'flex',
      'gap': '10px',
      'flex-direction': 'row',
      'justify-content': 'end',
      'align-items': 'center',
      'border-top': '1px solid #e5e5e5',
      'height': '50px'
    });

    this.selectButton = $('<button>', {class : "uk-button uk-button-small uk-button-primary"});
    this.selectButton.text(i18n.tr(this.selectButtonName));
    this.selectButton.prop('disabled', true);
    this.selectButton.css('border-radius', '3px');

    this.buttonContainer.append(this.selectButton);
    this.dialogContainer.append(this.treeContainer, this.buttonContainer);

    if(this.isButtonCancel) {
      this.buttonCancel = $('<button>', {class : "uk-button uk-button-small uk-button-default"});
      this.buttonCancel.text(i18n.tr(this.isButtonCancel));
      this.buttonContainer.append(this.buttonCancel);
      this.buttonCancel.css('border-radius', '3px');
    }

    this.treeContainer.jstree({
      core: {data: instance.treeData}
    });

    this.addListener();
  }
}
