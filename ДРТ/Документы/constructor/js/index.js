const parseTranslations = items => {
	const result = {};
  for(const key in items) {
  	const {message: {value, translations}} = items[key];
    result[value] = translations;
  }
  return result;
}

const getUrlParameter = param => {
  const url = new URLSearchParams(window.location.search);
  return url.has(param) ? url.get(param) : null;
}

//Иконка приложения
if(!$('link[rel="icon"]').length) $('head').append('<link rel="icon" href="../constructorFiles/documents/logo.svg">');

this.AppInit = async () => {
  let {doctypes, systemSettings, systemLocales, translations} = Cons.getAppStore();
	if(!localStorage.rowsPerPage) localStorage.rowsPerPage = 15;

	const urlLocale = getUrlParameter('locale');

  if(urlLocale) {
		AS.OPTIONS.locale = urlLocale;
    localStorage.locale = urlLocale;
    UTILS.Cookie.set('ConstructorLocale', urlLocale);
		fire({type: 'change_locale', locale: urlLocale});
	}

	return new Promise(async resolve => {
		if(!doctypes) {
			doctypes = await appAPI.getDoctypes();
			Cons.setAppStore({doctypes});
		}

		if(!systemSettings) {
			systemSettings = await appAPI.getSystemSettings();
	    Cons.setAppStore({systemSettings});
	  }

		if(!systemLocales) {
			systemLocales = await appAPI.getSystemLocales();
	    Cons.setAppStore({systemLocales});
	  }

		if(!translations && translations != 'not found') {
			const res = await appAPI.getDictionary('add_translations', false);
			if(res && res.hasOwnProperty('items')) {
				const t = parseTranslations(res.items);
				Cons.setAppStore({translations: t});
			} else {
				Cons.setAppStore({translations: 'not found'});
			}
	  }

		await updateTranslations();

		resolve({doctypes, systemSettings, systemLocales, translations});
	});
}

$('#buttonLogout').off().on('click', e => {
  e.preventDefault();
  e.target.blur();
  Cons.logout();
  $('.footer-panel').remove();
  Cons.setAppStore({openDocsWindow: null});
});

jQuery(document).mouseup(function(event){
	setTimeout(function(){
		AS.FORMS.popupPanel.hide();
	}, 0);
});

jQuery(document).click(function(event){
	AS.FORMS.popupPanel.hide();
});
