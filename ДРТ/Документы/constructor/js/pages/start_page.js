const getUrlParameter = param => {
  const url = new URLSearchParams(window.location.search);
  return url.has(param) ? url.get(param) : null;
}

const addLog = async data => {
  return new Promise(async resolve => {
    AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/logger/log", resolve, 'json', JSON.stringify(data), "application/json; charset=UTF-8", resolve);
  });
}

pageHandler('start_page', () => {
  const app = getUrlParameter('app');

  if(app) {
    Cons.setAppStore({appInIframe: true});
    const decodeStr = decodeURIComponent(escape(window.atob(app)));
    const separator = decodeStr.slice(0,8);
    const authParams = decodeStr.split(separator);
    Cons.login({
      login: authParams[1].split('').reverse().join(''),
      password: authParams[2].split('').reverse().join('')
    });
  } else {
    fire({type: 'goto_page', pageCode: 'auth_page'}, 'root-panel');
  }

  if (!Cons.getAppStore().start_page_auth_listener) {
    addListener('auth_success', 'root-panel', async authed => {
      const {login, password} = authed.creds;

      Cons.creds.login = login;
      Cons.creds.password = password;
      AS.apiAuth.setCredentials(login, password);
      AS.OPTIONS.login = login;
      AS.OPTIONS.password = password;
      AS.OPTIONS.currentUser = authed.data.person;

      Cons.showLoader();

      await AppInit();

      await addLog({
        "providerId": "AI_SEC",
        "eventId": "2005",
        "userId": AS.OPTIONS.currentUser.userid,
        "objects": [AS.OPTIONS.login, "Documents"]
      });

      Cons.hideLoader();

      fire({type: 'goto_page', pageCode: 'main_page'}, 'root-panel');
    });

    Cons.setAppStore({start_page_auth_listener: true});
  }
});
