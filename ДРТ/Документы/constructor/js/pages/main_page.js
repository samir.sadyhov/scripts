// Переменная для отслеживания текущих данных по дереву
let currentTreeData = []

const getUrlParameter = param => {
  const url = new URLSearchParams(window.location.search);
  return url.has(param) ? url.get(param) : null;
}

const initLocaleSelector = () => {
  const {systemLocales} = Cons.getAppStore();
  const items = systemLocales.map(x => ({title: x.initLocaleID.toUpperCase(), value: x.localeID}));
  const selectLocale = new CustomSelect({items, defaultValue: AS.OPTIONS.locale});

  selectLocale.container.on('selected', e => {
    fire({type: 'change_locale', locale: e.eventParam.value});
  });

  $('#panelLocale').append(selectLocale.container);
}

const generateGuid = () => 'R'+Math.random().toString(36).substring(2, 15)+Math.random().toString(36).substring(2, 15);

function findTreeElementById(tree, id) {
  if (tree.id == id) return tree;

  if (tree.children && tree.children.length > 0) {
    for (const child of tree.children) {
      const found = findTreeElementById(child, id);
      if (found) return found;
    }
  }

  return null;
}

function deleteTreeElementById(tree, id) {
  if (tree.children && tree.children.length > 0) {
    tree.children = tree.children.filter(x => x.id != id);
    for (const child of tree.children) deleteTreeElementById(child, id);
  }
}

const deleteFilter = async item => {
  const {id, filterID} = item;
  UIkit.modal.confirm(i18n.tr('Вы действительно удаляете фильтр?'),
    {labels: {ok: i18n.tr('Да'), cancel: i18n.tr('Отмена')}})
  .then(async () => {
    try {
      Cons.showLoader();

      //удаление фильтра
      const result = await appAPI.documentFilterDelete(filterID);
      if(!result) throw new Error('Произошла ошибка при удалении фильтра');

      //обновление дерева
      for(const tree of currentTreeData) deleteTreeElementById(tree, id);
      Cons.setAppStore({treeData: currentTreeData});
      fire({type: 'tree_set_data', data: currentTreeData}, 'treeDocumnts');

      Cons.hideLoader();
    } catch (err) {
      Cons.hideLoader();
      showMessage(i18n.tr(err.message), 'error');
      console.log(`ERROR [ deleteFilter ]: ${err.message}`);
    }
  }, () => null);
}

const filterBackDefault = async registerID => {
  UIkit.modal.confirm(i18n.tr('Удалить текущие фильтры?'),
    {labels: {ok: i18n.tr('Да'), cancel: i18n.tr('Отмена')}})
  .then(async () => {
    try {
      Cons.showLoader();

      const result = await appAPI.documentFilterBackDefault(registerID);
      if(!result) throw new Error('Произошла ошибка');

      Cons.hideLoader();

      __initTree();
    } catch (err) {
      Cons.hideLoader();
      showMessage(i18n.tr(err.message), 'error');
      console.log(`ERROR [ filterBackDefault ]: ${err.message}`);
    }
  }, () => null);
}

const markAsRead = async (filterType, filterID) => {
  try {
    Cons.showLoader();

    const params = new URLSearchParams();
    params.append("filterType", filterType);
    if(filterID) params.append("filterID", filterID);
    const apiResult = await appAPI.documentMarkAsRead(params.toString());
    if(!apiResult) throw new Error(i18n.tr('Ошибка выполнения'));
    if(apiResult.hasOwnProperty('errorCode') && apiResult.errorCode != 0) throw new Error(apiResult.errorMessage);

    fire({type: 'updateDocumentList'}, 'customDocumentList');

    Cons.hideLoader();
  } catch (err) {
    Cons.hideLoader();
    showMessage(err.message, 'error');
    console.log(`ERROR [ markAsRead ]: ${err.message}`);
  }
}

const getFilterContextMenu = item => {
  const {filterType, objectID, filterID} = item;
  const menu = [];

  menu.push({
    name: i18n.tr('Добавить фильтр'),
    code: 'create_filter',
    icon: "plus",
    handler: () => new DocumentsFilter(item, 'create', updateTree)
  });

  if(filterType == "REGISTER_FILTER" && objectID && !filterID) {
    menu.push({
      name: i18n.tr('Вернуть фильтры по умолчанию'),
      code: 'reset_filters_default',
      icon: "refresh",
      handler: () => filterBackDefault(objectID)
    });
  }

  menu.push({
    name: i18n.tr('Редактировать фильтр'),
    code: 'edit_filter',
    icon: "pencil",
    handler: () => new DocumentsFilter(item, 'edit', updateTree)
  });

  if(filterID) {
    menu.push({
      name: i18n.tr('Удалить фильтр'),
      code: 'delete_filter',
      icon: "trash",
      handler: () => deleteFilter(item)
    });
  }

  if(["ALL_USER_DOCUMENTS", "USER_OWN_DOCUMENTS", "USER_RECEIVED_DOCUMENTS", "USER_SENT_DOCUMENTS"].includes(filterType)) {
    menu.push({divider: true}),
    menu.push({
      name: i18n.tr('Отметить все как прочитанное'),
      code: 'mark_all_as_read',
      icon: "check",
      handler: () => markAsRead(filterType, filterID)
    });
  }

  return menu;
}

const parseTreeData = item => {
  item.id = generateGuid();
  item.hasChildren == "true" ? item.hasChildren = true : item.hasChildren = false;
  item.children = [];
  item.counters = {total: Number(item.count) || 0};

  if(item.filterID) {
    item.icon = 'folder';
    item.class = 'tree_filter';
  } else {
    switch (item.filterType) {
      case "REGISTER_FILTER": {
        item.icon = 'menu_book';
        item.class = 'tree_register';
        break;
      }
      case "DOCFILE_FILTER": {
        item.icon = 'archive';
        item.class = 'tree_docfile';
        break;
      }
      case "ALL_USER_DOCUMENTS":
      case "USER_OWN_DOCUMENTS":
      case "USER_RECEIVED_DOCUMENTS":
      case "USER_SENT_DOCUMENTS": {
        item.icon = 'description';
        item.class = 'tree_user_doc';
        break;
      }
    }
  }

  item.contextMenu = true;
  item.contextMenuItems = getFilterContextMenu(item);

  return item;
}

const getTreeData = async () => {
  const result = await appAPI.getDocumentsFilters();

  const f = async r => {
    for(let i = 0; i < r.length; i++) {
      const item = parseTreeData(r[i]);
      if(item.hasChildren) {
        const {filterType, objectID, filterID} = item;
        const params = new URLSearchParams();
        if(filterType) params.append("filterType", filterType);
        if(objectID) params.append("objectID", objectID);
        if(filterID) params.append("filterID", filterID);

        item.children = await appAPI.getDocumentsFilters(params.toString());
        await f(item.children);
      }
    }
  }

  await f(result);

  return result;
}

const getDocuments = async params => {
  return new Promise(async resolve => {
    rest.synergyGet(`api/docflow/doc/documents?locale=${AS.OPTIONS.locale}${params ? '&'+params : ''}`, resolve, err => {
      console.log(`ERROR [ getDocuments ]: ${JSON.stringify(err)}`);
      resolve(null);
    });
  });
}

// Функция для обновления дерева с динамическим добавлением/редактированием дочерних элементов дерева
const updateTree = async (savedFilter, id, event) => {
  let treeEl = null;

  for(const tree of currentTreeData) {
    treeEl = findTreeElementById(tree, id);
    if(treeEl) break;
  }

  if(!treeEl) return;

  const {filterType, objectID, filterID} = savedFilter;
  const params = new URLSearchParams();
  if(filterType) params.append("filterType", filterType);
  if(objectID) params.append("objectID", objectID);
  if(filterID) params.append("filterID", filterID);
  params.append("countInPart", 1);
  params.append("pageNumber", 0);

  const apiRes = await getDocuments(params.toString());

  if(event == 'edit') {
    treeEl.name = savedFilter.name;
    if(apiRes) treeEl.counters = {total: Number(apiRes.totalCount) || 0};
  } else {
    savedFilter.hasChildren = "false";
    savedFilter.count = apiRes ? apiRes.totalCount : 0;
    const newItem = parseTreeData(savedFilter);
    if(treeEl.hasOwnProperty('children')) treeEl.children = [...treeEl.children, newItem];
  }

  Cons.setAppStore({treeData: currentTreeData});
  fire({type: 'tree_set_data', data: currentTreeData}, 'treeDocumnts');
}

this.__initTree = async () => {
  Cons.showLoader();
  try {
    currentTreeData = await getTreeData();

    Cons.setAppStore({treeData: currentTreeData});

    fire({type: 'tree_set_data', data: currentTreeData}, 'treeDocumnts');
    fire({type: 'tree_item_click', item: currentTreeData[0], data: currentTreeData}, 'treeDocumnts');

    Cons.hideLoader();
  } catch (err) {
    showMessage(err.message, 'error');
    Cons.hideLoader();
  }
}

let typeViewData = localStorage.getItem(`registers_typeView_${AS.OPTIONS.currentUser.userid}`) || 'list';

this.renderButtonsChangeView = () => {
  const buttonGroup = $('<div>', {class: "uk-button-group"});
  const buttonList = $(`<button class="uk-button uk-button-default" uk-tooltip="title: ${i18n.tr('Строчный')}"><svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#5f6368"><path d="M0 0h24v24H0z" fill="none"/><path d="M3 13h2v-2H3v2zm0 4h2v-2H3v2zm0-8h2V7H3v2zm4 4h14v-2H7v2zm0 4h14v-2H7v2zM7 7v2h14V7H7z"/></svg></button>`);
  const buttonTable = $(`<button class="uk-button uk-button-default" uk-tooltip="title: ${i18n.tr('Табличный')}"><svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24px" viewBox="0 0 24 24" width="24px" fill="#5f6368"><g><rect fill="none" height="24" width="24"/><path d="M19,7H9C7.9,7,7,7.9,7,9v10c0,1.1,0.9,2,2,2h10c1.1,0,2-0.9,2-2V9C21,7.9,20.1,7,19,7z M19,9v2H9V9H19z M13,15v-2h2v2H13z M15,17v2h-2v-2H15z M11,15H9v-2h2V15z M17,13h2v2h-2V13z M9,17h2v2H9V17z M17,19v-2h2v2H17z M6,17H5c-1.1,0-2-0.9-2-2V5 c0-1.1,0.9-2,2-2h10c1.1,0,2,0.9,2,2v1h-2V5H5v10h1V17z"/></g></svg></button>`);
  const buttonTableSetting = $(`<button class="uk-button uk-button-default" uk-tooltip="title: ${i18n.tr('Скрыть/отобразить столбцы')}" style="display: none;"><svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="24px" viewBox="0 0 24 24" width="24px" fill="#5f6368"><g><path d="M0,0h24v24H0V0z" fill="none"/><path d="M19.14,12.94c0.04-0.3,0.06-0.61,0.06-0.94c0-0.32-0.02-0.64-0.07-0.94l2.03-1.58c0.18-0.14,0.23-0.41,0.12-0.61 l-1.92-3.32c-0.12-0.22-0.37-0.29-0.59-0.22l-2.39,0.96c-0.5-0.38-1.03-0.7-1.62-0.94L14.4,2.81c-0.04-0.24-0.24-0.41-0.48-0.41 h-3.84c-0.24,0-0.43,0.17-0.47,0.41L9.25,5.35C8.66,5.59,8.12,5.92,7.63,6.29L5.24,5.33c-0.22-0.08-0.47,0-0.59,0.22L2.74,8.87 C2.62,9.08,2.66,9.34,2.86,9.48l2.03,1.58C4.84,11.36,4.8,11.69,4.8,12s0.02,0.64,0.07,0.94l-2.03,1.58 c-0.18,0.14-0.23,0.41-0.12,0.61l1.92,3.32c0.12,0.22,0.37,0.29,0.59,0.22l2.39-0.96c0.5,0.38,1.03,0.7,1.62,0.94l0.36,2.54 c0.05,0.24,0.24,0.41,0.48,0.41h3.84c0.24,0,0.44-0.17,0.47-0.41l0.36-2.54c0.59-0.24,1.13-0.56,1.62-0.94l2.39,0.96 c0.22,0.08,0.47,0,0.59-0.22l1.92-3.32c0.12-0.22,0.07-0.47-0.12-0.61L19.14,12.94z M12,15.6c-1.98,0-3.6-1.62-3.6-3.6 s1.62-3.6,3.6-3.6s3.6,1.62,3.6,3.6S13.98,15.6,12,15.6z"/></g></svg></button>`);

  if(typeViewData == 'list') {
    buttonList.addClass('select');
    buttonTableSetting.hide();
    buttonTable.css('border-radius', '0 10px 10px 0');
  } else {
    buttonTable.addClass('select');
    buttonTable.css('border-radius', '0');
    buttonTableSetting.show();
  }

  buttonList.off().on('click', e => {
    e.preventDefault();
    e.target.blur();
    if(buttonList.hasClass('select')) return;

    typeViewData = 'list';
    localStorage.setItem(`registers_typeView_${AS.OPTIONS.currentUser.userid}`, typeViewData);

    buttonList.addClass('select');
    buttonTable.removeClass('select');
    buttonTableSetting.hide();
    buttonTable.css('border-radius', '0 10px 10px 0');

    fire({type: 'changeViewDocumentList', typeViewData}, 'customDocumentList');
  });

  buttonTable.off().on('click', e => {
    e.preventDefault();
    e.target.blur();
    if(buttonTable.hasClass('select')) return;

    typeViewData = 'table';
    localStorage.setItem(`registers_typeView_${AS.OPTIONS.currentUser.userid}`, typeViewData);

    buttonTable.addClass('select');
    buttonList.removeClass('select');
    buttonTableSetting.show();
    buttonTable.css('border-radius', '0');

    fire({type: 'changeViewDocumentList', typeViewData}, 'customDocumentList');
  });

  buttonTableSetting.off().on('click', e => {
    e.preventDefault();
    e.target.blur();

    fire({type: 'changeDisplayColumns', typeViewData}, 'customDocumentList');
  });

  buttonGroup.append(buttonList, buttonTable, buttonTableSetting);
  $('#panelChangeView').empty().append(buttonGroup);
}

pageHandler('main_page', () => {
  //переключатель локали
  initLocaleSelector();

  __initTree();

  renderButtonsChangeView();

  if (!Cons.getAppStore().main_page_listener) {

    //клик по элементу дерева
    addListener("tree_item_click", 'treeDocumnts', event => {
      $('#searchInput').val(null);
      const {item, data} = event;
      Cons.setAppStore({tree_item: item});
      fire({type: 'initDocumentList', item, typeViewData, buttonDetails: 'buttonDocumentDetails'}, 'customDocumentList');
    });

    // addListener('document_item_click', 'customDocumentList', event => {
    //   console.log('document_item_click', event);
    // });
    //
    // addListener('document_item_dbl_click', 'customDocumentList', event => {
    //   console.log('document_item_dbl_click', event);
    // });

    //Кнопка обновить список документов
    addListener('button_click', 'buttonRefreshRegList', e => {
      $('#searchInput').val(null);
      fire({type: 'updateDocumentList'}, 'customDocumentList');
    });

    Cons.setAppStore({main_page_listener: true});
  }

  $('#searchInput').off().on('keyup', e => {
    if (e.keyCode === 13) {
      e.preventDefault();
      fire({type: 'searchDocument', searchString: $('#searchInput').val()}, 'customDocumentList');
    }
  });

  $('#searchInput').parent().find('[uk-icon="icon: search"]').on('click', e => {
    fire({type: 'searchDocument', searchString: $('#searchInput').val()}, 'customDocumentList');
  });

  $('#customDocumentList').on('document_item_dbl_click', async e => {
    if(e.hasOwnProperty('eventParam')) {
      const {documentID, filterType, isNew} = e.eventParam;

      if(isNew == "true") {
        const params = new URLSearchParams();
        params.append("documentID", documentID);
        params.append("filterType", filterType);
        params.append("seen", true);
        params.append("locale", AS.OPTIONS.locale);

        await AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/docflow/doc/set_seen?${params.toString()}`);

        fire({type: 'updateDocumentList'}, 'customDocumentList');
      }

      const eventParam = {
        type: 'document',
        documentID,
        editable: false
      };
      $('#root-panel').trigger({type: 'custom_open_document', eventParam});
    }
  });

  //отчеты
  fire({type: 'reports_init', objectType: 1024}, 'buttonReports');

  const {appInIframe} = Cons.getAppStore();
  if(appInIframe) {
    $(`#buttonLogout`).hide();
    $(`#panelLocale`).hide();
  }

  const urlDocId = getUrlParameter('document_identifier');
  if(urlDocId) {
    $('#root-panel').trigger({
      type: 'custom_open_document',
      eventParam: {
        type: 'document',
        documentID: urlDocId,
        editable: false
      }
    });
  }
});
