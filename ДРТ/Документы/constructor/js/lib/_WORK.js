const closeDocument = _doc => {
  let openDocsWindow;
  if(_doc.type == 'work') {
    openDocsWindow = Cons.getAppStore().openDocsWindow.filter(x => x.actionID !== _doc.actionID);
  } else {
    openDocsWindow = Cons.getAppStore().openDocsWindow.filter(x => x.documentID !== _doc.documentID);
  }

  Cons.setAppStore({openDocsWindow: openDocsWindow});

  _doc.panels.documentWindow.remove();
  _doc.panels.documentPanel.remove();
  if(_doc.formPlayer) _doc.formPlayer.destroy();

  if(!openDocsWindow.length) {
    history.replaceState(null, null, `/${_doc.pathname}`);
  } else {
    let aw = $('.arm-window-document[state="open"]');
    if(aw.length) {
      aw = $(aw[0]);
      aw.attr('arm-active-window', true);

      if(_doc.type == 'work') {
        const actionID = aw.attr('actionid');
        $(`.footer-document[actionid="${actionID}"]`).addClass('arm-active-panel');
        history.replaceState(null, null, `/${_doc.pathname}/?actionID=${actionID}`);
      } else {
        const docID = aw.attr('documentidentifier');
        $(`.footer-document[documentidentifier="${docID}"]`).addClass('arm-active-panel');
        history.replaceState(null, null, `/${_doc.pathname}/?document_identifier=${docID}`);
      }

    } else {
      history.replaceState(null, null, `/${_doc.pathname}`);
    }
  }
}

const updateData = () => {
  return;
}

const getCommentForDialog = async () => {
  const commentText = i18n.tr("Комментарий");
  const container = $('<div>', {class: 'uk-margin'});
  const label = $(`<label class="uk-form-label" for="textarea-comment-text">${commentText}</label>`);
  const textarea = $('<textarea class="uk-textarea" rows="5" id="textarea-comment-text"></textarea>');
  container.append(label).append(textarea);
  return {container, textarea};
}

const getFileForDialog = () => {
  const container = $('<div class="uk-margin" uk-margin style="width: 100%">');
  const formCustom = $('<div uk-form-custom="target: true" style="width: 100%;">');
  const inputFile = $('<input type="file">');
  formCustom.append(inputFile)
  .append(`<input class="uk-input uk-form-width-medium" type="text" style="font-size: 14px; color: #333; background: #fff; min-width: 245px; width: 100%;" placeholder="Прикрепить результат" disabled>`);
  container.append(formCustom);
  return {container, inputFile};
}

const getDocumentForDialog = childDocuments => {
  const container = $('<div class="uk-margin" uk-margin style="width: 100%">');

  const values = childDocuments.map(x => {
    return {label: x.name, value: x.documentID};
  });
  const {container: docsContainer, select} = Components.createSelectComponent(i18n.tr("Выберите документ"), values);

  container.append(docsContainer);
  return {container, select};
}

const getCreateWorkForDialog = async (dialogInfo, defaultParamWork) => {
  const steps = dialogInfo.steps.flat().filter(x => x.show);
  const container = $('<div>');
  const workData = {};

  for(let i = 0; i < steps.length; i++) {
    const {type} = steps[i];
    if(['AUTHOR', 'LOAD', 'KEYWORDS', 'REPEAT', 'USERS'].includes(type)) continue;
    if(type == 'ATTACH_FILE') {
      const {container: component, inputFile} = await Components.create(type, workData, defaultParamWork);
      if(component) {
        container.append(component);
        workData.inputFile = inputFile;
      }
    } else {
      const component = await Components.create(type, workData, defaultParamWork);
      if(component) container.append(component);
    }
  }

  return {container, workData};
}

const getEditWorkForDialog = work => {
  const {systemSettings} = Cons.getAppStore();
  const {current_priorities, work_completion_forms} = systemSettings;

  const container = $('<div>');
  const workData = {
    NAME: work.name,
    RESPONSIBLE: [{personID: work.user.id, personName: work.user.name}],
    start: work.start_date,
    finish: work.finish_date
  };

  const workNamePlaceholder = i18n.tr("Введите формулировку работы");
  const workNameLabel = i18n.tr("Название");
  const {container: workContainer, textarea: workName} = Components.createInputTextArea(workNameLabel, workNamePlaceholder);
  workName.val(work.name);
  workName.on('change', e => workData.NAME = workName.val());
  container.append(workContainer);

  const userLabel = i18n.tr("Ответственный");
  const userContainer = Components.createUserParamBlock(userLabel, 'RESPONSIBLE', workData);
  container.append(userContainer);

  const {container: workPeriodContainer, dateStart, dateFinish} = Components.createDatePeriod(i18n.tr("Сроки"), work.start_date, work.finish_date);
  $(dateStart[0]).on('change', e => workData.start = `${$(dateStart[0]).val()} ${$(dateStart[1]).val()}:00`.slice(0, 19));
  $(dateStart[1]).on('change', e => workData.start = `${$(dateStart[0]).val()} ${$(dateStart[1]).val()}:00`.slice(0, 19));
  $(dateFinish[0]).on('change', e => workData.finish = `${$(dateFinish[0]).val()} ${$(dateFinish[1]).val()}:00`.slice(0, 19));
  $(dateFinish[1]).on('change', e => workData.finish = `${$(dateFinish[0]).val()} ${$(dateFinish[1]).val()}:00`.slice(0, 19));
  container.append(workPeriodContainer);

  const priorities = current_priorities.map(x => {
    return {label: x.name, value: x.id};
  });
  const {container: prioritetContainer, select: prioritetSelect} = Components.createSelectComponent(i18n.tr("Приоритет"), priorities);
  workData.PRIORITY = priorities.find(x => x.value == work.priority);
  prioritetSelect.val(work.priority);
  prioritetSelect.on('change', e => {
    workData.PRIORITY = priorities.find(x => x.value == prioritetSelect.val());
  });
  container.append(prioritetContainer);

  const completionForms = work_completion_forms.map(x => {
    return {label: x.name, value: x.id, code: x.code};
  });
  const {container: completionFormsContainer, select: completionFormsSelect} = Components.createSelectComponent(i18n.tr("Форма завершения"), completionForms);
  workData.COMPLETION_FORM = completionForms[0];
  completionFormsSelect.on('change', e => {
    workData.COMPLETION_FORM = completionForms.find(x => x.value == completionFormsSelect.val());
  });
  container.append(completionFormsContainer);

  const {container: componentFile, inputFile} = Components.createSelectFile(i18n.tr("Прикрепить документ"));
  container.append(componentFile);
  workData.inputFile = inputFile;

  return {container, workData};
}

const getSendWorkForDialog = async (actionType, dialogInfo) => {
  const {systemSettings} = Cons.getAppStore();
  const {current_priorities, work_completion_forms} = systemSettings;
  const container = $('<div>');
  const workData = {
    name: dialogInfo.name,
    users: null,
    finishDate: dialogInfo.finish_date + ':00'
  };

  const workNamePlaceholder = i18n.tr("Введите формулировку работы");
  const workNameLabel = i18n.tr("Название");
  const {container: workContainer, textarea: workName} = Components.createInputTextArea(workNameLabel, workNamePlaceholder);
  workName.val(dialogInfo.name);
  workName.on('change', e => workData.name = workName.val());
  container.append(workContainer);

  let filterDepartmentID = null;
  if(dialogInfo.show_all == "false") {
    const {positions} = AS.OPTIONS.currentUser;
    if(positions.length) {
      const posHead = positions.find(x => x.type == 1);
      if(posHead) {
        filterDepartmentID = posHead.departmentID;
      } else {
        filterDepartmentID = positions[0].departmentID;
      }
    }
  }

  if(actionType == 'REASSIGN') {
    const responsibleContainer = Components.createUserParamBlock(i18n.tr("Ответственный"), 'RESPONSIBLE', workData, false, filterDepartmentID);
    container.append(responsibleContainer);
  }

  const usersLabel = i18n.tr("Исполнители");
  const usersContainer = Components.createUserParamBlock(usersLabel, 'users', workData, true, filterDepartmentID);
  container.append(usersContainer);

  const finishLabel = i18n.tr("Завершение");
  const {container: workFinishContainer, dateTimeInput} = Components.createDateTimeInput(finishLabel, dialogInfo.finish_date);
  $(dateTimeInput[0]).on('change', e => workData.finishDate = `${$(dateTimeInput[0]).val()} ${$(dateTimeInput[1]).val()}:00`.slice(0, 19));
  $(dateTimeInput[1]).on('change', e => workData.finishDate = `${$(dateTimeInput[0]).val()} ${$(dateTimeInput[1]).val()}:00`.slice(0, 19));
  container.append(workFinishContainer);

  if(actionType == 'REASSIGN') {
    const completionForms = work_completion_forms.map(x => {
      return {label: x.name, value: x.id, code: x.code};
    });
    const {container: completionFormsContainer, select: completionFormsSelect} = Components.createSelectComponent(i18n.tr("Форма завершения"), completionForms);
    workData.COMPLETION_FORM = completionForms[0];
    completionFormsSelect.on('change', e => {
      workData.COMPLETION_FORM = completionForms.find(x => x.value == completionFormsSelect.val());
    });
    container.append(completionFormsContainer);
  }

  return {container, workData};
}

const getForwardWorkForDialog = async () => {
  const container = $('<div>');
  const workData = {users: null};
  container.append(Components.createUserParamBlock('', 'users', workData, true));
  return {container, workData};
}

const getTransferWorkForDialog = async (work) => {
  const container = $('<div>');
  const workData = {
    users: null,
    name: `Прошу Вас взять на исполнение работу "${work.name}"`
  };

  const {container: textareaContainer, textarea: workName} = Components.createInputTextArea('');
  container.append(textareaContainer, Components.createUserParamBlock('', 'users', workData));

  workName.val(workData.name);

  workName.on('change', e => {
    workData.name = workName.val();
  });

  return {container, workData};
}

this._WORK = {
  finishWork: async function(workID, _doc) {
    Cons.showLoader();
    try {
      const resultFinish = await appAPI.finishWork({workID, completionForm: "NOTHING"});
      if(!resultFinish) throw new Error('Произошла ошибка при завершении работы');

      Cons.hideLoader();
      showMessage('Работа завершена', 'success');
      closeDocument(_doc);
      updateData();

    } catch (err) {
      Cons.hideLoader();
      showMessage(err.message, 'error');
    }
  },

  acceptWork: async function(_doc) {
    Cons.showLoader();
    try {
      const { workInfo } = _doc;
      const {actionID, completionResultID} = workInfo;
      const acceptResult = await appAPI.acceptResultWork(completionResultID, actionID);

      if(!acceptResult) throw new Error('Произошла ошибка при завершении работы');

      Cons.hideLoader();
      showMessage('Работа завершена', 'success');
      closeDocument(_doc);
      updateData();

    } catch (err) {
      Cons.hideLoader();
      showMessage(err.message, 'error');
    }
  },

  finishForm: async function(workID, CompletionFormInfo, _doc) {
    const {formCode, formID} = CompletionFormInfo;
    const formResult = await appAPI.getFormForResult(formCode, workID);
    const player = UTILS.getSynergyPlayer(formResult.dataUUID, true);

    const dialogTitle = i18n.tr("Форма завершения");
    const dialogButton = i18n.tr("Сохранить");
    const msgSuccess = i18n.tr("Работа завершена");

    const dialog = await UTILS.getModalDialog(dialogTitle, player.view.container, dialogButton, async () => {
      Cons.showLoader();
      try {
        if (!player.model.isValid()) throw new Error(i18n.tr('Заполните обязательные поля'));

        player.saveFormData(async saveResult => {
          const resultFinish = await appAPI.finishWork({
              workID: workID,
              completionForm: "FORM",
              type: 'work',
              file_identifier: formResult.file_identifier
          });

          if(!resultFinish) throw new Error(i18n.tr('Произошла ошибка при завершении работы'));

          Cons.hideLoader();
          showMessage(msgSuccess, 'success');
          UIkit.modal(dialog).hide();
          player.destroy();
          closeDocument(_doc);
          updateData();
        });

      } catch (err) {
        Cons.hideLoader();
        UIkit.modal(dialog).hide();
        player.destroy();
        showMessage(err.message, 'error');
      }
    });

    UIkit.modal(dialog).show();
    dialog.on('hidden', () => {
      dialog.remove();
    });
  },

  finishComment: async function(workID, _doc) {
    const body = await getCommentForDialog();

    const dialogTitle = i18n.tr("Комментарий");
    const dialogButton = i18n.tr("Сохранить");
    const msgSuccess = i18n.tr("Работа завершена");

    const dialog = await UTILS.getModalDialog(dialogTitle, body.container, dialogButton, async () => {
      Cons.showLoader();
      try {
        const resultFinish = await appAPI.finishWork({
            workID: workID,
            completionForm: "COMMENT",
            comment: body.textarea.val()
        });

        if(!resultFinish) throw new Error('Произошла ошибка при завершении работы');

        Cons.hideLoader();
        showMessage(msgSuccess, 'success');
        UIkit.modal(dialog).hide();
        closeDocument(_doc);
        updateData();

      } catch (err) {
        Cons.hideLoader();
        UIkit.modal(dialog).hide();
        showMessage(err.message, 'error');
      }
    });

    UIkit.modal(dialog).show();
    dialog.on('hidden', () => {
      dialog.remove();
    });
  },

  finishFile: async function(workID, _doc) {
    const body = getFileForDialog();
    const {container, inputFile} = body;

    const dialogTitle = i18n.tr("Файл");
    const dialogButton = i18n.tr("Сохранить");
    const msgSuccess = i18n.tr("Работа завершена");

    const dialog = await UTILS.getModalDialog(dialogTitle, container, dialogButton, async () => {
      Cons.showLoader();
      try {

        if(!inputFile[0].files.length) throw new Error('Необходимо прикрепить файл');

        const sourceFile = inputFile[0].files[0];
        const fileReader = new FileReader();

        if(sourceFile.size > 20971520) throw new Error('Размер файла не должен превышать 20 МБ');

        const filePath = await appAPI.startUpload();
        if(!filePath) throw new Error('Ошибка создания временного файла');

        fileReader.onload = async function (e) {

          const b64encoded = btoa(new Uint8Array(e.target.result).reduce((data, byte) => data + String.fromCharCode(byte), ''));
          const data = new FormData();
          data.append('body', b64encoded);

          const uploadResult = await appAPI.uploadPart(filePath.file, data);
          if(!uploadResult) throw new Error('Ошибка загрузки файла');
          if(uploadResult.errorCode != '0') throw new Error(uploadResult.errorMessage);

          const resultFinish = await appAPI.finishWork({
              workID: workID,
              completionForm: "FILE",
              type: 'file',
              file_name: sourceFile.name,
              file_path: filePath.file
          });

          if(!resultFinish) throw new Error('Произошла ошибка при завершении работы');

          Cons.hideLoader();
          showMessage(msgSuccess, 'success');
          UIkit.modal(dialog).hide();
          closeDocument(_doc);
          updateData();

        }
        fileReader.readAsArrayBuffer(sourceFile);

      } catch (err) {
        Cons.hideLoader();
        showMessage(i18n.tr(err.message), 'error');
      }
    });

    UIkit.modal(dialog).show();
    dialog.on('hidden', () => {
      dialog.remove();
    });
  },

  finishProcess: async function(workInfo, _doc) {
    const {systemSettings} = Cons.getAppStore();
    const {needCert, enableEDS} = systemSettings;

    const {actionID, procInstID, name, parent_process} = workInfo;
    const procInfo = await appAPI.getProcessInfo(actionID);
    const {raw_data, demandSign, buttons} = procInfo;

    const dialog = await UTILS.getDialogFinishProcess(name, buttons, parent_process, async (signal, comment) => {
      Cons.showLoader();
      try {
        let succesMsg = 'Процесс завершен';

        const finishProcessBody = {
          procInstID, signal, comment,
          rawdata: raw_data
        };

        if(enableEDS && needCert && demandSign == "true") {
          NCALayer.sign('SIGN', _doc.documentID, async result => {
            Cons.showLoader();

            try {
              finishProcessBody.signdata = result.signedData;
              finishProcessBody.certificate = result.certificate;
              finishProcessBody.certID = result.certID;
              finishProcessBody.addSignature = true;
              succesMsg = 'Документ успешно подписан ЭЦП';

              const resultFinish = await appAPI.finishProcess(finishProcessBody);
              if(!resultFinish) throw new Error('Произошла ошибка при подписании документа');

              Cons.hideLoader();
              showMessage(i18n.tr(succesMsg), 'success');
              UIkit.modal(dialog).hide();
              closeDocument(_doc);
              updateData();

            } catch (e) {
              Cons.hideLoader();
              UIkit.modal(dialog).hide();
              showMessage(i18n.tr(e.message), 'error');
            }

          });
        } else {
          Cons.showLoader();

          const resultFinish = await appAPI.finishProcess(finishProcessBody);
          if(!resultFinish) throw new Error('Произошла ошибка при завершении процесса');

          Cons.hideLoader();
          showMessage(i18n.tr(succesMsg), 'success');
          UIkit.modal(dialog).hide();
          closeDocument(_doc);
          updateData();

        }

      } catch (err) {
        Cons.hideLoader();
        UIkit.modal(dialog).hide();
        showMessage(i18n.tr(err.message), 'error');
      }
    });

    UIkit.modal(dialog).show();
    dialog.on('hidden', () => {
      dialog.remove();
    });
  },

  finishDocument: async function(workID, childDocuments, _doc) {
    try {
      if(!childDocuments || !Array.isArray(childDocuments) || !childDocuments.length) {
        throw new Error(i18n.tr('Не удалось получить список дочерних документов'));
      }

      const body = await getDocumentForDialog(childDocuments);

      const dialogTitle = i18n.tr("Результат завершения");
      const dialogButton = i18n.tr("Готово");
      const msgSuccess = i18n.tr("Работа завершена");

      const dialog = await UTILS.getModalDialog(dialogTitle, body.container, dialogButton, async () => {
        Cons.showLoader();
        try {
          const child = childDocuments.find(x => x.documentID == body.select.val());

          const resultFinish = await appAPI.finishWork({
              workID: workID,
              completionForm: "DOCUMENT",
              documentID: child.documentID,
              document_title: child.name
          });

          if(!resultFinish) throw new Error(i18n.tr('Произошла ошибка при завершении работы'));

          Cons.hideLoader();
          showMessage(msgSuccess, 'success');
          UIkit.modal(dialog).hide();
          closeDocument(_doc);
          updateData();

        } catch (err) {
          Cons.hideLoader();
          UIkit.modal(dialog).hide();
          showMessage(err.message, 'error');
        }
      });

      UIkit.modal(dialog).show();
      dialog.on('hidden', () => {
        dialog.remove();
      });
    } catch (e) {
      Cons.hideLoader();
      showMessage(e.message, 'error');
    }
  },

  create: async function(){
    return new Promise(async resolve => {
      try {
        const dialogInfo = await appAPI.getDialogInfo();
        const defaultParamWork = await appAPI.getDefaultParamWork();
        const {container, workData} = await getCreateWorkForDialog(dialogInfo, defaultParamWork);

        const dialogButton = i18n.tr("Создать");
        const msgSuccess = "Работа создана";

        const dialog = await UTILS.getModalDialog(dialogInfo.name, container, dialogButton, async () => {
          Cons.showLoader();
          try {

            if(workData.NAME == "") throw new Error('Не задано название работы');
            if(!workData.RESPONSIBLE) throw new Error('Не выбран ответственный');
            // if(!workData.USERS) throw new Error('Не выбран исполнитель');

            const createWorkBody = new URLSearchParams();

            createWorkBody.append("authorID", AS.OPTIONS.currentUser.userid); //uuid автора работы
            createWorkBody.append("name", workData.NAME); //название работе
            createWorkBody.append("startDate", workData.DATES.start); //дата и время начала работы в формате yyyy-MM-dd HH:mm:ss
            createWorkBody.append("finishDate", workData.DATES.finish); //дата и время завершения работы в формате yyyy-MM-dd HH:mm:ss
            createWorkBody.append("userID", workData.RESPONSIBLE[0].personID); //uuid ответственного
            createWorkBody.append("priority", workData.PRIORITY.value); //приоритет
            createWorkBody.append("completionFormCode", workData.COMPLETION_FORM.code); //код формы завершения
            createWorkBody.append("completionFormID", workData.COMPLETION_FORM.value); //идентификатор формы завершения

            // uuid исполнителей работы
            // if(workData.USERS && workData.USERS.length > 0) {
            //   for(let i = 0; i < workData.USERS.length; i++) {
            //     const user = workData.USERS[i];
            //     createWorkBody.append("resUserID", user.personID);
            //   }
            // }

            // комментарий, который будет добавлен к работе после ее создания
            if(workData.COMMENT !== "") createWorkBody.append("comment", workData.COMMENT);

            // список вложений
            const {inputFile} = workData;
            const sourceFile = inputFile[0].files[0];

            if(sourceFile) {
              if(sourceFile.size > 20971520) throw new Error('Размер файла не должен превышать 20 МБ');
              const fileReader = new FileReader();

              fileReader.onload = async function (e) {
                const filePath = await appAPI.startUpload();
                if(!filePath) throw new Error('Ошибка создания временного файла');

                const b64encoded = btoa(new Uint8Array(e.target.result).reduce((data, byte) => data + String.fromCharCode(byte), ''));
                const data = new FormData();
                data.append('body', b64encoded);

                const uploadResult = await appAPI.uploadPart(filePath.file, data);
                if(!uploadResult) throw new Error('Ошибка загрузки файла');
                if(uploadResult.errorCode != '0') throw new Error(uploadResult.errorMessage);

                createWorkBody.append("path", `${filePath.file}:${sourceFile.name}`);

                const resultCreate = await appAPI.createWork(createWorkBody);

                if(resultCreate.errorCode != 0) {
                  Cons.hideLoader();
                  showMessage(i18n.tr(resultCreate.errorMessage), 'error');
                } else {
                  Cons.hideLoader();
                  showMessage(i18n.tr(msgSuccess), 'success');
                  UIkit.modal(dialog).hide();
                  updateData();
                }
              }

              fileReader.readAsArrayBuffer(sourceFile);
            } else {
              const resultCreate = await appAPI.createWork(createWorkBody);

              if(resultCreate.errorCode != 0) throw new Error(resultCreate.errorMessage);

              Cons.hideLoader();
              showMessage(i18n.tr(msgSuccess), 'success');
              UIkit.modal(dialog).hide();
              updateData();
            }

          } catch (err) {
            Cons.hideLoader();
            showMessage(i18n.tr(err.message), 'error');
          }
        });

        UIkit.modal(dialog).show();
        dialog.on('hidden', () => {
          dialog.remove();
        });

      } catch (err) {
        resolve({
          errorCode: 100500,
          errorMessage: err.message
        });
      }
    });
  },

  edit: async function(work){
    return new Promise(async resolve => {
      try {
        const {container, workData} = getEditWorkForDialog(work);

        const dialogButton = i18n.tr("Сохранить");
        const msgSuccess = "Работа изменена";

        const dialog = await UTILS.getModalDialog(i18n.tr("Работа"), container, dialogButton, async () => {
          Cons.showLoader();
          try {

            if(workData.NAME == "") throw new Error('Не задано название работы');
            if(!workData.RESPONSIBLE || !workData.RESPONSIBLE.length) throw new Error('Не выбран ответственный');

            const editWorkBody = new URLSearchParams();

            editWorkBody.append("actionID", work.actionID); //название работы
            editWorkBody.append("name", workData.NAME); //название работы
            editWorkBody.append("startDate", workData.start); //дата и время начала работы в формате yyyy-MM-dd HH:mm:ss
            editWorkBody.append("finishDate", workData.finish); //дата и время завершения работы в формате yyyy-MM-dd HH:mm:ss
            editWorkBody.append("userID", workData.RESPONSIBLE[0].personID); //uuid ответственного
            editWorkBody.append("priority", workData.PRIORITY.value); //приоритет
            editWorkBody.append("completionFormCode", workData.COMPLETION_FORM.code); //код формы завершения
            editWorkBody.append("completionFormID", workData.COMPLETION_FORM.value); //идентификатор формы завершения

            // список вложений
            const {inputFile} = workData;
            const sourceFile = inputFile[0].files[0];

            if(sourceFile) {
              if(sourceFile.size > 20971520) throw new Error('Размер файла не должен превышать 20 МБ');
              const fileReader = new FileReader();

              fileReader.onload = async function (e) {
                const filePath = await appAPI.startUpload();
                if(!filePath) throw new Error('Ошибка создания временного файла');

                const b64encoded = btoa(new Uint8Array(e.target.result).reduce((data, byte) => data + String.fromCharCode(byte), ''));
                const data = new FormData();
                data.append('body', b64encoded);

                const uploadResult = await appAPI.uploadPart(filePath.file, data);
                if(!uploadResult) throw new Error('Ошибка загрузки файла');
                if(uploadResult.errorCode != '0') throw new Error(uploadResult.errorMessage);

                editWorkBody.append("path", `${filePath.file}:${sourceFile.name}`);

                const resultSave = await appAPI.saveWork(editWorkBody);

                if(resultSave.errorCode != 0) {
                  Cons.hideLoader();
                  showMessage(i18n.tr(resultSave.errorMessage), 'error');
                } else {
                  Cons.hideLoader();
                  showMessage(i18n.tr(msgSuccess), 'success');
                  UIkit.modal(dialog).hide();
                  updateData();
                }
              }

              fileReader.readAsArrayBuffer(sourceFile);
            } else {
              const resultSave = await appAPI.saveWork(editWorkBody);

              if(resultSave.errorCode != 0) throw new Error(resultSave.errorMessage);

              Cons.hideLoader();
              showMessage(i18n.tr(msgSuccess), 'success');
              UIkit.modal(dialog).hide();
              updateData();
            }

          } catch (err) {
            Cons.hideLoader();
            showMessage(i18n.tr(err.message), 'error');
          }
        });

        UIkit.modal(dialog).show();
        dialog.on('hidden', () => {
          dialog.remove();
        });

      } catch (err) {
        resolve({
          errorCode: 100500,
          errorMessage: err.message
        });
      }
    });
  },

  transfer: async function(work){
    const {container, workData} = await getTransferWorkForDialog(work);
    const label = i18n.tr("Передать");
    const dialogButton = i18n.tr("Передать");

    const dialog = await UTILS.getModalDialog(label, container, dialogButton, async () => {
      Cons.showLoader();
      try {
        if(!workData.users || !workData.users.length) throw new Error('Не выбран исполнитель');

        const sendWorkBody = new URLSearchParams();
        sendWorkBody.append("name", workData.name);
        sendWorkBody.append("workID", work.actionID); // UUID работы

        // список идентификаторов пользователей, которым передается работа
        for(let i = 0; i < workData.users.length; i++) {
          const user = workData.users[i];
          sendWorkBody.append("userID", user.personID);
        }

        const resultTransfer = await appAPI.transferWork(sendWorkBody);
        if(resultTransfer.errorCode != 0) throw new Error(resultTransfer.errorMessage);

        Cons.hideLoader();
        showMessage(resultTransfer.errorMessage, 'success');

        UIkit.modal(dialog).hide();

      } catch (err) {
        Cons.hideLoader();
        showMessage(i18n.tr(err.message), 'error');
      }
    });

    UIkit.modal(dialog).show();
    dialog.on('hidden', () => {
      dialog.remove();
    });
  },

  forward: async function(work){
    const {container, workData} = await getForwardWorkForDialog();
    const label = i18n.tr("Переслать");
    const dialogButton = i18n.tr("Отправить");

    const dialog = await UTILS.getModalDialog(label, container, dialogButton, async () => {
      Cons.showLoader();
      try {
        if(!workData.users || !workData.users.length) throw new Error('Не выбран адресат');

        const documentID = await appAPI.getWorkDocument(work.actionID);

        const bodyParams = {
          documentID,
          users: workData.users.map(x => x.personID)
        }

        const resultForward = await appAPI.forwardWork(bodyParams);
        if(resultForward.errorCode != 0) throw new Error(resultForward.errorMessage);

        Cons.hideLoader();
        showMessage(resultForward.errorMessage, 'success');

        UIkit.modal(dialog).hide();

      } catch (err) {
        Cons.hideLoader();
        showMessage(i18n.tr(err.message), 'error');
      }
    });

    UIkit.modal(dialog).show();
    dialog.on('hidden', () => dialog.remove());
  },

  startRoute: async function(event){
    return new Promise(async resolve => {
      try {
        const {label, action: actionType, operation: operationType, actionID: workID, documentID} = event;
        const dialogInfo = await appAPI.getSendWorkInfo({workID, documentID, actionType, operationType});
        const {button_name: dialogButton} = dialogInfo;
        const {container, workData} = await getSendWorkForDialog(actionType, dialogInfo);

        let typeWork = 3;

        if(actionType == 'SEND') {
          switch (operationType) {
            case 'AGREEMENT': typeWork = 0; break;
            case 'APPROVAL': typeWork = 1; break;
            case 'ACQUAINTANCE': typeWork = 2; break;
          }
        }

        const dialog = await UTILS.getModalDialog(label, container, dialogButton, async () => {
          Cons.showLoader();
          try {
            if(workData.name == "") throw new Error('Не задано название работы');
            if(!workData.users || !workData.users.length) throw new Error('Не выбран исполнитель');

            const sendWorkBody = new URLSearchParams();
            sendWorkBody.append("workID", workID); // UUID родительской работы
            sendWorkBody.append("documentID", documentID); // UUID документа
            sendWorkBody.append("type", typeWork); // тип
            sendWorkBody.append("name", workData.name); //название работы
            sendWorkBody.append("finishDate", workData.finishDate); // Дата завершения работ по маршруту в формате yyyy-MM-dd HH:mm:ss

            if(actionType == 'REASSIGN') {
              if(workData.hasOwnProperty('RESPONSIBLE') && workData.RESPONSIBLE.length) {
                sendWorkBody.append("userID", workData.RESPONSIBLE[0].personID); //uuid ответственного
              }
              sendWorkBody.append("completionFormCode", workData.COMPLETION_FORM.code); //код формы завершения
              sendWorkBody.append("completionFormID", workData.COMPLETION_FORM.value); //идентификатор формы завершения
            }

            // список идентификаторов пользователей, которым отправляется работа
            for(let i = 0; i < workData.users.length; i++) {
              const user = workData.users[i];
              sendWorkBody.append("resUserID", user.personID);
            }

            const resultSend = await appAPI.startRouteWork(sendWorkBody);
            if(resultSend.errorCode != 0) throw new Error(resultSend.errorMessage);

            Cons.hideLoader();
            UIkit.modal(dialog).hide();
            resolve({
              errorCode: 0,
              errorMessage: 'Отправлено'
            });
          } catch (err) {
            Cons.hideLoader();
            showMessage(i18n.tr(err.message), 'error');
          }
        });

        UIkit.modal(dialog).show();
        dialog.on('hidden', () => {
          dialog.remove();
        });

      } catch (err) {
        resolve({
          errorCode: 100500,
          errorMessage: err.message
        });
      }
    });
  }

}
