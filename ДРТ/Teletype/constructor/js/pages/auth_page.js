pageHandler('auth_page', () => {
  if (!Cons.getAppStore().auth_page_listener) {

    addListener('auth_success', 'button-login', authed => {
      const {login, password} = authed.creds;

      Cons.creds.login = login;
      Cons.creds.password = password;
      AS.apiAuth.setCredentials(login, password);
      AS.OPTIONS.login = login;
      AS.OPTIONS.password = password;
      AS.OPTIONS.currentUser = authed.data.person;

      Cons.setAppStore({userGroups: authed.data.groups});

      fire({type: 'goto_page', pageCode: 'article_add_page'}, 'root-panel');
    });

    Cons.setAppStore({auth_page_listener: true});
  }
});
