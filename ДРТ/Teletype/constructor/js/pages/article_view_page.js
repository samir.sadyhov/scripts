pageHandler('article_view_page', () => {
  const {articleUUID} = Cons.getAppStore();
  if(articleUUID) fire({type: 'show_form_data', dataId: articleUUID}, 'formPlayer-1');
});
