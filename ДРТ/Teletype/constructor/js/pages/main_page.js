const getUrlParameter = sParam => {
  let sPageURL = window.location.search.substring(1), sURLVariables = sPageURL.split('&'), sParameterName;
  for (let i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split('=');
    if (sParameterName[0] === sParam)
    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
  }
}

const searchInRegistry = params => {
  return new Promise((resolve, reject) => {
    rest.synergyGet(`api/registry/data_ext?${params}`,
      res => resolve(res),
      err => reject(err.responseJSON.errorMessage));
  });
}

const searchArticle = link => {
  let params = $.param({
    registryCode: 'teletype_registry',
    field: 'teletype_form_link_base64',
    condition: 'TEXT_EQUALS',
    value: link,
    countInPart: 1
  });
  ['teletype_form_link_base64', 'teletype_form_link', 'teletype_form_title'].forEach(x => params += `&fields=${x}`);

  return new Promise(resolve => {
    searchInRegistry(params).then(res => {
      res.recordsCount == 0 ? resolve(null) : resolve(res.result);
    }).catch(err => {
      resolve(null);
      console.log('searchArticle ERROR', err);
    });
  });
}

const goErrorPage = () => fire({type: 'goto_page', pageCode: 'page_404'}, 'root-panel');

pageHandler('main_page', () => {
  const urlParam = getUrlParameter('g');

  if(!urlParam) {
    goErrorPage();
  } else if (urlParam == 'admin') {
    fire({type: 'goto_page', pageCode: 'article_add_page'}, 'root-panel');
  } else {
    searchArticle(urlParam).then(res => {
      if(!res) goErrorPage();

      if(urlParam == res[0].fieldValue.teletype_form_link_base64) {
        Cons.setAppStore({articleUUID: res[0].dataUUID});
        fire({type: 'goto_page', pageCode: 'article_view_page'}, 'root-panel');
      } else {
        goErrorPage();
      }
    });
  }

});
