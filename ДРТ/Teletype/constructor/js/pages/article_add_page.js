const generateBase64Data = data => btoa(escape(encodeURIComponent(data)));

const toTranslit = text => {
  return text.replace(/([а-яё])|([\s_-])|([^a-z\d])/gi,
    function (all, ch, space, words, i) {
      if (space || words) return space ? '_' : '';
      var code = ch.charCodeAt(0),
      index = code == 1025 || code == 1105 ? 0 :
      code > 1071 ? code - 1071 : code - 1039,
      t = ['yo', 'a', 'b', 'v', 'g', 'd', 'e', 'zh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o',
      'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'shch', '', 'y', '', 'e', 'yu', 'ya'];
      return t[index];
    });
}

pageHandler('article_add_page', () => {
  let asform;
  let dataUUID;
  let newRow = false;
  let formEditable = false;

  if (!Cons.getAppStore().article_add_page_listener) {

    addListener('registry_item_click', 'registry-1', (e) => {
      dataUUID = e.dataUUID;
      newRow = false;
      fire({type: 'set_hidden', hidden: false}, 'panelFormModal');
    });

    addListener("set_hidden", "panelFormModal", e => {
      if(e.hidden) {
        asform = null;
        dataUUID = null;
        if(newRow) fire({type: 'reload_data'}, 'registry-1');
        return;
      }
      if(!newRow) {
        fire({type: 'show_form_data', dataId: dataUUID}, 'formPlayer-1');
        fire({type: 'set_hidden', hidden: false}, 'panelAction');
        fire({type: 'set_hidden', hidden: true}, 'buttonSave');
        formEditable = false;
      }
    });

    addListener('loaded_form_data', 'formPlayer-1', e => {
      asform = e;
      if(newRow) {
        fire({type: 'set_form_editable', editable: true}, 'formPlayer-1');
        formEditable = true;
      }
    });

    addListener("button_click", "buttonEditForm", () => {
      formEditable = !formEditable;
      fire({type: 'set_form_editable', editable: formEditable}, 'formPlayer-1');
      fire({type: 'set_hidden', hidden: !formEditable}, 'buttonSave');
    });

    addListener("button_click", "buttonCreateRow", () => {
      newRow = true;
      fire({type: 'set_hidden', hidden: false}, 'panelFormModal');
    });

    addListener("button_click", "buttonSave", () => {
      let valid = !asform.model.getErrors().length;
      if (valid) {

        const itemTheme = asform.model.playerModel.getModelWithId('teletype_form_title');
        const itemLink = asform.model.playerModel.getModelWithId('teletype_form_link');
        const itemLinkCode = asform.model.playerModel.getModelWithId('teletype_form_link_base64');

        if(!itemTheme.getValue()) {
          showMessage('Заполните тему', 'error');
          return;
        }

        let b64 = generateBase64Data(toTranslit(itemTheme.getValue()));
        b64 = b64.replaceAll('=', '').substr(0,64);
        itemLinkCode.setValue(b64);
        itemLink.setValue(`${window.location.origin}/teletype/?g=${b64}`);

        if(newRow) {
          fire({type: "set_disabled", disabled: true, cmpID: "buttonSave"}, "buttonSave");
          fire({
            type: "create_form_data",
            registryCode: 'teletype_registry',
            activate: true,
            success: (id, docid) => {
              formEditable = false;
              fire({type: 'set_form_editable', editable: formEditable}, 'formPlayer-1');
              fire({type: 'set_hidden', hidden: true}, 'buttonSave');
              fire({type: "set_disabled", disabled: false, cmpID: "buttonSave"}, "buttonSave");
              fire({type: 'set_hidden', hidden: false}, 'panelAction');
              showMessage('Запись успешно создана', 'success');
            },
            error: (st, err) => {
              console.log("failed" + err.toString());
              showMessage('Ошибка при создании записи', 'error');
            }
          }, 'formPlayer-1');
        } else {
          fire({type: "set_disabled", disabled: true, cmpID: "buttonSave"}, "buttonSave");
          fire({
            type: "save_form_data",
            success: (id, docid) => {
              formEditable = false;
              fire({type: 'set_form_editable', editable: formEditable}, 'formPlayer-1');
              fire({type: 'set_hidden', hidden: true}, 'buttonSave');
              fire({type: "set_disabled", disabled: false, cmpID: "buttonSave"}, "buttonSave");
              fire({type: 'set_hidden', hidden: false}, 'panelAction');
              showMessage('Запись успешно изменена', 'success');
            },
            error: (st, err) => {
              console.log("failed" + err.toString());
              showMessage('Ошибка сохранения записи', 'error');
            }
          }, 'formPlayer-1');
        }

      } else {
        showMessage('Заполните обязательные поля', 'error');
      }
    });

  }
});
