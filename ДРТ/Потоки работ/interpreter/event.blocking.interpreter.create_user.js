function fetchUserFullName(lastname, firstname, patronymic) {
  firstname = firstname ? ' ' + firstname.charAt(0) + '.' : '';
  patronymic = patronymic ? ' ' + patronymic.charAt(0) + '.' : '';
  return lastname + firstname + patronymic;
}

function createNewUser(params) {
  let client = API.getHttpClient();
  let post = new org.apache.commons.httpclient.methods.PostMethod("http://127.0.0.1:8080/Synergy/rest/api/filecabinet/user/save");
  for(let key in params) post.addParameter(key, params[key]);
  post.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=utf-8");
  let resp = client.executeMethod(post);
  resp = JSON.parse(post.getResponseBodyAsString());
  post.releaseConnection();
  return resp;
}

function toTranslit(text) {
  return text.replace(/([а-яё])|([\s_-])|([^a-z\d])/gi,
    function (all, ch, space, words, i) {
      if (space || words) return space ? '_' : '';
      var code = ch.charCodeAt(0),
      index = code == 1025 || code == 1105 ? 0 :
      code > 1071 ? code - 1071 : code - 1039,
      t = ['yo', 'a', 'b', 'v', 'g', 'd', 'e', 'zh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o',
      'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'shch', '', 'y', '', 'e', 'yu', 'ya'];
      return t[index];
    });
}

var result = true;
var message = 'ok';

try {
  let currentFormData = API.getFormData(dataUUID);

  let lastname = UTILS.getValue(currentFormData, "workflow_form_hiring_note_lastname");
  let firstname = UTILS.getValue(currentFormData, "workflow_form_hiring_note_firstname");
  let patronymic = UTILS.getValue(currentFormData, "workflow_form_hiring_note_secondname");

  if(!firstname || !firstname.hasOwnProperty('value')) throw new Error('Не заполнено имя пользователя');
  if(!lastname || !lastname.hasOwnProperty('value')) throw new Error('Не заполнена фамилия пользователя');

  if(!patronymic || !patronymic.hasOwnProperty('value')) {
    patronymic = '';
  } else {
    patronymic = patronymic.value;
  }

  let fullName = fetchUserFullName(lastname.value, firstname.value, patronymic);

  let user = createNewUser({
    lastname: lastname.value,
    firstname: firstname.value,
    patronymic: patronymic,
    pointersCode: toTranslit(lastname.value + ' ' + firstname.value)
  });

  if(user.errorCode == '13') throw new Error(user.errorMessage);
  message = user.errorMessage;

  UTILS.setValue(currentFormData, "workflow_form_hiring_note_userid", {value: fullName, key: user.userID});
  API.mergeFormData(currentFormData);

  let personalFormCard = API.httpGetMethod('rest/api/personalrecord/forms/' + user.userID);

  personalFormCard = personalFormCard.filter(function(x){
    if(x.formCode == 'workflow_form_personal_card') return x;
  })[0];

  if(!personalFormCard) throw new Error('Не найдена карточка пользователя');

  personalFormCard = API.getFormData(personalFormCard['data-uuid']);

  let matching = [];
  matching.push({from: 'workflow_form_hiring_note_id', to: 'workflow_form_personal_card_id'});
  matching.push({from: 'workflow_form_hiring_note_birthday', to: 'workflow_form_personal_card_birthday'});
  matching.push({from: 'workflow_form_hiring_note_idcard_number', to: 'workflow_form_personal_card_idcard_number'});
  matching.push({from: 'workflow_form_hiring_note_rus_idcard_govag', to: 'workflow_form_personal_card_rus_idcard_govag'});
  matching.push({from: 'workflow_form_hiring_note_kz_idcard_govag', to: 'workflow_form_personal_card_kz_idcard_govag'});
  matching.push({from: 'workflow_form_hiring_note_idcard_date', to: 'workflow_form_personal_card_idcard_date'});
  matching.push({from: 'workflow_form_hiring_note_rus_adress', to: 'workflow_form_personal_card_rus_adress'});
  matching.push({from: 'workflow_form_hiring_note_kz_adress', to: 'workflow_form_personal_card_kz_adress'});
  matching.push({from: 'workflow_form_hiring_note_idcard', to: 'workflow_form_personal_card_idcard'});
  matching.push({from: 'workflow_form_hiring_note_application', to: 'workflow_form_personal_card_application'});
  matching.push({from: 'workflow_form_hiring_note_resume', to: 'workflow_form_personal_card_resume'});

  matching.forEach(function(id) {
    let fromData = UTILS.getValue(currentFormData, id.from);
    if(fromData && fromData.hasOwnProperty('value')) {
      let field = {id: id.to};
      for(let key in fromData) {
        if(key === 'id' || key === 'username' || key === 'userID') continue;
        field[key] = fromData[key];
      }
      UTILS.setValue(personalFormCard, id.to, field);
    }
  });

  API.mergeFormData(personalFormCard);

} catch (err) {
  log.error(err.message);
  result = false;
  message = err.message;
}
