var result = true;
var message = 'ok';

function getDepartmentsContent(departmentID) {
  return API.httpGetMethod("rest/api/departments/content?departmentID=" + departmentID);
}

function getDepartmentInfo(departmentID) {
  return API.httpGetMethod("rest/api/departments/get?departmentID=" + departmentID);
}

function getUserInfo(userID) {
  return API.httpGetMethod("rest/api/filecabinet/user/" + userID);
}

function createTimesheet(asfData, half) {
  let registryCode = 'timesheet_month';
  if(half != '1') registryCode = 'timesheet_halfmonth';
  return API.httpPostMethod("rest/api/registry/create_doc_rcc", {
    registryCode: registryCode,
    data: asfData,
    sendToActivation: true
  }, "application/json; charset=utf-8");
}

function parseDepartmentsData(data){
  let result = [];
  let ids = data.key.split(';');
  let names = data.value.split(';;');

  ids.forEach(function(id, i){
    result.push({
      id: id,
      name: names[i].trim()
    });
  });
  return result;
}

/*
  @param data - result getDepartmentsContent
  @return - список юзеров с должностями
*/
function getEmployees(data, department) {
  let res = [];

  data.departments.forEach(function(dep){
    if(dep.departmentHead.hasOwnProperty('headID')) {
      let head = getUserInfo(dep.departmentHead.headID);
      res.push({
        departmentID: dep.departmentID,
        departmentName: dep.departmentName,
        positionID: head.positions[0].positionID,
        positionName: head.positions[0].positionName,
        employeeID: dep.departmentHead.headID,
        employeeName: dep.departmentHead.headName
      });
    }
  });

  data.positions.forEach(function(pos){
    pos.employees.forEach(function(user){
      res.push({
        departmentID: department.key,
        departmentName: department.value,
        positionID: pos.positionID,
        positionName: pos.positionName,
        employeeID: user.employeeID,
        employeeName: user.employeeName
      });
    });
  });

  return res;
}

function getGroupEmployees(item){
  try {
    if(!item.hasOwnProperty('group')) throw new Error('Не найдено поля группы');
    if(!item.group.hasOwnProperty('key') || item.group.key == '') throw new Error('Не заполнено поле группы');

    let employees = [];
    let groupAsfData = API.getFormData(API.getAsfDataId(item.group.key));
    let table = UTILS.getValue(groupAsfData, "workflow_form_group_table_users");
    let users = UTILS.parseAsfTable(table);

    users.forEach(function(x){
      employees.push({
        departmentID: x.department.key,
        departmentName: x.department.value,
        positionID: x.position.key,
        positionName: x.position.value,
        employeeID: x.user.key,
        employeeName: x.user.value
      });
    });

    return {errorCode: 0, employees: employees};
  } catch (err) {
    return {errorCode: 13, errorMessage: err.message};
  }
}

function fetchUserFullName(lastname, firstname, patronymic) {
  firstname = firstname ? ' ' + firstname.charAt(0) + '.' : '';
  patronymic = patronymic ? ' ' + patronymic.charAt(0) + '.' : '';
  return lastname + firstname + patronymic;
}

function createAsfData(inputMonth, inputYear, employees, item) {
  let asfData = [];
  let table = {id: 'table', type: 'appendable_table', data: []};

  asfData.push({id: 'department', type: 'entity', value: item.department.value, key: item.department.key});
  asfData.push({id: 'month', type: 'listbox', value: inputMonth.value, key: inputMonth.key});
  asfData.push({id: 'year', type: 'listbox', value: inputYear.value, key: inputYear.key});
  asfData.push({id: 'user_manager', type: 'entity', value: item.manager.value, key: item.manager.key});
  asfData.push({id: 'position_manager', type: 'entity', value: item.managerPosition.value, key: item.managerPosition.key});

  if(item.type.value == '3') {
    asfData.push({id: 'group', type: 'reglink', value: item.group.value, key: item.group.key});
  }

  asfData.push(table);

  employees.forEach(function(employ, i){
    let tableBlockIndex = i + 1;
    table.data.push({id: 'department-b' + tableBlockIndex, type: 'entity', key: employ.departmentID, value: employ.departmentName});
    table.data.push({id: 'position-b' + tableBlockIndex, type: 'entity', key: employ.positionID, value: employ.positionName});
    table.data.push({id: 'user-b' + tableBlockIndex, type: 'entity', key: employ.employeeID, value: employ.employeeName});
  });

  return asfData;
}


function getTimesheetPeriod(inputYear, inputMonth, half) {
  let start = inputYear.value + '-' + inputMonth.key + '-01 00:00:00';
  let stop = new Date(inputYear.value + '-' + inputMonth.key + '-01 23:59:59');

  if(half != '1') {
    stop.setDate(15);
  } else {
    stop.setMonth(stop.getMonth() + 1);
    stop.setDate(stop.getDate() - 1);
  }

  return {
    start: start,
    stop: UTILS.formatDate(stop, true),
    endDay: stop.getDate()
  };
}

function parseTimesheetOrderSettings(data) {
  if(data.recordsCount == 0) return null;
  //приём - 1, перевод - 2, увольнение - 3
  let orders = {};

  data.result.forEach(function(x){
    let fieldValue = x.fieldValue;
    let fieldKey = x.fieldKey;

    if(!orders.hasOwnProperty(fieldKey.workflow_form_timesheet_order_settings_type)) {
      orders[fieldKey.workflow_form_timesheet_order_settings_type] = [];
    }

    orders[fieldKey.workflow_form_timesheet_order_settings_type].push({
      registryCode: fieldValue.workflow_form_timesheet_order_settings_registryCode || '',
      user: fieldValue.workflow_form_timesheet_order_settings_user || '',
      position: fieldValue.workflow_form_timesheet_order_settings_position || '',
      department: fieldValue.workflow_form_timesheet_order_settings_department || '',
      date: fieldValue.workflow_form_timesheet_order_settings_date || '',
      to_position: fieldValue.workflow_form_timesheet_order_settings_to_position || '',
      to_department: fieldValue.workflow_form_timesheet_order_settings_to_department || ''
    });
  });

  return orders;
}

function getTimesheetOrderSettings(){
  let fields = [
    "workflow_form_timesheet_order_settings_type",
    "workflow_form_timesheet_order_settings_registryCode",
    "workflow_form_timesheet_order_settings_user",
    "workflow_form_timesheet_order_settings_position",
    "workflow_form_timesheet_order_settings_department",
    "workflow_form_timesheet_order_settings_date",
    "workflow_form_timesheet_order_settings_to_position",
    "workflow_form_timesheet_order_settings_to_department"
  ];
  let searchURL = 'rest/api/registry/data_ext?registryCode=workflow_registry_timesheet_order_settings';
  searchURL += '&registryRecordStatus=STATE_SUCCESSFUL';

  fields.forEach(function(field){searchURL += '&fields=' + field;});

  let searchResult = API.httpGetMethod(searchURL);

  return parseTimesheetOrderSettings(searchResult);
}

function searchAdmissionOrder(timesheetOrderSettings, departmentID, period) {
  let result = [];
  if(!timesheetOrderSettings.hasOwnProperty('1')) return null;
  let orders = timesheetOrderSettings['1'];

  let stop = new Date(period.stop);
  stop.setMonth(stop.getMonth() + 1);
  stop.setDate(stop.getDate() - 1);
  stop = UTILS.formatDate(stop, true);

  orders.forEach(function(order){
    let fields = [
      order.user,
      order.position,
      order.department,
      order.date
    ];

    let urlSearch = 'rest/api/registry/data_ext?registryCode=' + encodeURIComponent(order.registryCode);
    urlSearch += '&field='+order.department+'&condition=TEXT_EQUALS&key=' + encodeURIComponent(departmentID);
    urlSearch += '&field1='+order.date+'&condition1=MORE_OR_EQUALS&key1=' + encodeURIComponent(period.start);
    urlSearch += '&field2='+order.date+'&condition2=LESS_OR_EQUALS&key2=' + encodeURIComponent(stop);

    fields.forEach(function(field){urlSearch += '&fields=' + field;});

    urlSearch += '&term=and&groupTerm=and&countInPart=1&registryRecordStatus=STATE_SUCCESSFUL';

    let searchResult = API.httpGetMethod(urlSearch);

    if(searchResult && searchResult.recordsCount > 0) {
      searchResult.result.forEach(function(x){x.settings = order;});
      result = result.concat(searchResult.result);
    }
  });

  if(result.length) {
    return result;
  } else {
    return null;
  }
}

function searchTransferOrder(timesheetOrderSettings, departmentID, period) {
  let result = [];
  if(!timesheetOrderSettings.hasOwnProperty('2')) return null;
  let orders = timesheetOrderSettings['2'];

  let stop = new Date(period.stop);
  stop.setMonth(stop.getMonth() + 1);
  stop.setDate(stop.getDate() - 1);
  stop = UTILS.formatDate(stop, true);

  orders.forEach(function(order){
    let fields = [
      order.user,
      order.position,
      order.department,
      order.date,
      order.to_position,
      order.to_department
    ];

    let urlSearch = 'rest/api/registry/data_ext?registryCode=' + encodeURIComponent(order.registryCode);
    urlSearch += '&field='+order.department+'&condition=TEXT_EQUALS&key=' + encodeURIComponent(departmentID);
    urlSearch += '&field='+order.to_department+'&condition=TEXT_EQUALS&key=' + encodeURIComponent(departmentID);
    urlSearch += '&field1='+order.date+'&condition1=MORE_OR_EQUALS&key1=' + encodeURIComponent(period.start);
    urlSearch += '&field2='+order.date+'&condition2=LESS_OR_EQUALS&key2=' + encodeURIComponent(stop);

    fields.forEach(function(field){urlSearch += '&fields=' + field;});

    urlSearch += '&term=or&groupTerm=and&registryRecordStatus=STATE_SUCCESSFUL';

    let searchResult = API.httpGetMethod(urlSearch);

    if(searchResult && searchResult.recordsCount > 0) {
      searchResult.result.forEach(function(x){x.settings = order;});
      result = result.concat(searchResult.result);
    }
  });

  if(result.length) {
    return result;
  } else {
    return null;
  }
}

function searchDismissalOrder(timesheetOrderSettings, departmentID, period) {
  let result = [];
  if(!timesheetOrderSettings.hasOwnProperty('3')) return null;
  let orders = timesheetOrderSettings['3'];

  let stop = new Date(period.stop);
  stop.setMonth(stop.getMonth() + 1);
  stop.setDate(stop.getDate() - 1);
  stop = UTILS.formatDate(stop, true);

  orders.forEach(function(order){
    let fields = [
      order.user,
      order.position,
      order.department,
      order.date
    ];

    let urlSearch = 'rest/api/registry/data_ext?registryCode=' + encodeURIComponent(order.registryCode);
    urlSearch += '&field='+order.department+'&condition=TEXT_EQUALS&key=' + encodeURIComponent(departmentID);
    urlSearch += '&field1='+order.date+'&condition1=MORE_OR_EQUALS&key1=' + encodeURIComponent(period.start);
    urlSearch += '&field2='+order.date+'&condition2=LESS_OR_EQUALS&key2=' + encodeURIComponent(stop);

    fields.forEach(function(field){urlSearch += '&fields=' + field;});

    urlSearch += '&term=and&groupTerm=and&countInPart=1&registryRecordStatus=STATE_SUCCESSFUL';

    let searchResult = API.httpGetMethod(urlSearch);

    if(searchResult && searchResult.recordsCount > 0) {
      searchResult.result.forEach(function(x){x.settings = order;});
      result = result.concat(searchResult.result);
    }
  });

  if(result.length) {
    return result;
  } else {
    return null;
  }
}

function searchUserPos(positions, posIDs) {
  let position = null;
  positions.forEach(function(pos){
    posIDs.forEach(function(posID){
      if(pos.positionID == posID) {
        position = {
          positionID: pos.positionID,
          positionName: pos.positionName
        }
      }
    });
  });

  return position;
}

try {
  let currentFormData = API.getFormData(dataUUID);
  let half = UTILS.getValue(currentFormData, "half");
  let dep_head = UTILS.getValue(currentFormData, "dep_head");
  let inputMonth = UTILS.getValue(currentFormData, "month");
  let inputYear = UTILS.getValue(currentFormData, "year");
  let table = UTILS.getValue(currentFormData, "managers_list");

  let data = UTILS.parseAsfTable(table);
  let period = getTimesheetPeriod(inputYear, inputMonth, half.key || '1');

  let timesheetOrderSettings = getTimesheetOrderSettings();

  for(let index = 0; index < data.length; index++) {
    let item = data[index];
    if(!item.hasOwnProperty('type') || !item.type.hasOwnProperty('value') || item.type.value == '') continue;

    if(item.type.value == '1') { // на 1 сотрудника

      let employees = [{
        departmentID: item.department.key,
        departmentName: item.department.value,
        positionID: item.position.key,
        positionName: item.position.value,
        employeeID: item.user.key,
        employeeName: item.user.value
      }];

      let asfData = createAsfData(inputMonth, inputYear, employees, item);

      createTimesheet(asfData, half.key || '1');

    } else if(item.type.value == '2') { // на подразделение

      let depContent = getDepartmentsContent(item.department.key);
      let depInfo = getDepartmentInfo(item.department.key);
      if(depInfo && depInfo.manager) {
        depContent.positions.push({
          positionID: depInfo.manager.positionID,
          positionName: depInfo.manager.nameRu,
          employees: []
        });
      }

      depContent.departments.forEach(function(dep){
        let depInfo = getDepartmentInfo(dep.departmentID);
        if(depInfo && depInfo.manager) {
          let f = depContent.positions.filter(function(x){
            if(x.positionID == depInfo.manager.positionID) {
              return x;
            }
          });

          if(!f.length) {
            depContent.positions.push({
              positionID: depInfo.manager.positionID,
              positionName: depInfo.manager.nameRu,
              employees: []
            });
          }
        }
      });

      let employees = getEmployees(depContent, item.department);
      let userAdmissionOrders = searchAdmissionOrder(timesheetOrderSettings, item.department.key, period); //приём - 1
      let userTransferOrders = searchTransferOrder(timesheetOrderSettings, item.department.key, period); //перевод - 2
      let userDismissalOrders = searchDismissalOrder(timesheetOrderSettings, item.department.key, period); //увольнение - 3

      if(dep_head && dep_head.hasOwnProperty('value') && dep_head.value == '1') {
        if(depInfo && depInfo.manager && depInfo.manager.managerID) {
          let head = getUserInfo(depInfo.manager.managerID);
          employees.push({
            departmentID: depInfo.departmentID,
            departmentName: depInfo.nameRu,
            positionID: depInfo.manager.positionID,
            positionName: depInfo.manager.nameRu,
            employeeID: head.userid,
            employeeName: fetchUserFullName(head.lastname, head.firstname, head.patronymic)
          });
        }
      }

      //приказы об увольнении
      if(userDismissalOrders && userDismissalOrders.length) {
        userDismissalOrders.forEach(function(order){
          let v = order.fieldValue;
          let k = order.fieldKey;
          let s = order.settings;

          //если есть приказ на юзера и дата увольнения меньше чем период табеля
          if(half.key != '1' && k[s.date] > period.stop) {

            //ищем должность на которой он был ранее
            let pos = searchUserPos(depContent.positions, [k[s.position]]);

            if(pos) {
              employees.push({
                departmentID: k[s.department],
                departmentName: v[s.department],
                positionID: pos.positionID,
                positionName: pos.positionName,
                employeeID: k[s.user],
                employeeName: v[s.user]
              });
            }

          } else if (half.key == '1') {
            //ищем должность на которой он был ранее
            let pos = searchUserPos(depContent.positions, [k[s.position]]);

            if(pos) {
              employees.push({
                departmentID: k[s.department],
                departmentName: v[s.department],
                positionID: pos.positionID,
                positionName: pos.positionName,
                employeeID: k[s.user],
                employeeName: v[s.user]
              });
            }
          }

        });
      }

      //поиск приказов о переводе
      if(userTransferOrders && userTransferOrders.length) {
        userTransferOrders.forEach(function(order){
          let v = order.fieldValue;
          let k = order.fieldKey;
          let s = order.settings;

          //ищем должность на которой он был ранее
          let pos = searchUserPos(depContent.positions, [k[s.position], k[s.to_position]]);

          //если такая есть то добавляем в список юзеров для генерации табеля
          if(pos) {
            //проверка есть ли чувак уже в списке
            let f = employees.filter(function(x){
              if(x.positionID == pos.positionID && x.employeeID == k[s.user]) {
                return x;
              }
            });

            //добавить в список если нет
            if(!f.length) {
              employees.push({
                departmentID: item.department.key,
                departmentName: item.department.value,
                positionID: pos.positionID,
                positionName: pos.positionName,
                employeeID: k[s.user],
                employeeName: v[s.user]
              });
            } else {
              if(item.department.key == k[s.department]) {
                employees.push({
                  departmentID: item.department.key,
                  departmentName: item.department.value,
                  positionID: k[s.position],
                  positionName: v[s.position],
                  employeeID: k[s.user],
                  employeeName: v[s.user]
                });
              }

            }
          }

        });
      }

      //приказы о приёме
      if(userAdmissionOrders && userAdmissionOrders.length) {
        userAdmissionOrders.forEach(function(order){
          let k = order.fieldKey;
          let s = order.settings;

          //если есть приказ на юзера и дата приема больше чем период табеля
          if(half.key != '1' && k[s.date] > period.stop) {
            //то выпиливаем юзера
            let f = employees.filter(function(x){
              if(x.employeeID != k[s.user]) return x;
            });
            employees = f;
          }
        });
      }

      if(employees.length > 0) {
        let asfData = createAsfData(inputMonth, inputYear, employees, item);
        createTimesheet(asfData, half.key || '1');
      }

    } else if(item.type.value == '3') { // на группу сотрудников

      let groupData = getGroupEmployees(item);

      if(groupData && groupData.errorCode == 0) {
        let employees = groupData.employees;

        if(dep_head && dep_head.hasOwnProperty('value') && dep_head.value == '1') {
          let depInfo = getDepartmentInfo(item.department.key);

          if(depInfo && depInfo.manager && depInfo.manager.managerID) {
            let head = getUserInfo(depInfo.manager.managerID);
            employees.push({
              departmentID: depInfo.departmentID,
              departmentName: depInfo.nameRu,
              positionID: depInfo.manager.positionID,
              positionName: depInfo.manager.nameRu,
              employeeID: head.userid,
              employeeName: fetchUserFullName(head.lastname, head.firstname, head.patronymic)
            });
          }
        }

        if(employees.length > 0) {
          let asfData = createAsfData(inputMonth, inputYear, employees, item);
          createTimesheet(asfData, half.key || '1');
        }

      }
    }
  }

} catch (err) {
  message = err.message;
  result = false;
}
