function parseOrderSettings(data){
  if(data.recordsCount == 0) return [];
  let result = [];
  data.result.forEach(function(x){
    result.push({
      abbr: x.fieldValue.workflow_form_workTable_settings_abbreviation || null,
      amount: x.fieldValue.workflow_form_workTable_settings_table_amount || null,
      sum: x.fieldValue.workflow_form_workTable_settings_table_sum || null
    });
  });
  return result;
}

function getOrderSettings(){
  let fields = [
    "workflow_form_workTable_settings_abbreviation",
    "workflow_form_workTable_settings_table_amount",
    "workflow_form_workTable_settings_table_sum"
  ];
  let searchURL = 'rest/api/registry/data_ext?registryCode=workTable_settings';
  searchURL += '&registryRecordStatus=STATE_SUCCESSFUL';
  fields.forEach(function(field){searchURL += '&fields=' + field;});

  let searchResult = API.httpGetMethod(searchURL);

  return parseOrderSettings(searchResult);
}

function getPersonalRecord(userID){
  let searchResult = API.httpGetMethod('rest/api/personalrecord/forms/' + userID);
  let userCard = searchResult.filter(function(x){
    if(x.formCode == 'workflow_form_trudovye_otnosheniya') return x;
  })[0];

  return userCard ? API.getFormData(userCard['data-uuid']) : null;
}

function getUserWorkTimeMode(userID, positionID){
  let personalCardData = getPersonalRecord(userID);
  if(!personalCardData) return null;

  let table = UTILS.parseAsfTable(UTILS.getValue(personalCardData, 'table-positions'));
  if(!table) return null;

  let block = table.filter(function(x){
    if(x.position.key == positionID) return x;
  })[0];
  if(!block) return null;

  if(!block.workmodeLink || !block.workmodeLink.hasOwnProperty('key')) return null;
  let workModelAsfData = API.getFormData(API.getAsfDataId(block.workmodeLink.key));

  if(!workModelAsfData) return null;

  return {
    scheduleType: UTILS.getValue(workModelAsfData, 'workflow_form_workmode_schedule'),
    scheduleSumHours: UTILS.getValue(workModelAsfData, 'workflow_form_workmode_sum_hours'),
    schedule: JSON.parse(UTILS.getValue(workModelAsfData, 'schedule_json').value || {})
  }
}

function fillResultField(timesheet, row, tableBlockIndex, userWorkTimeMode, tabelMonth){
  let scheduleType = userWorkTimeMode ? userWorkTimeMode.scheduleType || null : null;
  let scheduleSumHours = userWorkTimeMode ? userWorkTimeMode.scheduleSumHours || null : null;
  let schedule = userWorkTimeMode ? userWorkTimeMode.schedule || null : null;
  let work_days_res = 0;
  let work_hours_res = 0;
  let night_hours_res = 0;
  let weekends_and_holidays_res = 0;

  for(let day = 1; day <= 31; day++) {
    let dayField = 'day_' + day;
    if(!row.hasOwnProperty(dayField)) continue;

    let value = row[dayField].value || null;
    if(!value) continue;
    value = value.toUpperCase();

    value = value.split(';');

    if(value.indexOf('В') !== - 1 || value.indexOf('П') !== - 1)  {
      weekends_and_holidays_res++;
      continue;
    }

    for(let i = 0; i < value.length; i++) {
      let v = value[i];
      if(v == '') continue;

      if(scheduleType && scheduleType.value == '2') {
        if(v.indexOf('/') !== -1) {
          v = v.split('/');
          let d = Number(v[0].replace(/[^\d]/g, ''));
          let n = Number(v[1].replace(/[^\d]/g, ''));

          let dValue = d;
          if(scheduleSumHours && scheduleSumHours.hasOwnProperty('values') && scheduleSumHours.values[0] != null) dValue = d + n;
          if(dValue > 24) dValue = 24;

          work_days_res++;
          work_hours_res += dValue;
          night_hours_res += n;
        } else {
          if(!isNaN(Number(v))) {
            work_days_res++;
            work_hours_res += (Number(v) || 0);
          } else {
            let a = v.replace(/[\d]/g, '').trim();
            let h = v.replace(/[^\d]/g, '');

            if(a == 'К') {
              work_days_res++;
              work_hours_res += (Number(h) || 0);
            }
          }
        }
      } else {
        if(!isNaN(Number(v))) {
          work_days_res++;
          work_hours_res += (Number(v) || 0);
        } else {
          let a = v.replace(/[\d]/g, '').trim();
          let h = v.replace(/[^\d]/g, '');

          if(a == 'К') {
            work_days_res++;
            work_hours_res += (Number(h) || 0);
          }
        }
      }
    }
  }

  UTILS.setValue(timesheet, 'work_days-b' + tableBlockIndex, {type: 'numericinput', value: String(work_days_res), key: String(work_days_res)});
  UTILS.setValue(timesheet, 'work_hours-b' + tableBlockIndex, {type: 'numericinput', value: String(work_hours_res), key: String(work_hours_res)});
  UTILS.setValue(timesheet, 'night_hours-b' + tableBlockIndex, {type: 'numericinput', value: String(night_hours_res), key: String(night_hours_res)});


  if(schedule && tabelMonth) {
    let scheduleMonth = schedule[Number(tabelMonth)];
    UTILS.setValue(timesheet, 'day_norm-b' + tableBlockIndex, {type: 'numericinput', value: String(scheduleMonth.countDays), key: String(scheduleMonth.countDays)});
    UTILS.setValue(timesheet, 'hours_norm-b' + tableBlockIndex, {type: 'numericinput', value: String(scheduleMonth.sumHours), key: String(scheduleMonth.sumHours)});

    if(scheduleMonth.hasOwnProperty('dayOffMonth') && scheduleMonth.hasOwnProperty('dayOffHalfMonth') && scheduleMonth.hasOwnProperty('holidaysMonth') && scheduleMonth.hasOwnProperty('holidaysHalfMonth')) {

      if(row.hasOwnProperty('day_16')) {
        let scheduleHolidaysResult = scheduleMonth.dayOffMonth + scheduleMonth.holidaysMonth;
        UTILS.setValue(timesheet, 'weekends_and_holidays-b' + tableBlockIndex, {type: 'numericinput', value: String(scheduleHolidaysResult), key: String(scheduleHolidaysResult)});
      } else {
        let scheduleHolidaysResult = scheduleMonth.dayOffHalfMonth + scheduleMonth.holidaysHalfMonth;
        UTILS.setValue(timesheet, 'weekends_and_holidays-b' + tableBlockIndex, {type: 'numericinput', value: String(scheduleHolidaysResult), key: String(scheduleHolidaysResult)});
      }

    } else {
      UTILS.setValue(timesheet, 'weekends_and_holidays-b' + tableBlockIndex, {type: 'numericinput', value: String(weekends_and_holidays_res), key: String(weekends_and_holidays_res)});
    }

  } else {
    UTILS.setValue(timesheet, 'weekends_and_holidays-b' + tableBlockIndex, {type: 'numericinput', value: String(weekends_and_holidays_res), key: String(weekends_and_holidays_res)});
  }
}

function fillResultFieldAbbr(timesheet, item, row, tableBlockIndex){
  let abbr = item.abbr;
  let resultSum = 0;
  let resultAmount = 0;

  if(abbr) abbr = abbr.toUpperCase();

  for(let day = 1; day <= 31; day++) {
    let dayField = 'day_' + day;
    if(!row.hasOwnProperty(dayField)) continue;

    let value = row[dayField].value || null;
    if(!value) continue;
    value = value.toUpperCase();

    if(value.indexOf(abbr) == -1) continue;

    value = value.split(';');

    value.forEach(function(v) {
      let a = v.replace(/[\d]/g, '').trim();
      let h = v.replace(/[^\d]/g, '');

      if(a == abbr) resultAmount++;
      resultSum += (Number(h) || 0);
    });

    resultSum = Math.floor(resultSum * 100) / 100;
  }

  if(item.amount) {
    UTILS.setValue(timesheet, item.amount + '-b' + tableBlockIndex, {type: 'numericinput', value: String(resultAmount), key: String(resultAmount)});
  }

  if(item.sum) {
    UTILS.setValue(timesheet, item.sum + '-b' + tableBlockIndex, {type: 'numericinput', value: String(resultSum), key: String(resultSum)});
  }
}

var result = true;
var message = 'ok';

try {
  let currentFormData = API.getFormData(dataUUID);
  let tabelMonth = UTILS.getValue(currentFormData, 'month').key || null;
  let timesheet = UTILS.getValue(currentFormData, 'table');
  let parseTimesheet = UTILS.parseAsfTable(timesheet);
  let items = getOrderSettings();

  for(let i = 0; i < parseTimesheet.length; i++) {
    let row = parseTimesheet[i];
    let tableBlockIndex = i + 1;
    let userWorkTimeMode = getUserWorkTimeMode(row.user.key, row.position.key);

    //зашитые поля, кол-во дней, часов
    fillResultField(timesheet, row, tableBlockIndex, userWorkTimeMode, tabelMonth);

    //заполнение по аббревиатурам с настроек приказов
    for(let j = 0; j < items.length; j++) {
      fillResultFieldAbbr(timesheet, items[j], row, tableBlockIndex);
    }
  }

  API.mergeFormData({uuid: dataUUID, data: [timesheet]});

} catch (err) {
  message = err.message;
}
