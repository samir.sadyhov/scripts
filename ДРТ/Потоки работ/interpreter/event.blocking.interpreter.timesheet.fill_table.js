var result = true;
var message = 'ok';

function getTimesheetUsers(asfTable){
  if(!asfTable || !asfTable.hasOwnProperty('data')) return null;

  let users = [];

  for(let i = 1; i < UTILS.getTableBlockIndex(asfTable, 'user'); i++) {
    let user = UTILS.getValue(asfTable, 'user-b' + i);
    if(user && user.hasOwnProperty('key')) {
      let pos = UTILS.getValue(asfTable, 'position-b' + i);
      let dep = UTILS.getValue(asfTable, 'department-b' + i);

      users.push({
        tableBlockIndex: i,
        userID: user.key,
        fullName: user.value,
        positionID: pos.key || '',
        positionName: pos.value || '',
        departmentID: dep.key || '',
        departmentName: dep.value || ''
      });
    }
  }

  return users.length != 0 ? users : null;
}

function parseOrderSettings(data){
  if(data.recordsCount == 0) return [];
  let orders = [];
  data.result.forEach(function(x){
    let fieldValue = x.fieldValue;
    let fieldKey = x.fieldKey;

    let order = {
      registryCode: fieldValue.workflow_form_workTable_settings_type || '',
      formCode: fieldValue.workflow_form_workTable_settings_formCode || '',
      abbreviation: fieldValue.workflow_form_workTable_settings_abbreviation || '',
      user: fieldValue.workflow_form_workTable_settings_user || '',
      startDate: fieldValue.workflow_form_workTable_settings_ds || '',
      finishDate: fieldValue.workflow_form_workTable_settings_df || '',
      review: null
    };
    let check = fieldKey.workflow_form_workTable_settings_review_check || '';
    if(check && check == '1') {
      order.review = {
        registryCode: fieldValue.workflow_form_workTable_settings_type_review || '',
        formCode: fieldValue.workflow_form_workTable_settings_formCode_review || '',
        user: fieldValue.workflow_form_workTable_settings_user_review || '',
        startDate: fieldValue.workflow_form_workTable_settings_ds_review || '',
        finishDate: fieldValue.workflow_form_workTable_settings_df_review || ''
      }
    }

    orders.push(order);
  });
  return orders;
}

function getOrderSettings(){
  let fields = [
    "workflow_form_workTable_settings_type",
    "workflow_form_workTable_settings_formCode",
    "workflow_form_workTable_settings_abbreviation",
    "workflow_form_workTable_settings_user",
    "workflow_form_workTable_settings_ds",
    "workflow_form_workTable_settings_df",
    "workflow_form_workTable_settings_review_check",
    "workflow_form_workTable_settings_type_review",
    "workflow_form_workTable_settings_formCode_review",
    "workflow_form_workTable_settings_user_review",
    "workflow_form_workTable_settings_ds_review",
    "workflow_form_workTable_settings_df_review"
  ];
  let searchURL = 'rest/api/registry/data_ext?registryCode=workTable_settings';
  searchURL += '&registryRecordStatus=STATE_SUCCESSFUL';

  fields.forEach(function(field){searchURL += '&fields=' + field;});

  let searchResult = API.httpGetMethod(searchURL);

  return parseOrderSettings(searchResult);
}

function getPersonalRecord(userID){
  let searchResult = API.httpGetMethod('rest/api/personalrecord/forms/' + userID);
  let userCard = searchResult.filter(function(x){
    if(x.formCode == 'workflow_form_trudovye_otnosheniya') return x;
  })[0];

  return userCard ? API.getFormData(userCard['data-uuid']) : null;
}

function getUserWorkTimeMode(user){
  let personalCardData = getPersonalRecord(user.userID);
  if(!personalCardData) return null;

  let table = UTILS.parseAsfTable(UTILS.getValue(personalCardData, 'table-positions'));
  if(!table) return null;

  let block = table.filter(function(x){
    if(x.position.key == user.positionID) return x;
  })[0];
  if(!block) return null;

  if(!block.workmodeLink || !block.workmodeLink.hasOwnProperty('key')) return null;
  let workModelAsfData = API.getFormData(API.getAsfDataId(block.workmodeLink.key));

  if(!workModelAsfData) return null;

  return {
    rate: block.rate.value || '1',
    scheduleType: UTILS.getValue(workModelAsfData, 'workflow_form_workmode_schedule'),
    scheduleSumHours: UTILS.getValue(workModelAsfData, 'workflow_form_workmode_sum_hours'),
    schedule: JSON.parse(UTILS.getValue(workModelAsfData, 'schedule_json').value || {})
  }
}

function getTimesheetPeriod(inputYear, inputMonth, registryCode) {
  let start = inputYear.value + '-' + inputMonth.key + '-01 00:00:00';
  let stop = new Date(inputYear.value + '-' + inputMonth.key + '-01 23:59:59');

  if(registryCode == 'timesheet_halfmonth') {
    stop.setDate(15);
  } else {
    stop.setMonth(stop.getMonth() + 1);
    stop.setDate(stop.getDate() - 1);
  }

  return {
    start: start,
    stop: UTILS.formatDate(stop, true),
    endDay: stop.getDate()
  };
}

function parseUserOrders(userID, orders, orderSettings) {
  for(let registryCode in orders) {
    if(!orders[registryCode]['order'] || !orders[registryCode]['order'].length) continue;

    orders[registryCode]['order'].forEach(function(order){
      let settings = orderSettings.filter(function(x){ if(x.registryCode == registryCode) return x})[0];
      order.userID = userID;
      order.abbreviation = settings.abbreviation;

      let orderFormData = API.getFormData(order.dataUUID);
      order.startDate = UTILS.getValue(orderFormData, settings.startDate).key.split(' ')[0];
      order.finishDate = UTILS.getValue(orderFormData, settings.finishDate).key.split(' ')[0];
    });

    if(orders[registryCode]['review'] && orders[registryCode]['review'].length) {
      orders[registryCode]['review'].forEach(function(review){
        let settings = orderSettings.filter(function(x){ if(x.registryCode == registryCode) return x})[0];
        review.userID = userID;

        let orderFormData = API.getFormData(review.dataUUID);
        review.startDate = UTILS.getValue(orderFormData, settings.review.startDate).key.split(' ')[0];
        review.finishDate = UTILS.getValue(orderFormData, settings.review.finishDate).key.split(' ')[0];
      });
    }
  }

  return orders;
}

function searchUserOrders(userID, period, orderSettings) {
  let result = {};

  orderSettings.forEach(function(item){
    result[item.registryCode] = {};
    result[item.registryCode]['order'] = null;
    result[item.registryCode]['review'] = null;

    let urlParam = {
      searchInRegistry: "true",
      showDeleted: "false",
      registryRecordStatus: ["STATE_SUCCESSFUL"]
    };

    if(item.user.indexOf('.') != -1) {
      let userField = item.user.substr(item.user.indexOf('.') + 1);
      urlParam.query = "WHERE code = '" + item.formCode + "' AND " + userField + ".DATE = '" + userID + "' AND (" + item.startDate + ".DATE BETWEEN DATE('" + period.start + "') AND DATE('" + period.stop + "') OR " + item.finishDate + ".DATE BETWEEN DATE('" + period.start + "') AND DATE('" + period.stop + "'))";
      urlParam.parameters = [item.startDate + ".DATE",  item.finishDate + ".DATE"];
      urlParam.dynParams = [[userField + ".DATE"]];
    } else {
      urlParam.query = "WHERE code = '" + item.formCode + "' AND " + item.user + ".DATE = '" + userID + "' AND (" + item.startDate + ".DATE BETWEEN DATE('" + period.start + "') AND DATE('" + period.stop + "') OR " + item.finishDate + ".DATE BETWEEN DATE('" + period.start + "') AND DATE('" + period.stop + "'))";
      urlParam.parameters = [item.user + ".DATE", item.startDate + ".DATE",  item.finishDate + ".DATE"];
    }

    result[item.registryCode]['order'] = API.httpPostMethod("rest/api/asforms/search/advanced", urlParam, "application/json; charset=utf-8");

    if(item.review) {

      let urlParamReview = {
        searchInRegistry: "true",
        showDeleted: "false",
        registryRecordStatus: ["STATE_SUCCESSFUL"]
      };

      if(item.review.user.indexOf('.') != -1) {
        let userFieldReview = item.review.user.substr(item.review.user.indexOf('.') + 1);
        urlParamReview.query = "WHERE code = '" + item.review.formCode + "' AND " + userFieldReview + ".DATE = '" + userID + "' AND (" + item.review.startDate + ".DATE BETWEEN DATE('" + period.start + "') AND DATE('" + period.stop + "') OR " + item.review.finishDate + ".DATE BETWEEN DATE('" + period.start + "') AND DATE('" + period.stop + "'))";
        urlParamReview.parameters = [item.review.startDate + ".DATE",  item.review.finishDate + ".DATE"];
        urlParamReview.dynParams = [[userFieldReview + ".DATE"]];
      } else {
        urlParamReview.query = "WHERE code = '" + item.review.formCode + "' AND " + item.review.user + ".DATE = '" + userID + "' AND (" + item.review.startDate + ".DATE BETWEEN DATE('" + period.start + "') AND DATE('" + period.stop + "') OR " + item.review.finishDate + ".DATE BETWEEN DATE('" + period.start + "') AND DATE('" + period.stop + "'))";
        urlParamReview.parameters = [item.review.user + ".DATE", item.review.startDate + ".DATE",  item.review.finishDate + ".DATE"];
      }

      result[item.registryCode]['review'] = API.httpPostMethod("rest/api/asforms/search/advanced", urlParamReview, "application/json; charset=utf-8");
    }
  });

  return parseUserOrders(userID, result, orderSettings);
}

function getAbbreviation(userID, date, orders, userWorkTimeMode) {
  let abbr = [];
  let result = '';
  let checkDate = date.split(' ')[0];

  for(let registryCode in orders) {
    if(!orders[registryCode]['order'] || !orders[registryCode]['order'].length) continue;

    let abbrResult = null;

    orders[registryCode]['order'].forEach(function(order){
      if(checkDate >= order.startDate && checkDate <= order.finishDate) abbrResult = order.abbreviation;
    });

    if(orders[registryCode]['review'] && orders[registryCode]['review'].length) {
      orders[registryCode]['review'].forEach(function(review){
        if(checkDate >= review.startDate && checkDate <= review.finishDate) abbrResult = null;
      });
    }

    if(abbrResult) abbr.push(abbrResult);
  }

  if(!userWorkTimeMode) {
    result = abbr.length ? abbr.join(';') : '8';
  } else {
    let scheduleType = userWorkTimeMode.scheduleType;
    let scheduleSumHours = userWorkTimeMode.scheduleSumHours;
    let schedule = userWorkTimeMode.schedule;
    let rate = Number(userWorkTimeMode.rate);
    let dateSplit = date.split(/\D/);
    let month = Number(dateSplit[1]);
    let day = Number(dateSplit[2]);
    let dayAbbr = schedule[month]['days'][day];

    if(abbr.length) {
      if(dayAbbr == 'В') abbr.push(dayAbbr);

      dayAbbr = dayAbbr.split('/');
      if(dayAbbr.length == 1) dayAbbr.push('');
      let d = Number(dayAbbr[0].replace(/[^\d]/g, ''));
      let n = Number(dayAbbr[1].replace(/[^\d]/g, ''));

      let h = d;
      if(scheduleSumHours) h += n;
      if(h > 24) h = 24;

      result = abbr.map(function(a){
        if(a == 'В') return a;
        if(h == 0) return a;
        return a + ' ' + h;
      }).join(';');

    } else {
      if(isNaN(Number(dayAbbr))) {
        result = dayAbbr;
      } else {
        result = String(Math.floor((Number(dayAbbr) * rate) * 100) / 100);
      }
    }

  }

  return result;
}

function parseTimesheetOrderSettings(data) {
  if(data.recordsCount == 0) return null;
  //приём - 1, перевод - 2, увольнение - 3
  let orders = {};

  data.result.forEach(function(x){
    let fieldValue = x.fieldValue;
    let fieldKey = x.fieldKey;

    if(!orders.hasOwnProperty(fieldKey.workflow_form_timesheet_order_settings_type)) {
      orders[fieldKey.workflow_form_timesheet_order_settings_type] = [];
    }

    orders[fieldKey.workflow_form_timesheet_order_settings_type].push({
      registryCode: fieldValue.workflow_form_timesheet_order_settings_registryCode || '',
      user: fieldValue.workflow_form_timesheet_order_settings_user || '',
      position: fieldValue.workflow_form_timesheet_order_settings_position || '',
      department: fieldValue.workflow_form_timesheet_order_settings_department || '',
      date: fieldValue.workflow_form_timesheet_order_settings_date || '',
      to_position: fieldValue.workflow_form_timesheet_order_settings_to_position || '',
      to_department: fieldValue.workflow_form_timesheet_order_settings_to_department || ''
    });
  });

  return orders;
}

function getTimesheetOrderSettings(){
  let fields = [
    "workflow_form_timesheet_order_settings_type",
    "workflow_form_timesheet_order_settings_registryCode",
    "workflow_form_timesheet_order_settings_user",
    "workflow_form_timesheet_order_settings_position",
    "workflow_form_timesheet_order_settings_department",
    "workflow_form_timesheet_order_settings_date",
    "workflow_form_timesheet_order_settings_to_position",
    "workflow_form_timesheet_order_settings_to_department"
  ];
  let searchURL = 'rest/api/registry/data_ext?registryCode=workflow_registry_timesheet_order_settings';
  searchURL += '&registryRecordStatus=STATE_SUCCESSFUL';

  fields.forEach(function(field){searchURL += '&fields=' + field;});

  let searchResult = API.httpGetMethod(searchURL);

  return parseTimesheetOrderSettings(searchResult);
}

function searchAdmissionOrder(timesheetOrderSettings, userID, period) {
  let result = [];
  if(!timesheetOrderSettings.hasOwnProperty('1')) return null;
  let orders = timesheetOrderSettings['1'];

  let stop = new Date(period.stop);
  stop.setMonth(stop.getMonth() + 1);
  stop.setDate(stop.getDate() - 1);
  stop = UTILS.formatDate(stop, true);

  orders.forEach(function(order){
    let fields = [
      order.user,
      order.position,
      order.department,
      order.date
    ];

    let urlSearch = 'rest/api/registry/data_ext?registryCode=' + encodeURIComponent(order.registryCode);
    urlSearch += '&field='+order.user+'&condition=TEXT_EQUALS&key=' + encodeURIComponent(userID);
    urlSearch += '&field1='+order.date+'&condition1=MORE_OR_EQUALS&key1=' + encodeURIComponent(period.start);
    urlSearch += '&field2='+order.date+'&condition2=LESS_OR_EQUALS&key2=' + encodeURIComponent(stop);

    fields.forEach(function(field){urlSearch += '&fields=' + field;});

    urlSearch += '&term=and&groupTerm=and&countInPart=1&registryRecordStatus=STATE_SUCCESSFUL';

    let searchResult = API.httpGetMethod(urlSearch);

    if(searchResult && searchResult.recordsCount > 0) {
      searchResult.result.forEach(function(x){x.settings = order;});
      result = result.concat(searchResult.result);
    }
  });

  if(result.length) {
    return result[0];
  } else {
    return null;
  }
}

function searchTransferOrder(timesheetOrderSettings, userID, period) {
  let result = [];
  if(!timesheetOrderSettings.hasOwnProperty('2')) return null;
  let orders = timesheetOrderSettings['2'];

  let stop = new Date(period.stop);
  stop.setMonth(stop.getMonth() + 1);
  stop.setDate(stop.getDate() - 1);
  stop = UTILS.formatDate(stop, true);

  orders.forEach(function(order){
    let fields = [
      order.user,
      order.position,
      order.department,
      order.date,
      order.to_position,
      order.to_department
    ];

    let urlSearch = 'rest/api/registry/data_ext?registryCode=' + encodeURIComponent(order.registryCode);
    urlSearch += '&field='+order.user+'&condition=TEXT_EQUALS&key=' + encodeURIComponent(userID);
    urlSearch += '&field1='+order.date+'&condition1=MORE_OR_EQUALS&key1=' + encodeURIComponent(period.start);
    urlSearch += '&field2='+order.date+'&condition2=LESS_OR_EQUALS&key2=' + encodeURIComponent(stop);

    fields.forEach(function(field){urlSearch += '&fields=' + field;});

    urlSearch += '&countInPart=1&registryRecordStatus=STATE_SUCCESSFUL';

    let searchResult = API.httpGetMethod(urlSearch);

    if(searchResult && searchResult.recordsCount > 0) {
      searchResult.result.forEach(function(x){x.settings = order;});
      result = result.concat(searchResult.result);
    }
  });

  if(result.length) {
    return result[0];
  } else {
    return null;
  }
}

function searchDismissalOrder(timesheetOrderSettings, userID, period) {
  let result = [];
  if(!timesheetOrderSettings.hasOwnProperty('3')) return null;
  let orders = timesheetOrderSettings['3'];

  let stop = new Date(period.stop);
  stop.setMonth(stop.getMonth() + 1);
  stop.setDate(stop.getDate() - 1);
  stop = UTILS.formatDate(stop, true);

  orders.forEach(function(order){
    let fields = [
      order.user,
      order.position,
      order.department,
      order.date
    ];

    let urlSearch = 'rest/api/registry/data_ext?registryCode=' + encodeURIComponent(order.registryCode);
    urlSearch += '&field='+order.user+'&condition=TEXT_EQUALS&key=' + encodeURIComponent(userID);
    urlSearch += '&field1='+order.date+'&condition1=MORE_OR_EQUALS&key1=' + encodeURIComponent(period.start);
    urlSearch += '&field2='+order.date+'&condition2=LESS_OR_EQUALS&key2=' + encodeURIComponent(stop);

    fields.forEach(function(field){urlSearch += '&fields=' + field;});

    urlSearch += '&term=and&groupTerm=and&countInPart=1&registryRecordStatus=STATE_SUCCESSFUL';

    let searchResult = API.httpGetMethod(urlSearch);

    if(searchResult && searchResult.recordsCount > 0) {
      searchResult.result.forEach(function(x){x.settings = order;});
      result = result.concat(searchResult.result);
    }
  });

  if(result.length) {
    return result[0];
  } else {
    return null;
  }
}

try {
  let currentFormData = API.getFormData(dataUUID);
  let timesheet = UTILS.getValue(currentFormData, "table");
  let inputMonth = UTILS.getValue(currentFormData, "month");
  let inputYear = UTILS.getValue(currentFormData, "year");
  let users = getTimesheetUsers(timesheet);

  if(!users) throw new Error('Не найдены пользователи в табеле');

  let documentInfo = API.getDocumentInfo(documentID);
  let registryInfo = API.httpGetMethod('rest/api/registry/info?registryID=' + documentInfo.registryID);
  let orderSettings = getOrderSettings();
  let timesheetOrderSettings = getTimesheetOrderSettings();
  let period = getTimesheetPeriod(inputYear, inputMonth, registryInfo.code);

  users.forEach(function(user){
    let userWorkTimeMode = getUserWorkTimeMode(user);
    let userAdmissionOrder = searchAdmissionOrder(timesheetOrderSettings, user.userID, period); //приём
    let userTransferOrder = searchTransferOrder(timesheetOrderSettings, user.userID, period); //перевод
    let userDismissalOrder = searchDismissalOrder(timesheetOrderSettings, user.userID, period); //увольнение
    let userOrders = searchUserOrders(user.userID, period, orderSettings);

    for(let i = 1; i <= period.endDay; i++) {
      let dayCmp = 'day_' + i + '-b' + user.tableBlockIndex;
      let date = inputYear.value + '-' + inputMonth.key + '-' + ('0' + i).slice(-2) + ' 00:00:00';
      let abbr = '';

      function magic(){
        //проверка приказа о переводе
        if(userTransferOrder && userTransferOrder.hasOwnProperty('fieldKey')) {
          let orderData = userTransferOrder.fieldKey;
          let transferProp = userTransferOrder.settings;
          let checkDate = new Date(date);
          let orderDate = new Date(orderData[transferProp.date]);

          if(user.positionID == orderData[transferProp.position] && checkDate >= orderDate) {
            abbr = '';
          } else if (user.positionID == orderData[transferProp.to_position] && checkDate < orderDate) {
            abbr = '';
          } else {
            abbr = getAbbreviation(user.userID, date, userOrders, userWorkTimeMode);
          }
        } else {
          abbr = getAbbreviation(user.userID, date, userOrders, userWorkTimeMode);
        }
      }

      //если есть приказ об увольнении
      if(userDismissalOrder) {
        let dismissProp = userDismissalOrder.settings;
        //проверяем с какой даты увольнение
        let checkDismissalDate = new Date(date);
        let dismissalDate = new Date(userDismissalOrder.fieldKey[dismissProp.date]);
        //проверяемая дата >= даты приказа
        if(checkDismissalDate >= dismissalDate) {
          abbr = '';
        } else {
          //если есть приказ о приёме
          if(userAdmissionOrder) {
            let amissionProp = userAdmissionOrder.settings;
            //проверяем с какой даты приём
            let checkAdmissionDate = new Date(date);
            let admissionDate = new Date(userAdmissionOrder.fieldKey[amissionProp.date]);
            //проверяемая дата >= даты приказа
            if(checkAdmissionDate >= admissionDate) {
              magic();
            } else {
              abbr = '';
            }
          } else {
            magic();
          }
        }
      } else {
        //если есть приказ о приёме
        if(userAdmissionOrder) {
          let amissionProp = userAdmissionOrder.settings;
          //проверяем с какой даты приём
          let checkAdmissionDate = new Date(date);
          let admissionDate = new Date(userAdmissionOrder.fieldKey[amissionProp.date]);
          //проверяемая дата >= даты приказа
          if(checkAdmissionDate >= admissionDate) {
            magic();
          } else {
            abbr = '';
          }
        } else {
          magic();
        }
      }

      UTILS.setValue(timesheet, dayCmp, {
        type: 'textbox',
        value: abbr
      });
    }
  });

  API.mergeFormData({uuid: dataUUID, data: [timesheet]});

} catch (err) {
  message = err.message;
  result = false;
}
