//Сопоставление из дин.таблицы в дин.таблицу
function matchingTables(id, fromAsfData, toFormData, toFormDescription) {
  let tableFromData = UTILS.getValue(fromAsfData, id.from.split('.')[0]);
  let tableData = UTILS.getValue(toFormData, id.to.split('.')[0]);
  let fromCmpID = id.from.split('.')[1];
  let currentCmpID = id.to.split('.')[1];

  if(!tableData || !tableData.hasOwnProperty('data')) tableData = {id: id.to.split('.')[0], type: 'appendable_table', data: []}

  if(tableFromData && tableFromData.hasOwnProperty('data')) {
    tableFromData.data.forEach(function(item){
      if(item.id.substring(0, item.id.indexOf('-b')) == fromCmpID) {
        let field = UTILS.createField({
          id: currentCmpID + "-b" + UTILS.getTableBlockIndex(tableData, currentCmpID),
          type: UTILS.getCmpType(toFormDescription, currentCmpID)
        });
        for(let key in item) {
          if(key === 'id' || key === 'type') continue;
          field[key] = item[key];
        }

        tableData.data.push(field);
      }
    });
  }
}

// сопоставление из компонента в дин.таблицу
function matchingCmpToTable(id, fromData, toFormData, toFormDescription) {
  let tableID = id.to.split('.')[0];
  let cmpID = id.to.split('.')[1];
  let tableData = UTILS.getValue(toFormData, tableID);

  if(!tableData || !tableData.hasOwnProperty('data')) tableData = {id: tableID, type: 'appendable_table', data: []}

  let field = UTILS.createField({
    id: cmpID + "-b" + UTILS.getTableBlockIndex(tableData, cmpID),
    type: UTILS.getCmpType(toFormDescription, cmpID)
  });

  for(let key in fromData) {
    if(key === 'id' || key === 'type') continue;
    field[key] = fromData[key];
  }

  //костыль
  if(cmpID == 'number_of_days') {
    field.type = 'numericinput';
    field.key = field.value;
  }

  tableData.data.push(field);
}

var result = true;
var message = 'ok';

try {
  let personalCardCode = 'workflow_form_otpuska_sotrudnika';
  let currentFormData = API.getFormData(dataUUID);

  let user = UTILS.getValue(currentFormData, "workflow_form_leave_order_rus_user1");
  if(!user || !user.hasOwnProperty('key')) throw new Error('Не заполнено поле пользователь');

  let personalFormCard = API.httpGetMethod('rest/api/personalrecord/forms/' + user.key);

  personalFormCard = personalFormCard.filter(function(x){
    if(x.formCode == personalCardCode) return x;
  })[0];

  if(!personalFormCard) throw new Error('Не найдена карточка пользователя');

  personalFormCard = API.getFormData(personalFormCard['data-uuid']);
  let personalFormDescription = API.getFormDescription(personalFormCard.form);

  let matching = [];
  matching.push({from: 'workflow_form_leave_order_rus_date_ds1', to: 'labor_leave.date_start'});
  matching.push({from: 'workflow_form_leave_order_rus_date_df1', to: 'labor_leave.date_finish'});
  matching.push({from: 'workflow_form_leave_order_rus_days', to: 'labor_leave.number_of_days'});
  matching.push({from: 'workflow_form_leave_order_rus_ds', to: 'labor_leave.ds_period'});
  matching.push({from: 'workflow_form_leave_order_rus_df', to: 'labor_leave.df_period'});

  matching.forEach(function(id) {
    if(id.from.indexOf('.') != -1) {
      matchingTables(id, currentFormData, personalFormCard, personalFormDescription);
    } else {
      let fromData = UTILS.getValue(currentFormData, id.from);
      if(fromData) {
        if(id.to.indexOf('.') == -1) {
           UTILS.setValue(personalFormCard, id.to, fromData);
        } else {
          matchingCmpToTable(id, fromData, personalFormCard, personalFormDescription);
        }
      }
    }
  });

  API.mergeFormData(personalFormCard);

} catch (err) {
  log.error(err.message);
  message = err.message;
}
