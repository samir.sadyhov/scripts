const workCommentsContainer = $(`#${comp.code}`);
const addCommentButton = workCommentsContainer.find(`#add-comment-button`);
const addCommentInput = workCommentsContainer.find(`.add-comment-input`);

const commentListWork = workCommentsContainer.find(`.uk-comment-list[type-comment="work"]`);
const commentListDoc = workCommentsContainer.find(`.uk-comment-list[type-comment="doc"]`);
const commentListUser = workCommentsContainer.find(`.uk-comment-list[type-comment="user"]`);

const getModalDialog = async (body, handler) => {
  const dialogTitle = i18n.tr("Комментарий");
  const exitButtonText = i18n.tr("Закрыть");
  const saveButtonText = i18n.tr("Сохранить");

  const dialog = $('<div class="uk-flex-top" uk-modal>');
  const md = $('<div class="uk-modal-dialog uk-margin-auto-vertical">');
  const modalBody = $('<div class="uk-modal-body" uk-overflow-auto>');
  const footer = $('<div class="uk-modal-footer uk-text-right">');


  modalBody.append(body);
  footer.append(`<button class="uk-button uk-button-default uk-modal-close" type="button">${exitButtonText}</button>`);
  md.append(`<div class="uk-modal-header"><h3>${dialogTitle}</h3></div>`).append(modalBody).append(footer);
  dialog.append(md);

  const actionButton = $('<button class="uk-button uk-button-primary uk-margin-left" type="button">');
  actionButton.html(saveButtonText).on('click', handler);
  footer.append(actionButton);

  return dialog;
}

const getComments = async (workID, type) => {
  return new Promise(async resolve => {
    rest.synergyGet(`api/workflow/work/${workID}/comments?type=${type}&count=100&locale=${AS.OPTIONS.locale}`, resolve, err => {
      console.log(`ERROR [ getComments ]: ${JSON.stringify(err)}`);
      resolve(null);
    });
  });
}

const removeComments = async commentID => {
  return new Promise(async resolve => {
    rest.synergyGet(`api/workflow/work/comments/remove?commentID=${commentID}`, resolve, err => {
      console.log(`ERROR [ removeComments ]: ${JSON.stringify(err)}`);
      resolve(null);
    });
  });
}

const saveComment = async param => {
  const {workID, comment, type = 'WORK', commentID} = param;
  const bodyObject = {workID, comment: encodeURIComponent(comment), type};
  if(commentID) bodyObject.commentID = commentID;

  return new Promise(async resolve => {
    rest.synergyPost(`api/workflow/work/${workID}/comments/save?locale=${AS.OPTIONS.locale}`,
      bodyObject,
      "application/x-www-form-urlencoded; charset=utf-8",
      resolve,
      err => {
        console.log(`ERROR [ saveComment ]: ${JSON.stringify(err)}`);
        resolve(null);
      }
    );
  });
}

const getCommentArticle = (data, workID) => {
  const {commentID, comment, author: {name: authorName}, created_label, is_editable, is_deletable} = data;
  const article = $('<article>', {class: "uk-comment uk-comment-primary uk-visible-toggle uk-padding-small"});
  const header = $('<header>', {class: "uk-comment-header uk-position-relative uk-margin-remove"});
  const buttonsBlock = $('<div>', {class: "uk-position-top-right uk-hidden-hover"});
  const commentBody = $('<div>', {class: "uk-comment-body uk-margin-small-top"});

  if(is_editable == 'true') {
    const editCommentButton = $(`<a href="#" uk-icon="icon: pencil"></a>`);
    buttonsBlock.append(editCommentButton);

    editCommentButton.on('click', async e => {
      e.preventDefault();
      e.target.blur();

      const commentInput = $(`<textarea class="uk-textarea add-comment-input" rows="5"></textarea>`);
      commentInput.val(comment);

      const dialog = await getModalDialog(commentInput, async () => {
        try {
          if(commentInput.val() != "") {
            Cons.showLoader();
            commentInput.removeClass('uk-form-danger');
            const type = data.type != '0' ? 'PERSONAL' : 'WORK';
            const resultSave = await saveComment({
              workID, type, commentID,
              comment: commentInput.val()
            });
            renderComments(workID);
            Cons.hideLoader();
            UIkit.modal(dialog).hide();
          } else {
            commentInput.addClass('uk-form-danger');
          }
        } catch (err) {
          Cons.hideLoader();
          UIkit.modal(dialog).hide();
          showMessage(err.message, 'error');
        }
      });

      UIkit.modal(dialog).show();
      dialog.on('hidden', () => {
        dialog.remove();
      });

    });
  }

  if(is_deletable == 'true') {
    const delCommentButton = $(`<a href="#" uk-icon="icon: trash"></a>`);
    buttonsBlock.append(delCommentButton);

    delCommentButton.on('click', async e => {
      e.preventDefault();
      e.target.blur();
      const resultRemove = await removeComments(commentID);
      if(data.type == '0') {
        renderComments(workID);
      } else {
        article.remove();
      }
    });
  }

  header
  .append(`<h4 class="uk-comment-title uk-margin-remove">${authorName || ''}</h4>`)
  .append(`<span class="uk-comment-meta uk-margin-remove">${created_label}</span>`)
  .append(buttonsBlock);

  commentBody.html(comment.replaceAll('\n', '<br>'));
  article.append(header).append(commentBody);

  return article;
}

const workCommentsTranslate = () => {
  workCommentsContainer.find(`.tab-comments[type-comment="work"] a`).text(i18n.tr("Работа"));
  workCommentsContainer.find(`.tab-comments[type-comment="doc"] a`).text(i18n.tr("Документ"));
  workCommentsContainer.find(`.tab-comments[type-comment="user"] a`).text(i18n.tr("Личные"));
  addCommentInput.attr('placeholder', i18n.tr("Введите комментарий и нажмите Enter..."));
}

const initCommentListiners = workID => {
  addCommentInput.off().on('keyup', async e => {
    if (e.keyCode === 13) {
      e.preventDefault();
      e.target.blur();
      const comment = addCommentInput.val().replaceAll('\n', '').trim();
      if(comment != '') {
        const typeAttr = $('.tab-comments.uk-active').attr('type-comment');
        const type = typeAttr == 'user' ? 'PERSONAL' : 'WORK';
        const resultSave = await saveComment({workID, type, comment});
        addCommentInput.val('');
        renderComments(workID);
      } else {
        addCommentInput.val('');
      }
    }
  });

  addCommentButton.off().on('click', async e => {
    e.preventDefault();
    e.target.blur();

    const commentInput = $(`<textarea class="uk-textarea add-comment-input" rows="5"></textarea>`);

    const dialog = await getModalDialog(commentInput, async () => {
      try {
        if(commentInput.val() != "") {
          Cons.showLoader();
          commentInput.removeClass('uk-form-danger');
          const typeAttr = $('.tab-comments.uk-active').attr('type-comment');
          const type = typeAttr == 'user' ? 'PERSONAL' : 'WORK';
          const resultSave = await saveComment({
            workID, type,
            comment: commentInput.val()
          });
          renderComments(workID);
          Cons.hideLoader();
          UIkit.modal(dialog).hide();
        } else {
          commentInput.addClass('uk-form-danger');
        }
      } catch (err) {
        Cons.hideLoader();
        UIkit.modal(dialog).hide();
        showMessage(err.message, 'error');
      }
    });

    UIkit.modal(dialog).show();
    dialog.on('hidden', () => {
      dialog.remove();
    });

  });
}

const renderComments = async workID => {
  const workComments = await getComments(workID, 'WORK');
  const docComments = await getComments(workID, 'DOCUMENT');
  const userComments = await getComments(workID, 'PERSONAL');

  commentListWork.empty();
  commentListDoc.empty();
  commentListUser.empty();

  if(workComments && workComments.length > 0) {
    workComments.forEach(data => commentListWork.append(getCommentArticle(data, workID)));
  }

  if(docComments && docComments.length > 0) {
    docComments.forEach(data => commentListDoc.append(getCommentArticle(data, workID)));
  }

  if(userComments && userComments.length > 0) {
    userComments.forEach(data => commentListUser.append(getCommentArticle(data, workID)));
  }
  workCommentsTranslate();
}

workCommentsTranslate();

addListener('work_comments_init', comp.code, event => {
  const {workID} = event;
  renderComments(workID);
  initCommentListiners(workID);
});

addListener('work_comments_update', comp.code, event => {
  const {workID} = event;
  if(workID) renderComments(workID);
});

addListener('work_comments_translate', comp.code, () => {
  workCommentsTranslate();
});
