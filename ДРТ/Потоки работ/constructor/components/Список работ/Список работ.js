const compContainer = $(`#${comp.code}`);
const workContainer = compContainer.find(`.workflow-work-container`);
const tableContainer = workContainer.find(`.workflow-table-container`);
const paginatorContainer = workContainer.find(`.workflow-paginator-container`);

const getProgressBar = value => `<div class="work-progress-container"><div class="work-progress" style="width: ${value}%;">${value}%</div></div>`;

const getPosition = e => {
  let x = 0;
  let y = 0;

  if (e.pageX || e.pageY) {
    x = e.pageX;
    y = e.pageY;
  } else if (e.clientX || e.clientY) {
    x = e.clientX + document.body.scrollLeft + document.documentElement.scrollLeft;
    y = e.clientY + document.body.scrollTop + document.documentElement.scrollTop;
  }

  return {x, y};
}

const Paginator = {
  countInPart: 15,
  rows: 0,
  currentPage: 1,
  pages: 0,

  update: function(){
    this.pages = Math.ceil(this.rows / this.countInPart),
    this.label.text(this.currentPage + ' / ' + this.pages);

    if(this.pages == 0) {
      this.bPrevious.attr('disabled', 'disabled');
      this.bNext.attr('disabled', 'disabled');
    } else {
      if(this.currentPage == 1) {
        this.bPrevious.attr('disabled', 'disabled');
        if (this.currentPage == this.pages) {
          this.bNext.attr('disabled', 'disabled');
        } else {
          this.bNext.removeAttr('disabled');
        }
      } else {
        this.bPrevious.removeAttr('disabled');
        if (this.currentPage == this.pages) {
          this.bNext.attr('disabled', 'disabled');
        } else {
          this.bNext.removeAttr('disabled');
        }
      }
    }
  },

  addListeners: function(){
    this.bNext.click(() => {
      this.currentPage++;
      this.update();
      WorkTable.createBody();
    });

    this.bPrevious.click(() => {
      this.currentPage--;
      this.update();
      WorkTable.createBody();
    });

    this.label.on('dblclick', e => {
      e.preventDefault();
      this.input.show();
      this.label.hide();

      this.input.on('blur', () => {
        this.label.show();
        this.input.hide();
      });

      this.input.val("" + this.currentPage);

      this.input.on('keypress', e => {
        if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) return false;
      });

      this.input.on('keydown', e => {
        if(e.which === 13) {
          let inputVal = +this.input.val();
          if(inputVal && inputVal != this.currentPage && inputVal <= this.pages && inputVal >= 1){
            this.currentPage = inputVal;
            this.update();
            WorkTable.createBody();
          }
          this.label.show();
          this.input.hide();
          this.input.off();
        }
      });
      this.input.focus();
    });
  },

  render: function(){
    this.container = $('<div class="workflow-pt-container">');
    this.paginator = $('<div class="workflow-pt-paginator">');
    this.pContent = $('<div class="workflow-pt-paginator-content">');
    this.bPrevious = $(`<button class="workflow-pt-previous" disabled="disabled" title="${i18n.tr('Назад')}">`);
    this.bNext = $(`<button class="workflow-pt-next" disabled="disabled" title="${i18n.tr('Вперед')}">`);
    this.label = $('<label>');
    this.input = $('<input type="text">');

    paginatorContainer.empty().append(this.container);

    this.pContent.append(this.label).append(this.input);
    this.paginator.append(this.bPrevious).append(this.pContent).append(this.bNext);
    this.container.append(this.paginator);
  },

  reset: function(countInPart){
    this.countInPart = countInPart;
    this.rows = countInPart;
    this.currentPage = 1;
    this.pages = 0;
  },

  init: function(countInPart = 15){
    const {rowsPerPage = countInPart} = localStorage;
    this.render();
    this.reset(rowsPerPage);
    this.addListeners();
  }
};

const WorkTable = {
  filterType: 'OWN_WORKS',
  filterId: null,
  objectId: null,

  search: null,
  periodType: null,

  isAscSort: true,
  sortColumn: null,

  table: null,
  tHead: null,
  tBody: null,

  selectedItems: [],

  priorities: null,

  getUrl: function(){
    let url = `api/workflow/works/list_ext?userID=${AS.OPTIONS.currentUser.userid}&filterType=${this.filterType}`;

    const startRecord = Paginator.currentPage * Paginator.countInPart - Paginator.countInPart;
    url += `&startRecord=${startRecord}&recordsCount=${Paginator.countInPart}`;

    if(this.filterId) url += `&filterID=${this.filterId}`;
    if(this.objectId) url += `&objectID=${this.objectId}`;
    if(this.search) url += `&search=${this.search}`;
    if(this.periodType) url += `&hasExtSearchParams=true&periodType=${this.periodType}`;
    if(this.sortColumn) url += `&sortColumn=${this.sortColumn}&isAscSort=${this.isAscSort}`;

    url += `&locale=${AS.OPTIONS.locale}`;
    return url;
  },

  sortable: function(th) {
    if(th.attr('columnid') == 'checkbox' || th.hasClass("uk-checkbox")) return;
    this.tHead.find('th').not(th).removeClass('sortable sortable__down sortable__up');
    if (th.hasClass("sortable__down") || th.hasClass("sortable__up")) {
      th.toggleClass("sortable__down sortable__up");
    } else {
      th.addClass("sortable sortable__down");
    }
    this.sortColumn = th.attr('columnid');
    this.isAscSort = th.hasClass("sortable__down") ? true : false;
    this.createBody();
  },

  createHeader: function() {
    this.tHead.empty();
    this.isAscSort = true;
    this.sortColumn = null;

    const tr = $('<tr>');
    const th = $('<th columnid="checkbox" style="width: 40px;"></th>');
    // const checkbox = $('<input/>')
    // .addClass('uk-checkbox')
    // .attr('type', 'checkbox')
    // .on('change', e => {
    //   this.tBody.find('td > [type="checkbox"]').each((k, x) => {
    //     x.checked = !!e.target.checked;
    //   });
    //   this.tBody.find('td > [type="checkbox"]').each((k, x) => {
    //     $(x).trigger('change');
    //   });
    // });
    // th.append(checkbox);
    tr.append(th, '<th columnid="work_info" style="width: 40px;"></th>');

    tr.append(`<th columnid="name" class="uk-table-expand">${i18n.tr('Название')}</th>`);
    if(this.filterType == 'OWN_WORKS') {
      tr.append(`<th columnid="author" class="uk-width-small">${i18n.tr('Поставил')}</th>`);
    } else {
      tr.append(`<th columnid="responsible" class="uk-width-small">${i18n.tr('Ответственный')}</th>`);
    }
    tr.append(`<th columnid="start_date" class="uk-width-small">${i18n.tr('Начало')}</th>`);
    tr.append(`<th columnid="finish_date" class="uk-width-small">${i18n.tr('Завершение')}</th>`);
    tr.append(`<th columnid="left" class="uk-width-small">${i18n.tr('Осталось')}</th>`);
    tr.append(`<th columnid="percent" class="uk-width-small">${i18n.tr('Прогресс')}</th>`);

    tr.on('click', e => this.sortable($(e.target)));

    this.tHead.append(tr);
  },

  getContextMenuItem: function(name, icon, handler, disabled = false){
    const li = $(`<li>`);
    const a = $('<a>');
    const span = $(`<span class="uk-margin-small-right" uk-icon="icon: ${icon}"></span>`);

    if(disabled) li.addClass('uk-disabled');

    a.append(span, i18n.tr(name));
    li.append(a);

    li.on('click', e => {
      e.preventDefault();
      e.target.blur();
      if(handler) handler();
    });

    return li;
  },

  openWorkInfo: function(work){
    const createRow = (label, value) => $(`<tr><td>${label}</td><td>${value}</td></tr>`);

    const dialog = $('<div class="uk-flex-top" uk-modal>');
    const md = $('<div class="uk-modal-dialog uk-margin-auto-vertical">');
    const modalBody = $('<div>', {class: 'uk-modal-body uk-overflow-auto', style: 'padding: 15px;'});
    const container = $('<div>');
    const table = $('<table>', {class: 'uk-table work-info'});
    const body = $('<tbody>');

    table.append(body);
    container.append(table);
    modalBody.append(container);

    dialog.append(md);

    md.append(
      '<button class="uk-modal-close-default" type="button" uk-close></button>',
      `<div class="uk-modal-header"><h3 class="uk-modal-title">${i18n.tr('Информация')}</h3></div>`,
      modalBody,
      `<div class="uk-modal-footer uk-text-right"><button class="uk-button uk-button-default uk-modal-close" type="button">${i18n.tr('Закрыть')}</button></div>`
    );

    const priority = this.priorities.find(x => x.id == work.priority);
    const remained = Number(work.remained) < 0 ? `${i18n.tr('Просрочено')} ${work.remained_label.replaceAll('-', '')}` : `${i18n.tr('Осталось')} ${work.remained_label}`;

    body.append(
      createRow(i18n.tr('Наименование:'), work.name),
      createRow(i18n.tr('Автор:'), work.author.name),
      `<tr><td>${i18n.tr('Приоритет:')}</td><td style="display: flex; align-items: center;"><span style="width: 20px; height: 20px; background: ${priority.color}; margin-right: 10px;"></span><span>${priority.name}</span></td></tr>`,
      createRow(i18n.tr('Начало') + ':', work.start_date),
      createRow(i18n.tr('Завершение') + ':', work.finish_date),
      createRow(i18n.tr('Сроки:'), remained)
    );

    UIkit.modal(dialog).show();
    dialog.on('hidden', () => dialog.remove());
  },

  deleteWork: async function(work){
    UIkit.modal.confirm(i18n.tr('Вы действительно хотите удалить данную работу?'),
      {labels: {ok: i18n.tr('Да'), cancel: i18n.tr('Отмена')}})
    .then(async () => {
      Cons.showLoader();
      try {
        const resultDelete = await appAPI.deleteWork(work.actionID);
        if(!resultDelete) throw new Error(i18n.tr('Произошла ошибка при удалении работы'));
        if(resultDelete.errorCode != '0') throw new Error(resultDelete.errorMessage);

        Paginator.init();
        this.createBody();

        showMessage(resultDelete.errorMessage, 'success');
      } catch (e) {
        Cons.hideLoader();
        showMessage(e.message, 'error');
      }
    }, () => null);
  },

  stopRoute: async function(work){
    UIkit.modal.confirm(i18n.tr('Вы действительно хотите прервать данный маршрут?'),
      {labels: {ok: i18n.tr('Да'), cancel: i18n.tr('Отмена')}})
    .then(async () => {
      Cons.showLoader();
      try {
        const documentID = await appAPI.getWorkDocument(work.actionID);
        if(!documentID) throw new Error(i18n.tr('Произошла ошибка прерывания маршрута'));

        const result = await appAPI.stopRoute(documentID);
        if(!result) throw new Error(i18n.tr('Произошла ошибка прерывания маршрута'));
        if(result.errorCode != '0') throw new Error(result.errorMessage);

        Paginator.init();
        this.createBody();

        showMessage(result.errorMessage, 'success');
      } catch (e) {
        Cons.hideLoader();
        showMessage(e.message, 'error');
      }
    }, () => null);
  },

  startRoute: async function(work, param){
    work.documentID = await appAPI.getWorkDocument(work.actionID);
    try {
      const resultSendWork = await _WORK.startRoute({...param, actionID: work.actionID, documentID: work.documentID});
      if(resultSendWork.errorCode != 0) throw new Error(resultSendWork.errorMessage);
      showMessage(i18n.tr(resultSendWork.errorMessage), 'success');
    } catch (err) {
      showMessage(err.message, 'error');
    }
  },

  getContextNavItems: function(workAction){
    const contextNavItems = [];

    contextNavItems.push({
      name: 'Переслать',
      icon: 'forward',
      handler: function(me, work){
        _WORK.forward(work);
      }
    });

    if(workAction.find(x => x.action == "TRANSMIT")) {
      contextNavItems.push({
        name: 'Передать',
        icon: 'chevron-double-right',
        handler: function(me, work){
          _WORK.transfer(work);
        }
      });
    }

    contextNavItems.push({
      name: 'Перепоручить',
      icon: 'users',
      handler: function(me, work){
        me.startRoute(work, {
          action: "REASSIGN",
          label: i18n.tr("Перепоручить")
        });
      }
    });

    contextNavItems.push('divider');

    contextNavItems.push({
      name: 'На согласование/рассмотрение',
      icon: 'commenting',
      handler: function(me, work){
        me.startRoute(work, {
          label: i18n.tr("На согласование/рассмотрение"),
          action: "SEND",
          operation: "AGREEMENT"
        });
      },
      prop: 'can_send_agreement'
    });

    contextNavItems.push({
      name: 'На утверждение',
      icon: 'check',
      handler: function(me, work){
        me.startRoute(work, {
          label: i18n.tr("На утверждение"),
          action: "SEND",
          operation: "APPROVAL"
        });
      },
      prop: 'can_send_approval'
    });

    contextNavItems.push({
      name: 'На ознакомление',
      icon: 'file-text',
      handler: function(me, work){
        me.startRoute(work, {
          label: i18n.tr("На ознакомление"),
          action: "SEND",
          operation: "ACQUAINTANCE"
        });
      },
      prop: 'can_send_acquaintance'
    });

    contextNavItems.push({
      name: 'Как служебную записку',
      icon: 'file-text',
      handler: function(me, work){
        me.startRoute(work, {
          label: i18n.tr("Как служебную записку"),
          action: "SEND",
          operation: "ACQUAINTANCE"
        });
      }
    });

    contextNavItems.push('divider');

    contextNavItems.push({
      name: 'Информация',
      icon: 'info',
      handler: function(me, work){
        me.openWorkInfo(work);
      }
    });

    // contextNavItems.push(
    //   {
    //     name: 'Изменить',
    //     icon: 'pencil',
    //     handler: async function(me, work){
    //       try {
    //         const resultEditWork = await _WORK.edit(work);
    //       } catch (err) {
    //         showMessage(err.message, 'error');
    //       }
    //     },
    //     prop: 'can_edit'
    //   }
    // );

    contextNavItems.push('divider');

    contextNavItems.push({
      name: 'Прервать маршрут',
      icon: 'ban',
      handler: function(me, work){
        me.stopRoute(work);
      }
    });

    contextNavItems.push({
      name: 'Удалить',
      icon: 'trash',
      handler: function(me, work){
        me.deleteWork(work);
      },
      prop: 'can_delete'
    });

    return contextNavItems;
  },

  positionMenu: function(contextMenu, e) {
    const clickCoords = getPosition(e);
    const clickCoordsX = clickCoords.x;
    const clickCoordsY = clickCoords.y;
    const menuWidth = contextMenu.width() + 4;
    const menuHeight = 380;
    const windowWidth = window.innerWidth;
    const windowHeight = window.innerHeight;

    let left = 0;
    let top = 0;

    if ( (windowWidth - clickCoordsX) < menuWidth ) {
      left = windowWidth - menuWidth;
    } else {
      left = clickCoordsX;
    }

    if ( (windowHeight - clickCoordsY) < menuHeight ) {
      top = windowHeight - menuHeight;
    } else {
      top = clickCoordsY;
    }

    contextMenu.css({
      "left": left + "px",
      "top": top + "px"
    });
  },

  contextMenu: async function(el, work) {
    const workAction = await appAPI.getWorkActions(work.actionID);

    el.on('contextmenu', event => {
      $('.workflow-context-menu').remove();

      const contextMenu = $('<div>', {class: 'workflow-context-menu'});
      const nav = $('<ul class="uk-nav-default uk-nav-parent-icon" uk-nav>');

      contextMenu.append(nav);

      const contextNavItems = this.getContextNavItems(workAction);
      contextNavItems.forEach(item => {
        if(item == 'divider') {
          nav.append('<li class="uk-nav-divider"></li>');
        } else if (item.name == 'Прервать маршрут') {
          if(work.author.id == AS.OPTIONS.currentUser.userid) {
            const {name, icon, handler, prop = false} = item;
            nav.append(this.getContextMenuItem(name, icon, () => handler(this, work), prop && work[prop] == "false"));
          }
        } else {
          const {name, icon, handler, prop = false} = item;
          nav.append(this.getContextMenuItem(name, icon, () => handler(this, work), prop && work[prop] == "false"));
        }
      });

      $('body').append(contextMenu);
      this.positionMenu(contextMenu, event);

      this.tBody.find('tr').removeAttr('selected');
      el.attr('selected', true);

      contextMenu.show('fast');
      return false;
    });
  },

  getIconResolution: function(){
    return `<svg xmlns="http://www.w3.org/2000/svg" enable-background="new 0 0 24 24" height="18px" viewBox="0 0 24 24" width="20px" fill="#ac5e0f"><g><rect fill="none" height="24" width="24"/></g><g><g><path d="M4,16v6h16v-6c0-1.1-0.9-2-2-2H6C4.9,14,4,14.9,4,16z M18,18H6v-2h12V18z M12,2C9.24,2,7,4.24,7,7l5,7l5-7 C17,4.24,14.76,2,12,2z M12,11L9,7c0-1.66,1.34-3,3-3s3,1.34,3,3L12,11z"/></g></g></svg>`;
  },

  getIconFile: function(){
    return `<svg xmlns="http://www.w3.org/2000/svg" height="18px" viewBox="0 0 24 24" width="20px" fill="#797979"><path d="M0 0h24v24H0V0z" fill="none"/><path d="M16.5 6v11.5c0 2.21-1.79 4-4 4s-4-1.79-4-4V5c0-1.38 1.12-2.5 2.5-2.5s2.5 1.12 2.5 2.5v10.5c0 .55-.45 1-1 1s-1-.45-1-1V6H10v9.5c0 1.38 1.12 2.5 2.5 2.5s2.5-1.12 2.5-2.5V5c0-2.21-1.79-4-4-4S7 2.79 7 5v12.5c0 3.04 2.46 5.5 5.5 5.5s5.5-2.46 5.5-5.5V6h-1.5z"/></svg>`;
  },

  createRow: function(item) {
    const {
      actionID,
      name,
      is_new,
      is_expired,
      is_soon_expired,
      user,
      author,
      start_date,
      finish_date,
      remained_label,
      percent,
      has_attachments,
      isResolution
    } = item;

    const tr = $('<tr>');
    const checkbox = $('<input/>').addClass('uk-checkbox').attr('type', 'checkbox');
    const tmpTd = $('<td>');
    const workInfoTD = $('<td>', {style: "white-space: normal !important; padding: 0; text-align: center; vertical-align: middle;"});

    if(has_attachments && has_attachments == "true") workInfoTD.append(this.getIconFile());
    if(isResolution && isResolution == "true") workInfoTD.append(this.getIconResolution());

    if(this.selectedItems.indexOf(actionID) !== -1) checkbox.prop('checked', true);
    tmpTd.append(`<span class="mobile-table-header">Выбор</span>`).append(checkbox);
    tr.append(tmpTd, workInfoTD);

    if(this.priorities) {
      const priority = this.priorities.find(x => x.id == item.priority);
      if(priority) tmpTd.css({
        'background': priority.color
      });
    }

    checkbox.on('change', e => {
      if(e.target.checked) {
        if(this.selectedItems.indexOf(actionID) === -1) {
          this.selectedItems.push(actionID);
        }
      } else {
        const index = this.selectedItems.indexOf(actionID);
        if(index !== -1) this.selectedItems.splice(index, 1);
      }
      // if(this.selectedItems.length > 0) {
      //   $('.action-menu-item').removeClass('uk-disabled');
      //   if($('.action-menu-item.item-delete').length) {
      //     if(!this.isDelete) $('.action-menu-item.item-delete').addClass('uk-disabled');
      //   }
      // } else {
      //   $('.action-menu-item').addClass('uk-disabled');
      // }
    });

    if(is_new && is_new == "true") tr.addClass('new_work');
    if(is_expired && is_expired == "true") {
      tr.addClass('expired_work');
    } else if (is_soon_expired && is_soon_expired == "true") {
      tr.addClass('soon_expired_work');
    }

    if(isResolution && isResolution == "true") {
      tr.append(`<td style="text-decoration: underline;"><span uk-tooltip="title: ${name}; delay: 500; cls: wf-tooltip uk-active">${name}</span></td>`);
    } else {
      tr.append(`<td><span uk-tooltip="title: ${name}; delay: 500; cls: wf-tooltip uk-active">${name}</span></td>`);
    }

    if(this.filterType == 'OWN_WORKS') {
      tr.append(`<td uk-tooltip="title: ${author.name || ''}; cls: wf-tooltip uk-active">${author.name || ''}</td>`);
    } else {
      tr.append(`<td uk-tooltip="title: ${user.name || ''}; cls: wf-tooltip uk-active">${user.name || ''}</td>`);
    }

    const dateFormat = "${dd} ${month} ${HH}:${MM}";
    const dateTitleFormat = "${dd}.${mm}.${yyyy} ${HH}:${MM}";
    const startDateFormated = AS.FORMS.DateUtils.formatDate(AS.FORMS.DateUtils.parseDate(start_date), dateFormat);
    const startDateTitleFormated = AS.FORMS.DateUtils.formatDate(AS.FORMS.DateUtils.parseDate(start_date), dateTitleFormat);
    const finishDateFormated = AS.FORMS.DateUtils.formatDate(AS.FORMS.DateUtils.parseDate(finish_date), dateFormat);
    const finishDateTitleFormated = AS.FORMS.DateUtils.formatDate(AS.FORMS.DateUtils.parseDate(finish_date), dateTitleFormat);

    tr.append(`<td uk-tooltip="title: ${startDateTitleFormated}; cls: wf-tooltip uk-active">${startDateFormated}</td>`);
    tr.append(`<td uk-tooltip="title: ${finishDateTitleFormated}; cls: wf-tooltip uk-active">${finishDateFormated}</td>`);

    tr.append(`<td uk-tooltip="title: ${remained_label}; cls: wf-tooltip uk-active">${remained_label}</td>`);
    tr.append(`<td>${getProgressBar(percent)}</td>`);

    let timeoutId;

    tr.on('click', e => {
      if($(e.target).is("input")) return;
      timeoutId = setTimeout(() => {
        if(!timeoutId) return;
        this.tBody.find('tr').removeAttr('selected');
        tr.attr('selected', true);
        fire({type: 'worklist_item_click', ...item}, comp.code);
      }, 200);
    });

    tr.on('dblclick', e => {
      if($(e.target).is("input")) return;
      clearTimeout(timeoutId);
      timeoutId = null;
      fire({type: 'worklist_item_dbl_click', ...item}, comp.code);
    });

    this.contextMenu(tr, item);

    return tr;
  },

  createBody: function(rowsCount) {
    Cons.showLoader();
    rest.synergyGet(this.getUrl(), res => {
      this.tBody.empty();
      res.data.forEach(item => this.tBody.append(this.createRow(item)));
      Paginator.rows = res.totalCount;
      Paginator.update();
      Cons.hideLoader();
    });
  },

  render: function() {
    compContainer.find(`.jsx-parser`).css({
      'width': '100%',
      'height': '100%'
    });
    this.table = $('<table class="workflow-table-list uk-table uk-table-small uk-table-responsive">');
    this.tHead = $('<thead>');
    this.tBody = $('<tbody>');
    this.table.append(this.tHead).append(this.tBody);
    tableContainer.empty().append(this.table);
  },

  reset: function(){
    this.search = null;
    this.periodType = null;
    this.isAscSort = true;
    this.sortColumn = null;
    this.selectedItems = [];
  },

  init: function() {
    const {systemSettings} = Cons.getAppStore();
    if(systemSettings) this.priorities = systemSettings.current_priorities;

    this.render();
    this.createHeader();
    Paginator.init();
    this.createBody();
  }

};

WorkTable.init();

addListener('worklist_filter_arbitrary_change', comp.code, filterData => {
  const {filterType, filterId, objectId} = filterData;

  WorkTable.filterType = filterType;
  WorkTable.filterId = filterId;
  WorkTable.objectId = objectId;
  WorkTable.reset();
  WorkTable.createHeader();
  WorkTable.createBody();
  Paginator.init();
});

addListener('worklist_translate', comp.code, () => {
  WorkTable.init();
});

addListener('worklist_search', comp.code, e => {
  const {value = null} = e;
  WorkTable.search = value;
  WorkTable.createBody();
  Paginator.init();
});

addListener('worklist_row_count_on_page', comp.code, e => {
  const {rowsPerPage = null} = e;
  WorkTable.search = null;
  Paginator.init(rowsPerPage);
  WorkTable.createBody();
});

addListener('worklist_update', comp.code, e => {
  const {countInPart} = Paginator;
  Paginator.init(countInPart);
  WorkTable.createBody();
});

$(document).off()
.on('contextmenu', () => $('.workflow-context-menu').remove())
.on('click', () => $('.workflow-context-menu').remove());
