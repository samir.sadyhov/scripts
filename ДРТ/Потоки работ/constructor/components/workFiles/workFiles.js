const workFilesContainer = $(`#${comp.code}`);
const addFileButton = workFilesContainer.find(`#workflow_add_file_button`);
const workContainer = workFilesContainer.find(`.data[path="ase:workContainer"]`);
const attachmentContainer = workFilesContainer.find(`.data[path="ase:attachmentContainer"]`);

let attachmentFilesCount = 0;
let workFilesCount = 0;

const workFilesTranslate = () => {
  workFilesContainer.find(`.tab[path="ase:attachmentContainer"] a`).text(i18n.tr("Приложения") + ` (${attachmentFilesCount})`);
  workFilesContainer.find(`.tab[path="ase:workContainer"] a`).text(i18n.tr("Прочие") + ` (${workFilesCount})`);
}

const getAttachments = async workID => {
  return new Promise(async resolve => {
    rest.synergyGet(`api/workflow/work/${workID}/attachments?locale=${AS.OPTIONS.locale}`, resolve, err => {
      console.log(`ERROR ${comp.code} [ getAttachments ]: ${JSON.stringify(err)}`);
      resolve(null);
    });
  });
}

//  Скачать все "Приложения"/"Прочие"
//  ../Synergy/download-archive?objects={id},{id}&archive=zip

//  Скачать все файлы
//  ../Synergy/download-archive?inputFiles={id},{id}&workFiles={id},{id}&archive=zip

//resource_downloader?identifier={id}
//resource_downloader?identifier={id}&format=pdf

const getMenuCoordinates = (menu, pageX, pageY) => {
  const menuW = menu.width();
  const menuH = menu.height();
  const documentW = $(document).width();
  const documentH = $(document).height();
  const coordinates = {
    left: pageX,
    top: pageY
  };

  if((menuW + pageX) > documentW) coordinates.left = (documentW - menuW - 50);
  if((menuH + pageY) > documentH) coordinates.top = (documentH - menuH - 50);

  return coordinates;
}

const getContextItemMenu = (name, hoverColor, icon, handler) => {
  return $(`<li><a style="--ic: ${hoverColor}" href="#"><span class="uk-margin-small-right" uk-icon="icon: ${icon}"></span>${i18n.tr(name)}</a></li>`)
  .on('click', e => {
    e.preventDefault();
    e.target.blur();
    handler();
  });
}

const getContextMenu = (li, item) => {
  const menu = $('<div>', {class: 'workflow-context-menu', style: `min-width: 200px;`});
  const ul = $('<ul class="uk-nav-default uk-nav-parent-icon" uk-nav>');

  const open = getContextItemMenu('Открыть', '#999', 'push', () => {
    workFilesContainer.trigger({type: 'work_files_context_open', eventParam: item});
  });

  const download = getContextItemMenu('Скачать', '#999', 'download', () => {
    Cons.showLoader();
    const {name, uuid} = item;
    UTILS.fileDownload(name, uuid);
    Cons.hideLoader();
  });

  const del = getContextItemMenu('Удалить', '#ef2d35', 'trash', () => {
    const {name, uuid} = item;
    UIkit.modal.confirm(i18n.tr('Вы действительно хотите удалить файл ${fileName}?').replace('${fileName}', `"${name}"`)).then(async () => {
      const deleteResult = await appAPI.deleteFile(uuid);
      if(deleteResult && deleteResult.errorCode == 0) {
        li.remove();
      } else {
        showMessage(deleteResult.errorMessage, 'error');
      }
    }, () => null);
  });

  ul.append(open, download, '<li class="uk-nav-divider"></li>', del);
  menu.append(ul);

  return menu;
}

const addFileItemListeners = (li, item) => {
  const {name, uuid} = item;
  let timeoutId;
  li.on('click', e => {
    timeoutId = setTimeout(() => {
      if(!timeoutId) return;
      $('.worflow-file-item').removeClass('uk-active');
      li.addClass('uk-active');
      workFilesContainer.trigger({type: 'work_files_click', eventParam: item});
    }, 200);
  });

  li.on('dblclick', e => {
    clearTimeout(timeoutId);
    timeoutId = null;
    e.preventDefault();
    $('.worflow-file-item').removeClass('uk-active');
    li.addClass('uk-active');
    workFilesContainer.trigger({type: 'work_files_dbl_click', eventParam: item});
  });

  li.on('contextmenu', e => {
    $('.workflow-context-menu').remove();
    const contextmenu = getContextMenu(li, item);
    $('body').append(contextmenu);

    const {left, top} = getMenuCoordinates(contextmenu, event.pageX, event.pageY);
    contextmenu.css({
      'left': `${left}px`,
      'top': `${top}px`
    });

    contextmenu.show('fast');
    return false;
  });
}

const getFileList = (attachment, type) => {
  const ul = $('<ul class="uk-nav-default uk-nav-divider" uk-nav style="overflow: hidden;">');
  attachment = attachment.sort((a,b) => new Date(a.created) - new Date(b.created));

  attachment.forEach((item, i) => {
    const {name, uuid} = item;
    const li = $('<li>', {class: 'worflow-file-item'});
    li.append(`<a href="#" title="${name}" style="text-overflow: ellipsis; overflow: hidden;">${name}</a>`);
    if(type == 'ase:attachmentContainer' && i == 0) li.addClass('uk-active');
    addFileItemListeners(li, item);
    ul.append(li);
  });

  return ul;
}

const renderAttachments = async workID => {
  attachmentContainer.empty();
  workContainer.empty();
  const res = await getAttachments(workID);

  if(res) {
    attachmentContainer.append(getFileList(res[0].attachments, 'ase:attachmentContainer'));
    workContainer.append(getFileList(res[1].work_files));
    attachmentFilesCount = res[0].attachments.length;
    workFilesCount = res[1].work_files.length;
  } else {
    attachmentFilesCount = 0;
    workFilesCount = 0;
  }

  workFilesTranslate();
}

const renderListFiles = (files, listFiles) => {
  listFiles.empty();
  for(let i = 0; i < files.length; i++) {
    const file = files[i];
    const li = $(`<li class="uk-position-relative" index-file="${i}" title="${file.name}">${i+1}. ${file.name}</li>`);
    const delButton = $(`<a href="#" uk-icon="icon: trash" class="uk-position-center-right uk-hidden-hover"></a>`);
    li.append(delButton);
    listFiles.append(li);

    delButton.on('click', e => {
      files.splice(i, 1);
      li.remove();
      renderListFiles(files, listFiles);
    });
  }
}

addFileButton.on('click', async e => {
  e.preventDefault();
  e.target.blur();
  const path = $('.tab.uk-active').attr('path');
  const {workID} = comp;
  if(!workID) return;

  const content = $('<div>');
  const listFiles = $('<ul>', {class: 'uk-list uk-list-divider'});
  const {container, inputFile} = Components.createSelectFileMultiple(i18n.tr('Выбрать файлы'));
  let files = [];
  let uploadErrors = 0;
  let finishUpload = false;

  content.append(container, listFiles);

  inputFile.on('change', e => {
    files = [...inputFile[0].files];
    uploadErrors = 0;
    renderListFiles(files, listFiles);
  });

  const dialog = await UTILS.getModalDialog(i18n.tr('Добавить документ'), content, i18n.tr('Загрузить'), async () => {
    Cons.showLoader();
    try {
      if(!files.length) throw new Error('Необходимо прикрепить файл');

      dialog.find('.uk-button-primary').hide();
      container.hide();

      for(let i = 0; i < files.length; i++) {
        const file = files[i];
        const statusLable = listFiles.find(`[index-file="${i}"]`);
        const fileReader = new FileReader();

        statusLable.find('a').remove();

        if(file.size > 20971520) {
          statusLable.append(`<span style="color: orange;"> - ${i18n.tr('Размер файла не должен превышать 20 МБ')}</span>`);
          uploadErrors++;
        } else {
          Cons.showLoader();

          const filePath = await appAPI.startUpload();
          if(!filePath) {
            statusLable.append(`<span style="color: red;"> - ${i18n.tr('Ошибка создания временного файла')}</span>`);
            uploadErrors++;
          } else {

            fileReader.onload = async function (e) {
              Cons.showLoader();
              const b64encoded = btoa(new Uint8Array(e.target.result).reduce((data, byte) => data + String.fromCharCode(byte), ''));
              const data = new FormData();
              data.append('body', b64encoded);

              const uploadResult = await appAPI.uploadPart(filePath.file, data);
              if(!uploadResult) {
                statusLable.append(`<span style="color: red;"> - ${i18n.tr('Ошибка загрузки файла')}</span>`);
                uploadErrors++;
              } else {
                if(uploadResult.errorCode != '0') {
                  statusLable.append(`<span style="color: red;"> - ${i18n.tr(uploadResult.errorMessage)}</span>`);
                  uploadErrors++;
                } else {
                  Cons.showLoader();
                  const {result, message} = await appAPI.addFileToWork(workID, file.name, filePath.file, path) || {};
                  if(result && result == '0') {
                    statusLable.append(`<span style="color: green;"> - ${i18n.tr('готово')}</span>`);
                  } else {
                    if(message.indexOf('This node already exists') != -1) {
                      statusLable.append(`<span style="color: red;"> - ${i18n.tr('Файл ${fileName} уже существует').replace('${fileName}', file.name)}</span>`);
                    } else {
                      statusLable.append(`<span style="color: red;"> - ${i18n.tr(message)}</span>`);
                    }
                    uploadErrors++;
                  }
                  Cons.hideLoader();
                }
              }
              Cons.hideLoader();
              dialog.find('button.uk-modal-close').text('OK');
            }

            fileReader.readAsArrayBuffer(file);
          }
        }
      }

      Cons.hideLoader();
      if(uploadErrors > 0) showMessage(i18n.tr('При загрузке файлов возникли ошибки'), 'error');
      finishUpload = true;

    } catch (err) {
      Cons.hideLoader();
      showMessage(i18n.tr(err.message), 'error');
    }
  });
  UIkit.modal(dialog).show();
  dialog.on('hidden', () => {
    dialog.remove();
    if(finishUpload) renderAttachments(workID);
  });
});

workFilesTranslate();

addListener('work_files_init', comp.code, event => {
  const {workID} = event;
  comp.workID = workID;
  renderAttachments(workID);
});

addListener('work_files_translate', comp.code, () => workFilesTranslate());

$(document).on('contextmenu', () => $('.workflow-context-menu').remove());
$(document).on('click', () => $('.workflow-context-menu').remove());
