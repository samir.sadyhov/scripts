const compContainer = $(`#${comp.code}`);
const wfMenu = compContainer.find(`.wf-menu`);
const buttonCreate = compContainer.find(`.button_create`);
const buttonMenu = compContainer.find(`.button_menu`);

const getCreateMenuItems = async () => {
  return new Promise(resolve => {
    rest.synergyGet(`api/person/get_create_menu_items`, resolve, err => {
      console.log(`ERROR [ getCreateMenuItems ]: ${JSON.stringify(err)}`);
      resolve(null);
    });
  });
}

const setCreateMenuItems = async data => {
  return new Promise(async resolve => {
    try {
      const {login, password} = Cons.creds;
      const auth = "Basic " + btoa(unescape(encodeURIComponent(`${login}:${password}`)));
      const url = `../Synergy/rest/api/person/set_create_menu_items`;
      const response = await fetch(url, {
        method: 'POST',
        headers: {"Authorization": auth, "Content-Type": "application/json; charset=UTF-8"},
        body: JSON.stringify(data)
      });
      if(!response.ok) throw new Error(`HTTP: ${response.status}, message: ${response.statusText} [ ${response.text()} ]`);
      resolve(response.json());
    } catch (err) {
      console.log(`ERROR [ setCreateMenuItems ]: ${JSON.stringify(err)}`);
      resolve(null);
    }
  });
}

const createWork = () => {
  try {
    const resultCreateWork = _WORK.create();
  } catch (err) {
    showMessage(err.message, 'error');
  }
}

const createRegistryRow = async registryID => {
  Cons.showLoader();
  try {
    const regInfo = await appAPI.getRegistryInfoByID(registryID);

    if(!regInfo) throw new Error(i18n.tr('При создании записи произошла ошибка. Не удалось плучить информацию по реестру.'));
    if(regInfo.rr_create != "Y") throw new Error(i18n.tr('У вас нет прав на создание записи'));

    const doc = await appAPI.createDoc(regInfo.code);
    if(!doc) throw new Error(i18n.tr('Произошла ошибка при создании данного типа документа'));


    Cons.hideLoader();

    const eventParam = {
      type: 'document',
      documentID: doc.documentID,
      editable: true,
    };

    $('#root-panel').trigger({type: 'custom_open_document', eventParam});
  } catch (err) {
    showMessage(err.message, 'error');
    Cons.hideLoader();
  }
}

const checkEmpty = items => {
  let result = true;
  const s = arr => {
    arr.forEach(x => {
      if(x.is_bookmark == 1) result = false;
      if(x.is_group == 1) s(x.items);
    });
  }
  s(items);
  return result;
}

const renderItems = (items, container) => {
  for(let i = 0; i < items.length; i++) {
    const item = items[i];
    const title = item.translations.find(x => x.locale == AS.OPTIONS.locale);
    const titleValue = title ? title?.value : item.title;

    if(item.is_group == 1 && !checkEmpty(item.items)) {
      const li = $('<li>').attr('menu_itemID', item.menu_itemID);
      const span = $(`<span>${titleValue}</span>`);
      const ul = $('<ul>');
      li.append(span, ul);
      container.append(li);
      renderItems(item.items, ul);
    } else if(item.is_group == 0 && item.is_bookmark == 1) {
      if(!$(`li [menu_itemid="${item.menu_itemID}"]`).length) {
        const li = $('<li>').attr('menu_itemID', item.menu_itemID);
        const a = $(`<a href="#">${titleValue}</a>`);
        li.append(a);
        container.append(li);

        li.on('click', e => {
          closeAllSubMenu();
          wfMenu.fadeOut(300);
          createRegistryRow(item.registryID);
        });
      }
    }

  }
}

const getFavoritesModal = body => {
  const dialog = $('<div class="uk-flex-top" uk-modal="bg-close: false; esc-close: false;">');
  const md = $('<div class="uk-modal-dialog uk-margin-auto-vertical">');
  md.append(`<button class="uk-modal-close-default modal-close-custom" type="button" uk-close></button>`,
  `<div class="uk-modal-header"><h3>${i18n.tr("Избранное")}</h3></div>`, body);
  dialog.append(md);
  return dialog;
}

const getSearchComponent = () => {
  const container = $('<div>', {class: "uk-inline", style: "width: 100%"});
  const icon = $(`<a class="uk-form-icon uk-form-icon-flip" href="#" uk-icon="icon: search"></a>`);
  const input = $(`<input class="uk-input" type="text">`);

  container.append(icon, input)

  return {container, icon, input};
}

const favoritesInit = items => {
  let selected = null;
  let itemsIsChange = false;

  const favoritesContainer = $('<div>', {class: "wf_favorites_modal_container"});
  const search = getSearchComponent();
  const itemsContainer = $('<div>', {class: "wf_favorites_items_container"});
  const buttonsContainer = $('<div>', {class: "wf_favorites_buttons"});

  const buttonApply = $('<button>', {
    class: "uk-button uk-button-primary",
    style: "border-radius: 3px;",
    disabled: true
  });

  const modal = getFavoritesModal(favoritesContainer);

  const renderFavoritesItem = listItems => {
    for(let i = 0; i < listItems.length; i++) {
      const item = listItems[i];
      const title = item.translations.find(x => x.locale == AS.OPTIONS.locale);

      if(item.is_group == 1) {
        const d = $('<div>', {class: "favorites_group"});
        d.text(title ? title?.value : item.title);
        d.attr('menu_itemID', item.menu_itemID);
        itemsContainer.append(d);
        renderFavoritesItem(item.items);
      } else {
        if(itemsContainer.find(`[menu_itemid="${item.menu_itemID}"]`).length) continue;

        const d = $('<div>', {class: "favorites_item_container"});
        const itemIcon = $('<div>', {class: "favorites_icon"});
        const itemTitle = $('<div>', {class: "favorites_title"});

        if(item.is_bookmark == 1) {
          itemIcon.attr('is_bookmark', true);
        } else {
          itemIcon.attr('is_bookmark', false);
        }

        d.attr('menu_itemID', item.menu_itemID);

        itemTitle.text(title ? title?.value : item.title);
        itemIcon.append(`<span uk-icon="icon: star"></span>`);
        d.append(itemIcon, itemTitle);
        itemsContainer.append(d);

        d.on('click', e => {
          buttonApply.attr('disabled', false);
          $('.favorites_item_container').removeClass('selected');
          d.addClass('selected');
          selected = item;
        });

        itemIcon.on('click', async e => {
          const is_bookmark = item.is_bookmark == 0 ? 1 : 0;

          const setResult = await setCreateMenuItems([{
            itemID: item.menu_itemID, is_bookmark
          }]);

          if(setResult && setResult.errorCode == 0) {
            itemsIsChange = true;
            item.is_bookmark = is_bookmark;

            if(is_bookmark == 1) {
              itemIcon.attr('is_bookmark', true);
            } else {
              itemIcon.attr('is_bookmark', false);
            }
          } else {
            showMessage(i18n.tr('Произошла ошибка изменения списка избранного'), 'error');
          }
        });
      }
    }
  }

  const searchItem = () => {
    $.each(itemsContainer.find(".favorites_item_container"), function() {
      if($(this).text().toLowerCase().indexOf(search.input.val().toLowerCase()) === -1) $(this).hide();
      else $(this).show();
    });
  }

  buttonApply.text(i18n.tr('Выбрать'));
  buttonsContainer.append(buttonApply);
  favoritesContainer.append(search.container, itemsContainer, buttonsContainer);

  renderFavoritesItem(items);

  search.input.on('keyup', e => {
    searchItem();
  });

  search.icon.on('click', e => {
    searchItem();
  });

  buttonApply.on('click', e => {
    UIkit.modal(modal).hide();
    if(selected) createRegistryRow(selected.registryID);
  });

  UIkit.modal(modal).show();
  modal.on('hidden', () => {
    if(itemsIsChange) createMenu();
    modal.remove();
  });
}

const createMenu = async () => {
  buttonCreate.text(i18n.tr('Создать'));
  wfMenu.empty();
  const items = await getCreateMenuItems();

  renderItems(items, wfMenu);

  const liWork = $('<li>');
  const liMore = $('<li>');

  liWork.append(`<a href="#">${i18n.tr('Работа')}</a>`);
  liWork.on('click', e => {
    closeAllSubMenu();
    wfMenu.fadeOut(300);
    createWork();
  });

  liMore.append(`<a href="#">${i18n.tr('Ещё...')}</a>`);
  liMore.on('click', e => {
    closeAllSubMenu();
    wfMenu.fadeOut(300);
    favoritesInit(items);
  });

  wfMenu.append('<li><hr></li>', liWork, '<li><hr></li>', liMore);
}

const closeAllSubMenu = (current = null) => {
  const parents = [];
  const subMenu = document.querySelectorAll('.wf-menu ul');

  if (current) {
    let currentParent = current.parentNode;
    while (currentParent) {
      if (currentParent.classList.contains('wf-menu')) break;
      if (currentParent.nodeName === 'UL') parents.push(currentParent);
      currentParent = currentParent.parentNode;
    }
  }

  Array.from(subMenu).forEach(item => {
    if (item != current && !parents.includes(item)) {
      item.classList.remove('sub-menu-active');
      if (item.previousElementSibling.nodeName === 'SPAN') {
        item.previousElementSibling.classList.remove('sub-menu-active-span');
      }
    }
  });
}


wfMenu.on('click', event => {
  if (event.target.nodeName !== 'SPAN') return;
  closeAllSubMenu(event.target.nextElementSibling);
  event.target.classList.add('sub-menu-active-span');
  event.target.nextElementSibling.classList.toggle('sub-menu-active');
}).on('mouseleave', () => {
  closeAllSubMenu();
  wfMenu.fadeOut(300);
});

buttonMenu.on('click', e => {
  wfMenu.fadeIn(100);
});

buttonCreate.off().on('click', e => {
  createWork();
});

createMenu();

addListener('wf_menu_translate', comp.code, () => {
  createMenu();
});
