const compComtainer = $(`#${comp.code}`);
const dropdownButton = $(`#${comp.code} .uk-button`);
const dropdownList = $(`#${comp.code} .uk-dropdown-nav`);

const fillDropdownList = data => {
  const {buttonName, buttonClass, items} = data;

  if(!items.length) {
    compComtainer.hide();
    return;
  }

  if(buttonClass) dropdownButton.addClass(buttonClass);

  dropdownButton.text(buttonName);
  dropdownList.empty();

  items.forEach(item => {
    const li = $('<li>');
    const a = $(`<a href="#">${item.label}</a>`);

    li.append(a);
    dropdownList.append(li);

    a.on('click', e => {
      e.preventDefault();
      e.target.blur();
      fire({type: 'dropdown_btn_click', ...item}, comp.code);
    });
  });

}

addListener('dropdown_btn_init', comp.code, fillDropdownList);
