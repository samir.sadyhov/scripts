const compContainer = $(`#${comp.code}`);

compContainer.find('.workflow_user_name').text(UTILS.getCurrentUserFullName());
compContainer.find('.workflow_profile_img').attr('src', `../Synergy/load?userid=${AS.OPTIONS.currentUser.userid}`);

const worflowSettigsInterface = compContainer.find('#worflowSettigsInterface');
const worflowSettigsSecurity = compContainer.find('#worflowSettigsSecurity');
const worflowSettigsDelegation = compContainer.find('#worflowSettigsDelegation');
const worflowSettigsQR = compContainer.find('#worflowSettigsQR');
const worflowManual = compContainer.find('#worflowManual');
const worflowButtonLogout = compContainer.find('#worflowButtonLogout');

const worflowSettigsButton = compContainer.find('#worflowSettigsButton');

const applyLocales = () => {
  worflowSettigsInterface.find('.nav_item_text').text(i18n.tr('Настройки интерфейса'));
  worflowSettigsSecurity.find('.nav_item_text').text(i18n.tr('Параметры авторизации'));
  worflowSettigsDelegation.find('.nav_item_text').text(i18n.tr('Делегирование'));
  worflowSettigsQR.find('.nav_item_text').text(i18n.tr('QR-код для авторизации'));
  worflowManual.find('.nav_item_text').text(i18n.tr('Руководство пользователя'));
  worflowButtonLogout.find('.nav_item_text').text(i18n.tr('Выход'));
  worflowSettigsButton.find('.nav_item_text').text(i18n.tr('Настройки'));
}

applyLocales();

const {appInIframe} = Cons.getAppStore();
if(appInIframe) $(`#${comp.code} .synergy-app`).hide();

const changeCredentials = async param => {
  return new Promise(async (resolve, reject) => {
    rest.synergyPost("api/filecabinet/user/changeCredentials", param, "application/x-www-form-urlencoded; charset=UTF-8", resolve, reject);
  });
}

const getSettingsModalContent = () => {
  const {systemLocales} = Cons.getAppStore();
  const settingsContainer = $('<div>');
  const data = systemLocales.map(x => ({label: x.languageName, value: x.localeID}));
  const currentLocale = data.find(x => x.value == AS.OPTIONS.locale);

  const {
    container: localeContainer,
    select: localeSelect
  } = Components.createSelectComponent(i18n.tr('Локаль'), data);
  localeSelect.val(AS.OPTIONS.locale);

  const {
    container: countWorkContainer,
    select: countWorkSelect
  } = Components.createSelectComponent(i18n.tr('Кол-во работ на странице'), [
    {label: 5, value: 5},
    {label: 10, value: 10},
    {label: 15, value: 15},
    {label: 20, value: 20},
    {label: 25, value: 25},
    {label: 30, value: 30}
  ]);
  if(localStorage.rowsPerPage) {
    countWorkSelect.val(localStorage.rowsPerPage);
  }

  settingsContainer.append(countWorkContainer);
  if(!appInIframe) settingsContainer.append('<hr>', localeContainer);

  return {settingsContainer, localeSelect, countWorkSelect};
}

const getSecurityModalContent = () => {
  const securityContainer = $('<div>', {style: 'display: flex;'});
  const container = $('<div>', {style: 'width: 100%; max-width: 250px;'});
  securityContainer.append(container)

  const {container: loginContainer, input: loginInput} = Components.createInputText(i18n.tr('Логин'));
  const {container: passContainer, input: passInput} = Components.createInputPassword(i18n.tr('Пароль'));
  const {container: confPassContainer, input: confPassInput} = Components.createInputPassword(i18n.tr('Подтверждение пароля'));

  loginInput.val(AS.OPTIONS.login);

  container.append(loginContainer, passContainer, confPassContainer);
  securityContainer.append(container);

  return {securityContainer, loginInput, passInput, confPassInput};
}

const Delegation = {
  actions: [
    {"actionId": "111", "actionName": "Просмотр"},
    {"actionId": "161", "actionName": "Выбор исполнителя"},
    {"actionId": "106", "actionName": "Изменение процента выполнения"},
    {"actionId": "108", "actionName": "Изменение статуса"},
    {"actionId": "104", "actionName": "Редактирование"},
    {"actionId": "156", "actionName": "Согласование/Рассмотрение"},
    {"actionId": "101", "actionName": "Создание"},
    {"actionId": "107", "actionName": "Удаление"},
    {"actionId": "157", "actionName": "Утверждение"}
  ],

  openDelegateList: async function(userID, list = []) {
    const selected = ["111"];
    const container = $('<div>');

    container.css({
      'display': 'flex',
      'flex-direction': 'column'
    });

    for(let i = 0; i < this.actions.length; i++) {
      const {actionId, actionName} = this.actions[i];
      const label = $(`<label>`);
      const checkbox = $('<input/>', {class: 'uk-checkbox', type: 'checkbox'});

      label.append(checkbox, ` ${i18n.tr(actionName)}`);
      container.append(label);

      if(actionId == "111") {
        checkbox.prop('checked', true);
        checkbox.attr('disabled', true);
      }

      if(list.includes(actionId)) {
        checkbox.prop('checked', true);
        if(!selected.includes(actionId)) selected.push(actionId);
      }

      checkbox.on('change', e => {
        if(e.target.checked) {
          if(!selected.includes(actionId)) selected.push(actionId);
        } else {
          const index = selected.indexOf(actionId);
          if(index !== -1) selected.splice(index, 1);
        }
      });
    }

    const dialog = await UTILS.getModalDialog(i18n.tr('Действия'), container, i18n.tr('Сохранить'), async () => {
      const formData = new URLSearchParams();

      formData.append("userID", userID);

      for(let i = 0; i < selected.length; i++) formData.append("actionID", selected[i]);

      const addResult = await appAPI.delegated.add(formData);
      if(addResult && addResult.errorCode == '0') {
        UIkit.modal(dialog).hide();
        this.open();
      } else {
        UIkit.notification.closeAll();
        showMessage(addResult.errorMessage, 'error');
      }
    });

    UIkit.modal(dialog).show();
    dialog.on('hidden', () => dialog.remove());
  },

  addDelegate: async function(){
    let filterDepartmentID = null;
    const {positions} = AS.OPTIONS.currentUser;
    if(positions.length) {
      const posHead = positions.find(x => x.type == 1);
      if(posHead) {
        filterDepartmentID = posHead.departmentID;
      } else {
        filterDepartmentID = positions[0].departmentID;
      }
    }

    //(values, multiSelectable, isGroupSelectable, showWithoutPosition, filterPositionID, filterDepartmentID, locale, handler)
    AS.SERVICES.showUserChooserDialog(null, false, false, false, null, filterDepartmentID, AS.OPTIONS.locale, users => {
      this.openDelegateList(users[0].personID);
    });
  },

  editDelegate: async function(item){
    Cons.showLoader();
    const {userId, actions} = item;
    const formData = new URLSearchParams();

    formData.append("userID", userId);

    for(let i = 0; i < actions.length; i++) formData.append("actionID", actions[i].actionId);

    const deleteResult = await appAPI.delegated.del(formData);

    Cons.hideLoader();
    this.openDelegateList(userId, actions.map(x => x.actionId));
  },

  deleteDelegate: async function(item, successHandler){
    UIkit.modal.confirm(i18n.tr('Вы действительно хотите отменить делегирование полномочий данному пользователю?'),
      {labels: {ok: i18n.tr('Да'), cancel: i18n.tr('Отмена')}})
    .then(async () => {
      Cons.showLoader();
      try {
        const {actions} = item;
        const formData = new URLSearchParams();

        formData.append("userID", item.userId);

        for(let i = 0; i < actions.length; i++) formData.append("actionID", actions[i].actionId);

        const deleteResult = await appAPI.delegated.del(formData);
        if(!deleteResult || deleteResult.errorCode !== '0') throw new Error(i18n.tr('Произошла ошибка при удалении делегированных прав'));

        Cons.hideLoader();
        successHandler();
      } catch (e) {
        Cons.hideLoader();
        showMessage(e.message, 'error');
      }
    }, () => null);
  },

  getButton: function(param){
    const {icon, name, className} = param;
    const button = $(`<a href="javascript:void(0);" class="delegate_button">`);

    if(className) button.addClass(className);

    if(name) {
      button.append(
        `<span class="material-icons delegate_button_icon" style="margin-right: 5px;">${icon}</span>`,
        `<span class="delegate_button_name">${i18n.tr(name)}</span>`
      );
    } else {
      button.append(`<span class="material-icons delegate_button_icon">${icon}</span>`);
    }

    return button;
  },

  createRow: function(item){
    const {actions, userId, userName} = item;
    const tr = $('<tr>');
    const editButton = this.getButton({icon: 'more_horiz'});
    const deleteButton = this.getButton({icon: 'clear'});

    const tdEdit = $('<td>');
    const tdDel = $('<td>');

    tdEdit.append(editButton);
    tdDel.append(deleteButton);

    tr.append(
      `<td class="delegate_label">${userName}</td>`,
      `<td style="font-style: italic;">${actions.map(x => x.actionName).join(', ')}</td>`,
      tdEdit, tdDel
    );

    editButton.on('click', e => {
      e.preventDefault();
      e.target.blur();
      this.editDelegate(item);
    });

    deleteButton.on('click', e => {
      e.preventDefault();
      e.target.blur();
      this.deleteDelegate(item, () => {
        this.open();
      });
    });

    return tr;
  },

  getDelegatedTable: function(){
    const {delegatedAssignments} = this.delegatedList;
    if(!delegatedAssignments) return null;

    const table = $('<table>', {class: 'uk-table uk-table-divider'});
    const thead = $('<thead>');
    const tbody = $('<tbody>');
    const trHead = $('<tr>');

    trHead.append(
      `<th class="uk-table-shrink"></th>`,
      `<th class="uk-table-expand"></th>`,
      `<th class="uk-table-shrink"></th>`,
      `<th class="uk-table-shrink"></th>`
    );

    thead.append(trHead);
    table.append(thead, tbody);

    delegatedAssignments.forEach(item => {
      const row = this.createRow(item);
      tbody.append(row);
    });

    return table;
  },

  getDialogContent: function() {
    const container = $('<div>', {class: 'delegate_container'});
    const addPanel = $('<div>', {class: 'delegate_add_panel'});
    const label = $(`<span class="delegate_label">${i18n.tr('Работы')}</span>`);
    const addButton = this.getButton({icon: 'add_circle_outline', name: 'Добавить'});
    const delegateTable = this.getDelegatedTable();

    addPanel.append(label, addButton);
    container.append(addPanel);

    if(delegateTable) container.append(delegateTable);

    addButton.on('click', e => {
      e.preventDefault();
      e.target.blur();
      this.addDelegate();
    });

    return container;
  },

  openDialog: async function(){
    const body = this.getDialogContent();

    const dialog = await UTILS.getModalDialog(i18n.tr('Делегирование'), body);

    UIkit.modal(dialog).show();
    dialog.on('hidden', () => dialog.remove());
  },

  open: async function(){
    this.delegatedList = await appAPI.delegated.get();
    this.openDialog();
  }
}

worflowSettigsInterface.off().on('click', async e => {
  e.preventDefault();
  e.target.blur();

  const {settingsContainer, localeSelect, countWorkSelect} = getSettingsModalContent();
  const dialog = await UTILS.getModalDialog(i18n.tr('Настройки интерфейса'), settingsContainer, i18n.tr('Применить'), () => {
    if(localeSelect.val() != AS.OPTIONS.locale) {
      fire({type: 'change_locale', locale: localeSelect.val()});
    }

    if(localStorage.rowsPerPage != countWorkSelect.val()) {
      localStorage.rowsPerPage = countWorkSelect.val();
      fire({type: 'worklist_row_count_on_page', rowsPerPage: countWorkSelect.val()}, 'workList-1');
    }

    UIkit.modal(dialog).hide();
  });

  UIkit.modal(dialog).show();
  dialog.on('hidden', () => dialog.remove());
});

worflowSettigsSecurity.off().on('click', async e => {
  e.preventDefault();
  e.target.blur();
  const {securityContainer, loginInput, passInput, confPassInput} = getSecurityModalContent();
  const dialog = await UTILS.getModalDialog(i18n.tr('Параметры авторизации'), securityContainer, i18n.tr('Сменить'), async () => {
    try {
      const param = {
        actionCode: 'CHANGE_ALL',
        locale: AS.OPTIONS.locale,
        login: loginInput.val(),
        password: passInput.val(),
        passwordConfirm: confPassInput.val()
      };
      const changeResult = await changeCredentials(param);
      if(changeResult.errorCode != 0) throw new Error(changeResult.errorMessage);

      showMessage(i18n.tr('Авторизационные данные изменены'), 'success');
      UIkit.modal(dialog).hide();

      Cons.logout();
      Cons.login({login: param.login, password: param.password});
      AS.apiAuth.setCredentials(param.login, param.password);
      AS.OPTIONS.login = param.login;
      AS.OPTIONS.password = param.password;
      Cons.creds.login = param.login;
      Cons.creds.password = param.password;

    } catch (err) {
      showMessage(err.message, 'error');
    }
  });

  UIkit.modal(dialog).show();
  dialog.on('hidden', () => dialog.remove());
});

worflowSettigsQR.off().on('click', e => {
  e.preventDefault();
  e.target.blur();
  const contaienr = $('<div>', {style: 'display: flex; justify-content: space-between; align-items: center;'});
  const qr = $('<div>');
  const {origin, pathname} = window.location;

  new QRCode(qr[0], {
    text: `{"login":"${AS.OPTIONS.login}", "host":"${origin + pathname}"}`,
    width: 200,
    height: 200,
    colorDark : "#485156",
    colorLight : "transparent",
    correctLevel : QRCode.CorrectLevel.H
  });

  contaienr.append(`<span>${i18n.tr('QR-код для авторизации')}</span>`, qr);

  const dialog = UTILS.getSmallModalDialog(i18n.tr('QR-код для авторизации'), contaienr);

  UIkit.modal(dialog).show();
  dialog.on('hidden', () => dialog.remove());
});


worflowSettigsDelegation.off().on('click', e => {
  e.preventDefault();
  e.target.blur();
  Delegation.open();
});

worflowButtonLogout.off().on('click', e => {
  e.preventDefault();
  e.target.blur();
  Cons.logout();
  $('.footer-panel').remove();
  Cons.setAppStore({openDocsWindow: null});
});

addListener('workflow_profile_translate', comp.code, () => {
  applyLocales();
});
