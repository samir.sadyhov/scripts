const getRccInfoContent = (docRCC, docInfo, doctypes) => {
  const rccInfoContent = $('<div>');

  const {container: docNameContainer, textarea: docNameInput} = Components.createInputTextArea(i18n.tr("Кр. содерж."));
  docNameInput.val(docRCC.content);
  rccInfoContent.append(docNameContainer);

  const regData = $('<div uk-grid style="align-items: baseline; margin-top: 0;">');
  rccInfoContent.append(regData);

  const {container: docNumberContainer, input: docNumberInput} = Components.createInputText(i18n.tr("Номер"));
  docNumberInput.val(docRCC.number);
  docNumberInput.css('min-width', '290px');
  regData.append(docNumberContainer);

  const {container: docRegDateContainer, dateTimeInput: docRegDateInput} = Components.createDateTimeInput(i18n.tr("Дата регистрации"), docInfo.regDate || '');
  regData.append(docRegDateContainer);
  docRegDateContainer.find('.uk-form-controls').css('display', 'flex');

  const docTypeData = $('<div uk-grid style="align-items: baseline; margin-top: 0;">');
  rccInfoContent.append(docTypeData);

  const docTypeItems = doctypes.map(x => ({label: x.name, value: x.code}));
  const {container: docTypeContainer, select: docTypeSelect} = Components.createSelectComponent(i18n.tr("Тип документа"), docTypeItems);
  docTypeSelect.val(docRCC.docTypeCode);
  docTypeData.append(docTypeContainer);

  const {container: durationContainer, input: durationInput} = Components.createInputNumber(i18n.tr("Длительность (раб.дн)"));
  durationInput.val(docRCC.length || '1');
  docTypeData.append(durationContainer);

  const {container: correspondentOrgContainer, input: correspondentOrgInput} = Components.createInputText(i18n.tr("Корреспондент (орг)"));
  correspondentOrgInput.val(docInfo.correspondentOrg);
  rccInfoContent.append(correspondentOrgContainer);

  const {container: correspondentContainer, input: correspondentInput} = Components.createInputText(i18n.tr("Корреспондент"));
  correspondentInput.val(docInfo.correspondent);
  rccInfoContent.append(correspondentContainer);

  const outDocData = $('<div uk-grid style="align-items: baseline; margin-top: 0;">');
  rccInfoContent.append(outDocData);

  const {container: outDocNumberContainer, input: outDocNumberInput} = Components.createInputText(i18n.tr("Номер исх."));
  outDocNumberInput.css('min-width', '290px');
  outDocNumberInput.val(docRCC.numberOut || '');
  outDocData.append(outDocNumberContainer);

  const {container: outDocRegDateContainer, dateTimeInput: outDocRegDateInput} = Components.createDateTimeInput(i18n.tr("Дата исх."), docRCC.dateOut || '');
  outDocData.append(outDocRegDateContainer);
  outDocRegDateContainer.find('.uk-form-controls').css('display', 'flex');

  const inputs = {
    docNameInput,
    docNumberInput,
    docRegDateInput,
    docTypeSelect,
    durationInput,
    correspondentOrgInput,
    correspondentInput,
    outDocNumberInput,
    outDocRegDateInput
  }

  return {rccInfoContent, inputs};
}

const parseSignDate = date => {
	date = date.split(' ');
  return [date[1].replaceAll('.', ':'), date[0]].join(' ');
}

const signObjToArr = (data, fielsd) => {
  return data.map((x, i) => {
  	const tmp = [];
    fielsd.forEach(key =>
      key == 'index' ? tmp.push(String(i + 1)) : (key == 'signDate' ? tmp.push(parseSignDate(x[key])) : tmp.push(x[key]))
    );
    return tmp;
  });
}

const getSignListContent = list => {
  if(!list || !list.length) return null;

  const content = $('<div>');
  const headers = [i18n.tr('№ п/п'), i18n.tr('Фамилия И.О.'), i18n.tr('Должность'), i18n.tr('Дата'), i18n.tr('Тип подписи'), i18n.tr('Действие'), i18n.tr('Результат действия')];
  const rows = signObjToArr(list, ['index', 'userName', 'position', 'signDate', 'value', 'signType', 'actionResult']);
  const {table} = Components.createTable({headers, rows}, ['rcc-table', 'uk-table-small', 'uk-table-responsive']);
  content.append(table);
  return content;
}

const getOtherListContent = list => {
  if(!list || !list.length) return null;

  const content = $('<div>');
  const headers = [i18n.tr('№ п/п'), i18n.tr('Фамилия И.О.'), i18n.tr('Должность'), i18n.tr('Дата'), i18n.tr('Результат действия'), i18n.tr('Комментарий')];
  const rows = signObjToArr(list, ['index', 'userName', 'position', 'signDate', 'signResult', 'comment']);
  const {table} = Components.createTable({headers, rows}, ['rcc-table', 'uk-table-small', 'uk-table-responsive']);
  content.append(table);
  return content;
}

const getDocflowTable = htmlStr => {
  if(!htmlStr || htmlStr == '') return null;

  const docflowTable = $(htmlStr).find('.docflowTable');
  const docflowTableHeader = [...docflowTable.find('.docflowTableHeader')];

  docflowTableHeader.forEach(x => {
    const currentH = $(x).text().replaceAll('\n', '').trim();
    $(x).text(currentH);
  });

  docflowTable.addClass('uk-table uk-table-small uk-table-responsive rcc-table');

  return docflowTable;
}

const parseHistory = data => {
  const result = [];

  const f = d => {
    d.forEach(x => {
      const t = [];
      const {showInHistory, elementType, name, started, finished, comment, userName, authorName, finishedUser, subProcesses} = x;
      if(showInHistory) {
        if(elementType > 0) {
          t.push(`<span style="padding-left: ${elementType * 5}px;">${name}</span>`);
        } else {
          t.push(name);
        }
        t.push(userName || '');
        t.push(authorName || '');
        t.push(started ? UTILS.customFormatDate(UTILS.formatDate(new Date(started), true)) : '');
        t.push(finished ? UTILS.customFormatDate(UTILS.formatDate(new Date(finished), true)) : '');
        t.push(finishedUser || '');
        t.push(comment || '');
        result.push(t);
      }
      if(subProcesses && subProcesses.length) f(subProcesses);
    });
  }

  f(data);
  return result;
}

const parseChanges = data => {
  const result = [];

  const getDescription = item => {
    const {description, actionID, actionName, userName, fileName, folderName} = item;
    const tmp = {
      2: i18n.tr('{0} добавил в папку "{1}" файл "{2}"'),
      4: i18n.tr('{0} удалил из папки "{1}" файл "{2}"'),
      16: i18n.tr('{0} изменил в папке "{1}" файл "{2}"')
    };

    if(actionID) {
      if(tmp.hasOwnProperty(actionID)) {
        return tmp[actionID].replace('{0}', userName).replace('{1}', folderName).replace('{2}', fileName);
      } else {
        return '';
      }
    } else {
      return description.replaceAll('\n', '<br>');
    }
  }

  data.forEach((item, i) => {
    const {host, userName, date, comment} = item;
    const t = [];
    t.push(`${i+1}.`);
    t.push(userName || '');
    t.push(date ? UTILS.customFormatDate(UTILS.formatDate(new Date(date), true)) : '');
    t.push(host || '');
    t.push(getDescription(item));
    t.push(i18n.tr(comment) || '');
    result.push(t);
  });

  return result;
}

const parseChildDocuments = async (childDocuments, parentDocID) => {
  const result = [];

  if(childDocuments && childDocuments.length) {
    for(let i = 0; i < childDocuments.length; i++) {
      const {author, number, name, documentID, regDate} = childDocuments[i];
      const {docTypeName} = await appAPI.getDocumentRCC(documentID);

      const deleteButton = $(`<a href="javascript:void(0);" uk-icon="trash"></a>`);

      deleteButton.on('click', async e => {
        const msgConfirm = i18n.tr("Вы действительно хотите удалить связь с дочерним документом?");

        UIkit.modal.confirm(msgConfirm, {labels: {ok: i18n.tr('Да'), cancel: i18n.tr('Отмена')}})
        .then(async () => {
          const deleteResult = await appAPI.deleteChildDocument(parentDocID, documentID);
          if(deleteResult && deleteResult.errorCode == 0) {
            $(deleteButton).parent().parent().remove();
            showMessage(i18n.tr(deleteResult.errorMessage), 'success');
          } else {
            showMessage(i18n.tr('Произошла ошибка при удалении дочернего документа'), 'error');
          }
        }, () => {
          return;
        });
      });

      const t = [
        author,
        number,
        name,
        docTypeName || "",
        AS.FORMS.DateUtils.formatDate(new Date(regDate), '${dd}.${mm}.${yyyy}'),
        deleteButton,
        {documentID}
      ];

      result.push(t);
    }
  }

  return result;
}

const getDocHistoryTable = data => {
  if(!data || !data.length) return null;
  const content = $('<div>');
  const headers = [i18n.tr('Название'), i18n.tr('Ответственный'), i18n.tr('Автор'), i18n.tr('Дата начала'), i18n.tr('Дата завершения'), i18n.tr('Завершил'), i18n.tr('Комментарий')];
  const rows = parseHistory(data);
  const {table} = Components.createTable({headers, rows}, ['rcc-table', 'uk-table-small', 'uk-table-responsive']);
  content.append(table);
  return content;
}

const getDocChangesTable = data => {
  if(!data || !data.length) return null;
  const content = $('<div>');
  const headers = [i18n.tr('№'), i18n.tr('Пользователь'), i18n.tr('Дата'), i18n.tr('IP-адрес'), i18n.tr('Описание'), i18n.tr('Комментарий')];
  const rows = parseChanges(data);
  const {table} = Components.createTable({headers, rows}, ['rcc-table', 'uk-table-small', 'uk-table-responsive']);
  content.append(table);
  return content;
}

const createChildDocumentsTable = (data, classes = []) => {
  const {headers, rows} = data;
  const container = $('<div>');
  const table = $('<table>', {class: "uk-table"});
  const thead = $('<thead>');
  const tbody = $('<tbody>');
  const theadTr = $('<tr>');

  classes.forEach(addClass => table.addClass(addClass));

  headers.forEach(header => theadTr.append(`<th>${header}</th>`));

  rows.forEach(row => {
    const tr = $('<tr>');
    tbody.append(tr);
    row.forEach(text => {
      if(typeof text == 'object' && text.hasOwnProperty('documentID')) {
        tr.on('dblclick', e => {
          if(e.target.nodeName !== 'TD') return;

          const eventParam = {
            type: 'document',
            documentID: text.documentID
          };
          $('#root-panel').trigger({type: 'custom_open_document', eventParam});
        });
      } else {
        const td = $('<td>');
        td.html(text);
        tr.append(td);
      }
    });
  });

  thead.append(theadTr);
  table.append(thead).append(tbody);
  container.append(table);

  return {container, table};
}

const getChildDocumentsContent = async (childDocuments, parentDocID) => {
  const content = $('<div>');
  const headers = [i18n.tr('Автор'), i18n.tr('Номер'), i18n.tr('Кр. содерж.'), i18n.tr('Тип документа'), i18n.tr('Дата регистрации'), ''];
  const rows = await parseChildDocuments(childDocuments, parentDocID);
  const {table} = createChildDocumentsTable({headers, rows}, ['rcc-table', 'uk-table-small', 'uk-table-responsive']);
  content.append(table);
  return content;
}

this._RCC = class {

  constructor(docInfo, parentContainer){
    this.docInfo = docInfo;
    this.parentContainer = parentContainer;

    this.documentID = null;
    this.doctypes = null;
    this.docRCC = null;
    this.signList = {};
    this.documentHistory = null;
    this.docChanges = null;
    this.childDocuments = null;

    this.init();
  }

  initSaveRCC(inputs) {
    const getDateTime = dt => $(dt[0]).val() != '' ? `${$(dt[0]).val()}`.trim() : '';

    const {
      docNameInput,
      docNumberInput,
      docRegDateInput,
      docTypeSelect,
      durationInput,
      correspondentOrgInput,
      correspondentInput,
      outDocNumberInput,
      outDocRegDateInput
    } = inputs;

    this.buttonSaveRCC.on('click', e => {
      Cons.showLoader();

      const rccData = {};
      rccData.documentID = this.documentID;
      rccData.subject = docNameInput.val();
      rccData.number = docNumberInput.val();
      rccData.reg_date = getDateTime(docRegDateInput);
      rccData.doc_type = docTypeSelect.val();
      rccData.duration = durationInput.val();
      rccData.correspondent_org = correspondentOrgInput.val();
      rccData.correspondent = correspondentInput.val();
      rccData.base_number = outDocNumberInput.val();
      rccData.base_date = getDateTime(outDocRegDateInput);
      rccData.base = this.docRCC.bases;

      if(!rccData.doc_type) {
        showMessage(i18n.tr('Выберите тип документа. Если список пуст, то обратитесь к разработчику Synergy.'), 'error');
        Cons.hideLoader();
        return;
      }

      appAPI.saveRCC(rccData).then(res => {
        Cons.hideLoader();
        if(res && res.errorCode == 0) {
          showMessage(i18n.tr('Документ сохранен'), 'success');
        } else {
          showMessage(i18n.tr('Ошибка сохранения РКК'), 'error');
        }
      });

    });
  }

  async render() {
    const rccContainer = $('<div>', {class: 'rcc_container'});
    const rccData = $('<div>', {class: 'rcc_data'});
    const panelRccButtons = $('<div>', {class: 'rcc_buttons'});

    this.buttonSaveRCC = $('<div>', {id: 'buttonSaveRCC', title: i18n.tr("Сохранить")});
    this.buttonSaveRCC.append('<span class="material-icons">save</span>');

    panelRccButtons.append(this.buttonSaveRCC);
    rccContainer.append(panelRccButtons, rccData);
    this.parentContainer.empty().append(rccContainer);

    const accordionItems = [];

    //осноная информация по РКК
    const {rccInfoContent, inputs} = getRccInfoContent(this.docRCC, this.docInfo, this.doctypes);
    if(rccInfoContent) {
      accordionItems.push({
        title: i18n.tr("РКК"),
        content: rccInfoContent,
        open: true
      });
      this.initSaveRCC(inputs);
    }

    //ход выполнения
    const historyTable = getDocHistoryTable(this.documentHistory);
    if(historyTable) accordionItems.push({
      title: i18n.tr("Ход выполнения"),
      content: historyTable
    });

    //Изменения в документе
    const changesTable = getDocChangesTable(this.docChanges);
    if(changesTable) accordionItems.push({
      title: i18n.tr("Изменения в документе"),
      content: changesTable
    });

    // подписи
    const signListContent = getSignListContent(this.signList.signs);
    if(signListContent) accordionItems.push({
      title: i18n.tr("Лист подписей"),
      content: signListContent
    });

    // утверждения
    const approvalListContent = getOtherListContent(this.signList.approval);
    if(approvalListContent) accordionItems.push({
      title: i18n.tr("Лист утверждения"),
      content: approvalListContent
    });

    // согласования
    const agreementListContent = getOtherListContent(this.signList.agreement);
    if(agreementListContent) accordionItems.push({
      title: i18n.tr("Лист согласования"),
      content: agreementListContent
    });

    // ознакомления
    const acquaintanceListContent = getOtherListContent(this.signList.acquaintance);
    if(acquaintanceListContent) accordionItems.push({
      title: i18n.tr("Лист ознакомления"),
      content: acquaintanceListContent
    });

    // Дочерние документы
    const childDocumentsContent = await getChildDocumentsContent(this.childDocuments, this.documentID);
    if(childDocumentsContent) accordionItems.push({
      title: i18n.tr("Дочерние документы"),
      content: childDocumentsContent
    });

    const accordion = Components.createAccordion(accordionItems, 'rcc-accordion');

    rccData.append(accordion);
  }

  async init(){

    this.documentID = this.docInfo.documentID;
    this.doctypes = await appAPI.getDoctypes();
    this.docRCC = await appAPI.getDocumentRCC(this.documentID);
    this.documentHistory = await appAPI.getDocumentHistory(this.documentID); //ход выполнения
    this.docChanges = await appAPI.getDocChanges(this.documentID); //Изменения в документе
    this.signList.signs = await appAPI.getSignListXML(this.documentID, -1); // подписи
    this.signList.approval = await appAPI.getSignListXML(this.documentID, 1); // утверждения
    this.signList.agreement = await appAPI.getSignListXML(this.documentID, 0); // согласования
    this.signList.acquaintance = await appAPI.getSignListXML(this.documentID, 2); // ознакомления
    this.childDocuments = await appAPI.getChildDocuments(this.documentID); // Дочерние документы

    this.render();
  }
}
