const getUrlParameter = param => {
  const url = new URLSearchParams(window.location.search);
  return url.has(param) ? url.get(param) : null;
}

const setWorkInfo = selectWorkInfo => {
  const {name, author, user, start_date, finish_date} = selectWorkInfo;

  UTILS.changeLabel('workNameLabel', name, name);
  UTILS.changeLabel('workAuthorLabel', `Автор: ${author.name}`, `Автор: ${author.name}`, `Author: ${author.name}`);
  UTILS.changeLabel('workUserLabel', `Ответственный: ${user.name}`, `Жауапты: ${user.name}`, `Assignee: ${user.name}`);
  UTILS.changeLabel('workStartDateLabel', `Дата начала: ${UTILS.customFormatDate(start_date)}`, `Басталу күні: ${UTILS.customFormatDate(start_date)}`, `Starting date: ${UTILS.customFormatDate(start_date)}`);
  UTILS.changeLabel('workFinishDateLabel', `Дата завершения: ${UTILS.customFormatDate(finish_date)}`, `Аяқталу күні: ${UTILS.customFormatDate(finish_date)}`, `Completion date: ${UTILS.customFormatDate(finish_date)}`);
}

let rightPanelVisible = false;
const hideShowPanelWorkInfo = (visible = false) => {
  if(visible) {
    $('#rightPanel').css({
      'right': '-300px',
      'opacity': '0'
    });
    $('#panelContent').css({
      'width': 'calc(100% - 300px)',
      'right': '0px'
    });
    $('#buttonWorkInfo').removeClass('pressed');
  } else {
    $('#rightPanel').css({
      'right': '0px',
      'opacity': '1'
    });
    $('#panelContent').css({
      'width': 'calc(100% - 600px)',
      'right': '300px'
    });
    $('#buttonWorkInfo').addClass('pressed');
  }
  rightPanelVisible = visible;
}

pageHandler('listWorksPage', () => {
  if (!Cons.getAppStore().listWorksPage_listener) {

    addListener('tree_item_click', 'workFilterTree', e => {
      const { item: { filterData } } = e;
      fire({type: 'worklist_filter_arbitrary_change', ...filterData}, 'workList-1');
      $('#searchWorkInput').val('');
    });

    addListener('worklist_item_click', 'workList-1', selectWorkInfo => {
      const {actionID} = selectWorkInfo;
      const panelWorkComments = $('#panelWorkComments');

      UTILS.show('panelWorkLabels');
      setWorkInfo(selectWorkInfo);

      hideShowPanelWorkInfo(false);

      panelWorkComments.empty();
      new DocumentComments(null, selectWorkInfo.actionID, panelWorkComments);

      fire({type: 'work_files_init', workID: actionID}, 'workFilesList');
    });

    addListener('worklist_item_dbl_click', 'workList-1', async selectWorkInfo => {
      const {actionID, is_new} = selectWorkInfo;

      const event = {
        type: 'work',
        actionID,
        workInfo: selectWorkInfo
      };

      $('#root-panel').trigger({type: 'custom_open_document', eventParam: event});
    });

    addListener('button_click', 'buttonUpdateWorkList', e => {
      fire({type: 'worklist_update'}, 'workList-1');
      fire({type: 'reload_tree'}, 'workFilterTree');
    });

    addListener('text_input_key_down_enter', 'searchWorkInput', async e => {
      const {value: searchValue = ''} = e;
      fire({type: 'worklist_search', value: searchValue}, 'workList-1');
    });

    addListener('button_click', 'buttonAnalytics', e => {
      userWorkAnalytics();
    });

    Cons.setAppStore({listWorksPage_listener: true});
  }

  // файлы работ --------------------------------------------------------------------
  // двойной клик по файлу и пункт меню Открыть
  $('#workFilesList').off().on('work_files_dbl_click work_files_context_open', e => {
    const {name, uuid} = e.eventParam;
    const file = new AttachmentFile(name, uuid);
    file.open();
  });

  fire({type: 'reports_init', objectType: 4}, 'buttonReports');

  $('#searchWorkInput').parent().find('span').off().on('click', async e => {
    fire({type: 'worklist_search', value: $('#searchWorkInput').val()}, 'workList-1');
  });

  // скрыть/показать панель "Подробнее"
  let buttonWorkInfoClikc = false;
  $('#buttonWorkInfo').off().on('click', e => {
    e.preventDefault();
    e.target.blur();
    hideShowPanelWorkInfo(buttonWorkInfoClikc ? !rightPanelVisible : false);
    buttonWorkInfoClikc = true;
  });

  const urlActionID = getUrlParameter('actionID');
  const urlDocID = getUrlParameter('document_identifier');

  if(urlActionID) {
    $('#root-panel').trigger({
      type: 'custom_open_document',
      eventParam: {
        type: 'work',
        actionID: urlActionID,
      }
    });
  } else if (urlDocID) {
    $('#root-panel').trigger({
      type: 'custom_open_document',
      eventParam: {
        type: 'document',
        documentID: urlDocID,
      }
    });
  }
});
