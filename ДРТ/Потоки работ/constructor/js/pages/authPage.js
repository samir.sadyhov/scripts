const addLog = async data => {
  return new Promise(async resolve => {
    AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/logger/log", resolve, 'json', JSON.stringify(data), "application/json; charset=UTF-8", resolve);
  });
}

pageHandler('authPage', () => {
  if (!Cons.getAppStore().auth_page_listener) {
    addListener('auth_success', 'buttonLogin', async authed => {
      const {login, password} = authed.creds;

      Cons.creds.login = login;
      Cons.creds.password = password;
      AS.apiAuth.setCredentials(login, password);
      AS.OPTIONS.login = login;
      AS.OPTIONS.password = password;
      AS.OPTIONS.currentUser = authed.data.person;

      Cons.showLoader();

      await workflowAppInit();

      await addLog({
        "providerId": "AI_SEC",
        "eventId": "2005",
        "userId": AS.OPTIONS.currentUser.userid,
        "objects": [AS.OPTIONS.login, "Workflow"]
      });

      Cons.hideLoader();

      fire({type: 'goto_page', pageCode: 'listWorksPage'}, 'root-panel');
    });

    Cons.setAppStore({auth_page_listener: true});
  }
});
