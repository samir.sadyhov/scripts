const getUrlParameter = param => {
  const url = new URLSearchParams(window.location.search);
  return url.has(param) ? url.get(param) : null;
}

this.updateTranslations = async (locale = AS.OPTIONS.locale) => {
  const {translations} = Cons.getAppStore();
  i18n.locale = locale;
  return new Promise(resolve => {
    rest.synergyGet(`system/messages?localeID=${locale}`,
      res => {
        i18n.messages = res;
        if(translations && translations != 'not found') {
          for(const key in translations) i18n.messages[key] = translations[key][AS.OPTIONS.locale];
        }
        resolve(true);
      },
      err => {
        i18n.messages = null;
        resolve(true);
      }
    );
  });
}

//доп функции для переводов
const localized = () => {
  const {code} = Cons.getCurrentPage();

  switch (code) {
    case 'listWorksPage':
      fire({type: 'worklist_translate'}, 'workList-1');
      fire({type: 'work_files_translate'}, 'workFilesList');
      fire({type: 'workflow_profile_translate'}, 'workflowProfileMenu');
      fire({type: 'wf_menu_translate'}, 'customButtonMenu');

      $('#panelWorkComments').trigger({type: 'change_locale_after', eventParam: {locale: AS.OPTIONS.locale}});
      break;
  }
}

if (!Cons.getAppStore().change_locale_listener) {

  addListener('change_locale', 'root-panel', async e => {
    const urlLocale = getUrlParameter('locale');

    if(!urlLocale) {
      const {locale} = e;
      AS.OPTIONS.locale = locale;
      localStorage.locale = locale;
      UTILS.Cookie.set('ConstructorLocale', locale);

      await updateTranslations(locale);

      const systemSettings = await appAPI.getSystemSettings();
      Cons.setAppStore({systemSettings});

      localized();
  	}

  });

  Cons.setAppStore({change_locale_listener: true});
}
