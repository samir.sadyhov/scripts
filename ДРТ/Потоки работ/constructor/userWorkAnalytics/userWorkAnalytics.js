$.getScript("https://www.gstatic.com/charts/loader.js")
.done(function( script, textStatus ) {
  google.charts.load("current", { packages: ["corechart"] });
});

$.getScript("https://cdnjs.cloudflare.com/ajax/libs/html2canvas/1.4.1/html2canvas.min.js");

$.getScript("https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.2.12/pdfmake.min.js").done(() => {
  $.getScript("https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.2.12/vfs_fonts.min.js").done(() => {
    pdfMake.fonts = {
      Courier: {
        normal: 'Courier',
        bold: 'Courier-Bold',
        italics: 'Courier-Oblique',
        bolditalics: 'Courier-BoldOblique'
      },
      Helvetica: {
        normal: 'Helvetica',
        bold: 'Helvetica-Bold',
        italics: 'Helvetica-Oblique',
        bolditalics: 'Helvetica-BoldOblique'
      },
      Times: {
        normal: 'Times-Roman',
        bold: 'Times-Bold',
        italics: 'Times-Italic',
        bolditalics: 'Times-BoldItalic'
      },
      Symbol: {
        normal: 'Symbol'
      },
      ZapfDingbats: {
        normal: 'ZapfDingbats'
      },
      Roboto: {
        normal: 'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.2.12/fonts/Roboto/Roboto-Regular.ttf',
        bold: 'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.2.12/fonts/Roboto/Roboto-Medium.ttf',
        italics: 'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.2.12/fonts/Roboto/Roboto-Italic.ttf',
        bolditalics: 'https://cdnjs.cloudflare.com/ajax/libs/pdfmake/0.2.12/fonts/Roboto/Roboto-MediumItalic.ttf'
      }
    };
  });
});


const extDateSearchType = [
  {title: i18n.tr("Все"), value: 0},
  {title: i18n.tr("Дата завершения"), value: 1},
  {title: i18n.tr("Дата начала"), value: 2},
  {title: i18n.tr("Дата фактического завершения"), value: 3}
];

const periodType = [
  {title: i18n.tr('В работе'), value: 0},
  {title: i18n.tr('Произвольный период'), value: 1},
  {title: i18n.tr('Прошедший квартал'), value: 9},
  {title: i18n.tr('Прошедший месяц'), value: 8},
  {title: i18n.tr('Прошедшая неделя'), value: 7},
  {title: i18n.tr('Сегодня'), value: 6},
  {title: i18n.tr('Будущая неделя'), value: 2},
  {title: i18n.tr('Будущий месяц'), value: 3},
  {title: i18n.tr('Будущий квартал'), value: 4}
];

const defaultDate = AS.FORMS.DateUtils.formatDate(new Date(), '${yyyy}-${mm}-${dd} ${HH}:${MM}');

const uwaParams = {
  selectUser: null,
  extDateSearchType: extDateSearchType[0].value,
  periodType: periodType[0].value,
  extStartDate: `${defaultDate}:00`,
  extFinishDate: `${defaultDate}:00`,

  reset: function(){
    this.selectUser = null;
    this.extDateSearchType = extDateSearchType[0].value;
    this.periodType = periodType[0].value;
    this.extStartDate = `${defaultDate}:00`;
    this.extFinishDate = `${defaultDate}:00`;
  },

  checkError: function(){
    if(!this.selectUser) return i18n.tr('Не выбран сотрудник');

    if(this.periodType == 1) {
      if(this.extFinishDate < this.extStartDate) return i18n.tr('Некорректно выбран период');
    }

    return false;
  },

  parseDate: function(date){
    return AS.FORMS.DateUtils.formatDate(new Date(date), '${dd}-${mm}-${yyyy}');
  },

  getPeriod: function(){
    let startDate = new Date();
    let finishDate = new Date();

    switch (this.periodType) {
      case 1: // Произвольный период
        startDate = new Date(this.extStartDate);
        finishDate = new Date(this.extFinishDate);
      case 7: // Прошедшая неделя
        startDate.setDate(startDate.getDate() - 7);
        break;
      case 8: // Прошедший месяц
        startDate.setMonth(startDate.getMonth() - 1);
        break;
      case 9: // Прошедший квартал
        startDate.setMonth(startDate.getMonth() - 3);
        break;
      case 2: // Будущая неделя
        finishDate.setDate(finishDate.getDate() + 7);
        break;
      case 3: // Будущий месяц
        finishDate.setMonth(finishDate.getMonth() + 1);
        break;
      case 4: // Будущий квартал
        finishDate.setMonth(finishDate.getMonth() + 3);
        break;
    }

    startDate = AS.FORMS.DateUtils.formatDate(startDate, AS.FORMS.DateUtils.DATE_INPUT_FORMAT);
    finishDate = AS.FORMS.DateUtils.formatDate(finishDate, AS.FORMS.DateUtils.DATE_INPUT_FORMAT);

    return {startDate, finishDate};
  },

  getURLParams: function(){
    const urlencoded = new URLSearchParams();
    urlencoded.append("userID", this.selectUser[0].personID);
    urlencoded.append("filterType", "OWN_WORKS");
    urlencoded.append("recordsCount", 0);
    urlencoded.append("locale", AS.OPTIONS.locale);
    urlencoded.append("hasExtSearchParams", true);
    urlencoded.append("periodType", this.periodType);
    urlencoded.append("extDateSearchType", this.extDateSearchType);
    urlencoded.append("extShowCompleted", true);

    if(this.periodType == 1) {
      urlencoded.append("extStartDate", this.parseDate(this.extStartDate));
      urlencoded.append("extFinishDate", this.parseDate(this.extFinishDate));
    }

    return urlencoded.toString();
  }
};

const createSelectComponent = (label, items) => {
  const container = $('<div>');
  const fc = $('<div>', {class: 'uk-form-controls'});
  const select = new CustomComboBox(items);

  select.container.css('width', '100%');
  fc.append(select.container);
  container.append(`<label class="uk-form-label uk-text-bold">${i18n.tr(label)}</label>`).append(fc);

  return {container, select};
}

const createUserParamBlock = () => {
  const fc = $('<div>', {class: 'uk-form-controls'});
  const button = $('<a class="uk-form-icon uk-form-icon-flip" href="#" uk-icon="icon: users"></a>');
  const input = $('<input class="uk-input" type="text" disabled>');

  input.css({'background-color': '#fff', 'color': '#666'});

  fc.append($('<div class="uk-inline uk-width-expand">').append(button, input));

  if(uwaParams.selectUser) {
    let userNames = values.map(x => x.personName).join('; ');
    input.val(userNames).attr('title', userNames);
  }

  button.on('click', e => {
    //(values, multiSelectable, isGroupSelectable, showWithoutPosition, filterPositionID, filterDepartmentID, locale, handler)
    AS.SERVICES.showUserChooserDialog(uwaParams.selectUser, false, false, false, null, null, AS.OPTIONS.locale, users => {
      let userNames = users.map(x => x.personName).join('; ');
      input.val(userNames).attr('title', userNames);
      uwaParams.selectUser = users;
    });
  });

  return $('<div>').append(`<label class="uk-form-label uk-text-bold">${i18n.tr("Сотрудник")}</label>`, fc);
}

const getListWork = async params => {
  return new Promise(async resolve => {
    rest.synergyGet(`api/workflow/works/list_ext?${params}`, resolve, err => {
      console.log(`ERROR [ getListWork ]: ${JSON.stringify(err)}`);
      resolve(null);
    });
  });
}

const getUserWorkload = async () => {
  const {startDate, finishDate} = uwaParams.getPeriod();

  return new Promise(async resolve => {
    try {
      const {login, password} = Cons.creds;
      const auth = "Basic " + btoa(unescape(encodeURIComponent(`${login}:${password}`)));
      const url = `../Synergy/rest/person/workload`;
      const response = await fetch(url, {
        method: 'POST',
        headers: {"Authorization": auth, "Content-Type": "application/json; charset=UTF-8"},
        body: JSON.stringify({
          userID: uwaParams.selectUser[0].personID,
          startDate, finishDate
        })
      });
      if(!response.ok) throw new Error(`HTTP: ${response.status}, message: ${response.statusText} [ ${response.text()} ]`);
      resolve(response.json());
    } catch (err) {
      console.log(`ERROR [ getUserWorkload ]: ${JSON.stringify(err)}`);
      resolve(null);
    }
  });
}

const getUserTitle = () => {
  return `
  <div class="uk-grid-small uk-flex-middle uwa_userInfoPanel" uk-grid>
    <div class="uk-width-auto">
      <img class="uk-border-circle" width="110" height="110" src="../Synergy/load?userid=${uwaParams.selectUser[0].personID}" alt="Avatar">
    </div>
    <div class="uk-width-expand">
      <h3 class="uk-card-title uk-margin-remove-bottom">${uwaParams.selectUser[0].personName}</h3>
      <p class="uk-text-meta uk-margin-remove-top"><span>${uwaParams.selectUser[0].positionName}</span></p>
    </div>
  </div>`;
}

const renderBarChart = (barChartContainer, listWorks) => {
  let is_expired = 0;
  let finished = 0;
  let not_finished = 0;

  listWorks.data.forEach(work => {
    if(work.is_expired == "true") is_expired++;
    if(work.hasOwnProperty('finished')) {
      if(new Date(work.finish_date) < new Date(work.finished)) is_expired++;
      finished++;
    } else {
      not_finished++;
    }
  });

  const data = [];
  data.push([i18n.tr('Статус'), i18n.tr('Количество'), { role: 'style' }]);
  data.push([i18n.tr('Всего'), listWorks.totalCount, '#3366cc']);
  data.push([i18n.tr('Завершенных'), finished, '#109618']);
  data.push([i18n.tr('Просроченных'), is_expired, '#dc3912']);
  data.push([i18n.tr('На исполнении'), not_finished, '#0099c6']);

  const chartData = google.visualization.arrayToDataTable(data);
  const view = new google.visualization.DataView(chartData);
  view.setColumns([0, 1,
    {calc: "stringify",
    sourceColumn: 1,
    type: "string",
    role: "annotation"},
    2]);

  const options = {
    title: i18n.tr('Статистика по потокам работ'),
    legend: { position: "none" },
    width: '100%',
    chartArea: {width: '70%'}
  }

  const chart = new google.visualization.BarChart(barChartContainer[0]);
  chart.draw(view, options);
}

const renderLineChart = (lineChartContainer, workload) => {
  if(!workload.length) return;
  const data = [];
  data.push([i18n.tr('Дата'), i18n.tr('Нагрузка')]);

  workload.forEach(item => {
    const {date, value} = item;
    data.push([date, Number(value)]);
  });

  const chartData = google.visualization.arrayToDataTable(data);

  const options = {
    legend: 'none',
    title: i18n.tr('Занятость по потокам работ')
  }

  const chart = new google.visualization.LineChart(lineChartContainer[0]);
  chart.draw(chartData, options);
}

const renderInfo = (panelInfo, listWorks, workload) => {
  const chartsContainer = $('<div>', {class: 'uwa_chartsContainer'});

  const barChartContainer = $('<div>', {class: 'uwa_barChartContainer'});
  const lineChartContainer = $('<div>', {class: 'uwa_lineChartContainer'});

  chartsContainer.append(barChartContainer, lineChartContainer);
  panelInfo.append(getUserTitle(), chartsContainer);

  renderBarChart(barChartContainer, listWorks);
  renderLineChart(lineChartContainer, workload);
}

const createRow = async (tBody, item, is_expired_count) => {
  const {name, is_expired, start_date, finish_date, remained, remained_label, finished} = item;
  const dateFormat = "${dd}.${mm}.${yyyy} ${HH}:${MM}";
  const tr = $('<tr>');

  const startDateFormated = AS.FORMS.DateUtils.formatDate(AS.FORMS.DateUtils.parseDate(start_date), dateFormat);
  const finishedFormated = AS.FORMS.DateUtils.formatDate(AS.FORMS.DateUtils.parseDate(finished), dateFormat);
  const finishDateFormated = AS.FORMS.DateUtils.formatDate(AS.FORMS.DateUtils.parseDate(finish_date), dateFormat);

  tr.append(`<td>${is_expired_count}</td>`);
  tr.append(`<td>${name}</td>`);
  tr.append(`<td>${startDateFormated}</td>`);
  tr.append(`<td>${finishDateFormated}</td>`);
  tr.append(`<td>${finishedFormated}</td>`);
  if(remained_label == "") {
    tr.append(`<td>${Number(remained).toFixed()} ${i18n.tr('дн')}</td>`);
  } else {
    tr.append(`<td>${remained_label}</td>`);
  }

  tr.on('click', e => {
    tBody.find('tr').removeAttr('selected');
    tr.attr('selected', true);
  });

  tBody.append(tr);
}

const renderData = (panelData, listWorks) => {
  const table = $('<table class="workflow-table-list uk-table uk-table-small uk-table-responsive">');
  const tHead = $('<thead>');
  const tBody = $('<tbody>');
  const headTR = $('<tr>');

  headTR.append(`<th style="width: 40px;">№</th>`);
  headTR.append(`<th class="uk-table-expand">${i18n.tr('Название')}</th>`);
  headTR.append(`<th class="uk-width-small">${i18n.tr('Дата начала')}</th>`);
  headTR.append(`<th class="uk-width-small">${i18n.tr('Плановая дата')}</th>`);
  headTR.append(`<th class="uk-width-small">${i18n.tr('Дата завершения')}</th>`);
  headTR.append(`<th class="uk-width-small">${i18n.tr('Просрочено')}</th>`);

  let is_expired_count = 0;

  listWorks.data.forEach(item => {
    if(item.is_expired == "true") {
      is_expired_count++;
      createRow(tBody, item, is_expired_count);
    } else if (item.hasOwnProperty('finished') && new Date(item.finish_date) < new Date(item.finished)) {
      is_expired_count++;
      createRow(tBody, item, is_expired_count);
    }
  });

  tHead.append(headTR);
  table.append(tHead).append(tBody);
  panelData.empty().append(table);

  return;
}

const renderContent = async (panelContent, buttonDownload) => {
  panelContent.empty();
  try {
    Cons.showLoader();

    const isError = uwaParams.checkError();
    if(isError) throw new Error(isError);

    const urlParam = uwaParams.getURLParams();
    const listWorks = await getListWork(urlParam);

    if(!listWorks) throw new Error(i18n.tr('Ошибка получения списка работ'));
    if(listWorks.hasOwnProperty('errorCode') && listWorks.errorCode != 0) throw new Error(listWorks.errorMessage);
    if(listWorks.totalCount == 0) throw new Error(i18n.tr('Не найдено работ по заданным параметрам'));

    const workload = await getUserWorkload();

    const panelInfo = $('<div>', {class: 'uwa_dialog_panelContent_info'});
    const panelData = $('<div>', {class: 'uwa_dialog_panelContent_data'});

    panelContent.append(panelInfo, panelData);

    renderInfo(panelInfo, listWorks, workload);
    renderData(panelData, listWorks);

    buttonDownload.show();

    Cons.hideLoader();
  } catch (err) {
    showMessage(err.message, 'error');
    Cons.hideLoader();
  }

}

const generatePDF = async panelContent => {
  try {
    Cons.showLoader();

    const barChartContainer = panelContent.find('.uwa_barChartContainer>div');
    const rows = panelContent.find('.uwa_dialog_panelContent_data table>tbody>tr');

    const canvas = await html2canvas(barChartContainer.get(0));
    const imageCharts = canvas.toDataURL('image/png');

    const tableHeader = [
      {text: `№`,  bold: true},
      {text: `${i18n.tr('Название')}`,  bold: true, fillColor: '#FAFAFA'},
      {text: `${i18n.tr('Дата начала')}`,  bold: true, fillColor: '#FAFAFA'},
      {text: `${i18n.tr('Плановая дата')}`,  bold: true, fillColor: '#FAFAFA'},
      {text: `${i18n.tr('Дата завершения')}`,  bold: true, fillColor: '#FAFAFA'},
      {text: `${i18n.tr('Просрочено')}`,  bold: true, fillColor: '#FAFAFA'}
    ];
    const tableData = rows.get().map(row => {
      const cells = $(row).find('td');
      return cells.get().map(cell => cell.textContent.trim());
    });

    const docDefinition = {
      pageSize:'A4',
      pageOrientation: 'portrait',
      pageMargins: [30, 20, 30, 20], //[left, top, right, bottom]
      defaultStyle:{
        font: 'Roboto',
        fontSize: 9
      },

      content: [
        {text: `${uwaParams.selectUser[0].personName}\n`, fontSize: 18, bold: true},
        {text: `${uwaParams.selectUser[0].positionName}\n`, fontSize: 12, color: "#666"},
        {text: `За период с ${uwaParams.extStartDate} по ${uwaParams.extFinishDate}\n\n`, fontSize: 10, color: "#666"},
        {
          image: imageCharts,
          width: 400,
          alignment: 'center'
        },
        {text: `\n${i18n.tr('Данные по просроченным работам:')}\n`, fontSize: 14},
        {
          table: {
            headerRows: 1,
            body: [tableHeader, ...tableData]
          }
        }
      ]
    };

    pdfMake.createPdf(docDefinition).download(`${i18n.tr('Аналитика по работам пользователя')}.pdf`);

    Cons.hideLoader();
  } catch (error) {
    Cons.hideLoader();
    console.error('Ошибка при генерации PDF:', error);
  }
}

const openDialog = () => {
  const dialog = $('<div>', {class: 'uwa_dialog'});
  const panelContent = $('<div>', {class: 'uwa_dialog_panelContent'});
  const leftPanel = $('<div>', {class: 'uwa_dialog_leftPanel'});
  const panelFilters = $('<div>', {class: 'uwa_dialog_panelFilters'});
  const panelButtons = $('<div>', {class: 'uwa_dialog_panelButtons'});
  const buttonApply = $(`<button class="uk-button uk-button-primary uk-button-small" type="button">${i18n.tr('Применить')}</button>`);
  const buttonDownload = $(`<button class="uk-button uk-button-default uk-button-small" type="button" style="display: none;">${i18n.tr('Скачать')}</button>`);

  const {container: workPeriodContainer, dateStart, dateFinish} = Components.createDatePeriod(i18n.tr("Период"), defaultDate, defaultDate);
  workPeriodContainer.removeClass('uk-margin-small');
  workPeriodContainer.hide();

  $(dateStart[0]).on('change', e => uwaParams.extStartDate = `${$(dateStart[0]).val()} ${$(dateStart[1]).val()}:00`.slice(0, 19));
  $(dateStart[1]).on('change', e => uwaParams.extStartDate = `${$(dateStart[0]).val()} ${$(dateStart[1]).val()}:00`.slice(0, 19));
  $(dateFinish[0]).on('change', e => uwaParams.extFinishDate = `${$(dateFinish[0]).val()} ${$(dateFinish[1]).val()}:00`.slice(0, 19));
  $(dateFinish[1]).on('change', e => uwaParams.extFinishDate = `${$(dateFinish[0]).val()} ${$(dateFinish[1]).val()}:00`.slice(0, 19));

  const {container: selectPeriodTypeContainer, select: selectPeriodType} = createSelectComponent(i18n.tr("Тип периода"), periodType);
  const {container: selectDateSearchTypeContainer, select: selectDateSearchType} = createSelectComponent(i18n.tr("Тип поиска по датам"),extDateSearchType);

  selectPeriodType.container.on('selected', e => {
    uwaParams.periodType = e.eventParam;
    if(uwaParams.periodType == 1) {
      workPeriodContainer.show();
    } else {
      workPeriodContainer.hide();
    }
  });

  selectDateSearchType.container.on('selected', e => {
    uwaParams.extDateSearchType = e.eventParam;
  });

  leftPanel.append(panelFilters, panelButtons);
  panelButtons.append(buttonApply, buttonDownload);

  panelFilters.append(
    createUserParamBlock(),
    selectPeriodTypeContainer,
    workPeriodContainer,
    selectDateSearchTypeContainer
  );

  buttonApply.on('click', e => {
    e.preventDefault();
    e.target.blur();
    renderContent(panelContent, buttonDownload);
  });

  buttonDownload.on('click', e => {
    e.preventDefault();
    e.target.blur();

    generatePDF(panelContent);
  });

  dialog.append(leftPanel, panelContent);

  dialog.dialog({
    modal: true,
    width: window.innerWidth - 100,
    height: window.innerHeight - 100,
    title: i18n.tr('Аналитика по работам пользователя'),
    resizable: false,
    draggable: false,
    show: {effect: 'fade', duration: 300},
    hide: {effect: 'fade', duration: 300},
    close: function() {
      $(this).remove();
    }
  });
}

this.userWorkAnalytics = () => {
  uwaParams.reset();
  openDialog();
}
