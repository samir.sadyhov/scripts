const appDS = model.playerModel.getModelWithId('workflow_form_leave_application_ds'); // дата начала отпуска
const appDF = model.playerModel.getModelWithId('workflow_form_leave_application_df'); // дата завершения отпуска
const appDays = model.playerModel.getModelWithId('workflow_form_leave_application_days'); // количество дней отпуска
const appUser = model.playerModel.getModelWithId('workflow_form_leave_application_from_user'); //юзер на форме
const appPosition = model.playerModel.getModelWithId('workflow_form_leave_application_from_position'); //должность на форме

const personalUserCardCodeVacations = 'workflow_form_otpuska_sotrudnika'; // код персональной карточки юзера, с отпусками

const coef = 15.208;

//получить значение компонента в json asfData
const getValue = (asfData, cmpID) => asfData.data.find(x => x.id == cmpID);

const parseAsfValue = asfDataValue => {
  if(!asfDataValue) return null;
  if(asfDataValue.hasOwnProperty('key')) {
    return {
      type: asfDataValue.type,
      value: asfDataValue.value || '',
      key: asfDataValue.key || ''
    }
  } else if (asfDataValue.type == "check") {
    return {
      type: asfDataValue.type,
      values: asfDataValue.values || '',
      keys: asfDataValue.keys || ''
    }
  } else {
    return {
      type: asfDataValue.type,
      value: asfDataValue.value || ''
    }
  }
}

const parseAsfTable = asfTable => {
  const result = [];
  try {
    if(!asfTable || !asfTable.hasOwnProperty('data')) return result;

    const data = asfTable.data.filter(x => x.type != 'label');
    if(!data.length) return result;

    const tmpids = [];
    data.forEach(x => tmpids.push(x.id.slice(0, x.id.indexOf('-b'))));
    const ids = tmpids.uniq();

    let tbi =  data.slice(-1)[0].id;
    tbi = Number(tbi.slice(tbi.indexOf('-b') + 2));

    for(let i = 1; i <= tbi; i++) {
    	const item = {};
      ids.forEach(id => {
        const parseValue = parseAsfValue(getValue(asfTable, `${id}-b${i}`));
        if(parseValue) item[id] = parseValue;
      });
      result.push(item);
    }
    return result;
  } catch (err) {
    console.log(`ERROR [ parseAsfTable ]: ${err.message}`);
    return result;
  }
}

Array.prototype.uniq = function() {
  return this.filter(function(v, i, a){ return i == a.indexOf(v) });
}

const parseHolidayDict = holidayAsfData => {
  const table = parseAsfTable(getValue(holidayAsfData, "holiday_calendar_table"));

  if(!table.length) return null;

  const result = {
    workingDay: [],
    dayOff: [],
    holiday: []
  }

  table.forEach(item => {
    const {date, day_type} = item;
    if(date.hasOwnProperty('key')) {
      const dateValue = date.key.split(' ')[0];
      switch (day_type.key) {
        case '1': result.workingDay.push(dateValue); break;
        case '2': result.dayOff.push(dateValue); break;
        case '3': result.holiday.push(dateValue); break;
      }
    }
  });

  return result;
}

const getHolidayDict = async year => {
  let urlSearch = 'rest/api/registry/data_ext?registryCode=spravochnik_prazdnikov';
  urlSearch += `&field=holiday_calendar_year&condition=TEXT_EQUALS&value=${year}`;
  urlSearch += '&countInPart=1&loadData=false';

  const resp = await AS.FORMS.ApiUtils.simpleAsyncGet(urlSearch);

  if(resp.data.length) {
    const holidayAsfData = await AS.FORMS.ApiUtils.loadAsfData(resp.data[0].dataUUID);

    return parseHolidayDict(holidayAsfData);
  }

  return null;
}

// суммирует значениея столбца в таблице
const getSumRowTable = (table, cmp) => {
  let result = 0;
  if(table && table.hasOwnProperty('data')) {
    table.data.forEach(x => {
      if(x.id.substring(0, x.id.indexOf('-b')) == cmp && x.hasOwnProperty('value')) {
        result += Number(x.value) || 0;
      }
    });
  }
  return result;
}

// получение личной карточки
const getPersonalRecord = async (userID, cardCode) => {
  const personalrecord = await AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/personalrecord/forms/${userID}`);
  const userCard = personalrecord.find(x => x.formCode == cardCode);
  return userCard ? AS.FORMS.ApiUtils.loadAsfData(userCard['data-uuid']) : null;
}

const parsePersonalData = async () => {
  if(!appUser.getValue()) return;

  try {
    const userVacationsData = await getPersonalRecord(appUser.getValue()[0].personID, personalUserCardCodeVacations);
    if(!userVacationsData) throw new Error(`Не найдена личная карточка с кодом ${personalUserCardCodeVacations}`);

    const employment_date = getValue(userVacationsData, 'employment_date'); // дата приема на работу
    const TO = getSumRowTable(getValue(userVacationsData, 'labor_leave'), 'number_of_days'); // трудовой отпуск
    const OBS = getSumRowTable(getValue(userVacationsData, 'vacation_bez_zp'), 'number_of_days_bez_zp'); // Отпуск без содержания
    const OZ = getSumRowTable(getValue(userVacationsData, 'labor_back'), 'number_of_days'); // отзывы из отпуска

    const days = daysDifference(AS.FORMS.DateUtils.parseDate(employment_date.key), AS.FORMS.DateUtils.parseDate(appDS.getValue()));

    //Доступно дней
    const DK = Math.round(days / coef) - (TO + OBS - OZ);
    model.playerModel.getModelWithId('result').setValue(String(DK));

    if(Number(appDays.getValue()) <= DK) {
      model.playerModel.getModelWithId('is_ok').setValue('1');
    } else {
      model.playerModel.getModelWithId('is_ok').setValue('0');
    }

  } catch (e) {
    AS.SERVICES.showErrorMessage('Ошибка обработки персональной карточки');
    console.log('parsePersonalData ERROR', e.message);
  }
}

//дней в периоде
const daysDifference = (d1, d2) => Math.round((new Date(+d2).setHours(12) - new Date(+d1).setHours(12))/8.64e7) + 1;

//расчет календарных дней за выбранный период
const calcAndSetAppDays = async () => {
  const dateStart = AS.FORMS.DateUtils.parseDate(appDS.getValue());
  const dateFinish = AS.FORMS.DateUtils.parseDate(appDF.getValue());

  if(!dateStart) {
    appDays.setValue(null);
    appDF.setValue(null);
    view.playerView.getViewWithId('workflow_form_leave_application_ds').markInvalid();
    return false;
  }

  if(!dateFinish) {
    appDays.setValue(null);
    view.playerView.getViewWithId('workflow_form_leave_application_df').markInvalid();
    return false;
  }

  const typeDays = await getHolidayDict(dateStart.getFullYear());
  let days = daysDifference(dateStart, dateFinish);

  if(typeDays) {
    for(const tmpDate = new Date(dateStart); tmpDate <= dateFinish; tmpDate.setDate(tmpDate.getDate() + 1)) {
      const d = AS.FORMS.DateUtils.formatDate(tmpDate, AS.FORMS.DateUtils.DATE_INPUT_FORMAT);
      if(typeDays.holiday.includes(d)) days--;
    }
  }

  if(days < 0) {
    AS.SERVICES.showErrorMessage('Дата завершения отпуска не может быть меньше даты начала отпуска');
    appDays.setValue(null);
    appDF.setValue(null);
    view.playerView.getViewWithId('workflow_form_leave_application_df').markInvalid();
    return false;
  }

  view.playerView.getViewWithId('workflow_form_leave_application_ds').unmarkInvalid();
  view.playerView.getViewWithId('workflow_form_leave_application_df').unmarkInvalid();

  appDays.setValue(String(days));

  return true;
}

// Вешаем события когда форма в режиме редактирования
if(editable) {

  // События изменения даты
  appDS.on('valueChange', (_1, _2, value) => {
    if(!AS.FORMS.DateUtils.parseDate(value) || !AS.FORMS.DateUtils.parseDate(appDF.getValue())) return;
    if(!calcAndSetAppDays()) return;
    parsePersonalData();
  });
  appDF.on('valueChange', (_1, _2, value) => {
    if(!AS.FORMS.DateUtils.parseDate(value) || !AS.FORMS.DateUtils.parseDate(appDS.getValue())) return;
    if(!calcAndSetAppDays()) return;
    parsePersonalData();
  });

  // обработка ошибок (обязательные поля)
  appDS.getSpecialErrors = function() {
    if(!AS.FORMS.DateUtils.parseDate(appDS.getValue())) {
      return {id: appDS.asfProperty.id, errorCode: AS.FORMS.INPUT_ERROR_TYPE.wrongValue};
    }
  }
  appDF.getSpecialErrors = function() {
    if(!AS.FORMS.DateUtils.parseDate(appDF.getValue())) {
      return {id: appDF.asfProperty.id, errorCode: AS.FORMS.INPUT_ERROR_TYPE.wrongValue};
    } else if (AS.FORMS.DateUtils.parseDate(appDF.getValue()) < AS.FORMS.DateUtils.parseDate(appDS.getValue())) {
      return {id: appDF.asfProperty.id, errorCode: AS.FORMS.INPUT_ERROR_TYPE.wrongValue};
    }
  }
}
