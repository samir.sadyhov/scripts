const holidays = [
  {name: 'Новый год', day: '1', month: {key: '1', value: 'Январь'}, color: {key: '#FF0000', value: 'Красный'}},
  {name: 'Новый год', day: '2', month: {key: '1', value: 'Январь'}, color: {key: '#FF0000', value: 'Красный'}},
  {name: 'Рождество Христово', day: '7', month: {key: '1', value: 'Январь'}, color: {key: '#FF0000', value: 'Красный'}},
  {name: 'Международный женский день', day: '8', month: {key: '3', value: 'Март'}, color: {key: '#FF0000', value: 'Красный'}},
  {name: 'Наурыз мейрамы', day: '21', month: {key: '3', value: 'Март'}, color: {key: '#FF0000', value: 'Красный'}},
  {name: 'Наурыз мейрамы', day: '22', month: {key: '3', value: 'Март'}, color: {key: '#FF0000', value: 'Красный'}},
  {name: 'Наурыз мейрамы', day: '23', month: {key: '3', value: 'Март'}, color: {key: '#FF0000', value: 'Красный'}},
  {name: 'Праздник единства народа Казахстана', day: '1', month: {key: '5', value: 'Май'}, color: {key: '#FF0000', value: 'Красный'}},
  {name: 'День Защитника Отечества', day: '7', month: {key: '5', value: 'Май'}, color: {key: '#FF0000', value: 'Красный'}},
  {name: 'День Победы', day: '9', month: {key: '5', value: 'Май'}, color: {key: '#FF0000', value: 'Красный'}},
  {name: 'День столицы', day: '6', month: {key: '7', value: 'Июль'}, color: {key: '#FF0000', value: 'Красный'}},
  {name: 'День Конституции', day: '30', month: {key: '8', value: 'Август'}, color: {key: '#FF0000', value: 'Красный'}},
  {name: 'День Первого Президента', day: '1', month: {key: '12', value: 'Декабрь'}, color: {key: '#FF0000', value: 'Красный'}},
  {name: 'День независимости Республики Казахстан', day: '16', month: {key: '12', value: 'Декабрь'}, color: {key: '#FF0000', value: 'Красный'}},
  {name: 'День независимости Республики Казахстан', day: '17', month: {key: '12', value: 'Декабрь'}, color: {key: '#FF0000', value: 'Красный'}}
];

const clearTable = (table, cb) => {
  for(let i = 0; i < table.modelBlocks.length; i++) table.removeRow(i);
  setTimeout(() => {
    table.modelBlocks.length > 0 ? clearTable(table, cb) : cb();
  }, 0);
}

const getHolidayDate = (holiday, year) => {
  return `${year}-${('0' + holiday.month.key).slice(-2)}-${('0' + holiday.day).slice(-2)}`;
}

const fillTableHolidays = (year) => {
  const tableModel = model.playerModel.getModelWithId('holiday_calendar_table');

  clearTable(tableModel, () => {
    holidays.forEach(holiday => {
      const row = tableModel.createRow();
      model.playerModel.getModelWithId('description', tableModel.asfProperty.id, row.tableBlockIndex).setValue(holiday.name);
      model.playerModel.getModelWithId('date', tableModel.asfProperty.id, row.tableBlockIndex).setValue(getHolidayDate(holiday, year));
      model.playerModel.getModelWithId('day_type', tableModel.asfProperty.id, row.tableBlockIndex).setValue('3');
    });
  });

}

const renderButton = () => {
  $('.button_fill_holidays').remove();

  const button = $('<button>', {class: 'button_fill_holidays'});
  button.text('Заполнить значениями по умолчанию');
  button.on('click', e => {
    e.preventDefault();
    e.target.blur();
    fillTableHolidays(model.playerModel.getModelWithId('holiday_calendar_year').getAsfData().value);
  });

  view.container.append(button);
}

if(editable) renderButton();
