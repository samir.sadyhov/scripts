const jsonFieldModel = model.playerModel.getModelWithId(model.schedule_json);
const tableContainer = view.playerView.getViewWithId(model.schedule_container);
const yearFieldModel = model.playerModel.getModelWithId(model.year_field);

const getCountDay = monthDays => {
  let result = 0;

  for(const day in monthDays) {
    let value = monthDays[day];
    if(value.indexOf('/') == -1) {
      if(value != '' && !isNaN(Number(value))) result++;
    } else {
      result++;
    }
  }

  return result;
}

const getSumHours = monthDays => {
  const workmodeSumHoursModel = model.playerModel.getModelWithId('workflow_form_workmode_sum_hours');
  const isSumHours = workmodeSumHoursModel.getAsfData().values[0] == '1';

  let result = 0;

  for(const day in monthDays) {
    let value = monthDays[day];
    if(value.indexOf('/') == -1) {
      if(!isNaN(Number(value))) result += Number(value);
    } else {
      value = value.split('/');
      const d = Number(value[0].replace(/[^\d]/g, ''));
      const n = Number(value[1].replace(/[^\d]/g, ''));
      isSumHours ? result += (d + n) : result += d;
    }
  }

  return result;
}

const setDayOff = month => {
  if(!month.hasOwnProperty('dayOffMonth')) month.dayOffMonth = 0;
  if(!month.hasOwnProperty('dayOffHalfMonth')) month.dayOffHalfMonth = 0;
  month.dayOffMonth = 0;
  month.dayOffHalfMonth = 0;

  for(const day in month.days) {
    const value = month.days[day];
    if(value == 'В') {
      month.dayOffMonth++;
      if(day < 16) month.dayOffHalfMonth++;
    }
  }
}

const setAvgValues = calendar => {
  const avgDaysModel = model.playerModel.getModelWithId('workflow_form_workmode_avg_days');
  const avgHoursdModel = model.playerModel.getModelWithId('workflow_form_workmode_avg_hours');
  let d = 0;
  let h = 0;
  for(const month in calendar) {
    const {countDays, sumHours} = calendar[month];
    d += countDays;
    h += sumHours;
  }

  if(avgDaysModel) avgDaysModel.setValue(String((d / 12).toFixed(3)));
  if(avgHoursdModel) avgHoursdModel.setValue(String((h / 12).toFixed(3)));
}

const setHolidays = (scheduleYear, typeDays, calendar) => {
  for(const month in calendar) {
    if(!calendar[month].hasOwnProperty('holidaysMonth')) calendar[month].holidaysMonth = 0;
    if(!calendar[month].hasOwnProperty('holidaysHalfMonth')) calendar[month].holidaysHalfMonth = 0;
    calendar[month].holidaysMonth = 0;
    calendar[month].holidaysHalfMonth = 0;

    for(const day in calendar[month]['days']) {
      const formatDate = `${scheduleYear}-${('0' + month).slice(-2)}-${('0' + day).slice(-2)}`;
      const holiday = typeDays.holiday.includes(formatDate);
      if(holiday) {
        calendar[month].holidaysMonth++;
        if(day < 16) calendar[month].holidaysHalfMonth++;
      }
    }
  }
}

const getLastDayOfMonth = (year, month) => new Date(year, month, 0).getDate();

const getTmpCalendar = year => {
  const calendar = {};

  for(let month = 1; month < 13; month++) {
    calendar[month] = {
      countDays: 0,
      sumHours: 0,
      holidaysMonth: 0,
      holidaysHalfMonth: 0,
      dayOffMonth: 0,
      dayOffHalfMonth: 0,
      days: {}
    };
    const lastDay = getLastDayOfMonth(year, month);
    for(let day = 1; day <= lastDay; day++) {
      calendar[month]['days'][day] = '';
    }
  }

  return calendar;
}

const getValue = (asfData, cmp) => asfData.data.find(x => x.id == cmp);

const parseAsfValue = asfDataValue => {
  if(!asfDataValue) return null;
  if(asfDataValue.hasOwnProperty('key')) {
    return {
      type: asfDataValue.type,
      value: asfDataValue.value || '',
      key: asfDataValue.key || ''
    }
  } else if (asfDataValue.type == "check") {
    return {
      type: asfDataValue.type,
      values: asfDataValue.values || '',
      keys: asfDataValue.keys || ''
    }
  } else {
    return {
      type: asfDataValue.type,
      value: asfDataValue.value || ''
    }
  }
}

const parseAsfTable = asfTable => {
  const result = [];
  try {
    if(!asfTable || !asfTable.hasOwnProperty('data')) return result;

    const data = asfTable.data.filter(x => x.type != 'label');
    if(!data.length) return result;

    const tmpids = [];
    data.forEach(x => tmpids.push(x.id.slice(0, x.id.indexOf('-b'))));
    const ids = tmpids.uniq();

    let tbi =  data.slice(-1)[0].id;
    tbi = Number(tbi.slice(tbi.indexOf('-b') + 2));

    for(let i = 1; i <= tbi; i++) {
    	const item = {};
      ids.forEach(id => {
        const parseValue = parseAsfValue(getValue(asfTable, `${id}-b${i}`));
        if(parseValue) item[id] = parseValue;
      });
      result.push(item);
    }
    return result;
  } catch (err) {
    console.log(`ERROR [ parseAsfTable ]: ${err.message}`);
    return result;
  }
}

const parseHolidayDict = holidayAsfData => {
  const table = parseAsfTable(getValue(holidayAsfData, "holiday_calendar_table"));

  if(!table.length) return null;

  const result = {
    workingDay: [],
    dayOff: [],
    holiday: []
  }

  table.forEach(item => {
    const {date, day_type} = item;
    if(date.hasOwnProperty('key')) {
      const dateValue = date.key.split(' ')[0];
      switch (day_type.key) {
        case '1': result.workingDay.push(dateValue); break;
        case '2': result.dayOff.push(dateValue); break;
        case '3': result.holiday.push(dateValue); break;
      }
    }
  });

  return result;
}

const getHolidayDict = async year => {
  let urlSearch = 'rest/api/registry/data_ext?registryCode=spravochnik_prazdnikov';
  urlSearch += `&field=holiday_calendar_year&condition=TEXT_EQUALS&value=${year}`;
  urlSearch += '&countInPart=1&loadData=false';

  const resp = await AS.FORMS.ApiUtils.simpleAsyncGet(urlSearch);

  if(resp.data.length) {
    const holidayAsfData = await AS.FORMS.ApiUtils.loadAsfData(resp.data[0].dataUUID);

    return parseHolidayDict(holidayAsfData);
  }

  return null;
}

const getUserWorkTimeMode = () => {
  const result = {};

  for(let i = 1; i <= 7; i++) {
    const dayModel = model.playerModel.getModelWithId(`workflow_form_workmode_day${i}`);
    result[`day${i}`] = dayModel ? dayModel.getValue() : '8';
  }

  return result;
}

Array.prototype.uniq = function() {
  return this.filter(function(v, i, a){ return i == a.indexOf(v) });
}

const RenderScheduleTable = class {
  constructor(calendar){
    this.calendar = calendar;
    this.init();
  }

  createBody(){
    this.tBody.empty();

    for(const m in this.calendar) {
      const tr = $('<tr>');
      const monthName = AS.FORMS.DateUtils.getMonthName(Number(m));
      const TDcountDays = $(`<td>${this.calendar[m].countDays}</td>`);
      const TDsumHours = $(`<td>${this.calendar[m].sumHours}</td>`);
      tr.append(
        `<td>${monthName}</td>`,
        TDcountDays,
        TDsumHours
      );

      for(let d = 1; d <= 31; d++) {
        const td = $('<td>', {class: 'schedule_day'});
        if(this.calendar[m]['days'].hasOwnProperty(d)) {
          const input = $('<input>', {class: 'schedule_day_input'});
          const label = $('<span>', {class: 'schedule_day_value'});

          let value = this.calendar[m]['days'][d];
          if(value == '' || value == 'В') {
            td.addClass('weekend');
          } else if (value == 'П') {
            td.addClass('holiday');
          }

          label.text(value);
          input.val(value);

          td.append(input, label);
          td.attr('date', `${this.scheduleYear}-${('0' + m).slice(-2)}-${('0' + d).slice(-2)}`);

          label.on('dblclick', e => {
            if(!editable) return;

            e.preventDefault();
            input.val(value);
            input.show();
            label.hide();

            input.on('blur', () => {
              label.show();
              input.hide();
            });

            input.on('keydown', e => {
              if(e.which === 13) {
                value = input.val();
                label.text(value);
                this.calendar[m]['days'][d] = value;

                label.show();
                input.hide();
                input.off();

                td.removeClass('weekend');
                td.removeClass('holiday');
                if(value == '' || value == 'В') {
                  td.addClass('weekend');
                } else if (value == 'П') {
                  td.addClass('holiday');
                }

                this.calendar[m].countDays = getCountDay(this.calendar[m]['days']);
                this.calendar[m].sumHours = getSumHours(this.calendar[m]['days']);
                setDayOff(this.calendar[m]);
                setAvgValues(this.calendar);

                TDcountDays.text(this.calendar[m].countDays);
                TDsumHours.text(this.calendar[m].sumHours);

                jsonFieldModel.setValue(JSON.stringify(this.calendar));
              }
            });

            input.focus();
          });

        } else {
          td.addClass('no_day');
        }
        tr.append(td);
      }

      tr.on('click', e => {
        this.tBody.find('tr').removeAttr('selected');
        tr.attr('selected', true);
      });

      this.tBody.append(tr);
    }
  }

  createHeader(){
    this.colgroup.empty();
    this.tHead.empty();
    const tr = $('<tr>');

    this.colgroup.append(`<col style="width: 100px">`, `<col style="width: 70px">`, `<col style="width: 70px">`);

    tr.append(
      `<th>${i18n.tr('Месяц')}</th>`,
      `<th>${i18n.tr('Дней')}</th>`,
      `<th>${i18n.tr('Часов')}</th>`
    );

    for(let d = 1; d <= 31; d++) {
      this.colgroup.append(`<col>`);
      tr.append(`<th>${d}</th>`);
    }

    this.tHead.append(tr);
    this.createBody();
  }

  render(){
    this.table = $('<table>', {class: "schedule_table"});
    this.colgroup = $('<colgroup>');
    this.tHead = $('<thead>');
    this.tBody = $('<tbody>');

    this.table.append(this.colgroup, this.tHead, this.tBody);
    tableContainer.container.empty().append(this.table);

    this.createHeader();
  }

  init(){
    this.scheduleYear = yearFieldModel.getAsfData()?.value || new Date().getFullYear();
    this.render();
  }
}

//суббота или воскресенье + проверка рабочего графика юзера (пятидневка/шестидневка)
const isDayOff = (dayNumber, workTimeMode) => {
  if(workTimeMode) {
    if(!isNaN(Number(workTimeMode.day6))) {
      return dayNumber > 6 ? true : false;
    } else {
      return dayNumber > 5 ? true : false;
    }
  } else {
    return dayNumber > 5 ? true : false;
  }
}

const fillScheduleWeekMode = async (scheduleYear, typeDays, calendar) => {
  AS.SERVICES.showWaitWindow();
  try {
    const workTimeMode = getUserWorkTimeMode();

    for(const month in calendar) {
      for(const day in calendar[month]['days']) {
        const formatDate = `${scheduleYear}-${('0' + month).slice(-2)}-${('0' + day).slice(-2)}`;
        const date = new Date(formatDate);
        const dayNumber = date.getDay() || 7;
        const workingDay = typeDays.workingDay.includes(formatDate);
        const dayOff = typeDays.dayOff.includes(formatDate);
        const holiday = typeDays.holiday.includes(formatDate);
        const cDayOff = isDayOff(dayNumber, workTimeMode);

        let value = workTimeMode[`day${dayNumber}`];

        if(!workingDay && !dayOff && !holiday) {
          if(cDayOff && !isNaN(Number(value))) value = 'В';
        } else if (cDayOff && workingDay) {
          //ставим время понедельника, а то хз откуда еще брать значение
          if(isNaN(Number(value))) value = workTimeMode[`day1`];
        } else if (holiday && !isNaN(Number(value))) {
          value = 'П';
        } else if (dayOff && !isNaN(Number(value))) {
          value = 'В';
        } else {
          if(cDayOff && !isNaN(Number(value))) value = 'В';
        }

        if(holiday) value = 'П';

        calendar[month]['days'][day] = value;
      }

      calendar[month].countDays = getCountDay(calendar[month]['days']);
      calendar[month].sumHours = getSumHours(calendar[month]['days']);
      setDayOff(calendar[month]);
    }

    jsonFieldModel.setValue(JSON.stringify(calendar));
    new RenderScheduleTable(calendar);
    setAvgValues(calendar);

    AS.SERVICES.hideWaitWindow();
  } catch (e) {
    AS.SERVICES.hideWaitWindow();
    AS.SERVICES.showErrorMessage(i18n.tr(e.message));
  }
}

const fillScheduleCustomMode = async (scheduleYear, typeDays, calendar) => {
  AS.SERVICES.showWaitWindow();
  try {
    //Праздники - нерабочие дни (1 да, 0 нет)
    const workmodeHolidaysModel = model.playerModel.getModelWithId('workflow_form_workmode_holidays');
    //Учитывать выходные (для произвольного графика)
    // 1 - Не учитывать выходные
    // 2 - Учитывать выходной воскресенье
    // 3 - Учитывать выходные (суббота, воскресенье)
    const workmodeWeekendModel = model.playerModel.getModelWithId('workflow_form_workmode_weekend');
    //Вид времени (1 явка, 2 ночные)
    const workmodeTypeModel = model.playerModel.getModelWithId('workflow_form_workmode_type');
    // Дата отсчета день (1...31, workflow_form_workmode_day1....workflow_form_workmode_day31)
    const workmodeStartdayModel = model.playerModel.getModelWithId('workflow_form_workmode_startday');
    // Дата отсчета месяц (01....12)
    const workmodeStartmonthModel = model.playerModel.getModelWithId('workflow_form_workmode_startmonth');
    //Суммированный учет рабочего времени
    const workmodeSumHoursModel = model.playerModel.getModelWithId('workflow_form_workmode_sum_hours');
    //Шаблон расписания работы (дин.таблица)
    const workmodeTablePatternModel = model.playerModel.getModelWithId('workflow_form_workmode_table_pattern');
    const workmodeTable = parseAsfTable(workmodeTablePatternModel.getAsfData()[0]);

    const workmodeType = workmodeTypeModel.getAsfData().values.filter(x => x);
    const isHoliday = workmodeHolidaysModel.getAsfData()?.key == '1';
    const isWeekendType = Number(workmodeWeekendModel.getAsfData()?.value || 1);
    const isSumHours = workmodeSumHoursModel.getAsfData().values[0] == '1';
    const startDate = `${scheduleYear}-${workmodeStartmonthModel.getAsfData().key}-${('0' + workmodeStartdayModel.getAsfData().value).slice(-2)}`;

    const workMode = {
      dayHours: 0,
      nightHours: 0,
      allHours: 0,
      d: [],
      n: []
    }
    workmodeTable.forEach(row => {
      const {workflow_form_workmode_day, workflow_form_workmode_night} = row;
      workMode.dayHours += Number(workflow_form_workmode_day?.key || 0);
      workMode.nightHours += Number(workflow_form_workmode_night?.key || 0);

      workMode.d.push(workflow_form_workmode_day?.key || '');
      workMode.n.push(workflow_form_workmode_night?.key || '');
    });
    workMode.allHours = workMode.dayHours + workMode.nightHours;

    if(isSumHours && workMode.allHours > 24) throw new Error('Недопустимый диапазон рабочих часов, проверьте шаблон расписания работы');

    let indexDay = 0;

    const checkWorkMode = () => {
      if (workmodeType.includes('1') && workmodeType.includes('2')) {
        if(workMode.d[indexDay] != '' && workMode.n[indexDay] != '') return true;
      } else if (!workmodeType.includes('1') && workmodeType.includes('2')) {
        if(workMode.n[indexDay] != '') return true;
      } else if (workmodeType.includes('1') && !workmodeType.includes('2')) {
        if(workMode.d[indexDay] != '') return true;
      } else {
        return false
      }
    }

    for(const month in calendar) {
      for(const day in calendar[month]['days']) {
        let value = '';
        const formatDate = `${scheduleYear}-${('0' + month).slice(-2)}-${('0' + day).slice(-2)}`;
        const date = new Date(formatDate);
        const dayNumber = date.getDay() || 7;
        const workingDay = typeDays.workingDay.includes(formatDate);
        const dayOff = typeDays.dayOff.includes(formatDate);
        const holiday = typeDays.holiday.includes(formatDate);

        if(formatDate >= startDate) {

          if(indexDay > workMode.d.length - 1) indexDay = 0;

          const dayValue = workMode.d[indexDay];
          const nightValue = workMode.n[indexDay];

          if(checkWorkMode()) {
            if (workmodeType.includes('1') && workmodeType.includes('2')) {
              value = `Я${dayValue} / Н${nightValue}`;
            } else if (!workmodeType.includes('1') && workmodeType.includes('2')) {
              value = `Н${nightValue}`;
            } else {
              value = dayValue;
            }
          } else {
            value = 'В';
            if(isHoliday && dayOff && !holiday) indexDay++;
            if(indexDay > workMode.d.length - 1) indexDay = 0;
          }

          const checkHoliday = () => {
            if(isHoliday && (holiday || dayOff)) {
              if(holiday) value = 'П';
            } else {
              indexDay++;
            }
          }

          switch (isWeekendType) {
            // учитывать праздники и не учитывать выходные
            case 1: checkHoliday(); break;
            // Учитывать выходной воскресенье
            case 2: dayNumber == 7  ? value = 'В' : checkHoliday(); break;
            // Учитывать выходные (суббота, воскресенье)
            case 3: [6, 7].includes(dayNumber) ? value = 'В' : checkHoliday(); break;
          }

        }

        calendar[month]['days'][day] = value;
      }

      calendar[month].countDays = getCountDay(calendar[month]['days']);
      calendar[month].sumHours = getSumHours(calendar[month]['days']);
      setDayOff(calendar[month]);
    }

    jsonFieldModel.setValue(JSON.stringify(calendar));
    new RenderScheduleTable(calendar);
    setAvgValues(calendar);

    AS.SERVICES.hideWaitWindow();
  } catch (e) {
    AS.SERVICES.hideWaitWindow();
    AS.SERVICES.showErrorMessage(i18n.tr(e.message));
  }
}

const fillSchedule = async () => {
  try {
    const calendar = JSON.parse(jsonFieldModel.getValue());
    const workmodeScheduleModel = model.playerModel.getModelWithId('workflow_form_workmode_schedule');
    if(!workmodeScheduleModel || !workmodeScheduleModel.getValue()) throw new Error('Не выбран тип графика');

    const workmodeSchedule = workmodeScheduleModel.getAsfData()?.value;
    const scheduleYear = yearFieldModel.getAsfData()?.value || new Date().getFullYear();

    const typeDays = await getHolidayDict(scheduleYear);
    if(!typeDays) throw new Error(`Не заполнен справочник праздников за ${scheduleYear} год`);

    setHolidays(scheduleYear, typeDays, calendar);

    switch (workmodeSchedule) {
      case '1': fillScheduleWeekMode(scheduleYear, typeDays, calendar); break;
      case '2': fillScheduleCustomMode(scheduleYear, typeDays, calendar); break;
    }
  } catch (e) {
    AS.SERVICES.showErrorMessage(i18n.tr(e.message));
  }
}

const renderButtons = () => {
  view.container.find('.schedule_action_button').remove();

  const buttonFill = $('<button>', {class: 'schedule_action_button'});
  buttonFill.text(i18n.tr('Заполнить'));
  buttonFill.on('click', e => {
    e.preventDefault();
    e.target.blur();
    fillSchedule();
  });

  view.container.append(buttonFill);
}

tableContainer.container.empty();
tableContainer.container.addClass('schedule_container');

if(jsonFieldModel.getValue()) new RenderScheduleTable(JSON.parse(jsonFieldModel.getValue()));

if(editable) {
  const scheduleYear = yearFieldModel.getAsfData()?.value || new Date().getFullYear();
  if(!jsonFieldModel.getValue()) {
    const calendar = getTmpCalendar(scheduleYear);
    jsonFieldModel.setValue(JSON.stringify(calendar));
    new RenderScheduleTable(calendar);
  }
  renderButtons();
} else {
  view.container.find('.schedule_action_button').remove();
}
