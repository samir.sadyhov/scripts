const printReport = (defaultName, reportUrl) => {
  AS.SERVICES.showWaitWindow();
  try {
    let xhr = new XMLHttpRequest();
    xhr.open('POST', reportUrl, true);
    xhr.setRequestHeader("Authorization", "Basic " + btoa(unescape(encodeURIComponent(AS.OPTIONS.login + ":" + AS.OPTIONS.password))));
    xhr.responseType = 'arraybuffer';
    xhr.onload = function () {
      if (this.status === 200) {
        let filename = defaultName;
        let type = xhr.getResponseHeader('Content-Type');
        let blob = new Blob([this.response], {type: type});
        if (typeof window.navigator.msSaveBlob !== 'undefined') {
          window.navigator.msSaveBlob(blob, filename);
        } else {
          let URL = window.URL || window.webkitURL;
          let downloadUrl = URL.createObjectURL(blob);
          if (filename) {
            let a = document.createElement("a");
            if (typeof a.download === 'undefined') {
              window.location = downloadUrl;
            } else {
              a.href = downloadUrl;
              a.download = filename;
              document.body.appendChild(a);
              a.click();
            }
          } else {
            window.location = downloadUrl;
          }
          setTimeout(() => {
            URL.revokeObjectURL(downloadUrl);
          }, 100);
        }
        AS.SERVICES.hideWaitWindow();
      } else {
        AS.SERVICES.hideWaitWindow();
        console.log(this.status, this);
      }
    };
    xhr.send();
  } catch (e) {
    AS.SERVICES.hideWaitWindow();
    console.log(e);
  }
}

const timesheetPrint = report => {
  const {defaultName, reportID} = report;
  const reportUrl = `${window.location.origin}/Synergy/rest/api/report/do?reportID=${reportID}&asfDataID=${model.playerModel.asfDataId}`;
  printReport(defaultName, reportUrl);
}

const renderButton = report => {
  $('.button_print_result').remove();

  const button = $('<button>', {class: 'button_print_result'});

  button.text('Печать');

  button.on('click', e => {
    e.preventDefault();
    e.target.blur();
    timesheetPrint(report);
  });

  view.container.append(button);
}

const initTimesheetPrint = async () => {
  try {
    const reportList = await AS.FORMS.ApiUtils.simpleAsyncGet('rest/api/report/list');
    const report = reportList.find(x => x.code == model.reportCode);
    if(!report) return;
    renderButton(report);
  } catch (e) {
    console.log('ERROR init report timesheet', e.message);
  }
}

if(!editable) initTimesheetPrint();
