/* пример настроек на комопненте
model.FLOZEN_COLUMNS_DATA = {
  tableId: 'table',
  cmpIds: ["number", "user", "t_number"],
  maxHeight: 600
};
*/


const FROZEN_COLUMNS = {

  createDiv: function(table, headerDiv, tdstart, tdend, sign){
    const div = jQuery("<div>");
    div.html(sign);
    if(tdend) {
      div.css("width", (tdend.offset().left - tdstart.offset().left - 1 )+"px");
    } else {
      div.css("width", tdstart[0].clientWidth+"px");
    }
    div.css("position", "absolute");

    const left = Math.max(0, tdstart.offset().left-table.offset().left);

    div.css({
      "left": left,
      "top": "0",
      "background-color": "#ddeeff",
      "text-align": "center",
      "border": "solid 1px #606060"
    });

    headerDiv.append(div);
  },

  freezeHeaders: function(playerModel, playerView, tableId){
    const tableModel = playerModel.getModelWithId(tableId);
    const tableView = playerView.getViewWithId(tableId);

    const headerDiv = $("<div>");
    headerDiv.css({
      "background-color": "#ddeeff",
      "position": "absolute",
      "height": "2px",
      "top": "0"
    });

    tableView.container.append(headerDiv);
    tableView.frozenHeaderDiv = headerDiv;

    const paintHeader = tableView => {
      tableView.frozenHeaderDiv.children().remove();

      const table = $(tableView.container.children("table").first());
      const tableRow = $(table.children("tbody").first().children("tr")[0]);

      const tds = tableRow.children("td");
      for(let i = 0; i < tds.length; i++) {
        const td = $(tds[i]);
        let nextTd = null;
        if(i+1 < tds.length) nextTd = $(tds[i+1]);

        const div = FROZEN_COLUMNS.createCopyDiv(td, nextTd, table);
        if(div) {
          tableView.frozenHeaderDiv.append(div);
          tableView.frozenHeaderDiv.css("height", td.outerHeight() + "px");
        }
      }
      tableView.frozenHeaderDiv.css("width", table.outerWidth(true)+"px");
    }

    tableModel.playerModel.on("dataLoad", () => paintHeader(tableView));

    tableModel.on("repaint", () => {
      if(tableModel.playerModel.loading) return;
      setTimeout(function(){
        paintHeader(tableView);
      }, 100);
    });

    setTimeout(() => {
      paintHeader(tableView);
    }, 100);
  },

  createCopyDiv: function(td, nextTd, table){
    const div = jQuery("<div>");
    div.html(td.html());

    let width = td[0].clientWidth;

    if(nextTd && $(nextTd).is(':visible')) width = (nextTd.offset().left - td.offset().left - 1);

    if(!$(td).is(':visible') || !width || width == 0 || width < 10) return null;

    const left = Math.max(0,  (td.offset().left - table.offset().left));

    div.css({
      "width": width+"px",
      "text-align": (td.outerHeight()-1) + "px",
      "position": "absolute",
      "left": (td.offset().left - table.offset().left) + "px",
      "height": (td.outerHeight()-1) + "px",
      "border": "solid 1px #606060"
    });

    const content = div.children("div");
    for(let i = 0; i <= content.length; i++) {
      $(content[i]).css({
        // "bottom": ((content.length - 1 - i)*20)+"px",
        "position": "absolute"
      });
    }

    return div;
  },

  freezeColumns: function(playerModel, playerView, tableId, cmpIds, maxTableHeight){
    const tableModel = playerModel.getModelWithId(tableId);

    if(!tableModel.initedFrozen) {
      tableModel.on("tableRowAdd", function () {
        const tableView = playerView.getViewWithId(tableId);
        const table = $(tableView.container.children("table").first());
        const tableRows = table.children("tbody").first().children("tr");

        for (let i = 1; i < tableRows.length; i++) {
          const tds = $(tableRows[i]).children("td");
          for (let j = 0; j < cmpIds.length; j++) {
            const td = $(tds[j]);
            td.css({
              "min-width": `${td.width()}px`,
              "max-width": `${td.width()}px`
            });
          }
        }

        tableView.repaintFrozenDiv = true;
      });

      tableModel.on("tableRowDelete", () => tableView.repaintFrozenDiv = true);
      tableModel.initedFrozen = true;
    }

    const tableView = playerView.getViewWithId(tableId);
    tableView.container.css({
      "overflow": "auto",
      "position": "relative",
      "max-height": `${maxTableHeight}px`
    });
    if(playerView.container.width()){
      tableView.container.css("width", playerView.container.width()+"px");
    }

    const div = $("<div>");
    div.css({
      "background-color": "#ddeeff",
      "position": "absolute",
      "height": "2px",
      "top": "0",
      "display": "none",
      "opacity": "0.9"
    });

    tableView.container.append(div);
    tableView.frozenDiv = div;
  },

  checkColumnScroll : function(tableView, tableModel , tableId, cmpIds){
    const table = $(tableView.container.children("table").first());
    const tableRows = table.children("tbody").first().children("tr");

    tableView.frozenWidth = 0;

    let tds = $(tableRows[0]).children("td");
    for(let j = 0; j<cmpIds.length; j++) {
      tableView.frozenWidth += $(tds[j]).width();
    }

    tableView.frozenDiv.css({
      "left": tableView.container[0].scrollLeft,
      "width": `${tableView.frozenWidth}px`
    });

    if(tableView.container[0].scrollLeft < tableView.frozenWidth-10) {
      tableView.frozenDiv.fadeOut(200);
    } else {
      tableView.frozenDiv.fadeIn(300);
    }

    if(tableView.repaintFrozenDiv) {
      tableView.frozenDiv.children().remove();
      let top = 0;
      const divTop = $($(tableRows[0]).children("td")[0]).outerHeight();
      for (let i = 1; i < tableRows.length; i++) {
        tds = $(tableRows[i]).children("td");

        const childDiv = $("<div>");
        childDiv.css({
          "height": $(tds[0]).outerHeight() + "px",
          "width": (tableView.frozenWidth-10)+"px",
          "top": top + "px",
          "padding": "8px",
          "box-sizing": "border-box",
          "overflow": "hidden",
          "text-overflow": "ellipsis",
          "white-space": "nowrap"
        });

        let textContent = "";

        tableModel.modelBlocks.forEach(modelBlock => {
          cmpIds.forEach((cmpId, index) => {
            let v = tableView.getViewWithId(cmpId, tableId, modelBlock[0].asfProperty.tableBlockIndex);
            if (v.container[0] == $(tds[index]).children("div").first()[0]) {
              if (!textContent.isEmpty()) textContent += " - ";
              if(v.model.getTextValue()) textContent += v.model.getTextValue();
            }
          });
        });

        childDiv.html(textContent);
        tableView.frozenDiv.append(childDiv);
        top += $(tds[0]).outerHeight();
      }

      tableView.frozenDiv.css({
        "height": `${top}px`,
        "top": `${divTop}px`
      });

      tableView.repaintFrozenDiv = false;
    }
  },

  checkFrozenHeader:  function(tableView, tableModel){
    tableView.frozenHeaderDiv.css("top", tableView.container[0].scrollTop);
  },

  addFreezingScroll: function(playerModel, playerView, tableId, cmpIds){
    try {
      const tableModel = playerModel.getModelWithId(tableId);
      const tableView = playerView.getViewWithId(tableId);

      tableView.repaintFrozenDiv = true;

      tableView.container.scroll(evt => {
        FROZEN_COLUMNS.checkColumnScroll(tableView, tableModel, tableId, cmpIds);
        FROZEN_COLUMNS.checkFrozenHeader(tableView, tableModel);
      });

    } catch (e) {
      console.log(e);
    }
  },

  addListenersForFrozenFields: function(playerModel, playerView, tableId, cmpIds){
    const tableModel = playerModel.getModelWithId(tableId);

    if(tableModel.frozenChangesInited) return;

    tableModel.on("tableRowAdd", (avt, model, modelBlock) => {
      if(model.playerModel.loading) return;

      const blockIndex = modelBlock.tableBlockIndex;
      cmpIds.forEach(cmpId => {
        const cmpModel = tableModel.getModelWithId(cmpId, tableId, blockIndex);
        cmpModel.on("valueChange", () => {
          const tableView = playerView.getViewWithId(tableId);
          tableView.repaintFrozenDiv = true;
        });
      });

    });

    tableModel.frozenChangesInited = true;
  }

};

const initFrozen = () => {
  const { FLOZEN_COLUMNS_DATA, playerModel } = model;
  const { playerView } = view;

  if(!FLOZEN_COLUMNS_DATA) return;

  const {tableId, cmpIds, maxHeight} = FLOZEN_COLUMNS_DATA;

  FROZEN_COLUMNS.freezeColumns(playerModel, playerView, tableId, cmpIds, maxHeight);
  FROZEN_COLUMNS.freezeHeaders(playerModel, playerView, tableId);
  FROZEN_COLUMNS.addFreezingScroll(playerModel, playerView, tableId, cmpIds);
  FROZEN_COLUMNS.addListenersForFrozenFields(playerModel, playerView, tableId, cmpIds);
}

initFrozen();
