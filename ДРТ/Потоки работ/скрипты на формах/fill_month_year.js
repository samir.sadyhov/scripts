const fillMonthYear = async () => {
  const {playerModel} = model;
  const documentID = await AS.FORMS.ApiUtils.getDocumentIdentifier(playerModel.asfDataId);
  const processes = await AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/workflow/get_execution_process?documentID=${documentID}`);

  if(!processes.length) {
    const currentDate = new Date();
    const year = String(currentDate.getFullYear());
    const month = ('0' + (currentDate.getMonth() + 1)).slice(-2);
    const modelMonth = playerModel.getModelWithId('month');
    const modelYear = playerModel.getModelWithId('year');

    if(modelYear) modelYear.setValue(year);
    if(modelMonth) modelMonth.setValue(month);
  }

}

if(editable) {
  setTimeout(() => {
    fillMonthYear();
  }, 100);
}
