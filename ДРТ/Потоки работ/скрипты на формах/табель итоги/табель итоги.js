const getValue = (asfData, cmp) => asfData.data.find(x => x.id == cmp);

const parseAsfValue = asfDataValue => {
  if(!asfDataValue) return null;
  if(asfDataValue.hasOwnProperty('key')) {
    return {
      type: asfDataValue.type,
      value: asfDataValue.value || '',
      key: asfDataValue.key || ''
    }
  } else if (asfDataValue.type == "check") {
    return {
      type: asfDataValue.type,
      values: asfDataValue.values || '',
      keys: asfDataValue.keys || ''
    }
  } else {
    return {
      type: asfDataValue.type,
      value: asfDataValue.value || ''
    }
  }
}

const parseAsfTable = asfTable => {
  const result = [];
  try {
    if(!asfTable || !asfTable.hasOwnProperty('data')) return result;

    const data = asfTable.data.filter(x => x.type != 'label');
    if(!data.length) return result;

    const tmpids = [];
    data.forEach(x => tmpids.push(x.id.slice(0, x.id.indexOf('-b'))));
    const ids = tmpids.uniq();

    let tbi =  data.slice(-1)[0].id;
    tbi = Number(tbi.slice(tbi.indexOf('-b') + 2));

    for(let i = 1; i <= tbi; i++) {
    	const item = {};
      ids.forEach(id => {
        const parseValue = parseAsfValue(getValue(asfTable, `${id}-b${i}`));
        if(parseValue) item[id] = parseValue;
      });
      result.push(item);
    }
    return result;
  } catch (err) {
    console.log(`ERROR [ parseAsfTable ]: ${err.message}`);
    return result;
  }
}

Array.prototype.uniq = function() {
  return this.filter(function(v, i, a){ return i == a.indexOf(v) });
}

const getAsfDataUUID = async documentID => {
  return new Promise(async resolve => {
    AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/formPlayer/getAsfDataUUID?documentID=${documentID}`,
      resolve, 'text',  null,
      err => {
        console.log(`ERROR [ getAsfDataUUID ]: ${err.responseText}`);
        resolve(null);
      }
    );
  });
}

const getPersonalRecord = async userID => {
  const personalrecord = await AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/personalrecord/forms/${userID}`);
  const userCard = personalrecord.find(x => x.formCode == 'workflow_form_trudovye_otnosheniya');
  return userCard ? AS.FORMS.ApiUtils.loadAsfData(userCard['data-uuid']) : null;
}

const getUserWorkTimeMode = async (userID, positionID) => {
  try {
    const personalCardData = await getPersonalRecord(userID);
    if(!personalCardData) throw new Error('Не найдены карточки юзера');

    const table = parseAsfTable(getValue(personalCardData, 'table-positions'));
    if(!table.length) throw new Error('Таблица должностей пуста');

    const block = table.find(x => x.position.key == positionID);
    if(!block || !block.workmodeLink || !block.workmodeLink.hasOwnProperty('key'))
    throw new Error(`В таблице должностей не найдена запись с positionID: ${positionID}`);

    const workModelDataUUID = await getAsfDataUUID(block.workmodeLink.key);
    if(!workModelDataUUID) throw new Error(`Не удалось получить айди графика юзера`);

    const workModelAsfData = await AS.FORMS.ApiUtils.loadAsfData(workModelDataUUID);
    if(!workModelAsfData) throw new Error(`Не удалось получить график юзера`);

    return {
      scheduleType: getValue(workModelAsfData, 'workflow_form_workmode_schedule'),
      scheduleSumHours: getValue(workModelAsfData, 'workflow_form_workmode_sum_hours'),
      schedule: JSON.parse(getValue(workModelAsfData, 'schedule_json').value || {})
    }
  } catch (err) {
    console.log(`ERROR [ getUserWorkTimeMode ]: ${err.message}`);
    return null;
  }
}

const parseOrderSettings = data => {
  if(data.recordsCount == 0) return [];
  const result = [];
  for(let i = 0; i < data.result.length; i++) {
    const {fieldValue} = data.result[i];
    result.push({
      abbr: fieldValue?.workflow_form_workTable_settings_abbreviation || null,
      amount: fieldValue?.workflow_form_workTable_settings_table_amount || null,
      sum: fieldValue?.workflow_form_workTable_settings_table_sum || null
    });
  }
  return result;
}

const getOrderSettings = async () => {
  const fields = [
    "workflow_form_workTable_settings_abbreviation",
    "workflow_form_workTable_settings_table_amount",
    "workflow_form_workTable_settings_table_sum"
  ];
  let searchURL = 'rest/api/registry/data_ext?registryCode=workTable_settings';
  searchURL += '&registryRecordStatus=STATE_SUCCESSFUL';

  fields.forEach(field => searchURL += '&fields=' + field);

  const searchResult = await AS.FORMS.ApiUtils.simpleAsyncGet(searchURL);

  return parseOrderSettings(searchResult);
}

const fillResultFieldAbbr = (item, row, tableID, tableBlockIndex) => {
  let {abbr, amount, sum} = item;
  let resultSum = 0;
  let resultAmount = 0;

  if(abbr) abbr = abbr.toUpperCase();

  for(let day = 1; day <= 31; day++) {
    if(!row.hasOwnProperty(`day_${day}`)) continue;

    let {value} = row[`day_${day}`];
    if(!value) continue;
    value = value.toUpperCase();

    if(value.indexOf(abbr) == -1) continue;

    value = value.split(';');

    value.forEach(v => {
      const a = v.replace(/[\d]/g, '').trim();
      const h = v.replace(/[^\d]/g, '');

      if(a == abbr) resultAmount++;
      resultSum += (Number(h) || 0);
    });

    resultSum = Math.floor(resultSum * 100) / 100;
  }

  if(amount) {
    const fieldResultAmount = model.playerModel.getModelWithId(amount, tableID, tableBlockIndex);
    if(fieldResultAmount) fieldResultAmount.setValue(String(resultAmount));
  }

  if(sum) {
    const fieldResultSum = model.playerModel.getModelWithId(sum, tableID, tableBlockIndex);
    if(fieldResultSum) fieldResultSum.setValue(String(resultSum));
  }
}

const fillResultField = (row, tableID, tableBlockIndex, userWorkTimeMode, tabelMonth) => {
  try {
    const {scheduleType, scheduleSumHours, schedule} = userWorkTimeMode || {};

    let work_days_res = 0;
    let work_hours_res = 0;
    let night_hours_res = 0;
    let weekends_and_holidays_res = 0;

    for(let day = 1; day <= 31; day++) {
      if(!row.hasOwnProperty(`day_${day}`)) continue;

      let {value} = row[`day_${day}`];
      if(!value) continue;
      value = value.toUpperCase();

      value = value.split(';');

      if(value.indexOf('В') !== - 1 || value.indexOf('П') !== - 1)  {
        weekends_and_holidays_res++;
        continue;
      }

      for(let i = 0; i < value.length; i++) {
        let v = value[i];
        if(v == '') continue;

        if(scheduleType && scheduleType.value == '2') {
          if(v.indexOf('/') !== -1) {
            v = v.split('/');
            let d = Number(v[0].replace(/[^\d]/g, ''));
            let n = Number(v[1].replace(/[^\d]/g, ''));

            let dValue = d;
            if(scheduleSumHours && scheduleSumHours.hasOwnProperty('values') && scheduleSumHours.values[0] != null) dValue = d + n;
            if(dValue > 24) dValue = 24;

            work_days_res++;
            work_hours_res += dValue;
            night_hours_res += n;
          } else {
            if(!isNaN(Number(v))) {
              work_days_res++;
              work_hours_res += (Number(v) || 0);
            } else {
              let a = v.replace(/[\d]/g, '').trim();
              let h = v.replace(/[^\d]/g, '');

              if(a == 'К') {
                work_days_res++;
                work_hours_res += (Number(h) || 0);
              }
            }
          }
        } else {
          if(!isNaN(Number(v))) {
            work_days_res++;
            work_hours_res += (Number(v) || 0);
          } else {
            let a = v.replace(/[\d]/g, '').trim();
            let h = v.replace(/[^\d]/g, '');

            if(a == 'К') {
              work_days_res++;
              work_hours_res += (Number(h) || 0);
            }
          }
        }
      }

    }

    const work_days_Result = model.playerModel.getModelWithId('work_days', tableID, tableBlockIndex);
    if(work_days_Result) work_days_Result.setValue(String(work_days_res));

    const work_hours_Result = model.playerModel.getModelWithId('work_hours', tableID, tableBlockIndex);
    if(work_hours_Result) work_hours_Result.setValue(String(work_hours_res));

    const night_hours_Result = model.playerModel.getModelWithId('night_hours', tableID, tableBlockIndex);
    if(night_hours_Result) night_hours_Result.setValue(String(night_hours_res));

    const weekends_and_holidays_Result = model.playerModel.getModelWithId('weekends_and_holidays', tableID, tableBlockIndex);

    if(schedule) {
      const scheduleMonth = schedule[tabelMonth];

      const day_norm_Result = model.playerModel.getModelWithId('day_norm', tableID, tableBlockIndex);
      if(day_norm_Result) day_norm_Result.setValue(String(scheduleMonth.countDays));

      const hours_norm_Result = model.playerModel.getModelWithId('hours_norm', tableID, tableBlockIndex);
      if(hours_norm_Result) hours_norm_Result.setValue(String(scheduleMonth.sumHours));

      if(scheduleMonth.hasOwnProperty('dayOffMonth') && scheduleMonth.hasOwnProperty('dayOffHalfMonth') && scheduleMonth.hasOwnProperty('holidaysMonth') && scheduleMonth.hasOwnProperty('holidaysHalfMonth')) {

        if(row.hasOwnProperty(`day_16`)) {
          const scheduleHolidaysResult = scheduleMonth.dayOffMonth + scheduleMonth.holidaysMonth;
          if(weekends_and_holidays_Result) weekends_and_holidays_Result.setValue(String(scheduleHolidaysResult));
        } else {
          const scheduleHolidaysResult = scheduleMonth.dayOffHalfMonth + scheduleMonth.holidaysHalfMonth;
          if(weekends_and_holidays_Result) weekends_and_holidays_Result.setValue(String(scheduleHolidaysResult));
        }

      } else {
        if(weekends_and_holidays_Result) weekends_and_holidays_Result.setValue(String(weekends_and_holidays_res));
      }

    } else {
      if(weekends_and_holidays_Result) weekends_and_holidays_Result.setValue(String(weekends_and_holidays_res));
    }

  } catch (err) {
    console.log(`ERROR [ fillResultField ]: ${err.message}`);
  }
}

const fillTimesheetResult = async () => {
  const tableID = 'table';

  AS.SERVICES.showWaitWindow();
  try {
    const tableModel = model.playerModel.getModelWithId(tableID);
    const timesheet = parseAsfTable(tableModel.getAsfData()[0]);
    const monthModel = model.playerModel.getModelWithId('month');
    const tabelMonth = Number(monthModel.getValue()[0]);
    const items = await getOrderSettings();

    for(let i = 0; i < timesheet.length; i++) {
      const row = timesheet[i];
      const {department, position, user} = row;
      const tableBlockIndex = i + 1;
      const userWorkTimeMode = await getUserWorkTimeMode(user.key, position.key);

      //зашитые поля, кол-во дней, часов
      fillResultField(row, tableID, tableBlockIndex, userWorkTimeMode, tabelMonth);

      //заполнение по аббревиатурам с настроек компонента
      for(let j = 0; j < items.length; j++) {
        const item = items[j];
        fillResultFieldAbbr(item, row, tableID, tableBlockIndex);
      }
    }

    AS.SERVICES.hideWaitWindow();
  } catch (err) {
    console.log(`ERROR [ fillTimesheetResult ]: ${err.message}`);
    AS.SERVICES.hideWaitWindow();
    AS.SERVICES.showErrorMessage(i18n.tr(err.message));
  }
}

const renderButton = () => {
  $('.button_fill_result').remove();

  const button = $('<button>', {class: 'button_fill_result'});
  button.text(i18n.tr('Пересчитать итоги'));
  button.on('click', e => {
    e.preventDefault();
    e.target.blur();
    fillTimesheetResult();
  });

  view.container.append(button);
}

if(editable) renderButton();
