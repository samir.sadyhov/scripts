var result = true;
var message = 'ok';

function getExecutorsRow(planDate) {
  let query = "WHERE code = 'maintenance_form_executors' AND (maintenance_form_executors_status.DATE = 1 OR ('";
  query += planDate;
  query += "' NOT BETWEEN maintenance_form_executors_inactive_start.DATE AND maintenance_form_executors_inactive_end.DATE))";

  let urlParam = {
    query: query,
    parameters: ["maintenance_form_executors_status.DATE", "maintenance_form_executors_inactive_start.DATE", "maintenance_form_executors_inactive_end.DATE"],
    searchInRegistry: "true",
    showDeleted: "false",
    registryRecordStatus: ["STATE_SUCCESSFUL"]
  }

  return API.httpPostMethod("rest/api/asforms/search/advanced", urlParam, "application/json; charset=utf-8");
}

function getCountOrder(planDate, userID) {
  let url = 'rest/api/registry/count_data?registryCode=maintenance_registry_orders'
  url += '&field=maintenance_form_order_responsible_users&condition=TEXT_EQUALS&key=' + userID;
  url += '&field=maintenance_form_order_plan_date&condition=MORE_OR_EQUALS&key=' + encodeURIComponent(planDate + ' 00:00:00');
  url += '&field=maintenance_form_order_plan_date&condition=LESS_OR_EQUALS&key=' + encodeURIComponent(planDate + ' 00:00:00');
  url += '&registryRecordStatus=STATE_NOT_FINISHED';

  return API.httpGetMethod(url);
}

function searchResponsible(planDate) {
  let executors = getExecutorsRow(planDate);

  executors.forEach(function(item){
    let itemAsfData = API.getFormData(item.dataUUID);
    let user = UTILS.getValue(itemAsfData, "entity_maintenance_executor_userid");
    item.userID = user.key;
    item.userName = user.value;
    item.orderCount = getCountOrder(planDate, user.key).recordsCount;
  });

  executors.sort(function(a, b) {return a.orderCount - b.orderCount});

  if(executors.length) {
    let executor = executors[0];
    return {
      init_responsible: {
        type: 'reglink',
        key: executor.documentID,
        value: API.getDocMeaningContent(executor.documentID)
      },
      init_responsible_users: {
        type: 'entity',
        key: executor.userID,
        value: executor.userName
      }
    }
  } else {
    return null;
  }
}

try {
  let currentFormData = API.getFormData(dataUUID);
  let assigning_method = UTILS.getValue(currentFormData, "maintenance_form_order_assigning_method");
  let plan_date = UTILS.getValue(currentFormData, "maintenance_form_order_plan_date");

  if(plan_date && plan_date.hasOwnProperty('key')) plan_date = plan_date.key.split(' ')[0];
  if(assigning_method && assigning_method.hasOwnProperty('value')) assigning_method = assigning_method.value;

  //признак для включения автораспределения
  if(assigning_method == '2') {
    let resp = searchResponsible(plan_date);
    if(resp) {
      let orderAsfData = [];
      UTILS.setValue(orderAsfData, 'maintenance_form_order_responsible', resp.init_responsible);
      UTILS.setValue(orderAsfData, 'maintenance_form_order_responsible_users', resp.init_responsible_users);

      API.mergeFormData({
        uuid: dataUUID,
        data: orderAsfData
      });
    }
  }

} catch (err) {
  result = false;
  message = err.message;
}
