var result = true;
var message = 'ok';

function customFormatDate(datetime){
  return ('0' + datetime.getDate()).slice(-2) + '.' +
  ('0' + (datetime.getMonth() + 1)).slice(-2) + '.' +
  datetime.getFullYear() + ' ' +
  ('0' + datetime.getHours()).slice(-2) + ':' +
  ('0' + datetime.getMinutes()).slice(-2);
}

function createOrder(startDate, item, init_category, init_responsible, init_responsible_users, autoAssignment){
  let orderAsfData = [];

  UTILS.setValue(orderAsfData, 'maintenance_form_order_item', item);
  UTILS.setValue(orderAsfData, 'maintenance_form_order_item_category', init_category);
  UTILS.setValue(orderAsfData, 'maintenance_form_order_responsible', init_responsible);
  UTILS.setValue(orderAsfData, 'maintenance_form_order_responsible_users', init_responsible_users);
  UTILS.setValue(orderAsfData, 'maintenance_form_order_reason', {
    type: 'listbox',
    key: '1',
    value: 'Плановые работы'
  });
  UTILS.setValue(orderAsfData, 'maintenance_form_order_plan_date', {
    type: 'date',
    key: UTILS.formatDate(startDate, true),
    value: customFormatDate(startDate)
  });

  if(autoAssignment) {
    UTILS.setValue(orderAsfData, 'maintenance_form_order_assigning_method', {
      type: 'radio',
      value: '3',
      key: 'Исполнитель выбран автоматически (инициализация ППР)'
    });
  }

  return API.httpPostMethod("rest/api/registry/create_doc_rcc", {
    registryCode: 'maintenance_registry_orders',
    sendToActivation: true,
    data: orderAsfData
  }, "application/json; charset=utf-8");
}

function getExecutorsRow(planDate) {
  let query = "WHERE code = 'maintenance_form_executors' AND (maintenance_form_executors_status.DATE = 1 OR ('";
  query += planDate;
  query += "' NOT BETWEEN maintenance_form_executors_inactive_start.DATE AND maintenance_form_executors_inactive_end.DATE))";

  let urlParam = {
    query: query,
    parameters: ["maintenance_form_executors_status.DATE", "maintenance_form_executors_inactive_start.DATE", "maintenance_form_executors_inactive_end.DATE"],
    searchInRegistry: "true",
    showDeleted: "false",
    registryRecordStatus: ["STATE_SUCCESSFUL"]
  }

  return API.httpPostMethod("rest/api/asforms/search/advanced", urlParam, "application/json; charset=utf-8");
}

function getCountOrder(planDate, userID) {
  let url = 'rest/api/registry/count_data?registryCode=maintenance_registry_orders'
  url += '&field=maintenance_form_order_responsible_users&condition=TEXT_EQUALS&key=' + userID;
  url += '&field=maintenance_form_order_plan_date&condition=MORE_OR_EQUALS&key=' + encodeURIComponent(planDate + ' 00:00:00');
  url += '&field=maintenance_form_order_plan_date&condition=LESS_OR_EQUALS&key=' + encodeURIComponent(planDate + ' 00:00:00');
  url += '&registryRecordStatus=STATE_NOT_FINISHED';

  return API.httpGetMethod(url);
}

function searchResponsible(startDate) {
  let planDate = UTILS.formatDate(startDate);
  let executors = getExecutorsRow(planDate);

  executors.forEach(function(item){
    let itemAsfData = API.getFormData(item.dataUUID);
    let user = UTILS.getValue(itemAsfData, "entity_maintenance_executor_userid");
    item.userID = user.key;
    item.userName = user.value;
    item.orderCount = getCountOrder(planDate, user.key).recordsCount;
  });

  executors.sort(function(a, b) {return a.orderCount - b.orderCount});

  if(executors.length) {
    let executor = executors[0];
    return {
      init_responsible: {
        type: 'reglink',
        key: executor.documentID,
        value: API.getDocMeaningContent(executor.documentID)
      },
      init_responsible_users: {
        type: 'entity',
        key: executor.userID,
        value: executor.userName
      }
    }
  } else {
    return null;
  }
}

try {
  let currentFormData = API.getFormData(dataUUID);
  let start_chooser = UTILS.getValue(currentFormData, "maintenance_form_init_start_chooser");
  let init_start = UTILS.getValue(currentFormData, "maintenance_form_init_start");
  let init_duration = UTILS.getValue(currentFormData, "maintenance_form_init_duration");
  let init_time = UTILS.getValue(currentFormData, "maintenance_form_init_time");
  let init_responsible = UTILS.getValue(currentFormData, "maintenance_form_init_responsible");
  let init_responsible_users = UTILS.getValue(currentFormData, "maintenance_form_init_responsible_users");
  let init_category = UTILS.getValue(currentFormData, "maintenance_form_init_category");
  let assigning_method = UTILS.getValue(currentFormData, "maintenance_form_init_assigning_method");

  let table = UTILS.parseAsfTable(UTILS.getValue(currentFormData, "maintenance_form_init_table_items"));
  if(!table.length) throw new Error('Не заполнена таблица активов');

  if(!init_duration || !init_duration.hasOwnProperty('key') || init_duration.key == "") throw new Error('Не заполнена продолжительность');

  if(assigning_method && assigning_method.hasOwnProperty('value')) assigning_method = assigning_method.value;

  // Если maintenance_form_init_start_chooser == 1 то проверяем заполнение даты старта
  if(start_chooser && start_chooser.value == "1") {
    if(!init_start || !init_start.hasOwnProperty('key')) throw new Error('Не заполнена дата старта');
  }

  //Загрузка справочников
  let dict_period = UTILS.parseDict(API.getDictionary('maintenance_dict_period'));
  let dict_times = UTILS.parseDict(API.getDictionary('maintenance_dict_times'));

  init_time = Number(dict_times.filter(function(x) {if(x.sort == init_time.key) return x})[0].value);
  init_duration = init_duration.key * init_time;

  if(isNaN(init_duration)) init_duration = 0;

  let createOrderSuccess = 0;
  let createOrderFailed = 0;

  // пробегаемся по таблице активов
  for(let i = 0; i < table.length; i++) {
    let item = table[i].item;
    let period = table[i].period.key;
    let startDate = new Date();
    let dateStop = new Date();
    if(!item || !item.hasOwnProperty('key')) continue;

    period = Number(dict_period.filter(function(x) {if(x.sort == period) return x})[0].value);
    if(isNaN(period)) period = 0;

    // Если maintenance_form_init_start_chooser == 2, то первую запись создавать относительно даты последнего обслуживания Актива
    if(start_chooser && start_chooser.value == "2") {
      let itemAsfData = API.getFormData(API.getAsfDataId(item.key));
      let last_maintenance = UTILS.getValue(itemAsfData, "maintenance_form_equipment_last_maintenance");
      if(last_maintenance && last_maintenance.hasOwnProperty('key')) {
        startDate = new Date(last_maintenance.key);
        startDate.setDate(startDate.getDate() + period);
      }
    } else if (start_chooser && start_chooser.value == "1") {
      startDate = new Date(init_start.key);
      dateStop = new Date(init_start.key);
    }

    startDate.setHours(0);
    startDate.setMinutes(0);
    startDate.setSeconds(0);

    //окончание периода
    dateStop.setDate(dateStop.getDate() + init_duration);
    dateStop.setHours(23);
    dateStop.setMinutes(59);
    dateStop.setSeconds(59);

    let created = true;
    while (created) {

      if(start_chooser && start_chooser.value == "1") {
        //если дата создания меньше текущей даты, то ниче не создаем
        if(UTILS.formatDate(startDate) >= UTILS.formatDate(new Date())) {

          let autoAssignment = false;

          //признак для включения автораспределения
          if(assigning_method == '2') {
            let resp = searchResponsible(startDate);
            if(resp) {
              init_responsible = resp.init_responsible;
              init_responsible_users = resp.init_responsible_users;
              autoAssignment = true;
            }
          }

          let createResult = createOrder(startDate, item, init_category, init_responsible, init_responsible_users, autoAssignment);
          if(createResult.errorCode == 0) {
            createOrderSuccess++;
          } else {
            createOrderFailed++;
          }
        }
      } else if (start_chooser && start_chooser.value == "2") {
        //Если дата (дата_последнего_обслуживания + периодичность_обслуживания) < сейчас: то первую запись создавать датой сейчас.
        if(UTILS.formatDate(startDate) < UTILS.formatDate(new Date())) {
          startDate = new Date();
          startDate.setHours(0);
          startDate.setMinutes(0);
          startDate.setSeconds(0);
        }

        let autoAssignment = false;

        //признак для включения автораспределения
        if(assigning_method == '2') {
          let resp = searchResponsible(startDate);
          if(resp) {
            init_responsible = resp.init_responsible;
            init_responsible_users = resp.init_responsible_users;
            autoAssignment = true;
          }
        }
        let createResult = createOrder(startDate, item, init_category, init_responsible, init_responsible_users, autoAssignment);
        if(createResult.errorCode == 0) {
          createOrderSuccess++;
        } else {
          createOrderFailed++;
        }
      }

      startDate.setDate(startDate.getDate() + period);

      //проверяем, если дата начала больше даты окночания периода, то больше не создаем записи
      if(startDate.getTime() > dateStop.getTime()) created = false;
    }
  }

  message = "Создано успешно: " + createOrderSuccess + "; неуспешно: " + createOrderFailed;

} catch (err) {
  result = false;
  message = err.message;
}
