var result = true;
var message = 'ok';

function customFormatDate(datetime){
  return ('0' + datetime.getDate()).slice(-2) + '.' +
  ('0' + (datetime.getMonth() + 1)).slice(-2) + '.' +
  datetime.getFullYear() + ' ' +
  ('0' + datetime.getHours()).slice(-2) + ':' +
  ('0' + datetime.getMinutes()).slice(-2);
}

function searchOrders(orderID, plan_date){
  let urlSearch = 'rest/api/registry/data_ext?registryCode=maintenance_registry_orders';
  urlSearch += '&field=maintenance_form_order_work_type&condition=TEXT_EQUALS&key=1';
  urlSearch += '&field=maintenance_form_order_status&condition=TEXT_EQUALS&key=0';
  urlSearch += '&field=maintenance_form_order_item&condition=TEXT_EQUALS&key=' + encodeURIComponent(orderID);
  urlSearch += '&field=maintenance_form_order_plan_date&condition=MORE_OR_EQUALS&key=' + encodeURIComponent(plan_date);
  urlSearch += '&registryRecordStatus=STATE_NOT_FINISHED&loadData=false';
  urlSearch += '&sortCmpID=maintenance_form_order_plan_date&sortDesc=true';

  let searchResult = API.httpGetMethod(urlSearch);

  if(!searchResult) return null;
  if(searchResult.count == 0) return null;

  return searchResult.data;
}

function stopRout(documentID) {
  return API.httpPostMethod("rest/api/docflow/doc/stop_route?documentID=" + documentID, {}, "application/json; charset=utf-8");
}

try {
  let currentFormData = API.getFormData(dataUUID);
  let work_type = UTILS.getValue(currentFormData, "maintenance_form_order_work_type");
  let item = UTILS.getValue(currentFormData, "maintenance_form_order_item");
  let order_plan_date = UTILS.getValue(currentFormData, "maintenance_form_order_plan_date");

  if(!work_type || !work_type.hasOwnProperty('key')) throw new Error('Не определен Тип работ');
  if(work_type.key != '2') throw new Error('Тип работ определен как ' + work_type.value + ', даты не изменились');

  if(!item || !item.hasOwnProperty('key')) throw new Error('Не выбрано оборудование (актив)');

  if(!order_plan_date || !order_plan_date.hasOwnProperty('key')) throw new Error('Не выбрано Плановое время проведения');

  let itemAsfData = API.getFormData(API.getAsfDataId(item.key));
  let equipment_period = UTILS.getValue(itemAsfData, "maintenance_form_equipment_period");
  let orders = searchOrders(item.key, order_plan_date.key);

  if(!orders) throw new Error('Не найдено нарядов для изменения');

  // записать дату последнего ремонта в карточку актива
  API.mergeFormData({
    uuid: itemAsfData.uuid,
    data: [{
      id: 'maintenance_form_equipment_last_maintenance',
      type: 'date',
      key: order_plan_date.key,
      value: order_plan_date.value
    }]
  });

  //Загрузка справочников
  let dict_period = UTILS.parseDict(API.getDictionary('maintenance_dict_period'));

  let period = Number(dict_period.filter(function(x) {if(x.sort == equipment_period.key) return x})[0].value);
  if(isNaN(period)) period = 0;

  let startDate = new Date(order_plan_date.key);

  orders.forEach(function(order){
    //грохаем все процессы
    stopRout(order.documentID);

    //сохраняем новую дату
    startDate.setDate(startDate.getDate() + period);
    API.mergeFormData({
      uuid: order.dataUUID,
      data: [{
        id: 'maintenance_form_order_plan_date',
        type: 'date',
        key: UTILS.formatDate(startDate, true),
        value: customFormatDate(startDate)
      }]
    });

    //запускаем маршрут
    API.activateDoc(order.documentID);
  });

} catch (err) {
  message = err.message;
}
