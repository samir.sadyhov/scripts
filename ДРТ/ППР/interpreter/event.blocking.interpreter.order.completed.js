var result = true;
var message = 'ok';

function processesFilter(processes) {
  let result = [];
  function search(p) {
    p.forEach(function(x) {
      if (x.typeID == 'ASSIGNMENT_ITEM' && x.finished) result.push(x);
      if (x.subProcesses.length > 0) search(x.subProcesses);
    });
  }
  search(processes);
  return result;
}

function searchOrders(activeID, plan_date){
  let urlSearch = 'rest/api/registry/data_ext?registryCode=maintenance_registry_orders';
  urlSearch += '&field=maintenance_form_order_work_type&condition=TEXT_EQUALS&key=1';
  urlSearch += '&field=maintenance_form_order_status&condition=TEXT_EQUALS&key=0';
  urlSearch += '&field=maintenance_form_order_item&condition=TEXT_EQUALS&key=' + encodeURIComponent(activeID);
  urlSearch += '&field=maintenance_form_order_plan_date&condition=MORE_OR_EQUALS&key=' + encodeURIComponent(plan_date);
  urlSearch += '&registryRecordStatus=STATE_NOT_FINISHED';
  urlSearch += '&sortCmpID=maintenance_form_order_plan_date&sortDesc=true';
  urlSearch += '&fields=maintenance_form_order_plan_date&countInPart=1';

  let searchResult = API.httpGetMethod(urlSearch);

  if(!searchResult) return null;
  if(searchResult.recordCount == 0) return null;
  if(!searchResult.result.length) return null;

  return searchResult.result[0];
}

try {
  let currentFormData = API.getFormData(dataUUID);
  let processes = processesFilter(API.getProcesses(documentID));

  if(!processes.length) throw new Error('Не найденно завершенной работы');

  let resultFormWork = API.getFormForResult('maintenance_form_order_wcf', processes[0].actionID);
  let resultFormWorkData = API.getFormData(resultFormWork.dataUUID);
  let wcf_date = UTILS.getValue(resultFormWorkData, "maintenance_form_order_wcf_date");
  let table_files = UTILS.getValue(resultFormWorkData, "maintenance_form_order_wcf_table_files");
  let item = UTILS.getValue(currentFormData, "maintenance_form_order_item");
  let itemAsfData = API.getFormData(API.getAsfDataId(item.key));
  let nextOrder = searchOrders(item.key, wcf_date.key);

  if(table_files && table_files.hasOwnProperty('data') && table_files.data.length) {
    table_files.data = table_files.data.filter(function(x){
      if(x.type != 'label') return x;
    })
  }

  //записываем на форму наряда данные с ФЗ
  UTILS.setValue(currentFormData, 'maintenance_form_order_status', UTILS.getValue(resultFormWorkData, "maintenance_form_order_wcf_status"));
  UTILS.setValue(currentFormData, 'maintenance_form_order_fact_date', wcf_date);
  UTILS.setValue(currentFormData, 'maintenance_form_order_comment', UTILS.getValue(resultFormWorkData, "maintenance_form_order_wcf_comment"));
  if(table_files) UTILS.setValue(currentFormData, 'maintenance_form_order_table_files', table_files);

  //сохранение наряда
  API.mergeFormData(currentFormData);

  // записать дату последнего ремонта в карточку актива
  UTILS.setValue(itemAsfData, 'maintenance_form_equipment_last_maintenance', wcf_date);
  if(nextOrder) {
    UTILS.setValue(itemAsfData, 'maintenance_form_equipment_next_maintenance', {
      type: 'data',
      value: nextOrder.fieldValue.maintenance_form_order_plan_date || '',
      key: nextOrder.fieldKey.maintenance_form_order_plan_date || ''
    });
  }

  //сохранение карточки актива
  API.mergeFormData(itemAsfData);

} catch (err) {
  message = err.message;
}
