if(editable) {
  model.on('tableRowAdd', () => {
    const blocks = model.modelBlocks.length;
    const init_counter = model.playerModel.getModelWithId('maintenance_form_init_counter');
    if(init_counter) init_counter.setValue(String(blocks));
  });

  model.on('tableRowDelete', () => {
    const blocks = model.modelBlocks.length;
    const init_counter = model.playerModel.getModelWithId('maintenance_form_init_counter');
    if(init_counter) init_counter.setValue(String(blocks));
  });
}
