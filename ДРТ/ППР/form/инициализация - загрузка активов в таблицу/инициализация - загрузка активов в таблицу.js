const tableID = 'maintenance_form_init_table_items';

const searchRowInRegistry = async category => {
  let param = `registryCode=maintenance_registry_equipment`;
  param += `&field=maintenance_form_equipment_category&condition=TEXT_EQUALS&key=${category}`;
  param += `&fields=maintenance_form_equipment_last_maintenance`;
  param += `&fields=maintenance_form_equipment_period`;

  return new Promise(async resolve => {
    const res = await AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/data_ext?${param}`);
    if(res.errorCode) resolve(null);
    if(res.recordsCount == 0) resolve(null);
    resolve(res.result);
  });
}

const clearTable = tableModel => {
  for(const numb in tableModel.getBlockNumbers()) tableModel.removeRow(numb);
}

const fillHandler = async () => {
  AS.SERVICES.showWaitWindow();
  
  try {
    const categoryModel = model.playerModel.getModelWithId('maintenance_form_init_category');
    const categoryView = view.playerView.getViewWithId('maintenance_form_init_category');
    const tableModel = model.playerModel.getModelWithId(tableID);

    if(!categoryModel || !categoryModel.getValue()) {
      if(categoryView) categoryView.markInvalid();
      throw new Error('Не выбран вид оборудования');
    }

    if(categoryView) categoryView.unmarkInvalid();

    clearTable(tableModel);

    const data = await searchRowInRegistry(categoryModel.getValue());
    if(!data) throw new Error('Не найдены активы для данного вида оборудования');

    for(let i = 0; i < data.length; i++) {
      const {documentID, fieldKey} = data[i];
      const row = tableModel.createRow();

      const itemModel = model.playerModel.getModelWithId('item', tableID, row.tableBlockIndex);
      if(itemModel) itemModel.setValue(documentID);
    }

    AS.SERVICES.hideWaitWindow();
  } catch (err) {
    AS.SERVICES.hideWaitWindow();
    AS.SERVICES.showErrorMessage(i18n.tr(err.message));
  }
}

const clearHandler =  () => {
  const tableModel = model.playerModel.getModelWithId(tableID);
  clearTable(tableModel);
}

const renderButtons = () => {
  $('.maintenance_form_init_panel_buttons').remove();

  const panel = $('<div>', {class: 'maintenance_form_init_panel_buttons'});

  const buttonFill = $('<button>', {class: 'maintenance_form_init_button', id: 'button_fill_result'});
  buttonFill.text(i18n.tr('Загрузить'));
  buttonFill.on('click', e => {
    e.preventDefault();
    e.target.blur();
    fillHandler();
  });

  const buttonClear = $('<button>', {class: 'maintenance_form_init_button', id: 'button_clear_result'});
  buttonClear.text(i18n.tr('Очистить'));
  buttonClear.on('click', e => {
    e.preventDefault();
    e.target.blur();
    clearHandler();
  });

  panel.append(buttonFill, buttonClear);
  view.container.append(panel);
}

if(editable) renderButtons();
