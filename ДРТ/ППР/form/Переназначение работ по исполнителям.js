const matching = [];
matching.push({from: 'maintenance_form_reassign_responsible', to: 'maintenance_form_order_responsible'});
matching.push({from: 'maintenance_form_reassign_responsible_user', to: 'maintenance_form_order_responsible_users'});
matching.push({from: 'maintenance_form_reassign_responsible_user_pos', to: 'maintenance_form_order_responsible_pos'});
matching.push({from: 'maintenance_form_reassign_responsible_user_dep', to: 'maintenance_form_order_responsible_dep'});

const closeWindowInSynergy = () => {
  $('td > img[src*="edit.png"]').click();
  $('td > img[src*="close.png"]').click();
}

const mergeFormData = async player => {
  for(let i = 0; i < matching.length; i++) {
    const {from, to} = matching[i];
    const fromModel = player.model.getModelWithId(from);
    const toModel = model.playerModel.getModelWithId(to);
    if(fromModel && toModel) toModel.setValue(fromModel.getValue());
  }

  return AS.FORMS.ApiUtils.saveAsfData(model.playerModel.getAsfData().data, model.playerModel.formId, model.playerModel.asfDataId);
}

const getUserGroups = async () => {
  return new Promise(async resolve => {
    try {
      if(AS.OPTIONS.currentUser.hasOwnProperty('groups')) {
        resolve(AS.OPTIONS.currentUser.groups);
      } else {
        const res = await AS.FORMS.ApiUtils.simpleAsyncGet('rest/api/person/auth?getGroups=true');
        AS.OPTIONS.currentUser.groups = res.groups;
        resolve(AS.OPTIONS.currentUser.groups);
      }
    } catch (e) {
      console.log(e);
      resolve([]);
    }
  });
}

const processesFilter = processes => {
  const result = [];
  const search = p => {
    p.forEach(x => {
      if (x.typeID == 'ASSIGNMENT_ITEM' && !x.finished) result.push(x);
      if (x.subProcesses.length > 0) search(x.subProcesses);
    });
  }
  search(processes);
  return result;
}

const saveWork = async body => {
  return new Promise(async resolve => {
    try {
      const {login, password} = AS.OPTIONS;
      const url = `../Synergy/rest/api/workflow/work/save?locale=${AS.OPTIONS.locale}`;
      const headers = new Headers();
      headers.append("Authorization", "Basic " + btoa(unescape(encodeURIComponent(`${login}:${password}`))));
      headers.append("Content-Type", "application/x-www-form-urlencoded; charset=UTF-8");
      const response = await fetch(url, {method: 'POST', headers, body});
      resolve(response.json());
    } catch (err) {
      resolve({
        errorCode: 666,
        errorMessage: err.message
      });
    }
  });
}

const editWork = async (proc, responsible_user) => {
  const workInfo = await AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/workflow/works_by_id?workID=${proc.actionID}`);
  const work = workInfo[0];
  const editWorkBody = new URLSearchParams();

  editWorkBody.append("actionID", work.actionID);
  editWorkBody.append("name", work.name);
  editWorkBody.append("userID", responsible_user.getValue()[0].personID);
  editWorkBody.append("startDate", work.start_date);
  editWorkBody.append("finishDate", work.finish_date);
  editWorkBody.append("priority", work.priority);
  editWorkBody.append("completionFormCode", proc.completionFormCode);
  editWorkBody.append("completionFormID", proc.completionFormID);

  return await saveWork(editWorkBody);
}

const reassign = async (player, dialog) => {
  AS.SERVICES.showWaitWindow();
  try {
    if(!player.model.isValid()) throw new Error('Заполните обязательные поля!');

    const processes = await AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/workflow/get_execution_process?documentID=${model.playerModel.documentID}`);
    const proc = processesFilter(processes);

    if(!proc.length) throw new Error('Не найдено работ для переназначения');

    await mergeFormData(player);

    model.playerModel.hasChanges = false;

    const responsible_user = player.model.getModelWithId('maintenance_form_reassign_responsible_user');
    const editWorkResult = await editWork(proc[0], responsible_user);

    if(editWorkResult.errorCode != 0) throw new Error(editWorkResult.errorMessage);

    dialog.dialog("close");
    closeWindowInSynergy();

    AS.SERVICES.hideWaitWindow();
  } catch (err) {
    AS.SERVICES.hideWaitWindow();
    AS.SERVICES.showErrorMessage(err.message);
  }
}

const getSynergyPlayer = () => {
  const player = AS.FORMS.createPlayer();
  player.view.setEditable(true);
  player.showFormByCode('maintenance_form_reassign');
  return player;
}

const openReassignDialog = () => {
  const dialog = $('<div>');
  const player = getSynergyPlayer();

  dialog.append(player.view.container);

  dialog.dialog({
    modal: true,
    width: 500,
    height: 300,
    title: 'Переназначение',
    show: {
      effect: 'fade',
      duration: 300
    },
    hide: {
      effect: 'fade',
      duration: 300
    },
    buttons: {
      'Сохранить': function() {
        reassign(player, dialog);
      },
      'Отмена': function() {
        $(this).dialog("close");
      }
    },
    close: function() {
      $(this).remove();
    }
  });
}

const renderButton = () => {
  $('.custom-reassign-button').remove();

  const container = $(`[document_id=${model.playerModel.documentID}]`).closest('tbody');
  const reassignButton = $('<div>', {class: 'GCFCEAPDHS GCFCEAPDPJ'});

  reassignButton.css({
    'width': '100%',
    'background': '#2561c3',
    'color': '#fff'
  });

  reassignButton.append(`<span class="GCFCEAPDLO">${i18n.tr('Переназначить')}</span>`);

  container.find('td > div.gwt-HTML').each(function(k, item) {
    if($(item).height() == 6) $(item).remove();
  });

  const tr = $(`<tr colspan="3" class="custom-reassign-button">`);
  tr.append($('<td align="left" style="vertical-align: top;">').append(reassignButton));
  container.append(tr);
  container.append('<tr><td colspan="3"><div class="gwt-HTML" style="height: 6px;"></div></td></tr>');

  reassignButton.on('click', e => {
    e.preventDefault();
    e.target.blur();

    if(!model.playerModel.isValid()) {
      AS.SERVICES.showErrorMessage('Заполните обязательные поля');
      return;
    }

    openReassignDialog();
  });
}

const initButtonReassign = async () => {
  try {
    if(window.location.href.indexOf('Synergy') === -1) throw new Error('это не Synergy, сваливаем :)');

    const groups = await getUserGroups();
    const reassignGroup = groups.find(i => i.groupCode == 'maintenance_group_reassign_access');

    if(!reassignGroup) throw new Error('пу пу пууу.... прав жок!');

    model.playerModel.documentID = await AS.FORMS.ApiUtils.getDocumentIdentifier(model.playerModel.asfDataId);

    renderButton();
  } catch (err) {
    console.log(`[ initButtonReassign ]: ${err.message}`);
  }
}

initButtonReassign();
