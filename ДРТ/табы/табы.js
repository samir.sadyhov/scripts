/*
model.tabs = [];
model.tabs.push({name: '', blocks: []});
model.tabs.push({name: '', blocks: [], accordion: [
  {titleID: '', open: true, blocks: []}
]});

@param name - наименование таба
@param blocks - массив, перечисление ID компонентов которые будут входить в данный блок.

@param accordion.titleID - ID компонента на форме с типом label для заголовка блока
@param accordion.open - true/false, открытый или закрытый блок
@param accordion.blocks - массив, перечисление ID компонентов которые будут входить в данный блок.
*/

class Tabs {
  constructor(){
    this.parentContainer = view.playerView.container;
    this.tabContainer = $('<div>', {class: 'tab-container'});
    this.tab = $('<div>', {class: 'tab'});
    this.tabsData = [];

    this.init();
  }

  toggleAcc(el, block) {
    el = el[0];
    el.classList.toggle("active");
    block.open = $(el).hasClass('active');
    const panel = el.nextElementSibling;
    if (panel.style.maxHeight) panel.style.maxHeight = null;
    else panel.style.maxHeight = "initial";
  }

  render(){
    this.tabsData.forEach((item, i) => {
      const tabButton = $('<button>', {class: 'tablinks'}).text(item.name);
      const tabcontent = $('<div>', {class: 'tabcontent'});

      this.tab.append(tabButton);
      this.tabContainer.append(tabcontent);

      tabButton.on('click', e => {
        this.tabContainer.find('.tablinks').removeClass('active');
        tabButton.addClass('active');
        this.tabContainer.find('.tabcontent').removeClass('active');
        tabcontent.addClass('active');
      });

      if(i == 0) {
        tabButton.addClass('active');
        tabcontent.addClass('active');
      }

      if(item.hasOwnProperty('blocks')) {
        item.blocks.forEach(block => {
          const tabData = $(this.parentContainer).find(`[data-asformid $= ".container.${block}"]`);
          tabData.parent().parent().hide();
          tabcontent.append(tabData.detach());
        });
      }

      if(item.hasOwnProperty('accordion')) {
        const accordionContainer = $('<div class="custom-accordion">');
        tabcontent.append(accordionContainer);

        item.accordion.forEach(acc => {
          const block = $('<div class="accordion-block">')
          const title = $(this.parentContainer).find(`[data-asformid="label.label.${acc.titleID}"]`);
          const button = $(`<button class="accordion-button">${title.text()}</button>`);
          const body = $('<div class="accordion-body">');

          title.parent().parent().parent().hide();
          accordionContainer.append(block);
          block.append(button).append(body);
          button.on('click', () => toggleAcc(button, acc));

          acc.blocks.forEach(accblock => {
            const cc = $(this.parentContainer).find(`[data-asformid $= ".container.${accblock}"]`);
            cc.parent().parent().hide();
            body.append(cc.detach());
          });

          body.append('<div class="accordion-footer"></div>');
          if(acc.open) toggleAcc(button, acc);
        });
      }

    });
  }

  init(){
    this.tabContainer.append(this.tab);
    $(view.container).append(this.tabContainer);

    if(model.hasOwnProperty('tabs')) this.tabsData = model.tabs;

    this.render();
  }
}

if(!model.playerModel.hasOwnProperty('viewCode') && !model.playerModel.hasOwnProperty('tabsInited')) {
  model.playerModel.tabsInited = true;
  new Tabs();
}
