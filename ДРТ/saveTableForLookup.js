const cmpResult = 'table_skillsSEARCH';

const parseAsfTable = asfTable => {
  if(!asfTable.hasOwnProperty('data')) return [];
  const data = asfTable.data.filter(x => x.type != 'label');
  if(!data.length) return [];

  const result = [];
  const ids = data.map(x => x.id.slice(0, x.id.indexOf('-b'))).uniq();
  let tbi =  data.slice(-1)[0].id;
  tbi = Number(tbi.slice(tbi.indexOf('-b') + 2));

  for(let i = 1; i <= tbi; i++) {
    ids.forEach(key => {
      const itemAsfData = asfTable.data.find(x => x.id == `${key}-b${i}`);
      if(itemAsfData && itemAsfData.hasOwnProperty('value')) result.push(itemAsfData.value);
    });
  }

  return result;
}

const setParseTable = (tableModel, cmp) => {
  if(!tableModel) return;
  const arr = parseAsfTable(tableModel.getAsfData()[0]);
  const modelResult = model.playerModel.getModelWithId(cmp);
  if(modelResult) modelResult.setValue(arr.join(' '));
}

const addListener = block => {
  block.forEach(cmpModel => {
    if(cmpModel && cmpModel.asfProperty != 'label') {
      cmpModel.on('valueChange', () => {
        setParseTable(model, cmpResult);
      });
    }
  });
}

const initTableListener = () => {
  const {modelBlocks} = model;
  model.on('tableRowAdd', (event, tableModel, block) => addListener(block));
  modelBlocks.forEach(addListener);
}

if(editable) initTableListener();

Array.prototype.uniq = function() {
  return this.filter((v, i, a) => i == a.indexOf(v));
}
