if (window.location.href.indexOf('Synergy') !== -1) return;

function check(){
  if(model.getValue() == 0) {
    ['table-params', 'label-params', 'label-events', 'cjm_events', 'label-m8bcnp_copy1', 'label-feedback'].forEach(cmp => {
      let component = view.playerView.getViewWithId(cmp);
      if (component) component.setVisible(false);
    })
  }
}

check();
model.on("valueChange", check);
