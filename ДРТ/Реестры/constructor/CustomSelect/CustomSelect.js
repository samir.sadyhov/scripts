this.CustomSelect = class {
  constructor(param) {
    this.items = param.items;
    this.placeholder = i18n.tr(param.placeholder || 'Выбор...');
    this.search = param.search || false;
    this.selectItem = null;
    this.status = 'hide';
    this.init();
  }

  createSelectContainer() {
    this.selectContainer = $('<div>', {class: 'custom-select-container'});
    this.selected = $('<div>', {class: 'select-selected'}).text(this.placeholder);
    this.selectItems = $('<div>', {class: 'select-items select-hide'});
    this.selectContainer.append(this.selected, this.selectItems);

    this.selected.on('click', e => this.status === 'hide' ? this.show() : this.hide());
    this.selectContainer.on('close-select', e => this.hide());

    if(this.search) {
      const me = this;
      this.searchInput = $('<input>', {class: 'select-search-input select-hide', placeholder: i18n.tr('Поиск...')});
      this.selectContainer.append(this.searchInput);
      this.searchInput.keyup(function(){
        $.each(me.selectItems.find("div"), function() {
          if($(this).text().toLowerCase().indexOf(me.searchInput.val().toLowerCase()) === -1) $(this).hide();
          else $(this).show();
        });
      });
    }
  }

  createItem(item) {
    const menuItem = $(`<div data-value="${item.value}" title="${i18n.tr(item.title)}">${i18n.tr(item.title)}</div>`);
    menuItem.on('click', e => {
      this.hide();
      this.selected.text(i18n.tr(item.title));
      this.selected.attr('title', i18n.tr(item.title));
      this.selectItem = item;
      this.selectContainer.trigger({
      	type: 'selected',
        eventParam: this.value
      });
    });
    this.selectItems.append(menuItem);
  }

  createItems() {
    this.items.forEach(x => this.createItem(x));
  }

  get container() {
    return this.selectContainer;
  }
  get value() {
    return this.selectItem;
  }

  set value(value) {
    const findVal = this.items.find(x => x.value == value);
    if (!findVal) {
    	this.selectItem = null;
      this.selected.text(this.placeholder);
    }
    this.selectItem = findVal;
  }

  hide() {
    this.status = 'hide';
    this.selected.removeClass('select-arrow-active');
    this.selectItems.addClass('select-hide');
    if(this.search) this.searchInput.addClass('select-hide');
  }

  show() {
    this.status = 'open';
    this.selected.addClass('select-arrow-active');
    this.selectItems.removeClass('select-hide');
    if(this.search) {
      this.searchInput.removeClass('select-hide').val('');
      $.each(this.selectItems.find("div"), function() {
        $(this).show();
      });
    }
  }

  init() {
    this.createSelectContainer();
    this.createItems();
  }
}

const closeAllSelect = (e) => {
  if ($(e.target).closest('.custom-select-container').length == 0) $('.custom-select-container').trigger('close-select');
}
document.addEventListener("click", closeAllSelect);
