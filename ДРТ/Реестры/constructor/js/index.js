const parseTranslations = items => {
	const result = {};
  for(const key in items) {
  	const {message: {value, translations}} = items[key];
    result[value] = translations;
  }
  return result;
}

const registryAppInit = async () => {
  //Иконка приложения
  if(!$('link[rel="icon"]').length) $('head').append('<link rel="icon" href="../constructorFiles/synergy_registry/logo.svg">');

  const {doctypes, systemSettings, systemLocales, translations} = Cons.getAppStore();

  if(!doctypes) {
    setTimeout(() => {
      appAPI.getDoctypes().then(doctypes => Cons.setAppStore({doctypes}));
    }, 100);
  }

  if(!systemSettings) {
    setTimeout(() => {
      appAPI.getSystemSettings().then(systemSettings => {
        Cons.setAppStore({systemSettings});
      });
    }, 100);
  }

  if(!systemLocales) {
    setTimeout(() => {
      appAPI.getSystemLocales().then(systemLocales => {
        Cons.setAppStore({systemLocales});
      });
    }, 100);
  }

  if(!translations && translations != 'not found') {
    setTimeout(() => {
      appAPI.getDictionary('add_translations', false).then(res => {
        if(res && res.hasOwnProperty('items')) {
          const t = parseTranslations(res.items);
          Cons.setAppStore({translations: t});
        } else {
          Cons.setAppStore({translations: 'not found'});
        }
      });
    }, 100);
  }
}

registryAppInit();

$('#buttonLogout').off().on('click', e => {
  e.preventDefault();
  e.target.blur();
  Cons.logout();
  $('.footer-panel').remove();
  Cons.setAppStore({openDocsWindow: null});
});

jQuery(document).mouseup(function(event){
	setTimeout(function(){
		AS.FORMS.popupPanel.hide();
	}, 0);
});

jQuery(document).click(function(event){
	AS.FORMS.popupPanel.hide();
});
