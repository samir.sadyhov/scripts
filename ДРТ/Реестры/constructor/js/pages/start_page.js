const getUrlParameter = param => {
  const url = new URLSearchParams(window.location.search);
  return url.has(param) ? url.get(param) : null;
}

pageHandler('start_page', () => {
  const app = getUrlParameter('app');
  const locale = getUrlParameter('locale');

  if(locale) {
    localStorage.locale = locale;
    fire({type: 'change_locale', locale});
  }

  if(app) {
    Cons.setAppStore({appInIframe: true});
    const decodeStr = decodeURIComponent(escape(window.atob(app)));
    const separator = decodeStr.slice(0,8);
    const authParams = decodeStr.split(separator);
    Cons.login({
      login: authParams[1].split('').reverse().join(''),
      password: authParams[2].split('').reverse().join('')
    });
  } else {
    fire({type: 'goto_page', pageCode: 'auth_page'}, 'root-panel');
  }

  if (!Cons.getAppStore().start_page_auth_listener) {
    addListener('auth_success', 'label-loading', authed => {
      const {login, password} = authed.creds;
      Cons.creds.login = login;
      Cons.creds.password = password;
      AS.apiAuth.setCredentials(login, password);
      AS.OPTIONS.login = login;
      AS.OPTIONS.password = password;
      AS.OPTIONS.currentUser = authed.data.person;
      fire({type: 'goto_page', pageCode: 'main_page'}, 'root-panel');
    });

    Cons.setAppStore({start_page_auth_listener: true});
  }
});
