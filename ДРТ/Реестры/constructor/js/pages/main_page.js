const treeExpandedIDs = [];

// Переменная для отслеживания текущих данных по дереву
let currentTreeData = []

const deleteItem = async item => {
  UIkit.modal.confirm(i18n.tr('Вы действительно удаляете фильтр?'),
    {labels: {ok: i18n.tr('Да'), cancel: i18n.tr('Отмена')}})
  .then(async () => {
    try {
      Cons.showLoader();

      const {id: filterID} = item.data;
      const delResult = await appAPI.filter.delete(filterID);
      if(!delResult) throw new Error('Произошла ошибка при удалении фильтра');

      initTree();

      Cons.hideLoader();
    } catch (err) {
      Cons.hideLoader();
      showMessage(i18n.tr(err.message), 'error');
      console.log(`ERROR [ deleteFilter ]: ${err.message}`);
    }
  }, () => null);
}

const getFilterContextMenu = () => {
  const menu = [];
  menu.push({
    name: i18n.tr('Добавить фильтр'),
    code: 'create_filter',
    icon: "plus",
    handler: (e, item) => new RegistryUserFilter(item.data, 'create', updateTree)
  });
  menu.push({
    name: i18n.tr('Редактировать фильтр'),
    code: 'edit_filter',
    icon: "pencil",
    handler: (e, item) => new RegistryUserFilter(item.data, 'edit', updateTree)
  });
  menu.push({divider: true}),
  menu.push({
    name: i18n.tr('Удалить фильтр'),
    code: 'delete_filter',
    icon: "trash",
    handler: (e, item) => deleteItem(item)
  });
  return menu;
}

const parseRegistryList = list => {
  const result = [];
  if(!list) return result;

  const parse = (data, r, registryCode = null) => {
    for(let i = 0; i < data.length; i++) {
      const item = data[i];
      const t = {};

      if(item.hasOwnProperty('consistOf')) {
        t.id = item.regGroupID;
        t.name = item.regGroupName;
        t.hasChildren = true;
        t.children = [];
        t.type = 'group';
        t.icon = 'folder';
        t.class = 'tree_group';
        parse(item.consistOf, t.children);
      } else if(item.hasOwnProperty('registryCode')) {
        t.id = item.registryID;
        t.name = item.registryName;
        t.hasChildren = item.filters.length ? true : false;
        t.children = [];
        t.registryCode = item.registryCode;
        t.rights = item.rights.uniq();
        t.type = 'registry';
        t.icon = 'folder_open';
        t.class = 'tree_registry';
        if(t.hasChildren) {
          parse(item.filters, t.children, t.registryCode);
          treeExpandedIDs.push(t.id);
        }

        t.contextMenu = true;
        t.contextMenuItems = [{
          name: i18n.tr('Добавить фильтр'),
          code: 'create_filter',
          icon: "plus",
          handler: (e, menuItem) => new RegistryUserFilter(menuItem.data, 'create', updateTree)
        }];

      } else {
        t.id = item.id;
        t.name = item.name;
        t.hasChildren = false;
        t.filterCode = item.code;
        t.filterType = item.type;
        t.registryCode = registryCode;
        t.parentID = item.parentID;
        t.rights = item.rights.uniq();
        t.type = 'filter';
        if(t.filterType == 'service') {
          t.icon = 'filter_alt';
          t.class = 'tree_filter tree_service_filter';
          t.contextMenu = false;
        } else {
          t.icon = 'star_border';
          t.class = 'tree_filter tree_user_filter';
          t.contextMenu = true;
          t.contextMenuItems = getFilterContextMenu();
        }
        t.hasChildren = item.children.length ? true : false;
        t.children = [];
        if(t.hasChildren) {
          parse(item.children, t.children, registryCode);
          treeExpandedIDs.push(t.id);
        }
      }

      r.push(t);
    }
  }

  parse(list, result);

  return result;
}

const getTreeData = async () => {
  const registryList = await appAPI.getRegistryListWithFilters();

  const treeData = [
    {
      id: 'root',
      name: 'Реестры',
      icon: 'account_tree',
      class: 'tree_root',
      hasChildren: true,
      children: parseRegistryList(registryList)
    }
  ];

  return treeData;
}

const initTree = async () => {
  const treeData = await getTreeData();

  // запись данных по дереву в переменную
  currentTreeData = treeData

  fire({type: 'tree_set_data', data: treeData}, 'treeRegistryList');
  fire({type: 'tree_set_expanded', expanded: ['root', ...treeExpandedIDs]}, 'treeRegistryList');
  fire({type: 'tree_item_click', item: treeData[0].children[0] || 'root', data: treeData}, 'treeRegistryList');
}

// Функция по парсингу сохраненного фильтра в тот же формат как и все элементы дерева
const parseRawFilter = (item) => {
  const newItem = {};

  newItem.id = item.filterID;
  newItem.name = item.name;
  newItem.hasChildren = false;
  newItem.registryCode = item.registryCode;
  newItem.parentID = item.registryID;

  newItem.filterCode = item.code;
  newItem.filterType = item.type;
  newItem.rights = item.rights.uniq();
  newItem.type = 'filter';
  if(newItem.filterType == 'service') {
    newItem.icon = 'filter_alt';
    newItem.class = 'tree_filter tree_service_filter';
    newItem.contextMenu = false;
  } else {
    newItem.icon = 'star_border';
    newItem.class = 'tree_filter tree_user_filter';
    newItem.contextMenu = true;
    newItem.contextMenuItems = getFilterContextMenu();
  }
  newItem.hasChildren = false;
  newItem.children = [];
  return newItem
}

// Рекурсивный поиск необходимого родительского элемента в дереве для добавления к нему фильтра
function findTreeElementById(tree, id) {
  if (tree.id === id) {
    return tree;
  }

  if (tree.children && tree.children.length > 0) {
    for (const child of tree.children) {
      const found = findTreeElementById(child, id);
      if (found) {
        return found;
      }
    }
  }

  return null;
}

// Функция для обновления дерева с динамическим добавлением/редактированием дочерних элементов дерева
const updateTree = (savedFilter, event, filterID) => {
  const filterItem = parseRawFilter(savedFilter)
  const parentItem = findTreeElementById(currentTreeData[0], filterItem.parentID)

  if(event == 'edit') {
    parentItem.children = parentItem.children.filter(({id}) => id !== filterID)
  }

  parentItem.children = [...parentItem.children, filterItem]
  parentItem.hasChildren = true

  fire({type: 'tree_set_data', data: currentTreeData}, 'treeRegistryList');
}

this.__initTree = initTree;

const getGroupIcon = type => {
  if(type == "registry") return '';
  return `<svg xmlns="http://www.w3.org/2000/svg" height="24px" viewBox="0 0 24 24" width="24px" fill="#D8D8D8"><path d="M0 0h24v24H0z" fill="none"/><path d="M10 4H4c-1.1 0-1.99.9-1.99 2L2 18c0 1.1.9 2 2 2h16c1.1 0 2-.9 2-2V8c0-1.1-.9-2-2-2h-8l-2-2z"/></svg>`;
}

const getExpandedIDs = (treeEl, treeData) => {
  const result = [...treeExpandedIDs];
  result.push(treeData[0].id);

  const s = data => {
    const f = data.children.find(x => x.id == treeEl.id);

    if(f) {
      if(data.id !== treeData[0].id) result.push(data.id);
      result.push(f.id);
    } else {
      for (let i = 0; i < data.children.length; i++) {
        const {hasChildren, children} = data.children[i];
        if(hasChildren && children && children.length) s(data.children[i]);
      }
    }
  }

  s(treeData[0]);

  return result;
}

const renderGroupAndRegistry = (treeEl, treeData) => {
  const container = $('#panelGroupAndRegistry');
  const table = $('<table>', {class: "uk-table uk-table-responsive reg-group-table"});
  const thead = $('<thead>');
  const tbody = $('<tbody>');

  const trHead = $('<tr>');
  trHead.append(
    `<th style="width: 40px;"></th>`,
    `<th>${i18n.tr("Группы реестров и реестры")}</th>`
  );

  thead.append(trHead);
  table.append(thead, tbody);

  container.empty();
  container.fadeIn();
  container.append(table);

  for(let i = 0; i < treeEl.children.length; i++) {
    const item = treeEl.children[i];
    const {name, type} = item;
    const tr = $('<tr>');
    tr.append(
      `<td>${getGroupIcon(type)}</td>`,
      `<td>${name}</td>`
    );
    tbody.append(tr);

    let timeoutId;

    tr.on('click', e => {
      timeoutId = setTimeout(() => {
        if(!timeoutId) return;
        tbody.find('tr').removeAttr('selected');
        tr.attr('selected', true);
      }, 200);
    });

    tr.on('dblclick', e => {
      clearTimeout(timeoutId);
      timeoutId = null;
      fire({type: 'tree_item_click', item, data: treeData}, 'treeRegistryList');
      fire({type: 'tree_set_expanded', expanded: getExpandedIDs(treeEl, treeData)}, 'treeRegistryList');
    });
  }
}

const getUrlParameter = param => {
  const url = new URLSearchParams(window.location.search);
  return url.has(param) ? url.get(param) : null;
}

pageHandler('main_page', () => {
  initTree();

  let registryCode = null;

  $('#searchInput').parent().hide();
  $('#customRegistryList').hide();
  $('#panelGroupAndRegistry').hide();
  $('#buttonGetXLS').hide();

  $('#searchInput').off().on('keyup', e => {
    if (e.keyCode === 13) {
      e.preventDefault();
      $('#customRegistryList').trigger({
        type: 'searchInRegistry',
        eventParam: {
          searchString: $('#searchInput').val()
        }
      });
    }
  });

  const updateContent = event => {
    const {item, data} = event;

    if(["filter", "registry"].includes(item.type)) {
      registryCode = item.registryCode;

      const eventParam = {registryCode};
      if(item.type == "filter") eventParam.filterID = item.id;

      //инициация кастомного компонента записей реестра
      $('#customRegistryList').trigger({type: 'renderNewTable', eventParam});

      $('#searchInput').parent().fadeIn();
      $('#customRegistryList').fadeIn();
      $('#panelGroupAndRegistry').hide();
      $('#buttonGetXLS').fadeIn();

    } else {
      registryCode = null;
      $('#searchInput').parent().hide();
      $('#customRegistryList').hide();
      $('#buttonGetXLS').hide();
      renderGroupAndRegistry(item, data);
    }

    $('#searchInput').val(null);
  }

  if (!Cons.getAppStore().main_page_listener) {

    //клик по элементу дерева
    addListener("tree_item_click", 'treeRegistryList', updateContent);

    //Кнопка обновить список записей реестра
    addListener('button_click', 'buttonRefreshRegList', e => {
      initTree();
    });

    //кнопка выгрузки в excel
    addListener('button_click', 'buttonGetXLS', e => {
      $('#customRegistryList').trigger({type: 'getXLS'});
    });

    //клик по записи реестра
    addListener('registry_item_click', 'customRegistryList', async e => {
      Cons.showLoader();

      const eventParam = {
        type: 'document',
        documentID: e.documentID,
        editable: false
      };
      $('#root-panel').trigger({type: 'custom_open_document', eventParam});
    });

    //меню-кнопка создать запись реестра
    addListener('click_create_new_row', 'buttonCreateRow', async e => {
      try {
        if(!registryCode) throw new Error(i18n.tr('Не выбран реестр'));

        Cons.showLoader();

        const regInfo = await appAPI.getRegistryInfo(registryCode);
        if(!regInfo) throw new Error(i18n.tr('При создании записи произошла ошибка. Не удалось плучить информацию по реестру.'));
        if(regInfo.rr_create != "Y") throw new Error(i18n.tr('У вас нет прав на создание записи'));

        const doc = await appAPI.createDoc(registryCode);
        if(!doc) throw new Error(i18n.tr('Произошла ошибка при создании данного типа документа'));

        $('#customRegistryList').trigger({type: 'updateTableBody'});

        Cons.hideLoader();

        const eventParam = {
          type: 'document',
          documentID: doc.documentID,
          editable: true
        };
        $('#root-panel').trigger({type: 'custom_open_document', eventParam});
      } catch (err) {
        showMessage(err.message, 'error');
        Cons.hideLoader();
      }
    });

    //меню-кнопка клик по пункту меню
    addListener('create_registry_row', 'buttonCreateRow', async e => {
      try {
        Cons.showLoader();

        const regInfo = await appAPI.getRegistryInfoByID(e.registryID);
        if(!regInfo) throw new Error(i18n.tr('При создании записи произошла ошибка. Не удалось плучить информацию по реестру.'));
        if(regInfo.rr_create != "Y") throw new Error(i18n.tr('У вас нет прав на создание записи'));

        const doc = await appAPI.createDocRegistryID(e.registryID);
        if(!doc) throw new Error(i18n.tr('Произошла ошибка при создании данного типа документа'));

        Cons.hideLoader();

        const eventParam = {
          type: 'document',
          documentID: doc.documentID,
          editable: true
        };
        $('#root-panel').trigger({type: 'custom_open_document', eventParam});
      } catch (err) {
        showMessage(err.message, 'error');
        Cons.hideLoader();
      }
    });

    Cons.setAppStore({main_page_listener: true});
  }

  const {appInIframe} = Cons.getAppStore();
  if(appInIframe) {
    $(`#buttonLogout`).hide();
    $(`#localeSelector`).hide();
  }

  const urlDocId = getUrlParameter('document_identifier');
  if(urlDocId) {
    $('#root-panel').trigger({
      type: 'custom_open_document',
      eventParam: {
        type: 'document',
        documentID: urlDocId
      }
    });
  }
});
