const getKeyInfo = async () => {
  return new Promise(async resolve => {
    try {
      const res = await fetch("https://127.0.0.1:8389/?TYPE=INFO");
      resolve(res.json());
    } catch (err) {
      resolve(null);
      console.log(`ERROR [ getKeyInfo ]: ${JSON.stringify(err.message)}`);
    }
  });
}

const signDoc = async (rawdata, alg) => {
  return new Promise(async resolve => {
    rest.custom(`https://127.0.0.1:8389/?TYPE=SIGN&DATA=${encodeURIComponent(rawdata)}&ALG=${alg}`, {}, resolve, err => {
      console.log(`ERROR [ signDoc ]: ${JSON.stringify(err)}`);
      resolve(null);
    });
  });
}

this.synergyAgent = {
  sign: async function(documentID) {
    return new Promise(async resolve => {
      try {
        const keyInfo = await getKeyInfo();
        if(!keyInfo) throw new Error('Проверьте, запущен ли SynergyAgent и выбран ли в нем ключ');

        const { CERT: pemCer, KEY_CN: edsInfo } = keyInfo;

        const verificationkey = await AS.FORMS.ApiUtils.simpleAsyncPost("rest/sign/verificationkey", null, "text", {
          uuid: AS.OPTIONS.currentUser.userid,
          pemCer, edsInfo
        });

        if(verificationkey === null || verificationkey.indexOf("::::") === -1)
        throw new Error('Произошла ошибка проверки ключа. Проверьте, запущен ли SynergyAgent и выбран ли в нем ключ');
        if(verificationkey === "CERT REVOKED") throw new Error('Сертификат отозван. Выберите в Synergy Agent действующий сертификат');
        if(verificationkey === "CERT END") throw new Error('Сертификат просрочен. Выберите в Synergy Agent действующий сертификат');

        const { rawdata } = await AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/docflow/doc/document_info?documentID=${documentID}`);

        const certID = verificationkey.split('::::')[0];
        const alg = verificationkey.split('::::')[1];

        // Подписание контрольной суммы
        const sign = await signDoc(rawdata, alg);
        if(!sign) throw new Error('Произошла ошибка при подписании документа');

        const {signedData, certificate, dataForSign} = sign;

        resolve({errorCode: 0, signedData, certificate, dataForSign, certID});
      } catch (err) {
        resolve({
          errorCode: 100500,
          errorMessage: err.message
        });
      }
    });
  }

}
