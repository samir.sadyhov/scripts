const updateTranslations = () => {
  const {translations} = Cons.getAppStore();
  i18n.locale = AS.OPTIONS.locale;
  return new Promise(resolve => {
    rest.synergyGet(`system/messages?localeID=${AS.OPTIONS.locale}`,
      res => {
        i18n.messages = res;
        if(translations && translations != 'not found') {
          for(const key in translations) i18n.messages[key] = translations[key][AS.OPTIONS.locale];
        }
        resolve(true);
      },
      err => {
        i18n.messages = null;
        resolve(true);
      }
    );
  });
}

//доп функции для переводов
const localized = () => {
  const {code} = Cons.getCurrentPage();

  updateTranslations().then(async () => {
    const systemSettings = await appAPI.getSystemSettings();
    Cons.setAppStore({systemSettings});

    switch (code) {
      case 'main_page': {
        __initTree();
        fire({type: 'wf_menu_translate'}, 'buttonCreateRow');
        break;
      }
    }
  });
}

$('#localeSelector').off().on('change', e => {
  const locale = $('#localeSelector').val();
  AS.OPTIONS.locale = locale;
  localStorage.locale = locale;
  localized();
});

if(localStorage.locale && localStorage.locale != AS.OPTIONS.locale) {
  let localeSelect = document.querySelector("#localeSelector");
  if(localeSelect) {
    localeSelect.value = localStorage.locale;
    localeSelect.dispatchEvent(new Event("change", {bubbles: true}));
  }
}
