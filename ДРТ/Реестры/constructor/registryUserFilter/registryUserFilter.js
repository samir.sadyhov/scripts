const ignoreComponents = ['label', 'textarea', 'custom', 'formula', 'counter', 'projectlink', 'repeater', 'docnumber', 'personlink', 'link', 'htd', 'signlist', 'resolutionlist', 'processlist', 'doclink', 'filelink', 'image', 'file'];

const RD = {
  paramSearch: {},

  paramFields: {
    registryRouteStatus: ['STATE_SUCCESSFUL', 'STATE_NOT_FINISHED', 'NO_ROUTE', 'STATE_UNSUCCESSFUL'],
    textbox: ['MORE', 'MORE_OR_EQUALS', 'LESS', 'LESS_OR_EQUALS', 'EQUALS', 'NOT_EQUALS', 'CONTAINS', 'NOT_CONTAINS', 'START', 'NOT_START', 'NOT_END', 'END', 'TEXT_NOT_EQUALS', 'TEXT_EQUALS'],
    numericinput: ['MORE', 'MORE_OR_EQUALS', 'LESS', 'LESS_OR_EQUALS', 'EQUALS', 'NOT_EQUALS'],
    date: ['MORE', 'MORE_OR_EQUALS', 'LESS', 'LESS_OR_EQUALS', 'EQUALS', 'NOT_EQUALS'],
    entity: {
      users: ['TEXT_EQUALS', 'TEXT_NOT_EQUALS', 'CONTAINS_USER', 'NOT_CONTAINS_USER'],
      positions: ['TEXT_EQUALS', 'TEXT_NOT_EQUALS', 'CONTAINS_USER_POSITION', 'NOT_CONTAINS_USER_POSITION'],
      departments: ['TEXT_EQUALS', 'TEXT_NOT_EQUALS', 'CONTAINS_USER_DEPARTMENT', 'CONTAINS_USER_DEPARTMENT_UP', 'CONTAINS_USER_DEPARTMENT_DOWN', 'NOT_CONTAINS_USER_DEPARTMENT_UP', 'NOT_CONTAINS_USER_DEPARTMENT_DOWN', 'NOT_CONTAINS_USER_DEPARTMENT']
    },
    reglink: ['CONTAINS', 'NOT_CONTAINS'],
    listbox: ['EQUALS'],
    check: ['CONTAINS'],
    radio: ['EQUALS']
  },

  paramName: {
    STATE_SUCCESSFUL: 'Активная',
    STATE_NOT_FINISHED: 'В процессе',
    NO_ROUTE: 'Подготовка',
    STATE_UNSUCCESSFUL: 'Неуспешная',

    MORE: '>',
    MORE_OR_EQUALS: '>=',
    LESS: '<',
    LESS_OR_EQUALS: '<=',
    EQUALS: '=',
    NOT_EQUALS: '<>',

    CONTAINS: 'Содержит',
    NOT_CONTAINS: 'Не содержит',
    START: 'Начинается с',
    NOT_START: 'Не начинается с',
    NOT_END: 'Не заканчивается на',
    END: 'Заканчивается на',
    TEXT_NOT_EQUALS: 'Не совпадает',
    TEXT_EQUALS: 'Совпадает',

    CONTAINS_USER: 'Содержит текущего пользователя',
    NOT_CONTAINS_USER: 'Не содержит текущего пользователя',

    CONTAINS_USER_POSITION: 'Содержит текущую должность',
    NOT_CONTAINS_USER_POSITION: 'Не содержит текущую должность',

    CONTAINS_USER_DEPARTMENT: 'Содержит текущее подразделение',
    CONTAINS_USER_DEPARTMENT_UP: 'Содержит текущее или любое выше подразделение',
    CONTAINS_USER_DEPARTMENT_DOWN: 'Содержит текущее или дочернее подразделение',
    NOT_CONTAINS_USER_DEPARTMENT: 'Не содержит текущее подразделение',
    NOT_CONTAINS_USER_DEPARTMENT_UP: 'Не содержит текущее или любое выше подразделение',
    NOT_CONTAINS_USER_DEPARTMENT_DOWN: 'Не содержит текущее или дочернее подразделение'
  },

  getListItems: function(component) {
    const {type, entity} = component;
    const types = entity ? this.paramFields[type][entity] : this.paramFields[type];
    component.listItems = types.map(value => ({label: i18n.tr(this.paramName[value]), value}));
  },

  getTemplateItem: function(){
    return {
      sourceID: null,
      sourceValue: null,
      typeSourceValue: null,
      matchingValue: null,
      matchingType: null,
      sourceType: null
    }
  }
};

//получение элементов справочника
const getDictionaryItems = async el => {
  let elements = [];
  if (el.hasOwnProperty('dataSource')) {
    const dict = await appAPI.getDictionary(el.dataSource.dict);
    for (let key in dict.items) {
      const item = dict.items[key];
      elements.push({
        label: item[el.dataSource.key].value,
        value: item[el.dataSource.value].value
      });
    }
  } else if(el.hasOwnProperty('elements')) {
    elements = [...el.elements];
  }
  return elements.sort((a,b) => a.value - b.value);
}

const generateGuid = () => 'R'+Math.random().toString(36).substring(2, 15)+Math.random().toString(36).substring(2, 15);

const createSelectComponent = (items, multiple) => {
  const select = $('<select>', {class: "uk-select", style: "min-width: 140px;"});
  if(multiple) select.attr('multiple', 'multiple');

  const item = items.find(x => x.value == select.val());
  if(item) select.attr('uk-tooltip', `title: ${item.label};`);

  items.sort((a,b) => a.value - b.value)
  .forEach(item => select.append(`<option value="${item.value}" title="${i18n.tr(item.label)}">${i18n.tr(item.label)}</option>`));

  select.on('change', () => {
    const item = items.find(x => x.value == select.val());
    if(item) select.attr('uk-tooltip', `title: ${item.label};`);
  });

  return select;
};

const createInputText = () => $(`<input type="text" class="uk-input" placeholder="${i18n.tr('Введите значение')}">`);
const createInputNumber = () => $(`<input type="number" class="uk-input" placeholder="${i18n.tr('Введите значение')}">`);
const createInputDate = () => $('<input type="date" class="uk-input" style="max-width: 160px;"><input type="time" class="uk-input" style="padding-left: 10px; max-width: 100px;" value="00:00">');

const createBlockParam = param => {
  const container = $('<div uk-grid style="margin: 0;"></div>');
  const items = RD.paramFields[param.type].map(x => ({label: RD.paramName[x], value: x}));
  const select = createSelectComponent(items);
  let input;
  switch (param.type) {
    case 'date': input = createInputDate(); break;
    case 'numericinput': input = createInputNumber(); break;
    default: input = createInputText(); break;
  }

  select.addClass('uk-width-auto').css({'padding-left': '10px', 'margin-right': '5px'});
  input.addClass('uk-width-expand').css('padding-left', '10px');
  container.append(select, input);

  return {container, select, input};
};

const createRegistryParamBlock = (component, uuidRow, filterItem) => {
  const button = $('<a class="uk-form-icon uk-form-icon-flip" href="javascript:void(0);" uk-icon="icon: list"></a>');
  const input = $('<input class="uk-input" type="text" style="background: #fff;" disabled>');
  const select = createSelectComponent(component.listItems);

  RD.paramSearch[uuidRow] = RD.getTemplateItem();
  RD.paramSearch[uuidRow].sourceID = component.id;
  RD.paramSearch[uuidRow].typeSourceValue = 'REGISTRY_RECORD';
  RD.paramSearch[uuidRow].matchingType = select.val();
  RD.paramSearch[uuidRow].sourceType = 'CMP_VALUE';

  select.on('change', () => RD.paramSearch[uuidRow].matchingType = select.val());

  if(filterItem) {
    select.val(filterItem.matchingType);
    RD.paramSearch[uuidRow].matchingType = filterItem.matchingType;
    RD.paramSearch[uuidRow].matchingValue = filterItem.matchingValue;

    AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/formPlayer/getDocMeaningContent?documentId=${filterItem.matchingValue}`,
      content => {
        input.val(content).attr('uk-tooltip', `title: ${content};`);
      }, 'text',  null,
      err => {
        console.log(`ERROR [ createRegistryParamBlock getDocMeaningContent ]: ${JSON.stringify(err)}`);
      }
    );
  }

  button.on('click', async e => {
    Cons.showLoader();
    const selectedIds = RD.paramSearch[uuidRow].hasOwnProperty('matchingValue') ? [RD.paramSearch[uuidRow].matchingValue] : null;
    const registryInfo = await appAPI.getRegistryInfoByID(component.registryID);
    if(!registryInfo.hasOwnProperty('registryCustomFilters')) registryInfo.registryCustomFilters = [];
    Cons.hideLoader();

    //(registry, multi, selectedIds, handler)
    AS.SERVICES.showRegistryLinkDialog(registryInfo, false, selectedIds, async docIDs => {
      RD.paramSearch[uuidRow].matchingValue = docIDs.toString();
      const content = await AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/formPlayer/getDocMeaningContent?documentId=${RD.paramSearch[uuidRow].matchingValue}`, null, 'text');
      input.val(content).attr('uk-tooltip', `title: ${content};`);
    });
  });

  return $('<div class="uk-grid-small" uk-grid>')
  .append($('<div class="uk-width-1-4@s">').append(select))
  .append($('<div class="uk-inline uk-width-3-4@s">').append(button, input));
}

const createUserParamBlock = (component, uuidRow, filterItem) => {
  const button = $('<a class="uk-form-icon uk-form-icon-flip" href="javascript:void(0);" uk-icon="icon: users"></a>');
  const input = $('<input class="uk-input" type="text" style="background: #fff;" disabled>');
  const select = createSelectComponent(component.listItems);

  RD.paramSearch[uuidRow] = RD.getTemplateItem();
  RD.paramSearch[uuidRow].sourceID = component.id;
  RD.paramSearch[uuidRow].typeSourceValue = 'ENTITY';
  RD.paramSearch[uuidRow].matchingType = select.val();
  RD.paramSearch[uuidRow].sourceType = 'CMP_VALUE';
  RD.paramSearch[uuidRow].users = null;
  RD.paramSearch[uuidRow].matchingValue = '';

  if(filterItem) {
    select.val(filterItem.matchingType);
    RD.paramSearch[uuidRow].matchingType = filterItem.matchingType;
    RD.paramSearch[uuidRow].matchingValue = filterItem.matchingValue;

    if(filterItem.matchingValue && filterItem.matchingValue != '') {
      if(!["CONTAINS_USER", "NOT_CONTAINS_USER"].includes(RD.paramSearch[uuidRow].matchingType)) {
        const urlParams = JSON.parse(filterItem.matchingValue).map(x => x.value).join('&userID=');
        AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/userchooser/getUserInfo?userID=${urlParams}`).then(res => {
          RD.paramSearch[uuidRow].users = res;
          const userNames = res.map(x => x.personName).join('; ');
          input.val(userNames).attr('title', userNames);
        });
      }
    }
  }

  select.on('change', () => {
    RD.paramSearch[uuidRow].matchingType = select.val();
    if(["CONTAINS_USER", "NOT_CONTAINS_USER"].includes(RD.paramSearch[uuidRow].matchingType)) {
      RD.paramSearch[uuidRow].users = null;
      RD.paramSearch[uuidRow].matchingValue = '';
      input.val(null).attr('title', null).css('background', '#f8f8f8');
    } else {
      input.css('background', '#fff');
    }
  });

  button.on('click', e => {
    if(["CONTAINS_USER", "NOT_CONTAINS_USER"].includes(RD.paramSearch[uuidRow].matchingType)) return;
    const values = RD.paramSearch[uuidRow].users;
    //(values, multiSelectable, isGroupSelectable, showWithoutPosition, filterPositionID, filterDepartmentID, locale, handler)
    AS.SERVICES.showUserChooserDialog(values, true, false, false, null, null, AS.OPTIONS.locale, users => {
      const userNames = users.map(x => x.personName).join('; ');
      input.val(userNames).attr('title', userNames);
      RD.paramSearch[uuidRow].users = users;
      RD.paramSearch[uuidRow].matchingValue = JSON.stringify(users.map(x => ({value: x.personID})));
    });
  });

  return $('<div class="uk-grid-small" uk-grid>')
  .append($('<div class="uk-width-1-3@s">').append(select))
  .append($('<div class="uk-inline uk-width-2-3@s">').append(button, input));
}

const createDepParamBlock = (component, uuidRow, filterItem) => {
  const button = $('<a class="uk-form-icon uk-form-icon-flip" href="javascript:void(0);" uk-icon="icon: more"></a>');
  const input = $('<input class="uk-input" type="text" style="background: #fff;" disabled>');
  const select = createSelectComponent(component.listItems);

  RD.paramSearch[uuidRow] = RD.getTemplateItem();
  RD.paramSearch[uuidRow].sourceID = component.id;
  RD.paramSearch[uuidRow].typeSourceValue = 'ENTITY';
  RD.paramSearch[uuidRow].matchingType = select.val();
  RD.paramSearch[uuidRow].sourceType = 'CMP_VALUE';
  RD.paramSearch[uuidRow].departments = null;
  RD.paramSearch[uuidRow].matchingValue = '';

  const listBlock = ['CONTAINS_USER_DEPARTMENT', 'CONTAINS_USER_DEPARTMENT_UP', 'CONTAINS_USER_DEPARTMENT_DOWN', 'NOT_CONTAINS_USER_DEPARTMENT', 'NOT_CONTAINS_USER_DEPARTMENT_UP', 'NOT_CONTAINS_USER_DEPARTMENT_DOWN'];

  if(filterItem) {
    select.val(filterItem.matchingType);
    RD.paramSearch[uuidRow].matchingType = filterItem.matchingType;
    RD.paramSearch[uuidRow].matchingValue = filterItem.matchingValue;

    if(filterItem.matchingValue && filterItem.matchingValue != '') {
      if(!listBlock.includes(RD.paramSearch[uuidRow].matchingType)) {
        const urlParams = JSON.parse(filterItem.matchingValue).map(x => x.value).join('&departmentID=');
        AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/userchooser/getDepartmentInfo?departmentID=${urlParams}`).then(res => {
          RD.paramSearch[uuidRow].departments = res;
          const depNames = res.map(x => x.departmentName).join('; ');
          input.val(depNames).attr('title', depNames);
        });
      }
    }
  }

  select.on('change', () => {
    RD.paramSearch[uuidRow].matchingType = select.val();
    if(listBlock.includes(RD.paramSearch[uuidRow].matchingType)) {
      RD.paramSearch[uuidRow].departments = null;
      RD.paramSearch[uuidRow].matchingValue = '';
      input.val(null).attr('title', null).css('background', '#f8f8f8');
    } else {
      input.css('background', '#fff');
    }
  });

  button.on('click', e => {
    if(listBlock.includes(RD.paramSearch[uuidRow].matchingType)) return;
    const values = RD.paramSearch[uuidRow].departments;
    //(values, multiSelectable, filterUserID, filterPositionID, filterDepartmentID, filterChildDepartmentID, locale, handler)
    AS.SERVICES.showDepartmentChooserDialog(values, true, null, null, null, null, AS.OPTIONS.locale, departments => {
      const depNames = departments.map(x => x.departmentName).join('; ');
      input.val(depNames).attr('title', depNames);
      RD.paramSearch[uuidRow].departments = departments;
      RD.paramSearch[uuidRow].matchingValue = JSON.stringify(departments.map(x => ({value: x.departmentId})));
    });
  });

  return $('<div class="uk-grid-small" uk-grid>')
  .append($('<div class="uk-width-1-3@s">').append(select))
  .append($('<div class="uk-inline uk-width-2-3@s">').append(button, input));
}

const createPositionParamBlock = (component, uuidRow, filterItem) => {
  const button = $('<a class="uk-form-icon uk-form-icon-flip" href="javascript:void(0);" uk-icon="icon: more"></a>');
  const input = $('<input class="uk-input" type="text" style="background: #fff;" disabled>');
  const select = createSelectComponent(component.listItems);

  RD.paramSearch[uuidRow] = RD.getTemplateItem();
  RD.paramSearch[uuidRow].sourceID = component.id;
  RD.paramSearch[uuidRow].typeSourceValue = 'ENTITY';
  RD.paramSearch[uuidRow].matchingType = select.val();
  RD.paramSearch[uuidRow].sourceType = 'CMP_VALUE';
  RD.paramSearch[uuidRow].positions = null;
  RD.paramSearch[uuidRow].matchingValue = '';

  if(filterItem) {
    select.val(filterItem.matchingType);
    RD.paramSearch[uuidRow].matchingType = filterItem.matchingType;
    RD.paramSearch[uuidRow].matchingValue = filterItem.matchingValue;

    if(filterItem.matchingValue && filterItem.matchingValue != '') {
      if(!["CONTAINS_USER_POSITION", "NOT_CONTAINS_USER_POSITION"].includes(RD.paramSearch[uuidRow].matchingType)) {
        const urlParams = JSON.parse(filterItem.matchingValue).map(x => x.value).join('&positionID=');
        AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/userchooser/getPositionInfo?positionID=${urlParams}`).then(res => {
          RD.paramSearch[uuidRow].positions = res;
          const posNames = res.map(x => x.elementName).join('; ');
          input.val(posNames).attr('title', posNames);
        });
      }
    }
  }

  select.on('change', () => {
    RD.paramSearch[uuidRow].matchingType = select.val();
    if(["CONTAINS_USER_POSITION", "NOT_CONTAINS_USER_POSITION"].includes(RD.paramSearch[uuidRow].matchingType)) {
      RD.paramSearch[uuidRow].positions = null;
      RD.paramSearch[uuidRow].matchingValue = '';
      input.val(null).attr('title', null).css('background', '#f8f8f8');
    } else {
      input.css('background', '#fff');
    }
  });

  button.on('click', e => {
    if(["CONTAINS_USER_POSITION", "NOT_CONTAINS_USER_POSITION"].includes(RD.paramSearch[uuidRow].matchingType)) return;
    const values = RD.paramSearch[uuidRow].positions;
    //(values, multiSelect, filterUserId, filterDepartmentId, showVacant, locale, handler)
    AS.SERVICES.showPositionChooserDialog(values, true, null, null, null, AS.OPTIONS.locale, positions => {
      const posNames = positions.map(x => x.elementName).join('; ');
      input.val(posNames).attr('title', posNames);
      RD.paramSearch[uuidRow].positions = positions;
      RD.paramSearch[uuidRow].matchingValue = JSON.stringify(positions.map(x => ({value: x.elementID})));
    });
  });

  return $('<div class="uk-grid-small" uk-grid>')
  .append($('<div class="uk-width-1-3@s">').append(select))
  .append($('<div class="uk-inline uk-width-2-3@s">').append(button, input));
}

this.RegistryUserFilter = class {
  constructor(item, event, saveSuccessHandler){
    this.event = event;
    this.item = item;
    this.paramList = [];
    this.saveSuccessHandler = saveSuccessHandler;

    this.init();
  }

  getModal() {
    const dialog = $('<div class="uk-flex-top" uk-modal="bg-close: false; esc-close: false;">');
    const md = $('<div>', {class: 'uk-modal-dialog uk-margin-auto-vertical', style: 'width: 700px; height: 700px;'});
    const modalBody = $('<div>', {class: 'uk-modal-body reg_filter_modal_body'});
    const footer = $('<div>', {class: 'uk-modal-footer reg_filter_modal_footer'});
    const panelActions = $('<div>', {class: 'reg_filter_modal_panel_actions'});
    const panelContent = $('<div>', {class: 'reg_filter_modal_panel_items'});
    this.inputFilterName = $('<input>', {class: 'uk-input', type: 'text', placeholder: i18n.tr('Наименование')});

    const tableItems = $('<table>', {class: "uk-table uk-table-justify uk-table-divider uk-table-middle uk-table-small"});
    this.panelItems = $('<tbody>');
    tableItems.append(this.panelItems);

    if(this.event == 'edit') this.inputFilterName.val(this.filterInfo.name);

    const selectItems = this.paramList.map(x => ({title: x.label, value: x.id}));
    this.selectParamList = new CustomSelect({
      items: selectItems,
      placeholder: 'Добавить условие',
      search: true
    });
    panelActions.append(this.inputFilterName, this.selectParamList.container);

    this.buttonSaveFilter = $(`<button class="uk-button uk-button-primary uk-button-small" type="button">${i18n.tr("Сохранить")}</button>`);

    panelContent.append(tableItems);
    modalBody.append(panelActions, panelContent);
    footer.append(this.buttonSaveFilter);

    md.append(`<button class="uk-modal-close-default modal-close-custom" type="button" uk-close></button>`,
    `<div class="uk-modal-header"><h3>${i18n.tr("Редактирование фильтра")}</h3></div>`, modalBody, footer);
    dialog.append(md);

    return dialog;
  }

  async parseFormData(){
    const firstItem = {
      id: 'registryRecordStatus',
      type: 'registryRouteStatus',
      label: i18n.tr('Статус записи реестра')
    };
    RD.getListItems(firstItem);
    this.paramList.push(firstItem);

    const parse = async properties => {
      for(let i = 0; i < properties.length; i++) {
        const item = properties[i];
        if(ignoreComponents.includes(item.type)) continue;
        if(item.type == "table") {
          await parse(item.properties);
        } else {
          const component = {
            id: item.id,
            label: item.id,
            type: item.type
          }
          const column = this.regInfo.columns.find(x => x.columnID == item.id);
          if(column && column.label && column.label != '') component.label = column.label;

          switch (item.type) {
            case 'reglink': component.registryID = item.config.dateFormat; break;
            case 'entity': component.entity = item.config.entity; break;
            case 'listbox':
            case 'check':
            case 'radio':
              component.elements = await getDictionaryItems(item); break;
          }

          RD.getListItems(component);
          this.paramList.push(component);
        }
      }
    }

    await parse(this.form.properties);
  }

  checkFilterItems(){
    const result = {errorCode: 0, errorMessage: ''};
    const errors = new Set();

    const filterName = this.inputFilterName.val();
    if(!filterName || filterName.trim() == '') {
      this.inputFilterName.addClass('uk-form-danger');
      errors.add(i18n.tr('Не заполнено наименование фильтра'));
      result.errorCode++;
    }

    if(!Object.keys(RD.paramSearch).length) {
      errors.add(i18n.tr('Фильтр не должен быть пустым'));
      result.errorCode++;
    }

    const entityIgnoreTypes = ["CONTAINS_USER", "NOT_CONTAINS_USER", "CONTAINS_USER_POSITION", "NOT_CONTAINS_USER_POSITION", 'CONTAINS_USER_DEPARTMENT', 'CONTAINS_USER_DEPARTMENT_UP', 'CONTAINS_USER_DEPARTMENT_DOWN', 'NOT_CONTAINS_USER_DEPARTMENT', 'NOT_CONTAINS_USER_DEPARTMENT_UP', 'NOT_CONTAINS_USER_DEPARTMENT_DOWN'];

    for(const uuidRow in RD.paramSearch) {
      const item = RD.paramSearch[uuidRow];
      if(item.typeSourceValue == 'ENTITY') {
        if(!entityIgnoreTypes.includes(item.matchingType)) {
          if(!item.matchingValue || item.matchingValue == '' || item.matchingValue == "[]") {
            errors.add(i18n.tr('Не введены значения условия фильтра'));
            result.errorCode++;
          }
        }
      } else {
        if(!item.matchingValue || item.matchingValue == '' || item.matchingValue == "[]") {
          errors.add(i18n.tr('Не введены значения условия фильтра'));
          result.errorCode++;
        }
      }
    }

    if(result.errorCode != 0) result.errorMessage = [...errors].join('<br>');

    return result;
  }

  getParamSave(){
    const param = {
      filterName: this.inputFilterName.val(),
      parentCode: null,
      regFilterRights: {},
      registryCode: this.regInfo.code,
      conditions: []
    }

    if(this.filterInfo) {
      if(this.event == 'edit') {
        param.parentFilterId = this.filterInfo.parentID;
      } else {
        param.parentFilterId = this.filterInfo.id;
      }
    }

    for(const uuidRow in RD.paramSearch) {
      const item = {};

      for(const key in RD.paramSearch[uuidRow]) {
        if(!['users', 'departments', 'positions'].includes(key)) {
          item[key] = RD.paramSearch[uuidRow][key];
        }
      }
      param.conditions.push(item);
    }

    return param;
  }

  async saveFilter(){
    Cons.showLoader();
    try {
      const itemsError = this.checkFilterItems();

      if(itemsError.errorCode != 0) throw new Error(itemsError.errorMessage);

      let filterID;

      if(this.filterInfo && this.event == 'edit') {
        filterID = this.filterInfo.id;
        const delResult = await appAPI.filter.delete(this.filterInfo.id);
        if(!delResult) throw new Error('Произошла ошибка сохранения фильтра');
      }

      const params = this.getParamSave();
      const resultSave = await appAPI.filter.save(params);
      if(resultSave.hasOwnProperty('errorCode') && resultSave.errorCode != 0) throw new Error(resultSave.errorMessage);

      // Передаем сохраненный фильтр, также event и удаленный фильтр (для случаев с edit)
      if(this.saveSuccessHandler) this.saveSuccessHandler(resultSave, this.event, filterID);

      Cons.hideLoader();
      UIkit.modal(this.dialog).hide();
    } catch (err) {
      UIkit.notification.closeAll();
      showMessage(i18n.tr(err.message), 'error');
      Cons.hideLoader();
      console.log(err);
    }
  }

  async addParamRow(val, filterItem = null){
    const component = this.paramList.find(x => x.id == val);

    if(!component) return;

    const uuidRow = generateGuid();
    const tr = $('<tr>');
    const tdFilter = $('<td>');
    const deleteRow = $('<a href="#" uk-icon="trash"></a>');

    deleteRow.on('click', e => {
      e.preventDefault();
      e.stopPropagation();
      tr.remove();
      delete RD.paramSearch[uuidRow];
    });

    switch (component.type) {
      case 'registryRouteStatus': {
        const select = createSelectComponent(component.listItems, true);
        const values = select.val() || [];

        RD.paramSearch[uuidRow] = RD.getTemplateItem();
        RD.paramSearch[uuidRow].typeSourceValue = 'SET';
        RD.paramSearch[uuidRow].matchingType = 'EQUALS';
        RD.paramSearch[uuidRow].sourceType = 'STATUS';
        RD.paramSearch[uuidRow].matchingValue = JSON.stringify(values.map(x => ({value: x})));

        if(filterItem) {
          const matchingValue = JSON.parse(filterItem.matchingValue);
          select.val(matchingValue.map(x => x.value));
          RD.paramSearch[uuidRow].matchingValue = filterItem.matchingValue;
        }

        select.on('change', () => {
          const values = select.val() || [];
          RD.paramSearch[uuidRow].matchingValue = JSON.stringify(values.map(x => ({value: x})));
        });

        tdFilter.append(select);
        break;
      }
      case 'listbox':
      case 'radio':
      case 'check': {
        const select = createSelectComponent(component.elements, true);
        const values = select.val() || [];

        RD.paramSearch[uuidRow] = RD.getTemplateItem();
        RD.paramSearch[uuidRow].sourceID = component.id;
        RD.paramSearch[uuidRow].typeSourceValue = 'SET';
        RD.paramSearch[uuidRow].matchingType = 'LIKE';
        RD.paramSearch[uuidRow].sourceType = 'CMP_VALUE';
        RD.paramSearch[uuidRow].matchingValue = JSON.stringify(values.map(x => ({value: x})));

        if(filterItem) {
          const matchingValue = JSON.parse(filterItem.matchingValue);
          select.val(matchingValue.map(x => x.value));
          RD.paramSearch[uuidRow].matchingValue = filterItem.matchingValue;
        }

        select.on('change', () => {
          const values = select.val() || [];
          RD.paramSearch[uuidRow].matchingValue = JSON.stringify(values.map(x => ({value: x})));
        });

        tdFilter.append(select);
        break;
      }
      case 'date': {
        const {container, select} = createBlockParam(component);
        const inputDate = container.find('input[type="date"]');
        const inputTime = container.find('input[type="time"]');

        RD.paramSearch[uuidRow] = RD.getTemplateItem();
        RD.paramSearch[uuidRow].sourceID = component.id;
        RD.paramSearch[uuidRow].typeSourceValue = 'DATE';
        RD.paramSearch[uuidRow].matchingType = select.val();
        RD.paramSearch[uuidRow].sourceType = 'CMP_VALUE';
        RD.paramSearch[uuidRow].matchingValue = new Date(`${inputDate.val()} ${inputTime.val()}:00`).getTime().toString();

        if(filterItem) {
          select.val(filterItem.matchingType);
          RD.paramSearch[uuidRow].matchingValue = filterItem.matchingValue;
          RD.paramSearch[uuidRow].matchingType = filterItem.matchingType;

          const matchingValue = AS.FORMS.DateUtils.formatDate(new Date(Number(filterItem.matchingValue)), AS.FORMS.DateUtils.DATE_FORMAT_FULL).split(' ');
          inputDate.val(matchingValue[0]);
          inputTime.val(matchingValue[1].slice(0,5));
        }

        select.on('change', () => RD.paramSearch[uuidRow].matchingType = select.val());
        inputDate.on('change', () => RD.paramSearch[uuidRow].matchingValue = new Date(`${inputDate.val()} ${inputTime.val()}:00`).getTime().toString());
        inputTime.on('change', () => RD.paramSearch[uuidRow].matchingValue = new Date(`${inputDate.val()} ${inputTime.val()}:00`).getTime().toString());

        tdFilter.append(container);
        break;
      }
      case 'textbox': {
        const {container, select} = createBlockParam(component);
        const inputText = container.find('input[type="text"]');

        RD.paramSearch[uuidRow] = RD.getTemplateItem();
        RD.paramSearch[uuidRow].sourceID = component.id;
        RD.paramSearch[uuidRow].typeSourceValue = 'NUMBER';
        RD.paramSearch[uuidRow].matchingType = select.val();
        RD.paramSearch[uuidRow].sourceType = 'CMP_VALUE';
        RD.paramSearch[uuidRow].matchingValue = inputText.val();

        if(filterItem) {
          select.val(filterItem.matchingType);
          RD.paramSearch[uuidRow].typeSourceValue = filterItem.typeSourceValue;
          RD.paramSearch[uuidRow].matchingType = filterItem.matchingType;
          RD.paramSearch[uuidRow].matchingValue = filterItem.matchingValue;
          inputText.val(filterItem.matchingValue);

          if(RD.paramSearch[uuidRow].typeSourceValue == 'NUMBER') {
            inputText.off().on('change keypress', () => {
              inputText.val(inputText.val().replace(/[^\d]/g,''));
              RD.paramSearch[uuidRow].matchingValue = inputText.val();
            });
          } else if (RD.paramSearch[uuidRow].typeSourceValue == 'STRING') {
            inputText.off().on('change keypress', () => {
              RD.paramSearch[uuidRow].matchingValue = inputText.val();
            });
          }
        } else {
          inputText.on('change keypress', () => {
            inputText.val(inputText.val().replace(/[^\d]/g,''));
            RD.paramSearch[uuidRow].matchingValue = inputText.val();
          });
        }

        select.on('change', () => {
          RD.paramSearch[uuidRow].matchingType = select.val();
          if(RD.paramFields.numericinput.includes(RD.paramSearch[uuidRow].matchingType)) {
            RD.paramSearch[uuidRow].typeSourceValue = 'NUMBER';
            inputText.val(null);
            inputText.off().on('change keypress', () => {
              inputText.val(inputText.val().replace(/[^\d]/g,''));
              RD.paramSearch[uuidRow].matchingValue = inputText.val();
            });
          } else {
            RD.paramSearch[uuidRow].typeSourceValue = 'STRING';
            inputText.off().on('change keypress', () => {
              RD.paramSearch[uuidRow].matchingValue = inputText.val();
            });
          }
        });

        tdFilter.append(container);
        break;
      }
      case 'numericinput': {
        const {container, select} = createBlockParam(component);
        const inputText = container.find('input[type="number"]');

        RD.paramSearch[uuidRow] = RD.getTemplateItem();
        RD.paramSearch[uuidRow].sourceID = component.id;
        RD.paramSearch[uuidRow].typeSourceValue = 'NUMBER';
        RD.paramSearch[uuidRow].matchingType = select.val();
        RD.paramSearch[uuidRow].sourceType = 'CMP_VALUE';
        RD.paramSearch[uuidRow].matchingValue = '';

        if(filterItem) {
          select.val(filterItem.matchingType);
          RD.paramSearch[uuidRow].matchingType = filterItem.matchingType;
          RD.paramSearch[uuidRow].matchingValue = filterItem.matchingValue;
          inputText.val(filterItem.matchingValue);
        }

        inputText.on('change keypress', () => RD.paramSearch[uuidRow].matchingValue = inputText.val());
        select.on('change', () => RD.paramSearch[uuidRow].matchingType = select.val());

        tdFilter.append(container);
        break;
      }
      case 'reglink': tdFilter.append(createRegistryParamBlock(component, uuidRow, filterItem)); break;
      case 'entity':
        switch (component.entity) {
          case 'users': tdFilter.append(createUserParamBlock(component, uuidRow, filterItem)); break;
          case 'departments': tdFilter.append(createDepParamBlock(component, uuidRow, filterItem)); break;
          case 'positions': tdFilter.append(createPositionParamBlock(component, uuidRow, filterItem)); break;
        }
        break;
    }

    tr.append(
      `<td class="uk-form-label uk-text-default uk-width-small uk-text-truncate" style="min-width: 200px;" uk-tooltip="${i18n.tr(component.label)}">${i18n.tr(component.label)}</td>`,
      tdFilter,
      $(`<td uk-tooltip="${i18n.tr('Удалить условие')}" style="width: 30px; text-align: end;">`).append(deleteRow)
    );

    this.panelItems.append(tr);
  }

  addButtonListener(){
    this.buttonSaveFilter.on('click', e => {
      e.preventDefault();
      this.saveFilter();
    });

    this.selectParamList.container.on('selected', e => {
      this.addParamRow(e.eventParam.value);
      this.selectParamList.value = null;
    });

    this.inputFilterName.on('change keypress', () => {
      this.inputFilterName.removeClass('uk-form-danger');
    });
  }

  addDialogListener(){
    this.dialog.on('shown', () => {
      this.addButtonListener();
    });

    this.dialog.on('hidden', () => {
      this.dialog.remove();
    });
  }

  renderFilterItems(){
    for(let i = 0; i < this.filterInfo.items.length; i++) {
      const filterItem = this.filterInfo.items[i];
      const sourceID = filterItem.sourceType == "STATUS" ? 'registryRecordStatus' : filterItem.sourceID;
      this.addParamRow(sourceID, filterItem);
    }
  }

  async init(){
    try {
      Cons.showLoader();

      RD.paramSearch = {};

      this.regInfo = await appAPI.getRegistryInfo(this.item.registryCode);
      this.form = await appAPI.getAsfDefinition(this.regInfo.formId);

      if(this.item.type == 'filter') {
        this.filterID = this.item.id;
        this.parentID = this.item.parentID;
        this.filterInfo = await appAPI.filter.info(this.filterID);
      }

      await this.parseFormData();

      Cons.hideLoader();

      this.dialog = this.getModal();
      UIkit.modal(this.dialog).show();
      this.addDialogListener();

      if(this.event == 'edit') this.renderFilterItems();

    } catch (err) {
      showMessage(err.message, 'error');
      Cons.hideLoader();
      console.log(err);
    }
  }
}
