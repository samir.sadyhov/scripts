/*настройки скрипта в таблице

setTimeout(() => {
  view.container.synergyPaginationTable({
    perPage: 10, // кол-во отображаемых записей
    model: model, // модель таблицы обязательно, если не заблокировано добавление/удаление блоков
    header: true, // если есть заголовок таблицы то обязательно передавать true
    editable: editable // если header == true, передавать editable
  });
}, 100);
*/

(function($) {
  $.fn.synergyPaginationTable = function(options) {

    let defaults = {perPage: 5, currentPage: 1, header: false};
    let settings = $.extend({}, defaults, options);
    let table = $(this);

    return table.each(function() {
      let $rows = $('tbody tr', table);
      let pages = Math.ceil($rows.length / settings.perPage);
      let container = $('<div class="spt-container">');
      let paginator = $('<div class="spt-paginator">');
      let pContent = $('<div class="spt-paginator-content">');
      let bPrevious = $('<button class="spt-previous" disabled="disabled">');
      let bNext = $('<button class="spt-next" disabled="disabled">');
      let label = $('<label>');
      let input = $('<input type="text">');

      if(settings.header) {
        $('thead', table).append($($('tbody tr', table)[0]));

        if(settings.editable !== undefined) {
          let thead = $('thead', table);
          thead.find("td")
          .addClass("spt-header")
          .eq(0).find('.asf-label').addClass("spt-sorted-desc");

          thead.find("td").on("click", function () {
            thead.find("td").not($(this)).find('.asf-label').removeClass("spt-sorted-asc spt-sorted-desc");
            if ($(this).find('.asf-label').hasClass("spt-sorted-asc") || $(this).find('.asf-label').hasClass("spt-sorted-desc")) {
              $(this).find('.asf-label').toggleClass("spt-sorted-asc spt-sorted-desc");
            } else {
              $(this).find('.asf-label').addClass("spt-sorted-asc");
            }
            thead.find("tbody tr").show();
            sortTable($(this).index(), $(this).find('.asf-label').hasClass("spt-sorted-asc") ? "asc" : "desc");
          });
          sortTable(0, "desc");
          thead.find('.asf-tableDeleteCell.spt-header').removeClass('spt-header');
        }

        $rows = $('tbody tr', table);
        pages = Math.ceil($rows.length / settings.perPage);
      }

      pContent.append(label).append(input);
      paginator.append(bPrevious).append(pContent).append(bNext);
      container.append(paginator);
      table.after(container);
      update();

      bNext.click(() => {
        settings.currentPage++;
        update();
      });

      bPrevious.click(() => {
        settings.currentPage--;
        update();
      });

      label.on('dblclick', e => {
        e.preventDefault();
        input.show();
        label.hide();

        input.on('blur', () => {
          label.show();
          input.hide();
        });

        input.val("" + settings.currentPage);

        input.on('keypress', e => {
          if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) return false;
        });

        input.on('keydown', e => {
          if(e.which === 13) {
            let inputVal = +input.val();
            if(inputVal && inputVal != settings.currentPage && inputVal <= pages && inputVal >= 1){
              settings.currentPage = inputVal;
              update();
            }
            label.show();
            input.hide();
            input.off();
          }
        });
        input.focus();
      });

      if(settings.model) {
        settings.model.on('tableRowDelete', () => {
          $rows = $('tbody tr', table);
          pages = Math.ceil($rows.length / settings.perPage);
          if(settings.currentPage > pages) settings.currentPage--;
          update();
        });
        settings.model.on('tableRowAdd', () => {
          $rows = $('tbody tr', table);
          pages = Math.ceil($rows.length / settings.perPage);
          settings.currentPage = pages;
          update();
        });
      }

      function update() {
        $rows = $('tbody tr', table);
        pages = Math.ceil($rows.length / settings.perPage);

        let from = ((settings.currentPage - 1) * settings.perPage) + 1;
        let to = from + settings.perPage - 1;

        if (to > $rows.length) to = $rows.length;

        $rows.hide();
        $rows.slice((from - 1), to).show();

        label.text(settings.currentPage + ' / ' + pages);

        if (settings.currentPage == 1) {
          bPrevious.attr('disabled', 'disabled');
          bNext.removeAttr('disabled');
        } else {
          bPrevious.removeAttr('disabled');
          if (settings.currentPage == pages) {
            bNext.attr('disabled', 'disabled');
          } else {
            bNext.removeAttr('disabled');
          }
        }

        if ($rows.length <= settings.perPage) {
          container.hide();
        } else {
          container.show();
        }
      }

      function parseDateTime(datetime) {
        let d = datetime.split(/\D/);
        if (datetime.indexOf('-') === 4) {
          return new Date(('20' + d[0]).slice(-4), d[1] - 1, d[2], d[3] || 0, d[4] || 0, d[5] || 0);
        } else if (datetime.indexOf('.') === 2) {
          return new Date(('20' + d[2]).slice(-4), d[1] - 1, d[0], 0, 0, 0);
        }
        return datetime;
      }

      //return: 1 - number; 2 - date; 3 - string;
      function getFieldType(str) {
        if (isFinite(str)) return 1;
        if (str.indexOf('-') === 4 || str.indexOf('.') === 2) {
          return parseDateTime(str) != "Invalid Date" ? 2 : 3;
        }
        return 3;
      }

      function sortTable(column, order) {
        let asc = order === 'asc';
        let tbody = table.find('tbody');
        tbody.find('tr').sort((a, b) => {
          let tmpA = $('td:eq(' + column + ')', a).find('div.asf-container > div:nth-child(2)').text();
          let tmpB = $('td:eq(' + column + ')', b).find('div.asf-container > div:nth-child(2)').text();
          if(settings.editable) {
            tmpA = $('td:eq(' + column + ')', a).find('input').val();
            tmpB = $('td:eq(' + column + ')', b).find('input').val();
          }
          switch (getFieldType(tmpA)) {
            case 1:
              return asc ? parseInt(tmpA) - parseInt(tmpB) : parseInt(tmpB) - parseInt(tmpA);
              break;
            case 2:
              return asc ? parseDateTime(tmpA) - parseDateTime(tmpB) : parseDateTime(tmpB) - parseDateTime(tmpA);
              break;
            default:
              return asc ? tmpA.localeCompare(tmpB) : tmpB.localeCompare(tmpA);
          }
        }).appendTo(tbody);
        update();
      }

    });
  }
}(jQuery));
