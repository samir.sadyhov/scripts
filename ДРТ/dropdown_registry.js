/*
model.registryCode = 'experience_registry_services'; - код реестра
model.fieldTitle = 'experience_form_service_name'; - какое поле отображать в списке
model.asfProperty.required = true; - признаяк обязательности заполнения

model.customFilter = {
   field: 'experience_form_serviceStep_service',
   condition: 'CONTAINS',
   key: true,
   searchCmpId: 'custom-y50qvr'
}

model.matching = []; //Сопоставление из выбранной записи в текущую
model.matching.push({from: 'experience_form_serviceStep_number', to: 'cjm_servise_stepNum'})
*/

/*custom dropdown registry - это в конструкторе

.asf-dropdownItem {
  padding: 5px !important;
  margin: 5px !important;
}

.asf-dropdownItem, .asf-dropdown-input {
  font-family: 'Open Sans' !important;
  font-size: 14px !important;
  white-space: pre-line !important;
  height: auto !important;
  line-height: normal !important;
  min-height: 36px;
}

.asf-dropdownItemHover {
  background-color: #91c2f6 !important;
}
*/

let input = $('<div class="asf-dropdown-input uk-select">');
let button = $('<button class="asf-dropdown-button">');
let label = $(view.container).children(".asf-label");
let items = getItems();
view.container.append(input).append(button);

if (editable) {
  label.hide();
  input.show();
  button.show();
} else {
  label.show();
  button.hide();
  input.hide();
}

model.getAsfData = function(blockNumber){
  if(model.getValue()) {
    var result = AS.FORMS.ASFDataUtils.getBaseAsfData(model.asfProperty, blockNumber, model.getValue().value , model.getValue().key);
    result.valueID = model.getValue().key;
    return result;
  } else {
    return AS.FORMS.ASFDataUtils.getBaseAsfData(model.asfProperty, blockNumber);
  }
};

model.setAsfData = function(asfData){
  if(!asfData || !asfData.value) {
    return;
  }
  var value = {key: asfData.key, value: asfData.value};
  model.setValue(value);
};

model.getSpecialErrors = function() {
  if(model.getValue() && !model.getValue().key) {
    return {id : model.asfProperty.id, errorCode : AS.FORMS.INPUT_ERROR_TYPE.emptyValue};
  }
};

view.updateValueFromModel = function () {
  if (model.getValue()) {
    label.text(model.getValue().value);
    input.text(model.getValue().value);
    view.unmarkInvalid();
  } else {
    label.text("");
    input.text("");
  }
};

model.on(AS.FORMS.EVENT_TYPE.valueChange, function () {
  view.updateValueFromModel();
});

model.on(AS.FORMS.EVENT_TYPE.dataLoad, function () {
  view.updateValueFromModel();
});

view.markInvalid = function(){
   label.css({
     "background-color": "#fff3f3",
     "border": "1px solid #ecacad"
   });
   input.css({
     "background-color": "#fff3f3",
     "border": "1px solid #ecacad"
   });
};

view.unmarkInvalid = function(){
    input.css({
      "background-color": "#ffffff",
      "border": "1px solid #d6d6d6"
    });
    label.css({
      "background-color": "#ffffff",
      "border": ""
    });
};

view.updateValueFromModel();

if(model.customFilter) {
  let searchModel = model.playerModel.getModelWithId(model.customFilter.searchCmpId);
  searchModel.on(AS.FORMS.EVENT_TYPE.valueChange, function () {
    model.setValue(null);
    items = getItems();
  });
}

function getValue(data, id) {
  for(let i = 0; i < data.length; i++){
    if(data[i].id === id) return data[i].value;
  }
}

function getKey(data, id) {
  for(let i = 0; i < data.length; i++){
    if(data[i].id === id) return data[i].key;
  }
}

function matching(documentID){
  let api = AS.FORMS.ApiUtils;
  api.getAsfDataUUID(documentID, uuid => {
    api.loadAsfData(uuid).then(asfData => {
      model.matching.forEach(m => {
        let tmpModelTo = model.playerModel.getModelWithId(m.to);
        if(tmpModelTo) tmpModelTo.setValue(getValue(asfData.data, m.from));
      })
    })
  })
}

function getItems(){
  let result = [];
  let url = `rest/api/registry/data_ext?registryCode=${model.registryCode}&fields=${model.fieldTitle}`;

  if(model.customFilter) {
    let searchModel = model.playerModel.getModelWithId(model.customFilter.searchCmpId);
    console.log('searchModel getItems', searchModel);
    if(searchModel && searchModel.getValue()) {
      url = `${url}&condition=${model.customFilter.condition}&field=${model.customFilter.field}&${model.customFilter.key ? 'key' : 'value'}=${model.customFilter.key ? searchModel.getValue().key : searchModel.getValue().value}`;
    } else {
      return [];
    }
  }

  jQuery.ajax({
    url: AS.FORMS.ApiUtils.getFullUrl(url),
    type: "GET",
    beforeSend: AS.FORMS.ApiUtils.addAuthHeader,
    async: false
  }).done(res => {
      if(res.errorCode) return;
      res.result.forEach(item => {
        result.push({title: item.fieldValue[model.fieldTitle], value: item.documentID})
      });
  }).fail(err => {
    console.error(err);
  });

  return result;
}

function getTitle(value) {
  return items.filter(item => {
    return item.value === value;
  })[0].title;
}

function init(){
  AS.SERVICES.showDropDown(items, input, 100, function(item){
    model.setAsfData({key: item, value: getTitle(item)});
    if(model.matching && model.matching.length > 0) matching(item);
  });
}

button.on('click', init);
input.on('click', init);
