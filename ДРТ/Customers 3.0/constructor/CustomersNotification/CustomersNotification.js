const createAudio = sound => new Audio(sound);

const searchInRegistry = async param => {
  return new Promise(resolve => {
    rest.synergyGet(`api/registry/data_ext?${param}`, resolve, err => {
      console.log(`ERROR [ searchInRegistry ]: ${JSON.stringify(err)}`);
      resolve(null);
    });
  });
}

this.CustomersNotification = {
  registryCode: 'customers_tasks', // код реестра
  notificationsLimit: 3, // максимальное кол-во уведомлений на экране
  refreshInterval: 1, // интервал обновления в минутах

  fields: [
    'task_start',
    'task_finish',
    'task_author',
    'task_Resposible',
    'task_comment',
    'task_status',
    'task_action',
    'task_notification_timer'
  ],

  notificationsContainer: null,
  currentDate: new Date(),

  tasks: null,
  shownTasks: {},
  allTaskNotify: 0,

  getUrlParam: function() {
    const {formatDate, DATE_INPUT_FORMAT} = dateUtils;
    const start = `${formatDate(this.currentDate, DATE_INPUT_FORMAT)} 00:00:00`;
    const stop = `${formatDate(this.currentDate, DATE_INPUT_FORMAT)} 23:59:59`;

    let url = `registryCode=${this.registryCode}`;
    url += `&field=task_start&condition=MORE_OR_EQUALS&key=${start}`;
    url += `&field1=task_start&condition1=LESS_OR_EQUALS&key1=${stop}`;
    url += `&field2=task_options&condition2=TEXT_EQUALS&value2=show_notify`;
    url += `&field3=task_Resposible&condition3=TEXT_EQUALS&key3=${AS.OPTIONS.currentUser.userid}`;
    url += `&field4=task_status&condition4=TEXT_EQUALS&key4=0`;
    // url += `&countInPart=${this.notificationsLimit}`;
    url += `&registryRecordStatus=STATE_SUCCESSFUL`;

    this.fields.forEach(field => url += `&fields=${field}`);

    return url;
  },

  parseTasks: function(searchResult){
    const result = [];

    if(!searchResult || searchResult.recordsCount == 0) return result;

    for(let i = 0; i < searchResult.result.length; i++) {
      const {dataUUID, documentID, fieldKey, fieldValue} = searchResult.result[i];
      const tmp = {dataUUID, documentID};

      for(let key in fieldValue) {
        tmp[key] = {value: fieldValue[key]};
        if(fieldKey.hasOwnProperty(key)) tmp[key].key = fieldKey[key];
      }

      result.push(tmp);

      result.sort((a, b) => {
        const task_startA = new Date(a.task_start?.key);
        const task_startB = new Date(b.task_start?.key);
        return task_startA.getTime() - task_startB.getTime();
      });
    }

    return result;
  },

  searchTasks: async function() {
    const param = this.getUrlParam();
    const searchResult = await searchInRegistry(param);
    return this.parseTasks(searchResult);
  },

  viewTask: function(task, notifyContainer) {
    const {dataUUID, task_author, task_start} = task;

    const player = UTILS.getSynergyPlayer(dataUUID);
    const dialogTitle = dateUtils.getWeekDayName(new Date(task_start.key).getDay());

    const dialog = $('<div class="uk-flex-top" uk-modal>');
    const md = $('<div class="uk-modal-dialog uk-margin-auto-vertical">');
    const modalBody = $('<div class="uk-modal-body" uk-overflow-auto>');
    const footer = $('<div class="uk-modal-footer uk-text-right">');
    const actionPanel = $('<div>', {class: "customer_dialog_action_panel uk-hidden"});
    const finishButton = $(`<button class="uk-button uk-button-primary" type="button">${i18n.tr("Завершить")}</button>`);

    modalBody.append(player.view.container);
    footer.append(finishButton, `<button class="uk-button uk-button-default uk-modal-close" type="button">${i18n.tr("Закрыть")}</button>`);
    md.append(`<button class="uk-modal-close-default modal-close-custom" type="button" uk-close></button>`,
    `<div class="uk-modal-header"><h3>${dialogTitle}</h3></div>`, actionPanel, modalBody, footer);
    dialog.append(md);

    footer.css({
      "gap": "10px",
      "display": "flex",
      "justify-content": "end"
    });

    finishButton.on('click', e => {
      e.preventDefault();
      e.target.blur();
      this.finishTask(dataUUID, notifyContainer);
    });

    if(task_author?.key == AS.OPTIONS.currentUser.userid) {
      actionPanel.removeClass('uk-hidden');

      const editButton = $(`<div class="dialog_action_button">${getSvgIcon('edit')}</div>`);
      const saveButton = $(`<div class="dialog_action_button" style="visibility: hidden;">${getSvgIcon('save')}</div>`);
      actionPanel.append(saveButton, editButton);

      let editable = false;

      editButton.on('click', e => {
        e.preventDefault();
        e.target.blur();

        if(!editable) {
          saveButton.css('visibility', 'visible');
          editButton.html(getSvgIcon('view'));
        } else {
          saveButton.css('visibility', 'hidden');
          editButton.html(getSvgIcon('edit'));
        }

        editable = !editable;
        player.view.setEditable(editable);
      });

      saveButton.on('click', e => {
        e.preventDefault();
        e.target.blur();

        try {
          Cons.showLoader();
          if (!player.model.isValid()) throw new Error('Заполните обязательные поля');

          player.saveFormData(async asfDataId => {
            await appAPI.modifyDoc(asfDataId);
            Cons.hideLoader();
            showMessage('Задача изменена', 'success');
            UIkit.modal(dialog).hide();
            player.destroy();
            this.update();
            if($('#buttonRefresh').length) $('#buttonRefresh').trigger('calendar_update');
          }, (status, responseText) => {
            console.log('ERROR [saveFormData]', {status, responseText});
            throw new Error(responseText);
          });

        } catch (e) {
          Cons.hideLoader();
          showMessage(e.message, 'error');
        }
      });
    }

    UIkit.modal(dialog).show();
    dialog.on('hidden', () => dialog.remove());
  },

  finishTask: async function(dataUUID, notifyContainer) {
    UIkit.modal.confirm(i18n.tr('Вы действительно хотите завершить задачу?'), {labels: {ok: i18n.tr('Да'), cancel: i18n.tr('Отмена')}}).then(async () => {
      Cons.showLoader();
      try {
        const mergeResult = await appAPI.mergeFormData({
          uuid: dataUUID,
          data: [{
            id: 'task_status',
            type: "listbox",
            value: 'Завершено',
            key: '1'
          }]
        });

        if(!mergeResult) throw new Error(i18n.tr("Произошла ошибка при завершении задачи"));

        await appAPI.modifyDoc(dataUUID);

        delete this.shownTasks[dataUUID];

        Cons.setAppStore({shownTasks: this.shownTasks});

        notifyContainer.fadeOut(200, () => {
          notifyContainer.remove();
          if($('#buttonRefresh').length) $('#buttonRefresh').trigger('calendar_update');
          this.update();
        });

        Cons.hideLoader();
      } catch (err) {
        Cons.hideLoader();
        showMessage(err.message, 'error');
        console.log('ERROR finishTask CustomersNotification', err);
      }
    }, () => null);
  },

  extendTask: async function(dataUUID, task_notification_timer, notifyContainer) {
    Cons.showLoader();
    try {
      const currDate = new Date();
      const newStartDate = new Date(currDate);
      const newFinishDate = new Date(currDate);

      newStartDate.setMinutes(newStartDate.getMinutes() + Number(task_notification_timer?.key || 0) + 15);
      newFinishDate.setMinutes(newFinishDate.getMinutes() + Number(task_notification_timer?.key || 0) + 30);

      if(currDate.getHours() == 23) {
        if(newStartDate.getHours() != 23 || (newStartDate.getHours() == 23 && newStartDate.getMinutes() > 58)) {
          throw new Error(i18n.tr(`Больше нельзя отложить задачу`));
        }
      }

      const mergeResult = await appAPI.mergeFormData({
        uuid: dataUUID,
        data: [
          {
            id: 'task_start',
            type: "date",
            value: dateUtils.formatDate(newStartDate, "${yyyy}.${mm}.${dd} ${HH}:${MM}"),
            key: dateUtils.formatDate(newStartDate, dateUtils.DATE_FORMAT_FULL)
          },
          {
            id: 'task_finish',
            type: "date",
            value: dateUtils.formatDate(newFinishDate, "${yyyy}.${mm}.${dd} ${HH}:${MM}"),
            key: dateUtils.formatDate(newFinishDate, dateUtils.DATE_FORMAT_FULL)
          }
        ]
      });

      if(!mergeResult) throw new Error(i18n.tr("Произошла ошибка при попытке отложить задачу"));

      await appAPI.modifyDoc(dataUUID);

      this.shownTasks[dataUUID].extend = true;
      Cons.setAppStore({shownTasks: this.shownTasks});

      notifyContainer.fadeOut(200, () => {
        notifyContainer.remove();
        if($('#buttonRefresh').length) $('#buttonRefresh').trigger('calendar_update');
        this.update();
      });

      Cons.hideLoader();
    } catch (err) {
      Cons.hideLoader();
      showMessage(err.message, 'error');
      console.log('ERROR extendTask CustomersNotification', err);
    }
  },

  getMoreNotify: function(){
    let renderTask = 0;

    for(dataUUID in this.shownTasks) {
      const task = this.shownTasks[dataUUID];
      if(!task.close && task.render && !task.extend) renderTask++;
    }
    const moreTask = this.allTaskNotify - renderTask;

    this.notificationsContainer.find('.notification_add').remove();

    if(moreTask > 0) {
      this.notificationsContainer.append(`<div class="notification_add">${i18n.tr("Еще напоминаний:")} ${moreTask}</div>`);
    }
  },

  renderNotify: function(task){
    const {dataUUID, task_start, task_finish, task_action, task_notification_timer, task_comment, task_author} = task;

    //если таска отрисована и не закрыта то ниче не делаем
    if(this.shownTasks.hasOwnProperty(dataUUID) && !this.shownTasks[dataUUID].close) return;

    if(!this.shownTasks.hasOwnProperty(dataUUID)) {
      this.shownTasks[dataUUID] = {
        close: false,
        render: false,
        extend: false,
        sound: false
      }
    }

    if(!this.shownTasks[dataUUID].render) {
      const container = $('<div>', {class: "notification", dataUUID});
      const header = $('<div>', {class: "notification_header"});
      const body = $('<div>', {class: "notification_body"});
      const footer = $('<div>', {class: "notification_footer"});

      const buttonView = $(`<button class="uk-button uk-button-default uk-button-small">${i18n.tr('Подробнее')}</button>`);
      const buttonFinish = $(`<button class="uk-button uk-button-primary uk-button-small">${i18n.tr('Завершить')}</button>`);
      const buttonExtend = $(`<button class="uk-button uk-button-default uk-button-small uk-hidden">${i18n.tr('Отложить')}</button>`);
      const buttonCloseHeader = $(`<a href="#" uk-icon="icon: close; ratio: 1.5"></a>`);

      header.append(`${i18n.tr('Уведомление')}`, buttonCloseHeader);

      const taskLabel = `${task_start.value.split(' ')[1]} - ${task_finish.value.split(' ')[1]} ${task_action.value}`;
      let comment = (task_comment?.value || '').slice(0,50);
      if(task_comment?.value.length > 50) comment += '...';

      body.append(
        `<span class="uk-margin-small-right uk-text-meta">${taskLabel}</span>`,
        `<span class="uk-margin-small-right uk-text-small" uk-tooltip="${task_comment?.value}">${comment}</span>`
      );

      footer.append(buttonView, buttonExtend, buttonFinish);
      container.append(header, body, footer);
      this.notificationsContainer.prepend(container);

      if(task_author?.key == AS.OPTIONS.currentUser.userid) buttonExtend.removeClass('uk-hidden');

      buttonFinish.off().on('click', e => {
        e.preventDefault();
        e.target.blur();

        this.finishTask(dataUUID, container);
      });

      buttonExtend.off().on('click', e => {
        e.preventDefault();
        e.target.blur();

        this.extendTask(dataUUID, task_notification_timer, container);
      });

      buttonView.off().on('click', e => {
        e.preventDefault();
        e.target.blur();
        this.viewTask(task, container);
      });

      buttonCloseHeader.off().on('click', e => {
        e.preventDefault();
        e.target.blur();
        this.shownTasks[dataUUID].close = true;
        this.shownTasks[dataUUID].render = false;

        Cons.setAppStore({shownTasks: this.shownTasks});

        container.fadeOut(200, () => {
          container.remove();
          this.getMoreNotify();
        });
      });

      this.shownTasks[dataUUID].render = true;
      this.shownTasks[dataUUID].close = false;

      if(!this.shownTasks[dataUUID].sound) {
        this.soundNotification.play();
        this.shownTasks[dataUUID].sound = true;
      }
    }

    Cons.setAppStore({shownTasks: this.shownTasks});
  },

  update: async function(){
    this.tasks = await this.searchTasks();
    this.allTaskNotify = 0;
    const currentTime = new Date().getTime();

    for(const dataUUID in this.shownTasks) {
      if(this.tasks.find(x => x.dataUUID == dataUUID)) continue;
      delete this.shownTasks[dataUUID];
      $(`.notification[datauuid="${dataUUID}"]`).remove();
    }

    Cons.setAppStore({shownTasks: this.shownTasks});

    for(let i = 0; i < this.tasks.length; i++) {
      const task = this.tasks[i];
      const {task_notification_timer, task_start} = task;

      let notifyTime = new Date(task_start?.key);
      notifyTime.setMinutes(notifyTime.getMinutes() - Number(task_notification_timer?.key || 0));
      notifyTime = notifyTime.getTime();

      if(currentTime < notifyTime) continue;

      this.allTaskNotify++;

      if(this.allTaskNotify <= this.notificationsLimit &&
        this.notificationsContainer.find('.notification').length < this.notificationsLimit
      ) {
        this.renderNotify(task);
      }
    }

    this.getMoreNotify();
  },

  stop: function(){
    clearTimeout(window.customersNotificationTimer);
    window.customersNotificationTimer = null;
    this.timer = null;
    $('.notifications_container').remove();
    this.notificationsContainer = null;
    this.shownTasks = {};
    Cons.setAppStore({shownTasks: null});
  },

  init: function(){

    let me = this;
    this.soundNotification = createAudio(notificationSound);

    this.shownTasks = Cons.getAppStore().shownTasks || {};

    if($('.notifications_container').length) {
      this.notificationsContainer = $('.notifications_container');
    } else {
      this.notificationsContainer = $('<div>', {class: "notifications_container"});
      $('body').append(this.notificationsContainer);
    }

    if(window.customersNotificationTimer) {
      this.timer = window.customersNotificationTimer;
    } else {
      me.timer = setTimeout(async function run() {
        if(me.timer) {
          await me.update();
          me.timer = setTimeout(run, me.refreshInterval * 60000);
        }
      }, me.refreshInterval * 60000);

      window.customersNotificationTimer = this.timer;
    }
  }

}
