const getRegInfo = code => {
  const {registryList} = Cons.getAppStore();
  const list = UTILS.parseRegistryList(registryList);
  let result;
  list.forEach(item => {
    if (item.children) {
      item.children.forEach(child => {
        if (child.code == code) result = child;
      })
    } else {
      if (item.code == code) result = item;
    }
  });
  return result;
}

const filterAgrProcesses = processes => {
  const procCode = ['APPROVAL_ITEM', 'AGREEMENT_ITEM', 'ACQUAINTANCE_ITEM'];
  const result = [];
  function search(p) {
    p.forEach(function(process) {
      if (!process.finished && procCode.includes(process.typeID)) {
        result.push(process);
      }
      if (process.subProcesses.length > 0) search(process.subProcesses);
    });
  }
  search(processes);
  return result;
}

const checkSavedForm = (formPlayer, handler) => {
  if(formPlayer.model.hasChanges) {
    UIkit.modal.confirm('Документ был изменен. Сохранить произведенные изменения?',
    {labels: {ok: i18n.tr('Да'), cancel: i18n.tr('Отмена')}})
    .then(() => {
      if(!formPlayer.model.isValid()) {
        showMessage("Заполните обязательные поля!", 'warn');
      } else {
        Cons.showLoader();
        const asfData = formPlayer.model.getAsfData();
        const data = {
          data: '"data" : ' + JSON.stringify(asfData.data),
          form: asfData.form,
          uuid: asfData.uuid
        };
        AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/asforms/form/multipartdata", res => {
          Cons.hideLoader();
          showMessage('<svg width="35" height="35" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><polyline fill="none" stroke="#32d296" stroke-width="1.1" points="4,10 8,15 17,4"></polyline></svg> Успешно', 'success');
          handler();
        }, "text", data, "application/x-www-form-urlencoded; charset=UTF-8", err => {
          Cons.hideLoader();
          handler();
          showMessage("Нет прав на редактирование файла", 'error');
        });
      }
    }, handler);
  } else {
    handler();
  }
}

const closeDocument = formPlayer => {
  const openDocsWindow = Cons.getAppStore().openDocsWindow.filter(x => x.dataUUID !== formPlayer.dataUUID);
  Cons.setAppStore({openDocsWindow: openDocsWindow});
  formPlayer.panels.documentWindow.remove();
  formPlayer.panels.documentPanel.remove();
  formPlayer.destroy();
}

const buttonSort = (formPlayer) => {
  function s(a, b) {
    let idA = $(a).attr("id");
    let idB = $(b).attr("id");
    if (idA > idB) return 1;
    if (idA < idB) return -1;
    return 0;
  }
  const mobileMenuContent = formPlayer.panels.mobileMenu.find('.arm-mobile-two-panel');
  const buttons = formPlayer.panels.panelButtons.find('.window-action-button');
  buttons.sort(s);
  $(buttons).appendTo(formPlayer.panels.panelButtons);
  const buttons2 = mobileMenuContent.find('.window-action-button');
  buttons2.sort(s);
  $(buttons2).appendTo(mobileMenuContent);
}

const createButton = (formPlayer, name, id, handler, greenButton) => {
  const mobileMenu = formPlayer.panels.mobileMenu;
  const mobileMenuContent = mobileMenu.find('.arm-mobile-two-panel');
  const button = $(`<button id="${id}" class="uk-button margined uk-button-default window-action-button fonts">`)
  .css({
    'width': '100%',
    'white-space': 'nowrap',
    'text-overflow': 'ellipsis',
    'overflow': 'hidden',
    'padding': '0 5px',
  })
  .attr('title', name)
  .html(name)
  .on('click', e => {
    e.preventDefault();
    e.target.blur();

    if(/Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent)) {
      mobileMenu.trigger('hideShowMenu');
    }

    handler();
  });
  if(greenButton) button.addClass('window-action-button-green');
  formPlayer.panels.panelButtons.append(button);
  button.clone(true).appendTo(mobileMenuContent);
  buttonSort(formPlayer);
}

const saveFormData = (formPlayer, handler) => {
  if(!formPlayer.model.isValid()) {
    showMessage("Заполните обязательные поля!", 'warn');
  } else {
    Cons.showLoader();
    try {
      const asfData = formPlayer.model.getAsfData();
      const data = {
        data: '"data" : ' + JSON.stringify(asfData.data),
        form: asfData.form,
        uuid: asfData.uuid
      };
      AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/asforms/form/multipartdata", res => {
        Cons.hideLoader();
        showMessage('<svg width="35" height="35" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><polyline fill="none" stroke="#fff" stroke-width="1.1" points="4,10 8,15 17,4"></polyline></svg> Успешно', 'success');
        formPlayer.model.hasChanges = false;
        if(handler) handler();
      }, "text", data, "application/x-www-form-urlencoded; charset=UTF-8", err => {
        Cons.hideLoader();
        showMessage("Нет прав на редактирование файла", 'error');
      });
    } catch (error) {
      Cons.hideLoader();
      showMessage("Ошибка сохранения данных по форме", 'error');
      console.log(error);
    }
  }
}

const printForm = content => {
  const css = '<link rel="stylesheet" href="synergyForms.css" type="text/css" />';
  const WinPrint = window.open();
  WinPrint.document.write('');
  WinPrint.document.write(css);
  WinPrint.document.write(content.innerHTML);
  WinPrint.document.write('');
  WinPrint.document.close();
  WinPrint.focus();
  WinPrint.print();
}

const cutText = text => {
  if(text.length > 100) return text.slice(0, 100) + '...';
  return text;
}

const activateDocument = async formPlayer => {
  if(!formPlayer.model.isValid()) {
    showMessage("Заполните обязательные поля!", 'warn');
    return;
  }

  Cons.showLoader();
  try {
    const asfData = formPlayer.model.getAsfData();
    await AS.FORMS.ApiUtils.saveAsfData(asfData.data, asfData.form, asfData.uuid);
    await appAPI.activateDoc(asfData.uuid);

    Cons.hideLoader();
    showMessage('<svg width="35" height="35" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><polyline fill="none" stroke="#49B785" stroke-width="1.1" points="4,10 8,15 17,4"></polyline></svg> Успешно', 'success');

    //закрыть окно после активации
    closeDocument(formPlayer);

  } catch (error) {
    Cons.hideLoader();
    showMessage("Произошла ошибка при создании заявки, обратитесь к администратору", 'error');
    console.log(error);
  }
}

const getUserWork = (processes, userID) => {
  const result = [];
  function search(p) {
    p.forEach(function(process) {
      if (!process.finished &&
        process.actionID != "" &&
        (process.responsibleUserID == userID || process.authorID == userID)
      ) {
        result.push(process);
      }
      if (process.subProcesses.length > 0) search(process.subProcesses);
    });
  }
  search(processes);
  return result;
}

const setProgressWork = async (actionID, progress, formPlayer) => {
  if(progress == 100) {
    const msgConfirm = i18n.tr("Вы действительно хотите завершить данную работу? Все дочерние работы так же будут завершены.");
    const msgSuccess = i18n.tr("Работа завершена");

    UIkit.modal.confirm(msgConfirm, {labels: {ok: i18n.tr('Да'), cancel: i18n.tr('Отмена')}}).then(async () => {
      const resultFinish = await appAPI.setProgressWork(progress, actionID);
      if(resultFinish) {
        showMessage(msgSuccess, 'success');
        closeDocument(formPlayer);
      } else {
        showMessage('Произошла ошибка при завершении работы', 'error');
      }
    }, () => {
      return;
    });
  } else {
    const resultFinish = await appAPI.setProgressWork(progress, actionID);
    if(resultFinish) {
      closeDocument(formPlayer);
    } else {
      showMessage('Произошла ошибка при изменении прогресса работы', 'error');
    }
  }
}

const finishWork = async (formPlayer, workInfo) => {
  try {
    if(formPlayer.model.getErrors().length) throw new Error('Заполните обязательные поля');

    checkSavedForm(formPlayer, async () => {
      const msgConfirm = i18n.tr('Вы действительно хотите завершить работу?');
      const {actionID, author, completionForm, documentID} = workInfo;
      const childDocuments = await appAPI.getChildDocuments(documentID);
      workInfo.childDocuments = childDocuments;

      if(AS.OPTIONS.currentUser.userid == author.id) {
        UIkit.modal.confirm(msgConfirm, {labels: {ok: i18n.tr('Да'), cancel: i18n.tr('Отмена')}})
        .then(() => {
          _WORK.finishWork(actionID, formPlayer);
        }, null);
      } else {
        if(completionForm) {
          const {CompletionFormType, completionFormName, CompletionFormInfo} = completionForm;

          switch (CompletionFormType) {
            case "FORM": _WORK.finishForm(actionID, CompletionFormInfo, formPlayer); break;
            case "COMMENT": _WORK.finishComment(actionID, formPlayer); break;
            case "FILE": _WORK.finishFile(actionID, formPlayer); break;
            case "DOCUMENT": _WORK.finishDocument(actionID, childDocuments, formPlayer); break;
            case "NOTHING":
              UIkit.modal.confirm(msgConfirm, {labels: {ok: i18n.tr('Да'), cancel: i18n.tr('Отмена')}})
              .then(() => {
                _WORK.finishWork(actionID, formPlayer);
              }, null);
              break;
          }

        } else {
          UIkit.modal.confirm(msgConfirm, {labels: {ok: i18n.tr('Да'), cancel: i18n.tr('Отмена')}})
          .then(() => {
            setProgressWork(actionID, 100, formPlayer);
          }, null);
        }
      }
    });

  } catch (error) {
    Cons.hideLoader();
    showMessage("Произошла ошибка при завершении работы", 'error');
    console.log(error);
  }
}

const signDocument = async (documentID, signButton) => {
  try {
    NCALayer.sign('SIGN', documentID, async result => {
      Cons.showLoader();

      try {
        const signResult = await AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/docflow/sign", null, null, {
          documentID,
          rawdata: result.dataForSign,
          signdata: result.signedData,
          certificate: result.certificate,
          certID: result.certID
        });

        if(signResult.errorCode != 0) throw new Error(signResult.errorMessage);

        Cons.hideLoader();
        showMessage(i18n.tr('Документ успешно подписан ЭЦП'), 'success');
        signButton.hide();
      } catch (e) {
        Cons.hideLoader();
        showMessage(i18n.tr(e.message), 'error');
      }

    });
  } catch (err) {
    Cons.hideLoader();
    showMessage(i18n.tr(err.message), 'error');
  }
}

const setField = (from, modelTo, collation_type = null, label = null) => {
  if(from) {
    switch (from.type) {
      case 'textbox':
      case 'textarea':
        if(modelTo && from.hasOwnProperty('value') && from.value) {
          const currentValue = modelTo.getValue();

          switch (collation_type) {
            case 'PREFIX':
              if(currentValue && currentValue != '') {
                if(label && label != '') {
                  modelTo.setValue(`${label} ${from.value}`);
                } else {
                  modelTo.setValue(`${currentValue} ${from.value}`);
                }
              } else {
                if(label && label != '') {
                  modelTo.setValue(`${label} ${from.value}`);
                } else {
                  modelTo.setValue(from.value);
                }
              }
              break;
            case 'POSTFIX':
              if(currentValue && currentValue != '') {
                if(label && label != '') {
                  modelTo.setValue(`${from.value} ${label}`);
                } else {
                  modelTo.setValue(`${from.value} ${currentValue}`);
                }
              } else {
                if(label && label != '') {
                  modelTo.setValue(`${from.value} ${label}`);
                } else {
                  modelTo.setValue(from.value);
                }
              }
              break;
            default: modelTo.setValue(from.value);
          }

        } else {
          if(modelTo) modelTo.setValue(null);
        }
        break;
      case 'check':
        if(modelTo && from.hasOwnProperty('values')) {
          modelTo.setValue(from.values);
        } else {
          if(modelTo) modelTo.setValue(null);
        }
        break;
      case 'entity':
        if(modelTo && from.hasOwnProperty('key') && from.key && from.key !== 'null') {
          switch (modelTo.asfProperty.config.entity) {
            case "users":
              modelTo.setValue({
                personID: from.key,
                personName: from.value
              });
              break;
            case "departments":
              modelTo.setValue({
                departmentId: from.key,
                departmentName: from.value
              });
              break;
            case "positions":
              modelTo.setValue({
                elementID: from.key,
                elementName: from.value
              });
              break;
          }
        } else {
          if(modelTo) modelTo.setValue(null);
        }
        break;
      default:
        if(modelTo && from.hasOwnProperty('key') && from.key) {
          modelTo.setValue(from.key);
        } else {
          if(modelTo) modelTo.setValue(null);
        }
    }
  } else {
    if(modelTo) modelTo.setValue(null);
  }
}

const removeRow = tableModel => {
  for(let numb in tableModel.getBlockNumbers()) tableModel.removeRow(numb);
}

const initMatching = async player => {
  Cons.showLoader();
  const {matchingParam} = player;
  const {currentRegistryCode, parentDocumentID, parentDataUUID} = matchingParam;

  const parentDocInfo = await appAPI.getDocumentInfo(parentDocumentID);
  const parentRegInfo = await appAPI.getRegistryInfoByID(parentDocInfo.registryID);
  const collationInfo = await appAPI.getCollationInfo(parentRegInfo.code, currentRegistryCode);
  const parentAsfData = await appAPI.loadAsfData(parentDataUUID);
  const currentDocumentID = await appAPI.getDocumentIdentifier(player.model.asfDataId);

  await appAPI.setChildDocuments(parentDocumentID, [currentDocumentID]);

  Cons.hideLoader();

  for(let i = 0; i < collationInfo.length; i++) {
    const {collation_fields} = collationInfo[i];

    for(let j = 0; j < collation_fields.length; j++) {
      const {in_field, out_field, collation_type, label} = collation_fields[j];
      const from = parentAsfData.data.find(x => x.id === in_field);

      if(out_field.indexOf('.') != -1) {
        const tableID = out_field.split('.')[0];
        const fieldTableID = out_field.split('.')[1];
        const modelTable = player.model.playerModel.getModelWithId(tableID);

        if(modelTable) {
          const toBlock = modelTable.createRow();
          const tableModelOutField = player.model.playerModel.getModelWithId(fieldTableID, tableID, toBlock.tableBlockIndex);

          if(from && from.type == "appendable_table") {
            const fromTableData = UTILS.parseAsfTable(from);

            for(let k = 0; k < fromTableData.length; k++) {
              const fromBlock = fromTableData[k];

              for(const filedID in fromBlock) {
                if(fieldTableID != filedID) continue;
                const fromField = {id: filedID, ...fromBlock[filedID]}
                setField(fromField, tableModelOutField, collation_type, label);
              }
            }

          } else {
            setField(from, tableModelOutField, collation_type, label);
          }
        }

      } else {
        const modelTo = player.model.playerModel.getModelWithId(out_field);

        if(from && from.type == "appendable_table" && modelTo && modelTo.asfProperty.type == "table") {

          removeRow(modelTo);
          const fromTableData = UTILS.parseAsfTable(from);

          for(let k = 0; k < fromTableData.length; k++) {
            const fromBlock = fromTableData[k];
            const toBlock = modelTo.createRow();

            for(const filedID in fromBlock) {
              const tableModelOutField = player.model.playerModel.getModelWithId(filedID, out_field, toBlock.tableBlockIndex);
              const fromField = {id: filedID, ...fromBlock[filedID]}
              setField(fromField, tableModelOutField, collation_type, label);
            }
          }

        } else {
          setField(from, modelTo, collation_type, label);
        }
      }

    }
  }

}

const createChildDocument = async workInfo => {
  try {
    Cons.showLoader();

    const {documentID: parentDocumentID, dataUUID: parentDataUUID, completionForm} = workInfo;

    if(!parentDocumentID || !parentDataUUID) throw new Error('Произошла ошибка при создании документа');

    const {CompletionFormInfo, completionFormName} = completionForm;
    const regInfo = await appAPI.getRegistryInfo(CompletionFormInfo.registryCode);
    if(!regInfo) throw new Error(i18n.tr('При создании записи произошла ошибка. Не удалось плучить информацию по реестру.'));
    if(regInfo.rr_create != "Y") throw new Error(i18n.tr('У вас нет прав на создание записи'));

    const doc = await appAPI.createDoc(CompletionFormInfo.registryCode);
    if(!doc) throw new Error(i18n.tr('Произошла ошибка при создании данного типа документа'));

    const form = await AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/asforms/form/${regInfo.formId}?locale=${AS.OPTIONS.locale}`);

    Cons.hideLoader();

    const matchingParam = {currentRegistryCode: CompletionFormInfo.registryCode, parentDocumentID, parentDataUUID};

    const eventParam = {
      dataUUID: doc.dataUUID,
      registryName: regInfo.name,
      registryCode: regInfo.code,
      formName: form.name,
      editable: true,
      matchingParam
    };

    $('#root-panel').trigger({type: 'custom_open_document', eventParam});

  } catch (e) {
    Cons.hideLoader();
    console.log(e);
    showMessage(e.message, 'error');
  }
}

const registryReply = async (player, replyRegistryInfo) => {
  try {
    Cons.showLoader();

    const {documentID: parentDocumentID, dataUUID: parentDataUUID} = player;

    if(!parentDocumentID || !parentDataUUID) throw new Error('Произошла ошибка при создании документа');

    if(!replyRegistryInfo) throw new Error(i18n.tr('При создании записи произошла ошибка. Не удалось плучить информацию по реестру.'));
    if(replyRegistryInfo.rr_create != "Y") throw new Error(i18n.tr('У вас нет прав на создание записи'));

    const doc = await appAPI.createDoc(replyRegistryInfo.code);
    if(!doc) throw new Error(i18n.tr('Произошла ошибка при создании данного типа документа'));

    const form = await AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/asforms/form/${replyRegistryInfo.formId}?locale=${AS.OPTIONS.locale}`);

    Cons.hideLoader();

    const matchingParam = {currentRegistryCode: replyRegistryInfo.code, parentDocumentID, parentDataUUID};

    const eventParam = {
      dataUUID: doc.dataUUID,
      registryName: replyRegistryInfo.name,
      registryCode: replyRegistryInfo.code,
      formName: form.name,
      editable: true,
      matchingParam
    };

    $('#root-panel').trigger({type: 'custom_open_document', eventParam});

  } catch (e) {
    Cons.hideLoader();
    console.log(`ERROR registryReply ${e.message}`);
    showMessage(e.message, 'error');
  }
}

class Document {
  constructor(player, process) {
    this.formPlayer = player;
    this.dataUUID = player.dataUUID;
    this.documentID = player.documentID;
    this.workName = player.panels.workName;
    this.panelButtons = player.panels.panelButtons;
    this.process = process;
    this.registryInfo = player.registryInfo;

    this.init();
  }

  resizePanels(){
    const {workName, panelButtons, attachmentContainer, signPanel} = this.formPlayer.panels;
    let h = panelButtons.innerHeight();
    if(!workName.is(':hidden')) h += workName.innerHeight();
    if(signPanel) h += signPanel.innerHeight();
    attachmentContainer.css({'height': `calc(100% - ${h}px)`});
  }

  async renderButtons(){
    const {userid} = AS.OPTIONS.currentUser;
    const {workInfo} = this.formPlayer;

    if(workInfo) {
      const {parent_process, user, author, completionFormID, name, finished} = workInfo;

      if(finished) return;

      this.workName.show().text(cutText(name)).attr('title', name);

      let completionForm;
      if(completionFormID) {
        completionForm = await appAPI.getCompletionForm(completionFormID);
        workInfo.completionForm = completionForm;

        const {CompletionFormInfo, CompletionFormType} = completionForm;
        if(CompletionFormType == "DOCUMENT") {
          const buttonName = CompletionFormInfo['button-name'];
          if(CompletionFormInfo.child && workInfo.childDocuments && !workInfo.childDocuments.length) {
            createButton(this.formPlayer, buttonName, 'itsm-button2', () => {
              createChildDocument(workInfo);
            }, true);
          } else {
            createButton(this.formPlayer, buttonName, 'itsm-button4', () => {
              createChildDocument(workInfo);
            }, false);
          }
        }
      }

      //кнопка реестра ответа
      const {reply_btn_name, reply_registryID} = this.registryInfo;
      if(reply_registryID && reply_registryID != "") {
        const replyRegistryInfo = await appAPI.getRegistryInfoByID(reply_registryID);
        if(replyRegistryInfo.rr_create == "Y") {
          createButton(this.formPlayer, reply_btn_name, 'itsm-button5', () => {
            registryReply(this.formPlayer, replyRegistryInfo);
          }, false);
        }
      }

      if(AS.OPTIONS.currentUser.userid == user.id) {
        switch (parent_process) {
          case 'null':
          case 'assignment-single': //Завершить
            createButton(this.formPlayer, i18n.tr("Завершить"), 'itsm-button3', () => {
              const {dataUUID, mainDataUUID} = this.formPlayer;
              if(dataUUID != mainDataUUID) {
                this.formPlayer.showFormData(null, null, mainDataUUID);
                setTimeout(() => {
                  finishWork(this.formPlayer, workInfo);
                }, 500);
              } else {
                finishWork(this.formPlayer, workInfo);
              }
            }, true);
           break;
          case 'approval-single': //Утвердить
            createButton(this.formPlayer, i18n.tr("Утвердить"), 'itsm-button6', () => {
              _WORK.finishProcess(workInfo, this.formPlayer);
            }, false);
            break;
          case 'agreement-single': //Согласовать
            createButton(this.formPlayer, i18n.tr("Согласовать"), 'itsm-button6', () => {
              _WORK.finishProcess(workInfo, this.formPlayer);
            }, false);
            break;
          case 'acquaintance-single': //Ознакомиться
            createButton(this.formPlayer, i18n.tr("Ознакомиться"), 'itsm-button6', () => {
              _WORK.finishProcess(workInfo, this.formPlayer);
            }, false);
            break;
          case 'resolution-action-single': console.log('Резолюция? хммм....'); break; // Резолюция
        }
      } else if(AS.OPTIONS.currentUser.userid == author.id) {
        if(completionForm && completionForm.hasOwnProperty('is_result_free') && !completionForm.is_result_free) {
          createButton(this.formPlayer, i18n.tr("Завершить"), 'itsm-button3', () => {
            _WORK.acceptWork(this.formPlayer, workInfo);
          }, true);
        } else {
          createButton(this.formPlayer, i18n.tr("Завершить"), 'itsm-button3', () => {
            const {dataUUID, mainDataUUID} = this.formPlayer;
            if(dataUUID != mainDataUUID) {
              this.formPlayer.showFormData(null, null, mainDataUUID);
              setTimeout(() => {
                finishWork(this.formPlayer, workInfo);
              }, 500);
            } else {
              finishWork(this.formPlayer, workInfo);
            }
          }, true);
        }
      }
    }

    this.resizePanels();
  }

  init(refresh){
    if(refresh) {
      this.renderButtons();
    } else {
      this.renderButtons();
      setTimeout(() => {
        this.formPlayer.model.hasChanges = false;
      }, 1000);
    }
  }
}

class ButtonsActions {

  constructor(player) {
    this.player = player;
    this.documentID = player.documentID;
    this.dataUUID = player.dataUUID;
    this.panelButtons = player.panels.panelButtons;
    this.editReadFormButton = player.editReadFormButton;
    this.workName = player.panels.workName;
    this.registryCode = player.registryCode;
    this.regInfo = null;
    this.actionButtons = null;

    this.init();
  }

  resizePanels(){
    const {workName, panelButtons, attachmentContainer, signPanel} = this.player.panels;
    let h = panelButtons.innerHeight();
    if(!workName.is(':hidden')) h += workName.innerHeight();
    if(signPanel) h += signPanel.innerHeight();
    attachmentContainer.css({'height': `calc(100% - ${h}px)`});
  }

  async renderSignButton(process) {
    const {userid} = AS.OPTIONS.currentUser;
    const signList = await appAPI.getSignList(this.documentID);
    const acqAgrProc = filterAgrProcesses(process);
    const isSign = signList.find(x => x.userID == userid);

    if(!acqAgrProc.length && !isSign) {

      const {panelInfo} = this.player.panels;
      const signPanel = $('<div>');
      const signButton = $('<button>', {class: "uk-button margined uk-button-default window-action-button fonts"});
      const buttonName = i18n.tr('Подписать');

      signPanel.css({
        'display': 'flex',
        'flex-direction': 'column',
        'width': '100%',
        'align-items': 'center',
        'padding': '10px 15px',
      })

      signButton.css({
        'width': '100%',
        'white-space': 'nowrap',
        'text-overflow': 'ellipsis',
        'overflow': 'hidden',
        'padding': '0 5px',
      })
      .attr('title', buttonName)
      .html(buttonName);

      this.player.panels.signPanel = signPanel;
      signPanel.append(signButton);
      panelInfo.append(signPanel);

      signButton.on('click', e => {
        e.preventDefault();
        e.target.blur();
        signDocument(this.documentID, signButton);
      });

      this.resizePanels();
    }
  }

  async refresh(init) {
    this.panelButtons.empty();
    this.workName.empty().attr('title', null).hide();

    const {docActions, process, docInfo} = this.player;
    const run = docActions.find(x => x.operation == 'RUN');

    // Если запись не отправлена, создаем кнопку с активацией документа
    if(run) {
      if(this.player.model.hasChanges) {
        this.player.saveButton.show();
        this.player.saveButtonMobile.show();
      } else {
        this.player.saveButton.hide();
        this.player.saveButtonMobile.hide();
      }
      this.player.editReadFormButton.show();
      this.player.editReadFormButtonMobile.show();

      if(!this.regInfo) {
        //в случае если нет информации по реестру
        createButton(this.player, run.label, 'itsm-button1', () => activateDocument(this.player), true);
      } else if(this.regInfo.rights.indexOf('rr_create') !== -1) {
        //Если есть права на создание
        createButton(this.player, run.label, 'itsm-button1', () => activateDocument(this.player), true);
      }
    } else {
      const acqAgrProc = filterAgrProcesses(process);

      if(docInfo && docInfo.registered != "true" && !acqAgrProc.length) {
        if(this.player.model.hasChanges) {
          this.player.saveButton.show();
          this.player.saveButtonMobile.show();
        } else {
          this.player.saveButton.hide();
          this.player.saveButtonMobile.hide();
        }

        this.player.editReadFormButton.show();
        this.player.editReadFormButtonMobile.show();
      }

      if(init) {
        this.actionButtons = new Document(this.player, process);
      } else {
        this.actionButtons.init(true);
      }

    }

    this.renderSignButton(process);
  }

  init(){
    this.regInfo = getRegInfo(this.registryCode);

    //Если нет прав на редактирование
    if(this.regInfo && !this.regInfo.rights.includes('rr_edit')) this.editReadFormButton.hide();

    this.player.buttonClass = this;
    this.refresh(true);
  }
}

class ARMDocumentButtons {
  constructor(player) {
    this.player = player;
    this.dataUUID = player.model.asfDataId;
    this.editable = player.view.editable;
    this.workName = player.panels.workName;
    this.panelButtons = player.panels.panelButtons;
    this.panelActions = player.panels.panelActions;
    this.mobileMenu = player.panels.mobileMenu;
    this.formPanel = player.panels.formPanel;
    this.editReadFormButton = null;
    this.saveButton = null;
    this.printButton = null;

    this.init();
  }

  async canEdit() {
    const {docActions, process, docInfo, workInfo} = this.player;
    const acqAgrProc = filterAgrProcesses(process);

    if(!docInfo) return false;
    if(docInfo.registered == "true") return false;

    if(workInfo) {
      const {actionID, has_subprocesses, parent_process} = workInfo;
      const procCode = ['approval-single', 'agreement-single', 'acquaintance-single'];
      if(procCode.includes(parent_process)) return false;

      if(has_subprocesses == "true") {
        const subworks = await appAPI.getSubworks(actionID);
        const filtered = subworks.filter(x => procCode.includes(x.parent_process));
        if(filtered.length) return false;
      }
    }

    return true;
  }

  async changcheButtons(){
    Cons.showLoader();
    const can_edit = await this.canEdit();
    Cons.hideLoader();

    if(!can_edit) {
      this.saveButton.hide();
      this.saveButtonMobile.hide();
      this.editReadFormButton.hide();
      this.editReadFormButtonMobile.hide();
    } else {
      if(this.editable) {
        this.editReadFormButton.text('description');
        this.editReadFormButtonMobile.text('description');
        this.editReadFormButton.attr('uk-tooltip', `title: ${i18n.tr('Просмотр')}; duration: 300;`);

        this.printButton.hide();
        this.printButtonMobile.hide();
        this.saveButton.show();
        this.saveButtonMobile.show();

      } else {
        this.editReadFormButton.text('edit');
        this.editReadFormButtonMobile.text('edit');
        this.editReadFormButton.attr('uk-tooltip', `title: ${i18n.tr('Редактировать')}; duration: 300;`);

        this.printButton.show();
        this.printButtonMobile.show();
        if(this.player.model.hasChanges) {
          this.saveButton.show();
          this.saveButtonMobile.show();
        } else {
          this.saveButton.hide();
          this.saveButtonMobile.hide();
        }
      }
    }

  }

  renderPanelActions(){
    const buttonsPanel = $('<div>', {class: 'arm-panel-buttons'});
    const oneMobilePanel = $('<div>', {class: 'arm-mobile-one-panel'});
    this.menuIcon = $('<div>', {class: 'menu__icon'}).append('<span>');

    this.saveButton = $('<span class="material-icons action-icon-button" style="display: none;">save</span>');
    this.printButton = $('<span class="material-icons action-icon-button">print</span>');
    this.editReadFormButton = $('<span class="material-icons action-icon-button">edit</span>');

    this.saveButtonMobile = $('<span class="material-icons action-icon-button mobile" style="display: none;">save</span>');
    this.printButtonMobile = $('<span class="material-icons action-icon-button mobile">print</span>');
    this.editReadFormButtonMobile = $('<span class="material-icons action-icon-button mobile">edit</span>');

    this.saveButton.attr('uk-tooltip', `title: ${i18n.tr('Сохранить')}; duration: 300;`);
    this.printButton.attr('uk-tooltip', `title: ${i18n.tr('Печать')}; duration: 300;`);

    oneMobilePanel.append(this.printButtonMobile, this.editReadFormButtonMobile, this.saveButtonMobile);
    buttonsPanel.append(
      $('<div>').append(this.saveButton, this.printButton),
      $('<div>').append(this.editReadFormButton)
    );
    this.panelActions.append(this.menuIcon, buttonsPanel);
    this.mobileMenu.append(oneMobilePanel, $('<div>', {class: 'arm-mobile-two-panel'}));

    this.player.editReadFormButton = this.editReadFormButton;
    this.player.editReadFormButtonMobile = this.editReadFormButtonMobile;
    this.player.saveButton = this.saveButton;
    this.player.saveButtonMobile = this.saveButtonMobile;

    this.changcheButtons();
  }

  hideShowMobileMenu(){
    this.menuIcon.toggleClass('_active');
    this.mobileMenu.toggleClass('_active');
    this.formPanel.toggleClass('_lock');
  }

  editRead(e){
    e.preventDefault();
    e.target.blur();
    this.editable = !this.editable;
    this.player.view.setEditable(this.editable);
    this.changcheButtons();
  }

  save(e){
    e.preventDefault();
    e.target.blur();
    saveFormData(this.player);
  }

  print(e){
    e.preventDefault();
    e.target.blur();
    if(this.player.model.hasPrintable) {
      const dataUUID = this.player.model.asfDataId;
      window.open(`../Synergy/rest/asforms/template/print/form?format=pdf&dataUUID=${dataUUID}`);
    } else {
      printForm(this.player.view.container[0]);
    }
  }

  addListeners(){
    this.editReadFormButton.on('click', e => {
      this.editRead(e);
    });
    this.editReadFormButtonMobile.on('click', e => {
      this.editRead(e);
      this.hideShowMobileMenu();
    });

    this.saveButton.on('click', e => {
      this.save(e);
    });
    this.saveButtonMobile.on('click', e => {
      this.save(e);
      this.hideShowMobileMenu();
    });

    this.printButton.on('click', e => {
      this.print(e);
    });
    this.printButtonMobile.on('click', e => {
      this.print(e);
      this.hideShowMobileMenu();
    });

    this.menuIcon.on('click', e => {
      this.hideShowMobileMenu();
    });

    this.mobileMenu.on('hideShowMenu', e => {
      this.hideShowMobileMenu();
    });
  }

  init(){
    this.renderPanelActions();
    this.addListeners();
    this.workName.text(this.player.documentName);
    new ButtonsActions(this.player);
  }
}

class ARMDocument {
  constructor(panel, param) {
    this.documentName = param.meaning || param.formName;
    this.meaning = param.meaning;
    this.formName = param.formName;
    this.dataUUID = param.dataUUID;
    this.editable = param.editable || false;
    this.viewCode = param.viewCode || false;
    this.registryCode = param.registryCode;
    this.matchingParam = param.matchingParam || null;

    this.panel = panel;
    this.window = null;
    this.state = null;
    this.player = null;

    this.init();
  }

  async initSynergyPlayer(handler){
    this.player = AS.FORMS.createPlayer();
    this.player.view.setEditable(this.editable);
    this.player.showFormData(null, null, this.dataUUID);
    if(this.viewCode) this.player.model.showView(this.viewCode);

    this.player.registryCode = this.registryCode;
    this.player.formName = this.formName;
    this.player.documentName = this.documentName;
    this.player.meaning = this.meaning;
    this.player.dataUUID = this.dataUUID;
    this.player.mainDataUUID = this.dataUUID;
    this.player.matchingParam = this.matchingParam;

    this.player.documentID = await appAPI.getDocumentIdentifier(this.player.dataUUID);
    this.player.docInfo = await appAPI.getDocumentInfo(this.player.documentID);
    this.player.docActions = await AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/docflow/document_actions?documentID=${this.player.documentID}&locale=${AS.OPTIONS.locale}`);
    this.player.process = await AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/workflow/get_execution_process?documentID=${this.player.documentID}&locale=${AS.OPTIONS.locale}`);
    this.player.registryInfo = await appAPI.getRegistryInfo(this.registryCode);

    const processes = getUserWork(this.player.process, AS.OPTIONS.currentUser.userid);
    if(processes.length) {
      const {actionID} = processes[processes.length - 1];
      const workInfo = await appAPI.getWorkInfo(actionID);
      workInfo.childDocuments = await appAPI.getChildDocuments(this.player.documentID);
      workInfo.dataUUID = this.player.dataUUID;
      workInfo.documentID = this.player.documentID;
      this.player.workInfo = workInfo;
    }

    //чет событие dataLoad перестало работать, ебашим по таймауту
    // this.player.model.on('dataLoad', () => {
    //   if(this.player.matchingParam) initMatching(this.player);
    // });

    if(this.player.matchingParam) {
      setTimeout(() => {
        initMatching(this.player);
      }, 1000);
    }

    handler();
  }

  renderWindow(){
    this.window = $('<div>', {class: 'arm-window-document'}).attr('data-uuid', this.dataUUID);
    const header = $('<div>', {class: 'arm-header uk-modal-header'});
    const body = $('<div>', {class: 'arm-body'});
    const leftPanel = $('<div>', {class: 'arm-left-panel'});

    const content = $('<div>', {class: 'arm-content'});
    const formPanel = $('<div>', {class: 'arm-form-panel'});
    const panelActions = $('<div>', {class: 'arm-form-panel-actions'});
    const currentFormContainer = $('<div>', {class: 'arm-form-container file-view-panel active'});
    const attachmentViewContainer = $('<div>', {class: 'arm-attachment-container file-view-panel'});

    const buttonsHeader = $('<div>', {class: 'arm-header-buttons'});
    const minButton = $('<span class="arm-min-button uk-margin-small-right" uk-icon="chevron-down"></span>');
    const closeButton = $('<span class="arm-close-button" uk-icon="close"></span>');

    const panelButtons = $('<div>', {class: 'arm-form-panel-buttons'});
    const workName = $('<div>', {class: 'arm-work-name'});
    const mobileMenu = $('<div>', {class: 'arm-menu-mobile'});
    const commentsContainer = $('<div>', {class: 'arm-comments-container'});
    const attachmentContainer = $('<div>', {class: 'arm-attachment-container'});

    const panelSwitchButtons = $('<div>', {class: 'arm-switch-buttons uk-flex-inline uk-flex-wrap uk-flex-left uk-flex-middle uk-flex-wrap-middle'});
    const buttonSwitchInfo = $('<div>', {class: 'uk-button uk-button-default button-switch fonts pressed'});
    const buttonSwitchComment = $('<div>', {class: 'uk-button uk-button-default button-switch fonts'});

    const panelInfo = $('<div>', {class: 'arm-left-panel_info'});
    const panelComments = $('<div>', {class: 'arm-left-panel_comments', style: 'display: none;'});

    buttonSwitchInfo.append(`<span uk-icon="icon: info;"></span>`);
    buttonSwitchComment.append(`<span uk-icon="icon: comment;"></span>`);

    this.player.panels = {
      workName,
      commentsContainer,
      attachmentContainer,
      currentFormContainer,
      attachmentViewContainer,
      panelInfo,
      panelButtons,
      panelActions,
      mobileMenu,
      formPanel,
      leftPanel,
      documentWindow: this.window,
      documentPanel: this.panel
    };

    panelSwitchButtons.append(buttonSwitchInfo, buttonSwitchComment);
    panelInfo.append(workName, panelButtons, attachmentContainer);
    panelComments.append(commentsContainer);
    leftPanel.append(panelSwitchButtons, panelInfo, panelComments);

    formPanel.append(this.player.view.container);
    content.append(currentFormContainer, attachmentViewContainer);
    currentFormContainer.append(panelActions, formPanel);

    buttonsHeader.append(minButton, closeButton);
    header.append(`<h3 style="overflow: hidden; white-space: nowrap; width: calc(100% - 40px); margin: 0; font-size: 1.1em;">${this.documentName}</h3>`, buttonsHeader);
    body.append(leftPanel, content);
    this.window.append(header, body, mobileMenu);
    $('body').append(this.window);

    minButton.on('click', e => this.hideShowDocument());
    closeButton.on('click', e => {
      checkSavedForm(this.player, () => {
        closeDocument(this.player);
      });
    });

    buttonSwitchInfo.on('click', e => {
      e.preventDefault();
      e.target.blur();
      buttonSwitchComment.removeClass('pressed');
      buttonSwitchInfo.addClass('pressed');
      panelComments.hide();
      panelInfo.fadeIn();
    });

    buttonSwitchComment.on('click', e => {
      e.preventDefault();
      e.target.blur();
      buttonSwitchInfo.removeClass('pressed');
      buttonSwitchComment.addClass('pressed');
      panelInfo.hide();
      panelComments.fadeIn();
    });

    new ARMDocumentButtons(this.player);
  }

  openDocument(){
    this.window.fadeIn(200);
    this.state = 'open';
    $('.arm-window-document').attr('arm-active-window', null);
    this.window.attr('arm-active-window', true).attr('state', this.state);
    $('.footer-document').removeClass('arm-active-panel');
    this.panel.addClass('arm-active-panel');
  }

  hideShowDocument(){
    if(!this.window.attr('arm-active-window')) {
      this.openDocument();
    } else {
      if(this.state == 'open') {
        this.state = 'hide';
        this.panel.removeClass('arm-active-panel');
        this.window.fadeOut(200).attr('arm-active-window', null).attr('state', this.state);

        let aw = $('.arm-window-document[state="open"]');
        if(aw.length) {
          aw = $(aw[0]);
          aw.attr('arm-active-window', true);
          const uuid = aw.attr('data-uuid');
          $(`.footer-document[data-uuid="${uuid}"]`).addClass('arm-active-panel');
        }
      } else {
        this.openDocument();
      }
    }
  }

  addAttacmentListener(){
    const {attachmentContainer, currentFormContainer, attachmentViewContainer} = this.player.panels;

    attachmentContainer.on('file_dbl_click file_context_open', async e => {
      const {name, uuid, dataUUID, is_editable, mobile} = e.eventParam;
      if(dataUUID) {
        attachmentViewContainer.removeClass('active');
        currentFormContainer.addClass('active');
        if(dataUUID != this.dataUUID) this.player.showFormData(null, null, dataUUID);
      } else {
        attachmentViewContainer.empty();
        currentFormContainer.removeClass('active');
        attachmentViewContainer.addClass('active');
        const file = new AttachmentFile(name, uuid, attachmentViewContainer);
        file.open();
      }
    });
  }

  init(){
    this.initSynergyPlayer(() => {
      if(!this.window) this.renderWindow();
      this.openDocument();

      this.panel.on('click', e => {
        this.hideShowDocument();
      });

      this.window.on('show-document', e => {
        this.hideShowDocument();
      });

      this.window.on('hide-window', e => {
        this.window.hide();
        this.state = 'hide';
        this.window.attr('arm-active-window', null).attr('state', this.state);
        this.panel.removeClass('arm-active-panel');
      });

      new DocumentComments(this.player.documentID, this.player.panels.commentsContainer);
      new DocumentAttachments(this.player);

      this.addAttacmentListener();
    });
  }
}

class ARMFooter {
  constructor() {
    this.footer = null;
    this.init();
  }

  async renderDocToFooter(param){
    param.meaning = await appAPI.getDocMeaningContent(param.dataUUID);
    const panel = $('<div>', {class: 'footer-document'})
    .attr('data-uuid', param.dataUUID)
    .attr('title', param.meaning || param.formName)
    .html(`<span uk-icon="icon: file-text"></span> ${param.meaning || param.formName}`);

    this.footer.append(panel);
    new ARMDocument(panel, param);
  }

  openDocument(param){
    const {openDocsWindow = []} = Cons.getAppStore();
    const doc = openDocsWindow.find(x => x.dataUUID === param.dataUUID);
    if(!doc) {
      this.renderDocToFooter(param);
      openDocsWindow.push(param);
      Cons.setAppStore({openDocsWindow: openDocsWindow});
    } else {
      $(`.arm-window-document[data-uuid="${param.dataUUID}"]`).trigger('show-document');
    }
  }

  initListener(){
    if(Cons.getAppStore().open_document_listener) return;

    if(Cons.getCurrentPage().code == 'main_page') {
      addListener('custom_open_document', 'root-panel', event => this.openDocument(event));
      Cons.setAppStore({open_document_listener: true});
    };

    $('#root-panel').off().on('custom_open_document', e => {
      if(e.hasOwnProperty('eventParam')) this.openDocument(e.eventParam);
    });
  }

  renderPanel() {
    const {code} = Cons.getCurrentPage();
    if(['authPage', 'auth_page', 'start_page'].includes(code)) return;

    if($('.footer-panel').length) {
      this.footer = $('.footer-panel');
    } else {
      this.footer = $('<div>', {class: 'footer-panel'});
      const homeButton = $('<span class="uk-margin-small-right arm-home-button" uk-icon="icon: home; ratio: 1.5"></span>');

      homeButton.on('click', e => $('.arm-window-document').trigger('hide-window'));
      this.footer.append(homeButton);
    }

    $('#root-panel').append(this.footer);
  }

  init(){
    this.renderPanel();
    this.initListener();
  }
}

new ARMFooter();

$(document).mouseup(event => {
	setTimeout(() => {
		AS.FORMS.popupPanel.hide();
	}, 0);
}).click(event => {
	AS.FORMS.popupPanel.hide();
  $('.workflow-context-menu').remove();
}).on('contextmenu', () => $('.workflow-context-menu').remove());
