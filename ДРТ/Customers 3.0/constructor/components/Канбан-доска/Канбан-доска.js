$.getScript("https://cdnjs.cloudflare.com/ajax/libs/alasql/0.6.1/alasql.min.js");

const compContainer = $(`#${comp.code}`);
const boardContent = compContainer.find('.kanban-board-content');

const getRegistryList = async () => {
  const {registryList} = Cons.getAppStore();
  return new Promise(async resolve => {
    if(registryList) {
      resolve(registryList);
    } else {
      const list = await appAPI.getRegistryList();
      list ? resolve(UTILS.parseRegistryList(list)) : resolve(null);
    }
  });
}

const formatDate = () => {
  let d = new Date();
  return ['0' + d.getDate(), '0' + (d.getMonth() + 1), '' + d.getFullYear()].map(x => x.slice(-2)).join('.');
}

const getSystemReport = () => {
  Cons.showLoader();
  AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/filters?registryCode=${KanbanBoard.registryCode}&type=service`)
  .then(filters => {
    let filterID = filters.find(x => x.code === KanbanBoard.filterCode);
    if(filterID) filterID = filterID.id;
    Cons.hideLoader();

    let url = `${window.location.origin}/Synergy/rest/reg/load/xls?r=${KanbanBoard.registryID}`;
    url += `&l=ru&f=${filterID || ''}&s=&u=${AS.OPTIONS.currentUser.userid}&fn=${KanbanBoard.registryName}_${formatDate()}`;
    window.open(url);
  });
}

const KanbanBoard = {
  registryCode: null,
  registryID: null,
  registryName: '',
  registryRights: [],
  allRights: [],
  filterCode: null,
  formCode: null,
  formId: null,
  formName: '',
  statusDict: null,
  fields: null,
  fieldDict: null,
  countInPart: 5,

  heads: [],

  columns: [],
  searchString: null,
  filterSearchUrl: null,
  sum: null,
  pageCode: null,

  stringLength: 50,

  isDelete: false,
  selectedItems: [],

  finishedCreateRecord: null,

  getNextFieldNumber: function(url) {
    let p = url.substring(url.indexOf('?') + 1).split('&');
    p = p.map(x => {
      x = x.split('=');
      if(x[0].indexOf('field') !== -1 && x[0] !== 'fields') return x[0];
    }).filter(x => x).sort();
    if(p.length) return Number(p[p.length - 1].substring(5)) + 1;
    return '';
  },

  getFullDataUrl: function(){
    let url = `api/registry/data_ext?registryCode=${this.registryCode}&locale=${AS.OPTIONS.locale}`;
    if(this.filterCode) url+=`&filterCode=${this.filterCode}`;
    if(this.heads && this.heads.length > 0) this.heads.forEach(item => url+=`&fields=${item.columnID}`);
    if(this.searchString) url += `&searchString=${this.searchString}`;
    if(this.filterSearchUrl) url += this.filterSearchUrl;
    return url;
  },

  getUrlParam: function(column) {
    const {key, currentPage, countInPart} = column;
    let param = `?registryCode=${this.registryCode}`;
    if(this.filterCode) param += `&filterCode=${this.filterCode}`;
    if(this.fields && this.fields.length > 0) {
      this.fields.forEach(x => param += `&fields=${x.code}`);
      param+=`&fields=${this.fieldDict}`;
    }
    if(this.searchString) param += `&searchString=${this.searchString}`;

    if(this.filterSearchUrl) {
      param += this.filterSearchUrl;
      if(key) {
        let next = this.getNextFieldNumber(this.filterSearchUrl);
        param += `&field${next}=${this.fieldDict}&condition${next}=TEXT_EQUALS&key${next}=${key}`;
      }
    } else {
      if(key) param += `&field=${this.fieldDict}&condition=TEXT_EQUALS&key=${key}`;
    }

    param += `&pageNumber=${currentPage}&countInPart=${countInPart}`;
    return param;
  },

  searchInRegistry: async function(param){
    return new Promise(resolve => {
      rest.synergyGet(`api/registry/data_ext${param}`, resolve, err => {
        console.log(`KanbanBoard ERROR [ searchInRegistry ]: ${JSON.stringify(err)}`);
        resolve(null);
      });
    });
  },

  getSumRowInRegistry: async function(fieldCode, key){
    let param = `?registryCode=${this.registryCode}`;
    if(this.filterCode) param += `&filterCode=${this.filterCode}`;
    if(this.searchString) param += `&searchString=${this.searchString}`;

    if(this.filterSearchUrl) {
      param += this.filterSearchUrl;
      if(key) {
        let next = this.getNextFieldNumber(this.filterSearchUrl);
        param += `&field${next}=${this.fieldDict}&condition${next}=TEXT_EQUALS&key${next}=${key}`;
      }
    } else {
      if(key) param += `&field=${this.fieldDict}&condition=TEXT_EQUALS&key=${key}`;
    }

    param += `&fieldCode=${fieldCode}&countType=sum`;

    return new Promise(resolve => {
      rest.synergyGet(`api/registry/count_data${param}`, resolve, err => {
        console.log(`KanbanBoard ERROR [ getSumRowInRegistry ]: ${JSON.stringify(err)}`);
        resolve(null);
      });
    });
  },

  getDictInfo: async function(){
    const {code, title, value, color, end, quality} = this.statusDict;
    const {columns, items, name} = await appAPI.getDictionary(code);
    this.columns = [];

    for(let key in items) {
      const item = items[key];

      const parseItem = {
        key: item[value]?.value,
        countInPart: this.countInPart,
        currentPage: 0
      };

      if(item.hasOwnProperty(title)) {
        if(item[title].hasOwnProperty('translations')) {
          parseItem.name = item[title]?.translations[AS.OPTIONS.locale];
        } else {
          parseItem.name = item[title]?.value;
        }
      }

      if(color) parseItem.color = item[color]?.value;
      if(end) parseItem.end = item[end]?.value;
      if(quality) parseItem.quality = item[quality]?.value;

      this.columns.push(parseItem);
    }

    this.columns.sort((a,b) => a.key - b.key);
  },

  allowDrop: function(event) {
    event.preventDefault();
  },
  drag: function(param) {
    const {event, dataUUID, documentID} = param;
    const parentColumnID = event.target.dataset.parentcolumnid;
    event.originalEvent.dataTransfer.setData("dataUUID", dataUUID);
    event.originalEvent.dataTransfer.setData("documentID", documentID);
    event.originalEvent.dataTransfer.setData("parentColumnID", parentColumnID);
  },

  openDocument: function(dataUUID, updateData = false) {
    if(this.allRights.includes('rr_read') || this.allRights.includes('rr_edit')) {
      fire({ type: "registry_item_click", dataUUID }, comp.code);
    } else {
      showMessage(i18n.tr('У вас нет прав на просмотр этого документа'), 'warning');
    }
  },

  cutString: function(str) {
    return str.length > this.stringLength ? `${str.substr(0, this.stringLength)}...` : str;
  },

  getTitleBlock: function(text, dataUUID){
    const block = $('<div>', {class: 'column-card-block title'});
    const blockValue = $('<div>', {class: 'column-card-block-value'});

    block.append(blockValue);
    blockValue.text(this.cutString(text));
    blockValue.attr('uk-tooltip', `title: ${text}; duration: 300;`);

    block.on('click', e => {
      this.openDocument(dataUUID, true);
    });

    return block;
  },

  getSumBlock: function(sum, prefix){
    const block = $('<div>', {class: 'column-card-block sum'});
    const blockValue = $('<div>', {class: 'column-card-block-value'});

    block.append(blockValue);
    blockValue.text(`${sum} ${prefix}`);

    return block;
  },

  getReglinkBlock: function(title, code, fieldValue, fieldKey){
    if(!fieldKey.hasOwnProperty(code)) return;

    const block = $('<div>', {class: 'column-card-block reglink'});
    const blockTitle = $('<div>', {class: 'column-card-block-title'});
    const blockValue = $('<div>', {class: 'column-card-block-value'});

    block.append(blockTitle, blockValue);
    blockTitle.text(`${i18n.tr(title)}:`);
    blockValue.text(fieldValue[code]);

    block.on('click', async e => {
      const dataUUID = await appAPI.getAsfDataUUID(fieldKey[code]);
      this.openDocument(dataUUID);
    });

    return block;
  },

  getUserBlock: function(title, value){
    const block = $('<div>', {class: 'column-card-block user'});
    const blockTitle = $('<div>', {class: 'column-card-block-title'});
    const blockValue = $('<div>', {class: 'column-card-block-value'});

    block.append(blockTitle, blockValue);
    blockTitle.text(`${i18n.tr(title)}:`);
    blockValue.text(value);

    return block;
  },

  getDefaultBlock: function(title, value){
    const block = $('<div>', {class: 'column-card-block'});
    const blockTitle = $('<div>', {class: 'column-card-block-title'});
    const blockValue = $('<div>', {class: 'column-card-block-value'});

    block.append(blockTitle, blockValue);
    blockTitle.text(`${i18n.tr(title)}:`);
    blockValue.text(value);

    return block;
  },

  removeRegistryRow: function(dataUUID, e) {
    e.preventDefault();
    e.target.blur();
    UIkit.modal.confirm(i18n.tr('Вы действительно хотите удалить запись?'), {labels: {ok: i18n.tr('Да'), cancel: i18n.tr('Отмена')}}).then(() => {
      Cons.showLoader();
      try {
        rest.synergyGet(`api/registry/delete_doc?dataUUID=${dataUUID}`, res => {
          if(res.errorCode != '0') throw new Error(res.errorMessage);
          showMessage(i18n.tr("Запись удалена"), "success");
          this.render();
          Cons.hideLoader();
        });
      } catch (err) {
        Cons.hideLoader();
        showMessage(i18n.tr("Произошла ошибка при удалении записи"), "error");
        console.log(error);
      }
    }, () => null);
  },

  contextMenu: function(el, dataRow){
    this.isDelete = false;
    if(this.allRights.indexOf('rr_delete') !== -1 && dataRow.status != 'STATE_NOT_FINISHED') {
      this.isDelete = true;
    }

    el.on('contextmenu', event => {
      $('.board-context-menu').remove();
      $('<div>', {class: 'board-context-menu'})
      .css({
        "left": event.pageX + 'px',
        "top": event.pageY + 'px'
      })
      .appendTo('body')
      .append(
        $('<ul class="uk-nav-default uk-nav-parent-icon" uk-nav>')
        .append($(`<li ><a href="javascript:void(0);"><span class="uk-margin-small-right" uk-icon="icon: push"></span>${i18n.tr("Открыть")}</a></li>`)
          .on('click', e => {
            this.openDocument(dataRow.dataUUID, true);
          })
        )
        .append('<li class="uk-nav-divider"></li>')
        .append($(`<li ${this.isDelete ? '' : 'class="uk-disabled"'} ><a href="javascript:void(0);"><span class="uk-margin-small-right" uk-icon="icon: trash"></span>${i18n.tr("Удалить запись")}</a></li>`)
          .on('click', e => {
            this.isDelete ? this.removeRegistryRow(dataRow.dataUUID, e) : false;
          })
        )
      )
      .show('fast');
      return false;
    });
  },

  getCheckbox: function(){
    const checkboxContainer = $('<label>', {class: "custom-checkbox"});
    const inputCheckbox = $(`<input type="checkbox">`);
    const checkmark = $('<span>', {class: "checkmark"});

    checkboxContainer.append(inputCheckbox, checkmark);

    return {checkboxContainer, inputCheckbox};
  },

  getColumnCard: function(item, columnID){
    const {dataUUID, documentID, fieldValue, fieldKey} = item;
    const cardContainer = $('<div>', {class: 'column-card', id: `card-${dataUUID}`});
    const cardData = $('<div>', {class: 'card-data'});
    const cardAction = $('<div>', {class: 'card-action'});
    const {checkboxContainer, inputCheckbox} = this.getCheckbox();

    cardContainer.attr('data-uuid', dataUUID);
    cardContainer.attr('data-documentid', documentID);
    cardContainer.attr('data-parentcolumnid', columnID);
    cardContainer.attr('draggable', true);

    cardContainer.append(cardData, cardAction);

    cardAction.append(checkboxContainer);

    for(let i = 0; i < this.fields.length; i++) {
      const {title, code, type, prefix = '₸'} = this.fields[i];
      if(!fieldValue.hasOwnProperty(code)) continue;

      switch (type) {
        case 'title': cardData.append(this.getTitleBlock(fieldValue[code], dataUUID)); break;
        case 'sum': cardData.append(this.getSumBlock(fieldValue[code], prefix)); break;
        case 'reglink': cardData.append(this.getReglinkBlock(title, code, fieldValue, fieldKey)); break;
        case 'user': cardData.append(this.getUserBlock(title, fieldValue[code])); break;
        default: cardData.append(this.getDefaultBlock(title, fieldValue[code])); break;
      }
    }

    inputCheckbox.on('change', e => {
      if(e.target.checked) {
        if(this.selectedItems.indexOf(documentID) === -1) {
          this.selectedItems.push(documentID);
        }
      } else {
        let index = this.selectedItems.indexOf(documentID);
        if(index !== -1) this.selectedItems.splice(index, 1);
      }
      if(this.selectedItems.length > 0) {
        $('.action-menu-item').removeClass('uk-disabled');
        if($('.action-menu-item.item-delete').length) {
          if(!this.isDelete) $('.action-menu-item.item-delete').addClass('uk-disabled');
        }
      } else {
        $('.action-menu-item').addClass('uk-disabled');
      }
    });

    cardContainer.on("dragstart", event => this.drag({event, dataUUID, documentID}));

    this.contextMenu(cardContainer, item);

    return cardContainer;
  },

  openFinishRecord: async function(dataUUID) {
    Cons.showLoader();

    const player = UTILS.getSynergyPlayer(dataUUID, true);
    const meaning = await appAPI.getDocMeaningContent(dataUUID);

    const dialog = await UTILS.getModalDialog(
      meaning,
      player.view.container,
      i18n.tr('Запустить'),
      async () => {
        if (!player.model.isValid()) {
          showMessage(i18n.tr('Заполните обязательные поля'), 'error');
        } else {
          Cons.showLoader();

          player.saveFormData(async asfDataId => {
            await appAPI.activateDoc(asfDataId);
            Cons.hideLoader();
            UIkit.modal(dialog).hide();
          }, (status, responseText) => {
            console.log('ERROR [saveFormData]', {status, responseText});
            throw new Error(responseText);
          });

        }
      }
    );

    dialog.find('.uk-modal-dialog').css('width', '50%');

    UIkit.modal(dialog).show();

    Cons.hideLoader();

    dialog.on('hidden', () => {
      player.destroy();
      dialog.remove();
    });
  },

  createFinishRecord: async function(dataUUID) {
    Cons.showLoader();

    try {
      const {registryCode, matching = [], currentLink} = this.finishedCreateRecord;
      const documentID = await AS.FORMS.ApiUtils.getDocumentIdentifier(dataUUID);
      const meaning = await appAPI.getDocMeaningContent(dataUUID);
      const currentFormData = await appAPI.loadAsfData(dataUUID);

      const asfData = [];

      asfData.push({
        id: currentLink,
        type: 'reglink',
        key: documentID,
        valueID: documentID,
        value: meaning
      });

      for(let i = 0; i < matching.length; i++) {
        const {from, to} = matching[i];
        const fromData = UTILS.getValue(currentFormData, from);
        if (fromData) UTILS.setValue(asfData, to, fromData);
      }

      const resultCreate = await appAPI.createDocRCC(registryCode, asfData);

      if(!resultCreate) throw new Error(i18n.tr('Ошибка создания записи'));

      this.openFinishRecord(resultCreate.dataID);
    } catch (err) {
      Cons.hideLoader();
      showMessage(err.message, 'error');
    }
  },

  renderColumn: async function(column, data){
    const {name, key, color, recordsCount} = column;
    const columnContainer = $('<div>', {class: 'column-container', id: `column-${key}`});
    const columnTitle = $('<span>', {class: 'column-title'});
    const columnSum = $('<span>', {class: 'column-sum'});
    const columData = $('<div>', {class: 'column-data'});
    const getRowsButton = $('<span>', {class: 'column-get-rows-button'});
    let rowsButtonIsHidden = true;

    columnContainer.append(columnTitle, columnSum, columData);
    boardContent.append(columnContainer);

    if(this.sum) {
      const {cmp, prefix = '₸'} = this.sum;
      const sumResult = await this.getSumRowInRegistry(cmp, key);
      const sum = new Intl.NumberFormat('ru-RU').format(Number(sumResult[`${cmp}_0`]) || 0) + ` ${prefix}`;
      columnSum.text(sum);
    } else {
      columnSum.hide();
    }

    if(color) columnTitle.css('background', color);

    columnTitle.text(`${name} (${recordsCount})`);
    columnTitle.attr('uk-tooltip', `title: ${name} (${recordsCount}); duration: 300;`);
    getRowsButton.text(i18n.tr('ещё...'));

    data.forEach(item => {
      const card = this.getColumnCard(item, key);
      columData.append(card);
    });

    if(column.countInPart < recordsCount) {
      columData.append(getRowsButton);
      rowsButtonIsHidden = false;

      getRowsButton.on('click', async e => {
        Cons.showLoader();
        column.currentPage++;
        const searchResult = await this.searchInRegistry(this.getUrlParam(column));

        if(searchResult && searchResult.recordsCount > 0) {
          searchResult.result.forEach(item => {
            const card = this.getColumnCard(item, key);
            columData.append(card);
            if((column.countInPart * (column.currentPage + 1)) < recordsCount) {
              getRowsButton.detach().appendTo(columData);
              rowsButtonIsHidden = false;
            } else {
              getRowsButton.remove();
              rowsButtonIsHidden = true;
            }
          });
        } else {
          getRowsButton.hide();
          rowsButtonIsHidden = true;
        }
        Cons.hideLoader();
      });
    }

    columData.on('drop', async event => {
      event.preventDefault();
      const dataUUID = event.originalEvent.dataTransfer.getData("dataUUID");
      const parentColumnID = event.originalEvent.dataTransfer.getData("parentColumnID");
      const dataColumn = event.target.closest('.column-data');
      const parentColumn = this.columns.find(x => x.key == parentColumnID);

      if(parentColumn.key == column.key) return;

      const finishDrop = async () => {
        Cons.showLoader();

        const mergAsfData = [];

        mergAsfData.push({
          id: this.fieldDict,
          type: "listbox",
          value: column.name,
          key: column.key
        });

        if(this.finishedCreateRecord && column.end == '1' && column.quality == '1') {
          const currentDate = new Date();
          mergAsfData.push({
            id: this.finishedCreateRecord.currentDate,
            type: "date",
            value: dateUtils.formatDate(currentDate, dateUtils.CUSTOM_FORMAT_DATE),
            key: dateUtils.formatDate(currentDate, dateUtils.DATE_FORMAT_FULL)
          });
        }

        const mergeResult = await appAPI.mergeFormData({uuid: dataUUID, data: mergAsfData});

        if(mergeResult) {
          dataColumn.appendChild(document.getElementById(`card-${dataUUID}`));
          parentColumn.recordsCount--;
          column.recordsCount++;

          columnTitle.text(`${column.name} (${column.recordsCount})`);
          $(`#column-${parentColumnID} > .column-title`).text(`${parentColumn.name} (${parentColumn.recordsCount})`);
          $(`#card-${dataUUID}`).attr('data-parentcolumnid', column.key);

          if(this.sum) {
            const {cmp, prefix = '₸'} = this.sum;
            const parentSumResult = await this.getSumRowInRegistry(cmp, parentColumnID);
            const currentSumResult = await this.getSumRowInRegistry(cmp, key);

            const parentSum = new Intl.NumberFormat('ru-RU').format(Number(parentSumResult[`${cmp}_0`]) || 0) + ` ${prefix}`;
            const currentSum = new Intl.NumberFormat('ru-RU').format(Number(currentSumResult[`${cmp}_0`]) || 0) + ` ${prefix}`;
            columnSum.text(currentSum);
            $(`#column-${parentColumnID} > .column-sum`).text(parentSum);
          }

          if(!rowsButtonIsHidden) getRowsButton.detach().appendTo(columData);

          if(this.finishedCreateRecord && column.end == '1' && column.quality == '1') {
            this.createFinishRecord(dataUUID);
          } else {
            Cons.hideLoader();
          }
        } else {
          showMessage("Произошла ошибка при смене статуса", 'error');
          Cons.hideLoader();
        }
      }

      if(parentColumn.end == '1') {
        const tConfirm = {
          v1: {
            ru: `Вы уверены, что хотите перенести запись из конечного статуса "${parentColumn.name}" в статус "${column.name}"?`,
            kk: `Жазбаны "${parentColumn.name}" аяқталу күйінен "${column.name}" күйіне жылжытқыңыз келетініне сенімдісіз бе?`,
            en: `Are you sure you want to move the entry from ending status "${parentColumn.name}" to status "${column.name}"?`
          },
          v2: {
            ru: `Вы уверены, что хотите перенести ${this.finishedCreateRecord?.appName} из конечного статуса "${parentColumn.name}" в статус "${column.name}"?`,
            kk: `${this.finishedCreateRecord?.appName} "${parentColumn.name}" соңғы күйінен "${column.name}" күйіне шынымен жылжытқыңыз келе ме?`,
            en: `Are you sure you want to move ${this.finishedCreateRecord?.appName} from the final status "${parentColumn.name}" to the status "${column.name}"?`
          }
        };

        let msgConfirm = tConfirm.v1[AS.OPTIONS.locale];
        if(this.finishedCreateRecord) msgConfirm = tConfirm.v2[AS.OPTIONS.locale];

        UIkit.modal.confirm(msgConfirm, {labels: {ok: i18n.tr('Да'), cancel: i18n.tr('Отмена')}}).then(() => {
          finishDrop();
        }, () => null);
      } else {
        finishDrop();
      }

    }).on('dragover', this.allowDrop);
  },

  render: async function(){
    boardContent.empty();
    for(let i = 0; i < this.columns.length; i++) {
      const column = this.columns[i];
      const url = this.getUrlParam(column);
      const searchResult = await this.searchInRegistry(url);
      column.recordsCount = searchResult.recordsCount;
      this.renderColumn(column, searchResult.result);
    }
  },

  createMenuActions: function(massActions){
    const {containerID, items} = massActions;
    const menuContainer = $('<div uk-dropdown="mode: click">');
    const nav = $('<ul>', {class: 'uk-nav uk-dropdown-nav'});

    menuContainer.append(nav);
    $(`#${containerID}`).append(menuContainer);

    items.forEach(item => {
      const {name, handler, className = ''} = item;
      const navItem = $(`<li class="action-menu-item uk-disabled ${className}"><a href="#">${name}</a></li>`);
      nav.append(navItem);
      navItem.on('click', e => {
        handler(this.selectedItems, () => {
          this.reset();
          this.render();
        });
      });
    });

  },

  updateRigths: function(){
    if(this.filterCode) {
      const selectFilter = this.registryFilters.find(x => x.code == this.filterCode);
      if(selectFilter) {
        this.allRights = [...selectFilter.rights];
      } else {
        this.allRights = [...this.registryRights];
      }
    } else {
      this.allRights = [...this.registryRights];
    }
  },

  reset: function(){
    this.selectedItems = [];
    this.columns.forEach(column => column.currentPage = 0);
  },

  init: async function(params){
    this.initParams = params;
    Cons.showLoader();
    try {
      const {
        registryCode,
        filterCode = null,
        statusDict,
        fields,
        fieldDict,
        countInPart = 5,
        sum,
        searchString = null,
        massActions,
        finishedCreateRecord = null
      } = params;

      if(!registryCode) throw new Error(`Не передан параметр registryCode`);
      if(!statusDict) throw new Error(`Не передан параметр statusDict`);
      if(!fieldDict) throw new Error(`Не передан параметр fieldDict`);

      const registryList = await getRegistryList();
      const registry = registryList.find(x => x.registryCode == registryCode);

      if(registry) {
        const info = await appAPI.getRegistryInfo(registryCode);
        this.registryFilters = await appAPI.getRegistryFilters(registryCode);
        this.registryCode = registryCode;
        this.filterCode = filterCode;
        this.registryID = info.registryID;
        this.registryName = registry.registryName;
        this.registryRights = registry.rights;

        this.updateRigths();

        if(!this.allRights.includes("rr_list")) throw new Error(`Нет прав на просмотр данного реестра`);

        this.heads = info.columns.filter(item => item.visible != '0')
        .sort((a, b) => {
          if (a.order == 0) return 0;
          return a.order - b.order;
        })
        .map(item => {
          return {label: item.label, columnID: item.columnID}
        });

        this.formCode = info.formCode;
        this.formId = info.formId;
        this.formName = info.name;

        this.statusDict = statusDict;
        this.fields = fields;
        this.fieldDict = fieldDict;
        this.countInPart = countInPart;

        this.sum = sum;
        this.searchString = searchString;

        this.finishedCreateRecord = finishedCreateRecord;

        await this.getDictInfo();

        const saveParam = sessionStorage.getItem(`filterParam_${Cons.getCurrentPage().code}_${AS.OPTIONS.currentUser.userid}`);
        if(!saveParam) this.render();

        if(massActions) this.createMenuActions(massActions);

        Cons.hideLoader();
      } else {
        throw new Error(`Не найдено реестра с кодом: ${registryCode}`);
      }

    } catch (err) {
      Cons.hideLoader();
      console.log('ERROR init kanban board', err);
      showMessage(i18n.tr(err.message), 'error');
    }
  }

}

compContainer.off()
.on('renderNewBoard', e => {
  if(!e.hasOwnProperty('eventParam')) return;
  KanbanBoard.init(e.eventParam);
}).on('updateBoard', e => {
  KanbanBoard.reset();
  KanbanBoard.render();
}).on('translate', e => {
  if(KanbanBoard.initParams) KanbanBoard.init(KanbanBoard.initParams);
}).on('searchInRegistry', e => {
  if(!e.hasOwnProperty('eventParam')) return;
  const {searchString = null} = e.eventParam;
  KanbanBoard.searchString = searchString;
  KanbanBoard.reset();
  KanbanBoard.render();
}).on('changeFilterCode', e => {
  if(!e.hasOwnProperty('eventParam')) return;
  const {filterCode = null} = e.eventParam;
  KanbanBoard.filterCode = filterCode;
  KanbanBoard.reset();
  KanbanBoard.render();
}).on('filterRegistryRows', e => {
  if(!e.hasOwnProperty('eventParam')) return;
  const {filterSearchUrl = null} = e.eventParam;
  KanbanBoard.filterSearchUrl = filterSearchUrl;
  KanbanBoard.reset();
  KanbanBoard.render();
}).on('getXLS', e => {

  const getMsgConfirm = count => {
    const t = {
      ru: `Вы собираетесь выгрузить ${count} записей. Продолжить?`,
      kk: `Сіз ${count} жазбаны жүктеп алайын деп жатырсыз. Жалғастыру керек пе?`,
      en: `You are about to unload ${count} records. Continue?`
    }
    return t[AS.OPTIONS.locale] || t.ru;
  }

  Cons.showLoader();

  let url = KanbanBoard.getFullDataUrl();
  url += `&pageNumber=1&countInPart=1&loadData=false`;

  rest.synergyGet(url, part => {
    Cons.hideLoader();

    UIkit.modal.confirm(getMsgConfirm(part.count), {labels: {ok: i18n.tr('Да'), cancel: i18n.tr('Отмена')}})
    .then(() => {

      if(KanbanBoard.searchString || KanbanBoard.filterSearchUrl) {
        Cons.showLoader();
        rest.synergyGet(KanbanBoard.getFullDataUrl(), data => {
          let excelData = [];
          data.result.forEach(res => {
            let tmpValues = {};
            KanbanBoard.heads.forEach(col => tmpValues[col.label] = res.fieldValue[col.columnID] || "");
            excelData.push(tmpValues);
          });

          try {
            let opts = {headers: true, column: {style:{Font:{Bold:"1"}}}};
            let result = alasql(`SELECT * INTO XLS("${KanbanBoard.registryName}_${formatDate()}.xls",?) FROM ?`, [opts, excelData]);
            Cons.hideLoader();
          } catch (e) {
            console.log(e.message);
            Cons.hideLoader();
            showMessage(i18n.tr('Произошла ошибка при выгрузке записей реестра'), 'error');
          }
        });
      } else {
        getSystemReport();
      }

    }, () => null);
  });
});

$(document).off()
.on('contextmenu', () => $('.board-context-menu').remove())
.on('click', () => $('.board-context-menu').remove());
