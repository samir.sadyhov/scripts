const syfixViewComponent = 'Label';
const defaultParamDict = ['MORE', 'MORE_OR_EQUALS', 'LESS', 'LESS_OR_EQUALS', 'EQUALS', 'NOT_EQUALS', 'CONTAINS', 'NOT_CONTAINS', 'START', 'NOT_START', 'NOT_END', 'END', 'TEXT_NOT_EQUALS', 'TEXT_EQUALS'];

const compContainer = $(`#${comp.code}`);
const filterWindow = compContainer.find('.filter-window');
const filterClose = filterWindow.find('.filter-close');
const filterBody = filterWindow.find('.filter-body');
const btnFilter = filterWindow.find('#btn-filter');
const btnFilterDrop = filterWindow.find('#btn-filter-drop');
const tableParamBody = filterWindow.find('#rd-table-params').find('tbody');

const templateReportButton = filterWindow.find('#rd-button-select-report-template');
const templateSaveButton = filterWindow.find('#rd-button-save-report-template');
const templateDeleteButton = filterWindow.find('#rd-button-delete-report-template');
const templateForwardButton = filterWindow.find('#rd-button-forward-report-template');
const templateNameInput = filterWindow.find('#rd-template-name');

const translate = () => {
  $('.filter_button').attr('uk-tooltip', `title: ${i18n.tr("Расширенный поиск")}; duration: 300;`);

  compContainer.find('.filter-header h4').text(i18n.tr("Расширенный поиск"));

  templateReportButton.html(`<span uk-icon="icon: folder"></span> ${i18n.tr("Фильтры")}`);
  templateNameInput.attr('uk-tooltip', `title: ${i18n.tr("Наименование фильтра")}`);
  templateSaveButton.attr('uk-tooltip', `title: ${i18n.tr("Сохранить")}`);
  templateDeleteButton.attr('uk-tooltip', `title: ${i18n.tr("Удалить")}`);
  templateForwardButton.attr('uk-tooltip', `title: ${i18n.tr("Передать")}`);

  btnFilter.text(i18n.tr("Фильтровать"));
  btnFilterDrop.text(i18n.tr("Сбросить"));
}

const RD = {
  formCode: null,
  formData: null,
  paramSearch: {},

  paramFields: {
    registryRouteStatus: ['STATE_SUCCESSFUL', 'STATE_NOT_FINISHED', 'NO_ROUTE', 'STATE_UNSUCCESSFUL'],
    textarea: defaultParamDict,
    textbox: defaultParamDict,
    custom: defaultParamDict,
    formula: defaultParamDict,
    counter: defaultParamDict,
    projectlink: defaultParamDict,
    repeater: defaultParamDict,
    docnumber: defaultParamDict,
    personlink: defaultParamDict,
    link: defaultParamDict,
    htd: defaultParamDict,

    numericinput: ['MORE', 'MORE_OR_EQUALS', 'LESS', 'LESS_OR_EQUALS', 'EQUALS', 'NOT_EQUALS'],
    date: ['MORE', 'MORE_OR_EQUALS', 'LESS', 'LESS_OR_EQUALS', 'EQUALS', 'NOT_EQUALS'],
    entity: {
      users: ['CONTAINS', 'NOT_CONTAINS'],
      positions: ['CONTAINS', 'NOT_CONTAINS'],
      departments: ['CONTAINS', 'NOT_CONTAINS']
    },
    reglink: ['CONTAINS', 'NOT_CONTAINS'],
    listbox: ['EQUALS'],
    check: ['CONTAINS'],
    radio: ['EQUALS']
  },

  paramName: {
    STATE_SUCCESSFUL: 'Активная',
    STATE_NOT_FINISHED: 'В процессе',
    NO_ROUTE: 'Подготовка',
    STATE_UNSUCCESSFUL: 'Неуспешная',

    MORE: '>',
    MORE_OR_EQUALS: '>=',
    LESS: '<',
    LESS_OR_EQUALS: '<=',
    EQUALS: '=',
    NOT_EQUALS: '<>',

    CONTAINS: 'Содержит',
    NOT_CONTAINS: 'Не содержит',
    START: 'Начинается с',
    NOT_START: 'Не начинается с',
    NOT_END: 'Не заканчивается на',
    END: 'Заканчивается на',
    TEXT_NOT_EQUALS: 'Не совпадает',
    TEXT_EQUALS: 'Совпадает'
  },

  reset: function() {
    this.formData = null;
    this.paramSearch = {};
  }
};

const getUrlSearch = () => {
  let url = '&groupTerm=and';
  let count = 0;

  for(let key in RD.paramSearch) {
    if(RD.paramSearch[key].field == 'registryRecordStatus') {
      url += `&registryRecordStatus=${RD.paramSearch[key].value}`;
    } else {
      let gID = count > 0 ? count : '';
      switch (RD.paramSearch[key].type) {
        case 'listbox':
          RD.paramSearch[key].condition.indexOf('NOT') == -1 ? url += `&term${gID}=or` : url += `&term${gID}=and`;
          if(RD.paramSearch[key].hasOwnProperty('keys') && RD.paramSearch[key].keys) {
            RD.paramSearch[key].keys.forEach(fieldKey =>
              url += `&field${gID}=${RD.paramSearch[key].field}&condition${gID}=${RD.paramSearch[key].condition}&key${gID}=${fieldKey}`
            );
          } else {
            return null;
          }
          break;
        case 'radio':
        case 'check':
          RD.paramSearch[key].condition.indexOf('NOT') == -1 ? url += `&term${gID}=or` : url += `&term${gID}=and`;
          if(RD.paramSearch[key].hasOwnProperty('values') && RD.paramSearch[key].values) {
            RD.paramSearch[key].values.forEach(fieldValue =>
              url += `&field${gID}=${RD.paramSearch[key].field}&condition${gID}=${RD.paramSearch[key].condition}&value${gID}=${fieldValue}`
            );
          } else {
            return null;
          }
          break;
        case 'reglink':
          RD.paramSearch[key].condition.indexOf('NOT') == -1 ? url += `&term${gID}=or` : url += `&term${gID}=and`;
          if(RD.paramSearch[key].hasOwnProperty('selectedIds') && RD.paramSearch[key].selectedIds) {
            RD.paramSearch[key].selectedIds.forEach(docID =>
              url += `&field${gID}=${RD.paramSearch[key].field}&condition${gID}=${RD.paramSearch[key].condition}&key${gID}=${docID}`
            );
          } else {
            return null;
          }
          break;
        case 'users':
          RD.paramSearch[key].condition.indexOf('NOT') == -1 ? url += `&term${gID}=or` : url += `&term${gID}=and`;
          if(RD.paramSearch[key].hasOwnProperty('users') && RD.paramSearch[key].users) {
            RD.paramSearch[key].users.map(x => x.personID).forEach(userid =>
              url += `&field${gID}=${RD.paramSearch[key].field}&condition${gID}=${RD.paramSearch[key].condition}&key${gID}=${userid}`
            );
          } else {
            return null;
          }
          break;
        case 'positions':
          RD.paramSearch[key].condition.indexOf('NOT') == -1 ? url += `&term${gID}=or` : url += `&term${gID}=and`;
          if(RD.paramSearch[key].hasOwnProperty('positions') && RD.paramSearch[key].positions) {
            RD.paramSearch[key].positions.map(x => x.elementID).forEach(positionID =>
              url += `&field${gID}=${RD.paramSearch[key].field}&condition${gID}=${RD.paramSearch[key].condition}&key${gID}=${positionID}`
            );
          } else {
            return null;
          }
          break;
        case 'departments':
          RD.paramSearch[key].condition.indexOf('NOT') == -1 ? url += `&term${gID}=or` : url += `&term${gID}=and`;
          if(RD.paramSearch[key].hasOwnProperty('departments') && RD.paramSearch[key].departments) {
            RD.paramSearch[key].departments.map(x => x.departmentId).forEach(departmentId =>
              url += `&field${gID}=${RD.paramSearch[key].field}&condition${gID}=${RD.paramSearch[key].condition}&key${gID}=${departmentId}`
            );
          } else {
            return null;
          }
          break;
        default:
          url += `&field${gID}=${RD.paramSearch[key].field}&condition${gID}=${RD.paramSearch[key].condition}`;
          if(RD.paramSearch[key].hasOwnProperty('key')) {
            if(!RD.paramSearch[key].key || RD.paramSearch[key].key == "") {
              return null;
            } else {
              url += `&key${gID}=${RD.paramSearch[key].key}`;
            }
          } else {
            if(!RD.paramSearch[key].value || RD.paramSearch[key].value == "") {
              return null;
            } else {
              url += `&value${gID}=${RD.paramSearch[key].value}`;
            }
          }
      }
      count++;
    }
  }

  return url == '&groupTerm=and' ? null : url;
}

const checkFilterTemplate = () => {
  const urlSearch = getUrlSearch();
  if(urlSearch) {
    templateSaveButton.attr('disabled', false);

    if(templateNameInput.attr('datauuid')) {
      templateNameInput.attr('disabled', true).attr('placeholder', null);
    } else {
      templateNameInput.attr('disabled', false).attr('placeholder', i18n.tr('введите наименование фильтра...'));
    }
  }
}

const createSelectComponent = (items, multiple) => {
  const select = $('<select class="uk-select" style="min-width: 100px;">');
  if(multiple) select.attr('multiple', 'multiple');
  if(items) items.sort((a,b) => a.value - b.value)
  .forEach(item => select.append(`<option value="${item.value}" title="${item.label}">${item.label}</option>`));
  return select;
};
const createInputText = () => $(`<input type="text" class="uk-input" placeholder="${i18n.tr('Введите значение')}">`);
const createInputNumber = () => $(`<input type="number" class="uk-input" placeholder="${i18n.tr('Введите значение')}">`);
const createInputDate = () => $(`<input type="date" class="uk-input" style="max-width: 160px;"><input type="time" class="uk-input" style="padding-left: 10px;max-width: 100px;" value="00:00">`);

const createBlockParam = param => {
  const container = $('<div uk-grid style="margin: 0;"></div>');
  const items = RD.paramFields[param.type].map(x => ({label: i18n.tr(RD.paramName[x]), value: x}));
  const select = createSelectComponent(items);
  let input;
  switch (param.type) {
    case 'date': input = createInputDate(); break;
    case 'numericinput': input = createInputNumber(); break;
    default: input = createInputText(); break;
  }

  select.addClass('uk-width-auto').css({'padding-left': '10px', 'margin-right': '5px'});
  input.addClass('uk-width-expand').css('padding-left', '10px');
  container.append(select, input);

  return container;
};

const createRegistryParamBlock = (param, uuidRow, guid) => {
  const button = $('<a class="uk-form-icon uk-form-icon-flip" href="javascript:void(0);" uk-icon="icon: list"></a>');
  const input = $('<input class="uk-input" type="text" disabled>');
  const select = createSelectComponent(RD.paramFields.reglink.map(x => ({label: i18n.tr(RD.paramName[x]), value: x})));

  if(guid) {
    AS.FORMS.ApiUtils.simpleAsyncGet('rest/api/formPlayer/getDocMeaningContents?documentId=' + RD.paramSearch[uuidRow].selectedIds.join('&documentId='), content => {
      content = content.map(x => x.meaning).join('; ');
      input.val(content).attr('title', content);
    });
  } else {
    RD.paramSearch[uuidRow] = {field: param.id, type: param.type, condition: select.val()};
    checkFilterTemplate();
  }

  select.on('change', () => {
    RD.paramSearch[uuidRow].condition = select.val();
    checkFilterTemplate();
  });

  button.on('click', e => {
    Cons.showLoader();
    const selectedIds = RD.paramSearch[uuidRow].hasOwnProperty('selectedIds') ? RD.paramSearch[uuidRow].selectedIds : null;
    AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/info?registryID=${param.registryID}`, registryInfo => {
      if(!registryInfo.hasOwnProperty('registryCustomFilters')) registryInfo.registryCustomFilters = [];
      Cons.hideLoader();
      //(registry, multi, selectedIds, handler)
      AS.SERVICES.showRegistryLinkDialog(registryInfo, true, selectedIds, docIDs => {
        RD.paramSearch[uuidRow].selectedIds = docIDs;
        checkFilterTemplate();
        const contentURL = 'rest/api/formPlayer/getDocMeaningContents?documentId=' + docIDs.join('&documentId=');
        AS.FORMS.ApiUtils.simpleAsyncGet(contentURL, content => {
          content = content.map(x => x.meaning).join('; ');
          input.val(content).attr('title', content);
        });
      });
    });
  });

  return $('<div class="uk-grid-small" uk-grid>')
  .append($('<div class="uk-width-1-4@s">').append(select))
  .append($('<div class="uk-inline uk-width-3-4@s">').append(button, input));
}

const createUserParamBlock = (param, uuidRow, guid) => {
  const button = $('<a class="uk-form-icon uk-form-icon-flip" href="javascript:void(0);" uk-icon="icon: users"></a>');
  const input = $('<input class="uk-input" type="text" disabled>');
  const select = createSelectComponent(RD.paramFields.entity.users.map(x => ({label: i18n.tr(RD.paramName[x]), value: x})));

  if(guid) {
    const userNames = RD.paramSearch[uuidRow].users.map(x => x.personName).join('; ');
    input.val(userNames).attr('title', userNames);
  } else {
    RD.paramSearch[uuidRow] = {field: param.id, type: param.entity, condition: select.val()};
    checkFilterTemplate();
  }

  select.on('change', () => {
    RD.paramSearch[uuidRow].condition = select.val();
    checkFilterTemplate();
  });

  button.on('click', e => {
    const values = RD.paramSearch[uuidRow].hasOwnProperty('users') ? RD.paramSearch[uuidRow].users : null;
    //(values, multiSelectable, isGroupSelectable, showWithoutPosition, filterPositionID, filterDepartmentID, locale, handler)
    AS.SERVICES.showUserChooserDialog(values, true, false, false, null, null, AS.OPTIONS.locale, users => {
      const userNames = users.map(x => x.personName).join('; ');
      input.val(userNames).attr('title', userNames);
      RD.paramSearch[uuidRow].users = users;
      checkFilterTemplate();
    });
  });

  return $('<div class="uk-grid-small" uk-grid>')
  .append($('<div class="uk-width-1-4@s">').append(select))
  .append($('<div class="uk-inline uk-width-3-4@s">').append(button, input));
}

const createDepParamBlock = (param, uuidRow, guid) => {
  const button = $('<a class="uk-form-icon uk-form-icon-flip" href="javascript:void(0);" uk-icon="icon: more"></a>');
  const input = $('<input class="uk-input" type="text" disabled>');
  const select = createSelectComponent(RD.paramFields.entity.departments.map(x => ({label: i18n.tr(RD.paramName[x]), value: x})));

  if(guid) {
    const depNames = RD.paramSearch[uuidRow].departments.map(x => x.departmentName).join('; ');
    input.val(depNames).attr('title', depNames);
  } else {
    RD.paramSearch[uuidRow] = {field: param.id, type: param.entity, condition: select.val()};
    checkFilterTemplate();
  }

  select.on('change', () => {
    RD.paramSearch[uuidRow].condition = select.val();
    checkFilterTemplate();
  });

  button.on('click', e => {
    const values = RD.paramSearch[uuidRow].hasOwnProperty('departments') ? RD.paramSearch[uuidRow].departments : null;
    //(values, multiSelectable, filterUserID, filterPositionID, filterDepartmentID, filterChildDepartmentID, locale, handler)
    AS.SERVICES.showDepartmentChooserDialog(values, true, null, null, null, null, AS.OPTIONS.locale, departments => {
      const depNames = departments.map(x => x.departmentName).join('; ');
      input.val(depNames).attr('title', depNames);
      RD.paramSearch[uuidRow].departments = departments;
      checkFilterTemplate();
    });
  });

  return $('<div class="uk-grid-small" uk-grid>')
  .append($('<div class="uk-width-1-4@s">').append(select))
  .append($('<div class="uk-inline uk-width-3-4@s">').append(button, input));
}

const createPositionParamBlock = (param, uuidRow, guid) => {
  const button = $('<a class="uk-form-icon uk-form-icon-flip" href="javascript:void(0);" uk-icon="icon: more"></a>');
  const input = $('<input class="uk-input" type="text" disabled>');
  const select = createSelectComponent(RD.paramFields.entity.positions.map(x => ({label: i18n.tr(RD.paramName[x]), value: x})));

  if(guid) {
    const posNames = RD.paramSearch[uuidRow].positions.map(x => x.elementName).join('; ');
    input.val(posNames).attr('title', posNames);
  } else {
    RD.paramSearch[uuidRow] = {field: param.id, type: param.entity, condition: select.val()};
    checkFilterTemplate();
  }

  select.on('change', () => {
    RD.paramSearch[uuidRow].condition = select.val();
    checkFilterTemplate();
  });

  button.on('click', e => {
    const values = RD.paramSearch[uuidRow].hasOwnProperty('positions') ? RD.paramSearch[uuidRow].positions : null;
    //(values, multiSelect, filterUserId, filterDepartmentId, showVacant, locale, handler)
    AS.SERVICES.showPositionChooserDialog(values, true, null, null, null, AS.OPTIONS.locale, positions => {
      const posNames = positions.map(x => x.elementName).join('; ');
      input.val(posNames).attr('title', posNames);
      RD.paramSearch[uuidRow].positions = positions;
      checkFilterTemplate();
    });
  });

  return $('<div class="uk-grid-small" uk-grid>')
  .append($('<div class="uk-width-1-4@s">').append(select))
  .append($('<div class="uk-inline uk-width-3-4@s">').append(button, input));
}

const generateGuid = () => 'R'+Math.random().toString(36).substring(2, 15)+Math.random().toString(36).substring(2, 15);

const getFullName = person => {
  const {firstname, lastname, patronymic} = person;
  const fio = lastname.substr(0, 1).toUpperCase() + lastname.substr(1) + ' ' + firstname.substr(0, 1).toUpperCase() + '.';
  return patronymic ? fio + ' ' + patronymic.substr(0, 1).toUpperCase() + '.' : fio;
}

//Метод добавления параметра для поиска
const addParamRow = (param, guid) => {
  if(!param) return;

  if(param.type == 'file') {
    const t = {
      ru: `Неподдерживаемый тип параметра ${param.type}`,
      kk: `Қолдау көрсетілмейтін параметр түрі ${param.type}`,
      en: `Unsupported parameter type ${param.type}`
    }
    showMessage(t[AS.OPTIONS.locale], 'error');
    return;
  }

  let uuidRow = guid || generateGuid();
  let tr = $('<tr>');
  let tdFilter = $('<td>');
  let deleteRow = $('<a href="#" uk-icon="trash"></a>');

  deleteRow.on('click', e => {
    e.preventDefault();
    e.stopPropagation();
    tr.remove();
    delete RD.paramSearch[uuidRow];
    if (Object.keys(RD.paramSearch).length == 0) {
      templateSaveButton.attr('disabled', true);
      templateNameInput.attr('disabled', true).attr('placeholder', null);
    }
    checkFilterTemplate();
  });

  switch(param.type) {
    case 'registryRouteStatus':
      let statusRow = createSelectComponent(RD.paramFields[param.type].map(x => ({label: i18n.tr(RD.paramName[x]), value: x})));
      if(guid) {
        statusRow.val(RD.paramSearch[uuidRow].value);
      } else {
        RD.paramSearch[uuidRow] = {field: 'registryRecordStatus', value: statusRow.val()};
      }
      statusRow.on('change', () => {
        RD.paramSearch[uuidRow].value = statusRow.val();
        checkFilterTemplate();
      });
      tdFilter.append(statusRow);
      break;
    case 'listbox':
      let dictValues = createSelectComponent(param.elements, true);
      if(guid) {
        dictValues.val(RD.paramSearch[uuidRow].keys);
      } else {
        RD.paramSearch[uuidRow] = {
          field: param.id,
          type: param.type,
          condition: RD.paramFields[param.type][0],
          keys: dictValues.val()
        };
      }
      dictValues.on('change', () => {
        RD.paramSearch[uuidRow].keys = dictValues.val();
        checkFilterTemplate();
      });
      tdFilter.append(dictValues);
      break;
    case 'radio':
    case 'check':
      let dictValuesR = createSelectComponent(param.elements, true);
      if(guid) {
        dictValuesR.val(RD.paramSearch[uuidRow].values);
      } else {
        RD.paramSearch[uuidRow] = {
          field: param.id,
          type: param.type,
          condition: RD.paramFields[param.type][0],
          values: dictValuesR.val()
        };
      }
      dictValuesR.on('change', () => {
        RD.paramSearch[uuidRow].values = dictValuesR.val();
        checkFilterTemplate();
      });
      tdFilter.append(dictValuesR);
      break;
    case 'date':
      let blockParamND = createBlockParam(param);
      let selectND = blockParamND.find('select');
      let inputDate = blockParamND.find('input[type="date"]');
      let inputTime = blockParamND.find('input[type="time"]');

      if(guid) {
        selectND.val(RD.paramSearch[uuidRow].condition);
        inputDate.val(RD.paramSearch[uuidRow].key.split(' ')[0]);
        inputTime.val(RD.paramSearch[uuidRow].key.split(' ')[1].substr(0,5));
      } else {
        RD.paramSearch[uuidRow] = {
          field: param.id,
          type: param.type,
          condition: selectND.val(),
          key: `${inputDate.val()} ${inputTime.val()}:00`
        }
      }

      selectND.on('change', () => {
        RD.paramSearch[uuidRow].condition = selectND.val();
        checkFilterTemplate();
      });
      inputDate.on('change', () => {
        RD.paramSearch[uuidRow].key = `${inputDate.val()} ${inputTime.val()}:00`;
        checkFilterTemplate();
      });
      inputTime.on('change', () => {
        RD.paramSearch[uuidRow].key = `${inputDate.val()} ${inputTime.val()}:00`;
        checkFilterTemplate();
      });
      tdFilter.append(blockParamND);
      break;
    case 'reglink':
      tdFilter.append(createRegistryParamBlock(param, uuidRow, guid));
      break;
    case 'entity':
      switch (param.entity) {
        case 'users': tdFilter.append(createUserParamBlock(param, uuidRow, guid)); break;
        case 'departments': tdFilter.append(createDepParamBlock(param, uuidRow, guid)); break;
        case 'positions': tdFilter.append(createPositionParamBlock(param, uuidRow, guid)); break;
        default: tdFilter.append('<div>Параметр в разработке</div>');
      }
      break;
    default:
      let blockParamText = createBlockParam(param);
      let inputText = blockParamText.find('input');
      let selectText = blockParamText.find('select');

      if(guid) {
        inputText.val(RD.paramSearch[uuidRow].value);
        selectText.val(RD.paramSearch[uuidRow].condition);
      } else {
        RD.paramSearch[uuidRow] = {
          field: param.id,
          type: param.type,
          condition: selectText.val(),
          value: inputText.val()
        };
      }

      inputText.on('change', () => {
        RD.paramSearch[uuidRow].value = inputText.val();
        checkFilterTemplate();
      });
      selectText.on('change', () => {
        RD.paramSearch[uuidRow].condition = selectText.val();
        checkFilterTemplate();
      });
      tdFilter.append(blockParamText);
  }

  tr.append(`<td class="uk-form-label uk-text-primary uk-width-small uk-text-truncate" title="${i18n.tr(param.name)}">${i18n.tr(param.name)}</td>`)
  .append(tdFilter).append($(`<td title="${i18n.tr('Удалить условие')}" style="width: 30px; text-align: end;">`).append(deleteRow));

  tableParamBody.append(tr);
}

//получение элементов справочника для компонента listbox
const getDictionaryItems = async el => {
  if (el.hasOwnProperty('dataSource')) {
    const dict = await appAPI.getDictionary(el.dataSource.dict);
    el.elements = [];
    for (let key in dict.items) {
      const item = dict.items[key];
      el.elements.push({
        label: item[el.dataSource.key].value,
        value: item[el.dataSource.value].value
      });
    }
  }
}

//Действие при выборе параметра/условия
const selectParameter = (value, key) => {
  let param;
  if(value == 'registryRecordStatus') {
    param = {id: 'registryRecordStatus', type: 'registryRouteStatus', name: 'Статус записи реестра'};
  } else {
    param = RD.formData.component[value];
  }
  addParamRow(param, key);
}

//Инициализация справочника параметров/условий
const initParamList = () => {
  const paramList = filterWindow.find('#rd-form-param-list');
  RD.paramSearch = {};
  tableParamBody.empty();

  paramList.empty().off()
  .append(`<option disabled selected value="">${i18n.tr("Добавить условие")}</option>`)
  .on('change', () => {
    selectParameter(paramList.val());
    paramList.val("");
  });

  for(let key in RD.formData.label) {
    const x = RD.formData.label[key];
    paramList.append(`<option value="${key}" title="${i18n.tr(x.label)}">${i18n.tr(x.label)}</option>`);
  }
}

const getViewComponent = (component, componentsArrayList) => {
  const idxComponentLabel = component.id.lastIndexOf(syfixViewComponent);
  if(!~idxComponentLabel) return;

  const isComponentLabel = component.id.substr(idxComponentLabel) === syfixViewComponent;
  if(!isComponentLabel) return;

  const componentID = component.id.substr(0, idxComponentLabel);
  const filter = componentsArrayList.map(x => {
    const fComponent = x.filter(x => x.id === componentID).values().next().value || {};
    if (Object.keys(fComponent).length) {
      if(['listbox', 'check', 'radio'].indexOf(fComponent.type) != -1) getDictionaryItems(fComponent);
      if(fComponent.type == 'reglink') fComponent.registryID = fComponent.config.dateFormat;
      if(fComponent.type == 'entity') fComponent.entity = fComponent.config.entity;
      fComponent.name = component.label;
      return {
        label: {id: component.id, view: component},
        component: {id: fComponent.id, view: fComponent}
      };
    }
  });

  return filter;
}

const initFilter = (formCode, registryComponent, filterButton) => {
  let saveParam = sessionStorage.getItem(`filterParam_${formCode}_${AS.OPTIONS.currentUser.userid}`);
  RD.reset();

  RD.formCode = formCode;
  RD.registryComponent = registryComponent;

  if(saveParam) {
    saveParam = JSON.parse(saveParam);
    RD.formData = saveParam.formData;
    initParamList();
    RD.paramSearch = saveParam.paramSearch;
    for(key in RD.paramSearch) selectParameter(RD.paramSearch[key].field, key);
    let urlSearch = getUrlSearch();
    if(urlSearch) filterButton.addClass('_filter');
  } else {
    AS.FORMS.ApiUtils.simpleAsyncGet('rest/api/asforms/form_ext?formCode=' + formCode)
    .then(response => {
      RD.formData = response.properties.reduce((res, x) => {
        if(x.type === 'table' && !x.config.appendRows) {
          x.properties.forEach(component => {
            (getViewComponent(component, [response.properties, x.properties]) || []).forEach(x => {
              if (x instanceof Object) {
                res.label[x.component.id] = x.label.view;
                res.component[x.component.id] = x.component.view;
              }
            });
          });
        } else {
          (getViewComponent(x, [response.properties]) || []).forEach(x => {
            if(x instanceof Object) {
              res.label[x.component.id] = x.label.view;
              res.component[x.component.id] = x.component.view;
            }
          });
        }
        return res;
      }, {
        label: {},
        component: {}
      });


      setTimeout(() => {
        let urlSearch = getUrlSearch();
        if(urlSearch) {
          sessionStorage.setItem(`filterParam_${formCode}_${AS.OPTIONS.currentUser.userid}`, JSON.stringify({
            formData: RD.formData,
            paramSearch: RD.paramSearch
          }));
        }
      }, 500);

      initParamList();
    });
  }
}

const openFilterWindow = () => {
  translate();
  filterWindow.fadeIn();
  $('body').addClass('lock');
}

const closeFilterWindow = () => {
  filterWindow.fadeOut();
  $('body').removeClass('lock');
}

const checkSearchParam = (registryComponent, filterButton, formCode) => {
  let saveParam = sessionStorage.getItem(`filterParam_${formCode}_${AS.OPTIONS.currentUser.userid}`);

  if(saveParam) {
    saveParam = JSON.parse(saveParam);
    RD.formData = saveParam.formData;
    initParamList();
    RD.paramSearch = saveParam.paramSearch;
    for(key in RD.paramSearch) selectParameter(RD.paramSearch[key].field, key);
    let urlSearch = getUrlSearch();
    if(urlSearch) {
      filterButton.addClass('_filter');

      setTimeout(() => {
        $(`#${registryComponent}`).trigger({
          type: 'filterRegistryRows',
          eventParam: {
            filterSearchUrl: urlSearch
          }
        });
      }, 500);
    }
  }

}

const templateReset = () => {
  templateReportButton.attr('disabled', false);
  templateSaveButton.attr('disabled', true);
  templateDeleteButton.attr('disabled', true);
  templateForwardButton.attr('disabled', true);

  templateNameInput.val('')
  .attr('disabled', true)
  .attr('placeholder', null)
  .attr('datauuid', null);
}

compContainer.off()
.on('init_filters', e => {
  if(!e.hasOwnProperty('eventParam')) return;

  const {container, registryComponent, formCode, showTemplate = false} = e.eventParam;
  if(!formCode) return;

  $('.filter_button').remove();

  const filterButton = $(`<span class="filter_button">`);
  filterButton.append(getSvgIcon('manage_search'));

  if(showTemplate) {
    $('.rd-template-block').addClass('active');
    templateReset();
  } else {
    $('.rd-template-block').removeClass('active');
  }

  $(`#${container}`).append(filterButton);

  filterButton.attr('uk-tooltip', `title: ${i18n.tr("Расширенный поиск")}; duration: 300;`);
  filterButton.off().on('click', e => {
    openFilterWindow();
    initFilter(formCode, registryComponent, filterButton);
  });

  checkSearchParam(registryComponent, filterButton, formCode);

  filterClose.off().on('click', e => {
    closeFilterWindow();
  });

  btnFilter.off().on('click', e => {
    const urlSearch = getUrlSearch();
    e.preventDefault();
    e.target.blur();

    if(urlSearch) {
      sessionStorage.setItem(`filterParam_${formCode}_${AS.OPTIONS.currentUser.userid}`, JSON.stringify({
        formData: RD.formData,
        paramSearch: RD.paramSearch
      }));

      $(`#${registryComponent}`).trigger({
        type: 'filterRegistryRows',
        eventParam: {
          filterSearchUrl: urlSearch
        }
      });

      filterButton.addClass('_filter');

      closeFilterWindow();
    } else {
      showMessage(i18n.tr('Не все условия поиска заполнены'),'error');
      return;
    }
  });

  btnFilterDrop.off().on('click', e => {
    e.preventDefault();
    e.target.blur();

    RD.formData = null;
    RD.paramSearch = {};

    sessionStorage.removeItem(`filterParam_${formCode}_${AS.OPTIONS.currentUser.userid}`);

    if(showTemplate) templateReset();

    initFilter(formCode, registryComponent, filterButton);

    filterButton.removeClass('_filter');

    $(`#${registryComponent}`).trigger({
      type: 'filterRegistryRows',
      eventParam: {
        filterSearchUrl: null
      }
    });
  });

  filterWindow.off().on('click', e => {
    if ($(e.target).closest('.filter-content').length == 0) closeFilterWindow();
	});
});


//Метод сохранения шаблона отчета
const saveTemplateReport = () => {
  let url = getUrlSearch();
  if(!url) {
    showMessage(i18n.tr('Необходимо заполнить обязательное поле'), 'error');
    return;
  }

  let templateData = {
    filterCode: RD.filterCode,
    paramSearch: RD.paramSearch
  };

  if(!templateNameInput.attr('datauuid')) {
    if(templateNameInput.val() !== '') {
      Cons.showLoader();

      let name = templateNameInput.val();
      let url = `api/registry/data_ext?registryCode=customers_registry_lists&locale=${AS.OPTIONS.locale}`;
      url += `&field=customers_form_lists_authorID&value=${AS.OPTIONS.currentUser.userid}&condition=TEXT_EQUALS`;
      url += `&field1=customers_form_lists_form_code&value1=${RD.formCode}&condition1=TEXT_EQUALS`;
      url += `&field2=customers_form_lists_name&value2=${name}&condition2=TEXT_EQUALS`;
      url += `&loadData=false&pageNumber=0&countInPart=1`;

      //поиск шаблонов совпадающих по параметрам
      rest.synergyGet(url, res => {
        if(res.count > 0) {
          Cons.hideLoader();
          const alertMsg = {
            ru: `Фильтр с именем "${name}" уже существует, повторите попытку сохранения с другим именем`,
            kk: `"${name}" атауы бар сүзгі бұрыннан бар, басқа атпен қайта сақтап көріңіз`,
            en: `A filter with the name "${name}" already exists, try saving again with a different name`
          }
          UIkit.modal.alert(alertMsg[AS.OPTIONS.locale]);
        } else {
          let asfData = [];
          asfData.push({"id": "customers_form_lists_name", "type": "textbox", "value": name});
          asfData.push({"id": "customers_form_lists_authorID", "type": "textbox", "value": AS.OPTIONS.currentUser.userid});
          asfData.push({"id": "customers_form_lists_form_code", "type": "textbox", "value": RD.formCode});
          asfData.push({"id": "customers_form_lists_params", "type": "textarea", "value": JSON.stringify(templateData)});

          let body = {"registryCode": "customers_registry_lists", "data": asfData};

          AS.FORMS.ApiUtils.simpleAsyncPost('rest/api/registry/create_doc_rcc', null, null, JSON.stringify(body), "application/json; charset=utf-8")
          .then(res => {
            Cons.hideLoader();
            if(res.errorCode != '0') {
              showMessage(i18n.tr('Ошибка сохранения фильтра'), 'error');
            } else {
              showMessage(i18n.tr('Фильтр успешно сохранен'), 'success');
              templateSaveButton.attr('disabled', true);
              templateDeleteButton.attr('disabled', false);
              templateForwardButton.attr('disabled', false);
              templateNameInput
              .attr('disabled', true)
              .attr('placeholder', null)
              .attr('datauuid', res.dataID);
            }
          });
        }
      });
    } else {
      templateNameInput.addClass('uk-form-danger');
      UIkit.modal.alert(i18n.tr('Необходимо заполнить обязательное поле наименование фильтра'));
    }

  } else {
    Cons.showLoader();

    let asfData = [];
    asfData.push({"id": "customers_form_lists_params", "type": "textarea", "value": JSON.stringify(templateData)});
    let body = {"uuid": templateNameInput.attr('datauuid'), "data": asfData};

    AS.FORMS.ApiUtils.simpleAsyncPost('rest/api/asforms/data/merge', null, null, JSON.stringify(body), "application/json; charset=utf-8")
    .then(res => {
      Cons.hideLoader();
      if(res.errorCode != '0') {
        showMessage(i18n.tr('Ошибка сохранения фильтра'), 'error');
      } else {
        templateSaveButton.attr('disabled', true);
        showMessage(i18n.tr('Фильтр успешно сохранен'), 'success');
      }
    });
  }

}

//Метод удаления шаблона
const deleteTemplateReport = () => {
  try {
    Cons.showLoader();
    let uuid = templateNameInput.attr('datauuid');
    rest.synergyGet(`api/registry/delete_doc?dataUUID=${uuid}`, res => {
      Cons.hideLoader();
      $('#rd-registry-list').trigger('change');
      btnFilterDrop.trigger('click');
      showMessage(i18n.tr('Фильтр удален'), 'success');
    });
  } catch (e) {
    Cons.hideLoader();
    showMessage(i18n.tr('Ошибка удаления фильтра'), 'error');
  }
}

// метод передачи шаблона
const forwardTemplateReport = () => {
  try {
    const uuid = templateNameInput.attr('datauuid');

    //(values, multiSelectable, isGroupSelectable, showWithoutPosition, filterPositionID, filterDepartmentID, locale, handler)
    AS.SERVICES.showUserChooserDialog([], false, false, false, null, null, AS.OPTIONS.locale, users => {
      Cons.showLoader();

      const {personID} = users[0];
      const personName = getFullName(AS.OPTIONS.currentUser);
      const {filterCode, paramSearch, formCode} = RD;
      const templateData = {filterCode, paramSearch};
      const filterName = `${templateNameInput.val()} (${personName})`;
      const asfData = [];

      asfData.push({"id": "customers_form_lists_name", "type": "textbox", "value": filterName});
      asfData.push({"id": "customers_form_lists_authorID", "type": "textbox", "value": personID});
      asfData.push({"id": "customers_form_lists_form_code", "type": "textbox", "value": formCode});
      asfData.push({"id": "customers_form_lists_params", "type": "textarea", "value": JSON.stringify(templateData)});

      const body = {"registryCode": "customers_registry_lists", "data": asfData};

      AS.FORMS.ApiUtils.simpleAsyncPost('rest/api/registry/create_doc_rcc', null, null, JSON.stringify(body), "application/json; charset=utf-8")
      .then(res => {
        Cons.hideLoader();
        if(res.errorCode != '0') {
          showMessage(i18n.tr('Ошибка передачи фильтра'), 'error');
        } else {
          showMessage(i18n.tr('Фильтр передан'), 'success');
        }
      });
    });

  } catch (e) {
    Cons.hideLoader();
    showMessage(i18n.tr('Ошибка передачи фильтра'), 'error');
  }
}

//Инициализация обработки шаблона
const reportTemplateInit = template => {
  let p = JSON.parse(template.params);
  RD.filterCode = p.filterCode;
  RD.paramSearch = p.paramSearch;

  templateDeleteButton.attr('disabled', false);
  templateForwardButton.attr('disabled', false);
  templateSaveButton.attr('disabled', true);
  templateNameInput
  .val(template.value)
  .removeClass('uk-form-danger')
  .attr('disabled', true)
  .attr('datauuid', template.key);

  tableParamBody.empty();
  for(key in p.paramSearch) selectParameter(p.paramSearch[key].field, key);

  let urlSearch = getUrlSearch();
  if(urlSearch) {
    sessionStorage.setItem(`filterParam_${RD.formCode}_${AS.OPTIONS.currentUser.userid}`, JSON.stringify({
      formData: RD.formData,
      paramSearch: RD.paramSearch
    }));

    $('.filter_button').addClass('_filter');

    setTimeout(() => {
      $(`#${RD.registryComponent}`).trigger({
        type: 'filterRegistryRows',
        eventParam: {
          filterSearchUrl: urlSearch
        }
      });
    }, 500);
  }
}

//Модальное окно выбора шаблонов
const openModalTemplates = items => {
  const container = $('<div>');
  const table = $('<table class="template-report-table uk-table uk-table-divider uk-table-small uk-table-responsive">');
  const tBody = $('<tbody>');
  const dialog = $('<div class="uk-flex-top" uk-modal>');
  const md = $('<div class="uk-modal-dialog uk-margin-auto-vertical">');
  const modalBody = $('<div class="uk-modal-body template-modal">');
  const footer = $('<div class="uk-modal-footer uk-text-right">');
  const actionButton = $('<button class="uk-button uk-button-primary uk-button-small uk-margin-left" type="button" disabled>');
  const searchBlock = $('<div class="uk-inline" style="width: 100%;">');
  const searchInput = $(`<input class="uk-input" type="text" placeholder="${i18n.tr("поиск...")}">`);

  searchBlock.append('<span class="uk-form-icon uk-form-icon-flip" uk-icon="icon: search"></span>').append(searchInput);
  container.append(searchBlock).append($('<div class="uk-overflow-auto" style="max-height: 250px; margin-top: 20px;">').append(table));
  table.append(tBody);
  modalBody.append(container);

  footer.append(`<button class="uk-button uk-button-default uk-button-small uk-margin-left uk-modal-close" type="button">${i18n.tr("Закрыть")}</button>`, actionButton);

  dialog.append(md);
  md.append('<button class="uk-modal-close-default" type="button" uk-close></button>');
  md.append(`<div class="uk-modal-header"><h3>${i18n.tr("Сохраненные фильтры")}</h3></div>`);
  md.append(modalBody).append(footer);

  items.forEach(item => {
    const tr = $('<tr>').attr('key', item.key);
    tr.append(`<td>${item.value}</td>`);

    tBody.append(tr);

    tr.on('click', e => {
      tBody.find('tr').removeClass('selected');
      tr.addClass('selected');
      actionButton.attr('disabled', false);
    });
  });

  searchInput.keyup(function() {
    tBody.find('tr').removeClass('selected');
    actionButton.attr('disabled', true);
    $.each(tBody.find("tr"), function() {
      if($(this).text().toLowerCase().indexOf(searchInput.val().toLowerCase()) === -1) $(this).hide();
      else $(this).show();
    });
  });

  actionButton.html(i18n.tr("Выбрать")).on('click', e => {
    try {
      reportTemplateInit(items.find(x => x.key === tBody.find('.selected').attr('key')));
      UIkit.modal(dialog).hide();
    } catch (e) {
      console.log(e);
    }
  });

  Cons.hideLoader();
  UIkit.modal(dialog).show();
  dialog.on('hidden', () => dialog.remove());
}

//Кнопка выбора шаблона отчета
templateReportButton.on('click', e => {
  e.preventDefault();
  e.target.blur();
  Cons.showLoader();

  let url = `api/registry/data_ext?registryCode=customers_registry_lists&locale=${AS.OPTIONS.locale}`;
  url += `&field=customers_form_lists_authorID&value=${AS.OPTIONS.currentUser.userid}&condition=TEXT_EQUALS`;
  url += `&field1=customers_form_lists_form_code&value1=${RD.formCode}&condition1=TEXT_EQUALS`;
  url += `&fields=customers_form_lists_name&fields=customers_form_lists_params`;

  //поиск сохраненных шаблонов
  rest.synergyGet(url, res => {
    //парсинг найденных шаблонов
    let items = res.result
    .filter(x => x.fieldValue.hasOwnProperty('customers_form_lists_name'))
    .map(x => ({key: x.dataUUID, value: x.fieldValue['customers_form_lists_name'], params: x.fieldValue['customers_form_lists_params']}))
    .sort((a, b) => {
      a = a.value.toLowerCase();
      b = b.value.toLowerCase();
      return a > b ? 1 : a < b ? -1 : 0;
    });
    openModalTemplates(items);
  });

});

//Кнопка сохранения шаблона отчета
templateSaveButton.on('click', e => {
  e.preventDefault();
  e.target.blur();
  saveTemplateReport();
});

//Кнопка удаления шаблона отчета
templateDeleteButton.on('click', e => {
  e.preventDefault();
  e.target.blur();
  const name = templateNameInput.val();
  const msgConfirm = {
    ru: `Вы действительно хотите удалить фильтр "${name}"?`,
    kk: `"${name}" сүзгісін жойғыңыз келетініне сенімдісіз бе?`,
    en: `Are you sure you want to remove the "${name}" filter?`
  }
  UIkit.modal.confirm(msgConfirm[AS.OPTIONS.locale]).then(() => {
    deleteTemplateReport();
  }, () => null);
});

//Кнопка поделиться шаблоном
templateForwardButton.on('click', e => {
  e.preventDefault();
  e.target.blur();
  forwardTemplateReport();
});

templateNameInput.keyup(() => {
  templateNameInput.removeClass('uk-form-danger');
});
