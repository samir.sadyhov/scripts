const getSystemSettings = async () => {
  const {systemSettings} = Cons.getAppStore();
  return new Promise(resolve => {
    if(systemSettings) {
      resolve(systemSettings);
    } else {
      appAPI.getSystemSettings().then(systemSettings => {
        Cons.setAppStore({systemSettings});
        resolve(systemSettings);
      });
    }
  });
}

const searchInRegistry = async param => {
  return new Promise(resolve => {
    rest.synergyGet(`api/registry/data_ext?${param}`, resolve, err => {
      console.log(`ERROR [ searchInRegistry ]: ${JSON.stringify(err)}`);
      resolve(null);
    });
  });
}

const createDoc = async registryCode => {
  return new Promise(resolve => {
    rest.synergyGet(`api/registry/create_doc?registryCode=${registryCode}`, resolve, err => {
      console.log(`ERROR [ createDoc ]: ${JSON.stringify(err)}`);
      resolve(null);
    });
  });
}

const activateDoc = async dataUUID => {
  const url = `rest/api/registry/activate_doc?dataUUID=${dataUUID}&locale=${AS.OPTIONS.locale}`;
  return AS.FORMS.ApiUtils.simpleAsyncGet(url);
}

const createAccordion = (data, customClass) => {
  const container = $('<ul uk-accordion="multiple: true" style="width: 100%;">');
  if(customClass) container.addClass(customClass);

  for(let i = 0; i < data.length; i++) {
    const {title, content, open, selectClass} = data[i];
    const li = $('<li>');
    const accContent = $('<div>', {class: 'uk-accordion-content'}).append(content);
    if(open || selectClass) li.addClass('uk-open');
    li.append(`<a class="uk-accordion-title uk-position-relative uk-flex uk-flex-between uk-text-small ${selectClass || ''}" href="#">${title}</a>`, accContent);
    container.append(li);
  }

  return container;
}

this.CustomersCalendar = class {
  #selectDate = new Date();
  #registryCode = 'customers_tasks';
  #calendarType = 'day'; //default: day | month, week
  #tasks = null;
  searchString = '';
  selectResponsible = null;

  get calendarType() {
    return this.#calendarType;
  }

  get registryCode() {
    return this.#registryCode;
  }

  get selectDate() {
    return this.#selectDate;
  }

  getUrlParam() {
    const {formatDate, DATE_INPUT_FORMAT, DATE_FORMAT_FULL} = dateUtils;
    const fields = [
      'task_start',
      'task_finish',
      'task_author',
      'task_Resposible',
      'task_comment',
      'task_status',
      'task_action'
    ];

    let start;
    let stop;

    switch (this.#calendarType) {
      case 'day':
        start = `${formatDate(this.#selectDate, DATE_INPUT_FORMAT)} 00:00:00`;
        stop = `${formatDate(this.#selectDate, DATE_INPUT_FORMAT)} 23:59:59`;
        break;
      case 'week':
        const weekPeriod = dateUtils.getWeekPeriod(this.#selectDate)
        start = formatDate(weekPeriod.monday, DATE_FORMAT_FULL);
        stop = formatDate(weekPeriod.sunday, DATE_FORMAT_FULL);
        break;
      case 'month':
        const monthPeriod = dateUtils.getMonthPeriod(this.#selectDate)
        start = formatDate(monthPeriod.start, DATE_FORMAT_FULL);
        stop = formatDate(monthPeriod.stop, DATE_FORMAT_FULL);
        break;
    }

    let url = `registryCode=${this.#registryCode}`;
    url += `&field=task_start&condition=MORE_OR_EQUALS&key=${start}`;
    url += `&field1=task_finish&condition1=LESS_OR_EQUALS&key1=${stop}`;

    if(this.selectResponsible) {
      url += `&field2=task_author&condition2=TEXT_EQUALS&key2=${AS.OPTIONS.currentUser.userid}`;
      this.selectResponsible.forEach(user => {
        url += `&field3=task_Resposible&condition3=CONTAINS&key3=${user.personID}`
      });
      url += '&term3=or';
    } else {
      url += `&field2=task_Resposible&condition2=TEXT_EQUALS&key2=${AS.OPTIONS.currentUser.userid}`;
    }

    url += `&registryRecordStatus=STATE_SUCCESSFUL`;

    if(this.searchString) url += `&searchString=${this.searchString}`;
    fields.forEach(field => url += `&fields=${field}`);

    return url;
  }

  async #searchTasks() {
    const param = this.getUrlParam();
    const searchResult = await searchInRegistry(param);
    return this.#parseTasks(searchResult);
  }

  #parseTasks(searchResult){
    if(!searchResult || searchResult.recordsCount == 0) return null;
    const result = {};

    for(let i = 0; i < searchResult.result.length; i++) {
      const {dataUUID, documentID, fieldKey, fieldValue} = searchResult.result[i];

      const taskStartDate = fieldKey.task_start.split(' ')[0];
      const taskStartTime = Number(fieldKey.task_start.split(' ')[1].slice(0,2));

      if(!result.hasOwnProperty(taskStartDate)) result[taskStartDate] = {};
      if(!result[taskStartDate].hasOwnProperty(taskStartTime)) result[taskStartDate][taskStartTime] = [];

      const tmp = {dataUUID, documentID};
      for(let key in fieldValue) {
        tmp[key] = {value: fieldValue[key]};
        if(fieldKey.hasOwnProperty(key)) tmp[key].key = fieldKey[key];
      }

      result[taskStartDate][taskStartTime].push(tmp);

      result[taskStartDate][taskStartTime].sort((a, b) => {
        const task_startA = new Date(a.task_start?.key);
        const task_startB = new Date(b.task_start?.key);
        return task_startA.getTime() - task_startB.getTime();
      });
    }

    return result;
  }

  #renderDatepicker() {
    this.datepickerOptions.onSelect = (dateText, inst) => {
      this.#selectDate = new Date(dateText);
      this.update();
    }

    this.panelCalendar.datepicker(this.datepickerOptions);

    $(this.buttonSetNow).off().on('click', e => {
      e.preventDefault();
      e.target.blur();

      this.panelCalendar.datepicker("setDate", dateUtils.formatDate(new Date(), dateUtils.DATE_INPUT_FORMAT));
      this.#selectDate = new Date();
      this.update();
    });
  }

  #addListenerPanelActions(){

    this.buttonDay.off().on('click', e => {
      e.preventDefault();
      e.target.blur();
      if(this.buttonDay.hasClass('select')) return;

      this.#calendarType = 'day';

      this.buttonDay.addClass('select');
      this.buttonWeek.removeClass('select');
      this.buttonMonth.removeClass('select');

      this.update();
    });

    this.buttonWeek.off().on('click', e => {
      e.preventDefault();
      e.target.blur();
      if(this.buttonWeek.hasClass('select')) return;

      this.#calendarType = 'week';

      this.buttonDay.removeClass('select');
      this.buttonWeek.addClass('select');
      this.buttonMonth.removeClass('select');

      this.update();
    });

    this.buttonMonth.off().on('click', e => {
      e.preventDefault();
      e.target.blur();
      if(this.buttonMonth.hasClass('select')) return;

      this.#calendarType = 'month';

      this.buttonDay.removeClass('select');
      this.buttonWeek.removeClass('select');
      this.buttonMonth.addClass('select');

      this.update();
    });

  }

  #addListenerPrevNextButton(){

    this.buttonPrev.off().on('click', e => {
      e.preventDefault();
      e.target.blur();

      switch (this.#calendarType) {
        case 'day': this.#selectDate.setDate(this.#selectDate.getDate() - 1); break;
        case 'week':
          let currentWeekNumber = dateUtils.getWeekNumber(this.#selectDate);
          let currentYear = this.#selectDate.getFullYear();
          if(currentWeekNumber == 1) {
            currentWeekNumber = 52;
            currentYear--;
          } else {
            currentWeekNumber--;
          }
          let weekPeriod = dateUtils.getWeekPeriodNumber(currentWeekNumber, currentYear);
          this.#selectDate = new Date(weekPeriod.monday);
          break;
        case 'month': this.#selectDate = new Date(this.#selectDate.getFullYear(), this.#selectDate.getMonth() - 1, 1); break;
      }

      this.panelCalendar.datepicker("setDate", dateUtils.formatDate(this.#selectDate, dateUtils.DATE_INPUT_FORMAT));

      this.update();
    });

    this.buttonNext.off().on('click', e => {
      e.preventDefault();
      e.target.blur();

      switch (this.#calendarType) {
        case 'day': this.#selectDate.setDate(this.#selectDate.getDate() + 1); break;
        case 'week':
          let currentWeekNumber = dateUtils.getWeekNumber(this.#selectDate);
          let currentYear = this.#selectDate.getFullYear();
          currentWeekNumber++;
          if(currentWeekNumber > 52) {
            currentWeekNumber = 1;
            if(this.#selectDate.getMonth() == 11) currentYear++;
          }
          let weekPeriod = dateUtils.getWeekPeriodNumber(currentWeekNumber, currentYear);
          this.#selectDate = new Date(weekPeriod.monday);
          break;
        case 'month': this.#selectDate = new Date(this.#selectDate.getFullYear(), this.#selectDate.getMonth() + 1, 1); break;
      }

      this.panelCalendar.datepicker("setDate", dateUtils.formatDate(this.#selectDate, dateUtils.DATE_INPUT_FORMAT));

      this.update();
    });
  }

  changeCalendarTypeLabel() {
    let text = '';

    switch (this.#calendarType) {
      case 'day':
        text = dateUtils.formatDate(this.#selectDate, '${month} ${dd}, ${yyyy}');
        break;
      case 'week':
        const weekPeriod = dateUtils.getPeriod(this.#selectDate, 'week');
        const monday = dateUtils.formatDate(weekPeriod.monday, '${dd}.${mm}');
        const sunday = dateUtils.formatDate(weekPeriod.sunday, '${dd}.${mm}');
        text = `${monday}-${sunday}, ${this.#selectDate.getFullYear()}`;
        break;
      case 'month':
        text = dateUtils.formatDate(this.#selectDate, dateUtils.CUSTOM_FORMAT_MONTH_YEAR);
        break;
    }

    this.calendarTypeLabel.text(text);
  }

  #createUserChooser() {
    const {positions} = AS.OPTIONS.currentUser;
    let filterDepartmentID = null;

    if(positions.length) {
      const posHead = positions.find(x => x.type == 1);
      if(posHead) {
        filterDepartmentID = posHead.departmentID;
      } else {
        filterDepartmentID = positions[0].departmentID;
      }
    }

    const container = $('<div>');
    const button = $('<a class="uk-form-icon uk-form-icon-flip" href="#" uk-icon="icon: users"></a>');
    const input = $('<input class="uk-input" type="text" disabled style="background-color: #fff; border-radius: 6px; height: 35px;">');
    const clearButton = $(`<a href="#" class="uk-hidden" uk-icon="icon: close"></a>`);

    container.css({
      "min-width": "270px",
      "display": "flex",
      "align-items": "center",
      "gap": "10px"
    });

    container.append($('<div class="uk-inline uk-width-expand">').append(button, input), clearButton);

    clearButton.on('click', e => {
      e.preventDefault();
      e.target.blur();
      e.stopPropagation();
      this.selectResponsible = null;
      input.val(null);
      clearButton.addClass('uk-hidden');
      this.update();
    });

    button.on('click', e => {
      e.preventDefault();
      e.target.blur();
      e.stopPropagation();

      //(values, multiSelectable, isGroupSelectable, showWithoutPosition, filterPositionID, filterDepartmentID, locale, handler)
      AS.SERVICES.showUserChooserDialog(this.selectResponsible, true, false, false, null, filterDepartmentID, AS.OPTIONS.locale, users => {
        let userNames = users.map(x => x.personName).join('; ');
        input.val(userNames).attr('title', userNames);
        this.selectResponsible = users;
        clearButton.removeClass('uk-hidden');
        this.update();
      });
    });

    return container;
  }

  #renderPanelActions() {
    const buttonGroup = $('<div>', {class: "uk-button-group"});
    this.buttonDay = $(`<button class="uk-button uk-button-default select" uk-tooltip="title: ${i18n.tr('День')}">${getSvgIcon('calendar_day')}</button>`);
    this.buttonWeek = $(`<button class="uk-button uk-button-default" uk-tooltip="title: ${i18n.tr('Неделя')}">${getSvgIcon('calendar_week')}</button>`);
    this.buttonMonth = $(`<button class="uk-button uk-button-default" uk-tooltip="title: ${i18n.tr('Месяц')}">${getSvgIcon('calendar_month')}</button>`);

    buttonGroup.append(this.buttonDay, this.buttonWeek, this.buttonMonth);

    this.calendarTypeLabel = $('<span>');

    const switchPanel = $('<div>', {class: "calendar_switch_panel"});
    this.buttonPrev = $(`<a href="" id="button_prev" class="uk-icon-button" uk-icon="icon: triangle-left; ratio: 2" uk-tooltip="title: ${i18n.tr('Назад')}"></a>`);
    this.buttonNext = $(`<a href="" id="button_next" class="uk-icon-button" uk-icon="icon: triangle-right; ratio: 2" uk-tooltip="title: ${i18n.tr('Вперед')}"></a>`);

    switchPanel.append(this.buttonPrev, this.calendarTypeLabel, this.buttonNext);

    const userChooser = this.#createUserChooser();

    this.panelNav.empty();
    this.panelNav.addClass('calendar_nav');

    this.panelNav.append(userChooser, buttonGroup, switchPanel);

    this.#addListenerPanelActions();
    this.#addListenerPrevNextButton();
  }

  search(searchString = '') {
    this.searchString = searchString;
    this.update();
  }

  async createTask() {
    Cons.showLoader();

    try {
      const newDoc = await createDoc(this.#registryCode);
      if (!newDoc || newDoc.errorCode != 0) throw new Error('Ошибка создания задачи');

      const player = UTILS.getSynergyPlayer(newDoc.dataUUID, true);

      const dialog = await UTILS.getModalDialog(i18n.tr('Создание задачи'), player.view.container, i18n.tr('Создать'), async () => {
        try {
          Cons.showLoader();
          if (!player.model.isValid()) throw new Error('Заполните обязательные поля');

          player.saveFormData(async asfDataId => {
            await activateDoc(asfDataId);
            Cons.hideLoader();
            showMessage(i18n.tr('Задача создана'), 'success');
            UIkit.modal(dialog).hide();
            player.destroy();
            this.update();
            CustomersNotification.update();
          }, (status, responseText) => {
            console.log('ERROR [saveFormData]', {status, responseText});
            throw new Error(responseText);
          });

        } catch (e) {
          Cons.hideLoader();
          showMessage(i18n.tr(e.message), 'error');
        }
      });

      Cons.hideLoader();

      UIkit.modal(dialog).show();
      dialog.on('hidden', () => dialog.remove());

    } catch (err) {
      Cons.hideLoader();
      console.log('ERROR createTask CustomersCalendar', err);
      showMessage(i18n.tr(err.message), 'error');
    }
  }

  #viewTask(task) {
    const {dataUUID, task_author, task_start} = task;

    const player = UTILS.getSynergyPlayer(dataUUID);
    const dialogTitle = dateUtils.getWeekDayName(new Date(task_start.key).getDay());

    const dialog = $('<div class="uk-flex-top" uk-modal>');
    const md = $('<div class="uk-modal-dialog uk-margin-auto-vertical">');
    const modalBody = $('<div class="uk-modal-body" uk-overflow-auto>');
    const footer = $('<div class="uk-modal-footer uk-text-right">');
    const actionPanel = $('<div>', {class: "customer_dialog_action_panel uk-hidden"});
    const finishButton = $(`<button class="uk-button uk-button-primary" type="button">${i18n.tr("Завершить")}</button>`);

    modalBody.append(player.view.container);
    footer.append(finishButton, `<button class="uk-button uk-button-default uk-modal-close" type="button">${i18n.tr("Закрыть")}</button>`);
    md.append(`<button class="uk-modal-close-default modal-close-custom" type="button" uk-close></button>`,
    `<div class="uk-modal-header"><h3>${dialogTitle}</h3></div>`, actionPanel, modalBody, footer);
    dialog.append(md);

    footer.css({
      "gap": "10px",
      "display": "flex",
      "justify-content": "end"
    });

    finishButton.on('click', e => {
      e.preventDefault();
      e.target.blur();
      this.#finishTask(dataUUID);
    });

    if(task_author?.key == AS.OPTIONS.currentUser.userid) {
      actionPanel.removeClass('uk-hidden');

      const editButton = $(`<div class="dialog_action_button">${getSvgIcon('edit')}</div>`);
      const saveButton = $(`<div class="dialog_action_button" style="visibility: hidden;">${getSvgIcon('save')}</div>`);
      actionPanel.append(saveButton, editButton);

      let editable = false;

      editButton.on('click', e => {
        e.preventDefault();
        e.target.blur();

        if(!editable) {
          saveButton.css('visibility', 'visible');
          editButton.html(getSvgIcon('view'));
        } else {
          saveButton.css('visibility', 'hidden');
          editButton.html(getSvgIcon('edit'));
        }

        editable = !editable;
        player.view.setEditable(editable);
      });

      saveButton.on('click', e => {
        e.preventDefault();
        e.target.blur();

        try {
          Cons.showLoader();
          if (!player.model.isValid()) throw new Error('Заполните обязательные поля');

          player.saveFormData(async asfDataId => {
            await appAPI.modifyDoc(asfDataId);
            Cons.hideLoader();
            showMessage(i18n.tr('Задача изменена'), 'success');
            UIkit.modal(dialog).hide();
            player.destroy();
            this.update();
            CustomersNotification.update();
          }, (status, responseText) => {
            console.log('ERROR [saveFormData]', {status, responseText});
            throw new Error(responseText);
          });

        } catch (e) {
          Cons.hideLoader();
          showMessage(i18n.tr(e.message), 'error');
        }
      });
    }

    UIkit.modal(dialog).show();
    dialog.on('hidden', () => dialog.remove());
  }

  async #finishTask(dataUUID) {
    UIkit.modal.confirm(i18n.tr('Вы действительно хотите завершить задачу?'), {labels: {ok: i18n.tr('Да'), cancel: i18n.tr('Отмена')}}).then(async () => {
      Cons.showLoader();
      try {
        const mergeResult = await appAPI.mergeFormData({
          uuid: dataUUID,
          data: [{
            id: 'task_status',
            type: "listbox",
            value: 'Завершено',
            key: '1'
          }]
        });

        if(!mergeResult) throw new Error(i18n.tr("Произошла ошибка при завершении задачи"));

        await appAPI.modifyDoc(dataUUID);
        this.update();
        Cons.hideLoader();
        CustomersNotification.update();
      } catch (err) {
        Cons.hideLoader();
        showMessage(err.message, 'error');
        console.log('ERROR finishTask CustomersCalendar', err);
      }
    }, () => null);
  }

  #deleteTask(dataUUID) {
    UIkit.modal.confirm(i18n.tr('Вы действительно хотите удалить задачу?'), {labels: {ok: i18n.tr('Да'), cancel: i18n.tr('Отмена')}}).then(() => {
      Cons.showLoader();
      try {
        rest.synergyGet(`api/registry/delete_doc?dataUUID=${dataUUID}`, res => {
          if(res.errorCode != '0') throw new Error(res.errorMessage);
          showMessage(i18n.tr("Задача удалена"), "success");
          this.update();
          CustomersNotification.update();
          Cons.hideLoader();
        });
      } catch (err) {
        Cons.hideLoader();
        showMessage(i18n.tr("Произошла ошибка при удалении задачи"), "error");
        console.log(err);
      }
    }, () => null);
  }

  getPeriodLabelTask(task_start, task_finish, task_status) {
    if(task_status && task_status?.key == '1') {
      return i18n.tr(task_status?.value || 'Завершено');
    } else {
      if(task_start && task_start.hasOwnProperty('key')) {
        return `${task_start.key.split(' ')[1].slice(0, 5)} - ${task_finish.key.split(' ')[1].slice(0, 5)}`;
      } else {
        return '';
      }
    }
  }

  #filterTasks(date){
    if(!this.#tasks || !this.#tasks.hasOwnProperty(date)) return [];

    let tasks = [];

    for(const hour in this.#tasks[date]) {
      if(this.#tasks[date].hasOwnProperty(hour)) {
        tasks = [...tasks, ...this.#tasks[date][hour]];
      }
    }

    return tasks;
  }

  getAccordionItems(date, hour){
    if(!this.#tasks || !this.#tasks.hasOwnProperty(date)) return null;

    const container = $('<div>');
    let tasks = [];

    if(hour || hour == 0) {
      if(this.#tasks[date].hasOwnProperty(hour)) tasks = this.#tasks[date][hour];
    } else {
      tasks = this.#filterTasks(date);
    }

    for(let i = 0; i < tasks.length; i++) {
      const {
        dataUUID,
        task_author,
        task_Resposible,
        task_comment,
        task_start,
        task_finish,
        task_status,
        task_action
      } = tasks[i];

      const content = $('<div>', {class: "uk-position-relative uk-overflow-hidden uk-padding-small uk-flex uk-flex-middle uk-flex-between calendar_item divider", datauuid: dataUUID});
      const checkbox = $(`<input type="checkbox" class="uk-checkbox">`);
      const item = $('<div>', {class: "uk-margin-small-left uk-text-truncate uk-width-expand"});
      const actions = $('<div>', {class: "task_actions"});
      const viewTask = $('<a uk-icon="commenting" class="uk-margin-small-left uk-icon">');

      if(task_status?.key == 1) {
        content.addClass('success');
      } else if (task_status?.key == 0 && new Date().getTime() > new Date(task_finish.key).getTime()) {
        content.addClass('bad');
      }

      actions.append(viewTask);

      viewTask.on('click', e => {
        e.preventDefault();
        e.target.blur();
        this.#viewTask(tasks[i]);
      });

      if(task_author?.key == AS.OPTIONS.currentUser.userid) {
        const deleteTask = $('<a uk-icon="trash" class="uk-margin-small-left uk-icon">');

        deleteTask.on('click', e => {
          e.preventDefault();
          e.target.blur();
          this.#deleteTask(dataUUID);
        });

        actions.append(deleteTask);
      }

      checkbox.on('change', e => {
        if(e.target.checked) this.#finishTask(dataUUID);
      });

      if(task_status && task_status?.key == '1') {
        checkbox
        .prop('checked', true)
        .prop('disabled', true);
      }

      item.append(
        `<span class="uk-margin-small-right uk-text-meta">${this.getPeriodLabelTask(task_start, task_finish, task_status)}</span>`,
        `<span class="uk-margin-small-right uk-text-small">${i18n.tr(task_action?.value || '')}: ${task_comment?.value || ''}</span>`,
        `<span class="uk-margin-small-right uk-text-meta">${i18n.tr('Автор')}: ${task_author?.value || ''}</span>`,
        `<span class="uk-margin-small-right uk-text-meta">${i18n.tr('Ответственный')}: ${task_Resposible?.value || ''}</span>`
      )

      content.append(checkbox, item, actions);

      container.append(content);
    }

    return container;
  }

  #getTaskCard(item){
    const {
      dataUUID,
      task_author,
      task_Resposible,
      task_comment,
      task_finish,
      task_start,
      task_status,
      task_action
    } = item;

    const article = $('<article>', {class: "uk-comment uk-comment-primary uk-visible-toggle uk-padding-small"});
    const header = $('<header>', {class: "uk-comment-header uk-position-relative uk-margin-remove"});
    const buttonsBlock = $('<div>', {class: "uk-position-top-right uk-hidden-hover"});
    const commentBody = $('<div>', {class: "uk-comment-body uk-margin"});
    const viewTask = $(`<a href="#" uk-icon="icon: commenting"></a>`);
    const created_label = this.getPeriodLabelTask(task_start, task_finish, task_status);

    if(task_status?.key == 1) {
      article.addClass('success');
    } else if (task_status?.key == 0 && new Date().getTime() > new Date(task_finish.key).getTime()) {
      article.addClass('bad');
    }

    buttonsBlock.append(viewTask);

    viewTask.on('click', e => {
      e.preventDefault();
      e.target.blur();
      this.#viewTask(item);
    });

    if(task_author?.key == AS.OPTIONS.currentUser.userid) {
      const deleteTask = $(`<a href="#" uk-icon="icon: trash"></a>`);

      deleteTask.on('click', e => {
        e.preventDefault();
        e.target.blur();
        this.#deleteTask(dataUUID);
      });

      buttonsBlock.append(deleteTask);
    }

    header
    .append(
      `<h4 class="uk-comment-title uk-margin-remove">${task_author?.value || ""}</h4>`,
      `<span class="uk-comment-meta uk-margin-remove">${i18n.tr(task_action?.value || "")} - ${created_label}</span>`,
      `<span class="uk-comment-meta uk-margin-remove">${i18n.tr('Ответственный')}: ${task_Resposible?.value || ""}</span>`,
      buttonsBlock
    );

    commentBody.html((task_comment?.value || "").replaceAll('\n', '<br>'));
    article.append(header).append(commentBody);

    return article;
  }

  #renderTasksCard(tasks = []){
    this.panelTasks.empty();
    tasks.forEach(item => {
      const card = this.#getTaskCard(item);
      this.panelTasks.append(card);
    });
  }

  #renderDay(){
    const selectDate = dateUtils.formatDate(this.#selectDate, dateUtils.DATE_INPUT_FORMAT);
    const hourStart = Number(this.day_start_time.slice(0, 2));
    const hourStop = Number(this.day_finish_time.slice(0, 2));
    const accordionItems = [];
    const filterTasks = this.#filterTasks(selectDate);

    for(let i = 0; i <= 24; i++) {
      if(i == 0 || (i >= hourStart && i <= hourStop) || i == 24) {
        const content = this.getAccordionItems(selectDate, i);
        accordionItems.push({
          title: `${('0' + i).slice(-2)}:00`, content, open: true
        });
      } else {
        if(this.#tasks && this.#tasks.hasOwnProperty(selectDate) && this.#tasks[selectDate].hasOwnProperty(i)) {
          const content = this.getAccordionItems(selectDate, i);
          accordionItems.push({
            title: `${('0' + i).slice(-2)}:00`, content, open: true
          });
        } else {
          continue;
        }
      }
    }

    this.#renderTasksCard(filterTasks);

    const accordion = createAccordion(accordionItems, 'customers_calendar calendar_day');
    this.panelContent.empty().append(accordion);
  }

  #renderWeek(){
    const {monday, sunday} = dateUtils.getPeriod(this.#selectDate, 'week');
    const accordionItems = [];
    let weekTasks = [];

    for (var i = 0; i < 7; i++) {
      const renderDate = new Date(monday);
      renderDate.setDate(renderDate.getDate() + i);

      let selectClass = null;

      const curDate = new Date();
      if(curDate.getDate() == renderDate.getDate() &&
        curDate.getMonth() == renderDate.getMonth() &&
        curDate.getFullYear() == renderDate.getFullYear()) {
        selectClass = 'active';
      }

      const selectDate = dateUtils.formatDate(renderDate, dateUtils.DATE_INPUT_FORMAT);
      const dateLabel = dateUtils.formatDate(renderDate, '${month} ${dd}, ${yyyy}');
      const content = this.getAccordionItems(selectDate);
      const dayName = dateUtils.getWeekDayName(i+1);
      const filterTasks = this.#filterTasks(selectDate);
      const title = `<div><span class="count_tasks">${filterTasks.length}</span> ${dayName}</div> ${dateLabel}`;

      weekTasks = [...weekTasks, ...filterTasks];

      accordionItems.push({title, content, selectClass});
    }

    this.#renderTasksCard(weekTasks);

    const accordion = createAccordion(accordionItems, 'customers_calendar calendar_week');
    this.panelContent.empty().append(accordion);
  }

  #getDay(date){
    let day = date.getDay();
    return day == 0 ? 7 : day - 1;
  }

  #renderMonth(){
    const mon = this.#selectDate.getMonth();
    const d = new Date(this.#selectDate.getFullYear(), this.#selectDate.getMonth());

    const container = $('<div>', {class: 'calendar_widget_month'});
    const weekdays = $('<ul>', {class: "calendar_widget_month_weekdays"});
    const days = $('<ul>', {class: "calendar_widget_month_days"});

    container.append(weekdays, days);

    for(let i = 0; i < 7; i++) {
      const li = $('<li>');
      const dayName = dateUtils.getWeekDayShortName(i+1);
      li.text(dayName);
      weekdays.append(li);
    }

    const renderDayData = (day, month, year, dayClass) => {
      const renderDate = dateUtils.formatDate(new Date(year, month, day), dateUtils.DATE_INPUT_FORMAT);
      const filterTasks = this.#filterTasks(renderDate);
      const li = $('<li>');

      if(dayClass) li.addClass(dayClass);

      if(filterTasks.length) {
        li.append(`<span class="count_tasks">${filterTasks.length}</span> ${day}`);
      } else {
        li.text(day);
      }

      const curDate = new Date();
      if(curDate.getDate() == day && curDate.getMonth() == month && curDate.getFullYear() == year) {
        li.addClass('active');
      }

      li.on('click', e => {
        days.find('li').removeClass('select');
        li.addClass('select');
        this.#renderTasksCard(filterTasks);
      });

      days.append(li);
    }

    const {monday: prevMonth} = dateUtils.getPeriod(new Date(d.getFullYear(), d.getMonth(), 1), 'week');
    while (prevMonth.getMonth() != mon) {
      renderDayData(prevMonth.getDate(), prevMonth.getMonth(), prevMonth.getFullYear(), 'uk-text-muted');
      prevMonth.setDate(prevMonth.getDate() + 1);
    }

    while (d.getMonth() == mon) {
      renderDayData(d.getDate(), d.getMonth(), d.getFullYear());
      d.setDate(d.getDate() + 1);
    }

    const nextMonth = new Date(d);
    for (let i = 0; i < this.#getDay(nextMonth) ; i++) {
      nextMonth.setDate(nextMonth.getDate() + 1);
      renderDayData(nextMonth.getDate() - 1, nextMonth.getMonth(), nextMonth.getFullYear(), 'uk-text-muted');
    }

    this.panelContent.empty().append(container);
  }

  async update() {
    Cons.showLoader();

    this.panelTasks.empty();

    this.changeCalendarTypeLabel();
    this.#tasks = await this.#searchTasks();

    switch (this.#calendarType) {
      case 'day': this.#renderDay(); break;
      case 'week': this.#renderWeek(); break;
      case 'month': this.#renderMonth(); break;
    }

    Cons.hideLoader();
  }

  async init(selectors) {
    Cons.showLoader();
    try {

      const {
        containerContent,
        containerTasks,
        containerNav,
        containerCalendar,
        buttonSetNow
      } = selectors;

      const systemSettings = await getSystemSettings();
      const {day_finish_time, day_start_time} = systemSettings;

      this.day_finish_time = day_finish_time;
      this.day_start_time = day_start_time;

      this.datepickerOptions = getDatepickerOptions();

      this.panelContent = $(containerContent);
      this.panelTasks = $(containerTasks);
      this.panelNav = $(containerNav);
      this.panelCalendar = $(containerCalendar);
      this.buttonSetNow = $(buttonSetNow);

      this.panelTasks.addClass('calendar_task_items');

      this.#renderDatepicker();
      this.#renderPanelActions();
      this.update();

      Cons.hideLoader();
    } catch (err) {
      Cons.hideLoader();
      console.log('ERROR init CustomersCalendar', err);
      showMessage(err.message, 'error');
    }
  }

}
