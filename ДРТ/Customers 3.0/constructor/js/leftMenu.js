const findNameRu = name => {
  const {translations} = Cons.getAppStore();
  if(!translations || translations == 'not found') return name;

  let nameRu = null;

  for(const key in translations) {
    const items = translations[key];
    for(const locale in items) if(items[locale] == name) nameRu = items.ru;
  }

  return nameRu || name;
}

const addMenuButtonIcon = () => {
  const buttons = $('.mb-icon');
  if(!buttons.length) return;

  for(let i = 0; i < buttons.length; i++) {
    const button = $(buttons[i]);
    const classList = button.attr('class').split(/\s+/);
    const icon = classList.find(x => x.substr(0,5) == 'icon-').substr(5);
    const name = findNameRu(button.text());
    button.empty().append(
      getSvgIcon(icon),
      `<span class="button-name">${i18n.tr(name)}</span>`
    );
    button.attr('uk-tooltip', `title: ${i18n.tr(name)}; pos: right; duration: 300;`);
  }
}

const hideShowMenuItem = async (successHandler) => {
  const {registryList} = Cons.getAppStore();
  if(!registryList) return;

  const buttons = $('.registry_menu_item');
  if(!buttons.length) return;

  Cons.showLoader();

  for(let i = 0; i < buttons.length; i++) {
    const button = $(buttons[i]);
    const buttonID = button.attr('id');
    const registryCode = buttonID.substr(7);
    const regInfo = registryList.find(x => x.registryCode == registryCode);
    if(!regInfo) continue;

    if(!regInfo.hasOwnProperty('info')) {
       regInfo.info = await appAPI.getRegistryInfo(registryCode);
    }

    if(!regInfo.hasOwnProperty('filters')) {
      regInfo.filters = await appAPI.getRegistryFilters(registryCode);
      Cons.setAppStore({registryList});
    }

    if(!regInfo.hasOwnProperty('allRights')) regInfo.allRights = [...regInfo.rights];

    if(regInfo.filters && regInfo.filters.length) {
      for(let j = 0; j < regInfo.filters.length; j++) {
        const {rights = []} = regInfo.filters[j];
        regInfo.allRights = [...regInfo.allRights, ...rights].uniq();
      }
    }

    if(regInfo.allRights.includes("rr_list") || regInfo.allRights.includes("rr_read")) {
      $(`#${buttonID}`).removeClass('registry_menu_item_hidden');
    }

  }

  Cons.hideLoader();

  if(typeof successHandler == 'function') successHandler();
}

this.leftPanelInit = (successHandler) => {
  const leftPanel = $('#leftPanel');
  const {appInIframe, userGroups = []} = Cons.getAppStore();

  if(!leftPanel.length) return;

  setTimeout(() => {
    if(!appInIframe) $('#panelBottom2').removeClass('uk-hidden');
    if(userGroups.find(x => x.groupCode == 'customers_group_dicts_editors')) {
      $('#panelBottom1').removeClass('uk-hidden');
    }
  }, 100);

  // свернуть/развернуть левую панель
  let leftPanelVisible = true;
  $('#buttonHideShowLeftPanel').off().on('click', function(e) {
    e.preventDefault();
    e.target.blur();

    if(leftPanelVisible) {
      leftPanel.css({
        'width': '50px'
      });
      $('#panelContent, #panelAppActions').css({
        'width': 'calc(100% - 50px)',
        'left': '50px'
      });
      $(this).addClass('pressed');
      $(this).find('span').attr('uk-icon', 'icon: chevron-double-right');
      $('.menu-content').addClass('collapse');
      $('#profile').hide();
    } else {
      leftPanel.css({
        'width': '300px'
      });
      $('#panelContent, #panelAppActions').css({
        'width': 'calc(100% - 300px)',
        'left': '300px'
      });
      $(this).removeClass('pressed');
      $(this).find('span').attr('uk-icon', 'icon: chevron-double-left');
      $('.menu-content').removeClass('collapse');
      $('#profile').fadeIn(1000);
    }
    leftPanelVisible = !leftPanelVisible;
  });

  setTimeout(() => {
    addMenuButtonIcon();
    hideShowMenuItem(successHandler);
  }, 100);

}

this.leftPanelTranslate = () => {
  addMenuButtonIcon();
}

Array.prototype.uniq = function() {
  return this.filter(function(v, i, a){ return i == a.indexOf(v) });
}
