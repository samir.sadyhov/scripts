this.getDatepickerOptions = (year) => {
  const currentYear = new Date().getYear() + 1900;
  const minYear = year ? math.min(year, (currentYear - 90)) : (currentYear - 90);
  const maxYear = year ? math.max(year, (currentYear + 20)) : (currentYear + 20);

  const dayNames = [
    i18n.tr("Воскресенье"),
    i18n.tr("Понедельник"),
    i18n.tr("Вторник"),
    i18n.tr("Среда"),
    i18n.tr("Четверг"),
    i18n.tr("Пятница"),
    i18n.tr("Суббота")
  ];

  const dayNamesShort = [
    i18n.tr("Вс"),
    i18n.tr("Пн"),
    i18n.tr("Вт"),
    i18n.tr("Ср"),
    i18n.tr("Чт"),
    i18n.tr("Пт"),
    i18n.tr("Сб")
  ];

  const monthNames = [
    i18n.tr("Январь"),
    i18n.tr("Февраль"),
    i18n.tr("Март"),
    i18n.tr("Апрель"),
    i18n.tr("Май"),
    i18n.tr("Июнь"),
    i18n.tr("Июль"),
    i18n.tr("Август"),
    i18n.tr("Сентябрь"),
    i18n.tr("Октябрь"),
    i18n.tr("Ноябрь"),
    i18n.tr("Декабрь")
  ];

  return {
    showOtherMonths: true,
    showWeek: true,
    changeMonth: true,
    changeYear: true,
    dateFormat: AS.FORMS.DateUtils.DATE_PICKER_FORMAT,
    firstDay: 1,

    monthNames,
    monthNamesShort: monthNames,
    dayNames,
    dayNamesShort,
    dayNamesMin: dayNamesShort,

    weekHeader: i18n.tr("Нед"),
    closeText: i18n.tr("Закрыть"),
    prevText: "&#x3C;" + i18n.tr("Пред"),
    nextText: i18n.tr("След") + "&#x3E;",
    currentText: i18n.tr("Сегодня"),

    yearSuffix: "",
    yearRange: `${minYear}:${maxYear}`,

    isRTL: false
  }
}
