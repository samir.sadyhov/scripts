this.dateUtils = {

  isLeap: function(year) {
    return !((year % 4) || (!(year % 100) && (year % 400)));
  },

  getWeekNumber: function(date) {
    const now = date ? new Date(date) : new Date();
    now.setHours(0, 0, 0, 0);
    now.setDate(now.getDate() + 3 - (now.getDay() + 6) % 7);
    const week1 = new Date(now.getFullYear(), 0, 4);
    return 1 + Math.round(((now.getTime() - week1.getTime()) / 86400000 - 3 + (week1.getDay() + 6) % 7) / 7);
  },

  getWeekPeriodNumber: function(week = 1, year = new Date().getFullYear()) {
    const monday = new Date(year, 0, 7 * week);
    monday.setDate(monday.getDate() - (monday.getDay() || 7) + 1);

    const checkWeek = this.getWeekNumber(monday);
    if(checkWeek != week) monday.setDate(monday.getDate() - 7);

    const sunday = new Date(monday);
    sunday.setDate(sunday.getDate() - sunday.getDay() + 7);
    sunday.setHours(23,59,59,0);

    return {week, monday, sunday};
  },

  getWeekPeriod: function(date) {
    const now = date ? new Date(date) : new Date();
    const week = this.getWeekNumber(now);
    let year = now.getFullYear();
    if(now.getMonth() == 0 && week > 51) year--;
    return this.getWeekPeriodNumber(week, year);
  },

  getMonthPeriod: function(date) {
    const start = new Date(date.getFullYear(), date.getMonth(), 1);
    const stop = new Date(date.getFullYear(), date.getMonth() + 1, 0, 23, 59, 59);

    return {start, stop};
  },

  getDayPeriod: function(date) {
    const start = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 0, 0, 0);
    const stop = new Date(date.getFullYear(), date.getMonth(), date.getDate(), 23, 59, 59);
    return {start, stop};
  },

  getPeriod: function(date, type) {
    switch (type) {
      case 'month': return this.getMonthPeriod(date); break;
      case 'week': return this.getWeekPeriod(date); break;
      case 'day': return this.getDayPeriod(date); break;
      default: return this.getMonthPeriod(date);
    }
  },

  ...AS.FORMS.DateUtils,

  CUSTOM_FORMAT_DATE: '${dd}.${mm}.${yyyy}',
  CUSTOM_FORMAT_FULLDATE: '${dd}.${mm}.${yyyy} ${HH}:${MM}',
  CUSTOM_FORMAT_MONTH_YEAR: '${month}, ${yyyy}'
}
