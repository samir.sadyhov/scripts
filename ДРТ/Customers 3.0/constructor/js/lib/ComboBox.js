this.ComboBox = class {
  constructor(
    dropdownContainer,
    values = [],
    selectHandler,
    inputClass = "asf-dropdown-input",
    buttonClass = "asf-dropdown-button"
  ) {
    this.dropdownContainer = dropdownContainer;
    this.values = values;
    this.inputClass = inputClass;
    this.buttonClass = buttonClass;

    this.selectHandler = selectHandler;

    this.init();
  }

  setSelectedValue(newSelectedValue) {
    this.initialSelect = newSelectedValue;
    this.selectedValue = null;
    this.dropdownBox.val("");
    this.values.forEach(value => {
      value.selected = value.value === newSelectedValue;
      if(value.value === newSelectedValue) {
        this.dropdownBox.val(value.title);
        this.dropdownBox.attr('title', value.title);
        this.selectedValue = newSelectedValue;
      }
    });
  }

  showDropDown(filterValue){
    if (AS.SERVICES.isShownDropDown(this.container)) {
      AS.SERVICES.closeDropDown(this.container);
      return;
    }

    const values = this.values.filter(x => x.title.indexOf(filterValue) > -1);

    AS.SERVICES.showDropDown(values, this.container, null, selectedValue => {
      this.setSelectedValue(selectedValue);
      if(typeof this.selectHandler == 'function') this.selectHandler(this.selectedValue);
    });
  }

  addListener(){
    this.dropdownBox.on("input", event => {
      this.showDropDown(this.dropdownBox.val());
    });

    this.dropdownBox.on("blur", event => {
      this.setSelectedValue(this.initialSelect);
    });

    this.button.on('click', event => {
      event.stopPropagation();
      event.stopImmediatePropagation();
      event.preventDefault();
      this.showDropDown("", event);
    });

    this.dropdownBox.on('click', event => {
      event.stopPropagation();
      event.stopImmediatePropagation();
      event.preventDefault();
      this.showDropDown("", event);
    });
  }

  render(){
    this.dropdownContainer.empty();

    this.container = $('<div>', {class: "asf-InlineBlock", style: "vertical-align:top"});
    this.dropdownBox =  $('<input>', {type : 'text', class : this.inputClass});
    this.button = $("<button>", {class: this.buttonClass});

    this.dropdownBox.prop("readonly", true);

    this.container.append(this.dropdownBox);
    this.container.append(this.button);
    this.dropdownContainer.append(this.container);

    this.setSelectedValue(this.initialSelect);
  }

  init(){
    this.selectedValue = null;
    this.initialSelect = null;

    if(this.values.length) {
      this.initialSelect = this.values[0].value;
    }

    this.enabled = true;

    this.render();
    this.addListener();
  }
}
