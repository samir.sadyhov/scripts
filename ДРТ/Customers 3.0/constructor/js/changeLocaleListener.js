this.updateTranslations = async () => {
  const {translations} = Cons.getAppStore();
  i18n.locale = AS.OPTIONS.locale;
  return new Promise(resolve => {
    rest.synergyGet(`system/messages?localeID=${AS.OPTIONS.locale}`,
      res => {
        i18n.messages = res;
        if(translations && translations != 'not found') {
          for(const key in translations) i18n.messages[key] = translations[key][AS.OPTIONS.locale];
        }
        resolve(true);
      },
      err => {
        i18n.messages = null;
        resolve(true);
      }
    );
  });
}

const createSelectComponent = items => {
  const container = $('<div>', {class: 'locale_selector_container', style: "width: 100%;"});
  const fc = $(`<div style="width: 100%;" uk-form-custom="target: > * > span:last-child">`);
  const button = $('<button>', {class: "uk-button uk-button-default menu-button fonts"});
  const select = $('<select>', {style: "padding: 0 10px; width: calc(100% - 40px); left: 20px;"});

  if(items) items.sort((a,b) => a.value - b.value)
  .forEach(item => select.append(`<option value="${item.value}" title="${item.label}">${item.label}</option>`));

  button.append(
    getSvgIcon('translate'),
    `<span class="button-name"></span>`
  );

  fc.append(select, button);
  container.append(fc);

  return {container, select};
}

//доп функции для переводов
const localized = async () => {
  const {code} = Cons.getCurrentPage();
  await updateTranslations();

  switch (code) {
    case 'accountsPage':
    case 'activitesPage':
    case 'contactsPage':
    case 'contractsPage':
    case 'accountsPage':
    case 'salesPlanPage':
    case 'transactionsPage':
      $('#customRegistryList').trigger({type: 'translate'});
      break;
    case 'leadPages':
    case 'dealsPage':
      $('#KanbanBoardComponent').trigger({type: 'translate'});
      break;
    case 'dictionariesPage':
      await translateDictsMenu();
      $('#customRegistryList').trigger({type: 'translate'});
      break;
  }

  if($('#buttonReports').length) fire({type: 'reports_translate'}, 'buttonReports');

  if($('.filter_button').length) $('.filter_button').attr('uk-tooltip', `title: ${i18n.tr("Расширенный поиск")}; duration: 300;`);

  setTimeout(() => {
    leftPanelTranslate();
  }, 1000);
}

if(!Cons.getAppStore().change_locale_listener) {
  addListener('change_locale', 'root-panel', e => {
    const {locale} = e;
    AS.OPTIONS.locale = locale;
    localStorage.locale = locale;
    localized();
  });

  Cons.setAppStore({change_locale_listener: true});
}

if(localStorage.locale && localStorage.locale != AS.OPTIONS.locale) {
  fire({type: 'change_locale', locale: localStorage.locale});
}

const getLocales = async () => {
  const {systemLocales} = Cons.getAppStore();
  if(systemLocales) return systemLocales;

  const locales = await appAPI.getSystemLocales();
  Cons.setAppStore({systemLocales: locales});
  return locales;
}

const initLocaleSelector = async () => {
  if(!$('#leftPanel-bottom').length) return;

  const systemLocales = await getLocales();
  const {code} = Cons.getCurrentPage();
  const items = systemLocales.map(x => ({label: x.languageName, value: x.localeID}));
  const currentLocale = items.find(x => x.value == AS.OPTIONS.locale);

  const {container, select} = createSelectComponent(items);
  select.val(AS.OPTIONS.locale);

  select.on('change', e => {
    if(select.val() != AS.OPTIONS.locale) {
      fire({type: 'change_locale', locale: select.val()});
    }
  });

  if(code == 'dictionariesPage') {
    $('#leftPanel-center').css('height', 'calc(100% - 160px)');
    $('#leftPanel-bottom').css('height', '100px');
  } else {
    $('#leftPanel-center').css('height', 'calc(100% - 210px)');
    $('#leftPanel-bottom').css('height', '150px');
  }

  $('.locale_selector_container').remove();
  $('#leftPanel-bottom').prepend(container);
}


initLocaleSelector();
