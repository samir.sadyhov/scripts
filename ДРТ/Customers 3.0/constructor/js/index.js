//Иконка приложения
if(!$('link[rel="icon"]').length) $('head').append('<link rel="icon" href="../constructorFiles/customers/logo.svg">');

const getFullName = person => {
  const {firstname, lastname, patronymic} = person;
  const fio = lastname.substr(0, 1).toUpperCase() + lastname.substr(1) + ' ' + firstname.substr(0, 1).toUpperCase() + '.';
  return patronymic ? fio + ' ' + patronymic.substr(0, 1).toUpperCase() + '.' : fio;
}

const setProfileData = () => {
  if(!$('#profile').length) return;

  const photoURL = `../Synergy/load?userid=${AS.OPTIONS.currentUser.userid}`;
  $('#profileName span').text(getFullName(AS.OPTIONS.currentUser));
  $('#profileImage').attr('src', photoURL);
}

function getCookie(name) {
  let matches = document.cookie.match(new RegExp(
    "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
  ));
  return matches ? decodeURIComponent(matches[1]) : undefined;
}

function setCookie(name, value, options = {}) {

  options = {
    path: '/',
    ...options
  };

  if (options.expires instanceof Date) {
    options.expires = options.expires.toUTCString();
  }

  let updatedCookie = encodeURIComponent(name) + "=" + encodeURIComponent(value);

  for (let optionKey in options) {
    updatedCookie += "; " + optionKey;
    let optionValue = options[optionKey];
    if (optionValue !== true) {
      updatedCookie += "=" + optionValue;
    }
  }

  document.cookie = updatedCookie;
}

function deleteCookie(name) {
  setCookie(name, "", {
    'max-age': -1
  });
}

if($('#buttonLogout').length) {
  $('#buttonLogout').off().on('click', e => {
    deleteCookie(`constructor_creds_for_customers`);
    deleteCookie(`constructor_image_api_key`);
    CustomersNotification.stop();
    localStorage.removeItem('previousPageCode');
    if($('.footer-panel').length) $('.footer-panel').remove();
    AS.apiAuth.setCredentials(null, null);
    AS.OPTIONS.currentUser = {};
    AS.OPTIONS.login = null;
    AS.OPTIONS.password = null;
    Cons.creds.login = null;
    Cons.creds.password = null;
    Cons.setAppStore({registryList: null});
    Cons.setAppStore({userGroups: null});
    Cons.logout();
  });
}

setProfileData();

jQuery(document).mouseup(function(event){
	setTimeout(function(){
		AS.FORMS.popupPanel.hide();
	}, 0);
});

jQuery(document).click(function(event){
	AS.FORMS.popupPanel.hide();
});
