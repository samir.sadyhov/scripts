const getUrlParameter = sParam => {
  let sPageURL = window.location.search.substring(1), sURLVariables = sPageURL.split('&'), sParameterName;
  for (let i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split('=');
    if (sParameterName[0] === sParam)
    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
  }
}

const addLog = async data => {
  return new Promise(async resolve => {
    AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/logger/log", resolve, 'json', JSON.stringify(data), "application/json; charset=UTF-8", resolve);
  });
}

const getRegistryList = async () => {
  return new Promise(async resolve => {
    const {registryList} = Cons.getAppStore();
    if(registryList) {
      resolve(registryList);
    } else {
      let list = await appAPI.getRegistryList();
      list = UTILS.parseRegistryList(list);
      Cons.setAppStore({registryList: list});
      resolve(list);
    }
  });
}

pageHandler('start_page', () => {
  const app = getUrlParameter('app');
  const locale = getUrlParameter('locale');

  if(locale) {
    localStorage.locale = locale;
    fire({type: 'change_locale', locale});
  }

  if(app) {
    Cons.setAppStore({appInIframe: true});
    const decodeStr = decodeURIComponent(escape(window.atob(app)));
    const separator = decodeStr.slice(0,8);
    const authParams = decodeStr.split(separator);
    Cons.login({
      login: authParams[1].split('').reverse().join(''),
      password: authParams[2].split('').reverse().join('')
    });
  } else {
    fire({type: 'goto_page', pageCode: 'authPage'}, 'root-panel');
  }

  if (!Cons.getAppStore().start_page_auth_listener) {
    addListener('auth_success', 'root-panel', async authed => {
      const {login, password} = authed.creds;

      Cons.creds.login = login;
      Cons.creds.password = password;
      AS.apiAuth.setCredentials(login, password);
      AS.OPTIONS.login = login;
      AS.OPTIONS.password = password;
      AS.OPTIONS.currentUser = authed.data.person;

      Cons.setAppStore({userGroups: authed.data.groups});

      await getRegistryList();

      if(app) {
        await addLog({
          "providerId": "AI_SEC",
          "eventId": "2005",
          "userId": AS.OPTIONS.currentUser.userid,
          "objects": [AS.OPTIONS.login, "Customers"]
        });
      }

      const {previousPageCode = 'calendarPage'} = localStorage;
      fire({type: 'goto_page', pageCode: previousPageCode}, 'root-panel');

    });

    Cons.setAppStore({start_page_auth_listener: true});
  }
});
