pageHandler('calendarPage', () => {

  Cons.setAppStore({previousPageCode: 'calendarPage'});
  localStorage.setItem('previousPageCode', 'calendarPage');

  leftPanelInit();

  const calendar = new CustomersCalendar();

  calendar.init({
    containerContent: '#panelCalendarBody', // панель для отрисовки календаря с тасками в разрезе день, неделя, месяц
    containerTasks: '#calendarDataItems', // панель для трисовки задач
    containerNav: '#panelCalendarNav', // панель навигации, назад вперед, тип календаря
    containerCalendar: '#calendarWrapper', // панель для отрисовки календаря (datepicker)
    buttonSetNow: '#buttonCalendarSetNow' // кнопка сброса календаря, установка текущей даты (datepicker)
  });

  $('#searchInput').off().on('keyup', e => {
    if (e.keyCode === 13) {
      e.preventDefault();
      calendar.search($('#searchInput').val());
    }
  });

  $('#buttonRefresh').off().on('click', e => {
    e.preventDefault();
    e.target.blur();
    calendar.update();
    CustomersNotification.update();
  }).on('calendar_update', e => {
    calendar.update();
  });

  $('#buttonCreate').off().on('click', e => {
    e.preventDefault();
    e.target.blur();
    calendar.createTask();
  });

  //инициализация напоминалок
  CustomersNotification.init();

});
