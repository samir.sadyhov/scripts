pageHandler('createFormDataPage', () => {
  const name = UTILS.getPageParamValue('name');
  const registryCode = UTILS.getPageParamValue('registryCode');
  const formCode = UTILS.getPageParamValue('formCode');

  if(name) UTILS.changeLabel('documentNameLabel', name, name);

  const player = AS.FORMS.createPlayer();
  player.view.setEditable(true);
  player.showFormByCode(formCode);

  $('#panelFormData').empty().append(player.view.container);

  //Создать запись в реестре
  $('#buttonCreateFormData').off().on('click', e => {
    if (!player.model.getErrors().length) {
      fire({type: "set_disabled", disabled: true, cmpID: "buttonCreateFormData"}, "buttonCreateFormData");

      player.createRegistryDocument(registryCode, true, (dataID, documentID) => {
        showMessage('Запись успешно создана', 'success');
        const {previousPageCode} = Cons.getAppStore();
        fire({type: 'goto_page', pageCode: previousPageCode}, 'root-panel');
      }, (status, responseText) => {
        Cons.hideLoader();
        showMessage('Произошла ошибка при создании записи', 'error');
        console.log("create_form_data failed",  responseText);
      });
    } else {
      Cons.hideLoader();
      showMessage('Заполните обязательные поля', 'error');
    }
  });

  //Кнопка назад, переход на записаную в store страницу
  $('#buttonBack').off().on('click', e => {
    const {previousPageCode} = Cons.getAppStore();
    fire({type: 'goto_page', pageCode: previousPageCode}, 'root-panel');
  });

  //стопаем напоминалки
  CustomersNotification.stop();

});
