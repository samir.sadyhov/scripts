const eventParam = {
  registryCode: null
};

const createNavButton = (param, index) => {
  const {registryCode, registryID, registryName} = param;
  const button = $('<button>', {class: 'uk-button menu-button'});

  if(index == 0) button.addClass('selected');

  button.append(
    getSvgIcon('format_list_numbered'),
    `<span class="button-name" registryCode="${registryCode}">${registryName}</span>`
  )

  $('#panel-nav').append(button);

  button.on('click', e => {
    e.preventDefault();
    e.target.blur();

    $('.menu-button').removeClass('selected');
    button.addClass('selected');

    eventParam.registryCode = registryCode;
    $('#customRegistryList').trigger({
      type: 'renderNewTable',
      eventParam: eventParam
    });
  });
}

const getDictsRegList = async () => {
  const registryList = await appAPI.getRegistryList();
  const dictsRegGroup = registryList.find(x => x.regGroupName == "Справочные реестры");
  if(!dictsRegGroup || !dictsRegGroup.consistOf.length) return null;

  return dictsRegGroup.consistOf;
}

const initNavMenu = async () => {
  const dictsRegList = await getDictsRegList();
  if(!dictsRegList) return;

  eventParam.registryCode = dictsRegList[0].registryCode;
  $('#customRegistryList').trigger({
    type: 'renderNewTable',
    eventParam: eventParam
  });

  for(let i = 0; i < dictsRegList.length; i++) createNavButton(dictsRegList[i], i);
}

this.translateDictsMenu = async () => {
  const dictsRegList = await getDictsRegList();
  if(!dictsRegList) return;

  for(let i = 0; i < dictsRegList.length; i++) {
    const {registryCode, registryName} = dictsRegList[i];
    const buttonName = $(`.button-name[registrycode="${registryCode}"]`);
    if(buttonName.length) buttonName.text(registryName);
  }
}

pageHandler('dictionariesPage', () => {
  initNavMenu();

  const {previousPageCode = 'dealsPage'} = Cons.getAppStore();
  Cons.setAppStore({previousPageCode: 'dictionariesPage'});

  leftPanelInit();

  if (!Cons.getAppStore().dictionariesPage_listener) {
    addListener('button_click', 'buttonRefreshRegList', e => {
      $('#customRegistryList').trigger({type: 'updateTableBody'});
    });

    addListener('button_click', 'buttonBack2', e => {
      fire({type: 'goto_page', pageCode: previousPageCode}, 'root-panel');
    });

    //клик по записи реестра
    addListener('registry_item_click', 'customRegistryList', async e => {
      Cons.showLoader();

      const event = {
        dataUUID: e.dataUUID,
        registryCode: eventParam.registryCode,
        registryName: '',
        editable: false
      };
      if($(document).width() < 769) event.viewCode = 'mobile';

      const regInfo = await appAPI.getRegistryInfo(event.registryCode);
      const form = await AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/asforms/form/${regInfo.formId}`);

      Cons.hideLoader();

      event.registryName = regInfo.name;
      event.formName = form.nameru;

      $('#root-panel').trigger({type: 'custom_open_document', eventParam: event});
    });

    //Кнопка создать
    addListener('button_click', 'buttonCreateRow', async e => {
      try {
        Cons.showLoader();

        const regInfo = await appAPI.getRegistryInfo(eventParam.registryCode);
        if(!regInfo) throw new Error(i18n.tr('При создании записи произошла ошибка. Не удалось плучить информацию по реестру.'));
        if(regInfo.rr_create != "Y") throw new Error(i18n.tr('У вас нет прав на создание записи'));

        const doc = await appAPI.createDoc(eventParam.registryCode);
        if(!doc) throw new Error(i18n.tr('Произошла ошибка при создании данного типа документа'));

        const form = await AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/asforms/form/${regInfo.formId}`);

        $('#customRegistryList').trigger({type: 'updateTableBody'});

        Cons.hideLoader();

        const event = {
          dataUUID: doc.dataUUID,
          registryName: regInfo.name,
          registryCode: regInfo.code,
          formName: form.nameru,
          editable: true
        };

        $('#root-panel').trigger({type: 'custom_open_document', eventParam: event});
      } catch (err) {
        showMessage(err.message, 'error');
        Cons.hideLoader();
      }
    });

    Cons.setAppStore({dictionariesPage_listener: true});
  }

  $('#searchInput').off().on('keyup', e => {
    if (e.keyCode === 13) {
      e.preventDefault();
      $('#customRegistryList').trigger({
        type: 'searchInRegistry',
        eventParam: {
          searchString: $('#searchInput').val()
        }
      });
    }
  });

  //стопаем напоминалки
  CustomersNotification.stop();

});
