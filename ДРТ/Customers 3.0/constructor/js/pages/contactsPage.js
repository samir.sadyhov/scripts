const deleteDoc = documentID => {
  return AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/delete_doc?documentID=${documentID}`);
}

const appoint = async (selectedItems, callBack) => {

  let filterDepartmentID = null;
  const {positions} = AS.OPTIONS.currentUser;

  if(positions.length) {
    const posHead = positions.find(x => x.type == 1);
    if(posHead) {
      filterDepartmentID = posHead.departmentID;
    } else {
      filterDepartmentID = positions[0].departmentID;
    }
  }

  //(values, multiSelectable, isGroupSelectable, showWithoutPosition, filterPositionID, filterDepartmentID, locale, handler)
  AS.SERVICES.showUserChooserDialog([], false, false, false, null, filterDepartmentID, AS.OPTIONS.locale, async users => {
    Cons.showLoader();

    const {personID, personName} = users[0];
    const promises = selectedItems.map(documentID => appAPI.getAsfDataUUID(documentID));
    const dataUUIDs = await Promise.all(promises);

    const data = [
      {
        id: 'crm_form_contact_responsibleManager',
        type: 'entity',
        key: personID,
        value: personName
      }
    ];

    const mergePromises = dataUUIDs.map(uuid => appAPI.mergeFormData({uuid, data}));
    const mergeResult = await Promise.all(mergePromises);

    Cons.hideLoader();
    callBack();
  });

}

const deleteRows = (selectedItems, callBack) => {
  UIkit.modal.confirm('Вы действительно хотите удалить все выделенные записи реестра?', {labels: {ok: i18n.tr('Да'), cancel: i18n.tr('Отмена')}}).then(() => {
    Cons.showLoader();
    try {
      const promises = selectedItems.map(docID => deleteDoc(docID));

      Promise.all(promises).then(res => {
        showMessage(i18n.tr("Все выделенные записи реестра удалены"), "success");
        Cons.hideLoader();
        callBack();
      });

    } catch (err) {
      Cons.hideLoader();
      showMessage(i18n.tr("Произошла ошибка при удалении записей реестра"), "error");
      console.log(error);
    }
  }, () => null);
}


pageHandler('contactsPage', async () => {
  const registryCode = 'customers_registry_contacts';

  if (!Cons.getAppStore().contactsPage_listener) {

    //клик по записи реестра
    addListener('registry_item_click', 'customRegistryList', async e => {
      Cons.showLoader();

      const event = {
        dataUUID: e.dataUUID,
        registryCode,
        registryName: '',
        editable: false
      };
      if($(document).width() < 769) event.viewCode = 'mobile';

      const {registryList} = Cons.getAppStore();
      const regInfo = registryList.find(x => x.registryCode == registryCode);

      const form = await AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/asforms/form/${regInfo.info.formId}`);

      Cons.hideLoader();

      event.registryName = regInfo.name;
      event.formName = form.nameru;

      $('#root-panel').trigger({type: 'custom_open_document', eventParam: event});
    });

    Cons.setAppStore({contactsPage_listener: true});
  }

  Cons.setAppStore({previousPageCode: 'contactsPage'});
  localStorage.setItem('previousPageCode', 'contactsPage');

  leftPanelInit(async () => {
    const massActions = {
      containerID: 'panelMassAction',
      items: [
        {
          name: i18n.tr('Назначить'),
          handler: function(selectedItems, callBack){
            appoint(selectedItems, callBack)
          }
        },
        {
          name: i18n.tr('Удалить'),
          className: 'item-delete',
          handler: function(selectedItems, callBack){
            deleteRows(selectedItems, callBack)
          }
        }
      ]
    };

    const eventParam = {registryCode, massActions};

    const searchKey = `searchValue_${Cons.getCurrentPage().code}_${AS.OPTIONS.currentUser.userid}`;
    let searchValue = Cons.getAppStore()[searchKey];
    if(searchValue && searchValue !== "") {
      $('#searchInput').val(searchValue);
      eventParam.searchString = searchValue;
    }

    const {registryList} = Cons.getAppStore();
    const regInfo = registryList.find(x => x.registryCode == registryCode);

    //Кнопка обновить список записей реестра
    $('#buttonRefreshRegList').off().on('click', e => {
      $('#customRegistryList').trigger({type: 'updateTableBody'});
    });

    //Кнопка создать
    $('#buttonCreateRow').off().on('click', async e => {
      try {
        Cons.showLoader();

        if(!regInfo) throw new Error(i18n.tr('При создании записи произошла ошибка. Не удалось плучить информацию по реестру.'));
        if(!regInfo.allRights.includes("rr_create")) throw new Error(i18n.tr('У вас нет прав на создание записи'));

        const doc = await appAPI.createDoc(registryCode);
        if(!doc) throw new Error(i18n.tr('Произошла ошибка при создании данного типа документа'));

        const form = await AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/asforms/form/${regInfo.info.formId}`);

        $('#customRegistryList').trigger({type: 'updateTableBody'});

        Cons.hideLoader();

        const eventParam = {
          dataUUID: doc.dataUUID,
          registryName: regInfo.name,
          registryCode: regInfo.code,
          formName: form.nameru,
          editable: true
        };

        $('#root-panel').trigger({type: 'custom_open_document', eventParam});
      } catch (err) {
        showMessage(err.message, 'error');
        Cons.hideLoader();
      }
    });

    //отображаем кнопку создать если есть права
    if(regInfo.allRights.includes("rr_create")) $('#buttonCreateRow').removeClass('uk-hidden');

    //централизованные фильтры
    if(regInfo.hasOwnProperty('filters') && regInfo.filters.length) {

      const items = regInfo.filters.map(x => {
        const {name: title, code: value} = x;
        return {title, value};
      });

      if(regInfo.rights.includes("rr_list")) {
        eventParam.filterCode = null;
        items.unshift({title: i18n.tr('Нет'), value: null});
      } else {
        eventParam.filterCode = regInfo.filters[0].code;
      }

      new ComboBox($('#filterBox'), items, selectValue => {
        $('#customRegistryList').trigger({
          type: 'updateTableBody',
          eventParam: {
            filterCode: selectValue
          }
        });
      });
    } else {
      $('#panelRegistryFilters').hide();
    }

    //инициация кастомного компонента фильтрации записей реестра
    Cons.setAppStore({formCode: 'customers_form_contact'});
    $('#registryFilterComponent').trigger({
      type: 'init_filters',
      eventParam: {
        container: 'panelAppActionsRight',
        formCode: 'customers_form_contact',
        registryComponent: 'customRegistryList',
        showTemplate: true
      }
    });

    //инициация кастомного компонента записей реестра
    $('#customRegistryList').trigger({
      type: 'renderNewTable',
      eventParam: eventParam
    });

    $('#searchInput').off().on('keyup', e => {
      if (e.keyCode === 13) {
        e.preventDefault();
        const s = {};
        s[searchKey] = $('#searchInput').val();
        Cons.setAppStore(s);
        $('#customRegistryList').trigger({
          type: 'searchInRegistry',
          eventParam: {
            searchString: $('#searchInput').val()
          }
        });
      }
    });

    //кнопка выгрузки отчетов
    fire({
      type: 'reports_init',
      objectType: 65536,
      registryCode,
      registryListComponent: 'customRegistryList'
    }, 'buttonReports');

    //инициализация напоминалок
    CustomersNotification.init();
  });

});
