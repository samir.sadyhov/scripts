const addLog = async data => {
  return new Promise(async resolve => {
    AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/logger/log", resolve, 'json', JSON.stringify(data), "application/json; charset=UTF-8", resolve);
  });
}

const getRegistryList = async () => {
  return new Promise(async resolve => {
    let list = await appAPI.getRegistryList();
    list = UTILS.parseRegistryList(list);
    Cons.setAppStore({registryList: list});
    resolve(list);
  });
}

const parseTranslations = items => {
	const result = {};
  for(const key in items) {
  	const {message: {value, translations}} = items[key];
    result[value] = translations;
  }
  return result;
}

pageHandler('authPage', () => {
  if (!Cons.getAppStore().auth_page_listener) {
    addListener('auth_success', 'buttonLogin', async authed => {
      const {login, password} = authed.creds;

      Cons.creds.login = login;
      Cons.creds.password = password;
      AS.apiAuth.setCredentials(login, password);
      AS.OPTIONS.login = login;
      AS.OPTIONS.password = password;
      AS.OPTIONS.currentUser = authed.data.person;

      Cons.setAppStore({userGroups: authed.data.groups});

      await getRegistryList();

      const systemLocales = await appAPI.getSystemLocales();
      Cons.setAppStore({systemLocales});

      const dictTransaction = await appAPI.getDictionary('customers_translations', false);
      if(dictTransaction && dictTransaction.hasOwnProperty('items')) {
        const t = parseTranslations(dictTransaction.items);
        Cons.setAppStore({translations: t});
      } else {
        Cons.setAppStore({translations: 'not found'});
      }

      await updateTranslations();

      await addLog({
        "providerId": "AI_SEC",
        "eventId": "2005",
        "userId": AS.OPTIONS.currentUser.userid,
        "objects": [AS.OPTIONS.login, "Customers"]
      });

      const {previousPageCode = 'calendarPage'} = localStorage;
      fire({type: 'goto_page', pageCode: previousPageCode}, 'root-panel');
    });

    addListener('auth_failure', 'buttonLogin', e => {
      showMessage('Ошибка авторизации', 'error');
    });

    Cons.setAppStore({auth_page_listener: true});
  }
});
