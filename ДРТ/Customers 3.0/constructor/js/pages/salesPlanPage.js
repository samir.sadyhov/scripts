pageHandler('salesPlanPage', async () => {
  const registryCode = 'customers_registry_plans';

  if (!Cons.getAppStore().salesPlanPage_listener) {

    //клик по записи реестра
    addListener('registry_item_click', 'customRegistryList', async e => {
      Cons.showLoader();

      const event = {
        dataUUID: e.dataUUID,
        registryCode,
        registryName: '',
        editable: false
      };
      if($(document).width() < 769) event.viewCode = 'mobile';

      const {registryList} = Cons.getAppStore();
      const regInfo = registryList.find(x => x.registryCode == registryCode);

      const form = await AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/asforms/form/${regInfo.info.formId}`);

      Cons.hideLoader();

      event.registryName = regInfo.name;
      event.formName = form.nameru;

      $('#root-panel').trigger({type: 'custom_open_document', eventParam: event});
    });

    Cons.setAppStore({salesPlanPage_listener: true});
  }

  const pageCode = Cons.getCurrentPage().code;

  Cons.setAppStore({previousPageCode: pageCode});
  localStorage.setItem('previousPageCode', pageCode);

  leftPanelInit(async () => {
    const eventParam = {registryCode};

    const searchKey = `searchValue_${pageCode}_${AS.OPTIONS.currentUser.userid}`;
    let searchValue = Cons.getAppStore()[searchKey];
    if(searchValue && searchValue !== "") {
      $('#searchInput').val(searchValue);
      eventParam.searchString = searchValue;
    }

    const {registryList} = Cons.getAppStore();
    const regInfo = registryList.find(x => x.registryCode == registryCode);

    //Кнопка обновить список записей реестра
    $('#buttonRefreshRegList').off().on('click', e => {
      $('#customRegistryList').trigger({type: 'updateTableBody'});
    });

    //Кнопка создать
    $('#buttonCreateRow').off().on('click', async e => {
      try {
        Cons.showLoader();

        if(!regInfo) throw new Error(i18n.tr('При создании записи произошла ошибка. Не удалось плучить информацию по реестру.'));
        if(!regInfo.allRights.includes("rr_create")) throw new Error(i18n.tr('У вас нет прав на создание записи'));

        const doc = await appAPI.createDoc(registryCode);
        if(!doc) throw new Error(i18n.tr('Произошла ошибка при создании данного типа документа'));

        const form = await AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/asforms/form/${regInfo.info.formId}`);

        $('#customRegistryList').trigger({type: 'updateTableBody'});

        Cons.hideLoader();

        const eventParam = {
          dataUUID: doc.dataUUID,
          registryName: regInfo.name,
          registryCode: regInfo.code,
          formName: form.nameru,
          editable: true
        };

        $('#root-panel').trigger({type: 'custom_open_document', eventParam});
      } catch (err) {
        showMessage(err.message, 'error');
        Cons.hideLoader();
      }
    });

    //отображаем кнопку создать если есть права
    if(regInfo.allRights.includes("rr_create")) $('#buttonCreateRow').removeClass('uk-hidden');
    
    //централизованные фильтры
    if(regInfo.hasOwnProperty('filters') && regInfo.filters.length) {

      const items = regInfo.filters.map(x => {
        const {name: title, code: value} = x;
        return {title, value};
      });

      if(regInfo.rights.includes("rr_list")) {
        eventParam.filterCode = null;
        items.unshift({title: i18n.tr('Нет'), value: null});
      } else {
        eventParam.filterCode = regInfo.filters[0].code;
      }

      new ComboBox($('#filterBox'), items, selectValue => {
        $('#customRegistryList').trigger({
          type: 'updateTableBody',
          eventParam: {
            filterCode: selectValue
          }
        });
      });
    } else {
      $('#panelRegistryFilters').hide();
    }

    //инициация кастомного компонента фильтрации записей реестра
    Cons.setAppStore({formCode: 'customers_form_plan'});
    $('#registryFilterComponent').trigger({
      type: 'init_filters',
      eventParam: {
        container: 'panelAppActionsRight',
        formCode: 'customers_form_plan',
        registryComponent: 'customRegistryList',
        showTemplate: true
      }
    });

    //инициация кастомного компонента записей реестра
    $('#customRegistryList').trigger({
      type: 'renderNewTable',
      eventParam: eventParam
    });

    $('#searchInput').off().on('keyup', e => {
      if (e.keyCode === 13) {
        e.preventDefault();
        const s = {};
        s[searchKey] = $('#searchInput').val();
        Cons.setAppStore(s);
        $('#customRegistryList').trigger({
          type: 'searchInRegistry',
          eventParam: {
            searchString: $('#searchInput').val()
          }
        });
      }
    });

    fire({
      type: 'reports_init',
      objectType: 65536,
      registryCode,
      registryListComponent: 'customRegistryList'
    }, 'buttonReports');

    //инициализация напоминалок
    CustomersNotification.init();
  });

});
