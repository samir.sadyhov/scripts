var result = true;
var message = 'ok';

function customFormatDate(datetime){
  datetime = datetime.split(/\D/);
  return datetime[2] + '.' + datetime[1] + '.' + datetime[0];
}

function getPeriodMonths(planPeriod) {
  let result = [];

  if(!planPeriod || !planPeriod.hasOwnProperty('values')) {
    return ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12'];
  } else {
    planPeriod.values.forEach(function(quarter) {
      switch (quarter) {
        case '1': result.push('01', '02', '03'); break;
        case '2': result.push('04', '05', '06'); break;
        case '3': result.push('07', '08', '09'); break;
        case '4': result.push('10', '11', '12'); break;
      }
    });
  }

  return result;
}

function getManagerPlanAsfData(planData, months, planName, planAsfData){
  let asfData = [];
  if(planAsfData && planAsfData.hasOwnProperty('data')) asfData = planAsfData.data;

  //признак что план по менеджеру
  UTILS.setValue(asfData, 'customers_form_plan_type', {
    type: "radio",
    value: "2",
    key: " план по менеджеру"
  });

  //период (кварталы)
  let currentPlanPeriod = UTILS.getValue(asfData, 'customers_form_plan_period');
  if(currentPlanPeriod && currentPlanPeriod.hasOwnProperty('values')) {
    if(planData.planPeriod && planData.planPeriod.hasOwnProperty('values')) {
      for(let i = 0; i < planData.planPeriod.values.length; i++) {
        let value = planData.planPeriod.values[i];
        let key = planData.planPeriod.keys[i];
        if(currentPlanPeriod.values.indexOf(value) == -1) {
          currentPlanPeriod.values.push(value);
          currentPlanPeriod.keys.push(key);
        }
      }
      currentPlanPeriod.values.sort();
      currentPlanPeriod.keys.sort();
    }
  } else {
    UTILS.setValue(asfData, 'customers_form_plan_period', planData.planPeriod);
  }

  //суммы по месяцам
  months.forEach(function(month){
    let monthSum = Number(planData['month_sum_' + month].key);
    if(isNaN(monthSum)) monthSum = 0;
    UTILS.setValue(asfData, 'customers_form_plan_month_sum_' + month, {
      type: 'numericinput',
      key: String(monthSum),
      value: String(monthSum)
    });
  });

  //сумма за год
  let sumYearPlan = 0;
  for(let i = 1; i < 13; i++) {
    let monthNum = ('0' + i).slice(-2);
    let _sum_ = UTILS.getValue(asfData, 'customers_form_plan_month_sum_' + monthNum);
    if(_sum_ && _sum_.hasOwnProperty('key')) {
      sumYearPlan += Number(_sum_.key) || 0;
    }
  }
  UTILS.setValue(asfData, 'customers_form_plan_year_sum', {
    type: 'numericinput',
    key: String(sumYearPlan),
    value: String(sumYearPlan)
  });

  //дата изменения
  let currentDate = UTILS.getCurrentDateParse();
  UTILS.setValue(asfData, 'customers_form_plan_modified', {
    type: 'date',
    key: currentDate,
    value: customFormatDate(currentDate)
  });

  //ссылка на план по подразделению
  let parentPlanLink = UTILS.getValue(asfData, 'customers_form_plan_department_plan_link');
  if(parentPlanLink && parentPlanLink.hasOwnProperty('key')) {
    let docs = parentPlanLink.key.split(';');
    if(docs.indexOf(documentID) == -1) {
      UTILS.setValue(asfData, 'customers_form_plan_department_plan_link', {
        type: 'reglink',
        value: parentPlanLink.value + ';' + planName,
        key: parentPlanLink.key + ';' + documentID,
        valueID: parentPlanLink.valueID + ';' + documentID
      });
    }
  } else {
    UTILS.setValue(asfData, 'customers_form_plan_department_plan_link', {
      type: 'reglink',
      value: planName,
      key: documentID,
      valueID: documentID
    });
  }

  //дополнительные поля
  UTILS.setValue(asfData, 'customers_form_plan_salesmanagers', planData.manager);
  UTILS.setValue(asfData, 'customers_form_plan_year', planData.planYear);
  UTILS.setValue(asfData, 'customers_form_plan_service', planData.planService);
  UTILS.setValue(asfData, 'customers_form_plan_service_code', planData.planServiceCode);
  UTILS.setValue(asfData, 'customers_form_plan_department', planData.planDepartment);

  return asfData;
}

function searchManagerPlans(planYear, planService, planDepartment, manager) {
  let urlSearch = 'rest/api/registry/data_ext?registryCode=customers_registry_plans&loadData=true';

  urlSearch += '&field=customers_form_plan_type&condition=EQUALS&value=2';
  urlSearch += '&field1=customers_form_plan_year&condition1=TEXT_EQUALS&key1=' + planYear.key || '';
  urlSearch += '&field2=customers_form_plan_service&condition2=TEXT_EQUALS&key2=' + planService.key || '';
  urlSearch += '&field3=customers_form_plan_salesmanagers&condition3=CONTAINS&key3=' + manager.key || '';

  if(planDepartment && planDepartment.hasOwnProperty('key')) {
    urlSearch += '&field4=customers_form_plan_department&condition4=TEXT_EQUALS&key4=' + planDepartment.key || '';
  }

  return API.httpGetMethod(urlSearch);
}

try {
  let currentFormData = API.getFormData(dataUUID);
  let planType = UTILS.getValue(currentFormData, 'customers_form_plan_type');

  if(!planType || !planType.hasOwnProperty('value') || planType.value == '') throw new Error('Не удалось определить тип плана');
  if(planType.value != '1') throw new Error(planType.key);

  let planTableManagers = UTILS.getValue(currentFormData, 'customers_form_plan_table_managers');
  let planManagers = UTILS.parseAsfTable(planTableManagers);
  if(!planManagers.length) throw new Error('Не найдено информации в таблице customers_form_plan_table_managers');

  let currentDate = UTILS.getCurrentDateParse();
  let planYear = UTILS.getValue(currentFormData, 'customers_form_plan_year');
  let planService = UTILS.getValue(currentFormData, 'customers_form_plan_service');
  let planDepartment = UTILS.getValue(currentFormData, 'customers_form_plan_department');
  let planPeriod = UTILS.getValue(currentFormData, 'customers_form_plan_period');
  let planServiceCode = UTILS.getValue(currentFormData, 'customers_form_plan_service_code');

  let months = getPeriodMonths(planPeriod);
  let planName = API.getDocMeaningContent(documentID);


  planManagers.forEach(function(item, i){
    item.planYear = planYear;
    item.planService = planService;
    item.planDepartment = planDepartment;
    item.planPeriod = planPeriod;
    item.planServiceCode = planServiceCode;

    let searchResult = searchManagerPlans(planYear, planService, planDepartment, item.manager);

    if(searchResult.recordsCount > 0) {

      searchResult.result.forEach(function(plan){
        if(plan.fieldKey.hasOwnProperty('customers_form_plan_salesmanagers') && plan.fieldKey.customers_form_plan_salesmanagers == item.manager.key) {
          let planAsfData = API.getFormData(plan.dataUUID);
          let managerPlanAsfData = getManagerPlanAsfData(item, months, planName, planAsfData);

          API.mergeFormData({
            uuid: plan.dataUUID,
            data: managerPlanAsfData
          });
        }
      });

      let meaning = API.getDocMeaningContent(searchResult.result[0].documentID);

      UTILS.setValue(planTableManagers, 'plan_link-b' + (i + 1), {
        type: 'reglink',
        value: meaning,
        key: searchResult.result[0].documentID,
        valueID: searchResult.result[0].documentID
      });

    } else {
      let managerPlanAsfData = getManagerPlanAsfData(item, months, planName);

      let createResult = API.httpPostMethod("rest/api/registry/create_doc_rcc", {
        registryCode: 'customers_registry_plans',
        data: managerPlanAsfData,
        sendToActivation: true
      }, "application/json; charset=utf-8");

      if(createResult && createResult.errorCode == 0) {
        let meaning = API.getDocMeaningContent(createResult.documentID);

        UTILS.setValue(planTableManagers, 'plan_link-b' + (i + 1), {
          type: 'reglink',
          value: meaning,
          key: createResult.documentID,
          valueID: createResult.documentID
        });
      }
    }
  });

  UTILS.setValue(currentFormData, 'customers_form_plan_modified', {
    type: 'date',
    key: currentDate,
    value: customFormatDate(currentDate)
  });

  API.mergeFormData(currentFormData);

} catch (err) {
  log.error(err.message);
  message = err.message;
}
