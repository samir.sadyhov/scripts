var result = true;
var message = 'ok';

try {
  let currentFormData = API.getFormData(dataUUID);

  let registryCode = 'customers_activites';
  let appAsfData = [];
  let matching = [];

  let activities = UTILS.getValue(currentFormData, 'task_activity_link');

  matching.push({from: 'task_action', to: 'activities_form_actions'});
  matching.push({from: 'task_start', to: 'activities_form_date'});
  matching.push({from: 'task_contact', to: 'activities_form_contact'});
  matching.push({from: 'task_account', to: 'activities_form_account'});
  matching.push({from: 'task_deal', to: 'activities_form_deal'});
  matching.push({from: 'task_contract', to: 'activities_form_contract'});
  matching.push({from: 'task_lead', to: 'activities_form_lead'});
  matching.push({from: 'task_author', to: 'activities_form_author'});
  matching.push({from: 'task_Resposible', to: 'activities_form_executor'});
  matching.push({from: 'task_comment', to: 'activities_form_comment'});
  matching.push({from: 'task_status', to: 'activities_form_status'});

  matching.forEach(function(id) {
    let fromData = UTILS.getValue(currentFormData, id.from);
    if(fromData) UTILS.setValue(appAsfData, id.to, fromData);
  });

  if(activities && activities.hasOwnProperty('key')) {
    let activitiesDataUUID = API.getAsfDataId(activities.key);

    let mergeResult = API.mergeFormData({
      uuid: activitiesDataUUID,
      data: appAsfData
    });

  } else {
    appAsfData.push({
      id: "activities_form_creatework",
      type: "check",
      keys: [null],
      values: [null]
    });

    let createResult = API.httpPostMethod("rest/api/registry/create_doc_rcc", {
      registryCode: registryCode,
      data: appAsfData,
      sendToActivation: true
    }, "application/json; charset=utf-8");

    if(createResult && createResult.errorCode == 0) {

      let meaning = API.getDocMeaningContent(createResult.documentID) || "Активность";

      API.mergeFormData({
        uuid: dataUUID,
        data: [{
          id: 'task_activity_link',
          type: 'reglink',
          value: meaning,
          key: createResult.documentID,
          valueID: createResult.documentID
        }]
      });

    } else {
      throw new Error('Ошибка создания активности: (' + createResult.errorMessage + ')');
    }
  }

} catch (err) {
  log.error(err.message);
  result = false;
  message = err.message;
}
