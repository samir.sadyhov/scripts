const unmarkInvalidFields = () => {
  try {
    const managersView = view.playerView.getViewWithId('customers_form_plan_salesmanagers');
    if(managersView) managersView.unmarkInvalid();

    for(let i = 1; i < 13; i++) {
      const monthView = view.playerView.getViewWithId(`customers_form_plan_month_sum_${('0' + i).slice(-2)}`);
      if(monthView) monthView.unmarkInvalid();
    }
  } catch (e) {
    console.log(`ERROR [unmarkInvalidFields] ${e.message}`);
  }
}

const randomInteger = (min, max) => {
  const rand = min + Math.random() * (max + 1 - min);
  return Math.floor(rand);
}

const getManagersSumMonth = (id, randomManagerID, countManagers, planSumMonth) => {
  if(planSumMonth == 0) {
    return 0;
  } else {
    const W = parseInt((planSumMonth/countManagers));
    const P = planSumMonth % countManagers;
    let result = id == randomManagerID ? W + P : W;

    if(!Number.isInteger(result)) result = Number(result.fixed(2));

    return result;
  }
}

const fillTable = (managers, planSums) => {
  const tableID = 'customers_form_plan_table_managers';
  const tableModel = model.playerModel.getModelWithId(tableID);

  for(const numb in tableModel.getBlockNumbers()) tableModel.removeRow(numb);

  const randomManagerID = randomInteger(1, managers.length);

  for(let i = 0; i < managers.length; i++) {
    const row = tableModel.createRow();
    const {personID, personName, positionName} = managers[i];

    const managerModel = model.playerModel.getModelWithId('manager', tableID, row.tableBlockIndex);
    if(managerModel) managerModel.setValue({personID, personName, positionName});

    for(const month in planSums) {
      const planSumMonth = planSums[month];
      const monthSumModel = model.playerModel.getModelWithId(`month_sum_${('0' + month).slice(-2)}`, tableID, row.tableBlockIndex);
      const sum = getManagersSumMonth(i + 1, randomManagerID, managers.length, planSumMonth);
      if(monthSumModel) monthSumModel.setValue(String(sum));
    }
  }
}

const buttonHandler = () => {
  unmarkInvalidFields();

  try {
    const managers = model.playerModel.getModelWithId('customers_form_plan_salesmanagers')?.getValue() || null;
    const planSums = {};

    if(!managers || !managers.length) {
      const managersView = view.playerView.getViewWithId('customers_form_plan_salesmanagers');
      if(managersView) managersView.markInvalid();
      throw new Error("Выберите менеджеров");
    }

    let sumMonthError = 0;
    for(let i = 1; i < 13; i++) {
      const monthModel = model.playerModel.getModelWithId(`customers_form_plan_month_sum_${('0' + i).slice(-2)}`);
      if(monthModel && monthModel.getValue()) {
        planSums[i] = Number(monthModel.getValue());
      } else {
        planSums[i] = 0;
        sumMonthError++;
      }
    }

    if(sumMonthError == 12) {
      for(let i = 1; i < 13; i++) {
        const monthView = view.playerView.getViewWithId(`customers_form_plan_month_sum_${('0' + i).slice(-2)}`);
        if(monthView) monthView.markInvalid();
      }
      throw new Error("Заполните входные данные");
    }

    const msgConfirm = i18n.tr("Вы хотите начать распределение сумм?");
    if(window.confirm(msgConfirm)) fillTable(managers, planSums);

  } catch (e) {
    AS.SERVICES.showErrorMessage(e.message);
    console.log(e);
  }
}

const renderButton = () => {
  $('.distribute_sales_plan_button').remove();

  const button = $('<button>', {class: 'distribute_sales_plan_button'});

  button.text(i18n.tr('Распределить'));

  button.on('click', e => {
    e.preventDefault();
    e.target.blur();
    buttonHandler();
  });

  view.container.append(button);
}

if(editable) renderButton();
