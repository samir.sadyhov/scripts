try {
    /**
     * Метод для изменения внешнего ввида компонентов с помощью framework'a UIKit
     * @param {object} componentModel Модель компонента
     * @param {object} componentView Отоброжение компонента
     * @param {string} componentType Тип компонента
     * @param {object} playerModel Модель проигрывателя
     * @param {object} playerView Отоброжение проигрывателя
     * @author yandexphp
     */
    const componentSynergyRender = (componentModel, componentView, componentType, playerModel, playerView) => {
        /**
         * Если указан отоброжение компонента но нет контейнера в параметрах то
         * получаем контейнер из отоброжения компонента
         */
        const container = $(componentView.container);

        /**
         * Если тип компонента не был передан то пробуем его определить автоматический из модели компонента
         */
        if(!componentType && componentModel && componentModel instanceof Object) {
            componentType = componentModel.asfProperty.type;
        }

        /**
         * Режим отоброжения проигрывателя форм
         */
        const PlayerViewMode = editable;

        /**
         * Является ли устройство смартфоном - TRUE; иначе PC - FALSE
         */
        const isMobile = /Android|webOS|iPhone|iPad|iPod|BlackBerry|IEMobile|Opera Mini/i.test(navigator.userAgent);

        /**
         * Метод рендера для событий на компонентах
         */
        let render = () => componentSynergyRender(componentModel, componentView, componentType, playerModel, playerView);

        /**
         * Иконка
         */
        let icon = $('<span></span>');
        icon.addClass('uk-margin-small-left uk-margin-small-right');

        /**
         * Перерендериваем элемент удалить ряд у компонентов которые в дин.таблице
         */
        if(componentModel && componentModel.asfProperty.ownerTableId){
            container
                .closest('tr')
                .find('.asf-tableRemoveButton')
                .attr('uk-icon', 'trash')
                .addClass('uk-icon-link')
                .css('cursor', 'pointer')
                .removeClass('asf-tableRemoveButton')
                .closest('.asf-tableDeleteCell')
                .removeAttr('class')
                .addClass('get_width_salesPlan_form');
        }

        /**
         * Перерендериваем вид компонентов по их типу
         */
        switch(componentType){
            /**
             * Неизменяемый текст
             */
            case 'label':
                container
                    .find('.asf-label')
                    .addClass('uk-form-label')
                    .removeClass('asf-label')
                    .css({
                        'font-size': '',
                        'font-weight': ''
                    });
                break;
            /**
             * Однострочное поле
             * Числовое поле
             */
            case 'textbox':
            case 'numericinput':
                container
                    .find('.asf-textBox')
                    .addClass('uk-input')
                    .removeClass('asf-textBox');
                break;
            /**
             * Многострочное поле
             */
            case 'textarea':
                container
                    .find('.asf-textBox')
                    .addClass('uk-textarea')
                    .removeClass('asf-textBox');
                break;
            /**
             * Файл
             */
            case 'file':
            case 'filelink':
                if(!componentModel.componentSynergyRender){
                    componentModel.componentSynergyRender = true;
                    componentModel.on(AS.FORMS.EVENT_TYPE.valueChange, () => componentSynergyRender(componentModel, componentView, componentType, playerModel, playerView));
                }

                if(!PlayerViewMode){
                    return;
                }

                let fileData = componentModel.getValue();
                container
                    .find('.asf-file-delete-button')
                    .attr('uk-icon', 'close')
                    .css('cursor','pointer')
                    .removeClass('asf-file-delete-button')
                    .on('click', () => componentSynergyRender(componentModel, componentView, componentType, playerModel, playerView));
                container
                    .find('div[uk-form-custom]')
                    .remove();

                let fileName = container.find('.asf-file-name');
                if(fileName && fileName.length){
                    let URL = '&inline=true';
                    let link = $('<a></a>');

                    if(componentModel.hasOwnProperty('download') && componentModel.download){
                        URL = '';
                    }

                    URL = fileData.identifier + URL;

                    link
                        .addClass('uk-link uk-display-inline-block uk-text-small')
                        .attr('href', '/Synergy/resource_downloader?identifier=' + URL)
                        .attr('target', '_blank')
                        .text(fileData.name);

                    fileName
                        .parent()
                        .append(link);
                    fileName.remove();
                }else{
                    let ukCustomFile = $('<div></div>');
                    let inputFile = $('<input type="file">');
                    let button = $('<button></button>');

                    if(componentModel.hasOwnProperty('filterImage') && componentModel.filterImage){
                        inputFile.attr('accept','image/*');
                    }

                    container.addClass('uk-position-relative');

                    ukCustomFile.attr('uk-form-custom','');
                    ukCustomFile.addClass('uk-form-custom');

                    inputFile.on('change', e => {
                        let files = e.target.files;

                        if(files && files.length){
                            AS.SERVICES.showWaitWindow();

                            container
                                .find('button')
                                .remove();

                            let file = _.first(files);

                            let data = new FormData();
                            data.append('file', file);

                            AS.FORMS.ApiUtils.uploadFile(
                                playerModel.nodeId,
                                playerModel.asfDataId,
                                data,
                                response => {
                                    AS.FORMS.ApiUtils.simpleAsyncGet('rest/api/storage/description?elementID=' + response)
                                        .then(response => {
                                            AS.SERVICES.hideWaitWindow();
                                            return componentModel.setValue(response);
                                        });
                                },
                                error => {
                                    console.error(error);
                                    AS.SERVICES.hideWaitWindow();
                                }
                            );
                        }
                    });

                    button
                        .addClass('uk-button uk-button-default')
                        .text('Выбрать')
                        .css('cursor', 'pointer');

                    if(container.find('button[disabled]').length){
                        button.attr('disabled','');
                        inputFile.attr('disabled','');
                    }

                    ukCustomFile.append(inputFile);
                    button.appendTo(ukCustomFile);

                    container
                        .find('button')
                        .remove();

                    if(!fileData){
                        container.append(ukCustomFile);
                    }
                }
                break;
            /**
             * Дата/время
             */
            case 'date':
                icon.attr('uk-icon','calendar');

                container.css('width','auto');

                if(isMobile){
                    let phoneCalendar = $('<input type="date"/>');
                    phoneCalendar.addClass('uk-position-absolute uk-position-z-index');
                    phoneCalendar.css({
                        'top': 0,
                        'left': 0,
                        'right': 0,
                        'width': '100%',
                        'height': '100%',
                        'min-width': '100%',
                        'min-height': '100%',
                        'opacity': 0
                    });

                    phoneCalendar.on('change', () => componentModel.setValue(phoneCalendar.val()));

                    if(componentModel.getValue()){
                        let currentDate = new Date(Date.parse('' + componentModel.getValue()));

                        currentDate = [
                            currentDate.getFullYear(),
                            (currentDate.getMonth() + 1).toString().padStart(2, '0'),
                            currentDate.getDate()
                        ];

                        phoneCalendar.val(currentDate.join('.'));
                    }

                    container.append(phoneCalendar);

                    container.find('.asf-calendar-button').off();
                    container.find('.asf-dateBox').off();
                }

                container
                    .find('.asf-calendar-button')
                    .addClass('uk-button uk-button-default uk-padding-remove')
                    .removeClass('asf-calendar-button')
                    .css('margin-left', '-1px')
                    .html(icon);
                container
                    .find('.asf-dateBox')
                    .addClass('uk-input uk-text-center uk-display-block-inline')
                    .css('width','calc(100% - 41px)')
                    .removeClass('asf-dateBox');
                break;
            /**
             * Объекты Synergy
             * Ссылка на реестр
             * Ссылка на проект/портфель
             * Ссылка на адресную книгу
             */
            case 'entity':
            case 'reglink':
            case 'projectlink':
            case 'personlink':
                let button = container.find('.asf-browseButton');
                if(button.length){
                    icon.attr('uk-icon', 'more');
                    button
                        .removeClass('asf-browseButton')
                        .html(icon);
                }else{
                    button = container.find('.asf-user-chooser');
                    icon.attr('uk-icon', 'user');
                    button
                        .removeClass('asf-user-chooser')
                        .html(icon);
                }

                button
                    .addClass('uk-button uk-button-default uk-padding-remove')
                    .css({
                        'margin-left': '-1px',
                        'background': '#fff'
                    });
                container
                    .find('.ns-tagContainer')
                    .addClass('uk-input')
                    .css('width', 'calc(100% - 41px)')
                    .removeClass('ns-tagContainer');

                container
                    .find('.ns-tagItemInput')
                    .addClass('uk-label uk-text-truncate uk-padding-remove-horizontal')
                    .css({
                        'width': '90px',
                        'min-width': '90px',
                        'text-transform': 'none'
                    })
                    .parent()
                    .css('padding','0 6px 0 8px')
                    .removeClass('ns-tagItem')
                    .addClass('uk-label uk-margin-small-right');
                container
                    .find('.ns-tagDeleteImage')
                    .attr('uk-icon', 'close')
                    .addClass('uk-position-z-index')
                    .removeClass('ns-tagDeleteImage')
                    .css('cursor', 'pointer');

                if(!container.find('.ns-disabledInput').length){
                    container
                        .find('.asf-clickable-label')
                        .addClass('uk-display-inline-block');
                }else{
                    button.attr('disabled','');
                }

                container
                    .find('.asf-clickable-label')
                    .addClass('uk-button uk-button-text uk-text-left')
                    .css({
                        'font-size': '12px',
                        'width': 'auto'
                    })
                    .removeClass('asf-clickable-label');

                if(!componentModel.componentSynergyRender){
                    componentModel.componentSynergyRender = true;
                    componentModel.on(AS.FORMS.EVENT_TYPE.valueChange, render);
                    componentModel.on(AS.FORMS.EVENT_TYPE.dataLoad, render);

                    container
                        .find('.ns-tagSuggestionInput')
                        .on('keyup', e => {
                            switch(e.which){
                                case 13: render(); break;
                            }
                        });
                }
                break;
            /**
             * Выпадающий список
             */
            case 'listbox':
                if(!PlayerViewMode){
                    container
                        .find('.asf-label')
                        .addClass('uk-form-label')
                        .removeClass('asf-label');
                    break;
                }

                const listbox = container.find('.asf-dropdown-input');

                container.find('button').remove();

                listbox.get(0).obServerActions = props => {
                    const { target, type, attributeName } = props;

                    switch(type){
                        case 'attributes':
                            switch(attributeName){
                                case 'class':
                                    if(target.className.split(' ').includes('asf-invalidInput')){
                                        container.find('select').addClass('asf-invalidInput');
                                    }else{
                                        container.find('select').removeClass('asf-invalidInput');
                                    }
                                    break;
                                default:
                            }
                            break;
                        default:
                    }
                }

                obServer.observe(listbox.get(0), {
                    attributes: true,
                    attributeOldValue: true
                });

                listbox
                    .addClass('uk-select')
                    .removeClass('asf-dropdown-input')
                    .off()
                    .hide();

                const renderListBox = () => {
                    container.find('select').remove();

                    let listAllPlatform = $('<select></select>');
                    listAllPlatform.addClass('uk-select');

                    if(container.find('.ns-disabledInput').length){
                        listAllPlatform.attr('disabled','');
                    }

                    /**
                     * Пробегаемся и создаем элементы для нового списка а точнее HTML списка <select></select>
                     */
                    componentModel.listSelectHTML.forEach(x => {
                        let listElement = $('<option></option>');

                        listElement.attr('value', x.value);
                        listElement.text('' + x.label);

                        /**
                         * Устанавливаем значение по умолчанию как у компонента c Synergy
                         */
                        if(x.value === componentModel.getValue().values().next().value){
                            listElement.attr('selected', '');
                        }

                        listAllPlatform.append(listElement);
                    });

                    /**
                     * Подписываемся на событие изменения чтоб не только визуально изменились данные но и в самом компоненте Synergy
                     */
                    listAllPlatform.on('change', e => {
                        listAllPlatform.find('[selected]').removeAttr('selected');
                        componentModel.setValue(listAllPlatform.val());
                        $(e.target.options.item(e.target.options.selectedIndex)).attr('selected', '');
                    });

                    container.append(listAllPlatform);
                }

                /**
                 * Проверяем подгрузились ли данные компонента
                 */
                if(!componentModel.hasOwnProperty('listSelectHTML')){
                    const getListItems = () => {
                        let list = [];

                        if(componentModel.listCurrentElements && componentModel.listCurrentElements.length){
                            list = componentModel.listCurrentElements;
                        }else if(componentModel.listElements && componentModel.listElements.length){
                            list = componentModel.listElements;
                        }

                        return list;
                    }

                    componentModel.on(AS.FORMS.EVENT_TYPE.dataLoad, () => {
                        /**
                         * Создаем список значений из компонента "listBox"
                         */
                        componentModel.listSelectHTML = getListItems();

                        renderListBox();
                    });

                    componentModel.listSelectHTML = getListItems();
                    renderListBox();
                }else{
                    renderListBox();
                }
                break;
            /**
             * Ссылка
             */
            case 'link':
                if(!PlayerViewMode){
                    container
                        .find('.asf-link')
                        .addClass('uk-link uk-display-inline-block')
                        .removeClass('asf-link');
                }else{
                    icon.attr('uk-icon', 'link');
                    icon.addClass('uk-form-icon uk-form-icon-flip');

                    container
                        .find('.asf-link')
                        .addClass('uk-input uk-display-inline-block')
                        .removeClass('asf-link')
                        .parent()
                        .append(icon);
                }
                break;
            /**
             * Выбор вариантов
             */
            case 'check':
                container
                    .find('.asf-checkBox')
                    .addClass('uk-checkbox uk-margin-small-right');

                if(!componentModel.componentSynergyRender){
                    componentModel.componentSynergyRender = true;
                    componentModel.on(AS.FORMS.EVENT_TYPE.dataLoad, render);
                }
                break;
            /**
             * Переключатель вариантов
             */
            case 'radio':
                container
                    .find('.asf-radioButton')
                    .addClass('uk-radio uk-margin-small-right');

                if(!componentModel.componentSynergyRender){
                    componentModel.componentSynergyRender = true;
                    componentModel.on(AS.FORMS.EVENT_TYPE.dataLoad, render);
                }
                break;
            /**
             * Лист резолюций, Лист подписей, Ход выполнения
             */
            case 'resolutionlist':
            case 'signlist':
            case 'processlist':
                container.addClass('uk-overflow-auto');
                let table = container.find('.resolutionList');

                if(!table.length) table = container.find('.signsList');

                table
                    .addClass('uk-table uk-table-small')
                    .removeClass('resolutionList');
                break;
            /**
             * Перерендериваем прочие элементы таблицы и дин.таблицы
             */
            case 'appendable_table':
                /**
                 * "+ Добавить блок"
                 */
                container
                    .find('.asf-add-button')
                    .addClass('uk-button uk-button-text uk-text-small')
                    .removeClass('asf-add-button')
                    .css('font-size','12px');
                break;
            default:
                /* Рендер прочих элементов внутри проигрывателя формы */

                /**
                 * Удаляем рамки у всех таблиц.
                 */
                container.find('.asf-container[data-asformid*="table.container"]').find('.asf-borderedCell').css('border',0);
        }
    }

    /**
     * Наблюдатель Node Element'ов в DOM
     * @type {MutationObserver}
     */
    const obServer = new MutationObserver(mutation => {
        mutation.forEach(mutation => {
            const { target } = mutation;

            if(target.hasOwnProperty('obServerActions') && target.obServerActions instanceof Function){
                target.obServerActions(mutation);
            }
        });
    });

    /**
     * Пробегаемся по каждому компоненту на форме
     */
    if(~window.location.pathname.indexOf('Synergy') || ~window.location.pathname.indexOf('Configurator')) return;
    model.playerModel.getAsfData().data.forEach(component => {
        switch(component.type){
            /**
             * Если это динамическая таблица то...
             */
            case 'appendable_table':
                const tableModel = model.playerModel.getModelWithId(component.id);
                const tableView = view.playerView.getViewWithId(component.id);

                const componentSynergyTRender = (isUpdateTable) => {
                    if(isUpdateTable){
                        let tableRow = tableModel.modelBlocks[tableModel.modelBlocks.length - 1];
                        tableRow.forEach(tableComponent => {
                            const {id, tableBlockIndex} = tableComponent.asfProperty;
                            const componentModel = model.playerModel.getModelWithId(id, component.id, tableBlockIndex);
                            let componentView = view.playerView.getViewWithId(id, component.id, tableBlockIndex);

                            if(!componentView){
                                if(!componentModel.componentSynergyRender){
                                    componentModel.componentSynergyRender = true;
                                    componentModel.on(AS.FORMS.EVENT_TYPE.dataLoad, () => {
                                        componentView = view.playerView.getViewWithId(id, component.id, tableBlockIndex);

                                        componentSynergyRender(componentModel, componentView, tableComponent.asfProperty.type, model, view);
                                    });
                                }
                            }else{
                                componentSynergyRender(componentModel, componentView, tableComponent.asfProperty.type, model, view);
                            }
                        });
                    }else{
                        tableModel.headerModelBlock.forEach(tableHeaderComponent => {
                            const componentModel = model.playerModel.getModelWithId(tableHeaderComponent.asfProperty.id, component.id);
                            let componentView = view.playerView.getViewWithId(tableHeaderComponent.asfProperty.id, component.id);

                            if(!componentView){
                                let asformid = [componentModel.asfProperty.type, 'container', componentModel.asfProperty.id];
                                let tmp = $('.asf-container [data-asformid="' + asformid.join('.') + '"]');

                                componentView = {
                                    container: tmp
                                }
                            }

                            componentSynergyRender(componentModel, componentView, tableHeaderComponent.asfProperty.type, model, view);
                        });

                        tableModel.modelBlocks.forEach(tableRow => {
                            tableRow.forEach(tableComponent => {
                                const {id, tableBlockIndex} = tableComponent.asfProperty;
                                const componentModel = model.playerModel.getModelWithId(id, component.id, tableBlockIndex);
                                const componentView = view.playerView.getViewWithId(id, component.id, tableBlockIndex);

                                componentSynergyRender(componentModel, componentView, tableComponent.asfProperty.type, model, view);
                            });
                        });
                    }
                }

                /**
                 * Рендерим внешний вид дин.таблицы
                 */
                componentSynergyRender(tableModel, tableView, component.type, model, view);

                /**
                 * Подписываемся на событие добавление рядов в дин.таблицу чтобы отрендерить новые компоненты в таблице
                 */
                if(!tableModel.componentSynergyRender){
                    tableModel.componentSynergyRender = true;
                    tableModel.on(AS.FORMS.EVENT_TYPE.tableRowAdd, () => componentSynergyTRender(true));
                }

                componentSynergyTRender();
                break;
            /**
             * Если это пользовательский компонент то...
             */
            case 'custom':
                // ...
                break;
            /**
             * Если это не динамическая таблица то...
             */
            default:
                const componentModel = model.playerModel.getModelWithId(component.id);
                const componentView = view.playerView.getViewWithId(component.id);

                /**
                 * Рендерим новый стиль стандартного компонента
                 */
                componentSynergyRender(componentModel, componentView, component.type, model, view);
        }
    });

    componentSynergyRender(model.playerModel, view.playerView, null, model, view);
}catch(e){
    console.error('Custom Component - Execute `componentSynergyRender` func\n', e);
}
