AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  if (model.formCode == "Заявка_ключевых_показателей_деятельности,_доходов,_расходов,_кап._вложений_и_инвестиций") {

    var cmp = {
      table: "cmp-rscr2q",
      amount: ["cmp-1", "cmp-2", "cmp-3", "cmp-4"],
      total: "cmp-sum"
    };

    var setSumValue = function(model, cmp, index) {
      cmp.amount.forEach(function(cmp1) {
        var tmpAmount = model.getModelWithId(cmp1, cmp.table, index);
        tmpAmount.on("valueChange", function() {
          var TOTAL = 0;
          cmp.amount.forEach(function(cmp2) {
            var n = parseFloat(model.getModelWithId(cmp2, cmp.table, index).getValue());
            if(!isNaN(n)) TOTAL += n;
          });
          model.getModelWithId(cmp.total, cmp.table, index).setValue((Math.round(TOTAL * 100) / 100) + '');
        });
      });
    };

    setTimeout(function() {
      var dateTableView = view.getViewWithId(cmp.table);
      dateTableView.getViewBlocks().forEach(function(viewBlock, index){
        var tableBlockIndex = viewBlock.views[0].model.asfProperty.tableBlockIndex;
        // сумма значений
        setSumValue(model, cmp, tableBlockIndex);
      });
    }, 0);

    var dateTableModel = model.getModelWithId(cmp.table);
    if (view.editable) {

      dateTableModel.on('tableRowAdd', function(evt, modelTab, modelRow) {
        var tableBlockIndex = modelRow[0].asfProperty.tableBlockIndex;
        // сумма значений
        setSumValue(model, cmp, tableBlockIndex);
        // № п\п
        dateTableModel.modelBlocks.forEach(function(modelBlock, index){
          dateTableModel.getModelWithId("num", dateTableModel.asfProperty.id, modelBlock[0].asfProperty.tableBlockIndex).setValue((index+1)+"");
        });
      });

      dateTableModel.on('tableRowDelete', function(evt, rowModel, rowIndex, removedRow) {
        // № п\п
        dateTableModel.modelBlocks.forEach(function(modelBlock, index){
          dateTableModel.getModelWithId("num", dateTableModel.asfProperty.id, modelBlock[0].asfProperty.tableBlockIndex).setValue((index+1)+"");
        });
      });

    }

  }
});
