var changeButtonPodpis = function() {
  var allElem = $('.button-center-brightgray'); // все кнопки с этим классом
  var textElem = allElem.find('div.button-brightgray-text'); // подпись на кнопке

  if (textElem) {
    for (var i = 0; i < textElem.length; i++) {
      var tmpText = $(textElem[i]);
      if (tmpText.text() == "Подписать" || tmpText.text() == "Қол таңба қою" || tmpText.text() == "Sign") { // вот так вот ищем кнопку подписать
        tmpText.css("color", "black"); //текст кнопки черный

        var tmpElem = $(allElem[i]).parent().children();
        if (tmpElem) {
          for (var j = 0; j < tmpElem.length; j++) {
            $(tmpElem[j]).css("background-color", "#FF3400"); // перекрашиваем элементы кнопки в типа красный
          }
        }
      }
    }
  }

};

setInterval(changeButtonPodpis, 1000);
