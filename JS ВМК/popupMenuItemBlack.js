var REMOVE = {

  sendButton: function() {
    var popupMenu = $('.popupMenuBlack');
    var popupMenu2 = $('.GNTJMDODMK');


    if(AS.OPTIONS.locale == 'kk'){
        var textMenu = [];
    } else if(AS.OPTIONS.locale == 'ru') {
        var textMenu = ['На согласование/рассмотрение...', 'На согласование/рассмотрение', 'По маршруту...', 'Работа', 'Согласование'];
    } else {
        var textMenu = [];
    }

    function hideItemsMenu1(){
      if (popupMenu.length == 0) return;
      popupMenu.find('.menuSeperatorBlack').parent().parent().hide(); // скрываем разделители

      var itemMenu = popupMenu.find('.popup-menu-text');
      var menuHeight = 0;

      for (var i = 0; i < itemMenu.length; i++){
        var itemText = $(itemMenu[i]).text();
        var item = $(itemMenu[i]).parent().parent().parent().parent();
        if (textMenu.indexOf(itemText) == -1 ) {
          item.hide(); //скрываем лишнее
        } else {
          item.show();
          menuHeight++;
        }
      }
      // подгоняем высоту меню
      popupMenu.find('.popupMenuItemBlack')
      .css({'height': (menuHeight * 24)+'px'})
      .parent().css({'height': (menuHeight * 24)+'px'});
    }
    

    function hideItemsMenu2() {
      if (popupMenu2.length == 0) return;

      var menuHeight = 0;
      var removeRow = 0;
      var items = popupMenu2.find('.GNTJMDODNK');

      for(var i = 0; i < items.length; i++){
        if (['Работа', 'Жұмыс', 'Тапсырма', 'Work', 'Works'].indexOf($(items[i]).text()) != -1) removeRow++;
      }
      if(removeRow === 0) return;

      for(var i = 0; i < items.length; i++){
        var item = $(items[i]);
        if (textMenu.indexOf(item.text()) == -1 ) {
          item.hide(); //скрываем лишнее
        } else {
          item.show();
          menuHeight++;
        }
      }

      // подгоняем высоту меню
      popupMenu2.css({'height': (menuHeight * 32)+'px'})
      .find('.GNTJMDODDJ').css({'height': (menuHeight * 32)+'px'});

    }

    hideItemsMenu1();
    hideItemsMenu2();

  }

};


setInterval(function(){
  REMOVE.sendButton();
}, 0);
