AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  if (model.formId == '97dc1894-646d-4965-b297-444070eaa59f') {
    console.log("formID: [" + model.formId + "]");
    console.log("asfDataId: [" + model.asfDataId + "]");
    var monthModel = model.getModelWithId('month');
    monthModel.on(AS.FORMS.EVENT_TYPE.valueChange, function() {
      var monthNumber = monthModel.value;
      if (monthNumber && monthNumber.length > 0) {
        monthNumber=monthNumber[0];
        console.log('номер месяца: ' + monthNumber);
        clearStyle();
        searchHolidays(monthNumber, model);
      }
    }); // monthModel.on
  }
});

function searchHolidays (monthNumber, model) {
  var synergyLogin = '1';
  var synergyPass = '1';
  var cmpIdTable='table';
  var dictionary = {
    code: 'holidays',
    columns: {
      day: '5c43ea90-05d9-4f19-a844-e2bf426539dc',
      month: '1f3e4537-76d1-441e-92ba-95ad84320cf2',
      holiday: '70df5dd9-580c-47a6-884c-51e060ccc32d',
      color: '132a9b69-6965-4145-9f7d-201eb801d3c9'
    }
  };
  jQuery.ajax({
    url: 'rest/api/dictionary/get_by_code?dictionaryCode=' + dictionary.code,
    dataType: 'json',
    username: synergyLogin,
    password: synergyPass,
    success: function(data) {
      var dictionaryData = data.items;
      var items = [];
      var holidayArr = [];

      for (var dd = 0; dd < dictionaryData.length; dd++) {
        for (var dd2 = 0; dd2 < dictionaryData[dd].values.length; dd2++) {
          if (dictionaryData[dd].values[dd2].columnID == dictionary.columns.month) {
            if (dictionaryData[dd].values[dd2].value == monthNumber) {
              items.push(dictionaryData[dd].itemID);
            }
          }
        }
      }
      if (items.length > 0) {
        for (var dd = 0; dd < dictionaryData.length; dd++) {
          var tmpDay = null,
          tmpHol = null,
          tmpColor = null;
          for (var ii = 0; ii < items.length; ii++) {
            if (dictionaryData[dd].itemID == items[ii]) {
              for (var dd2 = 0; dd2 < dictionaryData[dd].values.length; dd2++) {
                if (dictionaryData[dd].values[dd2].columnID == dictionary.columns.day) {
                  tmpDay = dictionaryData[dd].values[dd2].value;
                }
              }
              for (var dd2 = 0; dd2 < dictionaryData[dd].values.length; dd2++) {
                if (dictionaryData[dd].values[dd2].columnID == dictionary.columns.holiday) {
                  tmpHol = dictionaryData[dd].values[dd2].value;
                }
              }
              for (var dd2 = 0; dd2 < dictionaryData[dd].values.length; dd2++) {
                if (dictionaryData[dd].values[dd2].columnID == dictionary.columns.color) {
                  tmpColor = dictionaryData[dd].values[dd2].value;
                }
              }
            }
          }
          if (tmpDay && tmpHol && tmpColor) {
            holidayArr.push({
              day: tmpDay,
              holiday: tmpHol,
              color: tmpColor
            });
          }
        }
      }

      if (holidayArr.length > 0) {
        for (var ha = 0; ha < holidayArr.length; ha++) {
          var cmpName = 'div[data-asformid="textbox.container.day_'+holidayArr[ha].day+'"]';
          var dynModel = model.getModelWithId(cmpIdTable);
          dynModel.on('tableRowAdd', function() {
            setStyle(cmpName, holidayArr[ha].holiday, holidayArr[ha].color);
          });
          setStyle(cmpName, holidayArr[ha].holiday, holidayArr[ha].color);
        }
      }
    }
  });
}

function setStyle(cmp, titleText, bgColor) {
  var tmpEl = $(cmp);
  for (var i = 0; i < tmpEl.length; i++) {
    var tmp2 = $(tmpEl[i]).parent('td')[0];
    if (tmp2) {
      tmp2.style.cursor = "pointer";
      tmp2.style.backgroundColor = bgColor;
      tmp2.title = titleText;
    }
  }
  console.log('setStyle');
}
function clearStyle() {
  for (var j=1; j<32; j++) {
    var searchEl='div[data-asformid="textbox.container.day_'+j+'"]';
    var tmpEl = $(searchEl);
    for (var i = 0; i < tmpEl.length; i++) {
      var tmp2 = $(tmpEl[i]).parent('td')[0];
      if (tmp2) {
        tmp2.style.cursor = '';
        tmp2.style.backgroundColor = '';
        tmp2.title = '';
      }
    }
  }
  console.log('clearStyle');
}
