<style>
span.boxShadow {
  position: relative;
  display: inline-block;
  font-size: 90%;
  font-weight: 700;
  color: rgb(209, 209, 217);
  text-decoration: none;
  text-shadow: 0 -1px 2px rgba(0, 0, 0, .2);
  padding: .5em 1em;
  outline: none;
  border-radius: 3px;
  background: linear-gradient(rgb(110, 112, 120), rgb(81, 81, 86)) rgb(110, 112, 120);
  box-shadow: 0 1px rgba(255, 255, 255, .2) inset, 0 3px 5px rgba(0, 1, 6, .5), 0 0 1px 1px rgba(0, 1, 6, .2);
  transition: .2s ease-in-out;
  cursor: pointer;
}

span.boxShadow:hover:not(:active) {
  background: linear-gradient(rgb(126, 126, 134), rgb(70, 71, 76)) rgb(126, 126, 134);
}

span.boxShadow:active {
  top: 1px;
  background: linear-gradient(rgb(76, 77, 82), rgb(56, 57, 62)) rgb(76, 77, 82);
  box-shadow: 0 0 1px rgba(0, 0, 0, .5) inset, 0 2px 3px rgba(0, 0, 0, .5) inset, 0 1px 1px rgba(255, 255, 255, .1);
}

div.msgFio {
text-align: center;
width: 100%;
height: 70%;
font-weight: 700;
font-size: 16pt;
text-decoration: none;
color: #9BC537;
text-shadow: 0 -1px 2px rgba(0, 0, 0, .2);
cursor: default;
}

div.buttonOK {
  width: 100px;
  position: absolute;
  left:50%;
  margin-left:-50px;
  font-size: 10pt;
  font-weight: bold;
  text-align: center;
  color: rgba(255,255,255,.6);
  text-shadow: 1px 1px rgba(0,0,0,.3);
  text-decoration: none;
  padding: 5px;
  border-radius: 10px;
  background: rgb(10,120,10);
  box-shadow:
   inset 0 0 3px 1px rgba(0,0,0,.8),
   inset rgba(0,0,0,.3) -5px -5px 8px 5px,
   inset rgba(255,255,255,.5) 5px 5px 8px 5px,
   1px 1px 1px rgba(255,255,255,.1),
   -2px -2px 5px rgba(0,0,0,.5);
  transition: .2s;
  cursor: pointer;
}
div.buttonOK:hover {
  color: rgba(255,255,255,.9);
  background: rgb(20,130,20);
}
div.buttonOK:active {
  background: rgb(0,110,0);
  box-shadow:
   inset 0 0 5px 3px rgba(0,0,0,.8),
   inset rgba(0,0,0,.3) -5px -5px 8px 5px,
   inset rgba(255,255,255,.5) 5px 5px 8px 5px,
   1px 1px 1px rgba(255,255,255,.2),
   -2px -2px 5px rgba(0,0,0,.5);
}
</style>

<span id="userName" class="boxShadow">КТО Я?</span>


userName.onclick = function() {
var userFIO = $CURRENT_USER.surname + ' ' + $CURRENT_USER.name + ' ' + $CURRENT_USER.patronymic;
  function clientWidth() { // Ширина окна просмотра
    return document.compatMode == 'CSS1Compat' && !window.opera ? document.documentElement.clientWidth : document.body.clientWidth;
  }

  function clientHeight() { // Высота окна просмотра
    return document.compatMode == 'CSS1Compat' && !window.opera ? document.documentElement.clientHeight : document.body.clientHeight;
  }

  document.getElementById('userName').onclick = function() {
    if (!document.getElementById('okno1')) {
      var okno = document.createElement('div'),
        oknoWidth = 350,
        oknoHeight = 100;
      okno.id = 'okno1';
      okno.style.position = 'fixed';
      okno.style.left = (clientWidth() - oknoWidth) / 2 + 'px';
      okno.style.top = (clientHeight() - oknoHeight) / 2 + 'px';
      okno.style.width = oknoWidth + 'px';
      okno.style.height = oknoHeight + 'px';
      okno.style.zIndex = '999';
      okno.style.boxSizing = 'border-box';
      okno.style.padding = '10px';
      okno.style.borderRadius = '7px';
      okno.style.background = 'linear-gradient(rgb(110, 112, 120), rgb(81, 81, 86)) rgb(110, 112, 120)';
      okno.style.boxShadow = '0 1px rgba(255, 255, 255, .2) inset, 0 3px 5px rgba(0, 1, 6, .5), 0 0 1px 1px rgba(0, 1, 6, .2)';
      okno.innerHTML ='<div class="msgFio">'+userFIO+'</div><div class="buttonOK" onclick="this.parentNode.parentNode.removeChild(this.parentNode)">OK</div>';
      document.body.appendChild(okno);
    }
    return false;
  };

};
