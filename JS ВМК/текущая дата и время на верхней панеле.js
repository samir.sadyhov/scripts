<div id="component-currentDate" align="center" style="transition: .2s ease-out; color: white;"></div>

var colorArr = ['red', 'blue', 'green', '#12BCB0', '#FABE0E', '#76B0C1', '#AAAAAA', '#C13AC5', '#96FF00', '#5C13D0'];

function getRandomInt(min, max) {
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

function currentDate() {
  var today = new Date();
  var month = Number(today.getMonth() + 1);
  if (month < 10) month = '0' + month;
  return ('0' + today.getDate()).slice(-2) + '.' + month + '.' + today.getFullYear() + ' ' + ('0' + today.getHours()).slice(-2) + ':' + ('0' + today.getMinutes()).slice(-2) + ':' + ('0' + today.getSeconds()).slice(-2);
}

var changeCurrentDate = function() {
  var component = $('div#component-currentDate');
  component[0].style.color = colorArr[getRandomInt(0, colorArr.length - 1)];
  component.text(currentDate());
};

setInterval(changeCurrentDate, 1000);
