var UTILS = {

  setNumberRow: function(model, table, number){
    var tableModel = model.getModelWithId(table);
    tableModel.on('tableRowAdd', function(evt, modelTab, modelRow) {
      tableModel.modelBlocks.forEach(function(modelBlock, index){
        tableModel.getModelWithId(number, table, modelBlock[0].asfProperty.tableBlockIndex).setValue((index+1)+"");
      });
    });
    tableModel.on('tableRowDelete', function(evt, rowModel, rowIndex, removedRow) {
      tableModel.modelBlocks.forEach(function(modelBlock, index){
        tableModel.getModelWithId(number, table, modelBlock[0].asfProperty.tableBlockIndex).setValue((index+1)+"");
      });
    });
  },

  setSumValue: function(model, tableModel, cmp) {
    var TOTAL = 0;
    tableModel.modelBlocks.forEach(function(modelBlock, index){
      var tableBlockIndex = modelBlock[0].asfProperty.tableBlockIndex;
      var n = parseFloat(tableModel.getModelWithId(cmp.sum, tableModel.asfProperty.id, tableBlockIndex).getValue());
      if(!isNaN(n)) TOTAL += n;
    });
    model.getModelWithId(cmp.total).setValue((Math.round(TOTAL * 100) / 100) + '');
  },

  compareDates: function(playerView, cmpStart, cmpStop, resCmp, table, blockNumber) {
    var startDate = null;
    var stopDate = null;
    var resultBox = null;
    if (!table && !blockNumber) {
      startDate = playerView.model.getModelWithId(cmpStart);
      stopDate = playerView.model.getModelWithId(cmpStop);
      if (resCmp) resultBox = playerView.model.getModelWithId(resCmp);
    } else {
      startDate = playerView.model.getModelWithId(cmpStart, table.ru, blockNumber);
      stopDate = playerView.model.getModelWithId(cmpStop, table.ru, blockNumber);
      if (resCmp) resultBox = playerView.model.getModelWithId(resCmp, table.ru, blockNumber);
    }
    if (startDate && stopDate) {
      var dateVerification = function(){
        if (AS.FORMS.DateUtils.parseDate(startDate.getValue()) && AS.FORMS.DateUtils.parseDate(stopDate.getValue()) ) {
          var d1 = AS.FORMS.DateUtils.parseDate(startDate.getValue()).getTime();
          var d2 = AS.FORMS.DateUtils.parseDate(stopDate.getValue()).getTime();
          var dd = Math.floor((d2-d1) / 86400000);
          if (dd < 0) {
            if (!table && !blockNumber) {
              if (playerView.getViewWithId(cmpStop)) playerView.getViewWithId(cmpStop).container.parent().css("background-color", "#FF9999");
            } else {
              setTimeout(function(){
                if (playerView.getViewWithId(cmpStop, table.ru, blockNumber)) playerView.getViewWithId(cmpStop, table.ru, blockNumber).container.parent().css("background-color", "#FF9999");
              }, 0);
            }
            UTILS.msgShow(
              "Дата ПО [" + stopDate.getValue() + "] меньше даты С [" + startDate.getValue() + "]" + "<br>" +
              "Аяқталу күні [" + stopDate.getValue() + "] басталу күнінен кіші [" + startDate.getValue() + "]"
            );
            if (resultBox) resultBox.setValue(null);
            stopDate.setValue(null);
            stopDate.asfProperty.required=true;
          } else {
            if (resultBox) {
              resultBox.setValue((dd+1) + "");
            }
          }
          if (!table && !blockNumber) {
            if (playerView.getViewWithId(cmpStop)) playerView.getViewWithId(cmpStop).container.parent().css("background-color", "");
          } else {
            setTimeout(function(){
              if (playerView.getViewWithId(cmpStop, table.ru, blockNumber)) playerView.getViewWithId(cmpStop, table.ru, blockNumber).container.parent().css("background-color", "");
            }, 0);
          }
        } else {
          if (resultBox) resultBox.setValue(null);
        }
      };
      startDate.on("valueChange", function() {
        dateVerification();
      });
      stopDate.on("valueChange", function() {
        dateVerification();
      });
    }
  },

  runAPIRequestGET: function(api) {
    var result = null;
    $.ajax({
      url: api,
      dataType: 'json',
      async: false,
      username: AS.OPTIONS.login,
      password: AS.OPTIONS.password,
      success: function(res) {result = res;}
    });
    return result;
  },

  getStatusUrl: function(link) {
    var ret = true;
    $.ajax({
      url: link,
      async: false,
      complete: function(q){
        if(q.status != 200) ret = false;
      }
    });
    return ret;
  },

  getUrlParameter: function(sParam) {
    var sPageURL = decodeURIComponent(window.location.href);
    var sURLVariables = sPageURL.split('&');
    var sParameterName;

    for (var i = 0; i < sURLVariables.length; i++) {
      sParameterName = sURLVariables[i].split('=');
      if (sParameterName[0] === sParam) {
        return sParameterName[1] === undefined ? true : sParameterName[1];
      }
    }
  },

  msgShow: function(msg, type) {
    var c1 = '#FF9999';
    var c2 = 'rgba(255, 153, 153, 0.7)';
    if (type == 'info') {
      c1 = '#B5D5FF';
      c2 = 'rgba(181, 213, 255, 0.7)';
    }
    var msgBox = $('<div/>');
    msgBox.css({
      'position': 'fixed',
      'left': '50%',
      'margin-right': '-50%',
      'transform': 'translate(-50%)',
      'top': '20px',
      'color': 'black',
      'font-size': '14px',
      'font-weight': 'bold',
      'text-align': 'center',
      'padding': '20px',
      'outline': 'none',
      'border-radius': '3px',
      'background': c1,
      'background-color': c2,
      'display': 'none',
      'z-index': '999'
    });
    $('body').append(msgBox);
    msgBox.html(msg).fadeToggle('normal');
    setTimeout(function() {
      msgBox.fadeToggle('normal');
      setTimeout(function() {
        msgBox.remove();
      }, 1000);
    }, 5000);
  }

};


AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
    console.group(model.formName);
    console.info("%c"+"formCode: ["+model.formCode+"]", "color: white");
    console.info("%c"+"formID: ["+model.formId+"]", "color: white");
    console.info("%c"+"asfDataID: ["+model.asfDataId+"]", "color: white");
    console.groupEnd();
  });
});
