var RECALC_TOTAL = {
  setings: [
    { formCode: "Счет_на_оплату_OMP",
      tableID: "tab",
      numberRowID: "nomer",
      itogoArr: [{sum: "stoimost", total: "summa"}]
    }
  ]
};

RECALC_TOTAL.setings.push({
  formCode: "Счет_на_оплату",
  tableID: "tab",
  numberRowID: "nomer",
  itogoArr: [{sum: "stoimost", total: "summa"}]
});

RECALC_TOTAL.setings.push({
  formCode: "Авансовый_отчет_1",
  tableID: "cmp-mau1ei",
  numberRowID: "cmp-y6r0ci",
  itogoArr: [{sum: "cmp-ru70a7", total: "cmp-568fv9"}]
});

AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  RECALC_TOTAL.setings.forEach(function(setings){

    if (model.formCode == setings.formCode) {

      setTimeout(function() {
        var tableView = view.getViewWithId(setings.tableID);
        var tableModel = model.getModelWithId(setings.tableID);

        tableView.getViewBlocks().forEach(function(viewBlock, index){
          var tableBlockIndex = viewBlock.views[0].model.asfProperty.tableBlockIndex;
          // ИТОГО
          setings.itogoArr.forEach(function(cmpObj){
            model.getModelWithId(cmpObj.sum, setings.tableID, tableBlockIndex).on("valueChange", function() {
              UTILS.setSumValue(model, tableModel, cmpObj);
            });
          });
        });

        tableModel.on('tableRowAdd', function(evt, modelTab, modelRow) {
          var tableBlockIndex = modelRow[0].asfProperty.tableBlockIndex;
          // ИТОГО
          setings.itogoArr.forEach(function(cmpObj){
            model.getModelWithId(cmpObj.sum, setings.tableID, tableBlockIndex).on("valueChange", function() {
              UTILS.setSumValue(model, tableModel, cmpObj);
            });
          });
        });

        tableModel.on('tableRowDelete', function(evt, rowModel, rowIndex, removedRow) {
          // ИТОГО
          setings.itogoArr.forEach(function(cmpObj){
            UTILS.setSumValue(model, tableModel, cmpObj);
          });
        });
      }, 0);

      // нумерация строк
      UTILS.setNumberRow(model, setings.tableID, setings.numberRowID);

    }

  });
});
