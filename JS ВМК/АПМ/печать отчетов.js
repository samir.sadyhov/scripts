var REPORTS = {
  param: [],

  searchJson: function(json, key, value) {
    var result = null;
    var tmpData = json.data ? json.data : json;
    for (var i = 0; i < tmpData.length; i++) {
      if (tmpData[i][key] == value) {
        result = tmpData[i];
        break;
      }
    }
    return result;
  },

  ADD_BUTTON: function(view) {
    var PARENT_COMPONENT = $('td.background-lilacgray').children('div.gwt-HTML');
    var buttonName = 'Выгрузить печатную форму';
    if (i18n.getCurrentLocale() == 'kk') buttonName = 'Баспа түрінде жүктеу';
    var BUTTON_REPORT = $('<button>', {class: 'NEW-BUTTON-REPORT'}).html(buttonName);

    BUTTON_REPORT.click(function(){

      var windowBody = $(view.container).closest('.window-body');
      var sendButton = $(windowBody).find("table[synergytest$='DocumentCardRegistrySendButton']");

      var documentID = UTILS.getUrlParameter('document_identifier');
      var documentInfo = UTILS.runAPIRequestGET('rest/api/docflow/doc/document_info?documentID=' + documentID);
      var param = REPORTS.searchJson(REPORTS.param, "formCode", documentInfo.formCode);

      var reportURL = "formreport?reportid=" + param.reportId + "&inline=false&filename=" + param.reportName + "&asfDataID=" + documentInfo.asfDataID;

      if(!param.model.isValid()) {
        UTILS.msgShow("Данные не корректны. Пожалуйста проверьте данные и попробуйте распечатать отчет еще раз");
        return;
      } else {
        if (sendButton.length === 0) {
          window.open(reportURL);
        } else {
          AS.SERVICES.showWaitWindow();
          var asfData = param.model.getAsfData();
          jQuery.when(AS.FORMS.ApiUtils.saveAsfData(asfData.data, documentInfo.formID, documentInfo.asfDataID)).then(function(asfDataId){
            window.open(reportURL);
            AS.SERVICES.hideWaitWindow();
          });
        }
      }

    });
    BUTTON_REPORT.prependTo(PARENT_COMPONENT);
  },

  hideOldPrintButton: function(editable) {
    var tmpPrint = $('td.background-lilacgray')[1];
    if(!editable) {
      $(tmpPrint).hide();
    } else {
      $(tmpPrint).show();
    }
  }
};


REPORTS.param.push({
  formCode: "Авансовый_отчет_1",
  reportId: "90a6d214-370d-48ab-943e-6b086d60ae04",
  reportName: "avansovyi_otchet.xls"
});

REPORTS.param.push({
  formCode: "Авансовый_отчет_иностранный",
  reportId: "75bd0003-38ab-41c1-be58-a2c7ef268509",
  reportName: "avansovyi_inostrannyi.xls"
});


AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  REPORTS.param.forEach(function(r) {
    if (model.formCode == r.formCode) {
      r.model = model;
      REPORTS.hideOldPrintButton(view.editable);
      model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
        if ($('.NEW-BUTTON-REPORT').length > 0) {
          $('.NEW-BUTTON-REPORT').remove();
          REPORTS.ADD_BUTTON(view);
        } else {
          REPORTS.ADD_BUTTON(view);
        }
      });
    }
  });
});



<style>
.NEW-BUTTON-REPORT {
  background-color: #9BC537;
  border: none;
  border-radius: 4px;
  color: black;
  padding: 4px 15px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 12px;
  font-weight: bold;
  cursor: pointer;
  float: left;
}

.NEW-BUTTON-REPORT:hover {
  background-color: #3e8e41;
}
</style>
