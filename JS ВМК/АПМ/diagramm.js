
var diagramData = {
    departmentId : null,
    userId : null,
    startDate : "",
    finishDate : "",
    listenerAdded : false,
    currentPath : null
};

function getPathElement(nodes){
    if(!nodes) return;
    for(var i=0; i<nodes.length; i++) {
        var text = jQuery(nodes[i]).text();
        if(!text || text == "") continue;
        return jQuery(nodes[i]);

    }
}

function path(){
    var nodes = jQuery(".gwt-TreeItem.gwt-TreeItem-selected");

    var path = [];
    var node = getPathElement(nodes);

    while(node && node.length > 0) {
        path.push(node.text());
        node = getPathElement(node.closest("div").closest("div").parent().closest("div").parent().closest("div").parent().children("table").find("td.gwt-TreeItem"));
    }
    return path;
}

function findDepartmentId(deps, name){
    var result = null;
    deps.forEach(function (dep){
        if(dep.departmentName == name) result = dep.departmentID;
    });
    return result;
}

function getDepartmentIds(pathItems, index, parentId, handler){
    var departmentName = pathItems[index];
    if (!parentId) {
        parentId = "32b36e0c-4621-4c96-99e1-3826284c363e";
    }
    var params = {
        departmentID: parentId,
        onlyPosition: false,
        onlyDepartments: true,
        locale: AS.OPTIONS.locale
    };

    AS.FORMS.ApiUtils.simpleAsyncGet('rest/api/departments/content?' + jQuery.param(params), function(result){
        var newParentId = findDepartmentId(result.departments, departmentName);
        if(index == 0){
            handler(newParentId);
        } else {
            getDepartmentIds(pathItems, index -1, newParentId, handler);
        }
    });
}

function createRow(title, count) {
    var title = jQuery("<td>").text(title);
    var count = jQuery("<td>").text(count);
    return jQuery("<tr>").append(title).append(count);
}

function drawReport(departmentId, userId, startDate, finishDate, canvasId, legendId, reportId){


    jQuery.when(AS.FORMS.ApiUtils.simpleAsyncGet("rest/api/report/list")).then(function(reports){
        var report = null;
        reports.forEach(function(r){
            if(r.defaultName == 'department.csv'){
                report = r;
            }
        });

        return AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/report/do?" +
                "reportID="+report.reportID+"" +
                "&fileName=1.csv" +
                "&departmentID="+departmentId+"" +
                "&userID="+userId+"" +
                "&finishDate="+finishDate +
                "&startDate="+startDate, null, "text");

    }).then(function(data){

        var parsedData = {};
        data.split("\n").forEach(function(item){
            var name = item.substring(0, item.indexOf(":"));
            var value = item.substring(item.indexOf(":")+1);
            if(value == "null") value = 0;
            parsedData[name] = value*1;
        });



        var all = parsedData.all;

        var reportData = {
            "Выполненные работы": parsedData.completed,
            "На исполнении": parsedData.inTime,
            "Просроченные": parsedData.late,
            "На контроле": parsedData.controlled
        };

        console.log(reportData);

        var canvas = jQuery('#'+canvasId).empty()[0];
        canvas.width = 400;
        canvas.height = 300;


        var legend = jQuery('#'+legendId).empty()[0];
        try {
            var myPiechart = new Piechart(
                    {
                        legend: legend,
                        canvas: canvas,
                        data: reportData,
                        colors: ["#0b871f", "#ddde78", "#c61b0e", "#1e4d82"]
                    }
            );

            myPiechart.draw();
        } catch (e){
            console.log(e);
        }

        var reportTable = jQuery("<table>", {width : "100%", border : "1", style : "border-collapse:collapse; border : 1px solid #cecece; width:100%", "cellPadding" : "5"});

        reportTable.append(createRow("Общее количество работ", parsedData.all));
        reportTable.append(createRow("Из них выполненные", parsedData.completed));
        reportTable.append(createRow("Из них на исполнении", parsedData.inTime));
        reportTable.append(createRow("Из них просроченные", parsedData.late));
        reportTable.append(createRow("Ознакомление", parsedData.acq));
        reportTable.append(createRow("Согласование", parsedData.ag));
        reportTable.append(createRow("Утверждение", parsedData.ap));
        reportTable.append(createRow("Протокол", parsedData.p));
        reportTable.append(createRow("На контроле", parsedData.controlled));

        jQuery("#"+reportId).empty().append(reportTable);

    });
}

function loadUserInfo(userId){
    console.log("LOADING user", userId)
    jQuery("#userImage").css("background-image", "url(\"load?max_width=120&max_height=180&userid="+userId+"\")");
    jQuery.when(AS.FORMS.ApiUtils.simpleAsyncGet("rest/api/filecabinet/user/"+userId)).then(function(userData){
        var name = userData.lastname+" "+userData.firstname;
        var positions = [];
        userData.positions.forEach(function(position){
            positions.push(position.positionName);
        });
        jQuery("#userName").text(name);
        jQuery("#userHeader").text(positions.join(", "));
    });
}

function buildDepartmentDiagram(){

    if(!diagramData.listenerAdded) return;

    var hash = window.location.hash;
    var userId = null;
    hash.split("&").forEach(function(item){
        if(item.indexOf("user_identifier") == -1) return;
        userId = item.substring(item.indexOf("=")+1);
    });

    if(!userId) {
        jQuery("#userDiv").hide();
        jQuery("#userInfoDiv").hide();
    }


    var pathItems = path();
    var departmentName = "";
    if(pathItems.length > 0) {
        departmentName = pathItems[0];
    }
    var newPath = pathItems.toString();
    var startDate = jQuery("#reportDateStart").first().val();
    var finishDate = jQuery("#reportDateFinish").first().val();

    if(diagramData.currentPath == newPath && diagramData.startDate == startDate && diagramData.finishDate == finishDate) return;

    diagramData.currentPath = newPath;
    diagramData.startDate = startDate;
    diagramData.finishDate = finishDate;

    jQuery("#departmentHeader").text(departmentName);
    jQuery("#departmentDiv").hide();


    getDepartmentIds(pathItems, pathItems.length - 1, null,
            function(depId){
                if(!depId || depId =='') return;
                jQuery("#departmentDiv").show();
                drawReport(depId, '', startDate, finishDate, 'departmentCanvas', "departmentLegend", "departmentReport");
            });
}

function buildUserDiagram(){

    if(!diagramData.listenerAdded) return;

    var hash = window.location.hash;
    var userId = null;
    hash.split("&").forEach(function(item){
        if(item.indexOf("user_identifier") == -1) return;
        userId = item.substring(item.indexOf("=")+1);
    });

    var startDate = jQuery("#reportDateStart").first().val();
    var finishDate = jQuery("#reportDateFinish").first().val();

    if(diagramData.userId == userId && diagramData.userStartDate == startDate && diagramData.userFinishDate == finishDate) return;

    diagramData.userId = userId;
    diagramData.userStartDate = startDate;
    diagramData.userFinishDate = finishDate;

    if(userId) {
        jQuery("#userDiv").show();
        jQuery("#userInfoDiv").show();
        drawReport("", userId, startDate, finishDate, 'userCanvas', "userLegend", "userReport");
        loadUserInfo(userId);
        addUserReportButton(userId);
    } else {
        jQuery("#userDiv").hide();
        jQuery("#userInfoDiv").hide();
    }

}

function addTreeListener(){
    if(window.location.hash.indexOf("submodule=employees&innermodule=structure") ==-1) return;

    var tree = jQuery(".tree-darkgray-stack").first();
    if(diagramData.listenerAdded) return;

    diagramData.listenerAdded = true;

    var observerConfig = {
        attributes: true,
        childList: true,
        characterData: true,
        subtree: true
    };
    var observer = new MutationObserver(function (mutations) {
        mutations.forEach(function (mutation) {
            if(mutation.attributeName != 'class') return;
            buildDepartmentDiagram();
            buildUserDiagram();
        });
    });
    observer.observe(tree[0], observerConfig);

}

function formatDate(date){
    var month = date.getMonth()+1;
    var day = date.getDate();
    if(month < 10){
        month = "0"+month;
    }
    if(day < 10){
        day = "0"+day;
    }
    return date.getFullYear()+"-"+(month)+"-"+day;
}

$(window).on('hashchange', function() {
   buildDepartmentDiagram();
   buildUserDiagram();
});

$( document ).on('DOMNodeInserted',".dygraph-rangesel-bgcanvas", function() {
    var table = jQuery(".dygraph-rangesel-bgcanvas").closest(".background-white");
    table.empty();


    var now = new Date();
    now.setMonth(now.getMonth()-1);
    var startDateBox = jQuery('<input/>', {type : 'text', class : 'asf-dateBox', id : 'reportDateStart', readonly : "readonly"}).val(formatDate(now));
    var startDateButton = jQuery("<button/>", {class : "asf-calendar-button"});
    startDateButton.click(function(){
        var dateParts = startDateBox.val().split("-");
        var date = new Date(dateParts[0], dateParts[1]-1, dateParts[2]);


        AS.SERVICES.showDatePicker(date, startDateBox, startDateBox, function(value){
            startDateBox.val(formatDate(value));
            buildDepartmentDiagram();
            buildUserDiagram();
        });
    });
    var startDiv = jQuery("<div>", {style : "display: inline-block; margin: 10px"}).append(startDateBox).append(startDateButton);


    now = new Date();
    var finishDateBox = jQuery('<input/>', {type : 'text', class : 'asf-dateBox', id : 'reportDateFinish', readonly : "readonly"}).val(formatDate(now));
    var finishDateButton = jQuery("<button/>", {class : "asf-calendar-button"});
    finishDateButton.click(function(){
        var dateParts = finishDateBox.val().split("-");
        var date = new Date(dateParts[0], dateParts[1]-1, dateParts[2]);
        AS.SERVICES.showDatePicker(date, finishDateBox, finishDateBox, function(value){

            finishDateBox.val(formatDate(value));
            buildDepartmentDiagram();
            buildUserDiagram();
        });
    });
    var finishDiv = jQuery("<div>", {style : "display: inline-block; margin: 10px"}).append(finishDateBox).append(finishDateButton);

    var datesDiv = jQuery("<div>", {id: "datesDiv"})
    .append(startDiv)
    .append(jQuery("<div>", {style : "display : inline"}).text("-"))
    .append(finishDiv);



    var userInfoDiv = jQuery("<div>", {id : "userInfoDiv", style : "display: inline-block; width: 120px; margin : 10px; vertical-align:top"});
    var userIcon = jQuery("<div>", {id : "userImage", style : "width: 120px; height:180px;; border : 1px solid #cecece; background-repeat: no-repeat; background-position: center center"});
    var userName = jQuery("<div>", {id : "userName", style : "width: 120px; "});

    userInfoDiv.append(userIcon).append(userName);

    var reportDataDiv = jQuery("<div>", {style : "display: inline-block"});

    var departmentDiv = jQuery("<div>", {id : "departmentDiv", style : "margin : 10px;"});
    var departmentHeader = jQuery("<div>", {id : "departmentHeader", style : "margin-left : 10px;"}).text("Данные о подразделении");
    var departmentCanvas = jQuery("<canvas>", {id : "departmentCanvas", width: "400px", height : "300px", style : "width:400px;height:300px; display:inline-block"});
    var departmentLegend = jQuery("<div>", {id : "departmentLegend", style : "width:200px; display:inline-block; margin: 10px;"});
    var departmentGraphic = jQuery("<div>", {style : "margin-left: 10px; border: 1px solid #cecece; display:inline-block"}).append(departmentCanvas).append(departmentLegend);
    var departmentReport = jQuery("<div>", {id : "departmentReport", style : "width:240px;height:300px;display:inline-block; margin-left:20px"});

    departmentDiv.append(departmentHeader);
    departmentDiv.append(departmentGraphic);
    departmentDiv.append(departmentReport);




    var userDiv = jQuery("<div>", {id : "userDiv", style : "margin : 10px;"});

    var userCanvas = jQuery("<canvas>", {id : "userCanvas", width: "400px", height : "300px", style : "width:400px;height:300px; display:inline-block"});
    var userHeader = jQuery("<div>", {id : "userHeader", style : "margin-left : 10px;"}).text("Данные о пользователе");
    var userLegend = jQuery("<div>", {id : "userLegend", style : "width:200px; display:inline-block; margin: 10px;"});
    var userGraphic = jQuery("<div>", {style : "margin-left: 10px; border: 1px solid #cecece; display:inline-block"}).append(userCanvas).append(userLegend);
    var userReport = jQuery("<div>", {id : "userReport", style : "width:240px;height:300px;display:inline-block; margin-left:20px"});

    userDiv.append(userHeader);
    userDiv.append(userGraphic);
    userDiv.append(userReport);

    reportDataDiv.append(departmentDiv);
    reportDataDiv.append(userDiv);


    var td = jQuery("<td>").append(datesDiv).append(userInfoDiv).append(reportDataDiv);

    table.append(jQuery("<tr>").append(td));


    addTreeListener();
    buildDepartmentDiagram();
    buildUserDiagram();
});

function addUserReportButton(userId){
    if($('.export-btn')) $('.export-btn').remove();
    if(!userId) return;

    var startDate = $("#reportDateStart").val();
    var finishDate = $("#reportDateFinish").val();

    var datesDiv = $("#datesDiv")
    var exportReportBtn = $('<button>', {class: 'export-btn'}).text("Выгрузить в PDF");
    exportReportBtn.click(function(){

      var reportURL = "formreport?reportid=ee35cb2a-4f6e-4360-b8db-b8ceb70b82ce&inline=false&filename=userWork.pdf&departmentID=&userID=" + userId + "&startDate=" + startDate + "&finishDate=" + finishDate;

      AS.SERVICES.showWaitWindow();
      jQuery.when(window.open(reportURL)).then(function(){
        AS.SERVICES.hideWaitWindow();
      });


    });
    datesDiv.append(exportReportBtn);
}

<style>
.export-btn {
  background-color: #9BC537;
  border: none;
  border-radius: 4px;
  color: black;
  padding: 4px 10px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 12px;
  font-weight: bold;
  cursor: pointer;
}

.export-btn:hover {
  background-color: #3e8e41;
}
</style>
