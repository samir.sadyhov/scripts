var WORK_UTIL = {

  createWork: function(workName, workRespID) {
    var d = new Date();
    var dateStart = AS.FORMS.DateUtils.formatDate(d, '${yyyy}-${mm}-${dd} ${HH}:${MM}:') + '00';
    var dateEnd = AS.FORMS.DateUtils.formatDate(new Date(d.getTime() + 10 * 86400000), '${yyyy}-${mm}-${dd} ${HH}:${MM}:') + '00';

    $.ajax({
      type: 'POST',
      url: 'rest/api/workflow/work/create',
      dataType: 'json',
      async: false,
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', 'Basic ' + btoa(AS.OPTIONS.login + ':' + AS.OPTIONS.password));
      },
      data: {
        name: workName,
        startDate: dateStart,
        finishDate: dateEnd,
        userID: workRespID,
        priority: 1
      },
      success: function (jqXHR, textStatus, errorThrown) {
        addComment(jqXHR.workID);
        if (jqXHR.errorCode === 0) {
          AS.SERVICES.showErrorMessage('Работа успешно создана');
        } else {
          AS.SERVICES.showErrorMessage('Работа не создана: ' + jqXHR.errorMessage);
        }
      },
      error: function (jqXHR, textStatus, errorThrown) {
        AS.SERVICES.showErrorMessage(jqXHR.errorMessage);
      }
    });
  },

  addComment: function(workID) {
    var addCommentURI = 'rest/api/workflow/work/' + workID + '/comments/save';
    var commentObj = {};
    commentObj.workID = workID;
    commentObj.comment = 'Ссылка на реестр: ' + window.location.hash;

    $.ajax({
      type: 'POST',
      data: commentObj,
      url: addCommentURI,
      dataType: 'json',
      async: true,
      beforeSend: function (xhr) {
        xhr.setRequestHeader('Authorization', 'Basic ' + btoa(AS.OPTIONS.login + ':' + AS.OPTIONS.password));
      }
    });
  }

};




function saveFormModifications(model, view) {
  var asfData = model.getAsfData();
  AS.FORMS.ApiUtils.saveAsfData(asfData.data, model.formId, model.asfDataId);
  view.updateValueFromModel();
}
