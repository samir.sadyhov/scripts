AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  if (model.formCode == "Служебная_записка_о_внесении_изменений_в_приказ_о_командирование") {

    // сумма значений "Планируемые командировочные расходы"
    var cmp = {
      table: {id: "cmp-ktahv1", dateS: "cmp-3fyud1", datePo: "cmp-pawsce"},
      amount: ["cmp-1", "cmp-2", "cmp-3", "cmp-4"],
      total: "cmp-sum"
    };
    recalcTotalValue(model, cmp);

    // С-ПО
    UTILS.compareDates(view, "date_start", "date_finish");

    setTimeout(function() {
      var dateTableView = view.getViewWithId(cmp.table.id);
      dateTableView.getViewBlocks().forEach(function(viewBlock, index){
        var tableBlockIndex = viewBlock.views[0].model.asfProperty.tableBlockIndex;
        // С-ПО
        UTILS.compareDates(view, cmp.table.dateS, cmp.table.datePo, null, cmp.table.id, tableBlockIndex);
      });
    }, 0);
    var dateTableModel = model.getModelWithId(cmp.table.id);
    if (view.editable) {
      dateTableModel.on('tableRowAdd', function(evt, modelTab, modelRow) {
        var tableBlockIndex = modelRow[0].asfProperty.tableBlockIndex;
        // С-ПО
        UTILS.compareDates(view, cmp.table.dateS, cmp.table.datePo, null, cmp.table.id, tableBlockIndex);
      });
    }

  }

  if (model.formCode == "Служебная_записка_о_командировании") {
    // сумма значений "Планируемые командировочные расходы"
    var cmp = {
      amount: ["cmp-1", "cmp-2", "cmp-3", "cmp-4"],
      total: "cmp-sum"
    };
    recalcTotalValue(model, cmp);

    // С-ПО
    UTILS.compareDates(view, "date_start", "date_finish");
  }

  if (model.formCode == "Служебная_записка_общего_вида") {
    var userBox = model.getModelWithId("cmp-4u6nci");
    var telephone = null;
    var userCard = null;

    if (view.editable) {
      if (userBox.getValue().length > 0) {
        userCard = UTILS.getCardsData("rest/api/personalrecord/forms/" + userBox.getValue()[0].personID, UTILS.PERSONAL_CARD_FORM_CODE);
          if (userCard) {
            telephone = UTILS.getAsfDataObject(userCard, 'work_number');
            if (telephone) {
              model.getModelWithId("cmp-39g609").setValue(telephone.value);
            } else {
              model.getModelWithId("cmp-39g609").setValue(null);
            }
          }
      } else {
        model.getModelWithId("cmp-39g609").setValue(null);
      }
    }
    userBox.on("valueChange", function() {
      if (userBox.getValue().length > 0) {
        userCard = UTILS.getCardsData("rest/api/personalrecord/forms/" + userBox.getValue()[0].personID, UTILS.PERSONAL_CARD_FORM_CODE);
        if (userCard) {
          telephone = UTILS.getAsfDataObject(userCard, 'work_number');
          if (telephone) {
            model.getModelWithId("cmp-39g609").setValue(telephone.value);
          } else {
            model.getModelWithId("cmp-39g609").setValue(null);
          }
        }
      } else {
        model.getModelWithId("cmp-39g609").setValue(null);
      }
    });

  }

});

var recalcTotalValue = function(model, cmp) {
  cmp.amount.forEach(function(cmp1) {
    var tmpAmount = model.getModelWithId(cmp1);
    tmpAmount.on("valueChange", function() {
      var TOTAL = 0;
      cmp.amount.forEach(function(cmp2) {
        var n = parseFloat(model.getModelWithId(cmp2).getValue());
        if(!isNaN(n)) TOTAL += n;
      });
      model.getModelWithId(cmp.total).setValue(TOTAL + '');
    });
  });
};
