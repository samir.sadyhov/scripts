var UTILS = {

  synergyLogin: AS.OPTIONS.login,
  synergyPass: AS.OPTIONS.password,

  PERSONAL_CARD_FORM_CODE : "Личные_данные",
  VACATION_CARD_FORM_CODE : "Отпуска_сотрудника",
  HOLYDAY_FORM_CODE: "Справочник_праздников",

  getCardsData: function(api, formCode) {
    var result = null;
    $.ajax({
      url: api,
      dataType: 'json',
      async: false,
      username: UTILS.synergyLogin,
      password: UTILS.synergyPass,
      success: function(cards) {
        var cardID = null;
        for (var i = 0; i < cards.length; i++) {
          if (cards[i].formCode == formCode) {
            cardID = cards[i]['data-uuid'];
            break;
          }
        }
        if (cardID) {
          $.ajax({
            url: 'rest/api/asforms/data/'+ cardID,
            dataType: 'json',
            async: false,
            username: UTILS.synergyLogin,
            password: UTILS.synergyPass,
            success: function(data) {
              result = data;
            }
          });
        }
      }
    });
    return result;
  },

  getAsfDataObject: function(data, cmpID) {
    var result = null;
    data.data.forEach(function(d) {
      if (d.id == cmpID) result = d;
    });
    return result;
  },

  compareDates: function(playerView, cmpStart, cmpStop, resCmp, table, blockNumber) {
    var formHolidaysCounting = [
      "Заявление_об_отпуске_с_материальной_помощью",
      "Заявление_о_предоставлении_оплачиваемого_ежегодного_отпуска",
      "Отпуск_за_счет_неиспользованных_дней_в_связи_с_отзывом_из_ежегодного_трудового_отпуска"
    ];
    var startDate = null;
    var stopDate = null;
    var resultBox = null;
    var holiday = 0;

    if (!table) {
      startDate = playerView.model.getModelWithId(cmpStart);
      stopDate = playerView.model.getModelWithId(cmpStop);
      if (resCmp) resultBox = playerView.model.getModelWithId(resCmp);
    } else {
      startDate = playerView.model.getModelWithId(cmpStart, table, blockNumber);
      stopDate = playerView.model.getModelWithId(cmpStop, table, blockNumber);
      if (resCmp) resultBox = playerView.model.getModelWithId(resCmp, table, blockNumber);
    }
    if (startDate && stopDate) {
      startDate.on("valueChange", function() {
        if (AS.FORMS.DateUtils.parseDate(startDate.getValue()) && AS.FORMS.DateUtils.parseDate(stopDate.getValue()) ) {
          var d1 = AS.FORMS.DateUtils.parseDate(startDate.getValue()).getTime();
          var d2 = AS.FORMS.DateUtils.parseDate(stopDate.getValue()).getTime();

          var dd = Math.floor((d2-d1) / 86400000);
          if (dd < 0) {
            if (table === null && blockNumber === null) {
              playerView.getViewWithId(cmpStop).container.parent().css("background-color", "#FF9999");
            } else {
              if (playerView.getViewWithId(cmpStop, table, blockNumber)) playerView.getViewWithId(cmpStop, table, blockNumber).container.parent().css("background-color", "#FF9999");
            }
            UTILS.msgShow("Дата ПО [" + stopDate.getValue() + "] меньше даты С [" + startDate.getValue() + "]");
            if (resultBox) {
              if (formHolidaysCounting.indexOf(playerView.model.formCode) !== -1) {
                // 335566
                holiday = UTILS.getHolidaysPeriod(startDate.getValue(), stopDate.getValue() );
                console.log("всего праздничных дней = " + holiday);
                resultBox.setValue((dd + 1 - holiday) + "");
              } else {
                resultBox.setValue((dd + 1) + "");
              }
            }
            stopDate.setValue(null);
            stopDate.asfProperty.required=true;
          } else {
            if (resultBox) {
              if (formHolidaysCounting.indexOf(playerView.model.formCode) !== -1) {
                // 335566
                holiday = UTILS.getHolidaysPeriod(startDate.getValue(), stopDate.getValue() );
                console.log("всего праздничных дней = " + holiday);
                resultBox.setValue((dd + 1 - holiday) + "");
              } else {
                resultBox.setValue((dd + 1) + "");
              }
            }
            if (table === null && blockNumber === null) {
              playerView.getViewWithId(cmpStop).container.parent().css("background-color", "");
            } else {
              if (playerView.getViewWithId(cmpStop, table, blockNumber)) playerView.getViewWithId(cmpStop, table, blockNumber).container.parent().css("background-color", "");
            }
          }
        } else {
          if (resultBox) resultBox.setValue(null);
        }
      });
      stopDate.on("valueChange", function() {
        if (AS.FORMS.DateUtils.parseDate(startDate.getValue()) && AS.FORMS.DateUtils.parseDate(stopDate.getValue()) ) {
          var d1 = AS.FORMS.DateUtils.parseDate(startDate.getValue()).getTime();
          var d2 = AS.FORMS.DateUtils.parseDate(stopDate.getValue()).getTime();

          var dd = Math.floor((d2-d1) / 86400000);
          if (dd < 0) {
            if (table === null && blockNumber === null) {
              playerView.getViewWithId(cmpStop).container.parent().css("background-color", "#FF9999");
            } else {
              if (playerView.getViewWithId(cmpStop, table, blockNumber)) playerView.getViewWithId(cmpStop, table, blockNumber).container.parent().css("background-color", "#FF9999");
            }
            UTILS.msgShow("Дата ПО [" + stopDate.getValue() + "] меньше даты С [" + startDate.getValue() + "]");
            if (resultBox) {
              if (formHolidaysCounting.indexOf(playerView.model.formCode) !== -1) {
                // 335566
                holiday = UTILS.getHolidaysPeriod(startDate.getValue(), stopDate.getValue() );
                console.log("всего праздничных дней = " + holiday);
                resultBox.setValue((dd + 1 - holiday) + "");
              } else {
                resultBox.setValue((dd + 1) + "");
              }
            }
            stopDate.setValue(null);
            stopDate.asfProperty.required=true;
          } else {
            if (resultBox) {
              if (formHolidaysCounting.indexOf(playerView.model.formCode) !== -1) {
                // 335566
                holiday = UTILS.getHolidaysPeriod(startDate.getValue(), stopDate.getValue() );
                console.log("всего праздничных дней = " + holiday);
                resultBox.setValue((dd + 1 - holiday) + "");
              } else {
                resultBox.setValue((dd + 1) + "");
              }
            }
            if (table === null && blockNumber === null) {
              playerView.getViewWithId(cmpStop).container.parent().css("background-color", "");
            } else {
              if (playerView.getViewWithId(cmpStop, table, blockNumber)) playerView.getViewWithId(cmpStop, table, blockNumber).container.parent().css("background-color", "");
            }
          }
        } else {
          if (resultBox) resultBox.setValue(null);
        }
      });
    }
  },

  getVacationDays : function(date, userID) {
    var availableDays = 0;
    var v = {
      userCard: UTILS.getCardsData("rest/api/personalrecord/forms/" + userID, UTILS.VACATION_CARD_FORM_CODE),
    };

    if (v.userCard) {
      v.vacation = UTILS.getAsfDataObject(v.userCard, 'table');
      v.reviewVacation = UTILS.getAsfDataObject(v.userCard, 'cmp-3ngjmo');
      v.vacationWithoutPay = UTILS.getAsfDataObject(v.userCard, 'cmp-8bctv8');
      v.employmentDate = UTILS.getAsfDataObject(v.userCard, 'day_start');

      var tmpVacation = 0;
      var tmpReviewVacation = 0;
      var tmpVacationWithoutPay = 0;
      var tmpFullExperience = 0;

      if (!v.employmentDate) return null;

      if ( AS.FORMS.DateUtils.parseDate(v.employmentDate.key) ) {
        // общий стаж дней на выбранную дату начала отпуска
        tmpFullExperience = Math.floor(( AS.FORMS.DateUtils.parseDate(date).getTime() - AS.FORMS.DateUtils.parseDate(v.employmentDate.key).getTime() ) / 86400000);
      }
      if (v.vacation) {
        v.vacation.data.forEach(function(d) {
          if (d.id.substring(0,4) == 'days') {
            var n = parseFloat(d.value);
            if (!isNaN(n)) tmpVacation += n;
          }
        });
      }
      if (v.reviewVacation) {
        v.reviewVacation.data.forEach(function(d) {
          if (d.id.substring(0,9) == 'day_otzyv') {
            var n = parseFloat(d.value);
            if (!isNaN(n)) tmpReviewVacation += n;
          }
        });
      }
      if (v.vacationWithoutPay) {
        v.vacationWithoutPay.data.forEach(function(d) {
          if (d.id.substring(0,10) == 'cmp-7mfg88') {
            var n = parseFloat(d.value);
            if (!isNaN(n)) tmpVacationWithoutPay += n;
          }
        });
      }
      // доступные дни отпуска
      availableDays = Math.floor((tmpFullExperience - tmpVacationWithoutPay) / 30 * 2.5 - tmpVacation + tmpReviewVacation);
      console.log('Отпуска: ' + tmpVacation);
      console.log('Дни из отзыва: ' + tmpReviewVacation);
      console.log('Отпуск без сохранения заработной платы: ' + tmpVacationWithoutPay);
      console.log('Общий стаж: ' + tmpFullExperience);
      console.log('Доступные дни отпуска: ' + availableDays);
    }
    console.log(v);
    return availableDays;
  },

  msgShow: function(msg, type) {
    var c1 = '#FF9999';
    var c2 = 'rgba(255, 153, 153, 0.7)';
    if (type == 'info') {
      c1 = '#B5D5FF';
      c2 = 'rgba(181, 213, 255, 0.7)';
    }
    var msgBox = $('<div/>');
    msgBox.css({
      'position': 'fixed',
      'left': '50%',
      'margin-right': '-50%',
      'transform': 'translate(-50%)',
      'top': '20px',
      'color': 'black',
      'font-size': '14px',
      'font-weight': 'bold',
      'text-align': 'center',
      'padding': '7px',
      'outline': 'none',
      'border-radius': '3px',
      'background': c1,
      'background-color': c2,
      'display': 'none',
      'z-index': '999'
    });
    $('body').append(msgBox);
    msgBox.html(msg).fadeToggle('normal');
    setTimeout(function() {
      msgBox.fadeToggle('normal');
      setTimeout(function() {
        msgBox.remove();
      }, 1000);
    }, 5000);
  },

  searchHolidays: function(year) {
    var formCode = UTILS.HOLYDAY_FORM_CODE;
    var holidayArr = []; // массив праздничных дней
    // Формирование поисковой строки
    var paramSearch = '?formCode='+formCode+'&search='+year+'&field=year&type=exact&recordCount=1&showDeleted=false&searchInRegistry=true';
    $.ajax({
      url: "rest/api/asforms/search" + paramSearch,
      dataType: 'json',
      async: false,
      username: UTILS.synergyLogin,
      password: UTILS.synergyPass,
      success: function(searchResult) {
        if (searchResult.length > 0) {
          $.ajax({
            url: 'rest/api/asforms/data/' + searchResult[0],
            dataType: 'json',
            async: false,
            username: UTILS.synergyLogin,
            password: UTILS.synergyPass,
            success: function(data) {
              data = data.data[2].data;
              for (var mm=1; mm<13; mm++) {
                for (var i = 0; i < data.length; i++) {
                  var tmp = data[i].id.substring(data[i].id.indexOf('-'));
                  var tmpDay = null, tmpHol = null;
                  if (data[i].id == ('nameHoliday' + tmp)) {
                    if (data[i + 2].key == mm) {
                      tmpHol = data[i].value;
                      tmpDay = parseInt(data[i + 1].key);
                      holidayArr.push({year: year, month: mm, day: tmpDay, holiday: tmpHol});
                    }
                  }
                }
              }
            }
          });
        }
      }
    });
    return holidayArr;
  },

  getHolidaysPeriod: function(startDate, stopDate) {
    var periodDay = Math.floor((AS.FORMS.DateUtils.parseDate(stopDate).getTime() - AS.FORMS.DateUtils.parseDate(startDate).getTime()) / 86400000);
    var neuchetDay = 0;
    var tmpDate1 = AS.FORMS.DateUtils.parseDate(startDate);
    var tmpDate2 = AS.FORMS.DateUtils.parseDate(stopDate);
    var tmpDate3 = AS.FORMS.DateUtils.parseDate(startDate);
    var tmpYear1 = tmpDate1.getFullYear();
    var tmpYear2 = tmpDate2.getFullYear();

    var holidays = UTILS.searchHolidays(tmpYear1);

    if (tmpYear1 != tmpYear2) {
      var tmpHoliday = UTILS.searchHolidays(tmpYear2);
      if (holidays && tmpHoliday) {
        tmpHoliday.forEach(function(holiday) {
          holidays.push(holiday);
        });
      }
    }
    if (holidays) {
      for (var ii = 1; ii < periodDay+2; ii++) {
        if (tmpDate3 > tmpDate2) {
          break;
        } else {
          holidays.forEach(function(d) {
            if (d.year == tmpDate3.getFullYear() && d.month == tmpDate3.getMonth()+1 && d.day == tmpDate3.getDate()) neuchetDay++;
            // console.log('getHolidaysPeriod: ' + neuchetDay);
          });
          tmpDate3.setDate(tmpDate3.getDate() + 1);
        }
      }
    }
    return neuchetDay;
  }

};

AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
    console.group(model.formName);
    console.info("%c"+"formCode: ["+model.formCode+"]", "color: white");
    console.info("%c"+"formID: ["+model.formId+"]", "color: white");
    console.info("%c"+"asfDataID: ["+model.asfDataId+"]", "color: white");
    console.groupEnd();
  });
});
