AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {

  // Заявление об отпуске с материальной помощью
  if (model.formCode == "Заявление_об_отпуске_с_материальной_помощью" || model.formCode == "Заявление_о_предоставлении_оплачиваемого_ежегодного_отпуска") {

    var recalcVacationDays = function(playerView, startDate, stopDate) {

      AS.FORMS.ApiUtils.getSigns(playerView.model.asfDataId, -1, "ru", function(signs){
        console.log("---===[ signs ]===---");
        console.log(signs.length);
        console.log(signs);
        console.log("---===[ signs ]===---");
        if(signs.length === 0)  {
          var userBox = playerView.model.getModelWithId("user");
          var dostupDay = UTILS.getVacationDays(startDate.getValue(), userBox.getValue()[0].personID);
          var vacDay = Math.floor((AS.FORMS.DateUtils.parseDate(stopDate.getValue()).getTime() - AS.FORMS.DateUtils.parseDate(startDate.getValue()).getTime()) / 86400000);
          vacDay = (vacDay + 1) - UTILS.getHolidaysPeriod(startDate.getValue(), stopDate.getValue());

          playerView.model.getModelWithId("result").setValue(dostupDay + '');
          playerView.model.getModelWithId("days_rest_from_last_vacation").setValue(dostupDay - vacDay + '');

          if (dostupDay < 1) {
            UTILS.msgShow("Для пользователя <u>" + userBox.getValue()[0].personName + "</u> нет доступных дней отпуска.", "info");
            playerView.model.getModelWithId("days").setValue(null);
            playerView.model.getModelWithId("is_ok").setValue(null);
            stopDate.setValue(null);
            playerView.model.getModelWithId("result").setValue(null);
            playerView.model.getModelWithId("days_rest_from_last_vacation").setValue(null);
            return;
          }

          if ( (dostupDay - vacDay) < 0 ) {
            UTILS.msgShow("Количество календарных дней <u>" + vacDay + "</u> превышает количество доступных дней <u>" + dostupDay + "</u>");
            playerView.model.getModelWithId("is_ok").setValue('0');
            playerView.getViewWithId("is_ok").container.parent().parent().css("background-color", "#FF9999");
            stopDate.setValue(null);
            playerView.model.getModelWithId("holiday").setValue(null);
          } else {
            playerView.model.getModelWithId("is_ok").setValue('1');
            playerView.getViewWithId("is_ok").container.parent().parent().css("background-color", "");
          }
        }
      });

    };

    var startDate = model.getModelWithId("date_start");
    var stopDate = model.getModelWithId("date_finish");
    startDate.on("valueChange", function() {
      if (AS.FORMS.DateUtils.parseDate(startDate.getValue()) && AS.FORMS.DateUtils.parseDate(stopDate.getValue()) ) {
        var nn = UTILS.getHolidaysPeriod(startDate.getValue(), stopDate.getValue() );
        model.getModelWithId("holiday").setValue(nn + '');
        AS.SERVICES.showWaitWindow();
        jQuery.when(recalcVacationDays(view, startDate, stopDate)).then(function(){
          AS.SERVICES.hideWaitWindow();
        });
      }
    });
    stopDate.on("valueChange", function() {
      if (AS.FORMS.DateUtils.parseDate(startDate.getValue()) && AS.FORMS.DateUtils.parseDate(stopDate.getValue()) ) {
        var nn = UTILS.getHolidaysPeriod(startDate.getValue(), stopDate.getValue() );
        model.getModelWithId("holiday").setValue(nn + '');
        AS.SERVICES.showWaitWindow();
        jQuery.when(recalcVacationDays(view, startDate, stopDate)).then(function(){
          AS.SERVICES.hideWaitWindow();
        });
      }
    });

    // С-ПО
    UTILS.compareDates(view, "date_start", "date_finish", "days");

    var table = {id: "a", dateS: "a2", datePo: "a4"};
    var replacementWorker = function(view, model, table, tableBlockIndex, startDate, stopDate) {
      // С-ПО
      UTILS.compareDates(view, table.dateS, table.datePo, null, table.id, tableBlockIndex);

      var tmpStartDate = model.getModelWithId(table.dateS, table.id, tableBlockIndex);
      var tmpStopDate = model.getModelWithId(table.datePo, table.id, tableBlockIndex);

      tmpStartDate.on("valueChange", function() {
        if (AS.FORMS.DateUtils.parseDate(tmpStartDate.getValue()) && AS.FORMS.DateUtils.parseDate(startDate.getValue()) && AS.FORMS.DateUtils.parseDate(stopDate.getValue()) ) {
          if ( (AS.FORMS.DateUtils.parseDate(tmpStartDate.getValue()) < AS.FORMS.DateUtils.parseDate(startDate.getValue())) || (AS.FORMS.DateUtils.parseDate(tmpStartDate.getValue()) > AS.FORMS.DateUtils.parseDate(stopDate.getValue())) ) {
            view.getViewWithId(table.dateS, table.id, tableBlockIndex).container.parent().css("background-color", "#FF9999");
            UTILS.msgShow("Выбранная дата не входит в выбранный период предоставления отпуска");
            tmpStartDate.setValue(null);
          } else {
            view.getViewWithId(table.dateS, table.id, tableBlockIndex).container.parent().css("background-color", "");
          }
        }
      });
      tmpStopDate.on("valueChange", function() {
        if (AS.FORMS.DateUtils.parseDate(tmpStopDate.getValue()) && AS.FORMS.DateUtils.parseDate(startDate.getValue()) && AS.FORMS.DateUtils.parseDate(stopDate.getValue()) ) {
          if ( (AS.FORMS.DateUtils.parseDate(tmpStopDate.getValue()) < AS.FORMS.DateUtils.parseDate(startDate.getValue())) || (AS.FORMS.DateUtils.parseDate(tmpStopDate.getValue()) > AS.FORMS.DateUtils.parseDate(stopDate.getValue())) ) {
            view.getViewWithId(table.datePo, table.id, tableBlockIndex).container.parent().css("background-color", "#FF9999");
            UTILS.msgShow("Выбранная дата не входит в выбранный период предоставления отпуска");
            tmpStopDate.setValue(null);
          } else {
            view.getViewWithId(table.datePo, table.id, tableBlockIndex).container.parent().css("background-color", "");
          }
        }
      });
    };

    setTimeout(function() {
      var dateTableView = view.getViewWithId(table.id);
      var dateTableModel = model.getModelWithId(table.id);
      if (view.editable) {
        dateTableView.getViewBlocks().forEach(function(viewBlock, index){
          var tableBlockIndex = viewBlock.views[0].model.asfProperty.tableBlockIndex;
          replacementWorker(view, model, table, tableBlockIndex, startDate, stopDate);
        });
        dateTableModel.on('tableRowAdd', function(evt, modelTab, modelRow) {
          var tableBlockIndex = modelRow[0].asfProperty.tableBlockIndex;
          replacementWorker(view, model, table, tableBlockIndex, startDate, stopDate);
        });
      }
    }, 0);

  }

  // Заявление о предоставлении отпуска без сохранения заработной платы
  if (model.formCode == "Заявление_о_предоставлении_отпуска_без_сохранения_заработной_платы") {
    // С-ПО
    UTILS.compareDates(view, "date_start", "date_finish", "days");
  }

  // Отпуск за счет неиспользованных дней в связи с отзывом из ежегодного трудового отпуска
  if (model.formCode == "Отпуск_за_счет_неиспользованных_дней_в_связи_с_отзывом_из_ежегодного_трудового_отпуска") {
    // С-ПО
    UTILS.compareDates(view, "date_start", "date_finish", "days");

    var startDate = model.getModelWithId("date_start");
    var stopDate = model.getModelWithId("date_finish");
    startDate.on("valueChange", function() {
      if (AS.FORMS.DateUtils.parseDate(startDate.getValue()) && AS.FORMS.DateUtils.parseDate(stopDate.getValue()) ) {
        var nn = UTILS.getHolidaysPeriod(startDate.getValue(), stopDate.getValue() );
        model.getModelWithId("holiday").setValue(nn + '');
      }
    });
    stopDate.on("valueChange", function() {
      if (AS.FORMS.DateUtils.parseDate(startDate.getValue()) && AS.FORMS.DateUtils.parseDate(stopDate.getValue()) ) {
        var nn = UTILS.getHolidaysPeriod(startDate.getValue(), stopDate.getValue() );
        model.getModelWithId("holiday").setValue(nn + '');
      }
    });

  }

});
