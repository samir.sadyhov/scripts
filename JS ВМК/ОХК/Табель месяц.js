var TABEL_UTIL = {

  yearModel: null,
  monthModel: null,
  holidays: null,
  holidaysMonth: [],

  setColorB: function(playerView, tableID, blockIndex) {
    for (var i = 1; i < 32; i++) {
      if (['В', 'в'].indexOf(playerView.model.getModelWithId('day_' + i, tableID, blockIndex).getValue()) !== -1) {
        playerView.getViewWithId('day_' + i, 'table', blockIndex).container.parent().css("background-color", "#FFFF00");
      }
      playerView.model.getModelWithId('day_' + i, tableID, blockIndex).on("valueChange", function(even, modelBox, val) {
        if (playerView.getViewWithId('day_' + i, tableID, blockIndex)) {
          if (['В', 'в'].indexOf(val) !== -1) {
            playerView.getViewWithId('day_' + i, tableID, blockIndex).container.parent().css("background-color", "#FFFF00");
          } else {
            playerView.getViewWithId('day_' + i, tableID, blockIndex).container.parent().css("background-color", "");
          }
        } else {
          setTimeout(function() {
            TABEL_UTIL.setColorB(playerView, tableID, blockIndex);
          }, 0);
        }
      });
    }
  },

  setColorHoliday: function(playerView, tableID, blockIndex, holidaysMonth, color) {
    holidaysMonth.forEach(function(d) {
      if (playerView.getViewWithId('day_' + d.day, tableID, blockIndex)) {
        playerView.getViewWithId('day_' + d.day, tableID, blockIndex).container.parent().css("background-color", color);
      } else {
        setTimeout(function() {
          TABEL_UTIL.setColorHoliday(playerView, tableID, blockIndex, holidaysMonth, color);
        }, 0);
      }
    });
  },

  tableRevrite: function(view, model) {
    if (TABEL_UTIL.holidays) {
      TABEL_UTIL.holidays.forEach(function(d) {
        if(d.month == TABEL_UTIL.monthModel.getValue()) TABEL_UTIL.holidaysMonth.push(d);
      });
      console.log(TABEL_UTIL.holidaysMonth);
    }
    setTimeout(function() {
      var tabelView = view.getViewWithId('table');
      tabelView.getViewBlocks().forEach(function(viewBlock, index) {
        var tableBlockIndex = viewBlock.views[0].model.asfProperty.tableBlockIndex;
        TABEL_UTIL.setColorHoliday(view, 'table', tableBlockIndex, TABEL_UTIL.holidaysMonth, "");
      });
    }, 0);

    setTimeout(function() {
      TABEL_UTIL.holidays = UTILS.searchHolidays(2014 + Number(TABEL_UTIL.yearModel.getValue()));

      if (TABEL_UTIL.holidays) {
        TABEL_UTIL.holidaysMonth = [];
        TABEL_UTIL.holidays.forEach(function(d) {
          if(d.month == TABEL_UTIL.monthModel.getValue()) TABEL_UTIL.holidaysMonth.push(d);
        });
        console.log(TABEL_UTIL.holidaysMonth);
      }

      setTimeout(function() {
        var tabelView = view.getViewWithId('table');
        tabelView.getViewBlocks().forEach(function(viewBlock, index) {
          var tableBlockIndex = viewBlock.views[0].model.asfProperty.tableBlockIndex;
          TABEL_UTIL.setColorB(view, 'table', tableBlockIndex);
          TABEL_UTIL.setColorHoliday(view, 'table', tableBlockIndex, TABEL_UTIL.holidaysMonth, "#FF3030");
        });
      }, 0);

      if (view.editable) {
        var tabelModel = model.getModelWithId('table');
        tabelModel.on('tableRowAdd', function(evt, modelTab, modelRow) {
          var tableBlockIndex = modelRow[0].asfProperty.tableBlockIndex;
          TABEL_UTIL.setColorB(view, 'table', tableBlockIndex);
          TABEL_UTIL.setColorHoliday(view, 'table', tableBlockIndex, TABEL_UTIL.holidaysMonth, "#FF3030");
        });
      }

    }, 1);

  }

};

AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  if (model.formCode == "Табель._месяц") {

    TABEL_UTIL.yearModel = model.getModelWithId('year');
    TABEL_UTIL.monthModel = model.getModelWithId('month');
    TABEL_UTIL.holidays = UTILS.searchHolidays(2014 + Number(TABEL_UTIL.yearModel.getValue()));
    TABEL_UTIL.holidaysMonth = [];

    TABEL_UTIL.yearModel.on("valueChange", function() {
      TABEL_UTIL.tableRevrite(view, model);
    });

    TABEL_UTIL.monthModel.on("valueChange", function() {
      TABEL_UTIL.tableRevrite(view, model);
    });

    TABEL_UTIL.tableRevrite(view, model);

  }
});
