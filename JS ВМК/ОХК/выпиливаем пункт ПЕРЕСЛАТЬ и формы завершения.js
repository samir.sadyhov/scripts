var REMOVE = {
  sendButton: function() {
    var popupMenuElem = $('td.popup-menu-text');
    if (popupMenuElem.length > 0) {
      for (var pmei = 0; pmei < popupMenuElem.length; pmei++) {
        var punktMenu = $(popupMenuElem[pmei]);
        var tmpText = ["Переслать", "Переслать...", "Жіберу", "Жіберу...", "Send", "Send..."];
        if (tmpText.indexOf(punktMenu.text()) !== -1 ) {
          $(punktMenu).parent().parent().parent().parent().hide();
        }
      }
    }
  },

  formsCompleted: function() {
    if ($('.dialogHeaderTextBlack').length > 0 &&
    ["Результат завершения", "Completion result", "Аяқталу нәтижесі"].indexOf($('.dialogHeaderTextBlack').text()) != -1) {
      return;
    }

    if ($('.gwt-DialogBox').length > 0 && $('input.gray-user-search-field').length === 0) {
      var k = 0;
      var completeArr = [
        'Нет', 'Комментарий', 'Комментарий без подтверждения', 'Файл', 'Результат работы', 'Исходящее письмо в АО «ФНБ «Самрук- Қазына»',
        'Не срочно', 'Плановое', 'Срочно', 'Критично', 'Без подтверждения',
        'Кол-во раб. дн.', 'Кол-во часов в день', 'Общее кол-во часов', '% рабочего времени',
        'По дням недели', 'По дням месяца', 'Ежегодно',
        'Работа', 'Согласование', 'Утверждение', 'Ознакомление', 'Резолюция', 'Отправка документа',
        'Жұмыстың нәтижесі', 'Түсініктеме', 'Жұмыс', 'Келісімдеу', 'Бекіту', 'Танысты', 'Бұрыштама', 'Құжатты жіберу',
        'Шұғыл емес', 'Жоспарлы', 'Тығыз', 'Шұғыл',
        'Жұмыс күндерінің саны', 'Күніне сағатының саны', 'Сағаттардың жалпы саны', 'Жұмыс уақытының %',
        'Жоқ', 'Апта күндері бойы', 'Ай күндері бойы', 'Жыл сайын',
        'No', 'Work result', 'Comments', 'File',
        'work', 'Consent', 'Approval', 'resolution', 'Acquaintance', 'document card',
        'Nonurgent', 'Routine', 'Urgent', 'Critical',
        'Number of working days', 'Number of hours per day', 'Total number of hours', '% of working time',
        'On week days', 'On month days', 'Annually',
        'Исходящее письмо в ТОО "Karabatan Utility Solutions"', 'Исходящее письмо в сторонние организации'
      ];

      var listComplete = $('div.comboPopup-Gray').children('div.popupContent').find('table.cursorDefault').find('div');

      for (var i = 0; i < listComplete.length; i++) {
        if (completeArr.indexOf(listComplete[i].title) == -1) {
          $(listComplete[i]).parent().parent().hide();
          k++;
        }
      }

      $('div.comboPopup-Gray').children('div.popupContent').css({'height': (listComplete.length - k) * 28 + 'px'});
    }
  }

};


setInterval(function(){
  REMOVE.sendButton();
  REMOVE.formsCompleted();
}, 0);
