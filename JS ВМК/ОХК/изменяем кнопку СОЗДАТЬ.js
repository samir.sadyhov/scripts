// кнопка создать
(function() {
  var NEW_COLOR = '#FF9999';
  var checkAgain = function() {
    setTimeout(replaceButtonCreate, 2000);
  };

  var replaceButtonCreate = function() {
    var CREATE_BUTTON_IMG = $('.button-multi-toggle-bright-green').children('.gwt-Image');

    if (CREATE_BUTTON_IMG === null || CREATE_BUTTON_IMG.length === 0) {
      checkAgain();
      return;
    }

    CREATE_BUTTON_IMG.parent().css({'width': '20', 'background': NEW_COLOR, 'cursor': 'pointer'});
    CREATE_BUTTON_IMG.css({'display':'block', 'margin':'0 auto', 'width':'70%', 'height':'70%'});
    CREATE_BUTTON_IMG.parent().parent().find('.gwt-HTML').css('background', NEW_COLOR);
    CREATE_BUTTON_IMG.parent().parent().find('.button-multi-toggle-right-bright-green').css({'background': NEW_COLOR, 'border-radius':'0px 3px 3px 0px', '-webkit-border-radius':'0px 3px 3px 0px', '-moz-border-radius':'0px 3px 3px 0px'});
  }

  replaceButtonCreate();

})();
