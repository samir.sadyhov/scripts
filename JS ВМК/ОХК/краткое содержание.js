var NAME_WORK_UTILS = {settings: []};

NAME_WORK_UTILS.settings.push({
  formCode: "Служебная_записка_общего_вида",
  topic: "topic"
});


NAME_WORK_UTILS.setTopic = function(text) {
  var result = false;
  var textarea = $('textarea.gwt-TextArea');
  if(textarea.length > 1) {
    for(var i = 0; i < textarea.length; i++) {
      $(textarea[i]).val(text);
    }
    result = true;
  }
  return result;
};

AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  NAME_WORK_UTILS.settings.forEach(function(settings){
    if (model.formCode == settings.formCode) {

      var newTopic = null;
      var topicModel = model.getModelWithId(settings.topic);
      newTopic = topicModel.getValue();

      topicModel.on("valueChange", function() {
        newTopic = topicModel.getValue();
        NAME_WORK_UTILS.init();
        return;
      });

      NAME_WORK_UTILS.init = function(){
        setTimeout(function s(){
          var res = NAME_WORK_UTILS.setTopic(newTopic);
          if (!res) setTimeout(s, 500);
        }, 500);
      };

      NAME_WORK_UTILS.init();
    }
  });
});
