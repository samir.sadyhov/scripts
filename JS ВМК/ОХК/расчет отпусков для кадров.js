AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  // Расчет отпускных дней
  if (model.formCode == 'Расчет_отпускных_дней') {

    if (view.editable) {

      setTimeout(function() {
        $('.button-center-green').parent().parent().parent().hide();
      }, 0);


      var c1 = '#CECECE';
      var c2 = '#49B785';
      var c3 = '#FF9999';

      var BUTTON = $('<button>', {class: 'ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only'}).html('Рассчитать');
      BUTTON.css({
        'cursor': 'pointer'
      });
      BUTTON.hover(function() {
        $(this).css('background-color', c2);
      }, function() {
        $(this).css('background-color', c1);
      });

      BUTTON.click(function() {
        var dateBox = model.getModelWithId('date');
        var userBox = model.getModelWithId('user');
        var divResult = $('<div>');
        divResult.css("font-size", "14px");

        if (AS.FORMS.DateUtils.parseDate(dateBox.getValue()) && userBox.getValue().length > 0 ) {
          console.log(userBox.getValue()[0].personID);
          console.log(userBox.getValue());
          var dostupDay = UTILS.getVacationDays(dateBox.getValue(), userBox.getValue()[0].personID);
          var tmpVal = AS.FORMS.DateUtils.parseDate(dateBox.getValue());
          tmpVal = AS.FORMS.DateUtils.formatDate(tmpVal, "${dd}.${mm}.${yyyy}");
          if (dostupDay) {
            divResult.html(userBox.getValue()[0].tagName + ', на <b>'+tmpVal+ '</b> доступно: <b>' + dostupDay + ' д.</b>');
          } else {
            divResult.html('Для расчета отпускных дней, необходимо заполнить карточку <b>Отпуска сотрудника</b> ' + userBox.getValue()[0].tagName);
          }

        } else {
          divResult.html('<b>Не заполнены поля</b>');
          // return;
        }

        $('[data-asformid="container"]').append(divResult);

      });

      view.getViewWithId('date').container.parent().parent().append(BUTTON);
      view.getViewWithId('date').container.parent().parent().parent().css({
        "border": "#EEEEEE solid 2px",
        "border-radius": "10px",
        "padding": "10px"
      });

    }

  }
});
