AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  //"Отпуска_сотрудника"
  if (model.formCode == UTILS.VACATION_CARD_FORM_CODE) {

    //if (view.getViewWithId('sum_available_vacat_days')) view.getViewWithId('sum_available_vacat_days').container.parent().parent().css("background-color", "#FF9999");

    if (view.getViewWithId('day_start')) {
      var c1 = '#CECECE';
      var c2 = '#49B785';
      var c3 = '#FF9999';

      var BUTTON = $('<button>', {class: 'ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only'}).html('Расчет доступных дней отпуска');

      BUTTON.click(function() {
        var dataTable = '<table class="asf-table" cellspacing="0" cellpadding="0" style="text-align: left; font-family: Arial, sans-serif; font-size: 12px;"><thead></thead><tbody><tr><td class="asf-cell" style="width: 350px;"><div class="asf-container"><div class="asf-label" style="text-align: left;font-family: Arial, sans-serif; font-size: 12px; font-weight: bold;">Дата расчета отпускных дней</div></div></td><td class="asf-cell" style="width: 350px;"><div class="asf-container" style="width: 128px;"><input type="text" class="asf-dateBox new-dateBox" style="text-align: left; font-family: Arial, sans-serif; font-size: 12px;" value="____-__-__" disabled=false><button class="asf-calendar-button new-calendar-button"></button></div> </td></tr></tbody></table>';

        dialogForm.html(dataTable);
        dialogForm.dialog("open");

        var dialogButton = $('div.ui-dialog').find('.ui-dialog-buttonset').children('button');
        $(dialogButton[0]).hover(function() {
          $(this).css('background-color', c2);
        }, function() {
          $(this).css('background-color', c1);
        });
        $(dialogButton[1]).hover(function() {
          $(this).css('background-color', c3);
        }, function() {
          $(this).css('background-color', c1);
        });

        var calendarButton = $('.new-calendar-button');
        calendarButton.click(function() {
          var dateBox = $('.new-dateBox');
          var tmpDate = new Date();
          if (AS.FORMS.DateUtils.parseDate(dateBox.val())) tmpDate = AS.FORMS.DateUtils.parseDate(dateBox.val());
          AS.SERVICES.showDatePicker(tmpDate, calendarButton, null, function(value){
            dateBox.val(AS.FORMS.DateUtils.formatDate(value, "${yyyy}-${mm}-${dd}"));
          });
        });

      });

      BUTTON.css({
        'cursor': 'pointer'
      });
      BUTTON.hover(function() {
        $(this).css('background-color', c2);
      }, function() {
        $(this).css('background-color', c1);
      });


      var dialogForm = $('<div>', {class: 'dialog-form'});
      dialogForm.dialog({
        autoOpen: false,
        height: 300,
        width: 400,
        modal: true,
        resizable: true,
        title: 'Расчет доступных дней отпуска',
        buttons: [
          {
            text: 'Рассчитать',
            click: function() {
              recalcVacationDay();
            }
          },
          {
            text: 'Закрыть',
            click: function() {
              dialogForm.html(null);
              dialogForm.dialog("close");
            }
          }
        ]
      });

      var recalcVacationDay = function() {
        var dateBox = $('.new-dateBox');
        var divResult = $('<div>', {class: 'ui-dialog-content'});
        divResult.css("font-size", "14px");
        if (AS.FORMS.DateUtils.parseDate(dateBox.val())) {
          dateBox.parent().parent().css("background-color", "");
          var dostupDay = UTILS.getVacationDays(dateBox.val(), $CURRENT_USER.id);
          var tmpVal = AS.FORMS.DateUtils.parseDate(dateBox.val());
          tmpVal = AS.FORMS.DateUtils.formatDate(tmpVal, "${dd}.${mm}.${yyyy}");
          divResult.html('На <b>'+tmpVal+ '</b> доступно: <b>' + dostupDay + ' д.</b>');
        } else {
          UTILS.msgShow("Введите дату");
          dateBox.parent().parent().css("background-color", "#FF9999");
          return;
        }
        dialogForm.append(divResult);
      };

      view.getViewWithId('day_start').container.parent().parent().parent().append(BUTTON);

    }

    // Положено дней (итого на сегодня)
    setTimeout(function() {
      var dostupDayNow = UTILS.getVacationDays(AS.FORMS.DateUtils.formatDate(new Date(), "${yyyy}-${mm}-${dd}"), $CURRENT_USER.id);
      model.getModelWithId('sum_available_vacat_days').setValue(dostupDayNow + '');
    }, 0);


    // Дни без содержания (итого на сегодня)
    var cmpArr = [
      {table: 'cmp-8bctv8', sum: 'cmp-7mfg88', total: 'days_not_pay'},
      {table: 'cmp-3ngjmo', sum: 'day_otzyv', total: 'sum_days_return'}
    ];

    var setSumValue = function(model, cmp) {
      var tableModel = model.getModelWithId(cmp.table);
      var TOTAL = 0;
      tableModel.modelBlocks.forEach(function(modelBlock, index){
        var tableBlockIndex = modelBlock[0].asfProperty.tableBlockIndex;
        var n = parseFloat(tableModel.getModelWithId(cmp.sum, cmp.table, tableBlockIndex).getValue());
        if(!isNaN(n)) TOTAL += n;
      });
      model.getModelWithId(cmp.total).setValue((Math.round(TOTAL * 100) / 100) + '');
    };

    cmpArr.forEach(function(cmp){
      setTimeout(function() {
        var dateTableView = view.getViewWithId(cmp.table);
        dateTableView.getViewBlocks().forEach(function(viewBlock, index){
          var tableBlockIndex = viewBlock.views[0].model.asfProperty.tableBlockIndex;
          var sumBox = model.getModelWithId(cmp.sum, cmp.table, tableBlockIndex);
          sumBox.on("valueChange", function() {
            // сумма значений
            setSumValue(model, cmp);
          });
        });
        // сумма значений
        setSumValue(model, cmp);
      }, 0);
      var dateTableModel = model.getModelWithId(cmp.table);
      if (view.editable) {
        dateTableModel.on('tableRowAdd', function(evt, modelTab, modelRow) {
          var tableBlockIndex = modelRow[0].asfProperty.tableBlockIndex;
          var sumBox = model.getModelWithId(cmp.sum, cmp.table, tableBlockIndex);
          sumBox.on("valueChange", function() {
            // сумма значений
            setSumValue(model, cmp);
          });
        });
        dateTableModel.on('tableRowDelete', function(evt, rowModel, rowIndex, removedRow) {
          // сумма значений
          setSumValue(model, cmp);
        });
      }
    });

    // // Подсветка заголовков таблиц
    // var tables = [
    //   ['cmp-9s7zv2', 'cmp-lvooqn', 'cmp-dcqvt9', 'cmp-828sd4', 'cmp-o283li'],
    //   ['cmp-fy1jx9', 'cmp-uris27', 'cmp-ocjsbd'],
    //   ['date_start', 'date_end', 'days_amount', 'vacation_type']
    // ];
    // tables.forEach(function(t) {
    //   t.forEach(function(c) {
    //     $('[data-asformid="label.container.'+c+'"]').parent().css("background-color", "#B5D5FF");
    //   });
    // });

  }
});
