AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {

  if (model.formCode == "Служебная_записка_на_изменение_данных_по_протоколам") {
    setTimeout(function() {
      var tabelView = view.getViewWithId('table03');
      var tabelModel = model.getModelWithId('table03');

      tabelView.getViewBlocks().forEach(function(viewBlock, index) {
        var tableBlockIndex = viewBlock.views[0].model.asfProperty.tableBlockIndex;
        changedLink(model, 'control', 'LinkToJournal', 'table03', tableBlockIndex);
      });
      tabelModel.on('tableRowAdd', function(evt, modelTab, modelRow) {
        var tableBlockIndex = modelRow[0].asfProperty.tableBlockIndex;
        changedLink(model, 'control', 'LinkToJournal', 'table03', tableBlockIndex);
        tabelView.setEnabled(false);
      });

      tabelView.setEnabled(false);
      tabelView.setColumnVisible(12, false);
    }, 0);
  }

  if (model.formCode == "СЗ_02_на_изменение_данных_") {
    setTimeout(function() {
      var tabelView = view.getViewWithId('table02');
      var tabelModel = model.getModelWithId('table02');

      tabelView.getViewBlocks().forEach(function(viewBlock, index) {
        var tableBlockIndex = viewBlock.views[0].model.asfProperty.tableBlockIndex;
        changedLink(model, 'control', 'LinkToJournal', 'table02', tableBlockIndex);
      });
      tabelModel.on('tableRowAdd', function(evt, modelTab, modelRow) {
        var tableBlockIndex = modelRow[0].asfProperty.tableBlockIndex;
        changedLink(model, 'control', 'LinkToJournal', 'table02', tableBlockIndex);
        tabelView.setEnabled(false);
      });

      tabelView.setEnabled(false);
      tabelView.setColumnVisible(9, false);
    }, 0);
  }

});

function changedLink(model, statCmp, linkCmp, tableCmp, tableBlockIndex){
  var tmpLink = {
    value: '#submodule=common&server_id=remote&file_identifier='+model.nodeId+'&action=open_document&document_identifier='+UTILS.getDocumentID(model.asfDataId),
    key: 'Документ; false'
  };
  var statusBox = model.getModelWithId(statCmp, tableCmp, tableBlockIndex);
  var linkBox = model.getModelWithId(linkCmp, tableCmp, tableBlockIndex);
  statusBox.on("valueChange", function() {
    linkBox.setValue(tmpLink);
  });
}
