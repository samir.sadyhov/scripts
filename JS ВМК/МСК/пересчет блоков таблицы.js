var RECALC_COUNT_ROWS = {

  setings: [],

  recalc: function(view, setings) {
    var model = view.model;
    var tableModel = model.getModelWithId(setings.tableID);
    var tableView = view.getViewWithId(setings.tableID);

    tableModel.on('tableRowAdd', function(evt, modelTab, modelRow) {
      if (setings.numberRowID) {
        tableModel.modelBlocks.forEach(function(modelBlock, index){
          model.getModelWithId(setings.numberRowID, setings.tableID, modelBlock[0].asfProperty.tableBlockIndex).setValue((index+1)+"");
        });
      }
      model.getModelWithId(setings.resultID).setValue(tableView.getBlocksCount() + "");
    });
    tableModel.on('tableRowDelete', function(evt, rowModel, rowIndex, removedRow) {
      if (setings.numberRowID) {
        tableModel.modelBlocks.forEach(function(modelBlock, index){
          model.getModelWithId(setings.numberRowID, setings.tableID, modelBlock[0].asfProperty.tableBlockIndex).setValue((index+1)+"");
        });
      }
      model.getModelWithId(setings.resultID).setValue(tableView.getBlocksCount() + "");
    });
  }

};

RECALC_COUNT_ROWS.setings.push({
  formCode: "Процесс_внесения_данных_по_обращениям_работников",
  tableID: "table02",
  resultID: "count",
  numberRowID: "number"
});

RECALC_COUNT_ROWS.setings.push({
  formCode: "Процесс_внесения_данных_по_вопросам_работников",
  tableID: "table03",
  resultID: "count",
  numberRowID: "number"
});

RECALC_COUNT_ROWS.setings.push({
  formCode: "Оценка_уровня_трудовой_дисциплины_для_КМГ",
  tableID: "discipline",
  resultID: "count",
  numberRowID: null
});

AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  RECALC_COUNT_ROWS.setings.forEach(function(setings){
    if (model.formCode == setings.formCode) RECALC_COUNT_ROWS.recalc(view, setings);
  });
});
