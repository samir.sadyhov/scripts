var UTILS = {
  synergyLogin: AS.OPTIONS.login,
  synergyPass: AS.OPTIONS.password,

  getDocumentID: function(dataUUID) {
    var result = null;
    $.ajax({
      url: 'rest/api/asforms/data/document?dataUUID='+ dataUUID,
      dataType: 'json',
      async: false,
      username: UTILS.synergyLogin,
      password: UTILS.synergyPass,
      success: function(d) {
        result = d[dataUUID].documentID;
      }
    });
    return result;
  },

  msgShow: function(msg, type) {
    var c1 = '#FF9999';
    var c2 = 'rgba(255, 153, 153, 0.7)';
    if (type == 'info') {
      c1 = '#B5D5FF';
      c2 = 'rgba(181, 213, 255, 0.7)';
    }
    var msgBox = $('<div/>');
    msgBox.css({
      'position': 'fixed',
      'left': '50%',
      'margin-right': '-50%',
      'transform': 'translate(-50%)',
      'top': '20px',
      'color': 'black',
      'font-size': '14px',
      'font-weight': 'bold',
      'text-align': 'center',
      'padding': '20px',
      'outline': 'none',
      'border-radius': '3px',
      'background': c1,
      'background-color': c2,
      'display': 'none',
      'z-index': '999'
    });
    $('body').append(msgBox);
    msgBox.html(msg).fadeToggle('normal');
    setTimeout(function() {
      msgBox.fadeToggle('normal');
      setTimeout(function() {
        msgBox.remove();
      }, 1000);
    }, 5000);
  }
};

AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
    console.group(model.formName);
    console.info("%c"+"formCode: ["+model.formCode+"]", "color: white");
    console.info("%c"+"formID: ["+model.formId+"]", "color: white");
    console.info("%c"+"asfDataID: ["+model.asfDataId+"]", "color: white");
    console.groupEnd();
  });
});
