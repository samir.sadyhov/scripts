// version 1

window.onmousemove = function() {
  if (document.getElementById("cmp-4lr0ro") && document.getElementById("table02")) {
    var il = (document.getElementById("table02").rows.length - 2) / 5;
    for (var i = 1; i <= il; i++) {
      var b = "b" + i;
      if (document.getElementById(b)) {
  //      document.getElementById(b).parentNode.removeChild(document.getElementById(b));
        document.getElementById(b).src="";
        var a = document.getElementById("table02").getElementsByTagName('IMG');
        for (var j = a.length - 1; j >= 0; j--) {
          if (a[j].src.indexOf('images/buttons/add.24.gif') !== -1) {
    //        a[j].parentNode.removeChild(a[j]);
            a[j].src="";
          }
        }
      }
    }
  }
}


// version 2

window.onmousemove = function() {
  if (document.getElementById("cmp-4lr0ro") && document.getElementById("table02")) {
    if (document.getElementById("b1")) {
      var a = document.getElementById("table02").getElementsByTagName('IMG');
      for (var j = a.length - 1; j >= 0; j--) {
        if (a[j].src.indexOf('images/buttons/add.24.gif') !== -1 || a[j].src.indexOf('images/buttons/light.gray/remove.png') !== -1) {
          a[j].src = "";
        }
      }
    }
  }
}


// version 3

window.onmousemove = function() {
  if (document.getElementById("cmp-4lr0ro") && document.getElementById("table02") && document.getElementById("b1")) {
    var image = document.getElementById("table02").getElementsByTagName('IMG');
    for (var i = image.length - 1; i >= 0; i--) {
      if (image[i].src.indexOf('add.24.gif') !== -1 || image[i].src.indexOf('remove.png') !== -1) {
        image[i].src = "";
      }
    }
  }
}
