AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  if ([
    'Обращение_по_поводу_проблем-ММГ',
    'Обращение_по_поводу_проблем-МТК',
    'Обращение_по_поводу_проблем-МЭМ',
    'Обращение_по_поводу_проблем-ОКК',
    'Обращение_по_поводу_проблем-ОСК',
    'Обращение_по_поводу_проблем-ОТК'
  ].indexOf(model.formCode) !== -1) {
    setTimeout(function() {
      var tabelView = view.getViewWithId('appeal');
      tabelView.setEnabled(false); // блокируем таблицу
      // скрываем столбцы
      tabelView.setColumnVisible(12, false);
      tabelView.setColumnVisible(13, false);
      tabelView.setColumnVisible(14, false);
    }, 0);
  }
});
