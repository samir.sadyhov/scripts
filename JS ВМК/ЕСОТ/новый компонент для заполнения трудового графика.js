var YEAR_GRAPHIC_UTILS = {
    YEAR_GRAPHIC_CODE : "Годовой_график_new1",
    CICLE_FORM_CODE : "Справочник_циклов",
    NORM_FORM_CODE : "Баланс_рабочего_времени",
    HOLIDAY_FORM_CODE : "Справочник_праздников",
};

YEAR_GRAPHIC_UTILS.getDayModels = function(model, blockNumber, year) {
  var monthDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
  if(year%4 === 0 && year%100 !== 0) {
    monthDays[1] = 29;
  }
  var daysInputs = {};
  for(var m = 1; m<=12; m++) {
    for(var d = 1; d <= monthDays[m-1]; d++) {
      var inputModel = model.getModelWithId(m+"_day_"+d, "table", blockNumber);
      daysInputs[m+"."+d] = inputModel;
    }
  }
  return daysInputs;
};


YEAR_GRAPHIC_UTILS.getAllDayModels = function(model, blockNumber) {
  var dayInputs = [];
  for(var m = 1; m<=12; m++) {
    for(var d = 1; d <= 31; d++) {
      var inputModel = model.getModelWithId(m+"_day_"+d, "table", blockNumber);
      dayInputs.push(inputModel);
    }
  }
  return dayInputs;
};


YEAR_GRAPHIC_UTILS.getClearDayInputs = function(model, blockNumber, year) {
  var daysInputs = YEAR_GRAPHIC_UTILS.getDayModels(model, blockNumber, year);
  Object.keys(daysInputs).forEach(function(dayInputId){
    if (daysInputs[dayInputId].getValue()) daysInputs[dayInputId].setValue("");
  });
  return daysInputs;
};


YEAR_GRAPHIC_UTILS.getDayViews = function(view, blockNumber, year) {
    var monthDays = [31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31];
    if(year%4 === 0 && year%100 !== 0) {
        monthDays[1] = 29;
    }
    var daysInputs = {};
    for(var m = 1; m<=12; m++) {
        for(var d = 1; d <= monthDays[m-1]; d++) {
            var inputModel = view.getViewWithId(m+"_day_"+d, "table", blockNumber);
            inputModel.setValue("");
            daysInputs[m+"."+d] = inputModel;
        }
    }
    return daysInputs;
};

YEAR_GRAPHIC_UTILS.markInputs = function(cycleInfo){
    cycleInfo.cycleInput.container.parent().css("background-color", "#ff99aa");
    cycleInfo.startInput.container.parent().css("background-color", "#ff99aa");
    cycleInfo.finishInput.container.parent().css("background-color", "#ff99aa");
    cycleInfo.periodInput.container.parent().css("background-color", "#ff99aa");
};

YEAR_GRAPHIC_UTILS.unmarkInputs = function(cycleInfo){
    cycleInfo.cycleInput.container.parent().css("background-color", "");
    cycleInfo.startInput.container.parent().css("background-color", "");
    cycleInfo.finishInput.container.parent().css("background-color", "");
    cycleInfo.periodInput.container.parent().css("background-color", "");
};

YEAR_GRAPHIC_UTILS.getYear = function(playerModel) {
    return 2014+parseInt(playerModel.getModelWithId("year").getValue());
};

YEAR_GRAPHIC_UTILS.parseHourValue = function(value) {
    if(value === null) {
        return [0, 0];
    }

    var index = (value+"").indexOf("/");
    if(index != -1) {
        var dayH = parseFloat(value.substring(0, index));
        var nightH = parseFloat(value.substring(index+1));
        if(isNaN(dayH)) {
            dayH = 0;
        }
        if(isNaN(nightH)) {
            nightH = 0;
        }
        return [dayH, nightH];
    } else {
        var all = parseInt(value);
        if (isNaN(all)) {
            return [0, 0];
        } else {
            return [all, 0];
        }
    }
};

YEAR_GRAPHIC_UTILS.isHoliday = function(year, month, day) {
    return YEAR_GRAPHIC_UTILS.HOLIDAYS[year].indexOf((month+1)+"_"+day) > -1;
};


YEAR_GRAPHIC_UTILS.checkEnteredData = function(model, targetModel, blockNumber, playerModel, playerView, easy) {

    if(playerModel.loading) {
        return;
    }

    var data = [];
    var targetId = "";
    var targetRow = 0;
    if(targetModel){
      targetId = targetModel.asfProperty.id;
      targetRow=1;
    }

    for(var i=1; i<=24; i++) {

        if(targetId.indexOf(i) > 0) {
            targetRow = i;
        }

        var cycleModel = model.getModelWithId("link"+i, "table", blockNumber);
        var startModel = model.getModelWithId("graficDay"+i, "table", blockNumber);
        var finishModel = model.getModelWithId("graficDayFinish"+i, "table", blockNumber);
        var periodModel = model.getModelWithId("dayNumber"+i, "table", blockNumber);

        var cycleInput = playerView.getViewWithId("link"+i, "table", blockNumber);
        var startInput = playerView.getViewWithId("graficDay"+i, "table", blockNumber);
        var finishInput = playerView.getViewWithId("graficDayFinish"+i, "table", blockNumber);
        var periodInput = playerView.getViewWithId("dayNumber"+i, "table", blockNumber);


        var period = parseInt(_.first(periodModel.getValue()));
        if(isNaN(period)) {
            period = null;
        }

        if(periodModel.listElements.length > 0) {
            if(!periodModel.originalElements) {
                periodModel.originalElements = periodModel.listElements;
            }
            if (cycleModel.getValue()) {
                var cycleValue = YEAR_GRAPHIC_UTILS.CYCLES[cycleModel.getValue()].cycles;
                periodModel.listElements = [];
                periodModel.originalElements.forEach(function(oElement, index){
                    if(index < cycleValue.size) {
                        periodModel.listElements.push(oElement);
                    }
                });

                periodModel.listCurrentElements = periodModel.listElements;
                periodModel.trigger("dataLoad");

                if(period > cycleValue.size) {
                    // console.log(_.first(periodModel.listElements));
                    periodModel.setValue(_.first(periodModel.listElements).value);
                    period=1;
                }
            } else {
                periodModel.listElements = periodModel.originalElements;
                periodModel.listCurrentElements = periodModel.listElements;
                periodModel.trigger("dataLoad");
            }

        }

        var cycleInfo = {number : i,
            cycleId : cycleModel.getValue(),
            startDate: AS.FORMS.DateUtils.parseDate(startModel.getValue()),
            finishDate : AS.FORMS.DateUtils.parseDate(finishModel.getValue()),
            period : period,
            cycleInput : cycleInput,
            startInput: startInput,
            finishInput: finishInput,
            periodInput: periodInput };

        data.push(cycleInfo);

        YEAR_GRAPHIC_UTILS.unmarkInputs(cycleInfo);
    }

    var result = [];
    var message = "";

    data.forEach(function(d, index){
      var ignore = false;
      if(d.cycleId === null || d.startDate === null || d.finishDate === null) {
        return;
      }

      if(d.startDate.getTime() > d.finishDate.getTime()) {
        if(d.number == targetRow) {
          message += "Дата начала в цикле " + d.number + " меньше даты завершения. Цикл игнорируется.\n";
        }
        YEAR_GRAPHIC_UTILS.markInputs(d);
        return;
      }

      result.some(function(r, index){
        if(r.startDate.getTime() <=d.finishDate.getTime() &&  r.finishDate.getTime() >= d.startDate.getTime()) {
          if(d.number == targetRow || r.number == targetRow) {
            message += "Цикл " + d.number + " пересекается по датам с циклом " + r.number + ". Игнорируется\n";
          }
          YEAR_GRAPHIC_UTILS.markInputs(d);
          ignore = true;
          return true;
        }
      });

      if(!ignore) {
        result.push(d);
      }

    });


    if(!message.isEmpty()) {
        alert(message);
    }

    var year = YEAR_GRAPHIC_UTILS.getYear(playerModel);

    var targetInputs = null;
    if(!easy) {
      targetInputs = YEAR_GRAPHIC_UTILS.getClearDayInputs(model, blockNumber, year);
    } else {
      targetInputs = YEAR_GRAPHIC_UTILS.getDayModels(model, blockNumber, year);
    }

    var workDays = [];
    var dayOff = [];
    var workHours = [];
    var normHours = [];
    var nightHours = [];
    var holidayHours = [];

    var hours = playerModel.getModelWithId("week", 'table', blockNumber).getValue()[0];
    if(hours == 1) {
        hours = 40;
    } else if (hours == 2) {
        hours = 36;
    } else {
        hours = 40;
    }


    var days = playerModel.getModelWithId("day", 'table', blockNumber).getValue()[0];
    if(days == '1') {
        days = 5;
    } else if (days == '2') {
        days = 6;
    } else {
        days = 5;
    }


    var norm = YEAR_GRAPHIC_UTILS.NORMS[parseInt(year)];

    var workDaysAll = 0;
    var dayOffAll = 0;
    var workHoursAll = 0;
    var normHoursAll = 0;
    if(norm) {
        normHoursAll = norm["n" + hours + "_" + days+"_sum"];
    }
    var nightHoursAll = 0;
    var holidayHoursAll = 0;

    for(i=0; i<12; i++) {
        workDays.push(0);
        if(norm) {
            var normValue = norm["n" + hours + "_" + days][(i + 1)].replace(/,/g, '.');
            var nh = parseFloat(normValue);
            normHours.push(nh);
        } else {
            normHours.push(0);
        }
        dayOff.push(0);
        workHours.push(0);
        nightHours.push(0);
        holidayHours.push(0);
    }

    if(!easy) {
      result.forEach(function(res){
        var startDate = new Date(year, res.startDate.getMonth(), res.startDate.getDate());
        var finishDate = new Date(year, res.finishDate.getMonth(), res.finishDate.getDate());

        var cycle = YEAR_GRAPHIC_UTILS.CYCLES[res.cycleId].cycles;
        var holidays = YEAR_GRAPHIC_UTILS.CYCLES[res.cycleId].holidays;
        var cyclePosition = res.period;

        while(startDate.getTime() <= finishDate.getTime()) {
          var id = (startDate.getMonth()+1)+"."+startDate.getDate();
          var value = cycle[cyclePosition];

          if(holidays && YEAR_GRAPHIC_UTILS.isHoliday(year, startDate.getMonth(), startDate.getDate())) {
            value = "В";
          }
          targetInputs[id].setValue(value);

          cyclePosition++;
          if(cyclePosition > cycle.size) {
            cyclePosition = 1;
          }
          startDate = new Date(year, startDate.getMonth(), startDate.getDate()+1);
        }
      });
    }

    if(easy) {
      for(i=0; i<12; i++) {
        for(var j=1; j<=31; j++) {
          var id = (i+1)+"."+j;
          var input = targetInputs[id];
          if(!input) {
            continue;
          }
          var value = input.getValue();
          var dayHours = YEAR_GRAPHIC_UTILS.parseHourValue(value);
          if(dayHours[0] === 0 && dayHours[1] === 0 ) {
            dayOff[i] =  dayOff[i]+1;
            dayOffAll++;
          } else {
            workDaysAll++;
            workHoursAll+=dayHours[0];
            nightHoursAll+=dayHours[1];
            workDays[i] =  workDays[i]+1;
            workHours[i] =  workHours[i]+dayHours[0];
            nightHours[i] =  nightHours[i]+dayHours[1];
            if(YEAR_GRAPHIC_UTILS.isHoliday(year, i, j)) {
              holidayHours[i] =  holidayHours[i]+dayHours[0];
              holidayHoursAll+=dayHours[0];
            }
          }
        }
        playerModel.getModelWithId((i+1)+"_work_day", "table", blockNumber).setValue(workDays[i]+"");
        playerModel.getModelWithId((i+1)+"_holiday", "table", blockNumber).setValue(dayOff[i]+"");
        playerModel.getModelWithId((i+1)+"_norm_hour", "table", blockNumber).setValue(normHours[i]+"");
        playerModel.getModelWithId((i+1)+"_work_hour", "table", blockNumber).setValue(workHours[i]+"");
        playerModel.getModelWithId((i+1)+"_overtime", "table", blockNumber).setValue((workHours[i]-normHours[i])+"");
        playerModel.getModelWithId((i+1)+"_night_hour", "table", blockNumber).setValue(nightHours[i]+"");
        playerModel.getModelWithId((i+1)+"_festival", "table", blockNumber).setValue(holidayHours[i]+"");
      }

      playerModel.getModelWithId("work_day_all", "table", blockNumber).setValue(workDaysAll+"");
      playerModel.getModelWithId("holiday_all", "table", blockNumber).setValue(dayOffAll+"");
      playerModel.getModelWithId("norm_hour_all", "table", blockNumber).setValue(normHoursAll+"");
      playerModel.getModelWithId("work_hour_all", "table", blockNumber).setValue(workHoursAll+"");
      playerModel.getModelWithId("overtime_all", "table", blockNumber).setValue((workHoursAll-normHoursAll)+"");
      playerModel.getModelWithId("night_hour_all", "table", blockNumber).setValue(nightHoursAll+"");
      playerModel.getModelWithId("festival_all", "table", blockNumber).setValue(holidayHoursAll+"");
    }

};


YEAR_GRAPHIC_UTILS.addHandlerToModelBlock = function(model, playerModel, playerView, blockNumber, modelRow) {

    var mainWatchModel = model.getModelWithId("watch", "table", blockNumber);
    mainWatchModel.on("valueChange", function(){
        for(var i=1; i<=12; i++) {
            model.getModelWithId(i+"_watch", "table", blockNumber).setValue(mainWatchModel.getValue());
        }
    });

    var mainChangeModel = model.getModelWithId("change", "table", blockNumber);
    mainChangeModel.on("valueChange", function(){
        for(var i=1; i<=12; i++) {
            model.getModelWithId(i+"_change", "table", blockNumber).setValue(mainChangeModel.getValue());
        }
    });

    var mainWeekModel = model.getModelWithId("week", "table", blockNumber);
    mainWeekModel.on("valueChange", function(){
        YEAR_GRAPHIC_UTILS.checkEnteredData(model, null, blockNumber, playerModel, playerView);
    });

    var mainDayModel = model.getModelWithId("day", "table", blockNumber);
    mainDayModel.on("valueChange", function(){
        YEAR_GRAPHIC_UTILS.checkEnteredData(model, null, blockNumber, playerModel, playerView);
    });



    for(var i=1; i<=24; i++) {
        var cycleModel = model.getModelWithId("link" + i, "table", blockNumber);
        var startModel = model.getModelWithId("graficDay" + i, "table", blockNumber);
        var finishModel = model.getModelWithId("graficDayFinish" + i, "table", blockNumber);
        var periodModel = model.getModelWithId("dayNumber" + i, "table", blockNumber);


        cycleModel.on("valueChange", function(evt, targetModel){
          YEAR_GRAPHIC_UTILS.checkEnteredData(model, targetModel, blockNumber, playerModel, playerView);
        });

        startModel.on("valueChange", function(evt, targetModel){
          YEAR_GRAPHIC_UTILS.checkEnteredData(model, targetModel, blockNumber, playerModel, playerView);
        });

        finishModel.on("valueChange", function(evt, targetModel){
          YEAR_GRAPHIC_UTILS.checkEnteredData(model, targetModel, blockNumber, playerModel, playerView);
        });

        periodModel.on("valueChange", function(evt, targetModel){
            YEAR_GRAPHIC_UTILS.checkEnteredData(model, targetModel, blockNumber, playerModel, playerView);
        });
    }

    var targetModels = YEAR_GRAPHIC_UTILS.getAllDayModels(model, blockNumber);
    targetModels.forEach(function(targetModel){
        targetModel.on("valueChange", function(){
            var year = YEAR_GRAPHIC_UTILS.getYear(playerModel);
            var value = targetModel.getValue();
            var id = targetModel.asfProperty.id;
            var month = parseInt(id.substring(0, id.indexOf("_")));
            var day = parseInt(id.substring(id.indexOf("day_")+4));
            if(YEAR_GRAPHIC_UTILS.isHoliday(year, month-1, day)) {
                playerView.getViewWithId(id, 'table', blockNumber).container.parent().css("background-color", "#ff0000");
            } else if("В" == value )  {
                playerView.getViewWithId(id, 'table', blockNumber).container.parent().css("background-color", "#ffff00");
            } else {
                playerView.getViewWithId(id, 'table', blockNumber).container.parent().css("background-color", "");
            }

            YEAR_GRAPHIC_UTILS.checkEnteredData(model, targetModel, blockNumber, playerModel, playerView, true);
        });

    });


};


YEAR_GRAPHIC_UTILS.addShowHideButton = function(model, playerModel, playerView, blockNumber, modelRow) {
    setTimeout(function(){
        var button = playerView.getViewWithId("button", "table", blockNumber);
        if(!button) {
            console.log("not button :( "+blockNumber);
            return;
        }

        var showCycles = true;
        var hideShowCycles = jQuery("<button>");
        hideShowCycles.css({"background": "#67ae90", "border": "1px solid #349067"});

        hideShowCycles.html("Скрыть");
        button.container.append(hideShowCycles);
        hideShowCycles.click(function(){
            var realRowNumber = model.modelBlocks.indexOf(modelRow)*41 + 1;
            var rows = playerView.getViewWithId("table").container.children("table").first().children("tbody").first().children("tr");

            var emptyRows = [];
            var result = [];
            var data = [];

            for(var i=1; i<=24; i++) {
                var cycleModel = model.getModelWithId("link"+i, "table", blockNumber);
                var startModel = model.getModelWithId("graficDay"+i, "table", blockNumber);
                var finishModel = model.getModelWithId("graficDayFinish"+i, "table", blockNumber);
                var periodModel = model.getModelWithId("dayNumber"+i, "table", blockNumber);

                var cycleInput = playerView.getViewWithId("link"+i, "table", blockNumber);
                var startInput = playerView.getViewWithId("graficDay"+i, "table", blockNumber);
                var finishInput = playerView.getViewWithId("graficDayFinish"+i, "table", blockNumber);
                var periodInput = playerView.getViewWithId("dayNumber"+i, "table", blockNumber);

                var period = parseInt(_.first(periodModel.getValue()));
                if(isNaN(period)) {
                    period = null;
                }
                var cycleInfo = {number : i,
                    cycleId : cycleModel.getValue(),
                    startDate: AS.FORMS.DateUtils.parseDate(startModel.getValue()),
                    finishDate : AS.FORMS.DateUtils.parseDate(finishModel.getValue()),
                    period : period,
                    cycleInput : cycleInput,
                    startInput: startInput,
                    finishInput: finishInput,
                    periodInput: periodInput };

                data.push(cycleInfo);
            }

            data.forEach(function(d, index){
                var ignore = false;
                if(d.cycleId === null || d.startDate === null || d.finishDate === null) {
                    emptyRows.push(index);
                    return;
                }

                if(d.startDate.getTime() > d.finishDate.getTime()) {
                    emptyRows.push(index);
                    return;
                }

                result.some(function(r){
                    if(r.startDate.getTime() <=d.finishDate.getTime() &&  r.finishDate.getTime() >= d.startDate.getTime()) {
                        emptyRows.push(index);
                        ignore = true;
                        return true;
                    }
                });
                if(!ignore) {
                    result.push(d);
                }
            });

            //console.log(emptyRows);
            if(showCycles){
                emptyRows.forEach(function (rowIndex){
                    console.log(rows[rowIndex+realRowNumber]);
                    jQuery(rows[rowIndex+realRowNumber]).hide();
                });

            } else {
                for(var i=realRowNumber; i<realRowNumber+24; i++) {
                    jQuery(rows[i]).show();
                }
            }


            if(showCycles) {
                hideShowCycles.html("Развернуть");
            } else {
                hideShowCycles.html("Скрыть");
            }
            showCycles = !showCycles;
        });


        var targetModels = YEAR_GRAPHIC_UTILS.getAllDayModels(model, blockNumber);
        targetModels.forEach(function(targetModel){
            var year = YEAR_GRAPHIC_UTILS.getYear(playerModel);
            var value = targetModel.getValue();
            var id = targetModel.asfProperty.id;
            var month = parseInt(id.substring(0, id.indexOf("_")));
            var day = parseInt(id.substring(id.indexOf("day_")+4));
            if(YEAR_GRAPHIC_UTILS.isHoliday(year, month-1, day)) {
                playerView.getViewWithId(id, 'table', blockNumber).container.parent().css("background-color", "#ff0000");
            } else if("В" == value )  {
                playerView.getViewWithId(id, 'table', blockNumber).container.parent().css("background-color", "#ffff00");
            } else {
                playerView.getViewWithId(id, 'table', blockNumber).container.parent().css("background-color", "");
            }

        });

        hideShowCycles.click();

    }, 0);
};


YEAR_GRAPHIC_UTILS.addFormShowListener  = function(playerModel, playerView){
    var mainTableModel = playerModel.getModelWithId('table');
    var mainTableView = playerView.getViewWithId('table');

    if(!playerModel.inited) {
        mainTableModel.on("tableRowAdd", function(evt, model, modelRow){
            YEAR_GRAPHIC_UTILS.addHandlerToModelBlock(model, playerModel, playerView, modelRow[0].asfProperty.tableBlockIndex, modelRow);
            YEAR_GRAPHIC_UTILS.addShowHideButton(model, playerModel, playerView, modelRow[0].asfProperty.tableBlockIndex, modelRow);
        });

        mainTableModel.modelBlocks.forEach(function(modelBlock){
            YEAR_GRAPHIC_UTILS.addHandlerToModelBlock(mainTableModel, playerModel, playerView, modelBlock[0].asfProperty.tableBlockIndex, modelBlock);
        });

        playerModel.inited = true;
    }

    mainTableModel.modelBlocks.forEach(function(modelBlock){
        YEAR_GRAPHIC_UTILS.addShowHideButton(mainTableModel, playerModel, playerView, modelBlock[0].asfProperty.tableBlockIndex, modelBlock);
    });
};


YEAR_GRAPHIC_UTILS.CYCLES = {};
YEAR_GRAPHIC_UTILS.HOLIDAYS = {};
YEAR_GRAPHIC_UTILS.NORMS = {};

YEAR_GRAPHIC_UTILS.getAsfDataWithId = function(id, asfData) {
    for(var i=0; i<asfData.data.length; i++){
        if(asfData.data[i].id == id) {
            return asfData.data[i];
        }
    }
};

YEAR_GRAPHIC_UTILS.extractHolidays = function(data) {
    var holidays = [];
    data.forEach(function (d) {
        if (d.id !== 'holidays') {
            return;
        }
        var blocksNumbers = AS.FORMS.TableUtils.extractBlockNumbers(d);
        holidays.size = blocksNumbers.length;
        blocksNumbers.forEach(function (blockNumber, index) {
            var day = YEAR_GRAPHIC_UTILS.getAsfDataWithId("dayHoliday-b" + blockNumber, d).value;
            var month = YEAR_GRAPHIC_UTILS.getAsfDataWithId("monthHoliday-b" + blockNumber, d).key;
            if (day !== undefined) {
                holidays.push(month+"_"+day);
            }
        });
    });
    return holidays;
};

YEAR_GRAPHIC_UTILS.extractCycles = function(data) {
    var workHours = {};
    data.forEach(function (d) {
        if (d.id !== 'table') {
            return;
        }
        var blocksNumbers = AS.FORMS.TableUtils.extractBlockNumbers(d);
        workHours.size = blocksNumbers.length;
        blocksNumbers.forEach(function (blockNumber, index) {
            var day = YEAR_GRAPHIC_UTILS.getAsfDataWithId("day-b" + blockNumber, d).value;
            var hour = YEAR_GRAPHIC_UTILS.getAsfDataWithId("hour-b" + blockNumber, d).value;
            if (day !== undefined) {
                workHours[day] = hour;
            }
        });
    });
    // console.log(workHours);
    return workHours;
};

YEAR_GRAPHIC_UTILS.extractNorms = function(data) {
    var n40_5 = {};
    var n40_6 = {};
    var n36_5 = {};
    var n36_6 = {};

    for(var i=1; i<=12; i++) {
        n40_5[i] = YEAR_GRAPHIC_UTILS.getAsfDataWithId("40_5_"+i, data).key;
        n40_6[i] = YEAR_GRAPHIC_UTILS.getAsfDataWithId("40_6_"+i, data).key;
        n36_5[i] = YEAR_GRAPHIC_UTILS.getAsfDataWithId("36_5_"+i, data).key;
        n36_6[i] = YEAR_GRAPHIC_UTILS.getAsfDataWithId("36_6_"+i, data).key;
    }

    var n40_5_sum = YEAR_GRAPHIC_UTILS.getAsfDataWithId("40_5_sum", data).key;
    var n40_6_sum = YEAR_GRAPHIC_UTILS.getAsfDataWithId("40_6_sum", data).key;
    var n36_5_sum = YEAR_GRAPHIC_UTILS.getAsfDataWithId("36_5_sum", data).key;
    var n36_6_sum = YEAR_GRAPHIC_UTILS.getAsfDataWithId("36_6_sum", data).key;



    return {n40_5: n40_5, n40_6: n40_6, n36_5:n36_5, n36_6: n36_6, n40_5_sum : n40_5_sum, n40_6_sum: n40_6_sum, n36_5_sum: n36_5_sum, n36_6_sum: n36_6_sum };
};

AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
    if (model.formCode == YEAR_GRAPHIC_UTILS.YEAR_GRAPHIC_CODE) {
        if(!model.inited) {

            AS.FORMS.ApiUtils.simpleAsyncGet("rest/api/asforms/search?formCode="+encodeURIComponent(YEAR_GRAPHIC_UTILS.NORM_FORM_CODE)+"&getDocIds=true&searchInRegistry=true", function (data) {
                var datas = "";
                data.forEach(function (d, index) {
                    datas += "&dataUUID=" + d.dataUUID;
                });
                AS.FORMS.ApiUtils.simpleAsyncGet("rest/api/asforms/data/get?" + datas, function (asfDatas) {
                    YEAR_GRAPHIC_UTILS.NORMS = {};
                    asfDatas.forEach(function(asfData){
                        var yearData = YEAR_GRAPHIC_UTILS.getAsfDataWithId("year", asfData);
                        if(yearData) {
                            var year = YEAR_GRAPHIC_UTILS.getAsfDataWithId("year", asfData).value;
                            YEAR_GRAPHIC_UTILS.NORMS[year] = YEAR_GRAPHIC_UTILS.extractNorms(asfData);
                        }
                    });
                    console.log("NORMS", YEAR_GRAPHIC_UTILS.NORMS);
                });
            });

            AS.FORMS.ApiUtils.simpleAsyncGet("rest/api/asforms/search?formCode="+encodeURIComponent(YEAR_GRAPHIC_UTILS.HOLIDAY_FORM_CODE)+"&getDocIds=true&searchInRegistry=true", function (data) {
                var datas = "";
                data.forEach(function (d, index) {
                    datas += "&dataUUID=" + d.dataUUID;
                });
                AS.FORMS.ApiUtils.simpleAsyncGet("rest/api/asforms/data/get?" + datas, function (asfDatas) {
                    YEAR_GRAPHIC_UTILS.HOLIDAYS = {};
                    asfDatas.forEach(function(asfData){
                        var yearData = YEAR_GRAPHIC_UTILS.getAsfDataWithId("year", asfData);
                        if(yearData) {
                            var year = YEAR_GRAPHIC_UTILS.getAsfDataWithId("year", asfData).value;
                            YEAR_GRAPHIC_UTILS.HOLIDAYS[year] = YEAR_GRAPHIC_UTILS.extractHolidays(asfData.data);
                        }
                    });
                    console.log("holidays", YEAR_GRAPHIC_UTILS.HOLIDAYS);


                    YEAR_GRAPHIC_UTILS.addFormShowListener(model, view);
                });
            });


            AS.FORMS.ApiUtils.simpleAsyncGet("rest/api/asforms/search?formCode="+encodeURIComponent(YEAR_GRAPHIC_UTILS.CICLE_FORM_CODE)+"&getDocIds=true", function (data) {
                var datas = "";
                var docsId = {};
                data.forEach(function (d, index) {
                    datas += "&dataUUID=" + d.dataUUID;
                    docsId[d.dataUUID] = d.documentID;
                });
                AS.FORMS.ApiUtils.simpleAsyncGet("rest/api/asforms/data/get?" + datas, function (asfDatas) {
                    YEAR_GRAPHIC_UTILS.CYCLES = {};
                    asfDatas.forEach(function (asfData, index) {
                        var festival = YEAR_GRAPHIC_UTILS.getAsfDataWithId("festival", asfData);
                        var cycle = {
                            asfDataUUID: asfData.uuid,
                            documentID: docsId[asfData.uuid],
                            holidays: festival ? festival.value == 1 : false,
                            cycles: YEAR_GRAPHIC_UTILS.extractCycles(asfData.data)
                        };
                        YEAR_GRAPHIC_UTILS.CYCLES[cycle.documentID] = cycle;
                    });
                    // console.log(YEAR_GRAPHIC_UTILS.CYCLES);
                });
            });

        } else {
            YEAR_GRAPHIC_UTILS.addFormShowListener(model, view);
        }


    }
});
