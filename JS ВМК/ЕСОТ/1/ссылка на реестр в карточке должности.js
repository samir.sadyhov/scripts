var changedButton = $('<button/>').html('Изменить карточки');
changedButton.css({
  'position': 'absolute',
  'bottom': '3px',
  'right': '100px',
  'background-color': '#49B785',
  'border': 'none',
  'border-radius': '4px',
  'color': 'black',
  'padding': '4px 15px',
  'font-weight': 'bold',
  'z-index': '999'
});

changedButton.hover(function() {
  $(this).css('background-color', '#53CC97');
}, function() {
  $(this).css('background-color', '#49B785');
});

changedButton.click(function(){
  try {
    changedLinkValue();
  } catch (e) {
    console.log(e);
  }
});

$('body').append(changedButton);


var changedLinkValue = function(){
  AS.SERVICES.showWaitWindow();
  AS.FORMS.ApiUtils.simpleAsyncGet("rest/api/asforms/search?formCode="+encodeURIComponent("Карточка_должности")+"&searchInRegistry=false", function(uuids){
    uuids.forEach(function(uuid, i){
      AS.SERVICES.showWaitWindow();
      AS.FORMS.ApiUtils.simpleAsyncGet("rest/api/asforms/data/"+uuid, function(data){
        var tmpData = data.data;
        var tmpForm = data.form;
        var prov = false;
        data.data.forEach(function(obj, index){
          if (obj.type == 'reglink') {
            if (obj.value && obj.value !== "" && obj.value !== "Ссылка на запись справочника") {
              prov = true;
              tmpData[index].key = obj.value;
              tmpData[index].valueID = obj.value;
              tmpData[index].value = "Ссылка на запись справочника";
              console.log("old object ["+JSON.stringify(obj)+"]");
              console.log("new object ["+JSON.stringify(tmpData[index])+"]");
            }
          }
        });
        if (prov) {
          console.log("data - " + i + "\nasfDataId: " + uuid);
          console.log(tmpData);
          try {
            AS.FORMS.ApiUtils.saveAsfData(tmpData, tmpForm, uuid);
          } catch (e) {
            console.log(i + " ошибка сохранения данных по форме");
            console.log(e);
          }
        }
      });
    });
    AS.SERVICES.hideWaitWindow();
  });
};
