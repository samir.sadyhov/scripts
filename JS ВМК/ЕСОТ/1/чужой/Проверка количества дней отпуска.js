(function() {
    console.log("вмк проверки количества дней загружен");

    var addDayStart = "date_last";
    var addDayEnd = "date_last_po";
    var actualStart = "date_main";

    var vacationForms = [
        {
            code: "Приказ_о_предоставлении_оплачиваемого_отпуска_(без_материальной_помощью)",
            tableId: "t",
            userId: "user",
            positionId: "position",
            startDate: "start_period",
            endDate: "end_period",
            mainDays: "main",
            additionalDays: "additional"
        },
        {
            code: "Приказ_о_предоставлении_оплачиваемого_отпуска_(с_материальной_помощью)",
            tableId: "t",
            userId: "user",
            positionId: "position",
            startDate: "start_period",
            endDate: "end_period",
            mainDays: "main",
            additionalDays: "additional"
        },
        {
            code: "Заявление_о_предоставлении_ежегодного_трудового_отпуска_с_мат.помощью",
            userId: "from_user",
            positionId: "from_position",
            startDate: "start_period",
            endDate: "end_period",
            mainDays: "main",
            additionalDays: "additional"
        },
        {
            code: "Заявление_о_предоставлении_ежегодного_трудового_отпуска_без_выплаты",
            userId: "from_user",
            positionId: "from_position",
            startDate: "start_period",
            endDate: "end_period",
            mainDays: "main",
            additionalDays: "additional"
        }
    ];

    function resetTime(date) {
        date.setHours(0);
        date.setMinutes(0);
        date.setSeconds(0);
        date.setMilliseconds(0);
    }

    function markView(view, valid) {
        if (valid)
            view.unmarkInvalid();
        else if (view !== null && view !== void 0)
            view.markInvalid();
    }

    var periodErrorTimer;

    function showError(message) {
        if (periodErrorTimer)
            clearTimeout(periodErrorTimer);
        periodErrorTimer = setTimeout(function () {
            AS.SERVICES.showErrorMessage(message);
        }, 500);
    }

    function Vacation(userView, startDate, endDate, mainDaysView, addDaysView, addDateStartView, addDateEndView, actualStart) {

        var thiz = this;

        this.userView = userView;
        this.startDate = startDate;
        this.endDate = endDate;
        this.mainDaysView = mainDaysView;
        this.addDaysView = addDaysView;
        this.actualStart = actualStart;

        this.periodValidity = true;

        function getData(id, data, number) {
            if (number !== void 0)
                id += "-b" + number;
            return UTILS.getAsfDataWithId(id, data);
        }

        // function enableAdditionalDates(enabled) {
        //     addDateStartView.setEnabled(enabled);
        //     addDateEndView.setEnabled(enabled);
        // }

        // this.updateADates = function() {
        //     var enableADates = enableAdditionalDates.bind(thiz, true);
        //     var disableADates = enableAdditionalDates.bind(thiz, false);
        //     thiz.chooseValidation(enableADates, enableADates, disableADates);
        // };

        this.markPeriodDates = function(valid) {
            this.periodValidity = valid;

            markView(this.startDate, valid);
            markView(this.endDate, valid);
        };

        this.updateAvailableDays = function() {
            // thiz.updateADates();
            thiz.markPeriodDates(false);

            var vacStart = AS.FORMS.DateUtils.parseDate(this.startDate.model.getValue());
            var vacEnd = AS.FORMS.DateUtils.parseDate(this.endDate.model.getValue());

            if (!vacEnd || !vacStart) {
                this.resetDays();
                return;
            }

            var ids = this.userView.model.getSelectedIds();
            if (!_.isArray(ids) || ids.length !== 1) {
                this.resetDays();
                return;
            }
            var userId = ids[0];
            AS.FORMS.ApiUtils.simpleAsyncGet("rest/api/personalrecord/forms/" + userId, function(forms){
                var restsForm = _.find(forms, function (form) {
                    return form.formCode == 'Отпуска';
                });

                if (restsForm === void 0) thiz.resetDays();

                function loadReserveData() {
                    AS.FORMS.ApiUtils.simpleAsyncGet("rest/api/asforms/data/" + restsForm["data-uuid"], function (data) {
                        var table = getData("reserve_table", data);
                        var blocksNumbers = UTILS.extractBlockNumbers(table);

                        var resBlockNumber = -1;

                        blocksNumbers.forEach(function (blockNumber) {
                            var startStr = getData("period_osn_start", table, blockNumber).key;
                            var endStr = getData("period_osn_finish", table, blockNumber).key;

                            var cmpPeriod = thiz.comparePeriod();
                            if (cmpPeriod !== null && cmpPeriod <= 0) {
                                var usedDays = getData("used_days", table, blockNumber).value;
                                var usedDaysNum = parseInt(usedDays);
                                if (_.isNumber(usedDaysNum) && usedDaysNum < 0) return;
                            }

                            var start = AS.FORMS.DateUtils.parseDate(startStr);
                            var end = AS.FORMS.DateUtils.parseDate(endStr);
                            resetTime(start);
                            resetTime(end);

                            if (start && end &&
                                vacStart && vacEnd &&
                                start.getTime() == vacStart.getTime() &&
                                end.getTime() == vacEnd.getTime()) {
                                resBlockNumber = blockNumber;
                            }
                        });

                        if (resBlockNumber === -1) {
                            thiz.resetDays();
                            thiz.markPeriodDates(false);
                            return;
                        }
                        thiz.markPeriodDates(true);

                        thiz.resMainDays = parseInt(getData("reserv_osn_days", table, resBlockNumber).value);
                        thiz.resAddDays = parseInt(getData("reserv_dop_days", table, resBlockNumber).value);
                        thiz.mainDays = parseInt(getData("osn_days", table, resBlockNumber).value);
                        thiz.factOsnDays = parseInt(getData("fact_osn_days", table, resBlockNumber).value);
                        thiz.factDopDays = parseInt(getData("fact_dop_days", table, resBlockNumber).value);

                        // thiz.validateDays();
                    });
                }

                var cmpPeriod = thiz.comparePeriod();
                if (cmpPeriod !== null && cmpPeriod === 0) {
                    var mainStart = AS.FORMS.DateUtils.parseDate(thiz.actualStart.model.getValue());
                    var mainStartStr = AS.FORMS.DateUtils.formatDate(mainStart, "${yyyy}-${mm}-${dd}");
                    var url = window.location.origin+"/kmg/rest/api/kmg/countFactDays?userID="+userId+"&date="+mainStartStr+"";
                    var p = jQuery.ajax({url : url, beforeSend : AS.FORMS.ApiUtils.addAuthHeader});

                    jQuery.when(p).then(function(){
                        loadReserveData();
                    });
                } else {
                    loadReserveData();
                }
            });
        };

        this.resetDays = function() {
            this.resMainDays = void 0;
            this.resAddDays = void 0;
            this.mainDays = void 0;
            this.factOsnDays = void 0;
            this.factDopDays = void 0;

            // this.validateDays();
        };

        // this.validateDays = function () {
        //     this.validateMainDay();
        //     this.validateAddDay();
        // };

        this.validateDay = function(dayView, getAvailable, errorMsg) {
            var dayModel = dayView.model;

            var days = parseInt(dayModel.getValue());
            var available = getAvailable();

            var isValid = true;

            if (days && days > 0 && _.isNumber(available) && days > available) {
                markView(dayView, false);
                AS.SERVICES.showErrorMessage(errorMsg(available));
                isValid = false;
            }

            markView(dayView, isValid);
            return isValid;
        };

        this.comparePeriod = function() {
            var start = AS.FORMS.DateUtils.parseDate(this.startDate.model.getValue());
            var end = AS.FORMS.DateUtils.parseDate((this.endDate.model.getValue()));

            if (!start || !end)
                return null;

            start = start.getTime();
            end = end.getTime();

            var now = Date.now();
            if (now > end && now > start) {
              return -1;
            } else if (now < start && now < end) {
              return 1;
            } else if (now > start && now < end) {
              console.log("this.comparePeriod > else if (now > start && now < end) > (возвращаем 1 хахахаха)");
              return 1;
              // return 0;
            }
            return null;
        };

        // this.chooseValidation = function (before, inside, after) {
        //     var cmp = thiz.comparePeriod();
        //     if (cmp === null)
        //         return true;
        //
        //     if (cmp === -1 && before)
        //         return before();
        //     if (cmp === 1 && after)
        //         return after();
        //     if (cmp === 0 && inside)
        //         return inside();
        //
        //     return true;
        // };

        // this.validateMainDay = function() {
        //     return thiz.chooseValidation(
        //         function() {
        //             return thiz.validateDay(thiz.mainDaysView,
        //                 function () {
        //                     return thiz.resMainDays;
        //                 },
        //                 function(days) {
        //                     return "В выбранном периоде у сотрудника резерв основных дней составляет " + days + " дней";
        //                 });
        //         },
        //         function() {
        //             return thiz.validateDay(thiz.mainDaysView,
        //                 function() {
        //                     return thiz.factOsnDays;
        //                 },
        //                 function(days) {
        //                     return "В выбранном периоде у сотрудника резерв основных дней составляет " + days + " дней";
        //                 }
        //             );
        //
        //         },
        //         function() {
        //             return thiz.validateDay(thiz.mainDaysView,
        //                 function () {
        //                     return thiz.mainDays;
        //                 },
        //                 function (days) {
        //                     return "В выбранном периоде у сотрудника резерв основных дней составляет " + days + " дней";
        //                 });
        //         }
        //     );
        // };
        //
        // this.validateAddDay = function() {
        //     return thiz.chooseValidation(
        //         function () {
        //             return thiz.validateDay(thiz.addDaysView,
        //                 function () {
        //                     return thiz.resAddDays;
        //                 },
        //                 function (days) {
        //                     return "В выбранном периоде у сотрудника резерв дополнительных дней составляет " + days + " дней";
        //                 });
        //         },
        //         function() {
        //             return thiz.validateDay(thiz.addDaysView,
        //                 function() {
        //                     return thiz.factDopDays;
        //                 },
        //                 function(days) {
        //                     return "В выбранном периоде у сотрудника резерв дополнительных дней составляет " + days + " дней";
        //                 }
        //             );
        //
        //         }
        //     );
        // };

        this.changeErrors = function(dayView, validateDay) {
            var dayModel = dayView.model;

            var oldErrorsF = dayModel.getSpecialErrors;
            var errorsWrapper = function() {
                var errors = [];
                if (_.isFunction(oldErrorsF) && errorsWrapper !== oldErrorsF) {
                    var otherErrors = oldErrorsF.call(dayModel);
                    if (_.isArray(otherErrors)) {
                        errors = errors.concat(otherErrors);
                    } else if (otherErrors) {
                        errors.push(otherErrors);
                    }
                }

                var isValid = validateDay();
                if (!isValid) {
                    errors.push("invalid number of vacation days");
                }

                return errors;
            };
            dayModel.getSpecialErrors = errorsWrapper;
        };


        this.addHandlers = function() {
            this.startDate.model.on("valueChange", this.updateAvailableDays.bind(this));
            this.endDate.model.on("valueChange", this.updateAvailableDays.bind(this));

            this.mainDaysView.model.on("valueChange", this.updateAvailableDays.bind(this));
            // this.addDaysView.model.on("valueChange", this.validateAddDay.bind(this));

            // this.changeErrors(this.mainDaysView, this.validateMainDay.bind(this));
            // this.changeErrors(this.addDaysView, this.validateAddDay.bind(this));

            function validatePeriod() {
                if (thiz.periodValidity) {
                    thiz.markPeriodDates(true);
                    return true;
                }
                showError("Введенного периода в резерве нет");
                thiz.markPeriodDates(false);
                return false;
            }

            this.changeErrors(this.startDate, validatePeriod);
            this.changeErrors(this.endDate, validatePeriod);
        };


        this.addHandlers();
        this.updateAvailableDays();
    }

    function VacationForm(form, view, model) {
        var thiz = this;

        var getViewWithId = function(cmpId, blockIndex) {
            var result = null;
            thiz.tableView.getViewBlocks().forEach(function(viewBlock){
                viewBlock.views.forEach(function(view){
                    if(view.model.asfProperty.id == cmpId && view.model.asfProperty.tableBlockIndex == blockIndex) {
                        result = view;
                    }
                });
            });
            return result;
        };

        this.addModelBlock = function(modelBlock) {
            var index = modelBlock.tableBlockIndex;

            new Vacation(getViewWithId(form.userId, index),
                getViewWithId(form.startDate, index),
                getViewWithId(form.endDate, index),
                getViewWithId(form.mainDays, index),
                getViewWithId(form.additionalDays, index),
                getViewWithId(addDayStart, index),
                getViewWithId(addDayEnd, index),
                getViewWithId(actualStart, index)
            );
        };

        this.addTableHandlers = function () {
            this.tableModel.on("tableRowAdd", function (evt, _, modelBlock) {
                thiz.addModelBlock(modelBlock);
            });

            this.tableModel.modelBlocks.forEach(function (modelBlock) {
                thiz.addModelBlock(modelBlock);
            });
        };

        this.form = form;
        this.view = view;
        this.model = model;

        if (!view.editable)
            return;
        if (form.tableId) {
            this.tableModel = model.getModelWithId(form.tableId);
            this.tableView = view.getViewWithId(form.tableId);
            this.addTableHandlers();
        } else {
            new Vacation(view.getViewWithId(form.userId),
                view.getViewWithId(form.startDate),
                view.getViewWithId(form.endDate),
                view.getViewWithId(form.mainDays),
                view.getViewWithId(form.additionalDays),
                view.getViewWithId(addDayStart),
                view.getViewWithId(addDayEnd),
                view.getViewWithId(actualStart)
            );
        }
    }

    AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
        var formData = _.find(vacationForms, function (form) {
            return form.code == model.formCode;
        });
        if (!formData)
            return;
        new VacationForm(formData, view, model);
    });
})();
