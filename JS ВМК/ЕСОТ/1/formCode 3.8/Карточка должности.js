AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  if (model.formCode == "Карточка_должности") {

    model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
      console.log(model.formName + '\nformCode: [' + model.formCode + ']\nformID: [' + model.formId +']\nasfDataID: [' + model.asfDataId+']');
    });

    var componentSluj = ['link_official', 'personal_type_official', 'rank_official', 'category_official', 'group_official', 'grade_official', 'additionalLevel'];
    var componentRab = ['link_work', 'sphere_work', 'discharge_work', 'category_work', 'grade_work', 'personal_type_work'];
    var cmpRabTransport = ['cmp-jgyspi', 'note', 'car_model', 'car_type', 'tech_param'];

    var checkRS = model.getModelWithId("type");
    var checkRabSphera = model.getModelWithId("sphere_work");

    if (checkRS.value[0] == "1") {
      componentSluj.forEach(function(cmp){
        view.getViewWithId(cmp).container.parent().parent().hide();
      });
      componentRab.forEach(function(cmp){
        view.getViewWithId(cmp).container.parent().parent().show();
      });
      if (checkRabSphera.value[0] == "транспортная") {
        cmpRabTransport.forEach(function(cmp){
          view.getViewWithId(cmp).container.parent().parent().show();
        });
      }
    } else if (checkRS.value[0] == "2") {
      componentSluj.forEach(function(cmp){
        view.getViewWithId(cmp).container.parent().parent().show();
      });
      componentRab.forEach(function(cmp){
        view.getViewWithId(cmp).container.parent().parent().hide();
      });
      cmpRabTransport.forEach(function(cmp){
        view.getViewWithId(cmp).container.parent().parent().hide();
      });
    } else {
      componentSluj.forEach(function(cmp){
        view.getViewWithId(cmp).container.parent().parent().hide();
      });
      componentRab.forEach(function(cmp){
        view.getViewWithId(cmp).container.parent().parent().hide();
      });
      cmpRabTransport.forEach(function(cmp){
        view.getViewWithId(cmp).container.parent().parent().hide();
      });
    }
    checkRS.on("valueChange", function() {
      if (checkRS.value[0] == "1") {
        componentSluj.forEach(function(cmp){
          view.getViewWithId(cmp).container.parent().parent().hide();
        });
        componentRab.forEach(function(cmp){
          view.getViewWithId(cmp).container.parent().parent().show();
        });
        if (checkRabSphera.value[0] == "транспортная") {
          cmpRabTransport.forEach(function(cmp){
            view.getViewWithId(cmp).container.parent().parent().show();
          });
        }
      } else {
        componentSluj.forEach(function(cmp){
          view.getViewWithId(cmp).container.parent().parent().show();
        });
        componentRab.forEach(function(cmp){
          view.getViewWithId(cmp).container.parent().parent().hide();
        });
        cmpRabTransport.forEach(function(cmp){
          view.getViewWithId(cmp).container.parent().parent().hide();
        });
      }
    });

    if (checkRabSphera.value[0] == "транспортная") {
      cmpRabTransport.forEach(function(cmp){
        view.getViewWithId(cmp).container.parent().parent().show();
      });
    } else {
      cmpRabTransport.forEach(function(cmp){
        view.getViewWithId(cmp).container.parent().parent().hide();
      });
    }
    checkRabSphera.on("valueChange", function() {
      if (checkRabSphera.value[0] == "транспортная") {
        cmpRabTransport.forEach(function(cmp){
          view.getViewWithId(cmp).container.parent().parent().show();
        });
      } else {
        cmpRabTransport.forEach(function(cmp){
          view.getViewWithId(cmp).container.parent().parent().hide();
        });
      }
    });

  }
});
