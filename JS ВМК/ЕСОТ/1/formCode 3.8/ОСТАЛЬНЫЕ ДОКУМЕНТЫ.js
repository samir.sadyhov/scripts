AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {

  if (model.formCode == "Доп_соглашение_к_Трудовому_договору_на_постоянную_основу") {
    var cmpArrDS=[{ru: "doc_num", kz: "doc_num_k"}, {ru: "td_date", kz: "td_date_k"}, {ru: "td_num", kz: "td_num_k"}, {ru: "doc_date", kz: "doc_date_k"}, {ru: "position", kz: "position_k"}, {ru: "user", kz: "user_k"}, {ru: "user1", kz: "user1_k"}, {ru: "num_pas", kz: "num_pas_k"}, {ru: "choice", kz: "choice_k"}, {ru: "date_pas", kz: "date_pas_k"}, {ru: "iin_pas", kz: "iin_pas_k"}, {ru: "choice1", kz: "choice1_k"}, {ru: "punkt", kz: "punkt_k"}, {ru: "razdel", kz: "razdel_k"}, {ru: "razdel1", kz: "razdel1_k"}, {ru: "date_razdel", kz: "date_razdel_k"}, {ru: "start_date", kz: "start_date_k"}, {ru: "choice2", kz: "choice2_k"}, {ru: "choice3", kz: "choice3_k"}, {ru: "position2", kz: "position2_k"}, {ru: "user2", kz: "user2_k"}, {ru: "position3", kz: "position3_k"}, {ru: "user3", kz: "user3_k"}, {ru: "finish_date", kz: "finish_date_k"}];
    model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
      console.log('formName: [' + model.formName + ']\nformCode: [' + model.formCode + ']\nformID: [' + model.formId +']\nasfDataID: [' + model.asfDataId+']');
      cmpArrDS.forEach(function(cmp) {
        ALL_DOCUMENTS_UTIL.changedValue(model, cmp);
      });
    });
    model.getModelWithId("user1").on("valueChange", function() {
      // № удостоверение личности
      ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Штатная_расстановка111", "user1", "numberofudlichnosti", "num_pas", null, null);
      // выдано кем
      ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Штатная_расстановка111", "user1", "organofud", "choice", null, null);
      // дата выдачи
      ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Штатная_расстановка111", "user1", "dateofud", "date_pas", null, null);
      // ИИН
      ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Штатная_расстановка111", "user1", "iin", "iin_pas", null, null);
    });
    // Наименование организации
    var oraganization = ALL_DOCUMENTS_UTIL.getOrganizationData();
    if (oraganization) {
      if (model.getModelWithId('company_name')) model.getModelWithId('company_name').setValue(oraganization.nameRu);
      if (model.getModelWithId('company_name_k')) model.getModelWithId('company_name_k').setValue(oraganization.nameKz);
    }
  }

  if (model.formCode == "Докладная__о_привлечении_к_дисциплинарной_ответственности") {
    var cmpArrDoklad=[{ru: "to_user", kz: "to_user_k"}, {ru: "copy_user", kz: "copy_user_k"}, {ru: "from_user", kz: "from_user_k"}, {ru: "user", kz: "user_k"}, {ru: "tab_num", kz: "tab_num_k"}, {ru: "choice1", kz: "choice1_k"}, {ru: "podpisant_user", kz: "podpisant_user_k"}, {ru: "ispolnitel_user", kz: "ispolnitel_user_k"}];
    model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
      console.log('formName: [' + model.formName + ']\nformCode: [' + model.formCode + ']\nformID: [' + model.formId +']\nasfDataID: [' + model.asfDataId+']');
      cmpArrDoklad.forEach(function(cmp) {
        ALL_DOCUMENTS_UTIL.changedValue(model, cmp);
      });
    });
  }

  if (model.formCode == "Больничный_лист_по_беременности_и_родам" || model.formCode == "Больничный_лист_1") {
    model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
      console.log('formName: [' + model.formName + ']\nformCode: [' + model.formCode + ']\nformID: [' + model.formId +']\nasfDataID: [' + model.asfDataId+']');
    });
    if (model.getModelWithId("user")) {
      model.getModelWithId("user").on("valueChange", function() {
        // табельный номер
        ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Штатная_расстановка111", "user", "t_number", "tab_num", null, null);
      });
    }
    // С-ПО
    ALL_DOCUMENTS_UTIL.compareDates(view, "date_start", "date_finish", null, null, null);
    // выпиливаем поле "cancel"
    ALL_DOCUMENTS_UTIL.setVisibleCmp(view, "cancel", false);
  }

  if (model.formCode =="Обходной_лист_при_увольнении") {
    var cmpArrOL=[{ru: "osnovanie", kz: "osnovanie_k"}, {ru: "doc_num", kz: "doc_num_k"}, {ru: "doc_date", kz: "doc_date_k"}, {ru: "user", kz: "user_k"}, {ru: "tab_num", kz: "tab_num_k"}, {ru: "ispolnitel_user", kz: "ispolnitel_user_k"}, {ru: "ispolnitel_position", kz: "ispolnitel_position_k"}];
    var tableArrOL=[{id: {ru: "t", kz: "t_k"}, cmp: [{ru: "user1", kz: "user1_k"}, {ru: "date1", kz: "date1_k"}, {ru: "sum", kz: "sum_k"}]}];

    model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
      console.log('formName: [' + model.formName + ']\nformCode: [' + model.formCode + ']\nformID: [' + model.formId +']\nasfDataID: [' + model.asfDataId+']');
      // дин.таб.
      tableArrOL.forEach(function(table) {
        ALL_DOCUMENTS_UTIL.tableOn(view, table.id, table.cmp);
      });
      // компоненты на форме
      cmpArrOL.forEach(function(cmp) {
        ALL_DOCUMENTS_UTIL.changedValue(model, cmp);
      });
    });

    // Наименование организации
    var oraganization = ALL_DOCUMENTS_UTIL.getOrganizationData();
    if (oraganization) {
      if (model.getModelWithId('company_name')) model.getModelWithId('company_name').setValue(oraganization.nameRu);
      if (model.getModelWithId('company_name_k')) model.getModelWithId('company_name_k').setValue(oraganization.nameKz);
      if (model.getModelWithId('organization_name')) model.getModelWithId('organization_name').setValue(oraganization.nameRu);
      if (model.getModelWithId('organization_name_k')) model.getModelWithId('organization_name_k').setValue(oraganization.nameKz);
    }

    // замена логотипа
    if (model.getModelWithId('logo')) model.getModelWithId('logo').asfProperty.config.url = "../logo.png";
    if (view.getViewWithId('logo')) view.getViewWithId('logo').container[0].firstChild.src = "../logo.png";
    if (model.getModelWithId('logo_k')) model.getModelWithId('logo_k').asfProperty.config.url = "../logo.png";
    if (view.getViewWithId('logo_k')) view.getViewWithId('logo_k').container[0].firstChild.src = "../logo.png";

    if (view.editable) {
      tableArrOL.forEach(function(table) {
        ALL_DOCUMENTS_UTIL.tableEnabled(view, table.id.kz); // блокируем дин.таблицы в каз. варианте
      });
    }

  }

  if (model.formCode == "Направление_на_медосмотр") {
    var cmpArrMedos=[{ru: "start_date", kz: "start_date_k"}, {ru: "to_user", kz: "to_user_k"}, {ru: "user", kz: "user_k"}, {ru: "date", kz: "date_k"}, {ru: "number", kz: "number_k"}, {ru: "ispolnitel_user", kz: "ispolnitel_user_k"}];
    model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
      console.log('formName: [' + model.formName + ']\nformCode: [' + model.formCode + ']\nformID: [' + model.formId +']\nasfDataID: [' + model.asfDataId+']');
      cmpArrMedos.forEach(function(cmp) {
        ALL_DOCUMENTS_UTIL.changedValue(model, cmp);
      });
    });
    model.getModelWithId("user").on("valueChange", function() {
      // Дата рождения
      ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Штатная_расстановка111", "user", "birthday", "date", null, null);
    });
    // Наименование организации
    var oraganization = ALL_DOCUMENTS_UTIL.getOrganizationData();
    if (oraganization) {
      if (model.getModelWithId('organization_name')) model.getModelWithId('organization_name').setValue(oraganization.nameRu);
      if (model.getModelWithId('organization_name_k')) model.getModelWithId('organization_name_k').setValue(oraganization.nameKz);
    }
    // замена логотипа
    if (model.getModelWithId('logo')) model.getModelWithId('logo').asfProperty.config.url = "../logo.png";
    if (view.getViewWithId('logo')) view.getViewWithId('logo').container[0].firstChild.src = "../logo.png";

  }

  if (model.formCode =="Изменения_наименовании_компаний1") {
    model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
      console.log('formName: [' + model.formName + ']\nformCode: [' + model.formCode + ']\nformID: [' + model.formId +']\nasfDataID: [' + model.asfDataId+']');
      var cmpArr = [{ru: "start_date1", kz: "start_date1_k"}, {ru: "finish_date1", kz: "finish_date1_k"}];
      cmpArr.forEach(function(cmp) {
        ALL_DOCUMENTS_UTIL.changedValue(model, cmp);
      });
    });
    // С-ПО
    ALL_DOCUMENTS_UTIL.compareDates(view, "start_date1", "finish_date1", null, null, null);
  }

  if (model.formCode =="Справочник_ТД") {
    var tableArrSTD=[
      {id: {ru: "t", kz: "t_k"}, cmp: [{ru: "tab_num", kz: "tab_num_k"}, {ru: "user", kz: "user_k"}, {ru: "doc_num", kz: "doc_num_k"}, {ru: "start_date", kz: "start_date_k"}, {ru: "finish_date", kz: "finish_date_k"}]}
    ];
    model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
      console.log('formName: [' + model.formName + ']\nformCode: [' + model.formCode + ']\nformID: [' + model.formId +']\nasfDataID: [' + model.asfDataId+']');
      tableArrSTD.forEach(function(table) {
        ALL_DOCUMENTS_UTIL.tableOn(view, table.id, table.cmp);
      });
    });
    // С-ПО
    ALL_DOCUMENTS_UTIL.compareDates(view, "start_date", "finish_date", null, tableArrSTD[0].id, 0);
    model.getModelWithId(tableArrSTD[0].id.ru).on('tableRowAdd', function(evt2, modelTab2, modelRow2) {
      ALL_DOCUMENTS_UTIL.compareDates(view, "start_date", "finish_date", null, tableArrSTD[0].id, modelRow2[0].asfProperty.tableBlockIndex);
    });
  }

  if (model.formCode =="Инициация_графика_за_период") {
    model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
      console.log('formName: [' + model.formName + ']\nformCode: [' + model.formCode + ']\nformID: [' + model.formId +']\nasfDataID: [' + model.asfDataId+']');
    });
    var cmpStart = "date_start";
    var cmpStop = "date_finish";
    var startDate = model.getModelWithId(cmpStart);
    var stopDate = model.getModelWithId(cmpStop);

    startDate.on("valueChange", function() {
      if (AS.FORMS.DateUtils.parseDate(startDate.getValue()) !== null && AS.FORMS.DateUtils.parseDate(stopDate.getValue()) !== null ) {
        var d1 = AS.FORMS.DateUtils.parseDate(startDate.getValue());
        var d2 = AS.FORMS.DateUtils.parseDate(stopDate.getValue());
        if (d1.getFullYear() == d2.getFullYear()) {
          if ( (d2.getMonth() - d1.getMonth()) > 1 ) {
            ALL_DOCUMENTS_UTIL.msgShow("В периоде не должно быть более 2-х месяцев");
            stopDate.setValue(null);
          } else {
            var dd = Math.floor((d2.getTime() - d1.getTime()) / 86400000);
            if (dd < 0) {
              view.getViewWithId(cmpStop).container.parent().css("background-color", "#FF9999");
              ALL_DOCUMENTS_UTIL.msgShow("Дата ПО [" + stopDate.getValue() + "] меньше даты С [" + startDate.getValue() + "]");
              stopDate.setValue(null);
              stopDate.asfProperty.required=true;
            } else {
              view.getViewWithId(cmpStop).container.parent().css("background-color", "");
              if (dd > 41) {
                view.getViewWithId(cmpStop).container.parent().css("background-color", "#FF9999");
                ALL_DOCUMENTS_UTIL.msgShow("Период между датой начала и датой окончания не должен быть более 42-х дней");
                stopDate.setValue(null);
                stopDate.asfProperty.required=true;
              }
            }
          }
        } else {
          ALL_DOCUMENTS_UTIL.msgShow("В периоде не должно быть более 1 года");
          stopDate.setValue(null);
        }
      }
    });

    stopDate.on("valueChange", function() {
      if (AS.FORMS.DateUtils.parseDate(startDate.getValue()) !== null && AS.FORMS.DateUtils.parseDate(stopDate.getValue()) !== null ) {
        var d1 = AS.FORMS.DateUtils.parseDate(startDate.getValue());
        var d2 = AS.FORMS.DateUtils.parseDate(stopDate.getValue());
        if (d1.getFullYear() == d2.getFullYear()) {
          if ( (d2.getMonth() - d1.getMonth()) > 1 ) {
            ALL_DOCUMENTS_UTIL.msgShow("В периоде не должно быть более 2-х месяцев", "info");
            stopDate.setValue(null);
          } else {
            var dd = Math.floor((d2.getTime() - d1.getTime()) / 86400000);
            if (dd < 0) {
              view.getViewWithId(cmpStop).container.parent().css("background-color", "#FF9999");
              ALL_DOCUMENTS_UTIL.msgShow("Дата ПО [" + stopDate.getValue() + "] меньше даты С [" + startDate.getValue() + "]");
              stopDate.setValue(null);
              stopDate.asfProperty.required=true;
            } else {
              view.getViewWithId(cmpStop).container.parent().css("background-color", "");
              if (dd > 41) {
                view.getViewWithId(cmpStop).container.parent().css("background-color", "#FF9999");
                ALL_DOCUMENTS_UTIL.msgShow("Период между датой начала и датой окончания не должен быть более 42-х дней");
                stopDate.setValue(null);
                stopDate.asfProperty.required=true;
              }
            }
          }
        } else {
          ALL_DOCUMENTS_UTIL.msgShow("В периоде не должно быть более 1 года", "info");
          stopDate.setValue(null);
        }
      }
    });
  }

  //Инструмент массового прикрепления сотрудников
  if (model.formCode =="Смена_и_вахта_сотрудника") {
    if (view.editable) {
      var usersBox = view.getViewWithId("user");
      //console.log(usersBox);
      var BUTTON_CLEAR = $('<button>', {class: 'NEW-BUTTON-REPORT'}).html('Очистить');
      BUTTON_CLEAR.click(function(){
        model.getModelWithId("user").setValue(null);
      });
      usersBox.container.append(BUTTON_CLEAR);

    }
  }

});
