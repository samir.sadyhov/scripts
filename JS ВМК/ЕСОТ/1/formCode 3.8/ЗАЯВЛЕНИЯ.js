AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  if (model.formCode.substring(0,9).toLowerCase() == "заявление") {
    var cmpArrZ=[{ru: "choice4", kz: "choice4_k"},{ru: "doc_num", kz: "doc_num_k"},{ru: "doc_date", kz: "doc_date_k"},{ru: "td_doc", kz: "td_doc_k"},{ru: "doc", kz: "doc_k"},{ru: "td_num", kz: "td_num_k"},{ru: "td_date", kz: "td_date_k"},{ru: "to_user", kz: "to_user_k"},{ru: "from_user", kz: "from_user_k"},{ru: "address", kz: "address_k"},{ru: "telephone", kz: "telephone_k"},{ru: "tab_num", kz: "tab_num_k"},{ru: "city", kz: "city_k"},{ru: "user", kz: "user_k"},{ru: "user1", kz: "user1_k"},{ru: "user2", kz: "user2_k"},{ru: "user3", kz: "user3_k"},{ru: "user4", kz: "user4_k"},{ru: "user5", kz: "user5_k"},{ru: "user6", kz: "user6_k"},{ru: "user7", kz: "user7_k"},{ru: "user8", kz: "user8_k"},{ru: "user9", kz: "user9_k"},{ru: "user10", kz: "user10_k"},{ru: "user_to", kz: "user_to_k"},{ru: "position", kz: "position_k"},{ru: "position_to", kz: "position_to_k"},{ru: "department_to", kz: "department_to_k"},{ru: "from_user", kz: "from_user_k"},{ru: "period", kz: "period_k"},{ru: "choice", kz: "choice_k"},{ru: "сhoice", kz: "сhoice_k"},{ru: "сhoice1", kz: "сhoice1_k"}, {ru: "choice2", kz: "choice2_k"},{ru: "сhoice2", kz: "сhoice2_k"},{ru: "сhoice3", kz: "сhoice3_k"},{ru: "сhoice4", kz: "сhoice4_k"},{ru: "сhoice5", kz: "сhoice5_k"}, {ru: "сhoice6", kz: "сhoice6_k"},{ru: "сhoice7", kz: "сhoice7_k"},{ru: "сhoice8", kz: "сhoice8_k"},{ru: "сhoice9", kz: "сhoice9_k"},{ru: "сhoice10", kz: "сhoice10_k"}, {ru: "start_date", kz: "start_date_k"}, {ru: "finish_date", kz: "finish_date_k"}, {ru: "vid_td", kz: "vid_td_k"}, {ru: "start_period", kz: "start_period_k"},{ru: "period_start", kz: "period_start_k"},{ru: "end_period", kz: "end_period_k"},{ru: "period_finish", kz: "period_finish_k"},{ru: "days", kz: "days_k"},{ru: "main", kz: "main_k"},{ru: "additional", kz: "additional_k"},{ru: "date_main", kz: "date_main_k"},{ru: "date_additional", kz: "date_additional_k"},{ru: "date_last", kz: "date_last_k"},{ru: "date_last_po", kz: "date_last_po_k"},{ru: "days_available", kz: "days_available_k"},{ru: "remainder", kz: "remainder_k"},{ru: "podpisant_user", kz: "podpisant_user_k"},{ru: "podpisant_position", kz: "podpisant_position_k"},{ru: "ispolnitel", kz: "ispolnitel_k"},{ru: "isponitel", kz: "isponitel_k"},{ru: "mat_pom_payment", kz: "mat_pom_payment_k"},{ru: "date_start", kz: "date_start_k"},{ru: "date_finish", kz: "date_finish_k"}];
    var tableArrZ = [{id: {ru: "table2_r", kz: "table2_k"}, cmp: [{ru: "user", kz: "user_k"}]}, {id: {ru: "table3_r", kz: "table3_k"}, cmp: [{ru: "сhoice2", kz: "сhoice2_k"}]}];

    model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
      console.log(model.formName + '\nformCode: [' + model.formCode + ']\nformID: [' + model.formId +'] asfDataID: [' + model.asfDataId+']');
      for (var ci=0; ci < cmpArrZ.length; ci++) {
        ALL_DOCUMENTS_UTIL.changedValue(model, cmpArrZ[ci]);
      }
      for (var ti=0; ti < tableArrZ.length; ti++) {
        ALL_DOCUMENTS_UTIL.tableOn(view, tableArrZ[ti].id, tableArrZ[ti].cmp);
      }
    });
    if (view.editable) {
      for (var ti2=0; ti2 < tableArrZ.length; ti2++) {
        ALL_DOCUMENTS_UTIL.tableEnabled(view, tableArrZ[ti2].id.kz);
      }
    }

    // Заявление о приеме на работу на период
    if (model.formCode == "Заявление_о_приеме_на_работу_на_период") {
      // логика с таблицами
      var periodCmp = model.getModelWithId("period");
      var tab1={ru: "table2_r", kz: "table2_k"};
      var tab2={ru: "table3_r", kz: "table3_k"};
      if (periodCmp.value[0] == "1") {
        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tab2, false);
        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tab1, true);
      } else {
        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tab2, true);
        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tab1, false);
      }
      periodCmp.on("valueChange", function() {
        var tt1 = model.getModelWithId(tab1.ru);
        var tt2 = model.getModelWithId(tab2.ru);
        if (periodCmp.value[0] == "1") {
          ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tab2, false);
          ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tab1, true);
          tt2.modelBlocks.forEach(function(modelBlock, index){
            tt2.removeRow(index);
          });
          if (tt1.modelBlocks.length == 0) {
            tt1.createRow();
          } else {
            tt1.modelBlocks.forEach(function(modelBlock, index){
              tt1.removeRow(index);
            });
            if (tt1.modelBlocks.length == 0) tt1.createRow();
          }
        } else {
          ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tab2, true);
          ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tab1, false);
          tt2.modelBlocks.forEach(function(modelBlock, index){
            tt2.removeRow(index);
          });
          if (tt1.modelBlocks.length > 0) {
            tt1.modelBlocks.forEach(function(modelBlock, index){
              tt1.removeRow(index);
            });
          }
          if (tt2.modelBlocks.length == 0) {
            tt2.createRow();
          } else {
            tt2.modelBlocks.forEach(function(modelBlock, index){
              tt2.removeRow(index);
            });
            if (tt2.modelBlocks.length == 0) tt2.createRow();
          }
        }
      });
    }

    // Всего календарных дней
    var vsegoOsnOtp=model.getModelWithId("main");
    var vsegoDopOtp=model.getModelWithId("additional");
    var vsegoDays=model.getModelWithId("days");
    if (vsegoOsnOtp && vsegoDopOtp && vsegoDays) {
      vsegoOsnOtp.on("valueChange", function() {
        if (vsegoOsnOtp.getValue() != undefined && vsegoDopOtp.getValue() != undefined) {
          vsegoDays.setValue(Number(vsegoOsnOtp.getValue())+Number(vsegoDopOtp.getValue())+"");
        } else if (vsegoOsnOtp.getValue() != undefined && vsegoDopOtp.getValue() == undefined) {
          vsegoDays.setValue(vsegoOsnOtp.getValue());
        } else if (vsegoOsnOtp.getValue() == undefined && vsegoDopOtp.getValue() != undefined) {
          vsegoDays.setValue(vsegoDopOtp.getValue());
        } else {
          vsegoDays.setValue(null);
        }
      });
      vsegoDopOtp.on("valueChange", function() {
        if (vsegoOsnOtp.getValue() != undefined && vsegoDopOtp.getValue() != undefined) {
          vsegoDays.setValue(Number(vsegoOsnOtp.getValue())+Number(vsegoDopOtp.getValue())+"");
        } else if (vsegoOsnOtp.getValue() != undefined && vsegoDopOtp.getValue() == undefined) {
          vsegoDays.setValue(vsegoOsnOtp.getValue());
        } else if (vsegoOsnOtp.getValue() == undefined && vsegoDopOtp.getValue() != undefined) {
          vsegoDays.setValue(vsegoDopOtp.getValue());
        } else {
          vsegoDays.setValue(null);
        }
      });
    }

    // логика  С-ПО
    ALL_DOCUMENTS_UTIL.compareDates(view, "start_period", "end_period", null, null, null);
    ALL_DOCUMENTS_UTIL.compareDates(view, "start_date", "finish_date", null, null, null);
    if (vsegoOsnOtp) ALL_DOCUMENTS_UTIL.compareDates(view, "date_main", "date_additional", "main", null, null);
    if (vsegoDopOtp) ALL_DOCUMENTS_UTIL.compareDates(view, "date_last", "date_last_po", "additional", null, null);

    // Заявление на отпуск без сохранения ЗП
    if (model.formCode == "Заявление_на_отпуск_без_сохранения_ЗП") {
      // логика  С-ПО
      ALL_DOCUMENTS_UTIL.compareDates(view, "start_date", "finish_date", "main", null, null);
      // телефон
      model.getModelWithId("from_user").on("valueChange", function() {
        ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Штатная_расстановка111", "from_user", "telephone", "telephone", null, null);
      });
    }

    // Заявление о предоставлении ежегодного трудового отпуска с мат.помощью
    // Заявление о предоставлении ежегодного трудового отпуска без выплаты
    if (model.formCode == "Заявление_о_предоставлении_ежегодного_трудового_отпуска_с_мат.помощью"
    || model.formCode == "Заявление_о_предоставлении_ежегодного_трудового_отпуска_без_выплаты") {
      // проверка дат основного и доп. отпусков
      var dateOsnS=model.getModelWithId("date_main");
      var dateOsnP=model.getModelWithId("date_additional");
      var dateDopS=model.getModelWithId("date_last");
      var dateDopP=model.getModelWithId("date_last_po");

      // дата дополнительного С не больше даты основного ПО
      dateDopS.on("valueChange", function() {
        if (AS.FORMS.DateUtils.parseDate(dateOsnP.getValue()) !=null) {
          var tmpD1=AS.FORMS.DateUtils.parseDate(dateOsnP.getValue());
          tmpD1.setDate(tmpD1.getDate() + 1);
          var tmpD2=AS.FORMS.DateUtils.parseDate(dateDopS.getValue());
          if (AS.FORMS.DateUtils.parseDate(dateDopS.getValue()) !=null) {
            if (tmpD2.getTime() != tmpD1.getTime()) {
              view.getViewWithId("date_last").container.parent().css("background-color", "#FF9999");
              ALL_DOCUMENTS_UTIL.msgShow("Дата дополнительного отпуска должна быть больше даты окончания основного отпуска на 1 день.");
              dateDopS.setValue(null);
            } else {
              view.getViewWithId("date_last").container.parent().css("background-color", "");
            }
          }
        }
      });

      model.getModelWithId("from_user").on("valueChange", function() {
        ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Штатная_расстановка111", "from_user", "telephone", "telephone", null, null);
      });


    } // заявления по отпускам

    // Заявление материальной помощи
    if (model.formCode == "Заявление_материальной_помощи") {
      model.getModelWithId("from_user").on("valueChange", function() {
        ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Штатная_расстановка111", "from_user", "telephone", "telephone", null, null);
      });
    }

  }
});
