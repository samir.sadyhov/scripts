var report = {
  //tabelOldID: "11cd2693-bea9-484b-bc69-876f70140690", //старый табель за месяц
  grafikYearID: "d4016d29-8345-45e0-8fe7-193297c9539f", //годовой график
  zakluchenieID: "88bf314b-eac8-4dea-859b-85a1676a4e59", // заключение о приеме
  vacationScheduleReportID : "e5ff07f9-8e1c-4702-b8da-cde10e8acd45", // график отпусков
  tabelPeriodID: "f62c4d25-abb0-4cb2-ae98-8a1047b4ffd3", // табель за период
  ScheduleReportIDNew: "bb1903cc-d7d3-43ec-ada0-c991e5510f36", // график за месяц

  ADD_BUTTON: function(model, reportID, reportName) {
    var PARENT_COMPONENT = $('td.background-lilacgray').children('div.gwt-HTML');
    var buttonName = 'Выгрузить печатную форму';
    if (i18n.getCurrentLocale() == 'kk') buttonName = 'Баспа түрінде жүктеу';
    var BUTTON_REPORT = $('<button>', {class: 'NEW-BUTTON-REPORT'}).html(buttonName);
    BUTTON_REPORT.click(function(){

      var reportURL = "formreport?reportid=" + reportID + "&inline=false&filename=" + reportName + "&asfDataID=" + model.asfDataId;

      if (model.formCode == "Табель._месяц_период"
      || model.formCode == "График_отпусков"
      || model.formCode == "График_работы_на_месяц11") {
        if(!model.isValid()) {
          ALL_DOCUMENTS_UTIL.msgShow("Данные отчеты не корректны. Пожалуйста проверьте данные и попробуйте распечатать отчет еще раз");
          return;
        }
        AS.SERVICES.showWaitWindow();
        var asfData = model.getAsfData();
        jQuery.when(AS.FORMS.ApiUtils.saveAsfData(asfData.data, model.formId, model.asfDataId)).then(function(asfDataId){
          window.open(reportURL);
          AS.SERVICES.hideWaitWindow();
        });
      } else {
        window.open(reportURL);
      }

    });
    BUTTON_REPORT.prependTo(PARENT_COMPONENT);
  },

  hideOldPrintButton: function(view) {
    var tmpPrint = $('td.background-lilacgray')[1];
    if(!view.editable) {
      $(tmpPrint).hide();
    } else {
      $(tmpPrint).show();
    }
  }
};


AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {

    // if (model.formCode == "Табель._месяц") {
    //   if ($('.NEW-BUTTON-REPORT').length > 0) {
    //     $('.NEW-BUTTON-REPORT').remove();
    //     report.ADD_BUTTON(model, report.tabelOldID, "tabel_ucheta_rabochego_vremeni.xls");
    //   } else {
    //     report.ADD_BUTTON(model, report.tabelOldID, "tabel_ucheta_rabochego_vremeni.xls");
    //   }
    // }

    if (model.formCode == "Табель._месяц_период") {
      report.hideOldPrintButton(view);
      model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
        if ($('.NEW-BUTTON-REPORT').length > 0) {
          $('.NEW-BUTTON-REPORT').remove();
          report.ADD_BUTTON(model, report.tabelPeriodID, "tabel_ucheta_rabochego_vremeni.xls");
        } else {
          report.ADD_BUTTON(model, report.tabelPeriodID, "tabel_ucheta_rabochego_vremeni.xls");
        }
      });
    }

    if (model.formCode == "Годовой_график_new1") {
      report.hideOldPrintButton(view);
      model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
        if ($('.NEW-BUTTON-REPORT').length > 0) {
          $('.NEW-BUTTON-REPORT').remove();
          report.ADD_BUTTON(model, report.grafikYearID, "godovoi_grafik_rabot.xls");
        } else {
          report.ADD_BUTTON(model, report.grafikYearID, "godovoi_grafik_rabot.xls");
        }
      });
    }

    if (model.formCode == "Представление_о_приеме_на_работу_1") {
      report.hideOldPrintButton(view);
      model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
        if ($('.NEW-BUTTON-REPORT').length > 0) {
          $('.NEW-BUTTON-REPORT').remove();
          report.ADD_BUTTON(model, report.zakluchenieID, "zakluchenie_o_prieme_na_rabotu.rtf");
        } else {
          report.ADD_BUTTON(model, report.zakluchenieID, "zakluchenie_o_prieme_na_rabotu.rtf");
        }
      });
    }

    if (model.formCode == "График_отпусков") {
      report.hideOldPrintButton(view);
      model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
        if ($('.NEW-BUTTON-REPORT').length > 0) {
          $('.NEW-BUTTON-REPORT').remove();
          report.ADD_BUTTON(model, report.vacationScheduleReportID, "vacation_schedule.xls");
        } else {
          report.ADD_BUTTON(model, report.vacationScheduleReportID, "vacation_schedule.xls");
        }
      });
    }

    if (model.formCode == "График_работы_на_месяц11") {
      report.hideOldPrintButton(view);
      model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
        if ($('.NEW-BUTTON-REPORT').length > 0) {
          $('.NEW-BUTTON-REPORT').remove();
          report.ADD_BUTTON(model, report.ScheduleReportIDNew, "grafik_rabot.xls");
        } else {
          report.ADD_BUTTON(model, report.ScheduleReportIDNew, "grafik_rabot.xls");
        }
      });
    }

});



<style>
.NEW-BUTTON-REPORT {
  background-color: #9BC537;
  border: none;
  border-radius: 4px;
  color: black;
  padding: 4px 15px;
  text-align: center;
  text-decoration: none;
  display: inline-block;
  font-size: 12px;
  font-weight: bold;
  cursor: pointer;
  float: left;
}

.NEW-BUTTON-REPORT:hover {
  background-color: #3e8e41;
}
</style>
