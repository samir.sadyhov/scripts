AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
    if (model.formCode == "Справочник_циклов") {
        var table = model.getModelWithId("table");
        table.on('tableRowAdd', function(evt, modelTab, modelRow) {
            table.modelBlocks.forEach(function(modelBlock, index){
                table.getModelWithId("day", table.asfProperty.id, modelBlock[0].asfProperty.tableBlockIndex).setValue((index+1)+"");
            });
        });
        table.on('tableRowDelete', function(evt, rowModel, rowIndex, removedRow) {
            table.modelBlocks.forEach(function(modelBlock, index){
                table.getModelWithId("day", table.asfProperty.id, modelBlock[0].asfProperty.tableBlockIndex).setValue((index+1)+"");
            });
        });
    }
});
