AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  if (model.formCode.substring(0, 9).toLowerCase() == "служебная") {
    var cmpArrS=[{ru: "doc_num", kz: "doc_num_k"},{ru: "doc_date", kz: "doc_date_k"},{ru: "to_user", kz: "to_user_k"}, {ru: "to_position", kz: "to_position_k"}, {ru: "copy_user", kz: "copy_user_k"}, {ru: "copy_position", kz: "copy_position_k"}, {ru: "from_user", kz: "from_user_k"}, {ru: "from_position", kz: "from_position_k"}, {ru: "doc_date", kz: "doc_date_k"}, {ru: "doc_num", kz: "doc_num_k"}, {ru: "user", kz: "user_k"}, {ru: "user5", kz: "user5_k"}, {ru: "tab_num", kz: "tab_num_k"}, {ru: "position", kz: "position_k"}, {ru: "position2", kz: "position2_k"}, {ru: "department", kz: "department_k"}, {ru: "department1", kz: "department1_k"}, {ru: "department2", kz: "department2_k"}, {ru: "department3", kz: "department3_k"}, {ru: "start_date", kz: "start_date_k"}, {ru: "finish_date", kz: "finish_date_k"},	{ru: "choice", kz: "choice_k"}, {ru: "сhoice1", kz: "сhoice1_k"}, {ru: "choice1", kz: "choice1_k"}, {ru: "choice2", kz: "choice2_k"}, {ru: "choice3", kz: "choice3_k"}, {ru: "choice4", kz: "choice4_k"}, {ru: "choice5", kz: "choice5_k"}, {ru: "choice6", kz: "choice6_k"}, {ru: "choice7", kz: "choice7_k"}, {ru: "choice8", kz: "choice8_k"}, {ru: "choice9", kz: "choice9_k"}, {ru: "choice10", kz: "choice10_k"}, {ru: "ispolnitel_user", kz: "ispolnitel_user_k"}, {ru: "ispolnitel_podpisant", kz: "ispolnitel_podpisant_k"}, {ru: "ispolnitel_position", kz: "ispolnitel_position_k"}, {ru: "podpisant_position", kz: "podpisant_position_k"}, {ru: "podpisant_user", kz: "podpisant_user_k"}, {ru: "itogo", kz: "itogo_k"}, {ru: "change_position", kz: "change_position_k"}, {ru: "change_department", kz: "change_department_k"}, {ru: "amount", kz: "amount_k"}, {ru: "compensation", kz: "compensation_k"}, {ru: "rashod1", kz: "rashod1_k"}, {ru: "rashod3", kz: "rashod3_k"}, {ru: "kol", kz: "kol_k"}, {ru: "days", kz: "days_k"}, {ru: "ruk_user", kz: "ruk_user_k"}, {ru: "srok", kz: "srok_k"}, {ru: "сhoice2", kz: "сhoice2_k"}];
    var tableArrS=[
      {id: {ru: "t", kz: "t_k"}, cmp: [{ru: "choice1", kz: "choice1_k"}, {ru: "choice2", kz: "choice2_k"}, {ru: "compensation", kz: "compensation_k"}, {ru: "finish_date", kz: "finish_date_k"}, {ru: "position", kz: "position_k"}, {ru: "start_date", kz: "start_date_k"}, {ru: "start_date1", kz: "start_date1_k"}, {ru: "tab_num", kz: "tab_num_k"}, {ru: "change_department", kz: "change_department_k"}, {ru: "user", kz: "user_k"}]},
      {id: {ru: "t", kz: "t1_k"}, cmp: [{ru: "department", kz: "department_k"}, {ru: "department1", kz: "department1_k"}, {ru: "position", kz: "position_k"}, {ru: "position1", kz: "position1_k"}, {ru: "prichina", kz: "prichina_k"}, {ru: "user", kz: "user_k"}]},
      {id: {ru: "t8", kz: "t8_k"}, cmp: [{ru: "doc", kz: "doc_k"}, {ru: "user8", kz: "user8_k"}]},
      {id: {ru: "table2_r", kz: "table2_k"}, cmp: [{ru: "change_department", kz: "change_department_k"}, {ru: "change_position", kz: "change_position_k"}, {ru: "department", kz: "department_k"}, {ru: "position", kz: "position_k"}, {ru: "tab_num", kz: "tab_num_k"}, {ru: "user", kz: "user_k"}, {ru: "user1", kz: "user1_k"}]},
      {id: {ru: "table5_r", kz: "table5_k"}, cmp: [{ru: "date2", kz: "date2_k"}, {ru: "user3", kz: "user3_k"}]},
      {id: {ru: "t4", kz: "t4_k"}, cmp: [{ru: "start_date", kz: "start_date_k"}, {ru: "finish_date", kz: "finish_date_k"}, {ru: "choice8", kz: "choice8_k"}]}
    ];

    // Наименование организации
    var oraganization = ALL_DOCUMENTS_UTIL.getOrganizationData();
    if (oraganization) {
      if (model.getModelWithId('organization_name')) model.getModelWithId('organization_name').setValue(oraganization.nameRu);
      if (model.getModelWithId('organization_name_k')) model.getModelWithId('organization_name_k').setValue(oraganization.nameKz);
      if (model.getModelWithId('company_name')) model.getModelWithId('company_name').setValue(oraganization.nameRu);
      if (model.getModelWithId('company_name_k')) model.getModelWithId('company_name_k').setValue(oraganization.nameKz);
    }

    model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
      console.log('formName: [' + model.formName + ']\nformCode: [' + model.formCode + ']\nformID: [' + model.formId +']\nasfDataID: [' + model.asfDataId+']');
      for (var ti=0; ti < tableArrS.length; ti++) {
        ALL_DOCUMENTS_UTIL.tableOn(view, tableArrS[ti].id, tableArrS[ti].cmp);
      }
      for (var ci=0; ci < cmpArrS.length; ci++) {
        ALL_DOCUMENTS_UTIL.changedValue(model, cmpArrS[ci]);
      }
    });
    if (view.editable) {
      for (var ti2=0; ti2 < tableArrS.length; ti2++) {
        ALL_DOCUMENTS_UTIL.tableEnabled(view, tableArrS[ti2].id.kz);
      }
    }

    // логика  С-ПО
    ALL_DOCUMENTS_UTIL.compareDates(view, "start_date", "finish_date", null, null, null);

    // Служебная записка на изменение графика работы
    if (model.formCode == "Служебная_записка_") {
      // логика  С-ПО
      ALL_DOCUMENTS_UTIL.compareDates(view, "date_start", "date_finish", null, null, null);
    }

    // Служебная записка о командировании
    if (model.formCode == "Служебная_записка_о_командировании") {
      // логика  С-ПО (Срок командирования (календарных дней))
      ALL_DOCUMENTS_UTIL.compareDates(view, "start_date", "finish_date", "srok", null, null);
    }

    // Служебная записка о временном переводе
    if (model.formCode == "Служебная_записка_о_временном_переводе") {
      // С-по\по событию
      var tmpCheck = model.getModelWithId("сhoice2");
      var tmpFin1 = {ru: "finish_date", kz: "finish_date_k"};
      var tmpFin2 = {ru: "finish_date1", kz: "finish_date1_k"};
      var cmpTable = {ru: "table2_r", kz: "table2_k"};
      var tmpTable = model.getModelWithId(cmpTable.ru);

      if (tmpCheck.value[0] == "1") {
        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tmpFin1, true);
        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tmpFin2, false);
        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmpTable, false);
        tmpTable.modelBlocks.forEach(function(modelBlock, index){
          tmpTable.removeRow(index);
        });
      } else if (tmpCheck.value[0] == "2") {
        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tmpFin1, false);
        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tmpFin2, true);
        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmpTable, false);
        tmpTable.modelBlocks.forEach(function(modelBlock, index){
          tmpTable.removeRow(index);
        });
      } else if (tmpCheck.value[0] == "3") {
        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tmpFin1, false);
        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tmpFin2, false);
        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmpTable, true);
        if (tmpTable.modelBlocks.length == 0) {
          tmpTable.createRow();
        }
      } else {
        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tmpFin1, false);
        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tmpFin2, false);
        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmpTable, false);
        tmpTable.modelBlocks.forEach(function(modelBlock, index){
          tmpTable.removeRow(index);
        });
      }
      tmpCheck.on("valueChange", function() {
        if (tmpCheck.value[0] == "1") {
          ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tmpFin1, true);
          ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tmpFin2, false);
          ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmpTable, false);
          tmpTable.modelBlocks.forEach(function(modelBlock, index){
            tmpTable.removeRow(index);
          });
        } else if (tmpCheck.value[0] == "2") {
          ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tmpFin1, false);
          ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tmpFin2, true);
          ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmpTable, false);
          tmpTable.modelBlocks.forEach(function(modelBlock, index){
            tmpTable.removeRow(index);
          });
        } else {
          ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tmpFin1, false);
          ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tmpFin2, false);
          ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmpTable, true);
          if (tmpTable.modelBlocks.length == 0) {
            tmpTable.createRow();
          }
        }
      });

    }

    // Служебная записка о привлечении к работе в ночное время
    if (model.formCode == "Служебная_записка_о_привлечении_к_работе_в_ночное_время") {
      var cmpTable = {ru: "t", kz: "t_k"};
      var tmpFin1 = {ru: "finish_date", kz: "finish_date_k"};
      var tmpFin2 = {ru: "finish_date1", kz: "finish_date1_k"};

      var tmpTable = model.getModelWithId(cmpTable.ru);
      tmpTable.modelBlocks.forEach(function(t, index) {
        var i = index;
        // С-ПО
        ALL_DOCUMENTS_UTIL.compareDates(view, "start_date", "finish_date", null, cmpTable, t.tableBlockIndex);

        // С-по\по событию
        var tmpCheck = model.getModelWithId("choice", "t", t.tableBlockIndex);
        if (tmpCheck.value[0] == "1") {
          setTimeout(function() {
            if (view.getViewWithId(tmpFin1.ru, "t", i)) view.getViewWithId(tmpFin1.ru, "t", i).setVisible(true);
            if (view.getViewWithId(tmpFin1.kz, "t_k", i)) view.getViewWithId(tmpFin1.kz, "t_k", i).setVisible(true);
            if (view.getViewWithId(tmpFin2.ru, "t", i)) view.getViewWithId(tmpFin2.ru, "t", i).setVisible(false);
            if (view.getViewWithId(tmpFin2.kz, "t_k", i)) view.getViewWithId(tmpFin2.kz, "t_k", i).setVisible(false);
          }, 0);
        } else if (tmpCheck.value[0] == "2") {
          setTimeout(function(){
            if (view.getViewWithId(tmpFin1.ru, "t", i)) view.getViewWithId(tmpFin1.ru, "t", i).setVisible(false);
            if (view.getViewWithId(tmpFin1.kz, "t_k", i)) view.getViewWithId(tmpFin1.kz, "t_k", i).setVisible(false);
            if (view.getViewWithId(tmpFin2.ru, "t", i)) view.getViewWithId(tmpFin2.ru, "t", i).setVisible(true);
            if (view.getViewWithId(tmpFin2.kz, "t_k", i)) view.getViewWithId(tmpFin2.kz, "t_k", i).setVisible(true);
          }, 0);
        } else {
          setTimeout(function(){
            if (view.getViewWithId(tmpFin1.ru, "t", i)) view.getViewWithId(tmpFin1.ru, "t", i).setVisible(false);
            if (view.getViewWithId(tmpFin1.kz, "t_k", i)) view.getViewWithId(tmpFin1.kz, "t_k", i).setVisible(false);
            if (view.getViewWithId(tmpFin2.ru, "t", i)) view.getViewWithId(tmpFin2.ru, "t", i).setVisible(false);
            if (view.getViewWithId(tmpFin2.kz, "t_k", i)) view.getViewWithId(tmpFin2.kz, "t_k", i).setVisible(false);
          }, 0);
        }
        tmpCheck.on("valueChange", function() {
          if (tmpCheck.value[0] == "1") {
            setTimeout(function(){
              if (view.getViewWithId(tmpFin1.ru, "t", i)) view.getViewWithId(tmpFin1.ru, "t", i).setVisible(true);
              if (view.getViewWithId(tmpFin1.kz, "t_k", i)) view.getViewWithId(tmpFin1.kz, "t_k", i).setVisible(true);
              if (view.getViewWithId(tmpFin2.ru, "t", i)) view.getViewWithId(tmpFin2.ru, "t", i).setVisible(false);
              if (view.getViewWithId(tmpFin2.kz, "t_k", i)) view.getViewWithId(tmpFin2.kz, "t_k", i).setVisible(false);
              model.getModelWithId(tmpFin2.ru, "t", t.tableBlockIndex).asfProperty.required=false;
              model.getModelWithId(tmpFin2.kz, "t_k", t.tableBlockIndex).asfProperty.required=false;
              model.getModelWithId(tmpFin2.ru, "t", t.tableBlockIndex).setValue(null);
              model.getModelWithId(tmpFin2.kz, "t_k", t.tableBlockIndex).setValue(null);
            }, 0);
          } else {
            setTimeout(function(){
              if (view.getViewWithId(tmpFin1.ru, "t", i)) view.getViewWithId(tmpFin1.ru, "t", i).setVisible(false);
              if (view.getViewWithId(tmpFin1.kz, "t_k", i)) view.getViewWithId(tmpFin1.kz, "t_k", i).setVisible(false);
              if (view.getViewWithId(tmpFin2.ru, "t", i)) view.getViewWithId(tmpFin2.ru, "t", i).setVisible(true);
              if (view.getViewWithId(tmpFin2.kz, "t_k", i)) view.getViewWithId(tmpFin2.kz, "t_k", i).setVisible(true);
              model.getModelWithId(tmpFin2.ru, "t", t.tableBlockIndex).asfProperty.required=false;
              model.getModelWithId(tmpFin2.kz, "t_k", t.tableBlockIndex).asfProperty.required=false;
              model.getModelWithId(tmpFin1.ru, "t", t.tableBlockIndex).setValue(null);
            }, 0);
          }
        });
      });

      if (view.editable) {
        tmpTable.on('tableRowAdd', function(evt, modelTab, modelRow) {
          // С-ПО
          ALL_DOCUMENTS_UTIL.compareDates(view, "start_date", "finish_date", null, cmpTable, modelRow[0].asfProperty.tableBlockIndex);

          // С-по\по событию
          var tmpCheck = model.getModelWithId("choice", "t", modelRow[0].asfProperty.tableBlockIndex);
          if (tmpCheck.value[0] == "1") {
            setTimeout(function() {
              if (view.getViewWithId(tmpFin1.ru, "t", modelRow[0].asfProperty.tableBlockIndex)) view.getViewWithId(tmpFin1.ru, "t", modelRow[0].asfProperty.tableBlockIndex).setVisible(true);
              if (view.getViewWithId(tmpFin1.kz, "t_k", modelRow[0].asfProperty.tableBlockIndex)) view.getViewWithId(tmpFin1.kz, "t_k", modelRow[0].asfProperty.tableBlockIndex).setVisible(true);
              if (view.getViewWithId(tmpFin2.ru, "t", modelRow[0].asfProperty.tableBlockIndex)) view.getViewWithId(tmpFin2.ru, "t", modelRow[0].asfProperty.tableBlockIndex).setVisible(false);
              if (view.getViewWithId(tmpFin2.kz, "t_k", modelRow[0].asfProperty.tableBlockIndex)) view.getViewWithId(tmpFin2.kz, "t_k", modelRow[0].asfProperty.tableBlockIndex).setVisible(false);
            }, 0);
          } else if (tmpCheck.value[0] == "2") {
            setTimeout(function(){
              if (view.getViewWithId(tmpFin1.ru, "t", modelRow[0].asfProperty.tableBlockIndex)) view.getViewWithId(tmpFin1.ru, "t", modelRow[0].asfProperty.tableBlockIndex).setVisible(false);
              if (view.getViewWithId(tmpFin1.kz, "t_k", modelRow[0].asfProperty.tableBlockIndex)) view.getViewWithId(tmpFin1.kz, "t_k", modelRow[0].asfProperty.tableBlockIndex).setVisible(false);
              if (view.getViewWithId(tmpFin2.ru, "t", modelRow[0].asfProperty.tableBlockIndex)) view.getViewWithId(tmpFin2.ru, "t", modelRow[0].asfProperty.tableBlockIndex).setVisible(true);
              if (view.getViewWithId(tmpFin2.kz, "t_k", modelRow[0].asfProperty.tableBlockIndex)) view.getViewWithId(tmpFin2.kz, "t_k", modelRow[0].asfProperty.tableBlockIndex).setVisible(true);
            }, 0);
          } else {
            setTimeout(function(){
              if (view.getViewWithId(tmpFin1.ru, "t", modelRow[0].asfProperty.tableBlockIndex)) view.getViewWithId(tmpFin1.ru, "t", modelRow[0].asfProperty.tableBlockIndex).setVisible(false);
              if (view.getViewWithId(tmpFin1.kz, "t_k", modelRow[0].asfProperty.tableBlockIndex)) view.getViewWithId(tmpFin1.kz, "t_k", modelRow[0].asfProperty.tableBlockIndex).setVisible(false);
              if (view.getViewWithId(tmpFin2.ru, "t", modelRow[0].asfProperty.tableBlockIndex)) view.getViewWithId(tmpFin2.ru, "t", modelRow[0].asfProperty.tableBlockIndex).setVisible(false);
              if (view.getViewWithId(tmpFin2.kz, "t_k", modelRow[0].asfProperty.tableBlockIndex)) view.getViewWithId(tmpFin2.kz, "t_k", modelRow[0].asfProperty.tableBlockIndex).setVisible(false);
            }, 0);
          }
          tmpCheck.on("valueChange", function() {
            if (tmpCheck.value[0] == "1") {
              setTimeout(function(){
                if (view.getViewWithId(tmpFin1.ru, "t", modelRow[0].asfProperty.tableBlockIndex)) view.getViewWithId(tmpFin1.ru, "t", modelRow[0].asfProperty.tableBlockIndex).setVisible(true);
                if (view.getViewWithId(tmpFin1.kz, "t_k", modelRow[0].asfProperty.tableBlockIndex)) view.getViewWithId(tmpFin1.kz, "t_k", modelRow[0].asfProperty.tableBlockIndex).setVisible(true);
                if (view.getViewWithId(tmpFin2.ru, "t", modelRow[0].asfProperty.tableBlockIndex)) view.getViewWithId(tmpFin2.ru, "t", modelRow[0].asfProperty.tableBlockIndex).setVisible(false);
                if (view.getViewWithId(tmpFin2.kz, "t_k", modelRow[0].asfProperty.tableBlockIndex)) view.getViewWithId(tmpFin2.kz, "t_k", modelRow[0].asfProperty.tableBlockIndex).setVisible(false);
                model.getModelWithId(tmpFin2.ru, "t", modelRow[0].asfProperty.tableBlockIndex).asfProperty.required=false;
                model.getModelWithId(tmpFin2.kz, "t_k", modelRow[0].asfProperty.tableBlockIndex).asfProperty.required=false;
                model.getModelWithId(tmpFin2.ru, "t", modelRow[0].asfProperty.tableBlockIndex).setValue(null);
                model.getModelWithId(tmpFin2.kz, "t_k", modelRow[0].asfProperty.tableBlockIndex).setValue(null);
              }, 0);
            } else {
              setTimeout(function(){
                if (view.getViewWithId(tmpFin1.ru, "t", modelRow[0].asfProperty.tableBlockIndex)) view.getViewWithId(tmpFin1.ru, "t", modelRow[0].asfProperty.tableBlockIndex).setVisible(false);
                if (view.getViewWithId(tmpFin1.kz, "t_k", modelRow[0].asfProperty.tableBlockIndex)) view.getViewWithId(tmpFin1.kz, "t_k", modelRow[0].asfProperty.tableBlockIndex).setVisible(false);
                if (view.getViewWithId(tmpFin2.ru, "t", modelRow[0].asfProperty.tableBlockIndex)) view.getViewWithId(tmpFin2.ru, "t", modelRow[0].asfProperty.tableBlockIndex).setVisible(true);
                if (view.getViewWithId(tmpFin2.kz, "t_k", modelRow[0].asfProperty.tableBlockIndex)) view.getViewWithId(tmpFin2.kz, "t_k", modelRow[0].asfProperty.tableBlockIndex).setVisible(true);
                model.getModelWithId(tmpFin2.ru, "t", modelRow[0].asfProperty.tableBlockIndex).asfProperty.required=false;
                model.getModelWithId(tmpFin2.kz, "t_k", modelRow[0].asfProperty.tableBlockIndex).asfProperty.required=false;
                model.getModelWithId(tmpFin1.ru, "t", modelRow[0].asfProperty.tableBlockIndex).setValue(null);
              }, 0);
            }
          });

        });
      }
    }

    // "Служебная записка на замещение временно  отсутствующего работника без установления доплаты";
    // "Служебная записка на замещение временно отсутствующего  работника с установлением доплаты";
    if (model.formCode == "Служебная_записка_на_замещение_временно__отсутствующего_работника_без_установления_доплаты"
    || model.formCode == "Служебная_записка_на_замещение_временно_отсутствующего_работника_с_установлением_доплаты") {
      // С-по\по событию
      var tmpCheck = model.getModelWithId("choice8");
      var tmpFin1 = {ru: "finish_date", kz: "finish_date_k"};
      var tmpFin2 = {ru: "finish_date1", kz: "finish_date1_k"};
      if (tmpCheck.value[0] == "1") {
        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tmpFin1, true);
        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tmpFin2, false);
      } else if (tmpCheck.value[0] == "2") {
        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tmpFin1, false);
        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tmpFin2, true);
      } else {
        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tmpFin1, false);
        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tmpFin2, false);
      }
      tmpCheck.on("valueChange", function() {
        if (tmpCheck.value[0] == "1") {
          ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tmpFin1, true);
          ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tmpFin2, false);
          model.getModelWithId(tmpFin2.ru).asfProperty.required=false;
          model.getModelWithId(tmpFin2.kz).asfProperty.required=false;
          model.getModelWithId(tmpFin2.ru).setValue(null);
          model.getModelWithId(tmpFin2.kz).setValue(null);
        } else if (tmpCheck.value[0] == "2") {
          ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tmpFin1, false);
          ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tmpFin2, true);
          model.getModelWithId(tmpFin1.ru).asfProperty.required=false;
          model.getModelWithId(tmpFin1.kz).asfProperty.required=false;
          model.getModelWithId(tmpFin1.ru).setValue(null);
        }
      });
    }

    // "Служебная записка на замещение временно отсутствующего  работника с установлением доплаты",
    // "Служебная записка о расширении зоны",
    if (model.formCode == "Служебная_записка_на_замещение_временно_отсутствующего_работника_с_установлением_доплаты") {
      // «тарифная ставка», «должностной оклад»
      var positionUser = model.getModelWithId("position");
      positionUser.on("valueChange", function() {
        if (positionUser.value.length > 0) ALL_DOCUMENTS_UTIL.setValueChoiceFromPosition(view, positionUser.value[0].elementID, ["choice10"]);
      });
    }

    // "Служебная записка о расширении зоны",
    if (model.formCode == "Служебная_записка_о_расширении_зоны") {
      var cmpTable = {ru: "t4", kz: "t4_k"};
      var tmpFin1 = {ru: "finish_date", kz: "finish_date_k"};
      var tmpFin2 = {ru: "finish_date1", kz: "finish_date1_k"};

      setTimeout(function() {
        var dateTableView = view.getViewWithId(cmpTable.ru);
        dateTableView.getViewBlocks().forEach(function(viewBlock, index){
          var tableBlockIndex =  viewBlock.views[0].model.asfProperty.tableBlockIndex;
          // С-по\по событию
          OTPUSK_UTILS.setWithOnByEvent(view, "choice8", tmpFin1, tmpFin2, cmpTable, tableBlockIndex);
          // С-ПО
          ALL_DOCUMENTS_UTIL.compareDates(view, "start_date", "finish_date", null, cmpTable, tableBlockIndex);
        });
      }, 0);

      var dateTableModel = model.getModelWithId(cmpTable.ru);
      if (view.editable) {
        dateTableModel.on('tableRowAdd', function(evt, modelTab, modelRow) {
          // С-ПО
          ALL_DOCUMENTS_UTIL.compareDates(view, "start_date", "finish_date", null, cmpTable, modelRow[0].asfProperty.tableBlockIndex);
          // С-по\по событию
          OTPUSK_UTILS.setWithOnByEvent(view, "choice8", tmpFin1, tmpFin2, cmpTable, modelRow[0].asfProperty.tableBlockIndex);
        });
      }

      // «тарифная ставка», «должностной оклад»
      var positionUser = model.getModelWithId("position");
      positionUser.on("valueChange", function() {
        if (positionUser.value.length > 0) ALL_DOCUMENTS_UTIL.setValueChoiceFromPosition(view, positionUser.value[0].elementID, ["choice10", "choice1"]);
      });

    }

    // Служебная записка об установлении доплаты за совмещение должностей / профессий
    if (model.formCode == "Служебная_записка_об_установлении_доплаты_за_совмещение_должностей_/_профессий") {
      // «тарифная ставка», «должностной оклад»
      var positionUser = model.getModelWithId("change_position");
      positionUser.on("valueChange", function() {
        if (positionUser.value.length > 0) ALL_DOCUMENTS_UTIL.setValueChoiceFromPosition(view, positionUser.value[0].elementID, ["choice10"]);
      });
    }

    // Служебная записка об отзыве из трудового отпуска
    if (model.formCode == "Служебная_записка_об_отзыве_из_отпуска") {
      var tmpCheck = model.getModelWithId("choice");
      var tmpFin1 = {ru: "choice2", kz: "choice2_k"};
      if (tmpCheck.value[0] == "1") {
        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tmpFin1, true);
      } else {
        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tmpFin1, false);
      }
      tmpCheck.on("valueChange", function() {
        if (tmpCheck.value[0] == "1") {
          ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tmpFin1, true);
        } else {
          ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tmpFin1, false);
        }
      });
      // С-ПО
      ALL_DOCUMENTS_UTIL.compareDates(view, "start_date", "finish_date", "days", null, null);
    }

    // 042 Служебная записка о направлении на профессиональное обучение
    if (model.formCode == "Служебная_записка_о_направлении_на_профессиональное_обучение") {
      // количество работников
      var table = model.getModelWithId("t");
      table.on('tableRowAdd', function(evt, modelTab, modelRow) {
        model.getModelWithId("itogo").setValue(view.getViewWithId("t").getBlocksCount()+'');
      });
      table.on('tableRowDelete', function(evt, rowModel, rowIndex, removedRow) {
        model.getModelWithId("itogo").setValue(view.getViewWithId("t").getBlocksCount()+'');
      });
    }

  }
});
