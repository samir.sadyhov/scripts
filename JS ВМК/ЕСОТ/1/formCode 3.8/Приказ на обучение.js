AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {

  //"Приказ на обучение без командировочных расходов", "Приказ на обучение с выплатой командировочных расходов"
  if (model.formCode == "Приказ_на_обучение_без_командировочных_расходов"
  || model.formCode == "Приказ_о_внесении_изменений_в_приказ_на_обучение_без_командировочных_расходов"
  || model.formCode == "Приказ_на_обучение_с_выплатой_командировочных_расходов"
  || model.formCode == "Приказ_о_внесении_изменений_в_приказ_о_направлении_на_профессиональное_обучение_с_выплатой_командировочных_расходов") {
    model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
      // ознакомление
      ALL_DOCUMENTS_UTIL.copyCmpValue(view, ["user"], ["user3"], "table2_r", "table5_r");
      // основание (синхронизация блоков)
      ALL_DOCUMENTS_UTIL.copyCmpValue(view, [], [], "table2_r", "table4_r");
    });

    var cmpTable = {ru: "table2_r", kz: "table2_k"};
    var tmpTable = model.getModelWithId(cmpTable.ru);

    setTimeout(function() {
      var dateTableView = view.getViewWithId(cmpTable.ru);
      dateTableView.getViewBlocks().forEach(function(viewBlock, index){
        var tableBlockIndex = viewBlock.views[0].model.asfProperty.tableBlockIndex;
        // С-ПО
        ALL_DOCUMENTS_UTIL.compareDates(view, "start_date", "finish_date", null, cmpTable, tableBlockIndex);
      });
    }, 0);
    if (view.editable) {
      tmpTable.on('tableRowAdd', function(evt, modelTab, modelRow) {
        // С-ПО
        ALL_DOCUMENTS_UTIL.compareDates(view, "start_date", "finish_date", null, cmpTable, modelRow[0].asfProperty.tableBlockIndex);
      });
    }

    // основание
    var osnovanieTable = model.getModelWithId("table4_r");

    osnovanieTable.modelBlocks.forEach(function(t) {
      var szLink = model.getModelWithId("sz_doc", "table4_r", t.tableBlockIndex);
      var dsLink = model.getModelWithId("dogovor", "table4_r", t.tableBlockIndex);
      szLink.on("valueChange", function() {
        if (szLink.getValue()) {
          var sluzhAsfData = ALL_DOCUMENTS_UTIL.getAsfDataJsonDocID(szLink.getValue());
          var numberSZ = null;
          if (sluzhAsfData) numberSZ = ALL_DOCUMENTS_UTIL.getAsfDataObject(sluzhAsfData, 'doc_num');
          if (numberSZ) model.getModelWithId("sz_num", "table4_r", t.tableBlockIndex).setValue(numberSZ.value);
        } else {
          model.getModelWithId("sz_num", "table4_r", t.tableBlockIndex).setValue(null);
        }
      });
      dsLink.on("valueChange", function() {
        if (dsLink.getValue()) {
          var dogovorAsfData = ALL_DOCUMENTS_UTIL.getAsfDataJsonDocID(dsLink.getValue());
          var numberDS = null;
          if (dogovorAsfData) {
            numberDS = ALL_DOCUMENTS_UTIL.getAsfDataObject(dogovorAsfData, 'doc_num');
          }
          if (numberDS) model.getModelWithId("dogovor_num", "table4_r", t.tableBlockIndex).setValue(numberDS.value);
        } else {
          model.getModelWithId("dogovor_num", "table4_r", t.tableBlockIndex).setValue(null);
        }
      });
    });

    osnovanieTable.on('tableRowAdd', function(evt, modelTab, modelRow) {
      var szLink = model.getModelWithId("sz_doc", "table4_r", modelRow[0].asfProperty.tableBlockIndex);
      var dsLink = model.getModelWithId("dogovor", "table4_r", modelRow[0].asfProperty.tableBlockIndex);
      szLink.on("valueChange", function() {
        if (szLink.getValue()) {
          var sluzhAsfData = ALL_DOCUMENTS_UTIL.getAsfDataJsonDocID(szLink.getValue());
          var numberSZ = null;
          if (sluzhAsfData) numberSZ = ALL_DOCUMENTS_UTIL.getAsfDataObject(sluzhAsfData, 'doc_num');
          if (numberSZ) model.getModelWithId("sz_num", "table4_r", modelRow[0].asfProperty.tableBlockIndex).setValue(numberSZ.value);
        } else {
          model.getModelWithId("sz_num", "table4_r", modelRow[0].asfProperty.tableBlockIndex).setValue(null);
        }
      });
      dsLink.on("valueChange", function() {
        if (dsLink.getValue()) {
          var dogovorAsfData = ALL_DOCUMENTS_UTIL.getAsfDataJsonDocID(dsLink.getValue());
          var numberDS = null;
          if (dogovorAsfData) {
            numberDS = ALL_DOCUMENTS_UTIL.getAsfDataObject(dogovorAsfData, 'doc_num');
          }
          if (numberDS) model.getModelWithId("dogovor_num", "table4_r", modelRow[0].asfProperty.tableBlockIndex).setValue(numberDS.value);
        } else {
          model.getModelWithId("dogovor_num", "table4_r", modelRow[0].asfProperty.tableBlockIndex).setValue(null);
        }
      });
    });

  }

});
