AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function (event, model, view) {
    if (model.formCode.substring(0, 16).toLowerCase() == "трудовой_договор") {
        model.on(AS.FORMS.EVENT_TYPE.dataLoad, function () {
            console.log('formName: [' + model.formName + ']\nformCode: [' + model.formCode + ']\nformID: [' + model.formId + ']\nasfDataID: [' + model.asfDataId + ']');
        });

        var cmp = {
            From: ["user3", "user3", "user3", "user3", "user3", "doc_num", "doc_date", "inn", "address"],
            To: ["user6", "user9", "user10", "user12", "user15", "number1", "date7", "number3", "address1"],
            Choice: "choice3",
            IIN: "inn",
            User: "user3",
            Organization: "organization_name",
            numUD: "number",
            dateUD: "date3"
        };

        var oraganization = ALL_DOCUMENTS_UTIL.getOrganizationData();


        //Трудовой NEW START
        if (model.formCode == "Трудовой_договор1") {
            model.on(AS.FORMS.EVENT_TYPE.dataLoad, function () {
                // синхронизация
                var cmpArrTD = [{ru: "user14", kz: "user14_k"}, {ru: "number5", kz: "number5_k"}, {
                    ru: "address2",
                    kz: "address2_k"
                }, {ru: "information23", kz: "information23_k"}, {ru: "choice9", kz: "choice9_k"}, {
                    ru: "date11",
                    kz: "date11_k"
                }];
                cmpArrTD.forEach(function (cmp) {
                    ALL_DOCUMENTS_UTIL.changedValue(model, cmp);
                });
            });

            // наименование организации, где работает работник
            if (view.editable) {
                if (oraganization) {
                    model.getModelWithId("department2").setValue({
                        departmentId: oraganization.departmentID,
                        id: oraganization.departmentID,
                        departmentName: oraganization.nameRu,
                        name: oraganization.nameRu,
                        tagName: oraganization.nameRu,
                        hasChildren: true
                    });
                    model.getModelWithId("department10").setValue({
                        departmentId: oraganization.departmentID,
                        id: oraganization.departmentID,
                        departmentName: oraganization.nameRu,
                        name: oraganization.nameRu,
                        tagName: oraganization.nameRu,
                        hasChildren: true
                    });
                }
            }

            if (oraganization) {
                if (model.getModelWithId("ruk_user")) model.getModelWithId("ruk_user").setValue(oraganization.nameRu);
                if (model.getModelWithId("ruk_user2")) model.getModelWithId("ruk_user2").setValue(oraganization.nameRu);
            }

            cmp.From = ["user1", "user1", "user1", "user1", "user1", "user1", "doc_num", "doc_date", "inn", "address", "organization_name", "organization_name", "department2", "user666", "user666"];
            cmp.To = ["user6", "user_work", "user9", "user10", "user11", "user14", "number7", "date10", "number5", "address2", "ruk_user", "ruk_user2", "department10", "user5", "user12"];
            cmp.Choice = "choice1";
            //cmp.Organization = "department";
            cmp.User = "user1";
            cmp.dateUD = "date";

            var newPosition = model.getModelWithId('position1');
            var uslovieTrudaGlobal = '1';
            var socialStatusGlobal = '1';
            newPosition.on("valueChange", function () {
                if (newPosition.value.length > 0) {
                    var posData = ALL_DOCUMENTS_UTIL.getCardPositionData(newPosition.value[0].elementID, "Карточка_должности");
                    posData.forEach(function (d) {
                        if (d.id == 'uslovie_truda') {
                            uslovieTrudaGlobal = d.key;
                            sociallyFunction();
                            if (uslovieTrudaGlobal == '2' || uslovieTrudaGlobal == '3' || uslovieTrudaGlobal == '4') {
                                showTables(['t40', 't41']);
                            } else {
                                hideTables(['t40', 't41']);
                            }
                        }
                    });
                }
            });

            var hideTables = function (tableIds) {
                tableIds.forEach(function (cmp) {
                    // удаляем все блоки таблицы
                    var tmpTable = model.getModelWithId(cmp);
                    if (tmpTable.modelBlocks.length > 0) {
                        tmpTable.modelBlocks.forEach(function (modelBlock, index) {
                            tmpTable.removeRow(index);
                        });
                    }
                    // скрываем таблицу
                    ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, false);
                });
            };

            var showTables = function (tableIds, tableEnable) {
                tableIds.forEach(function (cmp) {
                    ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, true);
                    // создаем один блок
                    var tmpTable = model.getModelWithId(cmp);
                    if (tmpTable.modelBlocks.length === 0) {
                        tmpTable.createRow();
                    }
                    if (!tableEnable) {
                        ALL_DOCUMENTS_UTIL.tableEnabled(view, cmp);
                    }
                });
            };

            var sociallyFunction = function () {
                if (uslovieTrudaGlobal == '1' && socialStatusGlobal == '1') {
                    showTables(['t30'], true);
                    hideTables(['t11', 't31']);

                    AS.FORMS.Model.prototype.getErrors = function () {
                        var errors = [];
                        if (this.asfProperty.required) {
                            var isEmpty = this.isEmpty();
                            if (isEmpty) {
                                errors.push({id: this.asfProperty.id, errorCode: AS.FORMS.INPUT_ERROR_TYPE.emptyValue});
                            }
                        }

                        if (this.asfProperty.id == 'days2' && this.value.trim() == 'дней.') {
                            AS.SERVICES.showErrorMessage("Заполните количество дней");
                            errors.push({id: this.asfProperty.id, errorCode: AS.FORMS.INPUT_ERROR_TYPE.emptyValue});
                        }

                        if (this.model.getSpecialErrors) {
                            var specialErrors = this.model.getSpecialErrors();
                            if (specialErrors) {
                                if (specialErrors instanceof Array) {
                                    errors = errors.concat(specialErrors);
                                } else {
                                    errors.push(specialErrors);
                                }
                            }
                        }

                        if (!errors.isEmpty()) {
                            this.trigger(AS.FORMS.EVENT_TYPE.markInvalid, [this.model, this.value]);
                        }
                        return errors;
                    };

                } else if (socialStatusGlobal == '2') {
                    showTables(['t30'], true);
                    showTables(['t31']);
                    hideTables(['t11']);

                    AS.FORMS.Model.prototype.getErrors = function () {
                        var errors = [];
                        if (this.asfProperty.required) {
                            var isEmpty = this.isEmpty();
                            if (isEmpty) {
                                errors.push({id: this.asfProperty.id, errorCode: AS.FORMS.INPUT_ERROR_TYPE.emptyValue});
                            }
                        }

                        if (this.asfProperty.id == 'days2' && this.value.trim() == 'дней.') {
                            AS.SERVICES.showErrorMessage("Заполните количество дней");
                            errors.push({id: this.asfProperty.id, errorCode: AS.FORMS.INPUT_ERROR_TYPE.emptyValue});
                        }

                        if (this.asfProperty.id == 'days1' && this.value.trim() == 'дней;') {
                            AS.SERVICES.showErrorMessage("Заполните количество дней");
                            errors.push({id: this.asfProperty.id, errorCode: AS.FORMS.INPUT_ERROR_TYPE.emptyValue});
                        }

                        if (this.model.getSpecialErrors) {
                            var specialErrors = this.model.getSpecialErrors();
                            if (specialErrors) {
                                if (specialErrors instanceof Array) {
                                    errors = errors.concat(specialErrors);
                                } else {
                                    errors.push(specialErrors);
                                }
                            }
                        }

                        if (!errors.isEmpty()) {
                            this.trigger(AS.FORMS.EVENT_TYPE.markInvalid, [this.model, this.value]);
                        }

                        return errors;
                    };

                } else if ((uslovieTrudaGlobal == '2' || uslovieTrudaGlobal == '3' || uslovieTrudaGlobal == '4') && socialStatusGlobal == '1') {
                    showTables(['t11']);
                    showTables(['t30'], true);
                    hideTables(['t31']);

                    AS.FORMS.Model.prototype.getErrors = function () {
                        var errors = [];
                        if (this.asfProperty.required) {
                            var isEmpty = this.isEmpty();
                            if (isEmpty) {
                                errors.push({id: this.asfProperty.id, errorCode: AS.FORMS.INPUT_ERROR_TYPE.emptyValue});
                            }
                        }

                        if (this.asfProperty.id == 'days' && this.value.trim() == 'дней;') {
                            AS.SERVICES.showErrorMessage("Заполните количество дней");
                            errors.push({id: this.asfProperty.id, errorCode: AS.FORMS.INPUT_ERROR_TYPE.emptyValue});
                        }

                        if (this.asfProperty.id == 'days1' && this.value.trim() == 'дней;') {
                            AS.SERVICES.showErrorMessage("Заполните количество дней");
                            errors.push({id: this.asfProperty.id, errorCode: AS.FORMS.INPUT_ERROR_TYPE.emptyValue});
                        }

                        if (this.model.getSpecialErrors) {
                            var specialErrors = this.model.getSpecialErrors();
                            if (specialErrors) {
                                if (specialErrors instanceof Array) {
                                    errors = errors.concat(specialErrors);
                                } else {
                                    errors.push(specialErrors);
                                }
                            }
                        }

                        if (!errors.isEmpty()) {
                            this.trigger(AS.FORMS.EVENT_TYPE.markInvalid, [this.model, this.value]);
                        }

                        return errors;
                    };
                }
            };

            // супер-пупер логика по супер-пупер новому договору
            var userBox = model.getModelWithId("user1");
            userBox.on("valueChange", function () {
                if (userBox.getValue().length > 0) {
                    var userCardData = ALL_DOCUMENTS_UTIL.getCardsData("rest/api/personalrecord/forms/" + userBox.getValue()[0].personID, "Штатная_расстановка111");
                    var pol = ALL_DOCUMENTS_UTIL.getAsfDataObject(userCardData, 'sex');
                    var socialStatus = ALL_DOCUMENTS_UTIL.getAsfDataObject(userCardData, 'social_status');

                    if (socialStatus) {
                        socialStatusGlobal = socialStatus.key;
                    }

                    // console.log("---------- user data ----------");
                    // console.log(pol);
                    // console.log(socialStatus);
                    // console.log("---------- user data ----------");

                    var cmpArr1 = {stat: ["Punkt3", "Punkt6", "Punkt13"], table: ["tt3", "tt5", "t7"]};
                    var cmpArr2 = {stat: ["Punkt4", "Punkt7", "Punkt14"], table: ["tt4", "tt6", "t17", "t9"]};
                    if (pol) {
                        if (pol.key == '1') {
                            cmpArr1.stat.forEach(function (cmp) {
                                ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, false);
                            });
                            cmpArr2.stat.forEach(function (cmp) {
                                ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, true);
                            });
                            hideTables(cmpArr1.table);
                            showTables(cmpArr2.table);
                        } else if (pol.key == "2") {
                            cmpArr2.stat.forEach(function (cmp) {
                                ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, false);
                            });
                            cmpArr1.stat.forEach(function (cmp) {
                                ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, true);
                            });
                            hideTables(cmpArr2.table);
                            showTables(cmpArr1.table);
                        } else {
                            var undefinedSex = {
                                statHide: ["Punkt4", "Punkt7", "Punkt14", "Punkt3", "Punkt6", "Punkt13"],
                                tableHide: ["tt4", "tt6", "tt3", "tt5", "t7", "t9", "t17"]
                            };
                            undefinedSex.statHide.forEach(function (cmp) {
                                ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, false);
                            });
                            hideTables(undefinedSex.tableHide);
                        }
                    }

                    var statusCmps = {status1: ["t11"], status2: ["t31", "t12"]};

                    if (socialStatus && socialStatus.key == "2") {
                        showTables(statusCmps.status2);
                        hideTables(statusCmps.status1);

                        model.getModelWithId("Punkt").setValue("4.12.");
                        model.getModelWithId("Punkt1").setValue("4.13.");
                        model.getModelWithId("Punkt2").setValue("4.14.");
                        model.getModelWithId("Punkt5").setValue("4.16.");
                        model.getModelWithId("Punkt8").setValue("4.18.");
                        model.getModelWithId("Punkt9").setValue("4.19.");
                        model.getModelWithId("Punkt10").setValue("4.20.");
                        model.getModelWithId("Punkt11").setValue("4.21.");

                        if (pol.key == '1') {
                            model.getModelWithId("Punkt7", "tt6", 0).setValue("4.17.");
                            model.getModelWithId("Punkt4", "tt4", 0).setValue("4.15.");
                        } else if (pol.key == '2') {
                            model.getModelWithId("Punkt3", "tt3", 0).setValue("4.15.");
                            model.getModelWithId("Punkt6", "tt5", 0).setValue("4.17.");
                        }
                    } else {
                        showTables(statusCmps.status1);
                        hideTables(statusCmps.status2);

                        model.getModelWithId("Punkt").setValue("4.11.");
                        model.getModelWithId("Punkt1").setValue("4.12.");
                        model.getModelWithId("Punkt2").setValue("4.13.");
                        model.getModelWithId("Punkt5").setValue("4.15.");
                        model.getModelWithId("Punkt8").setValue("4.17.");
                        model.getModelWithId("Punkt9").setValue("4.18.");
                        model.getModelWithId("Punkt10").setValue("4.19.");
                        model.getModelWithId("Punkt11").setValue("4.20.");

                        if (pol.key == '1') {
                            model.getModelWithId("Punkt7", "tt6", 0).setValue("4.16.");
                            model.getModelWithId("Punkt4", "tt4", 0).setValue("4.14.");
                        } else if (pol.key == '2') {
                            model.getModelWithId("Punkt3", "tt3", 0).setValue("4.14.");
                            model.getModelWithId("Punkt6", "tt5", 0).setValue("4.16.");
                        }
                    }
                    sociallyFunction();
                }
            });

            var choice0 = model.getModelWithId("choice0");
            var choice0Views = {work: ["t24", "t25", "t26"], pos: ["t21", "t22", "t23"]};
            choice0.on("valueChange", function (event, cmpModel, value) {
                if (value == "1") {
                    choice0Views.work.forEach(function (cmp) {
                        var tmpTable = model.getModelWithId(cmp);
                        if (tmpTable.modelBlocks.length > 0) {
                            tmpTable.modelBlocks.forEach(function (modelBlock, index) {
                                tmpTable.removeRow(index);
                            });
                        }
                        // скрываем таблицу
                        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, false);
                    });

                    choice0Views.pos.forEach(function (cmp) {
                        // показываем таблицу
                        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, true);
                        // создаем один блок
                        var tmpTable = model.getModelWithId(cmp);
                        if (tmpTable.modelBlocks.length === 0) {
                            tmpTable.createRow();
                        }
                        ALL_DOCUMENTS_UTIL.tableEnabled(view, cmp);
                    });
                } else if (value == "2") {
                    choice0Views.pos.forEach(function (cmp) {
                        var tmpTable = model.getModelWithId(cmp);
                        if (tmpTable.modelBlocks.length > 0) {
                            tmpTable.modelBlocks.forEach(function (modelBlock, index) {
                                tmpTable.removeRow(index);
                            });
                        }
                        // скрываем таблицу
                        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, false);
                    });

                    choice0Views.work.forEach(function (cmp) {
                        // показываем таблицу
                        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, true);
                        // создаем один блок
                        var tmpTable = model.getModelWithId(cmp);
                        if (tmpTable.modelBlocks.length === 0) {
                            tmpTable.createRow();
                        }
                        ALL_DOCUMENTS_UTIL.tableEnabled(view, cmp);
                    });
                }
            });

            var choice1 = model.getModelWithId("choice1");
            choice1.on("valueChange", function (event, cmpModel, value) {
                if (value == "1") {
                    var choice1Views = {show: ["t13", "t15", "t17"], hide: ["t16", "t18", "t19", "t28"]};
                    choice1Views.hide.forEach(function (cmp) {
                        var tmpTable = model.getModelWithId(cmp);
                        if (tmpTable.modelBlocks.length > 0) {
                            tmpTable.modelBlocks.forEach(function (modelBlock, index) {
                                tmpTable.removeRow(index);
                            });
                        }
                        // скрываем таблицу
                        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, false);
                    });

                    choice1Views.show.forEach(function (cmp) {
                        // показываем таблицу
                        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, true);
                        // создаем один блок
                        var tmpTable = model.getModelWithId(cmp);
                        if (tmpTable.modelBlocks.length === 0) {
                            tmpTable.createRow();
                        }
                        ALL_DOCUMENTS_UTIL.tableEnabled(view, cmp);
                    });

                    // выпиливаем из таблицы t17 2 строки
                    setTimeout(function() {
                      var dateTableView = view.getViewWithId('t17');
                      dateTableView.getViewBlocks().forEach(function(viewBlock, index){
                        var tableBlockIndex =  viewBlock.views[0].model.asfProperty.tableBlockIndex;
                        if (view.getViewWithId('text23', 't17', tableBlockIndex)) view.getViewWithId('text23', 't17', tableBlockIndex).container.parent().hide();
                        if (view.getViewWithId('date4', 't17', tableBlockIndex)) view.getViewWithId('date4', 't17', tableBlockIndex).container.parent().hide();
                      });
                    }, 0);

                    model.getModelWithId("Punkt15").setValue("7.11.");
                    model.getModelWithId("Punkt16").setValue("7.12.");
                    model.getModelWithId("Punkt17").setValue("7.13.");
                    model.getModelWithId("Punkt18").setValue("7.14.");
                    model.getModelWithId("Punkt19").setValue("7.15.");

                } else if (value == "3") {
                    var choice1Views = {show: ["t19"], hide: ["t9", "t13", "t15", "t16", "t17", "t18", "t28"]};
                    choice1Views.hide.forEach(function (cmp) {
                        var tmpTable = model.getModelWithId(cmp);
                        if (tmpTable.modelBlocks.length > 0) {
                            tmpTable.modelBlocks.forEach(function (modelBlock, index) {
                                tmpTable.removeRow(index);
                            });
                        }
                        // скрываем таблицу
                        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, false);
                    });

                    choice1Views.show.forEach(function (cmp) {
                        // показываем таблицу
                        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, true);
                        // создаем один блок
                        var tmpTable = model.getModelWithId(cmp);
                        if (tmpTable.modelBlocks.length === 0) {
                            tmpTable.createRow();
                        }
                        ALL_DOCUMENTS_UTIL.tableEnabled(view, cmp);
                    });
                    model.getModelWithId("Punkt15").setValue("7.9.");
                    model.getModelWithId("Punkt16").setValue("7.10.");
                    model.getModelWithId("Punkt17").setValue("7.11.");
                    model.getModelWithId("Punkt18").setValue("7.12.");
                    model.getModelWithId("Punkt19").setValue("7.13.");
                } else if (value == "4") {
                    var choice1Views = {show: "t28", hide: ["t9", "t13", "t15", "t16", "t17", "t18", "t19"]};
                    choice1Views.hide.forEach(function (cmp) {
                        var tmpTable = model.getModelWithId(cmp);
                        if (tmpTable.modelBlocks.length > 0) {
                            tmpTable.modelBlocks.forEach(function (modelBlock, index) {
                                tmpTable.removeRow(index);
                            });
                        }
                        // скрываем таблицу
                        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, false);
                    });
                    // показываем таблицу
                    ALL_DOCUMENTS_UTIL.setVisibleCmp(view, choice1Views.show, true);
                    // создаем один блок
                    var tmpTable = model.getModelWithId(choice1Views.show);
                    if (tmpTable.modelBlocks.length === 0) {
                        tmpTable.createRow();
                    }
                    ALL_DOCUMENTS_UTIL.tableEnabled(view, choice1Views.show);

                    model.getModelWithId("Punkt15").setValue("7.9.");
                    model.getModelWithId("Punkt16").setValue("7.10.");
                    model.getModelWithId("Punkt17").setValue("7.11.");
                    model.getModelWithId("Punkt18").setValue("7.12.");
                    model.getModelWithId("Punkt19").setValue("7.13.");
                } else if (value == "5" || value == "2") {
                    var choice1Views = {hide: ["t9", "t13", "t15", "t16", "t17", "t18", "t19", "t28"]};
                    choice1Views.hide.forEach(function (cmp) {
                        var tmpTable = model.getModelWithId(cmp);
                        if (tmpTable.modelBlocks.length > 0) {
                            tmpTable.modelBlocks.forEach(function (modelBlock, index) {
                                tmpTable.removeRow(index);
                            });
                        }
                        // скрываем таблицу
                        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, false);
                    });

                    model.getModelWithId("Punkt15").setValue("7.8.");
                    model.getModelWithId("Punkt16").setValue("7.9.");
                    model.getModelWithId("Punkt17").setValue("7.10.");
                    model.getModelWithId("Punkt18").setValue("7.11.");
                    model.getModelWithId("Punkt19").setValue("7.12.");
                }
            });

            var tableTModel = model.getModelWithId("t");
            tableTModel.on('tableRowDelete', function () {
                model.getModelWithId("Punkt21").setValue("3.1.22");
                model.getModelWithId("Punkt22").setValue("3.1.23");
                model.getModelWithId("Punkt23").setValue("3.1.24");
                model.getModelWithId("Punkt24").setValue("3.1.25");
                model.getModelWithId("Punkt25").setValue("3.1.26");
                model.getModelWithId("Punkt26").setValue("3.1.27");
                model.getModelWithId("Punkt27").setValue("3.1.28");
            });

            tableTModel.on('tableRowAdd', function () {
                model.getModelWithId("Punkt21").setValue("3.1.23");
                model.getModelWithId("Punkt22").setValue("3.1.24");
                model.getModelWithId("Punkt23").setValue("3.1.25");
                model.getModelWithId("Punkt24").setValue("3.1.26");
                model.getModelWithId("Punkt25").setValue("3.1.27");
                model.getModelWithId("Punkt26").setValue("3.1.28");
                model.getModelWithId("Punkt27").setValue("3.1.29");
            });

            var tableTT7Model = model.getModelWithId("tt7");
            tableTT7Model.on('tableRowDelete', function () {
                model.getModelWithId("Punkt35").setValue("2");
                model.getModelWithId("Punkt36").setValue("3");
                model.getModelWithId("Punkt37").setValue("4");
                model.getModelWithId("Punkt38").setValue("5");
                model.getModelWithId("Punkt39").setValue("6");
                model.getModelWithId("Punkt40").setValue("7");
                model.getModelWithId("Punkt41").setValue("8");
                model.getModelWithId("Punkt42").setValue("9");
                model.getModelWithId("Punkt43").setValue("10");
                model.getModelWithId("Punkt44").setValue("11");
            });

            tableTT7Model.on('tableRowAdd', function () {
                model.getModelWithId("Punkt35").setValue("3");
                model.getModelWithId("Punkt36").setValue("4");
                model.getModelWithId("Punkt37").setValue("5");
                model.getModelWithId("Punkt38").setValue("6");
                model.getModelWithId("Punkt39").setValue("7");
                model.getModelWithId("Punkt40").setValue("8");
                model.getModelWithId("Punkt41").setValue("9");
                model.getModelWithId("Punkt42").setValue("10");
                model.getModelWithId("Punkt43").setValue("11");
                model.getModelWithId("Punkt44").setValue("12");
            });

            var user2Model = model.getModelWithId("user2");
            user2Model.on('valueChange', function (event, cmpModel, value) {
                model.getModelWithId("user", "t28", 0).setValue(value);
            });
        }
        //Трудовой NEW END


        if (model.formCode == "Трудовой_договор_(актуальная_версия)3") {
            cmp.From = ["user3", "user3", "user3", "user3", "user3", "doc_num", "doc_date", "iin", "address"];
            cmp.To = ["user6", "user9", "user20", "user12", "user15", "number1", "date7", "number3", "address1"];
            cmp.IIN = "iin";
            cmp.numUD = "id";
        }

        // Наименование организации
        if (oraganization) {
            if (model.getModelWithId(cmp.Organization)) model.getModelWithId(cmp.Organization).setValue(oraganization.nameRu);
        }

        // заполнение данных юзера с ЛК
        model.getModelWithId(cmp.User).on("valueChange", function () {
            // № удостоверение личности
            ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Штатная_расстановка111", cmp.User, "numberofudlichnosti", cmp.numUD, null, null);
            // выдано кем
            ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Штатная_расстановка111", cmp.User, "organofud", "vydano", null, null);
            // дата выдачи
            ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Штатная_расстановка111", cmp.User, "dateofud", cmp.dateUD, null, null);
            // ИИН
            ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Штатная_расстановка111", cmp.User, "iin", cmp.IIN, null, null);
            // адрес
            ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Штатная_расстановка111", cmp.User, "fact_address", "address", null, null);
        });

        // копирование значений
        ALL_DOCUMENTS_UTIL.copyCmpValue(view, cmp.From, cmp.To);

        // С-ПО
        ALL_DOCUMENTS_UTIL.compareDates(view, "date1", "date2", null, null, null);

        // логика с переключателями
        var activeCmp = [];
        var removeCmp = [];
        var choice1 = model.getModelWithId(cmp.Choice);
        if (choice1.getValue()[0] == "1") {
            activeCmp = ["date1", "date2"];
            removeCmp = ["opisanie", "user2", "position2", "opisanie1"];
            activeCmp.forEach(function (cmp) {
                ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, true);
            });
            removeCmp.forEach(function (cmp) {
                ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, false);
                model.getModelWithId(cmp).setValue(null);
            });
        } else if (choice1.getValue()[0] == "2") {
            activeCmp = ["date1"];
            removeCmp = ["date2", "opisanie", "user2", "position2", "opisanie1"];
            activeCmp.forEach(function (cmp) {
                ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, true);
            });
            removeCmp.forEach(function (cmp) {
                ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, false);
                model.getModelWithId(cmp).setValue(null);
            });
        } else if (choice1.getValue()[0] == "3") {
            activeCmp = ["opisanie"];
            removeCmp = ["date1", "date2", "user2", "position2", "opisanie1"];
            activeCmp.forEach(function (cmp) {
                ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, true);
            });
            removeCmp.forEach(function (cmp) {
                ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, false);
                model.getModelWithId(cmp).setValue(null);
            });
        } else if (choice1.getValue()[0] == "4") {
            activeCmp = ["user2", "position2"];
            removeCmp = ["date1", "date2", "opisanie", "opisanie1"];
            activeCmp.forEach(function (cmp) {
                ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, true);
            });
            removeCmp.forEach(function (cmp) {
                ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, false);
                model.getModelWithId(cmp).setValue(null);
            });
        } else if (choice1.getValue()[0] == "5") {
            activeCmp = ["opisanie1"];
            removeCmp = ["date1", "date2", "opisanie", "user2", "position2"];
            activeCmp.forEach(function (cmp) {
                ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, true);
            });
            removeCmp.forEach(function (cmp) {
                ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, false);
                model.getModelWithId(cmp).setValue(null);
            });
        } else if (choice1.getValue()[0] == "6") {
            activeCmp = [];
            removeCmp = ["date1", "date2", "opisanie", "user2", "position2", "opisanie1"];
            removeCmp.forEach(function (cmp) {
                ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, false);
                model.getModelWithId(cmp).setValue(null);
            });
        }
        choice1.on("valueChange", function () {
            if (choice1.getValue()[0] == "1") {
                activeCmp = ["date1", "date2"];
                removeCmp = ["opisanie", "user2", "position2", "opisanie1"];
                activeCmp.forEach(function (cmp) {
                    ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, true);
                });
                removeCmp.forEach(function (cmp) {
                    ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, false);
                    model.getModelWithId(cmp).setValue(null);
                });
            } else if (choice1.getValue()[0] == "2") {
                activeCmp = ["date1"];
                removeCmp = ["date2", "opisanie", "user2", "position2", "opisanie1"];
                activeCmp.forEach(function (cmp) {
                    ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, true);
                });
                removeCmp.forEach(function (cmp) {
                    ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, false);
                    model.getModelWithId(cmp).setValue(null);
                });
            } else if (choice1.getValue()[0] == "3") {
                activeCmp = ["opisanie"];
                removeCmp = ["date1", "date2", "user2", "position2", "opisanie1"];
                activeCmp.forEach(function (cmp) {
                    ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, true);
                });
                removeCmp.forEach(function (cmp) {
                    ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, false);
                    model.getModelWithId(cmp).setValue(null);
                });
            } else if (choice1.getValue()[0] == "4") {
                activeCmp = ["user2", "position2"];
                removeCmp = ["date1", "date2", "opisanie", "opisanie1"];
                activeCmp.forEach(function (cmp) {
                    ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, true);
                });
                removeCmp.forEach(function (cmp) {
                    ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, false);
                    model.getModelWithId(cmp).setValue(null);
                });
            } else if (choice1.getValue()[0] == "5") {
                activeCmp = ["opisanie1"];
                removeCmp = ["date1", "date2", "opisanie", "user2", "position2"];
                activeCmp.forEach(function (cmp) {
                    ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, true);
                });
                removeCmp.forEach(function (cmp) {
                    ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, false);
                    model.getModelWithId(cmp).setValue(null);
                });
            } else if (choice1.getValue()[0] == "6") {
                activeCmp = [];
                removeCmp = ["date1", "date2", "opisanie", "user2", "position2", "opisanie1"];
                removeCmp.forEach(function (cmp) {
                    ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, false);
                    model.getModelWithId(cmp).setValue(null);
                });
            }
        });
    }
});
