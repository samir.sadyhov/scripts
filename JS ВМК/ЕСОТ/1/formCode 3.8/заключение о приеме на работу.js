AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  if (model.formCode == "Представление_о_приеме_на_работу_1") {

    var cmpArrZakl = [
      {ru: "user", kz: "user_k"},
      {ru: "user_position", kz: "user_position_k"},
      {ru: "user_department", kz: "user_department_k"},
      {ru: "ruk_position", kz: "ruk_position_k"},
      {ru: "ruk_position1", kz: "ruk_position1_k"},
      {ru: "cmp-rxsfhg", kz: "cmp-zz4mjv"}, /*Выберите квалификационные требования:*/
      {ru: "k_education_level", kz: "k_education_level_k"},
      {ru: "k_education_year1", kz: "k_education_year1_k"},
      {ru: "dzo1", kz: "dzo1_k"},
      {ru: "k_experience_year1", kz: "k_experience_year1_k"},
      {ru: "k_experience_year2", kz: "k_experience_year2_k"},
      {ru: "dzo2", kz: "dzo2_k"}, /*Дополнительные требования к стажу работы (Принятые в)*/
      {ru: "dzo3", kz: "dzo3_k"}, /*Требования к наличию лицензий, сертификатов и др. (Принятые в)*/
      {ru: "dzo4", kz: "dzo4_k"}, /*Профессиональные компетенции (знания, необходимые для исполнения обязанностей) (Принятые в)*/
      {ru: "dzo5", kz: "dzo5_k"}, /*Навыки, необходимые для исполнения обязанностей (Принятые в)*/
      {ru: "dzo6", kz: "dzo6_k"}, /*Личностные (деловые) компетенции (Принятые в)*/
      {ru: "ispolnitel_user", kz: "ispolnitel_user_k"},
      {ru: "ispolnitel_position", kz: "ispolnitel_position_k"},
      {ru: "ispolnitel_date", kz: "ispolnitel_date_k"},
      {ru: "podpisant_user", kz: "podpisant_user_k"},
      {ru: "podpisant_position", kz: "podpisant_position_k"},
      {ru: "podpisant_date", kz: "podpisant_date_k"},
      {ru: "oznakomlen_user", kz: "oznakomlen_user_k"},
      {ru: "oznakomlen_date", kz: "oznakomlen_date_k"}
    ];

    var tableArrZakl = [
      {id: {ru: "workers", kz: "workers_k"}, cmp: [{ru: "workers_department", kz: "workers_department_k"}, {ru: "workers_number", kz: "workers_number_k"}]},
      {id: {ru: "education_table", kz: "education_table_k"}, cmp: [{ru: "education_level", kz: "education_level_k"}, {ru: "education_year", kz: "education_year_k"}, {ru: "education_year1", kz: "education_year1_k"}, {ru: "cmp-np5uj6", kz: "cmp-1cq6l4"}, {ru: "education_result", kz: "education_result_k"}]},
      {id: {ru: "d_education_table", kz: "d_education_table_k"}, cmp: [{ru: "d_education_level", kz: "d_education_level_k"}, {ru: "d_education_year", kz: "d_education_year_k"}, {ru: "d_education_year1", kz: "d_education_year1_k"}, {ru: "d_education_result", kz: "d_education_result_k"}]},
      {id: {ru: "experience_table", kz: "experience_table_k"}, cmp: [{ru: "experience_year1", kz: "experience_year1_k"}, {ru: "experience_year2", kz: "experience_year2_k"}]},
      {id: {ru: "d_experience_table", kz: "d_experience_table_k"}, cmp: [{ru: "d_experience_year1", kz: "d_experience_year1_k"}, {ru: "d_experience_year1_result", kz: "d_experience_year1_result_k"}, {ru: "d_experience_year2", kz: "d_experience_year2_k"}, {ru: "d_experience_year2_result", kz: "d_experience_year2_result_k"}]},
      {id: {ru: "k_certificate_table", kz: "k_certificate_table_k"}, cmp: []},
      {id: {ru: "certificate_table", kz: "certificate_table_k"}, cmp: []},
      {id: {ru: "d_certificate_table", kz: "d_certificate_table_k"}, cmp: [{ru: "d_certificate_result", kz: "d_certificate_result_k"}]},
      {id: {ru: "k_workknowledge_table", kz: "k_workknowledge_table_k"}, cmp: []},
      {id: {ru: "workknowledge_table", kz: "workknowledge_table_k"}, cmp: []},
      {id: {ru: "d_workknowledge_table", kz: "d_workknowledge_table_k"}, cmp: [{ru: "d_workknowledge_result", kz: "d_workknowledge_result_k"}]},
      {id: {ru: "k_duties_table", kz: "k_duties_table_k"}, cmp: []},
      {id: {ru: "duties_table", kz: "duties_table_k"}, cmp: []},
      {id: {ru: "d_duties_table", kz: "d_duties_table_k"}, cmp: [{ru: "d_duties_result", kz: "d_duties_result_k"}]},
      {id: {ru: "k_competence_table", kz: "k_competence_table_k"}, cmp: []},
      {id: {ru: "competence_table", kz: "competence_table_k"}, cmp: []},
      {id: {ru: "d_competence_table", kz: "d_competence_table_k"}, cmp: [{ru: "d_competence_result", kz: "d_competence_result_k"}]}
    ];

    model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
      console.log(model.formName + '\nformCode: [' + model.formCode + ']\nformID: [' + model.formId +']\nasfDataID: [' + model.asfDataId+']');
      tableArrZakl.forEach(function(table) {
        ALL_DOCUMENTS_UTIL.tableOn(view, table.id, table.cmp);
      });
      cmpArrZakl.forEach(function(cmp) {
        ALL_DOCUMENTS_UTIL.changedValue(model, cmp);
      });
    });

    // Наименование организации
    var oraganization = ALL_DOCUMENTS_UTIL.getOrganizationData();

    if (view.editable) {
      tableArrZakl.forEach(function(table) {
        ALL_DOCUMENTS_UTIL.tableEnabled(view, table.id.kz); // блокируем дин.таблицы в каз. варианте
      });

      var dzoCmpArr = [
        {ru: "dzo1", kz: "dzo1_k"},
        {ru: "dzo2", kz: "dzo2_k"}, /*Дополнительные требования к стажу работы (Принятые в)*/
        {ru: "dzo3", kz: "dzo3_k"}, /*Требования к наличию лицензий, сертификатов и др. (Принятые в)*/
        {ru: "dzo4", kz: "dzo4_k"}, /*Профессиональные компетенции (знания, необходимые для исполнения обязанностей) (Принятые в)*/
        {ru: "dzo5", kz: "dzo5_k"}, /*Навыки, необходимые для исполнения обязанностей (Принятые в)*/
        {ru: "dzo6", kz: "dzo6_k"} /*Личностные (деловые) компетенции (Принятые в)*/
      ];

      dzoCmpArr.forEach(function(cmp) {
        model.getModelWithId(cmp.ru).setValue({
          departmentId: oraganization.departmentID,
          id: oraganization.departmentID,
          departmentName: oraganization.nameRu,
          name: oraganization.nameRu,
          tagName: oraganization.nameRu,
          hasChildren: true
        });
        model.getModelWithId(cmp.kz).setValue({
          departmentId: oraganization.departmentID,
          id: oraganization.departmentID,
          departmentName: oraganization.nameKz,
          name: oraganization.nameKz,
          tagName: oraganization.nameKz,
          hasChildren: true
        });
      });

    }

  }
});
