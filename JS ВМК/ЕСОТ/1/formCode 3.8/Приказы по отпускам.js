var OTPUSK_UTILS = {
  vsegoKalendarDays: function(model, table, block) {
    var vsegoOsnOtp=model.getModelWithId("main", table, block);
    var vsegoDopOtp=model.getModelWithId("additional", table, block);
    var vsegoDays=model.getModelWithId("days", table, block);
    if (vsegoOsnOtp && vsegoDopOtp && vsegoDays) {
      vsegoOsnOtp.on("valueChange", function() {
        if (vsegoOsnOtp.getValue() != undefined && vsegoDopOtp.getValue() != undefined) {
          vsegoDays.setValue(Number(vsegoOsnOtp.getValue())+Number(vsegoDopOtp.getValue())+"");
        } else if (vsegoOsnOtp.getValue() != undefined && vsegoDopOtp.getValue() == undefined) {
          vsegoDays.setValue(vsegoOsnOtp.getValue());
        } else if (vsegoOsnOtp.getValue() == undefined && vsegoDopOtp.getValue() != undefined) {
          vsegoDays.setValue(vsegoDopOtp.getValue());
        } else {
          vsegoDays.setValue(null);
        }
      });
      vsegoDopOtp.on("valueChange", function() {
        if (vsegoOsnOtp.getValue() != undefined && vsegoDopOtp.getValue() != undefined) {
          vsegoDays.setValue(Number(vsegoOsnOtp.getValue())+Number(vsegoDopOtp.getValue())+"");
        } else if (vsegoOsnOtp.getValue() != undefined && vsegoDopOtp.getValue() == undefined) {
          vsegoDays.setValue(vsegoOsnOtp.getValue());
        } else if (vsegoOsnOtp.getValue() == undefined && vsegoDopOtp.getValue() != undefined) {
          vsegoDays.setValue(vsegoDopOtp.getValue());
        } else {
          vsegoDays.setValue(null);
        }
      });
    }
  },

  proverkaDatStartFinish: function(view, cmpTable, block) {
    ALL_DOCUMENTS_UTIL.compareDates(view, "start_period", "end_period", null, cmpTable, block);
    ALL_DOCUMENTS_UTIL.compareDates(view, "date_start", "date_finish", null, cmpTable, block);
    ALL_DOCUMENTS_UTIL.compareDates(view, "date_main", "date_additional", "main", cmpTable, block);
    ALL_DOCUMENTS_UTIL.compareDates(view, "date_last", "date_last_po", "additional", cmpTable, block);
  },

  kolichestvoDostupnyhOstatok: function(model, table, block) {
    var userBox = model.getModelWithId("user", table, block);
    userBox.on("valueChange", function() {
      if (userBox.getValue().length > 0) {
        var userCardData = ALL_DOCUMENTS_UTIL.getCardsData("rest/api/personalrecord/forms/" + userBox.getValue()[0].personID, "Отпуска");
        var dostupDays = ALL_DOCUMENTS_UTIL.getAsfDataObject(userCardData, "sum_available_vacat_days");
        var ostatokDays = ALL_DOCUMENTS_UTIL.getAsfDataObject(userCardData, "sum_vacat_days");
        if (dostupDays) model.getModelWithId("days_available", table, block).setValue(dostupDays.value);
        if (ostatokDays) model.getModelWithId("remainder", table, block).setValue(ostatokDays.value);
      }
    });
  },

  proverkaDatOtpuskovSetUnikalnoe: function(model, view, table, block) {
    // проверка дат осн. и доп. отпусков
    var dateOsnS=model.getModelWithId("date_main", table.ru, block);
    var dateOsnP=model.getModelWithId("date_additional", table.ru, block);
    var dateDopS=model.getModelWithId("date_last", table.ru, block);
    var dateDopP=model.getModelWithId("date_last_po", table.ru, block);
    var unikalnoeDate=model.getModelWithId("unikalnoe_date_po", table.ru, block);

    if (dateOsnS && dateOsnP && dateDopS && dateDopP && unikalnoeDate) {
      // заполнение уникального
      dateOsnP.on("valueChange", function() {
        dateDopS.setValue(null);
        if (AS.FORMS.DateUtils.parseDate(dateDopP.getValue()) !=null) {
          unikalnoeDate.setValue(dateDopP.getValue());
        } else if (AS.FORMS.DateUtils.parseDate(dateOsnP.getValue()) !=null) {
          unikalnoeDate.setValue(dateOsnP.getValue());
        } else {
          dateOsnP.setValue(null);
        }
      });
      dateDopP.on("valueChange", function() {
        if (AS.FORMS.DateUtils.parseDate(dateDopP.getValue()) !=null) {
          unikalnoeDate.setValue(dateDopP.getValue());
        } else if (AS.FORMS.DateUtils.parseDate(dateOsnP.getValue()) !=null) {
          unikalnoeDate.setValue(dateOsnP.getValue());
        }
      });
      // дата дополнительного С не больше даты основного ПО
      dateDopS.on("valueChange", function() {
        if (AS.FORMS.DateUtils.parseDate(dateOsnP.getValue()) !=null) {
          var tmpD1=AS.FORMS.DateUtils.parseDate(dateOsnP.getValue());
          tmpD1.setDate(tmpD1.getDate() + 1);
          var tmpD2=AS.FORMS.DateUtils.parseDate(dateDopS.getValue());
          if (AS.FORMS.DateUtils.parseDate(dateDopS.getValue()) !=null) {
            if (tmpD2.getTime() != tmpD1.getTime()) {
              if (view.getViewWithId("date_last", table.ru, block)) view.getViewWithId("date_last", table.ru, block).container.parent().css("background-color", "#FF9999");
              ALL_DOCUMENTS_UTIL.msgShow("Дата дополнительного отпуска должна быть больше даты окончания основного отпуска на 1 день.");
              if (view.getViewWithId("date_last_k", table.kz, block)) view.getViewWithId("date_last_k", table.kz, block).container.parent().css("background-color", "#FF9999");
              ALL_DOCUMENTS_UTIL.msgShow("Дата дополнительного отпуска должна быть больше даты окончания основного отпуска на 1 день.");
              dateDopS.setValue(null);
            } else {
              if (view.getViewWithId("date_last", table.ru, block)) view.getViewWithId("date_last", table.ru, block).container.parent().css("background-color", "");
              if (view.getViewWithId("date_last_k", table.kz, block)) view.getViewWithId("date_last_k", table.kz, block).container.parent().css("background-color", "");
            }
          }
          if (view.getViewWithId("date_additional", table.ru, block)) view.getViewWithId("date_additional", table.ru, block).container.parent().css("background-color", "");
          if (view.getViewWithId("date_additional_k", table.kz, block)) view.getViewWithId("date_additional_k", table.kz, block).container.parent().css("background-color", "");
        } else {
          ALL_DOCUMENTS_UTIL.msgShow("Некорректное значение даты окончания основного отпуска");
          if (view.getViewWithId("date_additional", table.ru, block)) view.getViewWithId("date_additional", table.ru, block).container.parent().css("background-color", "#FF9999");
          if (view.getViewWithId("date_additional_k", table.kz, block)) view.getViewWithId("date_additional_k", table.kz, block).container.parent().css("background-color", "#FF9999");
        }
      });
    }
  },

  setWithOnByEvent: function(playerView, choice, tmpFin1, tmpFin2, cmpTable, tableBlockIndex) {
    var checkPo = playerView.model.getModelWithId(choice, cmpTable.ru, tableBlockIndex);
    if (checkPo.value[0] == "1") {
      if (playerView.getViewWithId(tmpFin1.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(tmpFin1.ru, cmpTable.ru, tableBlockIndex).setVisible(true);
      if (playerView.getViewWithId(tmpFin1.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(tmpFin1.kz, cmpTable.kz, tableBlockIndex).setVisible(true);
      if (playerView.getViewWithId(tmpFin2.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(tmpFin2.ru, cmpTable.ru, tableBlockIndex).setVisible(false);
      if (playerView.getViewWithId(tmpFin2.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(tmpFin2.kz, cmpTable.kz, tableBlockIndex).setVisible(false);
    } else if (checkPo.value[0] == "2") {
      if (playerView.getViewWithId(tmpFin1.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(tmpFin1.ru, cmpTable.ru, tableBlockIndex).setVisible(false);
      if (playerView.getViewWithId(tmpFin1.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(tmpFin1.kz, cmpTable.kz, tableBlockIndex).setVisible(false);
      if (playerView.getViewWithId(tmpFin2.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(tmpFin2.ru, cmpTable.ru, tableBlockIndex).setVisible(true);
      if (playerView.getViewWithId(tmpFin2.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(tmpFin2.kz, cmpTable.kz, tableBlockIndex).setVisible(true);
    } else if (checkPo.value[0] == "3") {
      if (playerView.getViewWithId(tmpFin1.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(tmpFin1.ru, cmpTable.ru, tableBlockIndex).setVisible(false);
      if (playerView.getViewWithId(tmpFin1.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(tmpFin1.kz, cmpTable.kz, tableBlockIndex).setVisible(false);
      if (playerView.getViewWithId(tmpFin2.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(tmpFin2.ru, cmpTable.ru, tableBlockIndex).setVisible(true);
      if (playerView.getViewWithId(tmpFin2.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(tmpFin2.kz, cmpTable.kz, tableBlockIndex).setVisible(true);
    } else {
      if (playerView.getViewWithId(tmpFin1.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(tmpFin1.ru, cmpTable.ru, tableBlockIndex).setVisible(false);
      if (playerView.getViewWithId(tmpFin1.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(tmpFin1.kz, cmpTable.kz, tableBlockIndex).setVisible(false);
      if (playerView.getViewWithId(tmpFin2.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(tmpFin2.ru, cmpTable.ru, tableBlockIndex).setVisible(false);
      if (playerView.getViewWithId(tmpFin2.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(tmpFin2.kz, cmpTable.kz, tableBlockIndex).setVisible(false);
    }
    checkPo.on("valueChange", function() {
      if (checkPo.value[0] == "1") {
        if (playerView.getViewWithId(tmpFin1.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(tmpFin1.ru, cmpTable.ru, tableBlockIndex).setVisible(true);
        if (playerView.getViewWithId(tmpFin1.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(tmpFin1.kz, cmpTable.kz, tableBlockIndex).setVisible(true);
        if (playerView.getViewWithId(tmpFin2.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(tmpFin2.ru, cmpTable.ru, tableBlockIndex).setVisible(false);
        if (playerView.getViewWithId(tmpFin2.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(tmpFin2.kz, cmpTable.kz, tableBlockIndex).setVisible(false);
        if (playerView.model.getModelWithId(tmpFin2.ru, cmpTable.ru, tableBlockIndex)) playerView.model.getModelWithId(tmpFin2.ru, cmpTable.ru, tableBlockIndex).asfProperty.required=false;
        if (playerView.model.getModelWithId(tmpFin2.kz, cmpTable.kz, tableBlockIndex)) playerView.model.getModelWithId(tmpFin2.kz, cmpTable.kz, tableBlockIndex).asfProperty.required=false;
        if (playerView.model.getModelWithId(tmpFin2.ru, cmpTable.ru, tableBlockIndex)) playerView.model.getModelWithId(tmpFin2.ru, cmpTable.ru, tableBlockIndex).setValue(null);
        if (playerView.model.getModelWithId(tmpFin2.kz, cmpTable.kz, tableBlockIndex)) playerView.model.getModelWithId(tmpFin2.kz, cmpTable.kz, tableBlockIndex).setValue(null);
      } else {
        if (playerView.getViewWithId(tmpFin1.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(tmpFin1.ru, cmpTable.ru, tableBlockIndex).setVisible(false);
        if (playerView.getViewWithId(tmpFin1.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(tmpFin1.kz, cmpTable.kz, tableBlockIndex).setVisible(false);
        if (playerView.getViewWithId(tmpFin2.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(tmpFin2.ru, cmpTable.ru, tableBlockIndex).setVisible(true);
        if (playerView.getViewWithId(tmpFin2.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(tmpFin2.kz, cmpTable.kz, tableBlockIndex).setVisible(true);
        if (playerView.model.getModelWithId(tmpFin2.ru, cmpTable.ru, tableBlockIndex)) playerView.model.getModelWithId(tmpFin2.ru, cmpTable.ru, tableBlockIndex).asfProperty.required=false;
        if (playerView.model.getModelWithId(tmpFin2.kz, cmpTable.kz, tableBlockIndex)) playerView.model.getModelWithId(tmpFin2.kz, cmpTable.kz, tableBlockIndex).asfProperty.required=false;
        if (playerView.model.getModelWithId(tmpFin1.ru, cmpTable.ru, tableBlockIndex)) playerView.model.getModelWithId(tmpFin1.ru, cmpTable.ru, tableBlockIndex).setValue(null);
        if (playerView.model.getModelWithId(tmpFin2.ru, cmpTable.ru, tableBlockIndex)) playerView.model.getModelWithId(tmpFin2.ru, cmpTable.ru, tableBlockIndex).setValue(null);
        if (playerView.model.getModelWithId(tmpFin2.kz, cmpTable.kz, tableBlockIndex)) playerView.model.getModelWithId(tmpFin2.kz, cmpTable.kz, tableBlockIndex).setValue(null);
      }
    });
  }

};

AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  // Приказы по отпускам
  if (model.formCode == "Приказ_о_предоставлении_оплачиваемого_отпуска_(с_материальной_помощью)"
  || model.formCode == "Приказ_о_предоставлении_оплачиваемого_отпуска_(без_материальной_помощью)"
  || model.formCode == "Приказ_о_предоставлении_оплачиваемого_отпуска_(без_материальной_помощью)1"
  || model.formCode == "Приказ_о_предоставлении_оплачиваемого_отпуска_(с_материальной_помощью)1") {

    model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
      // // табельный номер
      // ALL_DOCUMENTS_UTIL.copyCmpValue(view, ["tab_num"], ["tab_num1"], "t1", "t");
      // ознакомление
      ALL_DOCUMENTS_UTIL.copyCmpValue(view, ["user"], ["user3"], "t", "table5_r");
      // основание
      ALL_DOCUMENTS_UTIL.copyCmpValue(view, ["user"], ["osnovanie_user"], "t", "tab");
    });

    var cmpTable = {ru: "t", kz: "t_k"};
    var dateTable = model.getModelWithId(cmpTable.ru);

    setTimeout(function() {
      var dateTableView = view.getViewWithId(cmpTable.ru);
      dateTableView.getViewBlocks().forEach(function(viewBlock, index){
        var tableBlockIndex =  viewBlock.views[0].model.asfProperty.tableBlockIndex;
        // выпиливаем уникальное
        if (dateTableView.getViewWithId("unikalnoe_date_po", cmpTable.ru, tableBlockIndex)) {
          view.getViewWithId("unikalnoe_date_po", cmpTable.ru, tableBlockIndex).container.parent().parent().hide();
        }
        // Всего календарных дней
        OTPUSK_UTILS.vsegoKalendarDays(model, cmpTable.ru, tableBlockIndex);
        // С-ПО
        OTPUSK_UTILS.proverkaDatStartFinish(view, cmpTable, tableBlockIndex);
        // проверка дат отпусков, заполнение уникального
        OTPUSK_UTILS.proverkaDatOtpuskovSetUnikalnoe(model, view, cmpTable, tableBlockIndex);
        // Количество доступных и остаток за последний отпуск дней
        OTPUSK_UTILS.kolichestvoDostupnyhOstatok(model, cmpTable.ru, tableBlockIndex);
      });
    }, 0);

    if (view.editable) {
      dateTable.on('tableRowAdd', function(evt, modelTab, modelRow) {
        // С-ПО
        OTPUSK_UTILS.proverkaDatStartFinish(view, cmpTable, modelRow[0].asfProperty.tableBlockIndex);
        setTimeout(function() {
          // выпиливаем уникальное
          if (view.getViewWithId("unikalnoe_date_po", cmpTable.ru, modelRow[0].asfProperty.tableBlockIndex)) {
            view.getViewWithId("unikalnoe_date_po", cmpTable.ru, modelRow[0].asfProperty.tableBlockIndex).container.parent().parent().hide();
          }
          // проверка дат отпусков, заполнение уникального
          OTPUSK_UTILS.proverkaDatOtpuskovSetUnikalnoe(model, view, cmpTable, modelRow[0].asfProperty.tableBlockIndex);
        }, 0);
        // Всего календарных дней
        OTPUSK_UTILS.vsegoKalendarDays(model, cmpTable.ru, modelRow[0].asfProperty.tableBlockIndex);
        // Количество доступных и остаток за последний отпуск дней
        OTPUSK_UTILS.kolichestvoDostupnyhOstatok(model, cmpTable.ru, modelRow[0].asfProperty.tableBlockIndex);
      });
    }

  }

  // Приказ о предоставлении учебного отпуска
  if (model.formCode == "Приказ_о_предоставлении_учебного_отпуска"
   || model.formCode == "Приказ_о_внесении_изменений_приказа_о_предоставлении_учебного_отпуска11") {
    model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
      // ознакомление
      ALL_DOCUMENTS_UTIL.copyCmpValue(view, ["user"], ["user3"], "table2_r", "table5_r");
      // основание
      ALL_DOCUMENTS_UTIL.copyCmpValue(view, ["user"], ["z_user"], "table2_r", "table4_r");
    });
    var cmpTable = {ru: "table2_r", kz: "table2_k"};
    var dateTable = model.getModelWithId(cmpTable.ru);
    setTimeout(function() {
      var dateTableView = view.getViewWithId(cmpTable.ru);
      dateTableView.getViewBlocks().forEach(function(viewBlock, index){
        var tableBlockIndex =  viewBlock.views[0].model.asfProperty.tableBlockIndex;
        // С-ПО
        ALL_DOCUMENTS_UTIL.compareDates(view, "start_date", "finish_date", "days", cmpTable, tableBlockIndex);
      });
    }, 0);
    if (view.editable) {
      dateTable.on('tableRowAdd', function(evt, modelTab, modelRow) {
        // С-ПО
        ALL_DOCUMENTS_UTIL.compareDates(view, "start_date", "finish_date", "days", cmpTable, modelRow[0].asfProperty.tableBlockIndex);
      });
    }
  }

  // Приказ о предоставлении социального отпуска
  if (model.formCode == "Приказ_о_предоставлении_социального_отпуска"
   || model.formCode == "Приказ_о_внесении_изменений_приказа_о_предоставлении_социального_отпуска") {
    model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
      // ознакомление
      ALL_DOCUMENTS_UTIL.copyCmpValue(view, ["user"], ["user3"], "table2_r", "table5_r");
      // основание
      ALL_DOCUMENTS_UTIL.copyCmpValue(view, ["user"], ["z_user"], "table2_r", "table4_r");
    });
    var cmpTable = {ru: "table2_r", kz: "table2_r_k"};
    var dateTable = model.getModelWithId(cmpTable.ru);
    setTimeout(function() {
      var dateTableView = view.getViewWithId(cmpTable.ru);
      dateTableView.getViewBlocks().forEach(function(viewBlock, index){
        var tableBlockIndex =  viewBlock.views[0].model.asfProperty.tableBlockIndex;
        // С-ПО
        ALL_DOCUMENTS_UTIL.compareDates(view, "start_date", "finish_date", null, cmpTable, tableBlockIndex);
      });
    }, 0);
    if (view.editable) {
      dateTable.on('tableRowAdd', function(evt, modelTab, modelRow) {
        // С-ПО
        ALL_DOCUMENTS_UTIL.compareDates(view, "start_date", "finish_date", null, cmpTable, modelRow[0].asfProperty.tableBlockIndex);
      });
    }
  }

  // Приказ о предоставлении отпуска без сохранения заработной платы
  if (model.formCode == "Приказ_о_предоставлении_отпуска_без_сохранения_заработной_платы1"
  || model.formCode == "приказ_о_внесении_изменений_приказа_о_предоставлении_отпуска_без_сохранения_заработной_платы1") {
    model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
      // ознакомление
      ALL_DOCUMENTS_UTIL.copyCmpValue(view, ["user"], ["user3"], "t", "table5_r");
    });

    var cmpTable = {ru: "t", kz: "t_k"};
    var tmpFin1 = {ru: "finish_date", kz: "finish_date_k"};
    var tmpFin2 = {ru: "finish_date1", kz: "finish_date1_k"};

    setTimeout(function() {
      var dateTableView = view.getViewWithId(cmpTable.ru);
      dateTableView.getViewBlocks().forEach(function(viewBlock, index){
        var tableBlockIndex =  viewBlock.views[0].model.asfProperty.tableBlockIndex;

        // С-по\по событию
        //OTPUSK_UTILS.setWithOnByEvent(view, "choice", tmpFin1, tmpFin2, cmpTable, tableBlockIndex);
        // С-ПО
        ALL_DOCUMENTS_UTIL.compareDates(view, "start_date", "finish_date", "kol", cmpTable, tableBlockIndex);
        // основание
        var userBox = model.getModelWithId("user", cmpTable.ru, tableBlockIndex);
        userBox.on("valueChange", function() {
          model.getModelWithId("z_user", cmpTable.ru, tableBlockIndex).setValue(userBox.getValue());
        });

      });
    }, 0);

    var dateTableModel = model.getModelWithId(cmpTable.ru);
    if (view.editable) {
      dateTableModel.on('tableRowAdd', function(evt, modelTab, modelRow) {
        // С-по\по событию
        //OTPUSK_UTILS.setWithOnByEvent(view, "choice", tmpFin1, tmpFin2, cmpTable, modelRow[0].asfProperty.tableBlockIndex);
        // С-ПО
        ALL_DOCUMENTS_UTIL.compareDates(view, "start_date", "finish_date", "kol", cmpTable, modelRow[0].asfProperty.tableBlockIndex);
        // основание
        var userBox = model.getModelWithId("user", cmpTable.ru, modelRow[0].asfProperty.tableBlockIndex);
        userBox.on("valueChange", function() {
          model.getModelWithId("z_user", cmpTable.ru, modelRow[0].asfProperty.tableBlockIndex).setValue(userBox.getValue());
        });
      });
    }

  }

  // "Приказ о предоставлении отпуска по беременности и родам"
  if (model.formCode == "Приказ_о_предоставлении_отпуска_по_беременности_и_родам1"
   || model.formCode == "Приказ_о_внесении_изменений_в_приказ_о_предоставлении_отпуска_по_беременности_и_родам11") {
    model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
      // ознакомление
      ALL_DOCUMENTS_UTIL.copyCmpValue(view, ["user"], ["user3"], "t", "table5_r");
    });
    var cmpTable = {ru: "t", kz: "t_k"};
    var dateTable = model.getModelWithId(cmpTable.ru);
    setTimeout(function() {
      var dateTableView = view.getViewWithId(cmpTable.ru);
      dateTableView.getViewBlocks().forEach(function(viewBlock, index){
        var tableBlockIndex =  viewBlock.views[0].model.asfProperty.tableBlockIndex;
        // С-ПО
        ALL_DOCUMENTS_UTIL.compareDates(view, "start_date", "finish_date", null, cmpTable, tableBlockIndex);
      });
    }, 0);
    if (view.editable) {
      dateTable.on('tableRowAdd', function(evt, modelTab, modelRow) {
        // С-ПО
        ALL_DOCUMENTS_UTIL.compareDates(view, "start_date", "finish_date", null, cmpTable, modelRow[0].asfProperty.tableBlockIndex);
      });
    }
  }

  // Приказ об отзыве из трудового отпуска
  if (model.formCode == "Приказ_об_отзыве_из_трудового_отпуска"
   || model.formCode == "Приказ_об_отзыве_из_трудового_отпуска1") {
    model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
      // ознакомление
      ALL_DOCUMENTS_UTIL.copyCmpValue(view, ["user"], ["oznakomlenie_user"], "t", "ttt");
      // основание
      ALL_DOCUMENTS_UTIL.copyCmpValue(view, ["user"], ["osnovanie_user"], "t", "tab");
    });
    var cmpTable = {ru: "t", kz: "t_k"};
    var dateTable = model.getModelWithId(cmpTable.ru);
    setTimeout(function() {
      var dateTableView = view.getViewWithId(cmpTable.ru);
      dateTableView.getViewBlocks().forEach(function(viewBlock, index){
        var tableBlockIndex =  viewBlock.views[0].model.asfProperty.tableBlockIndex;
        // С-ПО
        ALL_DOCUMENTS_UTIL.compareDates(view, "start_date", "finish_date", "days", cmpTable, tableBlockIndex);
      });
    }, 0);
    if (view.editable) {
      dateTable.on('tableRowAdd', function(evt, modelTab, modelRow) {
        // С-ПО
        ALL_DOCUMENTS_UTIL.compareDates(view, "start_date", "finish_date", "days", cmpTable, modelRow[0].asfProperty.tableBlockIndex);
      });
    }
  }

  // Приказ об отзыве с выплатой компенсации
  if (model.formCode == "Приказ_об_отзыве_с_выплатой_компенсации") {
    // ознакомление, основание
    ALL_DOCUMENTS_UTIL.copyCmpValue(view, ["user", "user"], ["oznakomlenie_user", "osnovanie_user"]);
  }

  //Приказ о выходе на работу до истечения срока отпуска по уходу за ребенком
  if (model.formCode == "Приказ_о_выходе_на_работу") {
    // ознакомление
    ALL_DOCUMENTS_UTIL.copyCmpValue(view, ["user", "user"], ["user1", "user3"]);
  }

});
