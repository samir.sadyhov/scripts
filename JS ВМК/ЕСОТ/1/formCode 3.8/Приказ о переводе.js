AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  if (model.formCode == "Приказ_о_переводе"
   || model.formCode == "Приказ_о_внесении_изменений_в_приказ_о_переводе111") {
    var cmpTable = {ru: "t", kz: "t_k"};
    var tmpFin1 = {ru: "finish_date", kz: "finish_date_k"};
    var tmpFin2 = {ru: "finish_date1", kz: "finish_date1_k"};

    setTimeout(function() {
      var dateTableView = view.getViewWithId(cmpTable.ru);
      dateTableView.getViewBlocks().forEach(function(viewBlock, index){
        var tableBlockIndex = viewBlock.views[0].model.asfProperty.tableBlockIndex;

        // С-по\по событию
        OTPUSK_UTILS.setWithOnByEvent(view, "choice22", tmpFin1, tmpFin2, cmpTable, tableBlockIndex);
        var choice22 = model.getModelWithId("choice22", cmpTable.ru, tableBlockIndex);
        var cmpNaPeriod = [{ru: "user2", kz: "user2_k"}, {ru: "position1", kz: "position1_k"}, {ru: "department2", kz: "department2_k"}];
        if (choice22.value[0] == "3") {
          cmpNaPeriod.forEach(function(cmp) {
            if (view.getViewWithId(cmp.ru, cmpTable.ru, tableBlockIndex)) view.getViewWithId(cmp.ru, cmpTable.ru, tableBlockIndex).container.parent().parent().show();
            if (view.getViewWithId(cmp.kz, cmpTable.kz, tableBlockIndex)) view.getViewWithId(cmp.kz, cmpTable.kz, tableBlockIndex).container.parent().parent().show();
          });
        } else {
          cmpNaPeriod.forEach(function(cmp) {
            if (view.getViewWithId(cmp.ru, cmpTable.ru, tableBlockIndex)) view.getViewWithId(cmp.ru, cmpTable.ru, tableBlockIndex).container.parent().parent().hide();
            if (view.getViewWithId(cmp.kz, cmpTable.kz, tableBlockIndex)) view.getViewWithId(cmp.kz, cmpTable.kz, tableBlockIndex).container.parent().parent().hide();
          });
        }
        choice22.on("valueChange", function() {
          if (choice22.value[0] == "3") {
            cmpNaPeriod.forEach(function(cmp) {
              if (view.getViewWithId(cmp.ru, cmpTable.ru, tableBlockIndex)) view.getViewWithId(cmp.ru, cmpTable.ru, tableBlockIndex).container.parent().parent().show();
              if (view.getViewWithId(cmp.kz, cmpTable.kz, tableBlockIndex)) view.getViewWithId(cmp.kz, cmpTable.kz, tableBlockIndex).container.parent().parent().show();
            });
          } else {
            cmpNaPeriod.forEach(function(cmp) {
              if (view.getViewWithId(cmp.ru, cmpTable.ru, tableBlockIndex)) view.getViewWithId(cmp.ru, cmpTable.ru, tableBlockIndex).container.parent().parent().hide();
              if (view.getViewWithId(cmp.kz, cmpTable.kz, tableBlockIndex)) view.getViewWithId(cmp.kz, cmpTable.kz, tableBlockIndex).container.parent().parent().hide();
            });
          }
        });

        // С-ПО
        ALL_DOCUMENTS_UTIL.compareDates(view, "start_date", "finish_date", null, cmpTable, tableBlockIndex);
        // тарифная ставка, должностной оклад
        var positionUser = model.getModelWithId("change_position", cmpTable.ru, tableBlockIndex);
        positionUser.on("valueChange", function() {
          if (positionUser.value.length > 0) ALL_DOCUMENTS_UTIL.setValueChoiceFromPosition(view, positionUser.value[0].elementID, ["choice1"], cmpTable.ru, tableBlockIndex);
        });

        // выпиливание даты ПО если "постоянно"
        var checkVid = model.getModelWithId("choice3", cmpTable.ru, tableBlockIndex);
        if (checkVid.value[0] == "2") {
          if (view.getViewWithId(tmpFin1.ru, cmpTable.ru, tableBlockIndex)) view.getViewWithId(tmpFin1.ru, cmpTable.ru, tableBlockIndex).container.parent().parent().show();
          if (view.getViewWithId(tmpFin1.kz, cmpTable.kz, tableBlockIndex)) view.getViewWithId(tmpFin1.kz, cmpTable.kz, tableBlockIndex).container.parent().parent().show();
        } else {
          if (view.getViewWithId(tmpFin1.ru, cmpTable.ru, tableBlockIndex)) view.getViewWithId(tmpFin1.ru, cmpTable.ru, tableBlockIndex).container.parent().parent().hide();
          if (view.getViewWithId(tmpFin1.kz, cmpTable.kz, tableBlockIndex)) view.getViewWithId(tmpFin1.kz, cmpTable.kz, tableBlockIndex).container.parent().parent().hide();
        }
        checkVid.on("valueChange", function() {
          if (checkVid.value[0] == "2") {
            if (view.getViewWithId(tmpFin1.ru, cmpTable.ru, tableBlockIndex)) view.getViewWithId(tmpFin1.ru, cmpTable.ru, tableBlockIndex).container.parent().parent().show();
            if (view.getViewWithId(tmpFin1.kz, cmpTable.kz, tableBlockIndex)) view.getViewWithId(tmpFin1.kz, cmpTable.kz, tableBlockIndex).container.parent().parent().show();
          } else {
            if (view.getViewWithId(tmpFin1.ru, cmpTable.ru, tableBlockIndex)) view.getViewWithId(tmpFin1.ru, cmpTable.ru, tableBlockIndex).container.parent().parent().hide();
            if (view.getViewWithId(tmpFin1.kz, cmpTable.kz, tableBlockIndex)) view.getViewWithId(tmpFin1.kz, cmpTable.kz, tableBlockIndex).container.parent().parent().hide();
          }
        });

        // Синхронизация user1 user2
        var uBox1 = model.getModelWithId("user1", cmpTable.ru, tableBlockIndex);
        var uBox2 = model.getModelWithId("user2", cmpTable.ru, tableBlockIndex);
        uBox1.on("valueChange", function() {
          uBox2.setValue(uBox1.getValue());
        });
        uBox2.on("valueChange", function() {
          uBox1.setValue(uBox2.getValue());
        });

      });
    }, 0);

    var dateTableModel = model.getModelWithId(cmpTable.ru);
    if (view.editable) {
      dateTableModel.on('tableRowAdd', function(evt, modelTab, modelRow) {
        // С-по\по событию
        OTPUSK_UTILS.setWithOnByEvent(view, "choice22", tmpFin1, tmpFin2, cmpTable, modelRow[0].asfProperty.tableBlockIndex);
        var choice22 = model.getModelWithId("choice22", cmpTable.ru, modelRow[0].asfProperty.tableBlockIndex);
        var cmpNaPeriod = [{ru: "user2", kz: "user2_k"}, {ru: "position1", kz: "position1_k"}, {ru: "department2", kz: "department2_k"}];
        if (choice22.value[0] == "3") {
          cmpNaPeriod.forEach(function(cmp) {
            if (view.getViewWithId(cmp.ru, cmpTable.ru, modelRow[0].asfProperty.tableBlockIndex)) view.getViewWithId(cmp.ru, cmpTable.ru, modelRow[0].asfProperty.tableBlockIndex).container.parent().parent().show();
            if (view.getViewWithId(cmp.kz, cmpTable.kz, modelRow[0].asfProperty.tableBlockIndex)) view.getViewWithId(cmp.kz, cmpTable.kz, modelRow[0].asfProperty.tableBlockIndex).container.parent().parent().show();
          });
        } else {
          cmpNaPeriod.forEach(function(cmp) {
            if (view.getViewWithId(cmp.ru, cmpTable.ru, modelRow[0].asfProperty.tableBlockIndex)) view.getViewWithId(cmp.ru, cmpTable.ru, modelRow[0].asfProperty.tableBlockIndex).container.parent().parent().hide();
            if (view.getViewWithId(cmp.kz, cmpTable.kz, modelRow[0].asfProperty.tableBlockIndex)) view.getViewWithId(cmp.kz, cmpTable.kz, modelRow[0].asfProperty.tableBlockIndex).container.parent().parent().hide();
          });
        }
        choice22.on("valueChange", function() {
          if (choice22.value[0] == "3") {
            cmpNaPeriod.forEach(function(cmp) {
              if (view.getViewWithId(cmp.ru, cmpTable.ru, modelRow[0].asfProperty.tableBlockIndex)) view.getViewWithId(cmp.ru, cmpTable.ru, modelRow[0].asfProperty.tableBlockIndex).container.parent().parent().show();
              if (view.getViewWithId(cmp.kz, cmpTable.kz, modelRow[0].asfProperty.tableBlockIndex)) view.getViewWithId(cmp.kz, cmpTable.kz, modelRow[0].asfProperty.tableBlockIndex).container.parent().parent().show();
            });
          } else {
            cmpNaPeriod.forEach(function(cmp) {
              if (view.getViewWithId(cmp.ru, cmpTable.ru, modelRow[0].asfProperty.tableBlockIndex)) view.getViewWithId(cmp.ru, cmpTable.ru, modelRow[0].asfProperty.tableBlockIndex).container.parent().parent().hide();
              if (view.getViewWithId(cmp.kz, cmpTable.kz, modelRow[0].asfProperty.tableBlockIndex)) view.getViewWithId(cmp.kz, cmpTable.kz, modelRow[0].asfProperty.tableBlockIndex).container.parent().parent().hide();
            });
          }
        });

        // С-ПО
        ALL_DOCUMENTS_UTIL.compareDates(view, "start_date", "finish_date", null, cmpTable, modelRow[0].asfProperty.tableBlockIndex);
        // тарифная ставка, должностной оклад
        var positionUser = model.getModelWithId("change_position", cmpTable.ru, modelRow[0].asfProperty.tableBlockIndex);
        positionUser.on("valueChange", function() {
          if (positionUser.value.length > 0) ALL_DOCUMENTS_UTIL.setValueChoiceFromPosition(view, positionUser.value[0].elementID, ["choice1"], cmpTable.ru, modelRow[0].asfProperty.tableBlockIndex);
        });

        // выпиливание даты ПО если "постоянно"
        var checkVid = model.getModelWithId("choice3", cmpTable.ru, modelRow[0].asfProperty.tableBlockIndex);
        if (checkVid.value[0] == "2") {
          if (view.getViewWithId(tmpFin1.ru, cmpTable.ru, modelRow[0].asfProperty.tableBlockIndex)) view.getViewWithId(tmpFin1.ru, cmpTable.ru, modelRow[0].asfProperty.tableBlockIndex).container.parent().parent().show();
          if (view.getViewWithId(tmpFin1.kz, cmpTable.kz, modelRow[0].asfProperty.tableBlockIndex)) view.getViewWithId(tmpFin1.kz, cmpTable.kz, modelRow[0].asfProperty.tableBlockIndex).container.parent().parent().show();
        } else {
          if (view.getViewWithId(tmpFin1.ru, cmpTable.ru, modelRow[0].asfProperty.tableBlockIndex)) view.getViewWithId(tmpFin1.ru, cmpTable.ru, modelRow[0].asfProperty.tableBlockIndex).container.parent().parent().hide();
          if (view.getViewWithId(tmpFin1.kz, cmpTable.kz, modelRow[0].asfProperty.tableBlockIndex)) view.getViewWithId(tmpFin1.kz, cmpTable.kz, modelRow[0].asfProperty.tableBlockIndex).container.parent().parent().hide();
        }
        checkVid.on("valueChange", function() {
          if (checkVid.value[0] == "2") {
            if (view.getViewWithId(tmpFin1.ru, cmpTable.ru, modelRow[0].asfProperty.tableBlockIndex)) view.getViewWithId(tmpFin1.ru, cmpTable.ru, modelRow[0].asfProperty.tableBlockIndex).container.parent().parent().show();
            if (view.getViewWithId(tmpFin1.kz, cmpTable.kz, modelRow[0].asfProperty.tableBlockIndex)) view.getViewWithId(tmpFin1.kz, cmpTable.kz, modelRow[0].asfProperty.tableBlockIndex).container.parent().parent().show();
          } else {
            if (view.getViewWithId(tmpFin1.ru, cmpTable.ru, modelRow[0].asfProperty.tableBlockIndex)) view.getViewWithId(tmpFin1.ru, cmpTable.ru, modelRow[0].asfProperty.tableBlockIndex).container.parent().parent().hide();
            if (view.getViewWithId(tmpFin1.kz, cmpTable.kz, modelRow[0].asfProperty.tableBlockIndex)) view.getViewWithId(tmpFin1.kz, cmpTable.kz, modelRow[0].asfProperty.tableBlockIndex).container.parent().parent().hide();
          }
        });

        // Синхронизация user1 user2
        var uBox1 = model.getModelWithId("user1", cmpTable.ru, modelRow[0].asfProperty.tableBlockIndex);
        var uBox2 = model.getModelWithId("user2", cmpTable.ru, modelRow[0].asfProperty.tableBlockIndex);
        uBox1.on("valueChange", function() {
          uBox2.setValue(uBox1.getValue());
        });
        uBox2.on("valueChange", function() {
          uBox1.setValue(uBox2.getValue());
        });

      });
    }

    model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
      // ознакомить (юзер, должность)
      ALL_DOCUMENTS_UTIL.copyCmpValue(view, ["user", "change_position", "choice1"], ["ozn_user", "ozn_position", "ozn_choice"], "t", "t1");
      // ознакомление
      ALL_DOCUMENTS_UTIL.copyCmpValue(view, ["user"], ["user3"], "t", "table5_r");
    });

    // основание
    var osnovanieTable = model.getModelWithId("table4_r");
    osnovanieTable.modelBlocks.forEach(function(t) {
      var szLink = model.getModelWithId("document", "table4_r", t.tableBlockIndex);
      var dsLink = model.getModelWithId("document2", "table4_r", t.tableBlockIndex);
      szLink.on("valueChange", function() {
        if (szLink.getValue()) {
          var sluzhAsfData = ALL_DOCUMENTS_UTIL.getAsfDataJsonDocID(szLink.getValue());
          var numberSZ = null;
          if (sluzhAsfData) numberSZ = ALL_DOCUMENTS_UTIL.getAsfDataObject(sluzhAsfData, 'doc_num');
          if (numberSZ) model.getModelWithId("number", "table4_r", t.tableBlockIndex).setValue(numberSZ.value);
        } else {
          model.getModelWithId("number", "table4_r", t.tableBlockIndex).setValue(null);
        }
      });
      dsLink.on("valueChange", function() {
        if (dsLink.getValue()) {
          var dopSoglAsfData = ALL_DOCUMENTS_UTIL.getAsfDataJsonDocID(dsLink.getValue());
          var numberDS = null;
          var dateDS = null;
          if (dopSoglAsfData) {
            numberDS = ALL_DOCUMENTS_UTIL.getAsfDataObject(dopSoglAsfData, 'td_num');
            dateDS = ALL_DOCUMENTS_UTIL.getAsfDataObject(dopSoglAsfData, 'td_date');
          }
          if (numberDS) model.getModelWithId("td_num", "table4_r", t.tableBlockIndex).setValue(numberDS.value);
          if (dateDS) model.getModelWithId("td_date", "table4_r", t.tableBlockIndex).setValue(dateDS.key);
        } else {
          model.getModelWithId("td_num", "table4_r", t.tableBlockIndex).setValue(null);
          model.getModelWithId("td_date", "table4_r", t.tableBlockIndex).setValue(null);
        }
      });
    });

    osnovanieTable.on('tableRowAdd', function(evt, modelTab, modelRow) {
      var szLink = model.getModelWithId("document", "table4_r", modelRow[0].asfProperty.tableBlockIndex);
      var dsLink = model.getModelWithId("document2", "table4_r", modelRow[0].asfProperty.tableBlockIndex);
      szLink.on("valueChange", function() {
        if (szLink.getValue()) {
          var sluzhAsfData = ALL_DOCUMENTS_UTIL.getAsfDataJsonDocID(szLink.getValue());
          var numberSZ = null;
          if (sluzhAsfData) numberSZ = ALL_DOCUMENTS_UTIL.getAsfDataObject(sluzhAsfData, 'doc_num');
          if (numberSZ) model.getModelWithId("number", "table4_r", modelRow[0].asfProperty.tableBlockIndex).setValue(numberSZ.value);
        } else {
          model.getModelWithId("number", "table4_r", modelRow[0].asfProperty.tableBlockIndex).setValue(null);
        }
      });
      dsLink.on("valueChange", function() {
        if (dsLink.getValue()) {
          var dopSoglAsfData = ALL_DOCUMENTS_UTIL.getAsfDataJsonDocID(dsLink.getValue());
          var numberDS = null;
          var dateDS = null;
          if (dopSoglAsfData) {
            numberDS = ALL_DOCUMENTS_UTIL.getAsfDataObject(dopSoglAsfData, 'td_num');
            dateDS = ALL_DOCUMENTS_UTIL.getAsfDataObject(dopSoglAsfData, 'td_date');
          }
          if (numberDS) model.getModelWithId("td_num", "table4_r", modelRow[0].asfProperty.tableBlockIndex).setValue(numberDS.value);
          if (dateDS) model.getModelWithId("td_date", "table4_r", modelRow[0].asfProperty.tableBlockIndex).setValue(dateDS.key);
        } else {
          model.getModelWithId("td_num", "table4_r", modelRow[0].asfProperty.tableBlockIndex).setValue(null);
          model.getModelWithId("td_date", "table4_r", modelRow[0].asfProperty.tableBlockIndex).setValue(null);
        }
      });
    });

  }
});
