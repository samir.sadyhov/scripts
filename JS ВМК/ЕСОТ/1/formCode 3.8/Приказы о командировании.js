AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  //Приказ о командировании
  if (model.formCode == "Приказ_о_командировании"
   || model.formCode == "Приказ_о_внесении_изменений_в_приказ_о_командировании") {
    // С-ПО
    ALL_DOCUMENTS_UTIL.compareDates(view, "start_date", "finish_date", "srok", null, null);
    model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
      // ознакомление
      ALL_DOCUMENTS_UTIL.copyCmpValue(view, ["user"], ["user3"], "t", "t7");
    });

    // основание
    var osnovanieTable = model.getModelWithId("t6");
    osnovanieTable.modelBlocks.forEach(function(t) {
      var szLink = model.getModelWithId("document", "t6", t.tableBlockIndex);
      szLink.on("valueChange", function() {
        if (szLink.getValue()) {
          var sluzhAsfData = ALL_DOCUMENTS_UTIL.getAsfDataJsonDocID(szLink.getValue());
          var numberSZ = null;
          var dateSZ = null;
          if (sluzhAsfData) {
            numberSZ = ALL_DOCUMENTS_UTIL.getAsfDataObject(sluzhAsfData, 'doc_num');
            dateSZ = ALL_DOCUMENTS_UTIL.getAsfDataObject(sluzhAsfData, 'doc_date');
          }
          if (numberSZ) model.getModelWithId("osnovanie", "t6", t.tableBlockIndex).setValue(numberSZ.value);
          if (dateSZ) model.getModelWithId("date_osnovanie", "t6", t.tableBlockIndex).setValue(dateSZ.key);
        } else {
          model.getModelWithId("osnovanie", "t6", t.tableBlockIndex).setValue(null);
          model.getModelWithId("date_osnovanie", "t6", t.tableBlockIndex).setValue(null);
        }
      });
    });
    osnovanieTable.on('tableRowAdd', function(evt, modelTab, modelRow) {
      var szLink = model.getModelWithId("document", "t6", modelRow[0].asfProperty.tableBlockIndex);
      szLink.on("valueChange", function() {
        if (szLink.getValue()) {
          var sluzhAsfData = ALL_DOCUMENTS_UTIL.getAsfDataJsonDocID(szLink.getValue());
          var numberSZ = null;
          var dateSZ = null;
          if (sluzhAsfData) {
            numberSZ = ALL_DOCUMENTS_UTIL.getAsfDataObject(sluzhAsfData, 'doc_num');
            dateSZ = ALL_DOCUMENTS_UTIL.getAsfDataObject(sluzhAsfData, 'doc_date');
          }
          if (numberSZ) model.getModelWithId("osnovanie", "t6", modelRow[0].asfProperty.tableBlockIndex).setValue(numberSZ.value);
          if (dateSZ) model.getModelWithId("date_osnovanie", "t6", modelRow[0].asfProperty.tableBlockIndex).setValue(dateSZ.key);
        } else {
          model.getModelWithId("osnovanie", "t6", modelRow[0].asfProperty.tableBlockIndex).setValue(null);
          model.getModelWithId("date_osnovanie", "t6", modelRow[0].asfProperty.tableBlockIndex).setValue(null);
        }
      });
    });

  }

  //Приказ о прикомандировании
  if (model.formCode == "Приказ_о_прикомандировании1") {
    // С-ПО
    var tmpTab={ru: "t", kz: "t_k"};

    setTimeout(function() {
      var dateTableView = view.getViewWithId(tmpTab.ru);
      dateTableView.getViewBlocks().forEach(function(viewBlock, index){
        var tableBlockIndex =  viewBlock.views[0].model.asfProperty.tableBlockIndex;
        ALL_DOCUMENTS_UTIL.compareDates(view, "start_date", "finish_date", null, tmpTab, tableBlockIndex);
      });
    }, 0);

    model.getModelWithId(tmpTab.ru).on('tableRowAdd', function(evt, modelTab, modelRow) {
      ALL_DOCUMENTS_UTIL.compareDates(view, "start_date", "finish_date", null, tmpTab, modelRow[0].asfProperty.tableBlockIndex);
    });

    model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
      // ознакомление
      ALL_DOCUMENTS_UTIL.copyCmpValue(view, ["user"], ["user3"], "t", "t7");
    });
    
    // наименование организации, где работает работник
    if (view.editable) {
      var oraganization = ALL_DOCUMENTS_UTIL.getOrganizationData();
      if (oraganization) {
        model.getModelWithId("department1").setValue({
          departmentId: oraganization.departmentID,
          id: oraganization.departmentID,
          departmentName: oraganization.nameRu,
          name: oraganization.nameRu,
          tagName: oraganization.nameRu,
          hasChildren: true
        });
        model.getModelWithId("department1_k").setValue({
          departmentId: oraganization.departmentID,
          id: oraganization.departmentID,
          departmentName: oraganization.nameKz,
          name: oraganization.nameKz,
          tagName: oraganization.nameKz,
          hasChildren: true
        });
      }

    }
  }

});
