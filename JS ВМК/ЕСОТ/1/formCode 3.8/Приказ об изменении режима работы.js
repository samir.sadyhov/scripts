AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  // Приказ об изменении режима работы
  if (model.formCode == "Приказ_об_изменении_режима_работы"
   || model.formCode == "Приказ_о_внесении_изменении_в_приказ_об_изменении_режима_работы") {
    var cmpTable = {ru: "t", kz: "t_k"};
    var tmpFin1 = {ru: "finish_date", kz: "finish_date_k"};
    var tmpFin2 = {ru: "finish_date1", kz: "finish_date1_k"};

    model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
      // ознакомление
      ALL_DOCUMENTS_UTIL.copyCmpValue(view, ["user"], ["oznakomlenie_user"], "t", "tt");
      // основание
      ALL_DOCUMENTS_UTIL.copyCmpValue(view, ["user"], ["ispit_srok"], "t", "ttt");
    });

    setTimeout(function() {
      var dateTableView = view.getViewWithId(cmpTable.ru);
      dateTableView.getViewBlocks().forEach(function(viewBlock, index){
        var tableBlockIndex =  viewBlock.views[0].model.asfProperty.tableBlockIndex;
        // С-ПО
        ALL_DOCUMENTS_UTIL.compareDates(view, "start_date", "finish_date", null, cmpTable, tableBlockIndex);
        // по\по событию\на период
        poSobitPeriod(view, cmpTable, tableBlockIndex);
        // синхронизация дат
        var sd1 = model.getModelWithId("start_date", cmpTable.ru, tableBlockIndex);
        var sd2 = model.getModelWithId("start_date1", cmpTable.ru, tableBlockIndex);
        sd1.on("valueChange", function() {
          sd2.setValue(sd1.getValue());
        });
        sd2.on("valueChange", function() {
          sd1.setValue(sd2.getValue());
        });
      });
    }, 0);

    var dateTableModel = model.getModelWithId(cmpTable.ru);
    if (view.editable) {
      dateTableModel.on('tableRowAdd', function(evt, modelTab, modelRow) {
        // С-ПО
        ALL_DOCUMENTS_UTIL.compareDates(view, "start_date", "finish_date", null, cmpTable, modelRow[0].asfProperty.tableBlockIndex);
        // по\по событию\на период
        poSobitPeriod(view, cmpTable, modelRow[0].asfProperty.tableBlockIndex);
        // синхронизация дат
        var sd1 = model.getModelWithId("start_date", cmpTable.ru, modelRow[0].asfProperty.tableBlockIndex);
        var sd2 = model.getModelWithId("start_date1", cmpTable.ru, modelRow[0].asfProperty.tableBlockIndex);
        sd1.on("valueChange", function() {
          sd2.setValue(sd1.getValue());
        });
        sd2.on("valueChange", function() {
          sd1.setValue(sd2.getValue());
        });
      });
    }
  }
});

function poSobitPeriod(playerView, cmpTable, tableBlockIndex) {
  var cmp1 = {ru: "finish_date", kz: "finish_date_k"};
  var cmp2 = {ru: "finish_date1", kz: "finish_date1_k"};
  var cmp3 = {ru: "user1", kz: "user1_k"};
  var cmp4 = {ru: "choice1", kz: "choice1_k"};
  var cmp5 = {ru: "start_date1", kz: "start_date1_k"};

  var choice3 = playerView.model.getModelWithId("choice3", cmpTable.ru, tableBlockIndex);
  if (choice3.value[0] == "1") {
    if (playerView.getViewWithId(cmp1.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(cmp1.ru, cmpTable.ru, tableBlockIndex).setVisible(true);
    if (playerView.getViewWithId(cmp1.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(cmp1.kz, cmpTable.kz, tableBlockIndex).setVisible(true);
    if (playerView.getViewWithId(cmp2.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(cmp2.ru, cmpTable.ru, tableBlockIndex).container.parent().parent().hide();
    if (playerView.getViewWithId(cmp2.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(cmp2.kz, cmpTable.kz, tableBlockIndex).container.parent().parent().hide();
    if (playerView.getViewWithId(cmp3.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(cmp3.ru, cmpTable.ru, tableBlockIndex).container.parent().parent().hide();
    if (playerView.getViewWithId(cmp3.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(cmp3.kz, cmpTable.kz, tableBlockIndex).container.parent().parent().hide();
    if (playerView.getViewWithId(cmp4.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(cmp4.ru, cmpTable.ru, tableBlockIndex).container.parent().parent().hide();
    if (playerView.getViewWithId(cmp4.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(cmp4.kz, cmpTable.kz, tableBlockIndex).container.parent().parent().hide();
    if (playerView.getViewWithId(cmp5.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(cmp5.ru, cmpTable.ru, tableBlockIndex).setVisible(false);
    if (playerView.getViewWithId(cmp5.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(cmp5.kz, cmpTable.kz, tableBlockIndex).setVisible(false);
  } else if (choice3.value[0] == "2") {
    if (playerView.getViewWithId(cmp1.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(cmp1.ru, cmpTable.ru, tableBlockIndex).setVisible(false);
    if (playerView.getViewWithId(cmp1.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(cmp1.kz, cmpTable.kz, tableBlockIndex).setVisible(false);
    if (playerView.getViewWithId(cmp2.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(cmp2.ru, cmpTable.ru, tableBlockIndex).container.parent().parent().show();
    if (playerView.getViewWithId(cmp2.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(cmp2.kz, cmpTable.kz, tableBlockIndex).container.parent().parent().show();
    if (playerView.getViewWithId(cmp3.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(cmp3.ru, cmpTable.ru, tableBlockIndex).container.parent().parent().hide();
    if (playerView.getViewWithId(cmp3.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(cmp3.kz, cmpTable.kz, tableBlockIndex).container.parent().parent().hide();
    if (playerView.getViewWithId(cmp4.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(cmp4.ru, cmpTable.ru, tableBlockIndex).container.parent().parent().hide();
    if (playerView.getViewWithId(cmp4.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(cmp4.kz, cmpTable.kz, tableBlockIndex).container.parent().parent().hide();
    if (playerView.getViewWithId(cmp5.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(cmp5.ru, cmpTable.ru, tableBlockIndex).setVisible(false);
    if (playerView.getViewWithId(cmp5.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(cmp5.kz, cmpTable.kz, tableBlockIndex).setVisible(false);
  } else if (choice3.value[0] == "3") {
    if (playerView.getViewWithId(cmp1.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(cmp1.ru, cmpTable.ru, tableBlockIndex).setVisible(false);
    if (playerView.getViewWithId(cmp1.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(cmp1.kz, cmpTable.kz, tableBlockIndex).setVisible(false);
    if (playerView.getViewWithId(cmp2.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(cmp2.ru, cmpTable.ru, tableBlockIndex).container.parent().parent().hide();
    if (playerView.getViewWithId(cmp2.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(cmp2.kz, cmpTable.kz, tableBlockIndex).container.parent().parent().hide();
    if (playerView.getViewWithId(cmp3.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(cmp3.ru, cmpTable.ru, tableBlockIndex).container.parent().parent().show();
    if (playerView.getViewWithId(cmp3.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(cmp3.kz, cmpTable.kz, tableBlockIndex).container.parent().parent().show();
    if (playerView.getViewWithId(cmp4.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(cmp4.ru, cmpTable.ru, tableBlockIndex).container.parent().parent().show();
    if (playerView.getViewWithId(cmp4.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(cmp4.kz, cmpTable.kz, tableBlockIndex).container.parent().parent().show();
    if (playerView.getViewWithId(cmp5.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(cmp5.ru, cmpTable.ru, tableBlockIndex).setVisible(false);
    if (playerView.getViewWithId(cmp5.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(cmp5.kz, cmpTable.kz, tableBlockIndex).setVisible(false);
  } else {
    if (playerView.getViewWithId(cmp1.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(cmp1.ru, cmpTable.ru, tableBlockIndex).setVisible(false);
    if (playerView.getViewWithId(cmp1.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(cmp1.kz, cmpTable.kz, tableBlockIndex).setVisible(false);
    if (playerView.getViewWithId(cmp2.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(cmp2.ru, cmpTable.ru, tableBlockIndex).container.parent().parent().hide();
    if (playerView.getViewWithId(cmp2.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(cmp2.kz, cmpTable.kz, tableBlockIndex).container.parent().parent().hide();
    if (playerView.getViewWithId(cmp3.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(cmp3.ru, cmpTable.ru, tableBlockIndex).container.parent().parent().hide();
    if (playerView.getViewWithId(cmp3.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(cmp3.kz, cmpTable.kz, tableBlockIndex).container.parent().parent().hide();
    if (playerView.getViewWithId(cmp4.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(cmp4.ru, cmpTable.ru, tableBlockIndex).container.parent().parent().hide();
    if (playerView.getViewWithId(cmp4.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(cmp4.kz, cmpTable.kz, tableBlockIndex).container.parent().parent().hide();
    if (playerView.getViewWithId(cmp5.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(cmp5.ru, cmpTable.ru, tableBlockIndex).setVisible(false);
    if (playerView.getViewWithId(cmp5.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(cmp5.kz, cmpTable.kz, tableBlockIndex).setVisible(false);
  }
  choice3.on("valueChange", function() {
    if (choice3.value[0] == "1") {
      if (playerView.getViewWithId(cmp1.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(cmp1.ru, cmpTable.ru, tableBlockIndex).setVisible(true);
      if (playerView.getViewWithId(cmp1.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(cmp1.kz, cmpTable.kz, tableBlockIndex).setVisible(true);
      if (playerView.getViewWithId(cmp2.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(cmp2.ru, cmpTable.ru, tableBlockIndex).container.parent().parent().hide();
      if (playerView.getViewWithId(cmp2.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(cmp2.kz, cmpTable.kz, tableBlockIndex).container.parent().parent().hide();
      if (playerView.getViewWithId(cmp3.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(cmp3.ru, cmpTable.ru, tableBlockIndex).container.parent().parent().hide();
      if (playerView.getViewWithId(cmp3.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(cmp3.kz, cmpTable.kz, tableBlockIndex).container.parent().parent().hide();
      if (playerView.getViewWithId(cmp4.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(cmp4.ru, cmpTable.ru, tableBlockIndex).container.parent().parent().hide();
      if (playerView.getViewWithId(cmp4.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(cmp4.kz, cmpTable.kz, tableBlockIndex).container.parent().parent().hide();
      if (playerView.getViewWithId(cmp5.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(cmp5.ru, cmpTable.ru, tableBlockIndex).setVisible(false);
      if (playerView.getViewWithId(cmp5.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(cmp5.kz, cmpTable.kz, tableBlockIndex).setVisible(false);
      playerView.model.getModelWithId(cmp2.ru, cmpTable.ru, tableBlockIndex).setValue(null);
      playerView.model.getModelWithId(cmp2.kz, cmpTable.kz, tableBlockIndex).setValue(null);
      playerView.model.getModelWithId(cmp3.ru, cmpTable.ru, tableBlockIndex).setValue(null);
      playerView.model.getModelWithId(cmp4.ru, cmpTable.ru, tableBlockIndex).setValue(null);
      playerView.model.getModelWithId(cmp5.ru, cmpTable.ru, tableBlockIndex).setValue(null);
    } else if (choice3.value[0] == "2") {
      if (playerView.getViewWithId(cmp1.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(cmp1.ru, cmpTable.ru, tableBlockIndex).setVisible(false);
      if (playerView.getViewWithId(cmp1.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(cmp1.kz, cmpTable.kz, tableBlockIndex).setVisible(false);
      if (playerView.getViewWithId(cmp2.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(cmp2.ru, cmpTable.ru, tableBlockIndex).container.parent().parent().show();
      if (playerView.getViewWithId(cmp2.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(cmp2.kz, cmpTable.kz, tableBlockIndex).container.parent().parent().show();
      if (playerView.getViewWithId(cmp3.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(cmp3.ru, cmpTable.ru, tableBlockIndex).container.parent().parent().hide();
      if (playerView.getViewWithId(cmp3.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(cmp3.kz, cmpTable.kz, tableBlockIndex).container.parent().parent().hide();
      if (playerView.getViewWithId(cmp4.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(cmp4.ru, cmpTable.ru, tableBlockIndex).container.parent().parent().hide();
      if (playerView.getViewWithId(cmp4.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(cmp4.kz, cmpTable.kz, tableBlockIndex).container.parent().parent().hide();
      if (playerView.getViewWithId(cmp5.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(cmp5.ru, cmpTable.ru, tableBlockIndex).setVisible(false);
      if (playerView.getViewWithId(cmp5.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(cmp5.kz, cmpTable.kz, tableBlockIndex).setVisible(false);
      playerView.model.getModelWithId(cmp1.ru, cmpTable.ru, tableBlockIndex).setValue(null);
      playerView.model.getModelWithId(cmp3.ru, cmpTable.ru, tableBlockIndex).setValue(null);
      playerView.model.getModelWithId(cmp4.ru, cmpTable.ru, tableBlockIndex).setValue(null);
      playerView.model.getModelWithId(cmp5.ru, cmpTable.ru, tableBlockIndex).setValue(null);
    } else if (choice3.value[0] == "3") {
      if (playerView.getViewWithId(cmp1.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(cmp1.ru, cmpTable.ru, tableBlockIndex).setVisible(false);
      if (playerView.getViewWithId(cmp1.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(cmp1.kz, cmpTable.kz, tableBlockIndex).setVisible(false);
      if (playerView.getViewWithId(cmp2.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(cmp2.ru, cmpTable.ru, tableBlockIndex).container.parent().parent().hide();
      if (playerView.getViewWithId(cmp2.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(cmp2.kz, cmpTable.kz, tableBlockIndex).container.parent().parent().hide();
      if (playerView.getViewWithId(cmp3.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(cmp3.ru, cmpTable.ru, tableBlockIndex).container.parent().parent().show();
      if (playerView.getViewWithId(cmp3.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(cmp3.kz, cmpTable.kz, tableBlockIndex).container.parent().parent().show();
      if (playerView.getViewWithId(cmp4.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(cmp4.ru, cmpTable.ru, tableBlockIndex).container.parent().parent().show();
      if (playerView.getViewWithId(cmp4.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(cmp4.kz, cmpTable.kz, tableBlockIndex).container.parent().parent().show();
      if (playerView.getViewWithId(cmp5.ru, cmpTable.ru, tableBlockIndex)) playerView.getViewWithId(cmp5.ru, cmpTable.ru, tableBlockIndex).setVisible(false);
      if (playerView.getViewWithId(cmp5.kz, cmpTable.kz, tableBlockIndex)) playerView.getViewWithId(cmp5.kz, cmpTable.kz, tableBlockIndex).setVisible(false);
      playerView.model.getModelWithId(cmp1.ru, cmpTable.ru, tableBlockIndex).setValue(null);
      playerView.model.getModelWithId(cmp2.ru, cmpTable.ru, tableBlockIndex).setValue(null);
      playerView.model.getModelWithId(cmp2.kz, cmpTable.kz, tableBlockIndex).setValue(null);
      playerView.model.getModelWithId(cmp5.ru, cmpTable.ru, tableBlockIndex).setValue(null);
    }
  });
}
