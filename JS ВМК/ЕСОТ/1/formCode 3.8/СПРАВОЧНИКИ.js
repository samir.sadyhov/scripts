AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  if (model.formCode == "Справочник_должностей_служащих") {
    var tableArrSDS = [
      {id: {ru: "education_table", kz: "education_table_k"}, cmp: [{ru: "education_level", kz: "education_level_k"}, {ru: "education_year", kz: "education_year_k"}, {ru: "education_year1", kz: "education_year1_k"}]},
      {id: {ru: "experience_table", kz: "experience_table_k"}, cmp: [{ru: "experience_year1", kz: "experience_year1_k"}, {ru: "experience_year2", kz: "experience_year2_k"}]},
      {id: {ru: "certificate_table", kz: "certificate_table_k"}, cmp: []},
      {id: {ru: "workknowledge_table", kz: "workknowledge_table_k"}, cmp: []},
      {id: {ru: "duties_table", kz: "duties_table_k"}, cmp: []},
      {id: {ru: "competence_table", kz: "competence_table_k"}, cmp: []}
    ];

    model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
      console.log(model.formName + '\nformCode: [' + model.formCode + ']\nformID: [' + model.formId +']\nasfDataID: [' + model.asfDataId+']');
      tableArrSDS.forEach(function(table) {
        ALL_DOCUMENTS_UTIL.tableOn(view, table.id, table.cmp);
      });
    });

    if (view.editable) {
      var pageBreak=$('div.asf-pageBreak');
      if (pageBreak.length > 0) pageBreak.hide(); // скрываем разрыв страницы
      tableArrSDS.forEach(function(table) {
        ALL_DOCUMENTS_UTIL.tableEnabled(view, table.id.kz); // блокируем дин.таблицы в каз. варианте
      });
    }

    var rangList = model.getModelWithId("rang");
    var tableDT = {ru: "experience_table", kz: "experience_table_k"};
    var titleTable = {ru: "dop", kz: "dop_k"};
    if (rangList.value[0] == "РВ" || rangList.value[0] == "РС" || rangList.value[0] == "РМ" || rangList.value[0] == "ТМ") {
      ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tableDT, true);
      ALL_DOCUMENTS_UTIL.setVisibleCmp(view, titleTable, true);
    } else {
      ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tableDT, false);
      ALL_DOCUMENTS_UTIL.setVisibleCmp(view, titleTable, false);
    }
    rangList.on("valueChange", function() {
      var tmpTable = model.getModelWithId(tableDT.ru);
      if (rangList.value[0] == "РВ" || rangList.value[0] == "РС" || rangList.value[0] == "РМ" || rangList.value[0] == "ТМ") {
        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tableDT, true);
        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, titleTable, true);
        if (tmpTable.modelBlocks.length == 0) {
          tmpTable.createRow();
        } else {
          tmpTable.modelBlocks.forEach(function(modelBlock, index){
            tmpTable.removeRow(index);
          });
          if (tmpTable.modelBlocks.length == 0) tmpTable.createRow();
        }
      } else {
        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tableDT, false);
        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, titleTable, false);
        tmpTable.modelBlocks.forEach(function(modelBlock, index){
          tmpTable.removeRow(index);
        });
      }
    });

  }

  if (model.formCode == "Справочник_рабочих_профессий") {
    var tableArrSRP = [
      {id: {ru: "education_table", kz: "education_table_k"}, cmp: [{ru: "education_razryad", kz: "education_razryad_k"}, {ru: "education_level", kz: "education_level_k"}, {ru: "education_year", kz: "education_year_k"}, {ru: "education_year1", kz: "education_year1_k"}]},
      {id: {ru: "certificate_table", kz: "certificate_table_k"}, cmp: [{ru: "certificate_razrayd", kz: "certificate_razrayd_k"}]},
      {id: {ru: "workknowledge_table", kz: "workknowledge_table_k"}, cmp: [{ru: "workknowledge_razryad", kz: "workknowledge_razryad_k"}]},
      {id: {ru: "duties_table", kz: "duties_table_k"}, cmp: [{ru: "duties_razrayd", kz: "duties_razrayd_k"}]},
      {id: {ru: "competence_table", kz: "competence_table_k"}, cmp: [{ru: "competence_razryad", kz: "competence_razryad_k"}]}
    ];

    model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
      console.log(model.formName + '\nformCode: [' + model.formCode + ']\nformID: [' + model.formId +']\nasfDataID: [' + model.asfDataId+']');
      tableArrSRP.forEach(function(table) {
        ALL_DOCUMENTS_UTIL.tableOn(view, table.id, table.cmp);
      });
    });

    if (view.editable) {
      var pageBreak=$('div.asf-pageBreak');
      if (pageBreak.length > 0) pageBreak.hide(); // скрываем разрыв страницы
      tableArrSRP.forEach(function(table) {
        ALL_DOCUMENTS_UTIL.tableEnabled(view, table.id.kz); // блокируем дин.таблицы в каз. варианте
      });
    }

  }
});
