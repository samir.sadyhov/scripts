AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {

  // Приказ о возмещении вреда в связи со смертью кормильца.
  if (model.formCode == "Приказ_о_возмещении_вреда_в_связи_со_смертью_кормильца." ||
      model.formCode == "Приказ_о_внесении_изменении_в_приказ_о_возмещении_вреда_в_связи_со_смертью_кормильца.") {
    // Наименование организации
    var oraganization = ALL_DOCUMENTS_UTIL.getOrganizationData();
    if (oraganization) {
      if (model.getModelWithId('department2')) model.getModelWithId('department2').setValue(oraganization.nameRu);
    }

    // телефон исполнителя
    var ispUser2 = model.getModelWithId("ispolnitel_user10");
    ispUser2.on("valueChange", function() {
      ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Штатная_расстановка111", "ispolnitel_user10", "telephone", "ispolnitel_phone10", null, null);
    });
    ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Штатная_расстановка111", "ispolnitel_user10", "telephone", "ispolnitel_phone10", null, null);

    // табельный номер
    var tmpTable = model.getModelWithId('t3');
    tmpTable.modelBlocks.forEach(function(t) {
      model.getModelWithId("user_kormilec", 't3', t.tableBlockIndex).on("valueChange", function() {
        ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Штатная_расстановка111", "user_kormilec", "t_number", "tab_num", 't3', t.tableBlockIndex);
      });
    });
    tmpTable.on('tableRowAdd', function(evt, modelTab, modelRow) {
      model.getModelWithId("user_kormilec", 't3', modelRow[0].asfProperty.tableBlockIndex).on("valueChange", function() {
        ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Штатная_расстановка111", "user_kormilec", "t_number", "tab_num", 't3', modelRow[0].asfProperty.tableBlockIndex);
      });
    });
  }

  // Приказ о прекращении выплаты ущерба по потере кормильца
  if (model.formCode == "Приказ_о_прекращении_выплаты_ущерба_по_потере_кормильца"
   || model.formCode == "Приказ_о_внесении_изменении_приказа_о_прекращении_выплаты_ущерба_по_потере_кормильца") {

    var getArrayValue = function(playerView, tmpTableJson, cmp, key) {
      var tmpValue = [];
      tmpTableJson.data.forEach(function(data) {
        if (data.id.substring(0, data.id.indexOf('-b')) == cmp) {
          if (key) {
            tmpValue.push(data.key);
          } else {
            tmpValue.push(data.value);
          }
        }
      });
      return tmpValue;
    };

    var userBox = model.getModelWithId("user");
    var familyTable = model.getModelWithId("family_composition1");
    var FAMILY = {};

    userBox.on("valueChange", function() {
      if (userBox.getValue().length > 0) {

        if (familyTable.modelBlocks.length > 0) {
          familyTable.modelBlocks.forEach(function(modelBlock, index){
            familyTable.removeRow(index);
          });
        }

        var userCardData = ALL_DOCUMENTS_UTIL.getCardsData("rest/api/personalrecord/forms/" + userBox.getValue()[0].personID, "Штатная_расстановка111");
        var jsonTableFamily = ALL_DOCUMENTS_UTIL.getAsfDataObject(userCardData, 'family_composition');

        FAMILY.name = getArrayValue(view, jsonTableFamily, 'family_name');
        FAMILY.surname = getArrayValue(view, jsonTableFamily, 'family_surname');
        FAMILY.middlename = getArrayValue(view, jsonTableFamily, 'family_middlename');
        FAMILY.composition = getArrayValue(view, jsonTableFamily, 'family_composition', true);
        console.log("|||||||||||--FAMILY--|||||||||||");
        console.log(FAMILY);
        console.log("|||||||||||--FAMILY--|||||||||||");

        if (FAMILY.name.length > 0) {
          if (familyTable.modelBlocks.length > 0) {
            familyTable.modelBlocks.forEach(function(modelBlock, index){
              familyTable.removeRow(index);
            });
            if (familyTable.modelBlocks.length > 0) {
              familyTable.modelBlocks.forEach(function(modelBlock, index){
                familyTable.removeRow(index);
              });
            }
          }
          if (familyTable.modelBlocks.length == 0) {
            FAMILY.name.forEach(function(){
              familyTable.createRow();
            });
          }
          var tmpIndex = 0;
          familyTable.on('tableRowAdd', function(evt, modelTab, modelRow) {
            var tableBlockIndex = modelRow[0].asfProperty.tableBlockIndex;
            setTimeout(function(){
              if (model.getModelWithId('family_name', 'family_composition1', tableBlockIndex)) model.getModelWithId('family_name', 'family_composition1', tableBlockIndex).setValue(FAMILY.name[tmpIndex]);
              if (model.getModelWithId('family_surname', 'family_composition1', tableBlockIndex)) model.getModelWithId('family_surname', 'family_composition1', tableBlockIndex).setValue(FAMILY.surname[tmpIndex]);
              if (model.getModelWithId('family_middlename', 'family_composition1', tableBlockIndex)) model.getModelWithId('family_middlename', 'family_composition1', tableBlockIndex).setValue(FAMILY.middlename[tmpIndex]);
              if (model.getModelWithId('family_composition', 'family_composition1', tableBlockIndex)) model.getModelWithId('family_composition', 'family_composition1', tableBlockIndex).setValue(FAMILY.composition[tmpIndex]);
              tmpIndex++;
            }, 0);
          });
        } else {
          if (familyTable.modelBlocks.length > 0) {
            familyTable.modelBlocks.forEach(function(modelBlock, index){
              familyTable.removeRow(index);
            });
          }
        }

      } else {
        if (familyTable.modelBlocks.length > 0) {
          familyTable.modelBlocks.forEach(function(modelBlock, index){
            familyTable.removeRow(index);
          });
        }
      }
    });

    // ознакомление
    ALL_DOCUMENTS_UTIL.copyCmpValue(view, ["user"], ["oznakomlenie_user"]);
  }

});
