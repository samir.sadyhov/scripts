AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  if (model.formCode.substring(0,11).toLowerCase() == "уведомление") {
    var cmpArrU=[{ru: "address", kz: "address_k"}, {ru: "user", kz: "user_k"}, {ru: "position", kz: "position_k"}, {ru: "department", kz: "department_k"}, {ru: "doc_num", kz: "doc_num_k"}, {ru: "doc_date", kz: "doc_date_k"}, {ru: "choice1", kz: "choice1_k"}, {ru: "choice2", kz: "choice2_k"}, {ru: "ruk_user", kz: "ruk_user_k"}, {ru: "user2", kz: "user2_k"}, {ru: "date2", kz: "date2_k"}, {ru: "prikaz_num", kz: "prikaz_num_k"}, {ru: "date1", kz: "date1_k"}, {ru: "position2", kz: "position2_k"}, {ru: "prichina", kz: "prichina_k"}, {ru: "department2", kz: "department2_k"}];
    model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
      console.log(model.formName + '\nformCode: [' + model.formCode + '\nformID: [' + model.formId +'] asfDataID: [' + model.asfDataId+']');
      for (var ci=0; ci < cmpArrU.length; ci++) {
        ALL_DOCUMENTS_UTIL.changedValue(model, cmpArrU[ci]);
      }
    });

    // Уведомление об изменении условии труда
    // Уведомление о расторжении ТД в связи со снижением объема производства
    // Уведомление в связи с сокращением
    // Уведомление о расторжении ТД в связи с ликвидацией
    if (model.formCode == "Уведомление_об_изменении_условии_труда"
    || model.formCode == "Уведомление_в_связи_со_снижением"
    || model.formCode == "Уведомление_в_связи_с_сокращением"
    || model.formCode == "Уведомление_в_связи_с_ликвидацией") {
      // ознакомление
      ALL_DOCUMENTS_UTIL.copyCmpValue(view, ["user"], ["user2"]);
    }
  }
});
