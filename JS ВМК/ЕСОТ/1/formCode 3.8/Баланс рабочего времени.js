AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  if (model.formCode == "Баланс_рабочего_времени") {

    var graphTableModel = model.getModelWithId('table');
    var yearModel = model.getModelWithId('year');
    var holidays = ALL_DOCUMENTS_UTIL.searchHolidays(2014 + Number(yearModel.getValue()));

    model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
      AS.SERVICES.showWaitWindow();

      var dataNorm = [
        {n1: '1', n2: '1'},
        {n1: '1', n2: '2'},
        {n1: '2', n2: '1'},
        {n1: '2', n2: '2'}
      ];

      setTimeout(function() {
        var graphTableView = view.getViewWithId(graphTableModel.asfProperty.id);

        // добавляем 4 блока и блокируем таблицу на изменение
        if (graphTableView.getRowsCount() == 1) {
          dataNorm.forEach(function(norm, i) {
            graphTableModel.createRow(); //создаем блок

            // заполняем данными поля норма1 и норма2
            model.getModelWithId('week', graphTableModel.asfProperty.id, i + 1).setValue(norm.n1);
            model.getModelWithId('day', graphTableModel.asfProperty.id, i + 1).setValue(norm.n2);

            // заполняем праздниками "В" 2_day_1
            holidays.forEach(function(d) {
              model.getModelWithId(d.month + '_day_' + d.day, graphTableModel.asfProperty.id, i + 1).setValue('В');
            });

          });
          ALL_DOCUMENTS_UTIL.tableEnabled(view, graphTableModel.asfProperty.id);
        }

        AS.SERVICES.hideWaitWindow();
      }, 0);
    });

    yearModel.on("valueChange", function() {
      AS.SERVICES.showWaitWindow();

      setTimeout(function() {
        var graphTableView = view.getViewWithId(graphTableModel.asfProperty.id);

        graphTableView.getViewBlocks().forEach(function(viewBlock, index) {
          var tableBlockIndex = viewBlock.views[0].model.asfProperty.tableBlockIndex;
          // чистим прошлое
          holidays.forEach(function(d) {
            model.getModelWithId(d.month + '_day_' + d.day, graphTableModel.asfProperty.id, tableBlockIndex).setValue(null);
          });
          // заполняем праздниками "В"
          setTimeout(function() {
            holidays = ALL_DOCUMENTS_UTIL.searchHolidays(2014 + Number(yearModel.getValue()));
            holidays.forEach(function(d) {
              model.getModelWithId(d.month + '_day_' + d.day, graphTableModel.asfProperty.id, tableBlockIndex).setValue('В');
            });
            AS.SERVICES.hideWaitWindow();
          }, 0);
        });

      }, 0);

    });

  }
});
