AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  if (model.formCode == "Квал_требования") {
    var tableArrKval = [
      {id: {ru: "education_table", kz: "education_table_k"}, cmp: [{ru: "education_level", kz: "education_level_k"}, {ru: "education_year", kz: "education_year_k"}, {ru: "education_year1", kz: "education_year1_k"}]},
      {id: {ru: "d_education_table", kz: "d_education_table_k"}, cmp: [{ru: "d_education_level", kz: "d_education_level_k"}, {ru: "d_education_year", kz: "d_education_year_k"}, {ru: "d_education_year1", kz: "d_education_year1_k"}]},
      {id: {ru: "experience_table", kz: "experience_table_k"}, cmp: [{ru: "experience_year1", kz: "experience_year1_k"}, {ru: "experience_year2", kz: "experience_year2_k"}]},
      {id: {ru: "d_experience_table", kz: "d_experience_table_k"}, cmp: [{ru: "d_experience_year1", kz: "d_experience_year1_k"}, {ru: "d_experience_year2", kz: "d_experience_year2_k"}]},
      {id: {ru: "certificate_table", kz: "certificate_table_k"}, cmp: []},
      {id: {ru: "d_certificate_table", kz: "d_certificate_table_k"}, cmp: []},
      {id: {ru: "workknowledge_table", kz: "workknowledge_table_k"}, cmp: []},
      {id: {ru: "d_workknowledge_table", kz: "d_workknowledge_table_k"}, cmp: []},
      {id: {ru: "duties_table", kz: "duties_table_k"}, cmp: []},
      {id: {ru: "d_duties_table", kz: "d_duties_table_k"}, cmp: []},
      {id: {ru: "competence_table", kz: "competence_table_k"}, cmp: []},
      {id: {ru: "d_competence_table", kz: "d_competence_table_k"}, cmp: []}
    ];

    var dzoTableArr = [];
    for (var i = 1; i < tableArrKval.length; i+=2) {
      dzoTableArr.push(tableArrKval[i].id);
    }

    var componentsArr = [
      {ru: "experience_table", kz: "experience_table_k"},
      {ru: "d_experience_table", kz: "d_experience_table_k"},
      {ru: "dop", kz: "dop_k"},
      {ru: "dop1", kz: "dop1_k"},
      {ru: "dzo_label2", kz: "dzo_label2_k"}
    ];

    var dzoLabelArr = [
      {ru: "dzo_label1", kz: "dzo_label1_k"},
      {ru: "dzo_label2", kz: "dzo_label2_k"},
      {ru: "dzo_label3", kz: "dzo_label3_k"},
      {ru: "dzo_label4", kz: "dzo_label4_k"},
      {ru: "dzo_label5", kz: "dzo_label5_k"},
      {ru: "dzo_label6", kz: "dzo_label6_k"}
    ];

    model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
      console.log(model.formName + '\nformCode: [' + model.formCode + ']\nformID: [' + model.formId +']\nasfDataID: [' + model.asfDataId+']');
      tableArrKval.forEach(function(table) {
        ALL_DOCUMENTS_UTIL.tableOn(view, table.id, table.cmp);
      });
    });

    if (view.editable) {
      tableArrKval.forEach(function(table) {
        ALL_DOCUMENTS_UTIL.tableEnabled(view, table.id.kz); // блокируем дин.таблицы в каз. варианте
      });
      var tableSArr = ['education_table', 'certificate_table', 'workknowledge_table', 'duties_table', 'competence_table'];
      tableSArr.forEach(function(table) {
        ALL_DOCUMENTS_UTIL.tableEnabled(view, table);
      });
    }


    //1. Задача- Активация компонента Ссылка на реестр в зависимости от выбора вариантов.
    var checkRS = model.getModelWithId("kvalifikation_type");
    if (checkRS.value[0] == "1") {
      view.getViewWithId("link_table_sluzh").container.parent().parent().show();
      view.getViewWithId("link_table_rab").container.parent().parent().hide();
      componentsArr.forEach(function(cmp) {
        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, true);
      });
    } else if (checkRS.value[0] == "2") {
      view.getViewWithId("link_table_sluzh").container.parent().parent().hide();
      view.getViewWithId("link_table_rab").container.parent().parent().show();
      componentsArr.forEach(function(cmp) {
        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, false);
      });
    } else {
      view.getViewWithId("link_table_sluzh").container.parent().parent().hide();
      view.getViewWithId("link_table_rab").container.parent().parent().hide();
    }
    checkRS.on("valueChange", function() {
      if (checkRS.value[0] == "1") {
        view.getViewWithId("link_table_sluzh").container.parent().parent().show();
        view.getViewWithId("link_table_rab").container.parent().parent().hide();
        if (model.getModelWithId("link_table_rab")) model.getModelWithId("link_table_rab").setValue(null);
        componentsArr.forEach(function(cmp) {
          ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, true);
        });
      } else {
        view.getViewWithId("link_table_sluzh").container.parent().parent().hide();
        view.getViewWithId("link_table_rab").container.parent().parent().show();
        if (model.getModelWithId("link_table_sluzh")) model.getModelWithId("link_table_sluzh").setValue(null);
        componentsArr.forEach(function(cmp) {
          ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, false);
        });
      }
      tableArrKval.forEach(function(table) {
        var tmpTable = model.getModelWithId(table.id.ru);
        tmpTable.modelBlocks.forEach(function(modelBlock, index){
          tmpTable.removeRow(index);
        });
        if (tmpTable.modelBlocks.length > 0) {
          tmpTable.modelBlocks.forEach(function(modelBlock, index){
            tmpTable.removeRow(index);
          });
        }
      });
    });

    // 2 Если в реестре «Справочник должностей служащих» данные таблицы не заполнены, в форме «Квал требования» данные таблицы должны быть удалены.
    var linkSluzh = model.getModelWithId("link_table_sluzh");
    // требования ДЗО
    var sluzhAsfData = ALL_DOCUMENTS_UTIL.getAsfDataJsonDocID(linkSluzh.getValue());
    var rang = null;
    var trebDzo = null;
    if (sluzhAsfData) {
      rang = ALL_DOCUMENTS_UTIL.getAsfDataObject(sluzhAsfData, 'rang');
      trebDzo = ALL_DOCUMENTS_UTIL.getAsfDataObject(sluzhAsfData, 'DZO_CHECK');
    }
    if (rang) {
      console.log('------------------rang------------------');
      console.log(rang);
      rang = rang.value;
      if (rang == "РВ" || rang == "РС" || rang == "РМ" || rang == "ТМ") {
        componentsArr.forEach(function(cmp) {
          ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, true);
        });
      } else {
        componentsArr.forEach(function(cmp) {
          ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, false);
        });
      }
    }
    if (trebDzo) {
      console.log('------------------trebDzo------------------');
      console.log(trebDzo);
      trebDzo = trebDzo.value;
      if (trebDzo == "1") {
        dzoLabelArr.forEach(function(cmp) {
          ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, false);
        });
        dzoTableArr.forEach(function(table) {
          var tmpTable = model.getModelWithId(table.ru);
          if (tmpTable.modelBlocks.length > 0) {
            tmpTable.modelBlocks.forEach(function(modelBlock, index){
              tmpTable.removeRow(index);
            });
          }
          ALL_DOCUMENTS_UTIL.setVisibleCmp(view, table, false);
        });
      } else {
        dzoLabelArr.forEach(function(cmp) {
          ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, true);
        });
        dzoTableArr.forEach(function(table) {
          ALL_DOCUMENTS_UTIL.setVisibleCmp(view, table, true);
        });
      }
    }

    linkSluzh.on("valueChange", function() {
      var sluzhAsfData = ALL_DOCUMENTS_UTIL.getAsfDataJsonDocID(linkSluzh.getValue());
      var rang = null;
      var trebDzo = null;
      if (sluzhAsfData) {
        rang = ALL_DOCUMENTS_UTIL.getAsfDataObject(sluzhAsfData, 'rang');
        trebDzo = ALL_DOCUMENTS_UTIL.getAsfDataObject(sluzhAsfData, 'DZO_CHECK');
      }
      if (rang) {
        console.log('------------------rang------------------');
        console.log(rang);
        rang = rang.value;
        if (rang == "РВ" || rang == "РС" || rang == "РМ" || rang == "ТМ") {
          componentsArr.forEach(function(cmp) {
            ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, true);
          });
        } else {
          componentsArr.forEach(function(cmp) {
            ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, false);
          });
        }
      }
      if (trebDzo) {
        console.log('------------------trebDzo------------------');
        console.log(trebDzo);
        trebDzo = trebDzo.value;
        if (trebDzo == "1") {
          dzoLabelArr.forEach(function(cmp) {
            ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, false);
          });
          dzoTableArr.forEach(function(table) {
            var tmpTable = model.getModelWithId(table.ru);
            if (tmpTable.modelBlocks.length > 0) {
              tmpTable.modelBlocks.forEach(function(modelBlock, index){
                tmpTable.removeRow(index);
              });
            }
            ALL_DOCUMENTS_UTIL.setVisibleCmp(view, table, false);
          });
        } else {
          dzoLabelArr.forEach(function(cmp) {
            ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, true);
          });
          dzoTableArr.forEach(function(table) {
            ALL_DOCUMENTS_UTIL.setVisibleCmp(view, table, true);
          });
        }
      }
    });

    // ----------------------------------------------------------------------

    // 3. Задача - сопоставление таблиц при выборе реестра, в зависимости от разряда работника.
    var razryadList = model.getModelWithId("razryad");
    var linkRab = model.getModelWithId("link_table_rab");

    var educationTable = model.getModelWithId('education_table');
    var certificateTable = model.getModelWithId('certificate_table');
    var workknowledgeTable = model.getModelWithId('workknowledge_table');
    var dutiesTable = model.getModelWithId('duties_table');
    var competenceTable = model.getModelWithId('competence_table');

    razryadList.on("valueChange", function() {

      if (dutiesTable.modelBlocks.length > 0) {
        dutiesTable.modelBlocks.forEach(function(modelBlock, index){
          dutiesTable.removeRow(index);
        });
      }

      var razryad = razryadList.getValue()[0];
      if (!linkRab.getValue()) {
        view.getViewWithId("link_table_rab").container.parent().parent().css("background-color", "#FF9999");
        ALL_DOCUMENTS_UTIL.msgShow("Не выбран справочник рабочих профессий");
        if (razryadList) razryadList.setValue("9");
      } else {
        view.getViewWithId("link_table_rab").container.parent().parent().css("background-color", "");

        var table_rab_json = ALL_DOCUMENTS_UTIL.getAsfDataJsonDocID(linkRab.getValue());

        if (table_rab_json) {

          var educationTableJson = ALL_DOCUMENTS_UTIL.getAsfDataObject(table_rab_json, 'education_table');
          var educationTableJsonKz = ALL_DOCUMENTS_UTIL.getAsfDataObject(table_rab_json, 'education_table_k');

          var educationValueLevel = getArrayValue(view, educationTableJson, 'education_level', razryad, 'education_razryad', true);
          var educationValueType = getArrayValue(view, educationTableJson, 'education_type', razryad, 'education_razryad');
          var educationValueTypeK = getArrayValue(view, educationTableJsonKz, 'education_type_k', razryad, 'education_razryad_k');
          var educationValueYear = getArrayValue(view, educationTableJson, 'education_year', razryad, 'education_razryad', true);
          var educationValueYear1 = getArrayValue(view, educationTableJson, 'education_year1', razryad, 'education_razryad');

          console.log('---------------------education_table---------------------');
          console.log(educationValueLevel);
          console.log(educationValueType);
          console.log(educationValueTypeK);
          console.log(educationValueYear);
          console.log(educationValueYear1);
          console.log('---------------------education_table---------------------');
          if (educationValueType.length > 0) {
            if (educationTable.modelBlocks.length > 0) {
              educationTable.modelBlocks.forEach(function(modelBlock, index){
                educationTable.removeRow(index);
              });
              if (educationTable.modelBlocks.length > 0) {
                educationTable.modelBlocks.forEach(function(modelBlock, index){
                  educationTable.removeRow(index);
                });
              }
            }
            if (educationTable.modelBlocks.length == 0) {
              educationValueType.forEach(function(){
                educationTable.createRow();
              });
            }
            var educationIndex = 0;
            educationTable.on('tableRowAdd', function(evt, modelTab, modelRow) {
              setTimeout(function(){
                if (model.getModelWithId('education_level', 'education_table', modelRow[0].asfProperty.tableBlockIndex)) model.getModelWithId('education_level', 'education_table', modelRow[0].asfProperty.tableBlockIndex).setValue(educationValueLevel[educationIndex]);
                if (model.getModelWithId('education_type', 'education_table', modelRow[0].asfProperty.tableBlockIndex)) model.getModelWithId('education_type', 'education_table', modelRow[0].asfProperty.tableBlockIndex).setValue(educationValueType[educationIndex]);
                if (model.getModelWithId('education_type_k', 'education_table_k', modelRow[0].asfProperty.tableBlockIndex)) model.getModelWithId('education_type_k', 'education_table_k', modelRow[0].asfProperty.tableBlockIndex).setValue(educationValueTypeK[educationIndex]);
                if (model.getModelWithId('education_year', 'education_table', modelRow[0].asfProperty.tableBlockIndex)) model.getModelWithId('education_year', 'education_table', modelRow[0].asfProperty.tableBlockIndex).setValue(educationValueYear[educationIndex]);
                if (model.getModelWithId('education_year1', 'education_table', modelRow[0].asfProperty.tableBlockIndex)) model.getModelWithId('education_year1', 'education_table', modelRow[0].asfProperty.tableBlockIndex).setValue(educationValueYear1[educationIndex]);
                educationIndex++;
                ALL_DOCUMENTS_UTIL.tableEnabled(view, 'education_table');
                ALL_DOCUMENTS_UTIL.tableEnabled(view, 'education_table_k');
              }, 0);
            });
          } else {
            if (educationTable.modelBlocks.length > 0) {
              educationTable.modelBlocks.forEach(function(modelBlock, index){
                educationTable.removeRow(index);
              });
            }
          }

          var certificateTableJson = ALL_DOCUMENTS_UTIL.getAsfDataObject(table_rab_json, 'certificate_table');
          var certificateTableJsonKz = ALL_DOCUMENTS_UTIL.getAsfDataObject(table_rab_json, 'certificate_table_k');

          var certificateValue = getArrayValue(view, certificateTableJson, 'certificate_about', razryad, 'certificate_razrayd');
          var certificateValue_k = getArrayValue(view, certificateTableJsonKz, 'certificate_about_k', razryad, 'certificate_razrayd_k');
          console.log('---------------------certificate_table---------------------');
          console.log(certificateValue);
          console.log(certificateValue_k);
          console.log('---------------------certificate_table---------------------');
          if (certificateValue.length > 0) {
            if (certificateTable.modelBlocks.length == 0) {
              certificateValue.forEach(function(b){
                certificateTable.createRow();
              });
            } else {
              certificateTable.modelBlocks.forEach(function(modelBlock, index){
                certificateTable.removeRow(index);
              });
              if (certificateTable.modelBlocks.length == 0) {
                certificateValue.forEach(function(b){
                  certificateTable.createRow();
                });
              }
            }
            var certificateIndex = 0;
            certificateTable.on('tableRowAdd', function(evt, modelTab, modelRow) {
              setTimeout(function(){
                if (model.getModelWithId('certificate_about', 'certificate_table', modelRow[0].asfProperty.tableBlockIndex)) model.getModelWithId('certificate_about', 'certificate_table', modelRow[0].asfProperty.tableBlockIndex).setValue(certificateValue[certificateIndex]);
                if (model.getModelWithId('certificate_about_k', 'certificate_table_k', modelRow[0].asfProperty.tableBlockIndex)) model.getModelWithId('certificate_about_k', 'certificate_table_k', modelRow[0].asfProperty.tableBlockIndex).setValue(certificateValue_k[certificateIndex]);
                certificateIndex++;
                ALL_DOCUMENTS_UTIL.tableEnabled(view, 'certificate_table');
                ALL_DOCUMENTS_UTIL.tableEnabled(view, 'certificate_table_k');
              }, 0);
            });
          } else {
            if (certificateTable.modelBlocks.length > 0) {
              certificateTable.modelBlocks.forEach(function(modelBlock, index){
                certificateTable.removeRow(index);
              });
            }
          }

          var workknowledgeTableJson = ALL_DOCUMENTS_UTIL.getAsfDataObject(table_rab_json, 'workknowledge_table');
          var workknowledgeTableJsonKz = ALL_DOCUMENTS_UTIL.getAsfDataObject(table_rab_json, 'workknowledge_table_k');

          var workknowledgeValue = getArrayValue(view, workknowledgeTableJson, 'workknowledge_about', razryad, 'workknowledge_razryad');
          var workknowledgeValue_k = getArrayValue(view, workknowledgeTableJsonKz, 'workknowledge_about_k', razryad, 'workknowledge_razryad_k');
          console.log('---------------------workknowledge_table---------------------');
          console.log(workknowledgeValue);
          console.log(workknowledgeValue_k);
          console.log('---------------------workknowledge_table---------------------');
          if (workknowledgeValue.length > 0) {
            if (workknowledgeTable.modelBlocks.length == 0) {
              workknowledgeValue.forEach(function(b){
                workknowledgeTable.createRow();
              });
            } else {
              workknowledgeTable.modelBlocks.forEach(function(modelBlock, index){
                workknowledgeTable.removeRow(index);
              });
              if (workknowledgeTable.modelBlocks.length == 0) {
                workknowledgeValue.forEach(function(b){
                  workknowledgeTable.createRow();
                });
              }
            }
            var workknowledgeIndex = 0;
            workknowledgeTable.on('tableRowAdd', function(evt, modelTab, modelRow) {
              setTimeout(function(){
                if (model.getModelWithId('workknowledge_about', 'workknowledge_table', modelRow[0].asfProperty.tableBlockIndex)) model.getModelWithId('workknowledge_about', 'workknowledge_table', modelRow[0].asfProperty.tableBlockIndex).setValue(workknowledgeValue[workknowledgeIndex]);
                if (model.getModelWithId('workknowledge_about_k', 'workknowledge_table_k', modelRow[0].asfProperty.tableBlockIndex)) model.getModelWithId('workknowledge_about_k', 'workknowledge_table_k', modelRow[0].asfProperty.tableBlockIndex).setValue(workknowledgeValue_k[workknowledgeIndex]);
                workknowledgeIndex++;
                ALL_DOCUMENTS_UTIL.tableEnabled(view, 'workknowledge_table');
                ALL_DOCUMENTS_UTIL.tableEnabled(view, 'workknowledge_table_k');
              }, 0);
            });
          } else {
            if (workknowledgeTable.modelBlocks.length > 0) {
              workknowledgeTable.modelBlocks.forEach(function(modelBlock, index){
                workknowledgeTable.removeRow(index);
              });
            }
          }

          var dutiesTableJson = ALL_DOCUMENTS_UTIL.getAsfDataObject(table_rab_json, 'duties_table');
          var dutiesTableJsonKz = ALL_DOCUMENTS_UTIL.getAsfDataObject(table_rab_json, 'duties_table_k');

          var dutiesValue = getArrayValue(view, dutiesTableJson, 'duties_about', razryad, 'duties_razrayd');
          var dutiesValue_k = getArrayValue(view, dutiesTableJsonKz, 'duties_about_k', razryad, 'duties_razrayd_k');
          console.log('---------------------duties_table---------------------');
          console.log(dutiesValue);
          console.log(dutiesValue_k);
          console.log('---------------------duties_table---------------------');
          if (dutiesValue.length > 0) {
            if (dutiesTable.modelBlocks.length == 0) {
              dutiesValue.forEach(function(b){
                dutiesTable.createRow();
              });
            } else {
              dutiesTable.modelBlocks.forEach(function(modelBlock, index){
                dutiesTable.removeRow(index);
              });
              if (dutiesTable.modelBlocks.length == 0) {
                dutiesValue.forEach(function(b){
                  dutiesTable.createRow();
                });
              }
            }
            var dutiesIndex = 0;
            dutiesTable.on('tableRowAdd', function(evt, modelTab, modelRow) {
              setTimeout(function(){
                if (model.getModelWithId('duties_about', 'duties_table', modelRow[0].asfProperty.tableBlockIndex)) model.getModelWithId('duties_about', 'duties_table', modelRow[0].asfProperty.tableBlockIndex).setValue(dutiesValue[dutiesIndex]);
                if (model.getModelWithId('duties_about_k', 'duties_table_k', modelRow[0].asfProperty.tableBlockIndex)) model.getModelWithId('duties_about_k', 'duties_table_k', modelRow[0].asfProperty.tableBlockIndex).setValue(dutiesValue_k[dutiesIndex]);
                dutiesIndex++;
                ALL_DOCUMENTS_UTIL.tableEnabled(view, 'duties_table');
                ALL_DOCUMENTS_UTIL.tableEnabled(view, 'duties_table_k');
              }, 0);
            });
          } else {
            if (dutiesTable.modelBlocks.length > 0) {
              dutiesTable.modelBlocks.forEach(function(modelBlock, index){
                dutiesTable.removeRow(index);
              });
            }
          }

          var competenceTableJson = ALL_DOCUMENTS_UTIL.getAsfDataObject(table_rab_json, 'competence_table');
          var competenceTableJsonKz = ALL_DOCUMENTS_UTIL.getAsfDataObject(table_rab_json, 'competence_table_k');

          var competenceValue = getArrayValue(view, competenceTableJson, 'competence_about', razryad, 'competence_razryad');
          var competenceValue_k = getArrayValue(view, competenceTableJsonKz, 'competence_about_k', razryad, 'competence_razryad_k');
          console.log('---------------------competence_table---------------------');
          console.log(competenceValue);
          console.log(competenceValue_k);
          console.log('---------------------competence_table---------------------');
          if (competenceValue.length > 0) {
            if (competenceTable.modelBlocks.length == 0) {
              competenceValue.forEach(function(b){
                competenceTable.createRow();
              });
            } else {
              competenceTable.modelBlocks.forEach(function(modelBlock, index){
                competenceTable.removeRow(index);
              });
              if (competenceTable.modelBlocks.length == 0) {
                competenceValue.forEach(function(b){
                  competenceTable.createRow();
                });
              }
            }
            var competenceIndex = 0;
            competenceTable.on('tableRowAdd', function(evt, modelTab, modelRow) {
              setTimeout(function(){
                if (model.getModelWithId('competence_about', 'competence_table', modelRow[0].asfProperty.tableBlockIndex)) model.getModelWithId('competence_about', 'competence_table', modelRow[0].asfProperty.tableBlockIndex).setValue(competenceValue[competenceIndex]);
                if (model.getModelWithId('competence_about_k', 'competence_table_k', modelRow[0].asfProperty.tableBlockIndex)) model.getModelWithId('competence_about_k', 'competence_table_k', modelRow[0].asfProperty.tableBlockIndex).setValue(competenceValue_k[competenceIndex]);
                competenceIndex++;
                ALL_DOCUMENTS_UTIL.tableEnabled(view, 'competence_table');
                ALL_DOCUMENTS_UTIL.tableEnabled(view, 'competence_table_k');
              }, 0);
            });
          } else {
            if (competenceTable.modelBlocks.length > 0) {
              competenceTable.modelBlocks.forEach(function(modelBlock, index){
                competenceTable.removeRow(index);
              });
            }
          }

          if (educationValueLevel.length == 0
            && educationValueType.length == 0
            && educationValueYear.length == 0
            && educationValueYear1.length == 0
            && certificateValue.length == 0
            && workknowledgeValue.length == 0
            && dutiesValue.length == 0
            && competenceValue.length == 0) {
              ALL_DOCUMENTS_UTIL.msgShow("В выбранном справочнике нет разряда " + razryad);
            }

        } //if (table_rab_json)

      }
    });

    // требования ДЗО
    var rabAsfData = ALL_DOCUMENTS_UTIL.getAsfDataJsonDocID(linkRab.getValue());
    var trebDzoRab = null;
    if (rabAsfData) trebDzoRab = ALL_DOCUMENTS_UTIL.getAsfDataObject(rabAsfData, 'DZO_CHECK');
    if (trebDzoRab) {
      console.log('------------------trebDzoRab------------------');
      console.log(trebDzoRab);
      trebDzoRab = trebDzoRab.value;
      if (trebDzoRab == "1") {
        dzoLabelArr.forEach(function(cmp) {
          ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, false);
        });
        dzoTableArr.forEach(function(table) {
          var tmpTable = model.getModelWithId(table.ru);
          if (tmpTable.modelBlocks.length > 0) {
            tmpTable.modelBlocks.forEach(function(modelBlock, index){
              tmpTable.removeRow(index);
            });
          }
          ALL_DOCUMENTS_UTIL.setVisibleCmp(view, table, false);
        });
      } else {
        dzoLabelArr.forEach(function(cmp) {
          ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, true);
        });
        dzoTableArr.forEach(function(table) {
          ALL_DOCUMENTS_UTIL.setVisibleCmp(view, table, true);
        });
      }
    }

    linkRab.on("valueChange", function() {
      var tableRemove = ["education_table", "certificate_table", "workknowledge_table", "duties_table", "competence_table",
                         "education_table_k", "certificate_table_k", "workknowledge_table_k", "duties_table_k", "competence_table_k"];
      tableRemove.forEach(function(cmpTable){
        model.getModelWithId(cmpTable).modelBlocks.forEach(function(modelBlock, index){
          model.getModelWithId(cmpTable).removeRow(index);
        });
      });
      if (linkRab.getValue()) view.getViewWithId("link_table_rab").container.parent().parent().css("background-color", "");

      // требования ДЗО
      var rabAsfData = ALL_DOCUMENTS_UTIL.getAsfDataJsonDocID(linkRab.getValue());
      var trebDzoRab = null;
      if (rabAsfData) trebDzoRab = ALL_DOCUMENTS_UTIL.getAsfDataObject(rabAsfData, 'DZO_CHECK');
      if (trebDzoRab) {
        console.log('------------------trebDzoRab------------------');
        console.log(trebDzoRab);
        trebDzoRab = trebDzoRab.value;
        if (trebDzoRab == "1") {
          dzoLabelArr.forEach(function(cmp) {
            ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, false);
          });
          dzoTableArr.forEach(function(table) {
            var tmpTable = model.getModelWithId(table.ru);
            if (tmpTable.modelBlocks.length > 0) {
              tmpTable.modelBlocks.forEach(function(modelBlock, index){
                tmpTable.removeRow(index);
              });
            }
            ALL_DOCUMENTS_UTIL.setVisibleCmp(view, table, false);
          });
        } else {
          dzoLabelArr.forEach(function(cmp) {
            ALL_DOCUMENTS_UTIL.setVisibleCmp(view, cmp, true);
          });
          dzoTableArr.forEach(function(table) {
            ALL_DOCUMENTS_UTIL.setVisibleCmp(view, table, true);
          });
        }
      }
    });

    // ----------------------------------------------------------------------

  }
});


var getArrayValue = function(playerView, tmpTableJson, cmp, razryad, razryadCmp, key) {
  tmpTableJson = tmpTableJson.data;
  var tmpValue = [];
  var indexArr = [];
  tmpTableJson.forEach(function(data) {
    var tmpId = data.id.substring(0, data.id.indexOf('-b'));
    if (tmpId == razryadCmp && data.value == razryad) {
      indexArr.push(data.id.substring(data.id.indexOf('-b')));
    }
  });
  if (indexArr.length > 0) {
    indexArr.forEach(function(b){
      tmpTableJson.forEach(function(data) {
        if (data.id == cmp + b) {
          if (key) {
            tmpValue.push(data.key);
          } else {
            tmpValue.push(data.value);
          }
        }
      });
    });
  }
  return tmpValue;
};
