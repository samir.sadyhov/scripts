AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {

  // "Приказ о приеме на работу на определенный срок"
  if (model.formCode == "Приказ_о_приеме_на_работу_на_определенный_срок"
   || model.formCode == "Приказ_о_внесении_изменений_в_приказ_о_приеме_на_работу_на_определенный_срок") {
    // ограничение до 6 месяцев
    var dateSrokS = model.getModelWithId("srok");
    var dateSrokPo = model.getModelWithId("srok2");
    dateSrokPo.on("valueChange", function() {
      if (AS.FORMS.DateUtils.parseDate(dateSrokS.getValue()) && AS.FORMS.DateUtils.parseDate(dateSrokPo.getValue()) ) {
        if (AS.FORMS.DateUtils.parseDate(dateSrokPo.getValue()).getTime() > AS.FORMS.DateUtils.parseDate(ALL_DOCUMENTS_UTIL.getDatePlusMonth(dateSrokS.getValue(), 6)).getTime()) {
          view.getViewWithId(dateSrokPo.asfProperty.id).container.parent().css("background-color", "#FF9999");
          view.getViewWithId(dateSrokPo.asfProperty.id+"_k").container.parent().css("background-color", "#FF9999");
          ALL_DOCUMENTS_UTIL.msgShow("Испытательный срок не может превышать 6 месяцев");
          dateSrokPo.setValue(null);
        } else {
          view.getViewWithId(dateSrokPo.asfProperty.id).container.parent().css("background-color", "");
          view.getViewWithId(dateSrokPo.asfProperty.id+"_k").container.parent().css("background-color", "");
        }
      }
    });
    dateSrokS.on("valueChange", function() {
      if (AS.FORMS.DateUtils.parseDate(dateSrokS.getValue()) && AS.FORMS.DateUtils.parseDate(dateSrokPo.getValue()) ) {
        if (AS.FORMS.DateUtils.parseDate(dateSrokPo.getValue()).getTime() > AS.FORMS.DateUtils.parseDate(ALL_DOCUMENTS_UTIL.getDatePlusMonth(dateSrokS.getValue(), 6)).getTime()) {
          view.getViewWithId(dateSrokPo.asfProperty.id).container.parent().css("background-color", "#FF9999");
          view.getViewWithId(dateSrokPo.asfProperty.id+"_k").container.parent().css("background-color", "#FF9999");
          ALL_DOCUMENTS_UTIL.msgShow("Испытательный срок не может превышать 6 месяцев");
          dateSrokPo.setValue(null);
        } else {
          view.getViewWithId(dateSrokPo.asfProperty.id).container.parent().css("background-color", "");
          view.getViewWithId(dateSrokPo.asfProperty.id+"_k").container.parent().css("background-color", "");
        }
      }
    });

    // с испытательным сроком / без испытательного срока
    var checkSrok = model.getModelWithId("ispit_srok");
    var srokCmpArr = ["srok", "srok_k", "srok2", "srok2_k"];
    if (checkSrok.getValue() == "1") {
      srokCmpArr.forEach(function(cmp) {
        view.getViewWithId(cmp).container.parent().parent().show();
      });
    } else {
      srokCmpArr.forEach(function(cmp) {
        view.getViewWithId(cmp).container.parent().parent().hide();
      });
      dateSrokS.setValue(null);
      dateSrokPo.setValue(null);
      dateSrokS.asfProperty.required=false;
      dateSrokPo.asfProperty.required=false;
      view.getViewWithId(dateSrokPo.asfProperty.id).container.parent().css("background-color", "");
      view.getViewWithId(dateSrokPo.asfProperty.id+"_k").container.parent().css("background-color", "");
    }
    checkSrok.on("valueChange", function() {
      if (checkSrok.getValue() == "1") {
        srokCmpArr.forEach(function(cmp) {
          view.getViewWithId(cmp).container.parent().parent().show();
        });
      } else {
        srokCmpArr.forEach(function(cmp) {
          view.getViewWithId(cmp).container.parent().parent().hide();
        });
        dateSrokS.setValue(null);
        dateSrokPo.setValue(null);
        dateSrokS.asfProperty.required=false;
        dateSrokPo.asfProperty.required=false;
        view.getViewWithId(dateSrokPo.asfProperty.id).container.parent().css("background-color", "");
        view.getViewWithId(dateSrokPo.asfProperty.id+"_k").container.parent().css("background-color", "");
      }
    });

    // логика  С-ПО
    ALL_DOCUMENTS_UTIL.compareDates(view, "start_date", "srok", null, null, null);
    ALL_DOCUMENTS_UTIL.compareDates(view, "start_date", "finish_date", null, null, null);
    ALL_DOCUMENTS_UTIL.compareDates(view, "srok", "srok2", null, null, null);

    // должность\профессия
    var positionOznak = model.getModelWithId("position3");
    positionOznak.on("valueChange", function() {
      if (positionOznak.value.length > 0) ALL_DOCUMENTS_UTIL.setValueChoiceFromPosition(view, positionOznak.value[0].elementID, ["choice10", "choice11", "choice12"]);
    });

    // ознакомление
    ALL_DOCUMENTS_UTIL.copyCmpValue(view, ["user", "user", "user"], ["user5", "user3", "user6"]);
  }

  // "Приказ о приеме на работу на неопределенный срок"
  if (model.formCode == "Приказ_о_приеме_на_работу_на_неопределенный_срок"
   || model.formCode == "Приказ_о_внесении_изменений_в_приказ_приеме_на_работу_на_неопределенный_срок") {
    // ограничение до 6 месяцев
    var dateStart = model.getModelWithId("start_date");
    var dateSrok = model.getModelWithId("srok");
    if (dateSrok) {
      dateSrok.on("valueChange", function() {
        if (AS.FORMS.DateUtils.parseDate(dateStart.getValue()) !=null && AS.FORMS.DateUtils.parseDate(dateSrok.getValue()) !=null ) {
          if (AS.FORMS.DateUtils.parseDate(dateSrok.getValue()).getTime() > AS.FORMS.DateUtils.parseDate(ALL_DOCUMENTS_UTIL.getDatePlusMonth(dateStart.getValue(), 6)).getTime()) {
            view.getViewWithId(dateSrok.asfProperty.id).container.parent().css("background-color", "#FF9999");
            view.getViewWithId(dateSrok.asfProperty.id+"_k").container.parent().css("background-color", "#FF9999");
            ALL_DOCUMENTS_UTIL.msgShow("Испытательный срок не может превышать 6 месяцев");
            dateSrok.setValue(null);
          } else {
            view.getViewWithId(dateSrok.asfProperty.id).container.parent().css("background-color", "");
            view.getViewWithId(dateSrok.asfProperty.id+"_k").container.parent().css("background-color", "");
          }
        }
      });
    }
    // логика  С-ПО
    ALL_DOCUMENTS_UTIL.compareDates(view, "start_date", "srok", null, null, null);
    ALL_DOCUMENTS_UTIL.compareDates(view, "start_date", "finish_date", null, null, null);
    ALL_DOCUMENTS_UTIL.compareDates(view, "srok", "srok2", null, null, null);
    // должность\профессия
    var positionOznak = model.getModelWithId("position3");
    positionOznak.on("valueChange", function() {
      if (positionOznak.value.length > 0) ALL_DOCUMENTS_UTIL.setValueChoiceFromPosition(view, positionOznak.value[0].elementID, ["choice10", "choice11", "choice12"]);
    });
    // ознакомление
    ALL_DOCUMENTS_UTIL.copyCmpValue(view, ["user", "user", "user"], ["user5", "user3", "user6"]);
  }

  // "Приказ о приеме на работу на период отсутствующего сотр."
  // "Приказ о внесении изменений в приказ о приеме на работу на период отсутствующего сотр."
  if (model.formCode == "Приказ_о_приеме_на_работу_на_период_временно_отсутствующего_работника/выполнения_определенной_работы/выполнения_сезонной_работы"
  || model.formCode == "Приказ_о_внесении_изменений_в_приказ_о_приеме_на_работу_на_период_временно_отсутствующего_работника/выполнения_определенной_работы/выполнения_сезонной_работы") {
    // ограничение до 6 месяцев
    var dateStart = model.getModelWithId("start_date");
    var dateSrok = null;
    if (model.getModelWithId("srok") && model.getModelWithId("srok").asfProperty.type == "date") dateSrok = model.getModelWithId("srok");
    if (model.getModelWithId("ispit_srok") && model.getModelWithId("ispit_srok").asfProperty.type == "date") dateSrok = model.getModelWithId("ispit_srok");
    if (dateSrok) {
      dateSrok.on("valueChange", function() {
        if (AS.FORMS.DateUtils.parseDate(dateStart.getValue()) !=null && AS.FORMS.DateUtils.parseDate(dateSrok.getValue()) !=null) {
          if (AS.FORMS.DateUtils.parseDate(dateSrok.getValue()).getTime() > AS.FORMS.DateUtils.parseDate(ALL_DOCUMENTS_UTIL.getDatePlusMonth(dateStart.getValue(), 6)).getTime()) {
            view.getViewWithId(dateSrok.asfProperty.id).container.parent().css("background-color", "#FF9999");
            view.getViewWithId(dateSrok.asfProperty.id+"_k").container.parent().css("background-color", "#FF9999");
            ALL_DOCUMENTS_UTIL.msgShow("Испытательный срок не может превышать 6 месяцев");
            dateSrok.setValue(null);
          } else {
            view.getViewWithId(dateSrok.asfProperty.id).container.parent().css("background-color", "");
            view.getViewWithId(dateSrok.asfProperty.id+"_k").container.parent().css("background-color", "");
          }
        }
      });
    }
    // должность\профессия
    var positionOznak = model.getModelWithId("position3");
    positionOznak.on("valueChange", function() {
      if (positionOznak.value.length > 0) ALL_DOCUMENTS_UTIL.setValueChoiceFromPosition(view, positionOznak.value[0].elementID, ["choice3", "choice4", "choice7"]);
    });

    ALL_DOCUMENTS_UTIL.copyCmpValue(view, ["user", "user", "user", "position"], ["user6", "user3", "user7", "position3"]);

    // дата приема, дата испытательного
    ALL_DOCUMENTS_UTIL.compareDates(view, "start_date", "ispit_srok", null, null, null);

    // логика с таблицами
    var periodCmp = model.getModelWithId("period");
    var tab1 = {ru: "table2_r", kz: "table2_k"};
    var tab2 = {ru: "table3_r", kz: "table3_k"};

    // блокируем две таблицы ниже первого блока
    // if (view.editable) {
    //   ALL_DOCUMENTS_UTIL.tableEnabled(view, tab1.ru);
    //   ALL_DOCUMENTS_UTIL.tableEnabled(view, tab2.ru);
    // }

    if (periodCmp.value[0] == "1") {
      ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tab2, false);
      ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tab1, true);
    } else {
      ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tab2, true);
      ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tab1, false);
    }
    periodCmp.on("valueChange", function() {
      var tt1 = model.getModelWithId(tab1.ru);
      var tt2 = model.getModelWithId(tab2.ru);
      if (periodCmp.value[0] == "1") {
        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tab2, false);
        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tab1, true);
        tt2.modelBlocks.forEach(function(modelBlock, index){
          tt2.removeRow(index);
        });
        if (tt1.modelBlocks.length == 0) {
          tt1.createRow();
        } else {
          tt1.modelBlocks.forEach(function(modelBlock, index){
            tt1.removeRow(index);
          });
          if (tt1.modelBlocks.length == 0) tt1.createRow();
        }
        setTimeout(ALL_DOCUMENTS_UTIL.tableEnabled, 0, view, tab1.ru);
      } else {
        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tab2, true);
        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tab1, false);
        tt2.modelBlocks.forEach(function(modelBlock, index){
          tt2.removeRow(index);
        });
        if (tt1.modelBlocks.length > 0) {
          tt1.modelBlocks.forEach(function(modelBlock, index){
            tt1.removeRow(index);
          });
        }
        if (tt2.modelBlocks.length == 0) {
          tt2.createRow();
        } else {
          tt2.modelBlocks.forEach(function(modelBlock, index){
            tt2.removeRow(index);
          });
          if (tt2.modelBlocks.length == 0) tt2.createRow();
        }
        setTimeout(ALL_DOCUMENTS_UTIL.tableEnabled, 0, view, tab2.ru);
      }
    });

  }

});
