AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  if (model.formCode == "Штатная_расстановка111") {

    var setColorRow = function(cmp, color) {
      setTimeout(function(){
        var tmpEl = $('[data-asformid $= "container.'+cmp+'"]:odd');
        if (tmpEl.length > 0) {
          for (var i = 0; i < tmpEl.length; i++) {
            $(tmpEl[i]).parent().parent().css("background-color", color);
          }
        }
      }, 0);
    };

    // Подсветка заголовков таблиц
    var tablesHeader = [
      // Общие данные
      ['cmp-x8jdun', 'cmp-5k7cjx', 'cmp-r1wqag', 'cmp-11klcs', 'cmp-6tux6w', 'cmp-60rjm7', 'cmp-70gx6p', 'cmp-xkr85t', 'cmp-xng41t', 'cmp-lw7sgo', 'cmp-b80csx', 'cmp-mgkqwf', 'cmp-t5tluh', 'cmp-xr3y4c', 'cmp-a0dnjv', 'cmp-h4si7e', 'cmp-9c9oja', 'cmp-pkkhux', 'cmp-aolx1s', 'cmp-h5umsp', 'cmp-a28vsb', 'cmp-73483a', 'cmp-6bfphg', 'cmp-wx9g0b', 'cmp-2a0mok', 'cmp-yc8vet', 'cmp-rfowkd', 'cmp-fm1fld', 'cmp-y6110g', 'cmp-pmbdfr', 'cmp-6wvfp2', 'cmp-nh3irs', 'cmp-4cyyw3', 'cmp-erjs8a', 'cmp-kqpa2r', 'cmp-zef9ht', 'cmp-byaqr7'],
      // Документы
      ['cmp-vjgskl', 'cmp-m7x55i', 'cmp-g5wpye'],
      // Воинская обязанность
      ['cmp-lql0cj', 'cmp-nnoluo', 'cmp-znlhlx', 'cmp-or59oe', 'cmp-5roxw8'],
      // Образование
      ['cmp-6i7qqm', 'cmp-321dxh', 'cmp-fgxb7y', 'cmp-dc881q', 'cmp-jlcx3a', 'cmp-vn63nh', 'cmp-uxm27v', 'cmp-lreqj5', 'cmp-p36n0x', 'cmp-4essm5', 'cmp-9e5y4n', 'cmp-3mx4bt'],
      // Повышение квалификации
      ['cmp-4g1umr', 'cmp-fxjfin', 'cmp-ujtoxd', 'cmp-1q9c36'],
      // Знание языков
      ['cmp-v6diqp', 'cmp-isfbay'],
      // Ученая степень
      ['cmp-v8yq0l', 'cmp-a25l2v', 'cmp-m53sqy', 'cmp-uptzsr'],
      // Ученые звания
      ['cmp-vi40vt', 'cmp-1syumu'],
      // Научные труды
      ['cmp-1ph7by', 'cmp-4i7q5g', 'cmp-n65l4o', 'cmp-77qto3'],
      // Изобретения
      ['cmp-twuyvl', 'cmp-1c231n', 'cmp-arouut', 'cmp-yakg0s', 'cmp-03xati'],
      // Участие в выборных органах
      ['cmp-8z0uox', 'cmp-f37i8g', 'cmp-rivjkh', 'cmp-egzay6', 'cmp-e8nlit', 'cmp-qcho0b', 'cmp-ais6lk'],
      // Госнаграды
      ['cmp-d3dwht', 'cmp-odrbjr']
    ];

    var color = {
      background: "#40434C", //"#F5F5F5",
      text: "#FFFFFF",//"#58B0DC",
      row: "#A3BCD5",
      rowDel: "#5B0000"
    };

    setTimeout(function(){
      ALL_DOCUMENTS_UTIL.setColorHeaderTables(tablesHeader, color);
    }, 0);
    var tableDocModel = model.getModelWithId('cmp-u5jrmf');
    if (view.editable) {
      tableDocModel.on('tableRowAdd', function(evt, modelTab, modelRow) {
        setTimeout(function(){
          ALL_DOCUMENTS_UTIL.setColorHeaderTables([['cmp-vjgskl', 'cmp-m7x55i', 'cmp-g5wpye']], color);
        }, 0);
      });
    }

    // Подсветка строк в таблице
    var tableAndRow = [
      {tableID: 'education', cmpID: 'name_of_school'},
      {tableID: 'upqualificatin', cmpID: 'nameofcourse'},
      {tableID: 'languages', cmpID: 'language'},
      {tableID: 'degree', cmpID: 'nameofdegree'},
      {tableID: 'academic_title', cmpID: 'nameofacademic'},
      {tableID: 'treatise', cmpID: 'nameoftreatise'},
      {tableID: 'invention', cmpID: 'nameofinvention'},
      {tableID: 'elected', cmpID: 'nameofelected'},
      {tableID: 'state_awards', cmpID: 'nameofstate_awards'}
    ];
    tableAndRow.forEach(function(d) {
      setColorRow(d.cmpID, color.row);
      model.getModelWithId(d.tableID).on('tableRowAdd', function(evt, modelTab, modelRow) {
        setColorRow(d.cmpID, color.row);
      });
    });

    // выбор таблиц
    var ttt = [
      {id: '1', tableID: 'cmp-i4joqf', hedID: 'z1'},
      {id: '2', tableID: 'education', hedID: 'z2'},
      {id: '3', tableID: 'upqualificatin', hedID: 'z3'},
      {id: '4', tableID: 'languages', hedID: 'z4'},
      {id: '5', tableID: 'degree', hedID: 'z5'},
      {id: '6', tableID: 'academic_title', hedID: 'z6'},
      {id: '7', tableID: 'treatise', hedID: 'z7'},
      {id: '8', tableID: 'invention', hedID: 'z8'},
      {id: '9', tableID: 'elected', hedID: 'z9'},
      {id: '10', tableID: 'state_awards', hedID: 'z10'},
      {id: '11', tableID: 'family_composition', hedID: 'z11'},
      {id: '12', tableID: 'labor_activity', hedID: 'z12'},
      {id: '13', tableID: 'violation', hedID: 'z13'}
    ];

    var choiceTable = model.getModelWithId('choiceTable');

    setTimeout(function(){
      tableHideShow(view, choiceTable.getValue(), ttt);
    }, 0);
    choiceTable.on("valueChange", function() {
      tableHideShow(view, choiceTable.getValue(), ttt);
    });

    var tableHideShow = function(view, choiceValue, ttt) {
      if (choiceValue) {
        ttt.forEach(function(t) {
          if (choiceValue.indexOf(t.id) !== -1) {
            if (view.getViewWithId(t.tableID)) view.getViewWithId(t.tableID).container.parent().parent().show();
            if (view.getViewWithId(t.hedID)) view.getViewWithId(t.hedID).container.parent().parent().show();
          } else {
            if (view.getViewWithId(t.tableID)) view.getViewWithId(t.tableID).container.parent().parent().hide();
            if (view.getViewWithId(t.hedID)) view.getViewWithId(t.hedID).container.parent().parent().hide();
          }
        });
      } else {
        ttt.forEach(function(t) {
          if (view.getViewWithId(t.tableID)) view.getViewWithId(t.tableID).container.parent().parent().show();
          if (view.getViewWithId(t.hedID)) view.getViewWithId(t.hedID).container.parent().parent().show();
        });
      }
    };


  }
});
