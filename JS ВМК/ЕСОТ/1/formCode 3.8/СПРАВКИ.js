AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  if (model.formCode.substring(0,7).toLowerCase() == "справка") {
    var cmpArrSP=[{ru: "doc_num", kz: "doc_num_k"}, {ru: "doc_date", kz: "doc_date_k"}, {ru: "summa", kz: "summa_k"}, {ru: "summa2", kz: "summa2_k"}, {ru: "vznos1", kz: "vznos1_k"}, {ru: "nalog1", kz: "nalog1_k"}, {ru: "uderzhanie1", kz: "uderzhanie1_k"}, {ru: "vyplata1", kz: "vyplata1_k"}, {ru: "podpisant_position", kz: "podpisant_position_k"}, {ru: "podpisant_user", kz: "podpisant_user_k"}, {ru: "podpisant_user2", kz: "podpisant_user2_k"}, {ru: "ispolnitel_user", kz: "ispolnitel_user_k"}, {ru: "ispolnitel_phone", kz: "ispolnitel_phone_k"}, {ru: "position2", kz: "position2_k"}, {ru: "finish_date", kz: "finish_date_k"}, {ru: "start_date", kz: "start_date_k"}, {ru: "department", kz: "department_k"}, {ru: "position", kz: "position_k"}, {ru: "department1", kz: "department1_k"}, {ru: "tab_num", kz: "tab_num_k"}, {ru: "user", kz: "user_k"}, {ru: "date", kz: "date_k"}, {ru: "from_user", kz: "from_user_k"}, {ru: "telephone", kz: "telephone_k"}, {ru: "choice1", kz: "choice1_k"}, {ru: "choice2", kz: "choice2_k"}, {ru: "choice3", kz: "choice3_k"}, {ru: "ruk_user", kz: "ruk_user_k"}, {ru: "date2", kz: "date2_k"}, {ru: "stazh", kz: "stazh_k"}, {ru: "uslovie", kz: "uslovie_k"}, {ru: "dana", kz: "dana_k"}, {ru: "start_date1", kz: "start_date1_k"}, {ru: "finish_date1", kz: "finish_date1_k"}, {ru: "svedenia", kz: "svedenia_k"}];
    var tableArrSP=[
      {id: {ru: "t", kz: "t_k"}, cmp: [{ru: "start_date1", kz: "start_date1_k"}, {ru: "finish_date1", kz: "finish_date1_k"}, {ru: "month", kz: "month_k"}, {ru: "summa", kz: "summa_k"}, {ru: "summa1", kz: "summa1_k"}, {ru: "vznos", kz: "vznos_k"}, {ru: "nalog", kz: "nalog_k"}, {ru: "uderzhanie", kz: "uderzhanie_k"}, {ru: "vyplata", kz: "vyplata_k"}, {ru: "user", kz: "user_k"}, {ru: "tab_num", kz: "tab_num_k"}, {ru: "position", kz: "position_k"}, {ru: "number", kz: "number_k"}, {ru: "date1", kz: "date1_k"}]},
      {id: {ru: "table1", kz: "table1_k"}, cmp: [{ru: "podpisant_user2", kz: "podpisant_user2_k"}, {ru: "position2", kz: "position2_k"}]}
    ];

    // Наименование организации
    var oraganization = ALL_DOCUMENTS_UTIL.getOrganizationData();
    if (oraganization) {
      if (model.getModelWithId('organization_name')) model.getModelWithId('organization_name').setValue(oraganization.nameRu);
      if (model.getModelWithId('organization_name_k')) model.getModelWithId('organization_name_k').setValue(oraganization.nameKz);
      if (model.getModelWithId('company_name')) model.getModelWithId('company_name').setValue(oraganization.nameRu);
      if (model.getModelWithId('company_name_k')) model.getModelWithId('company_name_k').setValue(oraganization.nameKz);
    }
    // замена логотипа
    if (model.getModelWithId('logo')) model.getModelWithId('logo').asfProperty.config.url = "../logo.png";
    if (view.getViewWithId('logo')) view.getViewWithId('logo').container[0].firstChild.src = "../logo.png";

    model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
      console.log(model.formName + '\nformCode: [' + model.formCode + ']\nformID: [' + model.formId +']\nasfDataID: [' + model.asfDataId+']');
      // телефон исполнителя
      if (model.getModelWithId("ispolnitel_user")) ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Штатная_расстановка111", "ispolnitel_user", "telephone", "ispolnitel_phone", null, null);
      for (var ti=0; ti < tableArrSP.length; ti++) {
        ALL_DOCUMENTS_UTIL.tableOn(view, tableArrSP[ti].id, tableArrSP[ti].cmp);
      }
      for (var ci=0; ci < cmpArrSP.length; ci++) {
        ALL_DOCUMENTS_UTIL.changedValue(model, cmpArrSP[ci]);
      }
      if (view.editable) {
        for (var ti2=0; ti2 < tableArrSP.length; ti2++) {
          ALL_DOCUMENTS_UTIL.tableEnabled(view, tableArrSP[ti2].id.kz);
        }
      }
      // с - по
      ALL_DOCUMENTS_UTIL.compareDates(view, "start_date", "finish_date", null, null, null);
    });

    // Справка о заработной плате
    if (model.formCode == "Справка_о_заработной_плате") {
      // 3.Надо сделать возможность удалить главного бухгалтера, чтобы подписывал только уполномоченное лицо по доверенности.
      var positionList = model.getModelWithId("podpisant_position");
      var tableBuh={ru: "table1", kz: "table1_k"};
      if (positionList.value[0] == "2") {
        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tableBuh, false);
      } else {
        ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tableBuh, true);
      }
      positionList.on("valueChange", function() {
        var tt1 = model.getModelWithId(tableBuh.ru);
        if (positionList.value[0] == "2") {
          ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tableBuh, false);
          tt1.modelBlocks.forEach(function(modelBlock, index){
            tt1.removeRow(index);
          });
        } else {
          ALL_DOCUMENTS_UTIL.setVisibleCmp(view, tableBuh, true);
          if (tt1.modelBlocks.length == 0) {
            tt1.createRow();
          } else {
            tt1.modelBlocks.forEach(function(modelBlock, index){
              tt1.removeRow(index);
            });
            if (tt1.modelBlocks.length == 0) tt1.createRow();
          }
        }
      });

      var inMonths = function(d1, d2) {
        return (d2.getFullYear() - d1.getFullYear()) * 12 + d2.getMonth() - d1.getMonth();
      };

      if (view.editable) {
        ALL_DOCUMENTS_UTIL.tableEnabled(view, "t"); // блокируем таблицу месяцев
      }

      model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
        var itogoArr = [
          {ishod: 'summa1', result: 'summa2'},
          {ishod: 'vznos', result: 'vznos1'},
          {ishod: 'nalog', result: 'nalog1'},
          {ishod: 'uderzhanie', result: 'uderzhanie1'},
          {ishod: 'vyplata', result: 'vyplata1'}
        ];

        var startDate = model.getModelWithId("start_date");
        var finishDate = model.getModelWithId("finish_date");
        var tableMonth = model.getModelWithId("t");

        var tmpDate1 = null;
        var tmpDate2 = null;

        startDate.on("valueChange", function() {
          tmpDate1=AS.FORMS.DateUtils.parseDate(startDate.getValue());
          tmpDate2=AS.FORMS.DateUtils.parseDate(finishDate.getValue());
          // выпиливаем все блоки таблицы
          tableMonth.modelBlocks.forEach(function(modelBlock, index){
            tableMonth.removeRow(index);
          });
          if (tmpDate1 && tmpDate2) {
            var kolM = inMonths(tmpDate1, tmpDate2); // кол-во месяцев за выбранный период
            tableMonth.modelBlocks.forEach(function(modelBlock, index){
              tableMonth.removeRow(index);
            });
            if (tableMonth.modelBlocks.length == 0) {
              for (var i = 0; i <= kolM; i++) {
                tableMonth.createRow(); // создаем новый блок
              }
              ALL_DOCUMENTS_UTIL.tableEnabled(view, "t");
            } else {
              tableMonth.modelBlocks.forEach(function(modelBlock, index){
                tableMonth.removeRow(index);
              });
              if (tableMonth.modelBlocks.length == 0) {
                for (var i = 0; i <= kolM; i++) {
                  tableMonth.createRow(); // создаем новый блок
                }
                ALL_DOCUMENTS_UTIL.tableEnabled(view, "t");
              }
            }
          }
        });
        finishDate.on("valueChange", function() {
          tmpDate1=AS.FORMS.DateUtils.parseDate(startDate.getValue());
          tmpDate2=AS.FORMS.DateUtils.parseDate(finishDate.getValue());
          // выпиливаем все блоки таблицы
          tableMonth.modelBlocks.forEach(function(modelBlock, index){
            tableMonth.removeRow(index);
          });
          if (tmpDate1 && tmpDate2) {
            var kolM = inMonths(tmpDate1, tmpDate2); // кол-во месяцев за выбранный период
            tableMonth.modelBlocks.forEach(function(modelBlock, index){
              tableMonth.removeRow(index);
            });
            if (tableMonth.modelBlocks.length == 0) {
              for (var i = 0; i <= kolM; i++) {
                tableMonth.createRow(); // создаем новый блок
              }
              ALL_DOCUMENTS_UTIL.tableEnabled(view, "t");
            } else {
              tableMonth.modelBlocks.forEach(function(modelBlock, index){
                tableMonth.removeRow(index);
              });
              if (tableMonth.modelBlocks.length == 0) {
                for (var i = 0; i <= kolM; i++) {
                  tableMonth.createRow(); // создаем новый блок
                }
                ALL_DOCUMENTS_UTIL.tableEnabled(view, "t");
              }
            }
          }
        });

        tableMonth.on('tableRowAdd', function(evt, modelTab, modelRow) {
          if (tmpDate1 && tmpDate2) {
            // заполняем месяц
            model.getModelWithId('month', 't', modelRow[0].asfProperty.tableBlockIndex).setValue(AS.FORMS.DateUtils.formatDate(tmpDate1, '${yyyy}-${mm}-${dd} 00:00:00'));
            tmpDate1.setMonth(tmpDate1.getMonth() + 1); // увеличиваем месяц на 1
          }

          // заполняем таблицу ИТОГО
          itogoArr.forEach(function(c) {
            model.getModelWithId(c.ishod, 't', modelRow[0].asfProperty.tableBlockIndex).on("valueChange", function() {
              var summValue = 0;
              tableMonth.modelBlocks.forEach(function(modelBlock, index){
                var tmpValue = model.getModelWithId(c.ishod, 't', modelBlock.tableBlockIndex).getValue();
                tmpValue = parseFloat(tmpValue);
                if (isNaN(tmpValue)) tmpValue = 0;
                summValue += tmpValue;
              });
              model.getModelWithId(c.result).setValue(summValue + '');
            });
          });
        });

        // чистим ИТОГО
        tableMonth.on('tableRowDelete', function(evt, rowModel, rowIndex, removedRow) {
          itogoArr.forEach(function(c) {
            model.getModelWithId(c.result).setValue(null);
          });
        });

      });

    }

    // Справка, подтверждающая стаж работы
    if (model.formCode == "Справка,_подтверждающая_стаж_работы0") {
      if (model.getModelWithId("choice1").getValue()[0] == '2') {
        view.getViewWithId("finish_date").container.parent().parent().hide();
        view.getViewWithId("finish_date_k").container.parent().parent().hide();
      } else {
        view.getViewWithId("finish_date").container.parent().parent().show();
        view.getViewWithId("finish_date_k").container.parent().parent().show();
      }

      model.getModelWithId("user").on("valueChange", function() {
        var clearCmp = ["choice1", "choice2", "stazh", "start_date", "finish_date"];
        clearCmp.forEach(function(cmp) {
          model.getModelWithId(cmp).setValue(null);
        });

        var datePriem = ALL_DOCUMENTS_UTIL.getDataFromUserCard(model, "Отпуска", "user", "day_start");
        if (datePriem) datePriem = AS.FORMS.DateUtils.parseDate(datePriem.key);

        var dateUvol = ALL_DOCUMENTS_UTIL.getDataFromUserCard(model, "Отпуска", "user", "day_finish");
        if (dateUvol) {
          dateUvol = AS.FORMS.DateUtils.parseDate(dateUvol.key);
          model.getModelWithId("choice1").setValue('1'); // работал
          model.getModelWithId("finish_date").setValue(AS.FORMS.DateUtils.formatDate(dateUvol, '${yyyy}-${mm}-${dd} 00:00:00'));
          view.getViewWithId("finish_date").container.parent().parent().show();
          view.getViewWithId("finish_date_k").container.parent().parent().show();
        } else {
          model.getModelWithId("choice1").setValue('2'); // работает
          dateUvol = new Date();
          model.getModelWithId("finish_date").setValue(null);
          view.getViewWithId("finish_date").container.parent().parent().hide();
          view.getViewWithId("finish_date_k").container.parent().parent().hide();
        }

        // подсчет стажа
        var staj = ALL_DOCUMENTS_UTIL.WORK_EXPERIENCE.recalc(datePriem, dateUvol);
        staj = ALL_DOCUMENTS_UTIL.WORK_EXPERIENCE.parse(staj);
        if (staj) {
          model.getModelWithId("stazh").setValue(staj[0] + ', ' + staj[1] + ', ' + staj[2]);
        } else {
          model.getModelWithId("stazh").setValue(null);
        }

        // дата с
        if (datePriem && dateUvol) {
          model.getModelWithId("start_date").setValue(AS.FORMS.DateUtils.formatDate(datePriem, '${yyyy}-${mm}-${dd} 00:00:00'));
        }
        // пол
        var pol = ALL_DOCUMENTS_UTIL.getDataFromUserCard(model, "Штатная_расстановка111", "user", "sex");
        if (pol) model.getModelWithId("choice2").setValue(pol.key);

      });

      // Примечание:
      model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
        setCompanyHistory(model, view);
      });

    }

    // Справка подтверждающая особый характер работы или условия труда
    if (model.formCode == "Справка_подтверждающая_особый_характер_работы_или_условия_труда") {

      if (model.getModelWithId("choice1").getValue()[0] == '1') {
        view.getViewWithId("finish_date").container.parent().parent().hide();
        view.getViewWithId("finish_date_k").container.parent().parent().hide();
      } else {
        view.getViewWithId("finish_date").container.parent().parent().show();
        view.getViewWithId("finish_date_k").container.parent().parent().show();
      }

      model.getModelWithId("user").on("valueChange", function() {
        var clearCmp = ["choice1", "choice3", "stazh", "start_date", "finish_date"];
        clearCmp.forEach(function(cmp) {
          model.getModelWithId(cmp).setValue(null);
        });

        var datePriem = ALL_DOCUMENTS_UTIL.getDataFromUserCard(model, "Отпуска", "user", "day_start");
        if (datePriem) datePriem = AS.FORMS.DateUtils.parseDate(datePriem.key);

        var dateUvol = ALL_DOCUMENTS_UTIL.getDataFromUserCard(model, "Отпуска", "user", "day_finish");
        if (dateUvol) {
          dateUvol = AS.FORMS.DateUtils.parseDate(dateUvol.key);
          model.getModelWithId("choice1").setValue('2'); // работал

          model.getModelWithId("choice").setValue(null);
          model.getModelWithId("choice_k").setValue(null);
          model.getModelWithId("finish_date").setValue(AS.FORMS.DateUtils.formatDate(dateUvol, '${yyyy}-${mm}-${dd} 00:00:00'));
          view.getViewWithId("choice").container.parent().parent().hide();
          view.getViewWithId("choice_k").container.parent().parent().hide();
          view.getViewWithId("finish_date").container.parent().parent().show();
          view.getViewWithId("finish_date_k").container.parent().parent().show();

        } else {
          model.getModelWithId("choice1").setValue('1'); // работает
          dateUvol = new Date();

          model.getModelWithId("choice").setValue("по настоящее время");
          model.getModelWithId("choice_k").setValue("по настоящее время");
          model.getModelWithId("finish_date").setValue(null);
          view.getViewWithId("choice").container.parent().parent().show();
          view.getViewWithId("choice_k").container.parent().parent().show();
          view.getViewWithId("finish_date").container.parent().parent().hide();
          view.getViewWithId("finish_date_k").container.parent().parent().hide();
        }

        // подсчет стажа
        var staj = ALL_DOCUMENTS_UTIL.WORK_EXPERIENCE.recalc(datePriem, dateUvol);
        staj = ALL_DOCUMENTS_UTIL.WORK_EXPERIENCE.parse(staj);
        if (staj) {
          model.getModelWithId("stazh").setValue(staj[0] + ', ' + staj[1] + ', ' + staj[2]);
        } else {
          model.getModelWithId("stazh").setValue(null);
        }

        // дата с
        if (datePriem) {
          model.getModelWithId("start_date").setValue(AS.FORMS.DateUtils.formatDate(datePriem, '${yyyy}-${mm}-${dd} 00:00:00'));
        }
        // пол
        var pol = ALL_DOCUMENTS_UTIL.getDataFromUserCard(model, "Штатная_расстановка111", "user", "sex");
        if (pol) model.getModelWithId("choice3").setValue(pol.key);

      });

      // должность \ профессия
      var positionUser = model.getModelWithId("position");
      positionUser.on("valueChange", function() {
        if (positionUser.value.length > 0) ALL_DOCUMENTS_UTIL.setValueChoiceFromPosition(view, positionUser.value[0].elementID, ["choice2"]);
      });

      // Примечание:
      model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
        setCompanyHistory(model, view);
      });
    }

    // Справка о сданных и несданных имущественно-материальных и других ценностях
    if (model.formCode == "Справка_о_сданных_и_несданных_имуществ") {
      // телефон исполнителя
      model.getModelWithId("from_user").on("valueChange", function() {
        ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Штатная_расстановка111", "from_user", "telephone", "telephone", null, null);
      });

    }

  }
});



function setCompanyHistory(model, view) {
  var startDate = model.getModelWithId("start_date");
  var finishDate = model.getModelWithId("finish_date");
  var primTableRu = model.getModelWithId("t");
  var primTableKz = model.getModelWithId("t_k");
  var newCompanyHistory = [];
  var tmpIndex = 0;

  startDate.on("valueChange", function() {
    newCompanyHistory = [];
    tmpIndex = 0;
    if ( AS.FORMS.DateUtils.parseDate(startDate.getValue()) ) {
      var tmpChek = true;
      var chekDate = AS.FORMS.DateUtils.parseDate(startDate.getValue());
      var chekDatePo = AS.FORMS.DateUtils.parseDate(finishDate.getValue());
      if (chekDate > new Date()) {
        chekDate = new Date();
        chekDate.setHours(0, 0, 0, 0);
        ALL_DOCUMENTS_UTIL.msgShow("Дата \"С\" не может быть больше текущей даты: " + AS.FORMS.DateUtils.formatDate(new Date(), '${d} ${monthed} ${yyyy} года.'));
        view.getViewWithId("start_date").container.parent().css("background-color", "#FF9999");
        view.getViewWithId("start_date_k").container.parent().css("background-color", "#FF9999");
        startDate.setValue(null);
      } else {
        view.getViewWithId("start_date").container.parent().css("background-color", "");
        view.getViewWithId("start_date_k").container.parent().css("background-color", "");
      }
      if (chekDatePo) {
        if (chekDatePo > new Date()) {
          chekDatePo = new Date();
          chekDatePo.setHours(0, 0, 0, 0);
        }
      }
      console.log('====>>>> chekDate: '+chekDate);
      console.log('====>>>> chekDatePo: '+chekDatePo);
      var tt=0;
      ALL_DOCUMENTS_UTIL.companyHistory.forEach(function(company) {
        if (chekDatePo) {
          if (((chekDate >= company.periodS && chekDate <= company.periodPo) || chekDate <= company.periodS )
           && ((chekDatePo <= company.periodPo && chekDatePo >= company.periodS) || chekDatePo >= company.periodPo )  ) {
            tmpChek = false;
            newCompanyHistory.push(company);
          }
          if (chekDate < company.periodS ) tt++;
        } else {
          if ((chekDate >= company.periodS && chekDate <= company.periodPo) || chekDate <= company.periodS ) {
            tmpChek = false;
            newCompanyHistory.push(company);
          }
          if (chekDate < company.periodS ) tt++;
        }
      });
      if (tt >= ALL_DOCUMENTS_UTIL.companyHistory.length) {
        tmpChek = true;
        newCompanyHistory = [];
      }
      if (tmpChek) {
        if (chekDate) ALL_DOCUMENTS_UTIL.msgShow("Нет исторических данных по компании за <u>" + AS.FORMS.DateUtils.formatDate(chekDate, '${d} ${monthed} ${yyyy} года.') + "</u><br>Заполните данные в реестре <u>'Изменения наименования компании'</u>");
        primTableRu.modelBlocks.forEach(function(modelBlock, index){
          primTableRu.removeRow(index);
        });
      }
    } else {
      primTableRu.modelBlocks.forEach(function(modelBlock, index){
        primTableRu.removeRow(index);
      });
    }
    console.log('=======>>>newCompanyHistory<<<=======');
    console.log(newCompanyHistory);
    console.log('=======>>>newCompanyHistory<<<=======');
    primTableRu.modelBlocks.forEach(function(modelBlock, index){
      primTableRu.removeRow(index);
    });
    if (primTableRu.modelBlocks.length > 0) {
      primTableRu.modelBlocks.forEach(function(modelBlock, index){
        primTableRu.removeRow(index);
      });
    }
    newCompanyHistory.forEach(function(company) {
      primTableRu.createRow();
    });

  });

  finishDate.on("valueChange", function() {
    newCompanyHistory = [];
    tmpIndex = 0;
    if ( AS.FORMS.DateUtils.parseDate(startDate.getValue()) ) {
      var tmpChek = true;
      var chekDate = AS.FORMS.DateUtils.parseDate(startDate.getValue());
      var chekDatePo = AS.FORMS.DateUtils.parseDate(finishDate.getValue());
      if (chekDate > new Date()) {
        chekDate = new Date();
        chekDate.setHours(0, 0, 0, 0);
        ALL_DOCUMENTS_UTIL.msgShow("Дата \"С\" не может быть больше текущей даты: " + AS.FORMS.DateUtils.formatDate(new Date(), '${d} ${monthed} ${yyyy} года.'));
        view.getViewWithId("start_date").container.parent().css("background-color", "#FF9999");
        view.getViewWithId("start_date_k").container.parent().css("background-color", "#FF9999");
        startDate.setValue(null);
      } else {
        view.getViewWithId("start_date").container.parent().css("background-color", "");
        view.getViewWithId("start_date_k").container.parent().css("background-color", "");
      }
      if (chekDatePo) {
        if (chekDatePo > new Date()) {
          chekDatePo = new Date();
          chekDatePo.setHours(0, 0, 0, 0);
        }
      }
      console.log('====>>>> chekDate: '+chekDate);
      console.log('====>>>> chekDatePo: '+chekDatePo);
      var tt=0;
      ALL_DOCUMENTS_UTIL.companyHistory.forEach(function(company) {
        if (chekDatePo) {
          if (((chekDate >= company.periodS && chekDate <= company.periodPo) || chekDate <= company.periodS )
           && ((chekDatePo <= company.periodPo && chekDatePo >= company.periodS) || chekDatePo >= company.periodPo )  ) {
            tmpChek = false;
            newCompanyHistory.push(company);
          }
          if (chekDate < company.periodS ) tt++;
        } else {
          if ((chekDate >= company.periodS && chekDate <= company.periodPo) || chekDate <= company.periodS ) {
            tmpChek = false;
            newCompanyHistory.push(company);
          }
          if (chekDate < company.periodS ) tt++;
        }
      });
      if (tt >= ALL_DOCUMENTS_UTIL.companyHistory.length) {
        tmpChek = true;
        newCompanyHistory = [];
      }
      if (tmpChek) {
        if (chekDate) ALL_DOCUMENTS_UTIL.msgShow("Нет исторических данных по компании за <u>" + AS.FORMS.DateUtils.formatDate(chekDate, '${d} ${monthed} ${yyyy} года.') + "</u><br>Заполните данные в реестре <u>'Изменения наименования компании'</u>");
        primTableRu.modelBlocks.forEach(function(modelBlock, index){
          primTableRu.removeRow(index);
        });
      }
    } else {
      primTableRu.modelBlocks.forEach(function(modelBlock, index){
        primTableRu.removeRow(index);
      });
    }
    console.log('=======>>>newCompanyHistory<<<=======');
    console.log(newCompanyHistory);
    console.log('=======>>>newCompanyHistory<<<=======');
    primTableRu.modelBlocks.forEach(function(modelBlock, index){
      primTableRu.removeRow(index);
    });
    if (primTableRu.modelBlocks.length > 0) {
      primTableRu.modelBlocks.forEach(function(modelBlock, index){
        primTableRu.removeRow(index);
      });
    }
    newCompanyHistory.forEach(function(company) {
      primTableRu.createRow();
    });
  });

  primTableRu.on('tableRowAdd', function(evt, modelTab, modelRow) {
    if (newCompanyHistory[tmpIndex]) {
      var tmpFinDate = newCompanyHistory[tmpIndex].periodPo;
      var tmpCurDate = new Date();
      tmpFinDate.setHours(0, 0, 0, 0);
      tmpCurDate.setHours(0, 0, 0, 0);
      if (tmpFinDate.getTime() == tmpCurDate.getTime()) {
        tmpFinDate = null;
      } else {
        tmpFinDate = AS.FORMS.DateUtils.formatDate(tmpFinDate, '${yyyy}-${mm}-${dd} 00:00:00')
      }
      model.getModelWithId("naimenovanie", "t", modelRow[0].asfProperty.tableBlockIndex).setValue(newCompanyHistory[tmpIndex].name.ru);
      model.getModelWithId("naimenovanie_k", "t_k", modelRow[0].asfProperty.tableBlockIndex).setValue(newCompanyHistory[tmpIndex].name.kz);
      model.getModelWithId("start_date1", "t", modelRow[0].asfProperty.tableBlockIndex).setValue(AS.FORMS.DateUtils.formatDate(newCompanyHistory[tmpIndex].periodS, '${yyyy}-${mm}-${dd} 00:00:00'));
      model.getModelWithId("finish_date1", "t", modelRow[0].asfProperty.tableBlockIndex).setValue(tmpFinDate);
      model.getModelWithId("obosnovanie", "t", modelRow[0].asfProperty.tableBlockIndex).setValue(newCompanyHistory[tmpIndex].obosnovanie.ru);
      model.getModelWithId("obosnovanie_k", "t_k", modelRow[0].asfProperty.tableBlockIndex).setValue(newCompanyHistory[tmpIndex].obosnovanie.kz);
      tmpIndex++;
    }
    var tablePeriodPo = model.getModelWithId("finish_date1", "t", modelRow[0].asfProperty.tableBlockIndex);
    if (!AS.FORMS.DateUtils.parseDate(tablePeriodPo.getValue())) {
      setTimeout(function(){
        if (view.getViewWithId("finish_date1", "t", modelRow[0].asfProperty.tableBlockIndex)) {
          view.getViewWithId("finish_date1", "t", modelRow[0].asfProperty.tableBlockIndex).container.parent().parent().hide();
        }
        if (view.getViewWithId("finish_date1_k", "t_k", modelRow[0].asfProperty.tableBlockIndex)) {
          view.getViewWithId("finish_date1_k", "t_k", modelRow[0].asfProperty.tableBlockIndex).container.parent().parent().hide();
        }
      }, 0);
    }
    tablePeriodPo.on("valueChange", function() {
      if (AS.FORMS.DateUtils.parseDate(tablePeriodPo.getValue())) {
        view.getViewWithId("finish_date1", "t", modelRow[0].asfProperty.tableBlockIndex).container.parent().parent().show();
        view.getViewWithId("finish_date1_k", "t_k", modelRow[0].asfProperty.tableBlockIndex).container.parent().parent().show();
      } else {
        view.getViewWithId("finish_date1", "t", modelRow[0].asfProperty.tableBlockIndex).container.parent().parent().hide();
        view.getViewWithId("finish_date1_k", "t_k", modelRow[0].asfProperty.tableBlockIndex).container.parent().parent().hide();
      }
    });
  });
}
