AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {

  // Приказ об изменении учетно характеризующих данных
  if (model.formCode == "Приказ_об_изменении_учетно_характеризующих_данных"
   || model.formCode == "Приказ_о_внесении_изменении_приказа__об_изменении_учетно_характеризующих_данных") {
    var cmpTable = {ru: "t", kz: "t_k"};

    setTimeout(function() {
      var userTableView = view.getViewWithId(cmpTable.ru);
      userTableView.getViewBlocks().forEach(function(viewBlock, index){
        var tableBlockIndex = viewBlock.views[0].model.asfProperty.tableBlockIndex;
        var userBox = model.getModelWithId("user", cmpTable.ru, tableBlockIndex);

        userBox.on("valueChange", function() {
          if (userBox.getValue().length > 0) {
            var userCardData = ALL_DOCUMENTS_UTIL.getCardsData("rest/api/personalrecord/forms/" + userBox.getValue()[0].personID, "Штатная_расстановка111");
            var userData = ALL_DOCUMENTS_UTIL.getUserData(userBox.getValue()[0].personID);

            var currentUserData = {
              udo: {
                number: ALL_DOCUMENTS_UTIL.getAsfDataObject(userCardData, "numberofudlichnosti"),
                date: ALL_DOCUMENTS_UTIL.getAsfDataObject(userCardData, "dateofud"),
                organ: ALL_DOCUMENTS_UTIL.getAsfDataObject(userCardData, "organofud")
              },
              pasport: {
                number: ALL_DOCUMENTS_UTIL.getAsfDataObject(userCardData, "numberofpassport"),
                date: ALL_DOCUMENTS_UTIL.getAsfDataObject(userCardData, "dateofpassport"),
                organ: ALL_DOCUMENTS_UTIL.getAsfDataObject(userCardData, "organofpassport")
              },
              user: {
                lastname: userData.lastname,
                firstname: userData.firstname,
                patronymic: userData.patronymic
              }
            };

            console.log("-------------->>> currentUserData <<<--------------");
            console.log(currentUserData);
            console.log("-------------->>> currentUserData <<<--------------");
            // удостоверение
            model.getModelWithId("numberofudlichnosti", cmpTable.ru, tableBlockIndex).setValue(currentUserData.udo.number.value);
            model.getModelWithId("dateofud", cmpTable.ru, tableBlockIndex).setValue(currentUserData.udo.date.key);
            model.getModelWithId("organofud", cmpTable.ru, tableBlockIndex).setValue(currentUserData.udo.organ.key);
            // паспорт
            model.getModelWithId("numberofpassport", cmpTable.ru, tableBlockIndex).setValue(currentUserData.pasport.number.value);
            model.getModelWithId("dateofpassport", cmpTable.ru, tableBlockIndex).setValue(currentUserData.pasport.date.key);
            model.getModelWithId("organofpassport", cmpTable.ru, tableBlockIndex).setValue(currentUserData.pasport.organ.key);
            // фио
            model.getModelWithId("user_lastname", cmpTable.ru, tableBlockIndex).setValue(currentUserData.user.lastname);
            model.getModelWithId("user_name", cmpTable.ru, tableBlockIndex).setValue(currentUserData.user.firstname);
            model.getModelWithId("user_middle", cmpTable.ru, tableBlockIndex).setValue(currentUserData.user.patronymic);

          }
        });
      });
    }, 0);

    var userTableModel = model.getModelWithId(cmpTable.ru);
    if (view.editable) {
      userTableModel.on('tableRowAdd', function(evt, modelTab, modelRow) {
        var tableBlockIndex = modelRow[0].asfProperty.tableBlockIndex;
        var userBox = model.getModelWithId("user", cmpTable.ru, tableBlockIndex);

        userBox.on("valueChange", function() {
          if (userBox.getValue().length > 0) {
            var userCardData = ALL_DOCUMENTS_UTIL.getCardsData("rest/api/personalrecord/forms/" + userBox.getValue()[0].personID, "Штатная_расстановка111");
            var userData = ALL_DOCUMENTS_UTIL.getUserData(userBox.getValue()[0].personID);

            var currentUserData = {
              udo: {
                number: ALL_DOCUMENTS_UTIL.getAsfDataObject(userCardData, "numberofudlichnosti"),
                date: ALL_DOCUMENTS_UTIL.getAsfDataObject(userCardData, "dateofud"),
                organ: ALL_DOCUMENTS_UTIL.getAsfDataObject(userCardData, "organofud")
              },
              pasport: {
                number: ALL_DOCUMENTS_UTIL.getAsfDataObject(userCardData, "numberofpassport"),
                date: ALL_DOCUMENTS_UTIL.getAsfDataObject(userCardData, "dateofpassport"),
                organ: ALL_DOCUMENTS_UTIL.getAsfDataObject(userCardData, "organofpassport")
              },
              user: {
                lastname: userData.lastname,
                firstname: userData.firstname,
                patronymic: userData.patronymic
              }
            };

            console.log("-------------->>> currentUserData <<<--------------");
            console.log(currentUserData);
            console.log("-------------->>> currentUserData <<<--------------");
            // удостоверение
            model.getModelWithId("numberofudlichnosti", cmpTable.ru, tableBlockIndex).setValue(currentUserData.udo.number.value);
            model.getModelWithId("dateofud", cmpTable.ru, tableBlockIndex).setValue(currentUserData.udo.date.key);
            model.getModelWithId("organofud", cmpTable.ru, tableBlockIndex).setValue(currentUserData.udo.organ.key);
            // паспорт
            model.getModelWithId("numberofpassport", cmpTable.ru, tableBlockIndex).setValue(currentUserData.pasport.number.value);
            model.getModelWithId("dateofpassport", cmpTable.ru, tableBlockIndex).setValue(currentUserData.pasport.date.key);
            model.getModelWithId("organofpassport", cmpTable.ru, tableBlockIndex).setValue(currentUserData.pasport.organ.key);
            // фио
            model.getModelWithId("user_lastname", cmpTable.ru, tableBlockIndex).setValue(currentUserData.user.lastname);
            model.getModelWithId("user_name", cmpTable.ru, tableBlockIndex).setValue(currentUserData.user.firstname);
            model.getModelWithId("user_middle", cmpTable.ru, tableBlockIndex).setValue(currentUserData.user.patronymic);

          }
        });
      });
    }

    model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
      // ознакомление
      ALL_DOCUMENTS_UTIL.copyCmpValue(view, ["user"], ["oznakomlenie_user"], "t", "tt");
      // основание
      ALL_DOCUMENTS_UTIL.copyCmpValue(view, [], [], "t", "ttt");
    });
  }

});
