AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  if (model.formCode == 'Справочник_праздников') {
    model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
      var holidayArr=[
        {name: 'Новый год', day: '1', month: {key: '1', value: 'Январь'}, color: {key: '#FF0000', value: 'Красный'}},
        {name: 'Новый год', day: '2', month: {key: '1', value: 'Январь'}, color: {key: '#FF0000', value: 'Красный'}},
        {name: 'Рождество Христово', day: '7', month: {key: '1', value: 'Январь'}, color: {key: '#FF0000', value: 'Красный'}},
        {name: 'Международный женский день', day: '8', month: {key: '3', value: 'Март'}, color: {key: '#FF0000', value: 'Красный'}},
        {name: 'Наурыз мейрамы', day: '21', month: {key: '3', value: 'Март'}, color: {key: '#FF0000', value: 'Красный'}},
        {name: 'Наурыз мейрамы', day: '22', month: {key: '3', value: 'Март'}, color: {key: '#FF0000', value: 'Красный'}},
        {name: 'Наурыз мейрамы', day: '23', month: {key: '3', value: 'Март'}, color: {key: '#FF0000', value: 'Красный'}},
        {name: 'Праздник единства народа Казахстана', day: '1', month: {key: '5', value: 'Май'}, color: {key: '#FF0000', value: 'Красный'}},
        {name: 'День Защитника Отечества', day: '7', month: {key: '5', value: 'Май'}, color: {key: '#FF0000', value: 'Красный'}},
        {name: 'День Победы', day: '9', month: {key: '5', value: 'Май'}, color: {key: '#FF0000', value: 'Красный'}},
        {name: 'День столицы', day: '6', month: {key: '7', value: 'Июль'}, color: {key: '#FF0000', value: 'Красный'}},
        {name: 'День Конституции', day: '30', month: {key: '8', value: 'Август'}, color: {key: '#FF0000', value: 'Красный'}},
        {name: 'День Первого Президента', day: '1', month: {key: '12', value: 'Декабрь'}, color: {key: '#FF0000', value: 'Красный'}},
        {name: 'День независимости Республики Казахстан', day: '16', month: {key: '12', value: 'Декабрь'}, color: {key: '#FF0000', value: 'Красный'}},
        {name: 'День независимости Республики Казахстан', day: '17', month: {key: '12', value: 'Декабрь'}, color: {key: '#FF0000', value: 'Красный'}}
      ];
      var tableModel = model.getModelWithId('holidays');
      if (view.getViewWithId('holidays').getRowsCount() == 1) {
        console.log('Новая запись, заполняем праздниками.');
        for (var i = 0; i < holidayArr.length; i++) {
          tableModel.createRow();
          model.getModelWithId('nameHoliday', 'holidays', i + 1).setValue(holidayArr[i].name);
          model.getModelWithId('dayHoliday', 'holidays', i + 1).setValue(holidayArr[i].day);
          model.getModelWithId('monthHoliday', 'holidays', i + 1).setValue(holidayArr[i].month.key);
          model.getModelWithId('color', 'holidays', i + 1).setValue(holidayArr[i].color.key);
        }
      } else {
        console.log('Таблица уже содержит данные');
      }
    });
  }
});
