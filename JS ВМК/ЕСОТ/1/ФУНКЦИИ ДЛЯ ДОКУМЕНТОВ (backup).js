var ALL_DOCUMENTS_UTIL = {

  synergyLogin: AS.OPTIONS.login,
  synergyPass: AS.OPTIONS.password,
  companyHistory: [],

  setVisibleCmp: function(playerView, cmp, visible) {
    if (typeof cmp == "object") {
      if (playerView.getViewWithId(cmp.ru)) playerView.getViewWithId(cmp.ru).setVisible(visible);
      if (playerView.getViewWithId(cmp.kz)) playerView.getViewWithId(cmp.kz).setVisible(visible);
    } else {
      if (playerView.getViewWithId(cmp)) playerView.getViewWithId(cmp).setVisible(visible);
    }
  },

  changedValue: function(model, cmpObj) {
    if (model.getModelWithId(cmpObj.ru) && model.getModelWithId(cmpObj.kz)) {
      var userCmp = {ru: "user", kz: "user_k"};
      if (model.formCode.substring(0,9).toLowerCase() == "заявление") userCmp = {ru: "from_user", kz: "from_user_k"};

      model.getModelWithId(cmpObj.ru).on("valueChange", function() {
        if (model.getModelWithId(cmpObj.kz) ) {
          model.getModelWithId(cmpObj.kz).setValue(model.getModelWithId(cmpObj.ru).getValue());
        }
        if (cmpObj.ru == userCmp.ru) {
          ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Штатная_расстановка111", userCmp.ru, "t_number", "tab_num", null, null);
          ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Штатная_расстановка111", userCmp.ru, "fact_address", "address", null, null);
          ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Отпуска", userCmp.ru, "sum_available_vacat_days", "days_available", null, null);
          ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Отпуска", userCmp.ru, "sum_vacat_days", "remainder", null, null);
        }
        if (cmpObj.ru == "ispolnitel_user") {
          ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Штатная_расстановка111", "ispolnitel_user", "telephone", "ispolnitel_phone", null, null);
        }
      });
      model.getModelWithId(cmpObj.kz).on("valueChange", function() {
        if (model.getModelWithId(cmpObj.ru) ) {
          model.getModelWithId(cmpObj.ru).setValue(model.getModelWithId(cmpObj.kz).getValue());
        }
        if (cmpObj.kz == userCmp.kz) {
          ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Штатная_расстановка111", userCmp.kz, "t_number", "tab_num_k", null, null);
          ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Штатная_расстановка111", userCmp.kz, "fact_address", "address_k", null, null);
          ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Отпуска", userCmp.kz, "sum_available_vacat_days", "days_available_k", null, null);
          ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Отпуска", userCmp.kz, "sum_vacat_days", "remainder_k", null, null);
        }
        if (cmpObj.ru == "ispolnitel_user") {
          ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Штатная_расстановка111", "ispolnitel_user_k", "telephone", "ispolnitel_phone_k", null, null);
        }
      });
    }
  },

  changedValueTable: function(model, table, cmp, index) {
    if (model.getModelWithId(cmp.ru, table.ru, index) && model.getModelWithId(cmp.kz, table.kz, index)) {
      model.getModelWithId(cmp.ru, table.ru, index).on("valueChange", function() {
        if (cmp.ru == "user") {
          ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Штатная_расстановка111", cmp.ru, "t_number", "tab_num", table.ru, index);
          ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Штатная_расстановка111", cmp.ru, "fact_address", "address", table.ru, index);
          // ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Отпуска", cmp.ru, "sum_available_vacat_days", "days_available", table.ru, index);
          // ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Отпуска", cmp.ru, "sum_vacat_days", "remainder", table.ru, index);
        }
        if (model.getModelWithId(cmp.kz, table.kz, index) ) {
          model.getModelWithId(cmp.kz, table.kz, index).setValue(model.getModelWithId(cmp.ru, table.ru, index).getValue());
        }
      });
      model.getModelWithId(cmp.kz, table.kz, index).on("valueChange", function() {
        if (cmp.kz == "user_k") {
          ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Штатная_расстановка111", cmp.kz, "t_number", "tab_num_k", table.kz, index);
          ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Штатная_расстановка111", cmp.kz, "fact_address", "address_k", table.kz, index);
          // ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Отпуска", cmp.kz, "sum_available_vacat_days", "days_available_k", table.kz, index);
          // ALL_DOCUMENTS_UTIL.getCardUserValue(model, "Отпуска", cmp.kz, "sum_vacat_days", "remainder_k", table.kz, index);
        }
        if (model.getModelWithId(cmp.ru, table.ru, index) ) {
          model.getModelWithId(cmp.ru, table.ru, index).setValue(model.getModelWithId(cmp.kz, table.kz, index).getValue());
        }
      });
    }
  },

  tableOn: function(playerView, tabObj, cmpArr) {
    var tmpTableRu=playerView.model.getModelWithId(tabObj.ru);
    var tmpTableKz=playerView.model.getModelWithId(tabObj.kz);
    if (tmpTableRu && tmpTableKz && tmpTableRu.asfProperty.type == "table" && tmpTableKz.asfProperty.type == "table") {
      tmpTableRu.modelBlocks.forEach(function(t) {
        cmpArr.forEach(function(cmp) {
          ALL_DOCUMENTS_UTIL.changedValueTable(playerView.model, tabObj, cmp, t.tableBlockIndex);
        });
      });
      tmpTableRu.on('tableRowAdd', function(evt, modelTab, modelRow) {
        tmpTableKz.createRow();
        cmpArr.forEach(function(cmp) {
          ALL_DOCUMENTS_UTIL.changedValueTable(playerView.model, tabObj, cmp, modelRow[0].asfProperty.tableBlockIndex);
        });
        ALL_DOCUMENTS_UTIL.tableEnabled(playerView, tabObj.kz);
      });
    }
    if (tmpTableRu && tmpTableKz) {
      tmpTableRu.on('tableRowDelete', function(evt, rowModel, rowIndex, removedRow) {
        tmpTableKz.removeRow(rowIndex);
      });
    }
    ALL_DOCUMENTS_UTIL.tableEnabled(playerView, tabObj.kz);
  },

  tableEnabled: function(playerView, table) {
    if (playerView.editable) {
      var tmpTable=playerView.getViewWithId(table);
      if (tmpTable) tmpTable.setEnabled(false);
    } else {
      setTimeout(ALL_DOCUMENTS_UTIL.tableEnabled, 500, playerView, table);
    }
  },

  getCardUserValue: function(model, form, cmpUser, cmpParent, cmpChild, table, index) {
    // функция которая вытаскивает с карточки пользователя (form) значение определенного компонента (cmpParent) и вставляет их на форму (cmpChild)
    if (table == null) {
      if (model.getModelWithId(cmpChild)) model.getModelWithId(cmpChild).setValue(null);
    } else {
      if (model.getModelWithId(cmpChild, table, index)) model.getModelWithId(cmpChild, table, index).setValue(null);
    }
    var personID=null;
    if (table == null) {
      if (model.getModelWithId(cmpUser).value.length > 0 ) personID = model.getModelWithId(cmpUser).value[0].personID;
    } else {
      if (model.getModelWithId(cmpUser, table, index).value.length > 0 ) personID = model.getModelWithId(cmpUser, table, index).value[0].personID;
    }
    if (personID) {
      $.ajax({
        url: 'rest/api/personalrecord/forms/' + personID,
        dataType: 'json',
        username: ALL_DOCUMENTS_UTIL.synergyLogin,
        password: ALL_DOCUMENTS_UTIL.synergyPass,
        success: function(data) {
          var tmpJson;
          var cardID=null;
          tmpJson = JSON.stringify(data);
          tmpJson = String(tmpJson).replace(/form-uuid/g, "formUuid");
          tmpJson = tmpJson.replace(/data-uuid/g, "dataUuid");
          tmpJson = JSON.parse(tmpJson);
          for (var i = 0; i < tmpJson.length; i++) {
            if (tmpJson[i].formCode == form) {
              cardID = tmpJson[i].dataUuid;
              break;
            }
          }
          if (cardID) {
            $.ajax({
              url: 'rest/api/asforms/data/' + cardID,
              dataType: 'json',
              username: ALL_DOCUMENTS_UTIL.synergyLogin,
              password: ALL_DOCUMENTS_UTIL.synergyPass,
              success: function(cardData) {
                for (var j = 0; j < cardData.data.length; j++) {
                  if (cardData.data[j].id == cmpParent) {
                    var tmpBlok=null;
                    if (table == null) {
                      tmpBlok = model.getModelWithId(cmpChild);
                    } else {
                      tmpBlok = model.getModelWithId(cmpChild, table, index);
                    }
                    if (tmpBlok) {
                      if (tmpBlok.asfProperty.type == 'listbox') {
                        tmpBlok.setValue(cardData.data[j].key);
                      } else {
                        tmpBlok.setValue(cardData.data[j].value);
                      }
                    }
                    break;
                  }
                }
              }
            });
          }
        }
      });
    }
  },

  getDataFromUserCard : function(model, formCode, cmpUser, cmpParent) {
    var personID = null;
    var result = null;
    if (model.getModelWithId(cmpUser).value.length > 0 ) personID = model.getModelWithId(cmpUser).value[0].personID;
    if (personID) {
      $.ajax({
        url: 'rest/api/personalrecord/forms/' + personID,
        dataType: 'json',
        async: false,
        username: ALL_DOCUMENTS_UTIL.synergyLogin,
        password: ALL_DOCUMENTS_UTIL.synergyPass,
        success: function(data) {
          var tmpJson;
          var cardID = null;
          tmpJson = JSON.stringify(data);
          tmpJson = String(tmpJson).replace(/form-uuid/g, "formUuid");
          tmpJson = tmpJson.replace(/data-uuid/g, "dataUuid");
          tmpJson = JSON.parse(tmpJson);
          for (var i = 0; i < tmpJson.length; i++) {
            if (tmpJson[i].formCode == formCode) {
              cardID = tmpJson[i].dataUuid;
              break;
            }
          }
          if (cardID) {
            $.ajax({
              url: 'rest/api/asforms/data/' + cardID,
              dataType: 'json',
              async: false,
              username: ALL_DOCUMENTS_UTIL.synergyLogin,
              password: ALL_DOCUMENTS_UTIL.synergyPass,
              success: function(cardData) {
                for (var j = 0; j < cardData.data.length; j++) {
                  if (cardData.data[j].id == cmpParent) {
                    if (cardData.data[j].key && cardData.data[j].value) {
                      result = {key: cardData.data[j].key, value: cardData.data[j].value, asfDataID: cardData.uuid};
                    } else if (cardData.data[j].value) {
                      result = {value: cardData.data[j].value, asfDataID: cardData.uuid};
                    } else {
                      result = null;
                    }

                    break;
                  }
                }
              }
            });
          }
        }
      });
    }
    return result;
  },

  setValueChoiceFromPosition: function(playerView, posID, choice, table, index) {
    if (posID != undefined && posID != null) {
      $.ajax({
        url: 'rest/api/positions/get_cards?positionID=' + posID,
        dataType: 'json',
        username: ALL_DOCUMENTS_UTIL.synergyLogin,
        password: ALL_DOCUMENTS_UTIL.synergyPass,
        success: function(data) {
          var tmpJson;
          var cardID=null;
          tmpJson = JSON.stringify(data);
          tmpJson = String(tmpJson).replace(/form-uuid/g, "formUuid");
          tmpJson = tmpJson.replace(/data-uuid/g, "dataUuid");
          tmpJson = JSON.parse(tmpJson);
          for (var i = 0; i < tmpJson.length; i++) {
            if (tmpJson[i].formCode == "Карточка_должности") {
              cardID = tmpJson[i].dataUuid;
              break;
            }
          }
          if (cardID) {
            $.ajax({
              url: 'rest/api/asforms/data/' + cardID,
              dataType: 'json',
              username: ALL_DOCUMENTS_UTIL.synergyLogin,
              password: ALL_DOCUMENTS_UTIL.synergyPass,
              success: function(cardData) {
                for (var j = 0; j < cardData.data.length; j++) {
                  if (cardData.data[j].id == "type") {
                    choice.forEach(function(cmp) {
                      if (table == undefined) {
                        var tmpChoice = playerView.model.getModelWithId(cmp);
                        if (tmpChoice) tmpChoice.setValue(cardData.data[j].value);
                      } else {
                        var tmpChoice = playerView.model.getModelWithId(cmp, table, index);
                        if (tmpChoice) tmpChoice.setValue(cardData.data[j].value);
                      }
                    });
                    break;
                  }
                }
              }
            });
          }
        }
      });
    }
  },

  getCardPositionData: function(posID, formCode) {
    var result = null;
    if (posID) {
      $.ajax({
        url: 'rest/api/positions/get_cards?positionID=' + posID,
        dataType: 'json',
        async: false,
        username: ALL_DOCUMENTS_UTIL.synergyLogin,
        password: ALL_DOCUMENTS_UTIL.synergyPass,
        success: function(data) {
          var cardID = null;
          for (var i = 0; i < data.length; i++) {
            if (data[i].formCode == formCode) {
              cardID = data[i]['data-uuid'];
              break;
            }
          }
          if (cardID) {
            $.ajax({
              url: 'rest/api/asforms/data/' + cardID,
              dataType: 'json',
              async: false,
              username: ALL_DOCUMENTS_UTIL.synergyLogin,
              password: ALL_DOCUMENTS_UTIL.synergyPass,
              success: function(cardData) {
                result = cardData.data;
              }
            });
          }
        }
      });
    }
    return result;
  },

  compareDates: function(playerView, cmpStart, cmpStop, resCmp, table, blockNumber) {
    var startDate = null;
    var stopDate = null;
    var resultBox = null;
    if (table == null && blockNumber == null) {
      startDate = playerView.model.getModelWithId(cmpStart);
      stopDate = playerView.model.getModelWithId(cmpStop);
      if (resCmp != null) resultBox = playerView.model.getModelWithId(resCmp);
    } else {
      startDate = playerView.model.getModelWithId(cmpStart, table.ru, blockNumber);
      stopDate = playerView.model.getModelWithId(cmpStop, table.ru, blockNumber);
      if (resCmp != null) resultBox = playerView.model.getModelWithId(resCmp, table.ru, blockNumber);
    }
    if (startDate && stopDate) {
      startDate.on("valueChange", function() {
        if (AS.FORMS.DateUtils.parseDate(startDate.getValue()) !=null && AS.FORMS.DateUtils.parseDate(stopDate.getValue()) !=null ) {
          var d1 = AS.FORMS.DateUtils.parseDate(startDate.getValue()).getTime();
          var d2 = AS.FORMS.DateUtils.parseDate(stopDate.getValue()).getTime();

          var dd = Math.floor((d2-d1) / 86400000);
          if (dd < 0) {
            if (table == null && blockNumber == null) {
              playerView.getViewWithId(cmpStop).container.parent().css("background-color", "#FF9999");
              if (playerView.getViewWithId(cmpStop+"_k")) playerView.getViewWithId(cmpStop+"_k").container.parent().css("background-color", "#FF9999");
            } else {
              setTimeout(function(){
                if (playerView.getViewWithId(cmpStop, table.ru, blockNumber)) playerView.getViewWithId(cmpStop, table.ru, blockNumber).container.parent().css("background-color", "#FF9999");
                if (playerView.getViewWithId(cmpStop+"_k", table.kz, blockNumber)) playerView.getViewWithId(cmpStop+"_k", table.kz, blockNumber).container.parent().css("background-color", "#FF9999");
              }, 0);
            }
            ALL_DOCUMENTS_UTIL.msgShow("Дата ПО [" + stopDate.getValue() + "] меньше даты С [" + startDate.getValue() + "]");
            if (resultBox) resultBox.setValue(null);
            stopDate.setValue(null);
            stopDate.asfProperty.required=true;
          } else {
            if (resultBox) {
              if (playerView.model.formCode == "Приказ_о_предоставлении_оплачиваемого_отпуска_(с_материальной_помощью)"
              || playerView.model.formCode == "Приказ_о_предоставлении_оплачиваемого_отпуска_(без_материальной_помощью)"
              || playerView.model.formCode == "Приказ_о_предоставлении_оплачиваемого_отпуска_(без_материальной_помощью)1"
              || playerView.model.formCode == "Приказ_о_предоставлении_оплачиваемого_отпуска_(с_материальной_помощью)1") {
                // 335566
                var nn = ALL_DOCUMENTS_UTIL.getHolidaysPeriod(startDate.getValue(), stopDate.getValue() );
                console.log("всего праздничных дней = "+nn);
                resultBox.setValue((dd+1-nn) + "");
              } else {
                resultBox.setValue((dd+1) + "");
              }
            }
            if (table == null && blockNumber == null) {
              playerView.getViewWithId(cmpStop).container.parent().css("background-color", "");
              if (playerView.getViewWithId(cmpStop+"_k")) playerView.getViewWithId(cmpStop+"_k").container.parent().css("background-color", "");
              if ((playerView.model.formCode == "Заявление_об_отпуске_по_беременности_и_родам" || playerView.model.formCode == "Заявление_о_выходе_в_отпуск_по_беременности_и_родам") && dd > 126) {
                playerView.getViewWithId(cmpStop).container.parent().css("background-color", "#FF9999");
                if (playerView.getViewWithId(cmpStop+"_k")) playerView.getViewWithId(cmpStop+"_k").container.parent().css("background-color", "#FF9999");
                ALL_DOCUMENTS_UTIL.msgShow("Отпуск по беременности и родам превышает 126 календарных дней");
                if (resultBox) resultBox.setValue(null);
                stopDate.setValue(null);
                stopDate.asfProperty.required=true;
              }
            } else {
              setTimeout(function(){
                if (playerView.getViewWithId(cmpStop, table.ru, blockNumber)) playerView.getViewWithId(cmpStop, table.ru, blockNumber).container.parent().css("background-color", "");
                if (playerView.getViewWithId(cmpStop+"_k", table.kz, blockNumber)) playerView.getViewWithId(cmpStop+"_k", table.kz, blockNumber).container.parent().css("background-color", "");
              }, 0);
            }
          }
        } else {
          if (resultBox) resultBox.setValue(null);
        }
      });
      stopDate.on("valueChange", function() {
        if (AS.FORMS.DateUtils.parseDate(startDate.getValue()) !=null && AS.FORMS.DateUtils.parseDate(stopDate.getValue()) !=null ) {
          var d1 = AS.FORMS.DateUtils.parseDate(startDate.getValue()).getTime();
          var d2 = AS.FORMS.DateUtils.parseDate(stopDate.getValue()).getTime();
          var dd = Math.floor((d2-d1) / 86400000);
          if (dd < 0) {
            if (table == null && blockNumber == null) {
              playerView.getViewWithId(cmpStop).container.parent().css("background-color", "#FF9999");
              if (playerView.getViewWithId(cmpStop+"_k")) playerView.getViewWithId(cmpStop+"_k").container.parent().css("background-color", "#FF9999");
            } else {
              setTimeout(function(){
                if (playerView.getViewWithId(cmpStop, table.ru, blockNumber)) playerView.getViewWithId(cmpStop, table.ru, blockNumber).container.parent().css("background-color", "#FF9999");
                if (playerView.getViewWithId(cmpStop+"_k", table.kz, blockNumber)) playerView.getViewWithId(cmpStop+"_k", table.kz, blockNumber).container.parent().css("background-color", "#FF9999");
              }, 0);
            }
            ALL_DOCUMENTS_UTIL.msgShow("Дата ПО [" + stopDate.getValue() + "] меньше даты С [" + startDate.getValue() + "]");
            if (resultBox) resultBox.setValue(null);
            stopDate.setValue(null);
            stopDate.asfProperty.required=true;
          } else {
            if (resultBox) {
              if (playerView.model.formCode == "Приказ_о_предоставлении_оплачиваемого_отпуска_(с_материальной_помощью)"
              || playerView.model.formCode == "Приказ_о_предоставлении_оплачиваемого_отпуска_(без_материальной_помощью)"
              || playerView.model.formCode == "Приказ_о_предоставлении_оплачиваемого_отпуска_(без_материальной_помощью)1"
              || playerView.model.formCode == "Приказ_о_предоставлении_оплачиваемого_отпуска_(с_материальной_помощью)1") {
                // 335566
                var nn = ALL_DOCUMENTS_UTIL.getHolidaysPeriod(startDate.getValue(), stopDate.getValue() );
                console.log("всего праздничных дней = "+nn);
                resultBox.setValue((dd+1-nn) + "");
              } else {
                resultBox.setValue((dd+1) + "");
              }
            }
            if (table == null && blockNumber == null) {
              playerView.getViewWithId(cmpStop).container.parent().css("background-color", "");
              if (playerView.getViewWithId(cmpStop+"_k")) playerView.getViewWithId(cmpStop+"_k").container.parent().css("background-color", "");
              if ((playerView.model.formCode == "Заявление_об_отпуске_по_беременности_и_родам" || playerView.model.formCode == "Заявление_о_выходе_в_отпуск_по_беременности_и_родам") && dd > 126) {
                playerView.getViewWithId(cmpStop).container.parent().css("background-color", "#FF9999");
                if (playerView.getViewWithId(cmpStop+"_k")) playerView.getViewWithId(cmpStop+"_k").container.parent().css("background-color", "#FF9999");
                ALL_DOCUMENTS_UTIL.msgShow("Отпуск по беременности и родам превышает 126 календарных дней");
                if (resultBox) resultBox.setValue(null);
                stopDate.setValue(null);
                stopDate.asfProperty.required=true;
              }
            } else {
              setTimeout(function(){
                if (playerView.getViewWithId(cmpStop, table.ru, blockNumber)) playerView.getViewWithId(cmpStop, table.ru, blockNumber).container.parent().css("background-color", "");
                if (playerView.getViewWithId(cmpStop+"_k", table.kz, blockNumber)) playerView.getViewWithId(cmpStop+"_k", table.kz, blockNumber).container.parent().css("background-color", "");
              }, 0);
            }
          }
        } else {
          if (resultBox) resultBox.setValue(null);
        }
      });
    }
  },

  getDatePlusMonth: function(d, n) {
    var yy = parseInt(d.substring(0, 4));
    var mm = parseInt(d.substring(5, 7));
    var dd = parseInt(d.substring(8, 10));
    var tmpN = n / 12 >> 0;
    if (tmpN > 0) yy = yy + tmpN;
    if ((mm + n) > 12) {
      mm = mm + (n % 12);
    } else {
      mm = mm + n;
    }
    if (mm > 12) {
      yy = yy + 1;
      mm = mm - 12;
    }
    if (mm < 10) mm = '0' + mm;
    if (dd < 10) dd = '0' + dd;
    return (yy + "-" + mm + "-" + dd);
  },

  controlUser: function(playerView, check, cmpUser, cmpPos) {
    var provBox = playerView.model.getModelWithId(check);
    var cmpUserRu = playerView.model.getModelWithId(cmpUser.ru);
    var cmpUserKz = playerView.model.getModelWithId(cmpUser.kz);
    var allCmp = [cmpUser.ru, cmpUser.kz, cmpPos.ru, cmpPos.kz];
    if (provBox && provBox.value[0] == '0') {
      allCmp.forEach(function(cmp) {
        if (playerView.getViewWithId(cmp)) playerView.getViewWithId(cmp).container.parent().show();
      });
    } else if (provBox) {
      allCmp.forEach(function(cmp) {
        if (playerView.getViewWithId(cmp)) playerView.getViewWithId(cmp).container.parent().hide();
      });
      if (cmpUserRu) cmpUserRu.setValue(null);
    }
    if (provBox) {
      provBox.on("valueChange", function() {
        if (provBox.value[0] == '0') {
          cmpUserRu.asfProperty.required=true;
          cmpUserKz.asfProperty.required=true;
          allCmp.forEach(function(cmp) {
            if (playerView.getViewWithId(cmp)) playerView.getViewWithId(cmp).container.parent().show();
          });
        } else {
          cmpUserRu.asfProperty.required=false;
          cmpUserKz.asfProperty.required=false;
          allCmp.forEach(function(cmp) {
            if (playerView.getViewWithId(cmp)) playerView.getViewWithId(cmp).container.parent().hide();
          });
          if (cmpUserRu) cmpUserRu.setValue(null);
        }
      });
    }
  },

  copyCmpValue: function(playerView, cmpArr1, cmpArr2, t1, t2) {
    if (t1 == undefined && t2 == undefined) {
      cmpArr1.forEach(function(cmp, i) {
        var tmpCmp1 = null;
        tmpCmp1 = playerView.model.getModelWithId(cmp);
        if (tmpCmp1) {
          tmpCmp1.on("valueChange", function() {
            var tmpCmp2 = null;
            tmpCmp2 = playerView.model.getModelWithId(cmpArr2[i]);
            if (tmpCmp2) tmpCmp2.setValue(tmpCmp1.getValue());
          });
        }
      });

    } else {
      var table1 = playerView.model.getModelWithId(t1);
      var table2 = playerView.model.getModelWithId(t2);

      cmpArr1.forEach(function(cmp, i) {
        var tmpCmp1 = playerView.model.getModelWithId(cmp, t1, 0);
        var tmpCmp2 = playerView.model.getModelWithId(cmpArr2[i], t2, 0);
        if (tmpCmp1 && tmpCmp2) {
          tmpCmp1.on("valueChange", function() {
            tmpCmp2.setValue(tmpCmp1.getValue());
          });
        }
        playerView.model.getModelWithId(cmpArr2[i], t2, 0).setValue(playerView.model.getModelWithId(cmp, t1, 0).getValue());
      });

      if (table1 && table2) {
        table1.on('tableRowAdd', function(evt, modelTab, modelRow) {
          table2.createRow();
          cmpArr1.forEach(function(cmp, i) {
            var tmpCmp1 = playerView.model.getModelWithId(cmp, t1, modelRow[0].asfProperty.tableBlockIndex);
            var tmpCmp2 = playerView.model.getModelWithId(cmpArr2[i], t2, modelRow[0].asfProperty.tableBlockIndex);
            if (tmpCmp1 && tmpCmp2) {
              tmpCmp1.on("valueChange", function() {
                tmpCmp2.setValue(tmpCmp1.getValue());
              });
            }
          });
          ALL_DOCUMENTS_UTIL.tableEnabled(playerView, t2);
        });
        table1.on('tableRowDelete', function(evt, rowModel, rowIndex, removedRow) {
          table2.removeRow(rowIndex);
        });
      }
      ALL_DOCUMENTS_UTIL.tableEnabled(playerView, t2);
    }
  },

  msgShow: function(msg, type) {
    var c1 = '#FF9999';
    var c2 = 'rgba(255, 153, 153, 0.7)';
    if (type == 'info') {
      c1 = '#B5D5FF';
      c2 = 'rgba(181, 213, 255, 0.7)';
    }
    var msgBox = $('<div/>');
    msgBox.css({
      'position': 'fixed',
      'left': '50%',
      'margin-right': '-50%',
      'transform': 'translate(-50%)',
      'top': '20px',
      'color': 'black',
      'font-size': '14px',
      'font-weight': 'bold',
      'text-align': 'center',
      'padding': '20px',
      'outline': 'none',
      'border-radius': '3px',
      'background': c1,
      'background-color': c2,
      'display': 'none',
      'z-index': '999'
    });
    $('body').append(msgBox);
    msgBox.html(msg).fadeToggle('normal');
    setTimeout(function() {
      msgBox.fadeToggle('normal');
      setTimeout(function() {
        msgBox.remove();
      }, 1000);
    }, 5000);
  },

  WORK_EXPERIENCE: {
    recalc: function(d1, d2) {
      if (d1 && d2) {
        d2.setHours(0, 0, 0, 0);
        var m = d1.getMonth() + 1;
        var d = d1.getDate();
        for (m = 0;; m++) {
          g = new Date(d1.getFullYear(), d1.getMonth() + 2, 0);
          g.getDate() > d && g.setDate(d);
          if (g > d2) break;
          d1 = g;
        }
        d = Math.round((d2 - d1) / 864E5);
        g = Math.floor(m / 12);
        m = m % 12;
        return [g, m, d];
      } else {
        return null;
      }
    },

    parse: function(D) {
      if (D) {
        var result = [];
        var w = [
          ['год', 'года', 'лет'],
          ['месяц', 'месяца', 'месяцев'],
          ['день', 'дня', 'дней']
        ];
        D.forEach(function(n, i) {
          var k = n % 10,
          l = (!k || n > 5 && n < 21 || k > 4) ? 2 : ((k == 1) ? 0 : 1);
          result.push(n + ' ' + w[i][l]);
        });
        return result;
      } else {
        return null;
      }
    },

    sum: function(M) {
      if (M) {
        // M: [[0], [1],..., [n]]
        var g = 0, m = 0, d = 0;
        M.forEach(function(arr, i) {
          g += arr[0];
          m += arr[1];
          d += arr[2];
        });
        if (d > 30) m += Math.floor(d / 30), d = d % 30;
        if (m > 11) g += Math.floor(m / 12), m = m % 12;
        return [g, m, d];
      } else {
        return null;
      }
    }
  },

  getAsfDataJsonDocID: function(documentID) {
    var asfDataID = null;
    var result = null;
    $.ajax({
      url: 'rest/api/docflow/doc/document_info?documentID=' + documentID,
      dataType: 'json',
      async: false,
      username: ALL_DOCUMENTS_UTIL.synergyLogin,
      password: ALL_DOCUMENTS_UTIL.synergyPass,
      success: function(doc) {
        asfDataID = doc.asfDataID;
      }
    });
    if (asfDataID) {
      $.ajax({
        url: 'rest/api/asforms/data/' + asfDataID,
        dataType: 'json',
        async: false,
        username: ALL_DOCUMENTS_UTIL.synergyLogin,
        password: ALL_DOCUMENTS_UTIL.synergyPass,
        success: function(data) {
          result = data;
        }
      });
    }
    return result;
  },

  getAsfDataObject: function(data, cmpID) {
    var result = null;
    data.data.forEach(function(d) {
      if (d.id == cmpID) result = d;
    });
    return result;
  },

  getOrganizationData: function() {
    var result = null;
    $.ajax({
      url: 'rest/api/departments/content?departmentID=1',
      dataType: 'json',
      async: false,
      username: ALL_DOCUMENTS_UTIL.synergyLogin,
      password: ALL_DOCUMENTS_UTIL.synergyPass,
      success: function(data) {
        var depId = data.departments[0].departmentID;
        $.ajax({
          url: 'rest/api/departments/get?departmentID='+ depId,
          dataType: 'json',
          async: false,
          username: ALL_DOCUMENTS_UTIL.synergyLogin,
          password: ALL_DOCUMENTS_UTIL.synergyPass,
          success: function(data) {
            result = data;
          }
        });
      }
    });
    return result;
  },

  getCardsData: function(api, formCode){
    var result = null;
    $.ajax({
      url: api,
      dataType: 'json',
      async: false,
      username: ALL_DOCUMENTS_UTIL.synergyLogin,
      password: ALL_DOCUMENTS_UTIL.synergyPass,
      success: function(cards) {
        var cardID = null;
        for (var i = 0; i < cards.length; i++) {
          if (cards[i].formCode == formCode) {
            cardID = cards[i]['data-uuid'];
            break;
          }
        }
        if (cardID) {
          $.ajax({
            url: 'rest/api/asforms/data/'+ cardID,
            dataType: 'json',
            async: false,
            username: ALL_DOCUMENTS_UTIL.synergyLogin,
            password: ALL_DOCUMENTS_UTIL.synergyPass,
            success: function(data) {
              result = data;
            }
          });
        }
      }
    });
    return result;
  },

  searchHolidays: function(year) {
    var formCode = "Справочник_праздников";
    var holidayArr = []; // массив праздничных дней
    // Формирование поисковой строки
    var paramSearch = '?formCode='+formCode+'&search='+year+'&field=year&type=exact&recordCount=1&showDeleted=false&searchInRegistry=true';
    $.ajax({
      url: "rest/api/asforms/search" + paramSearch,
      dataType: 'json',
      async: false,
      username: ALL_DOCUMENTS_UTIL.synergyLogin,
      password: ALL_DOCUMENTS_UTIL.synergyPass,
      success: function(searchResult) {
        if (searchResult.length > 0) {
          $.ajax({
            url: 'rest/api/asforms/data/' + searchResult[0],
            dataType: 'json',
            async: false,
            username: ALL_DOCUMENTS_UTIL.synergyLogin,
            password: ALL_DOCUMENTS_UTIL.synergyPass,
            success: function(data) {
              data = data.data[2].data;
              for (var mm=1; mm<13; mm++) {
                for (var i = 0; i < data.length; i++) {
                  var tmp = data[i].id.substring(data[i].id.indexOf('-'));
                  var tmpDay = null, tmpHol = null;
                  if (data[i].id == ('nameHoliday' + tmp)) {
                    if (data[i + 2].key == mm) {
                      tmpHol = data[i].value;
                      tmpDay = parseInt(data[i + 1].key);
                      holidayArr.push({year: year, month: mm, day: tmpDay, holiday: tmpHol});
                    }
                  }
                }
              }
            }
          });
        }
      }
    });
    return holidayArr;
  },

  getHolidaysPeriod: function(startDate, stopDate) {
    var periodDay = Math.floor((AS.FORMS.DateUtils.parseDate(stopDate).getTime() - AS.FORMS.DateUtils.parseDate(startDate).getTime()) / 86400000);
    var neuchetDay = 0;
    var tmpDate1 = tmpDate3 = AS.FORMS.DateUtils.parseDate(startDate);
    var tmpDate2 = AS.FORMS.DateUtils.parseDate(stopDate);
    var tmpYear1 = tmpDate1.getFullYear();
    var tmpYear2 = tmpDate2.getFullYear();

    var holidays = ALL_DOCUMENTS_UTIL.searchHolidays(tmpYear1);

    if (tmpYear1 != tmpYear2) {
      var tmpHoliday = ALL_DOCUMENTS_UTIL.searchHolidays(tmpYear2);
      if (holidays && tmpHoliday) {
        tmpHoliday.forEach(function(holiday) {
          holidays.push(holiday);
        });
      }
    }

    if (holidays) {
      for (var ii = 1; ii < periodDay+2; ii++) {
        if (tmpDate3 > tmpDate2) {
          break;
        } else {
          //console.log("year: " + tmpDate3.getFullYear() + " month: " + (tmpDate3.getMonth()+1) + " day: " + tmpDate3.getDate());
          holidays.forEach(function(d) {
            if (d.year == tmpDate3.getFullYear()
            && d.month == tmpDate3.getMonth()+1
            && d.day == tmpDate3.getDate()) neuchetDay++;
          });
          tmpDate3.setDate(tmpDate3.getDate() + 1);
        }
      }
    }
    return neuchetDay;
  },

  setColorHeaderTables: function(tables, color) {
    tables.forEach(function(t) {
      t.forEach(function(c) {
        var tmpLabelEl = $('[data-asformid="label.label.'+c+'"]');
        if (tmpLabelEl.length > 0) {
          for(var i = 0; i < tmpLabelEl.length; i ++) {
            $(tmpLabelEl[i]).parent().parent().css('background-color', color.background); // фон
            $(tmpLabelEl[i]).css({"color": color.text, "cursor": "default"}); // текст
          }
        } else {
          tmpLabelEl.parent().parent().css('background-color', color.background); // фон
          tmpLabelEl.css({"color": color.text, "cursor": "default"}); // текст
        }
      });
    });
  }

};


AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
    if (ALL_DOCUMENTS_UTIL.companyHistory == 0) {
      console.time("******companyHistory******");
      AS.FORMS.ApiUtils.simpleAsyncGet("rest/api/asforms/search?formCode=Изменения_наименовании_компаний1&showDeleted=false&searchInRegistry=true", function (asfDataIDs) {
        asfDataIDs.forEach(function(uuid) {
          AS.FORMS.ApiUtils.simpleAsyncGet("rest/api/asforms/data/" + uuid, function (data) {
            var periodDateS = null;
            var periodDatePo = null;
            var nameOrganizationRu = null;
            var nameOrganizationKz = null;
            var obosnovanieRu = null;
            var obosnovanieKz = null;
            data.data.forEach(function(data) {
              if (data.id == "start_date1") {
                periodDateS = AS.FORMS.DateUtils.parseDate(data.key);
              }
              if (data.id == "finish_date1") {
                periodDatePo = AS.FORMS.DateUtils.parseDate(data.key);
                if (!periodDatePo) periodDatePo = new Date();
              }
              if (data.id == "naimenovanie") {
                nameOrganizationRu = data.value;
              }
              if (data.id == "naimenovanie_k") {
                nameOrganizationKz = data.value;
              }
              if (data.id == "obosnovanie") {
                obosnovanieRu = data.value;
              }
              if (data.id == "obosnovanie_k") {
                obosnovanieKz = data.value;
              }
            });
            ALL_DOCUMENTS_UTIL.companyHistory.push({
              name: {ru: nameOrganizationRu, kz: nameOrganizationKz},
              periodS: periodDateS,
              periodPo: periodDatePo,
              obosnovanie: {ru: obosnovanieRu, kz: obosnovanieKz}
            });
          });
        });
      });
      console.timeEnd("******companyHistory******");
    }
  });
});
