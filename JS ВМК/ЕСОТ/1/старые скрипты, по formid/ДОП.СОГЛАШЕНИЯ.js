var DOP_SOGLASH_UTIL = {
  changedValue: function(model, cmpObj) {
    if (model.getModelWithId(cmpObj.ru) && model.getModelWithId(cmpObj.kz)) {
      model.getModelWithId(cmpObj.ru).on("valueChange", function() {
        if (model.getModelWithId(cmpObj.kz) ) {
          model.getModelWithId(cmpObj.kz).setValue(model.getModelWithId(cmpObj.ru).getValue());
        }
      });
      model.getModelWithId(cmpObj.kz).on("valueChange", function() {
        if (model.getModelWithId(cmpObj.ru) ) {
          model.getModelWithId(cmpObj.ru).setValue(model.getModelWithId(cmpObj.kz).getValue());
        }
      });
    }
  }
};

AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  var formDopSogArr = ["4a694d51-6af2-4908-a1fb-ffc651ee256c"];
  for (var fi=0; fi < formDopSogArr.length; fi++) {
    if (model.formId == formDopSogArr[fi]) {
      var cmpArrDS=[{ru: "doc_num", kz: "doc_num_k"}, {ru: "td_date", kz: "td_date_k"}, {ru: "td_num", kz: "td_num_k"}, {ru: "doc_date", kz: "doc_date_k"}, {ru: "position", kz: "position_k"}, {ru: "user", kz: "user_k"}, {ru: "user1", kz: "user1_k"}, {ru: "num_pas", kz: "num_pas_k"}, {ru: "choice", kz: "choice_k"}, {ru: "date_pas", kz: "date_pas_k"}, {ru: "iin_pas", kz: "iin_pas_k"}, {ru: "choice1", kz: "choice1_k"}, {ru: "punkt", kz: "punkt_k"}, {ru: "razdel", kz: "razdel_k"}, {ru: "razdel1", kz: "razdel1_k"}, {ru: "date_razdel", kz: "date_razdel_k"}, {ru: "start_date", kz: "start_date_k"}, {ru: "choice2", kz: "choice2_k"}, {ru: "choice3", kz: "choice3_k"}, {ru: "position2", kz: "position2_k"}, {ru: "user2", kz: "user2_k"}, {ru: "position3", kz: "position3_k"}, {ru: "user3", kz: "user3_k"}, {ru: "finish_date", kz: "finish_date_k"}];

      model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
        console.log(model.formName + '\nformID: [' + model.formId +'] asfDataID: [' + model.asfDataId+']');
        for (var ci=0; ci < cmpArrDS.length; ci++) {
          DOP_SOGLASH_UTIL.changedValue(model, cmpArrDS[ci]);
        }
      });
    }
  }
});
