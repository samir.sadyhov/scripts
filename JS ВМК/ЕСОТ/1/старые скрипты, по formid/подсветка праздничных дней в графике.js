AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  if (model.formId == '87c786ff-5c9c-4593-8499-5ab90c770f53' || model.formId == 'da925a38-2e47-457f-b858-b15aa7cbc16e') { //годовые графики
    setTimeout(searchHolidaysG, 500, model);
    model.getModelWithId('year').on(AS.FORMS.EVENT_TYPE.valueChange, function() {
      searchHolidaysG(model);
    });
    model.getModelWithId('table').on('tableRowAdd', function() {
      searchHolidaysG(model);
    });
  }
});
function searchHolidaysG(model) {
  var synergyLogin = 'Administrator';
  var synergyPass = '$ystemAdm1n';
  var year = model.getModelWithId('year').value[0];
  clearStyleG();
  // Формирование поисковой строки
  var paramSearch = '?formUUID=412eebc4-08ee-4b61-b6d1-8036fecc5f32&search=' + fullYear(year) + '&field=year&type=exact&recordCount=1&showDeleted=false&searchInRegistry=true';
  $.ajax({
    url: "rest/api/asforms/search" + paramSearch,
    dataType: 'json',
    username: synergyLogin,
    password: synergyPass,
    success: function(searchResult) {
      if (searchResult.length > 0) {
        $.ajax({
          url: 'rest/api/asforms/data/' + searchResult[0],
          dataType: 'json',
          username: synergyLogin,
          password: synergyPass,
          success: function(data) {
            data = data.data[2].data;
            for (var mm=1; mm<13; mm++) {
              var holidayArrG = []; // массив праздничных дней
              for (var i = 0; i < data.length; i++) {
                var tmp = data[i].id.substring(data[i].id.indexOf('-'));
                var tmpDay = null, tmpHol = null, tmpColor = null;
                if (data[i].id == ('nameHoliday' + tmp)) {
                  if (data[i + 2].key == mm) {
                    tmpHol = data[i].value;
                    tmpDay = data[i + 1].key;
                    tmpColor = data[i + 3].key;
                    holidayArrG.push({day: tmpDay, holiday: tmpHol, color: tmpColor});
                  }
                }
              }
              if (holidayArrG.length > 0) { //console.log(holidayArrG);
                for (var ha = 0; ha < holidayArrG.length; ha++) {
                  var cmpName = 'div[data-asformid="textbox.container.'+mm+'_day_'+holidayArrG[ha].day+'"]';
                  setStyleG(cmpName, holidayArrG[ha].holiday, holidayArrG[ha].color);
                }
              }
            } // for (var mm=1; mm<13; mm++)
          }
        });
      } //if (searchResult.length > 0)
    }
  });
}
function setStyleG(cmp, titleText, bgColor) {
  var tmpEl = $(cmp);
  for (var i = 0; i < tmpEl.length; i++) {
    var tmp2 = $(tmpEl[i]).parent('td')[0];
    if (tmp2) {
      tmp2.style.cursor = "pointer";
      tmp2.style.backgroundColor = bgColor;
      tmp2.title = titleText;
    }
  } //console.log('setStyle');
}
function clearStyleG() {
  for (var mm=1; mm<13; mm++) {
    for (var j=1; j<32; j++) {
      var searchEl='div[data-asformid="textbox.container.'+mm+'_day_'+j+'"]';
      var tmpEl = $(searchEl);
      for (var i = 0; i < tmpEl.length; i++) {
        var tmp2 = $(tmpEl[i]).parent('td')[0];
        if (tmp2) {
          tmp2.style.cursor = '';
          tmp2.style.backgroundColor = '';
          tmp2.title = '';
        }
      }
    }
  } //console.log('clearStyle');
}
function fullYear(year) {
  var tmp=Number(year);
  switch (tmp) {
    case 1:
      return '2015';
      break;
    case 2:
      return '2016';
      break;
    case 3:
      return '2017';
      break;
  }
}
