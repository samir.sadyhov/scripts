AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  if (model.formId == "00812a52-1f51-4a7c-aca7-1009bde5bba9") {
    var table = model.getModelWithId("table");
    table.on('tableRowAdd', function(evt, modelTab, modelRow) {
      table.modelBlocks.forEach(function(modelBlock, index){
          table.getModelWithId("day", table.asfProperty.id, modelBlock[0].asfProperty.tableBlockIndex).setValue((index+1)+"");
      });
    });
    table.on('tableRowDelete', function(evt, rowModel, rowIndex, removedRow) {
      table.modelBlocks.forEach(function(modelBlock, index){
          table.getModelWithId("day", table.asfProperty.id, modelBlock[0].asfProperty.tableBlockIndex).setValue((index+1)+"");
      });
    });
  }
});
