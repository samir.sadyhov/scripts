AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  if (model.formId == '47fd9724-00b8-4fdc-958d-a06455628b4d') {
    model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
      var userCards = {
        lk: { //Личная карточка (штатная расстановка)
          formID: "6e7ee767-7f9b-4f53-b7d9-04a6091d066f",
          tabNomer: "t_number"
        }
      };

      var formCmp = {
        dateS: {ru: "start_date", kz: "start_date_k"},
        dateF: {ru: "finish_date", kz: "finish_date_k"},
        table1: {
          id: {ru: "table2_r", kz: "table2_k"},
          user: {ru: "user", kz: "user_k"},
          tabNom: {ru: "tab_num", kz: "tab_num_k"},
          position: {ru: "position", kz: "position_k"},
          vidPerevoda: {ru: "choice13", kz: "choice13_k"},
          positionN: {ru: "change_position", kz: "change_position_k"}
        },
        tarif: {ru: "choice11", kz: "choice11_k"},
        table2: {
          user: {ru: "oznak_user", kz: "oznak_user_k"}
        },
        table3: {
          id: {ru: "table3_r", kz: "table3_k"},
          choice2: {ru: "choice2", kz: "choice2_k"},
          choice3: {ru: "choice3", kz: "choice3_k"},
          choice4: {ru: "choice4", kz: "choice4_k"}
        },
        instruktUser: {ru: "instruct_user", kz: "instruct_user_k"},
        controlUser: {ru: "proverka_user", kz: "proverka_user_k"},
        table4: {
          id: {ru: "table4_r", kz: "table4_k"},
          nomerSZ: {ru: "sz_num", kz: "sz_num"},
          nomerTD: {ru: "td_num", kz: "td_num_k"},
          date: {ru: "td_date", kz: "td_date_k"}
        },
        podpisFio: {ru: "ruk_user", kz: "ruk_user_k"},
        podpisPos: {ru: "choice5", kz: "choice5_k"},
        table5: {
          id: {ru: "table5_r", kz: "table5_k"},
          user: {ru: "user3", kz: "user3_k"},
          date: {ru: "date2", kz: "date2_k"},
        },
        table6: {
          user: {ru: "ispolnitel_user", kz: "ispolnitel_user_k"},
          nomer: {ru: "ispolnitel_phone", kz: "ispolnitel_phone_k"}
        }

      };

      model.getModelWithId(formCmp.dateS.ru).on(AS.FORMS.EVENT_TYPE.valueChange, function() {
        copyValueKz(model, formCmp.dateS.kz, model.getModelWithId(formCmp.dateS.ru).value);
      });
      model.getModelWithId(formCmp.dateF.ru).on(AS.FORMS.EVENT_TYPE.valueChange, function() {
        copyValueKz(model, formCmp.dateF.kz, model.getModelWithId(formCmp.dateF.ru).value);
      });

      // table1
      // добавление строк в table1
      model.getModelWithId(formCmp.table1.id.ru).on('tableRowAdd', function() {
        model.getModelWithId(formCmp.table1.id.kz).createRow(); //добавление строк
        for (var t1 = 1; t1 < view.getViewWithId(formCmp.table1.id.ru).getRowsCount() + 1; t1++) {
          // юзер
          model.getModelWithId(formCmp.table1.user.ru, formCmp.table1.id.ru, t1).on(AS.FORMS.EVENT_TYPE.valueChange, function() {
            copyValueTableKz(model, formCmp.table1.user.kz, formCmp.table1.id.kz, model.getModelWithId(formCmp.table1.user.ru, formCmp.table1.id.ru, t1-1).value[0], t1-1);
            getCardUserValue(model, userCards.lk.formID, formCmp.table1.user.ru, userCards.lk.tabNomer, formCmp.table1.tabNom.ru, formCmp.table1.id.ru, t1-1); //табельный номер
          });
          model.getModelWithId(formCmp.table1.tabNom.ru, formCmp.table1.id.ru, t1).on(AS.FORMS.EVENT_TYPE.valueChange, function() {
            copyValueTableKz(model, formCmp.table1.tabNom.kz, formCmp.table1.id.kz, model.getModelWithId(formCmp.table1.tabNom.ru, formCmp.table1.id.ru, t1-1).value, t1-1);
          });
          model.getModelWithId(formCmp.table1.position.ru, formCmp.table1.id.ru, t1).on(AS.FORMS.EVENT_TYPE.valueChange, function() {
            copyValueTableKz(model, formCmp.table1.position.kz, formCmp.table1.id.kz, model.getModelWithId(formCmp.table1.position.ru, formCmp.table1.id.ru, t1-1).value[0], t1-1);
            copyValueTableKz(model, formCmp.table1.position.kz, formCmp.table1.id.kz, model.getModelWithId(formCmp.table1.position.ru, formCmp.table1.id.ru, t1-1).value[0], t1-1);
          });
          model.getModelWithId(formCmp.table1.positionN.ru, formCmp.table1.id.ru, t1).on(AS.FORMS.EVENT_TYPE.valueChange, function() {
            copyValueTableKz(model, formCmp.table1.positionN.kz, formCmp.table1.id.kz, model.getModelWithId(formCmp.table1.positionN.ru, formCmp.table1.id.ru, t1-1).value[0], t1-1);
            copyValueTableKz(model, formCmp.table1.positionN.kz, formCmp.table1.id.kz, model.getModelWithId(formCmp.table1.positionN.ru, formCmp.table1.id.ru, t1-1).value[0], t1-1);
          });
          model.getModelWithId(formCmp.table1.vidPerevoda.ru, formCmp.table1.id.ru, t1).on(AS.FORMS.EVENT_TYPE.valueChange, function() {
            copyValueTableKz(model, formCmp.table1.vidPerevoda.kz, formCmp.table1.id.kz, model.getModelWithId(formCmp.table1.vidPerevoda.ru, formCmp.table1.id.ru, t1-1).value[0], t1-1);
          });
        }
      });
      // удаление строк в table1
      model.getModelWithId(formCmp.table1.id.ru).on('tableRowDelete', function(evt, rowModel, rowIndex, removedRow) {
        model.getModelWithId(formCmp.table1.id.kz).removeRow(rowIndex);
      });

      model.getModelWithId(formCmp.tarif.ru).on(AS.FORMS.EVENT_TYPE.valueChange, function() {
        copyValueKz(model, formCmp.tarif.kz, model.getModelWithId(formCmp.tarif.ru).value[0]);
      });

      // table2
      model.getModelWithId(formCmp.table2.user.ru).on(AS.FORMS.EVENT_TYPE.valueChange, function() {
        copyValueKz(model, formCmp.table2.user.kz, model.getModelWithId(formCmp.table2.user.ru).value[0]);
        copyValueKz(model, formCmp.table2.user.kz, model.getModelWithId(formCmp.table2.user.ru).value[0]);
      });

      // добавление строк в table3
      model.getModelWithId(formCmp.table3.id.ru).on('tableRowAdd', function() {
        model.getModelWithId(formCmp.table3.id.kz).createRow(); //добавление строк
        for (var t3 = 1; t3 < view.getViewWithId(formCmp.table3.id.ru).getRowsCount() + 1; t3++) {
          model.getModelWithId(formCmp.table3.choice2.ru, formCmp.table3.id.ru, t3).on(AS.FORMS.EVENT_TYPE.valueChange, function() {
            copyValueTableKz(model, formCmp.table3.choice2.kz, formCmp.table3.id.kz, model.getModelWithId(formCmp.table3.choice2.ru, formCmp.table3.id.ru, t3-1).value[0], t3-1);
          });
          model.getModelWithId(formCmp.table3.choice3.ru, formCmp.table3.id.ru, t3).on(AS.FORMS.EVENT_TYPE.valueChange, function() {
            copyValueTableKz(model, formCmp.table3.choice3.kz, formCmp.table3.id.kz, model.getModelWithId(formCmp.table3.choice3.ru, formCmp.table3.id.ru, t3-1).value[0], t3-1);
          });
          model.getModelWithId(formCmp.table3.choice4.ru, formCmp.table3.id.ru, t3).on(AS.FORMS.EVENT_TYPE.valueChange, function() {
            copyValueTableKz(model, formCmp.table3.choice4.kz, formCmp.table3.id.kz, model.getModelWithId(formCmp.table3.choice4.ru, formCmp.table3.id.ru, t3-1).value[0], t3-1);
          });
        }
      });
      // удаление строк в table3
      model.getModelWithId(formCmp.table3.id.ru).on('tableRowDelete', function(evt, rowModel, rowIndex, removedRow) {
        model.getModelWithId(formCmp.table3.id.kz).removeRow(rowIndex);
      });

      model.getModelWithId(formCmp.instruktUser.ru).on(AS.FORMS.EVENT_TYPE.valueChange, function() {
        copyValueKz(model, formCmp.instruktUser.kz, model.getModelWithId(formCmp.instruktUser.ru).value[0]);
        copyValueKz(model, formCmp.instruktUser.kz, model.getModelWithId(formCmp.instruktUser.ru).value[0]);
      });

      model.getModelWithId(formCmp.controlUser.ru).on(AS.FORMS.EVENT_TYPE.valueChange, function() {
        copyValueKz(model, formCmp.controlUser.kz, model.getModelWithId(formCmp.controlUser.ru).value[0]);
      });


      // добавление строк в table4 (Основание:)
      model.getModelWithId(formCmp.table4.id.ru).on('tableRowAdd', function() {
        model.getModelWithId(formCmp.table4.id.kz).createRow(); //добавление строк
        for (var t4 = 1; t4 < view.getViewWithId(formCmp.table4.id.ru).getRowsCount() + 1; t4++) {
          model.getModelWithId(formCmp.table4.nomerSZ.ru, formCmp.table4.id.ru, t4).on(AS.FORMS.EVENT_TYPE.valueChange, function() {
            copyValueTableKz(model, formCmp.table4.nomerSZ.kz, formCmp.table4.id.kz, model.getModelWithId(formCmp.table4.nomerSZ.ru, formCmp.table4.id.ru, t4-1).value, t4-1);
          });
          model.getModelWithId(formCmp.table4.nomerTD.ru, formCmp.table4.id.ru, t4).on(AS.FORMS.EVENT_TYPE.valueChange, function() {
            copyValueTableKz(model, formCmp.table4.nomerTD.kz, formCmp.table4.id.kz, model.getModelWithId(formCmp.table4.nomerTD.ru, formCmp.table4.id.ru, t4-1).value, t4-1);
          });
          model.getModelWithId(formCmp.table4.date.ru, formCmp.table4.id.ru, t4).on(AS.FORMS.EVENT_TYPE.valueChange, function() {
            copyValueTableKz(model, formCmp.table4.date.kz, formCmp.table4.id.kz, model.getModelWithId(formCmp.table4.date.ru, formCmp.table4.id.ru, t4-1).value, t4-1);
          });
        }
      });
      // удаление строк в table4
      model.getModelWithId(formCmp.table4.id.ru).on('tableRowDelete', function(evt, rowModel, rowIndex, removedRow) {
        model.getModelWithId(formCmp.table4.id.kz).removeRow(rowIndex);
      });


      model.getModelWithId(formCmp.podpisFio.ru).on(AS.FORMS.EVENT_TYPE.valueChange, function() {
        copyValueKz(model, formCmp.podpisFio.kz, model.getModelWithId(formCmp.podpisFio.ru).value[0]);
      });
      model.getModelWithId(formCmp.podpisPos.ru).on(AS.FORMS.EVENT_TYPE.valueChange, function() {
        copyValueKz(model, formCmp.podpisPos.kz, model.getModelWithId(formCmp.podpisPos.ru).value[0]);
      });

      // добавление строк в table5 (ознакомить)
      model.getModelWithId(formCmp.table5.id.ru).on('tableRowAdd', function() {
        model.getModelWithId(formCmp.table5.id.kz).createRow(); //добавление строк
        for (var t5 = 1; t5 < view.getViewWithId(formCmp.table5.id.ru).getRowsCount() + 1; t5++) {
          model.getModelWithId(formCmp.table5.user.ru, formCmp.table5.id.ru, t5).on(AS.FORMS.EVENT_TYPE.valueChange, function() {
            copyValueTableKz(model, formCmp.table5.user.kz, formCmp.table5.id.kz, model.getModelWithId(formCmp.table5.user.ru, formCmp.table5.id.ru, t5-1).value[0], t5-1);
          });
          model.getModelWithId(formCmp.table5.date.ru, formCmp.table5.id.ru, t5).on(AS.FORMS.EVENT_TYPE.valueChange, function() {
            copyValueTableKz(model, formCmp.table5.date.kz, formCmp.table5.id.kz, model.getModelWithId(formCmp.table5.date.ru, formCmp.table5.id.ru, t5-1).value, t5-1);
          });
        }
      });
      // удаление строк в table5
      model.getModelWithId(formCmp.table5.id.ru).on('tableRowDelete', function(evt, rowModel, rowIndex, removedRow) {
        model.getModelWithId(formCmp.table5.id.kz).removeRow(rowIndex);
      });

      copyValueKz(model, formCmp.table6.user.kz, model.getModelWithId(formCmp.table6.user.ru).value[0]);
      model.getModelWithId(formCmp.table6.user.ru).on(AS.FORMS.EVENT_TYPE.valueChange, function() {
        copyValueKz(model, formCmp.table6.user.kz, model.getModelWithId(formCmp.table6.user.ru).value[0]);
      });
      model.getModelWithId(formCmp.table6.nomer.ru).on(AS.FORMS.EVENT_TYPE.valueChange, function() {
        copyValueKz(model, formCmp.table6.nomer.kz, model.getModelWithId(formCmp.table6.nomer.ru).value);
      });

    });
  }
});

function copyValueKz(model, cmp, value) {
  model.getModelWithId(cmp).setValue(value);
}
function copyValueTableKz(model, cmp, table, value, index) {
  model.getModelWithId(cmp, table, index).setValue(value);
}

function getCardUserValue(model, form, cmpUser, cmpParent, cmpChild, table, index) {
  // функция которая вытаскивает с карточки пользователя (form) значение определенного компонента (cmpParent) и вставляет их на форму (cmpChild)
  model.getModelWithId(cmpChild, table, index).setValue('');
  var synergyLogin = 'Administrator';
  var synergyPass = '$ystemAdm1n';
  $.ajax({ // получение карточек пользователя
    url: 'rest/api/personalrecord/forms/' + model.getModelWithId(cmpUser, table, index).value[0].personID,
    dataType: 'json',
    username: synergyLogin,
    password: synergyPass,
    success: function(data) {
      var tmpJson;
      var cardID=null;
      tmpJson = JSON.stringify(data);
      tmpJson = String(tmpJson).replace(/form-uuid/g, "formUuid");
      tmpJson = tmpJson.replace(/data-uuid/g, "dataUuid");
      tmpJson = JSON.parse(tmpJson);
      // поиск нужной карточки по formID
      for (var i = 0; i < tmpJson.length; i++) {
        if (tmpJson[i].formUuid == form) {
          cardID = tmpJson[i].dataUuid; // получаем ID карточки
          break;
        }
      }
      if (cardID) { // если нужная карточка есть достаем данные
        $.ajax({
          url: 'rest/api/asforms/data/' + cardID,
          dataType: 'json',
          username: synergyLogin,
          password: synergyPass,
          success: function(cardData) {
            for (var j = 0; j < cardData.data.length; j++) {
              if (cardData.data[j].id == cmpParent) {
                model.getModelWithId(cmpChild, table, index).setValue(cardData.data[j].value);
                break;
              }
            }
          }
        });
      } //if (cardID)
    }
  });
}
