AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  if (model.formId == '8e3a4959-b04d-4825-8145-fb4739999e16') {
    model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
      var userCards = {
        lk: { //Личная карточка (штатная расстановка)
          formID: "6e7ee767-7f9b-4f53-b7d9-04a6091d066f",
          tabNomer: "t_number",
          addres: "fact_address",
          telephone: ""
        },
        otpusk: { //Отпуска
          formID: "8ac1c13a-6676-4117-99cf-d2c49a489b2a",
          dataPriema: "day_start",
        }
      };

      var formCmp={
        komuUser: {ru: "to_user", kz: "to_user_k"},
        komuPos: {ru: "to_position", kz: "to_position_k"},
        komuDep: {ru: "to_department", kz: "to_department_k"},
        otKogo: {ru: "from_user", kz: "from_user_k"},
        position: {ru: "position", kz: "position_k"},
        address: {ru: "address", kz: "address_k"},
        tel: {ru: "telephone", kz: "telephone_k"},
        ispolnit: {ru: "ispolnitel_user", kz: "ispolnitel_user_k"},

        table: {
          dp: {ru: "choice1", kz: "choice1_k"},
          tabNom: {ru: "tab_num", kz: "tab_num_k"},
          date: {ru: "date", kz: "date_k"}
        }

      };

      // кому
      model.getModelWithId(formCmp.komuUser.ru).on(AS.FORMS.EVENT_TYPE.valueChange, function() {
        copyValueKz(model, formCmp.komuUser.kz, model.getModelWithId(formCmp.komuUser.ru).value[0]);
        copyValueKz(model, formCmp.komuUser.kz, model.getModelWithId(formCmp.komuUser.ru).value[0]);
      });
      // от кого
      model.getModelWithId(formCmp.otKogo.ru).on(AS.FORMS.EVENT_TYPE.valueChange, function() {
        copyValueKz(model, formCmp.otKogo.kz, model.getModelWithId(formCmp.otKogo.ru).value[0]);
        getCardUserValue(model, userCards.lk.formID, formCmp.otKogo.ru, userCards.lk.addres, formCmp.address.ru); //адрес
        getCardUserValue(model, userCards.lk.formID, formCmp.otKogo.ru, userCards.lk.tabNomer, formCmp.table.tabNom.ru); //табельный номер
      });
      // адрес
      model.getModelWithId(formCmp.address.ru).on(AS.FORMS.EVENT_TYPE.valueChange, function() {
        copyValueKz(model, formCmp.address.kz, model.getModelWithId(formCmp.address.ru).value);
      });
      // телефон
      model.getModelWithId(formCmp.tel.ru).on(AS.FORMS.EVENT_TYPE.valueChange, function() {
        copyValueKz(model, formCmp.tel.kz, model.getModelWithId(formCmp.tel.ru).value);
      });
      // должность\подразделение
      model.getModelWithId(formCmp.table.dp.ru).on(AS.FORMS.EVENT_TYPE.valueChange, function() {
        copyValueKz(model, formCmp.table.dp.kz, model.getModelWithId(formCmp.table.dp.ru).value[0]);
      });
      // табельный номер
      model.getModelWithId(formCmp.table.tabNom.ru).on(AS.FORMS.EVENT_TYPE.valueChange, function() {
        copyValueKz(model, formCmp.table.tabNom.kz, model.getModelWithId(formCmp.table.tabNom.ru).value);
      });
      // Исполнитель
      copyValueKz(model, formCmp.ispolnit.kz, model.getModelWithId(formCmp.ispolnit.ru).value[0]);
      model.getModelWithId(formCmp.ispolnit.ru).on(AS.FORMS.EVENT_TYPE.valueChange, function() {
        copyValueKz(model, formCmp.ispolnit.kz, model.getModelWithId(formCmp.ispolnit.ru).value[0]);
      });
      // дата
      model.getModelWithId(formCmp.table.date.ru).on(AS.FORMS.EVENT_TYPE.valueChange, function() {
        copyValueKz(model, formCmp.table.date.kz, model.getModelWithId(formCmp.table.date.ru).value);
      });
      // должность
      model.getModelWithId(formCmp.position.ru).on(AS.FORMS.EVENT_TYPE.valueChange, function() {
        copyValueKz(model, formCmp.position.kz, model.getModelWithId(formCmp.position.ru).value[0]);
      });

    });
  }
});

function copyValueKz(model, cmp, value) {
  model.getModelWithId(cmp).setValue(value);
}

function getCardUserValue(model, form, cmpUser, cmpParent, cmpChild) {
  // функция которая вытаскивает с карточки пользователя (form) значение определенного компонента (cmpParent) и вставляет их на форму (cmpChild)
  model.getModelWithId(cmpChild).setValue('');
  var synergyLogin = 'Administrator';
  var synergyPass = '$ystemAdm1n';
  $.ajax({ // получение карточек пользователя
    url: 'rest/api/personalrecord/forms/' + model.getModelWithId(cmpUser).value[0].personID,
    dataType: 'json',
    username: synergyLogin,
    password: synergyPass,
    success: function(data) {
      var tmpJson;
      var cardID=null;
      tmpJson = JSON.stringify(data);
      tmpJson = String(tmpJson).replace(/form-uuid/g, "formUuid");
      tmpJson = tmpJson.replace(/data-uuid/g, "dataUuid");
      tmpJson = JSON.parse(tmpJson);
      // поиск нужной карточки по formID
      for (var i = 0; i < tmpJson.length; i++) {
        if (tmpJson[i].formUuid == form) {
          cardID = tmpJson[i].dataUuid; // получаем ID карточки
          break;
        }
      }
      if (cardID) { // если нужная карточка есть достаем данные
        $.ajax({
          url: 'rest/api/asforms/data/' + cardID,
          dataType: 'json',
          username: synergyLogin,
          password: synergyPass,
          success: function(cardData) {
            for (var j = 0; j < cardData.data.length; j++) {
              if (cardData.data[j].id == cmpParent) {
                model.getModelWithId(cmpChild).setValue(cardData.data[j].value);
                break;
              }
            }
          }
        });
      } //if (cardID)
    }
  });
}
