AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  if (model.formId == '6bfe9ab1-11dc-4ad9-934d-9211957ba3a7') {
    model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
      var userCards = {
        lk: { //Личная карточка (штатная расстановка)
          formID: "6e7ee767-7f9b-4f53-b7d9-04a6091d066f",
          tabNomer: "t_number",
          addres: "fact_address",
          telephone: ""
        }
      };

      var formCmp={
        date: {ru: "date", kz: "date_k"},
        nomer: {ru: "nomer", kz: "nomer_k"},
        choice1: {ru: "choice1", kz: "choice1_k"},
        choice2: {ru: "choice2", kz: "choice2_k"},
        choice3: {ru: "choice3", kz: "choice3_k"},
        table1: {
          dp: {ru: "choice_position", kz: "choice_position_k"},
          razmer: {ru: "choice4", kz: "choice4_k"},
          amount: {ru: "amount", kz: "amount_k"},
          user: {ru: "user", kz: "user_k"},
          tabNom: {ru: "tab_num", kz: "cmp-p38udy"}
        },
        table2: { //2
          user: {ru: "user2", kz: "user2_k"}
        },
        controlUser: {ru: "proverka_user", kz: "proverka_user_k"},
        table3: { //основание
          id: {ru: "tab", kz: "tab_k"},
          docNum: {ru: "zapiska", kz: "zapiska_k"},
          user: {ru: "osnovanie_user", kz: "osnovanie_user_k"}
        },
        podpisFio: {ru: "podpisant_user", kz: "podpisant_user_k"},
        podpisPos: {ru: "podpisant_choice", kz: "podpisant_choice_k"},
        table4: { //ознакомление
          id: {ru: "ttt", kz: "ttt_k"},
          data: {ru: "oznakomlenie_date", kz: "oznakomlenie_date_k"},
          user: {ru: "oznakomlenie_user", kz: "oznakomlenie_user_k"}
        },
        ispUser: {ru: "ispolnitel_user", kz: "ispolnitel_user_k"},
        ispPhone: {ru: "ispolnitel_phone", kz: "ispolnitel_phone_k"}
      };

      model.getModelWithId(formCmp.date.ru).on("valueChange", function() {
        model.getModelWithId(formCmp.date.kz).setValue(model.getModelWithId(formCmp.date.ru).getValue());
      });
      model.getModelWithId(formCmp.nomer.ru).on("valueChange", function() {
        model.getModelWithId(formCmp.nomer.kz).setValue(model.getModelWithId(formCmp.nomer.ru).getValue());
      });
      model.getModelWithId(formCmp.choice1.ru).on("valueChange", function() {
        model.getModelWithId(formCmp.choice1.kz).setValue(model.getModelWithId(formCmp.choice1.ru).getValue());
      });
      model.getModelWithId(formCmp.choice2.ru).on("valueChange", function() {
        model.getModelWithId(formCmp.choice2.kz).setValue(model.getModelWithId(formCmp.choice2.ru).getValue());
      });
      model.getModelWithId(formCmp.choice3.ru).on("valueChange", function() {
        model.getModelWithId(formCmp.choice3.kz).setValue(model.getModelWithId(formCmp.choice3.ru).getValue());
      });
      model.getModelWithId(formCmp.table1.dp.ru).on("valueChange", function() {
        model.getModelWithId(formCmp.table1.dp.kz).setValue(model.getModelWithId(formCmp.table1.dp.ru).getValue());
      });
      model.getModelWithId(formCmp.table1.razmer.ru).on("valueChange", function() {
        model.getModelWithId(formCmp.table1.razmer.kz).setValue(model.getModelWithId(formCmp.table1.razmer.ru).getValue());
      });
      model.getModelWithId(formCmp.table1.amount.ru).on("valueChange", function() {
        model.getModelWithId(formCmp.table1.amount.kz).setValue(model.getModelWithId(formCmp.table1.amount.ru).getValue());
      });
      model.getModelWithId(formCmp.table1.user.ru).on("valueChange", function() {
        model.getModelWithId(formCmp.table1.user.kz).setValue(model.getModelWithId(formCmp.table1.user.ru).getValue());
        getCardUserValue(model, userCards.lk.formID, formCmp.table1.user.ru, userCards.lk.tabNomer, formCmp.table1.tabNom.ru); //табельный номер
      });
      model.getModelWithId(formCmp.table1.tabNom.ru).on("valueChange", function() {
        model.getModelWithId(formCmp.table1.tabNom.kz).setValue(model.getModelWithId(formCmp.table1.tabNom.ru).getValue());
      });
      model.getModelWithId(formCmp.table2.user.ru).on("valueChange", function() {
        model.getModelWithId(formCmp.table2.user.kz).setValue(model.getModelWithId(formCmp.table2.user.ru).getValue());
      });
      model.getModelWithId(formCmp.controlUser.ru).on("valueChange", function() {
        model.getModelWithId(formCmp.controlUser.kz).setValue(model.getModelWithId(formCmp.controlUser.ru).getValue());
      });
      model.getModelWithId(formCmp.podpisFio.ru).on("valueChange", function() {
        model.getModelWithId(formCmp.podpisFio.kz).setValue(model.getModelWithId(formCmp.podpisFio.ru).getValue());
      });
      model.getModelWithId(formCmp.podpisPos.ru).on("valueChange", function() {
        model.getModelWithId(formCmp.podpisPos.kz).setValue(model.getModelWithId(formCmp.podpisPos.ru).getValue());
      });
      model.getModelWithId(formCmp.ispUser.ru).on("valueChange", function() {
        model.getModelWithId(formCmp.ispUser.kz).setValue(model.getModelWithId(formCmp.ispUser.ru).getValue());
      });
      model.getModelWithId(formCmp.ispPhone.ru).on("valueChange", function() {
        model.getModelWithId(formCmp.ispPhone.kz).setValue(model.getModelWithId(formCmp.ispPhone.ru).getValue());
      });

      // table3
      model.getModelWithId(formCmp.table3.id.ru).on('tableRowAdd', function(evt, modelTab, modelRow) {
        var t3 = modelRow[0].asfProperty.tableBlockIndex;
        model.getModelWithId(formCmp.table3.id.kz).createRow(); //добавление строк
          // Служебная записка руководителя СП №
          model.getModelWithId(formCmp.table3.docNum.ru, formCmp.table3.id.ru, t3).on("valueChange", function() {
            model.getModelWithId(formCmp.table3.docNum.kz, formCmp.table3.id.kz, t3).setValue(model.getModelWithId(formCmp.table3.docNum.ru, formCmp.table3.id.ru, t3).getValue());
          });
          // Письменное согласие ФИО
          model.getModelWithId(formCmp.table3.user.ru, formCmp.table3.id.ru, t3).on("valueChange", function() {
            model.getModelWithId(formCmp.table3.user.kz, formCmp.table3.id.kz, t3).setValue(model.getModelWithId(formCmp.table3.user.ru, formCmp.table3.id.ru, t3).getValue());
          });
      });
      // удаление строк в table3
      model.getModelWithId(formCmp.table3.id.ru).on('tableRowDelete', function(evt, rowModel, rowIndex, removedRow) {
        model.getModelWithId(formCmp.table3.id.kz).removeRow(rowIndex);
      });
      // запрет на изменение таблицы table3.kz
      view.getViewWithId(formCmp.table3.id.kz).setEnabled(false);

      // table4
      model.getModelWithId(formCmp.table4.id.ru).on('tableRowAdd', function(evt, modelTab, modelRow) {
        var t4 = modelRow[0].asfProperty.tableBlockIndex;
        model.getModelWithId(formCmp.table4.id.kz).createRow(); //добавление строк
          // Дата
          model.getModelWithId(formCmp.table4.data.ru, formCmp.table4.id.ru, t4).on("valueChange", function() {
            model.getModelWithId(formCmp.table4.data.kz, formCmp.table4.id.kz, t4).setValue(model.getModelWithId(formCmp.table4.data.ru, formCmp.table4.id.ru, t4).getValue());
          });
          // ФИО
          model.getModelWithId(formCmp.table4.user.ru, formCmp.table4.id.ru, t4).on("valueChange", function() {
            model.getModelWithId(formCmp.table4.user.kz, formCmp.table4.id.kz, t4).setValue(model.getModelWithId(formCmp.table4.user.ru, formCmp.table4.id.ru, t4).getValue());
          });
      });
      // удаление строк в table4
      model.getModelWithId(formCmp.table4.id.ru).on('tableRowDelete', function(evt, rowModel, rowIndex, removedRow) {
        model.getModelWithId(formCmp.table4.id.kz).removeRow(rowIndex);
      });
      // запрет на изменение таблицы table4.kz
      view.getViewWithId(formCmp.table4.id.kz).setEnabled(false);

    });
  }
});

function getCardUserValue(model, form, cmpUser, cmpParent, cmpChild) {
  // функция которая вытаскивает с карточки пользователя (form) значение определенного компонента (cmpParent) и вставляет их на форму (cmpChild)
  model.getModelWithId(cmpChild).setValue('');
  var synergyLogin = 'Administrator';
  var synergyPass = '$ystemAdm1n';
  $.ajax({ // получение карточек пользователя
    url: 'rest/api/personalrecord/forms/' + model.getModelWithId(cmpUser).value[0].personID,
    dataType: 'json',
    username: synergyLogin,
    password: synergyPass,
    success: function(data) {
      var tmpJson;
      var cardID=null;
      tmpJson = JSON.stringify(data);
      tmpJson = String(tmpJson).replace(/form-uuid/g, "formUuid");
      tmpJson = tmpJson.replace(/data-uuid/g, "dataUuid");
      tmpJson = JSON.parse(tmpJson);
      // поиск нужной карточки по formID
      for (var i = 0; i < tmpJson.length; i++) {
        if (tmpJson[i].formUuid == form) {
          cardID = tmpJson[i].dataUuid; // получаем ID карточки
          break;
        }
      }
      if (cardID) { // если нужная карточка есть достаем данные
        $.ajax({
          url: 'rest/api/asforms/data/' + cardID,
          dataType: 'json',
          username: synergyLogin,
          password: synergyPass,
          success: function(cardData) {
            for (var j = 0; j < cardData.data.length; j++) {
              if (cardData.data[j].id == cmpParent) {
                model.getModelWithId(cmpChild).setValue(cardData.data[j].value);
                break;
              }
            }
          }
        });
      } //if (cardID)
    }
  });
}
