AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  if (model.formId == '0001c099-596d-4f51-93a2-37728c18d284') { //Табель. месяц (1 вар-т)
    var cmpArr = ['month', 'year'];
    setTimeout(searchHolidays, 700, model);
    for (var i = 0; i < cmpArr.length; i++) {
      model.getModelWithId(cmpArr[i]).on(AS.FORMS.EVENT_TYPE.valueChange, function() {
        searchHolidays(model);
      });
    }
    model.getModelWithId('table').on('tableRowAdd', function() {
      searchHolidays(model);
    });
  }
});
function searchHolidays(model) {
  var synergyLogin = 'Administrator';
  var synergyPass = '$ystemAdm1n';
  var month = model.getModelWithId('month').value[0];
  var year = model.getModelWithId('year').value[0];
  clearStyle();
  // Формирование поисковой строки
  var paramSearch = '?formUUID=412eebc4-08ee-4b61-b6d1-8036fecc5f32&search=' + fullYear(year) + '&field=year&type=exact&recordCount=1&showDeleted=false&searchInRegistry=true';
  $.ajax({
    url: "rest/api/asforms/search" + paramSearch,
    dataType: 'json',
    username: synergyLogin,
    password: synergyPass,
    success: function(searchResult) {
      if (searchResult.length > 0) {
        $.ajax({
          url: 'rest/api/asforms/data/' + searchResult[0],
          dataType: 'json',
          username: synergyLogin,
          password: synergyPass,
          success: function(data) {
            data = data.data[2].data;
            var holidayArr = []; // массив праздничных дней на выбранный месяц
            for (var i = 0; i < data.length; i++) {
              var tmp = data[i].id.substring(data[i].id.indexOf('-'));
              var tmpDay = null, tmpHol = null, tmpColor = null;
              if (data[i].id == ('nameHoliday' + tmp)) {
                if (data[i + 2].key == month) {
                  tmpHol = data[i].value;
                  tmpDay = data[i + 1].key;
                  tmpColor = data[i + 3].key;
                  holidayArr.push({day: tmpDay, holiday: tmpHol, color: tmpColor});
                }
              }
            }
            if (holidayArr.length > 0) {
              //console.log(holidayArr);
              for (var ha = 0; ha < holidayArr.length; ha++) {
                var cmpName = 'div[data-asformid="textbox.container.day_' + holidayArr[ha].day + '"]';
                setStyle(cmpName, holidayArr[ha].holiday, holidayArr[ha].color);
              }
            }
          }
        });
      } //if (searchResult.length > 0)
    }
  });
}
function setStyle(cmp, titleText, bgColor) {
  var tmpEl = $(cmp);
  for (var i = 0; i < tmpEl.length; i++) {
    var tmp2 = $(tmpEl[i]).parent('td')[0];
    if (tmp2) {
      tmp2.style.cursor = "pointer";
      tmp2.style.backgroundColor = bgColor;
      tmp2.title = titleText;
    }
  } //console.log('setStyle');
}
function clearStyle() {
  for (var j = 1; j < 32; j++) {
    var searchEl = 'div[data-asformid="textbox.container.day_' + j + '"]';
    var tmpEl = $(searchEl);
    for (var i = 0; i < tmpEl.length; i++) {
      var tmp2 = $(tmpEl[i]).parent('td')[0];
      if (tmp2) {
        tmp2.style.cursor = '';
        tmp2.style.backgroundColor = '';
        tmp2.title = '';
      }
    }
  } //console.log('clearStyle');
}
function fullYear(year) {
  var tmp = Number(year);
  switch (tmp) {
    case 1:
      return '2015';
      break;
    case 2:
      return '2016';
      break;
    case 3:
      return '2017';
      break;
  }
}
