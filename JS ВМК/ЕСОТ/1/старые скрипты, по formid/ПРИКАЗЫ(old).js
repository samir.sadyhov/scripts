var PRIKAZ_UTIL = {

  setVisibleCmp: function(cmp, visible) {
    var playerView = $('div[data-asformid="container"]')[0].playerView;
    if (typeof cmp == "object") {
      if (playerView.getViewWithId(cmp.ru)) playerView.getViewWithId(cmp.ru).setVisible(visible);
      if (playerView.getViewWithId(cmp.kz)) playerView.getViewWithId(cmp.kz).setVisible(visible);
    } else {
      if (playerView.getViewWithId(cmp)) playerView.getViewWithId(cmp).setVisible(visible);
    }
  },

  changedValue: function(model, cmpObj) {
    if (model.getModelWithId(cmpObj.ru) && model.getModelWithId(cmpObj.kz)) {
      var userCmpArr=[{ru: "user", kz: "user_k"}];
      model.getModelWithId(cmpObj.ru).on("valueChange", function() {
        if (model.getModelWithId(cmpObj.kz) ) {
          model.getModelWithId(cmpObj.kz).setValue(model.getModelWithId(cmpObj.ru).getValue());
        }
        for (var ri=0; ri < userCmpArr.length; ri++) {
          if (cmpObj.ru == userCmpArr[ri].ru) {
            PRIKAZ_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", userCmpArr[ri].ru, "t_number", "tab_num", null, null);
            PRIKAZ_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", userCmpArr[ri].ru, "fact_address", "address", null, null);
            PRIKAZ_UTIL.getCardUserValue(model, "8ac1c13a-6676-4117-99cf-d2c49a489b2a", userCmpArr[ri].ru, "sum_available_vacat_days", "days_available", null, null);
            PRIKAZ_UTIL.getCardUserValue(model, "8ac1c13a-6676-4117-99cf-d2c49a489b2a", userCmpArr[ri].ru, "sum_vacat_days", "remainder", null, null);
          }
        }
        if (cmpObj.ru == "ispolnitel_user") {
          PRIKAZ_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", "ispolnitel_user", "telephone", "ispolnitel_phone", null, null);
        }
      });
      model.getModelWithId(cmpObj.kz).on("valueChange", function() {
        if (model.getModelWithId(cmpObj.ru) ) {
          model.getModelWithId(cmpObj.ru).setValue(model.getModelWithId(cmpObj.kz).getValue());
        }
        for (var ki=0; ki < userCmpArr.length; ki++) {
          if (cmpObj.kz == userCmpArr[ki].kz) {
            PRIKAZ_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", userCmpArr[ki].kz, "t_number", "tab_num_k", null, null);
            PRIKAZ_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", userCmpArr[ki].kz, "fact_address", "address_k", null, null);
            PRIKAZ_UTIL.getCardUserValue(model, "8ac1c13a-6676-4117-99cf-d2c49a489b2a", userCmpArr[ki].kz, "sum_available_vacat_days", "days_available_k", null, null);
            PRIKAZ_UTIL.getCardUserValue(model, "8ac1c13a-6676-4117-99cf-d2c49a489b2a", userCmpArr[ki].kz, "sum_vacat_days", "remainder_k", null, null);
          }
        }
        if (cmpObj.ru == "ispolnitel_user") {
          PRIKAZ_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", "ispolnitel_user_k", "telephone", "ispolnitel_phone_k", null, null);
        }
      });
    }
  },

  changedValueTable: function(model, table, cmp, index) {
    if (model.getModelWithId(cmp.ru, table.ru, index) && model.getModelWithId(cmp.kz, table.kz, index)) {
      var userCmpArr=[{ru: "user", kz: "user_k"}];
      model.getModelWithId(cmp.ru, table.ru, index).on("valueChange", function() {
        for (var ri=0; ri < userCmpArr.length; ri++) {
          if (cmp.ru == userCmpArr[ri].ru) {
            PRIKAZ_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", userCmpArr[ri].ru, "t_number", "tab_num", table.ru, index);
            PRIKAZ_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", userCmpArr[ri].ru, "fact_address", "address", table.ru, index);
            PRIKAZ_UTIL.getCardUserValue(model, "8ac1c13a-6676-4117-99cf-d2c49a489b2a", userCmpArr[ri].ru, "sum_available_vacat_days", "days_available", table.ru, index);
            PRIKAZ_UTIL.getCardUserValue(model, "8ac1c13a-6676-4117-99cf-d2c49a489b2a", userCmpArr[ri].ru, "sum_vacat_days", "remainder", table.ru, index);
          }
        }
        if (model.getModelWithId(cmp.kz, table.kz, index) ) {
          model.getModelWithId(cmp.kz, table.kz, index).setValue(model.getModelWithId(cmp.ru, table.ru, index).getValue());
        }
      });
      model.getModelWithId(cmp.kz, table.kz, index).on("valueChange", function() {
        for (var ki=0; ki < userCmpArr.length; ki++) {
          if (cmp.kz == userCmpArr[ki].kz) {
            PRIKAZ_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", userCmpArr[ki].kz, "t_number", "tab_num_k", table.kz, index);
            PRIKAZ_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", userCmpArr[ki].kz, "fact_address", "address_k", table.kz, index);
            PRIKAZ_UTIL.getCardUserValue(model, "8ac1c13a-6676-4117-99cf-d2c49a489b2a", userCmpArr[ki].kz, "sum_available_vacat_days", "days_available_k", table.kz, index);
            PRIKAZ_UTIL.getCardUserValue(model, "8ac1c13a-6676-4117-99cf-d2c49a489b2a", userCmpArr[ki].kz, "sum_vacat_days", "remainder_k", table.kz, index);
          }
        }
        if (model.getModelWithId(cmp.ru, table.ru, index) ) {
          model.getModelWithId(cmp.ru, table.ru, index).setValue(model.getModelWithId(cmp.kz, table.kz, index).getValue());
        }
      });
    }
  },

  tableOn: function(tabObj, cmpObj) {
    var playerView = $('div[data-asformid="container"]')[0].playerView;
    var tmpTableRu=playerView.model.getModelWithId(tabObj.ru);
    var tmpTableKz=playerView.model.getModelWithId(tabObj.kz);
    if (tmpTableRu && tmpTableKz) {
      for (var tj=0; tj < cmpObj.length; tj++) {
        PRIKAZ_UTIL.changedValueTable(playerView.model, tabObj, cmpObj[tj], 0);
      }
      tmpTableRu.on('tableRowAdd', function(evt, modelTab, modelRow) {
        tmpTableKz.createRow();
        for (var tj=0; tj < cmpObj.length; tj++) {
          PRIKAZ_UTIL.changedValueTable(playerView.model, tabObj, cmpObj[tj], modelRow[0].asfProperty.tableBlockIndex);
        }
        PRIKAZ_UTIL.tableEnabled(tabObj.kz);
      });
    }
    if (tmpTableRu && tmpTableKz) {
      tmpTableRu.on('tableRowDelete', function(evt, rowModel, rowIndex, removedRow) {
        tmpTableKz.removeRow(rowIndex);
      });
    }
    PRIKAZ_UTIL.tableEnabled(tabObj.kz);
  },

   tableEnabled: function(table) {
     var view=$('div[data-asformid="container"]')[0].playerView;
     if (view.editable) {
       var tmpTable=view.getViewWithId(table);
       if (tmpTable) tmpTable.setEnabled(false);
     } else {
       setTimeout(PRIKAZ_UTIL.tableEnabled, 500, table);
     }
   },

  getCardUserValue: function(model, form, cmpUser, cmpParent, cmpChild, table, index) {
    // функция которая вытаскивает с карточки пользователя (form) значение определенного компонента (cmpParent) и вставляет их на форму (cmpChild)
    if (table == null) {
      if (model.getModelWithId(cmpChild)) model.getModelWithId(cmpChild).setValue('');
    } else {
      if (model.getModelWithId(cmpChild, table, index)) model.getModelWithId(cmpChild, table, index).setValue('');
    }
    var personID=null;
    if (table == null) {
      if (model.getModelWithId(cmpUser).value.length > 0 ) personID = model.getModelWithId(cmpUser).value[0].personID;
    } else {
      if (model.getModelWithId(cmpUser, table, index).value.length > 0 ) personID = model.getModelWithId(cmpUser, table, index).value[0].personID;
    }
    if (personID) {
      var synergyLogin = 'Administrator';
      var synergyPass = '$ystemAdm1n';
      $.ajax({
        url: 'rest/api/personalrecord/forms/' + personID,
        dataType: 'json',
        username: synergyLogin,
        password: synergyPass,
        success: function(data) {
          var tmpJson;
          var cardID=null;
          tmpJson = JSON.stringify(data);
          tmpJson = String(tmpJson).replace(/form-uuid/g, "formUuid");
          tmpJson = tmpJson.replace(/data-uuid/g, "dataUuid");
          tmpJson = JSON.parse(tmpJson);
          for (var i = 0; i < tmpJson.length; i++) {
            if (tmpJson[i].formUuid == form) {
              cardID = tmpJson[i].dataUuid;
              break;
            }
          }
          if (cardID) {
            $.ajax({
              url: 'rest/api/asforms/data/' + cardID,
              dataType: 'json',
              username: synergyLogin,
              password: synergyPass,
              success: function(cardData) {
                for (var j = 0; j < cardData.data.length; j++) {
                  if (cardData.data[j].id == cmpParent) {
                    var tmpBlok=null;
                    if (table == null) {
                      tmpBlok = model.getModelWithId(cmpChild);
                    } else {
                      tmpBlok = model.getModelWithId(cmpChild, table, index);
                    }
                    if (tmpBlok) tmpBlok.setValue(cardData.data[j].value);
                    break;
                  }
                }
              }
            });
          }
        }
      });
    }
  },

  getCardUserValue2: function(form, cmpForm, cmpCard, personID) {
    var playerView = $('div[data-asformid="container"]')[0].playerView;
    if (personID != undefined && personID != null) {
      var synergyLogin = 'Administrator';
      var synergyPass = '$ystemAdm1n';
      $.ajax({
        url: 'rest/api/personalrecord/forms/' + personID,
        dataType: 'json',
        username: synergyLogin,
        password: synergyPass,
        success: function(data) {
          var tmpJson;
          var cardID=null;
          tmpJson = JSON.stringify(data);
          tmpJson = String(tmpJson).replace(/form-uuid/g, "formUuid");
          tmpJson = tmpJson.replace(/data-uuid/g, "dataUuid");
          tmpJson = JSON.parse(tmpJson);
          for (var i = 0; i < tmpJson.length; i++) {
            if (tmpJson[i].formUuid == form) {
              cardID = tmpJson[i].dataUuid;
              break;
            }
          }
          if (cardID) {
            $.ajax({
              url: 'rest/api/asforms/data/' + cardID,
              dataType: 'json',
              username: synergyLogin,
              password: synergyPass,
              success: function(cardData) {
                for (var j = 0; j < cardData.data.length; j++) {
                  if (cardData.data[j].id == cmpCard) {
                    var formText = playerView.model.getModelWithId(cmpForm);
                    formText.on("valueChange", function() {
                      var d1 = Number(formText.getValue());
                      var d2 = Number(cardData.data[j].value);
                      if (d1 > d2) {
                        playerView.getViewWithId(cmpForm).container.parent().css("background-color", "#FF9999");
                        playerView.getViewWithId(cmpForm+"_k").container.parent().css("background-color", "#FF9999");
                        alert("Введенное количество дней больше чем указано в карточке\nМаксимальное значение = " + d2);
                        formText.setValue('');
                        if (cmpForm == "additional") {
                          playerView.getViewWithId("date_last_po").container.parent().css("background-color", "#FF9999");
                          playerView.getViewWithId("date_last_po_k").container.parent().css("background-color", "#FF9999");
                          playerView.model.getModelWithId("date_last_po").setValue('');
                        }
                        if (cmpForm == "main") {
                          playerView.getViewWithId("date_additional").container.parent().css("background-color", "#FF9999");
                          playerView.getViewWithId("date_additional_k").container.parent().css("background-color", "#FF9999");
                          playerView.model.getModelWithId("date_additional").setValue('');
                        }
                      } else {
                        playerView.getViewWithId(cmpForm).container.parent().css("background-color", "");
                        playerView.getViewWithId(cmpForm+"_k").container.parent().css("background-color", "");
                        if (cmpForm == "additional") {
                          playerView.getViewWithId("date_last_po").container.parent().css("background-color", "");
                          playerView.getViewWithId("date_last_po_k").container.parent().css("background-color", "");
                        }
                        if (cmpForm == "main") {
                          playerView.getViewWithId("date_additional").container.parent().css("background-color", "");
                          playerView.getViewWithId("date_additional_k").container.parent().css("background-color", "");
                        }
                      }
                      if (isNaN(d1)) {
                        playerView.getViewWithId(cmpForm).container.parent().css("background-color", "#FF9999");
                        playerView.getViewWithId(cmpForm+"_k").container.parent().css("background-color", "#FF9999");
                        alert("Введено некорректное значение");
                        formText.setValue('');
                      }
                    });
                    break;
                  }
                }
              }
            });
          }
        }
      });
    }
  },

  setValueChoiceFromPosition: function(posID, choice) {
    var playerView = $('div[data-asformid="container"]')[0].playerView;
    if (posID != undefined && posID != null) {
      var synergyLogin = 'Administrator';
      var synergyPass = '$ystemAdm1n';
      $.ajax({
        url: 'rest/api/positions/get_cards?positionID=' + posID,
        dataType: 'json',
        username: synergyLogin,
        password: synergyPass,
        success: function(data) {
          var tmpJson;
          var cardID=null;
          tmpJson = JSON.stringify(data);
          tmpJson = String(tmpJson).replace(/form-uuid/g, "formUuid");
          tmpJson = tmpJson.replace(/data-uuid/g, "dataUuid");
          tmpJson = JSON.parse(tmpJson);
          for (var i = 0; i < tmpJson.length; i++) {
            if (tmpJson[i].formUuid == "b355996d-caa6-4ad2-a998-d7588dad14ba") {
              cardID = tmpJson[i].dataUuid;
              break;
            }
          }
          if (cardID) {
            $.ajax({
              url: 'rest/api/asforms/data/' + cardID,
              dataType: 'json',
              username: synergyLogin,
              password: synergyPass,
              success: function(cardData) {
                for (var j = 0; j < cardData.data.length; j++) {
                  if (cardData.data[j].id == "type") {
                    if (choice.length > 0) {
                      for (var ch = 0; ch < choice.length; ch++) {
                        if (playerView.model.getModelWithId(choice[ch])) {
                          playerView.model.getModelWithId(choice[ch]).setValue(cardData.data[j].value);
                        }
                      }
                    } else {
                      console.log("не передан массив переключателей");
                    }
                    break;
                  }
                }
              }
            });
          }
        }
      });
    }
  },

  compareDates: function(cmpStart, cmpStop, resCmp, table, blockNumber) {
    var playerView = $('div[data-asformid="container"]')[0].playerView;
    var startDate = null;
    var stopDate = null;
    var resultBox = null;
    if (table == null && blockNumber == null) {
      startDate = playerView.model.getModelWithId(cmpStart);
      stopDate = playerView.model.getModelWithId(cmpStop);
      if (resCmp != null) resultBox = playerView.model.getModelWithId(resCmp);
    } else {
      startDate = playerView.model.getModelWithId(cmpStart, table.ru, blockNumber);
      stopDate = playerView.model.getModelWithId(cmpStop, table.ru, blockNumber);
      if (resCmp != null) resultBox = playerView.model.getModelWithId(resCmp, table.ru, blockNumber);
    }

    if (startDate && stopDate) {
      startDate.on("valueChange", function() {
        if (startDate.getValue() != undefined && stopDate.getValue() != undefined && stopDate.getValue() != "" && stopDate.getValue() != "____-__-__ 00:00:00") {
          var d1 = AS.FORMS.DateUtils.parseDate(startDate.getValue()).getTime();
          var d2 = AS.FORMS.DateUtils.parseDate(stopDate.getValue()).getTime();
          var dd = Math.floor((d2-d1) / 86400000);
          if (dd < 0) {
            if (table == null && blockNumber == null) {
              playerView.getViewWithId(cmpStop).container.parent().css("background-color", "#FF9999");
              playerView.getViewWithId(cmpStop+"_k").container.parent().css("background-color", "#FF9999");
            } else {
              playerView.getViewWithId(cmpStop, table.ru, blockNumber).container.parent().css("background-color", "#FF9999");
              playerView.getViewWithId(cmpStop+"_k", table.kz, blockNumber).container.parent().css("background-color", "#FF9999");
            }
            alert("Дата ПО [" + stopDate.getValue() + "] меньше даты С [" + startDate.getValue() + "]");
            if (resultBox) resultBox.setValue("");
            stopDate.setValue("");
            stopDate.asfProperty.required=true;
          } else {
            if (resultBox) resultBox.setValue(dd+"");
            if (table == null && blockNumber == null) {
              playerView.getViewWithId(cmpStop).container.parent().css("background-color", "");
              playerView.getViewWithId(cmpStop+"_k").container.parent().css("background-color", "");
            } else {
              playerView.getViewWithId(cmpStop, table.ru, blockNumber).container.parent().css("background-color", "");
              playerView.getViewWithId(cmpStop+"_k", table.kz, blockNumber).container.parent().css("background-color", "");
            }
          }
        }
      });
      stopDate.on("valueChange", function() {
        if (startDate.getValue() != undefined && stopDate.getValue() != undefined && stopDate.getValue() != "" && stopDate.getValue() != "____-__-__ 00:00:00") {
          var d1 = AS.FORMS.DateUtils.parseDate(startDate.getValue()).getTime();
          var d2 = AS.FORMS.DateUtils.parseDate(stopDate.getValue()).getTime();
          var dd = Math.floor((d2-d1) / 86400000);
          if (dd < 0) {
            if (table == null && blockNumber == null) {
              playerView.getViewWithId(cmpStop).container.parent().css("background-color", "#FF9999");
              playerView.getViewWithId(cmpStop+"_k").container.parent().css("background-color", "#FF9999");
            } else {
              playerView.getViewWithId(cmpStop, table.ru, blockNumber).container.parent().css("background-color", "#FF9999");
              playerView.getViewWithId(cmpStop+"_k", table.kz, blockNumber).container.parent().css("background-color", "#FF9999");
            }
            alert("Дата ПО [" + stopDate.getValue() + "] меньше даты С [" + startDate.getValue() + "]");
            if (resultBox) resultBox.setValue("");
            stopDate.setValue("");
            stopDate.asfProperty.required=true;
          } else {
            if (resultBox) resultBox.setValue(dd+"");
            if (table == null && blockNumber == null) {
              playerView.getViewWithId(cmpStop).container.parent().css("background-color", "");
              playerView.getViewWithId(cmpStop+"_k").container.parent().css("background-color", "");
            } else {
              playerView.getViewWithId(cmpStop, table.ru, blockNumber).container.parent().css("background-color", "");
              playerView.getViewWithId(cmpStop+"_k", table.kz, blockNumber).container.parent().css("background-color", "");
            }
          }
        }
      });
    }
  },

  getDatePlusMonth: function(d, n) {
    var yy = parseInt(d.substring(0, 4));
    var mm = parseInt(d.substring(5, 7));
    var dd = parseInt(d.substring(8, 10));
    var tmpN = n / 12 >> 0;
    if (tmpN > 0) yy = yy + tmpN;
    if ((mm + n) > 12) {
      mm = mm + (n % 12);
    } else {
      mm = mm + n;
    }
    if (mm > 12) {
      yy = yy + 1;
      mm = mm - 12;
    }
    if (mm < 10) mm = '0' + mm;
    if (dd < 10) dd = '0' + dd;
    return (yy + "-" + mm + "-" + dd);
  },

  controlUser: function(check, cmpUser) {
    var playerView = $('div[data-asformid="container"]')[0].playerView;
    var provBox = playerView.model.getModelWithId(check);
    var cmpUserRu = playerView.model.getModelWithId(cmpUser.ru);
    var cmpUserKz = playerView.model.getModelWithId(cmpUser.kz);
    if (provBox.value[0] == '0') {
      PRIKAZ_UTIL.setVisibleCmp(cmpUser, true);
    } else {
      PRIKAZ_UTIL.setVisibleCmp(cmpUser, false);
    }
    provBox.on("valueChange", function() {
      if (provBox.value[0] == '0') {
        cmpUserRu.asfProperty.required=true;
        cmpUserKz.asfProperty.required=true;
        PRIKAZ_UTIL.setVisibleCmp(cmpUser, true);
      } else {
        cmpUserRu.asfProperty.required=false;
        cmpUserKz.asfProperty.required=false;
        PRIKAZ_UTIL.setVisibleCmp(cmpUser, false);
      }
    });
  }

};

AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
    var formPrikazArr = ["09ff496a-0c58-460a-ad7e-8d1eb22c54e1", "0e65cdf8-836c-4e3f-9eab-1a21a1a9533a", "11ec6acc-2236-4b84-8d5d-32a3ea396bc4", "13ba076b-401d-414e-9709-4c2c624494e4", "18873383-7c4e-4d1c-bd16-e6dbf7ef2025", "19e7956b-36cf-4597-bee1-291990fbd664", "1d503130-a40a-42de-ae66-167a1c800b48", "2228a7a7-1248-4e1e-9899-e202e0391720", "2464bbf7-d614-43cc-8e6f-8ee572185dea", "2598d4af-f9af-47cd-ae6b-9db0d7aa4b48", "262b0f38-7e0c-4fc4-9964-28b45a9e6ad1", "26595e26-8348-4013-bfaa-c64d2e7f7f4b", "29d3760b-4709-4991-9534-02e62a104d4f", "2d843773-eaa5-4781-8801-2348c623f709", "2e57debb-225d-47ee-a325-fec1132bd982", "2ebb7a21-996a-4bac-b27f-351fafcd676a", "33b3596c-46ab-4122-a6ff-95c82eb32752", "365fc92c-29a5-4e95-a63a-4fa829801d53", "37f6c8fa-d007-4076-97e7-dee643adcf73", "3bcb4ad9-4c64-484c-a812-6cf92168fb51", "3dbd86fd-6765-4d9a-b319-e253375133ec", "44ada3cf-5b54-47ee-8f7a-43626fa494f6", "4792ea98-f283-474b-8eb2-0bc00355222a", "479af4b1-d1fc-48cf-9539-7c7b43ed13c0", "47fd9724-00b8-4fdc-958d-a06455628b4d", "50ae5606-bdf8-4a49-bc33-4b9d1d6abf07", "50fa497b-1887-4fcf-86b4-5c732e668441", "5f63ae2f-5b1b-4c5e-b671-16146462debe", "5f9d8823-9395-4b84-bd05-87b816e335d0", "65296e17-9249-49ac-896d-c61d62c21b16", "67661d4a-25dd-466f-8d45-253819f37c36", "69064b4d-ba79-4226-8f9f-354b4cc652d8", "69eeb946-6aa0-4803-a4db-943d642ac021", "6abd84e6-bcdb-4dba-bfb4-672c0024e514", "6bd363f7-864e-45bd-8160-0fb98474b8c7", "6bfe9ab1-11dc-4ad9-934d-9211957ba3a7", "6c101302-b55d-422f-82ec-2c30eb07e0e8", "6c375d25-c72f-46cb-9fef-acd0c72dee41", "6dd1e154-22b5-427d-a431-01e62d1386e7", "6fb5fc2b-9e74-4c9c-8c56-fa09b34e9146", "72614a87-dc62-453e-b4ed-9e47a8a0062c", "73853366-81d9-4d08-9f38-37a3185c5f89", "77dd31ed-daee-432e-8a05-15509355b8f6", "78c77cca-fad2-4e3e-adff-ee5f161fe625", "7aa93eb3-16c5-4f41-9aac-922d53fa2220", "7bb9fc1a-88d1-4229-b400-d10e212132e9", "833d9836-2a24-4e00-a68d-3c4173fb0644", "8bc25c71-ecd6-4ba4-8616-205562cae381", "8bf23a98-3971-47ab-977c-1db2e324e811", "8eb767b1-2dd9-48a1-a2cd-edef4a62587e", "8ef5ba24-6e4e-4210-a13b-8ee67df56790", "8f287860-0d36-46f8-88f6-c9cd15d277ec", "931612e9-c367-4377-8514-95b22f60c2b3", "95f08cb7-4a76-4e4d-8d11-a01dcea76a9c", "9924461c-9bbc-4e18-ae9f-e6e18791920e", "9f157173-a98f-48e8-b659-9f814ec62aa0", "9fee727a-b3b5-436a-abff-22fc11854e46", "a2cfdcf6-ab76-41d5-b792-1960a2ca67e4", "a8268923-b2ec-46da-809e-266e907fd72a", "a9769d44-8918-42a7-9ab3-efe2d8da4005", "b5281cd3-752e-4256-bfba-3dd440812348", "b57ac70f-83ab-4fb3-b733-aeafadf120a5", "badd4c76-1756-4ce4-9d66-e83c782543f7", "bb0dc03f-1449-4162-aa4f-f86fbe265d16", "bc61ee4d-6d6b-43ab-b3ee-8dd7ac1ac3e3", "bc8f927e-d65b-4a21-bf8d-47518ec1ef9e", "cbdb8665-5db8-4fae-9345-25a4d2f85ef0", "cddd6f0a-c282-4f9a-b061-487de76ddb4e", "cf3d27fa-ee9f-470b-8a1c-f94b1b6624cd", "d027a943-9080-4432-911a-5e2f9bd631e5", "d2d422a2-afa2-4aa1-b2b3-59648e82d555", "d3c68e82-f11d-458d-ab9f-e694829df9b2", "d8905568-a80c-446b-96ac-4ea8d237b526", "dcd088d9-6dcf-4543-a705-27afd39228ab", "de364a40-9559-441a-a3b1-94d3c095bd5e", "e5f78340-e0e0-48d7-9710-1c5c71ea598e", "e69e21db-71e0-474a-956a-07c34e665777", "e7565807-515b-4c7e-9949-968c52fd31a1", "ea603f01-d601-4620-b425-0908966b9d7d", "ec3ca4e7-576a-472f-b68e-c176e56c1507", "f0777180-27d9-4137-9011-7203e60dcc0a", "f0d9424b-060b-45f7-a959-8021d2531f2b", "f3c5cd82-aa87-451a-af76-06346e782ccd", "f8893b4b-02c6-439d-85da-bdfe8cd57257", "feca96e6-9033-4913-adfa-451c98b2a103", "814622f3-b00e-4997-93b9-103e7c4e6c3e", "04a4effc-b921-454e-a3d3-9835231a5db0", "fd81f2c9-262f-4fa6-8655-156f6b096acb", "d914cf68-d7b1-41c2-a609-833dc7861dc3", "a629a88b-e62f-43a5-80f8-0fd1af46d1c7", "afdc7d74-e084-405d-9a30-091dc4eb9718", "cc8ab9fb-dc5a-47f9-bb64-15394865b63e", "9052be89-31c7-49ea-9ff9-96374a99bc8a", "4f4f1101-534b-433a-bf62-7653886d1cca", "0660cfdb-90ea-46be-a973-bf7979bb67f6", "ddb908c4-b73f-49a7-9090-29b39bcc717f", "7fdca349-f48e-4b8c-b33d-6491eba12314", "b47be056-67d3-4bb5-912e-3160658fb65d"];
    for (var fi=0; fi < formPrikazArr.length; fi++) {
      if (model.formId == formPrikazArr[fi]) {
        // выпиливаем ченить с формы )
        var cmpVisibleArr = ["cancel", "unikalnoe_date_po"]; // массив выпиливаемых компонентов
        for (var cva=0; cva < cmpVisibleArr.length; cva++) {
          PRIKAZ_UTIL.setVisibleCmp(cmpVisibleArr[cva], false);
        }
        // скрываем разрыв страницы
        if (view.editable) {
          $('div.asf-pageBreak').hide();
        }
        // контроль
        if (model.getModelWithId("user1_proverka")) PRIKAZ_UTIL.controlUser("user1_proverka", {ru: "user_proverka", kz: "user_proverka_k"});
        if (model.getModelWithId("proverka_user1")) PRIKAZ_UTIL.controlUser("proverka_user1", {ru: "proverka_user", kz: "proverka_user_k"});

        var cmpArrP=[{ru: "start_date", kz: "start_date_k"}, {ru: "finish_date", kz: "finish_date_k"}, {ru: "user", kz: "user_k"}, {ru: "user1", kz: "user1_k"}, {ru: "user2", kz: "user2_k"}, {ru: "user3", kz: "user3_k"}, {ru: "user4", kz: "user4_k"}, {ru: "user5", kz: "user5_k"}, {ru: "user6", kz: "user6_k"}, {ru: "user7", kz: "user7_k"}, {ru: "user8", kz: "user8_k"}, {ru: "user9", kz: "user9_k"}, {ru: "user10", kz: "user10_k"}, {ru: "podpisant_user", kz: "podpisant_user_k"}, {ru: "date", kz: "date_k"}, {ru: "date1", kz: "date1_k"}, {ru: "date2", kz: "date2_k"}, {ru: "date3", kz: "date3_k"}, {ru: "date4", kz: "date4_k"}, {ru: "date5", kz: "date5_k"}, {ru: "date6", kz: "date6_k"}, {ru: "date7", kz: "date7_k"}, {ru: "date8", kz: "date8_k"}, {ru: "date9", kz: "date9_k"}, {ru: "date10", kz: "date10_k"}, {ru: "ispolnitel_user", kz: "ispolnitel_user_k"}, {ru: "ispolnitel_phone", kz: "ispolnitel_phone_k"}, {ru: "tab_num", kz: "tab_num_k"}, {ru: "td_num", kz: "td_num_k"}, {ru: "sz_num", kz: "sz_num_k"}, {ru: "td_date", kz: "td_date_k"}, {ru: "choice", kz: "choice_k"}, {ru: "choice1", kz: "choice1_k"}, {ru: "choice2", kz: "choice2_k"}, {ru: "choice3", kz: "choice3_k"}, {ru: "choice4", kz: "choice4_k"}, {ru: "choice5", kz: "choice5_k"}, {ru: "choice6", kz: "choice6_k"}, {ru: "choice7", kz: "choice7_k"}, {ru: "choice8", kz: "choice8_k"}, {ru: "choice9", kz: "choice9_k"}, {ru: "choice10", kz: "choice10_k"}, {ru: "choice11", kz: "choice11_k"}, {ru: "choice12", kz: "choice12_k"}, {ru: "user_proverka", kz: "user_proverka_k"}, {ru: "user1_proverka", kz: "user1_proverka_k"}, {ru: "days", kz: "days_k"}, {ru: "dop_position", kz: "dop_position_k"}, {ru: "dop_sog", kz: "dop_sog_k"}, {ru: "new_position", kz: "new_position_k"}, {ru: "oznak_user", kz: "oznak_user_k"}, {ru: "ozn_user", kz: "ozn_user_k"}, {ru: "ozn_choice", kz: "ozn_choice_k"}, {ru: "proverka_user", kz: "proverka_user_k"}, {ru: "proverka_user1", kz: "proverka_user1_k"}, {ru: "ruk_user", kz: "ruk_user_k"}, {ru: "number", kz: "number_k"}, {ru: "number1", kz: "number1_k"}, {ru: "number2", kz: "number2_k"}, {ru: "number3", kz: "number3_k"}, {ru: "number4", kz: "number4_k"}, {ru: "number5", kz: "number5_k"}, {ru: "number6", kz: "number6_k"}, {ru: "number7", kz: "number7_k"}, {ru: "number8", kz: "number8_k"}, {ru: "number9", kz: "number9_k"}, {ru: "number10", kz: "number10_k"}, {ru: "zapiska", kz: "zapiska_k"}, {ru: "osnovanie_user", kz: "osnovanie_user_k"}, {ru: "control_user", kz: "control_user_k"}, {ru: "oznakomlenie_date", kz: "oznakomlenie_date_k"}, {ru: "oznakomlenie_user", kz: "oznakomlenie_user_k"}, {ru: "ruk_user", kz: "ruk_user_k"}, {ru: "amount", kz: "amount_k"}, {ru: "punkt", kz: "punkt"}, {ru: "punkt1", kz: "punkt1"}, {ru: "punkt2", kz: "punkt2"}, {ru: "punkt3", kz: "punkt3"}, {ru: "punkt4", kz: "punkt4"}, {ru: "punkt5", kz: "punkt5"}, {ru: "punkt6", kz: "punkt6"}, {ru: "punkt7", kz: "punkt7"}, {ru: "punkt8", kz: "punkt8"}, {ru: "punkt9", kz: "punkt9"}, {ru: "punkt10", kz: "punkt10"}, {ru: "s_num", kz: "s_num_k"}, {ru: "otpusk_days", kz: "otpusk_days_k"}, {ru: "ispit_srok", kz: "ispit_srok_k"}, {ru: "srok", kz: "srok_k"}, {ru: "instruct_user", kz: "instruct_user_k"}, {ru: "money", kz: "money_k"}, {ru: "podpisant_position", kz: "podpisant_position_k"}, {ru: "change_position", kz: "change_position_k"}, {ru: "change_department", kz: "change_department_k"}, {ru: "kol", kz: "kol_k"}, {ru: "podpunkt", kz: "podpunkt_k"}, {ru: "start_period", kz: "start_period_k"}, {ru: "end_period", kz: "end_period_k"}, {ru: "date_main", kz: "date_main_k"}, {ru: "date_additional", kz: "date_additional_k"}, {ru: "date_last", kz: "date_last_k"}, {ru: "date_last_po", kz: "date_last_po_k"}, {ru: "main", kz: "main_k"}, {ru: "additional", kz: "additional_k"}, {ru: "z_user", kz: "z_user_k"}, {ru: "nomer", kz: "nomer_k"}, {ru: "podpisant_choice", kz: "podpisant_choice_k"}, {ru: "holiday", kz: "holiday_k"}, {ru: "izmena", kz: "izmena_k"}, {ru: "number_izmena", kz: "number_izmena_k"}, {ru: "date_izmena", kz: "date_izmena_k"}, {ru: "prikaz_izmena", kz: "prikaz_izmena_k"}, {ru: "document", kz: "document_k"}, {ru: "td_doc", kz: "td_doc_k"}, {ru: "razmer", kz: "razmer_k"}, {ru: "prilozhenie", kz: "prilozhenie_k"}, {ru: "department1", kz: "department1_k"}, {ru: "department2", kz: "department2_k"}];
        var tableArrP=[
          {id: {ru: "t", kz: "t_k"}, cmp: [{ru: "change_department", kz: "change_department_k"}, {ru: "choice", kz: "choice_k"}, {ru: "choice1", kz: "choice1_k"}, {ru: "choice2", kz: "choice2_k"},{ru: "compensation", kz: "compensation_k"}, {ru: "fifnish_date", kz: "fifnish_date_k"}, {ru: "date_finish", kz: "date_finish_k"}, {ru: "date_start", kz: "date_start_k"}, {ru: "date3", kz: "date3_k"}, {ru: "day_dop", kz: "day_dop_k"}, {ru: "day", kz: "day_k"}, {ru: "day_osnovn", kz: "day_osnovn_k"}, {ru: "department", kz: "department_k"}, {ru: "department1", kz: "department1_k"}, {ru: "finish_date", kz: "finish_date_k"}, {ru: "hours", kz: "hours_k"}, {ru: "name1", kz: "name1_k"}, {ru: "name2", kz: "name2_k"}, {ru: "position", kz: "position_k"}, {ru: "position1", kz: "position1_k"}, {ru: "start_date", kz: "start_date_k"}, {ru: "start_date2", kz: "start_date2_k"}, {ru: "tab_num", kz: "tab_num_k"}, {ru: "user", kz: "user_k"}, {ru: "user1", kz: "user1_k"}, {ru: "user3", kz: "user3_k"}, {ru: "amount", kz: "amount_k"}]},
          {id: {ru: "t1", kz: "t1_k"}, cmp: [{ru: "date", kz: "date_k"}, {ru: "date1", kz: "date1_k"}, {ru: "date2", kz: "date2_k"}, {ru: "date3", kz: "date3_k"}, {ru: "date4", kz: "date4_k"}, {ru: "date5", kz: "date5_k"}, {ru: "date6", kz: "date6_k"}, {ru: "date7", kz: "date7_k"}, {ru: "date8", kz: "date8_k"}, {ru: "date9", kz: "date9_k"}, {ru: "date10", kz: "date10_k"}, {ru: "department1", kz: "department1_k"}, {ru: "num", kz: "num_k"}, {ru: "num1", kz: "num1_k"}, {ru: "position1", kz: "position1_k"}, {ru: "user1", kz: "user1_k"}]},
          {id: {ru: "t10", kz: "t10_k"}, cmp: [{ru: "date2", kz: "date2_k"}, {ru: "user4", kz: "user4_k"}]},
          {id: {ru: "t2", kz: "t2_k"}, cmp: [{ru: "date1", kz: "date1_k"}, {ru: "date2", kz: "date2_k"}, {ru: "day", kz: "day_k"}, {ru: "day_dop", kz: "day_dop_k"}, {ru: "day_osnovn", kz: "day_osnovn_k"}, {ru: "finish_date", kz: "finish_date_k"}, {ru: "start_date", kz: "start_date_k"}]},
          {id: {ru: "t4", kz: "t4_k"}, cmp: [{ru: "date", kz: "date"}, {ru: "td_date", kz: "td_date_k"}, {ru: "td_num", kz: "td_num_k"}, {ru: "user8", kz: "user8_k"}]},
          {id: {ru: "t5", kz: "t5_k"}, cmp: [{ru: "date5", kz: "date5_k"}, {ru: "date9", kz: "date9_k"}, {ru: "user5", kz: "user5_k"}, {ru: "user9", kz: "user9_k"}]},
          {id: {ru: "t6", kz: "t6_k"}, cmp: [{ru: "date_osnovanie", kz: "date_osnovanie_k"}, {ru: "osnovanie", kz: "osnovanie_k"}, {ru: "osnovanie_user", kz: "osnovanie_user_k"}, {ru: "ispolnitel_podpisant", kz: "ispolnitel_podpisant_k"}, {ru: "document", kz: "document_k"}]},
          {id: {ru: "t7", kz: "t7_k"}, cmp: [{ru: "date2", kz: "date2_k"}, {ru: "osnovanie", kz: "osnovanie_k"}, {ru: "user3", kz: "user3_k"}]},
          {id: {ru: "t8", kz: "t8_k"}, cmp: [{ru: "doc", kz: "doc_k"}, {ru: "user8", kz: "user8_k"}]},
          {id: {ru: "tab", kz: "tab_k"}, cmp: [{ru: "document", kz: "document_k"}, {ru: "osnovanie", kz: "osnovanie_k"}, {ru: "osnovanie_user", kz: "osnovanie_user_k"}, {ru: "zapiska", kz: "zapiska_k"}]},
          {id: {ru: "table", kz: "table_k"}, cmp: [{ru: "osnovanie_agreement", kz: "osnovanie_agreement_k"}, {ru: "osnovanie_num", kz: "osnovanie_num_k"}]},
          {id: {ru: "table2_r", kz: "table2_k"}, cmp: [{ru: "change_department", kz: "change_department"}, {ru: "change_position", kz: "change_position_k"}, {ru: "choice7", kz: "choice7_k"}, {ru: "department", kz: "department_k"}, {ru: "ispit_srok", kz: "ispit_srok_k"}, {ru: "meropriyatie", kz: "meropriyatie_k"}, {ru: "position", kz: "position_k"}, {ru: "srok", kz: "srok_k"}, {ru: "tab_num", kz: "tab_num_k"}, {ru: "uchebn_zavedenie", kz: "uchebn_zavedenie_k"}, {ru: "user", kz: "user_k"}]},
          {id: {ru: "table2_r", kz: "table2_r_k"}, cmp: [{ru: "department", kz: "department_k"}, {ru: "position", kz: "position_k"}, {ru: "tab_num", kz: "tab_num_k"}, {ru: "user1", kz: "user1_k"}, {ru: "ispit_srok", kz: "ispit_srok_k"}]},
          {id: {ru: "table3_r", kz: "table3_k"},cmp: [{ru: "user3", kz: "user3_k"}, {ru: "user1", kz: "user1_k"}, {ru: "choice1", kz: "choice1_k"}, {ru: "choice2", kz: "choice2_k"}]},
          {id: {ru: "table4_r", kz: "table4_k"}, cmp: [{ru: "document", kz: "document_k"}, {ru: "choice5", kz: "choice5_k"}, {ru: "dogovor_num", kz: "dogovor_num_k"}, {ru: "ruk_user", kz: "ruk_user_k"}, {ru: "sz_num", kz: "sz_num_k"}, {ru: "sz_user", kz: "sz_user_k"}, {ru: "s_num", kz: "s_num_k"}, {ru: "td_date", kz: "td_date_k"}, {ru: "td_doc", kz: "td_doc_k"}, {ru: "td_num", kz: "td_num_k"}, {ru: "user2", kz: "user2_k"}, {ru: "zayavlenie_user", kz: "zayavlenie_user_k"}]},
          {id: {ru: "table4_r", kz: "table4_r_k"}, cmp: [{ru: "doci", kz: "doci_k"}]},
          {id: {ru: "table5_r", kz: "table5_k"}, cmp: [{ru: "date2", kz: "date2_k"}, {ru: "user3", kz: "user3_k"}, {ru: "user8", kz: "user8_k"}]},
          {id: {ru: "tt", kz: "tt_k"}, cmp: [{ru: "department", kz: "department_k"}, {ru: "oznakomlenie_date", kz: "oznakomlenie_date_k"}, {ru: "oznakomlenie_user", kz: "oznakomlenie_user_k"}, {ru: "position", kz: "position_k"}, {ru: "tab_num", kz: "tab_num_k"}, {ru: "user", kz: "user_k"}]},
          {id: {ru: "ttt", kz: "ttt_k"}, cmp: [{ru: "osnovanie_num", kz: "osnovanie_num_k"}, {ru: "oznakomlenie_date", kz: "oznakomlenie_date_k"}, {ru: "oznakomlenie_user", kz: "oznakomlenie_user_k"}]},
          {id: {ru: "tf1", kz: "tf1_k"}, cmp: [{ru: "fail", kz: "fail_k"}]},
          {id: {ru: "table6_r", kz: "table6_k"}, cmp: [{ru: "choice2", kz: "choice2_k"}, {ru: "choice8", kz: "choice8_k"}, {ru: "choice9", kz: "choice9_k"}, {ru: "day", kz: "day_k"}, {ru: "date2", kz: "date2_k"}]},
          {id: {ru: "table7_r", kz: "table7_k"}, cmp: [{ru: "sz_user2", kz: "sz_user2_k"}, {ru: "td_num2", kz: "td_num2_k"}, {ru: "td_date2", kz: "td_date2_k"}, {ru: "document2", kz: "document2_k"}]}
        ];

        model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
          console.log(model.formName + '\nformID: [' + model.formId +'] asdDataID: [' + model.asfDataId+']');
          PRIKAZ_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", "ispolnitel_user", "telephone", "ispolnitel_phone", null, null);
          PRIKAZ_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", "ispolnitel_user_k", "telephone", "ispolnitel_phone_k", null, null);
          // дин.таб.
          for (var ti=0; ti < tableArrP.length; ti++) {
            PRIKAZ_UTIL.tableOn(tableArrP[ti].id, tableArrP[ti].cmp);
          }
          // компоненты на форме
          for (var ci=0; ci < cmpArrP.length; ci++) {
            PRIKAZ_UTIL.changedValue(model, cmpArrP[ci]);
          }
        });


        //Приказ о командировании
        if (model.formId == "2d843773-eaa5-4781-8801-2348c623f709") {
          // С-ПО
          PRIKAZ_UTIL.compareDates("start_date", "finish_date", "srok", null, null);
        } //Приказ о командировании

        //Приказ о прикомандировании
        if (model.formId == "19e7956b-36cf-4597-bee1-291990fbd664") {
          // С-ПО
          var tmpTab={ru: "t", kz: "t_k"};
          PRIKAZ_UTIL.compareDates("start_date", "finish_date", null, tmpTab, 0);
          model.getModelWithId(tmpTab.ru).on('tableRowAdd', function(evt2, modelTab2, modelRow2) {
            PRIKAZ_UTIL.compareDates("start_date", "finish_date", null, tmpTab, modelRow2[0].asfProperty.tableBlockIndex);
          });
        } //Приказ о прикомандировании

        //"Приказ на обучение без командировочных расходов", "Приказ на обучение с выплатой командировочных расходов"
        if (model.formId == "29d3760b-4709-4991-9534-02e62a104d4f" || model.formId == "fd81f2c9-262f-4fa6-8655-156f6b096acb") {
          // С-ПО
          var tmpTab={ru: "table2_r", kz: "table2_k"};
          PRIKAZ_UTIL.compareDates("start_date", "finish_date", null, tmpTab, 0);
          model.getModelWithId(tmpTab.ru).on('tableRowAdd', function(evt2, modelTab2, modelRow2) {
            PRIKAZ_UTIL.compareDates("start_date", "finish_date", null, tmpTab, modelRow2[0].asfProperty.tableBlockIndex);
          });
        }

        // Приказ о приеме на работу на определенный срок
        // Приказ о перемещении
        // Приказ о предоставлении учебного отпуска
        // "Приказ о предоставлении отпуска по беременности и родам"
        // "Приказ о привлечении к работе в выходные и праздничные дни"
        if (model.formId == "bc61ee4d-6d6b-43ab-b3ee-8dd7ac1ac3e3"
        || model.formId == "2228a7a7-1248-4e1e-9899-e202e0391720"
        || model.formId == "9fee727a-b3b5-436a-abff-22fc11854e46"
        || model.formId == "1d503130-a40a-42de-ae66-167a1c800b48"
        || model.formId == "69eeb946-6aa0-4803-a4db-943d642ac021") {
          // С-ПО
          PRIKAZ_UTIL.compareDates("start_date", "finish_date", null, null, null);
        }


        // Приказ о переводе
        if (model.formId == "47fd9724-00b8-4fdc-958d-a06455628b4d") {
          // С-ПО
          PRIKAZ_UTIL.compareDates("start_date", "finish_date", null, null, null);

          // логика с таблицами
          var tab1={ru: "cmp-uvmuqe", kz: "cmp-747dz0"};
          var tab2={ru: "cmp-i0vwom", kz: "cmp-x51wwe"};
          var tab3={ru: "table4_r", kz: "table4_k"};
          var tab4={ru: "table7_r", kz: "table7_k"};
          var choiceCmp = model.getModelWithId("choice3");

          if (choiceCmp.value[0] != undefined) {
            if (choiceCmp.value[0] == "1") {
              PRIKAZ_UTIL.setVisibleCmp(tab1, false);
              PRIKAZ_UTIL.setVisibleCmp(tab2, true);
              PRIKAZ_UTIL.setVisibleCmp(tab3, true);
              PRIKAZ_UTIL.setVisibleCmp(tab4, false);
            } else {
              PRIKAZ_UTIL.setVisibleCmp(tab1, true);
              PRIKAZ_UTIL.setVisibleCmp(tab2, false);
              PRIKAZ_UTIL.setVisibleCmp(tab3, false);
              PRIKAZ_UTIL.setVisibleCmp(tab4, true);
            }
          }
          choiceCmp.on("valueChange", function() {
            if (choiceCmp.value[0] == "1") {
              PRIKAZ_UTIL.setVisibleCmp(tab1, false);
              PRIKAZ_UTIL.setVisibleCmp(tab2, true);
              PRIKAZ_UTIL.setVisibleCmp(tab3, true);
              PRIKAZ_UTIL.setVisibleCmp(tab4, false);
            } else {
              PRIKAZ_UTIL.setVisibleCmp(tab1, true);
              PRIKAZ_UTIL.setVisibleCmp(tab2, false);
              PRIKAZ_UTIL.setVisibleCmp(tab3, false);
              PRIKAZ_UTIL.setVisibleCmp(tab4, true);
            }
          });
        }

        // Приказ о награждении
        if (model.formId == "11ec6acc-2236-4b84-8d5d-32a3ea396bc4") {
          if (view.editable) {
            $('div[data-asformid="table.addRowButton.t"]').hide();
          }
        }

        // Приказ о предоставлении социального отпуска
        if (model.formId == "8bf23a98-3971-47ab-977c-1db2e324e811") {
          // С-ПО
          PRIKAZ_UTIL.compareDates("start_date", "finish_date", null, null, null);
          // логика с таблицами
          var tableChek={ru: "cmp", kz: "cmp_k"};
          var choiceCmp = model.getModelWithId("choice1");
          if (choiceCmp.value[0] == "1") {
            PRIKAZ_UTIL.setVisibleCmp(tableChek, false);
          } else {
            PRIKAZ_UTIL.setVisibleCmp(tableChek, true);
          }
          choiceCmp.on("valueChange", function() {
            if (choiceCmp.value[0] == "1") {
              PRIKAZ_UTIL.setVisibleCmp(tableChek, false);
            } else {
              PRIKAZ_UTIL.setVisibleCmp(tableChek, true);
            }
          });
        }

        // Приказ о совмещении должностей
        if (model.formId == "6fb5fc2b-9e74-4c9c-8c56-fa09b34e9146") {
          // тарифная ставка, должностной оклад
          var positionUser = model.getModelWithId("position2");
          positionUser.on("valueChange", function() {
            if (positionUser.value.length > 0) PRIKAZ_UTIL.setValueChoiceFromPosition(positionUser.value[0].elementID, ["choice10"]);
          });
          // С-ПО
          PRIKAZ_UTIL.compareDates("start_date", "finish_date", null, null, null);
        }

        // "Приказ о предоставлении оплачиваемого отпуска (с мат.)"
        // "Приказ о предоставлении оплачиваемого отпуска (без мат.)"
        if (model.formId == "4792ea98-f283-474b-8eb2-0bc00355222a" || model.formId == "7bb9fc1a-88d1-4229-b400-d10e212132e9") {
          // проверка дат осн. и доп. отпусков
          var dateOsnS=model.getModelWithId("date_main");
          var dateOsnP=model.getModelWithId("date_additional");
          var dateDopS=model.getModelWithId("date_last");
          var dateDopP=model.getModelWithId("date_last_po");
          var unikalnoeDate=model.getModelWithId("unikalnoe_date_po");
          // заполнение уникального
          dateOsnP.on("valueChange", function() {
            if (dateDopP.getValue() != undefined && dateDopP.getValue() != "" && dateDopP.getValue() != "____-__-__ 00:00:00") {
              unikalnoeDate.setValue(dateDopP.getValue());
            } else {
              unikalnoeDate.setValue(dateOsnP.getValue());
            }
          });
          dateDopP.on("valueChange", function() {
            if (dateDopP.getValue() != undefined && dateDopP.getValue() != "" && dateDopP.getValue() != "____-__-__ 00:00:00") {
              unikalnoeDate.setValue(dateDopP.getValue());
            } else if (dateOsnP.getValue() != undefined && dateOsnP.getValue() != "" && dateOsnP.getValue() != "____-__-__ 00:00:00") {
              unikalnoeDate.setValue(dateOsnP.getValue());
            }
          });
          // дата дополнительного С не больше даты основного ПО
          dateDopS.on("valueChange", function() {
            if (dateOsnP.getValue() != undefined && dateOsnP.getValue() != "" && dateOsnP.getValue() != "____-__-__ 00:00:00") {
              var tmpD1=AS.FORMS.DateUtils.parseDate(dateOsnP.getValue());
              tmpD1.setDate(tmpD1.getDate() + 1);
              var tmpD2=AS.FORMS.DateUtils.parseDate(dateDopS.getValue());
              if (dateDopS.getValue() != undefined && dateDopS.getValue() != "" && dateDopS.getValue() != "____-__-__ 00:00:00") {
                if (tmpD2.getTime() != tmpD1.getTime()) {
                  view.getViewWithId("date_last").container.parent().css("background-color", "#FF9999");
                  alert("Дата дополнительного отпуска должна быть больше даты окончания основного отпуска на 1 день.");
                  dateDopS.setValue("");
                } else {
                  view.getViewWithId("date_last").container.parent().css("background-color", "");
                }
              }
            }
          });
          // Всего календарных дней
          var vsegoOsnOtp=model.getModelWithId("main");
          var vsegoDopOtp=model.getModelWithId("additional");
          var vsegoDays=model.getModelWithId("days");
          if (vsegoOsnOtp && vsegoDopOtp && vsegoDays) {
            vsegoOsnOtp.on("valueChange", function() {
              if (vsegoOsnOtp.getValue() != undefined && vsegoDopOtp.getValue() != undefined) {
                vsegoDays.setValue(Number(vsegoOsnOtp.getValue())+Number(vsegoDopOtp.getValue())+"");
              } else if (vsegoOsnOtp.getValue() != undefined && vsegoDopOtp.getValue() == undefined) {
                vsegoDays.setValue(vsegoOsnOtp.getValue());
              } else if (vsegoOsnOtp.getValue() == undefined && vsegoDopOtp.getValue() != undefined) {
                vsegoDays.setValue(vsegoDopOtp.getValue());
              } else {
                vsegoDays.setValue("");
              }
            });
            vsegoDopOtp.on("valueChange", function() {
              if (vsegoOsnOtp.getValue() != undefined && vsegoDopOtp.getValue() != undefined) {
                vsegoDays.setValue(Number(vsegoOsnOtp.getValue())+Number(vsegoDopOtp.getValue())+"");
              } else if (vsegoOsnOtp.getValue() != undefined && vsegoDopOtp.getValue() == undefined) {
                vsegoDays.setValue(vsegoOsnOtp.getValue());
              } else if (vsegoOsnOtp.getValue() == undefined && vsegoDopOtp.getValue() != undefined) {
                vsegoDays.setValue(vsegoDopOtp.getValue());
              } else {
                vsegoDays.setValue("");
              }
            });
          }

          // С-ПО
          PRIKAZ_UTIL.compareDates("start_period", "end_period", null, null, null);
          PRIKAZ_UTIL.compareDates("date_start", "date_finish", null, null, null);
          PRIKAZ_UTIL.compareDates("date_main", "date_additional", "main", null, null);
          PRIKAZ_UTIL.compareDates("date_last", "date_last_po", "additional", null, null);
        } // "Приказ о предоставлении ежегодного оплачиваемого отпуска" (с материальной помощью), (без материальной помощи)

        // Приказ о возложении обязанностей на период временного отсутствия работника с установлением\без установления доплаты,
        // Приказ о расширении зоны обслуживания
        if (model.formId == "6bd363f7-864e-45bd-8160-0fb98474b8c7" || model.formId == "814622f3-b00e-4997-93b9-103e7c4e6c3e" || model.formId == "bc8f927e-d65b-4a21-bf8d-47518ec1ef9e") {
          // «С-по/по событию»
          var tmpCheck = model.getModelWithId("choice8");
          var tmpFin1 = {ru: "finish_date", kz: "finish_date_k"};
          var tmpFin2 = {ru: "finish_date1", kz: "finish_date1_k"};
          PRIKAZ_UTIL.setVisibleCmp(tmpFin1, false);
          PRIKAZ_UTIL.setVisibleCmp(tmpFin2, false);
          tmpCheck.on("valueChange", function() {
            if (tmpCheck.value[0] == "1") {
              PRIKAZ_UTIL.setVisibleCmp(tmpFin1, true);
              PRIKAZ_UTIL.setVisibleCmp(tmpFin2, false);
            } else {
              PRIKAZ_UTIL.setVisibleCmp(tmpFin1, false);
              PRIKAZ_UTIL.setVisibleCmp(tmpFin2, true);
            }
          });
          // С-ПО
          PRIKAZ_UTIL.compareDates("start_date", "finish_date", null, null, null);
        }

        // "Приказ о приеме на работу на период отсутствующего сотр."
        // "Приказ о приеме на работу на определенный\неопределенный срок"
        if (model.formId == "f0d9424b-060b-45f7-a959-8021d2531f2b" || model.formId == "bc61ee4d-6d6b-43ab-b3ee-8dd7ac1ac3e3" || model.formId == "d8905568-a80c-446b-96ac-4ea8d237b526") {
          // ограничение до 6 месяцев
          var dateStart = model.getModelWithId("start_date");
          var dateSrok = model.getModelWithId("srok");
          dateSrok.on("valueChange", function() {
            if (dateStart.getValue() != undefined && dateSrok.getValue() != undefined && dateSrok.getValue() != "" && dateSrok.getValue() != "____-__-__ 00:00:00") {
              if (AS.FORMS.DateUtils.parseDate(dateSrok.getValue()).getTime() > AS.FORMS.DateUtils.parseDate(PRIKAZ_UTIL.getDatePlusMonth(dateStart.getValue(), 6)).getTime()) {
                view.getViewWithId("srok").container.parent().css("background-color", "#FF9999");
                view.getViewWithId("srok_k").container.parent().css("background-color", "#FF9999");
                alert("Испытательный срок не может превышать 6 месяцев");
                dateSrok.setValue("");
              } else {
                view.getViewWithId("srok").container.parent().css("background-color", "");
                view.getViewWithId("srok_k").container.parent().css("background-color", "");
              }
            }
          });
          // должность\профессия
          var positionOznak = model.getModelWithId("position3");
          positionOznak.on("valueChange", function() {
            if (positionOznak.value.length > 0) PRIKAZ_UTIL.setValueChoiceFromPosition(positionOznak.value[0].elementID, ["choice10", "choice11", "choice12"]);
          });
        }

        // "Приказ о возложении обязанностей на период временного отсутствия работника с установлением доплаты",
        // "Приказ о расширении зоны обслуживания"
        if (model.formId == "6bd363f7-864e-45bd-8160-0fb98474b8c7" || model.formId == "bc8f927e-d65b-4a21-bf8d-47518ec1ef9e") {
          // «тарифная ставка», «должностной оклад»
          var positionUser = model.getModelWithId("position");
          positionUser.on("valueChange", function() {
            if (positionUser.value.length > 0) PRIKAZ_UTIL.setValueChoiceFromPosition(positionUser.value[0].elementID, ["choice10"]);
          });

          if (model.formId == "bc8f927e-d65b-4a21-bf8d-47518ec1ef9e") {
            model.getModelWithId("choice10").on("valueChange", function() {
              model.getModelWithId("choice5").setValue(model.getModelWithId("choice10").getValue());
            });
          }
        }

      }
    }
});
