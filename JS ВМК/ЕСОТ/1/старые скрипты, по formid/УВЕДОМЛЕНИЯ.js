var UVEDOMLENIE_UTIL = {
  changedValue: function(model, cmpObj) {
    if (model.getModelWithId(cmpObj.ru) && model.getModelWithId(cmpObj.kz)) {
      model.getModelWithId(cmpObj.ru).on("valueChange", function() {
        if (model.getModelWithId(cmpObj.kz) ) {
          model.getModelWithId(cmpObj.kz).setValue(model.getModelWithId(cmpObj.ru).getValue());
        }
        if (cmpObj.ru == "user") {
          UVEDOMLENIE_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", "user", "fact_address", "address", null, null);
        }
      });
      model.getModelWithId(cmpObj.kz).on("valueChange", function() {
        if (model.getModelWithId(cmpObj.ru) ) {
          model.getModelWithId(cmpObj.ru).setValue(model.getModelWithId(cmpObj.kz).getValue());
        }
        if (cmpObj.ru == "user_k") {
          UVEDOMLENIE_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", "user_k", "fact_address", "address_k", null, null);
        }
      });
    }
  },

  getCardUserValue: function(model, form, cmpUser, cmpParent, cmpChild, table, index) {
    // функция которая вытаскивает с карточки пользователя (form) значение определенного компонента (cmpParent) и вставляет их на форму (cmpChild)
    if (table == null) {
      if (model.getModelWithId(cmpChild)) model.getModelWithId(cmpChild).setValue('');
    } else {
      if (model.getModelWithId(cmpChild, table, index)) model.getModelWithId(cmpChild, table, index).setValue('');
    }
    var personID=null;
    if (table == null) {
      personID = model.getModelWithId(cmpUser).value[0].personID;
    } else {
      personID = model.getModelWithId(cmpUser, table, index).value[0].personID;
    }
    if (personID) {
      var synergyLogin = 'Administrator';
      var synergyPass = '$ystemAdm1n';
      $.ajax({ // получение карточек пользователя
        url: 'rest/api/personalrecord/forms/' + personID,
        dataType: 'json',
        username: synergyLogin,
        password: synergyPass,
        success: function(data) {
          var tmpJson;
          var cardID=null;
          tmpJson = JSON.stringify(data);
          tmpJson = String(tmpJson).replace(/form-uuid/g, "formUuid");
          tmpJson = tmpJson.replace(/data-uuid/g, "dataUuid");
          tmpJson = JSON.parse(tmpJson);
          // поиск нужной карточки по formID
          for (var i = 0; i < tmpJson.length; i++) {
            if (tmpJson[i].formUuid == form) {
              cardID = tmpJson[i].dataUuid; // получаем ID карточки
              break;
            }
          }
          if (cardID) { // если нужная карточка есть достаем данные
            $.ajax({
              url: 'rest/api/asforms/data/' + cardID,
              dataType: 'json',
              username: synergyLogin,
              password: synergyPass,
              success: function(cardData) {
                for (var j = 0; j < cardData.data.length; j++) {
                  if (cardData.data[j].id == cmpParent) {
                    var tmpBlok=null;
                    if (table == null) {
                      tmpBlok = model.getModelWithId(cmpChild);
                    } else {
                      tmpBlok = model.getModelWithId(cmpChild, table, index);
                    }
                    if (tmpBlok) tmpBlok.setValue(cardData.data[j].value);
                    break;
                  }
                }
              }
            });
          }
        }
      });
    }
  }

};

AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  var formUvedomArr = ["83ddf952-713b-412b-b74b-1390187383a0", "21303e61-896d-4eab-9053-322625b92096", "56a6fc2f-8f56-42de-bbb8-1103ea0fade8", "9cee46d3-760c-4e77-8ac9-04864a843ede"];
  for (var fi=0; fi < formUvedomArr.length; fi++) {
    if (model.formId == formUvedomArr[fi]) {
      var cmpArrU=[{ru: "address", kz: "address_k"}, {ru: "user", kz: "user_k"}, {ru: "position", kz: "position_k"}, {ru: "department", kz: "department_k"}, {ru: "doc_num", kz: "doc_num_k"}, {ru: "doc_date", kz: "doc_date_k"}, {ru: "choice1", kz: "choice1_k"}, {ru: "choice2", kz: "choice2_k"}, {ru: "ruk_user", kz: "ruk_user_k"}, {ru: "user2", kz: "user2_k"}, {ru: "date2", kz: "date2_k"}, {ru: "prikaz_num", kz: "prikaz_num_k"}, {ru: "date1", kz: "date1_k"}, {ru: "position2", kz: "position2_k"}, {ru: "prichina", kz: "prichina_k"}, {ru: "department2", kz: "department2_k"}];

      model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
        console.log(model.formName + '\nformID: [' + model.formId +'] asfDataID: [' + model.asfDataId+']');
        for (var ci=0; ci < cmpArrU.length; ci++) {
          UVEDOMLENIE_UTIL.changedValue(model, cmpArrU[ci]);
        }
      });

      // Уведомление об изменении условии труда
      // Уведомление о расторжении ТД в связи со снижением объема производства
      // Уведомление в связи с сокращением
      if (model.formId == "9cee46d3-760c-4e77-8ac9-04864a843ede"
      || model.formId == "83ddf952-713b-412b-b74b-1390187383a0"
      || model.formId == "56a6fc2f-8f56-42de-bbb8-1103ea0fade8") {
        // ознакомление
        PRIKAZ_UTIL.setOznakomlenieUser(view, "user", "user2");
      }

    }
  }
});
