var SLUJEB_ZAP_UTIL = {
  setVisibleCmp: function(playerView, cmp, visible) {
    if (typeof cmp == "object") {
      if (playerView.getViewWithId(cmp.ru)) playerView.getViewWithId(cmp.ru).setVisible(visible);
      if (playerView.getViewWithId(cmp.kz)) playerView.getViewWithId(cmp.kz).setVisible(visible);
    } else {
      if (playerView.getViewWithId(cmp)) playerView.getViewWithId(cmp).setVisible(visible);
    }
  },

  changedValue: function(model, cmpObj) {
    if (model.getModelWithId(cmpObj.ru) && model.getModelWithId(cmpObj.kz)) {
      model.getModelWithId(cmpObj.ru).on("valueChange", function() {
        if (cmpObj.ru == "user") {
          SLUJEB_ZAP_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", "user", "t_number", "tab_num", null, null);
        }
        if (model.getModelWithId(cmpObj.kz) ) {
          model.getModelWithId(cmpObj.kz).setValue(model.getModelWithId(cmpObj.ru).getValue());
        }
      });
      model.getModelWithId(cmpObj.kz).on("valueChange", function() {
        if (cmpObj.kz == "user_k") {
          SLUJEB_ZAP_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", "user_k", "t_number", "tab_num_k", null, null);
        }
        if (model.getModelWithId(cmpObj.ru) ) {
          model.getModelWithId(cmpObj.ru).setValue(model.getModelWithId(cmpObj.kz).getValue());
        }
      });
    }
  },

  changedValueTable: function(model, table, cmp, index) {
    if (model.getModelWithId(cmp.ru, table.ru, index) && model.getModelWithId(cmp.kz, table.kz, index)) {
      model.getModelWithId(cmp.ru, table.ru, index).on("valueChange", function() {
        if (cmp.ru == "user") {
          SLUJEB_ZAP_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", "user", "t_number", "tab_num", table.ru, index);
        }
        if (model.getModelWithId(cmp.kz, table.kz, index) ) {
          model.getModelWithId(cmp.kz, table.kz, index).setValue(model.getModelWithId(cmp.ru, table.ru, index).getValue());
        }
      });
      model.getModelWithId(cmp.kz, table.kz, index).on("valueChange", function() {
        if (cmp.kz == "user_k") {
          SLUJEB_ZAP_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", "user_k", "t_number", "tab_num_k", table.kz, index);
        }
        if (model.getModelWithId(cmp.ru, table.ru, index) ) {
          model.getModelWithId(cmp.ru, table.ru, index).setValue(model.getModelWithId(cmp.kz, table.kz, index).getValue());
        }
      });
    }
  },

  tableOn: function(playerView, tabObj, cmpObj) {
    var tmpTableRu=playerView.model.getModelWithId(tabObj.ru);
    var tmpTableKz=playerView.model.getModelWithId(tabObj.kz);
    if (tmpTableRu && tmpTableKz) {
      for (var tj=0; tj < cmpObj.length; tj++) {
        SLUJEB_ZAP_UTIL.changedValueTable(playerView.model, tabObj, cmpObj[tj], 0);
      }
      tmpTableRu.on('tableRowAdd', function(evt, modelTab, modelRow) {
        tmpTableKz.createRow();
        for (var tj=0; tj < cmpObj.length; tj++) {
          SLUJEB_ZAP_UTIL.changedValueTable(playerView.model, tabObj, cmpObj[tj], modelRow[0].asfProperty.tableBlockIndex);
        }
        SLUJEB_ZAP_UTIL.tableEnabled(playerView, tabObj.kz);
      });
    }
    if (tmpTableRu && tmpTableKz) {
      tmpTableRu.on('tableRowDelete', function(evt, rowModel, rowIndex, removedRow) {
        tmpTableKz.removeRow(rowIndex);
      });
    }
    SLUJEB_ZAP_UTIL.tableEnabled(playerView, tabObj.kz);
  },

   tableEnabled: function(view, table) {
     if (view.editable) {
       var tmpTable=view.getViewWithId(table);
       if (tmpTable) tmpTable.setEnabled(false);
     } else {
       setTimeout(SLUJEB_ZAP_UTIL.tableEnabled, 500, view, table);
     }
   },

  getCardUserValue: function(model, form, cmpUser, cmpParent, cmpChild, table, index) {
    // функция которая вытаскивает с карточки пользователя (form) значение определенного компонента (cmpParent) и вставляет их на форму (cmpChild)
    if (table == null) {
      if (model.getModelWithId(cmpChild)) model.getModelWithId(cmpChild).setValue('');
    } else {
      if (model.getModelWithId(cmpChild, table, index)) model.getModelWithId(cmpChild, table, index).setValue('');
    }
    var personID=null;
    if (table == null) {
      personID = model.getModelWithId(cmpUser).value[0].personID;
    } else {
      personID = model.getModelWithId(cmpUser, table, index).value[0].personID;
    }
    if (personID) {
      var synergyLogin = 'Administrator';
      var synergyPass = '$ystemAdm1n';
      $.ajax({
        url: 'rest/api/personalrecord/forms/' + personID,
        dataType: 'json',
        username: synergyLogin,
        password: synergyPass,
        success: function(data) {
          var tmpJson;
          var cardID=null;
          tmpJson = JSON.stringify(data);
          tmpJson = String(tmpJson).replace(/form-uuid/g, "formUuid");
          tmpJson = tmpJson.replace(/data-uuid/g, "dataUuid");
          tmpJson = JSON.parse(tmpJson);
          for (var i = 0; i < tmpJson.length; i++) {
            if (tmpJson[i].formUuid == form) {
              cardID = tmpJson[i].dataUuid;
              break;
            }
          }
          if (cardID) {
            $.ajax({
              url: 'rest/api/asforms/data/' + cardID,
              dataType: 'json',
              username: synergyLogin,
              password: synergyPass,
              success: function(cardData) {
                for (var j = 0; j < cardData.data.length; j++) {
                  if (cardData.data[j].id == cmpParent) {
                    var tmpBlok=null;
                    if (table == null) {
                      tmpBlok = model.getModelWithId(cmpChild);
                    } else {
                      tmpBlok = model.getModelWithId(cmpChild, table, index);
                    }
                    if (tmpBlok) tmpBlok.setValue(cardData.data[j].value);
                    break;
                  }
                }
              }
            });
          }
        }
      });
    }
  },

  // слушаем должность и запиливаем массив чеков от значения в карточке
  setValueChoiceFromPosition: function(playerView, posID, choice) {
    if (posID != undefined && posID != null) {
      var synergyLogin = 'Administrator';
      var synergyPass = '$ystemAdm1n';
      $.ajax({
        url: 'rest/api/positions/get_cards?positionID=' + posID,
        dataType: 'json',
        username: synergyLogin,
        password: synergyPass,
        success: function(data) {
          var tmpJson;
          var cardID=null;
          tmpJson = JSON.stringify(data);
          tmpJson = String(tmpJson).replace(/form-uuid/g, "formUuid");
          tmpJson = tmpJson.replace(/data-uuid/g, "dataUuid");
          tmpJson = JSON.parse(tmpJson);
          // поиск нужной карточки по formID
          for (var i = 0; i < tmpJson.length; i++) {
            if (tmpJson[i].formUuid == "b355996d-caa6-4ad2-a998-d7588dad14ba") {
              cardID = tmpJson[i].dataUuid;
              break;
            }
          }
          if (cardID) {
            $.ajax({
              url: 'rest/api/asforms/data/' + cardID,
              dataType: 'json',
              username: synergyLogin,
              password: synergyPass,
              success: function(cardData) {
                for (var j = 0; j < cardData.data.length; j++) {
                  if (cardData.data[j].id == "type") {
                    if (choice.length > 0) {
                      for (var ch = 0; ch < choice.length; ch++) {
                        if (playerView.model.getModelWithId(choice[ch])) {
                          playerView.model.getModelWithId(choice[ch]).setValue(cardData.data[j].value);
                        }
                      }
                    } else {
                      console.log("не передан массив переключателей");
                    }
                    break;
                  }
                }
              }
            });
          }
        }
      });
    }
  },

  compareDates: function(playerView, cmpStart, cmpStop, resCmp, table, blockNumber) {
    var startDate = null;
    var stopDate = null;
    var resultBox = null;
    if (table == null && blockNumber == null) {
      startDate = playerView.model.getModelWithId(cmpStart);
      stopDate = playerView.model.getModelWithId(cmpStop);
      if (resCmp != null) resultBox = playerView.model.getModelWithId(resCmp);
    } else {
      startDate = playerView.model.getModelWithId(cmpStart, table.ru, blockNumber);
      stopDate = playerView.model.getModelWithId(cmpStop, table.ru, blockNumber);
      if (resCmp != null) resultBox = playerView.model.getModelWithId(resCmp, table.ru, blockNumber);
    }

    if (startDate && stopDate) {
      startDate.on("valueChange", function() {
        if (AS.FORMS.DateUtils.parseDate(startDate.getValue()) !=null && AS.FORMS.DateUtils.parseDate(stopDate.getValue()) !=null ) {
          var d1 = AS.FORMS.DateUtils.parseDate(startDate.getValue()).getTime();
          var d2 = AS.FORMS.DateUtils.parseDate(stopDate.getValue()).getTime();
          var dd = Math.floor((d2-d1) / 86400000);
          if (dd < 0) {
            if (table == null && blockNumber == null) {
              playerView.getViewWithId(cmpStop).container.parent().css("background-color", "#FF9999");
              playerView.getViewWithId(cmpStop+"_k").container.parent().css("background-color", "#FF9999");
            } else {
              playerView.getViewWithId(cmpStop, table.ru, blockNumber).container.parent().css("background-color", "#FF9999");
              playerView.getViewWithId(cmpStop+"_k", table.kz, blockNumber).container.parent().css("background-color", "#FF9999");
            }
            alert("Дата ПО [" + stopDate.getValue() + "] меньше даты С [" + startDate.getValue() + "]");
            if (resultBox) resultBox.setValue("");
            stopDate.setValue("");
            stopDate.asfProperty.required=true;
          } else {
            if (resultBox) resultBox.setValue(dd+"");
            if (table == null && blockNumber == null) {
              playerView.getViewWithId(cmpStop).container.parent().css("background-color", "");
              playerView.getViewWithId(cmpStop+"_k").container.parent().css("background-color", "");
            } else {
              playerView.getViewWithId(cmpStop, table.ru, blockNumber).container.parent().css("background-color", "");
              playerView.getViewWithId(cmpStop+"_k", table.kz, blockNumber).container.parent().css("background-color", "");
            }
          }
        } else {
          if (resultBox) resultBox.setValue("");
        }
      });
      stopDate.on("valueChange", function() {
        if (AS.FORMS.DateUtils.parseDate(startDate.getValue()) !=null && AS.FORMS.DateUtils.parseDate(stopDate.getValue()) !=null ) {
          var d1 = AS.FORMS.DateUtils.parseDate(startDate.getValue()).getTime();
          var d2 = AS.FORMS.DateUtils.parseDate(stopDate.getValue()).getTime();
          var dd = Math.floor((d2-d1) / 86400000);
          if (dd < 0) {
            if (table == null && blockNumber == null) {
              playerView.getViewWithId(cmpStop).container.parent().css("background-color", "#FF9999");
              playerView.getViewWithId(cmpStop+"_k").container.parent().css("background-color", "#FF9999");
            } else {
              playerView.getViewWithId(cmpStop, table.ru, blockNumber).container.parent().css("background-color", "#FF9999");
              playerView.getViewWithId(cmpStop+"_k", table.kz, blockNumber).container.parent().css("background-color", "#FF9999");
            }
            alert("Дата ПО [" + stopDate.getValue() + "] меньше даты С [" + startDate.getValue() + "]");
            if (resultBox) resultBox.setValue("");
            stopDate.setValue("");
            stopDate.asfProperty.required=true;
          } else {
            if (resultBox) resultBox.setValue(dd+"");
            if (table == null && blockNumber == null) {
              playerView.getViewWithId(cmpStop).container.parent().css("background-color", "");
              playerView.getViewWithId(cmpStop+"_k").container.parent().css("background-color", "");
            } else {
              playerView.getViewWithId(cmpStop, table.ru, blockNumber).container.parent().css("background-color", "");
              playerView.getViewWithId(cmpStop+"_k", table.kz, blockNumber).container.parent().css("background-color", "");
            }
          }
        } else {
          if (resultBox) resultBox.setValue("");
        }
      });
    }
  }
};

AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  var formSluzhebArr = ["142bd5d8-67f2-4fda-a03a-ddc758472e6d", "1830465c-e4e4-4f4c-b146-45ebd6ef0ce4", "19fea228-f433-4077-8004-ca617589c3d0", "349dfff8-bd06-4531-bf1e-54176b9db12c", "3684deda-8016-4eb4-9f4f-7972acf97642", "3aed2795-c977-400e-945f-26fdf6d1004c", "409544ed-9cca-48f0-ab13-981d583a5ff0", "4189e683-59de-4e40-ae60-44e331d1e7ab", "4b00ab15-328b-47af-846e-b8362106b8f3", "55c1124c-adbc-44aa-8de4-a3f1fd91ab9b", "61cf7c86-913e-47e5-934f-f7df3dd529ae", "68d7ebf7-e1f3-4a08-88a3-bbb8ce0c6cbf", "6eb9482d-0188-4a26-8f5e-681834f90ef9", "6f31e847-04a0-4d9b-a09a-f734415dca34", "70eeebfe-9f14-4dac-a939-f63eaded482c", "770709a4-a0e1-4f11-ae40-3b17204f9f5b", "b5be77a0-9aa4-4d32-aa40-bf5922df7769", "bacb2f2a-ce78-4880-b038-bac77ab89cb6", "c20b202e-0108-4d97-b55e-a4ef187fa770", "c5e5fc1f-d8ff-40ef-8033-5f7288ae44c1", "c9e87e48-d252-4eb0-a5d6-4986a28f00d2", "ceb0714c-b365-4aa2-841d-35475b0821a1", "f1420cc1-5261-4606-96ff-1421346f9598", "f1e25bf5-3683-4bed-8045-4cd40b142ac7", "f2d7dbef-fdeb-4b59-acaa-168b0b091542", "d12f7da4-abb9-43d2-bd73-447a3c86b08e", "042b5e02-4034-4101-a1ed-8c507ef501de", "6578617c-f47b-44d4-a99a-0b43c1c448ea", "019d867e-a361-4e85-a469-aba6bbc0aabf"];

  for (var fi=0; fi < formSluzhebArr.length; fi++) {
    if (model.formId == formSluzhebArr[fi]) {
      var cmpArrS=[{ru: "to_user", kz: "to_user_k"}, {ru: "to_position", kz: "to_position_k"}, {ru: "copy_user", kz: "copy_user_k"}, {ru: "copy_position", kz: "copy_position_k"}, {ru: "from_user", kz: "from_user_k"}, {ru: "from_position", kz: "from_position_k"}, {ru: "doc_date", kz: "doc_date_k"}, {ru: "doc_num", kz: "doc_num_k"}, {ru: "user", kz: "user_k"}, {ru: "tab_num", kz: "tab_num_k"}, {ru: "position", kz: "position_k"}, {ru: "position2", kz: "position2_k"}, {ru: "department", kz: "department_k"}, {ru: "department1", kz: "department1_k"}, {ru: "department2", kz: "department2_k"}, {ru: "department3", kz: "department3_k"}, {ru: "start_date", kz: "start_date_k"}, {ru: "finish_date", kz: "finish_date_k"},	{ru: "choice", kz: "choice_k"}, {ru: "сhoice1", kz: "сhoice1_k"}, {ru: "choice1", kz: "choice1_k"}, {ru: "choice2", kz: "choice2_k"}, {ru: "choice3", kz: "choice3_k"}, {ru: "choice4", kz: "choice4_k"}, {ru: "choice5", kz: "choice5_k"}, {ru: "choice6", kz: "choice6_k"}, {ru: "choice7", kz: "choice7_k"}, {ru: "choice8", kz: "choice8_k"}, {ru: "choice9", kz: "choice9_k"}, {ru: "choice10", kz: "choice10_k"}, {ru: "ispolnitel_user", kz: "ispolnitel_user_k"}, {ru: "ispolnitel_podpisant", kz: "ispolnitel_podpisant_k"}, {ru: "ispolnitel_position", kz: "ispolnitel_position_k"}, {ru: "podpisant_position", kz: "podpisant_position_k"}, {ru: "podpisant_user", kz: "podpisant_user_k"}, {ru: "itogo", kz: "itogo_k"}, {ru: "change_position", kz: "change_position_k"}, {ru: "change_department", kz: "change_department_k"}, {ru: "amount", kz: "amount_k"}, {ru: "compensation", kz: "compensation_k"}, {ru: "rashod1", kz: "rashod1_k"}, {ru: "rashod3", kz: "rashod3_k"}, {ru: "kol", kz: "kol_k"}, {ru: "ruk_user", kz: "ruk_user_k"}, {ru: "srok", kz: "srok_k"}];
      var tableArrS=[{id: {ru: "t", kz: "t_k"}, cmp: [{ru: "choice1", kz: "choice1_k"}, {ru: "choice2", kz: "choice2_k"}, {ru: "compensation", kz: "compensation_k"}, {ru: "finish_date", kz: "finish_date_k"}, {ru: "position", kz: "position_k"}, {ru: "start_date", kz: "start_date_k"}, {ru: "tab_num", kz: "tab_num_k"}, {ru: "change_department", kz: "change_department_k"}, {ru: "user", kz: "user_k"}]},
      {id: {ru: "t", kz: "t1_k"}, cmp: [{ru: "department", kz: "department_k"}, {ru: "department1", kz: "department1_k"}, {ru: "position", kz: "position_k"}, {ru: "position1", kz: "position1_k"}, {ru: "prichina", kz: "prichina_k"}, {ru: "user", kz: "user_k"}]},
      {id: {ru: "t8", kz: "t8_k"}, cmp: [{ru: "doc", kz: "doc_k"}, {ru: "user8", kz: "user8_k"}]},
      {id: {ru: "table2_r", kz: "table2_k"}, cmp: [{ru: "change_department", kz: "change_department_k"}, {ru: "change_position", kz: "change_position_k"}, {ru: "department", kz: "department_k"}, {ru: "position", kz: "position_k"}, {ru: "tab_num", kz: "tab_num_k"}, {ru: "user", kz: "user_k"}]},
      {id: {ru: "table5_r", kz: "table5_k"}, cmp: [{ru: "date2", kz: "date2_k"}, {ru: "user3", kz: "user3_k"}]}];

      model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
        console.log(model.formName + '\nformID: [' + model.formId +'] asfDataID: [' + model.asfDataId+']');
        for (var ti=0; ti < tableArrS.length; ti++) {
          SLUJEB_ZAP_UTIL.tableOn(view, tableArrS[ti].id, tableArrS[ti].cmp);
        }
        for (var ci=0; ci < cmpArrS.length; ci++) {
          SLUJEB_ZAP_UTIL.changedValue(model, cmpArrS[ci]);
        }
      });
      if (view.editable) {
        for (var ti2=0; ti2 < tableArrS.length; ti2++) {
          SLUJEB_ZAP_UTIL.tableEnabled(view, tableArrS[ti2].id.kz);
        }
      }

      // логика  С-ПО
      SLUJEB_ZAP_UTIL.compareDates(view, "start_date", "finish_date", null, null, null);

      // Служебная записка на изменение графика работы
      if (model.formId == "1830465c-e4e4-4f4c-b146-45ebd6ef0ce4") {
        // логика  С-ПО
        SLUJEB_ZAP_UTIL.compareDates(view, "date_start", "date_finish", null, null, null);
      }

      // Служебная записка о командировании
      if (model.formId == "042b5e02-4034-4101-a1ed-8c507ef501de") {
        // логика  С-ПО (Срок командирования (календарных дней))
        SLUJEB_ZAP_UTIL.compareDates(view, "start_date", "finish_date", "srok", null, null);
      }

      // "Служебная записка на замещение временно  отсутствующего работника без установления доплаты";
      // "Служебная записка на замещение временно отсутствующего  работника с установлением доплаты";
      // "Служебная записка о расширении зоны"
      if (model.formId == "d12f7da4-abb9-43d2-bd73-447a3c86b08e" || model.formId == "68d7ebf7-e1f3-4a08-88a3-bbb8ce0c6cbf" || model.formId == "c5e5fc1f-d8ff-40ef-8033-5f7288ae44c1") {
        // С-по\по событию
        var tmpCheck = model.getModelWithId("choice8");
        var tmpFin1 = {ru: "finish_date", kz: "finish_date_k"};
        var tmpFin2 = {ru: "finish_date1", kz: "finish_date1_k"};
        SLUJEB_ZAP_UTIL.setVisibleCmp(view, tmpFin1, false);
        SLUJEB_ZAP_UTIL.setVisibleCmp(view, tmpFin2, false);
        tmpCheck.on("valueChange", function() {
          if (tmpCheck.value[0] == "1") {
            SLUJEB_ZAP_UTIL.setVisibleCmp(view, tmpFin1, true);
            SLUJEB_ZAP_UTIL.setVisibleCmp(view, tmpFin2, false);
            model.getModelWithId(tmpFin2.ru).setValue(null);
          } else {
            SLUJEB_ZAP_UTIL.setVisibleCmp(view, tmpFin1, false);
            SLUJEB_ZAP_UTIL.setVisibleCmp(view, tmpFin2, true);
            model.getModelWithId(tmpFin1.ru).setValue(null);
          }
        });
      }

      // "Служебная записка на замещение временно отсутствующего  работника с установлением доплаты",
      // "Служебная записка о расширении зоны",
      if (model.formId == "68d7ebf7-e1f3-4a08-88a3-bbb8ce0c6cbf") {
        // «тарифная ставка», «должностной оклад»
        var positionUser = model.getModelWithId("position");
        positionUser.on("valueChange", function() {
          if (positionUser.value.length > 0) SLUJEB_ZAP_UTIL.setValueChoiceFromPosition(view, positionUser.value[0].elementID, ["choice10"]);
        });
      }

      // "Служебная записка о расширении зоны",
      if (model.formId == "c5e5fc1f-d8ff-40ef-8033-5f7288ae44c1") {
        // «тарифная ставка», «должностной оклад»
        var positionUser = model.getModelWithId("position");
        positionUser.on("valueChange", function() {
          if (positionUser.value.length > 0) SLUJEB_ZAP_UTIL.setValueChoiceFromPosition(view, positionUser.value[0].elementID, ["choice10", "choice1"]);
        });
      }

      // Служебная записка об установлении доплаты за совмещение должностей / профессий
      if (model.formId == "6eb9482d-0188-4a26-8f5e-681834f90ef9") {
        // «тарифная ставка», «должностной оклад»
        var positionUser = model.getModelWithId("change_position");
        positionUser.on("valueChange", function() {
          if (positionUser.value.length > 0) SLUJEB_ZAP_UTIL.setValueChoiceFromPosition(view, positionUser.value[0].elementID, ["choice10"]);
        });
      }

      // Служебная записка об отзыве из трудового отпуска
      if (model.formId == "19fea228-f433-4077-8004-ca617589c3d0") {
        var tmpCheck = model.getModelWithId("choice");
        var tmpFin1 = {ru: "choice2", kz: "choice2_k"};
        if (tmpCheck.value[0] == "1") {
          SLUJEB_ZAP_UTIL.setVisibleCmp(view, tmpFin1, true);
        } else {
          SLUJEB_ZAP_UTIL.setVisibleCmp(view, tmpFin1, false);
        }
        tmpCheck.on("valueChange", function() {
          if (tmpCheck.value[0] == "1") {
            SLUJEB_ZAP_UTIL.setVisibleCmp(view, tmpFin1, true);
          } else {
            SLUJEB_ZAP_UTIL.setVisibleCmp(view, tmpFin1, false);
          }
        });
      }

    }
  }
});
