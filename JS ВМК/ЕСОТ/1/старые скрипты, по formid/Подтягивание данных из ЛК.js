AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {

  var userCards = {
    lk: { //Личная карточка (штатная расстановка)
      formID: "6e7ee767-7f9b-4f53-b7d9-04a6091d066f",
      tabNomer: "t_number",
      addres: "fact_address",
      telephone: ""
    },
    otpusk: { //Отпуска
      formID: "8ac1c13a-6676-4117-99cf-d2c49a489b2a",
      dataPriema: "day_start",
      dniBezSoderj: "days_not_pay",
      nachaloPosledOtpuska: "last_vacation_end",
      dostupnoDnei: "sum_available_vacat_days",
      ostatok: "sum_vacat_days",
      otozvano: "sum_days_return"
    }
  };

  var regArray = [
    {name: "Приказ о прекращении/расторжении трудового договора", formID: "dcd088d9-6dcf-4543-a705-27afd39228ab", tableId: "table2_r", fio: "user", tabNomer: "tab_num"},
    {name: "Служебная записка о направлении на профессиональное обучение", formID: "f1420cc1-5261-4606-96ff-1421346f9598", tableId: "table2_r", fio: "user", tabNomer: "tab_num"},
    {name: "Приказ о направлении на профессиональное обучение", formID: "29d3760b-4709-4991-9534-02e62a104d4f", tableId: "table2_r", fio: "user", tabNomer: "tab_num"},
    {name: "Приказ о направлении на профессиональное обучение c выплатой командировочных расходов", formID: "72614a87-dc62-453e-b4ed-9e47a8a0062c", tableId: "table2_r", fio: "user", tabNomer: "tab_num"},
    {name: "Служебная записка на перевод", formID: "6578617c-f47b-44d4-a99a-0b43c1c448ea", tableId: "table2_r", fio: "user", tabNomer: "tab_num"},
    {name: "Приказ о переводе", formID: "47fd9724-00b8-4fdc-958d-a06455628b4d", tableId: "table2_r", fio: "user", tabNomer: "tab_num"},
    {name: "Служебная записка о перемещении", formID: "c20b202e-0108-4d97-b55e-a4ef187fa770", tableId: "t", fio: "user", tabNomer: "tab_num"},
    {name: "Приказ о перемещении", formID: "2228a7a7-1248-4e1e-9899-e202e0391720", tableId: "t", fio: "user", tabNomer: "tab_num"},
    {name: "Служебная записка о замещении", formID: "68d7ebf7-e1f3-4a08-88a3-bbb8ce0c6cbf", tableId: "t", fio: "user", tabNomer: "tab_num"},
    {name: "Приказ о замещении", formID: "6bd363f7-864e-45bd-8160-0fb98474b8c7", tableId: "t", fio: "user", tabNomer: "tab_num"},
    {name: "Служебная записка о расширении зоны", formID: "c5e5fc1f-d8ff-40ef-8033-5f7288ae44c1", tableId: "t", fio: "user", tabNomer: "tab_num"},
    {name: "Приказ о расширении зоны обслуживания", formID: "bc8f927e-d65b-4a21-bf8d-47518ec1ef9e", tableId: "t", fio: "user", tabNomer: "tab_num"},
    {name: "Служебная записка о совмещении", formID: "6eb9482d-0188-4a26-8f5e-681834f90ef9", tableId: "t", fio: "user", tabNomer: "tab_num"},
    {name: "Приказ о совмещении должностей", formID: "6fb5fc2b-9e74-4c9c-8c56-fa09b34e9146", tableId: "t", fio: "user", tabNomer: "tab_num"},
    {name: "Служебная записка о прикомандировании", formID: "019d867e-a361-4e85-a469-aba6bbc0aabf", tableId: "t", fio: "user", tabNomer: "tab_num"},
    {name: "Приказ о прикомандировании", formID: "19e7956b-36cf-4597-bee1-291990fbd664", tableId: "t", fio: "user", tabNomer: "tab_num"},
    {name: "Приказ о выходе работника на работу до истечения срока отпуска по уходу за ребенком", formID: "78c77cca-fad2-4e3e-adff-ee5f161fe625", tableId: "t", fio: "user", tabNomer: "tab_num"},
    {name: "Приказ о предоставлении отпуска по беременности и родам", formID: "1d503130-a40a-42de-ae66-167a1c800b48", tableId: "table2_r", fio: "user", tabNomer: "tab_num"},
    {name: "Приказ о предоставлении отпуска без сохранения заработной платы", formID: "2598d4af-f9af-47cd-ae6b-9db0d7aa4b48", tableId: "table2_r", fio: "user", tabNomer: "tab_num"},
    {name: "Приказ о предоставлении учебного отпуска", formID: "9fee727a-b3b5-436a-abff-22fc11854e46", tableId: "table2_r", fio: "user", tabNomer: "tab_num"},
    {name: "Приказ о предоставлении социального отпуска", formID: "8bf23a98-3971-47ab-977c-1db2e324e811", tableId: "table2_r", fio: "user", tabNomer: "tab_num"},
    {name: "Служебная записка об отзыве из трудового отпуска", formID: "19fea228-f433-4077-8004-ca617589c3d0", tableId: "t", fio: "user", tabNomer: "tab_num"},
    {name: "Приказ об отзыве из трудового отпуска", formID: "6abd84e6-bcdb-4dba-bfb4-672c0024e514", tableId: "t", fio: "user", tabNomer: "tab_num"},
    {name: "Приказ об отзыве с выплатой компенсации", formID: "e69e21db-71e0-474a-956a-07c34e665777", tableId: "t", fio: "user", tabNomer: "tab_num"},
    {name: "Служебная записка о командировании", formID: "042b5e02-4034-4101-a1ed-8c507ef501de", tableId: "t", fio: "user", tabNomer: "tab_num"},
    {name: "Приказ о командировании", formID: "2d843773-eaa5-4781-8801-2348c623f709", tableId: "t", fio: "user", tabNomer: "tab_num"},
    {name: "Приказ о привлечении к работе в выходные и праздничные дни", formID: "69eeb946-6aa0-4803-a4db-943d642ac021", tableId: "t", fio: "user", tabNomer: "tab_num"},
    {name: "Служебная записка о привлечении к работе в выходные/праздничные дни", formID: "bacb2f2a-ce78-4880-b038-bac77ab89cb6", tableId: "t", fio: "user", tabNomer: "tab_num"},
    {name: "Служебная записка о привлечении к работе в сверхурочное время", formID: "3aed2795-c977-400e-945f-26fdf6d1004c", tableId: "t", fio: "user", tabNomer: "tab_num"},
    {name: "Приказ о привлечении к сверхурочной работе", formID: "6c101302-b55d-422f-82ec-2c30eb07e0e8", tableId: "t", fio: "user", tabNomer: "tab_num"},
    {name: "Приказ о наложении дисциплинарного взыскания", formID: "d3c68e82-f11d-458d-ab9f-e694829df9b2", tableId: "t", fio: "user", tabNomer: "tab_num"},
    {name: "Служебная записка о награждении", formID: "409544ed-9cca-48f0-ab13-981d583a5ff0", tableId: "t", fio: "user", tabNomer: "tab_num"},
    {name: "Приказ о награждении к государственному/профессиональному празднику", formID: "11ec6acc-2236-4b84-8d5d-32a3ea396bc4", tableId: "tt", fio: "user", tabNomer: "tab_num"},
    {name: "Приказ о приеме на работу на определенный срок", formID: "bc61ee4d-6d6b-43ab-b3ee-8dd7ac1ac3e3", tableId: "table2_r", fio: "user", tabNomer: "tab_num"},
    {name: "Приказ о приеме на работу на неопределенный срок", formID: "f0d9424b-060b-45f7-a959-8021d2531f2b", tableId: "table2_r", fio: "user", tabNomer: "tab_num"},
    {name: "Приказ о приеме на работу на период отсутствующего сотрудника и др.причины", formID: "6dd1e154-22b5-427d-a431-01e62d1386e7", tableId: "table2_r", fio: "user", tabNomer: "tab_num"},
    {name: "Справка о сданных и несданных имуществ", formID: "7a06a2e7-00cc-425b-9915-5fb569c522cf", tableId: "cmp-lekdkr", fio: "user", tabNomer: "tab_num"}
  ];

});
