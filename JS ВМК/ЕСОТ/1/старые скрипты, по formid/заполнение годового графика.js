AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  if (model.formId == '87c786ff-5c9c-4593-8499-5ab90c770f53') {
    model.getModelWithId('table').on('tableRowAdd', function() {
      var tmpRow = view.getViewWithId('table').getRowsCount() / 12 >> 0;
      for (var j = 1; j < tmpRow + 1; j++) {
        var vahta = model.getModelWithId('watch', 'table', j);
        vahta.on(AS.FORMS.EVENT_TYPE.valueChange, function() {
          changedValue(model, '_watch', 'table', vahta.value[0], j - 1);
        });
        var smena = model.getModelWithId('change', 'table', j);
        smena.on(AS.FORMS.EVENT_TYPE.valueChange, function() {
          changedValue(model, '_change', 'table', smena.value[0], j - 1);
        });
        var link = model.getModelWithId('link', 'table', j);
        link.on(AS.FORMS.EVENT_TYPE.valueChange, function() {
          setGraficDay(model, 'table', j - 1);
        });
        var dayNumber = model.getModelWithId('dayNumber', 'table', j);
        dayNumber.on(AS.FORMS.EVENT_TYPE.valueChange, function() {
          setGraficDay(model, 'table', j - 1);
        });
      }
    });
  }
});


function getColDay(yy, mm) {
  return new Date(yy, mm, 0).getDate();
}
function changedValue(model, cmp, table, value, index) {
  for (var i=1; i<13; i++) {
    model.getModelWithId(i+cmp, table, index).setValue(value);
  }
}
function setGraficDay(model, table, index) {
  var synergyLogin = 'Administrator';
  var synergyPass = '$ystemAdm1n';
  var year = model.getModelWithId('year').value[0];
  var tmpDocId = model.getModelWithId('link', table, index).value;
  var tmpVahta=model.getModelWithId('watch', table, index).value[0];
  var tmpSmena=model.getModelWithId('change', table, index).value[0];
  var tmpDay=Number(model.getModelWithId('dayNumber', table, index).value[0])-1;

  for (var mi=1; mi<13; mi++) { // месяц
    for (var di=1; di < 32; di++) { // день
      model.getModelWithId(mi+'_day_'+di, table, index).setValue('');
    }
  }

  //console.log('вахта: ' + tmpVahta + ' смена: ' + tmpSmena + ' день: '+ tmpDay);
  if (tmpDocId) {
    $.ajax({
      url: "rest/api/docflow/doc/document_info?documentID=" + tmpDocId,
      dataType: 'json',
      username: synergyLogin,
      password: synergyPass,
      success: function(docData) {
        if (docData.asfDataID) {
          $.ajax({
            url: "rest/api/asforms/data/" + docData.asfDataID,
            dataType: 'json',
            username: synergyLogin,
            password: synergyPass,
            success: function(asfData) {
              asfData=asfData.data;
              var grafArr = [];
              for (dd = 0; dd < asfData.length; dd++) {
                if (asfData[dd].id == 'table') {
                  asfData = asfData[dd].data;
                  for (dd2 = 0; dd2 < asfData.length; dd2++) {
                    var tmp = asfData[dd2].id.substring(asfData[dd2].id.indexOf('-'));
                    if (asfData[dd2].id == ('hour' + tmp)) {
                      grafArr.push(asfData[dd2].value);
                    }
                  }
                  break;
                }
              }
              // заполнение графика
              if(grafArr.length > 0) {
                for (var mi=1; mi<13; mi++) { // месяц
                  for (var di=1; di < getColDay(fullYear(year), mi)+1; di++) { // день
                    model.getModelWithId(mi+'_day_'+di, table, index).setValue(grafArr[tmpDay]);
                    tmpDay++;
                    if (tmpDay==grafArr.length) tmpDay=0;
                  }
                }
              } //if(grafArr.length > 0)
            }
          });
        } // if (docData.asfDataID)
      }
    });
  }
}
function fullYear(year) {
  var tmp = Number(year);
  switch (tmp) {
    case 1:
      return '2015';
      break;
    case 2:
      return '2016';
      break;
    case 3:
      return '2017';
      break;
  }
}
