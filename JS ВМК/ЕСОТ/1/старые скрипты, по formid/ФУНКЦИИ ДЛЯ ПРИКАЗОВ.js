var PRIKAZ_UTIL = {

  setVisibleCmp: function(playerView, cmp, visible) {
    if (typeof cmp == "object") {
      if (playerView.getViewWithId(cmp.ru)) playerView.getViewWithId(cmp.ru).setVisible(visible);
      if (playerView.getViewWithId(cmp.kz)) playerView.getViewWithId(cmp.kz).setVisible(visible);
    } else {
      if (playerView.getViewWithId(cmp)) playerView.getViewWithId(cmp).setVisible(visible);
    }
  },

  changedValue: function(model, cmpObj) {
    if (model.getModelWithId(cmpObj.ru) && model.getModelWithId(cmpObj.kz)) {
      var userCmpArr=[{ru: "user", kz: "user_k"}];
      model.getModelWithId(cmpObj.ru).on("valueChange", function() {
        if (model.getModelWithId(cmpObj.kz) ) {
          model.getModelWithId(cmpObj.kz).setValue(model.getModelWithId(cmpObj.ru).getValue());
        }
        for (var ri=0; ri < userCmpArr.length; ri++) {
          if (cmpObj.ru == userCmpArr[ri].ru) {
            PRIKAZ_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", userCmpArr[ri].ru, "t_number", "tab_num", null, null);
            PRIKAZ_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", userCmpArr[ri].ru, "fact_address", "address", null, null);
            PRIKAZ_UTIL.getCardUserValue(model, "8ac1c13a-6676-4117-99cf-d2c49a489b2a", userCmpArr[ri].ru, "sum_available_vacat_days", "days_available", null, null);
            PRIKAZ_UTIL.getCardUserValue(model, "8ac1c13a-6676-4117-99cf-d2c49a489b2a", userCmpArr[ri].ru, "sum_vacat_days", "remainder", null, null);
          }
        }
        if (cmpObj.ru == "ispolnitel_user") {
          PRIKAZ_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", "ispolnitel_user", "telephone", "ispolnitel_phone", null, null);
        }
      });
      model.getModelWithId(cmpObj.kz).on("valueChange", function() {
        if (model.getModelWithId(cmpObj.ru) ) {
          model.getModelWithId(cmpObj.ru).setValue(model.getModelWithId(cmpObj.kz).getValue());
        }
        for (var ki=0; ki < userCmpArr.length; ki++) {
          if (cmpObj.kz == userCmpArr[ki].kz) {
            PRIKAZ_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", userCmpArr[ki].kz, "t_number", "tab_num_k", null, null);
            PRIKAZ_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", userCmpArr[ki].kz, "fact_address", "address_k", null, null);
            PRIKAZ_UTIL.getCardUserValue(model, "8ac1c13a-6676-4117-99cf-d2c49a489b2a", userCmpArr[ki].kz, "sum_available_vacat_days", "days_available_k", null, null);
            PRIKAZ_UTIL.getCardUserValue(model, "8ac1c13a-6676-4117-99cf-d2c49a489b2a", userCmpArr[ki].kz, "sum_vacat_days", "remainder_k", null, null);
          }
        }
        if (cmpObj.ru == "ispolnitel_user") {
          PRIKAZ_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", "ispolnitel_user_k", "telephone", "ispolnitel_phone_k", null, null);
        }
      });
    }
  },

  changedValueTable: function(model, table, cmp, index) {
    if (model.getModelWithId(cmp.ru, table.ru, index) && model.getModelWithId(cmp.kz, table.kz, index)) {
      var userCmpArr=[{ru: "user", kz: "user_k"}];
      model.getModelWithId(cmp.ru, table.ru, index).on("valueChange", function() {
        for (var ri=0; ri < userCmpArr.length; ri++) {
          if (cmp.ru == userCmpArr[ri].ru) {
            PRIKAZ_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", userCmpArr[ri].ru, "t_number", "tab_num", table.ru, index);
            PRIKAZ_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", userCmpArr[ri].ru, "fact_address", "address", table.ru, index);
            PRIKAZ_UTIL.getCardUserValue(model, "8ac1c13a-6676-4117-99cf-d2c49a489b2a", userCmpArr[ri].ru, "sum_available_vacat_days", "days_available", table.ru, index);
            PRIKAZ_UTIL.getCardUserValue(model, "8ac1c13a-6676-4117-99cf-d2c49a489b2a", userCmpArr[ri].ru, "sum_vacat_days", "remainder", table.ru, index);
          }
        }
        if (model.getModelWithId(cmp.kz, table.kz, index) ) {
          model.getModelWithId(cmp.kz, table.kz, index).setValue(model.getModelWithId(cmp.ru, table.ru, index).getValue());
        }
      });
      model.getModelWithId(cmp.kz, table.kz, index).on("valueChange", function() {
        for (var ki=0; ki < userCmpArr.length; ki++) {
          if (cmp.kz == userCmpArr[ki].kz) {
            PRIKAZ_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", userCmpArr[ki].kz, "t_number", "tab_num_k", table.kz, index);
            PRIKAZ_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", userCmpArr[ki].kz, "fact_address", "address_k", table.kz, index);
            PRIKAZ_UTIL.getCardUserValue(model, "8ac1c13a-6676-4117-99cf-d2c49a489b2a", userCmpArr[ki].kz, "sum_available_vacat_days", "days_available_k", table.kz, index);
            PRIKAZ_UTIL.getCardUserValue(model, "8ac1c13a-6676-4117-99cf-d2c49a489b2a", userCmpArr[ki].kz, "sum_vacat_days", "remainder_k", table.kz, index);
          }
        }
        if (model.getModelWithId(cmp.ru, table.ru, index) ) {
          model.getModelWithId(cmp.ru, table.ru, index).setValue(model.getModelWithId(cmp.kz, table.kz, index).getValue());
        }
      });
    }
  },

  tableOn: function(playerView, tabObj, cmpObj) {
    var tmpTableRu=playerView.model.getModelWithId(tabObj.ru);
    var tmpTableKz=playerView.model.getModelWithId(tabObj.kz);
    if (tmpTableRu && tmpTableKz) {
      for (var tj=0; tj < cmpObj.length; tj++) {
        PRIKAZ_UTIL.changedValueTable(playerView.model, tabObj, cmpObj[tj], 0);
      }
      tmpTableRu.on('tableRowAdd', function(evt, modelTab, modelRow) {
        tmpTableKz.createRow();
        for (var tj=0; tj < cmpObj.length; tj++) {
          PRIKAZ_UTIL.changedValueTable(playerView.model, tabObj, cmpObj[tj], modelRow[0].asfProperty.tableBlockIndex);
        }
        PRIKAZ_UTIL.tableEnabled(playerView, tabObj.kz);
      });
    }
    if (tmpTableRu && tmpTableKz) {
      tmpTableRu.on('tableRowDelete', function(evt, rowModel, rowIndex, removedRow) {
        tmpTableKz.removeRow(rowIndex);
      });
    }
    PRIKAZ_UTIL.tableEnabled(playerView, tabObj.kz);
  },

   tableEnabled: function(playerView, table) {
     if (playerView.editable) {
       var tmpTable=playerView.getViewWithId(table);
       if (tmpTable) tmpTable.setEnabled(false);
     } else {
       setTimeout(PRIKAZ_UTIL.tableEnabled, 500, playerView, table);
     }
   },

  getCardUserValue: function(model, form, cmpUser, cmpParent, cmpChild, table, index) {
    // функция которая вытаскивает с карточки пользователя (form) значение определенного компонента (cmpParent) и вставляет их на форму (cmpChild)
    if (table == null) {
      if (model.getModelWithId(cmpChild)) model.getModelWithId(cmpChild).setValue('');
    } else {
      if (model.getModelWithId(cmpChild, table, index)) model.getModelWithId(cmpChild, table, index).setValue('');
    }
    var personID=null;
    if (table == null) {
      if (model.getModelWithId(cmpUser).value.length > 0 ) personID = model.getModelWithId(cmpUser).value[0].personID;
    } else {
      if (model.getModelWithId(cmpUser, table, index).value.length > 0 ) personID = model.getModelWithId(cmpUser, table, index).value[0].personID;
    }
    if (personID) {
      var synergyLogin = 'Administrator';
      var synergyPass = '$ystemAdm1n';
      $.ajax({
        url: 'rest/api/personalrecord/forms/' + personID,
        dataType: 'json',
        username: synergyLogin,
        password: synergyPass,
        success: function(data) {
          var tmpJson;
          var cardID=null;
          tmpJson = JSON.stringify(data);
          tmpJson = String(tmpJson).replace(/form-uuid/g, "formUuid");
          tmpJson = tmpJson.replace(/data-uuid/g, "dataUuid");
          tmpJson = JSON.parse(tmpJson);
          for (var i = 0; i < tmpJson.length; i++) {
            if (tmpJson[i].formUuid == form) {
              cardID = tmpJson[i].dataUuid;
              break;
            }
          }
          if (cardID) {
            $.ajax({
              url: 'rest/api/asforms/data/' + cardID,
              dataType: 'json',
              username: synergyLogin,
              password: synergyPass,
              success: function(cardData) {
                for (var j = 0; j < cardData.data.length; j++) {
                  if (cardData.data[j].id == cmpParent) {
                    var tmpBlok=null;
                    if (table == null) {
                      tmpBlok = model.getModelWithId(cmpChild);
                    } else {
                      tmpBlok = model.getModelWithId(cmpChild, table, index);
                    }
                    if (tmpBlok) tmpBlok.setValue(cardData.data[j].value);
                    break;
                  }
                }
              }
            });
          }
        }
      });
    }
  },

  setValueChoiceFromPosition: function(playerView, posID, choice) {
    if (posID != undefined && posID != null) {
      var synergyLogin = 'Administrator';
      var synergyPass = '$ystemAdm1n';
      $.ajax({
        url: 'rest/api/positions/get_cards?positionID=' + posID,
        dataType: 'json',
        username: synergyLogin,
        password: synergyPass,
        success: function(data) {
          var tmpJson;
          var cardID=null;
          tmpJson = JSON.stringify(data);
          tmpJson = String(tmpJson).replace(/form-uuid/g, "formUuid");
          tmpJson = tmpJson.replace(/data-uuid/g, "dataUuid");
          tmpJson = JSON.parse(tmpJson);
          for (var i = 0; i < tmpJson.length; i++) {
            if (tmpJson[i].formUuid == "b355996d-caa6-4ad2-a998-d7588dad14ba") {
              cardID = tmpJson[i].dataUuid;
              break;
            }
          }
          if (cardID) {
            $.ajax({
              url: 'rest/api/asforms/data/' + cardID,
              dataType: 'json',
              username: synergyLogin,
              password: synergyPass,
              success: function(cardData) {
                for (var j = 0; j < cardData.data.length; j++) {
                  if (cardData.data[j].id == "type") {
                    if (choice.length > 0) {
                      for (var ch = 0; ch < choice.length; ch++) {
                        if (playerView.model.getModelWithId(choice[ch])) {
                          playerView.model.getModelWithId(choice[ch]).setValue(cardData.data[j].value);
                        }
                      }
                    } else {
                      console.log("не передан массив переключателей");
                    }
                    break;
                  }
                }
              }
            });
          }
        }
      });
    }
  },

  compareDates: function(playerView, cmpStart, cmpStop, resCmp, table, blockNumber) {
    var startDate = null;
    var stopDate = null;
    var resultBox = null;
    if (table == null && blockNumber == null) {
      startDate = playerView.model.getModelWithId(cmpStart);
      stopDate = playerView.model.getModelWithId(cmpStop);
      if (resCmp != null) resultBox = playerView.model.getModelWithId(resCmp);
    } else {
      startDate = playerView.model.getModelWithId(cmpStart, table.ru, blockNumber);
      stopDate = playerView.model.getModelWithId(cmpStop, table.ru, blockNumber);
      if (resCmp != null) resultBox = playerView.model.getModelWithId(resCmp, table.ru, blockNumber);
    }

    if (startDate && stopDate) {
      startDate.on("valueChange", function() {
        if (AS.FORMS.DateUtils.parseDate(startDate.getValue()) !=null && AS.FORMS.DateUtils.parseDate(stopDate.getValue()) !=null ) {
          var d1 = AS.FORMS.DateUtils.parseDate(startDate.getValue()).getTime();
          var d2 = AS.FORMS.DateUtils.parseDate(stopDate.getValue()).getTime();
          var dd = Math.floor((d2-d1) / 86400000);
          if (dd < 0) {
            if (table == null && blockNumber == null) {
              playerView.getViewWithId(cmpStop).container.parent().css("background-color", "#FF9999");
              playerView.getViewWithId(cmpStop+"_k").container.parent().css("background-color", "#FF9999");
            } else {
              playerView.getViewWithId(cmpStop, table.ru, blockNumber).container.parent().css("background-color", "#FF9999");
              playerView.getViewWithId(cmpStop+"_k", table.kz, blockNumber).container.parent().css("background-color", "#FF9999");
            }
            alert("Дата ПО [" + stopDate.getValue() + "] меньше даты С [" + startDate.getValue() + "]");
            if (resultBox) resultBox.setValue("");
            stopDate.setValue("");
            stopDate.asfProperty.required=true;
          } else {
            if (resultBox) resultBox.setValue(dd+"");
            if (table == null && blockNumber == null) {
              playerView.getViewWithId(cmpStop).container.parent().css("background-color", "");
              playerView.getViewWithId(cmpStop+"_k").container.parent().css("background-color", "");
            } else {
              playerView.getViewWithId(cmpStop, table.ru, blockNumber).container.parent().css("background-color", "");
              playerView.getViewWithId(cmpStop+"_k", table.kz, blockNumber).container.parent().css("background-color", "");
            }
          }
        } else {
          if (resultBox) resultBox.setValue("");
        }
      });
      stopDate.on("valueChange", function() {
        if (AS.FORMS.DateUtils.parseDate(startDate.getValue()) !=null && AS.FORMS.DateUtils.parseDate(stopDate.getValue()) !=null ) {
          var d1 = AS.FORMS.DateUtils.parseDate(startDate.getValue()).getTime();
          var d2 = AS.FORMS.DateUtils.parseDate(stopDate.getValue()).getTime();
          var dd = Math.floor((d2-d1) / 86400000);
          if (dd < 0) {
            if (table == null && blockNumber == null) {
              playerView.getViewWithId(cmpStop).container.parent().css("background-color", "#FF9999");
              playerView.getViewWithId(cmpStop+"_k").container.parent().css("background-color", "#FF9999");
            } else {
              playerView.getViewWithId(cmpStop, table.ru, blockNumber).container.parent().css("background-color", "#FF9999");
              playerView.getViewWithId(cmpStop+"_k", table.kz, blockNumber).container.parent().css("background-color", "#FF9999");
            }
            alert("Дата ПО [" + stopDate.getValue() + "] меньше даты С [" + startDate.getValue() + "]");
            if (resultBox) resultBox.setValue("");
            stopDate.setValue("");
            stopDate.asfProperty.required=true;
          } else {
            if (resultBox) resultBox.setValue(dd+"");
            if (table == null && blockNumber == null) {
              playerView.getViewWithId(cmpStop).container.parent().css("background-color", "");
              playerView.getViewWithId(cmpStop+"_k").container.parent().css("background-color", "");
            } else {
              playerView.getViewWithId(cmpStop, table.ru, blockNumber).container.parent().css("background-color", "");
              playerView.getViewWithId(cmpStop+"_k", table.kz, blockNumber).container.parent().css("background-color", "");
            }
          }
        } else {
          if (resultBox) resultBox.setValue("");
        }
      });
    }
  },

  getDatePlusMonth: function(d, n) {
    var yy = parseInt(d.substring(0, 4));
    var mm = parseInt(d.substring(5, 7));
    var dd = parseInt(d.substring(8, 10));
    var tmpN = n / 12 >> 0;
    if (tmpN > 0) yy = yy + tmpN;
    if ((mm + n) > 12) {
      mm = mm + (n % 12);
    } else {
      mm = mm + n;
    }
    if (mm > 12) {
      yy = yy + 1;
      mm = mm - 12;
    }
    if (mm < 10) mm = '0' + mm;
    if (dd < 10) dd = '0' + dd;
    return (yy + "-" + mm + "-" + dd);
  },

  controlUser: function(playerView, check, cmpUser, cmpPos) {
    var provBox = playerView.model.getModelWithId(check);
    var cmpUserRu = playerView.model.getModelWithId(cmpUser.ru);
    var cmpUserKz = playerView.model.getModelWithId(cmpUser.kz);
    if (provBox && provBox.value[0] == '0') {
      PRIKAZ_UTIL.setVisibleCmp(playerView, cmpUser, true);
      if (playerView.model.getModelWithId(cmpPos.ru)) PRIKAZ_UTIL.setVisibleCmp(playerView, cmpPos, true);
    } else if (provBox) {
      PRIKAZ_UTIL.setVisibleCmp(playerView, cmpUser, false);
      if (playerView.model.getModelWithId(cmpPos.ru)) PRIKAZ_UTIL.setVisibleCmp(playerView, cmpPos, false);
      if (cmpUserRu) cmpUserRu.setValue('');
    }
    if (provBox) {
      provBox.on("valueChange", function() {
        if (provBox.value[0] == '0') {
          cmpUserRu.asfProperty.required=true;
          cmpUserKz.asfProperty.required=true;
          PRIKAZ_UTIL.setVisibleCmp(playerView, cmpUser, true);
          if (playerView.model.getModelWithId(cmpPos.ru)) PRIKAZ_UTIL.setVisibleCmp(playerView, cmpPos, true);
        } else {
          cmpUserRu.asfProperty.required=false;
          cmpUserKz.asfProperty.required=false;
          PRIKAZ_UTIL.setVisibleCmp(playerView, cmpUser, false);
          if (playerView.model.getModelWithId(cmpPos.ru)) PRIKAZ_UTIL.setVisibleCmp(playerView, cmpPos, false);
          if (cmpUserRu) cmpUserRu.setValue('');
        }
      });
    }
  },

  setOznakomlenieUser: function(playerView, u1, u2, t1, t2) {
    if (t1 == undefined && t2 == undefined) {
      var userBox = null;
      userBox = playerView.model.getModelWithId(u1);
      if (userBox) {
        userBox.on("valueChange", function() {
          var userBox2 = null;
          userBox2 = playerView.model.getModelWithId(u2);
          if (userBox2) userBox2.setValue(userBox.getValue());
        });
      }
    } else {
      var table1 = playerView.model.getModelWithId(t1);
      var table2 = playerView.model.getModelWithId(t2);
      var usTmp1 = playerView.model.getModelWithId(u1, t1, 0);
      var usTmp2 = playerView.model.getModelWithId(u2, t2, 0);
      if (usTmp1 && usTmp2) {
        usTmp1.on("valueChange", function() {
          usTmp2.setValue(usTmp1.getValue());
        });
      }
      playerView.model.getModelWithId(u2, t2, 0).setValue(playerView.model.getModelWithId(u1, t1, 0).getValue());
      if (table1 && table2) {
        table1.on('tableRowAdd', function(evt, modelTab, modelRow) {
          table2.createRow();
          var userTmp1 = playerView.model.getModelWithId(u1, t1, modelRow[0].asfProperty.tableBlockIndex);
          var userTmp2 = playerView.model.getModelWithId(u2, t2, modelRow[0].asfProperty.tableBlockIndex);
          if (userTmp1 && userTmp2) {
            userTmp1.on("valueChange", function() {
              userTmp2.setValue(userTmp1.getValue());
            });
          }
          PRIKAZ_UTIL.tableEnabled(playerView, t2);
        });
        table1.on('tableRowDelete', function(evt, rowModel, rowIndex, removedRow) {
          table2.removeRow(rowIndex);
        });
      }
      PRIKAZ_UTIL.tableEnabled(playerView, t2);
    }
  }

};
