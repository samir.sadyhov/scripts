var ZAYAVLEN_UTIL = {
  setVisibleCmp: function(playerView, cmp, visible) {
    if (typeof cmp == "object") {
      if (playerView.getViewWithId(cmp.ru)) playerView.getViewWithId(cmp.ru).setVisible(visible);
      if (playerView.getViewWithId(cmp.kz)) playerView.getViewWithId(cmp.kz).setVisible(visible);
    } else {
      if (playerView.getViewWithId(cmp)) playerView.getViewWithId(cmp).setVisible(visible);
    }
  },

  compareDates: function(playerView, cmpStart, cmpStop, resCmp) {
    var startDate=playerView.model.getModelWithId(cmpStart);
    var stopDate=playerView.model.getModelWithId(cmpStop);
    if (startDate && stopDate) {
      var resultBox=null;
      if (resCmp != null) resultBox = playerView.model.getModelWithId(resCmp);
      startDate.on("valueChange", function() {
        if (AS.FORMS.DateUtils.parseDate(startDate.getValue()) !=null && AS.FORMS.DateUtils.parseDate(stopDate.getValue()) !=null) {
          var d1 = AS.FORMS.DateUtils.parseDate(startDate.getValue()).getTime();
          var d2 = AS.FORMS.DateUtils.parseDate(stopDate.getValue()).getTime();
          var dd = Math.floor((d2-d1) / 86400000);
          if (dd < 0) {
            playerView.getViewWithId(cmpStop).container.parent().css("background-color", "#FF9999");
            playerView.getViewWithId(cmpStop+"_k").container.parent().css("background-color", "#FF9999");
            alert("Дата ПО [" + stopDate.getValue() + "] меньше даты С [" + startDate.getValue() + "]");
            if (resultBox) resultBox.setValue("");
            stopDate.setValue("");
            stopDate.asfProperty.required=true;
          } else {
            if (resultBox) resultBox.setValue(dd+"");
            playerView.getViewWithId(cmpStop).container.parent().css("background-color", "");
            playerView.getViewWithId(cmpStop+"_k").container.parent().css("background-color", "");
            if ((playerView.model.formId == "607f0e51-bac1-48b1-a18a-b8a7d9a2b9be" || playerView.model.formId == "1c8a3f23-3e34-466c-9f0a-742a08206fa0") && dd > 126) {
              playerView.getViewWithId(cmpStop).container.parent().css("background-color", "#FF9999");
              playerView.getViewWithId(cmpStop+"_k").container.parent().css("background-color", "#FF9999");
              alert("Отпуск по беременности и родам превышает 126 календарных дней");
              if (resultBox) resultBox.setValue("");
              stopDate.setValue("");
              stopDate.asfProperty.required=true;
            }

          }
        }
      });
      stopDate.on("valueChange", function() {
        if (AS.FORMS.DateUtils.parseDate(startDate.getValue()) !=null && AS.FORMS.DateUtils.parseDate(stopDate.getValue()) !=null) {
          var d1 = AS.FORMS.DateUtils.parseDate(startDate.getValue()).getTime();
          var d2 = AS.FORMS.DateUtils.parseDate(stopDate.getValue()).getTime();
          var dd = Math.floor((d2-d1) / 86400000);
          if (dd < 0) {
            playerView.getViewWithId(cmpStop).container.parent().css("background-color", "#FF9999");
            playerView.getViewWithId(cmpStop+"_k").container.parent().css("background-color", "#FF9999");
            alert("Дата ПО [" + stopDate.getValue() + "] меньше даты С [" + startDate.getValue() + "]");
            if (resultBox) resultBox.setValue("");
            stopDate.setValue("");
            stopDate.asfProperty.required=true;
          } else {
            if (resultBox) resultBox.setValue(dd+"");
            playerView.getViewWithId(cmpStop).container.parent().css("background-color", "");
            playerView.getViewWithId(cmpStop+"_k").container.parent().css("background-color", "");
            if ((playerView.model.formId == "607f0e51-bac1-48b1-a18a-b8a7d9a2b9be" || playerView.model.formId == "1c8a3f23-3e34-466c-9f0a-742a08206fa0") && dd > 126) {
              playerView.getViewWithId(cmpStop).container.parent().css("background-color", "#FF9999");
              playerView.getViewWithId(cmpStop+"_k").container.parent().css("background-color", "#FF9999");
              alert("Отпуск по беременности и родам превышает 126 календарных дней");
              if (resultBox) resultBox.setValue("");
              stopDate.setValue("");
              stopDate.asfProperty.required=true;
            }

          }
        }
      });
    }
  },

  changedValue: function(model, cmpObj) {
    if (model.getModelWithId(cmpObj.ru) && model.getModelWithId(cmpObj.kz)) {
      model.getModelWithId(cmpObj.ru).on("valueChange", function() {
        if (model.getModelWithId(cmpObj.kz) ) {
          model.getModelWithId(cmpObj.kz).setValue(model.getModelWithId(cmpObj.ru).getValue());
        }
        if (cmpObj.ru == "from_user") {
          ZAYAVLEN_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", "from_user", "t_number", "tab_num", null, null);
          ZAYAVLEN_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", "from_user", "fact_address", "address", null, null);
          ZAYAVLEN_UTIL.getCardUserValue(model, "8ac1c13a-6676-4117-99cf-d2c49a489b2a", "from_user", "sum_available_vacat_days", "days_available", null, null);
          ZAYAVLEN_UTIL.getCardUserValue(model, "8ac1c13a-6676-4117-99cf-d2c49a489b2a", "from_user", "sum_vacat_days", "remainder", null, null);
          ZAYAVLEN_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", "from_user", "telephone", "telephone", null, null);
        }
      });
      model.getModelWithId(cmpObj.kz).on("valueChange", function() {
        if (model.getModelWithId(cmpObj.ru) ) {
          model.getModelWithId(cmpObj.ru).setValue(model.getModelWithId(cmpObj.kz).getValue());
        }
        if (cmpObj.kz == "from_user_k") {
          ZAYAVLEN_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", "from_user_k", "t_number", "tab_num_k", null, null);
          ZAYAVLEN_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", "from_user_k", "fact_address", "address_k", null, null);
          ZAYAVLEN_UTIL.getCardUserValue(model, "8ac1c13a-6676-4117-99cf-d2c49a489b2a", "from_user_k", "sum_available_vacat_days", "days_available_k", null, null);
          ZAYAVLEN_UTIL.getCardUserValue(model, "8ac1c13a-6676-4117-99cf-d2c49a489b2a", "from_user_k", "sum_vacat_days", "remainder_k", null, null);
          ZAYAVLEN_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", "from_user_k", "telephone", "telephone_k", null, null);
        }
      });
    }
  },

  changedValueTable: function(model, table, cmp, index) {
    if (model.getModelWithId(cmp.ru, table.ru, index) && model.getModelWithId(cmp.kz, table.kz, index)) {
      model.getModelWithId(cmp.ru, table.ru, index).on("valueChange", function() {
        if (model.getModelWithId(cmp.kz, table.kz, index) ) {
          model.getModelWithId(cmp.kz, table.kz, index).setValue(model.getModelWithId(cmp.ru, table.ru, index).getValue());
        }
      });
      model.getModelWithId(cmp.kz, table.kz, index).on("valueChange", function() {
        if (model.getModelWithId(cmp.ru, table.ru, index) ) {
          model.getModelWithId(cmp.ru, table.ru, index).setValue(model.getModelWithId(cmp.kz, table.kz, index).getValue());
        }
      });
    }
  },

  tableOn: function(playerView, tabObj, cmpObj) {
    var tmpTableRu=playerView.model.getModelWithId(tabObj.ru);
    var tmpTableKz=playerView.model.getModelWithId(tabObj.kz);
    if (tmpTableRu && tmpTableKz) {
      for (var tj=0; tj < cmpObj.length; tj++) {
        ZAYAVLEN_UTIL.changedValueTable(playerView.model, tabObj, cmpObj[tj], 0);
      }
      tmpTableRu.on('tableRowAdd', function(evt, modelTab, modelRow) {
        tmpTableKz.createRow();
        for (var tj=0; tj < cmpObj.length; tj++) {
          ZAYAVLEN_UTIL.changedValueTable(playerView.model, tabObj, cmpObj[tj], modelRow[0].asfProperty.tableBlockIndex);
        }
        ZAYAVLEN_UTIL.tableEnabled(playerView, tabObj.kz);
      });
    }
    if (tmpTableRu && tmpTableKz) {
      tmpTableRu.on('tableRowDelete', function(evt, rowModel, rowIndex, removedRow) {
        tmpTableKz.removeRow(rowIndex);
      });
    }
    ZAYAVLEN_UTIL.tableEnabled(playerView, tabObj.kz);
  },

   tableEnabled: function(view, table) {
     if (view.editable) {
       var tmpTable=view.getViewWithId(table);
       if (tmpTable) tmpTable.setEnabled(false);
     } else {
       setTimeout(ZAYAVLEN_UTIL.tableEnabled, 500, view, table);
     }
   },

   getCardUserValue: function(model, form, cmpUser, cmpParent, cmpChild, table, index) {
     // функция которая вытаскивает с карточки пользователя (form) значение определенного компонента (cmpParent) и вставляет их на форму (cmpChild)
     if (table == null) {
       if (model.getModelWithId(cmpChild)) model.getModelWithId(cmpChild).setValue('');
     } else {
       if (model.getModelWithId(cmpChild, table, index)) model.getModelWithId(cmpChild, table, index).setValue('');
     }
     var personID=null;
     if (table == null) {
       personID = model.getModelWithId(cmpUser).value[0].personID;
     } else {
       personID = model.getModelWithId(cmpUser, table, index).value[0].personID;
     }
     if (personID) {
       var synergyLogin = 'Administrator';
       var synergyPass = '$ystemAdm1n';
       $.ajax({
         url: 'rest/api/personalrecord/forms/' + personID,
         dataType: 'json',
         username: synergyLogin,
         password: synergyPass,
         success: function(data) {
           var tmpJson;
           var cardID=null;
           tmpJson = JSON.stringify(data);
           tmpJson = String(tmpJson).replace(/form-uuid/g, "formUuid");
           tmpJson = tmpJson.replace(/data-uuid/g, "dataUuid");
           tmpJson = JSON.parse(tmpJson);
           for (var i = 0; i < tmpJson.length; i++) {
             if (tmpJson[i].formUuid == form) {
               cardID = tmpJson[i].dataUuid;
               break;
             }
           }
           if (cardID) {
             $.ajax({
               url: 'rest/api/asforms/data/' + cardID,
               dataType: 'json',
               username: synergyLogin,
               password: synergyPass,
               success: function(cardData) {
                 for (var j = 0; j < cardData.data.length; j++) {
                   if (cardData.data[j].id == cmpParent) {
                     var tmpBlok=null;
                     if (table == null) {
                       tmpBlok = model.getModelWithId(cmpChild);
                     } else {
                       tmpBlok = model.getModelWithId(cmpChild, table, index);
                     }
                     if (tmpBlok) tmpBlok.setValue(cardData.data[j].value);
                     break;
                   }
                 }
               }
             });
           }
         }
       });
     }
   },

  getUserOtpuskDays: function(playerView, form, cmpForm, cmpCard, personID) {
    if (personID != undefined && personID != null) {
      var synergyLogin = 'Administrator';
      var synergyPass = '$ystemAdm1n';
      $.ajax({
        url: 'rest/api/personalrecord/forms/' + personID,
        dataType: 'json',
        username: synergyLogin,
        password: synergyPass,
        success: function(data) {
          var tmpJson;
          var cardID=null;
          tmpJson = JSON.stringify(data);
          tmpJson = String(tmpJson).replace(/form-uuid/g, "formUuid");
          tmpJson = tmpJson.replace(/data-uuid/g, "dataUuid");
          tmpJson = JSON.parse(tmpJson);
          for (var i = 0; i < tmpJson.length; i++) {
            if (tmpJson[i].formUuid == form) {
              cardID = tmpJson[i].dataUuid;
              break;
            }
          }
          if (cardID) {
            $.ajax({
              url: 'rest/api/asforms/data/' + cardID,
              dataType: 'json',
              username: synergyLogin,
              password: synergyPass,
              success: function(cardData) {
                for (var j = 0; j < cardData.data.length; j++) {
                  if (cardData.data[j].id == cmpCard) {
                    var formText = playerView.model.getModelWithId(cmpForm);
                    formText.on("valueChange", function() {
                      var d1 = Number(formText.getValue());
                      var d2 = Number(cardData.data[j].value);
                      if (d1 > d2) {
                        playerView.getViewWithId(cmpForm).container.parent().css("background-color", "#FF9999");
                        playerView.getViewWithId(cmpForm+"_k").container.parent().css("background-color", "#FF9999");
                        alert("Введенное количество дней больше чем указано в карточке\nМаксимальное значение = " + d2);
                        formText.setValue('');
                        if (cmpForm == "additional") {
                          playerView.getViewWithId("date_last_po").container.parent().css("background-color", "#FF9999");
                          playerView.getViewWithId("date_last_po_k").container.parent().css("background-color", "#FF9999");
                          playerView.model.getModelWithId("date_last_po").setValue('');
                        }
                        if (cmpForm == "main") {
                          playerView.getViewWithId("date_additional").container.parent().css("background-color", "#FF9999");
                          playerView.getViewWithId("date_additional_k").container.parent().css("background-color", "#FF9999");
                          playerView.model.getModelWithId("date_additional").setValue('');
                        }
                      } else {
                        playerView.getViewWithId(cmpForm).container.parent().css("background-color", "");
                        playerView.getViewWithId(cmpForm+"_k").container.parent().css("background-color", "");
                        if (cmpForm == "additional") {
                          playerView.getViewWithId("date_last_po").container.parent().css("background-color", "");
                          playerView.getViewWithId("date_last_po_k").container.parent().css("background-color", "");
                        }
                        if (cmpForm == "main") {
                          playerView.getViewWithId("date_additional").container.parent().css("background-color", "");
                          playerView.getViewWithId("date_additional_k").container.parent().css("background-color", "");
                        }
                      }
                      if (isNaN(d1)) {
                        playerView.getViewWithId(cmpForm).container.parent().css("background-color", "#FF9999");
                        playerView.getViewWithId(cmpForm+"_k").container.parent().css("background-color", "#FF9999");
                        alert("Введено некорректное значение");
                        formText.setValue('');
                      }
                    });
                    break;
                  }
                }
              }
            });
          }
        }
      });
    }
  }
};

AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  var formZayavArr = ["20339b13-e458-46e6-98ca-39efb19a6078", "1c8a3f23-3e34-466c-9f0a-742a08206fa0", "0991ee5d-91b7-44fe-874e-acd85112f019", "0ed9385d-9409-4684-ba21-efce27c127a0", "19d4b914-cab4-493b-975d-30bf59200f5d", "26aefbbd-c657-4934-827f-6a6cfb389e99", "3d11d268-0f6e-48df-916c-87505893e309", "60652131-5919-4d69-a0d3-2cb47c6378e8", "607f0e51-bac1-48b1-a18a-b8a7d9a2b9be", "71077ec5-ebe2-46bc-a1ea-92721e996cd3", "75ae8198-55f0-4329-afab-f17a6d25c753", "7a9b57cc-738d-463c-9bf9-a32f312f8257", "86552db9-76ef-4de4-9482-229ae6634317", "8e3a4959-b04d-4825-8145-fb4739999e16", "990fc530-e170-471b-b2c8-99767898ff49", "a3c67f06-daf3-4864-9022-0e453bf6d788", "e4e66567-1fa6-4151-a9df-2c19b066e528", "e68371c7-9475-4260-8c16-c1e1640c2c1a", "ebda32de-2333-4790-988c-cb4771608a8d"];

  for (var fi=0; fi < formZayavArr.length; fi++) {
    if (model.formId == formZayavArr[fi]) {
      var cmpArrZ=[{ru: "td_doc", kz: "td_doc_k"},{ru: "doc", kz: "doc_k"},{ru: "td_num", kz: "td_num_k"},{ru: "td_date", kz: "td_date_k"},{ru: "to_user", kz: "to_user_k"},{ru: "from_user", kz: "from_user_k"},{ru: "address", kz: "address_k"},{ru: "telephone", kz: "telephone_k"},{ru: "tab_num", kz: "tab_num_k"},{ru: "city", kz: "city_k"},{ru: "user", kz: "user_k"},{ru: "user1", kz: "user1_k"},{ru: "user2", kz: "user2_k"},{ru: "user3", kz: "user3_k"},{ru: "user4", kz: "user4_k"},{ru: "user5", kz: "user5_k"},{ru: "user6", kz: "user6_k"},{ru: "user7", kz: "user7_k"},{ru: "user8", kz: "user8_k"},{ru: "user9", kz: "user9_k"},{ru: "user10", kz: "user10_k"},{ru: "user_to", kz: "user_to_k"},{ru: "position", kz: "position_k"},{ru: "position_to", kz: "position_to_k"},{ru: "department_to", kz: "department_to_k"},{ru: "from_user", kz: "from_user_k"},{ru: "period", kz: "period_k"},{ru: "choice", kz: "choice_k"},{ru: "сhoice", kz: "сhoice_k"},{ru: "сhoice1", kz: "сhoice1_k"}, {ru: "choice2", kz: "choice2_k"},{ru: "сhoice2", kz: "сhoice2_k"},{ru: "сhoice3", kz: "сhoice3_k"},{ru: "сhoice4", kz: "сhoice4_k"},{ru: "сhoice5", kz: "сhoice5_k"}, {ru: "сhoice6", kz: "сhoice6_k"},{ru: "сhoice7", kz: "сhoice7_k"},{ru: "сhoice8", kz: "сhoice8_k"},{ru: "сhoice9", kz: "сhoice9_k"},{ru: "сhoice10", kz: "сhoice10_k"}, {ru: "start_date", kz: "start_date_k"}, {ru: "finish_date", kz: "finish_date_k"}, {ru: "vid_td", kz: "vid_td_k"}, {ru: "start_period", kz: "start_period_k"},{ru: "period_start", kz: "period_start_k"},{ru: "end_period", kz: "end_period_k"},{ru: "period_finish", kz: "period_finish_k"},{ru: "days", kz: "days_k"},{ru: "main", kz: "main_k"},{ru: "additional", kz: "additional_k"},{ru: "date_main", kz: "date_main_k"},{ru: "date_additional", kz: "date_additional_k"},{ru: "date_last", kz: "date_last_k"},{ru: "date_last_po", kz: "date_last_po_k"},{ru: "days_available", kz: "days_available_k"},{ru: "remainder", kz: "remainder_k"},{ru: "podpisant_user", kz: "podpisant_user_k"},{ru: "podpisant_position", kz: "podpisant_position_k"},{ru: "ispolnitel", kz: "ispolnitel_k"},{ru: "isponitel", kz: "isponitel_k"},{ru: "mat_pom_payment", kz: "mat_pom_payment_k"},{ru: "date_start", kz: "date_start_k"},{ru: "date_finish", kz: "date_finish_k"}];
      var tableArrZ = [{id: {ru: "table2_r", kz: "table2_k"}, cmp: [{ru: "user", kz: "user_k"}]}, {id: {ru: "table3_r", kz: "table3_k"}, cmp: [{ru: "сhoice2", kz: "сhoice2_k"}]}];

      model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
        console.log(model.formName + '\nformID: [' + model.formId +'] asfDataID: [' + model.asfDataId+']');
        for (var ci=0; ci < cmpArrZ.length; ci++) {
          ZAYAVLEN_UTIL.changedValue(model, cmpArrZ[ci]);
        }
        for (var ti=0; ti < tableArrZ.length; ti++) {
          ZAYAVLEN_UTIL.tableOn(view, tableArrZ[ti].id, tableArrZ[ti].cmp);
        }
      });
      if (view.editable) {
        for (var ti2=0; ti2 < tableArrZ.length; ti2++) {
          ZAYAVLEN_UTIL.tableEnabled(view, tableArrZ[ti2].id.kz);
        }
      }

      // Заявление о приеме на работу на период
      if (model.formId == "26aefbbd-c657-4934-827f-6a6cfb389e99") {
        // логика с таблицами
        var periodCmp = model.getModelWithId("period");
        var tab1={ru: "table2_r", kz: "table2_k"};
        var tab2={ru: "table3_r", kz: "table3_k"};
        if (periodCmp.value[0] == "1") {
          ZAYAVLEN_UTIL.setVisibleCmp(view, tab2, false);
          ZAYAVLEN_UTIL.setVisibleCmp(view, tab1, true);
        } else {
          ZAYAVLEN_UTIL.setVisibleCmp(view, tab2, true);
          ZAYAVLEN_UTIL.setVisibleCmp(view, tab1, false);
        }

        periodCmp.on("valueChange", function() {
          var tt1 = model.getModelWithId(tab1.ru);
          var tt2 = model.getModelWithId(tab2.ru);
          if (periodCmp.value[0] == "1") {
            ZAYAVLEN_UTIL.setVisibleCmp(view, tab2, false);
            ZAYAVLEN_UTIL.setVisibleCmp(view, tab1, true);
            tt2.modelBlocks.forEach(function(modelBlock, index){
                tt2.removeRow(index);
            });
            if (tt1.modelBlocks.length == 0) {
              tt1.createRow();
            } else {
              tt1.modelBlocks.forEach(function(modelBlock, index){
                  tt1.removeRow(index);
              });
              if (tt1.modelBlocks.length == 0) tt1.createRow();
            }
          } else {
            ZAYAVLEN_UTIL.setVisibleCmp(view, tab2, true);
            ZAYAVLEN_UTIL.setVisibleCmp(view, tab1, false);
            tt2.modelBlocks.forEach(function(modelBlock, index){
                tt2.removeRow(index);
            });
            if (tt1.modelBlocks.length > 0) {
              tt1.modelBlocks.forEach(function(modelBlock, index){
                  tt1.removeRow(index);
              });
            }
            if (tt2.modelBlocks.length == 0) {
              tt2.createRow();
            } else {
              tt2.modelBlocks.forEach(function(modelBlock, index){
                  tt2.removeRow(index);
              });
              if (tt2.modelBlocks.length == 0) tt2.createRow();
            }
          }
        });
      }

      // Всего календарных дней
      var vsegoOsnOtp=model.getModelWithId("main");
      var vsegoDopOtp=model.getModelWithId("additional");
      var vsegoDays=model.getModelWithId("days");
      if (vsegoOsnOtp && vsegoDopOtp && vsegoDays) {
        vsegoOsnOtp.on("valueChange", function() {
          if (vsegoOsnOtp.getValue() != undefined && vsegoDopOtp.getValue() != undefined) {
            vsegoDays.setValue(Number(vsegoOsnOtp.getValue())+Number(vsegoDopOtp.getValue())+"");
          } else if (vsegoOsnOtp.getValue() != undefined && vsegoDopOtp.getValue() == undefined) {
            vsegoDays.setValue(vsegoOsnOtp.getValue());
          } else if (vsegoOsnOtp.getValue() == undefined && vsegoDopOtp.getValue() != undefined) {
            vsegoDays.setValue(vsegoDopOtp.getValue());
          } else {
            vsegoDays.setValue("");
          }
        });
        vsegoDopOtp.on("valueChange", function() {
          if (vsegoOsnOtp.getValue() != undefined && vsegoDopOtp.getValue() != undefined) {
            vsegoDays.setValue(Number(vsegoOsnOtp.getValue())+Number(vsegoDopOtp.getValue())+"");
          } else if (vsegoOsnOtp.getValue() != undefined && vsegoDopOtp.getValue() == undefined) {
            vsegoDays.setValue(vsegoOsnOtp.getValue());
          } else if (vsegoOsnOtp.getValue() == undefined && vsegoDopOtp.getValue() != undefined) {
            vsegoDays.setValue(vsegoDopOtp.getValue());
          } else {
            vsegoDays.setValue("");
          }
        });
      }
      // логика  С-ПО
      ZAYAVLEN_UTIL.compareDates(view, "start_period", "end_period", null);
      ZAYAVLEN_UTIL.compareDates(view, "start_date", "finish_date", null);
      if (vsegoOsnOtp) ZAYAVLEN_UTIL.compareDates(view, "date_main", "date_additional", "main");
      if (vsegoDopOtp) ZAYAVLEN_UTIL.compareDates(view, "date_last", "date_last_po", "additional");

      // Заявление о предоставлении ежегодного трудового отпуска с мат.помощью
      // Заявление о предоставлении ежегодного трудового отпуска без выплаты
      if (model.formId == "3d11d268-0f6e-48df-916c-87505893e309" || model.formId == "60652131-5919-4d69-a0d3-2cb47c6378e8") {
        // // проверка календарных дней основного и дополнительного отпуска
        // var tmpUser1=model.getModelWithId("from_user");
        // tmpUser1.on("valueChange", function() {
        //   ZAYAVLEN_UTIL.getUserOtpuskDays(view, "8ac1c13a-6676-4117-99cf-d2c49a489b2a", "additional", "day_dop", tmpUser1.value[0].personID);
        //   ZAYAVLEN_UTIL.getUserOtpuskDays(view, "8ac1c13a-6676-4117-99cf-d2c49a489b2a", "main", "day_osnovn", tmpUser1.value[0].personID);
        // });
        // проверка дат основного и доп. отпусков
        var dateOsnS=model.getModelWithId("date_main");
        var dateOsnP=model.getModelWithId("date_additional");
        var dateDopS=model.getModelWithId("date_last");
        var dateDopP=model.getModelWithId("date_last_po");

        // дата дополнительного С не больше даты основного ПО
        dateDopS.on("valueChange", function() {
          if (dateOsnP.getValue() != undefined && dateOsnP.getValue() != "" && dateOsnP.getValue() != "____-__-__ 00:00:00") {
            var tmpD1=AS.FORMS.DateUtils.parseDate(dateOsnP.getValue());
            tmpD1.setDate(tmpD1.getDate() + 1);
            var tmpD2=AS.FORMS.DateUtils.parseDate(dateDopS.getValue());
            if (dateDopS.getValue() != undefined && dateDopS.getValue() != "" && dateDopS.getValue() != "____-__-__ 00:00:00") {
              if (tmpD2.getTime() != tmpD1.getTime()) {
                view.getViewWithId("date_last").container.parent().css("background-color", "#FF9999");
                alert("Дата дополнительного отпуска должна быть больше даты окончания основного отпуска на 1 день.");
                dateDopS.setValue("");
              } else {
                view.getViewWithId("date_last").container.parent().css("background-color", "");
              }
            }
          }
        });
      } // заявления по отпускам

    }
  }
});
