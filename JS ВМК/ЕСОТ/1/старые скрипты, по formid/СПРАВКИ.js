var SPRAVKI_UTIL = {
  changedValue: function(model, cmpObj) {
    if (model.getModelWithId(cmpObj.ru) && model.getModelWithId(cmpObj.kz)) {
      model.getModelWithId(cmpObj.ru).on("valueChange", function() {
        if (model.getModelWithId(cmpObj.kz) ) {
          model.getModelWithId(cmpObj.kz).setValue(model.getModelWithId(cmpObj.ru).getValue());
        }
        if (cmpObj.ru == "user") {
          SPRAVKI_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", "user", "t_number", "tab_num", null, null);
          SPRAVKI_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", "user", "telephone", "telephone", null, null);
        }
        if (cmpObj.ru == "ispolnitel_user") {
          SPRAVKI_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", "ispolnitel_user", "telephone", "ispolnitel_phone", null, null);
        }
      });
      model.getModelWithId(cmpObj.kz).on("valueChange", function() {
        if (model.getModelWithId(cmpObj.ru) ) {
          model.getModelWithId(cmpObj.ru).setValue(model.getModelWithId(cmpObj.kz).getValue());
        }
        if (cmpObj.kz == "user_k") {
          SPRAVKI_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", "user_k", "t_number", "tab_num_k", null, null);
          SPRAVKI_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", "user_k", "telephone", "telephone_k", null, null);
        }
        if (cmpObj.ru == "ispolnitel_user_k") {
          SPRAVKI_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", "ispolnitel_user_k", "telephone", "ispolnitel_phone_k", null, null);
        }
      });
    }
  },

  changedValueTable: function(model, table, cmp, index) {
    if (model.getModelWithId(cmp.ru, table.ru, index) && model.getModelWithId(cmp.kz, table.kz, index)) {
      model.getModelWithId(cmp.ru, table.ru, index).on("valueChange", function() {
        if (cmp.ru == "user") {
          SPRAVKI_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", "user", "t_number", "tab_num", table.ru, index);
        }
        if (model.getModelWithId(cmp.kz, table.kz, index) ) {
          model.getModelWithId(cmp.kz, table.kz, index).setValue(model.getModelWithId(cmp.ru, table.ru, index).getValue());
        }
      });
      model.getModelWithId(cmp.kz, table.kz, index).on("valueChange", function() {
        if (cmp.kz == "user_k") {
          SPRAVKI_UTIL.getCardUserValue(model, "6e7ee767-7f9b-4f53-b7d9-04a6091d066f", "user_k", "t_number", "tab_num_k", table.kz, index);
        }
        if (model.getModelWithId(cmp.ru, table.ru, index) ) {
          model.getModelWithId(cmp.ru, table.ru, index).setValue(model.getModelWithId(cmp.kz, table.kz, index).getValue());
        }
      });
    }
  },

  tableOn: function(playerView, tabObj, cmpObj) {
    var tmpTableRu=playerView.model.getModelWithId(tabObj.ru);
    var tmpTableKz=playerView.model.getModelWithId(tabObj.kz);
    if (tmpTableRu && tmpTableKz) {
      for (var tj=0; tj < cmpObj.length; tj++) {
        SPRAVKI_UTIL.changedValueTable(playerView.model, tabObj, cmpObj[tj], 0);
      }
      tmpTableRu.on('tableRowAdd', function(evt, modelTab, modelRow) {
        tmpTableKz.createRow();
        for (var tj=0; tj < cmpObj.length; tj++) {
          SPRAVKI_UTIL.changedValueTable(playerView.model, tabObj, cmpObj[tj], modelRow[0].asfProperty.tableBlockIndex);
        }
        SPRAVKI_UTIL.tableEnabled(playerView, tabObj.kz);
      });
    }
    if (tmpTableRu && tmpTableKz) {
      tmpTableRu.on('tableRowDelete', function(evt, rowModel, rowIndex, removedRow) {
        tmpTableKz.removeRow(rowIndex);
      });
    }
    SPRAVKI_UTIL.tableEnabled(playerView, tabObj.kz);
  },

   tableEnabled: function(view, table) {
     if (view.editable) {
       var tmpTable=view.getViewWithId(table);
       if (tmpTable) tmpTable.setEnabled(false);
     } else {
       setTimeout(SPRAVKI_UTIL.tableEnabled, 500, view, table);
     }
   },

  getCardUserValue: function(model, form, cmpUser, cmpParent, cmpChild, table, index) {
    // функция которая вытаскивает с карточки пользователя (form) значение определенного компонента (cmpParent) и вставляет их на форму (cmpChild)
    if (table == null) {
      if (model.getModelWithId(cmpChild)) model.getModelWithId(cmpChild).setValue('');
    } else {
      if (model.getModelWithId(cmpChild, table, index)) model.getModelWithId(cmpChild, table, index).setValue('');
    }
    var personID=null;
    if (table == null) {
      personID = model.getModelWithId(cmpUser).value[0].personID;
    } else {
      personID = model.getModelWithId(cmpUser, table, index).value[0].personID;
    }
    if (personID) {
      var synergyLogin = 'Administrator';
      var synergyPass = '$ystemAdm1n';
      $.ajax({ // получение карточек пользователя
        url: 'rest/api/personalrecord/forms/' + personID,
        dataType: 'json',
        username: synergyLogin,
        password: synergyPass,
        success: function(data) {
          var tmpJson;
          var cardID=null;
          tmpJson = JSON.stringify(data);
          tmpJson = String(tmpJson).replace(/form-uuid/g, "formUuid");
          tmpJson = tmpJson.replace(/data-uuid/g, "dataUuid");
          tmpJson = JSON.parse(tmpJson);
          // поиск нужной карточки по formID
          for (var i = 0; i < tmpJson.length; i++) {
            if (tmpJson[i].formUuid == form) {
              cardID = tmpJson[i].dataUuid; // получаем ID карточки
              break;
            }
          }
          if (cardID) { // если нужная карточка есть достаем данные
            $.ajax({
              url: 'rest/api/asforms/data/' + cardID,
              dataType: 'json',
              username: synergyLogin,
              password: synergyPass,
              success: function(cardData) {
                for (var j = 0; j < cardData.data.length; j++) {
                  if (cardData.data[j].id == cmpParent) {
                    var tmpBlok=null;
                    if (table == null) {
                      tmpBlok = model.getModelWithId(cmpChild);
                    } else {
                      tmpBlok = model.getModelWithId(cmpChild, table, index);
                    }
                    if (tmpBlok) tmpBlok.setValue(cardData.data[j].value);
                    break;
                  }
                }
              }
            });
          }
        }
      });
    }
  },

  compareDates: function(playerView, cmpStart, cmpStop, resCmp, table, blockNumber) {
    var startDate = null;
    var stopDate = null;
    var resultBox = null;
    if (table == null && blockNumber == null) {
      startDate = playerView.model.getModelWithId(cmpStart);
      stopDate = playerView.model.getModelWithId(cmpStop);
      if (resCmp != null) resultBox = playerView.model.getModelWithId(resCmp);
    } else {
      startDate = playerView.model.getModelWithId(cmpStart, table.ru, blockNumber);
      stopDate = playerView.model.getModelWithId(cmpStop, table.ru, blockNumber);
      if (resCmp != null) resultBox = playerView.model.getModelWithId(resCmp, table.ru, blockNumber);
    }

    if (startDate && stopDate) {
      startDate.on("valueChange", function() {
        if (startDate.getValue() != undefined && stopDate.getValue() != undefined && stopDate.getValue() != "" && stopDate.getValue() != "____-__-__ 00:00:00") {
          var d1 = AS.FORMS.DateUtils.parseDate(startDate.getValue()).getTime();
          var d2 = AS.FORMS.DateUtils.parseDate(stopDate.getValue()).getTime();
          var dd = Math.floor((d2-d1) / 86400000);
          if (dd < 0) {
            if (table == null && blockNumber == null) {
              playerView.getViewWithId(cmpStop).container.parent().css("background-color", "#FF9999");
              playerView.getViewWithId(cmpStop+"_k").container.parent().css("background-color", "#FF9999");
            } else {
              playerView.getViewWithId(cmpStop, table.ru, blockNumber).container.parent().css("background-color", "#FF9999");
              playerView.getViewWithId(cmpStop+"_k", table.kz, blockNumber).container.parent().css("background-color", "#FF9999");
            }
            alert("Дата ПО [" + stopDate.getValue() + "] меньше даты С [" + startDate.getValue() + "]");
            if (resultBox) resultBox.setValue("");
            stopDate.setValue("");
            stopDate.asfProperty.required=true;
          } else {
            if (resultBox) resultBox.setValue(dd+"");
            if (table == null && blockNumber == null) {
              playerView.getViewWithId(cmpStop).container.parent().css("background-color", "");
              playerView.getViewWithId(cmpStop+"_k").container.parent().css("background-color", "");
            } else {
              playerView.getViewWithId(cmpStop, table.ru, blockNumber).container.parent().css("background-color", "");
              playerView.getViewWithId(cmpStop+"_k", table.kz, blockNumber).container.parent().css("background-color", "");
            }
          }
        }
      });
      stopDate.on("valueChange", function() {
        if (startDate.getValue() != undefined && stopDate.getValue() != undefined && stopDate.getValue() != "" && stopDate.getValue() != "____-__-__ 00:00:00") {
          var d1 = AS.FORMS.DateUtils.parseDate(startDate.getValue()).getTime();
          var d2 = AS.FORMS.DateUtils.parseDate(stopDate.getValue()).getTime();
          var dd = Math.floor((d2-d1) / 86400000);
          if (dd < 0) {
            if (table == null && blockNumber == null) {
              playerView.getViewWithId(cmpStop).container.parent().css("background-color", "#FF9999");
              playerView.getViewWithId(cmpStop+"_k").container.parent().css("background-color", "#FF9999");
            } else {
              playerView.getViewWithId(cmpStop, table.ru, blockNumber).container.parent().css("background-color", "#FF9999");
              playerView.getViewWithId(cmpStop+"_k", table.kz, blockNumber).container.parent().css("background-color", "#FF9999");
            }
            alert("Дата ПО [" + stopDate.getValue() + "] меньше даты С [" + startDate.getValue() + "]");
            if (resultBox) resultBox.setValue("");
            stopDate.setValue("");
            stopDate.asfProperty.required=true;
          } else {
            if (resultBox) resultBox.setValue(dd+"");
            if (table == null && blockNumber == null) {
              playerView.getViewWithId(cmpStop).container.parent().css("background-color", "");
              playerView.getViewWithId(cmpStop+"_k").container.parent().css("background-color", "");
            } else {
              playerView.getViewWithId(cmpStop, table.ru, blockNumber).container.parent().css("background-color", "");
              playerView.getViewWithId(cmpStop+"_k", table.kz, blockNumber).container.parent().css("background-color", "");
            }
          }
        }
      });
    }
  }

};

AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  var formSpravkaArr = ["0a07e25b-118a-4493-948a-f33f2561e7e9", "7a06a2e7-00cc-425b-9915-5fb569c522cf", "3b26c1e9-39f9-49d5-b8cd-15d9fa56f28f", "86af8c73-9671-411c-81c6-1ab07d2496e9", "6460c67d-5cb0-4fa4-a3a6-10ebcd5b9aee"];
  for (var fi=0; fi < formSpravkaArr.length; fi++) {
    if (model.formId == formSpravkaArr[fi]) {
      var cmpArrSP=[{ru: "doc_num", kz: "doc_num_k"}, {ru: "summa", kz: "summa_k"}, {ru: "summa2", kz: "summa2_k"}, {ru: "vznos1", kz: "vznos1_k"}, {ru: "nalog1", kz: "nalog1_k"}, {ru: "uderzhanie1", kz: "uderzhanie1_k"}, {ru: "vyplata1", kz: "vyplata1_k"}, {ru: "podpisant_position", kz: "podpisant_position_k"}, {ru: "podpisant_user", kz: "podpisant_user_k"}, {ru: "podpisant_user2", kz: "podpisant_user2_k"}, {ru: "ispolnitel_user", kz: "ispolnitel_user_k"}, {ru: "ispolnitel_phone", kz: "ispolnitel_phone_k"}, {ru: "position2", kz: "position2_k"}, {ru: "finish_date", kz: "finish_date_k"}, {ru: "start_date", kz: "start_date_k"}, {ru: "department", kz: "department_k"}, {ru: "position", kz: "position_k"}, {ru: "department1", kz: "department1_k"}, {ru: "tab_num", kz: "tab_num_k"}, {ru: "user", kz: "user_k"}, {ru: "date", kz: "date_k"}, {ru: "from_user", kz: "from_user_k"}, {ru: "telephone", kz: "telephone_k"}, {ru: "choice1", kz: "choice1_k"}, {ru: "ruk_user", kz: "ruk_user_k"}, {ru: "date2", kz: "date2_k"}, {ru: "stazh", kz: "stazh_k"}, {ru: "choice2", kz: "choice2_k"}, {ru: "uslovie", kz: "uslovie_k"}, {ru: "dana", kz: "dana_k"}, {ru: "naimenovanie", kz: "naimenovanie_k"}, {ru: "start_date1", kz: "start_date1_k"}, {ru: "finish_date1", kz: "finish_date1_k"}, {ru: "obosnovanie", kz: "obosnovanie_k"}, {ru: "svedenia", kz: "svedenia_k"}];
      var tableArrSP=[{id: {ru: "t", kz: "t_k"}, cmp: [{ru: "month", kz: "month_k"}, {ru: "summa", kz: "summa_k"}, {ru: "summa1", kz: "summa1_k"}, {ru: "vznos", kz: "vznos_k"}, {ru: "nalog", kz: "nalog_k"}, {ru: "uderzhanie", kz: "uderzhanie_k"}, {ru: "vyplata", kz: "vyplata_k"}, {ru: "user", kz: "user_k"}, {ru: "tab_num", kz: "tab_num_k"}, {ru: "position", kz: "position_k"}, {ru: "number", kz: "number_k"}, {ru: "date1", kz: "date1_k"}]}];

      model.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
        console.log(model.formName + '\nformID: [' + model.formId +'] asfDataID: [' + model.asfDataId+']');
        for (var ti=0; ti < tableArrSP.length; ti++) {
          SPRAVKI_UTIL.tableOn(view, tableArrSP[ti].id, tableArrSP[ti].cmp);
        }
        for (var ci=0; ci < cmpArrSP.length; ci++) {
          SPRAVKI_UTIL.changedValue(model, cmpArrSP[ci]);
        }
        if (view.editable) {
          for (var ti2=0; ti2 < tableArrSP.length; ti2++) {
            SPRAVKI_UTIL.tableEnabled(view, tableArrSP[ti2].id.kz);
          }
        }
        // с - по
        SPRAVKI_UTIL.compareDates(view, "start_date", "finish_date", null, null, null);
        SPRAVKI_UTIL.compareDates(view, "start_date1", "finish_date1", null, null, null);
      });

    }
  }
});
