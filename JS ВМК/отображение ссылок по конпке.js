window.onload=function(){

  var c1 = '#B5D5FF';
  var c2 = 'rgba(181, 213, 255, 0.5)';

  var sliderHtml = '<div class="slider">'+
  '<div class="slide-list">'+
    '<div class="slide-wrap">'+

      '<div class="slide-item">'+
        '<a title="Видео" href="https://www.youtube.com" target="_blank"><img width="100" height="40" src="https://www.youtube.com/yt/img/logo_1x.png" alt="www.youtube.com" /></a>'+
          '<span class="slide-title"></span>'+
      '</div>'+

      '<div class="slide-item">'+
        '<a href="http://www.enu.kz" target="_blank"><img width="100" height="40" src="http://www.enu.kz/pictures/novosti/nov-logo-kaz.png" alt="www.enu.kz" /></a>'+
          '<span class="slide-title"></span>'+
      '</div>'+

      '<div class="slide-item">'+
        '<a href="http://egov.kz/cms/ru" target="_blank"><img width="100" height="40" src="http://egov.kz/cms/sites/all/themes/egov_kz/images/logo.png" alt="egov.kz" /></a>'+
          '<span class="slide-title">подпись</span>'+
      '</div>'+

      '<div class="slide-item">'+
        '<a href="http://sud.gov.kz/rus" target="_blank"><img width="100" height="40" src="http://sud.gov.kz/sites/all/themes/supcourt/i/logo-ru.png" alt="sud.gov.kz" /></a>'+
          '<span class="slide-title"></span>'+
      '</div>'+

      '<div class="slide-item">'+
        '<a href="http://prokuror.gov.kz/rus" target="_blank"><img width="100" height="40" src="http://prokuror.gov.kz/sites/all/themes/newproc/logo.png" alt="prokuror.gov.kz/" /></a>'+
          '<span class="slide-title"></span>'+
      '</div>'+

      '</div>'+
      '<div class="clear"></div>'+
    '</div>'+
    '<div name="prev" class="navy prev-slide"></div>'+
    '<div name="next" class="navy next-slide"></div>'+
  '</div>';


  var footerBox = $('<div>', {class: 'footer'});
  footerBox.css({
    'position': 'absolute',
    'left': '50%',
    'margin-left': '-30%',
    'bottom': '34px',
    'width': '60%',
   // 'border-radius': '3px',
    'background': c1,
    'background-color': c2,
    'display': 'none',
    'z-index': '999'
  });
  $('body').append(footerBox);

  var but = $('<button>', {class: 'show-link-button'}).html('Показать ссылки');
  but.css({
    'position': 'absolute',
    'bottom': '3px',
    'right': '100px',
    'background-color': '#49B785',
    'border': 'none',
    'border-radius': '4px',
    'color': 'black',
    'padding': '4px 15px',
    'font-weight': 'bold'
  });
  but.hover(function() {
    $(this).css('background-color', '#53CC97');
  }, function() {
    $(this).css('background-color', '#49B785');
  });

  but.click(function() {

    var buttonClose = $('<button>').html('X');
    buttonClose.css({
      'position': 'absolute',
      'top': '3px',
      'right': '4px',
      'background-color': '#C11919',
      'border': 'none',
      'border-radius': '4px',
      'color': 'black',
      'padding': '4px 8px',
      'text-align': 'center',
      'text-decoration': 'none',
      'display': 'inline-block',
      'font-size': '14px',
      'font-weight': 'bold',
      'cursor': 'pointer'
    });
    buttonClose.attr("title", "Закрыть");
    buttonClose.hover(function() {
      $(this).css('background-color', '#5B0000');
    }, function() {
      $(this).css('background-color', '#C11919');
    });
    buttonClose.click(function() {
      footerBox.slideToggle(200);
      setTimeout(function(){
        but.show();
      }, 200);
    });

    footerBox.html('<div style="border-bottom: 2px solid #1C90F3; text-align: left; text-shadow: none; padding: 6px 8px; color: #1C90F3; font-size: 14px; font-weight: bold; background: #6495ED; background-color: rgba(181, 213, 255, 0.8); cursor: default;">Ссылки</div>'+sliderHtml);
    footerBox.slideToggle(300);
    but.hide();
    footerBox.append(buttonClose);

   htmSlider();
  });

  $('body').append(but);

  function htmSlider(){
    var slideWrap = $('.slide-wrap');
    var slideWidth = $('.slide-item').outerWidth();
    var scrollSlider = slideWrap.position().left - slideWidth;
    // кнопки вперед\назад
    var nextLink = $('.next-slide');
    var prevLink = $('.prev-slide');

    /* Клик по ссылке на следующий слайд */
    nextLink.click(function(){
      if(!slideWrap.is(':animated')) {
        slideWrap.animate({left: scrollSlider}, 500, function(){
          slideWrap
          .find('.slide-item:first')
          .appendTo(slideWrap)
          .parent()
          .css({'left': 0});
        });
      }
    });

    /* Клик по ссылке на предыдующий слайд */
    prevLink.click(function(){
      if(!slideWrap.is(':animated')) {
        slideWrap
        .css({'left': scrollSlider})
        .find('.slide-item:last')
        .prependTo(slideWrap)
        .parent()
        .animate({'left': 0}, 500);
      }
    });

    // timer = setInterval(function(){
    //   slideWrap.animate({left: scrollSlider}, 1500, function(){
    //     slideWrap
    //     .find('.slide-item:first')
    //     .appendTo(slideWrap)
    //     .parent()
    //     .css({'left': 0});
    //   });
    // }, 5000);

  }
};









<style>
html,
body {
  height: 100%;
}
/* Задаем сброс обтекания */
.clear {
	margin-top: -1px;
	height: 1px;
	clear:both;
	zoom: 1;
}
/* Slider */
.slider {
	/* Ширина контейнера */
  width: auto;
	/* Внешние тступы сверху и снизу */
	margin: 10px auto;
	/* Внутренние отступы для ссылок navy */
	//padding: 0 30px;
	/* Позиционирование */
	position: relative;
}
/* Двойной клик по кнопкам вперед/назад вызывает выделение всех элементов слайдера,
поэтому предотвращаем это */
.slider::-moz-selection { background: transparent; color: #fff; text-shadow: none; }
.slider::selection { background: transparent; color: #fff; text-shadow: none; }

.slide-list {
	position: relative;
  margin: 0;
	padding: 0;
	/* Скроем то что выходит за границы */
	overflow: hidden;
}
.slide-wrap {
	position: relative;
	left: 0px;
	top: 0;
	/* максимально возможная ширина обертки слайдера */
	width: 10000000px;
}
.slide-item {
	/* Ширина слайда */
	//width: 500px;
  width: auto;
	/* Внутренние отступы */
	padding: 10px 30px;
	/* Обтекание */
	float: left;
}
.slide-title {
	/* Шрифт */
	font: 10px monospace;
	/* Указываем, что элемент блочный */
	display: block;
  color: blue;
}
/* навигация вперед/назад */
.navy {
	/* абсолютное позиционирование */
	position: absolute;
	top: 0;
	z-index: 1;
	height: 100%;
	/* ширина элементов */
	width: 30px;
	cursor: pointer;
}
.prev-slide {
	left: 0;
	background: #6495ED  url(images/buttons/dark.gray/back.png) 11px 40% no-repeat;
  background-color: rgba(181, 213, 255, 0.7);
}
.next-slide {
	right: 0;
	background: #6495ED url(images/buttons/dark.gray/forward.png) 11px 40% no-repeat;
  background-color: rgba(181, 213, 255, 0.7);
}
.navy.disable {
  background: #dbdbdb;
}
</style>
