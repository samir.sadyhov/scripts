(function () {
  $.ajax({
    type: 'POST',
    url: 'rest/api/filecabinet/user/save',
    dataType: 'json',
    async: false,
    beforeSend: function (xhr) {
      xhr.setRequestHeader('Authorization', 'Basic ' + btoa(AS.OPTIONS.login + ':' + AS.OPTIONS.password))
    },
    data: {lastname: 'test526', firstname: 'test4576', patronymic: 'test5823', pointersCode: 'test526'},
    success: function () {
      console.log('User created')
    },
    error: function (jqXHR, textStatus, errorThrown) {
      console.log(textStatus + ' ' + errorThrown)
    }
  })
})()
