var KMG_MOVE_POSITION_CHOOSER_OPTIONS = {formCode : "Карточка_должности",  fields : {
    "1" : ["discharge_work", "category_work", "grade_work"],
    "2" : ["category_official", "rank_official", "group_official", "grade_official"]
}};

var KMG_MOVE_POSITION_CHOOSER = [];
KMG_MOVE_POSITION_CHOOSER.push({formCode : "Приказ_о_перемещении", cmpIds : ["change_position", "change_position_k"], cmpDepends : {"change_position" : "position", "change_position_k" : "position_k"}  });



if(!AS.FORMS.ApiUtils.getPositions) {
    AS.FORMS.ApiUtils.getPositions = AS.FORMS.ApiUtils.getPositionsSuggestions;
}




AS.FORMS.MovePositionChooserKMG = function(multiSelect, name, fields, selectedPositionId, locale){


    var instance = this;
    var showAll = true;
    var filterDepartmentID = null;
    var filterUserID = null;
    var showVacant;

    var loadedCount = 0;

    var dialog = new AS.FORMS.BasicChooserDialogKMG();
    dialog.init({
        showListItemIcon : false,
        showListItemInfo : true,
        showListItemStatus : true,
        multiSelect : multiSelect,
        showSearchField : true,
        title : i18n.tr("Выбор должности"),
        placeHolderText : i18n.tr("Поиск должностей"),
        showSelectedStack : true
    });

    this.correctListArray = function(positions) {

        console.log(name, fields, selectedPositionId);
        console.log(positions);

        if(!positions) {
            return [];
        }
        var filteredPositions = [];
        positions.forEach(function (position, index) {
            if (position.transitional) {
                position.id = position.elementID + "," + position.periodID;
            } else {
                position.id = position.elementID;
            }

            position.name = position.elementName;
            position.info = position.departmentName;


            var include = true;
            if(position.name == name && position.elementID != selectedPositionId) {
                Object.keys(fields).forEach(function(fieldName){
                    if(!position.customFields[fieldName+"_key"] == fields[fieldName]) {
                        include = false;
                    }
                });
            } else {
                include = false;
            }

            if(include) {
                filteredPositions.push(position);
            }

        });
        console.log(positions.length +" "+filteredPositions.length);

        return filteredPositions;
    };


    var listSize = 0;
    var loadPositions = function(search, selectedNodeId, startRecord, recordsCount){
        if(startRecord != 0){
            startRecord = loadedCount;
        }else {
            loadedCount = 0;
            listSize = 0;
        }

        console.log(startRecord +" "+recordsCount);

        AS.SERVICES.showWaitWindow();

        var cmpIds = "departmentID="+selectedNodeId+"&formID="+encodeURIComponent(KMG_MOVE_POSITION_CHOOSER_OPTIONS.formCode)+"&startRecord="+startRecord+"&recordsCount="+recordsCount+"&showAll="+showAll;
        Object.keys(fields).forEach(function(key){
            cmpIds+="&cmpId="+encodeURIComponent(key);
        });
        AS.FORMS.ApiUtils.simpleAsyncGet("rest/api/positions/searchPositionWithCardFields?"+cmpIds, function(positions){
            AS.SERVICES.hideWaitWindow();

            var empty = positions.length == 0;

            if(empty) {
                dialog.setListData("positions", positions, startRecord > 0 );
                return;
            }

            loadedCount+=positions.length;

            positions = instance.correctListArray(positions);

            listSize += positions.length;

            dialog.setListData("positions", positions, startRecord > 0 );

            if(recordsCount > 0 && (listSize < 8 || positions.length == 0 )) {
                loadPositions(search, selectedNodeId, loadedCount, recordsCount)
            }
        });
    };

    dialog.addTree("departments", function(parentNode, tree){
        var departmentID = undefined;
        if(parentNode) {
            departmentID = parentNode.id;
        }
        AS.FORMS.ApiUtils.getDepartments(departmentID, locale, function(departments){
            dialog.setTreeNodeData("departments", departmentID, departments.departments, "departmentID", "departmentName", "hasChildDepartments");
        });
    }, function(parentNode, tree){
        dialog.setListTitle(parentNode.name);
        dialog.showList("positions", parentNode, true);
    });

    dialog.addListType("positions", function(search, startRecord, recordsCount, selectedNodeId, stackId){
        loadPositions(search, selectedNodeId, startRecord, 100);
    }, function(search, selectedNodeId, stackId){
        loadPositions(search, selectedNodeId, 0, 0);
    });

    dialog.addStack("structure", i18n.tr("Орг. структура"), "departments", true, true, function(){});

    dialog.showStack("structure");


    this.setShowAll = function(newValue){
        showAll = newValue;
    };

    this.setFilterDepartmentID = function(newValue){
        filterDepartmentID = newValue;
    };

    this.setFilterUserID = function(newValue){
        filterUserID = newValue;
    };

    this.setShowVacant = function(newValue){
        showVacant = newValue;
    };

    this.setSelectedItems = function(newSelectedPersons){
        dialog.setSelectedItems(instance.correctListArray(newSelectedPersons));
    };

    this.getSelectedItems = function(){
        return dialog.getSelectedItems();
    };

    this.showDialog = function(){
        dialog.show();
    };


    this.on = function(event, handler) {
        dialog.on(event, handler);
    };


};




(function (asForms) {
    "use strict";
    asForms.MovePositionLinkViewKMG = function(model, playerView) {
        asForms.View.call(this, this, model, playerView);

        var config = model.asfProperty.config;

        var instance = this;

        var lastSearch = "";

        var locale = model.getLocale();

        var positionsTagArea = new asForms.TagArea({width : "calc(100% - 30px)"}, 0, config['multi'], config['editable-label'], config['custom']);

        this.addValue = function(item) {
            var value = model.getValue().slice();
            value.push(item);
            model.setValueFromInput(value);
            instance.updateValueFromModel();
        };


        if (model.asfProperty.config['read-⁠only']) {
            positionsTagArea.setEditable(false);
        }

        positionsTagArea.on(asForms.EVENT_TYPE.tagDelete, function(){
            model.setValueFromInput(positionsTagArea.getValues());
        });

        positionsTagArea.on(asForms.EVENT_TYPE.tagEdit, function(){
            model.setValueFromInput(positionsTagArea.getValues());
        });

        var chooserButton = jQuery("<button/>", {class : "asf-browseButton"});
        chooserButton.css("background", "url(\"js/asforms/resources/browse.png\") no-repeat center rgb(237, 164, 99)");

        asForms.ASFDataUtils.addAsFormId(positionsTagArea.getWidget(), "input", model.asfProperty);
        asForms.ASFDataUtils.addAsFormId(chooserButton, "button", model.asfProperty);


        var showPositionChooser = function(name, fields, masterPositionId) {

            var posChooser = new AS.FORMS.MovePositionChooserKMG(config['multi'], name, fields, masterPositionId, locale);
            posChooser.setFilterDepartmentID(model.getFilterDepartmentId());
            posChooser.setFilterUserID(model.getFilterUserId());
            posChooser.setShowVacant(config["only-⁠vacant"]);
            posChooser.setShowAll(true);
            posChooser.setSelectedItems(model.getValue());
            posChooser.showDialog();
            posChooser.on(AS.FORMS.BasicChooserEvent.applyClicked, function(){
                var selectedValues = posChooser.getSelectedItems();
                selectedValues.forEach(function (item){
                    if (item.transitional) {
                        item.tagName = item.elementName + " (" + item.departmentName + ")";
                    } else {
                        item.tagName = item.elementName;
                    }
                });

                model.setValue(selectedValues);
            });
        };

        var getAsfDataWithId = function(id, asfData) {
            for(var i=0; i<asfData.data.length; i++){
                if(asfData.data[i].id == id) {
                    return asfData.data[i];
                }
            }
            return {};
        };

        chooserButton.click(function() {

            var selectedPositionID = null;
            var selectedPositionName = null;
            KMG_MOVE_POSITION_CHOOSER.forEach(function(kmg){
                if(kmg.formCode == model.playerModel.formCode && kmg.cmpIds.indexOf( model.asfProperty.id) !== -1) {
                    var positionModel = model.playerModel.getModelWithId(kmg.cmpDepends[model.asfProperty.id], model.asfProperty.ownerTableId, model.asfProperty.tableBlockIndex);
                    if(!positionModel) {
                        positionModel = model.playerModel.getModelWithId(kmg.cmpDepends[model.asfProperty.id]);
                    }

                    selectedPositionID = _.first(positionModel.getValue()).elementID;
                    selectedPositionName = _.first(positionModel.getValue()).elementName;
                }
            });

            if(!selectedPositionID) {
                alert("Выберите должность");
                return;
            }


            asForms.ApiUtils.simpleAsyncGet("rest/api/positions/get_cards?positionID="+selectedPositionID+"&locale=ru", function(cards){
                console.log(cards);
                var card = cards.findElement("formCode", KMG_MOVE_POSITION_CHOOSER_OPTIONS.formCode);
                console.log(card);
                var masterCardId = card["data-uuid"];
                console.log(masterCardId);


                asForms.ApiUtils.simpleAsyncGet("rest/api/asforms/data/get?dataUUID=" + masterCardId, function (asfDatas) {
                    var masterAsfData = _.first(asfDatas);

                    var type = getAsfDataWithId("type", masterAsfData).value;

                    if(!type) {
                        console.log("no type");
                        return;
                    }

                    var fieldNames = KMG_MOVE_POSITION_CHOOSER_OPTIONS.fields[type];

                    var fields = {type :  type};

                    fieldNames.forEach(function(fieldName){
                        var value = getAsfDataWithId(fieldName, masterAsfData).key;
                        fields[fieldName]=value;
                    });

                    console.log(fields);

                    showPositionChooser(selectedPositionName, fields, selectedPositionID);


                });

            });




        });

        this.updateValueFromModel = function(){
            positionsTagArea.setValues(model.getValue());
        };

        this.unmarkInvalid = function() {
            positionsTagArea.markValidity(true);
        };

        this.markInvalid = function() {
            positionsTagArea.markValidity(false);
        };

        this.setEnabled = function(newEnabled) {
            positionsTagArea.setEditable(newEnabled);
            if(newEnabled) {
                chooserButton.css("pointer-⁠events", "auto");
            } else {
                chooserButton.css("pointer-⁠events", "none");
            }
        };
        this.setEnabled(!asForms.ASFDataUtils.isReadOnly(model));

        model.on(asForms.EVENT_TYPE.dataLoad, function(){
            instance.updateValueFromModel();
        });

        this.updateValueFromModel();

        this.container.append(positionsTagArea.getWidget());
        this.container.append(chooserButton);
    };

    asForms.MovePositionLinkViewKMG.prototype = Object.create(asForms.View.prototype);
    asForms.MovePositionLinkViewKMG.prototype.constructor = asForms.MovePositionLinkViewKMG;



    asForms.ComponentUtils.createViewOriginalForMovePositionKMG = asForms.ComponentUtils.createView;

    asForms.ComponentUtils.createView = function(model, playerView, editable) {


        if(playerView == true || playerView == false) {
            editable = playerView;
        }

        if( editable ){
            for(var i=0; i<KMG_MOVE_POSITION_CHOOSER.length; i++){
                var formInfo = KMG_MOVE_POSITION_CHOOSER[i];
                if(formInfo.formCode === model.playerModel.formCode && formInfo.cmpIds.indexOf(model.asfProperty.id) != -1) {
                    console.log("FOUND INPUT FOR MOVE CHOOSER");
                    try {
                        return new asForms.MovePositionLinkViewKMG(model, playerView, editable);
                    } catch (e) {
                        console.log(e);
                    }
                }
            }
        }

        try {
            return asForms.ComponentUtils.createViewOriginalForMovePositionKMG(model, playerView, editable);
        } catch (e) {
            console.log(e);
        }


    }

} (AS.FORMS));







var  KMG_MOVE_POSITION_CHOOSER_UTILS = {
    initDependence : function(playerModel, playerView, formInfo){
        formInfo.cmpIds.forEach(function(cmpId){
            var positionModel = playerModel.getModelWithId(formInfo.cmpDepends[cmpId]);
            positionModel.on("valueChange", function(){
                if(playerModel.loading) {
                    return;
                }

                var slavePositionModel = playerModel.getModelWithId(cmpId);
                slavePositionModel.setValue(null);

            });

        });
    }
};



AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
    for(var i=0; i<KMG_MOVE_POSITION_CHOOSER.length; i++){
        var formInfo = KMG_MOVE_POSITION_CHOOSER[i];
        if(formInfo.formId != model.playerModel.formId) {
            continue;
        }


        KMG_MOVE_POSITION_CHOOSER_UTILS.initDependence(model, view, formInfo);



    }
});
