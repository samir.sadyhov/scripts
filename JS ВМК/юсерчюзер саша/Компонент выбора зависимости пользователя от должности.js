var KMG_USER_SLAVE_OPTIONS = [
    {formCode : "Заявление_о_приеме_на_работу_на_период",  fields : [
        {tableId : "table2_r", slaveCmpId : "user", masterPositionId : "position"},
        {tableId : "table2_k", slaveCmpId : "user_k", masterPositionId : "position_k"}
    ]}
];




var KMG_USER_SLAVE_UTILS = {

    initBlock : function(playerModel, field, block) {


        var positionModel = playerModel.getModelWithId(field.masterPositionId);
        var userModel = playerModel.getModelWithId(field.slaveCmpId, field.tableId, block[0].asfProperty.tableBlockIndex);

        positionModel.on("valueChange", function(event, model, value){
            userModel.setValue(null);
        });



        userModel.getFilterPositionId = function(){

            if(positionModel.getSelectedIds().length>0) {
                return positionModel.getSelectedIds()[0];
            }
            return "notExistingString";
        };

    },

    initSlave : function(playerModel, playerView, field) {
        if(!playerModel.userSlaveInited) {

            var tableModel = playerModel.getModelWithId(field.tableId);

            tableModel.on("tableRowAdd", function(evt, model, modelRow){
                KMG_USER_SLAVE_UTILS.initBlock(playerModel, field, modelRow);
            });

            playerModel.userSlaveInited = true;
        }
    }
};



AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
    for(var i=0; i<KMG_USER_SLAVE_OPTIONS.length; i++){
        var formInfo = KMG_USER_SLAVE_OPTIONS[i];

        if(formInfo.formCode != model.formCode) {
            continue;
        }

        formInfo.fields.forEach(function(field){
            KMG_USER_SLAVE_UTILS.initSlave(model, view, field);
        });


    }
});
