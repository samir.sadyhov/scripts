
var KMG_POSITION_CHOOSER_OPTIONS = {formCode : "Карточка_должности", field : "type", values : {"1" : "рабочий", "2" : "служащий"}};

var KMG_POSITION_CHOOSER = [];
KMG_POSITION_CHOOSER.push({formCode : "Заявление1_о_приеме_на_работу_на_период", cmpIds : ["position", "position_k"], cmpDepends : {"position" : "сhoice1", "position_k" : "сhoice1_k"}  });

KMG_POSITION_CHOOSER.push({formCode : "Заявление_о_приеме_на_работу_на_период", cmpIds : ["position", "position_k"], cmpDepends : {"position" : "сhoice1", "position_k" : "сhoice1_k"}  });



KMG_POSITION_CHOOSER.push({formCode : "Служебная_записка_о_временном_переводе", cmpIds : ["change_position", "change_position_k"], cmpDepends : {"change_position" : "сhoice1", "change_position_k" : "сhoice1_k"}  });

KMG_POSITION_CHOOSER.push({formCode : "Заявление_о_предоставлении_отпуска_без_сохранения_заработной_платы", cmpIds : ["position", "position_k"], cmpDepends : {"position" : "сhoice1", "position_k" : "сhoice1_k"}  });

//KMG_POSITION_CHOOSER.push({formCode : "Приказ_о_выходе_на_работу", cmpIds : ["position", "position_k"], cmpDepends : {"position" : "сhoice8", "position_k" : "сhoice8_k"}  });

KMG_POSITION_CHOOSER.push({formCode : "Приказ_о_приеме_на_работу_на_неопределенный_срок", cmpIds : ["position3", "position3_k"], cmpDepends : {"position3" : "choice11", "position3_k" : "choice11_k"}  });

KMG_POSITION_CHOOSER.push({formCode : "Приказ_о_приеме_на_работу_на_определенный_срок", cmpIds : ["position3", "position3_k"], cmpDepends : {"position3" : "choice11", "position3_k" : "choice11_k"}  });

KMG_POSITION_CHOOSER.push({formCode : "Заявление_о_приеме_на_работу1", cmpIds : ["position", "position_k"], cmpDepends : {"position" : "сhoice1", "position_k" : "сhoice1_k"}  });


KMG_POSITION_CHOOSER.push({formCode : "Служебная_записка_о_временном_переводе", cmpIds : ["change_position", "change_position_k"], cmpDepends : {"change_position" : "сhoice1", "change_position_k" : "сhoice1_k"}  });

KMG_POSITION_CHOOSER.push({formCode : "Трудовой_договор1", cmpIds : ["position1"], cmpDepends : {"position1" : "choice0"}  });

if(!AS.FORMS.ApiUtils.getPositions) {
    AS.FORMS.ApiUtils.getPositions = AS.FORMS.ApiUtils.getPositionsSuggestions;
}




AS.FORMS.PositionChooserKMG = function(multiSelect, filterIds, locale){


    var instance = this;
    var showAll = true;
    var filterDepartmentID = null;
    var filterUserID = null;
    var showVacant;

    var loadedCount = 0;

    var dialog = new AS.FORMS.BasicChooserDialogKMG();
    dialog.init({
        showListItemIcon : false,
        showListItemInfo : true,
        showListItemStatus : true,
        multiSelect : multiSelect,
        showSearchField : true,
        title : i18n.tr("Выбор должности"),
        placeHolderText : i18n.tr("Поиск должностей"),
        showSelectedStack : true
    });

    this.correctListArray = function(positions) {
        if(!positions) {
            return [];
        }
        var filteredPositions = [];
        positions.forEach(function (position, index) {
            if (position.transitional) {
                position.id = position.elementID + "," + position.periodID;
            } else {
                position.id = position.elementID;
            }

            position.name = position.elementName;
            position.info = position.departmentName;

            if(filterIds.indexOf(position.elementID) !=-1) {
                filteredPositions.push(position);
            }

        });
        console.log(positions.length +" "+filteredPositions.length);

        return filteredPositions;
    };


    var listSize = 0;
    var loadPositions = function(search, selectedNodeId, startRecord, recordsCount){
        if(startRecord != 0){
            startRecord = loadedCount;
        }else {
            loadedCount = 0;
            listSize = 0;
        }

        console.log(startRecord +" "+recordsCount);

        AS.SERVICES.showWaitWindow();

        AS.FORMS.ApiUtils.getPositionsExt(search, filterUserID, filterDepartmentID, showVacant, showAll, selectedNodeId, startRecord, recordsCount, locale, function (positions) {

            AS.SERVICES.hideWaitWindow();

            var empty = positions.length == 0;

            if(empty) {
                dialog.setListData("positions", positions, startRecord > 0 );
                return;
            }

            loadedCount+=positions.length;

            positions = instance.correctListArray(positions);

            listSize += positions.length;

            dialog.setListData("positions", positions, startRecord > 0 );

            if(recordsCount > 0 && (listSize < 8 || positions.length == 0 )) {
                loadPositions(search, selectedNodeId, loadedCount, recordsCount)
            }
        });
    };

    dialog.addTree("departments", function(parentNode, tree){
        var departmentID = undefined;
        if(parentNode) {
            departmentID = parentNode.id;
        }
        AS.FORMS.ApiUtils.getDepartments(departmentID, locale, function(departments){
            dialog.setTreeNodeData("departments", departmentID, departments.departments, "departmentID", "departmentName", "hasChildDepartments");
        });
    }, function(parentNode, tree){
        dialog.setListTitle(parentNode.name);
        dialog.showList("positions", parentNode, true);
    });

    dialog.addListType("positions", function(search, startRecord, recordsCount, selectedNodeId, stackId){
        loadPositions(search, selectedNodeId, startRecord, 100);
    }, function(search, selectedNodeId, stackId){
        loadPositions(search, selectedNodeId, 0, 0);
    });

    dialog.addStack("structure", i18n.tr("Орг. структура"), "departments", true, true, function(){});

    dialog.showStack("structure");


    this.setShowAll = function(newValue){
        showAll = newValue;
    };

    this.setFilterDepartmentID = function(newValue){
        filterDepartmentID = newValue;
    };

    this.setFilterUserID = function(newValue){
        filterUserID = newValue;
    };

    this.setShowVacant = function(newValue){
        showVacant = newValue;
    };

    this.setSelectedItems = function(newSelectedPersons){
        dialog.setSelectedItems(instance.correctListArray(newSelectedPersons));
    };

    this.getSelectedItems = function(){
        return dialog.getSelectedItems();
    };

    this.showDialog = function(){
        dialog.show();
    };

    /**
     *
     * @param event SEE AS.FORMS.BasicChooserEvent
     * @param handler
     */
    this.on = function(event, handler) {
        dialog.on(event, handler);
    };


};




(function (asForms) {
    "use strict";
    asForms.PositionLinkViewKMG = function(model, playerView) {
        asForms.View.call(this, this, model, playerView);

        var config = model.asfProperty.config;

        var instance = this;

        var lastSearch = "";

        var locale = model.getLocale();

        var positionsTagArea = new asForms.TagArea({width : "calc(100% - 30px)"}, 0, config['multi'], config['editable-label'], config['custom']);

        this.addValue = function(item) {
            var value = model.getValue().slice();
            value.push(item);
            model.setValueFromInput(value);
            instance.updateValueFromModel();
        };


        if (model.asfProperty.config['read-only']) {
            positionsTagArea.setEditable(false);
        }

        positionsTagArea.on(asForms.EVENT_TYPE.tagDelete, function(){
            model.setValueFromInput(positionsTagArea.getValues());
        });

        positionsTagArea.on(asForms.EVENT_TYPE.tagEdit, function(){
            model.setValueFromInput(positionsTagArea.getValues());
        });

        var chooserButton = jQuery("<button/>", {class : "asf-browseButton"});
        chooserButton.css("background", "url(\"js/asforms/resources/browse.png\") no-repeat center");

        asForms.ASFDataUtils.addAsFormId(positionsTagArea.getWidget(), "input", model.asfProperty);
        asForms.ASFDataUtils.addAsFormId(chooserButton, "button", model.asfProperty);


        var showPositionChooser = function(filterIds) {
            if(filterIds.length ==0) {
                alert("Нет ни одной должности удовлетворяющей условию");
                return;
            }
            var posChooser = new AS.FORMS.PositionChooserKMG(config['multi'], filterIds, locale);
            posChooser.setFilterDepartmentID(model.getFilterDepartmentId());
            posChooser.setFilterUserID(model.getFilterUserId());
            posChooser.setShowVacant(config["only-vacant"]);
            posChooser.setShowAll(true);
            posChooser.setSelectedItems(model.getValue());
            posChooser.showDialog();
            posChooser.on(AS.FORMS.BasicChooserEvent.applyClicked, function(){
                var selectedValues = posChooser.getSelectedItems();
                selectedValues.forEach(function (item){
                    if (item.transitional) {
                        item.tagName = item.elementName + " (" + item.departmentName + ")";
                    } else {
                        item.tagName = item.elementName;
                    }
                });

                model.setValue(selectedValues);
            });
        };

        chooserButton.click(function() {

            var typeValue = 1;
            KMG_POSITION_CHOOSER.forEach(function(kmg){
                if(kmg.formCode == model.playerModel.formCode && kmg.cmpIds.indexOf( model.asfProperty.id) !== -1) {
                    var choiceModel = model.playerModel.getModelWithId(kmg.cmpDepends[model.asfProperty.id], model.asfProperty.ownerTableId, model.asfProperty.tableBlockIndex);
                    if(!choiceModel) {
                        choiceModel = model.playerModel.getModelWithId(kmg.cmpDepends[model.asfProperty.id]);
                    }

                    typeValue = _.first(choiceModel.getValue());
                }
            });

            if(!typeValue) {
                alert("Выберите тип должности");
                return;
            }


            AS.FORMS.ApiUtils.simpleAsyncGet("rest/api/positions/get_by_field_value?formCode="+KMG_POSITION_CHOOSER_OPTIONS.formCode+"&fieldName="+KMG_POSITION_CHOOSER_OPTIONS.field+"&value="+typeValue+"&locale=ru", function(filterIds){

                if(filterIds.length !=0 ){
                    showPositionChooser(filterIds);
                } else {

                    typeValue = KMG_POSITION_CHOOSER_OPTIONS.values[typeValue];

                    AS.FORMS.ApiUtils.simpleAsyncGet("rest/api/positions/get_by_field_value?formCode="+KMG_POSITION_CHOOSER_OPTIONS.formCode+"&fieldName="+KMG_POSITION_CHOOSER_OPTIONS.field+"&value="+typeValue+"&locale=ru", function(filterIds){
                        showPositionChooser(filterIds);
                    });
                }

            });

        });

        this.updateValueFromModel = function(){
            positionsTagArea.setValues(model.getValue());
        };

        this.unmarkInvalid = function() {
            positionsTagArea.markValidity(true);
        };

        this.markInvalid = function() {
            positionsTagArea.markValidity(false);
        };

        this.setEnabled = function(newEnabled) {
            positionsTagArea.setEditable(newEnabled);
            if(newEnabled) {
                chooserButton.css("pointer-events", "auto");
            } else {
                chooserButton.css("pointer-events", "none");
            }
        };
        this.setEnabled(!asForms.ASFDataUtils.isReadOnly(model));

        model.on(asForms.EVENT_TYPE.dataLoad, function(){
            instance.updateValueFromModel();
        });

        this.updateValueFromModel();

        this.container.append(positionsTagArea.getWidget());
        this.container.append(chooserButton);
    };

    asForms.PositionLinkViewKMG.prototype = Object.create(asForms.View.prototype);
    asForms.PositionLinkViewKMG.prototype.constructor = asForms.PositionLinkView;



    asForms.ComponentUtils.createViewOriginalForPositionKMG = asForms.ComponentUtils.createView;

    asForms.ComponentUtils.createView = function(model, playerView, editable) {


        if(playerView == true || playerView == false) {
            editable = playerView;
        }

        if( editable ){
            for(var i=0; i<KMG_POSITION_CHOOSER.length; i++){
                var formInfo = KMG_POSITION_CHOOSER[i];
                if(formInfo.formCode === model.playerModel.formCode && formInfo.cmpIds.indexOf(model.asfProperty.id) != -1) {
                    console.log("FOUND INPUT FOR RESERVE CHOOSER");
                    try {
                        return new asForms.PositionLinkViewKMG(model, playerView, editable);
                    } catch (e) {
                        console.log(e);
                    }
                }
            }
        }

        try {
            return asForms.ComponentUtils.createViewOriginalForPositionKMG(model, playerView, editable);
        } catch (e) {
            console.log(e);
        }


    }

} (AS.FORMS));







var  KMG_POSITION_CHOOSER_UTILS = {
    initDependence : function(playerModel, playerView, formInfo){
        formInfo.cmpIds.forEach(function(cmpId){
            var typeModel = playerModel.getModelWithId(formInfo.cmpDepends[cmpId]);
            typeModel.on("valueChange", function(){
                if(playerModel.loading) {
                    return;
                }

                var positionModel = playerModel.getModelWithId(cmpId);
                positionModel.setValue(null);

            });

        });
    }
};



AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
    for(var i=0; i<KMG_POSITION_CHOOSER.length; i++){
        var formInfo = KMG_POSITION_CHOOSER[i];
        if(formInfo.formCode != model.playerModel.formCode) {
            continue;
        }
        KMG_POSITION_CHOOSER_UTILS.initDependence(model, view, formInfo);
    }
});
