// новая функция поднятия должностей

AS.FORMS.ApiUtils.getPositionsExt = function(search, filterUserId, filterDepartmentId, showVacant, showAll, departmentID, startRecord, recordsCount, locale, handler) {
        var orgParams = {
            locale : locale,
            showOnlyVacant: showVacant,
            startRecord: startRecord,
            recordsCount: recordsCount,
            departmentID: departmentID,
            filterUserID: filterUserId,
            filterDepartmentID: filterDepartmentId,
            search: search
        };
        AS.SERVICES.showWaitWindow();
        return this.simpleAsyncGet('rest/api/userchooser/search_all_positions?'+jQuery.param(orgParams), this.getHandler(handler, true));
    };







AS.FORMS.BasicChooserDialogKMG = function() {

    var CONTAINER_WIDTH= 800;
    var CONTAINER_HEIGHT=500;

    var SPLIT_WIDTH = 782;
    var SPLIT_HEIGHT = 350;
    var SPLIT_TOP = 44;


    var SPLIT_WITHOUT_SEARCH_HEIGHT = 385;
    var SPLIT_WITHOUT_SEARCH_TOP = 9;

    var GAP = 9;

    var LEFT_WIDTH = 258;
    var STACK_HEIGHT = 26;


    var instance = this;
    AS.FORMS.ComponentUtils.makeBus(this);

    var countInPart = 30;

    var options = {
        multiSelect : false,
        showSearchField : true,
        showSelectedStack : true, // отображение стека и списка выделенных элементов
        showListItemIcon : false,  // имеет ли элемент списка иконку
        showListItemIconStyle : "", // стиль иконки элемента списка
        listItemIconSize: 50, // размер иконки элемента списка
        showListItemInfo : false,  // имеет ли элемент списка лейбл информации
        showListItemStatus : false, // имеет ли элемент списка лейбл статуса
        lazyListLoading : true,  // включать ли ненивую подгрузку списка
        placeHolderText : i18n.tr("Поиск"),  // плейсхолдер поле поиск
        title : i18n.getString("Выберите..."),  // загловок диалога
        canSelect :  null,   // функция которая определяет может ли быть выбран элемент или нет
        dblClick : null      // функция двоного клика по элементу
    };


    this.contentContainer = jQuery('<div/>', {class : 'ns-basicChooserDialogContent'});

    var splitPane = new AS.FORMS.SplitPane();

    var stacksContainer = jQuery('<div/>', {class : 'ns-basicChooserStacks'});
    var treeContainer = jQuery('<div/>', {class : 'ns-basicChooserTree'});

    var listContainer = jQuery('<div/>', {class : 'ns-basicChooserListContainer'});
    var listTitleContainer = jQuery('<div/>', {class : 'ns-basicChooserListTitleContainer'});
    var listTitle = jQuery('<div/>', {class : 'ns-basicChooserListTitle'});
    var selectAll = jQuery('<input/>', {type : "checkbox", class : 'ns-basicChooserSelectAll'});
    var selectAllLabel = jQuery('<label/>', {class : "ns-checkBoxLabel ns-basicChooserSelectAllContainer"});

    var searchBox = jQuery('<input/>', {type : 'text', class : 'ns-input ns-basicChooserSearchInput', placeholder : options.placeHolderText});
    var button = jQuery('<button>', {class : "ns-approveButton ns-basicChooserApplyButton"}).button().html(i18n.tr("Выбрать"));

    var searchImage = jQuery('<img>', {class : "ns-basicChooserSearchIcon", src : "js/asforms/resources/search.png"});

    // деревья
    var trees = {};

    // стеки
    var stacks = {};

    // списки
    var lists = {};

    // выделенные элементы
    var selectedItems = [];


    /**
     * инициализация диалога
     *
     * multiSelect - множественный выбор (boolean)
     * showSearchField - отображать поле поиска (boolean)
     * showSelectedStack - отображение стека и списка выделенных элементов (boolean)
     * showListItemIcon - имеет ли элемент списка иконку (boolean)
     * showListItemIconStyle - стиль иконки элемента списка (boolean)
     * listItemIconSize - размер иконки элемента списка (integer)
     * showListItemInfo - имеет ли элемент списка лейбл информации (boolean)
     * showListItemStatus - имеет ли элемент списка лейбл статуса (boolean)
     * lazyListLoading - включать ли ненивую подгрузку списка (boolean)
     * placeHolderText - плейсхолдер поле поиск (string)
     * title - заголовок диалога (string)
     * canSelect - функция которая определяет может ли быть выбран элемент или нет (function)
     * dblClick - функция двойного клика по элементу (function)
     *
     * @param customOptions
     */
    this.init = function(customOptions){
        if(!_.isUndefined(customOptions.multiSelect)) {
            options.multiSelect = customOptions.multiSelect;
        }
        if(!_.isUndefined(customOptions.showSearchField)) {
            options.showSearchField = customOptions.showSearchField;
        }
        if(!_.isUndefined(customOptions.showSelectAll)) {
            options.showSelectAll = customOptions.showSelectAll;
        }
        if(!_.isUndefined(customOptions.showListItemIcon)) {
            options.showListItemIcon = customOptions.showListItemIcon;
        }
        if(!_.isUndefined(customOptions.showListItemInfo)) {
            options.showListItemInfo = customOptions.showListItemInfo;
        }
        if(!_.isUndefined(customOptions.showListItemStatus)) {
            options.showListItemStatus = customOptions.showListItemStatus;
        }
        if(!_.isUndefined(customOptions.title)) {
            options.title = customOptions.title;
        }
        if(!_.isUndefined(customOptions.listItemIconSize)) {
            options.listItemIconSize = customOptions.listItemIconSize;
        }
        if(!_.isUndefined(customOptions.showListItemIconStyle)) {
            options.showListItemIconStyle = customOptions.showListItemIconStyle;
        }
        if(!_.isUndefined(customOptions.showSelectedStack)) {
            options.showSelectedStack = customOptions.showSelectedStack;
        }
        if(!_.isUndefined(customOptions.placeHolderText)) {
            options.placeHolderText = customOptions.placeHolderText;
            searchBox.attr("placeholder", options.placeHolderText);
        }
        if(!_.isUndefined(customOptions.lazyListLoading)) {
            options.lazyListLoading = customOptions.lazyListLoading;
        }
        if(!_.isUndefined(customOptions.canSelect)) {
            options.canSelect = customOptions.canSelect;
        }

        if(!_.isUndefined(customOptions.dblClick)) {
            options.dblClick = customOptions.dblClick;
        }




        if(options.showSearchField) {
            instance.contentContainer.append(searchBox);
            instance.contentContainer.append(searchImage);
        } else {
            SPLIT_HEIGHT = SPLIT_WITHOUT_SEARCH_HEIGHT;
            SPLIT_TOP = SPLIT_WITHOUT_SEARCH_TOP;
            listContainer.css("height", SPLIT_WITHOUT_SEARCH_HEIGHT+"px");
        }

        if(options.multiSelect) {
            selectAllLabel.append(selectAll);
            selectAllLabel.append(i18n.tr("Выбрать всех"));
        }

        selectAll.click(function(){
            if(!selectAll.listId) {
                return;
            }
            var list = lists[selectAll.listId];
            if(selectAll.prop("checked")) {
                list.selectAll();
            } else {
                instance.setSelectedItems([]);
            }
        });


        instance.contentContainer.append(splitPane.splitContainer);
        splitPane.splitContainer.addClass("ns-basicChooserSplit");
        splitPane.leftContainer.append(stacksContainer);
        splitPane.leftContainer.append(treeContainer);

        splitPane.minDividerPosition = 86;
        splitPane.maxDividerPosition = 605;

        listTitleContainer.append(listTitle);
        listTitleContainer.append(selectAllLabel);

        splitPane.rightContainer.append(listTitleContainer);
        splitPane.rightContainer.append(listContainer);


        instance.contentContainer.append(button);


        splitPane.setPixelSize(SPLIT_WIDTH, SPLIT_HEIGHT);
        splitPane.splitContainer.css("top", SPLIT_TOP+"px");

        splitPane.setDividerPosition(LEFT_WIDTH);

        treeContainer.css("width", "100%");
        treeContainer.css("height", SPLIT_HEIGHT - 2);

        if(options.showSelectedStack) {

            instance.addListType("selected", function () {
                instance.setListData("selected", selectedItems, false);
            });

            instance.addStack("selected", i18n.tr("Выбранные"), null, false, false, function () {
                instance.showList("selected", true);
            });
        }

        button.attr("disabled", "true");

        button.click(function(){
            instance.close();
        });

        instance.on(AS.FORMS.BasicChooserEvent.itemSelected, function(event, item){
            if(options.multiSelect) {
                selectedItems.push(item);
            } else {
                if(selectedItems.length === 1) {
                    instance.trigger(AS.FORMS.BasicChooserEvent.itemDeselected, [selectedItems[0]]);
                }
                selectedItems = [item];
            }
            instance.updateButtonState();

        });

        instance.on(AS.FORMS.BasicChooserEvent.itemDeselected, function(event, item){
            selectedItems.some(function(selectedItem, selectedIndex){
                if(item.id == selectedItem.id) {
                    selectedItems.splice(selectedIndex, 1);

                    instance.updateButtonState();

                    selectAll.prop("checked", false);

                    return true;
                }
            });
        });

        instance.on(AS.FORMS.BasicChooserEvent.listShowed, function(event, listId){
            searchBox.listId = listId;
            selectAll.listId = listId;
            jQuery(selectAll).prop("checked", false);
        });

        instance.on(AS.FORMS.BasicChooserEvent.treeSelected, function(event, treeId){
            searchBox.treeId = treeId;
            selectAllLabel.show();
        });

        instance.on(AS.FORMS.BasicChooserEvent.stackSelected, function(event, stackId){
            if(stackId == 'selected') {
                selectAllLabel.hide();
            } else {
                selectAllLabel.show();
            }
        });

        searchBox.keypress(function(event){
            if(event.keyCode == AS.FORMS.KEY_CODES.ENTER && searchBox.listId && searchBox.treeId) {
                lists[searchBox.listId].load(false);
            }
        });

        searchImage.click(function(event){
            if(searchBox.listId && searchBox.treeId) {
                lists[searchBox.listId].load(false);
            }
        });

        if(options.lazyListLoading) {
            listContainer.scroll(function (event) {
                var list = lists[searchBox.listId];
                if (listContainer[0].scrollHeight - listContainer[0].scrollTop - listContainer.height() < 100 ) {
                    list.load(true);
                }
            });
        }

        splitPane.bus.on(AS.COMPONENT_EVENTS.resized, function(){
            var leftWidth = splitPane.position;
            var rightWidth = splitPane.width - splitPane.position - splitPane.dividerSize;
            var height = splitPane.height;
            console.log(leftWidth+" "+rightWidth+" "+height);

        });

    };

    this.isSelectionValid = function() {
        var valid = selectedItems.length>0;
        selectedItems.some(function(selectedItem){
            if(options.canSelect && !options.canSelect(selectedItem)) {
                valid = false;
                return true;
            }
        });
        return valid;
    };

    this.updateButtonState = function(){

        if(instance.isSelectionValid()) {
            button.removeAttr("disabled");
        } else {
            button.attr("disabled", "true");
        }
    };


    /**
     * добавление дерева
     * @param treeId идентификатор
     * @param openHandler лисенер открытия ноды
     * @param selectHandler лисенер закрытия ноды
     * @param getIcon функция получения иконки (если таковая нужна)
     */
    this.addTree = function(treeId, openHandler, selectHandler, getIcon ){
        var tree = jQuery('<div/>', {class : 'ns-tree'});

        var closed = jQuery("<img/>", {src : "js/asforms/resources/node.closed.png"});
        var opened = jQuery("<img/>", {src : "js/asforms/resources/node.open.png"});
        tree.tree({
            data: [],
            autoOpen: false,
            dragAndDrop: false,
            closedIcon: closed,
            openedIcon: opened,
            slide: false,
            onCreateLi: function(node, $li) {
                if(!getIcon) {
                    if ($li.find("a").length === 0) {
                        //var icon = jQuery("<span>");
                        //icon.css("margin-left", "12px");
                        $li.find("span").addClass("ns-margin12Important");
                    }
                } else {
                    var icon = getIcon(node);
                    if ($li.find("a").length === 0) {
                        icon.css("margin-left", "12px");
                        $li.find("span").before(icon);
                    } else {
                        $li.find("a").after(icon);
                    }
                }
            }
        });

        tree.loadRoots = function(){
            openHandler(null);
        };
        tree.bind(
            'tree.open',
            function(e) {
                var parent_node = e.node;
                if(openHandler) {
                    openHandler(parent_node, tree);
                }
            }
        );
        tree.bind(
            'tree.select',
            function(e) {
                instance.trigger(AS.FORMS.BasicChooserEvent.treeSelected, [treeId]);
                var parent_node = e.node;
                if(!parent_node) {
                    return;
                }
                tree.lastSelection = parent_node;
                instance.trigger(AS.FORMS.BasicChooserEvent.treeNodeSelected, [treeId, parent_node.id]);
                if(selectHandler) {
                    selectHandler(parent_node, tree);
                }
            }
        );

        instance.on(AS.FORMS.BasicChooserEvent.treeSelected, function(event, selectedTreeId){
            var selected = treeId == selectedTreeId;
            if(tree.selected == selected) {
                return;
            }
            tree.selected = selected;
            if(!tree.selected) {
                return;
            }
            if(tree.lastSelection) {
                tree.tree('selectNode', null);
                tree.tree('selectNode', tree.lastSelection);
            } else {
                var tree_data = tree.tree('getTree');
                if(tree_data.length > 0) {
                    tree.tree('selectNode', tree_data[0]);
                } else {
                    tree.loadRoots();
                }
            }
        });
        trees[treeId] = tree;
    };

    /**
     * показать дерево
     * @param treeId
     */
    this.showTree = function(treeId) {
        var tree = trees[treeId];
        tree.selected = false;
        treeContainer.children().detach();
        treeContainer.append(tree);
        instance.trigger(AS.FORMS.BasicChooserEvent.treeSelected, [treeId]);
    };

    /**
     * вставка данных дерева
     * @param treeId идентификатор дерева
     * @param parentNodeId идентификатор ноды
     * @param data данные
     * @param idField  - поле в данных которое является идентификатором
     * @param nameField - поле в данных которое является наименованием
     * @param hasChildrenField - поле в данных которое определяет имеются ли дочерние элементы
     */
    this.setTreeNodeData = function(treeId, parentNodeId, data, idField, nameField, hasChildrenField){
        var tree = trees[treeId];
        var parentNode = undefined;
        if(parentNodeId) {
            parentNode = tree.tree('getNodeById', parentNodeId);
            for(var i = parentNode.children.length-1; i>=0; i--) {
                if(parentNode.children[i]) {
                    tree.tree("removeNode", parentNode.children[i]);
                }
            }
        }

        var roots = [];
        data.forEach(function(item){
            item.id = item[idField];
            item.label = item[nameField];

            var v = tree.tree(
                'appendNode',
                item,
                parentNode
            );
            if(item[hasChildrenField]) {
                tree.tree(
                    'appendNode',
                    { label: '', phantom: true },
                    v
                );
            }
            if(!parentNode) {
                roots.push(v);
            }
        });

        if( roots.length > 0) {
            roots.forEach(function (root) {
                if (root.children) {
                    tree.tree('openNode', root, false);
                }
            });
            tree.tree('selectNode', roots[0]);
        }
    };

    /**
     * выделение ноды дерева
     * @param treeId идентификатор дерева
     * @param nodeId идентификатор ноды
     */
    this.selectTreeNode = function(treeId, nodeId){
        var tree = trees[treeId];
        var node = tree.tree('getNodeById', nodeId);
        if(node) {
            tree.tree('selectNode', node);
        }
    };

    /**
     * открытие ноды дерева
     * @param treeId идентификатор дерева
     * @param nodeId идентификатор ноды
     */
    this.openTreeNode = function(treeId, nodeId){
        var tree = trees[treeId];
        var node = tree.tree('getNodeById', nodeId);
        if(node) {
            tree.tree('openNode', node, false);
        }
    };

    /**
     * добавление стека
     * @param stackId идентификатор стека
     * @param stackTitle наименование
     * @param treeId идентификатор дерева (которое должно отображаться для этого стека моджет пусть и null )
     * @param enableSearch есть ли поиск для этого стека
     * @param showSelectAll отображать ли галку "Выбрать всех"
     * @param selectHandler лисенер выделения стека
     */
    this.addStack = function(stackId, stackTitle, treeId, enableSearch, showSelectAll, selectHandler) {
        var stack = jQuery('<div/>', {class : 'ns-basicChooserStack'});
        stack.html(stackTitle);
        stack.click(function(){
            instance.setListTitle(stackTitle);
            instance.trigger(AS.FORMS.BasicChooserEvent.stackSelected, [stackId]);
            selectHandler();
        });

        instance.on(AS.FORMS.BasicChooserEvent.stackSelected, function(event, selectedStackId){
            if(selectedStackId != stackId) {
                stack.removeClass("ns-basicChooserStackSelected");
            } else {
                stack.addClass("ns-basicChooserStackSelected");
                if(enableSearch) {
                    searchBox.removeAttr("disabled");
                } else {
                    searchBox.attr("disabled", "disabled");
                    searchBox.valueOf("");
                }

                if(showSelectAll) {
                    selectAll.show();
                } else {
                    selectAll.hide();
                }
                if(treeId) {
                    instance.showTree(treeId);
                }
            }
        });

        instance.on(AS.FORMS.BasicChooserEvent.treeNodeSelected, function(event, selectedTreeId){
            if(selectedTreeId != treeId) {
                stack.removeClass("ns-basicChooserStackSelected");
            }
        });

        stacks[stackId] = stack;
        stacksContainer.append(stack);

        treeContainer.css("height", SPLIT_HEIGHT-2-STACK_HEIGHT*Object.keys(stacks).length);
    };

    /**
     * переименование стека
     * @param stackId идентификатор стека
     * @param stackTitle наименование
     */
    this.renameStack = function(stackId, stackTitle){
        stacks[stackId].html(stackTitle);
    };

    /**
     * отобразить стек
     * @param stackId идентификатор стека
     */
    this.showStack = function(stackId) {
        stacks[stackId].click();
    };

    /**
     * добавление типа списка
     * @param listId идентификатор списка
     * @param loadHandler лисенер загрузки
     * @param selectAllHandler лисенер загрузки всех элементов
     */
    this.addListType = function(listId, loadHandler, selectAllHandler) {
        var list = jQuery('<div/>', {class : 'ns-basicChooserList'});
        list.load = function(append) {
            if(!list.treeId) {
                return;
            }

            if(!append) {
                list.loadedAll = false;
            }

            if(list.loading || list.loadedAll) {
                return;
            }

            list.loading = true;
            var selectedNode = trees[list.treeId].tree('getSelectedNode');
            var start = 0;
            if(append) {
                start = list.loadedCount;
            }
            loadHandler(searchBox.val(), start, countInPart, selectedNode ? selectedNode.id : null, list.stackId);
        };
        list.selectAll = function(){
            if(!list.treeId) {
                return;
            }
            var selectedNode = trees[list.treeId].tree('getSelectedNode');
            selectAllHandler(searchBox.val(), selectedNode ? selectedNode.id : null, list.stackId);
        };

        lists[listId]=list;
        instance.on(AS.FORMS.BasicChooserEvent.treeSelected, function(event, treeId){
            list.treeId = treeId;
        });

        instance.on(AS.FORMS.BasicChooserEvent.stackSelected, function(event, stackId){
            list.stackId = stackId;
        });
    };

    /**
     * отображение списка
     * @param listId идентификатор
     * @param reload перегружать содержимое
     */
    this.showList = function(listId, reload) {
        var list = lists[listId];
        if(reload) {
            list.load(false);
        }
        listContainer.children().detach();
        listContainer.append(list);
        instance.trigger(AS.FORMS.BasicChooserEvent.listShowed, [listId]);
    };

    /**
     *
     * @param listId
     * @param data массив объектов, в котором должны быть элементы со свойствами id, name, info, status, statusColor, icon
     * @param append
     */
    this.setListData = function(listId, data, append) {
        var list = lists[listId];
        list.loading = false;

        list.loadedAll = data.length === 0;

        if(!append){
            list.children().detach();
            list.loadedCount=0;
        }
        data.forEach(function(item){
            list.append( instance.buildListElement( item, list ));
            list.loadedCount++;
        });
    };

    this.setListTitle = function(title) {
        listTitle.html(title);
    };

    this.setSelectedItems =function(newSelectedItems){
        var oldSelectedItems = selectedItems.splice(0);
        selectedItems = [];
        oldSelectedItems.forEach(function(item){
            instance.trigger(AS.FORMS.BasicChooserEvent.itemDeselected, item);
        });
        newSelectedItems.forEach(function(item){
            instance.trigger(AS.FORMS.BasicChooserEvent.itemSelected, item);
        });
    };

    this.show = function(){
        jQuery("body").append(this.contentContainer);
        jQuery(this.contentContainer).dialog({
            modal : true,
            width: CONTAINER_WIDTH,
            height: CONTAINER_HEIGHT,
            resizable: false,
            title: options.title
        });
    };

    this.getSelectedItems = function(){
        return selectedItems;
    };

    this.isSelected = function(item) {
        return selectedItems.some(function(selectedItem){
            if(item.id == selectedItem.id) {
                return true;
            }
        });
    };

    this.buildListElement = function(item, list) {
        var container = jQuery('<div/>', {class : 'ns-basicChooserItem'});
        if(instance.isSelected(item)) {
            container.addClass("ns-basicChooserSelectedItem");
        }
        var name = jQuery('<div/>');
        var info = jQuery('<div/>', {class: 'ns-basicChooserItemInfo'});
        var status = jQuery('<div/>', {class: 'ns-basicChooserItemStatus'});
        container.append(name);
        name.html(item.name);
        name.attr("title", item.name);

        if(options.showListItemStatus && item.status) {

            status.css("color", item.statusColor);
            status.html(item.status);
            status.attr("title", item.status);
            container.append(status);
            name.addClass("ns-basicChooserItemNameStatus");

            if (options.showListItemIcon) {
                name.css("max-width", (250-options.listItemIconSize));
            }

        } else if (options.showListItemIcon) {
            name.css("max-width", "calc(100% - "+(options.listItemIconSize +27)+"px)");
        }

        if(options.showListItemInfo) {
            info.html(item.info);
            info.attr("title", item.info);
            container.append(info);
            container.addClass("ns-basicChooserItemFull");
            name.addClass("ns-basicChooserItemName");
        } else {
            container.addClass("ns-basicChooserItemShort");
            name.addClass("ns-basicChooserItemNameShort");
        }


        if(options.showListItemIcon) {
            var imgDefault = jQuery("<div/>", {class : "ns-basicChooserItemIcon "+options.showListItemIconStyle});
            container.append(imgDefault);

            if(item.icon) {
                var img = jQuery("<div/>", {class: "ns-basicChooserItemIcon " + options.showListItemIconStyle});
                img.css("width", options.listItemIconSize);
                img.css("height", options.listItemIconSize);
                img.css("background-image", "url(" + item.icon + ")");
                container.append(img);
            }
            name.css("margin-left", options.listItemIconSize+GAP);
            info.css("margin-left", options.listItemIconSize+GAP);
            status.css("margin-left", options.listItemIconSize+GAP);
            info.css("max-width", "calc(100% - "+(options.listItemIconSize+GAP*3)+"px)");
        }


        instance.on(AS.FORMS.BasicChooserEvent.itemSelected, function(event, selectedItem){
            if(item.id == selectedItem.id) {
                container.addClass("ns-basicChooserSelectedItem");
            }
        });

        instance.on(AS.FORMS.BasicChooserEvent.itemDeselected, function(event, deselectedItem){
            if(item.id == deselectedItem.id) {
                container.removeClass("ns-basicChooserSelectedItem");
            }
        });

        container.click(function(){
            if(instance.isSelected(item) ){
                if(options.multiSelect) {
                    instance.trigger(AS.FORMS.BasicChooserEvent.itemDeselected, [item]);
                }
            } else {
                instance.trigger(AS.FORMS.BasicChooserEvent.itemSelected, [item]);
            }
        });

        if(!options.multiSelect) {
            container.dblclick(function(){
                if(instance.isSelectionValid()) {
                    instance.close();
                }
            });
        }
        if(options.dblClick) {
            container.dblclick(function(){
                options.dblClick(item);
            });
        }
        return container;

    };

    this.close = function() {

        instance.contentContainer.dialog( "destroy" );
        instance.contentContainer.detach();
        instance.trigger(AS.FORMS.BasicChooserEvent.applyClicked);
    };
};
