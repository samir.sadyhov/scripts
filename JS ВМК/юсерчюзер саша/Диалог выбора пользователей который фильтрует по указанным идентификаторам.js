if(!AS.FORMS.ApiUtils.getUsers) {
    AS.FORMS.ApiUtils.getUsers = AS.FORMS.ApiUtils.getUsersSuggestions;
}

AS.FORMS.FilterUserChooser = function(multiSelect, filterIds, locale){
    var instance = this;
    var showAll = true;
    var filterDepartmentID = null;
    var filterPositionID = null;
    var showNoPosition = true;


    var loadedCount = 0;

    var dialog = new AS.FORMS.BasicChooserDialogKMG();

    dialog.init({
        showListItemIcon: true,
        showListItemInfo: true,
        showListItemStatus: true,
        listItemIconSize: 32,
        multiSelect: multiSelect,
        showSearchField: true,
        title: i18n.tr("Выбор пользователя"),
        placeHolderText : i18n.tr("Поиск пользователей"),
        showListItemIconStyle: "ns-userChooserIcon",
        showSelectedStack: multiSelect
    });

    this.correctListArray = function(users) {
        if(!users) {
            return [];
        }
        var filteredUsers = [];
        users.forEach(function (user, index) {
            user.id = user.personID;
            user.name = user.personName;
            user.info = user.positionName;

            if(user.personID.indexOf(AS.FORMS.USER_CHOOSER_SUFFIXES.group) === 0) {
                user.icon = "js/asforms/resources/group.png";
            } else if(user.personID.indexOf(AS.FORMS.USER_CHOOSER_SUFFIXES.contact) === 0) {
                user.icon = "js/asforms/resources/unknown.png";
            } else if(user.personID.indexOf(AS.FORMS.USER_CHOOSER_SUFFIXES.text) === 0) {
                user.icon = "js/asforms/resources/unknown.png";
            } else {
                user.icon = "rest/formPlayerIconService/photo?max_width=32&max_height=32&inscribe=false&personID="+user.personID;
                user.isPerson = true;
            }

            if (user.customFields && user.customFields.calendarStatusLabel) {
                user.status = user.customFields.calendarStatusLabel;
                user.statusColor = user.customFields.calendarColor;
            }

            if(!filterIds  || filterIds.indexOf(user.personID) != -1) {
                filteredUsers.push(user);
            }


        });
        return filteredUsers;
    };


    dialog.addTree("departments", function(parentNode, tree){

        var departmentID = undefined;
        if(parentNode) {
            departmentID = parentNode.id;
        }
        AS.FORMS.ApiUtils.getDepartments(departmentID, locale, function(departments){
            dialog.setTreeNodeData("departments", departmentID, departments.departments, "departmentID", "departmentName", "hasChildDepartments");
        });
    }, function(parentNode, tree){
        dialog.setListTitle(parentNode.name);
        dialog.showList("users", true);
    });

    var listSize = 0;

    var loadUsers = function(search, selectedNodeId, startRecord, recordsCount){

        if(startRecord !== 0){
            startRecord = loadedCount;
        }else {
            loadedCount = 0;
            listSize = 0;
        }

        AS.SERVICES.showWaitWindow();



        AS.FORMS.ApiUtils.getUsers(search, filterPositionID, filterDepartmentID, selectedNodeId, 0, showNoPosition, startRecord, recordsCount, showAll, false, false, locale, function (users) {

            AS.SERVICES.hideWaitWindow();

            var empty = users.length == 0;

            if(empty) {
                dialog.setListData("users", users, startRecord > 0 );
                return;
            }

            loadedCount+=users.length;

            users = instance.correctListArray(users);

            listSize+=users.length;


            dialog.setListData("users", users, startRecord > 0 );

            //console.log(listSize+" "+filterIds.length);
            // VM221385:115 Uncaught TypeError: Cannot read property 'length' of null
            if(recordsCount > 0 &&  (filterIds && listSize<filterIds.length) && ( listSize < 8 || users.length == 0 ) ) {
                loadUsers(search, selectedNodeId, loadedCount, recordsCount)
            }
        });
    };

    dialog.addListType("users", function(search, startRecord, recordsCount, selectedNodeId, stackId){

        loadUsers(search, selectedNodeId, startRecord, 100);

    }, function(search, selectedNodeId, stackId){
        loadUsers(search, selectedNodeId, 0, -1);
    });


    dialog.addStack("structure", i18n.tr("Орг. структура"), "departments", true, true, function(){});

    dialog.showStack("structure");


    this.setShowAll = function(newValue){
        showAll = newValue;
    };

    this.setFilterDepartmentID = function(newValue){
        filterDepartmentID = newValue;
    };

    this.setFilterPositionID = function(newValue){
        filterPositionID = newValue;
    };

    this.setShowNoPosition = function(newValue){
        showNoPosition = newValue;
    };

    this.setSelectedItems = function(newSelectedPersons){
        dialog.setSelectedItems(instance.correctListArray(newSelectedPersons));
    };

    this.getSelectedItems = function(){
        return dialog.getSelectedItems();
    };

    this.showDialog = function(){
        dialog.show();
    };

    /**
     *
     * @param event SEE AS.FORMS.BasicChooserEvent
     * @param handler
     */
    this.on = function(event, handler) {
        dialog.on(event, handler);
    };


};
