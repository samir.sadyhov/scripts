

var KMG_FEMALE_USER_CHOOSER_OPTIONS = {formCode : "Штатная_расстановка111", field : "sex", value : "1" };

var KMG_FEMALE_USER_CHOOSER = [];
KMG_FEMALE_USER_CHOOSER.push({ formCode : 'Приказ_о_предоставлении_отпуска_по_беременности_и_родам1', cmpIds : ["user", "user_k", "user3", "user3_k", "user4", "user4_k"]});
KMG_FEMALE_USER_CHOOSER.push({ formCode : 'Больничный_лист_по_беременности_и_родам' , cmpIds : ["user"]});


KMG_FEMALE_USER_CHOOSER.push({ formCode : 'fПриказ_о_внесении_изменений_в_приказ_о_предоставлении_отпуска_по_беременности_и_родам11' , cmpIds :  ["user", "user3", "user_k", "user3_k"]});






if(!AS.FORMS.ApiUtils.getUsers) {
    AS.FORMS.ApiUtils.getUsers = AS.FORMS.ApiUtils.getUsersSuggestions;
}




AS.FORMS.FemaleUserChooser = function(multiSelect, filterIds, locale){
    var instance = this;
    var showAll = true;
    var filterDepartmentID = null;
    var filterPositionID = null;
    var showNoPosition = true;


    var loadedCount = 0;

    var dialog = new AS.FORMS.BasicChooserDialogKMG();

    dialog.init({
        showListItemIcon: true,
        showListItemInfo: true,
        showListItemStatus: true,
        listItemIconSize: 32,
        multiSelect: multiSelect,
        showSearchField: true,
        title: i18n.tr("Выбор пользователя"),
        placeHolderText : i18n.tr("Поиск пользователей"),
        showListItemIconStyle: "ns-userChooserIcon",
        showSelectedStack: multiSelect
    });

    this.correctListArray = function(users) {
        if(!users) {
            return [];
        }
        var filteredUsers = [];
        users.forEach(function (user, index) {
            user.id = user.personID;
            user.name = user.personName;
            user.info = user.positionName;

            if(user.personID.indexOf(AS.FORMS.USER_CHOOSER_SUFFIXES.group) === 0) {
                user.icon = "js/asforms/resources/group.png";
            } else if(user.personID.indexOf(AS.FORMS.USER_CHOOSER_SUFFIXES.contact) === 0) {
                user.icon = "js/asforms/resources/unknown.png";
            } else if(user.personID.indexOf(AS.FORMS.USER_CHOOSER_SUFFIXES.text) === 0) {
                user.icon = "js/asforms/resources/unknown.png";
            } else {
                user.icon = "rest/formPlayerIconService/photo?max_width=32&max_height=32&inscribe=false&personID="+user.personID;
                user.isPerson = true;
            }

            if (user.customFields && user.customFields.calendarStatusLabel) {
                user.status = user.customFields.calendarStatusLabel;
                user.statusColor = user.customFields.calendarColor;
            }

            if(filterIds.indexOf(user.personID) != -1) {
                filteredUsers.push(user);
            }


        });
        return filteredUsers;
    };


    dialog.addTree("departments", function(parentNode, tree){

        var departmentID = undefined;
        if(parentNode) {
            departmentID = parentNode.id;
        }
        AS.FORMS.ApiUtils.getDepartments(departmentID, locale, function(departments){
            dialog.setTreeNodeData("departments", departmentID, departments.departments, "departmentID", "departmentName", "hasChildDepartments");
        });
    }, function(parentNode, tree){
        dialog.setListTitle(parentNode.name);
        dialog.showList("users", true);
    });

    var listSize = 0;

    var loadUsers = function(search, selectedNodeId, startRecord, recordsCount){

        if(startRecord !== 0){
            startRecord = loadedCount;
        }else {
            loadedCount = 0;
            listSize = 0;
        }

        AS.SERVICES.showWaitWindow();

        AS.FORMS.ApiUtils.getUsers(search, filterPositionID, filterDepartmentID, selectedNodeId, 0, showNoPosition, startRecord, recordsCount, showAll, false, false, locale, function (users) {

            AS.SERVICES.hideWaitWindow();

            var empty = users.length == 0;

            if(empty) {
                dialog.setListData("users", users, startRecord > 0 );
                return;
            }

            loadedCount+=users.length;

            users = instance.correctListArray(users);

            listSize+=users.length;


            dialog.setListData("users", users, startRecord > 0 );

            if(recordsCount > 0 && (listSize < 8 || users.length == 0 )) {
                loadUsers(search, selectedNodeId, loadedCount, recordsCount)
            }
        });
    };

    dialog.addListType("users", function(search, startRecord, recordsCount, selectedNodeId, stackId){

        loadUsers(search, selectedNodeId, startRecord, 100);

    }, function(search, selectedNodeId, stackId){
        loadUsers(search, selectedNodeId, 0, -1);
    });


    dialog.addStack("structure", i18n.tr("Орг. структура"), "departments", true, true, function(){});

    dialog.showStack("structure");


    this.setShowAll = function(newValue){
        showAll = newValue;
    };

    this.setFilterDepartmentID = function(newValue){
        filterDepartmentID = newValue;
    };

    this.setFilterPositionID = function(newValue){
        filterPositionID = newValue;
    };

    this.setShowNoPosition = function(newValue){
        showNoPosition = newValue;
    };

    this.setSelectedItems = function(newSelectedPersons){
        dialog.setSelectedItems(instance.correctListArray(newSelectedPersons));
    };

    this.getSelectedItems = function(){
        return dialog.getSelectedItems();
    };

    this.showDialog = function(){
        dialog.show();
    };

    /**
     *
     * @param event SEE AS.FORMS.BasicChooserEvent
     * @param handler
     */
    this.on = function(event, handler) {
        dialog.on(event, handler);
    };


};















(function (asForms) {
    "use strict";
    asForms.FemaleUserLinkView = function(model, playerView) {
        asForms.View.call(this, this, model, playerView);

        var config = model.asfProperty.config;

        var instance = this;

        var locale = model.getLocale();

        var usersTagArea = new asForms.TagArea({width : "calc(100% - 30px)"}, 0, config['multi'], config['editable-label'], config['custom']);

        this.addValue = function(item) {
            var value = model.getValue().slice();
            value.push(item);
            model.setValueFromInput(value);
            instance.updateValueFromModel();
            usersTagArea.focus();
        };


        if (model.asfProperty.config['read-only']) {
            usersTagArea.setEditable(false);
        }

        usersTagArea.on(asForms.EVENT_TYPE.tagDelete, function(){
            model.setValueFromInput(usersTagArea.getValues());
        });


        var chooserButton = jQuery("<button/>", {class : "asf-browseButton"});
        chooserButton.css("background", "rgb(255, 230, 240) url(\"js/asforms/resources/user.browse.png\") no-repeat center");

        asForms.ASFDataUtils.addAsFormId(usersTagArea.getWidget(), "input", model.asfProperty);
        asForms.ASFDataUtils.addAsFormId(chooserButton, "button", model.asfProperty);

        // !filterIds || я (самир) добавил, а то он в консоль ругался на filterIds.length
        // на user пишет не найдено элементов
        // на user3 нормально срабатывает (ознакомление)
        var showUserChooser = function(filterIds) {
            if(!filterIds || filterIds.length == 0) {
                alert("Не найдено ни одного элемента удовлетворяющего условиям поиска");
                return;
            }
            var userChooser = new AS.FORMS.FemaleUserChooser(config['multi'], filterIds, locale);
            userChooser.setFilterDepartmentID( model.getFilterDepartmentId());
            userChooser.setFilterPositionID(model.getFilterPositionId());
            userChooser.setShowNoPosition(config['show-without-position']);
            userChooser.setSelectedItems(model.getValue());
            userChooser.showDialog();
            userChooser.on(AS.FORMS.BasicChooserEvent.applyClicked, function(){
                model.setValue(userChooser.getSelectedItems());
            });
        };


        if(KMG_TABEL_FEMALE_USER_CHOOSER) {
            for(var i=0; i<KMG_TABEL_FEMALE_USER_CHOOSER.length; i++) {
                var formInfo = KMG_TABEL_FEMALE_USER_CHOOSER[i];
                if (formInfo.formCode === model.playerModel.formCode) {
                    var tabelField = formInfo.fields.findElement("user", model.asfProperty.id);
                    if(tabelField) {
                        model.asfProperty.tabelFieldId = tabelField.tabel;
                    }
                }
            }
        }


        console.log("TABEL CMP", model.asfProperty.tabelFieldId);

        var loadFemales = function(tabelFilterIds){
            var value1 = KMG_FEMALE_USER_CHOOSER_OPTIONS.value;
            AS.FORMS.ApiUtils.simpleAsyncGet("rest/api/filecabinet/get_by_field_value?formCode="+encodeURIComponent(KMG_FEMALE_USER_CHOOSER_OPTIONS.formCode)+"&fieldName="+KMG_FEMALE_USER_CHOOSER_OPTIONS.field+"&value="+value1+"&locale=ru", function(filterIds){
                if(tabelFilterIds) {
                    filterIds = _.intersection(tabelFilterIds, filterIds);
                }
                showUserChooser(filterIds);
            });
        };

        chooserButton.click(function() {



            if(!model.asfProperty.tabelFieldId) {
                loadFemales(null);
            } else {
                var value1 = "";

                var tabelNumberModel = model.playerModel.getModelWithId(model.asfProperty.tabelFieldId, model.asfProperty.ownerTableId, model.asfProperty.tableBlockIndex);
                if(tabelNumberModel) {
                    value1 = tabelNumberModel.getValue();
                }

                if(value1 == null ||  value1 == undefined || value1.length == 0) {
                    showUserChooser(null);
                    return;
                }
                AS.FORMS.ApiUtils.simpleAsyncGet("rest/api/filecabinet/get_by_field_value?formCode="+encodeURIComponent(KMG_TABEL_USER_CHOOSER_OPTIONS.formCode)+"&fieldName="+KMG_TABEL_USER_CHOOSER_OPTIONS.field+"&value="+value1+"&locale=ru", function(filterIds){
                    loadFemales(filterIds);
                });
            }




        });

        this.updateValueFromModel = function(){
            usersTagArea.setValues(model.getValue());
        };

        this.unmarkInvalid = function() {
            usersTagArea.markValidity(true);
        };

        this.markInvalid = function() {
            usersTagArea.markValidity(false);
        };

        this.setEnabled = function(newEnabled) {
            usersTagArea.setEditable(newEnabled);
            if(newEnabled) {
                chooserButton.css("pointer-events", "auto");
            } else {
                chooserButton.css("pointer-events", "none");
            }
        };
        this.setEnabled(!asForms.ASFDataUtils.isReadOnly(model));

        model.on(asForms.EVENT_TYPE.dataLoad, function(){
            instance.updateValueFromModel();
        });

        this.updateValueFromModel();

        this.container.append(usersTagArea.getWidget());
        this.container.append(chooserButton);
    };

    asForms.FemaleUserLinkView.prototype = Object.create(asForms.View.prototype);
    asForms.FemaleUserLinkView.prototype.constructor = asForms.FemaleUserLinkView;


    asForms.ComponentUtils.createViewOriginalForFemale = asForms.ComponentUtils.createView;

    asForms.ComponentUtils.createView = function(model, playerView, editable) {



        if(playerView == true || playerView == false) {
            editable = playerView;
        }

        if( editable ){
            for(var i=0; i<KMG_FEMALE_USER_CHOOSER.length; i++){
                var formInfo = KMG_FEMALE_USER_CHOOSER[i];
                if(formInfo.formCode === model.playerModel.formCode && formInfo.cmpIds.indexOf(model.asfProperty.id) != -1) {
                    console.log("FOUND INPUT FOR RESERVE CHOOSER");
                    try {
                        return new asForms.FemaleUserLinkView(model, playerView, editable);
                    } catch (e) {
                        console.log(e);
                    }
                }
            }
        }

        try {
            return asForms.ComponentUtils.createViewOriginalForFemale(model, playerView, editable);
        } catch (e) {
            console.log(e);
        }


    }

} (AS.FORMS));
