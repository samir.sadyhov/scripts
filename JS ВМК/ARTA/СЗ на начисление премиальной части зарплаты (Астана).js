AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  if (model.formCode == "СЗ_на_начисление_премиальной_части_зарплаты_Астана") {

    var cmp = {table: 'cmp-wgnoj1', sum: 'cmp-4qxd3o', total: 'summa'};

    var setSumValue = function(model, cmp) {
      var tableModel = model.getModelWithId(cmp.table);
      var TOTAL = 0;
      tableModel.modelBlocks.forEach(function(modelBlock, index){
        var tableBlockIndex = modelBlock[0].asfProperty.tableBlockIndex;
        var n = parseFloat(tableModel.getModelWithId(cmp.sum, cmp.table, tableBlockIndex).getValue());
        if(!isNaN(n)) TOTAL += n;
      });
      model.getModelWithId(cmp.total).setValue((Math.round(TOTAL * 100) / 100) + '');
    };

    setTimeout(function() {
      var dateTableView = view.getViewWithId(cmp.table);
      dateTableView.getViewBlocks().forEach(function(viewBlock, index){
        var tableBlockIndex = viewBlock.views[0].model.asfProperty.tableBlockIndex;
        var sumBox = model.getModelWithId(cmp.sum, cmp.table, tableBlockIndex);
        sumBox.on("valueChange", function() {
          // сумма значений
          setSumValue(model, cmp);
        });
      });
    }, 0);

    var dateTableModel = model.getModelWithId(cmp.table);
    if (view.editable) {
      dateTableModel.on('tableRowAdd', function(evt, modelTab, modelRow) {
        var tableBlockIndex = modelRow[0].asfProperty.tableBlockIndex;
        var sumBox = model.getModelWithId(cmp.sum, cmp.table, tableBlockIndex);
        sumBox.on("valueChange", function() {
          // сумма значений
          setSumValue(model, cmp);
        });
      });
      dateTableModel.on('tableRowDelete', function(evt, rowModel, rowIndex, removedRow) {
        // сумма значений
        setSumValue(model, cmp);
      });
    }

  }
});
