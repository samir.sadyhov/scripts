AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  if (model.formCode == 'Заявка_на_создание_отчета' || model.formCode == 'Заявка_на_доработку_отчета') {

    setTimeout(function() {

      view.getViewWithId('stage1').setVisible(false);

      var cmpArr = ['ispol', 'data', 'dlit'];

      if (model.getModelWithId('stage1').getValue() != '1') {
        cmpArr.forEach(function(cmp) {
          view.getViewWithId(cmp).setVisible(false);
        });
      } else {
        cmpArr.forEach(function(cmp) {
          view.getViewWithId(cmp).setVisible(true);
        });
      }
      
    }, 0);

  }
});


// stage1

view.setVisible(false);

var cmpArr = ['ispol', 'data', 'dlit'];

if (model.getValue() != '1') {
  cmpArr.forEach(function(cmp) {
    view.playerView.getViewWithId(cmp).setVisible(false);
  });
} else {
  cmpArr.forEach(function(cmp) {
    view.playerView.getViewWithId(cmp).setVisible(true);
  });
}
