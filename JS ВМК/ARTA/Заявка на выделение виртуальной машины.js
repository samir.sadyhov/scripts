AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  //"Заявка на выделение виртуальной машины"
  if (model.formCode == "Заявка_на_выделение_виртуальной_машины") {

    var checkResult = function(uVal, vmVal, resultBox){
      if (uVal.length > 0 && vmVal) {
        resultBox.setValue('5');
      } else if (uVal.length === 0 && vmVal) {
        resultBox.setValue('6');
      } else if (uVal.length === 0 && !vmVal) {
        resultBox.setValue('7');
      } else {
        resultBox.setValue(null);
      }
    };

    var userBox = model.getModelWithId('user');
    var vmBox = model.getModelWithId('virtual');

    view.getViewWithId('uslov').setVisible(false);
    if (view.editable) {
      view.getViewWithId('uslov').setVisible(false);
    }

    checkResult(userBox.getValue(), vmBox.getValue(), model.getModelWithId('uslov'));
    vmBox.on("valueChange", function() {
      checkResult(userBox.getValue(), vmBox.getValue(), model.getModelWithId('uslov'));
    });
    userBox.on("valueChange", function() {
      checkResult(userBox.getValue(), vmBox.getValue(), model.getModelWithId('uslov'));
    });
  }
});
