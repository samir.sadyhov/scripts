AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  console.log("FORMSHOW " + model.formId + " " + model.asfDataId);
  if (model.formId == 'a8f97c33-09e7-4e92-9402-ba4aa0cdd1b6') {
    var synergyLogin = '1';
    var synergyPass = '1';
    var cardPos = null;
    var posCmpID = 'position';
    var cardFormID = 'c26eb9f4-76ee-4e0a-a393-9563bbcc6274';

    // получаем модель текстового поля
    var razryadText = model.getModelWithId('razryad'); // разряд
    var gredText = model.getModelWithId('gred'); // грейд
    var categoryText = model.getModelWithId('category'); // категория

    var posModel = model.getModelWithId(posCmpID);
    posModel.on(AS.FORMS.EVENT_TYPE.valueChange, function() {
      var value = posModel.value;
      if (value && value.length > 0) {
        value = value[0];

        var razryadValue='';
        var gredValue='';
        var categoryValue='';
        razryadText.setValue(razryadValue);
        gredText.setValue(gredValue);
        categoryText.setValue(categoryValue);

        console.log('[positionid]: ' + value.elementID);

        jQuery.ajax({ // получение карточек должности
          url: 'rest/api/positions/get_cards?positionID=' + value.elementID,
          dataType: 'json',
          username: synergyLogin,
          password: synergyPass,
          success: function(data) {
              var tmpJson;

              tmpJson = JSON.stringify(data);
              tmpJson = String(tmpJson).replace(/form-uuid/g, "formUuid");
              tmpJson = tmpJson.replace(/data-uuid/g, "dataUuid");
              tmpJson = JSON.parse(tmpJson);

              // поиск нужной карточки по formID
              for (var i = 0; i < tmpJson.length; i++) {
                if (tmpJson[i].formUuid == cardFormID) {
                  cardPos = tmpJson[i].dataUuid; // получаем ID карточки
                  break;
                }
              }

              console.log('Карточка должности [asfDataId]: ' + cardPos);

              if (cardPos) { // если нужная карточка есть достаем данные
                jQuery.ajax({
                  url: 'rest/api/asforms/data/' + cardPos,
                  dataType: 'json',
                  username: synergyLogin,
                  password: synergyPass,
                  success: function(data) {

                    var typePos = null; // Рабочий/служащий
                    for (var j = 0; j < data.data.length; j++) {
                      if (data.data[j].id == 'type') {
                        typePos = data.data[j].value;
                        break;
                      }
                    }
                    if (typePos) {
                      if (typePos == '1') { // рабочий
                        razryadValue='';
                        gredValue='';
                        categoryValue='';
                        for (var j = 0; j < data.data.length; j++) {
                          if (data.data[j].id == 'discharge_work') {
                            razryadValue=data.data[j].value;
                          }
                          if (data.data[j].id == 'grade_work') {
                            gredValue=data.data[j].value;
                          }
                          if (data.data[j].id == 'category_work') {
                            categoryValue=data.data[j].value;
                          }
                        }
                      } else if (typePos == '2') { // служащий
                        razryadValue='';
                        gredValue='';
                        categoryValue='';
                        for (var j = 0; j < data.data.length; j++) {
                          if (data.data[j].id == 'grade_official') {
                            gredValue=data.data[j].value;
                          }
                          if (data.data[j].id == 'category_official') {
                            categoryValue=data.data[j].value;
                          }
                        }
                      } else {
                        razryadValue='';
                        gredValue='';
                        categoryValue='';
                      }
                      // записываем значения на форму
                      razryadText.setValue(razryadValue);
                      gredText.setValue(gredValue);
                      categoryText.setValue(categoryValue);
                    }
                  }
                }); // data cardPos
              }  //if (cardPos)
            } // success: function(data) {
        }); // get_cards
      } //if (value && value.length > 0)
    }); //posModel.on(AS.FORMS.EVENT_TYPE.valueChange, function()
  }
});
