AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  // общие настройки
  var synergy = {
    login: "administrator",
    password: "qazwsx123"
  };

  var akt = {
    year: { // АКТ ежегодного освидетельствования судна
      formID: '0fecf5ef-c0ab-48ea-b4b2-551a78d358df',
      regNumber: 'reg_nomer',
      date: 'year1',
      dateE: {pred: 'ezh1', sled: 'ezh2'},
      dateO: {pred: 'och1', sled: 'och2'},
      dateS: {pred: 'slip1', sled: 'slip2'},
      dateK: {pred: 'klas1', sled: 'klas2'}
    },
    klassif:  { // АКТ классификационного освидетельствования судна
      formID: '99cbaffd-33b9-4582-8c81-fbc8c0c3f5ac',
      regNumber: 'reg_nomer',
      date: 'data',
      dateE: {pred: 'pred_data_ezh', sled: 'sled_data_ezh'},
      dateO: {pred: 'pred_data_och', sled: 'sled_data_och'},
      dateS: {pred: 'pred_data_dok', sled: 'sled_data_dok'},
      dateK: {pred: 'pred_data_klass', sled: 'sled_data_klass'}
    },
    ochered: { // АКТ очередного освидетельствования судна
      formID: '43d29472-4fc7-4c86-b8aa-afab7e172991',
      regNumber: 'reg_nomer',
      date: 'data_osvid',
      dateE: {pred: 'pred_data_ezhegod', sled: 'sled_data_ezhegodnoe'},
      dateO: {pred: 'pred_data_ocherednoe', sled: 'sled_data_ochered'},
      dateS: {pred: 'pred_data__dok', sled: 'sled_data_dok'},
      dateK: {pred: 'pred_data_klassific', sled: 'sled_data_klass'}
    }
  };

  // АКТ ежегодного освидетельствования судна
  if (model.formId == akt.year.formID) {
    console.log('АКТ ежегодного освидетельствования судна');
    console.log("formID: [" + model.formId + "]" + " asfDataId: [" + model.asfDataId + "]");

    // Получаем "Регистровый №", будет осуществлен поиск предыдущей записи по этому значению
    var regNumberModel = model.getModelWithId(akt.year.regNumber);
    regNumberModel.on(AS.FORMS.EVENT_TYPE.valueChange, function() {

      // Формирование поисковой строки
      var paramSearch = '{"query": "where uuid = \'' + akt.year.formID + '\' and `' + akt.year.regNumber + '.TEXT` = \'' + regNumberModel.value + '\' order by `' + akt.year.date + '.DATE` desc", "parameters": ["' + akt.year.regNumber + '.TEXT", "' + akt.year.date + '.DATE"], "showDeleted": "false", "searchInRegistry": "true", "recordsCount": "10"}';
      $.ajax({
        url: "rest/api/asforms/search/advanced",
        contentType: "application/json; charset=utf-8",
        type: "POST",
        data: paramSearch,
        dataType: 'json',
        username: synergy.login,
        password: synergy.password,
        success: function(previousRecord) {
            //console.log("Результат поиска: " + JSON.stringify(previousRecord));
            if (previousRecord.length >= 1) {
              var STATE_SUCCESSFUL = false;
              for (var i = 0; i < previousRecord.length; i++) {
                if (previousRecord[i].registryRecordStatus == 'STATE_SUCCESSFUL') {
                  previousRecord = previousRecord[i].dataUUID;
                  STATE_SUCCESSFUL = true;
                  break;
                } //if
              } //for
              if (STATE_SUCCESSFUL) {
                console.log('asfDataId последней записи по судну: ' + previousRecord);

                $.ajax({
                  url: 'rest/api/asforms/data/' + previousRecord,
                  dataType: 'json',
                  username: synergy.login,
                  password: synergy.password,
                  success: function(previousDataOnTheShip) {
                      //console.log("Данные по последней записи: " + JSON.stringify(previousDataOnTheShip));
                      for (var i = 0; i < previousDataOnTheShip.data.length; i++) {
                        if (previousDataOnTheShip.data[i].id == akt.year.dateE.sled) {
                          //Предыдущая дата Ежегодное
                          model.getModelWithId(akt.year.dateE.pred).setValue(previousDataOnTheShip.data[i].key);
                          //console.log('Предыдущая дата Ежегодное ' + previousDataOnTheShip.data[i].key);
                        }
                        if (previousDataOnTheShip.data[i].id == akt.year.dateO.sled) {
                          // Предыдущая дата Очередное
                          model.getModelWithId(akt.year.dateO.pred).setValue(previousDataOnTheShip.data[i].key);
                          //console.log('Предыдущая дата Очередное ' + previousDataOnTheShip.data[i].key);
                          if (previousDataOnTheShip.data[i].key >= currentDate()) {
                            // Следующая дата Очередное (если след.дата больше текущей даты)
                            model.getModelWithId(akt.year.dateO.sled).setValue(previousDataOnTheShip.data[i].key);
                            //console.log('Следующая дата Очередное (если след.дата больше текущей даты) ' + previousDataOnTheShip.data[i].key);
                          } else {
                            // Следующая дата Очередное (+ 5 лет)
                            model.getModelWithId(akt.year.dateO.sled).setValue(getDateKey(previousDataOnTheShip.data[i].key, 5));
                            //console.log('Следующая дата Очередное (+ 5 лет) ' + getDateKey(previousDataOnTheShip.data[i].key, 5));
                          }
                        }
                        if (previousDataOnTheShip.data[i].id == akt.year.dateS.sled) {
                          // Предыдущая дата На слипе
                          model.getModelWithId(akt.year.dateS.pred).setValue(previousDataOnTheShip.data[i].key);
                          //console.log('Предыдущая дата На слипе ' + previousDataOnTheShip.data[i].key);
                          if (previousDataOnTheShip.data[i].key >= currentDate()) {
                            // Следующая дата На слипе (если след.дата больше текущей даты)
                            model.getModelWithId(akt.year.dateS.sled).setValue(previousDataOnTheShip.data[i].key);
                            //console.log('Следующая дата На слипе (если след.дата больше текущей даты) ' + previousDataOnTheShip.data[i].key);
                          } else {
                            // Следующая дата На слипе (+ 5 лет)
                            model.getModelWithId(akt.year.dateS.sled).setValue(getDateKey(previousDataOnTheShip.data[i].key, 5));
                            //console.log('Следующая дата На слипе (+ 5 лет) ' + getDateKey(previousDataOnTheShip.data[i].key, 5));
                          }
                        }
                        if (previousDataOnTheShip.data[i].id == akt.year.dateK.sled) {
                          // Предыдущая дата Классификационное
                          model.getModelWithId(akt.year.dateK.pred).setValue(previousDataOnTheShip.data[i].key);
                          //console.log('Предыдущая дата Классификационное ' + previousDataOnTheShip.data[i].key);
                          if (previousDataOnTheShip.data[i].key >= currentDate()) {
                            // Следующая дата Классификационное (если след.дата больше текущей даты)
                            model.getModelWithId(akt.year.dateK.sled).setValue(previousDataOnTheShip.data[i].key);
                            //console.log('Следующая дата Классификационное (если след.дата больше текущей даты) ' + previousDataOnTheShip.data[i].key);
                          } else {
                            // Следующая дата Классификационное (+ 5 лет)
                            model.getModelWithId(akt.year.dateK.sled).setValue(getDateKey(previousDataOnTheShip.data[i].key, 5));
                            //console.log('Следующая дата Классификационное (+ 5 лет) ' + getDateKey(previousDataOnTheShip.data[i].key, 5));
                          }
                        }
                      } // конец цикла
                    } // success
                }); // ajax(
              } else console.log('не найдено предыдущих записей по данному судну');
            } // previousRecord.length >= 2
          } // success
      }); // ajax(
    });
    var dateOsvid = model.getModelWithId(akt.year.date);
    dateOsvid.on(AS.FORMS.EVENT_TYPE.valueChange, function() {
      model.getModelWithId(akt.year.dateE.sled).setValue(getDateKey(dateOsvid.value, 1)); //Следующая дата Ежегодное (+ 1 год)
      //console.log('Следующая дата Ежегодное: ' + getDateKey(dateOsvid.value, 1));
    });
  }

  // АКТ классификационного освидетельствования судна
  if (model.formId == akt.klassif.formID) {
    console.log('АКТ классификационного освидетельствования судна');
    console.log("formID: [" + model.formId + "]" + " asfDataId: [" + model.asfDataId + "]");

    // Получаем "Регистровый №", будет осуществлен поиск предыдущей записи по этому значению
    var regNumberModel = model.getModelWithId(akt.klassif.regNumber);
    regNumberModel.on(AS.FORMS.EVENT_TYPE.valueChange, function() {

      // Формирование поисковой строки
      var paramSearch = '{"query": "where uuid = \'' + akt.klassif.formID + '\' and `' + akt.klassif.regNumber + '.TEXT` = \'' + regNumberModel.value + '\' order by `' + akt.klassif.date + '.DATE` desc", "parameters": ["' + akt.klassif.regNumber + '.TEXT", "' + akt.klassif.date + '.DATE"], "showDeleted": "false", "searchInRegistry": "true", "recordsCount": "10"}';
      $.ajax({
        url: "rest/api/asforms/search/advanced",
        contentType: "application/json; charset=utf-8",
        type: "POST",
        data: paramSearch,
        dataType: 'json',
        username: synergy.login,
        password: synergy.password,
        success: function(previousRecord) {
            //console.log("Результат поиска: " + JSON.stringify(previousRecord));
            if (previousRecord.length >= 1) {
              var STATE_SUCCESSFUL = false;
              for (var i = 0; i < previousRecord.length; i++) {
                if (previousRecord[i].registryRecordStatus == 'STATE_SUCCESSFUL') {
                  previousRecord = previousRecord[i].dataUUID;
                  STATE_SUCCESSFUL = true;
                  break;
                } //if
              } //for
              if (STATE_SUCCESSFUL) {
                console.log('asfDataId последней записи по судну: ' + previousRecord);
                $.ajax({
                  url: 'rest/api/asforms/data/' + previousRecord,
                  dataType: 'json',
                  username: synergy.login,
                  password: synergy.password,
                  success: function(previousDataOnTheShip) {
                      //console.log("Данные по последней записи: " + JSON.stringify(previousDataOnTheShip));
                      for (var i = 0; i < previousDataOnTheShip.data.length; i++) {
                        if (previousDataOnTheShip.data[i].id == akt.klassif.dateE.sled) {
                          //Предыдущая дата Ежегодное
                          model.getModelWithId(akt.klassif.dateE.pred).setValue(previousDataOnTheShip.data[i].key);
                          //console.log('Предыдущая дата Ежегодное ' + previousDataOnTheShip.data[i].key);
                        }
                        if (previousDataOnTheShip.data[i].id == akt.klassif.dateO.sled) {
                          // Предыдущая дата Очередное
                          model.getModelWithId(akt.klassif.dateO.pred).setValue(previousDataOnTheShip.data[i].key);
                          //console.log('Предыдущая дата Очередное ' + previousDataOnTheShip.data[i].key);
                          if (previousDataOnTheShip.data[i].key >= currentDate()) {
                            // Следующая дата Очередное (если след.дата больше текущей даты)
                            model.getModelWithId(akt.klassif.dateO.sled).setValue(previousDataOnTheShip.data[i].key);
                            //console.log('Следующая дата Очередное (если след.дата больше текущей даты) ' + previousDataOnTheShip.data[i].key);
                          } else {
                            // Следующая дата Очередное (+ 5 лет)
                            model.getModelWithId(akt.klassif.dateO.sled).setValue(getDateKey(previousDataOnTheShip.data[i].key, 5));
                            //console.log('Следующая дата Очередное (+ 5 лет) ' + getDateKey(previousDataOnTheShip.data[i].key, 5));
                          }
                        }
                        if (previousDataOnTheShip.data[i].id == akt.klassif.dateS.sled) {
                          // Предыдущая дата На слипе
                          model.getModelWithId(akt.klassif.dateS.pred).setValue(previousDataOnTheShip.data[i].key);
                          //console.log('Предыдущая дата На слипе ' + previousDataOnTheShip.data[i].key);
                          if (previousDataOnTheShip.data[i].key >= currentDate()) {
                            // Следующая дата На слипе (если след.дата больше текущей даты)
                            model.getModelWithId(akt.klassif.dateS.sled).setValue(previousDataOnTheShip.data[i].key);
                            //console.log('Следующая дата На слипе (если след.дата больше текущей даты) ' + previousDataOnTheShip.data[i].key);
                          } else {
                            // Следующая дата На слипе (+ 5 лет)
                            model.getModelWithId(akt.klassif.dateS.sled).setValue(getDateKey(previousDataOnTheShip.data[i].key, 5));
                            //console.log('Следующая дата На слипе (+ 5 лет) ' + getDateKey(previousDataOnTheShip.data[i].key, 5));
                          }
                        }
                        if (previousDataOnTheShip.data[i].id == akt.klassif.dateK.sled) {
                          // Предыдущая дата Классификационное
                          model.getModelWithId(akt.klassif.dateK.pred).setValue(previousDataOnTheShip.data[i].key);
                          //console.log('Предыдущая дата Классификационное ' + previousDataOnTheShip.data[i].key);
                        }
                      } // конец цикла
                    } // success
                }); // ajax(
              }
            } else console.log('не найдено предыдущих записей по данному судну');
          } // success
      }); // ajax(
    });
    var dateOsvid = model.getModelWithId(akt.klassif.date);
    dateOsvid.on(AS.FORMS.EVENT_TYPE.valueChange, function() {
      model.getModelWithId(akt.klassif.dateE.sled).setValue(getDateKey(dateOsvid.value, 1)); //Следующая дата Ежегодное (+ 1 год)
      //console.log('Следующая дата Ежегодное: ' + getDateKey(dateOsvid.value, 1));
      model.getModelWithId(akt.klassif.dateK.sled).setValue(getDateKey(dateOsvid.value, 5)); //Следующая дата Классификационное (+ 5 лет)
      //console.log('Следующая дата Классификационное (+ 5 лет): ' + getDateKey(dateOsvid.value, 5));
    });
  }

  // АКТ очередного освидетельствования судна
  if (model.formId == akt.ochered.formID) {
    console.log('АКТ очередного освидетельствования судна');
    console.log("formID: [" + model.formId + "]" + " asfDataId: [" + model.asfDataId + "]");

    // Получаем "Регистровый №", будет осуществлен поиск предыдущей записи по этому значению
    var regNumberModel = model.getModelWithId(akt.ochered.regNumber);
    regNumberModel.on(AS.FORMS.EVENT_TYPE.valueChange, function() {

      // Формирование поисковой строки (акт ежегодного освидетельствования судна)
      var paramSearchYear = '{"query": "where uuid = \''+akt.year.formID+'\' and `'+akt.ochered.regNumber+'.TEXT` = \''+regNumberModel.value+'\' order by `'+akt.year.date+'.DATE` desc", "parameters": ["'+akt.ochered.regNumber+'.TEXT", "'+akt.year.date+'.DATE"], "showDeleted": "false", "searchInRegistry": "true", "recordsCount": "10"}';
      $.ajax({
        url: "rest/api/asforms/search/advanced",
        contentType: "application/json; charset=utf-8",
        type: "POST",
        data: paramSearchYear,
        dataType: 'json',
        username: synergy.login,
        password: synergy.password,
        success: function(previousRecordYear) {
            //console.log("Результат поиска (ежегод.): " + JSON.stringify(previousRecordYear));
            var yearDate=null;
            if (previousRecordYear.length >= 1) {
              var STATE_SUCCESSFUL = false;
              for (var i = 0; i < previousRecordYear.length; i++) {
                if (previousRecordYear[i].registryRecordStatus == 'STATE_SUCCESSFUL') {
                  previousRecordYear = previousRecordYear[i].dataUUID;
                  STATE_SUCCESSFUL = true;
                  break;
                } //if
              } //for
              if (STATE_SUCCESSFUL) {
                console.log('asfDataId последней записи по судну (ежегод.): ' + previousRecordYear);
                $.ajax({
                  url: 'rest/api/asforms/data/' + previousRecordYear,
                  dataType: 'json',
                  username: synergy.login,
                  password: synergy.password,
                  success: function(previousDataYear) {
                    if (previousDataYear.data.length > 0) {
                      for (var i = 0; i < previousDataYear.data.length; i++) {
                        if (previousDataYear.data[i].id == akt.year.dateK.sled) {
                          yearDate=previousDataYear.data[i].key; // вытаскиваем дату след. классифик. из последнего акта ежегодного освидетельствования судна
                          break;
                        }
                      }
                      if (yearDate) {
                        //Следующая дата Ежегодное (+ 1 год)
                        model.getModelWithId(akt.ochered.dateE.sled).setValue(getDateKey(yearDate, 1));
                        //console.log('Следующая дата Ежегодное (+ 1 год) ' + getDateKey(yearDate, 1));
                        // Следующая дата Классификационное
                        model.getModelWithId(akt.ochered.dateK.sled).setValue(yearDate);
                        //console.log('Следующая дата Классификационное ' + yearDate);
                      }
                    }
                  } // success
                }); // ajax(
              }
            } else console.log('не найдено предыдущих записей по данному судну (в реестре ежегод. освид.)');
          } // success
      }); // ajax(

      // Формирование поисковой строки
      var paramSearch = '{"query": "where uuid = \'' + akt.ochered.formID + '\' and `' + akt.ochered.regNumber + '.TEXT` = \'' + regNumberModel.value + '\' order by `' + akt.ochered.date + '.DATE` desc", "parameters": ["' + akt.ochered.regNumber + '.TEXT", "' + akt.ochered.date + '.DATE"], "showDeleted": "false", "searchInRegistry": "true", "recordsCount": "10"}';
      $.ajax({
        url: "rest/api/asforms/search/advanced",
        contentType: "application/json; charset=utf-8",
        type: "POST",
        data: paramSearch,
        dataType: 'json',
        username: synergy.login,
        password: synergy.password,
        success: function(previousRecord) {
            //console.log("Результат поиска: " + JSON.stringify(previousRecord));
            if (previousRecord.length >= 1) {
              var STATE_SUCCESSFUL = false;
              for (var i = 0; i < previousRecord.length; i++) {
                if (previousRecord[i].registryRecordStatus == 'STATE_SUCCESSFUL') {
                  previousRecord = previousRecord[i].dataUUID;
                  STATE_SUCCESSFUL = true;
                  break;
                }
              }
              if (STATE_SUCCESSFUL) {
                console.log('asfDataId последней записи по судну: ' + previousRecord);
                $.ajax({
                  url: 'rest/api/asforms/data/' + previousRecord,
                  dataType: 'json',
                  username: synergy.login,
                  password: synergy.password,
                  success: function(previousDataOnTheShip) {
                      //console.log("Данные по последней записи: " + JSON.stringify(previousDataOnTheShip));
                      for (var i = 0; i < previousDataOnTheShip.data.length; i++) {
                        if (previousDataOnTheShip.data[i].id == akt.ochered.dateE.sled) {
                          //Предыдущая дата Ежегодное
                          model.getModelWithId(akt.ochered.dateE.pred).setValue(previousDataOnTheShip.data[i].key);
                          //console.log('Предыдущая дата Ежегодное ' + previousDataOnTheShip.data[i].key);
                        }
                        if (previousDataOnTheShip.data[i].id == akt.ochered.dateO.sled) {
                          // Предыдущая дата Очередное
                          model.getModelWithId(akt.ochered.dateO.pred).setValue(previousDataOnTheShip.data[i].key);
                          //console.log('Предыдущая дата Очередное ' + previousDataOnTheShip.data[i].key);
                          // Следующая дата Очередное (+ 5 лет)
                          model.getModelWithId(akt.ochered.dateO.sled).setValue(getDateKey(previousDataOnTheShip.data[i].key, 5));
                          //console.log('Следующая дата Очередное (+ 5 лет) ' + getDateKey(previousDataOnTheShip.data[i].key, 5));
                        }
                        if (previousDataOnTheShip.data[i].id == akt.ochered.dateS.sled) {
                          // Предыдущая дата На слипе
                          model.getModelWithId(akt.ochered.dateS.pred).setValue(previousDataOnTheShip.data[i].key);
                          //console.log('Предыдущая дата На слипе ' + previousDataOnTheShip.data[i].key);
                          // Следующая дата На слипе
                          model.getModelWithId(akt.ochered.dateS.sled).setValue(getDateKey(previousDataOnTheShip.data[i].key, 5));
                          //console.log('Следующая дата На слипе (+ 5 лет) ' + getDateKey(previousDataOnTheShip.data[i].key, 5));
                        }
                        if (previousDataOnTheShip.data[i].id == akt.ochered.dateK.sled) {
                          // Предыдущая дата Классификационное
                          model.getModelWithId(akt.ochered.dateK.pred).setValue(previousDataOnTheShip.data[i].key);
                          //console.log('Предыдущая дата Классификационное ' + previousDataOnTheShip.data[i].key);
                        }
                      } // конец цикла
                    } // success
                }); // ajax(
              }
            } else console.log('не найдено предыдущих записей по данному судну');
          } // success
      }); // ajax(
    });
  }

});

// текущая дата в виде "YYYY-MM-DD 00:00:00"
function currentDate() {
  var today = new Date();
  var month = Number(today.getMonth() + 1);
  if (month < 10) month = '0' + month;
  return today.getFullYear() + '-' + (month) + '-' + ('0' + today.getDate()).slice(-2) + ' 00:00:00';
}
// функция преобразования дат
// на входе принимает дату вида "YYYY-MM-DD 00:00:00" и прибавляет t лет
function getDateKey(dateStr, t) {
  return (Number(dateStr.substring(0, 4)) + Number(t)) + dateStr.substring(4);
}
