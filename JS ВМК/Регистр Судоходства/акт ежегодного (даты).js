AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  if (model.formId == '0fecf5ef-c0ab-48ea-b4b2-551a78d358df') {
    console.log("formID: [" + model.formId + "]");
    console.log("asfDataId: [" + model.asfDataId + "]");
    // общие настройки
    var synergy = {
      login: "administrator",
      password: "qazwsx123"
    };
    // АКТ ежегодного освидетельствования судна
    var akt = {
      regNumber: 'reg_nomer',
      date: 'year1',
      dateE: {pred: 'ezh1', sled: 'ezh2'},
      dateO: {pred: 'och1', sled: 'och2'},
      dateS: {pred: 'slip1', sled: 'slip2'},
      dateK: {pred: 'klas1', sled: 'klas2'}
    };

    // Получаем "Регистровый №", будет осуществлен поиск предыдущей записи по этому значению
    var regNumberModel = model.getModelWithId(akt.regNumber);
    regNumberModel.on(AS.FORMS.EVENT_TYPE.valueChange, function() {
      //console.log('Регистровый №: ' + regNumberModel.value);
      // Формирование поисковой строки
      var paramSearch = '{"query": "where uuid = \'' + model.formId + '\' and `' + akt.regNumber + '.TEXT` = \'' + regNumberModel.value + '\' order by `' + akt.date + '.DATE` desc", "parameters": ["' + akt.regNumber + '.TEXT", "' + akt.date + '.DATE"], "showDeleted": "false", "searchInRegistry": "true", "recordsCount": "10"}';

      //console.log('paramSearch: ' + paramSearch);
      $.ajax({
        url: "rest/api/asforms/search/advanced",
        contentType: "application/json; charset=utf-8",
        type: "POST",
        data: paramSearch,
        dataType: 'json',
        username: synergy.login,
        password: synergy.password,
        success: function(previousRecord) {
            console.log("Результат поиска: " + JSON.stringify(previousRecord));
            if (previousRecord.length >= 1) {
              var STATE_SUCCESSFUL = false;
              for (var i = 0; i < previousRecord.length; i++) {
                if (previousRecord[i].registryRecordStatus == 'STATE_SUCCESSFUL') {
                  previousRecord = previousRecord[i].dataUUID;
                  STATE_SUCCESSFUL = true;
                  break;
                } //if
              } //for
              if (STATE_SUCCESSFUL) {
                console.log('asfSataId последней записи по судну: ' + previousRecord);

                $.ajax({
                  url: 'rest/api/asforms/data/' + previousRecord,
                  dataType: 'json',
                  username: synergy.login,
                  password: synergy.password,
                  success: function(previousDataOnTheShip) {
                      //console.log("Данные по последней записи: " + JSON.stringify(previousDataOnTheShip));
                      for (var i = 0; i < previousDataOnTheShip.data.length; i++) {
                        if (previousDataOnTheShip.data[i].id == akt.dateE.sled) {
                          //Предыдущая дата Ежегодное
                          model.getModelWithId(akt.dateE.pred).setValue(previousDataOnTheShip.data[i].key);
                          console.log('Предыдущая дата Ежегодное ' + previousDataOnTheShip.data[i].key);
                        }
                        if (previousDataOnTheShip.data[i].id == akt.dateO.sled) {
                          // Предыдущая дата Очередное
                          model.getModelWithId(akt.dateO.pred).setValue(previousDataOnTheShip.data[i].key);
                          console.log('Предыдущая дата Очередное ' + previousDataOnTheShip.data[i].key);
                          if (previousDataOnTheShip.data[i].key >= currentDate()) {
                            // Следующая дата Очередное (если след.дата больше текущей даты)
                            model.getModelWithId(akt.dateO.sled).setValue(previousDataOnTheShip.data[i].key);
                            console.log('Следующая дата Очередное (если след.дата больше текущей даты) ' + previousDataOnTheShip.data[i].key);
                          } else {
                            // Следующая дата Очередное (+ 5 лет)
                            model.getModelWithId(akt.dateO.sled).setValue(getDateKey(previousDataOnTheShip.data[i].key, 5));
                            console.log('Следующая дата Очередное (+ 5 лет) ' + getDateKey(previousDataOnTheShip.data[i].key, 5));
                          }
                        }
                        if (previousDataOnTheShip.data[i].id == akt.dateS.sled) {
                          // Предыдущая дата На слипе
                          model.getModelWithId(akt.dateS.pred).setValue(previousDataOnTheShip.data[i].key);
                          console.log('Предыдущая дата На слипе ' + previousDataOnTheShip.data[i].key);
                          if (previousDataOnTheShip.data[i].key >= currentDate()) {
                            // Следующая дата На слипе (если след.дата больше текущей даты)
                            model.getModelWithId(akt.dateS.sled).setValue(previousDataOnTheShip.data[i].key);
                            console.log('Следующая дата На слипе (если след.дата больше текущей даты) ' + previousDataOnTheShip.data[i].key);
                          } else {
                            // Следующая дата На слипе (+ 5 лет)
                            model.getModelWithId(akt.dateS.sled).setValue(getDateKey(previousDataOnTheShip.data[i].key, 5));
                            console.log('Следующая дата На слипе (+ 5 лет) ' + getDateKey(previousDataOnTheShip.data[i].key, 5));
                          }
                        }
                        if (previousDataOnTheShip.data[i].id == akt.dateK.sled) {
                          // Предыдущая дата Классификационное
                          model.getModelWithId(akt.dateK.pred).setValue(previousDataOnTheShip.data[i].key);
                          console.log('Предыдущая дата Классификационное ' + previousDataOnTheShip.data[i].key);
                          if (previousDataOnTheShip.data[i].key >= currentDate()) {
                            // Следующая дата Классификационное (если след.дата больше текущей даты)
                            model.getModelWithId(akt.dateK.sled).setValue(previousDataOnTheShip.data[i].key);
                            console.log('Следующая дата Классификационное (если след.дата больше текущей даты) ' + previousDataOnTheShip.data[i].key);
                          } else {
                            // Следующая дата Классификационное (+ 5 лет)
                            model.getModelWithId(akt.dateK.sled).setValue(getDateKey(previousDataOnTheShip.data[i].key, 5));
                            console.log('Следующая дата Классификационное (+ 5 лет) ' + getDateKey(previousDataOnTheShip.data[i].key, 5));
                          }
                        }
                      } // конец цикла
                    } // success
                }); // ajax(
              } else console.log('не найдено предыдущих записей по данному судну');
            } // previousRecord.length >= 2
          } // success
      }); // ajax(
    });

    var dateOsvid = model.getModelWithId(akt.date);
    dateOsvid.on(AS.FORMS.EVENT_TYPE.valueChange, function() {
      model.getModelWithId(akt.dateE.sled).setValue(getDateKey(dateOsvid.value, 1)); //Следующая дата Ежегодное (+ 1 год)
      console.log('Следующая дата Ежегодное: ' + getDateKey(dateOsvid.value, 1));
    });

  }
});

// текущая дата в виде "YYYY-MM-DD 00:00:00"
function currentDate() {
  var today = new Date();
  var month = Number(today.getMonth() + 1);
  if (month < 10) month = '0' + month;
  return today.getFullYear() + '-' + (month) + '-' + ('0' + today.getDate()).slice(-2) + ' 00:00:00';
}
// функция преобразования дат
// на входе принимает дату вида "YYYY-MM-DD 00:00:00" и прибавляет t лет
function getDateKey(dateStr, t) {
  return (Number(dateStr.substring(0, 4)) + Number(t)) + dateStr.substring(4);
}
