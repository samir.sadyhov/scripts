AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  var akt = {
    year: { // АКТ ежегодного освидетельствования судна
      formID: '0fecf5ef-c0ab-48ea-b4b2-551a78d358df',
      cmpArr: ['est_net_kz1', 'est_net1', 'ispr_neispr_kz1', 'ispr_neispr1', 'sootv_nesootv_kz1', 'sootv_nesootv1', 'sootv_nesootv_kz2', 'sootv_nesootv2', 'obnarujeno_neobnarujeno_kz', 'obnarujeno_neobnarujeno', 'obnarujeno_neobnarujeno_kz1', 'obnarujeno_neobnarujeno1', 'est_net_kz2', 'est_net2'],
    },
    klassif: { // АКТ классификационного освидетельствования судна
      formID: '99cbaffd-33b9-4582-8c81-fbc8c0c3f5ac',
      cmpArr: ['k1', 'k2', 'k3', 'k4', 'k5'],
    },
    ochered: { // АКТ очередного освидетельствования судна
      formID: '43d29472-4fc7-4c86-b8aa-afab7e172991',
      cmpArr: ['o1', 'o2'],
    }
  };
  if (model.formId == akt.year.formID) {
    checkAndRecordResults(akt.year.cmpArr, model);
    for (var i = 0; i < akt.year.cmpArr.length; i++) {
      model.getModelWithId(akt.year.cmpArr[i]).on(AS.FORMS.EVENT_TYPE.valueChange, function() {
        checkAndRecordResults(akt.year.cmpArr, model);
      });
    }
  }
});

function checkAndRecordResults(cmpArr, model) {
  var res = 0;
  var resultKZ = model.getModelWithId('godnoe_negodnoe_kz');
  var resultRU = model.getModelWithId('godnoe_negodnoe');

  for (var j = 0; j < cmpArr.length; j++) {
    if (model.getModelWithId(cmpArr[j]).value[0] == '2') {
      res = 1;
    }
    console.log('checkResults ' + cmpArr[j] + ' = ' + model.getModelWithId(cmpArr[j]).value[0] + ' | res = ' + res);
  }

  if (res == 1) {
    resultKZ.setValue('2');
    resultRU.setValue('2');
  } else {
    resultKZ.setValue('1');
    resultRU.setValue('1');
  }
  console.log('godnoe_negodnoe_kz = ' + resultKZ.value[0]);
  console.log('godnoe_negodnoe = ' + resultRU.value[0]);
}
