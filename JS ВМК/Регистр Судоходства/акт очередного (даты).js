AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  if (model.formId == '43d29472-4fc7-4c86-b8aa-afab7e172991') {
    console.log("formID: [" + model.formId + "]");
    console.log("asfDataId: [" + model.asfDataId + "]");
    // общие настройки
    var synergy = {
      login: "administrator",
      password: "qazwsx123"
    };
    // АКТ очередного освидетельствования судна
    var akt = {
        regNumber: 'reg_nomer',
        date: 'data_osvid',
        dateE: {pred: 'pred_data_ezhegod', sled: 'sled_data_ezhegodnoe'},
        dateO: {pred: 'pred_data_ocherednoe', sled: 'sled_data_ochered'},
        dateS: {pred: 'pred_data__dok', sled: 'sled_data_dok'},
        dateK: {pred: 'pred_data_klassific', sled: 'sled_data_klass'}
    };

    // АКТ ежегодного освидетельствования судна
    var aktYear = {
        formID: '0fecf5ef-c0ab-48ea-b4b2-551a78d358df',
        regNumber: 'reg_nomer',
        date: 'year1',
        dateE: {pred: 'ezh1', sled: 'ezh2'},
        dateO: {pred: 'och1', sled: 'och2'},
        dateS: {pred: 'slip1', sled: 'slip2'},
        dateK: {pred: 'klas1', sled: 'klas2'}
    };

    // Получаем "Регистровый №", будет осуществлен поиск предыдущей записи по этому значению
    var regNumberModel = model.getModelWithId(akt.regNumber);
    regNumberModel.on(AS.FORMS.EVENT_TYPE.valueChange, function() {
      //console.log('Регистровый №: ' + regNumberModel.value);

      // Формирование поисковой строки (акт ежегодного освидетельствования судна)
      var paramSearchYear = '{"query": "where uuid = \''+aktYear.formID+'\' and `'+akt.regNumber+'.TEXT` = \''+regNumberModel.value+'\' order by `'+aktYear.date+'.DATE` desc", "parameters": ["'+akt.regNumber+'.TEXT", "'+aktYear.date+'.DATE"], "showDeleted": "false", "searchInRegistry": "true", "recordsCount": "10"}';

      $.ajax({
        url: "rest/api/asforms/search/advanced",
        contentType: "application/json; charset=utf-8",
        type: "POST",
        data: paramSearchYear,
        dataType: 'json',
        username: synergy.login,
        password: synergy.password,
        success: function(previousRecordYear) {
            console.log("Результат поиска (ежегод.): " + JSON.stringify(previousRecordYear));
            var yearDate=null;
            if (previousRecordYear.length >= 1) {
              var STATE_SUCCESSFUL = false;
              for (var i = 0; i < previousRecordYear.length; i++) {
                if (previousRecordYear[i].registryRecordStatus == 'STATE_SUCCESSFUL') {
                  previousRecordYear = previousRecordYear[i].dataUUID;
                  STATE_SUCCESSFUL = true;
                  break;
                } //if
              } //for
              if (STATE_SUCCESSFUL) {
                console.log('asfSataId последней записи по судну: ' + previousRecordYear);
                $.ajax({
                  url: 'rest/api/asforms/data/' + previousRecordYear,
                  dataType: 'json',
                  username: synergy.login,
                  password: synergy.password,
                  success: function(previousDataYear) {
                    if (previousDataYear.data.length > 0) {
                      for (var i = 0; i < previousDataYear.data.length; i++) {
                        if (previousDataYear.data[i].id == aktYear.dateK.sled) {
                          yearDate=previousDataYear.data[i].key; // вытаскиваем дату след. классифик. из последнего акта ежегодного освидетельствования судна
                          break;
                        }
                      }
                      if (yearDate) {
                        //Следующая дата Ежегодное (+ 1 год)
                        model.getModelWithId(akt.dateE.sled).setValue(getDateKey(yearDate, 1));
                        console.log('Следующая дата Ежегодное (+ 1 год) ' + getDateKey(yearDate, 1));
                        // Следующая дата Классификационное
                        model.getModelWithId(akt.dateK.sled).setValue(yearDate);
                        console.log('Следующая дата Классификационное ' + yearDate);
                      }
                    }
                  } // success
                }); // ajax(
              }
            } else console.log('не найдено предыдущих записей по данному судну (в реестре ежегод. освид.)');
          } // success
      }); // ajax(


      // Формирование поисковой строки
      var paramSearch = '{"query": "where uuid = \'' + model.formId + '\' and `' + akt.regNumber + '.TEXT` = \'' + regNumberModel.value + '\' order by `' + akt.date + '.DATE` desc", "parameters": ["' + akt.regNumber + '.TEXT", "' + akt.date + '.DATE"], "showDeleted": "false", "searchInRegistry": "true", "recordsCount": "10"}';

      //console.log('paramSearch: ' + paramSearch);
      $.ajax({
        url: "rest/api/asforms/search/advanced",
        contentType: "application/json; charset=utf-8",
        type: "POST",
        data: paramSearch,
        dataType: 'json',
        username: synergy.login,
        password: synergy.password,
        success: function(previousRecord) {
            console.log("Результат поиска: " + JSON.stringify(previousRecord));
            if (previousRecord.length >= 1) {
              var STATE_SUCCESSFUL = false;
              for (var i = 0; i < previousRecord.length; i++) {
                if (previousRecord[i].registryRecordStatus == 'STATE_SUCCESSFUL') {
                  previousRecord = previousRecord[i].dataUUID;
                  STATE_SUCCESSFUL = true;
                  break;
                } //if
              } //for
              if (STATE_SUCCESSFUL) {
                console.log('asfSataId последней записи по судну: ' + previousRecord);

                $.ajax({
                  url: 'rest/api/asforms/data/' + previousRecord,
                  dataType: 'json',
                  username: synergy.login,
                  password: synergy.password,
                  success: function(previousDataOnTheShip) {
                      //console.log("Данные по последней записи: " + JSON.stringify(previousDataOnTheShip));
                      for (var i = 0; i < previousDataOnTheShip.data.length; i++) {
                        if (previousDataOnTheShip.data[i].id == akt.dateE.sled) {
                          //Предыдущая дата Ежегодное
                          model.getModelWithId(akt.dateE.pred).setValue(previousDataOnTheShip.data[i].key);
                          console.log('Предыдущая дата Ежегодное ' + previousDataOnTheShip.data[i].key);
                        }
                        if (previousDataOnTheShip.data[i].id == akt.dateO.sled) {
                          // Предыдущая дата Очередное
                          model.getModelWithId(akt.dateO.pred).setValue(previousDataOnTheShip.data[i].key);
                          console.log('Предыдущая дата Очередное ' + previousDataOnTheShip.data[i].key);
                          // Следующая дата Очередное (+ 5 лет)
                          model.getModelWithId(akt.dateO.sled).setValue(getDateKey(previousDataOnTheShip.data[i].key, 5));
                          console.log('Следующая дата Очередное (+ 5 лет) ' + getDateKey(previousDataOnTheShip.data[i].key, 5));
                        }

                        if (previousDataOnTheShip.data[i].id == akt.dateS.sled) {
                          // Предыдущая дата На слипе
                          model.getModelWithId(akt.dateS.pred).setValue(previousDataOnTheShip.data[i].key);
                          console.log('Предыдущая дата На слипе ' + previousDataOnTheShip.data[i].key);
                          // Следующая дата На слипе
                          model.getModelWithId(akt.dateS.sled).setValue(getDateKey(previousDataOnTheShip.data[i].key, 5));
                          console.log('Следующая дата На слипе (+ 5 лет) ' + getDateKey(previousDataOnTheShip.data[i].key, 5));
                        }

                        if (previousDataOnTheShip.data[i].id == akt.dateK.sled) {
                          // Предыдущая дата Классификационное
                          model.getModelWithId(akt.dateK.pred).setValue(previousDataOnTheShip.data[i].key);
                          console.log('Предыдущая дата Классификационное ' + previousDataOnTheShip.data[i].key);
                        }

                      } // конец цикла
                    } // success
                }); // ajax(
              }
            } else console.log('не найдено предыдущих записей по данному судну');
          } // success
      }); // ajax(
    });

  }
});

// текущая дата в виде "YYYY-MM-DD 00:00:00"
function currentDate() {
  var today = new Date();
  var month = Number(today.getMonth() + 1);
  if (month < 10) month = '0' + month;
  return today.getFullYear() + '-' + (month) + '-' + ('0' + today.getDate()).slice(-2) + ' 00:00:00';
}
// функция преобразования дат
// на входе принимает дату вида "YYYY-MM-DD 00:00:00" и прибавляет t лет
function getDateKey(dateStr, t) {
  return (Number(dateStr.substring(0, 4)) + Number(t)) + dateStr.substring(4);
}
