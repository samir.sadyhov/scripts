AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  var akt = {
    year: { // АКТ ежегодного освидетельствования судна
      formID: '0fecf5ef-c0ab-48ea-b4b2-551a78d358df',
      cmpArr: ['cmp-o3g067', 'cmp-pdb07g', 'est_net_kz1', 'cmp-352jhq', 'cmp-bzs9sh', 'est_net1', 'cmp-2roh4f', 'ispr_neispr_kz1', 'cmp-ihclf2', 'ispr_neispr1', 'cmp-muialb', 'cmp-wsqa3k', 'sootv_nesootv_kz1', 'cmp-tovwgn', 'sootv_nesootv1', 'cmp-wunvt6', 'sootv_nesootv_kz2', 'cmp-b8mi2m', 'sootv_nesootv2', 'cmp-x3b9jg', 'cmp-0irl8e', 'cmp-d85c45', 'cmp-c48f07', 'cmp-piu417', 'obnarujeno_neobnarujeno_kz', 'cmp-7b5l5a', 'obnarujeno_neobnarujeno', 'cmp-bcg7ko', 'obnarujeno_neobnarujeno_kz1', 'cmp-myqh30', 'obnarujeno_neobnarujeno1', 'cmp-0da676', 'cmp-lm28sf', 'cmp-tumpqm', 'cmp-1uo68e', 'cmp-4xj0gu', 'cmp-utvit7', 'est_net_kz2', 'cmp-z9d527', 'est_net2', 'cmp-s7lgrk', 'cmp-rybwwq', 'cmp-ck44wg', 'cmp-mixuiu', 'cmp-1uq7lj', 'cmp-cah79o'],
      resArr: ['godnoe_negodnoe_kz', 'teh_sost_kz15', 'godnoe_negodnoe', 'teh_sost15']
    },
    klassif: { // АКТ классификационного освидетельствования судна
      formID: '99cbaffd-33b9-4582-8c81-fbc8c0c3f5ac',
      cmpArr: ['cmp-8ldn1o', 'pigodn_kz', 'cmp-hxspyt', 'bar', 'cmp-qytf4l', 'ispravni', 'cmp-63kx2j', 'inf', 'cmp-biy2lo', 'isp_kz', 'cmp-1b64hh', 'isp', 'cmp-uk9mwa', 'cmp-vmlvta', 'sootvet_kz', 'sootvet', 'cmp-6w36wv', 'konstr_zashity_kz', 'cmp-uouvex', 'konstr_zashity', 'cmp-5f94q8', 'cmp-fardoz', 'cmp-p9r5vw', 'cmp-8kfe0t', 'cmp-6hnamc', 'ust_kz', 'cmp-g3cmmf', 'ust', 'cmp-5k02j1', 'bnaruzheno_kz', 'cmp-tcyy86', 'cmp-4gz9lp', 'cmp-2pasdj', 'cmp-vlvhdg', 'plomb_kz', 'cmp-idlk71', 'plomb', 'cmp-7qmagp', 'cmp-g6svsm', 'cmp-flexns', 'cmp-39ksv2', 'cmp-nioyxf', 'xx', 'cmp-hkv0fv', 'xx2', 'cmp-n2o7zj', 'cmp-k59nln', 'cmp-wlu9ts', 'cmp-lhuixm'],
      resArr: ['cmp-t2q4wy', 'cmp-sy5ljh', 'cmp-14x4mw', 'cmp-y7tr6n', 'cmp-tki8fs']
    }
  };
  // АКТ ежегодного освидетельствования судна
  if (model.formId == akt.year.formID) {
    checkAndRecordResults(model, akt.year.cmpArr, akt.year.resArr);
    for (var i = 0; i < akt.year.cmpArr.length; i++) {
      model.getModelWithId(akt.year.cmpArr[i]).on(AS.FORMS.EVENT_TYPE.valueChange, function() {
        checkAndRecordResults(model, akt.year.cmpArr, akt.year.resArr);
      });
    }
  }
  // АКТ классификационного освидетельствования судна
  if (model.formId == akt.klassif.formID) {
    checkAndRecordResults(model, akt.klassif.cmpArr, akt.klassif.resArr);
    for (var i = 0; i < akt.klassif.cmpArr.length; i++) {
      model.getModelWithId(akt.klassif.cmpArr[i]).on(AS.FORMS.EVENT_TYPE.valueChange, function() {
        checkAndRecordResults(model, akt.klassif.cmpArr, akt.klassif.resArr);
      });
    }
  }
});

function checkAndRecordResults(model, cmpArr, resArr) {
  var res = 0;
  for (var j = 0; j < cmpArr.length; j++) {
    if (model.getModelWithId(cmpArr[j]).value[0] == '2') {
      res = 1;
    }
    console.log('checkResults ' + cmpArr[j] + ' = ' + model.getModelWithId(cmpArr[j]).value[0] + ' | res = ' + res);
  }
  if (res == 1) {
    for (var k = 0; k < resArr.length; k++) {
      model.getModelWithId(resArr[k]).setValue('2');
    }
  } else {
    for (var k = 0; k < resArr.length; k++) {
      model.getModelWithId(resArr[k]).setValue('1');
    }
  }
}
