AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view){
  var apiDep = 'rest/api/departments/get?departmentID=';
  var synergyLogin = 'kamyrov';
  var synergyPass = '123456';
  var parDepID;

  var kafedraComponentID = 'kafedra1';
  var facultetComponentID = 'facultet';
  if (document.getElementById('facultet')) {
    facultetComponentID = 'facultet';
  }
  if (document.getElementById('fakultet')) {
    facultetComponentID = 'fakultet';
  }
  if (document.getElementById('facultet2')) {
    facultetComponentID = 'facultet2';
  }
    var cafedraModel = model.getModelWithId(kafedraComponentID);
    var facultyModel = model.getModelWithId(facultetComponentID);
    cafedraModel.on(AS.FORMS.EVENT_TYPE.valueChange, function(){
        var value = cafedraModel.value;
        if(value && value.length > 0) {
            value = value[0];
            console.log(value);

            jQuery.ajax({
              url: apiDep + value.departmentId,
              dataType: 'json',
              username: synergyLogin,
              password: synergyPass,
              success: function(data) {
                if (value.departmentId == '1') {
                  parDepID = '1';
                } else {
                  parDepID = data.parentDepartmentID;
                }
                jQuery.ajax({
                  url: apiDep + parDepID,
                  dataType: 'json',
                  username: synergyLogin,
                  password: synergyPass,
                  success: function(data) {
                    var parentData = [{
                      departmentId: parDepID,
                      departmentName: data.nameKz,
                      tagName: data.nameKz
                    }];
                    facultyModel.setValue(parentData);
                  }
                });
              }
            });
        }
    });
});
