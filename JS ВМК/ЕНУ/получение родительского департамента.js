$EVENT_BUS.subscribe(new EventHandler('DEPARTMENT_ENTITY_CHANGED', function(event, componentValue) {
  getParentDepartmentData(componentValue.args.values[0].key);
}));

function getParentDepartmentData(childDepartment) {
  var apiDep = 'rest/api/departments/get?departmentID=';
  var synergyLogin = '1';
  var synergyPass = '1';
  var parDepID;
  var parDepName;

  jQuery.ajax({
    url: apiDep + childDepartment,
    dataType: 'json',
    username: synergyLogin,
    password: synergyPass,
    success: function(data) {
      if (childDepartment == '1') {
        parDepID = '1';
      } else {
        parDepID = data.parentDepartmentID;
      }
      jQuery.ajax({
        url: apiDep + parDepID,
        dataType: 'json',
        username: synergyLogin,
        password: synergyPass,
        success: function(data) {
          parDepName = data.nameRu;
          setParentDepartmentData(parDepName, parDepID);
        }
      });
    }
  });
}

var setParentDepartmentData = function(d1, d2) {
  var parentComponentID = "parentDep";
  var parentData = {
    id: parentComponentID,
    type: "entity",
    value: d1,
    key: d2
  };
  window.setFormEntityData(parentComponentID, parentData);
}
