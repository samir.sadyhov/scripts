<form>
  <span>locale</span>
  <input type="radio" name="locale" value="ru" checked>RU
  <input type="radio" name="locale" value="kk">KZ
  <input type="radio" name="locale" value="en">EN
</form>

<form>
  <span>number</span><br>
  <input type="number" class="from" placeholder="введите число" style="width: 300px" step="10">
</form>

<form>
  <span>string number</span><br>
  <input type="text" class="to" style="width: 300px">
</form>

<form>
  <span>10</span>
  <input type="range" name="points" min="10" max="10000" step="10" style="width: 230px">
  <span>10000</span>
</form>

<form>
  <input type="reset" name="reset">
</form>



var locale = "ru";
var translate = {
  kk: {
    "один": "бір",
    "два": "екі",
    "три": "үш",
    "четыре": "төрт",
    "пять": "бес",
    "шесть": "алты",
    "семь": "жеті",
    "восемь": "сегіз",
    "девять": "тоғыз",
    "десять": "он",
    "одиннадцать": "он бір",
    "двенадцать": "он екі",
    "тринадцать": "он үш",
    "четырнадцать": "он төрт",
    "пятнадцать": "он бес",
    "шестнадцать": "он алты",
    "семнадцать": "он жеті",
    "восемнадцать": "он сегіз",
    "девятнадцать": "он тоғыз",
    "двадцать": "жиырма",
    "тридцать": "отыз",
    "сорок": "қырық",
    "пятьдесят": "елу",
    "шестьдесят": "алпыс",
    "семьдесят": "жетпіс",
    "восемьдесят": "сексен",
    "девяносто": "тоқсан",
    "сто": "жүз",
    "двести": "екі жүз",
    "триста": "үш жүз",
    "четыреста": "төрт жүз",
    "пятьсот": "бес жүз",
    "шестьсот": "алты жүз",
    "семьсот": "жеті жүз",
    "восемьсот": "сегіз жүз",
    "девятьсот": "тоғыз жүз",
    "тысяча": "бір мың",
    "тысячи": "мыңдағандар",
    "тысяч": "мың",
    "миллион": "миллион",
    "миллиона": "миллион",
    "миллионов": "миллион",
    "миллиард": "миллиард",
    "миллиарда": "миллиард",
    "миллиардов": "миллиард",
    "триллион": "триллион",
    "триллиона": "триллион",
    "триллионов": "триллион",
    "квадриллион": "квадриллион",
    "квадриллиона": "квадриллион",
    "квадриллионов": "квадриллион",
    "квинтиллион": "квинтиллион",
    "квинтиллиона": "квинтиллион",
    "квинтиллионов": "квинтиллион",
    "одна": "біреуі",
    "две": "екі"
  },
  en: {
    "один": "one",
    "два": "two",
    "три": "three",
    "четыре": "four",
    "пять": "five",
    "шесть": "six",
    "семь": "seven",
    "восемь": "eight",
    "девять": "nine",
    "десять": "ten",
    "одиннадцать": "eleven",
    "двенадцать": "twelve",
    "тринадцать": "thirteen",
    "четырнадцать": "fourteen",
    "пятнадцать": "fifteen",
    "шестнадцать": "sixteen",
    "семнадцать": "seventeen",
    "восемнадцать": "eighteen",
    "девятнадцать": "nineteen",
    "двадцать": "twenty",
    "тридцать": "thirty",
    "сорок": "forty",
    "пятьдесят": "fifty",
    "шестьдесят": "sixty",
    "семьдесят": "seventy",
    "восемьдесят": "eighty",
    "девяносто": "ninety",
    "сто": "one hundred",
    "двести": "two hundred",
    "триста": "three hundred",
    "четыреста": "four hundred",
    "пятьсот": "five hundred",
    "шестьсот": "six hundred",
    "семьсот": "seven hundred",
    "восемьсот": "eight hundred",
    "девятьсот": "nine hundred",
    "тысяча": "one thousand",
    "тысячи": "thousands",
    "тысяч": "thousands",
    "миллион": "million",
    "миллиона": "million",
    "миллионов": "millions",
    "миллиард": "billion",
    "миллиарда": "billion",
    "миллиардов": "billions",
    "триллион": "trillion",
    "триллиона": "trillion",
    "триллионов": "trillions",
    "квадриллион": "quadrillion",
    "квадриллиона": "quadrillion",
    "квадриллионов": "quadrillion",
    "квинтиллион": "quintillion",
    "квинтиллиона": "quintillion",
    "квинтиллионов": "quintillion",
    "одна": "one",
    "две": "two"
  }
};

function trans(value) {
  if (typeof locale === "undefined" || locale == 'ru') return value;
  return translate[locale][value];
}

function numberToText(c) {
  function k(b, c) {
    var d = c[0],
      e = c[1],
      f = c[2];
    return b % 10 == 1 && b % 100 != 11 ? d : b % 10 >= 2 && b % 10 <= 4 && (b % 100 < 10 || b % 100 >= 20) ? e : f
  }
  for (var d = {
      0: {
        1: trans("один"),
        2: trans("два"),
        3: trans("три"),
        4: trans("четыре"),
        5: trans("пять"),
        6: trans("шесть"),
        7: trans("семь"),
        8: trans("восемь"),
        9: trans("девять"),
        10: trans("десять"),
        11: trans("одиннадцать"),
        12: trans("двенадцать"),
        13: trans("тринадцать"),
        14: trans("четырнадцать"),
        15: trans("пятнадцать"),
        16: trans("шестнадцать"),
        17: trans("семнадцать"),
        18: trans("восемнадцать"),
        19: trans("девятнадцать"),
        20: trans("двадцать"),
        30: trans("тридцать"),
        40: trans("сорок"),
        50: trans("пятьдесят"),
        60: trans("шестьдесят"),
        70: trans("семьдесят"),
        80: trans("восемьдесят"),
        90: trans("девяносто"),
        100: trans("сто"),
        200: trans("двести"),
        300: trans("триста"),
        400: trans("четыреста"),
        500: trans("пятьсот"),
        600: trans("шестьсот"),
        700: trans("семьсот"),
        800: trans("восемьсот"),
        900: trans("девятьсот")
      },
      1: {
        1: trans("одна"),
        2: trans("две")
      }
    }, i = {
      0: ["", "", ""],
      1: [trans("тысяча"), trans("тысячи"), trans("тысяч")],
      2: [trans("миллион"), trans("миллиона"), trans("миллионов")],
      3: [trans("миллиард"), trans("миллиарда"), trans("миллиардов")],
      4: [trans("триллион"), trans("триллиона"), trans("триллионов")],
      5: [trans("квадриллион"), trans("квадриллиона"), trans("квадриллионов")],
      6: [trans("квинтиллион"), trans("квинтиллиона"), trans("квинтиллионов")]
    }, h = "", j = (("" + c).match(/(\d{1,3})(?=((\d{3})*([^\d]|$)))/g) || []).reverse(), e = 0; e < j.length; e++) {
    for (var f = d[e], c = j[e], b = "", g = 0; g < c.length; g++)
      if (a = c.substr(g), f && f[a] || d[0][a]) {
        b = b + " " + (f && f[a] || d[0][a]);
        break;
      } else a = +c.substr(g, 1) * Math.pow(10, a.length - 1), +a in d[0] && (b = b + " " + d[0][a]);
    b && (b = b + " " + k(+c, i[e] || i[0]));
    h = b + h
  }
  return h || ""
}

$('[name="reset"]').on("click", function() {
  $('.from').val("").trigger('change');
});

$('[name="locale"]').bind("change paste keyup", function() {
  locale = $(this).val();
  $('.from').trigger('change');
});

$('[name="points"]').bind("change paste keyup", function() {
  $('.from').val($(this).val()).trigger('change');
});


$('.from').bind("change paste keyup", function() {
  $('.to').val(numberToText($(this).val()));
});
//console.log(numberToText(15));
