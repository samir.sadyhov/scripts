function compareDates(playerView, cmpStart, cmpStop, resCmp, table, blockNumber) {
  var startDate = null;
  var stopDate = null;
  var resultBox = null;
  if (table == null && blockNumber == null) {
    startDate = playerView.model.getModelWithId(cmpStart);
    stopDate = playerView.model.getModelWithId(cmpStop);
    if (resCmp != null) resultBox = playerView.model.getModelWithId(resCmp);
  } else {
    startDate = playerView.model.getModelWithId(cmpStart, table.ru, blockNumber);
    stopDate = playerView.model.getModelWithId(cmpStop, table.ru, blockNumber);
    if (resCmp != null) resultBox = playerView.model.getModelWithId(resCmp, table.ru, blockNumber);
  }

  if (startDate && stopDate) {
    startDate.on("valueChange", function() {
      if (AS.FORMS.DateUtils.parseDate(startDate.getValue()) !=null && AS.FORMS.DateUtils.parseDate(stopDate.getValue()) !=null ) {
        var d1 = AS.FORMS.DateUtils.parseDate(startDate.getValue()).getTime();
        var d2 = AS.FORMS.DateUtils.parseDate(stopDate.getValue()).getTime();
        var dd = Math.floor((d2-d1) / 86400000);
        if (dd < 0) {
          if (table == null && blockNumber == null) {
            playerView.getViewWithId(cmpStop).container.parent().css("background-color", "#FF9999");
            playerView.getViewWithId(cmpStop+"_k").container.parent().css("background-color", "#FF9999");
          } else {
            playerView.getViewWithId(cmpStop, table.ru, blockNumber).container.parent().css("background-color", "#FF9999");
            playerView.getViewWithId(cmpStop+"_k", table.kz, blockNumber).container.parent().css("background-color", "#FF9999");
          }
          alert("Дата ПО [" + stopDate.getValue() + "] меньше даты С [" + startDate.getValue() + "]");
          if (resultBox) resultBox.setValue("");
          stopDate.setValue("");
          stopDate.asfProperty.required=true;
        } else {
          if (resultBox) resultBox.setValue(dd+"");
          if (table == null && blockNumber == null) {
            playerView.getViewWithId(cmpStop).container.parent().css("background-color", "");
            playerView.getViewWithId(cmpStop+"_k").container.parent().css("background-color", "");
          } else {
            playerView.getViewWithId(cmpStop, table.ru, blockNumber).container.parent().css("background-color", "");
            playerView.getViewWithId(cmpStop+"_k", table.kz, blockNumber).container.parent().css("background-color", "");
          }
        }
      } else {
        if (resultBox) resultBox.setValue("");
      }
    });
    stopDate.on("valueChange", function() {
      if (AS.FORMS.DateUtils.parseDate(startDate.getValue()) !=null && AS.FORMS.DateUtils.parseDate(stopDate.getValue()) !=null ) {
        var d1 = AS.FORMS.DateUtils.parseDate(startDate.getValue()).getTime();
        var d2 = AS.FORMS.DateUtils.parseDate(stopDate.getValue()).getTime();
        var dd = Math.floor((d2-d1) / 86400000);
        if (dd < 0) {
          if (table == null && blockNumber == null) {
            playerView.getViewWithId(cmpStop).container.parent().css("background-color", "#FF9999");
            playerView.getViewWithId(cmpStop+"_k").container.parent().css("background-color", "#FF9999");
          } else {
            playerView.getViewWithId(cmpStop, table.ru, blockNumber).container.parent().css("background-color", "#FF9999");
            playerView.getViewWithId(cmpStop+"_k", table.kz, blockNumber).container.parent().css("background-color", "#FF9999");
          }
          alert("Дата ПО [" + stopDate.getValue() + "] меньше даты С [" + startDate.getValue() + "]");
          if (resultBox) resultBox.setValue("");
          stopDate.setValue("");
          stopDate.asfProperty.required=true;
        } else {
          if (resultBox) resultBox.setValue(dd+"");
          if (table == null && blockNumber == null) {
            playerView.getViewWithId(cmpStop).container.parent().css("background-color", "");
            playerView.getViewWithId(cmpStop+"_k").container.parent().css("background-color", "");
          } else {
            playerView.getViewWithId(cmpStop, table.ru, blockNumber).container.parent().css("background-color", "");
            playerView.getViewWithId(cmpStop+"_k", table.kz, blockNumber).container.parent().css("background-color", "");
          }
        }
      } else {
        if (resultBox) resultBox.setValue("");
      }
    });
  }
}


AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, model, view) {
  if (model.formId == "id формы" || model.formId == "id формы2") {
    compareDates(view, "start_date", "finish_date", "srok", null, null);
    //
  }
});
