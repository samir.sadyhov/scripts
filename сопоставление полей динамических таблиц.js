//параметры сопоставления
let matchingTable = {
  table: {from: 'table_cargo_info', to: 'table_ttn'},
  cmpIds: []
};

//также можно передать дополнительное сопоставление из ссылки на реестр
/*
{from: 'reglink_from', to: 'reglink_to', children: [
  {from: 'research_method', to: 'research_method'},
  {from: 'num_price', to: 'num_price'}
]}
*/

//Настройки сопоставления из РКК документа
//в объекте to, ключ является именем поля из апи rest/api/docflow/doc/document_info
//а значением ключя является имя поля в динамической таблице
matchingTable.cmpIds.push({
  from: 'RKK_DATA',
  to: {
    number: 'textbox_number_ttn', //номер документа
    regDate: 'date_ttn' //время и дата регистрации документа в формате (DD.MM.YY hh:mm)
  }
});

matchingTable.cmpIds.push({from: 'checkbox_dangerous_cargo', to: 'checkbox_dangerous_cargo'});
matchingTable.cmpIds.push({from: 'name_dangerous_cargo', to: 'name_dangerous_cargo'});
matchingTable.cmpIds.push({from: 'textbox_class_dangerous_cargo', to: 'textbox_class_dangerous_cargo'});
matchingTable.cmpIds.push({from: 'textbox_dangerous_cargo_doc_number', to: 'textbox_dangerous_cargo_doc_number'});
matchingTable.cmpIds.push({from: 'textbox_dangerous_cargo_doc_type', to: 'textbox_dangerous_cargo_doc_type'});
matchingTable.cmpIds.push({from: 'date_of_issue', to: 'date_of_issue'});
matchingTable.cmpIds.push({from: 'date_expiration_date', to: 'date_expiration_date'});

let currentValue = model.getValue();

setTimeout(() => {
  currentValue = model.getValue();
}, 500);

const getTableBlocksCount = data => {
  data = data.data ? data.data : data;
  data = data.filter(row => row.type !== 'label');
  data = data[data.length - 1].id;
  return data.slice(data.indexOf('-b') + 2) * 1;
}

const removeRow = tableModel => {
  for(let numb in tableModel.getBlockNumbers()) tableModel.removeRow(numb);
}

const getAsfData = (asfData, cmpID) => asfData.data.find(x => x.id === cmpID);

const setField = (from, modelTo) => {
  if(from) {
    switch (from.type) {
      case 'textbox':
      case 'textarea':
        if(modelTo && from.hasOwnProperty('value') && from.value) {
          modelTo.setValue(from.value);
        } else {
          if(modelTo) modelTo.setValue(null);
        }
        break;
      case 'check':
        if(modelTo && from.hasOwnProperty('values')) {
          modelTo.setValue(from.values);
        } else {
          if(modelTo) modelTo.setValue(null);
        }
        break;
      case 'entity':
        if(modelTo && from.hasOwnProperty('key') && from.key && from.key !== 'null') {
          switch (modelTo.asfProperty.config.entity) {
            case "users":
              modelTo.setValue({
                personID: from.key,
                personName: from.value
              });
              break;
            case "departments":
              modelTo.setValue({
                departmentId: from.key,
                departmentName: from.value
              });
              break;
            case "positions":
              modelTo.setValue({
                elementID: from.key,
                elementName: from.value
              });
              break;
          }
        } else {
          if(modelTo) modelTo.setValue(null);
        }
        break;
      default:
        if(modelTo && from.hasOwnProperty('key') && from.key) {
          modelTo.setValue(from.key);
        } else {
          if(modelTo) modelTo.setValue(null);
        }
    }
  } else {
    if(modelTo) modelTo.setValue(null);
  }
}

const setRkkData = (documentID, fields, row) => {
  AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/docflow/doc/document_info?documentID=${documentID}`)
  .then(rkk => {
    for(let key in fields) {
      let tmpModel = row.filter(x => x.asfProperty.id === fields[key])[0];
      if(tmpModel) tmpModel.setValue(rkk[key]);
    }
  });
}

const setChildrenFields = (childrenModel, documentID, matching) => {
  $.when(AS.FORMS.ApiUtils.getAsfDataUUID(documentID))
  .then(uuid => AS.FORMS.ApiUtils.loadAsfData(uuid))
  .then(asfData => {
    matching.forEach(id => {
      let tmpModel = childrenModel.playerModel.getModelWithId(id.to, childrenModel.asfProperty.ownerTableId, childrenModel.asfProperty.tableBlockIndex);
      let tmpField = getAsfData(asfData, id.from);
      setField(tmpField, tmpModel);
    });
  });
}

const fillTable = documentIDs => {
  AS.SERVICES.showWaitWindow();
  let tableToModel = model.playerModel.getModelWithId(matchingTable.table.to);
  removeRow(tableToModel);

  documentIDs.forEach(documentID => {
    $.when(AS.FORMS.ApiUtils.getAsfDataUUID(documentID))
    .then(uuid => AS.FORMS.ApiUtils.loadAsfData(uuid))
    .then(asfData => {
      let tableFromData = getAsfData(asfData, matchingTable.table.from);
      let tableBlocks = getTableBlocksCount(tableFromData);

      for(let i = 1; i <= tableBlocks; i++) {
        let tableRow = tableToModel.createRow();
        matchingTable.cmpIds.forEach(item => {
          if(item.from === 'RKK_DATA') {
            setRkkData(documentID, item.to, tableRow);
          } else {
            let tmpField = getAsfData(tableFromData, `${item.from}-b${i}`);
            let tmpModel = tableRow.filter(x => x.asfProperty.id === item.to)[0];
            setField(tmpField, tmpModel);
            if(item.hasOwnProperty('children') && item.children.length > 0) {
              setChildrenFields(tmpModel, tmpField.key, item.children);
            }
          }
        });
      }

    })
    .fail(err => console.log(err))
    .always(() => AS.SERVICES.hideWaitWindow());
  });
}

const matchingTableInit = value => {
  if(!editable || !view.playerView.editable) return;
  if(!value) return;
  if(currentValue == value) return;
  currentValue = value;
  fillTable(value);
}

setTimeout(() => {
  model.on("valueChange", (_1, _2, value) => matchingTableInit(value));
}, 1000);
