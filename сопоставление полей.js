const matching = [];
matching.push({from: 'cmp_from', to: 'cmp_to'});

const clearFields = () => {
  matching.forEach(id => {
    let tmpModel = model.playerModel.getModelWithId(id.to, model.asfProperty.ownerTableId, model.asfProperty.tableBlockIndex);
    if (tmpModel) tmpModel.setValue(null);
  });
}

const setField = (from, modelTo) => {
  if(from) {
    switch (from.type) {
      case 'textbox':
      case 'textarea':
        if(modelTo && from.hasOwnProperty('value') && from.value) {
          modelTo.setValue(from.value);
        } else {
          if(modelTo) modelTo.setValue(null);
        }
        break;
      case 'check':
        if(modelTo && from.hasOwnProperty('values')) {
          modelTo.setValue(from.values);
        } else {
          if(modelTo) modelTo.setValue(null);
        }
        break;
      case 'entity':
        if(modelTo && from.hasOwnProperty('key') && from.key && from.key !== 'null') {
          switch (modelTo.asfProperty.config.entity) {
            case "users":
              modelTo.setValue({
                personID: from.key,
                personName: from.value
              });
              break;
            case "departments":
              modelTo.setValue({
                departmentId: from.key,
                departmentName: from.value
              });
              break;
            case "positions":
              modelTo.setValue({
                elementID: from.key,
                elementName: from.value
              });
              break;
          }
        } else {
          if(modelTo) modelTo.setValue(null);
        }
        break;
      default:
        if(modelTo && from.hasOwnProperty('key') && from.key) {
          modelTo.setValue(from.key);
        } else {
          if(modelTo) modelTo.setValue(null);
        }
    }
  } else {
    if(modelTo) modelTo.setValue(null);
  }
}

const matchingInit = docID => {
  if(!editable) return;
  if(!docID) {
    clearFields();
    return;
  }

  AS.SERVICES.showWaitWindow();
  try {
    AS.FORMS.ApiUtils.getAsfDataUUID(docID)
    .then(uuid => AS.FORMS.ApiUtils.loadAsfData(uuid))
    .then(asfData => {
      matching.forEach(item => {
        let from = asfData.data.find(x => x.id === item.from);
        let modelTo = model.playerModel.getModelWithId(item.to, model.asfProperty.ownerTableId, model.asfProperty.tableBlockIndex);
        setField(from, modelTo);
      });
      AS.SERVICES.hideWaitWindow();
    });
  } catch (e) {
    AS.SERVICES.hideWaitWindow();
  }
}

model.on('valueChange', (_1, _2, value) => matchingInit(value));
