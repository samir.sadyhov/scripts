const printForm = content => {
  var WinPrint = window.open();
  WinPrint.document.write('');
  WinPrint.document.write(content.innerHTML);
  WinPrint.document.write('');
  WinPrint.document.close();
  WinPrint.focus();
  setTimeout(() => WinPrint.print(), 1000);
}

function initCustomButton(){
  $('.custom-print-button').remove(); //грохаем кнопки если уже есть
  let originPrintButton = $('[src="light/images/simple.button/dark.gray.slim/print.png"]').closest('table').parent().closest('table');
  originPrintButton.hide(); //прячем оригинальную кнопку

  let container = originPrintButton.closest('tr');
  let td = $('<td align="left" style="vertical-align: top;">');
  let customPrintButton = $('<button>', {class: "custom-print-button"});

  container.append(td.append(customPrintButton)); //вставляем свою кнопку
  customPrintButton.on('click', e => {
    e.preventDefault();
    e.target.blur();
    printForm(view.playerView.container[0]);
  });
}

if (!editable) {
  //Инициализируем кнопку если форма в режиме просмотра
  initCustomButton();
} else {
  //Иначе грохаем если есть
  $('.custom-print-button').remove();
}


/*
template.html

<style>
  .custom-print-button {
    display: block;
    cursor: pointer;
    width: 37px;
    height: 25px;
    background: #f1f1f1 url("light/images/simple.button/dark.gray.slim/print.png") no-repeat center;
    border: 1px solid #cfcfcf;
    border-radius: 2px;
    cursor: pointer;
    transition: .3s;
  }
  .custom-print-button:hover {
    background-color: #f9f9f9;
  }
</style>

*/
