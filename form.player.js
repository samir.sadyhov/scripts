webpackJsonp([0],{

/***/ 100:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.insertEnteredData = insertEnteredData;
exports.splitMask = splitMask;
exports.initInput = initInput;
exports.mobileInitInput = mobileInitInput;
exports.getDiff = getDiff;
exports.getXRegExpression = getXRegExpression;
exports.getXRegSymbolExpression = getXRegSymbolExpression;
exports.isStaticSymbol = isStaticSymbol;
exports.getStaticSymbolValue = getStaticSymbolValue;
exports.getValueForInput = getValueForInput;
exports.getValueFromInput = getValueFromInput;
exports.isValidSymbol = isValidSymbol;
exports.escapeRegExp = escapeRegExp;

var _xregexp = __webpack_require__(154);

var _xregexp2 = _interopRequireDefault(_xregexp);

var _caretUtils = __webpack_require__(127);

var caretUtils = _interopRequireWildcard(_caretUtils);

var _constants = __webpack_require__(2);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * вставить значение в указанное место если значение подходит под маску
 * @param enterData значение
 * @param caretPosition полоджение курсора
 * @param splitMask маска
 * @param textBox тектовое поле
 */
function insertEnteredData(enterData, caretPosition, splitMask, textBox) {

    var value = textBox.val();

    // запрещает печатать что-либо длинее маски
    if (enterData !== null && caretPosition === splitMask.length) {
        return;
    }

    // проверяем если данные такой же длины как маска - пропускаем
    if (enterData && enterData.length > 0) {
        if (isValidSymbol(enterData, splitMask[caretPosition])) {
            value = value.substring(0, caretPosition) + enterData + value.substring(caretPosition + 1);
            caretPosition++;
        }

        while (caretPosition < splitMask.length) {
            if (!isStaticSymbol(splitMask[caretPosition])) {
                break;
            }
            caretPosition++;
        }

        textBox.val(value);

        caretUtils.setCaretPosition(textBox[0], caretPosition);
    }
}

function splitMask(mask) {
    var maskRegExp = [];
    var maskSymbols = mask.split('');
    for (var i = 0; i < maskSymbols.length; i++) {
        var maskSymbol = maskSymbols[i];
        if (maskSymbol == '\\' && i + 1 < maskSymbols.length) {
            i++;
            maskRegExp.push("\\" + maskSymbols[i]);
        } else {
            maskRegExp.push(maskSymbols[i]);
        }
    }
    return maskRegExp;
}

/**
 * инициализация поля ввода для маски
 * @param model
 * @param textBox
 */
function initInput(model, textBox) {
    if (_constants.OPTIONS.mobilePlayer) return mobileInitInput.call(this, model, textBox);

    var inputMask = model.asfProperty.config['input-mask'];

    var mask = splitMask(inputMask);

    var defaultValue = getValueForInput('', mask);

    textBox.val(defaultValue);

    var checkEnteredValue = function checkEnteredValue(event) {

        // если это что-то не печатаемое - пропускаем

        if (event.keyCode === _constants.KEY_CODES.delete || event.keyCode === _constants.KEY_CODES.backspace) {
            event.preventDefault();
            event.stopPropagation();
            return false;
        }

        if (event.which < 8) {
            return;
        }

        // если это попытка отменить то не разрешаем
        if ((event.ctrlKey || event.metaKey) && event.keyCode == _constants.KEY_CODES.Z_KEY) {
            event.preventDefault();
            return false;
        }

        var caretPosition = caretUtils.getCaretPosition(textBox[0]);
        var enterData = String.fromCharCode(event.which);

        insertEnteredData(enterData, caretPosition, mask, textBox);

        event.preventDefault();
        event.stopPropagation();
    };

    /**
     * проверка удаляемых данных
     * @param event
     */
    var checkDeletedData = function checkDeletedData(event) {
        var value = textBox.val();
        var caretPosition = caretUtils.getCaretPosition(textBox[0]);
        if (event.keyCode === _constants.KEY_CODES.delete) {
            if (caretPosition >= value.length - 1) {
                return;
            }
            var replaceSymbol = mask[caretPosition];
            if (!isStaticSymbol(replaceSymbol)) {
                replaceSymbol = "_";
            }
            value = value.substring(0, caretPosition) + replaceSymbol + value.substring(caretPosition + 1);
            textBox.val(value);

            caretUtils.setCaretPosition(textBox[0], caretPosition);

            event.preventDefault();
            event.stopPropagation();
        } else if (event.keyCode === _constants.KEY_CODES.backspace) {
            if (caretPosition === 0) {
                return;
            }
            var replaceSymbol = mask[caretPosition - 1];
            if (!isStaticSymbol(replaceSymbol)) {
                replaceSymbol = "_";
            }

            value = value.substring(0, caretPosition - 1) + getStaticSymbolValue(replaceSymbol) + value.substring(caretPosition);
            textBox.val(value);
            caretPosition = caretPosition - 1;
            caretUtils.setCaretPosition(textBox[0], caretPosition);

            event.preventDefault();
            event.stopPropagation();
        }
    };

    var checkPaste = function checkPaste() {
        // берем положение каретки
        var value = textBox.val();
        var formattedValue = getValueForInput(value, mask);
        textBox.val(formattedValue);
    };

    textBox.on("keypress", checkEnteredValue);

    textBox.on("keydown", checkDeletedData);

    textBox.on("paste", function () {
        setTimeout(function () {
            checkPaste();
        }, 0);
    });

    textBox.on("cut", function () {
        setTimeout(function () {
            checkPaste();
        }, 0);
    });

    textBox.on("change", function () {
        checkPaste();
    });
}

function mobileInitInput(model, textBox) {

    var inputMask = model.asfProperty.config['input-mask'];

    var splitMask = splitMask(inputMask);

    var defaultValue = getValueForInput('', splitMask);

    var prevValue = "";

    var caretPosition = 0;

    textBox.val(defaultValue);

    var checkPaste = function checkPaste() {
        // берем положение каретки
        var value = textBox.val();
        var formattedValue = getValueForInput(value, splitMask);
        textBox.val(formattedValue);
    };

    textBox.keydown(function () {
        prevValue = textBox.val();
    });

    textBox.on("input", function () {

        var value = textBox.val();
        var diff = getDiff(value, prevValue);

        if (diff) {
            if (diff.delta.length === 1) {
                insertEnteredData(diff.delta, diff.startIndex, splitMask, textBox);
                prevValue = textBox.val();
                return;
            }
        }
        caretPosition = caretUtils.getCaretPosition(textBox[0]);
        checkPaste();
        caretUtils.setCaretPosition(textBox[0], caretPosition);
        prevValue = textBox.val();
    });
};

/**
 * получение разницы ВВЕДЕННОЙ пользователем между  prevValue и value
 * @param value - значение которое получилось после ввода пользователем символов (именно ввода а не удаления0
 * @param prevValue - предыдущее значение
 * @returns {delta : string, startIndex : integer} значение разницы, и значение начала разницы
 */
function getDiff(value, prevValue) {
    var delta = '';
    var startIndex = 0;

    var length = value.length - prevValue.length;
    for (var i = 0; i < value.length; i++) {
        var str = value.substr(0, i) + value.substr(i + length);

        if (str === prevValue) {
            delta = value.substr(i, length);
            startIndex = i;
        }
    }

    if (delta === '') {
        return null;
    }
    return { delta: delta, startIndex: startIndex };
}

/**
 * получение регулярного выражения для проверки введенных данных
 * @param mask
 * @returns {string}
 */
function getXRegExpression(mask) {
    var xRegValue = "";
    var maskSymbols = splitMask(mask);
    for (var i = 0; i < maskSymbols.length; i++) {
        xRegValue += getXRegSymbolExpression(maskSymbols[i]);
    }
    return xRegValue;
}

/**
 * получение регулярного выражения для отдельно взятого символа
 * @param maskSymbol
 * @returns {*}
 */
function getXRegSymbolExpression(maskSymbol) {
    switch (maskSymbol) {
        case 'A':
            {
                return '[\\p{Lu}]';
            }
        case 'a':
            {
                return '[\\p{Ll}]';
            }
        case '#':
            {
                return '[0-9]';
            }
        case 'L':
            {
                return '[A-Z]';
            }
        case 'l':
            {
                return '[a-z]';
            }
        case '*':
            {
                return '.';
            }
        default:
            {
                var result = getStaticSymbolValue(maskSymbol) + "";
                if ('+-(){}[]-\\/*&^%.,<>~;:\'"'.indexOf(result) > -1) {
                    return "\\" + result;
                } else {
                    return result;
                }
            }
    }
}

/**
 * является ли символ статическим символом
 * @param maskSymbol
 * @returns {boolean}
 */
function isStaticSymbol(maskSymbol) {
    return 'Aa#Ll*'.indexOf(maskSymbol) == -1;
}

/**
 * является ли символ статическим символом
 * @param staticSymbol
 * @returns {boolean}
 */
function getStaticSymbolValue(staticSymbol) {
    if (staticSymbol.length == 1) {
        return staticSymbol;
    } else {
        return staticSymbol.substring(1);
    }
}

/**
 * форматирование введенного значения маски
 * @param value
 * @param splitMask
 * @returns {string}
 */
function getValueForInput(value, splitMask) {

    var inputValue = '';
    if (!value) {
        value = '';
    }
    var inputSymbols = value.split('');
    var j = 0;
    var formattedSymbol = '';

    // коротко о том что здесь происходит
    // метод пытается подстроить пользовательский ввод под маску
    // проходимся по маске ввода
    // если символа с заданной позицией нету в пользовательском вводе (то есть пользовательский ввод короче чем маска),
    // то мы просто вставляем символ по умолчанмию согласно маске - либо статический либо "_"

    // если в пользовательском вводе есть символ то
    //   если это должен быть статический символ согласно маске, то его добавляем к результату
    //   проверяем если текущий символ пользовательского ввода - это "_" - то
    //      если соответствующий символ пользовательского ввода равен символу статики - его и оставляем,
    //      если
    // если да, то проверяем равен ли этот символ символу из пользовательского ввода,
    // если да передвигаем позицию в пользовательском вводе на следующую
    //
    //
    //
    // ыва
    for (var i = 0; i < splitMask.length; i++) {
        formattedSymbol = '';
        var maskSymbol = splitMask[i];
        if (inputSymbols.length > j) {
            var inputSymbol = inputSymbols[j];
            if (!isStaticSymbol(maskSymbol)) {
                if (isValidSymbol(inputSymbol, maskSymbol) || inputSymbol == '_') {
                    formattedSymbol = inputSymbol;
                } else {
                    j++;
                    inputSymbol = inputSymbols[j];
                    if (isValidSymbol(inputSymbol, maskSymbol)) {
                        formattedSymbol = inputSymbol;
                    } else {
                        formattedSymbol = '_';
                    }
                }
            } else {
                formattedSymbol = getStaticSymbolValue(maskSymbol);
                if (inputSymbol === '_') {
                    j++;
                } else if (formattedSymbol !== inputSymbol) {
                    j = j - 1;
                }
            }
            j++;
        } else {
            if (!isStaticSymbol(maskSymbol)) {
                formattedSymbol = '_';
            } else {
                formattedSymbol = getStaticSymbolValue(maskSymbol);
            }
        }
        inputValue += formattedSymbol;
    }

    return inputValue;
}

/**
 * получение чистого значения инпута
 * @param inputValue
 * @param splitMask
 * @returns {string}
 */
function getValueFromInput(inputValue, splitMask) {
    var value = '';

    var inputSymbols = inputValue.split('');
    var inputSymbol = '';
    var symbol = '';

    for (var i = 0; i < inputSymbols.length; i++) {
        symbol = '';
        inputSymbol = inputSymbols[i];
        if (!isStaticSymbol(splitMask[i])) {
            if (isValidSymbol(inputSymbol, splitMask[i])) {
                symbol = inputSymbol;
            } else {
                return "";
            }
        }
        value += symbol;
    }
    return value;
}

/**
 * подходит ли символ под маску
 * @param symbol
 * @param maskSymbol
 */
function isValidSymbol(symbol, maskSymbol) {
    return new _xregexp2.default(getXRegSymbolExpression(maskSymbol)).test(symbol);
}

function escapeRegExp(str) {
    return str.replace(/[\-\[\]\/\{\}\(\)\*\+\?\.\\\^\$\|]/g, "\\$&");
}

/***/ }),

/***/ 1005:
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAAYxJREFUeNqkkzFIW1EUhr9z42BGkYDY1sGKtUETmi4q2ClkUTq7punWZxwEcQiKpbRdXFR0dhJLh5ROyXPQB12FwGuJQ6eWLoFGUImQeG8HE8nzvQjVM917OP93z/3PvVKwDwHM6ts1/idWlhcARN1FDNDUGJmcemm4R3TdTGxvfiAWiwYWu9/LWNkc9Xo9GDAyMkQsFuX9x3VKpR8AGAyhUIiJ8edkrQyDgwMcH/8MBjyLjwJQKB7QaFx6Tj87PSdrZYiPRT0A1V5kvUlT3Hd8YoC/1ROK+w7z2ddUKtVgAIDrlolEen2ASKQX1y1f71sQzxS+OV/QWiMibG3vsLuXxxjDzHSSpUULYwxKKYafvgj2QGuNUldNvUrPkkxOISI86O9DRBARtNadx9gSA4TD3TwZfuy7SnuNz4NPn79Sq110fDS12gW7e3lPzuNBu7uloyK/fv8B4NHDfuKJVCBUeZ3uuV7HEyls28G2nY5iACnYh77P1N7JbbG58Q4FSPNrBnZym9iay8m/AQD/TItATUTiggAAAABJRU5ErkJggg=="

/***/ }),

/***/ 101:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Логгер для сообщений и ошибок
 */



Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.debug = undefined;
exports.log = log;
exports.prettifyFrames = prettifyFrames;
exports.logError = logError;
exports.logServer = logServer;

var _constants = __webpack_require__(2);

var _apiUtils = __webpack_require__(8);

var _stacktraceJs = __webpack_require__(378);

var _stacktraceJs2 = _interopRequireDefault(_stacktraceJs);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var debug = exports.debug = true;

function log(message) {
    console.log(message);
}

function prettifyFrames(frames) {
    return frames.filter(Boolean).map(function (f) {
        return f.toString().replace(':///', ':///./');
    }).join('\n');
}

function prettifyError(error) {
    return _stacktraceJs2.default.fromError(error).then(function (frames) {
        var prettyFrames = prettifyFrames(frames);
        return error.message + "\n" + prettyFrames;
    }).catch(function () {
        return JSON.stringify(error);
    });
}

function logError(error) {
    return prettifyError(error).then(console.error).catch(function (stackError) {
        console.error(stackError);
        return stackError;
    });
}

function logServer(error, formId, asfDataId) {
    logError(error).then(function (prettyError) {
        (0, _apiUtils.logError)(prettyError, _constants.OPTIONS.currentUser.userId, formId, asfDataId);
    });
}

window.onerror = function (msg, file, line, col, error) {
    if (!error) return;
    logError(error);
};

/***/ }),

/***/ 11:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = View;

var _componentUtils = __webpack_require__(15);

var componentUtils = _interopRequireWildcard(_componentUtils);

var _styleUtils = __webpack_require__(21);

var _dataUtils = __webpack_require__(5);

var _constants = __webpack_require__(2);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function View(view, model, playerView, input) {
    this.view = view;
    this.model = model;
    this.input = input;
    this.playerView = playerView;

    view.model = model;
    view.container = componentUtils.createContainer();
    (0, _dataUtils.addAsFormId)(view.container, "container", model.asfProperty);

    this.label = componentUtils.createLabel("");
    (0, _dataUtils.addAsFormId)(this.label, "label", model.asfProperty);
    this.label.addClass('asf-label');

    view.container.append(this.label);

    if (model.asfProperty && model.asfProperty.label) {
        this.updateLabel();
    }

    this.applyStyle();

    model.on(_constants.EVENT_TYPE.valueChange, function () {
        if (view.updateValueFromModel) {
            view.updateValueFromModel.apply(view, arguments);
        }
    });

    model.on(_constants.EVENT_TYPE.markInvalid, function () {
        if (view.markInvalid) {
            view.markInvalid();
        }
    });

    if (!this.model.asfProperty.config || !this.model.asfProperty.config.script) {
        this.executeScript = function () {};
    } else {
        this.executeScript = componentUtils.createFunction(this.model.playerModel, this.model.asfProperty.config.script, "var model = arguments[0], view = arguments[1], editable = arguments[2];");
    }

    if (model.playerModel.building) {
        this.container.click(function () {
            if (view.unmarkInvalid) {
                view.unmarkInvalid();
            }
        });
    }

    model.on(_constants.EVENT_TYPE.modelDestroyed, function () {
        if (view.destroy) {
            view.destroy();
        }
    });
};

View.prototype.updateLabel = function () {
    var text = componentUtils.getCurrentTranslation(this.model.asfProperty);
    this.label.text(text);
};

View.prototype.unmarkInvalid = function () {
    if (!this.input) {
        this.view.container.removeClass('asf-invalidInput');
    } else {
        this.input.removeClass('asf-invalidInput');
    }
};

View.prototype.markInvalid = function () {
    if (!this.input) {
        this.view.container.addClass('asf-invalidInput');
    } else {
        this.input.addClass('asf-invalidInput');
    }
};

View.prototype.checkValid = function () {
    if (this.model.isValid()) {
        this.view.unmarkInvalid();
    } else {
        this.view.markInvalid();
    }
};

View.prototype.setEnabled = function (enabled) {
    if (this.input) {
        if (enabled) {
            this.input.prop("readonly", true);
        } else {
            this.input.removeProp("readonly");
        }
    }
};

View.prototype.setVisible = function (visible) {
    if (visible) {
        this.container.show();
    } else {
        this.container.hide();
    }
};

View.prototype.applyStyle = function () {
    (0, _styleUtils.applyPositionStyle)(this.view.container, this.model.asfProperty.style, this.view.initialWidth);
    if (this.view.applySpecialStyle) {
        this.view.applySpecialStyle();
    }
};

/***/ }),

/***/ 112:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.bigMath = undefined;

var _mathjs = __webpack_require__(212);

var _mathjs2 = _interopRequireDefault(_mathjs);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var bigMath = exports.bigMath = _mathjs2.default.create({
    number: 'BigNumber'
});

exports.default = bigMath;

/***/ }),

/***/ 113:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(jQuery) {

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.ExtendedTableEvent = undefined;
exports.default = ExtendedTable;

var _constants = __webpack_require__(2);

var _componentUtils = __webpack_require__(15);

var compUtils = _interopRequireWildcard(_componentUtils);

var _domUtils = __webpack_require__(44);

var domUtils = _interopRequireWildcard(_domUtils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

var ExtendedTableEvent = exports.ExtendedTableEvent = {
    /**
     * элементе выделен
     */
    rowSelected: "stackSelected",

    /**
     * колонка выделена
     */
    columnSortClicked: "columnSortClicked",

    /**
     * таблица реинициализирована
     */
    inited: "inited",

    /**
     * ширина колонки изменилась
     */
    colResized: "colResized",

    /**
     * Начало изменения ширины колонки
     */
    colResizeStarted: "colResizeStarted",

    /**
     * данные загружены
     */
    dataLoaded: "dataLoaded"
};

function ExtendedTable() {

    var instance = this;
    compUtils.makeBus(this);

    var headerRowHeight = 32;
    var rowHeight = 26;
    var width = 500;

    var headerRow = null;
    var rows = [];

    var container = jQuery("<div>", { class: "ns-extendedTableContainer", tabIndex: "0" });

    var selectedData = null;
    var selectedIndex = -1;

    var columns = [];

    this.init = function (options, newColumns) {
        instance.clear();
        headerRow = new ExtendedTableHeaderRow(instance, -1);
        container.append(headerRow.getWidget());

        columns = newColumns;

        headerRowHeight = options["headerRowHeight"] || headerRowHeight;
        rowHeight = options["rowHeight"] || rowHeight;
        width = options["width"] || width;

        instance.trigger(ExtendedTableEvent.inited, []);

        instance.on(ExtendedTableEvent.rowSelected, function (evt, rowIndex, data) {
            if (data) {
                selectedData = data;
                selectedIndex = rowIndex;
            } else {
                selectedData = null;
                selectedIndex = -1;
            }
            container.focus();
        });

        container.keydown(function (evt) {
            if (evt.keyCode !== _constants.KEY_CODES.up && evt.keyCode !== _constants.KEY_CODES.down) {
                return;
            }

            domUtils.cancelEvent(evt);

            if (selectedIndex === -1) {
                return;
            }

            var newSelectedIndex = selectedIndex;
            if (evt.keyCode === _constants.KEY_CODES.down) {

                if (rows.length === selectedIndex + 1) {

                    return;
                }
                newSelectedIndex++;
            } else {
                if (selectedIndex === 0) {
                    return;
                }
                newSelectedIndex--;
            }

            rows[newSelectedIndex].selectRow();
        });
    };

    this.getSelectedData = function () {
        return selectedData;
    };

    this.getColumns = function () {
        return columns;
    };

    this.getHeaderRowHeight = function () {
        return headerRowHeight;
    };

    this.getRowHeight = function () {
        return rowHeight;
    };

    this.clear = function () {
        container.empty();
    };

    this.setData = function (data, append) {
        if (!append) {
            rows.forEach(function (row) {
                row.getWidget().detach();
                row.getWidget().empty();
            });
            rows = [];
        }

        var rowsCount = rows.length;

        data.forEach(function (data, dataIndex) {
            var row = new ExtendedTableRow(instance, data, dataIndex + rowsCount);
            rows.push(row);
            container.append(row.getWidget());
        });

        instance.trigger(ExtendedTableEvent.dataLoaded, []);
    };

    this.getWidget = function () {
        return container;
    };
};

function ExtendedTableHeaderRow(table, rowIndex) {
    var headerRow = jQuery("<div>", { class: "ns-extendedTableRowHeader" });
    headerRow.css("height", table.getHeaderRowHeight() + "px");
    headerRow.css("line-height", table.getHeaderRowHeight() + "px");
    var cells = [];

    table.on(ExtendedTableEvent.inited, function () {
        cells = [];
        headerRow.empty();
        var columns = table.getColumns();
        columns.forEach(function (column, columnIndex) {
            var headerCell = new ExtendedTableHeaderCell(table, column, columnIndex);
            cells.push(headerCell);
            headerRow.append(headerCell.getWidget());
        });
    });

    this.getWidget = function () {
        return headerRow;
    };

    var startX = -1;
    var movingColumn = null;
    var movingColumnIndex = -1;
    table.on(ExtendedTableEvent.colResizeStarted, function (evt, screenX, columnIndex) {
        startX = screenX;
        movingColumn = table.getColumns()[columnIndex];
        movingColumnIndex = columnIndex;
    });

    headerRow.mousemove(function (evt) {
        if (!movingColumn) {
            return;
        }

        var targetWidth = movingColumn.width - (startX - evt.screenX);
        if (movingColumn.minWidth) {
            targetWidth = Math.max(movingColumn.minWidth, targetWidth);
        }

        if (startX === -1) {
            return;
        }

        movingColumn.width = targetWidth;
        table.trigger(ExtendedTableEvent.colResized, [movingColumn, movingColumnIndex]);

        startX = evt.screenX;
    });

    headerRow.mouseup(function () {
        startX = -1;
        movingColumn = null;
        movingColumnIndex = -1;
    });

    headerRow.mouseout(function () {
        startX = -1;
        movingColumn = null;
        movingColumnIndex = -1;
    });
};

function ExtendedTableHeaderCell(table, columnOption, columnIndex) {
    var header = jQuery("<div>", { class: "ns-extendedTableHeader", title: columnOption.name });
    header.css("width", columnOption.width + "px");

    var text = jQuery("<div>", { class: "ns-extendedTableHeaderText" });

    text.html(columnOption.name);

    var movingAnchor = jQuery("<div>", { class: "ns-extendedTableMovingAnchor" });
    var sortImage = jQuery("<div>", { class: "ns-extendedTableSortImage" });
    sortImage.hide();
    var sortAsc = null;

    header.append(text);
    header.append(sortImage);
    header.append(movingAnchor);

    this.getWidget = function () {
        return header;
    };

    domUtils.cancelEventDispatch(movingAnchor, 'mouseout');
    domUtils.cancelEventDispatch(header, 'mouseout');
    domUtils.cancelEventDispatch(text, 'mouseout');

    /**  resizing header  **/
    movingAnchor.mousedown(function (evt) {
        table.trigger(ExtendedTableEvent.colResizeStarted, [evt.screenX, columnIndex]);
        evt.cancelBubble = true;
        evt.stopped = true;
        evt.stopPropagation();
    });

    table.on(ExtendedTableEvent.colResized, function (evt, column) {
        if (columnOption === column) {
            header.css("width", columnOption.width + "px");
        }
    });

    /**  sorting header  **/
    if (columnOption.sortable) {
        domUtils.notMovingClick(header, function () {
            sortAsc = !sortAsc;
            table.trigger(ExtendedTableEvent.columnSortClicked, [columnIndex, sortAsc, columnOption]);
        });

        table.on(ExtendedTableEvent.columnSortClicked, function (event, columnIndex, sortAsc, column) {
            if (columnOption === column) {
                text.css("width", "calc(100% - 31px)");
                sortImage.css("display", "inline-block");
                if (sortAsc) {
                    sortImage.addClass("ns-tableSortDescIcon");
                    sortImage.removeClass("ns-tableSortAscIcon");
                } else {
                    sortImage.removeClass("ns-tableSortDescIcon");
                    sortImage.addClass("ns-tableSortAscIcon");
                }
            } else {
                text.css("width", "");
                sortAsc = null;
                sortImage.hide();
            }
        });
    }
};

function ExtendedTableRow(table, data, rowIndex) {
    var row = jQuery("<div>", { class: "ns-extendedTableRow" });
    if (rowIndex % 2 > 0) {
        row.addClass("ns-extendedTableRowOdd");
    }
    row.css("height", table.getRowHeight() + "px");
    row.css("line-height", table.getRowHeight() + "px");
    var cells = [];

    var columns = table.getColumns();
    columns.forEach(function (column) {
        var cell = new ExtendedTableCell(column.width, data[column.id]);
        cells.push(cell);
        row.append(cell.getWidget());
    });

    var instance = this;

    table.on(ExtendedTableEvent.colResized, function (evt, column, columnIndex) {
        cells[columnIndex].setWidth(column.width);
    });

    row.click(function () {
        instance.selectRow();
    });

    this.selectRow = function () {
        table.trigger(ExtendedTableEvent.rowSelected, [rowIndex, data]);
    };

    table.on(ExtendedTableEvent.rowSelected, function (evt, selectedRowIndex, selectedData) {
        if (data === selectedData) {
            row.addClass("ns-extendedTableRowSelected");
        } else {
            row.removeClass("ns-extendedTableRowSelected");
        }
    });

    this.getWidget = function () {
        return row;
    };
};

function ExtendedTableCell(width, value) {
    var cell = jQuery("<div>", { class: "ns-extendedTableCell", title: value });

    cell.text(value);

    this.getWidget = function () {
        return cell;
    };

    this.setWidth = function (width) {
        cell.css("width", width + "px");
    };

    this.setWidth(width);
};
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ }),

/***/ 127:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.setCaretPosition = setCaretPosition;
exports.mobileSetCaretPosition = mobileSetCaretPosition;
exports.selectRegion = selectRegion;
exports.getCaretPosition = getCaretPosition;
exports.getSelectionLength = getSelectionLength;

var _constants = __webpack_require__(2);

function setCaretPosition(field, cursorPosition) {
    if (_constants.OPTIONS.mobilePlayer) mobileSetCaretPosition.call(this, field, cursorPosition);

    if (field.createTextRange) {
        var range = field.createTextRange();
        range.collapse(true);
        range.moveEnd('character', cursorPosition);
        range.moveStart('character', cursorPosition);
        range.select();
    } else if (field.setSelectionRange) {
        field.focus();
        field.setSelectionRange(cursorPosition, cursorPosition);
    }
}

function mobileSetCaretPosition(field, cursorPosition) {
    setTimeout(function () {
        if (field.createTextRange) {
            var range = field.createTextRange();
            range.collapse(true);
            range.moveEnd('character', cursorPosition);
            range.moveStart('character', cursorPosition);
            range.select();
        } else if (field.setSelectionRange) {
            field.focus();
            field.setSelectionRange(cursorPosition, cursorPosition);
        }
    }, 0);
};

function selectRegion(field, cursorPosition, length) {
    if (field.createTextRange) {
        var range = field.createTextRange();
        range.collapse(true);
        range.moveStart('character', cursorPosition);
        range.moveEnd('character', cursorPosition + length);
        range.select();
    } else if (field.setSelectionRange) {
        field.blur();
        field.setSelectionRange(0, cursorPosition + length);
    }
}

function getCaretPosition(field) {
    var iCaretPos = 0;
    if (document.selection) {
        field.focus();
        var oSel = document.selection.createRange();
        oSel.moveStart('character', -field.value.length);
        iCaretPos = oSel.text.length;
    } else if (field.selectionStart || field.selectionStart == '0') {
        iCaretPos = field.selectionStart;
    }
    return iCaretPos;
}

function getSelectionLength(field) {
    var iCaretPos = 0;
    if (document.selection) {
        field.focus();
        var oSel = document.selection.createRange();
        oSel.moveStart('character', -field.value.length);
        iCaretPos = oSel.text.length;
        console.log(oSel.text);
    } else if (field.selectionStart || field.selectionStart == '0') {
        iCaretPos = field.selectionStart;
        console.log(field.selectionEnd);
    }
    return iCaretPos;
}

/***/ }),

/***/ 128:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.popupPanel = undefined;
exports.default = PopupPanel;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Created by user on 05.05.16.
 * попап панель (автоскрываемая)
 */
function PopupPanel() {

    if (PopupPanel.prototype._singletonInstance) {
        return PopupPanel.prototype._singletonInstance;
    }

    PopupPanel.prototype._singletonInstance = this;

    var openedPopups = [];

    var instance = this;

    /**
     * скрываем компонент с попап панели, если это последний компонент на панели или компонент не передан скрываем панель полностью и уничтожаем компоненты
     * @param component
     */
    this.hide = function (component) {
        if (!component) {
            openedPopups.forEach(function (openedPopup) {
                instance.hide(openedPopup);
            });
        }
        if (openedPopups.indexOf(component) !== -1) {
            component.dialog("destroy");
            component.detach();
            openedPopups.remove(component);
        }
    };

    // здесь нужно добавить событие в конец ивент лупа, потому что компоненты должны получить событие клика
    (0, _jquery2.default)(document).mouseup(function (event) {
        setTimeout(function () {
            instance.hide();
        }, 0);
    });

    (0, _jquery2.default)(document).click(function (event) {
        instance.hide();
    });

    /**
     * показываем элемент в попап панели поверх всего
     * @param component компонент
     * @param anchor относительно чего показывать элемент
     * @param width ширина
     * @param height высота
     * @param nonExclusive true - значит элементы которые есть сейчас в попап панели остаются открытыми, false - удаляем все элементы с попап панели
     */
    this.showComponent = function (component, anchor, width, height, nonExclusive) {
        component.addClass("asf-popupInnerComponent");

        var at = "left+3 bottom";
        if (anchor.is("button")) {
            at = "left bottom";
        }

        component.dialog({ dialogClass: 'noTitleStuff',
            open: function open() {
                setTimeout(function () {
                    openedPopups.push(component);
                }, 0);
            },
            close: function close() {},
            position: { my: "left top", at: at, of: anchor, collision: "flipfit" },
            width: width,
            height: height,
            resizable: false
        });
    };
};
var popupPanel = exports.popupPanel = new PopupPanel();

/***/ }),

/***/ 129:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = TableStaticView;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _constants = __webpack_require__(2);

var _view = __webpack_require__(11);

var _view2 = _interopRequireDefault(_view);

var _tableUtils = __webpack_require__(52);

var tableUtils = _interopRequireWildcard(_tableUtils);

var _tableBlock = __webpack_require__(159);

var _tableBlock2 = _interopRequireDefault(_tableBlock);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * отображения для страниц, статической таблицы или страницы
 * @param tableModel
 * @param playerView
 * @param editable
 * @constructor
 */
function TableStaticView(tableModel, playerView, editable) {
    _view2.default.call(this, this, tableModel, playerView);

    this.viewBlock = {};

    /**
     * невидимые колонки
     * @type {Array}
     */
    var invisibleColumns = [];

    var instance = this;

    var $table = tableUtils.createASFTable(tableModel, playerView);

    this.getErrors = function () {};

    this.getAllViews = function (tableId, tableBlockIndex) {
        var allViews = [];
        instance.viewBlock.views.forEach(function (view) {
            if (tableBlockIndex) {
                // если запрашивают компонент из блока динамической таблицы, то мы добавляем все таблицы и
                // все компоненты с этим номером блока , если идентификатор текущей таблицы равен tableId
                if (view.model.asfProperty.type === _constants.WIDGET_TYPE.table || instance.model.asfProperty.id === tableId && view.model.asfProperty.tableBlockIndex == tableBlockIndex) {
                    allViews.push(view);
                }
            } else {
                allViews.push(view);
            }
        });
        return allViews;
    };

    this.getTable = function () {
        return $table;
    };

    this.getViewWithId = function (cmpId, tableId, tableBlockIndex) {
        var allViews = this.getAllViews(tableId, tableBlockIndex);
        return tableUtils.getViewWithId(cmpId, allViews, tableId, tableBlockIndex);
    };

    this.getViewsWithType = function (type, tableId, blockIndex) {
        var allViews = this.getAllViews(tableId, blockIndex);
        return tableUtils.getViewsWithType(type, allViews, tableId, blockIndex);
    };

    this.getInvisibleColumns = function () {
        return invisibleColumns;
    };

    this.setColumnVisible = function (column, visible) {
        if (visible) {
            invisibleColumns.remove(column);
            tableModel.trigger(_constants.TABLE_EVENT_TYPE.COLUMN_SHOW, [column]);
        } else {
            invisibleColumns.pushUnique(column);
            tableModel.trigger(_constants.TABLE_EVENT_TYPE.COLUMN_HIDE, [column]);
        }
    };

    this.getRowsCount = function () {
        return $table.children("tbody").first().children("tr").length;
    };

    this.executeSelfScript = this.executeScript;

    this.executeScript = function (view, model, editable) {
        instance.executeSelfScript(view, model, editable);

        instance.viewBlock.executeScript();
    };

    this.drawTable = function () {
        $table.children('tbody').first().children().detach();
        $table.children('tbody').first().append(instance.viewBlock.rows);

        if ($table.parent().length > 0) tableModel.playerModel.trigger(_constants.EVENT_TYPE.layoutChanged, tableModel);
    };

    if (tableModel.isPage()) {
        this.container.addClass("asf-page");
    }

    this.viewBlock = new _tableBlock2.default(tableModel, tableModel.modelBlocks[0], playerView, editable, false);

    this.drawTable();

    if (tableModel.asfProperty.config.fixedLayout && !tableModel.playerModel.building) {
        var playerConfig = playerView.model.definition.config;
        var background = "#ffffff";
        var cssClass = "padding";
        if (playerConfig && playerConfig.background) {
            background = playerConfig.background;
        }
        if (tableModel.asfProperty.style && tableModel.asfProperty.style.border) {
            cssClass = "paddingBordered";
        }

        var makePadding = function makePadding() {
            return (0, _jquery2.default)('<div/>', { class: cssClass, style: "backgroundColor :" + background });
        };

        $table.find('tbody > tr > td.asf-cell').append(makePadding);
    }

    this.container.append($table);
};

TableStaticView.prototype = Object.create(_view2.default.prototype);
TableStaticView.prototype.constructor = TableStaticView;

/***/ }),

/***/ 14:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

__webpack_require__(406);

var i18n = {
    tr: function tr() {
        for (var _len = arguments.length, args = Array(_len), _key = 0; _key < _len; _key++) {
            args[_key] = arguments[_key];
        }

        return window.i18n.tr.call(this, args);
    },
    getCurrentLocale: function getCurrentLocale() {
        for (var _len2 = arguments.length, args = Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
            args[_key2] = arguments[_key2];
        }

        return window.i18n.getCurrentLocale.tr.call(this, args);
    },
    getString: function getString() {
        for (var _len3 = arguments.length, args = Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
            args[_key3] = arguments[_key3];
        }

        return window.i18n.getString.call(this, args);
    }
};

exports.default = i18n;

/***/ }),

/***/ 15:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.createModel = createModel;
exports.createView = createView;
exports.initEditingLabel = initEditingLabel;
exports.getPropertyWithId = getPropertyWithId;
exports.createContainer = createContainer;
exports.getCurrentTranslation = getCurrentTranslation;
exports.updateCurrentTranslationWidget = updateCurrentTranslationWidget;
exports.makeBus = makeBus;
exports.createLabel = createLabel;
exports.createInlineStyledLabel = createInlineStyledLabel;
exports.fillView = fillView;
exports.createPageBreak = createPageBreak;
exports.initInput = initInput;
exports.findMaster = findMaster;
exports.setTranslatedValue = setTranslatedValue;
exports.addSimpleHtml = addSimpleHtml;
exports.createFunction = createFunction;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _underscore = __webpack_require__(7);

var _underscore2 = _interopRequireDefault(_underscore);

var _i18n = __webpack_require__(14);

var _i18n2 = _interopRequireDefault(_i18n);

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

var _logger = __webpack_require__(101);

var LOGGER = _interopRequireWildcard(_logger);

var _constants = __webpack_require__(2);

var _styleUtils = __webpack_require__(21);

var styleUtils = _interopRequireWildcard(_styleUtils);

var _domUtils = __webpack_require__(44);

var domUtils = _interopRequireWildcard(_domUtils);

var _stacktraceJs = __webpack_require__(378);

var _stacktraceJs2 = _interopRequireDefault(_stacktraceJs);

var _AddressLinkModel = __webpack_require__(220);

var _AddressLinkModel2 = _interopRequireDefault(_AddressLinkModel);

var _ComboModel = __webpack_require__(221);

var _ComboModel2 = _interopRequireDefault(_ComboModel);

var _CustomComponentModel = __webpack_require__(222);

var _CustomComponentModel2 = _interopRequireDefault(_CustomComponentModel);

var _DateModel = __webpack_require__(223);

var _DateModel2 = _interopRequireDefault(_DateModel);

var _DepartmentLinkModel = __webpack_require__(224);

var _DepartmentLinkModel2 = _interopRequireDefault(_DepartmentLinkModel);

var _DocAttributeModel = __webpack_require__(225);

var _DocAttributeModel2 = _interopRequireDefault(_DocAttributeModel);

var _DocLinkModel = __webpack_require__(226);

var _DocLinkModel2 = _interopRequireDefault(_DocLinkModel);

var _FileLinkModel = __webpack_require__(227);

var _FileLinkModel2 = _interopRequireDefault(_FileLinkModel);

var _FileModel = __webpack_require__(228);

var _FileModel2 = _interopRequireDefault(_FileModel);

var _ImageModel = __webpack_require__(230);

var _ImageModel2 = _interopRequireDefault(_ImageModel);

var _LabelModel = __webpack_require__(231);

var _LabelModel2 = _interopRequireDefault(_LabelModel);

var _LinkModel = __webpack_require__(232);

var _LinkModel2 = _interopRequireDefault(_LinkModel);

var _NumericInputModel = __webpack_require__(157);

var _NumericInputModel2 = _interopRequireDefault(_NumericInputModel);

var _PositionLinkModel = __webpack_require__(234);

var _PositionLinkModel2 = _interopRequireDefault(_PositionLinkModel);

var _ProjectLinkModel = __webpack_require__(235);

var _ProjectLinkModel2 = _interopRequireDefault(_ProjectLinkModel);

var _RegistryLinkModel = __webpack_require__(236);

var _RegistryLinkModel2 = _interopRequireDefault(_RegistryLinkModel);

var _RepeatPeriodModel = __webpack_require__(237);

var _RepeatPeriodModel2 = _interopRequireDefault(_RepeatPeriodModel);

var _SimpleModel = __webpack_require__(238);

var _SimpleModel2 = _interopRequireDefault(_SimpleModel);

var _TextBoxModel = __webpack_require__(240);

var _TextBoxModel2 = _interopRequireDefault(_TextBoxModel);

var _TableModel = __webpack_require__(239);

var _TableModel2 = _interopRequireDefault(_TableModel);

var _UserLinkModel = __webpack_require__(241);

var _UserLinkModel2 = _interopRequireDefault(_UserLinkModel);

var _TextView = __webpack_require__(273);

var _TextView2 = _interopRequireDefault(_TextView);

var _tableBuilderView = __webpack_require__(160);

var _tableBuilderView2 = _interopRequireDefault(_tableBuilderView);

var _tableDynView = __webpack_require__(277);

var _tableDynView2 = _interopRequireDefault(_tableDynView);

var _tableStaticView = __webpack_require__(129);

var _tableStaticView2 = _interopRequireDefault(_tableStaticView);

var _paragraphView = __webpack_require__(276);

var _paragraphView2 = _interopRequireDefault(_paragraphView);

var _AddressLinkView = __webpack_require__(244);

var _AddressLinkView2 = _interopRequireDefault(_AddressLinkView);

var _BuilderComponentView = __webpack_require__(245);

var _BuilderComponentView2 = _interopRequireDefault(_BuilderComponentView);

var _CheckBoxView = __webpack_require__(246);

var _CheckBoxView2 = _interopRequireDefault(_CheckBoxView);

var _ComboBoxView = __webpack_require__(247);

var _ComboBoxView2 = _interopRequireDefault(_ComboBoxView);

var _CustomComponentView = __webpack_require__(248);

var _CustomComponentView2 = _interopRequireDefault(_CustomComponentView);

var _DateView = __webpack_require__(249);

var _DateView2 = _interopRequireDefault(_DateView);

var _DepartmentLinkView = __webpack_require__(250);

var _DepartmentLinkView2 = _interopRequireDefault(_DepartmentLinkView);

var _DocAttributeView = __webpack_require__(251);

var _DocAttributeView2 = _interopRequireDefault(_DocAttributeView);

var _DocLinkTextView = __webpack_require__(252);

var _DocLinkTextView2 = _interopRequireDefault(_DocLinkTextView);

var _DocLinkView = __webpack_require__(253);

var _DocLinkView2 = _interopRequireDefault(_DocLinkView);

var _FileLinkView = __webpack_require__(254);

var _FileLinkView2 = _interopRequireDefault(_FileLinkView);

var _FileView = __webpack_require__(255);

var _FileView2 = _interopRequireDefault(_FileView);

var _ImageView = __webpack_require__(256);

var _ImageView2 = _interopRequireDefault(_ImageView);

var _LabelView = __webpack_require__(257);

var _LabelView2 = _interopRequireDefault(_LabelView);

var _LinkView = __webpack_require__(258);

var _LinkView2 = _interopRequireDefault(_LinkView);

var _NumericInputView = __webpack_require__(259);

var _NumericInputView2 = _interopRequireDefault(_NumericInputView);

var _PositionLinkView = __webpack_require__(261);

var _PositionLinkView2 = _interopRequireDefault(_PositionLinkView);

var _ProcessExecutionView = __webpack_require__(262);

var _ProcessExecutionView2 = _interopRequireDefault(_ProcessExecutionView);

var _ProjectLinkTextView = __webpack_require__(263);

var _ProjectLinkTextView2 = _interopRequireDefault(_ProjectLinkTextView);

var _ProjectLinkView = __webpack_require__(264);

var _ProjectLinkView2 = _interopRequireDefault(_ProjectLinkView);

var _RadioView = __webpack_require__(265);

var _RadioView2 = _interopRequireDefault(_RadioView);

var _RegistryLinkView = __webpack_require__(266);

var _RegistryLinkView2 = _interopRequireDefault(_RegistryLinkView);

var _RepeatPeriodView = __webpack_require__(267);

var _RepeatPeriodView2 = _interopRequireDefault(_RepeatPeriodView);

var _ResolutionListView = __webpack_require__(268);

var _ResolutionListView2 = _interopRequireDefault(_ResolutionListView);

var _RichTextView = __webpack_require__(269);

var _RichTextView2 = _interopRequireDefault(_RichTextView);

var _SignListView = __webpack_require__(270);

var _SignListView2 = _interopRequireDefault(_SignListView);

var _TextAreaView = __webpack_require__(271);

var _TextAreaView2 = _interopRequireDefault(_TextAreaView);

var _TextBoxView = __webpack_require__(272);

var _TextBoxView2 = _interopRequireDefault(_TextBoxView);

var _UserLinkView = __webpack_require__(274);

var _UserLinkView2 = _interopRequireDefault(_UserLinkView);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function createModel(asfProperty, playerModel) {
    var model = null;
    var widgetType = asfProperty.type;
    try {
        switch (widgetType) {
            case _constants.WIDGET_TYPE.label:
                {
                    model = new _LabelModel2.default(asfProperty, playerModel);
                    break;
                }
            case _constants.WIDGET_TYPE.textbox:
                {
                    model = new _TextBoxModel2.default(asfProperty, playerModel);
                    break;
                }
            case _constants.WIDGET_TYPE.table:
                {
                    model = new _TableModel2.default(asfProperty, playerModel);
                    break;
                }
            case _constants.WIDGET_TYPE.page:
                {
                    model = new _TableModel2.default(asfProperty, playerModel);
                    break;
                }
            case _constants.WIDGET_TYPE.textarea:
                {
                    model = new _SimpleModel2.default(asfProperty, playerModel);
                    break;
                }
            case _constants.WIDGET_TYPE.numericinput:
                {
                    model = new _NumericInputModel2.default(asfProperty, playerModel);
                    break;
                }

            case _constants.WIDGET_TYPE.date:
                {
                    model = new _DateModel2.default(asfProperty, playerModel);
                    break;
                }
            case _constants.WIDGET_TYPE.listbox:
                {
                    model = new _ComboModel2.default(asfProperty, playerModel);
                    break;
                }
            case _constants.WIDGET_TYPE.radio:
                {
                    model = new _ComboModel2.default(asfProperty, playerModel);
                    break;
                }
            case _constants.WIDGET_TYPE.check:
                {
                    model = new _ComboModel2.default(asfProperty, playerModel);
                    break;
                }
            case _constants.WIDGET_TYPE.docnumber:
                {
                    model = new _DocAttributeModel2.default(asfProperty, playerModel);
                    break;
                }
            case _constants.WIDGET_TYPE.doclink:
                {
                    model = new _DocLinkModel2.default(asfProperty, playerModel);
                    break;
                }
            case _constants.WIDGET_TYPE.counter:
                {
                    model = new _SimpleModel2.default(asfProperty, playerModel);
                    break;
                }

            case _constants.WIDGET_TYPE.image:
                {
                    model = new _ImageModel2.default(asfProperty, playerModel);
                    break;
                }

            case _constants.WIDGET_TYPE.htd:
                {
                    model = new _SimpleModel2.default(asfProperty, playerModel);
                    break;
                }

            case _constants.WIDGET_TYPE.processlist:
                {
                    model = new _SimpleModel2.default(asfProperty, playerModel);
                    break;
                }

            case _constants.WIDGET_TYPE.resolutionlist:
                {
                    model = new _SimpleModel2.default(asfProperty, playerModel);
                    break;
                }

            case _constants.WIDGET_TYPE.signlist:
                {
                    model = new _SimpleModel2.default(asfProperty, playerModel);
                    break;
                }

            case _constants.WIDGET_TYPE.personlink:
                {
                    model = new _AddressLinkModel2.default(asfProperty, playerModel);
                    break;
                }

            case _constants.WIDGET_TYPE.link:
                {
                    model = new _LinkModel2.default(asfProperty, playerModel);
                    break;
                }

            case _constants.WIDGET_TYPE.reglink:
                {
                    model = new _RegistryLinkModel2.default(asfProperty, playerModel);
                    break;
                }

            case _constants.WIDGET_TYPE.projectlink:
                {
                    model = new _ProjectLinkModel2.default(asfProperty, playerModel);
                    break;
                }

            case _constants.WIDGET_TYPE.filelink:
                {
                    model = new _FileLinkModel2.default(asfProperty, playerModel);
                    break;
                }

            case _constants.WIDGET_TYPE.file:
                {
                    model = new _FileModel2.default(asfProperty, playerModel);
                    break;
                }

            case _constants.WIDGET_TYPE.entity:
                {
                    var entityType = asfProperty.config.entity;
                    if (entityType == _constants.ENTITY_TYPE.departments) {
                        model = new _DepartmentLinkModel2.default(asfProperty, playerModel);
                    } else if (entityType == _constants.ENTITY_TYPE.positions) {
                        model = new _PositionLinkModel2.default(asfProperty, playerModel);
                    } else {
                        model = new _UserLinkModel2.default(asfProperty, playerModel);
                    }
                    break;
                }

            case _constants.WIDGET_TYPE.repeater:
                {
                    model = new _RepeatPeriodModel2.default(asfProperty, playerModel);
                    break;
                }

            case _constants.WIDGET_TYPE.custom:
                {
                    model = new _CustomComponentModel2.default(asfProperty, playerModel);
                    break;
                }

            default:
                {
                    model = new _SimpleModel2.default(asfProperty, playerModel);
                    LOGGER.logServer("no model for " + JSON.stringify(asfProperty), playerModel.formId, playerModel.asfDataId);
                    break;
                }
        }
    } catch (e) {
        LOGGER.logServer(e, playerModel.formId, playerModel.asfDataId);
    }

    if (asfProperty.data) {
        model.setAsfData(asfProperty.data);
    }

    return model;
}

function createView(model, playerView, editable, building) {
    var view = null;
    var widgetType = model.asfProperty.type;
    try {
        switch (widgetType) {
            case _constants.WIDGET_TYPE.label:
                {
                    view = new _LabelView2.default(model, playerView);
                    break;
                }
            case _constants.WIDGET_TYPE.textbox:
                {
                    if (editable) {
                        view = new _TextBoxView2.default(model, playerView);
                    } else {
                        view = new _TextView2.default(model, playerView);
                    }
                    break;
                }
            case _constants.WIDGET_TYPE.table:
                {
                    if (building) {
                        view = new _tableBuilderView2.default(model, playerView, editable);
                    } else {
                        if (editable) {
                            if (model.isStatic()) {
                                view = new _tableStaticView2.default(model, playerView, editable);
                            } else {
                                view = new _tableDynView2.default(model, playerView, editable);
                            }
                        } else {
                            // отображаем свертку если таблица статическая и включена свертка или если таблица динамическая, сверкта включена и заполнена форматная строка
                            if (model.isParagraph() && (model.isStatic() || model.asfProperty.config && model.asfProperty.config.format && model.asfProperty.config.format != '')) {
                                view = new _paragraphView2.default(model, playerView);
                            } else if (model.isStatic()) {
                                view = new _tableStaticView2.default(model, playerView, editable);
                            } else {
                                view = new _tableDynView2.default(model, playerView, editable);
                            }
                        }
                    }
                    break;
                }
            case _constants.WIDGET_TYPE.textarea:
                {
                    if (editable) {
                        view = new _TextAreaView2.default(model, playerView);
                    } else {
                        view = new _TextView2.default(model, playerView);
                    }
                    break;
                }
            case _constants.WIDGET_TYPE.numericinput:
                {
                    if (editable) {
                        view = new _NumericInputView2.default(model, playerView);
                    } else {
                        view = new _TextView2.default(model, playerView);
                    }
                    break;
                }
            case _constants.WIDGET_TYPE.date:
                {
                    if (editable) {
                        view = new _DateView2.default(model, playerView);
                    } else {
                        view = new _TextView2.default(model, playerView);
                    }
                    break;
                }
            case _constants.WIDGET_TYPE.listbox:
                {
                    if (editable) {
                        view = new _ComboBoxView2.default(model, playerView);
                    } else {
                        view = new _TextView2.default(model, playerView);
                    }
                    break;
                }
            case _constants.WIDGET_TYPE.check:
                {
                    if (editable) {
                        if (building) {
                            view = new _CheckBoxView.CheckBoxBuilderView(model, playerView);
                        } else {
                            view = new _CheckBoxView2.default(model, playerView);
                        }
                    } else {
                        view = new _TextView2.default(model, playerView);
                    }
                    break;
                }
            case _constants.WIDGET_TYPE.radio:
                {
                    if (editable) {
                        if (building) {
                            view = new _RadioView.RadioButtonBuilderView(model, playerView);
                        } else {
                            view = new _RadioView2.default(model, playerView);
                        }
                    } else {
                        view = new _TextView2.default(model, playerView);
                    }
                    break;
                }
            case _constants.WIDGET_TYPE.docnumber:
                {
                    if (building) {
                        view = new _BuilderComponentView2.default(model, playerView);
                    } else {
                        if (editable) {
                            view = new _DocAttributeView2.default(model, playerView);
                        } else {
                            view = new _TextView2.default(model, playerView);
                        }
                    }
                    break;
                }
            case _constants.WIDGET_TYPE.doclink:
                {
                    if (building) {
                        view = new _BuilderComponentView2.default(model, playerView);
                    } else {
                        if (editable) {
                            view = new _DocLinkView2.default(model, playerView);
                        } else {
                            view = new _DocLinkTextView2.default(model, playerView);
                        }
                    }
                    break;
                }

            case _constants.WIDGET_TYPE.counter:
                {
                    if (building) {
                        view = new _BuilderComponentView2.default(model, playerView);
                    } else {
                        view = new _TextView2.default(model, playerView);
                    }
                    break;
                }

            case _constants.WIDGET_TYPE.htd:
                {
                    if (editable) {
                        view = new _RichTextView2.default(model, playerView);
                    } else {
                        view = new _TextView2.default(model, playerView);
                    }
                    break;
                }
            case _constants.WIDGET_TYPE.image:
                {
                    view = new _ImageView2.default(model, playerView);
                    break;
                }

            case _constants.WIDGET_TYPE.processlist:
                {
                    view = new _ProcessExecutionView2.default(model, playerView, editable);
                    break;
                }

            case _constants.WIDGET_TYPE.resolutionlist:
                {
                    view = new _ResolutionListView2.default(model, playerView, editable);
                    break;
                }

            case _constants.WIDGET_TYPE.signlist:
                {
                    view = new _SignListView2.default(model, playerView, editable);
                    break;
                }

            case _constants.WIDGET_TYPE.personlink:
                {
                    view = new _AddressLinkView2.default(model, playerView, editable);
                    break;
                }

            case _constants.WIDGET_TYPE.link:
                {
                    view = new _LinkView2.default(model, playerView, editable);
                    break;
                }

            case _constants.WIDGET_TYPE.reglink:
                {
                    view = new _RegistryLinkView2.default(model, playerView, editable);
                    break;
                }

            case _constants.WIDGET_TYPE.projectlink:
                {
                    if (editable) {
                        view = new _ProjectLinkView2.default(model, playerView, editable);
                    } else {
                        view = new _ProjectLinkTextView2.default(model, playerView);
                    }

                    break;
                }

            case _constants.WIDGET_TYPE.filelink:
                {
                    view = new _FileLinkView2.default(model, playerView, editable);
                    break;
                }

            case _constants.WIDGET_TYPE.file:
                {
                    view = new _FileView2.default(model, playerView, editable);
                    break;
                }

            case _constants.WIDGET_TYPE.entity:
                {
                    var entityType = model.asfProperty.config.entity;
                    if (entityType == 'departments') {
                        if (editable) {
                            view = new _DepartmentLinkView2.default(model, playerView, editable);
                        } else {
                            view = new _TextView2.default(model, playerView);
                        }
                    } else if (entityType == 'positions') {
                        if (editable) {
                            view = new _PositionLinkView2.default(model, playerView, editable);
                        } else {
                            view = new _TextView2.default(model, playerView);
                        }
                    } else {
                        if (editable) {
                            view = new _UserLinkView2.default(model, playerView, editable);
                        } else {
                            view = new _TextView2.default(model, playerView);
                        }
                    }

                    break;
                }

            case _constants.WIDGET_TYPE.repeater:
                {
                    if (editable) {
                        view = new _RepeatPeriodView2.default(model, playerView, editable);
                    } else {
                        view = new _TextView2.default(model, playerView);
                    }

                    break;
                }

            case _constants.WIDGET_TYPE.custom:
                {
                    if (building) {
                        view = new _BuilderComponentView2.default(model, playerView);
                    } else {
                        view = new _CustomComponentView2.default(model, playerView, editable);
                    }
                    break;
                }

            default:
                {
                    view = new _TextView2.default(model, playerView);
                    LOGGER.logServer("no view for " + JSON.stringify(model.asfProperty), model.playerModel.formId, model.playerModel.asfDataId);
                    break;
                }
        }
    } catch (e) {
        LOGGER.logServer(e, model.playerModel.formId, model.playerModel.asfDataId);
        view = new _TextView2.default(model, playerView);
    }

    if (editable && building) {
        view.setEnabled(true);
    }

    if (building) {
        if (model.asfProperty.type == _constants.WIDGET_TYPE.label || model.playerModel.format !== 1 && !model.isTable()) {
            initEditingLabel(view);
        }
    }

    return view;
}

function initEditingLabel(view) {

    var dummyText = _i18n2.default.tr("Нажмите, чтобы ввести текст");

    var editing = false;

    var textBox = (0, _jquery2.default)('<input/>', { type: 'text', class: 'asf-building-labelInput' });

    view.stopEditing = function (acceptNewValue) {
        if (acceptNewValue) {
            var newLabel = "";
            if (textBox.val() !== '') {
                newLabel = textBox.val();
            }
            updateCurrentTranslationWidget(view.model.asfProperty, newLabel);
        }
        view.label.empty();
        view.updateLabel();
        view.checkEmptyLabel();
        editing = false;
    };

    view.setEditing = function () {
        if (editing) {
            return;
        }
        editing = true;
        textBox.val(getCurrentTranslation(view.model.asfProperty));

        view.label.empty();
        view.label.append(textBox);
        textBox.keyup(function (event) {
            if (event.keyCode == _constants.KEY_CODES.ENTER) {
                view.stopEditing(true);
            } else if (event.keyCode == _constants.KEY_CODES.escape) {
                view.stopEditing(false);
            }
        });
        textBox.blur(function () {
            view.stopEditing(true);
        });
        textBox.focus();
    };

    view.label.click(function (evt) {
        view.setEditing(true);
    });

    view.checkEmptyLabel = function () {
        if (view.label.text() == '') {
            view.label.html(dummyText);
        }
    };

    view.checkEmptyLabel();
}

function getPropertyWithId(properties, cmpId, deep, ignoredTypes) {
    if (_underscore2.default.isUndefined(ignoredTypes)) {
        ignoredTypes = [];
    }
    for (var i = 0; i < properties.length; i++) {
        if (properties[i].id === cmpId && !ignoredTypes.contains(properties[i].type)) {
            return properties[i];
        } else if (deep && properties[i].properties) {
            var p = getPropertyWithId(properties[i].properties, cmpId);
            if (p) {
                return p;
            }
        }
    }
    return null;
}

function createContainer() {

    return (0, _jquery2.default)('<div></div>', { class: 'asf-container' });
}

function getCurrentTranslation(asfProperty) {
    if (asfProperty.translations) {
        var trans = asfProperty.translations;
        var defaultText = "";
        var defaultExits = false;
        for (var i = 0; i < trans.length; i++) {
            if (trans[i].localeID === _constants.OPTIONS.locale) {
                return trans[i].text;
            }
            if (trans[i].localeID === "c") {
                defaultText = trans[i].text;
                defaultExits = true;
            }
        }
        if (defaultExits) {
            return defaultText;
        } else {
            return asfProperty.label;
        }
    } else {
        return asfProperty.label;
    }
}

function updateCurrentTranslationWidget(asfProperty, newLabel) {
    if (asfProperty.translations) {
        var trans = asfProperty.translations;
        var defaultText = null;
        for (var i = 0; i < trans.length; i++) {
            if (trans[i].localeID === _constants.OPTIONS.locale) {
                trans[i].text = newLabel;
                return;
            }
            if (trans[i].localeID === "c") {
                defaultText = trans[i];
            }
        }
        if (defaultText) {
            defaultText.text = newLabel;
            return;
        }
    }
    asfProperty.label = newLabel;
}

function makeBus(object) {
    object.bus = (0, _jquery2.default)({});
    object.trigger = function (eventType, args) {
        object.bus.trigger(eventType, args);
    };
    object.on = function (eventType, handler) {
        object.bus.on(eventType, handler);
    };
    object.off = function (eventType, handler) {
        object.bus.off(eventType, handler);
    };
}

function createLabel(text) {
    return (0, _jquery2.default)('<div></div>', { class: 'asf-label' }).html(text);
}

function createInlineStyledLabel(text, style) {
    var label = (0, _jquery2.default)('<div></div>', { class: 'asf-Inline' }).html(text);
    styleUtils.applyFontStyle(label, style);
    styleUtils.applySize(label, style);
    return label;
}

function fillView(view, pageViews, editable) {

    view.container.children().detach();

    var addPageBreak = false;
    pageViews.forEach(function (pageView) {
        if (addPageBreak && editable) {
            view.container.append(createPageBreak());
        }
        view.container.append(pageView.container);
        addPageBreak = true;
    });

    view.appendButtons();
}

function createPageBreak() {
    var pageBreak = (0, _jquery2.default)("<div>", { class: "asf-pageBreak" });
    pageBreak.append((0, _jquery2.default)("<span style='background:#fff; padding:0 10px;'>" + _i18n2.default.tr("разрыв страницы") + "</span>"));
    return pageBreak;
}

function initInput(model, view, input, ignoreSize) {

    styleUtils.applyLabelStyle(input, model.asfProperty.style, ignoreSize);

    input.blur(function () {
        if (view.getValueForModel) {
            model.setValueFromInput(view.getValueForModel());
        } else {
            model.setValueFromInput(input.val());
        }
        view.checkValid();
    });

    input.keyup(function () {
        if (view.getValueForModel) {
            model.setValueFromInput(view.getValueForModel());
        } else {
            model.setValueFromInput(input.val());
        }
    });

    input.focus(function () {
        view.unmarkInvalid();
    });
}

function findMaster(asfProperty, playerModel) {
    var masterModel = playerModel.getModelWithId(asfProperty.config.depends, asfProperty.ownerTableId, asfProperty.tableBlockIndex);
    if (!masterModel) {
        masterModel = playerModel.getModelWithId(asfProperty.config.depends);
    }
    return masterModel;
}

function setTranslatedValue(value, locale, target) {
    if (locale == _constants.OPTIONS.locale) {
        (0, _jquery2.default)(target).html(_i18n2.default.tr(value));
    } else {
        api.translate(value, locale, function (translatedValue) {
            (0, _jquery2.default)(target).html(translatedValue);
        });
    }
}

function addSimpleHtml(value, preserveWhiteSpace) {
    var result = value || '';
    if (!preserveWhiteSpace) {
        result = result.trim();
    }

    result = result.replace(/\n/g, '<br>');
    return result;
}

function createFunction(playerModel, functionCode, functionPrefix) {
    if (_constants.OPTIONS.noCustomScripting || !functionCode || playerModel.building) {
        return function () {};
    } else {
        var script = "try {\n";
        script += functionPrefix + "\n";
        script += functionCode + "\n";
        script += "} catch (e) {\n " + " var formId, asfDataId; \n " + " if(model && model.playerModel) {\n " + "    formId = model.playerModel.formId; \n " + "    asfDataId = model.playerModel.asfDataId; \n " + " }" + " AS.LOGGER.logServer(e, formId, asfDataId);\n" + "}\n";
        return new Function(script);
    }
}

/***/ }),

/***/ 156:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.componentSettingsChanged = componentSettingsChanged;
exports.extendAsfProperty = extendAsfProperty;

var _tableUtils = __webpack_require__(52);

var _constants = __webpack_require__(2);

function componentSettingsChanged(newAsfProperty, model, tableModel, playerView) {

    var locationInfo = (0, _tableUtils.getComponentById)(tableModel.asfProperty.layout, model.asfProperty.id);

    if (newAsfProperty.type === _constants.WIDGET_TYPE.listbox || newAsfProperty.type === _constants.WIDGET_TYPE.check || newAsfProperty.type === _constants.WIDGET_TYPE.radio) {
        delete newAsfProperty.default;
        delete newAsfProperty.data;

        var selectedIds = model.getValue();

        tableModel.deleteModel(model);
        var newModel = tableModel.insertComponentTo(newAsfProperty, locationInfo.row, locationInfo.column);

        if (selectedIds && selectedIds.length > 0) {
            newModel.setValue(selectedIds);
        }

        if (newAsfProperty.dataSource) {
            var locale = newAsfProperty.dataSource.locale;
            if (!locale) {
                locale = _constants.OPTIONS.locale;
            }
            model.playerModel.dictionaryCache.loadDictionary(newAsfProperty.dataSource.dict, locale);
        }
    } else if (newAsfProperty.type == _constants.WIDGET_TYPE.table) {

        var asfProperty = model.asfProperty;

        tableModel.deleteModel(model);
        try {
            var newTableModel = tableModel.insertComponentTo(newAsfProperty, locationInfo.row, locationInfo.column);
        } catch (e) {
            console.log(e);
            console.log(newAsfProperty);
        }

        if (newAsfProperty.config.isHaveHeaders && !asfProperty.config.isHaveHeaders) {
            newTableModel.addTableRow(0);
        } else if (!newAsfProperty.config.isHaveHeaders && asfProperty.config.isHaveHeaders) {
            newTableModel.deleteTableRow(0);
        }
    } else {
        tableModel.deleteModel(model);
        tableModel.insertComponentTo(newAsfProperty, locationInfo.row, locationInfo.column);
    }
}

function extendAsfProperty(model, tableModel) {
    var asfProperty = model.getProperty();
    asfProperty.fullId = asfProperty.id;
    if (tableModel.isDynamic()) {
        asfProperty.fullId = tableModel.asfProperty.id + "." + asfProperty.id;
    }
    return asfProperty;
}

/***/ }),

/***/ 157:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(jQuery) {

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.NumericUtils = undefined;
exports.default = NumericModel;

var _constants = __webpack_require__(2);

var _model = __webpack_require__(25);

var _model2 = _interopRequireDefault(_model);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _caretUtils = __webpack_require__(127);

var caretUtils = _interopRequireWildcard(_caretUtils);

var _math = __webpack_require__(112);

var _math2 = _interopRequireDefault(_math);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Created by exile
 * Date: 15.03.16
 * Time: 11:25
 */

/**
 * asfData
 * {
     *        "id": "cmp-qdg8xo",
     *        "type": "numericinput",
     *        "value": "2  222,00",
     *        "key": "2222.00"
     *    }
 * модель число��ого поля
 * @param asfProperty
 * @param playerModel
 * @constructor
 */
function NumericModel(asfProperty, playerModel) {
    _model2.default.call(this, this, asfProperty, playerModel);

    var instance = this;

    this.getTextValue = function () {
        return NumericUtils.formatForTextView(instance.value, asfProperty);
    };

    this.getSpecialErrors = function () {
        if (asfProperty.config['BV_ACTIV']) {
            if (this.value) {
                var valueBigNumber = _math2.default.bignumber(this.value);
                if (asfProperty.config['BV_ACTIV']) {
                    var minBigNumber = _math2.default.bignumber(asfProperty.config['MIN']);
                    var maxBigNumber = _math2.default.bignumber(asfProperty.config['MAX']);

                    if (_math2.default.compare(valueBigNumber, minBigNumber) == -1) {
                        return { id: asfProperty.id, errorCode: _constants.INPUT_ERROR_TYPE.valueTooSmall };
                    }
                    if (_math2.default.compare(valueBigNumber, maxBigNumber) == 1) {
                        return { id: asfProperty.id, errorCode: _constants.INPUT_ERROR_TYPE.valueTooHigh };
                    }
                }
            }
        }
    };

    this.getAsfData = function (blockNumber) {
        var result = dataUtils.getBaseAsfData(asfProperty, blockNumber, instance.getTextValue(), instance.value);
        return result;
    };

    this.setAsfData = function (asfData) {
        if (!asfData) {
            return;
        }
        this.setValue(asfData.key);
    };

    return this;
};

NumericModel.prototype = Object.create(_model2.default.prototype);
NumericModel.prototype.constructor = NumericModel;

var NumericUtils = exports.NumericUtils = {
    /**
     * инициализация поля ввода, добавляем лисенеры для фильтрации ввода, следим за вставкой, удалением символов, режим калькулятора
     * @param model
     * @param view
     * @param textBox
     */
    initNumericInput: function initNumericInput(model, view, textBox) {
        var oldValue = "";

        view.updateValueFromModel = function () {
            if (view.getValueForModel() == model.getValue()) {
                return;
            }
            oldValue = NumericUtils.formatForInput(model.getValue(), model.asfProperty);

            if (jQuery(textBox).is(":focus")) {
                textBox.val(oldValue);
            } else {
                var notFocusedValue = NumericUtils.formatForTextView(model.getValue(), model.asfProperty);
                textBox.val(notFocusedValue);
            }
        };

        var getMode = function getMode(value) {
            return value && value.indexOf('=') === 0;
        };

        var calculate = function calculate(value) {
            try {
                value = value.substring(1).replace(/,/g, '.');
                var bigNumber = _math2.default.eval(value);
                var result = NumericUtils.removeTrailingZero(_math2.default.format(bigNumber, { notation: 'fixed', precision: 18 }));
                if (result === 'Infinity') {
                    result = "";
                }
                textBox.val(result);
            } catch (exc) {
                console.log(exc);
                textBox.val('');
            }
        };

        var checkBeforeInput = function checkBeforeInput(event) {

            var value = textBox.val();
            var calculatorMode = getMode(value);

            if (calculatorMode) {
                if (event.keyCode == _constants.KEY_CODES.ENTER) {
                    calculate(value);
                    return;
                }
            } else {
                if (event.keyCode == _constants.KEY_CODES.ENTER) {
                    textBox.blur();
                    return;
                }
            }
        };

        var checkAfterInput = function checkAfterInput(event) {
            var caretPosition = caretUtils.getCaretPosition(textBox[0]);
            var value = textBox.val();
            if (value == oldValue) {
                return;
            }
            value = validateEnteredData(value);
            textBox.val(value);
            caretUtils.setCaretPosition(textBox[0], caretPosition);
            oldValue = textBox.val();
        };

        var checkPaste = function checkPaste(event) {
            var caretPosition = caretUtils.getCaretPosition(textBox[0]);
            var value = textBox.val();
            if (value == oldValue) {
                return;
            }

            var calculatorMode = getMode(value);
            if (calculatorMode) {
                calculate(value);
            } else {
                value = validateEnteredData(value);
                value = NumericUtils.checkNumber(value);
                if (value == null) {
                    // да вот здесь именно с null сравниваем ибо может вернуться и 0 и это будет нормально
                    value = oldValue;
                }
                textBox.val(value);
                caretUtils.setCaretPosition(textBox[0], caretPosition);
            }
            oldValue = textBox.val();
        };

        var selectAll = function selectAll(event) {
            textBox.select();
        };

        var scheduleFormatValue = function scheduleFormatValue() {
            setTimeout(function () {
                checkPaste();
            }, 0);
        };

        var validateEnteredData = function validateEnteredData(inputValue) {
            if (model.asfProperty.config['DS_TYPE'] == _constants.DECIMAL_DELIMITER.COMMA) {
                inputValue = inputValue.replace(/\./, ',');
            } else {
                inputValue = inputValue.replace(',', '.');
            }
            return inputValue;
        };

        var setRealValue = function setRealValue() {
            textBox.val(oldValue);
            textBox.select();
        };

        var hideRealValue = function hideRealValue() {
            oldValue = textBox.val();

            var calculatorMode = getMode(value);

            if (calculatorMode) {
                return;
            }

            var value = NumericUtils.parseFromInput(oldValue, model.asfProperty);

            if (model.asfProperty.config['BV_ACTIV'] && value != '') {
                var valueBigNumber = _math2.default.bignumber(value);
                var min = model.asfProperty.config['MIN'];
                var minBigNumber = _math2.default.bignumber(min);
                var max = model.asfProperty.config['MAX'];
                var maxBigNumber = _math2.default.bignumber(max);

                if (_math2.default.compare(valueBigNumber, minBigNumber) == -1) {
                    value = min;
                }
                if (_math2.default.compare(valueBigNumber, maxBigNumber) == 1) {
                    value = max;
                }
            }
            textBox.val(NumericUtils.formatForTextView(value, model.asfProperty));
        };

        view.updateValueFromModel();

        var prevCaretPosition = "";
        var prevValue = "";

        textBox.on("keydown", function () {
            prevCaretPosition = caretUtils.getCaretPosition(textBox[0]);
            prevValue = textBox.val();
        });

        textBox.on("input", function () {

            var invalid = false;

            var value = textBox.val();
            var allowedChars = '0123456789.,';
            if (value.startsWith("=")) {
                value = value.substring(1);
                allowedChars += "-*/+";

                if (value.startsWith("*") || value.startsWith("/") || value.startsWith("+")) {
                    invalid = true;
                }
            } else {
                if (value.startsWith("-")) {
                    value = value.substring(1);
                }
            }

            value.split("").forEach(function (s) {
                if (allowedChars.indexOf(s) === -1) {
                    invalid = true;
                }
            });

            var invalidSequences = ['++', '+*', '+/', '*+', '**', '*/', '/+', '/*', '//', '---', '+-+', '-+-', '*-*', '-*-', '/-/', '-/-', '+--', '*--', '/--'];

            invalidSequences.forEach(function (invalidSequence) {
                if (value.indexOf(invalidSequence) !== -1) {
                    invalid = true;
                }
            });

            var numbers = value.match(/[\*\/\+\-]{0,2}(.[^\*\/\+\-]*)[\*\/\+\-]{0,2}/g);
            if (numbers !== null) {
                numbers.forEach(function (number) {
                    var n = number.match(/[\*\/\+\-]{0,2}(.[^\*\/\+\-]*)[\*\/\+\-]{0,2}/);
                    if (!n || n.length < 2 || NumericUtils.checkNumber(n[1]) == null) {
                        invalid = true;
                    }
                });
            }

            if (invalid) {
                textBox.val(prevValue);
                caretUtils.setCaretPosition(textBox[0], prevCaretPosition);
            }
        });

        textBox.on("keypress", checkBeforeInput);
        textBox.on("keyup", checkAfterInput);

        textBox.on("click", selectAll);
        textBox.blur(hideRealValue);
        textBox.focus(setRealValue);
    },

    /**
     * парсим ввод, сначала смотрим число ли это, потом смотрим настройки. возвращает либо строку представляющую собой число
     * либо null если переданная строка не число
     * @param{string} inputValue значение поля ввода
     * @param{asfProperty} asfProperty свойства компонента
     * @returns {string} число или null
     */
    parseFromInput: function parseFromInput(inputValue, asfProperty) {

        inputValue = NumericUtils.checkNumber(inputValue);
        if (inputValue == null || inputValue == '' || inputValue == '-') {
            return '';
        }

        var bigNumber = _math2.default.bignumber(inputValue);
        var result = _math2.default.format(bigNumber, { notation: 'fixed', precision: NumericUtils.getNumberPrecision(inputValue) });
        if (asfProperty.config && asfProperty.config['RP_ACTIV']) {
            var cnt = asfProperty.config['RP_COUNT'];
            if (asfProperty.config['ROUND']) {
                result = _math2.default.format(bigNumber, { notation: 'fixed', precision: cnt });
            } else {
                if (inputValue.match("(-{0,1}[0-9]+\\.{0,1}[0-9]{0," + cnt + "})")) {
                    bigNumber = _math2.default.bignumber(inputValue.match("(-{0,1}[0-9]+\\.{0,1}[0-9]{0," + cnt + "})")[1]);
                    result = _math2.default.format(bigNumber, { notation: 'fixed', precision: cnt });
                } else {
                    result = _math2.default.format(bigNumber, { notation: 'fixed', precision: cnt });
                }
            }
        }

        return result;
    },

    /**
     *  убираем ничего не значащие нули в конце дроби
     * @param{int} number
     * @returns {*}
     */
    removeTrailingZero: function removeTrailingZero(number) {
        if (number.indexOf(",") > -1) {
            number = number.replace(",", ".");
            number = number.replace(/(\.[0-9]*?)0+$/, "$1").replace(/\.$/, "");
            return number.replace(".", ",");
        } else {
            return number.replace(/(\.[0-9]*?)0+$/, "$1").replace(/\.$/, "");
        }
    },

    /**
     * проверка число ли это
     * @param inputValue значение поля ввода
     * @param asfProperty свойства компонента
     * @returns {Number} число или null
     */
    checkNumber: function checkNumber(inputValue) {

        inputValue = inputValue.replace(',', '.');

        if (inputValue.match(/^-?\d*\.?\d*$/)) {
            return inputValue;
        } else {
            return null;
        }
    },

    /**
     * форматируем для инпута число
     * @param {Number} value
     * @param asfProperty
     * @returns {string}
     */
    formatForInput: function formatForInput(value, asfProperty) {

        if (value == null || value == undefined || value == '') {
            return '';
        }
        var bigNumber = _math2.default.bignumber(value);

        var inputValue = _math2.default.format(bigNumber, { notation: 'fixed', precision: NumericUtils.getNumberPrecision(value) });
        if (asfProperty.config['RP_ACTIV']) {
            var cnt = asfProperty.config['RP_COUNT'];
            inputValue = _math2.default.format(bigNumber, { notation: 'fixed', precision: cnt });
        }

        if (asfProperty.config['DS_TYPE'] == _constants.DECIMAL_DELIMITER.COMMA) {
            inputValue = inputValue.replace(/\./, ',');
        }

        return inputValue;
    },

    /**
     * форматируем число согласно заданному формату
     * @param {Number} value
     * @param asfProperty
     * @returns {string}
     */
    formatForTextView: function formatForTextView(value, asfProperty) {
        var textValue = NumericUtils.formatForInput(value, asfProperty);
        if (textValue != '' && asfProperty.config['TS_ACTIVE']) {
            var integerPart = textValue;
            var fractionPart = "";
            var dsType = asfProperty.config['DS_TYPE'] || '.';
            if (integerPart.indexOf(".") != -1) {
                integerPart = textValue.substring(0, textValue.indexOf('.'));
                fractionPart = textValue.substring(textValue.indexOf('.'));
            } else if (integerPart.indexOf(",") != -1) {
                integerPart = textValue.substring(0, textValue.indexOf(','));
                fractionPart = textValue.substring(textValue.indexOf(','));
            }
            textValue = integerPart.formatThousands(asfProperty.config['TS_VALUE']) + fractionPart;
        }
        return textValue;
    },

    getNumberPrecision: function getNumberPrecision(value) {
        var precision = 0;
        if (value.indexOf(".") != -1) {
            precision = value.length - value.indexOf(".") - 1;
        } else if (value.indexOf(",") != -1) {
            precision = value.length - value.indexOf(",") - 1;
        }
        return precision;
    }

};
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ }),

/***/ 158:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.formatName = formatName;
exports.getFormattedValue = getFormattedValue;
exports.getFilterEntityId = getFilterEntityId;
exports.parseAsfData = parseAsfData;
exports.join = join;

var _underscore = __webpack_require__(7);

var _underscore2 = _interopRequireDefault(_underscore);

var _constants = __webpack_require__(2);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function formatName(person, asfProperty) {

    if (!person.lastname) {
        return person.personName;
    }

    var locale = dataUtils.getComponentLocale(asfProperty);
    var format = "";
    if (asfProperty.config && asfProperty.config.customNameFormats) {
        format = asfProperty.config.customNameFormats[dataUtils.validateLocale(locale)] || format;
    }

    var parts = format.match(/(\$\{[^\}\{\$]+\})/g);
    if (!parts || parts.length == 0) {
        return person.personName;
    } else {
        parts.forEach(function (part) {
            format = format.replace(part, getFormattedValue(person, part));
        });
    }
    return format;
}

function getFormattedValue(person, part) {
    var f = part.match(/\$\{(.+)\}/)[1];

    switch (f) {
        case _constants.NAME_FORMAT_PARTS.l:
            {
                return person.lastname || "";
            }
        case _constants.NAME_FORMAT_PARTS.l_short:
            {
                if (person.lastname && person.lastname.length > 0) {
                    return person.lastname.substring(0, 1);
                } else {
                    return "";
                }
            }
        case _constants.NAME_FORMAT_PARTS.f:
            {
                return person.firstname || "";
            }
        case _constants.NAME_FORMAT_PARTS.f_short:
            {
                if (person.firstname && person.firstname.length > 0) {
                    return person.firstname.substring(0, 1);
                } else {
                    return "";
                }
            }
        case _constants.NAME_FORMAT_PARTS.f_short_dot:
            {
                if (person.firstname && person.firstname.length > 0) {
                    return person.firstname.substring(0, 1) + ".";
                } else {
                    return "";
                }
            }
        case _constants.NAME_FORMAT_PARTS.p:
            {
                return person.patronymic || "";
            }
        case _constants.NAME_FORMAT_PARTS.p_short:
            {
                if (person.patronymic && person.patronymic.length > 0) {
                    return person.patronymic.substring(0, 1);
                } else {
                    return "";
                }
            }
        case _constants.NAME_FORMAT_PARTS.p_short_dot:
            {
                if (person.patronymic && person.patronymic.length > 0) {
                    return person.patronymic.substring(0, 1) + ".";
                } else {
                    return "";
                }
            }
    }
    return part;
}

function getFilterEntityId(masterModel, entityType) {
    if (masterModel && masterModel.getEntityType() === entityType) {
        if (masterModel.getSelectedIds().length > 0) {
            return masterModel.getSelectedIds()[0];
        }
    }
    return null;
}

function parseAsfData(asfData, keyDelimiter, valueDelimiter) {
    var itemIds = asfData.key.split(keyDelimiter);
    var manualTags = asfData.manualTags || {};
    var tags = [];

    if (asfData.value) {
        tags = asfData.value.split(valueDelimiter);
    }

    if (_underscore2.default.last(itemIds) === '') {
        itemIds.splice(itemIds.length - 1, 1);
    }

    if (_underscore2.default.last(tags) === '') {
        tags.splice(tags.length - 1, 1);
    }

    if (!asfData.formatVersion) {
        itemIds.forEach(function (itemId, index) {
            manualTags[itemId] = tags[index].trim();
        });
    }
    return { itemIds: itemIds, manualTags: manualTags, tags: tags };
}

function join(data, attName, delimiter) {
    if (data === null || !(data instanceof Array) || data.length === 0) {
        return "";
    }
    var result = "";
    var sign = "";
    data.forEach(function (val) {
        result += sign + val[attName];
        sign = delimiter;
    });
    return result;
}

/***/ }),

/***/ 159:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = TableBlock;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _underscore = __webpack_require__(7);

var _underscore2 = _interopRequireDefault(_underscore);

var _constants = __webpack_require__(2);

var _componentUtils = __webpack_require__(15);

var compUtils = _interopRequireWildcard(_componentUtils);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _layoutUtils = __webpack_require__(65);

var layoutUtils = _interopRequireWildcard(_layoutUtils);

var _tableUtils = __webpack_require__(52);

var tableUtils = _interopRequireWildcard(_tableUtils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function TableBlock(model, models, playerView, editable, header) {
    var instance = this;

    this.rows = [];

    this.updateDom = function () {
        instance.rows.forEach(function (row) {
            row.detach();
        });
        var fixed = model.asfProperty.config.fixedLayout;

        var layout = model.asfProperty.layout;
        var matrix = layoutUtils.makeLayoutMatrix(layout);
        instance.matrix = matrix;

        this.rows = [];

        var maxRow = matrix.length - 1;
        if (header) maxRow = Math.min(0, maxRow);

        for (var rowi = 0; rowi <= maxRow; rowi++) {
            var $tr = (0, _jquery2.default)('<tr/>');
            for (var coli = 0; coli < matrix[rowi].length; coli++) {
                var comp = matrix[rowi][coli];
                var $cell = tableUtils.createCellForComp(comp, layout, model.asfProperty.style, fixed);
                if ($cell) $tr.append($cell);
            }
            this.rows.push($tr);
        }
        if (!model.isStatic() && !model.isPage() && editable) {
            this.rows.forEach(function (row) {
                var cell = (0, _jquery2.default)('<td></td>', { class: 'asf-cell asf-tableDeleteCell' });
                row.append(cell);
                instance.deleteColumnCells.push(cell);
            });

            if (!header) {
                var removeRowButton = (0, _jquery2.default)('<div/>', { class: 'asf-tableRemoveButton' });
                dataUtils.addAsFormId(removeRowButton, "removeRowButton", model.asfProperty);
                _underscore2.default.last(this.deleteColumnCells).append(removeRowButton);
                removeRowButton.click(function () {
                    var indexToRemove = instance.getBlockNumber();
                    model.removeRow(indexToRemove);
                });
            }
        }

        instance.fillRows(model, this.views, this.rows, header);

        model.trigger(_constants.EVENT_TYPE.tableDomUpdated);
    };

    this.fillRows = function (model, views, rows, isHeader) {

        var components = model.asfProperty.layout.components;
        if (isHeader) {
            components = model.asfProperty.layout.headerComponents;
        }

        if (!components) {
            return;
        }

        _underscore2.default.each(components, function (comp) {
            var $td = instance.findTd(layoutUtils.compToCell(comp));

            $td.children().detach();
            var view = tableUtils.shallowFindView(comp.id, views);

            if (view) {
                if (view.model.asfProperty.type == _constants.WIDGET_TYPE.table) {
                    $td.css("padding-left", "0px");
                    $td.css("padding-right", "0px");
                }
                $td.append(view.container);
            }
        });
    };

    model.on(_constants.TABLE_EVENT_TYPE.COLUMN_SHOW, function (event, colNumber) {
        instance.rows.forEach(function (row) {
            (0, _jquery2.default)(row.children("td")[colNumber]).show();
        });
    });

    model.on(_constants.TABLE_EVENT_TYPE.COLUMN_HIDE, function (event, colNumber) {
        instance.rows.forEach(function (row) {
            (0, _jquery2.default)(row.children("td")[colNumber]).hide();
        });
    });

    model.on(_constants.TABLE_EVENT_TYPE.BLOCK_SHOW, function (event, block) {
        if (block !== instance) {
            return;
        }
        instance.rows.forEach(function (row) {
            row.show();
        });
    });

    model.on(_constants.TABLE_EVENT_TYPE.BLOCK_HIDE, function (event, block) {
        if (block !== instance) {
            return;
        }
        instance.rows.forEach(function (row) {
            row.hide();
        });
    });

    model.on(_constants.TABLE_EVENT_TYPE.BLOCK_DELETED, function (event, block) {
        if (block !== instance) {
            return;
        }
        instance.rows.forEach(function (row) {
            row.remove();
        });
    });

    model.on(_constants.TABLE_EVENT_TYPE.MERGE_CELL, function (event, row, column, rows, cols) {
        if (header) {
            return;
        }
        instance.rows.forEach(function (row, rowIndex) {
            if (rowIndex < row || rowIndex >= row + rows) {
                return;
            }
            row.children('td').each(function (cellIndex, cell) {
                if (cellIndex < column || cellIndex >= column + cols) {
                    return;
                }
                if (rowIndex === row && cellIndex === column) {

                    (0, _jquery2.default)(cell).attr("rowSpan", rows);
                    (0, _jquery2.default)(cell).attr("colSpan", cols);

                    return;
                }
                (0, _jquery2.default)(cell).hide();
            });
        });
    });

    model.on(_constants.TABLE_EVENT_TYPE.SPLIT_CELL, function (event, row, column, rows, cols) {
        if (header) {
            return;
        }
        instance.rows.forEach(function (row, rowIndex) {
            if (rowIndex < row || rowIndex >= row + rows) {
                return;
            }
            row.children('td').each(function (cellIndex, cell) {
                if (cellIndex < column || cellIndex >= column + cols) {
                    return;
                }
                if (rowIndex === row && cellIndex === column) {
                    (0, _jquery2.default)(cell).removeAttr("rowSpan");
                    (0, _jquery2.default)(cell).removeAttr("colSpan");
                    return;
                }
                (0, _jquery2.default)(cell).show();
            });
        });
    });

    this.findTd = function (cell) {
        var tdIndex = layoutUtils.findTdIndex(instance.matrix, cell);
        if (tdIndex < 0) return null;
        return (0, _jquery2.default)(instance.rows[cell[0]].children('td')[tdIndex]);
    };

    this.findCompTd = _underscore2.default.compose(this.findTd, layoutUtils.compToCell);

    this.getViewByTd = function ($td) {
        if (!$td) return null;
        return _underscore2.default.find(instance.views, function (view) {
            return view.container.parent()[0] === $td[0];
        });
    };

    this.getViewCoords = function (view) {
        var component = tableUtils.getComponentById(model.asfProperty.layout, view.model.asfProperty.id);
        if (component) return [component.row, component.column];else return null;
    };

    this.insertComponentTo = function (newModel) {
        var component = tableUtils.getComponentById(model.asfProperty.layout, newModel.asfProperty.id);
        var view = compUtils.createView(newModel, playerView, editable, model.playerModel.building);
        instance.views.push(view);
        var $td = instance.findCompTd(component);
        $td.append(view.container);
    };

    this.clearComponent = function (modelToDelete) {
        var viewToDelete = null;
        instance.views.forEach(function (view, index) {
            if (view.model == modelToDelete) {
                viewToDelete = view;
            }
        });

        var index = instance.views.indexOf(viewToDelete);
        instance.views.splice(index, 1);

        var $td = viewToDelete.container.parent();
        $td.children('.asf-container').remove();
    };

    this.getTdCoord = function ($td) {
        var rowIndex = -1;
        var colIndex = -1;

        _underscore2.default.some(instance.rows, function (row, rowI) {
            colIndex = row.children('td').index($td);
            if (colIndex === -1) return false;
            rowIndex = rowI;
            return true;
        });

        if (rowIndex === -1) return null;

        var comp = layoutUtils.findMatrixComp(instance.matrix, {
            row: rowIndex,
            column: colIndex
        });

        return [comp.row, comp.column];
    };

    this.getCellRowNumber = _underscore2.default.compose(_underscore2.default.first, this.getTdCoord);
    this.getCellColumnNumber = _underscore2.default.compose(_underscore2.default.last, this.getTdCoord);

    this.getBlockNumber = function () {
        return model.modelBlocks.indexOf(models);
    };

    this.executeScript = function () {
        instance.views.forEach(function (view) {
            view.executeScript(view.model, view, editable);
        });
    };

    this.views = tableUtils.createViews(model, models, playerView, editable, model.playerModel.building);

    this.deleteColumnCells = [];

    this.updateDom();

    if (!model.isStatic() && !model.isPage() && editable) {
        model.on(_constants.TABLE_EVENT_TYPE.ENABLED, function () {
            instance.deleteColumnCells.forEach(function (cell) {
                cell.show();
            });
        });

        model.on(_constants.TABLE_EVENT_TYPE.DISABLED, function () {
            instance.deleteColumnCells.forEach(function (cell) {
                cell.hide();
            });
        });
    }

    if (!model.isHaveHeader() && header) {
        instance.rows.forEach(function (row) {
            row.hide();
        });
    }
};

/***/ }),

/***/ 160:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = TableBuilderView;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _underscore = __webpack_require__(7);

var _underscore2 = _interopRequireDefault(_underscore);

var _i18n = __webpack_require__(14);

var _i18n2 = _interopRequireDefault(_i18n);

var _services = __webpack_require__(23);

var services = _interopRequireWildcard(_services);

var _constants = __webpack_require__(2);

var _styleUtils = __webpack_require__(21);

var styleUtils = _interopRequireWildcard(_styleUtils);

var _utils = __webpack_require__(82);

var _layoutUtils = __webpack_require__(65);

var layoutUtils = _interopRequireWildcard(_layoutUtils);

var _tableUtils = __webpack_require__(52);

var tableUtils = _interopRequireWildcard(_tableUtils);

var _domUtils = __webpack_require__(44);

var domUtils = _interopRequireWildcard(_domUtils);

var _math = __webpack_require__(112);

var _math2 = _interopRequireDefault(_math);

var _tableStaticView = __webpack_require__(129);

var _tableStaticView2 = _interopRequireDefault(_tableStaticView);

var _selectionInfo = __webpack_require__(275);

var _selectionInfo2 = _interopRequireDefault(_selectionInfo);

var _selectTableButton = __webpack_require__(405);

var _selectTableButton2 = _interopRequireDefault(_selectTableButton);

var _clearTableButton = __webpack_require__(402);

var _clearTableButton2 = _interopRequireDefault(_clearTableButton);

var _rowRuler = __webpack_require__(404);

var _rowRuler2 = _interopRequireDefault(_rowRuler);

var _columnRuler = __webpack_require__(403);

var _columnRuler2 = _interopRequireDefault(_columnRuler);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function TableBuilderView(tableModel, playerView, editable) {

    tableModel.isStatic = function () {
        return true;
    };

    _tableStaticView2.default.call(this, tableModel, playerView, editable);

    var instance = this;

    var rowRulers = [];
    var colRulers = [];
    var selectTableButton = null;
    var clearPageButton = null;

    tableModel.selection.on(_constants.EVENT_TYPE.selectSingleCell, function (evt, row, col) {
        var cellView = instance.getCellView([row, col]);
        var selectionInfo = new _selectionInfo2.default(tableModel.asfProperty.id, row, col, cellView);
        playerView.model.trigger(_constants.EVENT_TYPE.selectSingleCell, selectionInfo);
    });

    tableModel.on(_constants.EVENT_TYPE.tableDomUpdated, function () {
        instance.createRulers();
        instance.initSelectionListeners();
        instance.updateSelection();
    });

    tableModel.selection.on(_constants.EVENT_TYPE.selectionChange, function () {
        playerView.model.trigger(_constants.EVENT_TYPE.selectionChange);
        instance.updateSelection();
        instance.validateRulersVisibility();
    });

    tableModel.on(_constants.EVENT_TYPE.tableDomUpdate, function () {
        instance.layoutChanged();
    });

    function onSelectSingleCell(evt, selectionInfo) {
        if (selectionInfo.tableId !== instance.model.asfProperty.id) {
            instance.model.selection.clear();
            return;
        }
        instance.clearSelection();
        var $cell = instance.findCompTd(selectionInfo);
        instance.markSelected($cell);
        instance.validateRulersVisibility();
    }

    function onLayoutChanged() {
        tableUtils.maybeChangeWidth(instance.getTable(), tableModel.asfProperty, 80);
        setTimeout(instance.recalculateRulers, 10);
    }

    playerView.model.on(_constants.EVENT_TYPE.selectSingleCell, onSelectSingleCell);

    playerView.model.on(_constants.EVENT_TYPE.layoutChanged, onLayoutChanged);

    playerView.model.on(_constants.EVENT_TYPE.cellSizeChange, onLayoutChanged);

    this.getTdCoord = instance.viewBlock.getTdCoord;
    this.getViewByTd = instance.viewBlock.getViewByTd;
    this.findTd = instance.viewBlock.findTd;
    this.getCellView = _underscore2.default.compose(this.getViewByTd, this.findTd);
    this.findCompTd = instance.viewBlock.findCompTd;

    instance.getViewCoords = instance.viewBlock.getViewCoords;

    this.findTr = function (row) {
        return instance.viewBlock.rows[row];
    };

    this.getTableViews = function () {
        return _underscore2.default.filter(instance.viewBlock.views, function (view) {
            return view.model.asfProperty.type === _constants.WIDGET_TYPE.table;
        });
    };

    this.getFullSelection = function () {
        var selection = [{
            view: instance,
            cells: instance.model.selection.cells,
            selection: instance.model.selection
        }];

        return _underscore2.default.reduce(this.getTableViews(), function (sel, tView) {
            return sel.concat(tView.getFullSelection());
        }, selection);
    };

    this.updateSelection = function () {
        _underscore2.default.each(instance.viewBlock.rows, function (row, rowNum) {
            var matrixRow = _underscore2.default.reject(instance.viewBlock.matrix[rowNum], _underscore2.default.isNull);
            row.children('td').each(function (colNum, td) {
                var comp = matrixRow[colNum];
                var selected = instance.model.selection.isCompSelected(comp);
                instance.markSelection(selected, td);
            });
        });
    };

    this.clearSelection = function () {
        instance.viewBlock.rows.forEach(function (row) {
            row.children('td.asf-selectedCell').removeClass('asf-selectedCell');
        });
    };

    this.selectOneView = function (view) {
        var cell = instance.getViewCoords(view);
        tableModel.selection.selectOne(cell);
    };

    this.selectOneCell = function (row, column) {
        tableModel.selection.selectOne([row, column]);
    };

    this.markSelection = function (selected, cell) {
        if (!cell) return;
        if (!(cell instanceof _jquery2.default)) cell = (0, _jquery2.default)(cell);
        var $cell = cell instanceof _jquery2.default ? cell : (0, _jquery2.default)(cell);
        if (selected) $cell.addClass('asf-selectedCell');else $cell.removeClass('asf-selectedCell');
    };

    this.markInvalid = function () {
        this.container.addClass('asf-invalidInput');
    };

    this.unmarkInvalid = function () {
        this.container.addClass('asf-invalidInput');
    };

    this.markSelected = this.markSelection.bind(this, true);
    this.unmarkSelected = this.markSelection.bind(this, false);

    this.clear = function () {
        var history = playerView.history;

        var hItems = _underscore2.default.map(instance.model.modelBlocks[0], function (model) {
            var cell = instance.model.findModelCell(model);

            return history.createDeleteItem((0, _utils.clone)(model.asfProperty), cell, instance.model.asfProperty.id);
        });

        var oldLayout = (0, _utils.clone)(instance.model.asfProperty.layout);
        instance.model.clear();

        hItems.push(history.layoutItem(oldLayout, (0, _utils.clone)(instance.model.asfProperty.layout), instance.model.asfProperty.id));
        history.addItems(hItems);
    };

    this.addRow = function (index) {
        playerView.history.addItem(playerView.history.createRowItem(index, instance.model.asfProperty.id));
        tableModel.addTableRow(index);
    };

    function modelsToDeleteHistoryItems(models) {
        var tProperty = instance.model.asfProperty;
        var history = playerView.history;

        return _underscore2.default.map(models, function (model) {
            var comp = layoutUtils.findById(model.asfProperty.id, tProperty.layout);
            return history.createDeleteItem(model.getProperty(), [comp.row, comp.column], tProperty.id);
        });
    }

    this.deleteRow = function (index) {
        if (!tableModel.canDeleteRow(index)) {
            return;
        }
        if (tableModel.asfProperty.layout.rows == 1 && tableModel.isPage()) {
            if (!confirm(_i18n2.default.tr("Вы действительно хотите удалить эту страницу?"))) {
                return;
            }
        } else {
            if (!confirm(_i18n2.default.tr("Вы уверены, что хотите удалить строку?"))) {
                return;
            }
            var models = tableModel.getRowModels(index);
            var hItems = modelsToDeleteHistoryItems(models);
            var history = playerView.history;

            hItems.push(history.deleteRowItem(index, instance.model.asfProperty.id));
            history.addItems(hItems);
        }

        tableModel.deleteTableRow(index);
    };

    this.addColumn = function (index) {
        playerView.history.addItem(playerView.history.createColumnItem(index, instance.model.asfProperty.id));
        tableModel.addTableColumn(index);
    };

    this.deleteColumn = function (index) {
        if (!tableModel.canDeleteColumn(index)) {
            return;
        }
        if (tableModel.asfProperty.layout.columns == 1 && tableModel.isPage()) {
            if (!confirm(_i18n2.default.tr("Вы действительно хотите удалить эту страницу?"))) {
                return;
            }
        } else {
            if (!confirm(_i18n2.default.tr("Вы уверены, что хотите удалить столбец?"))) {
                return;
            }

            var models = tableModel.getColumnModels(index);
            var hItems = modelsToDeleteHistoryItems(models);
            var history = playerView.history;

            hItems.push(history.deleteColumnItem(index, instance.model.asfProperty.id));
            history.addItems(hItems);
        }

        tableModel.deleteTableColumn(index);
    };

    this.showWidthSettings = function (columnIndex) {
        var layout = tableModel.asfProperty.layout;
        var width = styleUtils.getCellWidth(layout, columnIndex);
        if (!width) {
            width = "";
        }
        services.showWidthSettings(width, function (newWidth) {
            var history = playerView.history;
            history.addItem(history.columnWidthChange(width, newWidth, columnIndex, tableModel.asfProperty.id));
            layoutUtils.changeColumnWidth(columnIndex, newWidth, layout);

            instance.layoutChanged();
            instance.clearSelection();
        });
    };

    this.layoutChanged = function () {
        instance.viewBlock.updateDom();
        instance.drawTable();
    };

    this.insertComponentTo = function (model, row, column) {
        instance.viewBlock.insertComponentTo(model, row, column);
    };

    this.recalculateRulers = function () {

        var visible = instance.getTable().filter(":visible").length > 0;
        if (!visible) {
            return;
        }

        var curHeight = 0;
        var rows = instance.viewBlock.rows;
        var ruleri = 0;
        for (var rowi = 0; rowi < rows.length; rowi++) {
            var row = rows[rowi];
            var $td = row.children('td').first();
            var rowspan = Math.max($td.prop('rowspan'), 1) || 1;

            curHeight = 0;
            for (var spani = 0; spani < rowspan; spani++) {
                var ruler = rowRulers[ruleri];
                var height = _math2.default.max(60, rows[rowi + spani].height());

                ruler.div.css('height', height);
                ruler.div.css('top', curHeight);
                curHeight += height;

                ruleri++;
            }

            rowi += rowspan - 1;
        }

        ruleri = 0;
        var firstRow = rows[0].children('td');
        var curWidth = 0;

        var ths = instance.getTable().children('thead').children('th');

        for (var coli = 0; coli < firstRow.length; coli++) {
            $td = (0, _jquery2.default)(firstRow[coli]);
            var colspan = $td.prop('colspan') || 1;

            curWidth = 0;
            for (spani = 0; spani < colspan; spani++) {
                ruler = colRulers[ruleri];
                var width = _math2.default.max((0, _jquery2.default)(ths[coli + spani]).width(), 80) + 2;

                ruler.div.css('width', width);
                ruler.div.css('left', curWidth);
                curWidth += width;

                ruleri++;
            }

            var $th = (0, _jquery2.default)(instance.getTable().find("thead").first().find("th")[coli]);
            var thWidth = $th.width();
            if (thWidth > 0 && thWidth < 80) {
                $th.css('width', "80px");
            }
        }
    };

    this.createRulers = function () {

        if (selectTableButton != null) {
            selectTableButton.destroy();
        }

        if (clearPageButton != null) {
            clearPageButton.destroy();
        }

        var rows = instance.viewBlock.rows;
        rows.forEach(function (row, index) {
            row.css("height", "60px");
        });

        rowRulers.forEach(function (ruler) {
            ruler.destroy();
        });
        colRulers.forEach(function (ruler) {
            ruler.destroy();
        });
        rowRulers = [];
        colRulers = [];

        for (var rowi = 0; rowi < rows.length; rowi++) {
            var row = rows[rowi];
            var $td = row.children("td").first();
            var rowspan = $td.prop('rowspan') || 1;

            _underscore2.default.each(_underscore2.default.range(rowi, rowi + rowspan), function (ri) {
                var ruler = new _rowRuler2.default(tableModel, instance, ri);
                $td.append(ruler.div);
                rowRulers.push(ruler);
            });

            rowi += rowspan - 1;
        }

        var firstRow = rows[0].children('td');
        var celli = 0;
        for (var coli = 0; coli < firstRow.length; coli++) {
            $td = (0, _jquery2.default)(firstRow[coli]);

            var colspan = $td.prop('colspan') || 1;

            _underscore2.default.each(_underscore2.default.range(0, colspan), function () {
                var ruler = new _columnRuler2.default(tableModel, instance, celli++);

                $td.append(ruler.div);
                colRulers.push(ruler);
            });
        }

        var $thead = instance.getTable().children('thead');
        $thead.empty();
        for (coli = 0; coli < instance.model.asfProperty.layout.columns; coli++) {
            var $th = (0, _jquery2.default)('<th/>');
            styleUtils.applyCellStyle($th, coli, instance.model.asfProperty.layout);
            $thead.append($th);
        }

        var cell = (0, _jquery2.default)(firstRow[0]);
        selectTableButton = new _selectTableButton2.default(instance, cell);
        clearPageButton = new _clearTableButton2.default(instance, cell);

        instance.validateRulersVisibility();
    };

    this.validateRulersVisibility = function () {
        var visible = tableModel.isPage() || this.model.wasSelectedLast();
        if (visible) {
            rowRulers.forEach(function (ruler) {
                ruler.show();
            });
            colRulers.forEach(function (ruler) {
                ruler.show();
            });
            if (selectTableButton != null) {
                selectTableButton.show();
            }
        } else {
            rowRulers.forEach(function (ruler) {
                ruler.hide();
            });
            colRulers.forEach(function (r) {
                r.hide();
            });

            if (selectTableButton != null) {
                selectTableButton.hide();
            }
        }
    };

    this.initSelectionListeners = function () {
        instance.viewBlock.rows.forEach(function (row) {
            row.children("td").mousedown(function (evt) {
                if (evt.shiftKey) {
                    if (document.selection) {
                        document.selection.empty();
                    } else {
                        window.getSelection().removeAllRanges();
                    }
                }
            });
            row.children("td").click(function (evt) {
                if (evt.shiftKey) {
                    if (document.selection) {
                        document.selection.empty();
                    } else {
                        window.getSelection().removeAllRanges();
                    }
                }

                var td = this;
                var selection = instance.createSelectionObjectForCell((0, _jquery2.default)(td));
                selection.toggle = evt.ctrlKey;
                selection.area = evt.shiftKey;

                instance.model.selection.selectWithInfo(selection);
                domUtils.stopPropagation(evt);
            });
        });
    };

    this.createSelectionObjectForCell = function ($td) {
        var cell = instance.getTdCoord($td);
        var row = cell[0];
        var column = cell[1];
        var view = instance.viewBlock.getViewByTd($td);
        return new _selectionInfo2.default(tableModel.asfProperty.id, row, column, view);
    };

    this.unmarkInvalid = function () {
        instance.container.removeClass('asf-invalidInput');
        instance.getAllViews().forEach(function (view) {
            view.unmarkInvalid();
        });
    };

    this.destroy = function () {
        playerView.model.off(_constants.EVENT_TYPE.selectSingleCell, onSelectSingleCell);

        playerView.model.off(_constants.EVENT_TYPE.layoutChanged, onLayoutChanged);

        playerView.model.off(_constants.EVENT_TYPE.cellSizeChange, onLayoutChanged);
    };

    instance.container.css("position", "relative");

    tableModel.on(_constants.EVENT_TYPE.modelDeleted, function (evt, model) {
        instance.viewBlock.clearComponent(model);
        instance.recalculateRulers();
    });

    tableModel.on(_constants.EVENT_TYPE.modelInserted, function (evt, model, row, column) {
        instance.viewBlock.insertComponentTo(model);

        if (!model.asfProperty.fromHistory) if (model.asfProperty.type === _constants.WIDGET_TYPE.table) {
            model.selection.selectOne([0, 0]);
        } else instance.model.selection.selectOne([row, column]);
        instance.recalculateRulers();
    });

    instance.getTable().css("border", "1px solid rgb(200, 226, 255)");
    instance.getTable().attr("border", "1");

    instance.createRulers();
    instance.initSelectionListeners();

    tableUtils.maybeChangeWidth(instance.getTable(), tableModel.asfProperty, 80);

    tableModel.playerModel.trigger(_constants.EVENT_TYPE.layoutChanged, tableModel);
};

TableBuilderView.prototype = Object.create(_tableStaticView2.default.prototype);
TableBuilderView.prototype.constructor = TableBuilderView;

/***/ }),

/***/ 161:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _backbone = __webpack_require__(43);

var _backbone2 = _interopRequireDefault(_backbone);

var _underscore = __webpack_require__(7);

var _underscore2 = _interopRequireDefault(_underscore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Компонент для выборки, которое имеет кнопку для выбора и сам текст
 * @type {void|*}
 */
exports.default = _backbone2.default.ItemView.extend({
    ui: {
        chooserForm: '.chooser',
        chooserButton: 'button',
        chooserValueControl: '.chooser-value-control'
    },
    events: {
        'click @ui.chooserButton': 'chooseButtonClicked'
    },

    setSelectedModel: function setSelectedModel(model) {
        this.model = model;
        this.setValue(model.getValue());
    },
    getSelectedModel: function getSelectedModel() {
        return this.model;
    },
    /**
     * Делает компонент доступным/недоступным
     * @param enabled булевое значение
     */
    setEnabled: function setEnabled(enabled) {
        if (enabled) {
            this.ui.chooserForm.removeClass('disabled');
            this.ui.chooserButton.attr('disabled', !enabled);
        } else {
            this.ui.chooserForm.addClass('disabled');
            this.ui.chooserButton.attr('disabled', !enabled);
        }
    },

    /**
     * Применяет css стил�� ошибки к компоненту
     * @param errored
     */
    setErrored: function setErrored(errored) {
        if (errored) {
            this.ui.chooserForm.addClass('has-error');
        } else {
            this.ui.chooserForm.removeClass('has-error');
        }
    },

    /**
     * Изменяет текстовое значение компонента
     * @param textValue
     */
    setValue: function setValue(textValue) {
        this.model.setValue(textValue);
        this.render();
    },

    /**
     * Метод срабатывает при нажатии на кнопку выборки.
     * Не будет работать если компонент недоступный для редактирования
     * Сообщает всем слушателям что кнопка была нажата
     * @param event
     */
    chooseButtonClicked: function chooseButtonClicked(event) {
        this.triggerMethod('chooser:button:clicked');
    },

    markInvalid: function markInvalid() {
        this.ui.chooserForm.addClass('asf-invalidInput');
    },

    getTemplate: function getTemplate() {
        return _underscore2.default.template('<div class="chooser">' + '<span class="chooser-value-control">' + '<%= value %>' + '</span>' + '<button type="button" class="btn">.</button>' + '</div>');
    }
});

/***/ }),

/***/ 162:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function($) {

Object.defineProperty(exports, "__esModule", {
    value: true
});

var _backbone = __webpack_require__(43);

var _backbone2 = _interopRequireDefault(_backbone);

var _underscore = __webpack_require__(7);

var _underscore2 = _interopRequireDefault(_underscore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Компонент пагинатор
 * @type {void|*}
 */
exports.default = _backbone2.default.ItemView.extend({
    getTemplate: function getTemplate() {
        return _underscore2.default.template('<div class="paginator">' + '<button disabled class="previous"></button>' + '<div class="paginator-content">' + '<label><%= currentPage%> / <%= totalPage%></label>' + '<input type="text" title="aaa" value="<%= currentPage%>">' + '</div>' + '<button class="next"></button>' + '</div>');
    },
    ui: {
        prevButton: 'button.previous',
        nextButton: 'button.next',
        content: 'label',
        inputField: 'input'
    },
    events: {
        'click @ui.prevButton': 'previousPage',
        'click @ui.nextButton': 'nextPage',
        'click @ui.content': 'onContentAreaClicked',
        'keypress @ui.inputField': 'onKeyPressed',
        'blur @ui.inputField': 'onFocusOut'
    },
    modelEvents: {
        'change': 'render'
    },

    /**
     * Метод сообщает что текущая страница была изменена всем слушателям
     */
    triggerChangeEvent: function triggerChangeEvent() {
        this.triggerMethod('paginator:page:changed', this.getCurrentPage());
        this.trigger('pageChanged', this.getCurrentPage());
    },
    /**
     * Вызывается при нажатии на кнопку для перехода на след страницу в пагинаторе
     * @param event событие нажатия
     */
    nextPage: function nextPage(event) {
        var pageChanged = this.model.nextPage();
        if (pageChanged) {
            this.triggerChangeEvent();
        }
    },

    /**
     * Вызывается при нажатии на кнопку для перехода на пред. страницу в пагинаторе
     * @param event событие нажатия
     */
    previousPage: function previousPage(event) {
        var pageChanged = this.model.prevPage();
        if (pageChanged) {
            this.triggerChangeEvent();
        }
    },

    /**
     * Срабатывает при нажатии на поле с данными
     * @param event событие
     */
    onContentAreaClicked: function onContentAreaClicked(event) {
        this.ui.content.parent().addClass('editing');
        this.ui.inputField.select();
    },

    /**
     * Метод вызывается при печати в текстовое поле
     * @param event
     */
    onKeyPressed: function onKeyPressed(event) {
        var ENTER_KEY = 13;
        if (event.which == ENTER_KEY) {
            var enteredPage = this.ui.inputField.val().trim();
            if (Math.floor(enteredPage) == enteredPage && $.isNumeric(enteredPage)) {
                var changed = this.model.setCurrentPage(parseInt(enteredPage, 10));
                if (changed) {
                    this.triggerChangeEvent();
                }
            }

            this.onFocusOut();
        }
    },

    /**
     * При потере фокуса срабатывает этот метод.
     * Он убирает css класс `editing`
     * @param event
     */
    onFocusOut: function onFocusOut(event) {
        this.ui.content.parent().removeClass('editing');
        this.ui.inputField.val(this.getCurrentPage());
    },

    /**
     * При отрисовке проверяем
     */
    onRender: function onRender() {
        this.ui.nextButton.attr('disabled', this.getCurrentPage() >= this.getTotalPage());
        this.ui.prevButton.attr('disabled', this.getCurrentPage() <= 1);
    },

    /**
     * Возвращает номер текущей страницы
     * @returns номер текущей страницы
     */
    getCurrentPage: function getCurrentPage() {
        return this.model.getCurrentPage();
    },

    /**
     * Указывает номер текущей страницы
     * @param currentPage номер текущей страницы
     * @returns {*}
     */
    setCurrentPage: function setCurrentPage(currentPage) {
        return this.model.setCurrentPage(currentPage);
    },

    /**
     * возвращает количество страниц
     * @returns {*}
     */
    getTotalPage: function getTotalPage() {
        return this.model.getTotalPage();
    },

    /**
     * Указываем количество страниц
     * @param totalPage количество страниц
     * @returns {*}
     */
    setTotalPage: function setTotalPage(totalPage) {
        return this.model.setTotalPage(totalPage);
    }
});
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ }),

/***/ 163:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _backbone = __webpack_require__(39);

var _backbone2 = _interopRequireDefault(_backbone);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ChooserModel = _backbone2.default.Model.extend({
    defaults: {
        'value': '',
        'isSelected': false
    },
    getValue: function getValue() {
        return this.get('value');
    },
    setValue: function setValue(value) {
        this.set('value', value);
    }
});
exports.default = ChooserModel;

/***/ }),

/***/ 164:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _backbone = __webpack_require__(39);

var _backbone2 = _interopRequireDefault(_backbone);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var PaginatorModel = _backbone2.default.Model.extend({
    defaults: {
        'currentPage': 1,
        'totalPage': 1
    },

    nextPage: function nextPage() {
        if (this.getCurrentPage() + 1 <= this.getTotalPage()) {
            this.setCurrentPage(this.getCurrentPage() + 1);
            return true;
        }
        return false;
    },

    prevPage: function prevPage() {
        if (this.getCurrentPage() - 1 >= 1) {
            this.setCurrentPage(this.getCurrentPage() - 1);
            return true;
        }
        return false;
    },

    setCurrentPage: function setCurrentPage(currentPage) {
        if (currentPage <= this.getTotalPage() && currentPage >= 1) {
            this.set('currentPage', currentPage);
            return true;
        }
        return false;
    },

    getCurrentPage: function getCurrentPage() {
        return this.get('currentPage');
    },

    getTotalPage: function getTotalPage() {
        return this.get('totalPage');
    },

    setTotalPage: function setTotalPage(totalPage) {
        this.set('totalPage', totalPage);
    }

});

exports.default = PaginatorModel;

/***/ }),

/***/ 165:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.SelectCollection = exports.SelectModel = undefined;

var _backbone = __webpack_require__(39);

var _backbone2 = _interopRequireDefault(_backbone);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Модель для компонента выборки
 * @type {void|*}
 */
var SelectModel = exports.SelectModel = _backbone2.default.Model.extend({
    defaults: {
        'selected': false,
        'key': '',
        'value': ''
    }
});

/**
 * Коллекция для компонента выборки
 * @type {void|*}
 */
var SelectCollection = exports.SelectCollection = _backbone2.default.Collection.extend({
    model: SelectModel,
    initialize: function initialize(models, options) {
        var self = this;
        this.on('add', function (selectModel) {
            var foundModel = self.findWhere({ selected: true });
            !_.isUndefined(foundModel) && foundModel != selectModel && foundModel.set('selected', false);
        });
    }
});

/***/ }),

/***/ 166:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Organizations = exports.Organization = exports.Users = exports.User = undefined;

var _underscore = __webpack_require__(7);

var _underscore2 = _interopRequireDefault(_underscore);

var _backbone = __webpack_require__(39);

var _backbone2 = _interopRequireDefault(_backbone);

var _chooser = __webpack_require__(163);

var _chooser2 = _interopRequireDefault(_chooser);

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Модель который хранит информацию о пользователе
 * @type {void|*}
 */
var User = exports.User = _chooser2.default.extend({
    defaults: {
        'firstName': '',
        'middleName': '',
        'lastName': '',
        'organization': ''
    },
    initialize: function initialize(options) {
        if (!_underscore2.default.isEmpty(options) && !_underscore2.default.isEmpty(options.workPlaces)) {
            this.set('organization', options.workPlaces[0].organizationName);
        }
    },
    getFirstName: function getFirstName() {
        return this.get('firstName');
    },
    getPatronymic: function getPatronymic() {
        return this.get('patronymic');
    },
    getLastName: function getLastName() {
        return this.get('lastName');
    },
    getOrganization: function getOrganization() {
        return this.get('organization');
    },

    getValue: function getValue() {
        var value = '';
        if (!_underscore2.default.isEmpty(this.getLastName())) {
            value = value + this.getLastName() + ' ';
        }
        if (!_underscore2.default.isEmpty(this.getFirstName())) {
            value = value + this.getFirstName() + ' ';
        }
        if (!_underscore2.default.isEmpty(this.getPatronymic())) {
            value = value + this.getPatronymic() + ' ';
        }
        if (!_underscore2.default.isEmpty(this.getOrganization())) {
            value = value + '(' + this.getOrganization() + ')';
        }
        return value;
    },

    getType: function getType() {
        if (this.get("itemType") == 'PERSON') {
            return "0";
        } else {
            return "1";
        }
    },
    getItemId: function getItemId() {
        return this.get('itemID');
    },
    getUUID: function getUUID() {
        return '0:' + this.get('itemID');
    }
});

/**
 * Коллекция пользователей, которая содержит в себе логику подтягивания данных из рест сервиса.
 * @type {void|*}
 */
var Users = exports.Users = _backbone2.default.Collection.extend({
    model: User,
    searchQuery: '',
    page: 1,
    totalPage: 2,
    /**
     * URL для подтягивания будет меняться динамический, в зависимости от переданных параметров
     * @returns {string}
     */
    url: function url() {
        return '/Synergy/rest/api/addressbook/items?' + _jquery2.default.param({ page: this.page - 1, search: this.searchQuery });
    },
    search: function search(searchQuery, callback) {
        this.searchQuery = searchQuery;
        this.fetch({ reset: true, success: callback });
    },
    fetchPage: function fetchPage(page, searchQuery, callback) {
        this.page = page || this.page;
        this.searchQuery = searchQuery || '';
        this.fetch({ reset: true, success: callback });
    },
    parse: function parse(response) {
        this.page = response.begin + 1;
        this.totalPage = parseInt(response.count);
        if (this.totalPage == 0) {
            this.totalPage = 1;
        }
        return response.array;
    }
});

/**
 * Модель который хранит информацию об организации
 * @type {void|*}
 */
var Organization = exports.Organization = _chooser2.default.extend({
    defaults: {
        'name': '',
        'address': ''
    },
    initialize: function initialize(options) {
        if (!_underscore2.default.isEmpty(options) && !_underscore2.default.isEmpty(options.itemsGroup)) {
            var addresses = _underscore2.default.findWhere(options.itemsGroup, { categoryType: 'ADDRESS' });
            if (!_underscore2.default.isUndefined(addresses.items) && _underscore2.default.has(addresses.items[0], 'contact')) {
                this.set('address', addresses.items[0].contact);
            }
        }
    },
    getOrganization: function getOrganization() {
        return this.get('name');
    },

    getAddress: function getAddress() {
        return this.get('address');
    },
    getValue: function getValue() {
        var value = '';
        if (!_underscore2.default.isEmpty(this.getOrganization())) {
            value = value + this.getOrganization() + ' ';
        }
        if (!_underscore2.default.isEmpty(this.getAddress())) {
            value = value + '(' + this.getAddress() + ')';
        }
        return value;
    },
    getUUID: function getUUID() {
        return '1:' + this.get('itemID');
    },
    getType: function getType() {
        if (this.get("itemType") == 'PERSON') {
            return "0";
        } else {
            return "1";
        }
    },
    getItemId: function getItemId() {
        return this.get('itemID');
    }
});

/**
 * Коллекция моделей организации
 * @type {void|*}
 */
var Organizations = exports.Organizations = Users.extend({
    model: Organization,
    url: function url() {
        return '/Synergy/rest/api/addressbook/items?' + _jquery2.default.param({
            page: this.page - 1,
            search: this.searchQuery,
            itemType: 1
        });
    }
});

/***/ }),

/***/ 2:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
  value: true
});
// перечис��ение типов событий
var EVENT_TYPE = exports.EVENT_TYPE = {

  /**
   * отображение формы (строят новую форму)
   */
  formShow: 'formShow',

  /**
   *  закрытие формы
   */
  formDestroy: "formDestroy",

  /**
   * подгрузка данных
   */
  dataLoad: 'dataLoad',
  /**
   * изменение значение модели
   */
  valueChange: 'valueChange',

  /**
   * модель говорит о необходимости отображению подсветить невалидное значение
   */
  markInvalid: 'markInvalid',

  unmarkInvalid: 'unmarkInvalid',
  /**
   * событие внутреннее для добавление ряда
   */
  tableRowAddInternal: "tableRowAddInternal",

  /**
   * добавление ряда в таблице
   */
  tableRowAdd: 'tableRowAdd',

  /**
   * Добавлен ряд в таблице
   */
  tableRowAdded: 'tableRowAdded',
  /**
   * удаление ряда в таблице
   */
  tableRowDelete: 'tableRowDelete',

  /**
   * подгрузка справочника
   */
  dictionaryLoad: 'dictionaryLoad',

  /**
   * редактирвоание тага
   */
  tagEdit: 'tagEdit',
  /**
   * удаление тага
   */
  tagDelete: 'tagDelete',
  /**
   * событие подгрузки дополниельнйо информации компонента (сейчас используется для пользовательского компонента)
   */
  loadComponentAdditionalInfo: 'loadComponentAdditionalInfo',

  /**
   * щелчок по ячейке таблицы
   */
  componentClicked: 'componentClicked',

  playerAttached: 'playerAttached',

  modelInserted: 'modelInserted',

  modelDeleted: 'modelDeleted',

  layoutChanged: 'layoutChanged',

  selectionChange: 'tableSelectionChange',

  selectSingleCell: 'tableSelectSingleCell',

  tableDomUpdate: 'tableDomUpdate',

  tableDomUpdated: 'tableDomUpdated',

  /**
   * событие удаления страницы (в качестве доп параметров передается модель страницы)
   */
  pageDeleted: 'pageDeleted',

  cellSizeChange: 'cellSizeChange',

  modelDestroyed: 'modelDestroyed',

  historyChange: 'historyChange'
};

/**
 * типы представлений
 */
var FORM_VIEW_TYPE = exports.FORM_VIEW_TYPE = {
  common: 0,
  mobile: 1,
  print: 2
};

// See kz.arta.synergy.forms.common.object.WidgetType
//перечисление типов компонентов
var WIDGET_TYPE = exports.WIDGET_TYPE = {
  label: 'label',
  textbox: 'textbox',
  textarea: 'textarea',
  listbox: 'listbox',
  radio: 'radio',

  check: 'check',
  date: 'date',
  table: 'table',
  entity: 'entity',
  image: 'image',
  file: 'file',
  filelink: 'filelink',
  userchooser: 'userchooser',
  counter: 'counter',
  signlist: 'signlist',
  resolutionlist: 'resolutionlist',
  htd: 'htd',
  doclink: 'doclink',
  link: 'link',
  processlist: 'processlist',
  repeater: 'repeater',
  reglink: 'reglink',
  projectlink: 'projectlink',
  personlink: 'personlink',
  appendable_table: 'appendable_table',
  users: 'users',
  departments: 'departments',
  positions: 'positions',
  docnumber: 'docnumber',
  numericinput: 'numericinput',
  errorlabel: 'errorlabel',
  page: 'page',
  custom: 'custom'
};

var ENTITY_TYPE = exports.ENTITY_TYPE = Object.freeze({
  users: 'users',
  departments: 'departments',
  positions: 'positions'
});

/**
 * перечисление типов ошибок пользовательского ввода
 */
var INPUT_ERROR_TYPE = exports.INPUT_ERROR_TYPE = Object.freeze({
  emptyValue: 'emptyValue',
  wrongValue: 'wrongValue',
  deletedValue: 'deletedValue',
  valueTooHigh: 'valueTooHigh',
  valueTooSmall: 'valueTooSmall'
});

var BUILDER_ERROR_TYPE = exports.BUILDER_ERROR_TYPE = Object.freeze({
  sameIds: 'emptyValue',
  emptyTable: 'emptyTable',
  emptyPage: 'emptyPage'
});

/**
 * перечисление кодов клавиш (вынести в отдельное место и вообще сделать  натсраиваемым и зависмым от )
 */
var KEY_CODES = exports.KEY_CODES = Object.freeze({
  backspace: 8,
  delete: 46,
  insert: 45,
  left: 37,
  up: 38,
  right: 39,
  down: 40,
  home: 36,
  end: 35,
  pageUp: 33,
  pageDown: 34,
  tab: 9,
  MINUS: 189,
  DOT: 190,
  COMMA: 188,
  ENTER: 13,
  escape: 27,
  Z_KEY: 90
});

/**
 * перечисление генерируемых событий
 */
var DECIMAL_DELIMITER = exports.DECIMAL_DELIMITER = {
  COMMA: 'COMMA',
  DOT: 'DOT',
  EMPTY: 'EMPTY'
};

/**
 * перечисление частей формата даты
 */
var DATE_FORMAT_PARTS = exports.DATE_FORMAT_PARTS = {
  yyyy: 'yyyy',
  yy: 'yy',
  mm: 'mm',
  m: 'm',
  month: 'month',
  month_short: 'month_short',
  monthed: 'monthed',
  monthec: 'monthec',
  dd: 'dd',
  d: 'd',
  hh: 'hh',
  h: 'h',
  HH: 'HH',
  H: 'H',
  MM: 'MM',
  M: 'M',
  SS: 'SS',
  S: 'S'
};

/**
 * перечисление частей формата имени пользователя
 */
var NAME_FORMAT_PARTS = exports.NAME_FORMAT_PARTS = Object.freeze({
  l: 'l',
  l_short: 'l.short',
  f: 'f',
  f_short: 'f.short',
  f_short_dot: 'f.short.dot',
  p: 'p',
  p_short: 'p.short',
  p_short_dot: 'p.short.dot'
});

/**
 * перечисление типов полей ркк документа (для компонента свойство документа)
 */
var DOC_ATTRIBUTE_FIELD = exports.DOC_ATTRIBUTE_FIELD = {
  number: 'number',
  subject: 'subject',
  createDate: 'createDate',
  author: 'author',
  reg_date: 'reg_date',
  doc_type: 'doc_type',
  registry: 'registry'
};

/**
 * перечисление типов полей ркк документа (для компонента свойство документа)
 */
var REPEAT_PERIOD_TYPE = exports.REPEAT_PERIOD_TYPE = {
  none: 0,
  week_days: 1,
  month_days: 2,
  year_days: 4
};

/**
 * настройки приложения
 * @type {{coreUrl: string, login: string, password: string, currentUser: {userId: string, lastname: string, firstname: string, patronymic: string}, locale: string, options: {}}}
 */
var OPTIONS = exports.OPTIONS = {
  coreUrl: "http://127.0.0.1:8080/Synergy/",
  login: '',
  password: '',
  currentUser: {
    userId: "1",
    lastname: "Lastname",
    firstname: "Firstname",
    patronymic: "Patronymic",
    positions: {},
    sessionID: ""
  },
  locale: "ru",
  options: {},
  requestTimeout: 5000,
  noCustomScripting: false,
  mobilePlayer: false
};

if (window.mobilePlayer) OPTIONS.mobilePlayer = true;

var OBJECT_TYPES = exports.OBJECT_TYPES = {
  PLAN: 256,
  PORTFOLIO: 128
};

var COMPONENT_EVENTS = exports.COMPONENT_EVENTS = {
  resized: 'resized'
};

var TABLE_EVENT_TYPE = exports.TABLE_EVENT_TYPE = {
  ENABLED: 'ENABLED',
  DISABLED: 'DISABLED',
  COLUMN_SHOW: 'COLUMN_SHOW',
  COLUMN_HIDE: 'COLUMN_HIDE',
  BLOCK_SHOW: 'BLOCK_SHOW',
  BLOCK_HIDE: 'BLOCK_HIDE',
  BLOCK_DELETED: 'BLOCK_DELETED',
  MERGE_CELL: 'MERGE_CELL',
  SPLIT_CELL: 'SPLIT_CELL'
};

var CMP_DOM_ATTR = exports.CMP_DOM_ATTR = "data-asformid";
var CMP_DOM_FORM_ID = exports.CMP_DOM_FORM_ID = "data-asformuuid";
var CMP_DOM_DATA_ID = exports.CMP_DOM_DATA_ID = "data-asformdatauuid";

/***/ }),

/***/ 21:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.applyTableStyle = applyTableStyle;
exports.applyLabelStyle = applyLabelStyle;
exports.applyColorStyle = applyColorStyle;
exports.applyCellStyle = applyCellStyle;
exports.getCellWidthParsed = getCellWidthParsed;
exports.getCellWidth = getCellWidth;
exports.parseWidth = parseWidth;
exports.applyPositionStyle = applyPositionStyle;
exports.applySize = applySize;
exports.applyImageSize = applyImageSize;
exports.applyFontStyle = applyFontStyle;
exports.applyTextAlign = applyTextAlign;
exports.applyBorder = applyBorder;
exports.getSize = getSize;

var _layoutUtils = __webpack_require__(65);

var layoutUtils = _interopRequireWildcard(_layoutUtils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

/**
 * утилита для применения стилей
 * @type {{}}
 */

function applyTableStyle(table, style) {
    if (style) {
        applyTextAlign(table, style);
        applyFontStyle(table, style);
    }
}

function applyLabelStyle(label, style, withoutSize) {
    if (style) {
        applyTextAlign(label, style);
        applyFontStyle(label, style);
        if (!withoutSize) {
            applySize(label, style);
        }
    }
}

function applyColorStyle(container, style) {
    if (!style) {
        return;
    }
    if (style.background) {
        container.css('background-color', style.background);
    }
}

function applyCellStyle(cell, column, asfLayout, fixed) {
    if (column == null || !asfLayout) {
        return;
    }
    var width = getCellWidthParsed(asfLayout, column);
    if (width) {
        cell.css('width', width.str);
        if (fixed && width.unit !== '%') cell.css('max-width', width.str);
    }
}

function getCellWidthParsed(asfLayout, column) {
    if (!asfLayout || !asfLayout.config) {
        return null;
    }
    var c = _.find(asfLayout.config, layoutUtils.eqCol(column));

    if (!c) {
        return null;
    }
    return parseWidth(c.width);
}

function getCellWidth(asfLayout, column) {
    var width = getCellWidthParsed(asfLayout, column);
    return width ? width.str : null;
}

function parseWidth(width) {
    if (!width) return null;
    var length = width.length;
    if (length === 0) return null;

    var numberStr;
    var unit;
    if (width[length - 1] === '%') {
        numberStr = width.substring(0, length - 1);
        unit = '%';
    } else {
        numberStr = width;
        unit = 'px';
    }

    var number = parseInt(numberStr);
    if (isNaN(number)) return null;
    return {
        number: number,
        unit: unit,
        str: number + unit
    };
}

function applyPositionStyle(container, style, initialWidth) {
    if (style) {
        var width = initialWidth;
        if (!width) {
            width = style.width;
        }

        container.css('width', '');
        container.css('margin', '');
        container.css('margin-left', '');

        if (style.align && width) {
            container.css("width", getSize(width));
            if (style.align == 'right') {
                container.css('margin-left', 'auto');
            } else if (style.align == 'center') {
                container.css('margin', 'auto');
            }
        }
    }
}

function applySize(widget, style) {
    if (style) {
        if (style.width) {
            widget.css('max-width', getSize(style.width));
        } else {
            widget.css('max-width', "");
        }
        if (style.height) {
            widget.css('min-height', getSize(style.height));
        } else {
            widget.css('min-height', "");
        }
    }
}

function applyImageSize(widget, style) {
    if (style) {
        if (style.width) {
            widget.css('max-width', getSize(style.width));
        }
        if (style.height) {
            widget.css('max-height', getSize(style.height));
        }
        if (style.align) {
            widget.css("display", "block");
            if (style.align == 'right') {
                widget.css('margin-left', 'auto');
            } else if (style.align == 'center') {
                widget.css('margin', 'auto');
            }
        }
    }
}

function applyFontStyle(widget, style) {
    if (!widget || !style) {
        return;
    }
    if (style.font) {
        widget.css('font-family', style.font + ", sans-serif");
    }
    if (style.fontsize) {
        widget.css('font-size', style.fontsize + "px");
    } else {
        widget.css('font-size', '');
    }

    if (style.bold) {
        widget.css('font-weight', 'bold');
    } else {
        widget.css('font-weight', '');
    }

    if (style.italic) {
        widget.css('font-style', 'italic');
    } else {
        widget.css('font-style', '');
    }

    if (style.strike || style.underline) {
        var textDecoration = '';
        if (style.strike) {
            textDecoration = "line-through ";
        }
        if (style.underline) {
            textDecoration += "underline";
        }
        widget.css('text-decoration', textDecoration);
    } else {
        widget.css('text-decoration', "");
    }
}

function applyTextAlign(widget, style) {
    if (style && style.align) {
        widget.css('text-align', style.align);
    } else {
        widget.css('text-align', "");
    }
}

function applyBorder(cell, style) {
    if (style && style.border) {
        cell.addClass('asf-borderedCell');
    }
}

function getSize(value) {
    if (value && value.toString().match(/^\d+$/)) {
        return value + "px";
    } else {
        return value;
    }
}

/***/ }),

/***/ 213:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = AddressBookTable;

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

var _i18n = __webpack_require__(14);

var _i18n2 = _interopRequireDefault(_i18n);

var _ExtendedTable = __webpack_require__(113);

var _ExtendedTable2 = _interopRequireDefault(_ExtendedTable);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function AddressBookTable() {
    var humanColumns = [{ id: "lastName", name: _i18n2.default.tr("Фамилия"), width: 150 }, { id: "firstName", name: _i18n2.default.tr("Имя"), width: 150 }, { id: "patronymic", name: _i18n2.default.tr("Отчество"), width: 150 }, { id: "organization", name: _i18n2.default.tr("Организация"), width: 330 }];
    var orgColumns = [{ id: "name", name: _i18n2.default.tr("Организация"), width: 389 }, { id: "address", name: _i18n2.default.tr("Адрес"), width: 389 }];

    var pagesCount = 0;
    var currentPage = 0;
    var search = null;
    var countInPage = 15;
    var itemType = 0;

    var instance = this;

    var table = new _ExtendedTable2.default();

    table.init({}, humanColumns);

    this.dblclick = function (handler) {
        table.getWidget().dblclick(handler);
    };

    this.showHuman = function () {
        table.init({}, humanColumns);
        itemType = 0;
        instance.loadData(0);
    };

    this.showOrganization = function () {
        table.init({}, orgColumns);
        itemType = 1;
        instance.loadData(0);
    };

    table.on(_ExtendedTable.ExtendedTableEvent.columnSortClicked, function (event, columnIndex, sortAsc, columnOption) {
        instance.sort(columnOption.id, sortAsc);
    });

    this.search = function (newSearch) {
        search = newSearch;
        this.loadData(0);
    };

    this.loadData = function (pageNumber) {
        currentPage = pageNumber;
        api.getAddressBookData(itemType, search, pageNumber, function (data) {

            pagesCount = data.count;

            data.array.forEach(function (item) {
                item.itemType = itemType;
                if (itemType === 0) {
                    if (item.workPlaces && item.workPlaces.length > 0) {
                        item.organization = item.workPlaces[0].organizationName;
                    }
                    item.name = item.lastName + " " + item.firstName + " " + item.patronymic;
                } else {
                    var address = [];
                    if (item.itemsGroup) {
                        item.itemsGroup.some(function (itemGroup) {
                            if (itemGroup.categoryID === 'address_org') {
                                itemGroup.items.forEach(function (item) {
                                    address.push(item.contact);
                                });
                            }
                        });
                    }
                    item.address = address.arrayToString(", ");
                }
            });
            table.setData(data.array);
        });
    };

    this.getSelectedData = function () {
        return table.getSelectedData();
    };

    this.getCurrentPage = function () {
        return currentPage;
    };

    this.getRecordsCount = function () {
        return recordsCount;
    };

    this.getPagesCount = function () {
        return pagesCount;
    };

    this.getSelectedData = function () {
        return table.getSelectedData();
    };

    this.loadData(0);

    this.on = function (event, handler) {
        table.on(event, handler);
    };

    this.getWidget = function () {
        return table.getWidget();
    };
};

/***/ }),

/***/ 214:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = BuilderToolBar;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _Button = __webpack_require__(215);

var _Button2 = _interopRequireDefault(_Button);

var _ComboBox = __webpack_require__(216);

var _ComboBox2 = _interopRequireDefault(_ComboBox);

var _constants = __webpack_require__(2);

var _tableUtils = __webpack_require__(52);

var tableUtils = _interopRequireWildcard(_tableUtils);

var _builderUtils = __webpack_require__(156);

var builderUtils = _interopRequireWildcard(_builderUtils);

var _services = __webpack_require__(23);

var services = _interopRequireWildcard(_services);

var _domUtils = __webpack_require__(44);

var domUtils = _interopRequireWildcard(_domUtils);

var _utils = __webpack_require__(82);

var utils = _interopRequireWildcard(_utils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function BuilderToolBar(playerView) {

    var toolBar = (0, _jquery2.default)("<div>", { class: "asf-building-toolBar" });

    var firstLine = (0, _jquery2.default)("<div>", { class: "asf-building-toolbarLine" });
    var secondLine = (0, _jquery2.default)("<div>", { class: "asf-building-toolbarLine" });
    var thirdLine = (0, _jquery2.default)("<div>", { class: "asf-building-toolbarLine" });

    var idInput = (0, _jquery2.default)('<input/>', {
        type: 'text',
        class: 'asf-building-textBox asf-builderMargin',
        style: 'width:200px',
        'data-id': "toolbarIdInput"
    });

    var settingsButton = new _Button2.default(null, "asf-icon-button asf-builderMargin asf-building-settingsButton", false, true, true);
    settingsButton.getElement().prop("title", i18n.tr("Настройки"));

    var selectTableButton = new _Button2.default(null, "asf-icon-button asf-builderMargin asf-building-selectTableButton", false, true, true);
    selectTableButton.getElement().prop("title", i18n.tr("Настройки"));

    var addToTableButton = new _Button2.default(null, "asf-icon-button asf-builderMargin asf-building-addTableButton", false, true, true);
    addToTableButton.getElement().prop("title", i18n.tr("Добавить в таблицу ..."));
    addToTableButton.getElement().css("width", "40px");

    var clearTableButton = new _Button2.default(null, "asf-icon-button asf-builderMargin asf-building-clearTableButton", false, true, true);
    clearTableButton.getElement().prop("title", i18n.tr("Очистить таблицу"));

    var deleteButton = new _Button2.default(null, "asf-icon-button asf-builderMargin asf-building-deleteButton", false, true, true);
    deleteButton.getElement().prop("title", i18n.tr("Удалить"));

    var fontFamilyList = new _ComboBox2.default("asf-builder-dropdownInput", "asf-builder-dropdownButton");
    fontFamilyList.getElement().addClass("asf-builderMargin");
    fontFamilyList.setValues([{ value: "Arial", title: "Arial" }, { value: "Courier New", title: "Courier New" }, { value: "Tahoma", title: "Tahoma" }, { value: "Times New Roman", title: "Times New Roman" }, { value: "Trebuchet", title: "Trebuchet" }, { value: "Verdana", title: "Verdana" }]);
    fontFamilyList.getElement().css("width", "135px");

    var fontSizeInput = (0, _jquery2.default)('<input/>', {
        type: 'text',
        class: 'asf-building-textBox asf-builderMargin',
        style: 'width:61px',
        'data-id': "toolbarFontSizeInput"
    });

    var boldButton = new _Button2.default(null, "asf-icon-button asf-building-boldButton", true, true, false);
    boldButton.getElement().prop("title", i18n.tr("Полужирный"));

    var italicButton = new _Button2.default(null, "asf-icon-button asf-building-italicButton", true, true, false);
    italicButton.getElement().prop("title", i18n.tr("Курсив"));

    var underlineButton = new _Button2.default(null, "asf-icon-button asf-building-underlineButton", true, true, false);
    underlineButton.getElement().prop("title", i18n.tr("Подчеркнутый"));

    var lineButton = new _Button2.default(null, "asf-icon-button asf-builderMargin asf-building-lineThroughButton", true, true, true);
    lineButton.getElement().prop("title", i18n.tr("Зачеркнутый"));

    var widthIcon = (0, _jquery2.default)("<div/>", { class: "asf-building-widthIcon" });
    widthIcon.prop("title", i18n.tr("Ширина"));
    var widthInput = (0, _jquery2.default)('<input/>', {
        type: 'text',
        class: 'asf-building-textBox asf-builderMargin',
        style: 'width:60px',
        'data-id': "toolbarWidthInput"
    });
    widthInput.prop("title", i18n.tr("Ширина"));

    var heightIcon = (0, _jquery2.default)("<div/>", { class: "asf-building-heightIcon" });
    heightIcon.prop("title", i18n.tr("Высота"));
    var heightInput = (0, _jquery2.default)('<input/>', {
        type: 'text',
        class: 'asf-building-textBox asf-builderMargin',
        style: 'width:60px',
        'data-id': "toolbarHeightInput"
    });
    heightInput.prop("title", i18n.tr("Высота"));

    var leftAlignButton = new _Button2.default(null, "asf-icon-button asf-building-alignLeftButton", true, true, false);
    leftAlignButton.getElement().prop("title", i18n.tr("По левому краю"));

    var centerAlignButton = new _Button2.default(null, "asf-icon-button asf-building-alignCenterButton", true, true, false);
    centerAlignButton.getElement().prop("title", i18n.tr("По центру"));

    var rightAlignButton = new _Button2.default(null, "asf-icon-button asf-building-alignRightButton", true, true, false);
    rightAlignButton.getElement().prop("title", i18n.tr("По правому краю"));

    var justifyAlignButton = new _Button2.default(null, "asf-icon-button asf-building-alignJustifyButton asf-builderMargin", true, true, true);
    justifyAlignButton.getElement().prop("title", i18n.tr("По ширине"));

    var instance = this;

    this.playerView = playerView;

    this.currentView = null;
    this.currentModel = null;
    this.currentTableView = null;
    this.currentTableModel = null;
    this.currentViews = null;
    this.currentMainCell = null;
    this.currentSelectedCells = null;
    this.destStyle = null;

    this.playerView.model.on(_constants.EVENT_TYPE.cellSizeChange, function () {
        instance.validateToolbarPosition();
    });

    this.updateToolbarState = function (mainViewCell, selectedCells) {

        instance.currentMainCell = mainViewCell;

        if (mainViewCell) {
            instance.currentView = mainViewCell.getView();
            instance.currentModel = instance.currentView.model;
            instance.currentTableView = instance.playerView.getViewWithId(mainViewCell.getTableId());
            instance.currentTableModel = instance.currentTableView.model;
        } else {
            instance.currentView = null;
            instance.currentModel = null;
            instance.currentTableView = null;
            instance.currentTableModel = null;
        }

        instance.currentSelectedCells = selectedCells;

        instance.currentViews = [];

        if (selectedCells) {
            selectedCells.forEach(function (selectedCell) {
                if (!selectedCell.getView()) {
                    return;
                }
                instance.currentViews.push(selectedCell.getView());
            });
        }

        toolBar.children().detach();
        firstLine.children().detach();
        secondLine.children().detach();
        thirdLine.children().detach();

        toolBar.hide();

        if (!instance.currentView) {
            return;
        }

        toolBar.show();

        if (instance.currentViews.length === 1) {
            if (instance.currentModel.asfProperty.type === _constants.WIDGET_TYPE.table) {
                instance.addSingleTableViewProperties();
            } else {
                instance.addSingleViewProperties();
            }
        }

        var hasTable = false;
        var hasOnlyTables = true;
        instance.currentViews.forEach(function (view) {
            hasTable = hasTable || view.model.asfProperty.type === _constants.WIDGET_TYPE.table;
            hasOnlyTables = hasOnlyTables && view.model.asfProperty.type === _constants.WIDGET_TYPE.table;
        });

        if (!hasTable) {
            instance.addCommonProperties();
        }

        if (instance.currentViews.length > 1 && hasTable) {
            instance.addDelete();
        }
        instance.addToScroll();

        instance.validateToolbarPosition();
    };

    this.validateToolbarPosition = function () {

        if (!instance.currentView) {
            return;
        }

        if (instance.currentView.container.parent().length === 0) {
            return;
        }

        var offset = instance.currentView.container.parent().offset();
        var playerOffset = instance.playerView.container.offset();
        var viewPort = {
            top: 0,
            left: 0,
            bottom: instance.playerView.container.height(),
            right: instance.playerView.container.width()
        };

        if (instance.playerView.scroll) {
            viewPort.top = instance.playerView.scroll.scrollTop() - instance.playerView.topMargin;
            viewPort.left = instance.playerView.scroll.scrollLeft() - instance.playerView.leftMargin;
            viewPort.bottom = viewPort.top + instance.playerView.scroll.outerHeight();
            viewPort.right = viewPort.left + instance.playerView.scroll.outerWidth();
        }

        var top = offset.top - playerOffset.top + instance.currentView.container.parent().outerHeight();
        var left = offset.left - playerOffset.left;

        if (top < viewPort.top) {
            top = viewPort.top + 10;
        } else if (top + toolBar.outerHeight() > viewPort.bottom) {

            top = offset.top - playerOffset.top - toolBar.outerHeight();

            if (top < viewPort.top) {
                top = viewPort.bottom - toolBar.outerHeight() - 10;
            }
        }

        if (left < viewPort.left) {
            left = viewPort.left + 10;
        } else if (left + toolBar.outerWidth() > viewPort.right) {
            left = viewPort.right - toolBar.outerWidth() - 10;
        }

        toolBar.css("top", top);
        toolBar.css("left", left);
    };

    this.addToScroll = function () {
        if (toolBar.parent().length > 0) {
            return;
        }

        if (!instance.playerView.scroll) {
            return;
        }

        instance.playerView.container.append(toolBar);

        instance.playerView.scroll.scroll(instance.validateToolbarPosition);
    };

    this.addSingleViewProperties = function () {
        firstLine.append(idInput);
        firstLine.append(settingsButton.getElement());
        firstLine.append(addToTableButton.getElement());
        firstLine.append(selectTableButton.getElement());
        firstLine.append(deleteButton.getElement());
        toolBar.append(firstLine);

        instance.validateIdInput();
        instance.validateSelectTableButtonState();
    };

    this.addSingleTableViewProperties = function () {
        firstLine.append(idInput);
        firstLine.append(settingsButton.getElement());
        firstLine.append(addToTableButton.getElement());
        firstLine.append(clearTableButton.getElement());
        firstLine.append(deleteButton.getElement());

        toolBar.append(firstLine);

        instance.validateIdInput();
        instance.validateSelectTableButtonState();
    };

    this.validateSelectTableButtonState = function () {
        if (instance.currentModel.asfProperty.type == _constants.WIDGET_TYPE.table) {
            idInput.css("max-width", "176px");
            idInput.css("min-width", "176px");
        } else if (instance.currentTableModel.asfProperty.type == _constants.WIDGET_TYPE.page) {
            selectTableButton.getElement().detach();
            idInput.css("max-width", "228px");
            idInput.css("min-width", "228px");
        } else {
            idInput.css("max-width", "200px");
            idInput.css("min-width", "200px");
        }
    };

    this.addCommonProperties = function () {
        secondLine.append(fontFamilyList.getElement());
        secondLine.append(fontSizeInput);
        secondLine.append(boldButton.getElement());
        secondLine.append(italicButton.getElement());
        secondLine.append(underlineButton.getElement());
        secondLine.append(lineButton.getElement());

        thirdLine.append(widthIcon);
        thirdLine.append(widthInput);
        thirdLine.append(heightIcon);
        thirdLine.append(heightInput);
        thirdLine.append(leftAlignButton.getElement());
        thirdLine.append(centerAlignButton.getElement());
        thirdLine.append(rightAlignButton.getElement());
        thirdLine.append(justifyAlignButton.getElement());
        thirdLine.append(deleteButton.getElement());

        toolBar.append(secondLine);
        toolBar.append(thirdLine);

        instance.updateCommonButtonsState();
    };

    function mergeValue(value1, value2, emptyValue) {
        if (value1 == value2) {
            if (!value1) {
                if (emptyValue) {
                    return emptyValue;
                } else {
                    return "";
                }
            }
            return value1;
        }
        return "";
    }

    function mergeStyle(style1, style2) {
        return {
            font: mergeValue(style1.font, style2.font, "Arial"),
            fontsize: mergeValue(style1.fontsize, style2.fontsize),
            bold: mergeValue(style1.bold, style2.bold),
            italic: mergeValue(style1.italic, style2.italic),
            strike: mergeValue(style1.strike, style2.strike),
            underline: mergeValue(style1.underline, style2.underline),
            align: mergeValue(style1.align, style2.align),
            width: mergeValue(style1.width, style2.width),
            height: mergeValue(style1.height, style2.height)
        };
    }

    this.updateCommonButtonsState = function () {
        instance.destStyle = instance.currentView.model.asfProperty.style;

        instance.currentViews.forEach(function (view) {
            var style = view.model.asfProperty.style;
            instance.destStyle = mergeStyle(instance.destStyle, style);
        });

        fontFamilyList.setSelectedValue(instance.destStyle.font);
        fontSizeInput.val(instance.destStyle.fontsize);

        boldButton.setState(instance.destStyle.bold);
        italicButton.setState(instance.destStyle.italic);
        underlineButton.setState(instance.destStyle.underline);
        lineButton.setState(instance.destStyle.strike);

        leftAlignButton.setState(instance.destStyle.align === 'left');
        centerAlignButton.setState(instance.destStyle.align === 'center');
        rightAlignButton.setState(instance.destStyle.align === 'right');
        justifyAlignButton.setState(instance.destStyle.align === 'justify');

        widthInput.val(instance.destStyle.width);
        heightInput.val(instance.destStyle.height);
    };

    this.addDelete = function () {
        thirdLine.append(deleteButton.getElement());
        toolBar.append(thirdLine);
    };

    this.checkId = function (newId) {

        if (newId == instance.currentModel.asfProperty.id) {
            return true;
        }

        if (newId === '') {
            services.showErrorMessage(i18n.tr("Имя компонента не может быть пустым"));
            return false;
        }

        if (newId.indexOf(".") != -1 || newId.indexOf("-b") != -1) {
            services.showErrorMessage(i18n.tr("Имя компонента не может содержать \".\" или \"-b\""));
            return false;
        }

        var tableId = instance.currentTableModel.asfProperty.id;
        if (!instance.currentTableModel.isDynamic()) {
            tableId = null;
        }
        var conflict = instance.currentModel.playerModel.getModelWithId(newId, tableId);
        if (conflict) {
            conflict.trigger(_constants.EVENT_TYPE.markInvalid, conflict);
            services.showErrorMessage(i18n.tr("Имя компонента должно быть уникальным."));
            return false;
        }

        return true;
    };

    this.changeId = function (newId) {
        var asfProperty = instance.currentModel.getProperty();
        asfProperty.id = newId;

        var tModel = instance.currentTableModel;
        var locationInfo = tableUtils.getComponentById(tModel.asfProperty.layout, instance.currentModel.asfProperty.id);

        var hItems = [];
        hItems.push(playerView.history.createDeleteItem(instance.currentModel.getProperty(), [locationInfo.row, locationInfo.column], tModel.asfProperty.id));
        hItems.push(playerView.history.createInsertItem(utils.clone(asfProperty), [locationInfo.row, locationInfo.column], tModel.asfProperty.id));
        playerView.history.addItems(hItems);

        tModel.deleteModel(instance.currentModel);
        tModel.insertComponentTo(asfProperty, locationInfo.row, locationInfo.column);
    };

    this.validateIdInput = function (read) {
        idInput.val(instance.currentView.model.asfProperty.id);
    };

    this.applyStyleChange = function (field, value) {
        instance.destStyle[field] = value;

        var history = playerView.history;
        var hItems = [];

        instance.currentViews.forEach(function (view) {
            var model = view.model;
            var style = model.asfProperty.style;
            if (!style) {
                style = {};
                model.asfProperty.style = style;
            }
            var oldStyle = utils.clone(style);

            style[field] = value;
            view.applyStyle();

            hItems.push(history.createStyleItem(oldStyle, utils.clone(style), model.asfProperty.id, instance.currentTableModel.asfProperty.id, instance.currentTableModel.isPage()));
        });
        history.addItems(hItems);

        instance.validateToolbarPosition();
        instance.playerView.model.trigger(_constants.EVENT_TYPE.cellSizeChange);
    };

    this.showAddToTableMenu = function () {
        var values = [{ value: "addRowBefore", title: i18n.tr("Добавить строку выше") }, { value: "addRowAfter", title: i18n.tr("Добавить строку ниже") }, { value: "addColumnBefore", title: i18n.tr("Добавить колонку слева") }, { value: "addColumnAfter", title: i18n.tr("Добавить колонку справа") }];

        services.showDropDown(values, addToTableButton.getElement(), 150, function (selectedValue) {

            var row = instance.currentMainCell.getRow();
            var column = instance.currentMainCell.getColumn();

            switch (selectedValue) {
                case 'addRowBefore':
                    instance.currentTableView.addRow(row);
                    break;
                case 'addRowAfter':
                    instance.currentTableView.addRow(row + 1);
                    break;
                case 'addColumnBefore':
                    instance.currentTableView.addColumn(column);
                    break;
                case 'addColumnAfter':
                    instance.currentTableView.addColumn(column + 1);
                    break;
            }
        });
    };

    settingsButton.getElement().click(function (event) {
        domUtils.cancelEvent(event);
        var model = instance.currentModel;
        var tableModel = instance.currentTableModel;
        var asfProperty = builderUtils.extendAsfProperty(model, tableModel);
        var oldProperty = utils.clone(asfProperty);

        AS.SERVICES.showComponentSettings(asfProperty, function (asfProperty) {
            playerView.history.addItem(playerView.history.settingsItem(oldProperty, utils.clone(asfProperty), tableModel.asfProperty.id, tableModel.isPage()));
            builderUtils.componentSettingsChanged(asfProperty, model, tableModel, instance.playerView);
            instance.validateToolbarPosition();
        });
    });

    selectTableButton.getElement().click(function (event) {
        domUtils.cancelEvent(event);
        var ownerTablePage = instance.playerView.getOwnerPage(instance.currentTableView.model.asfProperty.id);
        ownerTablePage.selectOneView(instance.currentTableView);
    });

    clearTableButton.getElement().click(function (event) {
        domUtils.cancelEvent(event);
        if (!confirm(i18n.tr("Вы действительно хотите очистить таблицу?"))) {
            return;
        }
        instance.currentView.clear();
        instance.validateToolbarPosition();
    });

    boldButton.getElement().click(function (event) {
        instance.applyStyleChange("bold", boldButton.getState());
        domUtils.cancelEvent(event);
    });

    italicButton.getElement().click(function (event) {
        instance.applyStyleChange("italic", italicButton.getState());
        domUtils.cancelEvent(event);
    });

    underlineButton.getElement().click(function (event) {
        instance.applyStyleChange("underline", underlineButton.getState());
        domUtils.cancelEvent(event);
    });

    lineButton.getElement().click(function (event) {
        instance.applyStyleChange("strike", lineButton.getState());
        domUtils.cancelEvent(event);
    });

    leftAlignButton.getElement().click(function (event) {
        if (leftAlignButton.getState()) {
            rightAlignButton.setState(false);
            centerAlignButton.setState(false);
            justifyAlignButton.setState(false);
            instance.applyStyleChange("align", "left");
        } else {
            instance.applyStyleChange("align", "");
        }
        domUtils.cancelEvent(event);
    });

    centerAlignButton.getElement().click(function (event) {
        if (centerAlignButton.getState()) {
            leftAlignButton.setState(false);
            rightAlignButton.setState(false);
            justifyAlignButton.setState(false);
            instance.applyStyleChange("align", "center");
        } else {
            instance.applyStyleChange("align", "");
        }
        domUtils.cancelEvent(event);
    });

    rightAlignButton.getElement().click(function (event) {
        if (rightAlignButton.getState()) {
            leftAlignButton.setState(false);
            centerAlignButton.setState(false);
            justifyAlignButton.setState(false);
            instance.applyStyleChange("align", "right");
        } else {
            instance.applyStyleChange("align", "");
        }
        domUtils.cancelEvent(event);
    });

    justifyAlignButton.getElement().click(function (event) {
        if (justifyAlignButton.getState()) {
            leftAlignButton.setState(false);
            centerAlignButton.setState(false);
            rightAlignButton.setState(false);
            instance.applyStyleChange("align", "justify");
        } else {
            instance.applyStyleChange("align", "");
        }
        domUtils.cancelEvent(event);
    });

    widthInput.keyup(function (event) {
        if (event.keyCode == _constants.KEY_CODES.ENTER) {
            var newWidth = widthInput.val().trim();
            if (newWidth === instance.destStyle.width) {
                return;
            }
            instance.applyStyleChange("width", newWidth);
        } else if (event.keyCode == _constants.KEY_CODES.escape) {
            widthInput.val(instance.destStyle.width);
        }
    });

    heightInput.keyup(function (event) {
        if (event.keyCode == _constants.KEY_CODES.ENTER) {
            var newHeight = heightInput.val().trim();
            if (newHeight === instance.destStyle.height) {
                return;
            }
            instance.applyStyleChange("height", newHeight);
        } else if (event.keyCode == _constants.KEY_CODES.escape) {
            heightInput.val(instance.destStyle.height);
        }
    });

    deleteButton.getElement().click(function (event) {

        if (!confirm(i18n.tr("Удалить элемент формы?"))) {
            return;
        }

        var currentCells = instance.currentSelectedCells.slice();

        var playerView = instance.playerView;

        var hItems = [];

        currentCells.forEach(function (selectedCell) {
            if (!selectedCell.getView()) {
                return;
            }
            var view = selectedCell.getView();
            var tableView = playerView.getViewWithId(selectedCell.getTableId());

            hItems.push(playerView.history.createDeleteItem(view.model.getProperty(), [selectedCell.row, selectedCell.column], tableView.model.asfProperty.id));
            tableView.model.deleteModel(view.model);
        });

        playerView.history.addItems(hItems);

        instance.updateToolbarState();

        domUtils.cancelEvent(event);
    });

    fontFamilyList.on(_constants.EVENT_TYPE.valueChange, function (evt, selectedFont) {
        instance.applyStyleChange("font", selectedFont);
    });

    fontSizeInput.keyup(function (event) {
        if (event.keyCode == _constants.KEY_CODES.ENTER) {
            var newSize = fontSizeInput.val().trim();
            if (newSize === instance.destStyle.fontsize) {
                return;
            }
            instance.applyStyleChange("fontsize", newSize);
        } else if (event.keyCode == _constants.KEY_CODES.escape) {
            fontSizeInput.val(instance.destStyle.fontsize);
        }
    });

    addToTableButton.getElement().click(function (event) {
        instance.showAddToTableMenu();
        domUtils.cancelEvent(event);
    });

    idInput.keyup(function (event) {
        if (event.keyCode == _constants.KEY_CODES.ENTER) {
            var newId = idInput.val().trim();
            if (newId !== instance.currentModel.asfProperty.id) {
                if (!instance.checkId(newId)) {
                    return;
                }
                instance.changeId(newId);
            } else {
                instance.validateIdInput(true);
            }
        } else if (event.keyCode == _constants.KEY_CODES.escape) {
            instance.validateIdInput(true);
        }
    });
};

BuilderToolBar.prototype = Object.create(BuilderToolBar.prototype);

/***/ }),

/***/ 215:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = Button;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function Button(title, className, toggle, hasLeftBorder, hasRightBorder) {

    var instance = this;

    this.state = false;

    var container = (0, _jquery2.default)('<div/>', { class: "asf-InlineBlock" });

    container.button();

    if (!hasRightBorder) {
        container.css("border-right", "none");
        container.css("border-bottom-right-radius", "2px");
        container.css("border-top-right-radius", "2px");
    }

    if (!hasLeftBorder) {
        container.css("border-left", "none");
        container.css("border-bottom-left-radius", "2px");
        container.css("border-top-left-radius", "2px");
    }

    if (className) {
        container.addClass(className);
    }

    if (title) {
        container.text(title);
    }

    if (toggle) {
        container.mousedown(function () {
            instance.setState(!instance.state);
        });
    }

    this.getState = function () {
        return instance.state;
    };

    this.setState = function (newState) {
        if (newState) {
            container.addClass("asf-common-button-selected");
            this.state = true;
        } else {
            container.removeClass("asf-common-button-selected");
            this.state = false;
        }
    };

    this.getElement = function () {
        return container;
    };
};

Button.prototype = Object.create(Button.prototype);

/***/ }),

/***/ 216:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = ComboBox;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _componentUtils = __webpack_require__(15);

var _services = __webpack_require__(23);

var services = _interopRequireWildcard(_services);

var _constants = __webpack_require__(2);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ComboBox(inputClass, buttonClass) {

    var instance = this;

    (0, _componentUtils.makeBus)(instance);

    var container = (0, _jquery2.default)('<div/>', { class: "asf-InlineBlock", style: "vertical-align:top" });

    if (!inputClass) {
        inputClass = "asf-dropdown-input";
    }

    if (!buttonClass) {
        buttonClass = "asf-dropdown-button";
    }

    var dropdownBox = (0, _jquery2.default)('<div/>', { class: inputClass });
    var button = (0, _jquery2.default)("<button/>", { class: buttonClass });

    container.append(dropdownBox);
    container.append(button);

    var selectedValue = null;

    this.values = [];

    var showDropDown = function showDropDown(event) {

        event.stopPropagation();
        event.stopImmediatePropagation();
        event.preventDefault();

        if (services.isShownDropDown(instance.container)) {
            services.closeDropDown(instance.container);
            return;
        }

        instance.values.forEach(function (value) {
            value.selected = selectedValue === value.value;
        });

        services.showDropDown(instance.values, container, null, function (selectedValue) {
            instance.setSelectedValue(selectedValue);
            instance.trigger(_constants.EVENT_TYPE.valueChange, selectedValue, instance);
        });
    };

    button.click(function (event) {
        showDropDown(event);
    });

    dropdownBox.click(function (event) {
        showDropDown(event);
    });

    this.unmarkInvalid = function () {
        dropdownBox.removeClass('asf-invalidInput');
    };

    this.markInvalid = function () {
        dropdownBox.addClass('asf-invalidInput');
    };

    this.setEnabled = function (newEnabled) {
        if (newEnabled) {
            dropdownBox.removeClass('asf-disabledInput');
        } else {
            dropdownBox.addClass('asf-disabledInput');
        }
    };

    this.setSelectedValue = function (newSelectedValue) {
        selectedValue = newSelectedValue;
        dropdownBox.text("");
        instance.values.forEach(function (value) {
            if (value.value === newSelectedValue) {
                dropdownBox.text(value.title);
            }
        });
    };

    this.getSelectedValue = function () {
        return selectedValue;
    };

    this.setValues = function (newValues) {
        this.values = newValues;
        this.setSelectedValue(selectedValue);
    };

    this.getElement = function () {
        return container;
    };
};

ComboBox.prototype = Object.create(ComboBox.prototype);

/***/ }),

/***/ 217:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.dropDownMenu = undefined;
exports.default = DropDownMenu;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _constants = __webpack_require__(2);

var _domUtils = __webpack_require__(44);

var domUtils = _interopRequireWildcard(_domUtils);

var _PopupPane = __webpack_require__(128);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Created by user on 05.05.16.
 * выпадающий список
 * синглтон
 */

var MIN_ITEM_HEIGHT = 24;
var MAX_HEIGHT = 240;

function DropDownMenu() {

    if (DropDownMenu.prototype._singletonInstance) {
        return DropDownMenu.prototype._singletonInstance;
    }

    DropDownMenu.prototype._singletonInstance = this;

    var instance = this;

    var div = (0, _jquery2.default)("<div>", { class: "asf-dropdownPopup" });

    var valueItems = [];

    var lastAnchor = null;

    (0, _jquery2.default)(document).keydown(function (event) {
        if (!instance.isShowing() || event.keyCode !== _constants.KEY_CODES.down && event.keyCode !== _constants.KEY_CODES.up && event.keyCode !== _constants.KEY_CODES.ENTER) {
            return;
        }
        if (event.keyCode === _constants.KEY_CODES.ENTER) {
            instance.selectElement();
        } else if (event.keyCode === _constants.KEY_CODES.up) {
            instance.moveSelectionUp();
        } else if (event.keyCode === _constants.KEY_CODES.down) {
            instance.moveSelectionDown();
        }

        event.cancelBubble = true;
        event.stopped = true;
        event.preventDefault();
        event.stopPropagation();
    });

    (0, _jquery2.default)(document).mouseup(function (e) {
        if (!div.is(e.target) && div.has(e.target).length === 0) {
            closeDropdown();
        }
    });

    var closeDropdown = function closeDropdown() {
        div.empty();
        valueItems = [];
        _PopupPane.popupPanel.hide(div);
        lastAnchor = null;
    };

    this.getSelectedIndex = function () {
        var selectedIndex = -1;
        valueItems.forEach(function (valueItem, index) {
            if (valueItem.isSelected()) {
                selectedIndex = index;
            }
        });
        return selectedIndex;
    };

    this.moveSelectionDown = function () {
        var selectedIndex = instance.getSelectedIndex();
        if (selectedIndex < valueItems.length - 1) {
            if (selectedIndex !== -1) {
                valueItems[selectedIndex].setSelected(false);
            }
            selectedIndex++;
            valueItems[selectedIndex].setSelected(true);
        }
    };

    this.moveSelectionUp = function () {
        var selectedIndex = instance.getSelectedIndex();
        if (selectedIndex > 0) {
            valueItems[selectedIndex].setSelected(false);
            selectedIndex--;
            valueItems[selectedIndex].setSelected(true);
        }
    };

    this.selectElement = function () {
        var selectedIndex = instance.getSelectedIndex();
        if (selectedIndex > -1) {
            valueItems[selectedIndex].selectElement();
        }
    };

    this.show = function (values, anchor, minWidth, handler, exclusive) {

        closeDropdown();
        var lastAnchor = void 0;

        // если второй раз пытаются показать тот же самый объект то не показываем
        if (values.length === 0) {
            lastAnchor = null;
            return;
        }

        lastAnchor = anchor;

        values.forEach(function (value) {
            var item = new DropDownItem(value, function () {
                handler(value.value);
                closeDropdown();
            });
            div.append(item.getElement());
            valueItems.push(item);
        });

        var height = Math.min(values.length * MIN_ITEM_HEIGHT, MAX_HEIGHT) + 2;
        var width = Math.max(anchor.width() - 6, minWidth);

        _PopupPane.popupPanel.showComponent(div, anchor, width, height, exclusive);

        div.css("width", width + "px");
        div.css("height", height + "px");
    };

    this.hide = function (anchor) {
        if (instance.isShownDropDown(anchor)) {
            closeDropdown();
        }
    };

    this.isShownDropDown = function (anchor) {
        return anchor == lastAnchor && instance.isShowing();
    };

    this.isShowing = function () {
        return _jquery2.default.contains(document, div[0]);
    };
};

function DropDownItem(value, handler) {

    var valueElement = (0, _jquery2.default)("<div>", { class: "asf-dropdownItem" });
    valueElement.text(value.title);
    valueElement.attr('title', value.title);
    valueElement.click(function (event) {
        instance.selectElement();
        domUtils.cancelEvent(event);
    });

    valueElement.hover(function () {
        valueElement.removeClass("asf-dropdownItemSelected");
        valueElement.addClass("asf-dropdownItemHover");
    }, function () {
        valueElement.removeClass("asf-dropdownItemHover");
        instance.setSelected(value.selected);
    });

    var instance = this;

    this.setSelected = function (selected) {
        if (selected) {
            valueElement.addClass("asf-dropdownItemSelected");
        } else {
            valueElement.removeClass("asf-dropdownItemSelected");
        }
        value.selected = selected;
    };

    this.getElement = function () {
        return valueElement;
    };

    this.getValue = function () {
        return value;
    };

    this.selectElement = function () {
        handler(value.value);
    };

    this.isSelected = function () {
        return value.selected;
    };

    if (value.selected) {
        instance.setSelected(true);
    }
};

var dropDownMenu = exports.dropDownMenu = new DropDownMenu();

/***/ }),

/***/ 218:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = RegistryTable;

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

var _ExtendedTable = __webpack_require__(113);

var _ExtendedTable2 = _interopRequireDefault(_ExtendedTable);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function RegistryTable(registry, width) {
    var columns = [];

    var recordsCount = 0;
    var currentPage = 0;
    var search = null;
    var sortCmpId = null;
    var sortAsc = null;

    var countInPage = 15;

    var instance = this;
    this.filterID = null;

    registry.columns.forEach(function (col) {
        if (col.visible != 1) {
            return;
        }
        col.id = col.columnID;
        col.name = col.label;
        columns.push(col);
    });

    var columnWidth = 150;
    if (columns.length <= 5) {
        columnWidth = (width - 1) / columns.length;
    }

    columns = columns.sort(function (item1, item2) {
        var number1 = item1.order;
        var number2 = item2.order;

        if (number1 === number2) {
            if (item1.name < item2.name) {
                return -1;
            } else if (item1.name > item2.name) {
                return 1;
            }
        } else {
            if (number1 === 0) {
                return 1;
            } else if (number2 === 0) {
                return -1;
            } else if (number1 < number2) {
                return -1;
            } else {
                return 1;
            }
        }
        return 0;
    });
    console.log(columns);

    columns.forEach(function (column) {
        column.width = columnWidth;
        column.minWidth = 35;
    });

    var table = new _ExtendedTable2.default();
    table.init({}, columns);

    this.dblclick = function (handler) {
        table.getWidget().dblclick(handler);
    };

    table.on(_ExtendedTable.ExtendedTableEvent.columnSortClicked, function (event, columnIndex, sortAsc, columnOption) {
        instance.sort(columnOption.id, sortAsc);
    });

    this.search = function (newSearch) {
        search = newSearch;
        this.loadData(0, this.filterID);
    };

    this.sort = function (cmpId, asc) {
        sortCmpId = cmpId;
        sortAsc = asc;
        this.loadData(0, this.filterID);
    };

    this.loadData = function (pageNumber, filterID) {
        currentPage = pageNumber;
        var handler = function handler(data) {
            recordsCount = data.recordsCount;

            var registryData = [];

            data.result.forEach(function (item) {

                var mergedItem = item.fieldValue;
                mergedItem.documentId = item.documentID;
                mergedItem.asfDataUUID = item.dataUUID;
                mergedItem.uuid = item.documentID;

                registryData.push(mergedItem);
            });

            table.setData(registryData);
        };
        this.filterID = filterID;
        if (filterID) {
            api.getRegistryFilterData(registry.registryID, filterID, pageNumber, countInPage, search, sortCmpId, sortAsc, handler);
        } else {
            api.getRegistryData(registry.registryID, pageNumber, countInPage, search, sortCmpId, sortAsc, handler);
        }
    };

    this.getCurrentPage = function () {
        return currentPage;
    };

    this.getRecordsCount = function () {
        return recordsCount;
    };

    this.getPagesCount = function () {
        var pagesCount = Math.floor(recordsCount / countInPage);
        if (recordsCount % countInPage) {
            pagesCount++;
        }
        return pagesCount;
    };

    this.getSelectedData = function () {
        return table.getSelectedData();
    };

    this.loadData(0, this.filterID);

    this.on = function (event, handler) {
        table.on(event, handler);
    };

    this.getSelectedData = function () {
        return table.getSelectedData();
    };

    this.getWidget = function () {
        return table.getWidget();
    };
};

/***/ }),

/***/ 219:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(jQuery) {

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = SplitPane;

var _constants = __webpack_require__(2);

/**
 * вертикальная сплит панель
 * @constructor
 */

function SplitPane() {
    this.splitContainer = jQuery('<div/>', { class: 'ns-splitPanel' });

    this.leftContainer = jQuery('<div/>', { class: 'ns-leftSplitPanel' });

    this.splitDivider = jQuery('<div/>', { class: 'ns-splitDivider' });

    this.rightContainer = jQuery('<div/>', { class: 'ns-rightSplitPanel' });

    this.minDividerPosition = null;

    this.maxDividerPosition = null;

    this.dividerSize = 5;

    this.width = 200;
    this.height = 200;

    this.position = 100;

    this.splitContainer.append(this.leftContainer);
    this.splitContainer.append(this.splitDivider);
    this.splitContainer.append(this.rightContainer);

    this.bus = jQuery({});

    var instance = this;

    (function () {
        var startX = -1;
        instance.splitDivider.mousedown(function (evt) {
            startX = evt.screenX;
            instance.leftContainer.addClass("disabled-selection");
            instance.rightContainer.addClass("disabled-selection");
        });

        instance.splitDivider.mousemove(function (evt) {
            event.cancelBubble = true;
            event.stopped = true;
            event.stopPropagation();
        });

        instance.splitContainer.mousemove(function (evt) {

            var targetPosition = instance.position - (startX - evt.screenX);

            if (!instance.validPosition(targetPosition)) {
                startX = -1;
                instance.leftContainer.removeClass("disabled-selection");
                instance.rightContainer.removeClass("disabled-selection");
            }

            if (startX == -1) {
                return;
            }

            instance.setDividerPosition(targetPosition);
            startX = evt.screenX;
        });

        instance.splitContainer.mouseup(function (evt) {
            startX = -1;
            instance.leftContainer.removeClass("disabled-selection");
            instance.rightContainer.removeClass("disabled-selection");
        });
    })();

    this.setPixelSize = function (newWidth, newHeight) {
        this.width = newWidth;
        this.height = newHeight;
        this.splitContainer.css("width", newWidth);
        this.splitContainer.css("height", newHeight);
        this.leftContainer.css("height", newHeight);
        this.splitDivider.css("height", newHeight);
        this.rightContainer.css("height", newHeight);
        this.setDividerPosition(this.position);

        if (!this.maxDividerPosition) {
            this.maxDividerPosition = newWidth;
        }
    };

    this.validPosition = function (newPosition) {
        if (instance.minDividerPosition && instance.minDividerPosition > newPosition) {
            return false;
        }
        if (this.maxDividerPosition && this.maxDividerPosition < newPosition) {
            return false;
        }

        return true;
    };

    this.setDividerPosition = function (newPosition) {
        if (instance.minDividerPosition && instance.minDividerPosition > newPosition) {
            newPosition = this.minDividerPosition;
        }

        if (this.maxDividerPosition && this.maxDividerPosition < newPosition) {
            newPosition = this.maxDividerPosition;
        }

        this.position = newPosition;

        this.leftContainer.css("left", 0);
        this.leftContainer.css("width", newPosition);

        this.splitDivider.css("left", newPosition);

        this.rightContainer.css("left", newPosition + this.dividerSize + "px");
        this.rightContainer.css("width", this.width - newPosition - this.dividerSize);

        instance.bus.trigger(_constants.COMPONENT_EVENTS.resized, []);
    };
};
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ }),

/***/ 220:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = AddressLinkModel;

var _model = __webpack_require__(25);

var _model2 = _interopRequireDefault(_model);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * ссылка на адресную книгу
 * asfData:
 *   "id": "cmp-7gj2xv", "type": "personlink", "value": "555555 555555 555555 (454545)", "key": "0:idenitifier", "valueID": "0:identifier"
 * defaultValue: {"valueID":null,"value":null}
 * @param asfProperty
 * @param playerModel
 * @constructor
 */
function AddressLinkModel(asfProperty, playerModel) {
    _model2.default.call(this, this, asfProperty, playerModel);

    var instance = this;
    var textValue = "";
    this.type = "";

    this.getTextValue = function (onlyText) {
        return textValue;
    };

    this.getAsfData = function (blockNumber) {
        var v = this.value;
        var result = dataUtils.getBaseAsfData(asfProperty, blockNumber);
        if (v) {
            result.value = textValue;
            result.key = instance.type + ":" + instance.value;
            result.valueID = instance.type + ":" + instance.value;
        }
        return result;
    };

    this.setAsfData = function (asfData) {
        if (!asfData || !asfData.key) {
            return;
        }
        textValue = asfData.value;
        instance.type = asfData.key.split(":")[0];
        instance.setValue(asfData.key.split(":")[1]);
    };

    this.setValueFromInput = function (newValue, newTextValue, newType) {
        if (!newValue) {
            textValue = '';
            instance.setValue(newValue);
        } else {
            textValue = newTextValue;
            instance.type = newType;
            instance.setValue(newValue);
        }
    };
};

AddressLinkModel.prototype = Object.create(_model2.default.prototype);
AddressLinkModel.prototype.constructor = AddressLinkModel;

/***/ }),

/***/ 221:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = ComboBoxModel;

var _constants = __webpack_require__(2);

var _model = __webpack_require__(25);

var _model2 = _interopRequireDefault(_model);

var _componentUtils = __webpack_require__(15);

var compUtils = _interopRequireWildcard(_componentUtils);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Выпадающий список, выбор вариантов, переключатель вариантов
 * asfData:
 * Выпадающий список
 * "id": "cmp-7gj2xv", "type": "listbox", "value": "наименование выбранного элемента", "key": "значение выбранного элемента"
 * Выбор вариантов
 * "id": "cmp-7gj2xv", "type": "check", "values": [«значение элемента 1», «значение элемента 2», «значение элемента 3»], "keys": [«наименование элемента 1», «наименование элемента», «наименование элемента 3»]
 * Переключатель вариантов
 * "id": "cmp-7gj2xv", "type": "radio", "value": "значение выбраного элемента", "key": "наименование выбранного элемента"
 * defaultValue:
 * @param asfProperty
 * @param playerModel
 * @constructor
 */
function ComboBoxModel(asfProperty, playerModel) {
    _model2.default.call(this, this, asfProperty, playerModel);

    this.asfDataElements = []; // выбранные элементы из данных файла по форме

    /**
     * все элементы списка
     * @type {Array}
     */
    this.listElements = [];
    /**
     * элементы списка согласно фильтру (или все если фильтра нет)
     * @type {Array}
     */
    this.listCurrentElements = [];

    /**
     * загружены ли данные
     * @type {boolean}
     */
    var dataLoaded = false;
    /**
     * инстанс
     * @type {ComboBoxModel}
     */
    var instance = this;
    /**
     * владелец
     * @type {null}
     */
    var masterModel = null;

    this.value = [];

    this.doSetValue = function (newValue) {
        if (newValue != null) {
            if (!(newValue instanceof Array)) {
                newValue = [newValue];
            } else if (newValue.length == 0) {
                newValue = null;
            }
        }

        if (dataLoaded && (masterModel || playerModel.building)) {
            // новое значение нужно фильтровать только если настроена зависимость, иначе не фильтруем
            newValue = ComboUtils.validateValueToFilter(instance.listCurrentElements, newValue, asfProperty.type == _constants.WIDGET_TYPE.listbox);
        }

        if (newValue == this.value || newValue != null && newValue.equals(this.value)) {
            return;
        }

        this.value = newValue;

        this.fireChangeEvents();
    };

    this.getSpecialErrors = function () {
        if (instance.value instanceof Array) {
            if (asfProperty.required) {
                var hasNotEmptyValue = false;
                this.value.some(function (val) {
                    if (val && val !== '') {
                        hasNotEmptyValue = true;
                        return true;
                    }
                });
                if (!hasNotEmptyValue) {
                    return { id: asfProperty.id, errorCode: _constants.INPUT_ERROR_TYPE.emptyValue };
                }
            }

            var deletedSelected = false;
            this.listCurrentElements.forEach(function (currentElement) {
                if (currentElement.deleted && instance.value.contains(currentElement.value)) {
                    if (!currentElement.label.startsWith('(!)')) {
                        currentElement.label = '(!) ' + currentElement.label;
                    }
                    deletedSelected = true;
                }
            });
            if (deletedSelected) {
                instance.trigger(_constants.EVENT_TYPE.dataLoad, instance);
                return { id: asfProperty.id, errorCode: _constants.INPUT_ERROR_TYPE.deletedValue };
            }
        }
    };

    this.findMaster = function () {
        dataLoaded = true;

        if (playerModel.building) {
            instance.updateModelData();
            return;
        }
        setTimeout(function () {
            if (asfProperty.config.depends) {
                masterModel = compUtils.findMaster(asfProperty, playerModel);
                if (!masterModel) {
                    return;
                }
                masterModel.on(_constants.EVENT_TYPE.valueChange, function (event, model, value) {
                    instance.updateModelData();
                });
            }
            instance.updateModelData();
        }, 0);
    };

    this.updateModelData = function () {
        var filterValue = [];

        var listElements = [];
        listElements = instance.listElements.slice();
        instance.asfDataElements.forEach(function (asfDataElement) {
            var exist = instance.listElements.some(function (listElement) {
                return listElement.value == asfDataElement.value;
            });
            if (!exist) {
                asfDataElement.deleted = true;
                listElements.push(asfDataElement);
            }
        });

        if (masterModel) {
            filterValue = masterModel.getValue();
            if (filterValue && !(filterValue instanceof Array)) {
                filterValue = [filterValue];
            }
            instance.listCurrentElements = ComboUtils.filterElements(listElements, filterValue);
        } else {
            instance.listCurrentElements = listElements.slice();
        }

        var newValue = instance.value;

        if (!newValue && instance.listCurrentElements.length > 0 && asfProperty.type == _constants.WIDGET_TYPE.listbox) {
            if (asfProperty.default && asfProperty.default.default) {
                newValue = [asfProperty.default.default];
            } else {
                newValue = [instance.listCurrentElements[0].value];
            }
        }

        instance.doSetValue(newValue);
        instance.trigger(_constants.EVENT_TYPE.dataLoad, instance);
    };

    this.addRemovedData = function () {};

    this.getTextValues = function () {
        if (!this.value || this.value.length == 0) {
            return [];
        }

        var textValues = [];

        if (dataLoaded) {
            this.listCurrentElements.forEach(function (element) {
                if (instance.value.indexOf(element.value) > -1) {
                    textValues.push(element.label);
                }
            });
        } else {
            this.asfDataElements.forEach(function (element) {
                if (instance.value.indexOf(element.value) > -1) {
                    textValues.push(element.label);
                }
            });
        }
        return textValues;
    };

    this.getTextValue = function () {
        return this.getTextValues().arrayToString(", ");
    };

    this.getAsfData = function (blockNumber) {
        var v = null;
        var k = null;

        if (asfProperty.type == _constants.WIDGET_TYPE.listbox) {
            if (this.value && this.value.length > 0) {
                k = this.value[0];
                v = instance.getTextValue();
            }
            return dataUtils.getBaseAsfData(asfProperty, blockNumber, v, k);
        } else if (asfProperty.type == _constants.WIDGET_TYPE.radio) {
            if (this.value && this.value.length > 0) {
                v = this.value[0];
                k = instance.getTextValue();
            }
            return dataUtils.getBaseAsfData(asfProperty, blockNumber, v, k);
        } else if (asfProperty.type == _constants.WIDGET_TYPE.check) {
            if (this.value && this.value.length > 0) {
                v = this.value;
                k = instance.getTextValues();
            } else {
                v = [null];
                k = [null];
            }

            var result = dataUtils.getBaseAsfData(asfProperty, blockNumber);
            result.values = v;
            result.keys = k;
            return result;
        }
    };

    // если asfData содержит поле collate то значит значение приходит из сопоставления, и мы не добавляем удаленный элемент в данные списка
    this.setAsfData = function (asfData) {
        // данный массив хранит в себе текущее значение компонента,
        // на случай если он был удален из справочника. 
        // если setASfData вызывают несколько раз, то этот массив должен содержать только последние значения asfData
        instance.asfDataElements = [];
        if (!asfData) {
            return;
        }
        var newValue = null;
        if (asfProperty.type == _constants.WIDGET_TYPE.listbox) {

            if (!asfData.key) {
                return;
            }

            if (!asfData.collate) {
                instance.asfDataElements.push({ value: asfData.key, label: asfData.value });
            }
            newValue = asfData.key;
        } else if (asfProperty.type == _constants.WIDGET_TYPE.radio) {

            if (!asfData.value) {
                return;
            }

            if (!asfData.collate) {
                instance.asfDataElements.push({ value: asfData.value, label: asfData.key });
            }
            newValue = asfData.value;
        } else if (asfProperty.type == _constants.WIDGET_TYPE.check) {

            if (!asfData.values || asfData.values.length == 0 || asfData.values[0] === null) {
                return;
            }

            if (!asfData.collate) {
                if (asfData.values && asfData.values.length > 0) {
                    asfData.values.forEach(function (value, index) {
                        if (value !== null) {
                            var key = "";
                            if (asfData.keys) {
                                key = asfData.keys[index];
                            }
                            instance.asfDataElements.push({ value: value, label: key });
                        }
                    });
                }
            }
            newValue = asfData.values;
        }

        instance.doSetValue(newValue);

        instance.updateModelData();
    };

    this.reloadDictionary = function () {
        var locale = asfProperty.dataSource.locale;
        if (!locale) {
            locale = _constants.OPTIONS.locale;
        }

        playerModel.dictionaryCache.getDictionary(asfProperty.dataSource.dict, locale, function (dictionary) {
            if (dictionary != null) {
                instance.listElements = ComboUtils.extractListElements(dictionary, asfProperty.dataSource.value, asfProperty.dataSource.key, asfProperty.dataSource.filter);
            } else {
                // значит справочник удалили
                instance.listElements = [];
                instance.asfDataElements = [];
            }
            instance.findMaster();
        });
    };

    if (asfProperty.dataSource) {
        instance.reloadDictionary();
    } else if (asfProperty.elements) {
        // берем элементы из asf_definition
        instance.listElements = [];
        instance.listElements.push.apply(instance.listElements, asfProperty.elements);
        instance.findMaster();
    } else {
        // элементы не найдены
        asfProperty.elements = [];
        instance.listElements = [];
        instance.findMaster();
    }
};

ComboBoxModel.prototype = Object.create(_model2.default.prototype);
ComboBoxModel.prototype.constructor = ComboBoxModel;

var ComboUtils = {

    /**
     * объект dictionary переделываем в  объект списка для моделей выбор вариантов комбобокс и переключатель вариантов
     * @param dictionary
     * @param valueField
     * @param titleField
     * @param filterField
     * @returns {Array}
     */
    extractListElements: function extractListElements(dictionary, valueField, titleField, filterField) {
        var listElements = [];
        dictionary.forEach(function (data, index) {
            listElements.push({ value: data[valueField], label: data[titleField], filter: data[filterField] });
        });
        listElements.sort(function (item1, item2) {
            if (item1.value == item2.value) {
                return 0;
            }
            if (item1.value > item2.value) {
                return 1;
            }
            return -1;
        });
        return listElements;
    },

    /**
     * фильтруем компоненты списка и возвращаем новый массив отфильтрованных элементов
     * @param listElements
     * @param filter
     */
    filterElements: function filterElements(listElements, filter) {
        if (filter == null || filter.length == 0) {
            return [];
        }
        return listElements.filter(function (listElement) {
            return filter.indexOf(listElement.filter) > -1;
        });
    },

    validateValueToFilter: function validateValueToFilter(listCurrentElements, valuesToCheck, addIfAbsent) {
        if (listCurrentElements.length == 0) {
            return null;
        } else if (!valuesToCheck || valuesToCheck.length == 0) {
            if (addIfAbsent) {
                return [listCurrentElements[0].value];
            } else {
                return null;
            }
        } else {
            var selectedElements = [];

            listCurrentElements.forEach(function (element) {

                if (valuesToCheck.indexOf(element.value) > -1 && selectedElements.indexOf(element.value) == -1) {
                    selectedElements.push(element.value);
                }
            });
            if (selectedElements.length == 0) {
                selectedElements.push(listCurrentElements[0].value);
            }

            return selectedElements;
        }
    }
};

/***/ }),

/***/ 222:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = CustomComponentModel;

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

var _constants = __webpack_require__(2);

var _model = __webpack_require__(25);

var _model2 = _interopRequireDefault(_model);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function CustomComponentModel(asfProperty, playerModel) {
    _model2.default.call(this, this, asfProperty, playerModel);

    this.customComponent = null;

    var instance = this;

    var asfData = null;

    if (asfProperty.config.component) {
        api.getCustomComponent(asfProperty.config.component, function (component) {
            instance.customComponent = component;
            instance.trigger(_constants.EVENT_TYPE.loadComponentAdditionalInfo, [instance]);
            if (asfData) {
                instance.setAsfData(asfData);
                asfData = null;
            }
        });
    }

    this.setAsfData = function (data) {
        asfData = data;
    };
};

CustomComponentModel.prototype = Object.create(_model2.default.prototype);
CustomComponentModel.prototype.constructor = CustomComponentModel;

/***/ }),

/***/ 223:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = DateModel;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

var _constants = __webpack_require__(2);

var _model = __webpack_require__(25);

var _model2 = _interopRequireDefault(_model);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _dateUtils = __webpack_require__(73);

var dateUtils = _interopRequireWildcard(_dateUtils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Created by exile
 * Date: 15.03.16
 * Time: 11:25
 */

/**
 * дата время
 * asfData:
 * {
            "id": "cmp-pfa3r5",
            "type": "date",
            "value": "2016--маусымның--маусым-15-",
            "key": "2016-06-15 12:07:00"
        }
 * defaultValue:
 *  если только дата
 * {
            "default": "2016-06-15"
        }
 * @param asfProperty
 * @param playerModel
 * @constructor
 */
function DateModel(asfProperty, playerModel) {
    _model2.default.call(this, this, asfProperty, playerModel);

    var textValue = "";
    var locale = dataUtils.getComponentLocale(asfProperty);
    var instance = this;

    var format = asfProperty.config.dateFormat;
    if (!format) {
        format = "${yyyy}-${mm}-${dd}";
        asfProperty.config.dateFormat = format;
    }

    this.getTextValue = function () {
        return textValue;
    };

    this.getSpecialErrors = function () {
        if (instance.value) {
            if (this.asfProperty.required || !dateUtils.isEmpty(instance.value)) {
                var d = dateUtils.parseDate(instance.value);
                if (d == null || !d.isValid()) {
                    return { id: asfProperty.id, errorCode: _constants.INPUT_ERROR_TYPE.wrongValue };
                }
            }
        }
    };

    this.getAsfData = function (blockNumber) {
        var key = null;
        var val = null;
        if (instance.value) {
            var d = dateUtils.parseDate(instance.value);
            if (d != null && d.isValid()) {
                key = instance.value;
                val = instance.getTextValue();
            }
        }

        return dataUtils.getBaseAsfData(asfProperty, blockNumber, val, key);
    };

    this.setAsfData = function (asfData) {
        if (!asfData) {
            return;
        }
        if (asfData.key === '') {
            this.setValue(null);
        } else {
            var newValue = null;
            var d = dateUtils.parseDate(asfData.key);
            if (d != null && d.isValid() && d != 'Invalid Date') {
                newValue = dateUtils.formatDate(d, dateUtils.DATE_FORMAT_FULL);
            }
            textValue = asfData.value;
            this.setValue(newValue);
        }
    };

    this.on(_constants.EVENT_TYPE.valueChange, function () {
        var d = dateUtils.parseDate(instance.value);
        if (d != null && d.isValid()) {
            if (dateUtils.needDictionary(format)) {
                _jquery2.default.when(api.formatDate(instance.value, format, locale)).then(function (data) {
                    textValue = data;
                    instance.trigger(_constants.EVENT_TYPE.dataLoad, instance);
                });
            } else {
                textValue = dateUtils.formatDate(d, format);
                instance.trigger(_constants.EVENT_TYPE.dataLoad, instance);
            }
        } else {
            textValue = '';
            instance.trigger(_constants.EVENT_TYPE.dataLoad, instance);
        }
    });

    if (asfProperty.config['fill-with-current'] && !playerModel.building) {
        instance.setValue(dateUtils.formatDate(new Date(), "${yyyy}-${mm}-${dd} ${HH}:${MM}:${SS}"));
    }
};

DateModel.prototype = Object.create(_model2.default.prototype);
DateModel.prototype.constructor = DateModel;

/***/ }),

/***/ 224:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = DepartmentLinkModel;

var _underscore = __webpack_require__(7);

var _underscore2 = _interopRequireDefault(_underscore);

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

var _constants = __webpack_require__(2);

var _model = __webpack_require__(25);

var _model2 = _interopRequireDefault(_model);

var _componentUtils = __webpack_require__(15);

var compUtils = _interopRequireWildcard(_componentUtils);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _i18n = __webpack_require__(14);

var _i18n2 = _interopRequireDefault(_i18n);

var _UserChooser = __webpack_require__(81);

var _userLinkUtils = __webpack_require__(158);

var userLinkUtils = _interopRequireWildcard(_userLinkUtils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * ссылка на департамент
 * asfData:
 * {
     *        "id": "masterDepartment1",
     *        "type": "entity",
     *        "value": "дочернее подразделение бага 12576",
     *        "key": "4a76ae9b-a460-431a-9edc-c9bf2966f2fb"
     *    }
 *  множественное значение
 * {
     *        "id": "masterDepartment1",
     *        "type": "entity",
     *        "value": "name1;; name1;; name3;; name4",
     *        "key": "identifier1;identifier2;identifier3"
     *    }
 * defaultValue:
 * @param asfProperty
 * @param playerModel
 * @constructor
 */
function DepartmentLinkModel(asfProperty, playerModel) {
    _model2.default.call(this, this, asfProperty, playerModel);
    var instance = this;

    instance.value = [];

    var textValue = "";

    var masterModel = null;

    var textNumber = 0;

    this.getNextTextNumber = function () {
        return textNumber++;
    };

    this.checkNextTextNumber = function () {
        instance.getSelectedIds().forEach(function (id) {
            if (id.startsWith('text-')) {
                textNumber = Math.max(textNumber, id.substring(5)) + 1;
            }
        });
    };

    this.updateDepartmentLinkItem = function (item) {
        if (item.departmentId.indexOf(_UserChooser.USER_CHOOSER_SUFFIXES.text) === 0) {
            item.status = _i18n2.default.tr("Введен вручную");
            item.statusColor = "#df6c6d";
        }
    };

    this.findMaster = function () {
        setTimeout(function () {
            if (asfProperty.config.depends && !playerModel.building) {
                masterModel = compUtils.findMaster(asfProperty, playerModel);
                if (!masterModel) {
                    return;
                }
                masterModel.on(_constants.EVENT_TYPE.valueChange, function () {
                    instance.updateModelData();
                });
            }
        }, 0);
    };

    this.getEntityType = function () {
        return asfProperty.config.entity;
    };

    this.getSelectedIds = function (target) {
        var selectedIds = [];
        if (_underscore2.default.isUndefined(target)) {
            target = instance.value;
        }
        if (target && target instanceof Array) {
            target.forEach(function (department) {
                selectedIds.push(department.departmentId);
            });
        }
        return selectedIds;
    };

    this.setValueFromInput = function (inputValue) {
        this.doSetValue(inputValue);
        playerModel.trigger(_constants.EVENT_TYPE.valueChange, [this, this.value]);
    };

    this.getFilterUserId = function () {
        return userLinkUtils.getFilterEntityId(masterModel, _constants.ENTITY_TYPE.users);
    };

    this.getFilterPositionId = function () {
        return userLinkUtils.getFilterEntityId(masterModel, _constants.ENTITY_TYPE.positions);
    };

    this.getFilterDepartmentId = function () {
        return userLinkUtils.getFilterEntityId(masterModel, _constants.ENTITY_TYPE.departments);
    };

    this.setAsfData = function (asfData) {
        if (!asfData || !asfData.key) {
            return;
        }
        textValue = asfData.value;

        var parsedAsfData = userLinkUtils.parseAsfData(asfData, ";", ";; ");

        var itemIds = parsedAsfData.itemIds;
        var manualTags = parsedAsfData.manualTags;
        var tags = parsedAsfData.tags;

        var selectedValues = [];
        itemIds.forEach(function (itemId, index) {
            var department = {};
            department.departmentId = itemId;
            department.departmentName = manualTags[itemId] || tags[index];
            department.tagName = department.departmentName;
            selectedValues.push(department);
        });
        instance.doSetValue(selectedValues);

        api.getDepartmentsInfo(itemIds, instance.getLocale(), function (departments) {
            selectedValues = selectedValues.slice();
            selectedValues.forEach(function (selectedValue, selectedIndex) {
                var found = departments.findElement("departmentId", selectedValue.departmentId);
                if (found && found.tagName !== found.departmentName) {
                    found.tagName = manualTags[selectedValue.departmentId] || found.departmentName;
                    selectedValues[selectedIndex] = found;
                }
            });
            instance.doSetValue(selectedValues, true, false);
        });
    };

    this.updateModelData = function () {
        var selectedIds = instance.getSelectedIds();

        var filterUserID = instance.getFilterUserId();
        var filterPositionID = instance.getFilterPositionId();
        var filterDepartmentID = instance.getFilterDepartmentId();

        if (!filterUserID && !filterPositionID && !filterDepartmentID) {
            instance.doSetValue(null);
            return;
        }

        api.checkDepartments(selectedIds, filterUserID, filterPositionID, filterDepartmentID, instance.getLocale(), function (departments) {
            if (departments && departments.length > 0) {
                departments[0].tagName = departments[0].departmentName;
            }
            instance.doSetValue(departments, true, true);
            instance.trigger(_constants.EVENT_TYPE.dataLoad, instance);
        });
    };

    this.doSetValue = function (newValue) {
        var fireEvent = true;
        if (this.value == newValue || instance.getSelectedIds().equals(instance.getSelectedIds(newValue))) {
            // ничего не делаем если новое значение такое же как и старое
            // даже событие отправлять не будем,
            fireEvent = false;
        }

        if (!newValue) {
            this.value = [];
        } else if (!(newValue instanceof Array)) {
            this.value = [newValue];
        } else {
            this.value = newValue;
        }

        textValue = "";
        var sign = "";
        this.value.forEach(function (val) {
            instance.updateDepartmentLinkItem(val);
            textValue += sign + val.tagName;
            sign = ", ";
        });

        if (fireEvent) {
            instance.fireChangeEvents();
        } else {
            instance.trigger(_constants.EVENT_TYPE.dataLoad, [instance]);
        }

        instance.checkNextTextNumber();
    };

    this.getTextValue = function () {
        return textValue;
    };

    this.getKey = function () {
        var array = instance.getSelectedIds();
        if (array.length === 0) {
            return null;
        } else {
            return array.arrayToString(";");
        }
    };

    this.getManualTags = function () {
        var manualTags = {};
        this.value.forEach(function (val) {
            if (val.tagName != val.departmentName) {
                manualTags[val.departmentId] = val.tagName;
            }
        });
        return manualTags;
    };

    this.getAsfData = function (blockNumber) {

        var text = null;
        var key = instance.getKey();
        if (key) {
            text = "";
            var sign = "";
            this.value.forEach(function (val) {
                text += sign + val.tagName;
                sign = ";; ";
            });
        }

        var base = dataUtils.getBaseAsfData(asfProperty, blockNumber, text, key);
        base.manualTags = instance.getManualTags();
        base.formatVersion = "V1";
        return base;
    };

    this.findMaster();
};

DepartmentLinkModel.prototype = Object.create(_model2.default.prototype);
DepartmentLinkModel.prototype.constructor = DepartmentLinkModel;

/***/ }),

/***/ 225:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = DocAttributeModel;

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

var _constants = __webpack_require__(2);

var _model = __webpack_require__(25);

var _model2 = _interopRequireDefault(_model);

var _i18n = __webpack_require__(14);

var _i18n2 = _interopRequireDefault(_i18n);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

/**
 * свойства документа
 * @param asfProperty
 * @param playerModel
 * @constructor
 */
function DocAttributeModel(asfProperty, playerModel) {
    _model2.default.call(this, this, asfProperty, playerModel);

    var instance = this;

    var field = asfProperty.config.field;
    if (!field) {
        field = _constants.DOC_ATTRIBUTE_FIELD.number;
    }
    var locale = _constants.OPTIONS.locale;
    if (asfProperty.config && asfProperty.config.locale) {
        locale = asfProperty.config.locale;
    }

    var text = _i18n2.default.tr("Номер");

    if (asfProperty.config && asfProperty.config.field) {
        switch (asfProperty.config.field) {
            case _constants.DOC_ATTRIBUTE_FIELD.subject:
                {
                    text = _i18n2.default.tr("Краткое содержание");
                    break;
                }
            case _constants.DOC_ATTRIBUTE_FIELD.createDate:
                {
                    text = _i18n2.default.tr("Дата создания");
                    break;
                }
            case _constants.DOC_ATTRIBUTE_FIELD.author:
                {
                    text = _i18n2.default.tr("Автор");
                    break;
                }
            case _constants.DOC_ATTRIBUTE_FIELD.reg_date:
                {
                    text = _i18n2.default.tr("Дата регистрации");
                    break;
                }
            case _constants.DOC_ATTRIBUTE_FIELD.doc_type:
                {
                    text = _i18n2.default.tr("Тип документа");
                    break;
                }
            case _constants.DOC_ATTRIBUTE_FIELD.registry:
                {
                    text = _i18n2.default.tr("Реестр");
                    break;
                }
            default:
                {
                    text = _i18n2.default.tr("Номер");
                    break;
                }
        }
    }

    this.getAttributeTitle = function () {
        return text;
    };

    this.setAsfData = function (asfData) {
        var dataUUID = playerModel.asfDataId;

        if (dataUUID) {
            api.getDocAttribute(dataUUID, field, locale, function (data) {
                instance.value = data;
                instance.trigger(_constants.EVENT_TYPE.dataLoad, instance);
            });
        }
    };

    this.setAsfData();
};

DocAttributeModel.prototype = Object.create(_model2.default.prototype);
DocAttributeModel.prototype.constructor = DocAttributeModel;

/***/ }),

/***/ 226:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = DocLinkModel;

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

var _constants = __webpack_require__(2);

var _model = __webpack_require__(25);

var _model2 = _interopRequireDefault(_model);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _i18n = __webpack_require__(14);

var _i18n2 = _interopRequireDefault(_i18n);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

/**
 * ссылка на документ (основание)
 * @param asfProperty
 * @param playerModel
 * @constructor
 */

function DocLinkModel(asfProperty, playerModel) {
    _model2.default.call(this, this, asfProperty, playerModel);

    var instance = this;
    var textValue = "";
    this.subject = _i18n2.default.tr("Не указан");

    this.documentBases = [];

    this.getTextValue = function () {
        return textValue;
    };
    var format = asfProperty.config ? asfProperty.config.format : undefined;
    var locale = dataUtils.getComponentLocale(asfProperty);

    this.on(_constants.EVENT_TYPE.valueChange, function () {
        instance.updateTextValue();
    });

    this.updateTextValue = function () {
        if (instance.value) {
            instance.documentBases.forEach(function (docBase) {
                if (docBase.id === instance.value) {
                    instance.subject = docBase.title;
                }
            });
            if (!format) {
                textValue = "";
                instance.trigger(_constants.EVENT_TYPE.dataLoad, instance);
            } else {
                api.getDocLinkFormatted(instance.value, format, locale, function (data) {
                    textValue = data;
                    instance.trigger(_constants.EVENT_TYPE.dataLoad, instance);
                });
            }
        } else {
            instance.subject = _i18n2.default.tr("Не указан");
            textValue = '';
            instance.trigger(_constants.EVENT_TYPE.dataLoad, instance);
        }
    };

    this.setAsfData = function (asfData) {
        var newValue = null;
        if (asfData) {
            newValue = asfData.value;
        }
        instance.doSetValue(newValue);
        instance.updateBases();
    };

    this.updateBases = function () {

        if (!playerModel.asfDataId) {
            return;
        }

        api.getDocumentBases(playerModel.asfDataId, dataUtils.getComponentLocale(asfProperty), function (bases) {
            var exist = false;

            instance.documentBases = [];

            instance.documentBases.push({ id: "", title: _i18n2.default.tr("Не указан") });
            bases.forEach(function (docBase) {
                if (instance.value === docBase.documentID) {
                    exist = true;
                }
                instance.documentBases.push({ id: docBase.documentID, title: docBase.subject });
            });

            if (exist) {
                instance.setValue(instance.value);
            } else {
                instance.setValue(null);
            }

            instance.updateTextValue();
        });
    };

    this.updateBases();
};

DocLinkModel.prototype = Object.create(_model2.default.prototype);
DocLinkModel.prototype.constructor = DocLinkModel;

/***/ }),

/***/ 227:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = FileLinkModel;

var _underscore = __webpack_require__(7);

var _underscore2 = _interopRequireDefault(_underscore);

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

var _constants = __webpack_require__(2);

var _model = __webpack_require__(25);

var _model2 = _interopRequireDefault(_model);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _i18n = __webpack_require__(14);

var _i18n2 = _interopRequireDefault(_i18n);

var _unknown = __webpack_require__(1005);

var _unknown2 = _interopRequireDefault(_unknown);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * файл
 * asfData:
 * {
     *       "id": "cmp-5hpcpy",
            "type": "filelink",
            "value": "4444.htd.pdf",
            "key": "36afa2d9-cd78-4638-8132-773a69ff0c55"
     *    }
 * defaultValue:
 * {
     *       "id": "cmp-5hpcpy",
     *        "type": "filelink",
     *        "value": "4444.htd.pdf",
     *        "key": "36afa2d9-cd78-4638-8132-773a69ff0c55"
     *    }
 * @param asfProperty
 * @param playerModel
 * @constructor
 */
function FileLinkModel(asfProperty, playerModel) {
    _model2.default.call(this, this, asfProperty, playerModel);

    var instance = this;

    this.asfProperty = asfProperty;

    var newWindow = asfProperty.config['open-in-new-window'];

    newWindow = _underscore2.default.isBoolean(newWindow) && newWindow;
    this.newWindow = newWindow;

    this.setDefaultValue = function (defaultValue) {
        var value = { identifier: defaultValue.key, name: defaultValue.value, icon: api.getFullUrl('rest/storage/icons/get?aiType=ai_image&fileName=' + encodeURIComponent(defaultValue.value) + '&format') };
        instance.doSetValue(value);
        this.updateFileInfo(value.identifier);
    };

    this.getTextValue = function () {
        if (instance.getValue()) {
            return instance.getValue().name;
        }
        return "";
    };

    this.getAsfData = function (blockNumber) {
        var value = "";
        var key = null;
        if (this.value) {
            value = this.value.name;
            key = this.value.identifier;
        }
        return dataUtils.getBaseAsfData(asfProperty, blockNumber, value, key);
    };

    this.setAsfData = function (asfData) {
        if (!asfData || !asfData.key) {
            return;
        }

        var value = { identifier: asfData.key, name: asfData.value,
            icon: _unknown2.default, path: _i18n2.default.tr("Доступ к файлу запрещен") };
        this.setValue(value);
        this.updateFileInfo(value.identifier);
    };

    this.updateFileInfo = function (id) {
        if (id) {
            api.getFileInfo(id, function (data) {
                instance.setValue(data);
            }, function (data) {
                if (data.errorCode == '13') {

                    if (data.errorMessage == 'Недостаточно прав для выполнения действия') {
                        var value = {
                            identifier: instance.getValue().identifier,
                            name: instance.getValue().name,
                            icon: _unknown2.default,
                            path: _i18n2.default.tr("Доступ к файлу запрещен"),
                            accessDenied: true
                        };
                        instance.setValue(value);
                    } else if (data.errorMessage == "Элемент был удален") {
                        var value = {
                            identifier: instance.getValue().identifier,
                            name: instance.getValue().name,
                            icon: _unknown2.default,
                            path: _i18n2.default.tr("Файл был удален"),
                            deleted: true
                        };
                        instance.setValue(value);
                    } else {
                        var value = {
                            identifier: instance.getValue().identifier,
                            name: instance.getValue().name,
                            icon: _unknown2.default,
                            path: _i18n2.default.tr("Доступ к файлу запрещен"),
                            accessDenied: true
                        };
                        instance.setValue(value);
                    }
                }
            });
        } else {
            instance.doSetValue(null);
        }
    };

    this.setValue = function (value) {
        if (instance.value && value && instance.value.identifier === value.identifier) {
            instance.value = value;
            instance.trigger(_constants.EVENT_TYPE.dataLoad, instance);
            return;
        }
        instance.doSetValue(value);
    };
};

FileLinkModel.prototype = Object.create(_model2.default.prototype);
FileLinkModel.prototype.constructor = FileLinkModel;

/***/ }),

/***/ 228:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = FileModel;

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

var _model = __webpack_require__(25);

var _model2 = _interopRequireDefault(_model);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

/**
 * файл
 * asfData:
 * {
     *        "id": "cmp-d84aev",
     *        "type": "file",
     *        "value": "1 7.png",
     *        "key": "14064b88-633c-4748-b9c6-9fbf8c8a86e6"
     *    }
 * defaultValue:
 * @param asfProperty
 * @param playerModel
 * @constructor
 */
function FileModel(asfProperty, playerModel) {
    _model2.default.call(this, this, asfProperty, playerModel);

    this.asfProperty = asfProperty;
    this.showFullPath = asfProperty.config.showFullPath;
    this.showContent = asfProperty.config.showContent;
    this.readOnly = asfProperty.config['read-only'];

    var instance = this;

    this.updateFileInfo = function (id, filePath) {
        if (id) {
            api.getFileInfo(id, function (data) {
                data.fromStorage = false;
                data.path = "";
                if (filePath) {
                    data.fromStorage = true;
                    data.path = filePath;
                }
                instance.doSetValue(data);
            });
        } else {
            instance.doSetValue(null);
        }
    };

    this.setValueFromInput = function (id, filePath) {
        if (this.value != null) {
            instance.updateFileInfo(id, filePath);
        } else {
            instance.updateFileInfo(id, filePath);
        }
    };

    this.getAsfData = function (blockNumber) {
        var key = null;
        var value = "";
        if (instance.value) {
            key = instance.value.identifier;
            value = instance.value.name;
        }
        var asfData = dataUtils.getBaseAsfData(asfProperty, blockNumber, value, key);
        if (instance.value && instance.value.fromStorage) {
            asfData.valueID = instance.value.path;
        }
        return asfData;
    };

    this.setAsfData = function (asfData, nodeId) {
        if (!asfData || !asfData.key) {
            return;
        }
        var value = { identifier: asfData.key, name: asfData.value };
        if (asfData.valueID) {
            value.fromStorage = true;
            value.path = asfData.valueID;
        }
        instance.doSetValue(value);
    };
};

FileModel.prototype = Object.create(_model2.default.prototype);
FileModel.prototype.constructor = FileModel;

/***/ }),

/***/ 229:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = History;

var _constants = __webpack_require__(2);

var _componentUtils = __webpack_require__(15);

var _utils = __webpack_require__(82);

var _builderUtils = __webpack_require__(156);

var _layoutUtils = __webpack_require__(65);

var layoutUtils = _interopRequireWildcard(_layoutUtils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function History(playerView) {
    var instance = this;
    var items = [];
    var last = -1;
    var max = -1;

    (0, _componentUtils.makeBus)(this);

    function triggerChange() {
        instance.trigger(_constants.EVENT_TYPE.historyChange);
    }

    instance.shift = function () {
        items.shift();
        last--;
        max--;
    };

    instance.addItem = function (item) {
        if (last === History.MAX_LENGTH - 1) instance.shift();
        last++;
        items[last] = item;
        max = last;

        triggerChange();
    };

    instance.goBack = function () {
        try {
            if (!instance.canGoBack()) return;
            var result = items[last];
            last--;
            return result;
        } finally {
            triggerChange();
        }
    };

    instance.goForward = function () {
        try {
            if (!instance.canForward()) return;
            last++;
            return items[last];
        } finally {
            triggerChange();
        }
    };

    instance.canForward = function () {
        return last < max;
    };
    instance.canGoBack = function () {
        return last >= 0;
    };

    instance.redo = function () {
        if (!instance.canForward()) return;

        var item = instance.goForward();
        if (item) item.redo();
    };

    instance.undo = function () {
        if (!instance.canGoBack()) return;

        var item = instance.goBack();
        if (item) item.undo();
    };

    function highlight($el, after) {
        if (!$el || $el.length === 0) return;
        $el.removeClass('asf-change-transition');
        $el.addClass('asf-change');

        setTimeout(function () {
            $el.addClass('asf-change-transition');
            $el.removeClass('asf-change');

            setTimeout(function () {
                $el.removeClass('asf-change-transition');
                if (_.isFunction(after)) after();
            }, 500);
        }, 750);
    }

    function highlightCell(tView, cell, after) {
        var $td = tView.findTd(cell);
        if ($td) highlight($td, after);
    }

    function highlightRow(tView, row, after) {
        highlight(tView.findTr(row), after);
    }

    function highlightTable(tView) {
        if (!tView) return;
        highlight(tView.getTable().find(' > tbody > tr > td'));
    }

    function highlightColumn(tView, column) {
        var rows = tView.getRowsCount();
        for (var row = 0; row < rows; row++) {
            var $td = tView.findTd([row, column]);
            if ($td) highlight($td);
        }
    }

    function _undo(obj) {
        obj.undo();
    }

    function _redo(obj) {
        obj.redo();
    }

    instance.addItems = function (items) {
        instance.addItem(instance.chainItems(items));
    };

    instance.chainItems = function (items) {
        if (!_.isArray(items)) return null;
        if (items.length === 1) return items[0];
        return {
            undo: function undo() {
                _.each((0, _utils.reverse)(items), _undo);
            },
            redo: function redo() {
                _.each(items, _redo);
            }
        };
    };

    instance.createInsertItem = function (prop, cell, tableId) {
        if (prop) prop.fromHistory = true;

        return {
            redo: function redo() {
                var tableView = playerView.getViewWithId(tableId);
                tableView.model.selection.clear();
                tableView.model.paste(prop, cell[0], cell[1]);

                highlightCell(tableView, cell);
            },

            undo: function undo() {
                var tableView = playerView.getViewWithId(tableId);
                var tableModel = tableView.model;
                tableModel.selection.clear();

                var dynTableId = tableModel.asfProperty.id;
                if (!tableModel.isDynamic()) {
                    dynTableId = null;
                }

                var model = tableModel.getModelWithId(prop.id, dynTableId);
                tableModel.deleteModel(model);

                highlightCell(tableView, cell);
            }
        };
    };

    function reverse(item) {
        return {
            undo: item.redo,
            redo: item.undo
        };
    }

    instance.createDeleteItem = function (prop, cell, tableId) {
        return reverse(instance.createInsertItem(prop, cell, tableId));
    };

    instance.createRowItem = function (row, tableId) {
        return {
            undo: function undo() {
                var tableView = playerView.getViewWithId(tableId);
                tableView.model.deleteTableRow(row);
            },

            redo: function redo() {
                var tableView = playerView.getViewWithId(tableId);
                tableView.model.addTableRow(row);

                highlightRow(tableView, row);
            }
        };
    };

    instance.deleteRowItem = function (row, tableId) {
        return reverse(instance.createRowItem(row, tableId));
    };

    instance.createColumnItem = function (column, tableId) {
        return {
            undo: function undo() {
                var tableView = playerView.getViewWithId(tableId);
                tableView.model.deleteTableColumn(column);
            },
            redo: function redo() {
                var tableView = playerView.getViewWithId(tableId);
                tableView.model.addTableColumn(column);

                highlightColumn(tableView, column);
            }
        };
    };

    instance.deleteColumnItem = function (column, tableId) {
        return reverse(instance.createColumnItem(column, tableId));
    };

    function findView(id, tableId, isPage) {
        if (isPage) {
            return playerView.getViewWithId(id);
        } else {
            return playerView.getViewWithId(id, tableId);
        }
    }

    instance.settingsItem = function (oldProp, newProp, tableId, isPage) {
        return {
            undo: function undo() {
                var tView = playerView.getViewWithId(tableId);
                var view = findView(newProp.id, tableId, isPage);

                (0, _builderUtils.componentSettingsChanged)(oldProp, view.model, tView.model, playerView);

                var cell = tView.getViewCoords(view);
                highlightCell(tView, cell);
            },

            redo: function redo() {
                var tView = playerView.getViewWithId(tableId);
                var view = findView(oldProp.id, tableId, isPage);

                (0, _builderUtils.componentSettingsChanged)(newProp, view.model, tView.model, playerView);

                var cell = tView.getViewCoords(view);
                highlightCell(tView, cell);
            }
        };
    };

    instance.createStyleItem = function (oldStyle, newStyle, id, tableId, isPage) {
        function applyStyle(style) {
            var tView = playerView.getViewWithId(tableId);
            tView.model.selection.clear();
            var view = findView(id, tableId, isPage);
            if (view) {
                view.model.asfProperty.style = style;
                view.applyStyle(style);
                var cell = tView.getViewCoords(view);
                if (cell) highlightCell(tView, cell);
            }
        }

        return {
            redo: applyStyle.bind(null, newStyle),
            undo: applyStyle.bind(null, oldStyle)
        };
    };

    instance.layoutItem = function (oldLayout, newLayout, tableId) {
        function changeLayout(layout) {
            var tView = playerView.getViewWithId(tableId);
            var currentLayout = tView.model.asfProperty.layout;

            var changedCells = layoutUtils.difference(layout, currentLayout).concat(layoutUtils.difference(currentLayout, layout));
            changedCells = _.map(changedCells, _layoutUtils.compToCell);

            _.extend(currentLayout, layout);
            tView.model.trigger(_constants.EVENT_TYPE.tableDomUpdate);

            _.map(changedCells, highlightCell.bind(null, tView));
        }

        return {
            undo: changeLayout.bind(null, oldLayout),
            redo: changeLayout.bind(null, newLayout)
        };
    };

    instance.columnWidthChange = function (oldWidth, newWidth, column, tableId) {
        function changeWidth(width) {
            var tView = playerView.getViewWithId(tableId);
            layoutUtils.changeColumnWidth(column, width, tView.model.asfProperty.layout);
            tView.layoutChanged();
            highlightColumn(tView, column);
        }

        return {
            undo: changeWidth.bind(null, oldWidth),
            redo: changeWidth.bind(null, newWidth)
        };
    };

    function findLastPage() {
        return _.chain(playerView.views).filter(function (view) {
            return view.model.isPage();
        }).reduce(function (last, view) {
            return last.model.pageNumber < view.model.pageNumber ? view : last;
        }).value();
    }

    instance.createPage = function (initLayout) {
        var layout = (0, _utils.clone)(initLayout);
        return {
            undo: function undo() {
                var lastPage = findLastPage();
                playerView.model.deletePage(lastPage.model, true);
            },
            redo: function redo() {
                playerView.insertComponentTo({
                    type: _constants.WIDGET_TYPE.page,
                    layout: layout
                }, true);
                var tView = findLastPage();
                highlightTable(tView);
            }
        };
    };
    instance.deletePage = function (layout) {
        return reverse(instance.createPage(layout));
    };
};

History.MAX_LENGTH = 50;
History.prototype.constructor = History;

/***/ }),

/***/ 23:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(jQuery) {

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.showUserChooserDialog = showUserChooserDialog;
exports.showDepartmentChooserDialog = showDepartmentChooserDialog;
exports.showPositionChooserDialog = showPositionChooserDialog;
exports.showEditLinkDialog = showEditLinkDialog;
exports.showFileLinkDialog = showFileLinkDialog;
exports.openFile = openFile;
exports.openFilePath = openFilePath;
exports.showRegisterLinkDialog = showRegisterLinkDialog;
exports.showProjectLinkDialog = showProjectLinkDialog;
exports.showAddressBookDialog = showAddressBookDialog;
exports.showCreateFileDialog = showCreateFileDialog;
exports.showUploadFileDialog = showUploadFileDialog;
exports.showDropDown = showDropDown;
exports.showSuggestOracle = showSuggestOracle;
exports.closeDropDown = closeDropDown;
exports.isShownDropDown = isShownDropDown;
exports.showDatePicker = showDatePicker;
exports.showTimePicker = showTimePicker;
exports.showDaysChooser = showDaysChooser;
exports.openLink = openLink;
exports.parseUrl = parseUrl;
exports.showFileMenu = showFileMenu;
exports.showPlan = showPlan;
exports.showPortfolio = showPortfolio;
exports.showDocument = showDocument;
exports.showAddressBookItem = showAddressBookItem;
exports.showErrorMessage = showErrorMessage;
exports.showWaitWindow = showWaitWindow;
exports.hideWaitWindow = hideWaitWindow;
exports.unAuthorized = unAuthorized;
/**
 * Сервисы Arta Synergy
 * можно переписать на свои, открытие всяческих диалогов, переходы по ссылкам
 * @type {{showAddressBookDialog: showAddressBookDialog, showUserChooserDialog: showUserChooserDialog, showDepartmentChooserDialog: showDepartmentChooserDialog, showPositionChooserDialog: showPositionChooserDialog, showEditLinkDialog: showEditLinkDialog, openAddressBook: openAddressBook}}
 */
function showUserChooserDialog(values, multiSelectable, isGroupSelectable, showWithoutPosition, filterPositionID, filterDepartmentID, locale, handler) {

    var userChooser = new AS.FORMS.UserChooser(multiSelectable, isGroupSelectable, locale);
    userChooser.setFilterDepartmentID(filterDepartmentID);
    userChooser.setFilterPositionID(filterPositionID);
    userChooser.setShowNoPosition(showWithoutPosition);
    userChooser.setSelectedItems(values);
    userChooser.showDialog();
    userChooser.on(AS.FORMS.BasicChooserEvent.applyClicked, function () {
        handler(userChooser.getSelectedItems());
    });
}
function showDepartmentChooserDialog(values, multiSelectable, filterUserID, filterPositionID, filterDepartmentID, filterChildDepartmentID, locale, handler) {
    var depChooser = new AS.FORMS.DepartmentChooser(multiSelectable, locale);
    depChooser.setFilterPositionID(filterPositionID);
    depChooser.setFilterUserID(filterUserID);
    depChooser.setFilterDepartmentID(filterDepartmentID);
    depChooser.setSelectedItems(values);
    depChooser.showDialog();
    depChooser.on(AS.FORMS.BasicChooserEvent.applyClicked, function () {
        handler(depChooser.getSelectedItems());
    });
}
function showPositionChooserDialog(values, multiSelect, filterUserId, filterDepartmentId, showVacant, locale, handler) {

    var posChooser = new AS.FORMS.PositionChooser(multiSelect, locale);
    posChooser.setFilterDepartmentID(filterDepartmentId);
    posChooser.setFilterUserID(filterUserId);
    posChooser.setShowVacant(showVacant);
    posChooser.setShowAll(true);
    posChooser.setSelectedItems(values);
    posChooser.showDialog();
    posChooser.on(AS.FORMS.BasicChooserEvent.applyClicked, function () {
        handler(posChooser.getSelectedItems());
    });
}
function showEditLinkDialog(url, title, openInNew, handler) {

    var linkDialog = new AS.FORMS.LinkEditDialog();
    linkDialog.setData(title, url, openInNew);
    linkDialog.showDialog();
    linkDialog.on(AS.FORMS.BasicChooserEvent.applyClicked, function () {
        handler(linkDialog.getUrl(), linkDialog.getLabel(), linkDialog.getOpenInNew());
    });
}

function showFileLinkDialog(multiSelect, showHomeFolder, mimeTypes, handler) {
    var fileChooser = new AS.FORMS.FileChooser(multiSelect, showHomeFolder, mimeTypes);
    fileChooser.showDialog();
    fileChooser.on(AS.FORMS.BasicChooserEvent.applyClicked, function () {
        handler(fileChooser.getSelectedItems());
    });
}
function openFile(id, newWindow) {
    console.log('using default opening method');
    var hash = '#' + jQuery.param({
        submodule: 'common',
        action: 'open_file',
        file_identifier: id
    });
    window.open(AS.FORMS.ApiUtils.getFullUrl('Synergy.html' + hash));
}
function openFilePath(id, newWindow) {
    "use strict";

    console.log('using default opening method');
    var hash = '#' + jQuery.param({
        submodule: 'common',
        action: 'open_path',
        file_identifier: id });
    window.open('/Synergy/Synergy.html' + hash);
}
function showRegisterLinkDialog(registry, handler) {
    var registerDialog = new AS.FORMS.RegistryLinkChooser(false, registry);
    registerDialog.showDialog();

    //registerDialog.listenTo(registerDialog, 'registrylink:submit:clicked', function (registerModel, columnCollection) {
    //    registerDialog.destroy();
    //    handler(registerModel.uuid);
    //});
    registerDialog.on(AS.FORMS.BasicChooserEvent.applyClicked, function () {
        handler(registerDialog.getSelectedItems()[0].uuid);
    });
}
function showProjectLinkDialog(handler) {
    var projectChooser = new AS.FORMS.ProjectChooser(false);
    projectChooser.showDialog();
    projectChooser.on(AS.FORMS.BasicChooserEvent.applyClicked, function () {
        handler(projectChooser.getSelectedItems());
    });
}
function showAddressBookDialog(handler) {
    var addressDialog = new AS.Chooser.AddressLink.DialogLayout({});
    addressDialog.render();
    var dialog = jQuery(addressDialog.el).dialog({
        modal: true,
        width: 800,
        resizable: false,
        close: function close(event, ui) {
            dialog.dialog('destroy').remove();
        },
        title: i18n.tr("Выбор записи адресной книги")
    }).addClass("as-chooser").addClass("addresslink");

    addressDialog.listenTo(addressDialog, 'addresslink:submit:clicked', function (model) {
        addressDialog.destroy();
        handler(model.itemID, model.name, model.itemType, model.address, model.organization);
    });
}
function showCreateFileDialog(nodeId, dataId, handler) {
    var newFileView = new AS.FORMS.NewFileView({
        dataId: dataId,
        nodeId: nodeId
    });
    newFileView.render().show();
    newFileView.on('file:created', handler);
}
function showUploadFileDialog(nodeId, dataId, mimeTypes, handler) {

    var uploadModel = new AS.FORMS.UploadFileModel({
        dataId: dataId,
        nodeId: nodeId
    });
    var fileChooserView = new AS.FORMS.FileUploadView({
        model: uploadModel,
        mimeTypes: mimeTypes
    });
    uploadModel.on('change:id', function () {
        "use strict";

        fileChooserView.remove();
        handler(uploadModel.toJSON().id);
    });
    fileChooserView.render().show();
}
/**
 *
 * @param values массив значений [{value : "value1", title : "title1", selected : true}, {value : "value2", title : "title2"}, {value : "value3", title : "title3"}]
 * @param anchor якорный компонент, к которому следует привязать попап
 * @param minWidth минимальная ширина
 * @param handler выделенное значение
 */
function showDropDown(values, anchor, minWidth, handler) {
    AS.FORMS.dropDownMenu.show(values, anchor, minWidth, handler);
    return AS.FORMS.dropDownMenu;
}
function showSuggestOracle(values, anchor, minWidth, handler) {
    AS.FORMS.dropDownMenu.show(values, anchor, minWidth, handler);
    return AS.FORMS.dropDownMenu;
}
function closeDropDown(anchor) {
    AS.FORMS.dropDownMenu.hide(anchor);
}
function isShownDropDown(anchor) {
    return AS.FORMS.dropDownMenu.isShownDropDown(anchor);
}
function showDatePicker(value, anchor, input, handler) {
    AS.FORMS.calendarPopup.showDatePopup(value, anchor, input, handler);
}
function showTimePicker(value, anchor, input, handler) {}
function showDaysChooser(type, values, anchor, handler) {
    AS.FORMS.daysChooserPopup.showDaysPopup(type, values, anchor, handler);
}
function openLink(url, openInNew) {
    if (url.startsWith("http://") || url.startsWith("https://")) {
        if (openInNew) {
            window.open(url, "_blank");
        } else {
            window.location = url;
        }
    } else {
        if (openInNew) {
            url = AS.FORMS.ApiUtils.getFullUrl(url);
            window.open(url);
        } else {
            window.location.hash = url;
        }
    }
}
function parseUrl() {
    "use strict";

    var match,
        pl = /\+/g,
        // Regex for replacing addition symbol with a space
    search = /([^&=]+)=?([^&]*)/g,
        decode = function decode(s) {
        return decodeURIComponent(s.replace(pl, " "));
    },
        query = window.location.search.substring(1);

    var urlParams = {};
    while (match = search.exec(query)) {
        urlParams[decode(match[1])] = decode(match[2]);
    }AS.SERVICES.urlParams = urlParams;
}
function showFileMenu(anchor, newHandler, compHandler, storeHandler, hideCreateNew) {
    var items = [];
    items.push({ value: "comp", title: i18n.tr("С компьютера") });
    items.push({ value: "store", title: i18n.tr("Из хранилища") });
    if (!hideCreateNew) {
        items.push({ value: "new", title: i18n.tr("Создать новый") });
    }
    AS.SERVICES.showDropDown(items, anchor, 100, function (option) {
        if (option == 'new') {
            newHandler();
        } else if (option == 'comp') {
            compHandler();
        } else if (option == 'store') {
            storeHandler();
        }
    });
}
function showPlan(elementId) {
    var hash = '#' + jQuery.param({
        submodule: 'plans',
        action: 'open_project',
        project_identifier: elementId });

    window.location.hash = hash;
}
function showPortfolio(elementId) {
    var hash = '#' + jQuery.param({
        submodule: 'plans',
        action: 'open_portfolio',
        objectID: elementId });
    window.location.hash = hash;
}
function showDocument(elementId) {
    var hash = '#' + jQuery.param({
        submodule: 'common',
        action: 'open_document',
        document_identifier: elementId });
    window.location.hash = hash;
}
function showAddressBookItem(elementId, objectType) {
    var hash = '#' + jQuery.param({
        submodule: 'address_book',
        action: 'show_contact',
        objectType: objectType,
        objectID: elementId });
    window.location.hash = hash;
}
function showErrorMessage(message) {}
function showWaitWindow(message) {}
function hideWaitWindow(message) {}
function unAuthorized() {}
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ }),

/***/ 230:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(jQuery) {

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = ImageModel;

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

var _model = __webpack_require__(25);

var _model2 = _interopRequireDefault(_model);

var _emptyImage = __webpack_require__(379);

var _emptyImage2 = _interopRequireDefault(_emptyImage);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

/**
 * Created by exile
 * Date: 03.02.16
 * Time: 16:25
 */
/**
 * модель статического рисунка
 * @param asfProperty
 * @constructor
 */
function ImageModel(asfProperty, playerModel) {
    _model2.default.call(this, this, asfProperty, playerModel);

    this.getHTMLValue = function () {
        var url = _emptyImage2.default;
        if (asfProperty.config && asfProperty.config.url) {
            url = api.getFullUrl(asfProperty.config.url);
        }
        return jQuery("<image>", { "src": url })[0].outerHTML;
    };
};

ImageModel.prototype = Object.create(_model2.default.prototype);
ImageModel.prototype.constructor = ImageModel;
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ }),

/***/ 231:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = LabelModel;

var _model = __webpack_require__(25);

var _model2 = _interopRequireDefault(_model);

var _componentUtils = __webpack_require__(15);

var compUtils = _interopRequireWildcard(_componentUtils);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _styleUtils = __webpack_require__(21);

var styleUtils = _interopRequireWildcard(_styleUtils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Created by exile
 * Date: 03.02.16
 * Time: 16:25
 */

/**
 * модель подписи
 * @param asfProperty
 * @constructor
 */
function LabelModel(asfProperty, playerModel) {
    _model2.default.call(this, this, asfProperty, playerModel);
    this.setValue(compUtils.getCurrentTranslation(asfProperty));

    this.getHTMLValue = function () {
        var label = compUtils.createInlineStyledLabel(this.getTextValue(), asfProperty.style);
        if (asfProperty.style && asfProperty.style.width) {
            var width = styleUtils.getSize(asfProperty.style.width);
            label.css("display", "inline-block");
            label.css("width", width);
        }
        return label[0].outerHTML;
    };

    this.getAsfData = function (blockNumber) {
        return dataUtils.getBaseAsfData(asfProperty, blockNumber);
    };

    this.setAsfData = function () {};

    this.isEmpty = function () {
        return true;
    };
};

LabelModel.prototype = Object.create(_model2.default.prototype);
LabelModel.prototype.constructor = LabelModel;

/***/ }),

/***/ 232:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = LinkModel;

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

var _constants = __webpack_require__(2);

var _model = __webpack_require__(25);

var _model2 = _interopRequireDefault(_model);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

/**
 * asfData:
 * {
     *        "id": "cmp-7u4wuv",
     *        "type": "link",
     *        "value": "#submodule=common&action=open_document&document_identifier=6e39c180-32bf-11e6-a3ae-3085a93a6496",
     *        "key": "Документ; false"
     *    }
 * defaultValue: тоже самое что и asfData но сериализованное
 * @param asfProperty
 * @param playerModel
 * @constructor
 */
function LinkModel(asfProperty, playerModel) {

    _model2.default.call(this, this, asfProperty, playerModel);
    this.label = "";
    this.openInNew = false;
    var instance = this;

    this.getTextValue = function () {
        return instance.label;
    };

    this.isOpenInNew = function () {
        return instance.openInNew;
    };

    this.getAsfData = function (blockNumber) {
        return dataUtils.getBaseAsfData(asfProperty, blockNumber, instance.value, instance.label + "; " + instance.openInNew);
    };

    this.setValueFromInput = function (newUrl, newTitle, newOpenInNew) {
        var v = { id: asfProperty.id, type: _constants.WIDGET_TYPE.link, value: newUrl, key: newTitle + "; " + newOpenInNew };
        instance.setValue(v);
    };

    this.doSetValue = function (newValue) {
        if (!newValue) {
            return;
        }

        this.value = newValue.value;
        this.label = newValue.key || '';

        this.openInNew = false;
        if (newValue.key && newValue.key.lastIndexOf("; ") > -1) {
            this.label = newValue.key.substring(0, newValue.key.lastIndexOf("; "));
            this.openInNew = newValue.key.substring(newValue.key.lastIndexOf("; ") + 2).trim() == "true";
        }
        this.fireChangeEvents();
    };

    this.setAsfData = function (asfData) {
        if (!asfData) {
            return;
        }

        if (asfData.value == LinkModel.currentDocLink || asfData.value == null && asfProperty.config["fill-with-current"]) {
            instance.setValue(asfData);
            if (!playerModel.asfDataId) {
                return;
            }
            api.getDocumentIdentifier(playerModel.asfDataId, function (identifier) {
                asfData.value = "#submodule=common&action=open_document&document_identifier=" + identifier;
                if (instance.label === '' || !instance.label) {
                    instance.label = i18n.tr("Документ");
                }
                asfData.key = instance.label + "; " + instance.openInNew;
                instance.setValue(asfData);
            });
        } else if (asfData.value && asfData.value.indexOf("${docID}") !== -1) {
            instance.setValue(asfData);
            if (!playerModel.asfDataId) {
                return;
            }
            api.getDocumentIdentifier(playerModel.asfDataId, function (identifier) {
                asfData.value = asfData.value.replace("${docID}", identifier);
                instance.setValue(asfData);
            });
        } else {
            instance.setValue(asfData);
        }
    };
};

LinkModel.prototype = Object.create(_model2.default.prototype);
LinkModel.prototype.constructor = LinkModel;

LinkModel.currentDocLink = '0xDEADBEEF';

/***/ }),

/***/ 233:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = PlayerModel;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _underscore = __webpack_require__(7);

var _underscore2 = _interopRequireDefault(_underscore);

var _i18n = __webpack_require__(14);

var _i18n2 = _interopRequireDefault(_i18n);

var _services = __webpack_require__(23);

var services = _interopRequireWildcard(_services);

var _logger = __webpack_require__(101);

var _constants = __webpack_require__(2);

var _componentUtils = __webpack_require__(15);

var compUtils = _interopRequireWildcard(_componentUtils);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _layoutUtils = __webpack_require__(65);

var layoutUtils = _interopRequireWildcard(_layoutUtils);

var _dictCache = __webpack_require__(278);

var _dictCache2 = _interopRequireDefault(_dictCache);

var _model = __webpack_require__(25);

var _model2 = _interopRequireDefault(_model);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

global.$ = _jquery2.default;
global.jQuery = _jquery2.default;

/**
 * Created by exile
 * Date: 20.01.16
 * Time: 16:25
 */
"use strict";

function PlayerModel() {
    _model2.default.call(this, this, {}, this);

    // модели
    this.models = [];

    this.errorDataLoad = false;
    this.formId = null;
    this.formCode = null;
    this.definition = null;
    this.asfDataId = null;
    this.nodeId = null;
    this.hasChanges = false;
    this.loading = false;

    this.formName = "";
    this.formVersion = 0;
    this.formats = "";
    this.defaultPrintFormat = "";
    this.hasMobile = false;
    this.hasPrintable = false;

    this.building = false;

    this.dictionaryCache = new _dictCache2.default();

    this.config = {};

    var instance = this;

    instance.on(_constants.EVENT_TYPE.valueChange, function (event, model) {
        if (instance.loading) {
            return;
        }
        instance.hasChanges = true;
    });

    /**
     * строим отображение
     * @namespace asForms.Player
     * @method buildView
     * @param {Object} newDefinition
     * @param {asForms.PlayerModel} playerModel
     */
    this.buildModelsDefinition = function (newDefinition, dataUid) {
        try {
            instance.loading = true;

            instance.formId = newDefinition.uuid;
            instance.formCode = newDefinition.code;
            instance.formVersion = newDefinition.version;

            instance.dataUid = dataUid;

            instance.config = newDefinition.config;
            instance.format = newDefinition.format;
            if (instance.format === undefined) instance.format = 0;

            if (_constants.OPTIONS.locale == "en") {
                instance.formName = newDefinition.name;
            } else if (_constants.OPTIONS.locale == "kk" || _constants.OPTIONS.locale == "kz") {
                instance.formName = newDefinition.namekz;
            } else {
                instance.formName = newDefinition.nameru;
            }

            if (newDefinition.views) {
                newDefinition.views.forEach(function (view) {
                    if (view.typeform == _constants.FORM_VIEW_TYPE.print) {
                        if (view.uuid) {
                            instance.formats = view.printable.substring(1, view.printable.length - 1);
                            instance.defaultPrintFormat = view.defaultPrintFormat;
                            instance.hasPrintable = true;
                        }
                    } else if (view.typeform == _constants.FORM_VIEW_TYPE.mobile) {
                        if (view.uuid) {
                            instance.hasMobile = true;
                        }
                    }
                });
            }

            instance.models = [];

            instance.definition = dataUtils.updateDefinition(newDefinition);

            instance.value = { form: instance.formId,
                formVersion: instance.formVersion,
                data: {} };

            console.log("form definition", instance.definition);

            instance.definition.layout.pages.forEach(function (asfLayout, index) {
                var properties = instance.definition.properties;
                instance.addPage(asfLayout, properties);
            });

            var dataSources = dataUtils.extractDataSources(instance.definition);

            dataSources.forEach(function (datasource) {
                var locale = datasource.locale;
                if (!locale) {
                    locale = _constants.OPTIONS.locale;
                }
                instance.dictionaryCache.loadDictionary(datasource.dict, locale);
            });

            instance.trigger(_constants.EVENT_TYPE.valueChange, [instance, instance.definition]);

            instance.loading = false;
        } catch (e1) {
            (0, _logger.logServer)(e1, instance.formId, instance.asfDataId);
        }
    };

    this.checkDefinition = function () {
        var errors = [];
        instance.models.forEach(function (model) {
            if (model.asfProperty.properties.length === 0) {
                errors.push({ id: model.asfProperty.id, errorCode: _constants.BUILDER_ERROR_TYPE.emptyPage });
            }
            errors.push.apply(errors, model.checkProperties());
        });
        return errors;
    };

    this.addPage = function (layout, properties) {

        var fixed = !_underscore2.default.isBoolean(instance.definition.config.fixedLayout) ? instance.format > 0 : instance.definition.config.fixedLayout;

        var pageNumber = instance.models.length;

        var pageProperties = [];
        properties.forEach(function (property) {
            var comp = layoutUtils.findById(property.id, layout);
            if (comp) {
                pageProperties.push(property);
            }
        });

        var model = compUtils.createModel({
            layout: layout,
            properties: pageProperties,
            type: _constants.WIDGET_TYPE.page,
            id: "formPlayerPage" + pageNumber,
            config: {
                fixedLayout: fixed
            }

        }, instance);
        model.pageNumber = pageNumber;
        instance.models.push(model);
        return model;
    };

    this.deletePage = function (pageModel, ignoreHistory) {
        if (instance.models.length === 1) {
            services.showErrorMessage(_i18n2.default.tr("Нельзя удалить последнюю страницу"));
            return;
        }

        pageModel.destroy();

        var index = instance.models.indexOf(pageModel);
        instance.models.splice(index, 1);
        instance.trigger(_constants.EVENT_TYPE.pageDeleted, pageModel);

        if (ignoreHistory !== true) {
            instance.history.addItem(instance.history.deletePage(pageModel.asfProperty.layout));
        }
    };

    /**
     *
     */
    this.getErrors = function () {
        var errors = [];
        this.models.forEach(function (model) {
            model.getErrors().forEach(function (error) {
                errors.push(error);
            });
        });
        return errors;
    };

    /**
     * получение данных по форме
     * @returns {*}
     */
    this.getAsfData = function () {
        try {
            var data = [];
            this.models.forEach(function (model) {
                data = data.concat(model.getAsfData());
            });
            this.value.data = data;
            return this.value;
        } catch (e) {
            (0, _logger.logServer)(e, instance.formId, instance.asfDataId);
        }
    };

    /**
     * настраиваем данные для отображения
     * @namespace asForms.Player
     * @param asfData
     */
    this.setAsfData = function (asfData) {
        if (!asfData) {
            return;
        }
        try {
            instance.errorDataLoad = false;
            instance.loading = true;

            instance.asfDataId = asfData.uuid;
            instance.nodeId = asfData.nodeUUID;

            var data = dataUtils.correctAsfData(asfData, this.models);
            this.value = data;

            this.models.forEach(function (model) {
                try {
                    model.setAsfData(data);
                } catch (e) {
                    (0, _logger.logServer)(e, instance.formId, instance.asfDataId);
                }
            });
            instance.trigger(_constants.EVENT_TYPE.dataLoad, [instance, instance.value]);

            setTimeout(function () {
                instance.hasChanges = false;
                instance.loading = false;
            }, 0);
        } catch (e) {
            this.errorDataLoad = true;
            (0, _logger.logServer)(e, instance.formId, instance.asfDataId);
        }
    };

    this.getModelWithId = function (cmpId, tableId, tableBlockIndex) {
        var targetModel = null;
        this.models.some(function (model) {
            if (model.asfProperty.id == cmpId) {
                targetModel = model;
                return true;
            }
            var tmp = model.getModelWithId(cmpId, tableId, tableBlockIndex);
            if (tmp) {
                targetModel = tmp;
                return true;
            }
        });
        return targetModel;
    };

    this.getDefinitionProperties = function () {

        var properties = [];
        instance.models.forEach(function (pageModel, index) {
            properties.push.apply(properties, pageModel.getProperties());
        });
        return properties;
    };

    this.fillDataFromProp = function (p, data) {
        var newData;
        if (p.data) {
            newData = _jquery2.default.extend(true, {}, p.data);
            if (p.elements) {
                newData.values = p.elements;
            }
            data.push(newData);
        } else {
            if (p.elements) {
                newData = { id: p.id };
                newData.values = p.elements;
                data.push(newData);
            }
        }
    };

    this.getDefaultData = function () {
        var properties = this.getDefinitionProperties();
        var data = [];
        properties.forEach(function (property, index) {
            instance.fillDataFromProp(property, data);
            if (property.properties) {
                property.properties.forEach(function (p, i) {
                    instance.fillDataFromProp(p, data);
                });
            }
        });
        return data;
    };

    this.getDataSources = function () {
        var properties = this.getDefinitionProperties();
        var dataSources = [];
        properties.forEach(function (property, index) {
            if (property.dataSource) {
                property.dataSource.id = property.id;
                dataSources.push(property.dataSource);
            }
            if (property.properties) {
                property.properties.forEach(function (p, i) {
                    if (p.dataSource) {
                        p.dataSource.id = p.id;
                        dataSources.push(p.dataSource);
                    }
                });
            }
        });
        return dataSources;
    };

    this.getPropertiesWithType = function (type) {

        var properties = [];
        instance.models.forEach(function (pageModel, index) {
            properties.push.apply(properties, pageModel.getPropertiesWithType(type));
        });
        return properties;
    };

    this.getLayout = function () {
        var layout = { totalPages: instance.models.length, pages: [] };
        instance.models.forEach(function (pageModel, index) {
            var pageLayout = pageModel.asfProperty.layout;
            layout.pages.push(pageLayout);
        });
        return layout;
    };

    this.getConfig = function () {
        if (!instance.config) {
            return {};
        }
        return _jquery2.default.extend(true, {}, instance.config);
    };

    this.destroy = function () {
        this.models.forEach(function (model) {
            model.destroy();
        });
        instance.trigger(_constants.EVENT_TYPE.formDestroy, [instance]);

        this.off();

        this.models = [];
        this.errorDataLoad = false;
        this.formId = null;
        this.definition = null;
        this.formCode = null;
        this.asfDataId = null;
        this.nodeId = null;
        this.hasChanges = false;
        this.loading = false;

        this.formName = "";
        this.formats = "";
        this.defaultPrintFormat = "";
        this.hasMobile = false;
        this.hasPrintable = false;
    };
};

PlayerModel.prototype = Object.create(_model2.default.prototype);
PlayerModel.prototype.constructor = PlayerModel;
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(80)))

/***/ }),

/***/ 234:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = PositionLinkModel;

var _underscore = __webpack_require__(7);

var _underscore2 = _interopRequireDefault(_underscore);

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

var _constants = __webpack_require__(2);

var _model = __webpack_require__(25);

var _model2 = _interopRequireDefault(_model);

var _componentUtils = __webpack_require__(15);

var compUtils = _interopRequireWildcard(_componentUtils);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _userLinkUtils = __webpack_require__(158);

var userLinkUtils = _interopRequireWildcard(_userLinkUtils);

var _UserChooser = __webpack_require__(81);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * модель компонента выбора должности
 * asfData:
 * {
     *        "id": "cmp-rgi3pr",
     *        "type": "entity",
     *        "value": "руководитель дочернего подразделения 12576",
     *        "key": "f1af818e-cd5d-4390-9e96-b26dd148e42d"
     *  }
 * defaultValue:
 * @param asfProperty
 * @param playerModel
 * @constructor
 */
function PositionLinkModel(asfProperty, playerModel) {
    _model2.default.call(this, this, asfProperty, playerModel);
    var instance = this;

    instance.value = [];

    var textValue = "";

    var masterModel = null;

    var textNumber = 0;

    this.getNextTextNumber = function () {
        return textNumber++;
    };

    this.checkNextTextNumber = function () {
        instance.getSelectedIds().forEach(function (id) {
            if (id.startsWith('text-')) {
                textNumber = Math.max(textNumber, id.substring(5)) + 1;
            }
        });
    };

    this.findMaster = function () {
        setTimeout(function () {
            if (asfProperty.config.depends && !playerModel.building) {
                masterModel = compUtils.findMaster(asfProperty, playerModel);
                if (!masterModel) {
                    return;
                }
                masterModel.on(_constants.EVENT_TYPE.valueChange, function () {
                    instance.updateModelData();
                });
            }
        }, 0);
    };

    this.getSelectedIds = function (target) {
        var selectedIds = [];
        if (_underscore2.default.isUndefined(target)) {
            target = instance.value;
        }
        if (target && target instanceof Array) {
            target.forEach(function (position) {
                selectedIds.push(position.elementID);
            });
        }
        return selectedIds;
    };

    this.getFullSelectedIds = function (target) {
        var selectedIds = [];
        if (_underscore2.default.isUndefined(target)) {
            target = instance.value;
        }
        if (target && target instanceof Array) {
            target.forEach(function (position) {
                if (position.transitional) {
                    selectedIds.push(position.elementID + "," + position.periodID);
                } else {
                    selectedIds.push(position.elementID);
                }
            });
        }
        return selectedIds;
    };

    this.updatePositionLinkItem = function (item) {
        if (item.elementID.indexOf(_UserChooser.USER_CHOOSER_SUFFIXES.text) === 0) {
            item.status = i18n.tr("Введен вручную");
            item.statusColor = "#df6c6d";
        }
    };

    this.setValueFromInput = function (inputValue) {
        this.doSetValue(inputValue);
        playerModel.trigger(_constants.EVENT_TYPE.valueChange, [this, this.value]);
    };

    this.getEntityType = function () {
        return asfProperty.config.entity;
    };

    this.getFilterUserId = function () {
        return userLinkUtils.getFilterEntityId(masterModel, _constants.ENTITY_TYPE.users);
    };

    this.getFilterDepartmentId = function () {
        return userLinkUtils.getFilterEntityId(masterModel, _constants.ENTITY_TYPE.departments);
    };

    this.setAsfData = function (asfData) {
        if (!asfData || !asfData.key) {
            return;
        }
        textValue = asfData.value;

        var parsedAsfData = userLinkUtils.parseAsfData(asfData, ";", ";; ");

        var itemIds = parsedAsfData.itemIds;
        var manualTags = parsedAsfData.manualTags;
        var tags = parsedAsfData.tags;

        var selectedValues = [];
        itemIds.forEach(function (itemId, index) {
            var position = {};
            var commaIndex = itemId.indexOf(",");
            if (commaIndex > 0) {
                position.elementID = itemId.substring(0, commaIndex);
                position.periodID = itemId.substring(commaIndex + 1);
                position.transitional = true;

                itemIds[index] = position.elementID;
            } else {
                position.elementID = itemId;
            }

            position.elementName = manualTags[itemId] || tags[index];
            position.tagName = position.elementName;
            selectedValues.push(position);
        });
        instance.doSetValue(selectedValues, true, false);

        api.getPositionsInfo(itemIds, instance.getLocale(), function (positions) {
            selectedValues = selectedValues.slice();
            selectedValues.forEach(function (selectedValue, selectedIndex) {
                var found = positions.findElement("elementID", selectedValue.elementID);
                if (found) {
                    found.tagName = manualTags[selectedValue.elementID] || found.elementName;
                    selectedValues[selectedIndex] = found;
                }
            });
            instance.doSetValue(selectedValues, true, false);
        });
    };

    this.updateModelData = function () {
        var selectedIds = instance.getFullSelectedIds(true);

        var filterDepartmentID = instance.getFilterDepartmentId();
        var filterUserID = instance.getFilterUserId();

        if (!filterDepartmentID && !filterUserID) {
            instance.doSetValue(null);
            return;
        }

        api.checkPositions(selectedIds, filterUserID, filterDepartmentID, instance.isShowVacant(), true, instance.getLocale(), function (positions) {
            if (positions && positions.length > 0) {
                positions[0].tagName = positions[0].elementName;
            }

            instance.doSetValue(positions, true, true);
            instance.trigger(_constants.EVENT_TYPE.dataLoad, instance);
        });
    };

    this.isShowVacant = function () {
        return asfProperty != null && !_underscore2.default.isUndefined(asfProperty.config["only-vacant"]) && asfProperty.config["only-vacant"];
    };

    this.doSetValue = function (newValue) {
        var fireEvent = true;
        if (this.value == newValue || instance.getFullSelectedIds().equals(instance.getFullSelectedIds(newValue))) {
            // ничего ��е делаем если новое значение такое же как и старое
            // даже событие отправлять не будем
            fireEvent = false;
        }
        if (!newValue) {
            this.value = [];
        } else if (!(newValue instanceof Array)) {
            this.value = [newValue];
        } else {
            this.value = newValue;
        }

        textValue = "";
        var sign = "";
        this.value.forEach(function (val) {
            instance.updatePositionLinkItem(val);
            textValue += sign + val.tagName;
            sign = ", ";
        });

        if (fireEvent) {
            this.fireChangeEvents();
        } else {
            instance.trigger(_constants.EVENT_TYPE.dataLoad, [instance]);
        }

        instance.checkNextTextNumber();
    };

    this.getTextValue = function () {
        return textValue;
    };

    this.getKey = function () {
        var array = instance.getFullSelectedIds();
        if (array.length === 0) {
            return null;
        } else {
            return array.arrayToString(";");
        }
    };

    this.getManualTags = function () {
        var manualTags = {};
        this.value.forEach(function (val) {
            if (val.tagName != val.elementName) {
                manualTags[val.elementID] = val.tagName;
            }
        });
        return manualTags;
    };

    this.getAsfData = function (blockNumber) {
        var text = null;
        var key = instance.getKey();
        if (key) {
            text = userLinkUtils.join(this.value, "tagName", ";; ");
        }
        var base = dataUtils.getBaseAsfData(asfProperty, blockNumber, text, key);
        base.manualTags = instance.getManualTags();
        base.formatVersion = "V1";
        return base;
    };

    this.findMaster();
};

PositionLinkModel.prototype = Object.create(_model2.default.prototype);
PositionLinkModel.prototype.constructor = PositionLinkModel;

/***/ }),

/***/ 235:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = ProjectLinkModel;

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

var _constants = __webpack_require__(2);

var _model = __webpack_require__(25);

var _model2 = _interopRequireDefault(_model);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

/**
 * модель ссылки на проект портфель
 * asfData:
 * {
     *        "id": "cmp-nd99h5",
     *        "type": "projectlink",
     *        "value": "Портфель: портфель 1",   //для плана План: наименование плана
     *        "key": "00e24c3d-d3de-423a-9b86-5d501975b922",
     *        "valueID": "00e24c3d-d3de-423a-9b86-5d501975b922"
     *    }
 * defaultValue:
 * {
     *        "value": "Портфель: портфель 1",   //для плана План: наименование плана
     *        "valueID": "00e24c3d-d3de-423a-9b86-5d501975b922"
     *    }
 * @param asfProperty
 * @param playerModel
 * @constructor
 */
function ProjectLinkModel(asfProperty, playerModel) {

    _model2.default.call(this, this, asfProperty, playerModel);

    var instance = this;

    var textValue = null;
    var elementType = _constants.OBJECT_TYPES.PLAN;
    var deleted = false;

    this.setValueFromInput = function (inputValue) {
        if (inputValue == null) {
            textValue = "";
            elementType = 0;
            this.doSetValue(null);
        } else {
            var actionID = inputValue.actionID;
            var name = inputValue.name;
            elementType = inputValue.elementType;

            if (elementType == _constants.OBJECT_TYPES.PLAN) {
                textValue = i18n.tr("Проект: ") + name;
            } else {
                textValue = i18n.tr("Портфель: ") + name;
            }

            this.doSetValue(actionID);
        }
    };

    this.getTextValue = function () {
        if (textValue == null) {
            return "";
        }
        return textValue;
    };

    this.getElementType = function () {
        return elementType;
    };

    this.getAsfData = function (blockNumber) {
        var base = dataUtils.getBaseAsfData(asfProperty, blockNumber, textValue, this.value);
        if (this.value) {
            base.valueID = this.value;
        }
        return base;
    };

    this.setAsfData = function (asfData) {
        if (!asfData || !asfData.valueID) {
            return;
        }
        textValue = asfData.value;
        this.setValue(asfData.valueID);
        this.loadItemInfo(asfData.valueID);
    };

    this.loadItemInfo = function (actionID) {
        deleted = true;
        if (!actionID) {
            return;
        }
        api.getProjectTreeItemInfo(actionID, function (data) {
            var name = data.name;
            elementType = data.elementType;
            // если план удален все равно позволяем его открыть, если портфель удален не позволяем
            if (elementType == _constants.OBJECT_TYPES.PLAN) {
                textValue = i18n.tr("Проект: ") + name;
                deleted = false;
            } else {
                textValue = i18n.tr("Портфель: ") + name;
                deleted = !!data.deleted;
            }
            instance.trigger(_constants.EVENT_TYPE.dataLoad, instance);
        });
    };

    this.isDeleted = function () {
        return deleted;
    };
};

ProjectLinkModel.prototype = Object.create(_model2.default.prototype);
ProjectLinkModel.prototype.constructor = ProjectLinkModel;

/***/ }),

/***/ 236:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(jQuery) {

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = RegistryLinkModel;

var _underscore = __webpack_require__(7);

var _underscore2 = _interopRequireDefault(_underscore);

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

var _constants = __webpack_require__(2);

var _model = __webpack_require__(25);

var _model2 = _interopRequireDefault(_model);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _tableUtils = __webpack_require__(52);

var tableUtils = _interopRequireWildcard(_tableUtils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * asfData:
 * {
     *        "id": "cmp-6vrrkp",
     *        "type": "reglink",
     *        "value": "1111-GPON",
     *        "key": "a1b478c0-2d33-11e6-b327-3085a93a6496",
     *        "valueID": "a1b478c0-2d33-11e6-b327-3085a93a6496",
     *        "username": "Admin Admin Admin",
     *        "userID": "1"
     *    }
 * @param asfProperty
 * @param playerModel
 * @constructor
 */
function RegistryLinkModel(asfProperty, playerModel) {
    var asfDataId = null;
    var textValue = "";
    var instance = this;
    _model2.default.call(this, this, asfProperty, playerModel);

    this.getTextValue = function () {
        return textValue;
    };

    /**
     * Получение данных компонента в виде json объекта
     *
     * @param blockNumber - номер блока
     */
    this.getAsfData = function (blockNumber) {
        var user = _constants.OPTIONS.currentUser;
        var userName = user.lastname + " " + user.firstname + " " + user.patronymic;
        var result = dataUtils.getBaseAsfData(asfProperty, blockNumber, textValue, instance.value);
        result.username = userName;
        result.userID = user.userId;
        if (instance.value) {
            result.valueID = instance.value;
        }
        return result;
    };

    /**
     * Изменение данных компонента
     *
     * @param asfData - данные формы
     */
    this.setAsfData = function (asfData) {
        if (!asfData || !asfData.key) {
            this.setValue("");
            return;
        }
        textValue = asfData.value;
        this.setValue(asfData.key, true);
    };

    /**
     * Получение идентификатора реестра, почему в dateFormat - так исторически сложилось
     */
    this.getRegistryID = function () {
        return asfProperty.config["dateFormat"];
    };

    this.updateTextValue = function () {
        if (asfDataId) {
            api.getDocMeaningContent(instance.getRegistryID(), asfDataId, function (text) {
                textValue = text;
                if (!textValue || textValue === "") {
                    textValue = i18n.tr("Документ");
                }
                instance.trigger(_constants.EVENT_TYPE.dataLoad, [instance]);
            });
        } else {
            textValue = "";
            instance.trigger(_constants.EVENT_TYPE.dataLoad, [instance]);
        }
    };

    this.setValue = function (newDocumentId, ignoreCollate) {
        if (newDocumentId) {
            asfDataId = null;
            instance.doSetValue(newDocumentId);
            api.getAsfDataUUID(newDocumentId, function (newAsfDataId) {
                asfDataId = newAsfDataId;
                instance.updateTextValue();
                if (!ignoreCollate && asfProperty.config.CollationGroup) {
                    instance.loadCollated();
                }
            });
        } else {
            asfDataId = null;
            instance.doSetValue(null);
            instance.updateTextValue();
        }
    };

    /**
     * Подгрузка сопоставлений
     */
    this.loadCollated = function () {
        var me = this;
        api.getCollatedData(instance.value, this.playerModel.asfDataId, this.playerModel.formId, this.asfProperty.config.CollationGroup, this.playerModel.formVersion, function (result) {
            var data = jQuery.parseJSON('{' + result + '}');
            me.setDependence(data);
        });
    };

    /**
     * Изменение значений зависимых полей
     *
     * @param data - объект с информацией о зависимых полях
     */
    this.setDependence = function (data) {
        var dataValues = data.data;
        var values = [];
        var object;
        var i = 0;
        for (i; i < dataValues.length; i++) {
            object = dataValues[i];
            if (object.id === 'CollationGroupOutFields') {
                values = object.values;
            }
        }

        var collationTables = instance.parseCollationData(values, dataValues);

        collationTables.forEach(function (collationTable) {
            var tableId = null;
            var blockNumber = null;
            if (!collationTable.tableId.isEmpty()) {
                tableId = collationTable.tableId;
                var dynTable = instance.playerModel.getModelWithId(tableId);
                var modelRow = dynTable.createRow();
                blockNumber = modelRow.tableBlockIndex;
            }
            collationTable.cmpCollationData.forEach(function (cmpCollationData, index) {
                var model = playerModel.getModelWithId(cmpCollationData.cmpId, tableId, blockNumber);
                var object = cmpCollationData.data;
                if (model && object) {
                    model.setAsfData(object);
                }
            });
        });
    };

    this.parseCollationData = function (collationValues, dataValues) {
        var collation = [];
        collationValues.forEach(function (collationValue, index) {
            var tableId = "";
            var cmpId = collationValue;

            if (collationValue.indexOf(".") !== -1) {
                tableId = collationValue.substring(0, collationValue.indexOf("."));
                cmpId = collationValue.substring(collationValue.indexOf(".") + 1);
            }

            var tCollation = collation.findElement("tableId", tableId);
            if (tCollation === null) {
                tCollation = { tableId: tableId, cmpCollationData: [] };
                collation.push(tCollation);
            }

            if (tableId === "") {
                var object = dataValues.findElement("id", cmpId);
                tCollation.cmpCollationData.push({ cmpId: cmpId, data: object });
            } else {
                var tableData = dataValues.findElement("id", tableId);

                var blockNumbers = tableUtils.extractBlockNumbers(tableData);

                var objectData = tableData.data.findElement("id", cmpId + "-b" + _underscore2.default.last(blockNumbers));
                tCollation.cmpCollationData.push({ cmpId: cmpId, data: objectData });
            }
        });
        return collation;
    };

    this.on(_constants.EVENT_TYPE.valueChange, function () {
        if (instance.value) {
            instance.textValue = instance.value;
        } else {
            instance.textValue = '';
        }
    });
};

RegistryLinkModel.prototype = Object.create(_model2.default.prototype);
RegistryLinkModel.prototype.constructor = RegistryLinkModel;
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ }),

/***/ 237:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = RepeatPeriodModel;

var _constants = __webpack_require__(2);

var _model = __webpack_require__(25);

var _model2 = _interopRequireDefault(_model);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _dateUtils = __webpack_require__(73);

var dateUtils = _interopRequireWildcard(_dateUtils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Компонент период повторения
 * @param asfProperty
 * @param playerModel
 * @constructor
 */
//принимает следующие значения
//{"id":"cmp-mqk3ik","type":"repeater","value":"Нет","key":"0"}
//{"id":"cmp-mqk3ik","type":"repeater","value":"По дням недели: Понедельник, Воскресе��ье","key":"1|1.0;7.0;"}
//{"id":"cmp-mqk3ik","type":"repeater","value":"По дням месяца: 12, 17, 23","key":"2|12.0;17.0;23.0;"}
//{"id":"cmp-mqk3ik","type":"repeater","value":"Ежегодно: 4.1, 5.11, 7.12, 9.30","key":"4|1.4;11.5;12.7;30.9;"}
function RepeatPeriodModel(asfProperty, playerModel) {

    _model2.default.call(this, this, asfProperty, playerModel);

    this.doSetValue = function (newValue, fireInternalEvent, fireEvent) {

        this.value = [];
        this.textValue = "";
        this.type = _constants.REPEAT_PERIOD_TYPE.none;

        if (newValue.key) {
            newValue = newValue.key;
            this.type = parseInt(newValue.substring(0, 1));

            instance.textValue = instance.getTypeText();
            if (instance.type !== _constants.REPEAT_PERIOD_TYPE.none) {
                instance.textValue += ": ";
            }

            if (newValue.length > 3) {
                newValue = newValue.substring(2).trim();
                var sign = "";
                newValue.split(";").forEach(function (item) {
                    if (item.length === 0) {
                        return;
                    }
                    instance.value.push(item);
                    instance.textValue += sign + instance.getValueString(item, instance.type, true);
                    sign = ", ";
                });
            } else {
                instance.textValue = "";
            }
        }
        this.fireChangeEvents();
    };

    this.setAsfData = function (asfData) {
        if (!asfData) {
            return;
        }
        this.setValue(asfData);
    };

    var instance = this;
    instance.value = [];

    this.type = _constants.REPEAT_PERIOD_TYPE.none;
    this.textValue = i18n.tr("Нет");

    this.getTextValue = function () {
        return instance.textValue;
    };

    this.getTypeText = function () {
        switch (this.type) {
            case _constants.REPEAT_PERIOD_TYPE.none:
                {
                    return i18n.tr("Нет");
                }
            case _constants.REPEAT_PERIOD_TYPE.week_days:
                {
                    return i18n.tr("По дням недели");
                }
            case _constants.REPEAT_PERIOD_TYPE.month_days:
                {
                    return i18n.tr("По дням месяца");
                }
            case _constants.REPEAT_PERIOD_TYPE.year_days:
                {
                    return i18n.tr("Ежегодно");
                }
        }
        return "";
    };

    this.getAsfData = function (blockNumber) {
        return dataUtils.getBaseAsfData(asfProperty, blockNumber, instance.textValue, instance.getKey(this.type, this.value));
    };

    this.getKey = function (type, values) {
        var vStr = type + "";
        if (values && type != _constants.REPEAT_PERIOD_TYPE.none) {
            vStr += "|" + values.arrayToString(";") + ";";
        }
        return vStr;
    };

    this.setValueFromInput = function (newType, newValues) {

        if (!newValues && newType == instance.type) {
            newValues = value;
        }

        var v = { id: asfProperty.id, type: _constants.WIDGET_TYPE.repeater, value: "", key: instance.getKey(newType, newValues) };
        instance.setValue(v, true);
    };

    this.getValueString = function (v, type, full) {
        var day = parseInt(v.substring(0, v.indexOf(".")));
        var month = parseInt(v.substring(v.indexOf(".") + 1));

        if (type == _constants.REPEAT_PERIOD_TYPE.year_days) {
            if (full) {
                return day + " " + dateUtils.getMonthName(month);
            } else {
                return day + " " + dateUtils.getMonthShortName(month);
            }
        } else if (type == _constants.REPEAT_PERIOD_TYPE.week_days) {
            if (full) {
                return dateUtils.getWeekDayName(day);
            } else {
                return dateUtils.getWeekDayShortName(day);
            }
        } else {
            return day + "";
        }
    };
};

RepeatPeriodModel.prototype = Object.create(_model2.default.prototype);
RepeatPeriodModel.prototype.constructor = RepeatPeriodModel;

/***/ }),

/***/ 238:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = SimpleModel;

var _model = __webpack_require__(25);

var _model2 = _interopRequireDefault(_model);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function SimpleModel(asfProperty, playerModel) {
    _model2.default.call(this, this, asfProperty, playerModel);
};

SimpleModel.prototype = Object.create(_model2.default.prototype);
SimpleModel.prototype.constructor = SimpleModel;

/***/ }),

/***/ 239:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = TableModel;

var _model = __webpack_require__(25);

var _model2 = _interopRequireDefault(_model);

var _underscore = __webpack_require__(7);

var _underscore2 = _interopRequireDefault(_underscore);

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _layoutUtils = __webpack_require__(65);

var lo = _interopRequireWildcard(_layoutUtils);

var _constants = __webpack_require__(2);

var _tableUtils = __webpack_require__(52);

var tableUtils = _interopRequireWildcard(_tableUtils);

var _selection = __webpack_require__(242);

var _selection2 = _interopRequireDefault(_selection);

var _maskUtils = __webpack_require__(100);

var _componentUtils = __webpack_require__(15);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function TableModel(asfProperty, playerModel) {
    _model2.default.call(this, this, asfProperty, playerModel);

    /**
     * массивы моделей компонентов по рядам динамической таблицы, хранятся здесь, за отображением следит view
     * @type {Array}
     */
    this.modelBlocks = [];

    /**
     * массив компонентов заголовка таблицы
     * @type {Array}
     */
    this.headerModelBlock = [];

    var instance = this;

    /**
     * номер по порядку создаваемых блоков (не соответствует -b компонента)
     * @type {number}
     */
    var blockNumber = 0;

    if (!_underscore2.default.isObject(asfProperty.config)) asfProperty.config = {};

    if (!_underscore2.default.isBoolean(asfProperty.config.fixedLayout)) asfProperty.config.fixedLayout = playerModel.format > 0;

    /**
     *
     * @param handler в функцию передаются 2 параметра,
     * model - модель,
     * blockNumber - номер блока, -1 для заголовка
     */
    var iterateAllModels = function iterateAllModels(handler) {
        instance.headerModelBlock.forEach(function (headerModel) {
            handler(headerModel, -1);
        });
        instance.modelBlocks.forEach(function (ms, blockNumber) {
            ms.forEach(function (model) {
                handler(model, blockNumber);
            });
        });
    };

    this.findModelCell = function (model) {
        var comp = lo.findById(model.asfProperty.id, instance.asfProperty.layout);
        return lo.compToCell(comp);
    };

    this.selection = new _selection2.default(this.asfProperty.layout);

    this.wasSelectedLast = function () {
        return this.selection.lastCell !== null;
    };

    this.checkProperties = function () {
        var errors = [];
        iterateAllModels(function (model) {
            var prop = model.getProperty();
            if (prop.type != _constants.WIDGET_TYPE.table) {
                return;
            }

            if (prop.properties.length === 0) {
                model.fireInvalid();
                errors.push({ id: prop.id, errorCode: _constants.BUILDER_ERROR_TYPE.emptyTable });
            } else {
                if (model.isHaveHeader() && prop.layout.rows < 2) {
                    model.fireInvalid();
                    errors.push({ id: prop.id, errorCode: _constants.BUILDER_ERROR_TYPE.emptyTable });
                }
                errors.push.apply(errors, model.checkProperties());
            }
        });
        return errors;
    };

    this.getProperties = function () {
        var properties = [];
        if (instance.isPage()) {
            iterateAllModels(function (model) {
                properties.push(model.getProperty());
            });
        } else {
            properties.push(instance.getProperty());
        }
        return properties;
    };

    this.getProperty = function () {
        var asfProperty = _jquery2.default.extend(true, {}, instance.asfProperty);
        asfProperty.properties = [];
        var ids = [];
        iterateAllModels(function (model) {
            var childProperty = model.getProperty();
            if (childProperty.id && childProperty.type) {
                ids.push(childProperty.id);
                asfProperty.properties.push(childProperty);
            }
        });
        lo.filterLayout(asfProperty.layout, ids);
        lo.sortLayout(asfProperty.layout);

        return asfProperty;
    };

    this.getPropertiesWithType = function (type) {
        var properties = [];
        iterateAllModels(function (model) {
            if (!type || model.asfProperty.type === type) {
                var asfProperty = _jquery2.default.extend(true, {}, model.asfProperty);

                if (instance.asfProperty.config && instance.asfProperty.config.appendRows) {
                    asfProperty.fullId = instance.asfProperty.id + "." + asfProperty.id;
                } else {
                    asfProperty.fullId = asfProperty.id;
                }

                properties.push(asfProperty);
            }

            if (model.asfProperty.type == _constants.WIDGET_TYPE.table) {
                properties.push.apply(properties, model.getPropertiesWithType(type));
            }
        });
        return properties;
    };

    this.getModelWithId = function (cmpId, tableId, tableBlockIndex) {

        var allModels = [];
        iterateAllModels(function (model) {
            if (!tableId) {
                if (!instance.isDynamic()) {
                    allModels.push(model);
                }
            } else if (instance.asfProperty.id !== tableId) {
                if (model.asfProperty.type == _constants.WIDGET_TYPE.table) {
                    allModels.push(model);
                }
            } else {
                if (!tableBlockIndex || model.asfProperty.tableBlockIndex == tableBlockIndex) {
                    allModels.push(model);
                }
            }
        });

        return tableUtils.getModelWithId(cmpId, allModels, tableId, tableBlockIndex);
    };

    this.getTextValue = function () {
        var format = asfProperty.config.format || '';
        if (this.isStatic()) {
            format = '';
        }
        var delimiter = asfProperty.config.delimeter;
        if (!delimiter) {
            delimiter = '<br/>\n';
        }
        var textValue = '';

        if (format == '') {
            instance.modelBlocks.forEach(function (modelRow, index) {
                modelRow.forEach(function (model) {
                    textValue += model.getHTMLValue();
                });
            });
        } else {
            instance.modelBlocks.forEach(function (modelRow, index) {
                modelRow.forEach(function (model) {
                    var absoluteCompId = "${";
                    absoluteCompId += model.asfProperty.id + "-b" + (index + 1);
                    absoluteCompId += "}";

                    absoluteCompId = (0, _maskUtils.escapeRegExp)(absoluteCompId);

                    var regex = new RegExp(absoluteCompId, "g");
                    format = format.replace(regex, model.getHTMLValue());
                });
            });

            textValue = format;
            var blockFormats = format.match(/\[(.*?)\]/g);
            if (blockFormats) {
                blockFormats.forEach(function (blockFormat, formatIndex) {
                    var blockTextValue = "";
                    instance.modelBlocks.forEach(function (modelRow, index) {
                        if (blockTextValue) {
                            blockTextValue += delimiter;
                        }
                        var rowValue = blockFormat;
                        if (rowValue.length > 1) {
                            rowValue = rowValue.substring(1, rowValue.length - 1);
                        }
                        modelRow.forEach(function (model, i) {
                            var s = "${";
                            s += model.asfProperty.id;
                            s += "}";
                            rowValue = rowValue.replace(s, model.getHTMLValue());
                        });
                        blockTextValue += rowValue;
                    });
                    textValue = textValue.replace(blockFormat, blockTextValue);
                });
            }
        }
        return textValue;
    };

    this.createRow = function () {

        blockNumber = blockNumber + 1;

        var modelRow = tableUtils.createModels(instance, false, blockNumber);
        modelRow.forEach(function (model) {
            try {
                model.on(_constants.EVENT_TYPE.valueChange, function () {
                    instance.trigger(_constants.EVENT_TYPE.dataLoad, [instance]);
                });

                model.on(_constants.EVENT_TYPE.dataLoad, function () {
                    instance.trigger(_constants.EVENT_TYPE.dataLoad, [instance]);
                });
            } catch (e) {
                console.log(model);
            }
        });
        instance.modelBlocks.push(modelRow);
        this.trigger(_constants.EVENT_TYPE.tableRowAddInternal, [instance, modelRow]);
        this.trigger(_constants.EVENT_TYPE.tableRowAdd, [instance, modelRow]);
        this.playerModel.trigger(_constants.EVENT_TYPE.valueChange, [instance]);

        return modelRow;
    };

    this.deleteModel = function (modelToDelete) {
        var index = instance.modelBlocks[0].indexOf(modelToDelete);
        instance.modelBlocks[0].splice(index, 1);

        modelToDelete.destroy();

        var layout = instance.asfProperty.layout;

        var component = lo.findById(modelToDelete.asfProperty.id, layout);
        if (lo.isMerged(component)) {
            component.id = undefined;
        } else {
            layout.components = _underscore2.default.reject(layout.components, function (comp) {
                return comp.id === modelToDelete.asfProperty.id;
            });
        }

        var deletedProperty = tableUtils.getPropertyById(asfProperty.properties, modelToDelete.asfProperty.id);
        var propertyIndex = asfProperty.properties.indexOf(deletedProperty);
        asfProperty.properties.splice(propertyIndex, 1);

        instance.trigger(_constants.EVENT_TYPE.modelDeleted, modelToDelete);

        instance.playerModel.trigger(_constants.EVENT_TYPE.modelDeleted, [modelToDelete]);
    };

    this.paste = function (asfProperty, row, column) {
        var copy = _jquery2.default.extend(true, {}, asfProperty);

        var tableId = null;
        if (instance.isDynamic()) {
            tableId = instance.asfProperty.id;
        }

        copy.id = TableModel.getUniqueId(copy, tableId, playerModel);

        if (copy.type == _constants.WIDGET_TYPE.table && (!copy.config || !copy.config.appendRows)) {
            // и тут начинается веселье
            var insertingTableLayout = copy.layout;
            copy.properties.forEach(function (prop) {
                var component = lo.findById(prop.id, insertingTableLayout);
                var candidateId = TableModel.getUniqueId(prop, null, playerModel);

                prop.id = candidateId;
                component.id = candidateId;
            });
        }

        instance.insertComponentTo(copy, row, column);
        return copy;
    };

    this.insertComponentTo = function (newAsfProperty, row, column) {
        var model = (0, _componentUtils.createModel)(newAsfProperty, playerModel);
        if (asfProperty.config && asfProperty.config.appendRows) {
            model.tableBlockIndex = 0;
            model.ownerTableId = instance.asfProperty.id;
        }

        instance.modelBlocks[0].push(model);

        var component = { id: newAsfProperty.id, row: row, column: column };
        var layout = instance.asfProperty.layout;
        var currentComp = lo.find(component, layout);
        if (currentComp) {
            currentComp.id = newAsfProperty.id;
        } else {
            layout.components.push(component);
        }

        instance.asfProperty.properties.push(newAsfProperty);

        instance.trigger(_constants.EVENT_TYPE.modelInserted, [model, row, column]);
        this.selection.selectOne([row, column]);
        return model;
    };

    this.clear = function () {

        var tmpModels = [];
        tmpModels.push.apply(tmpModels, instance.modelBlocks[0]);

        tmpModels.forEach(function (model) {
            instance.deleteModel(model);
        });

        asfProperty.layout.config = [];
        asfProperty.layout.components = [];

        asfProperty.layout.rows = 1;
        if (instance.isHaveHeader()) {
            asfProperty.layout.rows = 2;
        }
        asfProperty.layout.columns = 1;

        instance.trigger(_constants.EVENT_TYPE.tableDomUpdate);

        instance.selection.clear();
    };

    this.addTableRow = function (index) {
        lo.insertRow(index, asfProperty.layout);
        instance.selection.insertRowAfter(index - 1);
        instance.trigger(_constants.EVENT_TYPE.tableDomUpdate);
        this.selection.selectOne([index, 0]);
    };

    this.canDeleteRow = function (index) {
        var layout = instance.asfProperty.layout;
        if (layout.rows === 1) {
            if (!instance.isPage()) {
                return false;
            }
        }

        if (layout.rows === 2 && instance.isHaveHeader()) {
            return false;
        }
        return true;
    };

    this.getRowModels = function (index) {
        var idsToRemove = lo.getRowIds(index, instance.asfProperty.layout);

        return _underscore2.default.chain(instance.modelBlocks[0]).filter(function (model) {
            return _underscore2.default.contains(idsToRemove, model.asfProperty.id);
        }).value();
    };

    this.deleteTableRow = function (index) {
        if (!instance.canDeleteRow(index)) {
            return;
        }

        var layout = instance.asfProperty.layout;
        if (layout.rows === 1) {
            if (instance.isPage()) {
                instance.playerModel.deletePage(instance);
            }
            return;
        }

        _underscore2.default.each(instance.getRowModels(index), instance.deleteModel);

        lo.deleteRow(index, asfProperty.layout);
        instance.trigger(_constants.EVENT_TYPE.tableDomUpdate);
        instance.selection.deleteRow(index);
    };

    this.addTableColumn = function (index) {
        lo.insertColumn(index, asfProperty.layout);
        instance.selection.insertColumnAfter(index - 1);
        instance.trigger(_constants.EVENT_TYPE.tableDomUpdate);
        this.selection.selectOne([0, index]);
    };

    this.canDeleteColumn = function (index) {
        var layout = instance.asfProperty.layout;
        if (layout.columns === 1) {
            if (!instance.isPage()) {
                return false;
            }
        }
        return true;
    };

    this.getColumnModels = function (index) {
        var idsToRemove = lo.getColumnIds(index, instance.asfProperty.layout);

        return _underscore2.default.chain(instance.modelBlocks[0]).filter(function (model) {
            return _underscore2.default.contains(idsToRemove, model.asfProperty.id);
        }).value();
    };

    this.deleteTableColumn = function (index) {

        if (!instance.canDeleteColumn(index)) {
            return;
        }

        var layout = instance.asfProperty.layout;
        if (layout.columns === 1) {
            if (instance.isPage()) {
                instance.playerModel.deletePage(instance);
            }
            return;
        }

        if (layout.config) {
            layout.config = _underscore2.default.reject(layout.config, function (c) {
                return c.column === index;
            });
        }

        _underscore2.default.each(instance.getColumnModels(index), instance.deleteModel);

        lo.deleteColumn(index, asfProperty.layout);
        instance.trigger(_constants.EVENT_TYPE.tableDomUpdate);
        instance.selection.deleteColumn(index);
    };

    this.removeRow = function (index) {
        var removedRow = this.modelBlocks.splice(index, 1);
        instance.trigger(_constants.EVENT_TYPE.tableRowDelete, [instance, index, removedRow]);
        instance.playerModel.trigger(_constants.EVENT_TYPE.valueChange, [instance]);
    };

    this.getErrors = function () {
        var errors = [];
        iterateAllModels(function (model) {
            errors = errors.concat(model.getErrors());
        });
        return errors;
    };

    this.getAsfData = function () {
        var asfData = [];
        iterateAllModels(function (model, blockNumber) {
            var modelData;
            if (blockNumber == -1) {
                modelData = model.getAsfData();
            } else {
                if (instance.isPage() || instance.isStatic()) {
                    modelData = model.getAsfData();
                } else {
                    modelData = model.getAsfData(blockNumber + 1);
                }
            }
            if (modelData) {
                asfData = asfData.concat(modelData);
            }
        });
        if (instance.isPage() || instance.isStatic()) {
            return asfData;
        }
        var tableData = dataUtils.getBaseAsfData(asfProperty, null, null, instance.getTextValue());
        if (!instance.isStatic() && !instance.isPage()) {
            tableData.type = _constants.WIDGET_TYPE.appendable_table;
        }
        tableData.data = asfData;
        if (asfProperty.tableCounterData) {
            return asfProperty.tableCounterData.concat(tableData);
        } else {
            return asfData;
        }
    };

    var getAsfDataWithId = function getAsfDataWithId(id, asfData) {
        if (!asfData.data) {
            return;
        }
        for (var i = 0; i < asfData.data.length; i++) {
            if (asfData.data[i].id == id) {
                return asfData.data[i];
            }
        }
    };

    this.setAsfData = function (asfData) {
        if (!asfData) {
            if (!instance.isPage() && !instance.isStatic()) {
                while (instance.modelBlocks.length) {
                    this.removeRow(instance.modelBlocks.length - 1);
                }

                for (var initRowIndex = 0; initRowIndex < asfProperty.config['init-rows']; initRowIndex++) {
                    this.createRow();
                }
            }
            return;
        }
        var ids = tableUtils.extractComponentIds(instance.asfProperty.layout.components);
        var blocksNumbers = tableUtils.extractBlockNumbers(asfData, ids);
        if (!instance.isPage() && !instance.isStatic()) {
            // удаляем все текущие ряды, и создаем новые согласно asfData
            while (instance.modelBlocks.length) {
                this.removeRow(instance.modelBlocks.length - 1);
            }
            while (blocksNumbers.length > instance.modelBlocks.length) {
                this.createRow();
            }
        }
        iterateAllModels(function (model, blockNumber) {
            if (blockNumber == -1) {
                model.setAsfData(getAsfDataWithId(model.asfProperty.id, asfData), asfData.nodeUUID);
            } else {
                if (instance.isPage() || instance.isStatic()) {
                    model.setAsfData(getAsfDataWithId(model.asfProperty.id, asfData), asfData.nodeUUID);
                } else {
                    var bNumber = blocksNumbers[blockNumber];
                    model.setAsfData(getAsfDataWithId(model.asfProperty.id + "-b" + bNumber, asfData), asfData.nodeUUID);
                }
            }
        });
    };

    this.isHaveHeader = function () {
        return this.asfProperty.config && this.asfProperty.config.isHaveHeaders;
    };

    this.isPage = function () {
        return asfProperty.type == _constants.WIDGET_TYPE.page;
    };

    this.isStatic = function () {
        return !instance.isDynamic();
    };

    this.isDynamic = function () {
        return asfProperty.config && asfProperty.config.appendRows;
    };

    this.isParagraph = function () {
        return asfProperty.style && asfProperty.style.wrap;
    };

    this.getBlockNumbers = function () {
        var modelBlocks = [];
        if (this.isDynamic()) {
            instance.modelBlocks.forEach(function (modelBlock) {
                modelBlocks.push(modelBlock[0].asfProperty.tableBlockIndex);
            });
        }
        return modelBlocks;
    };

    this.destroy = function () {
        iterateAllModels(function (model) {
            model.destroy();
        });
        this.model.trigger(_constants.EVENT_TYPE.modelDestroyed);
        instance.off();
    };

    // заполняем данными по умолчанию

    if (this.isPage() || this.isStatic() || playerModel.building) {
        this.createRow();
    } else {

        dataUtils.updateHeaderComponents(asfProperty);

        if (this.isHaveHeader()) {
            this.headerModelBlock = tableUtils.createModels(this, true);
        }

        for (var initRowIndex = 0; initRowIndex < asfProperty.config['init-rows']; initRowIndex++) {
            this.createRow();
        }
    }
};

TableModel.prototype = Object.create(_model2.default.prototype);
TableModel.prototype.constructor = TableModel;

TableModel.parseId = function (id) {
    if (!_underscore2.default.isString(id)) return null;
    var reg = /^(.*)_copy(\d+)$/;
    var match = id.match(reg);

    var res = {
        id: id,
        isCopy: match !== null
    };
    if (res.isCopy) {
        res.num = parseInt(match[2]);
        res.prefix = match[1];
    }
    return res;
};

TableModel.getUniqueId = function (asfProperty, tableId, playerModel) {
    var parsedId = TableModel.parseId(asfProperty.id);
    if (!parsedId.isCopy) {
        parsedId.prefix = parsedId.id;
    }
    var candidateId = parsedId.id;
    var index = 1;
    while (playerModel.getModelWithId(candidateId, tableId)) {
        candidateId = parsedId.prefix + "_copy" + index;
        index++;
    }
    return candidateId;
};

/***/ }),

/***/ 240:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = TextBoxModel;

var _constants = __webpack_require__(2);

var _model = __webpack_require__(25);

var _model2 = _interopRequireDefault(_model);

var _maskUtils = __webpack_require__(100);

var maskUtils = _interopRequireWildcard(_maskUtils);

var _xregexp = __webpack_require__(154);

var _xregexp2 = _interopRequireDefault(_xregexp);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Created by exile
 * Date: 03.02.16
 * Time: 16:25
 */
function TextBoxModel(asfProperty, playerModel) {
    _model2.default.call(this, this, asfProperty, playerModel);

    var instance = this;

    if (asfProperty.config && asfProperty.config['input-mask']) {

        this.getTextValue = function () {
            return maskUtils.getValueForInput(instance.value, splitMask);
        };

        var splitMask = maskUtils.splitMask(asfProperty.config['input-mask']);

        this.getSpecialErrors = function () {
            if (asfProperty.config['input-mask']) {
                if (instance.value != '') {
                    var mask = maskUtils.getXRegExpression(asfProperty.config['input-mask']);

                    if (!new _xregexp2.default(mask).test(instance.value)) {
                        return { id: asfProperty.id, errorCode: _constants.INPUT_ERROR_TYPE.wrongValue };
                    }
                }
            }
        };
    }
};

TextBoxModel.prototype = Object.create(_model2.default.prototype);
TextBoxModel.prototype.constructor = TextBoxModel;

/***/ }),

/***/ 241:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = UserLinkModel;

var _underscore = __webpack_require__(7);

var _underscore2 = _interopRequireDefault(_underscore);

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

var _constants = __webpack_require__(2);

var _model = __webpack_require__(25);

var _model2 = _interopRequireDefault(_model);

var _componentUtils = __webpack_require__(15);

var compUtils = _interopRequireWildcard(_componentUtils);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _UserChooser = __webpack_require__(81);

var _userLinkUtils = __webpack_require__(158);

var userLinkUtils = _interopRequireWildcard(_userLinkUtils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * модель компоненты выбора пользователя
 * asfData:
 * {
     *        "id": "user1",
     *        "type": "entity",
     *        "value": "Абрамов Егор Григорьевич",
     *        "key": "1ba41729-871f-4575-88d8-5a6c3de6297a",
     *        "formatVersion": "V1",
     *        "manual_tags": {"identifier" : "введенный вручную тэг"}
     *    }
 *
 * множественное значение
 * {
     *        "id": "user1",
     *        "type": "entity",
     *        "value": "formattedName1, formattedName2, formattedName3",
     *        "key": "identifier1;identifier2",
     *        "formatVersion": "V1",
     *        "manual_tags": {"identifier" : "введенный вручную тэг"}
     *    }
 * так же следует учесть, что идентификаторы могут иметь приставки
 * без приставки  пользователь
 * g-  группа
 * text-  просто введенный пользователем текст
 * contact-  выбранный контакт из адресной книги
 *
 * @param asfProperty
 * @param playerModel
 * @constructor
 */
function UserLinkModel(asfProperty, playerModel) {
    _model2.default.call(this, this, asfProperty, playerModel);

    var instance = this;

    instance.value = [];

    var textValue = "";

    var masterModel = null;

    var textNumber = 0;

    this.getNextTextNumber = function () {
        return textNumber++;
    };

    this.checkNextTextNumber = function () {
        instance.getSelectedIds().forEach(function (id) {
            if (id.startsWith(_UserChooser.USER_CHOOSER_SUFFIXES.text + '-')) {
                textNumber = Math.max(textNumber, id.substring(5)) + 1;
            }
        });
    };

    this.findMaster = function () {
        setTimeout(function () {
            if (asfProperty.config.depends && !playerModel.building) {
                masterModel = compUtils.findMaster(asfProperty, playerModel);
                if (!masterModel) {
                    return;
                }
                masterModel.on(_constants.EVENT_TYPE.valueChange, function () {
                    instance.updateModelData();
                });
            }
        }, 0);
    };

    this.getSelectedIds = function (target) {
        var selectedIds = [];
        if (_underscore2.default.isUndefined(target)) {
            target = instance.value;
        }
        if (target && target instanceof Array) {
            target.forEach(function (user) {
                selectedIds.push(user.personID);
            });
        }
        return selectedIds;
    };

    this.getEntityType = function () {
        return _constants.ENTITY_TYPE.users;
    };

    this.getFilterPositionId = function () {
        return userLinkUtils.getFilterEntityId(masterModel, _constants.ENTITY_TYPE.positions);
    };

    this.setValueFromInput = function (inputValue) {
        this.doSetValue(inputValue);
        playerModel.trigger(_constants.EVENT_TYPE.valueChange, [this, this.value]);
    };

    this.getFilterDepartmentId = function () {
        return userLinkUtils.getFilterEntityId(masterModel, _constants.ENTITY_TYPE.departments);
    };

    /**
     * записываем значения те что пришли сначала, запоминаем пришедшие таги и текстовые значения
     * потом поднимаем с сервера информацию о выбранных значениях и обратно присваиваем таги и текстовые значения
     * @param asfData
     */
    this.setAsfData = function (asfData) {
        if (!asfData || !asfData.key) {
            return;
        }
        textValue = asfData.value;

        var parsedAsfData = userLinkUtils.parseAsfData(asfData, ";", ",");

        var itemIds = parsedAsfData.itemIds;
        var manualTags = parsedAsfData.manualTags;
        var tags = parsedAsfData.tags;

        var selectedValues = [];
        itemIds.forEach(function (itemId, index) {
            var person = {};
            person.personID = itemId;
            person.personName = tags[index];
            person.tagName = manualTags[itemId] || person.personName;
            person.customFields = {};
            selectedValues.push(person);
        });
        instance.doSetValue(selectedValues);

        if (itemIds.length > 0) {

            selectedValues = selectedValues.slice();

            api.getUsersInfo(itemIds, instance.getLocale(), function (users) {
                selectedValues.forEach(function (selectedValue, selectedIndex) {
                    var found = users.findElement("personID", selectedValue.personID);
                    if (found) {
                        found.tagName = manualTags[found.personID] || found.personName;
                        selectedValues[selectedIndex] = found;
                    }
                });
                instance.doSetValue(selectedValues);
            });
        }
    };

    this.updateModelData = function () {
        var selectedIds = instance.getSelectedIds();

        var filterDepartmentID = instance.getFilterDepartmentId();
        var filterPositionID = instance.getFilterPositionId();

        if (!filterDepartmentID && !filterPositionID) {
            instance.doSetValue(null);
            return;
        }

        api.checkUsers(selectedIds, filterPositionID, filterDepartmentID, instance.isShowNoPosition(), true, instance.getLocale(), function (users) {
            instance.doSetValue(users, true, true);
            instance.trigger(_constants.EVENT_TYPE.dataLoad, instance);
        });
    };

    this.isShowNoPosition = function () {
        if (asfProperty.config) {
            return asfProperty.config['show-without-position'];
        }
        return false;
    };

    this.doSetValue = function (newValue) {
        var fireEvent = true;
        if (this.value == newValue || instance.getSelectedIds().equals(instance.getSelectedIds(newValue))) {
            fireEvent = false;
        }
        if (!newValue) {
            this.value = [];
        } else if (!(newValue instanceof Array)) {
            this.value = [newValue];
        } else {
            this.value = newValue;
        }

        textValue = "";
        var sign = "";
        this.value.forEach(function (val) {
            instance.updateUserLinkItem(val);
            textValue += sign + val.tagName;
            sign = ", ";
        });

        if (fireEvent) {
            this.fireChangeEvents();
        } else {
            instance.trigger(_constants.EVENT_TYPE.dataLoad, [instance]);
        }

        instance.checkNextTextNumber();
    };

    this.updateUserLinkItem = function (item) {
        if (item.personID.indexOf(_UserChooser.USER_CHOOSER_SUFFIXES.group) === 0) {
            item.tagName = item.personName;
        } else if (item.personID.indexOf(_UserChooser.USER_CHOOSER_SUFFIXES.contact) === 0) {
            item.tagName = item.personName;
        } else if (item.personID.indexOf(_UserChooser.USER_CHOOSER_SUFFIXES.text) === 0) {
            item.tagName = item.personName;
            item.customFields = {
                text: "true",
                calendarColor: "#df6c6d",
                calendarStatusLabel: i18n.tr("Введен вручную"),
                type: _UserChooser.USER_CHOOSER_SUFFIXES.text
            };
        } else {
            var changeTag = true;
            if (item.tagName && item.tagName != item.personName) {
                changeTag = false;
            }
            item.personName = userLinkUtils.formatName(item, asfProperty);
            if (changeTag) {
                item.tagName = item.personName;
            }
        }
    };

    this.getTextValue = function () {
        return textValue;
    };

    this.getKey = function () {
        var array = instance.getSelectedIds();
        if (array.length == 0) {
            return null;
        } else {
            return array.arrayToString(";");
        }
    };

    this.getManualTags = function () {
        var manualTags = {};

        this.value.forEach(function (val) {
            if (val.tagName != val.personName) {
                manualTags[val.personID] = val.tagName;
            }
        });
        return manualTags;
    };

    this.getAsfData = function (blockNumber) {

        var text = null;
        var key = instance.getKey();
        if (key) {
            text = textValue;
        }

        var base = dataUtils.getBaseAsfData(asfProperty, blockNumber, text, key);
        base.manualTags = instance.getManualTags();
        base.formatVersion = "V1";
        return base;
    };

    this.findMaster();
};

UserLinkModel.prototype = Object.create(_model2.default.prototype);
UserLinkModel.prototype.constructor = UserLinkModel;

/***/ }),

/***/ 242:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = Selection;

var _componentUtils = __webpack_require__(15);

var _constants = __webpack_require__(2);

var _underscore = __webpack_require__(7);

var _underscore2 = _interopRequireDefault(_underscore);

var _layoutUtils = __webpack_require__(65);

var _utils = __webpack_require__(82);

var utils = _interopRequireWildcard(_utils);

var _math = __webpack_require__(112);

var _math2 = _interopRequireDefault(_math);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var SelectionMode = {
    ON: 1,
    OFF: 2
};

function Selection(layout) {
    var thiz = this;
    (0, _componentUtils.makeBus)(this);

    this.cells = [];
    this.lastCell = null;

    this.layout = layout;

    this.triggerChange = function () {
        this.trigger(_constants.EVENT_TYPE.selectionChange);
    };

    this.filterRow = function (row) {
        return _underscore2.default.filter(this.cells, eqRow(row));
    };

    this.deleteRow = function (row) {
        this.rows--;

        var afterRowIndex = _math2.default.max(0, row - 1);

        this.cells = _underscore2.default.map(this.cells, decRow(utils.gt(afterRowIndex)));
        this.ensureUnique();
        this.triggerChange();
    };

    this.deleteColumn = function (col) {
        this.cols--;

        var afterColumnIndex = _math2.default.max(0, col - 1);
        this.cells = _underscore2.default.map(this.cells, decColumn(utils.gt(afterColumnIndex)));
        this.ensureUnique();
        this.triggerChange();
    };

    this.selectOne = function (cell) {
        if (!cell) return;
        this.cells = [cell];
        this.setLastCell(cell, SelectionMode.ON);
        this.trigger(_constants.EVENT_TYPE.selectSingleCell, cell);
    };

    function indexInCells(cells, cell) {
        return _underscore2.default.findIndex(cells, eqCell(cell));
    }

    function isCellSelected(cells, cell) {
        return indexInCells(cells, cell) > -1;
    }

    this.indexOf = function (cell) {
        return indexInCells(thiz.cells, cell);
    };

    this.toggleCell = function (cell) {
        var index = this.indexOf(cell);
        var mode;
        if (index > -1) {
            thiz.cells.splice(index, 1);
            mode = SelectionMode.OFF;
        } else {
            thiz.cells.push(cell);
            mode = SelectionMode.ON;
        }
        this.setLastCell(cell, mode);
        this.triggerChange();
    };

    this.addCell = function (cell) {
        if (this.isCellSelected(cell)) return;
        this.cells.push(cell);
        this.triggerChange();
    };

    this.removeCell = function (cell) {
        if (!this.isCellSelected(cell)) return;
        var index = this.indexOf(cell);
        this.cells.splice(index, 1);
        this.triggerChange();
    };

    this.isCellSelected = function (cell) {
        return isCellSelected(thiz.cells, cell);
    };

    this.isCompSelected = _underscore2.default.compose(this.isCellSelected, _layoutUtils.compToCell);

    var getRow = _underscore2.default.property(0);
    var getCol = _underscore2.default.property(1);

    var rowLens = {
        view: getRow,
        update: function update(value, cell) {
            cell[0] = value;
            return cell;
        }
    };

    var colLens = {
        view: getCol,
        update: function update(value, cell) {
            cell[1] = value;
            return cell;
        }
    };

    var incColumn = utils.changeIf.bind(thiz, utils.inc, colLens);
    var incRow = utils.changeIf.bind(thiz, utils.inc, rowLens);
    var decColumn = utils.changeIf.bind(thiz, utils.dec, colLens);
    var decRow = utils.changeIf.bind(thiz, utils.dec, rowLens);
    var eqCol = utils.eqLens.bind(thiz, colLens);
    var eqRow = utils.eqLens.bind(thiz, rowLens);

    function eqCell(cell) {
        return function (oCell) {
            return _underscore2.default.isEqual(cell, oCell);
        };
    }

    this.makeColumn = function (col) {
        var array = [];
        var i;
        for (i = 0; i < this.layout.rows; i++) {
            array.push([i, col]);
        }
        return array;
    };

    this.makeRow = function (row) {
        var array = [];
        var i;
        for (i = 0; i < this.layout.columns; i++) {
            array.push([row, i]);
        }
        return array;
    };

    this.insertColumnAfter = function (col) {
        this.cells = _underscore2.default.map(this.cells, incColumn(utils.gt(col)));
    };

    this.insertRowAfter = function (row) {
        this.cells = _underscore2.default.map(this.cells, incRow(utils.gt(row)));
    };

    this.ensureUnique = function () {
        var matrix = (0, _layoutUtils.makeLayoutMatrix)(this.layout);

        this.cells = _underscore2.default.filter(this.cells, function (cell) {
            return matrix[cell[0]][cell[1]];
        });

        this.cells = _underscore2.default.filter(this.cells, function (cell, key) {
            return thiz.indexOf(cell) === key;
        });
    };

    this.selectRow = function (row, append) {
        var rowCells = this.makeRow(row);
        if (append === true) {
            var selected = this.isRowSelected(row);
            if (!selected) this.cells = this.cells.concat(rowCells);else this.cells = _underscore2.default.reject(this.cells, isCellSelected.bind(null, rowCells));
        } else {

            this.trigger(_constants.EVENT_TYPE.selectSingleCell, [row, 0]);
            this.cells = rowCells;
        }
        this.ensureUnique();
        this.triggerChange();
    };

    this.lensLength = function (lens, value) {
        var matrix = (0, _layoutUtils.makeLayoutMatrix)(this.layout);
        return _underscore2.default.chain(matrix).flatten(true).filter(Boolean).map(_layoutUtils.compToCell).filter(utils.eqLens(lens, value)).value().length;
    };

    this.isColumnSelected = function (col) {
        return _underscore2.default.filter(this.cells, eqCol(col)).length === this.lensLength(colLens, col);
    };

    this.isRowSelected = function (row) {
        return _underscore2.default.filter(this.cells, eqRow(row)).length === this.lensLength(rowLens, row);
    };

    this.selectColumn = function (col, toggle) {

        var colCells = this.makeColumn(col);
        if (toggle === true) {
            var selected = this.isColumnSelected(col);
            if (!selected) this.cells = this.cells.concat(colCells);else this.cells = _underscore2.default.reject(this.cells, isCellSelected.bind(null, colCells));
        } else {

            this.trigger(_constants.EVENT_TYPE.selectSingleCell, [0, col]);

            this.cells = colCells;
        }

        this.ensureUnique();
        this.triggerChange();
    };

    this.deselectRow = function (row) {
        this.cells = _underscore2.default.reject(this.cells, eqRow(row));
    };

    this.deselectColumn = function (col) {
        this.cells = _underscore2.default.reject(this.cells, eqCol(col));
    };

    this.setLastCell = function (cell, mode) {
        this.lastCell = cell;
        this.lastMode = mode;
        this.beforeArea = undefined;
    };

    this.selectArea = function (end) {
        if (!_underscore2.default.isArray(this.lastCell) || !_underscore2.default.isArray(end) || !_underscore2.default.isNumber(this.lastMode)) return;

        if (this.lastMode === SelectionMode.ON) this.addArea(end);else if (this.lastMode == SelectionMode.OFF) this.removeArea(end);

        this.triggerChange();
    };

    function expandCellFromLayout(cell) {
        var comp = (0, _layoutUtils.find)((0, _layoutUtils.cellToComp)(cell), thiz.layout);
        if (!comp) return [cell];
        return [(0, _layoutUtils.compToCell)(comp), (0, _layoutUtils.endCompToCell)(comp)];
    }

    function makeRectFromCells(cells) {
        var ends = _underscore2.default.chain(cells).map(expandCellFromLayout).flatten(true).value();

        var rect = {
            y1: minRow(ends),
            y2: maxRow(ends),
            x1: minCol(ends),
            x2: maxCol(ends)
        };
        rect.height = rect.y2 - rect.y1 + 1;
        rect.width = rect.x2 - rect.x1 + 1;
        rect.area = rect.height * rect.width;
        return rect;
    }

    function makeRectFromCell(cell) {
        return makeRectFromCells([cell]);
    }

    function iterateBetweenCells(f, cells) {
        var rect = makeRectFromCells(cells);

        var x, y;
        for (y = rect.y1; y <= rect.y2; y++) {
            for (x = rect.x1; x <= rect.x2; x++) {
                f([y, x]);
            }
        }
    }

    this.restoreBeforeArea = function () {
        if (!_underscore2.default.isUndefined(this.beforeArea)) this.cells = [].concat(this.beforeArea);else this.beforeArea = [].concat(this.cells);
    };

    this.addArea = function (end) {
        this.restoreBeforeArea();

        iterateBetweenCells(function (cell) {
            thiz.cells.push(cell);
        }, [this.lastCell, end]);
        this.ensureUnique();
    };

    this.removeArea = function (end) {
        this.restoreBeforeArea();
        var remove = [];

        iterateBetweenCells(function (cell) {
            remove.push(cell);
        }, [this.lastCell, end]);

        this.cells = _underscore2.default.reject(this.cells, function (selected) {
            return _underscore2.default.some(remove, eqCell(selected));
        });

        this.ensureUnique();
    };

    this.clear = function () {
        if (this.cells.length === 0) {
            return;
        }
        this.cells = [];
        this.lastCell = null;
        this.lastMode = null;
        this.triggerChange();
    };

    this.selectAll = function () {
        if (this.isAllSelected()) return;

        this.cells = [];
        var row, col;
        for (row = 0; row < this.layout.rows; row++) {
            for (col = 0; col < this.layout.columns; col++) {
                this.cells.push([row, col]);
            }
        }this.triggerChange();
    };

    this.isAllSelected = function () {
        this.ensureUnique();
        return this.cells.length === this.layout.rows * this.layout.columns;
    };

    this.selectWithInfo = function (selectInfo) {
        var cell = [selectInfo.row, selectInfo.column];
        if (selectInfo.toggle !== true && selectInfo.area !== true) {
            this.selectOne(cell);
            return;
        }
        if (selectInfo.area !== true) {
            this.toggleCell(cell);
            return;
        }
        this.selectArea(cell);
    };

    function max(lens, cells) {
        return _underscore2.default.max(_underscore2.default.map(cells, lens.view));
    }

    var maxRow = max.bind(null, rowLens);
    var maxCol = max.bind(null, colLens);

    function min(lens, cells) {
        return _underscore2.default.min(_underscore2.default.map(cells, lens.view));
    }

    var minRow = min.bind(null, rowLens);
    var minCol = min.bind(null, colLens);

    function sum(a, b) {
        return a + b;
    }

    this.constructMerge = function () {
        this.ensureUnique();

        var rect = makeRectFromCells(this.cells);

        var areasSum = _underscore2.default.chain(this.cells).map(makeRectFromCell).map(_underscore2.default.property('area')).reduce(sum, 0).value();

        if (rect.area !== areasSum) return null;

        return {
            row: rect.y1,
            column: rect.x1,

            rowspan: rect.height,
            colspan: rect.width
        };
    };
};

Selection.prototype.constructor = Selection;

/***/ }),

/***/ 243:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.bus = undefined;
exports.createPlayer = createPlayer;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

var _services = __webpack_require__(23);

var services = _interopRequireWildcard(_services);

var _PlayerModel = __webpack_require__(233);

var _PlayerModel2 = _interopRequireDefault(_PlayerModel);

var _PlayerView = __webpack_require__(260);

var _PlayerView2 = _interopRequireDefault(_PlayerView);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var bus = exports.bus = (0, _jquery2.default)({});
function createPlayer() {
    var model = new _PlayerModel2.default();
    var view = new _PlayerView2.default(model);

    return {
        model: model,
        view: view,
        showFormData: function showFormData(formUid, version, dataUid, dataVersion) {
            var instance = this;
            if (!formUid && !dataUid) {
                return;
            }
            services.showWaitWindow();
            if (!dataUid) {
                _jquery2.default.when(api.loadDefaultAsfData(formUid)).then(function (data) {
                    return _jquery2.default.when(api.loadFormDefinition(formUid, version), data);
                }).then(instance.dataLoaded).fail(function () {
                    services.hideWaitWindow();
                });
            } else if (dataUid) {
                _jquery2.default.when(api.loadAsfData(dataUid, dataVersion)).then(function (data) {
                    return _jquery2.default.when(api.loadFormDefinition(data.form, data.formVersion), data);
                }).then(instance.dataLoaded).fail(function () {
                    services.hideWaitWindow();
                });
            }
        },

        showFormByCode: function showFormByCode(formCode, version) {
            var instance = this;
            if (!formCode) {
                return;
            }
            services.showWaitWindow();
            _jquery2.default.when(api.loadFormDefinitionByCode(formCode, version)).then(function (definition) {
                var formUUID = definition.uuid;
                return _jquery2.default.when(definition, api.loadDefaultAsfData(formUUID));
            }).then(instance.dataLoaded).fail(function () {
                services.hideWaitWindow();
            });
        },

        dataLoaded: function dataLoaded(definition, data) {
            model.buildModelsDefinition(definition);

            services.hideWaitWindow();

            if (data) {
                model.setAsfData(data);
            }
        },

        saveFormData: function saveFormData(handler) {
            if (!model.isValid()) {
                throw new Error('incorrect values');
            }
            var asfData = model.getAsfData();
            _jquery2.default.when(api.saveAsfData(asfData.data, model.formId, model.asfDataId)).then(function (asfDataId) {
                model.asfDataId = asfDataId;
                handler(asfDataId);
            });
        },
        destroy: function destroy() {
            view.container.empty();
            view.destroy();
            model.destroy();
        }
    };
};

/***/ }),

/***/ 244:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = AddressLinkView;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _constants = __webpack_require__(2);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _view = __webpack_require__(11);

var _view2 = _interopRequireDefault(_view);

var _TagArea = __webpack_require__(72);

var _TagArea2 = _interopRequireDefault(_TagArea);

var _services = __webpack_require__(23);

var services = _interopRequireWildcard(_services);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Created by exile
 * Date: 03.02.16
 * Time: 16:25
 */
function AddressLinkView(model, playerView, editable) {
    _view2.default.call(this, this, model, playerView);

    var enabled = true;
    var instance = this;

    var tagArea = new _TagArea2.default({ width: "calc(100% - 30px)" }, 0, false, false, false);
    var chooserButton = (0, _jquery2.default)("<button/>", { class: "asf-browseButton" });

    var fileNameLabel = (0, _jquery2.default)('<span>', { class: "asf-label asf-file-name" });

    if (editable) {
        this.container.append(tagArea.getWidget());
        this.container.append(chooserButton);
    } else {
        this.container.append(fileNameLabel);
    }

    dataUtils.addAsFormId(tagArea.getWidget(), "input", model.asfProperty);
    dataUtils.addAsFormId(chooserButton, "button", model.asfProperty);

    this.showAddressLinkDialog = function () {
        if (!enabled) {
            return;
        }
        services.showAddressBookDialog(function (uuid, value, type, address, organization) {
            if (type === 0) {
                if (organization != null) {
                    value = value + " (" + organization + ")";
                }
            } else {
                if (address != null) {
                    value = value + " (" + address + ")";
                }
            }

            model.setValueFromInput(uuid, value, type);
        });
    };

    tagArea.on(_constants.EVENT_TYPE.tagDelete, function () {
        model.setValueFromInput(null);
    });

    chooserButton.click(function () {
        instance.showAddressLinkDialog();
    });

    fileNameLabel.click(function () {
        services.showAddressBookItem(model.value, model.type);
    });

    this.unmarkInvalid = function () {
        tagArea.markValidity(true);
    };

    this.markInvalid = function () {
        tagArea.markValidity(false);
    };

    this.updateValueFromModel = function () {
        if (model.value) {
            var valueObject = { value: model.value, tagName: model.getTextValue() };
            tagArea.setValues([valueObject]);
        } else {
            tagArea.setValues([]);
        }

        fileNameLabel.html(model.getTextValue());
        instance.unmarkInvalid();
    };

    this.setEnabled = function (newEnabled) {
        enabled = newEnabled;
        tagArea.setEditable(newEnabled);
    };

    this.setEnabled(!dataUtils.isReadOnly(model));

    this.updateValueFromModel();
};

AddressLinkView.prototype = Object.create(_view2.default.prototype);
AddressLinkView.prototype.constructor = AddressLinkView;

/***/ }),

/***/ 245:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = BuilderComponentView;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

var _constants = __webpack_require__(2);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _view = __webpack_require__(11);

var _view2 = _interopRequireDefault(_view);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Created by exile
 * Date: 03.02.16
 * Time: 16:25
 */
function BuilderComponentView(model, playerView) {

    _view2.default.call(this, this, model, playerView);

    var textBox = (0, _jquery2.default)('<input/>', { type: 'text', class: 'asf-textBox', 'readonly': "true" });
    dataUtils.addAsFormId(textBox, "input", model.asfProperty);

    this.unmarkInvalid = function () {
        textBox.removeClass('asf-invalidInput');
    };

    this.markInvalid = function () {
        textBox.addClass('asf-invalidInput');
    };

    this.updateValueFromModel = function () {
        var value = "";
        switch (model.asfProperty.type) {
            case _constants.WIDGET_TYPE.doclink:
                if (model.asfProperty.config.format) {
                    value = model.asfProperty.config.format;
                }
                break;
            case _constants.WIDGET_TYPE.custom:
                if (model.asfProperty.config.component) {
                    api.getCustomComponent(model.asfProperty.config.component, function (component) {
                        textBox.val(component.name);
                    });
                }

                break;
            case _constants.WIDGET_TYPE.docnumber:
                {
                    value = model.getAttributeTitle();
                    break;
                }
        }

        textBox.val(value);
    };

    this.updateValueFromModel();

    this.container.append(textBox);
};

BuilderComponentView.prototype = Object.create(_view2.default.prototype);
BuilderComponentView.prototype.constructor = BuilderComponentView;

/***/ }),

/***/ 246:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = CheckBoxView;
exports.CheckBoxBuilderView = CheckBoxBuilderView;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _i18n = __webpack_require__(14);

var _i18n2 = _interopRequireDefault(_i18n);

var _constants = __webpack_require__(2);

var _styleUtils = __webpack_require__(21);

var styleUtils = _interopRequireWildcard(_styleUtils);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _view = __webpack_require__(11);

var _view2 = _interopRequireDefault(_view);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function CheckBoxView(model, playerView) {
    _view2.default.call(this, this, model, playerView);

    var checksList = [];

    var instance = this;

    var enabled = true;

    this.updateDataFromModel = function () {
        instance.container.empty();
        checksList = [];
        model.listCurrentElements.forEach(function (listElement) {
            var checkBox = (0, _jquery2.default)('<input/>', { type: 'checkBox', class: 'asf-checkBox' });
            dataUtils.addAsFormId(checkBox, "input", model.asfProperty);

            var $label = (0, _jquery2.default)('<div/>', { class: "asf-buttonLabel" });
            styleUtils.applyLabelStyle($label, model.asfProperty.style, true);

            $label.append(checkBox);
            $label.append(listElement.label);

            $label.click(function (evt) {
                if (evt.target !== $label[0] || !enabled) {
                    return;
                }
                if (checkBox.prop("checked")) {
                    checkBox.removeProp("checked", "checked");
                } else {
                    checkBox.prop("checked", "checked");
                }
                instance.updateModelValue();
            });

            // здесь именно клик, потому как мне нужно следить только за тем что делает пользователь, а не за тем как обновляется
            //значение из модели
            checkBox.click(function (evt) {
                instance.updateModelValue();
            });
            checksList.push({ value: listElement.value, component: checkBox, label: $label });
            instance.container.append($label);
        });

        instance.setEnabled(enabled);

        instance.updateValueFromModel();

        model.playerModel.trigger(_constants.EVENT_TYPE.cellSizeChange, model);
    };

    this.unmarkInvalid = function () {
        this.container.removeClass('asf-invalidInput');
    };

    this.markInvalid = function () {
        this.container.addClass('asf-invalidInput');
    };

    this.updateModelValue = function () {
        var selectedValues = [];
        checksList.forEach(function (checkElement) {
            if (checkElement.component.prop("checked")) {
                selectedValues.push(checkElement.value);
            }
        });
        model.setValue(selectedValues);
        this.unmarkInvalid();
    };

    this.updateValueFromModel = function () {
        var selectedKeys = model.getValue();
        checksList.forEach(function (checkElement) {
            if (selectedKeys && selectedKeys.indexOf(checkElement.value) > -1) {
                (0, _jquery2.default)(checkElement.component).prop("checked", "checked");
            } else {
                (0, _jquery2.default)(checkElement.component).removeProp("checked");
            }
        });
    };

    this.setEnabled = function (newEnabled) {
        enabled = newEnabled;
        checksList.forEach(function (checkElement) {
            if (enabled) {
                (0, _jquery2.default)(checkElement.component).removeProp("disabled");
            } else {
                (0, _jquery2.default)(checkElement.component).prop("disabled", true);
            }
        });
    };
    this.setEnabled(!dataUtils.isReadOnly(model));

    this.updateDataFromModel();

    model.on(_constants.EVENT_TYPE.dataLoad, function () {
        instance.updateDataFromModel();
    });
};

CheckBoxView.prototype = Object.create(_view2.default.prototype);
CheckBoxView.prototype.constructor = CheckBoxView;

function CheckBoxBuilderView(model, playerView) {
    CheckBoxView.call(this, model, playerView);

    if (model.listCurrentElements.length === 0) {
        var emptyLabel = (0, _jquery2.default)("<div>", { class: "asf-label" });
        emptyLabel.text(_i18n2.default.tr("Выбор вариантов"));
        this.container.append(emptyLabel);
    }
};

CheckBoxBuilderView.prototype = Object.create(CheckBoxView.prototype);
CheckBoxBuilderView.prototype.constructor = CheckBoxBuilderView;

/***/ }),

/***/ 247:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = ComboBoxView;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _constants = __webpack_require__(2);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _view = __webpack_require__(11);

var _view2 = _interopRequireDefault(_view);

var _services = __webpack_require__(23);

var services = _interopRequireWildcard(_services);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ComboBoxView(model, playerView) {

    _view2.default.call(this, this, model, playerView);

    var dropdownBox = (0, _jquery2.default)('<div/>', { class: 'asf-dropdown-input' });

    var button = (0, _jquery2.default)("<button/>", { class: "asf-dropdown-button" });

    dataUtils.addAsFormId(dropdownBox, "input", model.asfProperty);
    dataUtils.addAsFormId(button, "button", model.asfProperty);

    this.container.append(dropdownBox);
    this.container.append(button);

    this.container.css("min-width", "80px");

    var enabled = true;

    var showDropDown = function showDropDown(event) {
        if (!enabled) {
            return;
        }

        event.stopPropagation();
        event.stopImmediatePropagation();
        event.preventDefault();

        if (services.isShownDropDown(instance.container)) {
            services.closeDropDown(instance.container);
            return;
        }

        var values = [];
        var selectedKeys = model.getValue();
        model.listCurrentElements.forEach(function (listElement) {
            var selected = selectedKeys && selectedKeys.indexOf(listElement.value) > -1;
            if (selected) {
                values.push({ value: listElement.value, title: listElement.label, selected: true });
            } else {
                values.push({ value: listElement.value, title: listElement.label, selected: false });
            }
        });

        services.showDropDown(values, instance.container, null, function (selectedValue) {
            model.setValueFromInput(selectedValue);
            instance.updateValueFromModel();
        });
    };

    button.click(function (event) {
        showDropDown(event);
    });

    dropdownBox.click(function (event) {
        showDropDown(event);
    });

    var instance = this;

    this.unmarkInvalid = function () {
        dropdownBox.removeClass('asf-invalidInput');
    };

    this.markInvalid = function () {
        dropdownBox.addClass('asf-invalidInput');
    };

    this.updateValueFromModel = function () {
        instance.unmarkInvalid();
        dropdownBox.html(model.getTextValue());
    };

    this.setEnabled = function (newEnabled) {
        enabled = newEnabled;
        if (enabled) {
            dropdownBox.removeClass('asf-disabledInput');
        } else {
            dropdownBox.addClass('asf-disabledInput');
        }
    };
    this.setEnabled(!dataUtils.isReadOnly(model));

    this.updateValueFromModel();

    model.on(_constants.EVENT_TYPE.dataLoad, function () {
        instance.updateValueFromModel();
    });
};

ComboBoxView.prototype = Object.create(_view2.default.prototype);
ComboBoxView.prototype.constructor = ComboBoxView;

/***/ }),

/***/ 248:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = CustomComponentView;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _logger = __webpack_require__(101);

var LOGGER = _interopRequireWildcard(_logger);

var _constants = __webpack_require__(2);

var _componentUtils = __webpack_require__(15);

var compUtils = _interopRequireWildcard(_componentUtils);

var _view = __webpack_require__(11);

var _view2 = _interopRequireDefault(_view);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function CustomComponentView(model, playerView, editable) {
    _view2.default.call(this, this, model, playerView);

    var instance = this;

    var customComponent = null;

    var buildComponent = function buildComponent() {
        if (customComponent) {
            return;
        }
        customComponent = model.customComponent;
        if (!customComponent) {
            return;
        }

        try {
            var element = (0, _jquery2.default)(customComponent.source);
            instance.container.append(element);
        } catch (e) {
            LOGGER.logServer(e, model.playerModel.formId, model.playerModel.asfDataId);
        }

        try {
            var initCustomComponent = compUtils.createFunction(model.playerModel, customComponent.scriptSource, "var model = arguments[0], view = arguments[1], editable = arguments[2];");
            initCustomComponent(model, instance, editable);
        } catch (e) {
            LOGGER.logServer(e, model.playerModel.formId, model.playerModel.asfDataId);
        }
    };

    model.on(_constants.EVENT_TYPE.loadComponentAdditionalInfo, function () {
        buildComponent();
    });
    // добавляем проверка на скрипт и его выполнение в конец ивент пула
    setTimeout(function () {
        buildComponent();
    }, 0);
};

CustomComponentView.prototype = Object.create(_view2.default.prototype);
CustomComponentView.prototype.constructor = CustomComponentView;

/***/ }),

/***/ 249:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = DateView;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _services = __webpack_require__(23);

var services = _interopRequireWildcard(_services);

var _componentUtils = __webpack_require__(15);

var compUtils = _interopRequireWildcard(_componentUtils);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _view = __webpack_require__(11);

var _view2 = _interopRequireDefault(_view);

var _dateUtils = __webpack_require__(73);

var dateUtils = _interopRequireWildcard(_dateUtils);

var _maskUtils = __webpack_require__(100);

var maskUtils = _interopRequireWildcard(_maskUtils);

var _MobileDateView = __webpack_require__(400);

var _MobileDateView2 = _interopRequireDefault(_MobileDateView);

var _constants = __webpack_require__(2);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Created by exile
 * Date: 03.02.16
 * Time: 16:25
 */
function DateView(model, playerView) {
    if (_constants.OPTIONS.mobilePlayer) return _MobileDateView2.default.call(this, model, playerView);

    _view2.default.call(this, this, model, playerView);

    this.initialWidth = 188;
    if (!model.asfProperty.config['time-Enable']) {
        this.initialWidth = 128;
    }

    _view2.default.call(this, this, model, playerView);
    var dateBox = (0, _jquery2.default)('<input/>', { type: 'text', class: 'asf-dateBox' });
    var timeBox = (0, _jquery2.default)('<input/>', { type: 'text', class: 'asf-timeBox' });

    var button = (0, _jquery2.default)("<button/>", { class: "asf-calendar-button" });

    this.container.append(dateBox);
    this.container.append(button);
    if (model.asfProperty.config['time-Enable']) {
        this.container.append(timeBox);
    }

    maskUtils.initInput({ asfProperty: { config: { 'input-mask': '####-##-##' } } }, dateBox);
    maskUtils.initInput({ asfProperty: { config: { 'input-mask': '##:##' } } }, timeBox);

    compUtils.initInput(model, this, dateBox, true);
    compUtils.initInput(model, this, timeBox, true);

    dataUtils.addAsFormId(dateBox, "date", model.asfProperty);
    dataUtils.addAsFormId(timeBox, "time", model.asfProperty);
    dataUtils.addAsFormId(button, "button", model.asfProperty);

    var instance = this;

    var enabled = true;

    dateBox.change(function () {
        var v = dateBox.val();
        if (v.match(/[0-9]{2}-[0-9]{2}-[0-9]{4}/)) {
            if (timeBox.val() == dateUtils.INPUT_TIME_MASK) {
                timeBox.val("00:00");
            }
        }
    });

    /*dateBox.keyup(function(){
     instance.checkValid();
     });
      timeBox.keyup(function(){
     instance.checkValid();
     });*/

    button.click(function () {
        instance.showDatePicker();
    });

    dateBox.click(function () {
        instance.showDatePicker();
    });

    timeBox.focus(function () {
        instance.showTimePicker();
    });

    this.showDatePicker = function () {
        if (enabled) {
            services.showDatePicker(dateUtils.parseDate(model.getValue()), instance.container, dateBox, function (value) {
                dateBox.val(dateUtils.formatDate(value, "${yyyy}-${mm}-${dd}"));
                model.setValueFromInput(instance.getValueForModel());
            });
        }
    };

    this.showTimePicker = function () {
        if (enabled) {
            services.showTimePicker(dateUtils.parseDate(model.getValue()), instance.container, dateBox, function (value) {
                timeBox.val(dateUtils.formatDate(value, "${HH}:${MM}"));
                model.setValueFromInput(instance.getValueForModel());
            });
        }
    };

    this.getValueForModel = function () {
        return dateBox.val() + " " + timeBox.val() + ":00";
    };

    this.unmarkInvalid = function () {
        dateBox.removeClass('asf-invalidInput');
        timeBox.removeClass('asf-invalidInput');
    };

    this.markInvalid = function () {
        dateBox.addClass('asf-invalidInput');
        timeBox.addClass('asf-invalidInput');
    };

    this.updateValueFromModel = function () {
        var date = model.getValue();
        if (date == instance.getValueForModel()) {
            return;
        }
        if (!date) {
            dateBox.val('____-__-__');
            timeBox.val("00:00");
        } else {
            dateBox.val(date.substring(0, 10));
            timeBox.val(date.substring(11, 16));
        }
    };

    this.setEnabled = function (newEnabled) {
        enabled = newEnabled;
        if (enabled) {
            dateBox.removeProp("readonly");
            timeBox.removeProp("readonly");
            dateBox.removeClass('asf-disabledInput');
            timeBox.removeClass('asf-disabledInput');
        } else {
            dateBox.prop("readonly", true);
            timeBox.prop("readonly", true);
            dateBox.addClass('asf-disabledInput');
            timeBox.addClass('asf-disabledInput');
        }
    };
    this.setEnabled(!dataUtils.isReadOnly(model));

    this.updateValueFromModel();
};

DateView.prototype = Object.create(_view2.default.prototype);
DateView.prototype.constructor = DateView;

/***/ }),

/***/ 25:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(jQuery) {

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = Model;

var _componentUtils = __webpack_require__(15);

var _dataUtils = __webpack_require__(5);

var _constants = __webpack_require__(2);

function Model(model, asfProperty, playerModel) {
    this.model = model;
    this.asfProperty = asfProperty;
    this.playerModel = playerModel;
    this.value = null;
    (0, _componentUtils.makeBus)(model);
}

Model.prototype.getErrors = function () {
    var errors = [];
    if (this.asfProperty.required) {
        var isEmpty = this.isEmpty();
        if (isEmpty) {
            errors.push({ id: this.asfProperty.id, errorCode: _constants.INPUT_ERROR_TYPE.emptyValue });
        }
    }

    if (this.model.getSpecialErrors) {
        var specialErrors = this.model.getSpecialErrors();
        if (specialErrors) {
            if (specialErrors instanceof Array) {
                errors = errors.concat(specialErrors);
            } else {
                errors.push(specialErrors);
            }
        }
    }

    if (!errors.isEmpty()) {
        this.fireInvalid();
    }

    return errors;
};

// пустое ли значение в компонент
Model.prototype.isEmpty = function () {
    // не учитываем настройки обязательности полей лейбла и изображения вот такие вот дела
    return this.asfProperty.type != _constants.WIDGET_TYPE.label && this.asfProperty.type != _constants.WIDGET_TYPE.image && (!this.value || this.value instanceof Array && this.value.length === 0);
};

// пустое ли значение в компонент
Model.prototype.fireInvalid = function () {
    this.trigger(_constants.EVENT_TYPE.markInvalid, [this.model, this.value]);
};

// локаль компонента
Model.prototype.getLocale = function () {
    return (0, _dataUtils.getComponentLocale)(this.asfProperty);
};

/**
 * правильное ли значение в компоненте
 * @returns {boolean}
 */
Model.prototype.isValid = function () {
    return this.getErrors().length == 0;
};

/**
 * изменение значения (обновляет модель и генерирует события)
 * @param newValue
 */
Model.prototype.setValue = function (newValue) {
    this.doSetValue(newValue);
};

Model.prototype.getHTMLValue = function () {
    var modelTextValue = this.getTextValue();
    if (modelTextValue) {
        return (0, _componentUtils.createInlineStyledLabel)(modelTextValue, this.asfProperty.style)[0].outerHTML;
    } else {
        return "";
    }
};

/**
 * изменение значения
 * @param newValue новое значение
 * @param fireInternalEvent обновить отображение
 * @param fireEvent генерировать событие
 */
Model.prototype.doSetValue = function (newValue) {
    if (this.value == newValue) {
        // ничего не делаем если новое значение такое же как и старое
        // даже событие отправлять не будем,
        return;
    }
    this.value = newValue;
    this.fireChangeEvents();
};

/**
 * генерация события об изменении
 */
Model.prototype.fireChangeEvents = function () {
    this.model.trigger(_constants.EVENT_TYPE.valueChange, [this.model, this.value]);
    this.playerModel.trigger(_constants.EVENT_TYPE.valueChange, [this.model, this.value]);
};

/**
 * вставляем значение из отображения
 * @param inputValue
 */
Model.prototype.setValueFromInput = function (inputValue) {
    this.doSetValue(inputValue);
};

/**
 * получение значения модели (в формате модели)
 * @returns {*}
 */
Model.prototype.getValue = function () {
    return this.value;
};

/**
 * получение текстового значения
 * @returns {*}
 */
Model.prototype.getTextValue = function () {
    return this.value;
};

/**
 * получение asfData
 * @param blockNumber
 * @returns {{id: string, type: *, value: *, key: *}}
 */
Model.prototype.getAsfData = function (blockNumber) {

    return (0, _dataUtils.getBaseAsfData)(this.asfProperty, blockNumber, this.value);
};

/**
 * вставка asfData
 * @param asfData
 */
Model.prototype.setAsfData = function (asfData) {
    if (!asfData) {
        return;
    }
    this.setValue(asfData.value);
};

Model.prototype.getProperty = function () {
    var asfProperty = jQuery.extend(true, {}, this.asfProperty);
    delete asfProperty.data;
    delete asfProperty.dataSource;
    delete asfProperty.default;
    delete asfProperty.elements;
    delete asfProperty.ownerTableId;
    delete asfProperty.tableBlockIndex;
    if (asfProperty.style) {
        delete asfProperty.style.maxWidth;
    }
    if (!this.isEmpty()) {
        asfProperty.data = this.getAsfData();
    }
    if (this.asfProperty.dataSource) {
        asfProperty.dataSource = jQuery.extend(true, {}, this.asfProperty.dataSource);
    }
    if (this.asfProperty.elements) {
        asfProperty.elements = this.asfProperty.elements.slice();
    }

    return asfProperty;
};

Model.prototype.destroy = function () {
    this.model.off();
    this.model.trigger(_constants.EVENT_TYPE.modelDestroyed);
};

Model.prototype.isTable = function () {
    return this.asfProperty.type == _constants.WIDGET_TYPE.page || this.asfProperty.type == _constants.WIDGET_TYPE.table;
};
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ }),

/***/ 250:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = DepartmentLinkView;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _services = __webpack_require__(23);

var services = _interopRequireWildcard(_services);

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

var _constants = __webpack_require__(2);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _view = __webpack_require__(11);

var _view2 = _interopRequireDefault(_view);

var _UserChooser = __webpack_require__(81);

var _TagArea = __webpack_require__(72);

var _TagArea2 = _interopRequireDefault(_TagArea);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function DepartmentLinkView(model, playerView) {
    _view2.default.call(this, this, model, playerView);

    var config = model.asfProperty.config;

    var instance = this;

    var lastSearch = "";

    var locale = model.getLocale();

    var showSuggestions = function showSuggestions(search, suggestionInput) {
        if (search === lastSearch) {
            return;
        }
        lastSearch = search;
        api.getDepartmentsSuggestions(search, model.getFilterUserId(), model.getFilterPositionId(), model.getFilterDepartmentId(), null, 0, 30, locale, false, function (suggestions) {
            var values = [];
            suggestions.forEach(function (s, index) {
                values.push({ value: s.departmentId, title: s.departmentName, selected: index == 0 });
            });
            services.showSuggestOracle(values, suggestionInput, 200, function (selectedValue) {
                suggestions.forEach(function (s) {
                    if (s.departmentId == selectedValue) {
                        s.tagName = s.departmentName;
                        instance.addValue(s);
                    }
                });
                suggestionInput.val("");
            });
        });
    };

    var departmentsTagArea = new _TagArea2.default({ width: "calc(100% - 30px)" }, 0, config['multi'], config['editable-label'], config['custom'], function (search, suggestionInput) {
        if (search.isEmpty()) {
            services.showDropDown([]);
        } else {
            showSuggestions(search, suggestionInput);
        }
    }, function (randomText) {
        var object = { departmentId: _UserChooser.USER_CHOOSER_SUFFIXES.text + "-" + model.getNextTextNumber(),
            departmentName: randomText,
            tagName: randomText };

        instance.addValue(object);
    });

    this.addValue = function (item) {
        var value = model.getValue().slice();
        value.push(item);
        model.setValueFromInput(value);
        instance.updateValueFromModel();
    };

    departmentsTagArea.on(_constants.EVENT_TYPE.tagDelete, function () {
        model.setValueFromInput(departmentsTagArea.getValues());
    });

    departmentsTagArea.on(_constants.EVENT_TYPE.tagEdit, function () {
        model.setValueFromInput(departmentsTagArea.getValues());
    });
    var chooserButton = (0, _jquery2.default)("<button/>", { class: "asf-browseButton" });

    dataUtils.addAsFormId(departmentsTagArea.getWidget(), "input", model.asfProperty);
    dataUtils.addAsFormId(chooserButton, "button", model.asfProperty);

    chooserButton.click(function () {
        services.showDepartmentChooserDialog(model.getValue(), config['multi'], model.getFilterUserId(), model.getFilterPositionId(), model.getFilterDepartmentId(), null, locale, function (selectedValues) {

            selectedValues.forEach(function (item) {
                item.tagName = item.departmentName;
            });

            model.setValue(selectedValues);
            instance.updateValueFromModel();
        });
    });

    this.updateValueFromModel = function () {
        departmentsTagArea.setValues(model.getValue());
    };

    this.unmarkInvalid = function () {
        departmentsTagArea.markValidity(true);
    };

    this.markInvalid = function () {
        departmentsTagArea.markValidity(false);
    };

    this.setEnabled = function (newEnabled) {
        departmentsTagArea.setEditable(newEnabled);
        if (newEnabled) {
            chooserButton.css("pointer-events", "auto");
        } else {
            chooserButton.css("pointer-events", "none");
        }
    };
    this.setEnabled(!dataUtils.isReadOnly(model));

    model.on(_constants.EVENT_TYPE.dataLoad, function () {
        instance.updateValueFromModel();
    });

    this.updateValueFromModel();

    this.container.append(departmentsTagArea.getWidget());
    this.container.append(chooserButton);
};

DepartmentLinkView.prototype = Object.create(_view2.default.prototype);
DepartmentLinkView.prototype.constructor = DepartmentLinkView;

/***/ }),

/***/ 251:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = DocAttributeView;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _styleUtils = __webpack_require__(21);

var styleUtils = _interopRequireWildcard(_styleUtils);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _view = __webpack_require__(11);

var _view2 = _interopRequireDefault(_view);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function DocAttributeView(model, playerView) {
    _view2.default.call(this, this, model, playerView);

    var label = (0, _jquery2.default)('<div></div>');

    dataUtils.addAsFormId(label, "label", model.asfProperty);

    label.addClass('asf-label');

    styleUtils.applyLabelStyle(label, model.asfProperty.style, true);

    this.container.append(label);

    label.html(model.getAttributeTitle());
};

DocAttributeView.prototype = Object.create(_view2.default.prototype);
DocAttributeView.prototype.constructor = DocAttributeView;

/***/ }),

/***/ 252:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = DocLinkTextView;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _services = __webpack_require__(23);

var services = _interopRequireWildcard(_services);

var _constants = __webpack_require__(2);

var _styleUtils = __webpack_require__(21);

var styleUtils = _interopRequireWildcard(_styleUtils);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _view = __webpack_require__(11);

var _view2 = _interopRequireDefault(_view);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function DocLinkTextView(model, playerView) {
    _view2.default.call(this, this, model, playerView);

    var instance = this;

    var link = (0, _jquery2.default)('<div/>', { class: 'asf-link asf-label' });

    dataUtils.addAsFormId(link, "text", model.asfProperty);

    styleUtils.applyLabelStyle(link, model.asfProperty.style, true);
    link.click(function () {
        services.showDocument(model.value);
    });

    this.updateValueFromModel = function () {
        link.html(model.getTextValue());
        if (model.value) {
            link.show();
        } else {
            link.hide();
        }
    };

    model.on(_constants.EVENT_TYPE.dataLoad, function () {
        instance.updateValueFromModel();
    });

    this.container.append(link);

    instance.updateValueFromModel();
};

DocLinkTextView.prototype = Object.create(_view2.default.prototype);
DocLinkTextView.prototype.constructor = DocLinkTextView;

/***/ }),

/***/ 253:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = DocLinkView;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _i18n = __webpack_require__(14);

var _i18n2 = _interopRequireDefault(_i18n);

var _services = __webpack_require__(23);

var services = _interopRequireWildcard(_services);

var _constants = __webpack_require__(2);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _view = __webpack_require__(11);

var _view2 = _interopRequireDefault(_view);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function DocLinkView(model, playerView) {
    _view2.default.call(this, this, model, playerView);

    var instance = this;

    var enabled = true;

    var dropdownBox = (0, _jquery2.default)('<div/>', { class: 'asf-dropdown-input' });

    var button = (0, _jquery2.default)("<button/>", { class: "asf-dropdown-button" });

    dataUtils.addAsFormId(dropdownBox, "input", model.asfProperty);
    dataUtils.addAsFormId(button, "button", model.asfProperty);

    this.container.append(dropdownBox);
    this.container.append(button);

    var showDropDown = function showDropDown() {
        if (!enabled) {
            return;
        }
        var selectedValue = model.getValue();
        if (!selectedValue) {
            selectedValue = '';
        }
        var values = [];
        model.documentBases.forEach(function (base) {
            values.push({ value: base.id, title: base.title, selected: selectedValue == base.id });
        });

        services.showDropDown(values, instance.container, null, function (selectedValue) {
            model.setValueFromInput(selectedValue);
        });
    };

    button.click(function () {
        showDropDown();
    });

    dropdownBox.click(function () {
        showDropDown();
    });

    this.unmarkInvalid = function () {
        dropdownBox.removeClass('asf-invalidInput');
    };

    this.markInvalid = function () {
        dropdownBox.addClass('asf-invalidInput');
    };

    this.updateValueFromModel = function () {
        if (model.documentBases.length == 0) {
            dropdownBox.html(_i18n2.default.tr("Данные загружаются"));
            return;
        }
        dropdownBox.html(model.subject);
    };

    this.setEnabled = function (newEnabled) {
        enabled = newEnabled;
        if (enabled) {
            dropdownBox.removeClass('asf-disabledInput');
        } else {
            dropdownBox.addClass('asf-disabledInput');
        }
    };

    this.setEnabled(!dataUtils.isReadOnly(model));

    model.on(_constants.EVENT_TYPE.dataLoad, function () {
        instance.updateValueFromModel();
    });

    instance.updateValueFromModel();
};

DocLinkView.prototype = Object.create(_view2.default.prototype);
DocLinkView.prototype.constructor = DocLinkView;

/***/ }),

/***/ 254:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = FileLinkView;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _underscore = __webpack_require__(7);

var _underscore2 = _interopRequireDefault(_underscore);

var _i18n = __webpack_require__(14);

var _i18n2 = _interopRequireDefault(_i18n);

var _services = __webpack_require__(23);

var services = _interopRequireWildcard(_services);

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

var _constants = __webpack_require__(2);

var _styleUtils = __webpack_require__(21);

var styleUtils = _interopRequireWildcard(_styleUtils);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _view = __webpack_require__(11);

var _view2 = _interopRequireDefault(_view);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function FileLinkView(model, playerView, editable) {
    _view2.default.call(this, this, model, playerView);

    var config = model.asfProperty.config;

    _view2.default.call(this, this, model, playerView);
    var initialWidth = 120;

    var instance = this;

    var enabled = true;

    var button = (0, _jquery2.default)('<button>', { class: "asf-file-choose-button" }).html(_i18n2.default.tr("Выбрать файл"));
    var notChosenLabel = (0, _jquery2.default)('<div>', { class: "asf-label" });
    notChosenLabel.html(_i18n2.default.tr("Файл не выбран"));

    var icon = (0, _jquery2.default)('<img>', { class: "asf-InlineBlock asf-file-link-icon" });
    var fileNameLabel = (0, _jquery2.default)('<div>', { class: "asf-label asf-file-link-name" });
    var filePathLabel = (0, _jquery2.default)('<div>', { class: "asf-file-link-path" });
    var editButton = (0, _jquery2.default)('<div>', { class: "asf-InlineBlock asf-file-link-edit-button" });
    var deleteButton = (0, _jquery2.default)('<div>', { class: "asf-InlineBlock asf-file-link-delete-button" });

    var namePathContainer = (0, _jquery2.default)('<div>', { class: "asf-InlineBlock asf-file-link-namePathContainer" });
    var readContainer = (0, _jquery2.default)('<div>', { class: "asf-InlineBlock asf-file-link-readContainer" });

    namePathContainer.append(fileNameLabel);
    namePathContainer.append(filePathLabel);

    readContainer.append(icon);
    readContainer.append(namePathContainer);
    readContainer.append(editButton);
    readContainer.append(deleteButton);

    dataUtils.addAsFormId(button, "choose.button", model.asfProperty);

    styleUtils.applyLabelStyle(notChosenLabel, model.asfProperty.style, true);
    styleUtils.applyLabelStyle(fileNameLabel, model.asfProperty.style, true);
    styleUtils.applyLabelStyle(filePathLabel, model.asfProperty.style, true);

    this.updateValueFromModel = function () {

        button.detach();
        notChosenLabel.detach();
        readContainer.detach();

        instance.initialWidth = "100%";

        if (model.value) {

            var path = model.value.path || "";
            if (path.startsWith("/company_root")) {
                path = _i18n2.default.tr("Хранилище") + path.substring(13);
            } else if (path.startsWith("/aiservice")) {
                path = null;
            }

            icon.prop("src", api.getFullUrl(model.value.icon));
            fileNameLabel.html(model.value.name);
            filePathLabel.html(path);
            this.container.append(readContainer);
        } else {
            instance.initialWidth = 120;
            if (editable) {
                this.container.append(button);
            } else {
                this.container.append(notChosenLabel);
            }
        }
        styleUtils.applyPositionStyle(instance.container, model.asfProperty.style, instance.initialWidth);
    };

    this.markInvalid = function () {
        button.addClass("asf-invalidInput");
    };
    this.unmarkInvalid = function () {
        button.removeClass("asf-invalidInput");
    };

    this.setEnabled = function (newEnabled) {
        enabled = newEnabled;
        if (enabled) {
            button.prop('disabled', false);
            deleteButton.css("display", "inline-block");
            editButton.css("display", "inline-block");
            namePathContainer.css("max-width", "calc(100% - 60px)");
        } else {
            button.prop('disabled', true);
            deleteButton.hide();
            editButton.hide();
            namePathContainer.css("max-width", "calc(100% - 22px)");
        }
    };
    this.setEnabled(!dataUtils.isReadOnly(model) && editable);

    button.click(function () {
        services.showFileLinkDialog(false, false, [], function (collection) {
            if (_underscore2.default.isEmpty(collection)) return;
            var file = _underscore2.default.first(collection);

            model.setValueFromInput(file, true);
        });
    });

    deleteButton.click(function () {
        model.setValueFromInput(null);
    });

    editButton.click(function () {
        services.showFileLinkDialog(false, false, [], function (collection) {
            if (_underscore2.default.isEmpty(collection)) {
                return;
            }
            var file = _underscore2.default.first(collection);
            model.setValueFromInput(file, true);
        });
    });

    fileNameLabel.click(function () {
        if (model.value.accessDenied || model.value.deleted) {
            services.showErrorMessage(_i18n2.default.tr("Элемент был удален либо у вас нет прав на просмотр"));
            return;
        }
        services.openFile(model.value.identifier, model.newWindow);
    });

    filePathLabel.click(function () {
        if (model.value.accessDenied || model.value.deleted) {
            services.showErrorMessage(_i18n2.default.tr("Элемент был удален либо у вас нет прав на просмотр"));
            return;
        }
        services.openFilePath(model.value.identifier, model.newWindow);
    });

    model.on(_constants.EVENT_TYPE.dataLoad, function () {
        instance.updateValueFromModel();
    });

    this.updateValueFromModel();
};

FileLinkView.prototype = Object.create(_view2.default.prototype);
FileLinkView.prototype.constructor = FileLinkView;

/***/ }),

/***/ 255:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = FileView;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _underscore = __webpack_require__(7);

var _underscore2 = _interopRequireDefault(_underscore);

var _i18n = __webpack_require__(14);

var _i18n2 = _interopRequireDefault(_i18n);

var _services = __webpack_require__(23);

var services = _interopRequireWildcard(_services);

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

var _styleUtils = __webpack_require__(21);

var styleUtils = _interopRequireWildcard(_styleUtils);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _view = __webpack_require__(11);

var _view2 = _interopRequireDefault(_view);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function FileView(model, playerView, editable) {

    _view2.default.call(this, this, model, playerView);
    var initialWidth = 120;

    var instance = this;

    var name = _i18n2.default.tr("Выбрать файл");
    var button = (0, _jquery2.default)('<button>', { class: "asf-file-choose-button" }).html(name);
    var notChosenLabel = (0, _jquery2.default)('<div>', { class: "asf-label asf-InlineBlock" });
    notChosenLabel.html();

    var fileNameLabel = (0, _jquery2.default)('<span>', { class: "asf-label asf-file-name" });
    var filePathLabel = (0, _jquery2.default)('<span>', { class: "asf-label asf-file-path" });
    var deleteButton = (0, _jquery2.default)('<div>', { class: "asf-file-delete-button" });

    var image = (0, _jquery2.default)('<img>');

    var readContainer = (0, _jquery2.default)('<div>', { class: "asf-InlineBlock" });

    this.container.css("white-space", "nowrap");

    dataUtils.addAsFormId(button, "choose.button", model.asfProperty);
    dataUtils.addAsFormId(notChosenLabel, "notchosen", model.asfProperty);
    dataUtils.addAsFormId(fileNameLabel, "filename", model.asfProperty);
    dataUtils.addAsFormId(deleteButton, "delete.button", model.asfProperty);
    dataUtils.addAsFormId(image, "image", model.asfProperty);
    dataUtils.addAsFormId(filePathLabel, "path", model.asfProperty);

    styleUtils.applyLabelStyle(notChosenLabel, model.asfProperty.style);
    styleUtils.applyLabelStyle(fileNameLabel, model.asfProperty.style);
    styleUtils.applyLabelStyle(filePathLabel, model.asfProperty.style);

    styleUtils.applyImageSize(image, model.asfProperty.style);

    var enabled = true;

    var mimeTypes = [];
    if (model.showContent) {
        mimeTypes.push("image/png");
        mimeTypes.push("image/jpg");
        mimeTypes.push("image/jpeg");
        mimeTypes.push("image/gif");
    }

    if (model.showContent) {
        readContainer.append(image);
        readContainer.addClass("asf-file-imageContainer");
    } else {
        readContainer.append(fileNameLabel);
        readContainer.append(filePathLabel);
        readContainer.addClass("asf-file-namePathContainer");
    }

    this.updateValueFromModel = function () {
        button.detach();
        notChosenLabel.detach();
        readContainer.detach();
        deleteButton.detach();

        if (model.asfProperty.style && model.asfProperty.style.width) {
            initialWidth = model.asfProperty.style.width;
        } else {
            initialWidth = "100%";
        }

        if (model.value) {

            if (model.showContent) {

                var name = model.value.name.toLowerCase();
                var mime = "image/jpeg";
                if (name.indexOf(".png") > -1) {
                    mime = "image/png";
                }
                if (name.indexOf(".gif") > -1) {
                    mime = "image/gif";
                }
                api.getImage('rest/api/storage/file/get?inline=true&identifier=' + instance.model.value.identifier, function (b64encoded) {
                    image.prop("src", "data:" + mime + ";base64," + b64encoded);
                });
            } else {
                fileNameLabel.html(instance.model.value.name);
                if (model.showFullPath && model.value.fromStorage) {
                    filePathLabel.html("(" + instance.model.value.path + ")");
                } else {
                    filePathLabel.html("");
                }
            }
            this.container.append(readContainer);
            if (editable) {
                this.container.append(deleteButton);
            }
        } else {
            if (editable) {
                this.container.append(button);
                initialWidth = 120;
            } else {
                this.container.append(notChosenLabel);
                initialWidth = 120;
            }
        }

        styleUtils.applyPositionStyle(instance.container, model.asfProperty.style, initialWidth);
    };

    this.markInvalid = function () {
        button.addClass("asf-invalidInput");
    };
    this.unmarkInvalid = function () {
        button.removeClass("asf-invalidInput");
    };

    this.createNew = function () {
        services.showCreateFileDialog(model.playerModel.nodeId, model.playerModel.asfDataId, function (id) {
            model.setValueFromInput(id);
        });
    };

    this.fromComp = function () {
        services.showUploadFileDialog(model.playerModel.nodeId, model.playerModel.asfDataId, mimeTypes, function (id) {
            model.setValueFromInput(id);
        });
    };

    this.fromStore = function () {

        services.showFileLinkDialog(false, true, mimeTypes, function (collection) {
            if (_underscore2.default.isEmpty(collection)) {
                return;
            }
            var file = _underscore2.default.first(collection);

            var data = {
                objectId: file.identifier,
                targetId: model.playerModel.nodeId,
                dataId: model.playerModel.asfDataId
            };

            services.showWaitWindow();

            api.copyFileToForm(data, function (data) {

                services.hideWaitWindow();

                var path = file.path;
                if (path.startsWith("/company_root")) {
                    path = _i18n2.default.tr("Хранилище") + path.substring(13, path.lastIndexOf("/"));
                } else if (path.startsWith("/aiservice")) {
                    path = null;
                }
                model.setValueFromInput(data.identifier, path);
            });
        });
    };

    this.setEnabled = function (newEnabled) {
        enabled = newEnabled;
        if (enabled) {
            button.prop('disabled', false);
            deleteButton.css("display", "inline-block");
        } else {
            button.prop('disabled', true);
            deleteButton.hide();
        }
    };
    this.setEnabled(!dataUtils.isReadOnly(model));

    button.click(function () {
        if (model.playerModel.building) {
            return;
        }
        services.showFileMenu(button, instance.createNew, instance.fromComp, instance.fromStore, model.showContent);
    });

    deleteButton.click(function () {
        if (!confirm(_i18n2.default.tr("Вы хотите удалить файл?"))) {
            return;
        }
        model.setValueFromInput(null);
    });

    fileNameLabel.click(function () {
        services.openFile(model.value.identifier, false);
    });

    this.updateValueFromModel();
};
FileView.prototype = Object.create(_view2.default.prototype);
FileView.prototype.constructor = FileView;

/***/ }),

/***/ 256:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = ImageView;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

var _constants = __webpack_require__(2);

var _styleUtils = __webpack_require__(21);

var styleUtils = _interopRequireWildcard(_styleUtils);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _view = __webpack_require__(11);

var _view2 = _interopRequireDefault(_view);

var _emptyImage = __webpack_require__(379);

var _emptyImage2 = _interopRequireDefault(_emptyImage);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ImageView(model, playerView) {

    _view2.default.call(this, this, model, playerView);

    var image = (0, _jquery2.default)('<img/>', { border: 0 });
    this.image = image;

    image.on("load", function () {
        model.playerModel.trigger(_constants.EVENT_TYPE.cellSizeChange, model);
    });

    var url = _emptyImage2.default;
    if (model.asfProperty.config && model.asfProperty.config.url) {
        url = api.getFullUrl(model.asfProperty.config.url);
    }

    image.prop("src", url);

    dataUtils.addAsFormId(image, "image", model.asfProperty);

    styleUtils.applyImageSize(image, model.asfProperty.style);

    this.container.append(image);
};

ImageView.prototype = Object.create(_view2.default.prototype);
ImageView.prototype.constructor = ImageView;
ImageView.prototype.applySpecialStyle = function () {
    if (this.image) {
        styleUtils.applyImageSize(this.image, this.model.asfProperty.style);
    }
};

/***/ }),

/***/ 257:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = LabelView;

var _styleUtils = __webpack_require__(21);

var styleUtils = _interopRequireWildcard(_styleUtils);

var _componentUtils = __webpack_require__(15);

var compUtils = _interopRequireWildcard(_componentUtils);

var _view = __webpack_require__(11);

var _view2 = _interopRequireDefault(_view);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function LabelView(model, playerView) {
    _view2.default.call(this, this, model, playerView);
    if (!compUtils.getCurrentTranslation(model.asfProperty)) {
        this.label.text(i18n.tr("Нажмите, чтобы ввести текст"));
    }
};

LabelView.prototype = Object.create(_view2.default.prototype);
LabelView.prototype.constructor = LabelView;

LabelView.prototype.applySpecialStyle = function () {
    styleUtils.applyLabelStyle(this.label, this.model.asfProperty.style);
};

/***/ }),

/***/ 258:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = LinkView;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _i18n = __webpack_require__(14);

var _i18n2 = _interopRequireDefault(_i18n);

var _services = __webpack_require__(23);

var services = _interopRequireWildcard(_services);

var _styleUtils = __webpack_require__(21);

var styleUtils = _interopRequireWildcard(_styleUtils);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _view = __webpack_require__(11);

var _view2 = _interopRequireDefault(_view);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Created by exile
 * Date: 03.03.16
 * Time: 16:25
 */
function LinkView(model, playerView, editable) {

    _view2.default.call(this, this, model, playerView);

    var enabled = true;

    var label = (0, _jquery2.default)('<div></div>', { class: "asf-link" });
    label.html(_i18n2.default.tr("Ссылка: "));
    dataUtils.addAsFormId(label, "label", model.asfProperty);

    this.container.append(label);

    styleUtils.applyLabelStyle(label, model.asfProperty.style, true);

    var instance = this;

    this.updateValueFromModel = function () {
        if (editable) {
            label.html(_i18n2.default.tr("Ссылка: ") + model.getTextValue());
        } else {
            label.html(model.getTextValue());
        }
    };

    this.linkClicked = function () {
        if (!editable || !enabled) {
            if (model.getValue()) {
                services.openLink(model.getValue(), model.isOpenInNew());
            }
        } else {
            services.showEditLinkDialog(model.getValue(), model.getTextValue(), model.isOpenInNew(), function (newUrl, newTitle, newOneInNew) {
                model.setValueFromInput(newUrl, newTitle, newOneInNew);
            });
        }
    };

    label.click(function () {
        instance.linkClicked();
    });

    this.setEnabled = function (newEnabled) {
        enabled = newEnabled;
    };

    this.setEnabled(!dataUtils.isReadOnly(model));

    this.updateValueFromModel();
};

LinkView.prototype = Object.create(_view2.default.prototype);
LinkView.prototype.constructor = LinkView;

/***/ }),

/***/ 259:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = NumericInputView;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _i18n = __webpack_require__(14);

var _i18n2 = _interopRequireDefault(_i18n);

var _componentUtils = __webpack_require__(15);

var compUtils = _interopRequireWildcard(_componentUtils);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _view = __webpack_require__(11);

var _view2 = _interopRequireDefault(_view);

var _NumericInputModel = __webpack_require__(157);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Created by exile
 * Date: 03.02.16
 * Time: 16:25
 */
function NumericInputView(model, playerView) {
    _view2.default.call(this, this, model, playerView);

    var textBox = (0, _jquery2.default)('<input/>', { type: 'text', class: 'asf-textBox', placeholder: _i18n2.default.tr("Введите число") });

    compUtils.initInput(model, this, textBox);
    dataUtils.addAsFormId(textBox, "input", model.asfProperty);

    this.unmarkInvalid = function () {
        textBox.removeClass('asf-invalidInput');
    };

    this.markInvalid = function () {
        textBox.addClass('asf-invalidInput');
    };

    this.getInputValue = function () {
        return textBox.getInputValue();
    };

    this.getValueForModel = function () {
        return _NumericInputModel.NumericUtils.parseFromInput(textBox.val(), model.asfProperty);
    };

    _NumericInputModel.NumericUtils.initNumericInput(model, this, textBox);

    this.setEnabled = function (editable) {
        if (editable) {
            textBox.removeProp("readonly");
            textBox.removeClass("asf-disabledInput");
        } else {
            textBox.prop("readonly", true);
            textBox.addClass("asf-disabledInput");
        }
    };
    this.setEnabled(!model.asfProperty.config['read-only']);

    this.container.append(textBox);
};

NumericInputView.prototype = Object.create(_view2.default.prototype);
NumericInputView.prototype.constructor = NumericInputView;

/***/ }),

/***/ 260:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function($) {

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = PlayerView;

var _view = __webpack_require__(11);

var _view2 = _interopRequireDefault(_view);

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _underscore = __webpack_require__(7);

var _underscore2 = _interopRequireDefault(_underscore);

var _History = __webpack_require__(229);

var _History2 = _interopRequireDefault(_History);

var _domUtils = __webpack_require__(44);

var _utils = __webpack_require__(82);

var _componentUtils = __webpack_require__(15);

var _player = __webpack_require__(243);

var _styleUtils = __webpack_require__(21);

var _tableStaticView = __webpack_require__(129);

var _tableStaticView2 = _interopRequireDefault(_tableStaticView);

var _tableBuilderView = __webpack_require__(160);

var _tableBuilderView2 = _interopRequireDefault(_tableBuilderView);

var _BuilderToolBar = __webpack_require__(214);

var _BuilderToolBar2 = _interopRequireDefault(_BuilderToolBar);

var _selectionInfo = __webpack_require__(275);

var _selectionInfo2 = _interopRequireDefault(_selectionInfo);

var _layoutUtils = __webpack_require__(65);

var _constants = __webpack_require__(2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function PlayerView(playerModel) {
    _view2.default.call(this, this, playerModel);

    var instance = this;

    // для ctrl-z например
    instance.container.attr('tabindex', 30);
    instance.container.focus();

    this.editable = false;
    this.views = [];

    this.topMargin = 16;
    this.leftMargin = 16;

    instance.toolBar = new _BuilderToolBar2.default(this);

    var $splitButton = (0, _jquery2.default)('<div>', { class: 'asf-split-button', title: i18n.tr('Разъеденить') });
    var $mergeButton = (0, _jquery2.default)('<div>', { class: 'asf-merge-button', title: i18n.tr('Объединить') });
    var $undoButton = (0, _jquery2.default)('<div>', { class: 'asf-undo-button disable', title: i18n.tr('Отменить действие') });
    var $redoButton = (0, _jquery2.default)('<div>', { class: 'asf-redo-button disable', title: i18n.tr('Вернуть действие') });

    var history = new _History2.default(this);
    playerModel.history = history;
    instance.history = history;
    $undoButton.click(history.undo);
    $redoButton.click(history.redo);

    var mergeErrors = {
        tooManyOrNone: 'выделено больше одной таблицы или не выделено ничего',
        notRectangle: 'неправильная область для объединения',
        hasIntersects: "есть компоненты, которые пересекают область выделения",
        singleCellMerge: 'объединять одну ячейку нельзя'
    };

    var splitErrors = {
        noMergedCellsSelected: 'не выделено объединенных ячеек',
        tooManyOrNone: 'выделено больше одной таблицы или не выделено ничего'
    };

    function canSplit() {
        var selection = instance.getFullSelection();
        selection = _underscore2.default.filter(selection, function (sel) {
            return sel.cells.length > 0;
        });
        if (selection.length == 0 || selection.length > 1) {
            return {
                error: splitErrors.tooManyOrNone
            };
        }

        var tSelection = selection[0];

        var cells = tSelection.cells;

        var layout = tSelection.view.model.asfProperty.layout;

        var mergedComps = [];
        cells.forEach(function (cell) {
            var comp = (0, _layoutUtils.cellToComp)(cell);
            var realComponent = (0, _layoutUtils.find)(comp, layout);
            if ((0, _layoutUtils.isMerged)(realComponent)) {
                mergedComps.push(realComponent);
            }
        });

        if (mergedComps.length === 0) {
            return {
                error: splitErrors.noMergedCellsSelected
            };
        }

        return {
            tSelection: tSelection,
            mergedComps: mergedComps
        };
    }

    $splitButton.click(function () {
        var _canSplit = canSplit(),
            error = _canSplit.error,
            tSelection = _canSplit.tSelection,
            mergedComps = _canSplit.mergedComps;

        if (error) {
            console.error(error);
            return;
        }

        var tModel = tSelection.view.model;

        var oldLayout = (0, _utils.clone)(tModel.asfProperty.layout);

        _underscore2.default.each(mergedComps, function (comp) {
            comp.rowspan = 1;
            comp.colspan = 1;
        });

        history.addItem(history.layoutItem(oldLayout, (0, _utils.clone)(tModel.asfProperty.layout), tModel.asfProperty.id));

        tModel.trigger(_constants.EVENT_TYPE.tableDomUpdate);
    });

    function canMerge() {
        var selection = instance.getFullSelection();
        selection = _underscore2.default.filter(selection, function (sel) {
            return sel.cells.length > 0;
        });
        if (selection.length == 0 || selection.length > 1) {
            return {
                error: mergeErrors.tooManyOrNone
            };
        }

        var tSelection = selection[0];

        if (tSelection.selection.cells.length === 1) {
            return {
                error: mergeErrors.singleCellMerge
            };
        }

        var mergeArea = tSelection.selection.constructMerge();
        if (mergeArea === null) {
            return {
                error: mergeErrors.notRectangle
            };
        }

        var layout = tSelection.view.model.asfProperty.layout;
        var intersect = (0, _layoutUtils.getIntersect)(mergeArea, layout);
        if (intersect.length > 0) {
            return {
                error: mergeErrors.hasIntersects
            };
        }

        return {
            layout: layout,
            mergeArea: mergeArea,
            tSelection: tSelection
        };
    }

    $mergeButton.click(function () {
        var mergeValidation = canMerge();
        if (mergeValidation.error) {
            console.error(mergeValidation.error);
            return;
        }

        var layout = mergeValidation.layout;
        var tSelection = mergeValidation.tSelection;
        var mergeArea = mergeValidation.mergeArea;

        (0, _layoutUtils.sortLayout)(layout);
        var inside = (0, _layoutUtils.getInside)(mergeArea, layout);

        var tView = tSelection.view;
        var hItems = [];
        var oldLayout = (0, _utils.clone)(layout);

        var insideWithComps = _underscore2.default.filter(inside, _underscore2.default.property('id'));
        if (insideWithComps.length > 0) {
            if (insideWithComps.length > 1) {
                if (!confirm(i18n.tr('"При объединении сохранится только компонент в верхней левой непустой ячейке. Продолжить?" '))) {
                    return;
                }
            }

            var compsViews = _underscore2.default.chain(insideWithComps).map(function (comp) {
                var view = tView.getViewWithId(comp.id);
                if (!view) return;
                return {
                    comp: comp,
                    id: comp.id,
                    view: view,
                    model: view.model,
                    property: view.model.property
                };
            }).filter(Boolean).value();

            var leftestProperty = (0, _utils.clone)(compsViews[0].view.model.asfProperty);

            _underscore2.default.chain(compsViews).each(function (compView) {
                var hItem = history.createDeleteItem(compView.model.getProperty(), (0, _layoutUtils.compToCell)(compView.comp), tView.model.asfProperty.id);
                hItems.push(hItem);
            });

            _underscore2.default.chain(compsViews).map(_underscore2.default.property('model')).each(tView.model.deleteModel);
            tView.model.paste(leftestProperty, mergeArea.row, mergeArea.column);
            hItems.push(history.createInsertItem((0, _utils.clone)(leftestProperty), (0, _layoutUtils.compToCell)(mergeArea), tView.model.asfProperty.id));
        }

        layout.components = _underscore2.default.reject(layout.components, (0, _layoutUtils.isInsidePointNoEqual)(mergeArea));

        var topLeftComp = (0, _layoutUtils.find)(mergeArea, layout);
        if (_underscore2.default.isUndefined(topLeftComp)) {
            topLeftComp = {};
            layout.components.push(topLeftComp);
        }
        _underscore2.default.extend(topLeftComp, mergeArea);
        (0, _layoutUtils.sortLayout)(layout);

        hItems.push(history.layoutItem(oldLayout, (0, _utils.clone)(layout), tView.model.asfProperty.id));
        history.addItems(hItems);
        tView.model.trigger(_constants.EVENT_TYPE.tableDomUpdate);
        tView.model.selection.selectOne([mergeArea.row, mergeArea.column]);
    });

    this.container.bind('copy', function (evt) {
        if ((0, _domUtils.isEventForInput)(evt)) {
            return;
        }

        var selection = instance.getSelection();
        if (selection === null) {
            alert(i18n.tr('Должна быть выделена ровно одна ячейка формы'));
            return;
        }

        instance.clipboard = selection.view.model.getProperty();
        (0, _domUtils.cancelEvent)(evt);
    });

    this.container.keydown(function (e) {
        if (e.which === 90 && e.ctrlKey) {
            if (e.shiftKey) instance.history.redo();else instance.history.undo();
        }
        if (e.which === 89 && e.ctrlKey) {
            instance.history.redo();
        }
    });

    this.pasteToSelection = function (asfProperty) {
        var selection = instance.getSelection();
        if (selection === null) {
            alert(i18n.tr('Должна быть выделена ровно одна ячейка формы'));
            return;
        }

        if (asfProperty.type == _constants.WIDGET_TYPE.table) {
            if (!selection.tableView.model.isPage()) {
                return;
            }
        }

        var historyItems = [];

        var tableView = selection.tableView;
        var tableModel = tableView.model;
        if (selection.view) {
            var answer = confirm(i18n.tr('В ячейке уже имеется компонент. Заменить?'));
            if (!answer) return;
            historyItems.push(history.createDeleteItem((0, _utils.clone)(selection.view.model.getProperty()), [selection.row, selection.column], tableModel.asfProperty.id));
            tableModel.deleteModel(selection.view.model);
        }

        var pastedProperty = tableModel.paste(asfProperty, selection.row, selection.column);

        historyItems.push(history.createInsertItem(pastedProperty, [selection.row, selection.column], tableModel.asfProperty.id));

        history.addItems(historyItems);

        instance.container.focus();
    };

    this.container.bind('paste', function (evt) {

        if ((0, _domUtils.isEventForInput)(evt)) {
            // надо игнорировать события вставки в текстовые поля
            return;
        }

        if (!instance.clipboard) {
            console.error('clipboard is empty');
            return;
        }

        instance.pasteToSelection(instance.clipboard);

        (0, _domUtils.cancelEvent)(evt);
    });

    this.container.bind('cut', function (evt) {

        if ((0, _domUtils.isEventForInput)(evt)) {
            return;
        }

        var selection = instance.getSelection();
        if (selection === null) {
            alert(i18n.tr('Должна быть выделена ровно одна ячейка формы'));
            return;
        }

        if (!selection.view) return;
        selection.tableView.model.deleteModel(selection.view.model);
        instance.clipboard = (0, _utils.clone)(selection.view.model.getProperty());

        history.addItem(history.createDeleteItem(selection.view.model.getProperty(), [selection.row, selection.column], selection.tableView.model.asfProperty.id));

        (0, _domUtils.cancelEvent)(evt);
    });

    history.on(_constants.EVENT_TYPE.historyChange, function () {
        $undoButton.toggleClass('disable', !history.canGoBack());
        $redoButton.toggleClass('disable', !history.canForward());
    });

    function updateEnabled($e, validate) {
        $e.toggleClass('disable', Boolean(validate().error));
    }

    this.model.on(_constants.EVENT_TYPE.selectionChange, function () {
        instance.showMenu();
        updateEnabled($mergeButton, canMerge);
        updateEnabled($splitButton, canSplit);
    });

    this.model.on(_constants.EVENT_TYPE.selectSingleCell, function (evt, selectInfo) {
        instance.showMenu();

        updateEnabled($mergeButton, canMerge);
        updateEnabled($splitButton, canSplit);
    });

    this.model.on(_constants.EVENT_TYPE.layoutChanged, function () {
        updateEnabled($mergeButton, canMerge);
        updateEnabled($splitButton, canSplit);
    });

    this.model.on(_constants.EVENT_TYPE.pageDeleted, function (evt, pageModel) {
        instance.deletePage(pageModel);
    });

    this.setEditable = function (newEditable) {
        if (this.editable == newEditable) {
            return;
        }
        this.editable = newEditable;
        this.updateValueFromModel(_constants.EVENT_TYPE.valueChange, playerModel);
    };

    this.setForBuilder = function (building) {
        if (playerModel.building == building) {
            return;
        }

        playerModel.building = building;

        this.updateValueFromModel(_constants.EVENT_TYPE.valueChange, playerModel);
    };

    var fireCellSizeChanged = function fireCellSizeChanged() {
        playerModel.trigger(_constants.EVENT_TYPE.cellSizeChange);
    };

    $(window).resize(fireCellSizeChanged);

    this.appendButtons = function () {
        if (instance.model.building) {
            var container = instance.container;
            container.append($splitButton);
            container.append($mergeButton);
            container.append($undoButton);
            container.append($redoButton);
        } else {
            $splitButton.detach();
            $mergeButton.detach();
            $undoButton.detach();
            $redoButton.detach();
        }
    };

    this.updateValueFromModel = function (event, model, newValue) {
        if (model !== playerModel || !playerModel.config) {
            //значит событие кидает не модель проигрывателя форм а какой-то внутренний компонент либо меняют тип отображения не указав форму
            return;
        }

        var editable = this.editable;
        var building = playerModel.building;

        // очищаем контейнер
        this.container.children().empty();

        if (playerModel.config) {
            this.applyConfig(playerModel.config);
        }

        instance.views = [];
        playerModel.models.forEach(function (model) {
            var page = null;
            if (building) {
                page = new _tableBuilderView2.default(model, instance, editable);
            } else {
                page = new _tableStaticView2.default(model, instance, editable);
            }
            instance.views.push(page);
        });
        (0, _componentUtils.fillView)(this, instance.views, editable);

        instance.views.forEach(function (page) {
            page.executeScript(page.model, page, editable);
        });

        playerModel.trigger(_constants.EVENT_TYPE.formShow, [playerModel, this]);

        _player.bus.trigger(_constants.EVENT_TYPE.formShow, [playerModel, this]);

        if (playerModel.building && instance.views.length > 0) {
            instance.views[0].selectOneCell(0, 0);
            window.getSelection().removeAllRanges();
        }
    };

    this.unmarkInvalid = function () {
        instance.views.forEach(function (page) {
            page.unmarkInvalid();
        });
    };

    this.applyConfig = function (config) {
        if (!config) {
            config = {};
        }
        playerModel.config = config;
        (0, _styleUtils.applyColorStyle)(this.container, config);
    };

    this.getFullSelection = function () {
        return _underscore2.default.reduce(instance.views, function (memo, view) {
            return memo.concat(view.getFullSelection());
        }, []);
    };

    this.getSelection = function () {

        var tableSelections = instance.getFullSelection();

        var count = _underscore2.default.reduce(tableSelections, function (cnt, tableSelection) {
            return cnt + tableSelection.cells.length;
        }, 0);

        if (count !== 1) return null;

        var first = _underscore2.default.find(tableSelections, function (sel) {
            return sel.cells.length > 0;
        });
        var cell = first.cells[0];
        var result = {
            row: cell[0],
            column: cell[1],
            tableId: first.view.model.asfProperty.id,
            tableView: first.view
        };
        result.view = first.view.getCellView(cell);
        return result;
    };

    function scrollToBottom() {
        var scroll = instance.scrollParent;
        if (scroll) scroll.topScroll = 10000;
    }

    this.insertComponentTo = function (asfProperty, ignoreHistory) {
        if (!playerModel.building || asfProperty.type !== _constants.WIDGET_TYPE.page) {
            return;
        }
        var pageModel = playerModel.addPage(asfProperty.layout, []);
        var page = new _tableBuilderView2.default(pageModel, instance, instance.editable);
        instance.views.push(page);
        (0, _componentUtils.fillView)(this, instance.views, instance.editable);
        page.selectOneCell(0, 0);

        scrollToBottom();
        instance.container.focus();

        if (ignoreHistory !== true) {
            history.addItem(history.createPage(asfProperty.layout));
        }
    };

    this.deletePage = function (pageModel) {
        var pageView = null;
        var pageIndex = -1;
        instance.views.some(function (candidate, index) {
            if (candidate.model == pageModel) {
                pageView = candidate;
                pageIndex = index;
                return true;
            }
        });

        if (!pageView) return;

        pageView.container.empty();

        instance.views.splice(pageIndex, 1);
        (0, _componentUtils.fillView)(this, instance.views, instance.editable);

        this.container.append(instance.toolBar);
    };

    this.getViewWithId = function (cmpId, tableId, blockNumber) {
        var targetView = null;
        this.views.some(function (view) {

            if (view.model.asfProperty.id == cmpId) {
                targetView = view;
                return true;
            }

            var tmp = view.getViewWithId(cmpId, tableId, blockNumber);
            if (tmp) {
                targetView = tmp;
                return true;
            }
        });
        return targetView;
    };

    this.getPageWithView = function (view) {
        var page = _underscore2.default.find(this.views, function (page) {
            return page.viewBlock.getViewCoords(view);
        });

        if (page) return {
            page: page,
            cell: page.viewBlock.getViewCoords(view)
        };
    };

    this.getViewsWithType = function (type, tableId, blockNumber) {
        var result = [];
        this.views.forEach(function (view) {
            var subviews = view.getViewsWithType(type, tableId, blockNumber);
            if (subviews.length > 0) {
                result = result.concat(subviews);
            }
        });
        return result;
    };

    this.appendTo = function (widget, scrollParent) {
        instance.scrollParent = scrollParent;
        widget.append(this.container);
        playerModel.trigger(_constants.EVENT_TYPE.playerAttached);
    };

    this.destroy = function () {
        this.views = [];
        this.container.empty();
        $(window).off("resize", fireCellSizeChanged);
    };

    this.showMenu = function () {

        var tableSelections = instance.getFullSelection();
        var selectedCells = _underscore2.default.reduce(tableSelections, function (selectedCells, tableSelection) {
            tableSelection.cells.forEach(function (cell) {
                var view = tableSelection.view.getCellView(cell);
                if (!view) return;
                selectedCells.push(new _selectionInfo2.default(tableSelection.view.model.asfProperty.id, cell[0], cell[1], view));
            });
            return selectedCells;
        }, []);

        if (selectedCells.length === 0) {
            instance.hideMenu();
            return;
        }

        var toolBarCell = null;
        var minTop = 10000;
        var minLeft = 10000;

        var views = [];

        selectedCells.forEach(function (selectedCell) {
            if (!selectedCell.getView()) {
                return;
            }

            views.push(selectedCell.getView());

            var offset = selectedCell.getView().container.offset();
            if (offset.top > minTop) {
                return;
            }
            if (offset.top < minTop) {
                toolBarCell = selectedCell;
                minTop = offset.top;
                minLeft = offset.left;
                return;
            }

            if (offset.left < minLeft) {
                toolBarCell = selectedCell;
                minTop = offset.top;
                minLeft = offset.left;
            }
        });

        if (!toolBarCell) {
            instance.hideMenu();
            return;
        }

        instance.toolBar.updateToolbarState(toolBarCell, selectedCells);
    };

    this.getOwnerPage = function (cmpId, tableId, blockNumber) {
        var ownerPage = null;
        this.views.some(function (view) {
            var tmp = view.getViewWithId(cmpId, tableId, blockNumber);
            if (tmp) {
                ownerPage = view;
                return true;
            }
        });
        return ownerPage;
    };

    this.hideMenu = function () {
        instance.toolBar.updateToolbarState();
    };

    this.container.css("position", "relative");
}

PlayerView.prototype = Object.create(_view2.default.prototype);
PlayerView.prototype.constructor = PlayerView;
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ }),

/***/ 261:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = PositionLinkView;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _services = __webpack_require__(23);

var services = _interopRequireWildcard(_services);

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

var _constants = __webpack_require__(2);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _view = __webpack_require__(11);

var _view2 = _interopRequireDefault(_view);

var _UserChooser = __webpack_require__(81);

var _TagArea = __webpack_require__(72);

var _TagArea2 = _interopRequireDefault(_TagArea);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function PositionLinkView(model, playerView) {
    _view2.default.call(this, this, model, playerView);

    var config = model.asfProperty.config;

    var instance = this;

    var lastSearch = "";

    var locale = model.getLocale();

    var showSuggestions = function showSuggestions(search, suggestionInput) {

        if (search === lastSearch) {
            return;
        }
        lastSearch = search;
        api.getPositionsSuggestions(search, model.getFilterUserId(), model.getFilterDepartmentId(), model.isShowVacant(), true, locale, function (suggestions) {
            var values = [];
            suggestions.forEach(function (s, index) {
                values.push({ value: s.elementID, title: s.elementName, selected: index == 0 });
            });
            services.showSuggestOracle(values, suggestionInput, 200, function (selectedValue) {
                suggestions.forEach(function (s) {
                    if (s.elementID == selectedValue) {
                        s.tagName = s.elementName;
                        instance.addValue(s);
                    }
                });
                suggestionInput.val("");
            });
        });
    };

    var positionsTagArea = new _TagArea2.default({ width: "calc(100% - 30px)" }, 0, config['multi'], config['editable-label'], config['custom'], function (search, suggestionInput) {
        if (search.isEmpty()) {
            services.showDropDown([]);
        } else {
            showSuggestions(search, suggestionInput);
        }
    }, function (randomText) {
        var object = { elementID: _UserChooser.USER_CHOOSER_SUFFIXES.text + "-" + model.getNextTextNumber(),
            elementName: randomText,
            tagName: randomText };

        instance.addValue(object);
    });

    this.addValue = function (item) {
        var value = model.getValue().slice();
        value.push(item);
        model.setValueFromInput(value);
        instance.updateValueFromModel();
    };

    if (model.asfProperty.config['read-only']) {
        positionsTagArea.setEditable(false);
    }

    positionsTagArea.on(_constants.EVENT_TYPE.tagDelete, function () {
        model.setValueFromInput(positionsTagArea.getValues());
    });

    positionsTagArea.on(_constants.EVENT_TYPE.tagEdit, function () {
        model.setValueFromInput(positionsTagArea.getValues());
    });

    var chooserButton = (0, _jquery2.default)("<button/>", { class: "asf-browseButton" });

    dataUtils.addAsFormId(positionsTagArea.getWidget(), "input", model.asfProperty);
    dataUtils.addAsFormId(chooserButton, "button", model.asfProperty);

    chooserButton.click(function () {
        services.showPositionChooserDialog(model.getValue(), config['multi'], model.getFilterUserId(), model.getFilterDepartmentId(), config["only-vacant"], locale, function (selectedValues) {

            selectedValues.forEach(function (item) {
                if (item.transitional) {
                    item.tagName = item.elementName + " (" + item.departmentName + ")";
                } else {
                    item.tagName = item.elementName;
                }
            });

            model.setValue(selectedValues);
            instance.updateValueFromModel();
        });
    });

    this.updateValueFromModel = function () {
        positionsTagArea.setValues(model.getValue());
    };

    this.unmarkInvalid = function () {
        positionsTagArea.markValidity(true);
    };

    this.markInvalid = function () {
        positionsTagArea.markValidity(false);
    };

    this.setEnabled = function (newEnabled) {
        positionsTagArea.setEditable(newEnabled);
        if (newEnabled) {
            chooserButton.css("pointer-events", "auto");
        } else {
            chooserButton.css("pointer-events", "none");
        }
    };
    this.setEnabled(!dataUtils.isReadOnly(model));

    model.on(_constants.EVENT_TYPE.dataLoad, function () {
        instance.updateValueFromModel();
    });

    this.updateValueFromModel();

    this.container.append(positionsTagArea.getWidget());
    this.container.append(chooserButton);
};

PositionLinkView.prototype = Object.create(_view2.default.prototype);
PositionLinkView.prototype.constructor = PositionLinkView;

/***/ }),

/***/ 262:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = ProcessExecutionView;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

var _constants = __webpack_require__(2);

var _componentUtils = __webpack_require__(15);

var compUtils = _interopRequireWildcard(_componentUtils);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _view = __webpack_require__(11);

var _view2 = _interopRequireDefault(_view);

var _tableUtils = __webpack_require__(52);

var tableUtils = _interopRequireWildcard(_tableUtils);

var _dateUtils = __webpack_require__(73);

var dateUtils = _interopRequireWildcard(_dateUtils);

var _styleUtils = __webpack_require__(21);

var styleUtils = _interopRequireWildcard(_styleUtils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ProcessExecutionView(model, playerView, editable) {

    var MIN_WIDTHS = [100, 80, 80, 80, 80, 80, 80];
    var COMMON_WIDTHS = [0, 120, 100, 100, 100, 100, 140];
    var MIN_WIDTH = 600;
    var COMMON_WIDTH = 860;

    _view2.default.call(this, this, model, playerView);

    var instance = this;

    var table = tableUtils.createTable("processExecutionTable", model.asfProperty.style);
    table.prop('border', 1);
    table.prop('cellpadding', 2);

    var locale = dataUtils.getComponentLocale(model.asfProperty);

    var nameHeader = tableUtils.createHeaderCell("Наименование", locale, "120px", true);
    table.children('thead').append(nameHeader);

    var resUserHeader = tableUtils.createHeaderCell("Ответственный", locale, "120px", true);
    table.children('thead').append(resUserHeader);

    var authorHeader = tableUtils.createHeaderCell("Автор", locale, "100px", true);
    table.children('thead').append(authorHeader);

    var startDateHeader = tableUtils.createHeaderCell("Начало", locale, "100px", true);
    table.children('thead').append(startDateHeader);

    var finishDateHeader = tableUtils.createHeaderCell("Завершение", locale, "100px", true);
    table.children('thead').append(finishDateHeader);

    var finishedUserHeader = tableUtils.createHeaderCell("Завершил", locale, "100px", true);
    table.children('thead').append(finishedUserHeader);

    var commentHeader = tableUtils.createHeaderCell("Комментарий", locale, "140px", true);
    table.children('thead').append(commentHeader);

    var tr = tableUtils.createRow({ columns: 1 });
    compUtils.setTranslatedValue("Данные заполняются автоматически в режиме просмотра и в версии для печати", locale, tr.children('td')[0]);
    (0, _jquery2.default)(tr.children('td')[0]).prop("colspan", 7);
    (0, _jquery2.default)(tr.children('td')[0]).addClass("asf-borderedCell");
    table.children('tbody').first().append(tr);

    this.loadData = function () {
        if (editable) {
            return;
        }
        if (!model.playerModel.asfDataId) {
            return;
        }
        table.children('tbody').children().empty();
        api.getProcessExecution(model.playerModel.asfDataId, dataUtils.getComponentLocale(model.asfProperty), function (histories) {
            table.children('tbody').first().children().empty();
            instance.addHistories(histories, 0);
        });
    };

    var setDate = function setDate(date, cell) {
        if (locale === _constants.OPTIONS.locale) {
            var data = dateUtils.formatDate(new Date(date), dateUtils.EXECUTION_LIST_FORMAT);
            cell.html(data);
        } else {
            var formattedDate = dateUtils.formatDate(new Date(date), dateUtils.DATE_FORMAT_FULL);
            _jquery2.default.when(api.formatDate(formattedDate, dateUtils.EXECUTION_LIST_FORMAT, locale)).then(function (data) {
                cell.html(data);
            });
        }
    };

    this.addHistories = function (histories, level) {
        histories.forEach(function (history) {
            var tr = tableUtils.createRow({ columns: 7 });

            var className = "asf-borderedCell";
            if (history.deleted) {
                className += " asf-linedThrough";
            }

            if (!history.procInstID) {
                className += " asf-lightGray";
            }

            (0, _jquery2.default)(tr.children('td')[0]).html(history.name);
            (0, _jquery2.default)(tr.children('td')[0]).addClass(className);

            if (level > 0) {
                (0, _jquery2.default)(tr.children('td')[0]).css("text-indent", level * 20 + "px");
            }

            if (history.userName) {
                (0, _jquery2.default)(tr.children('td')[1]).html(history.userName);
            }
            if (history.authorName) {
                (0, _jquery2.default)(tr.children('td')[2]).html(history.authorName);
            }
            if (history.started) {
                setDate(history.started, (0, _jquery2.default)(tr.children('td')[3]));
            }
            if (history.finished) {
                setDate(history.finished, (0, _jquery2.default)(tr.children('td')[4]));
            }
            if (history.finishedUser) {
                (0, _jquery2.default)(tr.children('td')[5]).html(history.finishedUser);
            }

            if (history.comment) {
                (0, _jquery2.default)(tr.children('td')[6]).html(history.comment);
            }

            for (var i = 0; i < 7; i++) {
                (0, _jquery2.default)(tr.children('td')[i]).addClass(className);
            }

            table.children('tbody').first().append(tr);
            if (history.subProcesses) {
                instance.addHistories(history.subProcesses, level + 1);
            }
        });
    };

    this.applySpecialStyle = function () {
        styleUtils.applyTableStyle(table, model.asfProperty.style);
    };

    model.playerModel.on(_constants.EVENT_TYPE.dataLoad, function () {
        instance.loadData();
    });

    this.loadData();

    this.container.append(table);

    this.updateTableSize = function () {
        var parentWidth = instance.container.width();
        if (parentWidth < COMMON_WIDTH) {
            table.css('min-width', MIN_WIDTH + "px");
            nameHeader.css('width', MIN_WIDTHS[0] + "px");
            resUserHeader.css('width', MIN_WIDTHS[1] + "px");
            authorHeader.css('width', MIN_WIDTHS[2] + "px");
            startDateHeader.css('width', MIN_WIDTHS[3] + "px");
            finishDateHeader.css('width', MIN_WIDTHS[4] + "px");
            finishedUserHeader.css('width', MIN_WIDTHS[5] + "px");
            commentHeader.css('width', MIN_WIDTHS[6] + "px");
        } else {
            table.css('width', "100%");
            nameHeader.css('width', "auto");
            resUserHeader.css('width', COMMON_WIDTHS[1] + "px");
            authorHeader.css('width', COMMON_WIDTHS[2] + "px");
            startDateHeader.css('width', COMMON_WIDTHS[3] + "px");
            finishDateHeader.css('width', COMMON_WIDTHS[4] + "px");
            finishedUserHeader.css('width', COMMON_WIDTHS[5] + "px");
            commentHeader.css('width', COMMON_WIDTHS[6] + "px");
        }
    };

    setTimeout(function () {
        instance.updateTableSize();
    }, 0);
};

ProcessExecutionView.prototype = Object.create(_view2.default.prototype);
ProcessExecutionView.prototype.constructor = ProcessExecutionView;

/***/ }),

/***/ 263:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = ProjectLinkTextView;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _i18n = __webpack_require__(14);

var _i18n2 = _interopRequireDefault(_i18n);

var _services = __webpack_require__(23);

var services = _interopRequireWildcard(_services);

var _constants = __webpack_require__(2);

var _styleUtils = __webpack_require__(21);

var styleUtils = _interopRequireWildcard(_styleUtils);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _view = __webpack_require__(11);

var _view2 = _interopRequireDefault(_view);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ProjectLinkTextView(model, playerView) {
    _view2.default.call(this, this, model, playerView);

    var instance = this;

    var link = (0, _jquery2.default)('<div/>', { class: 'asf-link asf-label' });
    dataUtils.addAsFormId(link, "text", model.asfProperty);
    styleUtils.applyLabelStyle(link, model.asfProperty.style, true);
    link.click(function () {
        if (model.getElementType() == _constants.OBJECT_TYPES.PLAN) {
            if (model.isDeleted()) {
                alert(_i18n2.default.tr("У вас нет прав на чтение данного проекта"));
            } else {
                services.showPlan(model.value);
            }
        } else {
            if (model.isDeleted()) {
                alert(_i18n2.default.tr("У вас нет прав на чтение данного портфеля либо портфель был удален"));
            } else {
                services.showPortfolio(model.value);
            }
        }
    });

    this.updateValueFromModel = function () {
        link.html(model.getTextValue());
    };

    model.on(_constants.EVENT_TYPE.dataLoad, function () {
        instance.updateValueFromModel();
    });

    this.container.append(link);

    instance.updateValueFromModel();
};

ProjectLinkTextView.prototype = Object.create(_view2.default.prototype);
ProjectLinkTextView.prototype.constructor = ProjectLinkTextView;

/***/ }),

/***/ 264:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = ProjectLinkView;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _services = __webpack_require__(23);

var services = _interopRequireWildcard(_services);

var _constants = __webpack_require__(2);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _view = __webpack_require__(11);

var _view2 = _interopRequireDefault(_view);

var _TagArea = __webpack_require__(72);

var _TagArea2 = _interopRequireDefault(_TagArea);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ProjectLinkView(model, playerView) {
    _view2.default.call(this, this, model, playerView);

    var enabled = true;

    var instance = this;

    var tagArea = new _TagArea2.default({ width: "calc(100% - 30px)" }, 0, false, false, false);

    var button = (0, _jquery2.default)("<button/>", { class: "asf-browseButton" });

    this.container.append(tagArea.getWidget());
    this.container.append(button);

    dataUtils.addAsFormId(tagArea, "input", model.asfProperty);
    dataUtils.addAsFormId(button, "button", model.asfProperty);

    this.showProjectLinkDialog = function () {
        if (enabled) {
            services.showProjectLinkDialog(function (value) {
                model.setValueFromInput(value[0]);
                instance.updateValueFromModel();
            });
        }
    };

    tagArea.on(_constants.EVENT_TYPE.tagDelete, function () {
        model.setValueFromInput(null);
    });

    button.click(function () {
        instance.showProjectLinkDialog();
    });

    this.unmarkInvalid = function () {
        tagArea.markValidity(true);
    };

    this.markInvalid = function () {
        tagArea.markValidity(false);
    };

    this.updateValueFromModel = function () {
        if (model.value) {
            var valueObject = { value: model.value, tagName: model.getTextValue() };
            tagArea.setValues([valueObject]);
        } else {
            tagArea.setValues([]);
        }
        instance.unmarkInvalid();
    };

    this.setEnabled = function (newEnabled) {
        enabled = newEnabled;
        tagArea.setEditable(enabled);
    };

    this.setEnabled(!dataUtils.isReadOnly(model));

    instance.updateValueFromModel();
};

ProjectLinkView.prototype = Object.create(_view2.default.prototype);
ProjectLinkView.prototype.constructor = ProjectLinkView;

/***/ }),

/***/ 265:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = RadioButtonView;
exports.RadioButtonBuilderView = RadioButtonBuilderView;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _constants = __webpack_require__(2);

var _styleUtils = __webpack_require__(21);

var styleUtils = _interopRequireWildcard(_styleUtils);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _view = __webpack_require__(11);

var _view2 = _interopRequireDefault(_view);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function RadioButtonView(model, playerView) {
    _view2.default.call(this, this, model, playerView);

    var radiosList = [];

    var instance = this;

    var enabled = true;

    this.updateDataFromModel = function () {
        instance.container.empty();
        radiosList = [];
        model.listCurrentElements.forEach(function (listElement) {
            var asfProperty = model.asfProperty;
            var name = asfProperty.ownerTableId + (asfProperty.tableBlockIndex ? "." + asfProperty.tableBlockIndex : "") + "." + asfProperty.id;
            var radioButton = (0, _jquery2.default)('<input/>', { type: 'radio', class: 'asf-radioButton', name: name });
            dataUtils.addAsFormId(radioButton, "input", asfProperty);

            var $label = (0, _jquery2.default)('<div/>', { class: "asf-buttonLabel" });
            styleUtils.applyLabelStyle($label, model.asfProperty.style, true);

            $label.append(radioButton);
            $label.append(listElement.label);

            $label.click(function (evt) {
                if (evt.target !== $label[0] || !enabled) {
                    return;
                }
                if (radioButton.prop("checked")) {
                    radioButton.removeProp("checked", "checked");
                } else {
                    radioButton.prop("checked", "checked");
                }
                instance.updateModelValue();
            });

            radioButton.click(function (evt) {
                instance.updateModelValue();
            });

            radiosList.push({ value: listElement.value, component: radioButton, label: $label });
            instance.container.append($label);

            model.playerModel.trigger(_constants.EVENT_TYPE.cellSizeChange, model);
        });

        instance.setEnabled(enabled);

        instance.updateValueFromModel();
    };

    this.unmarkInvalid = function () {
        this.container.removeClass('asf-invalidInput');
    };

    this.markInvalid = function () {
        this.container.addClass('asf-invalidInput');
    };

    this.setEnabled = function (newEnabled) {
        enabled = newEnabled;
        radiosList.forEach(function (radioElement) {
            if (enabled) {
                (0, _jquery2.default)(radioElement.component).removeProp("disabled");
            } else {
                (0, _jquery2.default)(radioElement.component).prop("disabled", true);
            }
        });
    };
    this.setEnabled(!dataUtils.isReadOnly(model));

    this.updateModelValue = function () {
        var selectedValues = [];
        radiosList.forEach(function (radioElement) {
            if (radioElement.component.prop("checked")) {
                selectedValues.push(radioElement.value);
            }
        });
        model.setValue(selectedValues);
        this.unmarkInvalid();
    };

    this.updateValueFromModel = function () {
        var selectedKeys = model.getValue();

        radiosList.forEach(function (radioElement) {
            if (selectedKeys && selectedKeys.indexOf(radioElement.value) > -1) {
                (0, _jquery2.default)(radioElement.component).prop("checked", true);
            } else {
                (0, _jquery2.default)(radioElement.component).prop("checked", false);
            }
        });
    };

    this.updateDataFromModel();

    model.on(_constants.EVENT_TYPE.dataLoad, function () {
        instance.updateDataFromModel();
    });
};

RadioButtonView.prototype = Object.create(_view2.default.prototype);
RadioButtonView.prototype.constructor = RadioButtonView;

function RadioButtonBuilderView(model, playerView) {
    RadioButtonView.call(this, model, playerView);

    if (model.listCurrentElements.length === 0) {
        var emptyLabel = (0, _jquery2.default)("<div>", { class: "asf-label" });
        emptyLabel.text("Переключатель вариантов");
        this.container.append(emptyLabel);
    }
};

RadioButtonBuilderView.prototype = Object.create(RadioButtonView.prototype);
RadioButtonBuilderView.prototype.constructor = RadioButtonBuilderView;

/***/ }),

/***/ 266:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = RegistryLinkView;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _i18n = __webpack_require__(14);

var _i18n2 = _interopRequireDefault(_i18n);

var _services = __webpack_require__(23);

var services = _interopRequireWildcard(_services);

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

var _constants = __webpack_require__(2);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _view = __webpack_require__(11);

var _view2 = _interopRequireDefault(_view);

var _TagArea = __webpack_require__(72);

var _TagArea2 = _interopRequireDefault(_TagArea);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function RegistryLinkView(model, playerView, editable) {
    var instance = this;
    var enabled = false;
    _view2.default.call(this, this, model, playerView);

    var tagArea = new _TagArea2.default({ width: "calc(100% - 30px)" }, 0, false, false, false);
    var chooserButton = (0, _jquery2.default)("<button/>", { class: "asf-browseButton" });

    var fileNameLabel = (0, _jquery2.default)('<span>', { class: "asf-label asf-file-name" });

    if (editable) {
        this.container.append(tagArea.getWidget());
        this.container.append(chooserButton);
    } else {
        this.container.append(fileNameLabel);
    }

    dataUtils.addAsFormId(tagArea.getWidget(), "input", model.asfProperty);
    dataUtils.addAsFormId(chooserButton, "button", model.asfProperty);

    this.showRegistryLinkLinkDialog = function () {

        if (!model.getRegistryID() || model.getRegistryID() === '') {
            alert(_i18n2.default.tr("Не выбран реестр"));
            return;
        }

        var registry = null;

        api.getRegistry(model.getRegistryID(), function (reg) {
            registry = reg;
            registry.registryID = model.getRegistryID();

            if (registry === null) {
                return;
            }

            if (registry.rights === 'no') {
                alert(_i18n2.default.tr("Доступ к реестру запрещен"));
                return;
            }

            services.showRegisterLinkDialog(registry, function (documentId) {
                model.setValue(documentId);
            });
        });
    };

    tagArea.on(_constants.EVENT_TYPE.tagDelete, function () {
        model.setValue(null);
    });

    chooserButton.click(function () {
        if (enabled) {
            instance.showRegistryLinkLinkDialog();
        }
    });

    fileNameLabel.click(function () {
        services.showDocument(model.value);
    });

    this.unmarkInvalid = function () {
        tagArea.markValidity(true);
    };

    this.markInvalid = function () {
        tagArea.markValidity(false);
    };

    this.updateValueFromModel = function () {

        if (model.value) {
            var valueObject = { value: model.value, tagName: model.getTextValue() };
            tagArea.setValues([valueObject]);
        } else {
            tagArea.setValues([]);
        }

        fileNameLabel.html(model.getTextValue());
        instance.unmarkInvalid();
    };

    this.setEnabled = function (newEnabled) {
        enabled = newEnabled;
        tagArea.setEditable(newEnabled);
    };

    this.setEnabled(!dataUtils.isReadOnly(model));

    model.on(_constants.EVENT_TYPE.dataLoad, function () {
        instance.updateValueFromModel();
    });

    this.updateValueFromModel();

    this.tagArea = function () {
        return tagArea;
    };
};

RegistryLinkView.prototype = Object.create(_view2.default.prototype);
RegistryLinkView.prototype.constructor = RegistryLinkView;

/***/ }),

/***/ 267:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = RepeatPeriodView;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _i18n = __webpack_require__(14);

var _i18n2 = _interopRequireDefault(_i18n);

var _services = __webpack_require__(23);

var services = _interopRequireWildcard(_services);

var _constants = __webpack_require__(2);

var _styleUtils = __webpack_require__(21);

var styleUtils = _interopRequireWildcard(_styleUtils);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _view = __webpack_require__(11);

var _view2 = _interopRequireDefault(_view);

var _TagArea = __webpack_require__(72);

var _TagArea2 = _interopRequireDefault(_TagArea);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Created by exile
 * Date: 03.03.16
 * Time: 16:25
 */
function RepeatPeriodView(model, playerView) {

    this.initialWidth = 345;

    _view2.default.call(this, this, model, playerView);

    var typeInput = (0, _jquery2.default)('<div/>', { class: 'asf-dropdown-input' });
    typeInput.css("width", "90px");

    var typeDropdownButton = (0, _jquery2.default)("<button/>", { class: "asf-dropdown-button" });

    var typeContainer = (0, _jquery2.default)('<div/>', { class: 'asf-InlineBlock' });
    typeContainer.append(typeInput);
    typeContainer.append(typeDropdownButton);
    typeContainer.css("vertical-align", "top");

    this.container.append(typeContainer);

    this.container.append((0, _jquery2.default)('<div style="width:2px; display: inline-block"/>'));

    var valuesInput = (0, _jquery2.default)('<div/>', { class: 'asf-repeatPeriodInput' });
    valuesInput.css("width", "140px");
    var valuesButton = (0, _jquery2.default)("<button/>", { class: "asf-calendar-button" });
    valuesButton.css("vertical-align", "top");

    var valuesContainer = (0, _jquery2.default)('<div/>', { class: 'asf-Inline' });

    var valuesTagArea = new _TagArea2.default({ width: "140px" }, 140, true, false, false);
    valuesTagArea.on(_constants.EVENT_TYPE.tagDelete, function () {

        var values = [];
        valuesTagArea.getValues().forEach(function (v) {
            values.push(v.value);
        });

        model.setValueFromInput(model.type, values);
    });

    valuesContainer.append(valuesTagArea.getWidget());
    valuesContainer.append(valuesInput);
    valuesContainer.append(valuesButton);

    this.container.append(valuesContainer);

    dataUtils.addAsFormId(typeInput, "input", model.asfProperty);
    dataUtils.addAsFormId(typeDropdownButton, "type.button", model.asfProperty);
    dataUtils.addAsFormId(valuesInput, "value", model.asfProperty);
    dataUtils.addAsFormId(valuesButton, "value.button", model.asfProperty);
    dataUtils.addAsFormId(valuesTagArea.getWidget(), "year", model.asfProperty);

    var instance = this;

    var enabled = false;

    var showTypeDropDown = function showTypeDropDown() {
        if (!enabled) {
            return;
        }
        var values = [{ value: _constants.REPEAT_PERIOD_TYPE.none, title: _i18n2.default.tr("Нет"), selected: model.type == _constants.REPEAT_PERIOD_TYPE.none }, { value: _constants.REPEAT_PERIOD_TYPE.week_days, title: _i18n2.default.tr("По дням недели"), selected: model.type == _constants.REPEAT_PERIOD_TYPE.week_days }, { value: _constants.REPEAT_PERIOD_TYPE.month_days, title: _i18n2.default.tr("По дням месяца"), selected: model.type == _constants.REPEAT_PERIOD_TYPE.month_days }, { value: _constants.REPEAT_PERIOD_TYPE.year_days, title: _i18n2.default.tr("Ежегодно"), selected: model.type == _constants.REPEAT_PERIOD_TYPE.year_days }];
        services.showDropDown(values, typeContainer, null, function (selectedValue) {
            model.setValueFromInput(selectedValue, []);
            instance.updateValueFromModel();
        });
    };

    var showDaysChooser = function showDaysChooser() {
        if (!enabled) {
            return;
        }
        services.showDaysChooser(model.type, model.value, valuesButton, function (selectedValues) {

            selectedValues = selectedValues.sort(function (a, b) {
                var dayA = parseInt(a.substring(0, a.indexOf(".")));
                var monthA = parseInt(a.substring(a.indexOf(".") + 1));

                var dayB = parseInt(b.substring(0, b.indexOf(".")));
                var monthB = parseInt(b.substring(b.indexOf(".") + 1));

                if (monthA != monthB) {
                    return monthA - monthB;
                }
                return dayA - dayB;
            });

            model.setValueFromInput(model.type, selectedValues);
            instance.updateValueFromModel();
        });
    };

    typeInput.click(function () {
        showTypeDropDown();
    });

    typeDropdownButton.click(function () {
        showTypeDropDown();
    });

    valuesInput.click(function () {
        showDaysChooser();
    });

    valuesButton.click(function () {
        showDaysChooser();
    });

    this.updateValueFromModel = function () {

        instance.unmarkInvalid();

        typeInput.html(model.getTypeText());
        valuesInput.empty();

        if (model.type == _constants.REPEAT_PERIOD_TYPE.none) {
            valuesContainer.hide();
            instance.initialWidth = 125;
        } else if (model.type == _constants.REPEAT_PERIOD_TYPE.year_days) {
            valuesContainer.css("display", "inline-block");
            valuesInput.hide();
            valuesTagArea.setVisible(true);

            var values = [];
            model.value.forEach(function (v) {
                values.push({
                    value: v,
                    tagName: model.getValueString(v, model.type)
                });
            });

            valuesTagArea.setValues(values);
            instance.initialWidth = 345;
        } else {
            valuesContainer.css("display", "inline-block");
            valuesInput.css("display", "inline-block");
            valuesTagArea.setVisible(false);
            instance.initialWidth = 300;
        }

        styleUtils.applyPositionStyle(instance.container, model.asfProperty.style, instance.initialWidth);
        var str = "";
        var sign = "";
        model.value.forEach(function (v) {
            str += sign + model.getValueString(v, model.type);
            sign = ", ";
        });
        valuesInput.html(str);
    };

    this.unmarkInvalid = function () {
        typeInput.removeClass('asf-invalidInput');
        valuesInput.removeClass('asf-invalidInput');
        valuesTagArea.markValidity(true);
    };

    this.markInvalid = function () {
        typeInput.addClass('asf-invalidInput');
        valuesInput.addClass('asf-invalidInput');
        valuesTagArea.markValidity(false);
    };

    this.setEnabled = function (newEnabled) {
        enabled = newEnabled;
        if (enabled) {
            typeInput.removeClass('asf-disabledInput');
            valuesInput.removeClass('asf-disabledInput');
        } else {
            typeInput.addClass('asf-disabledInput');
            valuesInput.addClass('asf-disabledInput');
        }
        valuesTagArea.setEditable(newEnabled);
    };
    this.setEnabled(!dataUtils.isReadOnly(model));

    this.updateValueFromModel();
};

RepeatPeriodView.prototype = Object.create(_view2.default.prototype);
RepeatPeriodView.prototype.constructor = RepeatPeriodView;

/***/ }),

/***/ 268:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = ResolutionListView;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

var _constants = __webpack_require__(2);

var _componentUtils = __webpack_require__(15);

var compUtils = _interopRequireWildcard(_componentUtils);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _view = __webpack_require__(11);

var _view2 = _interopRequireDefault(_view);

var _tableUtils = __webpack_require__(52);

var tableUtils = _interopRequireWildcard(_tableUtils);

var _dateUtils = __webpack_require__(73);

var dateUtils = _interopRequireWildcard(_dateUtils);

var _styleUtils = __webpack_require__(21);

var styleUtils = _interopRequireWildcard(_styleUtils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ResolutionListView(model, playerView, editable) {

    var MIN_WIDTHS = [100, 100, 100, 100, 100];
    var COMMON_WIDTHS = [120, 0, 120, 160, 120];
    var MIN_WIDTH = 500;
    var COMMON_WIDTH = 620;

    _view2.default.call(this, this, model, playerView);

    var instance = this;

    var locale = dataUtils.getComponentLocale(model.asfProperty);

    var table = tableUtils.createTable("resolutionList", model.asfProperty.style);
    table.prop('border', 1);
    table.prop('cellpadding', 2);

    var authorHeader = tableUtils.createHeaderCell("Автор", locale, "120px", true);
    table.children('thead').append(authorHeader);

    var nameHeader = tableUtils.createHeaderCell("Название", locale, null, true);
    table.children('thead').append(nameHeader);

    var resUserHeader = tableUtils.createHeaderCell("Ответственный", locale, "120px", true);
    table.children('thead').append(resUserHeader);

    var resUsersHeader = tableUtils.createHeaderCell("Исполнители", locale, "160px", true);
    table.children('thead').append(resUsersHeader);

    var finishDateHeader = tableUtils.createHeaderCell("Завершение", locale, "100px", true);
    table.children('thead').append(finishDateHeader);

    var tr = tableUtils.createRow({ columns: 1 });
    compUtils.setTranslatedValue("Данные заполняются автоматически в режиме просмотра и в версии для печати", locale, tr.children('td')[0]);
    (0, _jquery2.default)(tr.children('td')[0]).addClass("asf-borderedCell");
    (0, _jquery2.default)(tr.children('td')[0]).prop("colspan", 5);
    table.children('tbody').first().append(tr);

    this.loadData = function () {

        if (editable) {
            return;
        }

        if (!model.playerModel.asfDataId) {
            return;
        }

        api.getResolutionsList(model.playerModel.asfDataId, dataUtils.getComponentLocale(model.asfProperty), function (projects) {
            instance.setResolutions(projects);
        });
    };

    this.setResolutions = function (projects) {
        table.children('tbody').children().empty();

        projects.forEach(function (project) {
            var tr = tableUtils.createRow({ columns: 5 });
            (0, _jquery2.default)(tr.children('td')[0]).html(project.authorName);
            (0, _jquery2.default)(tr.children('td')[0]).prop("rowspan", project.items.length);
            (0, _jquery2.default)(tr.children('td')[0]).addClass("asf-borderedCell");

            if (project.items[0].name) {
                (0, _jquery2.default)(tr.children('td')[1]).html(project.items[0].name);
                (0, _jquery2.default)(tr.children('td')[1]).addClass("asf-borderedCell");
            }

            if (project.items[0].user) {
                (0, _jquery2.default)(tr.children('td')[2]).html(project.items[0].user);
                (0, _jquery2.default)(tr.children('td')[2]).addClass("asf-borderedCell");
            }

            (0, _jquery2.default)(tr.children('td')[3]).html(project.items[0].userRow);
            (0, _jquery2.default)(tr.children('td')[3]).addClass("asf-borderedCell");

            if (project.items[0].finishDate) {
                (0, _jquery2.default)(tr.children('td')[4]).html(dateUtils.formatDate(new Date(project.items[0].finishDate), dateUtils.RESOLUTION_LIST_FORMAT));
                (0, _jquery2.default)(tr.children('td')[4]).addClass("asf-borderedCell");
            }

            table.children('tbody').first().append(tr);

            project.items.splice(1).forEach(function (item) {
                var tr = tableUtils.createRow({ columns: 4 });
                if (item.name) {
                    (0, _jquery2.default)(tr.children('td')[0]).html(item.name);
                    (0, _jquery2.default)(tr.children('td')[0]).addClass("asf-borderedCell");
                }

                if (item.user) {
                    (0, _jquery2.default)(tr.children('td')[1]).html(item.user);
                    (0, _jquery2.default)(tr.children('td')[1]).addClass("asf-borderedCell");
                }

                (0, _jquery2.default)(tr.children('td')[2]).html(item.userRow);
                (0, _jquery2.default)(tr.children('td')[2]).addClass("asf-borderedCell");

                if (item.finishDate) {
                    (0, _jquery2.default)(tr.children('td')[3]).html(dateUtils.formatDate(new Date(item.finishDate), dateUtils.RESOLUTION_LIST_FORMAT));
                    (0, _jquery2.default)(tr.children('td')[3]).addClass("asf-borderedCell");
                }

                table.children('tbody').first().append(tr);
            });
        });
    };

    this.applySpecialStyle = function () {
        styleUtils.applyTableStyle(table, model.asfProperty.style);
    };

    model.playerModel.on(_constants.EVENT_TYPE.dataLoad, function () {
        instance.loadData();
    });

    this.loadData();

    this.container.append(table);

    this.updateTableSize = function () {
        var parentWidth = instance.container.width();
        if (parentWidth < COMMON_WIDTH) {
            table.css('min-width', MIN_WIDTH + "px");
            authorHeader.css('width', MIN_WIDTHS[0] + "px");
            nameHeader.css('width', "auto");
            resUserHeader.css('width', MIN_WIDTHS[2] + "px");
            resUsersHeader.css('width', MIN_WIDTHS[3] + "px");
            finishDateHeader.css('width', MIN_WIDTHS[4] + "px");
        } else {
            table.css('width', "100%");
            authorHeader.css('width', COMMON_WIDTHS[0] + "px");
            nameHeader.css('width', "auto");
            resUserHeader.css('width', COMMON_WIDTHS[2] + "px");
            resUsersHeader.css('width', COMMON_WIDTHS[3] + "px");
            finishDateHeader.css('width', COMMON_WIDTHS[4] + "px");
        }
    };

    setTimeout(function () {
        instance.updateTableSize();
    }, 0);
};

ResolutionListView.prototype = Object.create(_view2.default.prototype);
ResolutionListView.prototype.constructor = ResolutionListView;

/***/ }),

/***/ 269:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = RichTextView;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _underscore = __webpack_require__(7);

var _underscore2 = _interopRequireDefault(_underscore);

var _constants = __webpack_require__(2);

var _styleUtils = __webpack_require__(21);

var styleUtils = _interopRequireWildcard(_styleUtils);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _view = __webpack_require__(11);

var _view2 = _interopRequireDefault(_view);

var _tinymce = __webpack_require__(1003);

var _tinymce2 = _interopRequireDefault(_tinymce);

__webpack_require__(1001);

__webpack_require__(1000);

__webpack_require__(999);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

if (window.FORM_PLAYER_URL_PREFIX) {
    _tinymce2.default.baseURL = FORM_PLAYER_URL_PREFIX + 'js/vendors/tinymce/';
} else {
    _tinymce2.default.baseURL = 'js/vendors/tinymce/';
}

/**
 * Created by exile
 * Date: 03.02.16
 * Time: 16:25
 */
function RichTextView(model, playerView) {

    _view2.default.call(this, this, model, playerView);

    var id = model.asfProperty.id.replace(/ /g, '_') + "_" + Math.ceil(Math.random() * 1000) + "_" + Math.ceil(Math.random() * 1000);
    var textArea = (0, _jquery2.default)('<textarea/>', { class: 'asf-textBox', "id": id });
    styleUtils.applySize(textArea, model.asfProperty.style);

    var instance = this;

    var editorInstance = null;

    var oldValue = "";

    this.updateValueFromModel = function () {
        if (oldValue == model.getTextValue()) {
            return;
        }
        textArea.val(model.getTextValue());
        if (editorInstance) {
            try {
                editorInstance.setContent(model.getTextValue());
            } catch (ignored) {}
        }
    };

    this.updateValueFromModel();

    this.container.append(textArea);

    var enabled = true;

    setTimeout(function () {
        // надо чтобы значение вставилось а потом мы его проверим, а таким образом мы добавляем вызов функции в конец ивентлупа
        _tinymce2.default.init({
            selector: '#' + id,
            plugins: "textcolor lists",
            toolbar: 'fontselect fontsizeselect | bold italic underline strikethrough | alignleft aligncenter alignright alignjustify | forecolor backcolor | bullist  numlist | outdent indent | removeformat',
            font_formats: 'Шрифт=arial;Arial=arial;Courier New=courier new,courier;Tahoma=tahoma;Times New Roman=times new roman,times;Trebuchet MS=trebuchet ms,geneva;Verdana=verdana,geneva',
            fontsize_formats: '7px 8px 9px 10px 12px 14px 18px 24px',
            content_style: ".mce-content-body {font-size:10px;font-family:Arial}",
            menubar: false,
            statusbar: false,
            language: _constants.OPTIONS.locale,
            readonly: !enabled,
            force_p_newlines: false,
            force_br_newlines: true,
            convert_newlines_to_brs: false,
            remove_linebreaks: true,
            forced_root_block: '',
            // skin_url: "js/vendors/tinymce/skins/lightgray",
            // theme_url: "js/vendors/tinymce/themes/modern/theme.min.js",
            init_instance_callback: function init_instance_callback() {
                model.playerModel.trigger(_constants.EVENT_TYPE.cellSizeChange, model);
            },
            setup: function setup(editor) {

                editorInstance = editor;
                editor.on('focus', function (e) {
                    instance.unmarkInvalid();
                });

                editor.on('blur', function (e) {
                    oldValue = editor.getContent();
                    model.setValueFromInput(editor.getContent());
                });

                editor.on('change', function (ed, l) {
                    oldValue = editor.getContent();
                    model.setValueFromInput(editor.getContent());
                });

                editor.on('mouseup', function () {
                    if (instance.container.parent()[0]) {
                        instance.container.parent().click();
                    }
                });
            }
        });

        instance.updateValueFromModel();
    }, 0);

    this.setEnabled = function (newEnabled) {
        enabled = newEnabled;
        if (editorInstance) {
            try {
                editorInstance.getBody().setAttribute('contenteditable', enabled);
            } catch (ignored) {}
        }
    };
    this.setEnabled(!dataUtils.isReadOnly(model));

    this.reInit = function () {
        _tinymce2.default.EditorManager.execCommand('mceRemoveEditor', true, id);
        _tinymce2.default.EditorManager.execCommand('mceAddEditor', true, id);
    };

    playerView.model.on(_constants.EVENT_TYPE.layoutChanged, instance.reInit);
};

RichTextView.prototype = Object.create(_view2.default.prototype);
RichTextView.prototype.constructor = RichTextView;

/***/ }),

/***/ 270:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = SignListView;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _i18n = __webpack_require__(14);

var _i18n2 = _interopRequireDefault(_i18n);

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

var _constants = __webpack_require__(2);

var _styleUtils = __webpack_require__(21);

var styleUtils = _interopRequireWildcard(_styleUtils);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _view = __webpack_require__(11);

var _view2 = _interopRequireDefault(_view);

var _tableUtils = __webpack_require__(52);

var tableUtils = _interopRequireWildcard(_tableUtils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function SignListView(model, playerView, editable) {

    var MIN_COLUMN_WIDTH = 80;
    var COLUMN_WIDTH = { "number": 50,
        "name": 120,
        "currentName": 120,
        "signedName": 120,
        "position": 160,
        "currentPosition": 160,
        "signedPosition": 160,
        "date": 120,
        "signType": 100,
        "comment": 160,
        "typeSign": 100,
        "result": 100
    };

    _view2.default.call(this, this, model, playerView);

    var instance = this;

    var inited = false;

    var locale = dataUtils.getComponentLocale(model.asfProperty);

    var type = -1;

    var headers = [];

    var table = tableUtils.createTable("signsList", model.asfProperty.style);
    table.prop('border', 1);
    table.prop('cellpadding', 2);

    this.initTable = function (newHeaders) {
        headers = newHeaders;

        var commonTableWidth = 0;
        headers.forEach(function (header) {
            commonTableWidth += COLUMN_WIDTH[header.code];
        });

        var minColumnWidth = null;
        if (table.width() < commonTableWidth) {
            table.css("min-width", MIN_COLUMN_WIDTH * newHeaders.length + "px");
            minColumnWidth = MIN_COLUMN_WIDTH;
        } else {
            table.css("width", "100%");
            table.css("table-layout", "fixed");
        }

        headers.forEach(function (header) {
            var colWidth = minColumnWidth || COLUMN_WIDTH[header.code];
            var cell = null;
            if (header.code === "comment") {
                cell = tableUtils.createHeaderCell(null, null, null, true);
            } else {
                cell = tableUtils.createHeaderCell(null, null, colWidth + "px", true);
            }
            cell.html(header.name);
            table.children('thead').append(cell);
        });

        var tr = tableUtils.createRow({ columns: 1 });
        (0, _jquery2.default)(tr.children('td')[0]).html(_i18n2.default.tr("Данные заполняются автоматически в режиме просмотра и в версии для печати"));
        (0, _jquery2.default)(tr.children('td')[0]).prop("colspan", newHeaders.length);
        (0, _jquery2.default)(tr.children('td')[0]).addClass("asf-borderedCell");
        table.children('tbody').first().append(tr);

        inited = true;

        instance.loadData();
    };

    this.applySpecialStyle = function () {
        styleUtils.applyTableStyle(table, model.asfProperty.style);
    };

    this.loadData = function () {
        if (editable) {
            return;
        }
        if (!model.playerModel.asfDataId) {
            return;
        }

        if (!inited) {
            return;
        }

        api.getSigns(model.playerModel.asfDataId, type, locale, function (signs) {
            instance.setSigns(signs);
        });
    };

    this.setSigns = function (signs) {
        table.children('tbody').children().empty();
        signs.forEach(function (sign, rowNumber) {
            var tr = tableUtils.createRow({ columns: headers.length });
            tr.children("td").each(function (index, td) {
                var h = headers[index];
                if (h.code == 'number') {
                    (0, _jquery2.default)(td).html(rowNumber + 1);
                } else {
                    (0, _jquery2.default)(td).html(sign[h.code]);
                }
                (0, _jquery2.default)(td).addClass("asf-borderedCell");
                styleUtils.applyFontStyle((0, _jquery2.default)(td), model.asfProperty.style);
            });
            table.children('tbody').first().append(tr);
        });
    };

    if (model.asfProperty.config && model.asfProperty.config.fields) {
        type = model.asfProperty.config.type;
        api.getSignHeaders(model.asfProperty.config.fields, type, locale, function (newHeaders) {
            instance.initTable(newHeaders);
        });
    } else {
        var headerTitles = ["Фамилия И.О.", "Должность", "Дата", "Тип подписи", "Комментарий", "Действие", "Результат действия"];
        api.translateMultiple(headerTitles, locale, function (data) {
            var newHeaders = [];

            newHeaders.push({ number: 1, code: 'name', name: data["Фамилия И.О."] });
            newHeaders.push({ number: 2, code: 'position', name: data["Должность"] });
            newHeaders.push({ number: 3, code: 'date', name: data["Дата"] });
            newHeaders.push({ number: 4, code: 'signType', name: data["Действие"] });
            newHeaders.push({ number: 5, code: 'result', name: data["Результат действия"] });
            newHeaders.push({ number: 6, code: 'comment', name: data["Комментарий"] });
            newHeaders.push({ number: 7, code: 'typeSign', name: data["Тип подписи"] });
            instance.initTable(newHeaders);
        });
    }

    model.playerModel.on(_constants.EVENT_TYPE.dataLoad, function () {
        instance.loadData();
    });

    this.container.append(table);
};

SignListView.prototype = Object.create(_view2.default.prototype);
SignListView.prototype.constructor = SignListView;

/***/ }),

/***/ 271:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = TextAreaView;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _constants = __webpack_require__(2);

var _componentUtils = __webpack_require__(15);

var compUtils = _interopRequireWildcard(_componentUtils);

var _styleUtils = __webpack_require__(21);

var styleUtils = _interopRequireWildcard(_styleUtils);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _view = __webpack_require__(11);

var _view2 = _interopRequireDefault(_view);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Created by exile
 * Date: 03.02.16
 * Time: 16:25
 *
 * Компонент многострочный текст
 */

function TextAreaView(model, playerView) {

    _view2.default.call(this, this, model, playerView);

    var textArea = (0, _jquery2.default)('<textarea/>', { class: 'asf-textBox' });

    compUtils.initInput(model, this, textArea);
    dataUtils.addAsFormId(textArea, "input", model.asfProperty);

    if (model.asfProperty.config['add-space']) {
        textArea.addClass("asf-preserveWhiteSpace");
    }

    this.unmarkInvalid = function () {
        textArea.removeClass('asf-invalidInput');
    };

    this.markInvalid = function () {
        textArea.addClass('asf-invalidInput');
    };

    this.updateValueFromModel = function () {
        if (textArea.text() == model.getTextValue()) {
            return;
        }
        if (!model.getTextValue()) {
            textArea.text("");
        } else {
            textArea.text(model.getTextValue());
        }

        resize();
    };

    this.applySpecialStyle = function () {
        styleUtils.applyLabelStyle(textArea, model.asfProperty.style);
    };

    this.setEnabled = function (editable) {
        if (editable) {
            textArea.removeProp("readonly");
            textArea.removeClass("asf-disabledInput");
        } else {
            textArea.prop("readonly", "readonly");
            textArea.addClass("asf-disabledInput");
        }
    };

    function resize() {
        textArea.css({ 'height': 'auto', 'overflow-y': 'hidden' });
        textArea.height(textArea[0].scrollHeight - 4);
        model.playerModel.trigger(_constants.EVENT_TYPE.cellSizeChange, model);
    }

    this.setEnabled(!model.asfProperty.config['read-only']);

    this.updateValueFromModel();

    this.container.append(textArea);

    setTimeout(resize, 0);

    textArea.on('change', resize);

    textArea.on('input', resize);
};

TextAreaView.prototype = Object.create(_view2.default.prototype);
TextAreaView.prototype.constructor = TextAreaView;

/***/ }),

/***/ 272:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = TextBoxView;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _componentUtils = __webpack_require__(15);

var compUtils = _interopRequireWildcard(_componentUtils);

var _styleUtils = __webpack_require__(21);

var styleUtils = _interopRequireWildcard(_styleUtils);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _view = __webpack_require__(11);

var _view2 = _interopRequireDefault(_view);

var _maskUtils = __webpack_require__(100);

var maskUtils = _interopRequireWildcard(_maskUtils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Created by exile
 * Date: 03.02.16
 * Time: 16:25
 */
function TextBoxView(model, playerView) {

    _view2.default.call(this, this, model, playerView);

    var textBox = (0, _jquery2.default)('<input/>', { type: 'text', class: 'asf-textBox' });
    compUtils.initInput(model, this, textBox);
    dataUtils.addAsFormId(textBox, "input", model.asfProperty);

    this.unmarkInvalid = function () {
        textBox.removeClass('asf-invalidInput');
    };

    this.markInvalid = function () {
        textBox.addClass('asf-invalidInput');
    };

    this.updateValueFromModel = function () {
        if (textBox.val() == model.getTextValue()) {
            return;
        }
        textBox.val(model.getTextValue());
    };

    if (model.asfProperty.config['input-mask']) {
        maskUtils.initInput(model, textBox);
    }

    this.applySpecialStyle = function () {
        styleUtils.applyLabelStyle(textBox, model.asfProperty.style);
    };

    this.setEnabled = function (editable) {
        if (editable) {
            textBox.removeProp("readonly");
            textBox.removeClass("asf-disabledInput");
        } else {
            textBox.prop("readonly", true);
            textBox.addClass("asf-disabledInput");
        }
    };
    this.setEnabled(!model.asfProperty.config['read-only']);

    this.updateValueFromModel();

    this.container.append(textBox);
};

TextBoxView.prototype = Object.create(_view2.default.prototype);
TextBoxView.prototype.constructor = TextBoxView;

/***/ }),

/***/ 273:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = TextView;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _constants = __webpack_require__(2);

var _componentUtils = __webpack_require__(15);

var compUtils = _interopRequireWildcard(_componentUtils);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _styleUtils = __webpack_require__(21);

var styleUtils = _interopRequireWildcard(_styleUtils);

var _view = __webpack_require__(11);

var _view2 = _interopRequireDefault(_view);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function TextView(model, playerView) {
    _view2.default.call(this, this, model, playerView);

    var instance = this;

    var label = (0, _jquery2.default)('<div></div>');
    dataUtils.addAsFormId(label, "text", model.asfProperty);

    label.addClass('asf-label');

    function getAddSpace() {
        var config = model.asfProperty.config;
        if (!config) return undefined;
        return config['add-space'];
    }

    this.updateValueFromModel = function () {
        label.html(compUtils.addSimpleHtml(model.getTextValue(), getAddSpace()));
    };

    styleUtils.applyLabelStyle(label, model.asfProperty.style);

    if (model.asfProperty.style && model.asfProperty.style.maxWidth) {
        var parsedWidth = styleUtils.parseWidth(model.asfProperty.style.maxWidth);
        if (parsedWidth && parsedWidth.unit != '%') {
            label.css("max-width", parsedWidth.str);
        }
    }

    instance.updateValueFromModel();

    model.on(_constants.EVENT_TYPE.dataLoad, function () {
        instance.updateValueFromModel();
    });

    if (getAddSpace()) {
        label.addClass("asf-preserveWhiteSpace");
    }

    this.container.append(label);
};

TextView.prototype = Object.create(_view2.default.prototype);
TextView.prototype.constructor = TextView;

/***/ }),

/***/ 274:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = UserLinkView;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _services = __webpack_require__(23);

var services = _interopRequireWildcard(_services);

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

var _constants = __webpack_require__(2);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _view = __webpack_require__(11);

var _view2 = _interopRequireDefault(_view);

var _UserChooser = __webpack_require__(81);

var _TagArea = __webpack_require__(72);

var _TagArea2 = _interopRequireDefault(_TagArea);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function UserLinkView(model, playerView) {
    _view2.default.call(this, this, model, playerView);

    var config = model.asfProperty.config;

    var instance = this;

    var lastSearch = "";

    var locale = model.getLocale();

    this.showSuggestions = function (search, suggestionInput) {

        if (search === lastSearch) {
            return;
        }
        lastSearch = search;

        instance.showSuggestions(search, suggestionInput);

        api.getUsersSuggestions(search, model.getFilterPositionId(), model.getFilterDepartmentId(), config['show-without-position'], true, config['custom'], locale, function (suggestions) {
            var values = [];
            suggestions.forEach(function (s, index) {
                values.push({ value: s.personID, title: s.personName, selected: index == 0 });
            });

            services.showSuggestOracle(values, suggestionInput, 300, function (selectedValue) {
                suggestions.some(function (s) {
                    if (s.personID == selectedValue) {
                        s.tagName = s.personName;
                        instance.addValue(s);
                        return true;
                    }
                });
                suggestionInput.val("");
            });
        });
    };

    var usersTagArea = new _TagArea2.default({ width: "calc(100% - 30px)" }, 0, config['multi'], config['editable-label'], config['custom'], function (search, suggestionInput) {
        if (search.isEmpty()) {
            services.showDropDown([]);
        } else {
            instance.showSuggestions(search, suggestionInput);
        }
    }, function (randomText) {
        var object = {
            personID: _UserChooser.USER_CHOOSER_SUFFIXES.text + "-" + model.getNextTextNumber(),
            lastname: "",
            firstname: "",
            patronymic: "",
            personName: randomText,
            tagName: randomText
        };

        instance.addValue(object);
    });

    this.addValue = function (item) {
        var value = model.getValue().slice();
        value.push(item);
        model.setValueFromInput(value);
        instance.updateValueFromModel();

        usersTagArea.focus();
    };

    if (model.asfProperty.config['read-only']) {
        usersTagArea.setEditable(false);
    }

    usersTagArea.on(_constants.EVENT_TYPE.tagDelete, function () {
        model.setValueFromInput(usersTagArea.getValues());
    });

    usersTagArea.on(_constants.EVENT_TYPE.tagEdit, function () {
        model.setValueFromInput(usersTagArea.getValues());
    });

    var chooserButton = (0, _jquery2.default)("<button/>", { class: "asf-user-chooser" });

    dataUtils.addAsFormId(usersTagArea.getWidget(), "input", model.asfProperty);
    dataUtils.addAsFormId(chooserButton, "button", model.asfProperty);

    chooserButton.click(function () {
        instance.showUserChooser();
    });

    this.showUserChooser = function () {
        services.showUserChooserDialog(model.getValue(), config['multi'], config['groups'], config['show-without-position'], model.getFilterPositionId(), model.getFilterDepartmentId(), locale, function (selectedValues) {
            model.setValue(selectedValues);
        });
    };

    this.updateValueFromModel = function () {
        usersTagArea.setValues(model.getValue());
    };

    this.unmarkInvalid = function () {
        usersTagArea.markValidity(true);
    };

    this.markInvalid = function () {
        usersTagArea.markValidity(false);
    };

    this.setEnabled = function (newEnabled) {
        usersTagArea.setEditable(newEnabled);
        if (newEnabled) {
            chooserButton.css("pointer-events", "auto");
        } else {
            chooserButton.css("pointer-events", "none");
        }
    };
    this.setEnabled(!dataUtils.isReadOnly(model));

    model.on(_constants.EVENT_TYPE.dataLoad, function () {
        instance.updateValueFromModel();
    });

    this.updateValueFromModel();

    this.container.append(usersTagArea.getWidget());
    this.container.append(chooserButton);
};

UserLinkView.prototype = Object.create(_view2.default.prototype);
UserLinkView.prototype.constructor = UserLinkView;

/***/ }),

/***/ 275:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = SelectionInfo;
function SelectionInfo(tableId, row, column, view, toggle, area) {
    this.tableId = tableId;
    this.row = row;
    this.column = column;
    this.view = view;

    this.toggle = toggle;
    this.area = area;

    this.getTableId = function () {
        return this.tableId;
    };
    this.getRow = function () {
        return this.row;
    };
    this.getColumn = function () {
        return this.column;
    };
    this.getView = function () {
        return this.view;
    };
}

/***/ }),

/***/ 276:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = TableParagraphView;

var _constants = __webpack_require__(2);

var _componentUtils = __webpack_require__(15);

var compUtils = _interopRequireWildcard(_componentUtils);

var _styleUtils = __webpack_require__(21);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _view = __webpack_require__(11);

var _view2 = _interopRequireDefault(_view);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

/**
 * Отображение свертки динамической таблицы
 * @param tableModel
 * @param playerView
 * @constructor
 */
function TableParagraphView(tableModel, playerView) {
    _view2.default.call(this, this, tableModel, playerView);

    var label = compUtils.createLabel("");
    dataUtils.addAsFormId(label, "label", tableModel.asfProperty);
    if (!this.model.asfProperty.style) {
        this.model.asfProperty.style = {};
    }
    this.model.asfProperty.style.align = 'justify';
    (0, _styleUtils.applyLabelStyle)(label, this.model.asfProperty.style);
    this.container.append(label);

    var instance = this;
    tableModel.on(_constants.EVENT_TYPE.dataLoad, function () {
        instance.updateValueFromModel();
    });

    this.updateValueFromModel = function () {
        label.html(tableModel.getTextValue());
    };

    this.updateValueFromModel();
};

TableParagraphView.prototype = Object.create(_view2.default.prototype);
TableParagraphView.prototype.constructor = TableParagraphView;

/***/ }),

/***/ 277:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = TableDynamicView;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _i18n = __webpack_require__(14);

var _i18n2 = _interopRequireDefault(_i18n);

var _constants = __webpack_require__(2);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _view = __webpack_require__(11);

var _view2 = _interopRequireDefault(_view);

var _tableUtils = __webpack_require__(52);

var tableUtils = _interopRequireWildcard(_tableUtils);

var _tableBlock = __webpack_require__(159);

var _tableBlock2 = _interopRequireDefault(_tableBlock);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * отображение динамической таблицы в режиме редактирования
 * @param tableModel модель таблицы
 * @param playerView
 * @param editable режим чтения - редактирования
 * @constructor
 */
function TableDynamicView(tableModel, playerView, editable) {
    _view2.default.call(this, this, tableModel, playerView);

    var instance = this;

    /**
     * список отображений
     */
    var viewBlocks = [];
    /**
     * отображения заголовка
     */
    var headerViewBlock = new _tableBlock2.default(tableModel, tableModel.headerModelBlock, playerView, editable, true, []);

    /**
     * можно ли редактировать таблицу, добавлять удалять блоки
     * @type {boolean}
     */
    this.enabled = true;

    /**
     * невидимые колонки
     * @type {Array<integer>}
     */
    var invisibleColumns = [];

    /**
     * объединенные ячейки
     * @type {Array<Object{x, y, rows, cols}>}
     */
    var mergedCells = [];

    /**
     * iterate all table views
     * @param handler - функция, в нее передаются 2 параметра,
     * model - модель,
     * blockNumber - номер блока
     */
    var iterateAllViews = function iterateAllViews(handler) {
        viewBlocks.forEach(function (viewBlock, blockNumber) {
            viewBlock.views.forEach(function (view) {
                return handler(view, blockNumber);
            });
        });
    };

    this.getViewBlocks = function () {
        return viewBlocks;
    };

    this.getAllViews = function (tableId, tableBlockIndex) {
        var allViews = [];
        iterateAllViews(function (view) {
            if (tableBlockIndex) {
                // если запрашивают компонент из блока динамической таблицы, то мы добавляем все таблицы и
                // все компоненты с этим номером блока , если идентификатор текущей таблицы равен tableId

                if (view.model.asfProperty.type === _constants.WIDGET_TYPE.table || instance.model.asfProperty.id === tableId && view.model.asfProperty.tableBlockIndex == tableBlockIndex) {
                    allViews.push(view);
                }
            } else {
                allViews.push(view);
            }
        });
        return allViews;
    };

    /**
     * получение отображения по идентификатору таблицы владельца и номеру блока
     * @param cmpId идентификатор компонента
     * @param tableId идентификатор таблицы владельца
     * @param tableBlockIndex идентификатор блока
     * @returns {Object extends View}
     */
    this.getViewWithId = function (cmpId, tableId, tableBlockIndex) {
        var allViews = this.getAllViews(tableId, tableBlockIndex);
        return tableUtils.getViewWithId(cmpId, allViews, tableId, tableBlockIndex);
    };

    this.getViewsWithType = function (type, tableId, blockIndex) {
        var allViews = this.getAllViews(tableId, blockIndex);
        return tableUtils.getViewsWithType(type, allViews, tableId, blockIndex);
    };

    /**
     * позволять ли редактировать количество блоков динамической таблицы
     * @param {boolean} newEnabled
     */
    this.setEnabled = function (newEnabled) {
        instance.enabled = newEnabled;
        if (newEnabled) {
            addRowButton.show();
        } else {
            addRowButton.hide();
        }
        if (newEnabled) {
            tableModel.trigger(_constants.TABLE_EVENT_TYPE.ENABLED);
        } else {
            tableModel.trigger(_constants.TABLE_EVENT_TYPE.DISABLED);
        }
    };

    /**
     * сделать колонку не видимой
     * @param {integer} column  номер колонки
     * @param {boolean} visible отображается или нет
     */
    this.setColumnVisible = function (column, visible) {
        if (visible) {
            invisibleColumns.remove(column);
            tableModel.trigger(_constants.TABLE_EVENT_TYPE.COLUMN_SHOW, [column]);
        } else {
            invisibleColumns.pushUnique(column);
            tableModel.trigger(_constants.TABLE_EVENT_TYPE.COLUMN_HIDE, [column]);
        }
    };

    /**
     * объединение ячеек
     * @param {int} row  номер ряда
     * @param {int} column номер колонки
     * @param {int} rows количество рядов
     * @param {int} cols количество колонок
     */
    this.mergeCell = function (row, column, rows, cols) {
        var mergedCell = { row: row, column: column, rows: rows, cols: cols };
        mergedCells.push(mergedCell);
        tableModel.trigger(_constants.TABLE_EVENT_TYPE.MERGE_CELL, [mergedCell.row, mergedCell.column, mergedCell.rows, mergedCell.cols]);
    };

    /**
     * объединение ячеек
     * @param {int} row  номер р��да
     * @param {int} column номер колонки
     */
    this.splitCell = function (row, column) {
        var newMergedCells = [];
        mergedCells.forEach(function (mergedCell) {
            if (mergedCell.row !== row || mergedCell.column !== column) {
                newMergedCells.push(mergedCell);
            } else {
                tableModel.trigger(_constants.TABLE_EVENT_TYPE.SPLIT_CELL, [mergedCell.row, mergedCell.column, mergedCell.rows, mergedCell.cols]);
            }
        });
        mergedCells = newMergedCells;
    };

    this.getRowsCount = function () {
        return table.children("tbody").first().children("tr").length;
    };

    this.getBlocksCount = function () {
        return viewBlocks.length;
    };

    this.executeSelfScript = this.executeScript;

    this.executeScript = function (view, model, editable) {
        instance.executeSelfScript(view, model, editable);

        instance.getViewBlocks().forEach(function (viewBlock) {
            viewBlock.executeScript();
        });
    };

    // подписываемся на события модели
    tableModel.on(_constants.EVENT_TYPE.tableRowAddInternal, function (event, model, modelRow) {
        var viewBlock = new _tableBlock2.default(model, modelRow, playerView, editable, false, []);
        viewBlocks.push(viewBlock);
        table.children('tbody').first().append(viewBlock.rows);
        invisibleColumns.forEach(function (column) {
            tableModel.trigger(_constants.TABLE_EVENT_TYPE.COLUMN_HIDE, [column]);
        });
        mergedCells.forEach(function (mergedCell) {
            tableModel.trigger(_constants.TABLE_EVENT_TYPE.MERGE_CELL, [mergedCell.row, mergedCell.column, mergedCell.rows, mergedCell.cols]);
        });

        viewBlock.executeScript();

        tableModel.playerModel.trigger(_constants.EVENT_TYPE.tableRowAdded, [model, this]);
    });

    tableModel.on(_constants.EVENT_TYPE.tableRowDelete, function (event, model, index) {
        var deletingBlock = viewBlocks.splice(index, 1);

        if (deletingBlock) {
            deletingBlock.forEach(function (block) {
                tableModel.trigger(_constants.TABLE_EVENT_TYPE.BLOCK_DELETED, [block]);
            });
        }
    });

    var table = tableUtils.createASFTable(tableModel, playerView);

    table.children('tbody').first().append(headerViewBlock.rows);

    tableModel.modelBlocks.forEach(function (models) {
        viewBlocks.push(new _tableBlock2.default(tableModel, models, playerView, editable, false, []));
    });

    viewBlocks.forEach(function (viewBlock) {
        table.children('tbody').first().append(viewBlock.rows);
    });

    var addRowButton = (0, _jquery2.default)('<div/>', { class: "asf-add-button" }).html(_i18n2.default.tr("+ Добавить бло��"));
    dataUtils.addAsFormId(addRowButton, "addRowButton", tableModel.asfProperty);
    addRowButton.click(function () {
        tableModel.createRow();
    });

    instance.container.append(table);
    if (editable) {
        instance.container.append(addRowButton);
    }
};

TableDynamicView.prototype = Object.create(_view2.default.prototype);
TableDynamicView.prototype.constructor = TableDynamicView;

/***/ }),

/***/ 278:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * кеш справочников
 */


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = DictionaryCache;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _apiUtils = __webpack_require__(8);

var _constants = __webpack_require__(2);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function DictionaryCache() {

    var instance = this;

    this.dictionariesCache = (0, _jquery2.default)({});
    this.activeDictionaries = [];
    this.loadDictionary = function (dictionaryCode, locale) {
        if (instance.activeDictionaries.indexOf(dictionaryCode + "_" + locale) != -1) {
            return;
        }

        instance.activeDictionaries.push(dictionaryCode + "_" + locale);

        (0, _apiUtils.loadDictionary)(dictionaryCode, locale, function (loadedData) {
            if (loadedData && loadedData.dictionary_code) {
                instance.parseDictionary(dictionaryCode, locale, loadedData);
            } else {
                instance.dictionariesCache.trigger(_constants.EVENT_TYPE.dictionaryLoad, [dictionaryCode, locale, null]);
            }
        }, function () {
            // в случае ошибки получения значений справочников вот что делаем
            instance.dictionariesCache.trigger(_constants.EVENT_TYPE.dictionaryLoad, [dictionaryCode, locale, null]);
        });
    };

    this.getDictionary = function (dictionaryCode, locale, handler) {
        if (instance.dictionariesCache[dictionaryCode + "_" + locale]) {
            handler(instance.dictionariesCache[dictionaryCode + "_" + locale]);
            return;
        }

        var loadedHandler = function loadedHandler(event, loadedDictionaryCode, loadedLocale, dictionaryData) {
            if (dictionaryCode == loadedDictionaryCode && locale == loadedLocale) {
                handler(dictionaryData);
            }
        };
        instance.dictionariesCache.on(_constants.EVENT_TYPE.dictionaryLoad, loadedHandler);
    };
};

DictionaryCache.prototype.parseDictionary = function (dictionaryCode, locale, loadedData) {
    var dictionaryData = [];
    var columns = {};
    loadedData.columns.forEach(function (col, index) {
        columns[col.columnID] = col.code;
    });
    loadedData.items.forEach(function (item) {
        var newItem = { itemID: item.itemID };
        item.values.forEach(function (value) {
            if (value.translation) {
                newItem[columns[value.columnID]] = value.translation;
            } else {
                newItem[columns[value.columnID]] = value.value;
            }
        });
        dictionaryData.push(newItem);
    });

    this.dictionariesCache[dictionaryCode + "_" + locale] = dictionaryData;
    this.dictionariesCache.trigger(_constants.EVENT_TYPE.dictionaryLoad, [dictionaryCode, locale, dictionaryData]);
};

/***/ }),

/***/ 279:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _backbone = __webpack_require__(43);

var _backbone2 = _interopRequireDefault(_backbone);

var _underscore = __webpack_require__(7);

var _underscore2 = _interopRequireDefault(_underscore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var DialogLayout = _backbone2.default.LayoutView.extend({
    template: '#dialog-component-template',
    regions: {
        dialogTitle: 'header h5'
    },
    ui: {
        dialogTitle: 'h5',
        submitButton: 'footer button',
        closeButton: '.close-btn'
    },
    events: {
        'click @ui.submitButton': 'onSubmitButtonClick',
        'click @ui.closeButton': 'onCloseButtonClick'
    },
    title: '',
    initialize: function initialize(options) {
        if (!_underscore2.default.isUndefined(options)) {
            this.title = options.title;
        }
    },
    onSubmitButtonClick: function onSubmitButtonClick(event) {
        this.triggerMethod('submit:clicked');
    },
    onCloseButtonClick: function onCloseButtonClick(event) {
        this.triggerMethod('close:clicked');
        this.destroy();
    },
    setTitle: function setTitle(title) {
        this.ui.dialogTitle.html(title);
    },

    setEnabledButton: function setEnabledButton(enabled) {
        this.ui.submitButton.attr('disabled', !enabled);
    }
});
exports.default = DialogLayout;

/***/ }),

/***/ 280:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _backbone = __webpack_require__(43);

var _backbone2 = _interopRequireDefault(_backbone);

var _underscore = __webpack_require__(7);

var _underscore2 = _interopRequireDefault(_underscore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _backbone2.default.ItemView.extend({
    className: 'link-component',
    events: {
        'click': 'onClickEvent'
    },
    modelEvents: {
        'change': 'render'
    },
    onClickEvent: function onClickEvent(event) {
        this.triggerMethod('link:clicked');
    },
    getTemplate: function getTemplate() {
        return _underscore2.default.template('<a href="#" ><%= value %></a>');
    },
    setSelectedModel: function setSelectedModel(model) {
        this.model = model;
        this.setValue(model.getValue());
    },
    setValue: function setValue(value) {
        this.model.set('value', value);
        this.render();
    }
});

/***/ }),

/***/ 281:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _backbone = __webpack_require__(43);

var _backbone2 = _interopRequireDefault(_backbone);

var _underscore = __webpack_require__(7);

var _underscore2 = _interopRequireDefault(_underscore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Компонент для поиска текста, который содержит в себе текстовое поле и иконку поиска.
 * При нажатии на кнопку enter внутри компонента или при нажатии на иконку поиска, компонент отправляет
 * слушателю значение внутри текстового поля.
 *
 * Наименование события: `searchfield:enter:pressed`
 * Передаваемый аргумент для слушателя: Значение внутри текстового поля
 * Событие срабатывает: при нажатии enter внутри текстового поля / при нажатии на иконку поиска
 * Пример использования:
 * <code>
 *   var searchField = new AS.Components.SearchField();
 searchField.listenTo(this.searchField, 'searchfield:enter:pressed', function (value) {
            // do something with value
         });
 * </code>
 *
 * Дополнительные публичные методы:
 * setSearchPlaceHolder(placeholder)
 * emptySearchField()
 *
 * @type {void|*}
 */
var SearchField = _backbone2.default.ItemView.extend({
    className: 'search-bar',
    ui: {
        searchField: 'input',
        searchButton: '.search-button-icon'
    },
    events: {
        'keypress @ui.searchField': 'onKeyPressed',
        'click @ui.searchButton': 'onSearchButtonPressed'
    },
    /**
     * Метод вызывается при печати в текстовое поле
     * @param event
     */
    onKeyPressed: function onKeyPressed(event) {
        var ENTER_KEY = 13;
        if (event.which == ENTER_KEY) {
            var value = this.ui.searchField.val().trim();
            this.triggerMethod('searchfield:enter:pressed', value);
        }
    },
    /**
     * Метод срабатывает при нажатии на иконку(кнопку) поиска.
     * Передает слушателю значение внутри текстового поля
     */
    onSearchButtonPressed: function onSearchButtonPressed() {
        var value = this.ui.searchField.val().trim();
        this.triggerMethod('searchfield:enter:pressed', value);
    },

    /**
     * Устанавливает значаение для плейсхолдера текстового поля
     * @param placeholder текст плейсхолдера
     */
    setSearchPlaceHolder: function setSearchPlaceHolder(placeholder) {
        this.ui.searchField.attr("placeholder", placeholder);
    },

    /**
     * Делает значение текстового поля пустым
     */
    emptySearchField: function emptySearchField() {
        this.ui.searchField.val('');
    },
    /**
     * Шаблон для компонента
     */
    getTemplate: function getTemplate() {
        return _underscore2.default.template('<input type="text" class="search-field" placeholder="<%=i18n.tr(\'Поиск записей\')%>" />' + '<div class="search-button-icon" />');
    }
});

exports.default = SearchField;

/***/ }),

/***/ 282:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _backbone = __webpack_require__(43);

var _backbone2 = _interopRequireDefault(_backbone);

var _underscore = __webpack_require__(7);

var _underscore2 = _interopRequireDefault(_underscore);

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Компонент для выборки.
 * Есть 2 события которые могут переданы для слушателя.
 *
 * Наименование события: `select:item:clicked`
 * Передаваемый аргумент для слушателя: модель который был выбран
 * Событие срабатывает: при нажатии на элемента выборки
 * Пример использования:
 * <code>
 var collection = new AS.Entities.SelectCollection([
 new SelectModel({key: '0', value: 'Пользователи', selected: true}),
 new SelectModel({key: '1', value: 'Организации'})
 ]);
 var selectComponent = new AS.Components.Select({collection: collection});
 selectComponent.listenTo(this.selectComponent, 'select:item:clicked', function (model) {
            //do something with clicked model
        });
 * </code>
 *
 * Наименование события: `select:item:changed`
 * Передаваемый аргумент для слушателя: модель который был выбран
 * Событие срабатывает: Срабатывает при изменении значения выборки.
 * Пример использования:
 * <code>
 var collection = new AS.Entities.SelectCollection([
 new SelectModel({key: '0', value: 'Пользователи', selected: true}),
 new SelectModel({key: '1', value: 'Организации'})
 ]);
 var selectComponent = new AS.Components.Select({collection: collection});
 selectComponent.listenTo(this.selectComponent, 'select:item:changed', function (model) {
                //do something with clicked model
        });
 * </code>
 *
 *
 * Дополнительные публичные методы:
 * getSelectedModel()
 *
 * @type {void|*}
 */

var Option = _backbone2.default.ItemView.extend({
    tagName: 'div',
    className: 'sel-option',
    getTemplate: function getTemplate() {
        return _underscore2.default.template('<%= value %>');
    },
    collectionEvents: {
        "add": "render",
        "change": "render",
        'remove': 'render'
    },
    events: {
        'click': 'selectItemClicked'
    },
    selectItemClicked: function selectItemClicked(event) {
        event.stopPropagation();
        var model = this.model;
        var txtBlock = (0, _jquery2.default)('.sel-txt');
        txtBlock.text(model.get('value'));
        txtBlock.addClass('selected').siblings('div').removeClass('selected');
        (0, _jquery2.default)('.options').hide();
        this.triggerMethod('select:item:changed', this.model);
    },
    attributes: function attributes() {
        var attrs = {
            value: this.model.get('key')
        };
        if (this.model.get('selected')) {
            attrs.selected = 'selected';
        }
        return attrs;
    }
});

exports.default = _backbone2.default.CompositeView.extend({
    tagName: 'div',
    className: 'sel',
    childView: Option,
    childViewContainer: '.options',
    events: {
        'click': 'selectItemClicked'
    },

    collectionEvents: {
        "add": "render",
        "change": "render",
        'remove': 'render'
    },
    /**
     * Срабатывает при нажатии на элемент выборки
     * @param event
     */
    selectItemClicked: function selectItemClicked(event) {
        event.stopPropagation();
        (0, _jquery2.default)('.options').width((0, _jquery2.default)('.field-select').width() - 2).show();
    },
    serializeData: function serializeData() {
        var value = '';
        _underscore2.default.forEach(this.collection.models, function (model) {
            if (model.get('selected')) {
                value = model.get('value');
            }
        });
        return { value: value };
    },

    initialize: function initialize() {
        var self = this;
        (0, _jquery2.default)('body').bind('click', function (e) {
            (0, _jquery2.default)('.options').hide();
        });
        this.on('childview:select:item:changed', function (view, model) {
            var selectedKey = model.get('key');
            var selectedModel = self.collection.findWhere({ key: selectedKey });
            _underscore2.default.forEach(self.collection.where({ selected: true }), function (model) {
                model.set('selected', false);
            });
            selectedModel.set('selected', true);
        }, this);
    },

    /**
     * Возвращает выбранную модель
     * @returns {*}
     */
    getSelectedModel: function getSelectedModel() {
        return this.collection.findWhere({ selected: true });
    },
    getModels: function getModels() {
        return this.collection;
    },
    getTemplate: function getTemplate() {
        return _underscore2.default.template('<div class="field-select">' + '<div class="sel-txt"><%= value %></div>' + '<div class="sel-button"></div>' + '</div>' + '<div class="options hide"></div>');
    }
});

/***/ }),

/***/ 283:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.DataCollection = exports.DataModel = undefined;

var _backbone = __webpack_require__(39);

var _backbone2 = _interopRequireDefault(_backbone);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var DataModel = exports.DataModel = _backbone2.default.Model.extend({
    defaults: {
        'value': '',
        'selected': false
    },
    isSelected: function isSelected() {
        return this.get('selected');
    },
    setSelected: function setSelected(selected) {
        this.set('selected', selected);
    },
    getValue: function getValue() {
        return this.get('value');
    },
    setValue: function setValue(value) {
        this.set('value', value);
    }
});

var DataCollection = exports.DataCollection = _backbone2.default.Collection.extend({
    model: DataModel
});

/***/ }),

/***/ 284:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _dialog_layout = __webpack_require__(279);

var _dialog_layout2 = _interopRequireDefault(_dialog_layout);

var _search = __webpack_require__(281);

var _search2 = _interopRequireDefault(_search);

var _select = __webpack_require__(165);

var _paginator = __webpack_require__(162);

var _paginator2 = _interopRequireDefault(_paginator);

var _paginator3 = __webpack_require__(164);

var _paginator4 = _interopRequireDefault(_paginator3);

var _ExtendedTable = __webpack_require__(113);

var _AddressBookTable = __webpack_require__(213);

var _AddressBookTable2 = _interopRequireDefault(_AddressBookTable);

var _addresslink_select_component = __webpack_require__(285);

var _addresslink_select_component2 = _interopRequireDefault(_addresslink_select_component);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AddressLinkDialogLayout = _dialog_layout2.default.extend({
    regions: {
        'selectContainer': '#select-container',
        'searchContainer': '#search-container',
        'paginatorContainer': '#paginator-container',
        'tableContainer': '#table-container'
    },
    /**
     * Запрос для поиска
     */
    searchQuery: '',
    /**
     * Выбранная модель в диалоговом окне
     */
    selectedModel: null,
    role: 'users',
    /**
     * Инициализация компонентов
     */
    initialize: function initialize(options) {
        this.initSelectComponent();
        this.initSearchField();
        this.initPaginator();
        this.initTableView();
        this.on('submit:clicked', this.onSubmitButtonClicked, this);
    },
    /**
     * Инициализирует компонент выборки.
     */
    initSelectComponent: function initSelectComponent() {
        this.selectComponent = new _addresslink_select_component2.default();

        if (this.selectComponent.getModels().isEmpty()) {
            this.selectComponent.getModels().add(new _select.SelectModel({
                key: 'users',
                value: i18n.tr("Люди"),
                selected: true
            }));
            this.selectComponent.getModels().add(new _select.SelectModel({
                key: 'organizations',
                value: i18n.tr("Организации")
            }));
        }

        this.selectComponent.getModels().forEach(function (model) {
            model.set("selected", 'users' == model.get('key'));
        });
        this.selectComponent.on('childview:select:item:changed', this.onSelectItemChanged, this);
    },

    /**
     * Инициализация компонента поиска
     */
    initSearchField: function initSearchField() {
        this.searchField = new _search2.default();
        this.searchField.on('searchfield:enter:pressed', this.onSearchPressed, this);
    },

    /**
     * Инициализация пагинатора
     */
    initPaginator: function initPaginator() {
        this.paginatorComponent = new _paginator2.default({
            model: new _paginator4.default({
                currentPage: 1,
                totalPage: 1
            })
        });
        this.paginatorComponent.on('paginator:page:changed', this.onPaginatorChanged, this);
    },

    /**
     * Инициализируем таблицу для записей пользователей
     */
    initTableView: function initTableView() {
        this.tableComponent = new _AddressBookTable2.default();

        var paginatorComponent = this.paginatorComponent;
        var tableComponent = this.tableComponent;

        this.tableComponent.on(_ExtendedTable.ExtendedTableEvent.dataLoaded, function () {
            paginatorComponent.setCurrentPage(tableComponent.getCurrentPage() + 1);
            var totalPage = tableComponent.getPagesCount();
            if (isNaN(totalPage)) {
                totalPage = 0;
            }
            console.log(totalPage);
            if (totalPage === 0) {
                paginatorComponent.setCurrentPage(0);
            }
            paginatorComponent.setTotalPage(totalPage);
        });

        var instance = this;
        this.tableComponent.on(_ExtendedTable.ExtendedTableEvent.rowSelected, function (evt, rowIndex, data) {
            instance.setSelectedModel(data);
        });

        this.tableComponent.dblclick(function () {
            if (instance.selectedModel) {
                instance.triggerMethod('addresslink:submit:clicked', instance.selectedModel);
            }
        });
    },

    /**
     * Задает выбранную модель для диалогового окна. Дизейблит кнопку выбрать при необходимости
     * @param model
     */
    setSelectedModel: function setSelectedModel(model) {
        this.selectedModel = model;
        this.setEnabledButton(!_.isNull(model));
    },

    onSelectItemChanged: function onSelectItemChanged(view, model) {
        this.searchQuery = '';
        this.searchField.emptySearchField();
        this.setSelectedModel(null);

        switch (model.get('key')) {
            case 'users':
                this.tableComponent.showHuman();
                break;
            case 'organizations':
                this.tableComponent.showOrganization();
                break;
        }
    },

    onSearchPressed: function onSearchPressed(searchText) {
        this.searchQuery = searchText;
        this.tableComponent.search(searchText);
    },

    onPaginatorChanged: function onPaginatorChanged(page) {
        this.tableComponent.loadData(page - 1);
    },

    onSubmitButtonClicked: function onSubmitButtonClicked() {
        // TODO
        this.triggerMethod('addresslink:submit:clicked', this.selectedModel);
    },

    fetchPageResult: function fetchPageResult(page) {},
    /**
     * Задаем регионы для компонентов
     */
    onRender: function onRender() {
        this.showChildView('selectContainer', this.selectComponent);
        this.showChildView('searchContainer', this.searchField);
        this.showChildView('paginatorContainer', this.paginatorComponent);
        var ItemView = Backbone.Marionette.ItemView.extend({ template: '<span></span>', model: new Backbone.Model() });
        var view = new ItemView();

        this.showChildView('tableContainer', view);

        var region = this.getRegion('tableContainer');
        region.$el.append(this.tableComponent.getWidget());
    },

    /**
     * Обновляет данные о страницах пагинатора
     */
    updatePaginatorNumbers: function updatePaginatorNumbers(currentPage, totalPage) {
        this.paginatorComponent.setCurrentPage(currentPage);
        this.paginatorComponent.setTotalPage(totalPage);
    },

    /**
     * Шаблон для диалогового окна
     */
    getTemplate: function getTemplate() {
        return _.template('<main>' + '<div class="data-wrapper">' + '<div class="control-panel">' + '<span id="select-container"></span>' + '<span id="search-container"></span>' + '<span id="paginator-container"></span>' + '</div>' + '<div id="table-container" class="registryTableContainerClass">' + '</div>' + '</div>' + '</main>' + '<footer>' + '<button disabled class="button button-success">' + i18n.tr("Выбрать") + '</button>' + '</footer>');
    }
});
exports.default = AddressLinkDialogLayout;

/***/ }),

/***/ 285:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _select = __webpack_require__(282);

var _select2 = _interopRequireDefault(_select);

var _select3 = __webpack_require__(165);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

exports.default = _select2.default.extend({
    collection: new _select3.SelectCollection([])
});

/***/ }),

/***/ 286:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.UserTableView = exports.UserRowView = undefined;

var _backbone = __webpack_require__(43);

var _backbone2 = _interopRequireDefault(_backbone);

var _underscore = __webpack_require__(7);

var _underscore2 = _interopRequireDefault(_underscore);

var _i18n = __webpack_require__(14);

var _i18n2 = _interopRequireDefault(_i18n);

var _addresslink_models = __webpack_require__(166);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * ItemView одного ряда для таблицы Адресной книги с пользователями
 * @type {void|*}
 */
var UserRowView = exports.UserRowView = _backbone2.default.ItemView.extend({
    tagName: "tr",
    events: {
        click: 'tableRowClicked'
    },
    /**
     * Метод срабатывает при нажатии на элемент одного ряда
     * @param event
     */
    tableRowClicked: function tableRowClicked(event) {
        this.$el.siblings().removeClass('selected');
        if (this.$el.hasClass('selected')) {
            this.triggerMethod('tablerow:selected', null);
        } else {
            this.triggerMethod('tablerow:selected', this.model);
        }
        this.$el.toggleClass('selected');
    },
    /**
     * Шаблон для одного ряда
     */
    getTemplate: function getTemplate() {
        return _underscore2.default.template('<td><%= lastName %></td>' + '<td><%= firstName %></td>' + '<td><%= patronymic %></td>' + '<td><%= organization %></td>');
    }
});

/**
 *
 * @type {void|*}
 */
var UserTableView = exports.UserTableView = _backbone2.default.CompositeView.extend({
    childView: UserRowView,
    childViewContainer: "tbody",
    className: 'addresslink-table',
    initializeCollection: function initializeCollection() {
        this.collection = new _addresslink_models.Users();
    },
    fetchPage: function fetchPage(page, searchQuery, callback) {
        this.collection.fetchPage(page, searchQuery, function (collection) {
            !_underscore2.default.isUndefined(callback) && callback(collection.page, collection.totalPage, collection);
        });
    },
    getTemplate: function getTemplate() {
        return _underscore2.default.template('<table width="100%">' + '<thead>' + '<tr>' + '<th width="150px">' + _i18n2.default.tr("Фамилия") + '</th>' + '<th width="150px">' + _i18n2.default.tr("Имя") + '</th>' + '<th width="150px">' + _i18n2.default.tr("Отчество") + '</th>' + '<th width="330px">' + _i18n2.default.tr("Организация") + '</th>' + '</tr>' + '</thead>' + '<tbody></tbody>' + '</table>');
    }
});

/***/ }),

/***/ 379:
/***/ (function(module, exports) {

module.exports = "data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAFYAAABECAYAAAAbSMJnAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAABiBJREFUeNrsnEtsElsYx/88fERvGuzCEJuwsKm10Whc2cTdTXN33dy40pg0Lt11d417vXuXaqLLWgcKDFIeItiS0spLtHqFSiAVnA4l0FokMDDcxbU3fcIMDC85v+VwOCW/nvnO+b5zZmQTExMAMATgbwBjAPpAqIdNAHYAfwGIKAEMA/AAUBE3DdEH4E8AvwMYlQO4T6RKigrAffnP258gLX8o98bUu3fvQqlUEjUiKJVKePDgwc5Lv+0zqFAocPbsWWJLBF++fNl3TU60NAcilojtQbGVSoWY3EPd0//KygrcbjfC4TDy+TyUSiU0Gg2uXbuGCxcu4OjRo0SsGPL5PLRaLex2+77PUqkUfD4fLl26hJs3b+L06dMkFAiB4zg8ffr0QKk7CYVCePjwIRiGIWKF8OrVKywtLQlq+/XrV8zMzKBcLhOxtUKAzWYT1bnP50MkEiFiq/HhwwdsbGyITvXC4TARW41kMlnXbZ1MJsHzPBFbbfTVA8dxRGw1VCoVZDKZ6D9w6tQpKBQKIvYwzp8/X9eif3BwsK5/SM+IHRgYwMWLF0V1PjAwgJGRETJ51eL69evo6xO21yiTyTA+Pg6VSkXE1kKtVuPOnTs1U9Vjx47hxo0buHr1KqkVCGV4eBiTk5Ow2WzweDz48ePHrlF65coVjI2NtSUEcBwHuVzeEZNlXdUttVqNW7duYXx8HAzDIJPJ4OTJk1Cr1ejv72/5nlmpVEIoFEIwGER/fz9GR0ehVqu7T+zOJVi7YyjP87DZbJienv6/Lry4uIjbt29jaGioO2JsJxIMBqHX63cV2xmGwbNnz7C+vk7E1kM6nQZFUSgUCvs+SyQSMBqNbcv6ulZspVKByWRCMpk8tM3CwgK8Xi8RK4a3b99ifn6+5iqBoiiwLEvECiGVSoGiKHAcV7Mty7KgKKrlG57ybgwBL168EDUKvV4vHA4HEVsNh8MBv98veklmsVgQi8WI2IOIx+OwWq111YZTqRQMBgOKxSIRu5NisQiDwdDQRBQIBOBwOFoSb7tGrMvlEh0CDsJisRx4OrAnxUajUZhMJkn6ymaz0Ol0yOVyvS12++SN2B3ianz8+LHmoZNGaWoZKh6PY2lpCeVyGZcvX66rlGi1WrG8vCz5b7PZbBgZGcG5c+e6a8SGw2E8evQIL1++hMViwZMnT+B0OkX18fnzZ9GHRISSy+UwNTWFzc3N7hGbTqcxNTWFRCKx69rz58/x/v17wbFwenq6qbEwGo3CbDZ3h9hKpQKz2YxoNHpgvKQoSlC8tNvtLZm9XS4XAoFA54v1eDx48+ZN1bhrNBqrriWDwWDLUtB8Pg+DwYBUKtW5YlmWhdlsrlkcmZ+fP7Scl06nQdM08vl8y1YesVgMFotF0tqtZGJLpRJMJhNWV1drti0UCtBqtftGCc/zmJ2dbUkI2IvT6ZQkAZFc7OLiYtUQsBeGYaDT6Xbl/X6/v+VVqG3K5TIoisLa2lrniP327Rt0Op3o73m9Xng8HgDA+vo6KIpq6wE6hmGg1+sl+Q0Niy0Wi6AoCul0WvR3OY4DTdOIxWKgabojjtYvLCzA7Xa3P/Oam5trKDatra3h8ePHkt2CUqDX6zE4OIgzZ860Z8Surq6CpumGy3CJRKLu87fNSnAO2/1tuthcLgetVotsNotfkWAwWHOzsili3W43QqEQflV4nofJZEI8Hm+d2JWVlbYehmgVmUym7tqtaLHb9dGtrS30Au/evRNdlRMtdrvA8unTJ/QSVqtV9PNqosQuLy83vfLeiWxuboouYcrFdE5RVEuLI51EJBIBTdPSit0+gNbKAw+dyOvXrwWvhASJDQQCcLlc6HUKhQJ0Op2gtXtNsZlMBjMzMw1lIb8SsVgMs7OzNbPNqmJLpRKMRqOgGmsv4XQ64fP5qrapWoTx+/2Ym5uDQqHoyacLq4UEo9EIjUZz6KNZVcVqNBpMTk7iyJEjxOaedJfneZw4cUL4iC2Xy7t2WI8fP05M7mH7OTKWZcGy7IGvG5BNTExsgLwzVmq25PjvZbIEibNgOYB7ALLEhWRkAdyTA/gHwCgALYDvxEvdfP/pcBTAP/8OAH1oI9vCujeHAAAAAElFTkSuQmCC"

/***/ }),

/***/ 380:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _backbone = __webpack_require__(43);

var _backbone2 = _interopRequireDefault(_backbone);

var _services = __webpack_require__(23);

var services = _interopRequireWildcard(_services);

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

var _logger = __webpack_require__(101);

var LOGGER = _interopRequireWildcard(_logger);

var _constants = __webpack_require__(2);

var _model = __webpack_require__(25);

var _model2 = _interopRequireDefault(_model);

var _componentUtils = __webpack_require__(15);

var compUtils = _interopRequireWildcard(_componentUtils);

var _styleUtils = __webpack_require__(21);

var styleUtils = _interopRequireWildcard(_styleUtils);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _view = __webpack_require__(11);

var _view2 = _interopRequireDefault(_view);

var _utils = __webpack_require__(82);

var utils = _interopRequireWildcard(_utils);

var _layoutUtils = __webpack_require__(65);

var LayoutUtils = _interopRequireWildcard(_layoutUtils);

var _tableUtils = __webpack_require__(52);

var tableUtils = _interopRequireWildcard(_tableUtils);

var _dateUtils = __webpack_require__(73);

var dateUtils = _interopRequireWildcard(_dateUtils);

var _caretUtils = __webpack_require__(127);

var caretUtils = _interopRequireWildcard(_caretUtils);

var _player = __webpack_require__(243);

var player = _interopRequireWildcard(_player);

var _dictCache = __webpack_require__(278);

var _dictCache2 = _interopRequireDefault(_dictCache);

var _builderUtils = __webpack_require__(156);

var BuilderUtils = _interopRequireWildcard(_builderUtils);

var _History = __webpack_require__(229);

var _History2 = _interopRequireDefault(_History);

var _domUtils = __webpack_require__(44);

var domUtils = _interopRequireWildcard(_domUtils);

var _maskUtils = __webpack_require__(100);

var maskUtils = _interopRequireWildcard(_maskUtils);

var _ComboBox = __webpack_require__(216);

var _ComboBox2 = _interopRequireDefault(_ComboBox);

var _Button = __webpack_require__(215);

var _Button2 = _interopRequireDefault(_Button);

var _BuilderToolBar = __webpack_require__(214);

var _BuilderToolBar2 = _interopRequireDefault(_BuilderToolBar);

var _UserChooser = __webpack_require__(81);

var _UserChooser2 = _interopRequireDefault(_UserChooser);

var _AddressBookTable = __webpack_require__(213);

var _AddressBookTable2 = _interopRequireDefault(_AddressBookTable);

var _SplitPane = __webpack_require__(219);

var _SplitPane2 = _interopRequireDefault(_SplitPane);

var _BasicChooserDialog = __webpack_require__(71);

var _BasicChooserDialog2 = _interopRequireDefault(_BasicChooserDialog);

var _TagArea = __webpack_require__(72);

var _TagArea2 = _interopRequireDefault(_TagArea);

var _ExtendedTable = __webpack_require__(113);

var _ExtendedTable2 = _interopRequireDefault(_ExtendedTable);

var _PopupPane = __webpack_require__(128);

var _PopupPane2 = _interopRequireDefault(_PopupPane);

var _CalendarChooser = __webpack_require__(388);

var _CalendarChooser2 = _interopRequireDefault(_CalendarChooser);

var _DropDownMenu = __webpack_require__(217);

var _DropDownMenu2 = _interopRequireDefault(_DropDownMenu);

var _DaysChooser = __webpack_require__(389);

var _DaysChooser2 = _interopRequireDefault(_DaysChooser);

var _DepartmentChooser = __webpack_require__(390);

var _DepartmentChooser2 = _interopRequireDefault(_DepartmentChooser);

var _PositionChooser = __webpack_require__(393);

var _PositionChooser2 = _interopRequireDefault(_PositionChooser);

var _LinkEditDialog = __webpack_require__(392);

var _LinkEditDialog2 = _interopRequireDefault(_LinkEditDialog);

var _FileChooser = __webpack_require__(391);

var _FileChooser2 = _interopRequireDefault(_FileChooser);

var _ProjectChooser = __webpack_require__(394);

var _ProjectChooser2 = _interopRequireDefault(_ProjectChooser);

var _RegistryTable = __webpack_require__(218);

var _RegistryTable2 = _interopRequireDefault(_RegistryTable);

var _paginator = __webpack_require__(162);

var _paginator2 = _interopRequireDefault(_paginator);

var _RegistryLinkChooser = __webpack_require__(395);

var _RegistryLinkChooser2 = _interopRequireDefault(_RegistryLinkChooser);

var _AddressLinkModel = __webpack_require__(220);

var _AddressLinkModel2 = _interopRequireDefault(_AddressLinkModel);

var _ComboModel = __webpack_require__(221);

var _ComboModel2 = _interopRequireDefault(_ComboModel);

var _CustomComponentModel = __webpack_require__(222);

var _CustomComponentModel2 = _interopRequireDefault(_CustomComponentModel);

var _DateModel = __webpack_require__(223);

var _DateModel2 = _interopRequireDefault(_DateModel);

var _DepartmentLinkModel = __webpack_require__(224);

var _DepartmentLinkModel2 = _interopRequireDefault(_DepartmentLinkModel);

var _DocAttributeModel = __webpack_require__(225);

var _DocAttributeModel2 = _interopRequireDefault(_DocAttributeModel);

var _DocLinkModel = __webpack_require__(226);

var _DocLinkModel2 = _interopRequireDefault(_DocLinkModel);

var _FileLinkModel = __webpack_require__(227);

var _FileLinkModel2 = _interopRequireDefault(_FileLinkModel);

var _FileModel = __webpack_require__(228);

var _FileModel2 = _interopRequireDefault(_FileModel);

var _ImageModel = __webpack_require__(230);

var _ImageModel2 = _interopRequireDefault(_ImageModel);

var _LabelModel = __webpack_require__(231);

var _LabelModel2 = _interopRequireDefault(_LabelModel);

var _LinkModel = __webpack_require__(232);

var _LinkModel2 = _interopRequireDefault(_LinkModel);

var _NumericInputModel = __webpack_require__(157);

var _NumericInputModel2 = _interopRequireDefault(_NumericInputModel);

var _PlayerModel = __webpack_require__(233);

var _PlayerModel2 = _interopRequireDefault(_PlayerModel);

var _PositionLinkModel = __webpack_require__(234);

var _PositionLinkModel2 = _interopRequireDefault(_PositionLinkModel);

var _ProjectLinkModel = __webpack_require__(235);

var _ProjectLinkModel2 = _interopRequireDefault(_ProjectLinkModel);

var _RegistryLinkModel = __webpack_require__(236);

var _RegistryLinkModel2 = _interopRequireDefault(_RegistryLinkModel);

var _RepeatPeriodModel = __webpack_require__(237);

var _RepeatPeriodModel2 = _interopRequireDefault(_RepeatPeriodModel);

var _selection = __webpack_require__(242);

var _selection2 = _interopRequireDefault(_selection);

var _SimpleModel = __webpack_require__(238);

var _SimpleModel2 = _interopRequireDefault(_SimpleModel);

var _UploadFileModel = __webpack_require__(398);

var _UploadFileModel2 = _interopRequireDefault(_UploadFileModel);

var _TextBoxModel = __webpack_require__(240);

var _TextBoxModel2 = _interopRequireDefault(_TextBoxModel);

var _TableModel = __webpack_require__(239);

var _TableModel2 = _interopRequireDefault(_TableModel);

var _UserLinkModel = __webpack_require__(241);

var _UserLinkModel2 = _interopRequireDefault(_UserLinkModel);

var _PlayerView = __webpack_require__(260);

var _PlayerView2 = _interopRequireDefault(_PlayerView);

var _TextView = __webpack_require__(273);

var _TextView2 = _interopRequireDefault(_TextView);

var _tableBuilderView = __webpack_require__(160);

var _tableBuilderView2 = _interopRequireDefault(_tableBuilderView);

var _tableBlock = __webpack_require__(159);

var _tableBlock2 = _interopRequireDefault(_tableBlock);

var _tableDynView = __webpack_require__(277);

var _tableDynView2 = _interopRequireDefault(_tableDynView);

var _tableStaticView = __webpack_require__(129);

var _tableStaticView2 = _interopRequireDefault(_tableStaticView);

var _paragraphView = __webpack_require__(276);

var _paragraphView2 = _interopRequireDefault(_paragraphView);

var _AddressLinkView = __webpack_require__(244);

var _AddressLinkView2 = _interopRequireDefault(_AddressLinkView);

var _BuilderComponentView = __webpack_require__(245);

var _BuilderComponentView2 = _interopRequireDefault(_BuilderComponentView);

var _CheckBoxView = __webpack_require__(246);

var _CheckBoxView2 = _interopRequireDefault(_CheckBoxView);

var _ComboBoxView = __webpack_require__(247);

var _ComboBoxView2 = _interopRequireDefault(_ComboBoxView);

var _CustomComponentView = __webpack_require__(248);

var _CustomComponentView2 = _interopRequireDefault(_CustomComponentView);

var _DateView = __webpack_require__(249);

var _DateView2 = _interopRequireDefault(_DateView);

var _DepartmentLinkView = __webpack_require__(250);

var _DepartmentLinkView2 = _interopRequireDefault(_DepartmentLinkView);

var _DocAttributeView = __webpack_require__(251);

var _DocAttributeView2 = _interopRequireDefault(_DocAttributeView);

var _DocLinkTextView = __webpack_require__(252);

var _DocLinkTextView2 = _interopRequireDefault(_DocLinkTextView);

var _DocLinkView = __webpack_require__(253);

var _DocLinkView2 = _interopRequireDefault(_DocLinkView);

var _FileLinkView = __webpack_require__(254);

var _FileLinkView2 = _interopRequireDefault(_FileLinkView);

var _FileUploadView = __webpack_require__(399);

var _FileUploadView2 = _interopRequireDefault(_FileUploadView);

var _FileView = __webpack_require__(255);

var _FileView2 = _interopRequireDefault(_FileView);

var _ImageView = __webpack_require__(256);

var _ImageView2 = _interopRequireDefault(_ImageView);

var _LabelView = __webpack_require__(257);

var _LabelView2 = _interopRequireDefault(_LabelView);

var _NewHtdFileView = __webpack_require__(401);

var _NewHtdFileView2 = _interopRequireDefault(_NewHtdFileView);

var _LinkView = __webpack_require__(258);

var _LinkView2 = _interopRequireDefault(_LinkView);

var _NumericInputView = __webpack_require__(259);

var _NumericInputView2 = _interopRequireDefault(_NumericInputView);

var _PositionLinkView = __webpack_require__(261);

var _PositionLinkView2 = _interopRequireDefault(_PositionLinkView);

var _ProcessExecutionView = __webpack_require__(262);

var _ProcessExecutionView2 = _interopRequireDefault(_ProcessExecutionView);

var _ProjectLinkTextView = __webpack_require__(263);

var _ProjectLinkTextView2 = _interopRequireDefault(_ProjectLinkTextView);

var _ProjectLinkView = __webpack_require__(264);

var _ProjectLinkView2 = _interopRequireDefault(_ProjectLinkView);

var _RadioView = __webpack_require__(265);

var _RadioView2 = _interopRequireDefault(_RadioView);

var _RegistryLinkView = __webpack_require__(266);

var _RegistryLinkView2 = _interopRequireDefault(_RegistryLinkView);

var _RepeatPeriodView = __webpack_require__(267);

var _RepeatPeriodView2 = _interopRequireDefault(_RepeatPeriodView);

var _ResolutionListView = __webpack_require__(268);

var _ResolutionListView2 = _interopRequireDefault(_ResolutionListView);

var _RichTextView = __webpack_require__(269);

var _RichTextView2 = _interopRequireDefault(_RichTextView);

var _SignListView = __webpack_require__(270);

var _SignListView2 = _interopRequireDefault(_SignListView);

var _TextAreaView = __webpack_require__(271);

var _TextAreaView2 = _interopRequireDefault(_TextAreaView);

var _TextBoxView = __webpack_require__(272);

var _TextBoxView2 = _interopRequireDefault(_TextBoxView);

var _UserLinkView = __webpack_require__(274);

var _UserLinkView2 = _interopRequireDefault(_UserLinkView);

var _paginator3 = __webpack_require__(164);

var _paginator4 = _interopRequireDefault(_paginator3);

var _chooser = __webpack_require__(161);

var _chooser2 = _interopRequireDefault(_chooser);

var _dialog_layout = __webpack_require__(279);

var _dialog_layout2 = _interopRequireDefault(_dialog_layout);

var _chooser3 = __webpack_require__(163);

var _chooser4 = _interopRequireDefault(_chooser3);

var _addresslink_chooser = __webpack_require__(410);

var _addresslink_chooser2 = _interopRequireDefault(_addresslink_chooser);

var _addresslink_dialog_layout = __webpack_require__(284);

var _addresslink_dialog_layout2 = _interopRequireDefault(_addresslink_dialog_layout);

var _select = __webpack_require__(165);

var _dataModel = __webpack_require__(283);

var _tree = __webpack_require__(409);

var _select2 = __webpack_require__(282);

var _select3 = _interopRequireDefault(_select2);

var _search = __webpack_require__(281);

var _search2 = _interopRequireDefault(_search);

var _link = __webpack_require__(280);

var _link2 = _interopRequireDefault(_link);

var _dialog_list_view = __webpack_require__(407);

var _tree_views = __webpack_require__(408);

var _addresslink_select_component = __webpack_require__(285);

var _addresslink_select_component2 = _interopRequireDefault(_addresslink_select_component);

var _addresslink_user_table_component = __webpack_require__(286);

var _addresslink_organization_table_component = __webpack_require__(412);

var _addresslink_models = __webpack_require__(166);

var _addresslink_widget = __webpack_require__(411);

var _addresslink_widget2 = _interopRequireDefault(_addresslink_widget);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AS = new _backbone2.default.Application();

Object.assign(AS, {
    LOGGER: LOGGER,
    FORMS: {
        ApiUtils: api,
        Model: _model2.default,
        View: _view2.default,
        PlayerModel: _PlayerModel2.default,
        PlayerView: _PlayerView2.default,
        ComponentUtils: compUtils,
        StyleUtils: styleUtils,
        ASFDataUtils: dataUtils,

        EVENT_TYPE: _constants.EVENT_TYPE,
        WIDGET_TYPE: _constants.WIDGET_TYPE,
        KEY_CODES: _constants.KEY_CODES,
        REPEAT_PERIOD_TYPE: _constants.REPEAT_PERIOD_TYPE,
        INPUT_ERROR_TYPE: _constants.INPUT_ERROR_TYPE,
        NAME_FORMAT_PARTS: _constants.NAME_FORMAT_PARTS,
        ENTITY_TYPE: _constants.ENTITY_TYPE,
        DOC_ATTRIBUTE_FIELD: _constants.DOC_ATTRIBUTE_FIELD,
        DECIMAL_DELIMITER: _constants.DECIMAL_DELIMITER,
        FORM_VIEW_TYPE: _constants.FORM_VIEW_TYPE,
        TABLE_EVENT_TYPE: _constants.TABLE_EVENT_TYPE,

        LayoutUtils: LayoutUtils,
        TableUtils: tableUtils,
        DateUtils: dateUtils,
        CaretUtils: caretUtils,
        bus: player.bus,
        createPlayer: player.createPlayer,
        BuilderUtils: BuilderUtils,
        History: _History2.default,
        Selection: _selection2.default,
        TableModel: _TableModel2.default,
        MaskUtils: maskUtils,
        Combo: _ComboBox2.default,
        Button: _Button2.default,
        BuilderToolBar: _BuilderToolBar2.default,
        TagArea: _TagArea2.default,
        UserChooser: _UserChooser2.default,
        USER_CHOOSER_SUFFIXES: _UserChooser.USER_CHOOSER_SUFFIXES,

        SimpleModel: _SimpleModel2.default,
        SplitPane: _SplitPane2.default,
        BasicChooserDialog: _BasicChooserDialog2.default,
        BasicChooserEvent: _BasicChooserDialog.BasicChooserEvent,
        PopupPanel: _PopupPane2.default,
        popupPanel: _PopupPane.popupPanel,
        CalendarPopup: _CalendarChooser2.default,
        calendarPopup: _CalendarChooser.calendarPopup,
        DropDownMenu: _DropDownMenu2.default,
        dropDownMenu: _DropDownMenu.dropDownMenu,
        DaysChooserPopup: _DaysChooser2.default,
        daysChooserPopup: _DaysChooser.daysChooserPopup,
        DepartmentChooser: _DepartmentChooser2.default,
        PositionChooser: _PositionChooser2.default,
        FileChooser: _FileChooser2.default,
        LinkEditDialog: _LinkEditDialog2.default,
        ProjectChooser: _ProjectChooser2.default,
        RegistryLinkChooser: _RegistryLinkChooser2.default,

        AddressLinkModel: _AddressLinkModel2.default,
        ComboBoxModel: _ComboModel2.default,
        CustomComponentModel: _CustomComponentModel2.default,
        DateModel: _DateModel2.default,
        DepartmentLinkModel: _DepartmentLinkModel2.default,
        DocAttributeModel: _DocAttributeModel2.default,
        DocLinkModel: _DocLinkModel2.default,
        FileLinkModel: _FileLinkModel2.default,
        FileModel: _FileModel2.default,
        ImageModel: _ImageModel2.default,
        LabelModel: _LabelModel2.default,
        LinkModel: _LinkModel2.default,
        NumericModel: _NumericInputModel2.default,
        NumericUtils: _NumericInputModel.NumericUtils,
        PositionLinkModel: _PositionLinkModel2.default,
        ProjectLinkModel: _ProjectLinkModel2.default,
        RegistryLinkModel: _RegistryLinkModel2.default,
        RepeatPeriodModel: _RepeatPeriodModel2.default,
        UploadFileModel: _UploadFileModel2.default,
        TextBoxModel: _TextBoxModel2.default,
        UserLinkModel: _UserLinkModel2.default,

        TableBuilderView: _tableBuilderView2.default,
        TableBlock: _tableBlock2.default,
        TableDynamicView: _tableDynView2.default,
        TableStaticView: _tableStaticView2.default,
        TableParagraphView: _paragraphView2.default,
        TextView: _TextView2.default,
        AddressLinkView: _AddressLinkView2.default,
        BuilderComponentView: _BuilderComponentView2.default,
        CheckBoxView: _CheckBoxView2.default,
        CheckBoxBuilderView: _CheckBoxView.CheckBoxBuilderView,
        ComboBoxView: _ComboBoxView2.default,
        CustomComponentView: _CustomComponentView2.default,
        DateView: _DateView2.default,
        DepartmentLinkView: _DepartmentLinkView2.default,
        DocAttributeView: _DocAttributeView2.default,
        DocLinkTextView: _DocLinkTextView2.default,
        DocLinkView: _DocLinkView2.default,
        FileLinkView: _FileLinkView2.default,
        FileUploadView: _FileUploadView2.default,
        FileView: _FileView2.default,
        ImageView: _ImageView2.default,
        LabelView: _LabelView2.default,
        NewFileView: _NewHtdFileView2.default,
        NumericInputView: _NumericInputView2.default,
        PositionLinkView: _PositionLinkView2.default,
        ProcessExecutionView: _ProcessExecutionView2.default,
        ProjectLinkTextView: _ProjectLinkTextView2.default,
        ProjectLinkView: _ProjectLinkView2.default,
        RadioButtonView: _RadioView2.default,
        RadioButtonBuilderView: _RadioView.RadioButtonBuilderView,
        RegistryLinkView: _RegistryLinkView2.default,
        RepeatPeriodView: _RepeatPeriodView2.default,
        ResolutionListView: _ResolutionListView2.default,
        RichTextView: _RichTextView2.default,
        SignListView: _SignListView2.default,
        TextAreaView: _TextAreaView2.default,
        TextBoxView: _TextBoxView2.default,
        UserLinkView: _UserLinkView2.default,
        LinkView: _LinkView2.default
    },
    COMPONENTS: {
        Utils: domUtils,
        ExtendedTableEvent: _ExtendedTable.ExtendedTableEvent,
        ExtendedTable: _ExtendedTable2.default,
        AddressBookTable: _AddressBookTable2.default,
        RegistryTable: _RegistryTable2.default,
        Paginator: _paginator2.default,
        Chooser: _chooser2.default,
        DialogLayout: _dialog_layout2.default,
        Select: _select3.default,
        SearchField: _search2.default,
        Link: _link2.default,
        Navigation: _dialog_list_view.Navigation,
        ListItem: _dialog_list_view.ListItem,
        ListView: _dialog_list_view.ListView,
        Navigations: _dialog_list_view.Navigations,
        TreeRoot: _tree_views.TreeRoot,
        TreeView: _tree_views.TreeView
    },
    Entities: {
        PaginatorModel: _paginator4.default,
        ChooserModel: _chooser4.default,
        SelectCollection: _select.SelectCollection,
        SelectModel: _select.SelectModel,
        DataModel: _dataModel.DataModel,
        DataCollection: _dataModel.DataCollection,
        DepartmentNode: _tree.DepartmentNode,
        TreeNode: _tree.TreeNode,
        TreeNodeCollection: _tree.TreeNodeCollection,
        DepartmentNodeCollection: _tree.DepartmentNodeCollection
    },
    SERVICES: services,
    OPTIONS: _constants.OPTIONS,
    OBJECT_TYPES: _constants.OBJECT_TYPES,
    COMPONENT_EVENTS: _constants.COMPONENT_EVENTS,
    Utils: utils,
    DictionaryCache: _dictCache2.default,
    addDialogCloseListener: domUtils.addDialogCloseListener,
    getUrlParam: domUtils.getUrlParam,
    absoluteOffset: domUtils.absoluteOffset,

    CMP_DOM_ATTR: _constants.CMP_DOM_ATTR, CMP_DOM_FORM_ID: _constants.CMP_DOM_FORM_ID, CMP_DOM_DATA_ID: _constants.CMP_DOM_DATA_ID
});

AS.Chooser = AS.module("Chooser");
AS.Chooser.AddressLink = {
    Chooser: _addresslink_chooser2.default,
    DialogLayout: _addresslink_dialog_layout2.default,
    SelectComponent: _addresslink_select_component2.default,
    UserRowView: _addresslink_user_table_component.UserRowView,
    UserTableView: _addresslink_user_table_component.UserTableView,
    OrganizationRowView: _addresslink_organization_table_component.OrganizationRowView,
    OrganizationTableComponent: _addresslink_organization_table_component.OrganizationTableComponent,
    Organization: _addresslink_models.Organization,
    Organizations: _addresslink_models.Organizations,
    User: _addresslink_models.User,
    Users: _addresslink_models.Users,
    AddressWidget: _addresslink_widget2.default
};

AS.Components = AS.COMPONENTS;

exports.default = AS;

/***/ }),

/***/ 381:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = polyfill;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

__webpack_require__(610);

__webpack_require__(599);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function polyfill() {
    // здесь будут добавлены всякие полезные методы к базовым классом, ибо так удобнее
    String.prototype.endsWith = function (pattern) {
        var d = this.length - pattern.length;
        return d >= 0 && this.lastIndexOf(pattern) === d;
    };

    String.prototype.isEmpty = function () {
        return !this.match(/\S/);
    };

    String.prototype.formatThousands = function (delimiter) {
        return this.toString().replace(/\B(?=(\d{3})+(?!\d))/g, delimiter);
    };

    String.prototype.getWidth = function (font) {
        // re-use canvas object for better performance
        var canvas = String.prototype.canvas || (String.prototype.canvas = document.createElement("canvas"));
        var context = canvas.getContext("2d");
        context.font = font;
        var metrics = context.measureText(this);
        return metrics.width;
    };

    Number.prototype.round = function (digits) {
        digits = Math.floor(digits);
        if (isNaN(digits) || digits === 0) {
            return Math.round(this);
        }
        if (digits < 0 || digits > 16) {
            throw 'RangeError: Number.round() digits argument must be between 0 and 16';
        }
        var multiplier = Math.pow(10, digits);
        return Math.round(this * multiplier) / multiplier;
    };

    Number.prototype.fixed = function (digits) {
        digits = Math.floor(digits);
        if (isNaN(digits) || digits === 0) {
            return Math.round(this).toString();
        }
        var parts = this.round(digits).toString().split('.');
        var fraction = parts.length === 1 ? '' : parts[1];
        if (digits > fraction.length) {
            fraction += new Array(digits - fraction.length + 1).join('0');
        }
        return parts[0] + '.' + fraction;
    };

    Number.prototype.fixedInt = function (digits) {
        var result = this.toString();
        while (result.length < digits) {
            result = "0" + result;
        }
        result = result.substr(result.length - digits);
        return result;
    };

    Date.prototype.isValid = function () {
        // An invalid date object returns NaN for getTime() and NaN is the only
        // object not strictly equal to itself.
        return this.getTime() === this.getTime();
    };

    Array.prototype.isEmpty = function () {
        return this.length === 0;
    };

    Array.prototype.arrayToString = function (delimiter) {
        var v = "";
        var sign = "";
        this.forEach(function (item) {
            v += sign + item;
            sign = delimiter;
        });
        return v;
    };

    Array.prototype.equals = function (anotherArray) {
        if (this === anotherArray) return true;
        if (anotherArray == null) return false;
        if (this.length != anotherArray.length) return false;

        // If you don't care about the order of the elements inside
        // the array, you should sort both arrays here.

        for (var i = 0; i < this.length; ++i) {
            if (!_.isEqual(this[i], anotherArray[i])) {
                return false;
            }
        }
        return true;
    };

    /**
     * содержится ли элемент в массиве
     * @param element
     * @returns {boolean}
     */
    Array.prototype.contains = function (element) {
        return this.indexOf(element) > -1;
    };

    /**
     * удаление элемента из массива
     * @param element
     */
    Array.prototype.remove = function (element) {
        return this.splice(this.indexOf(element), 1);
    };

    /**
     * добавление элемента в массиве если такого там нет
     * @param element
     */
    Array.prototype.pushUnique = function (element) {
        if (this.contains(element)) {
            return;
        }
        this.push(element);
    };

    /**
     * найти элемент с указанным ззначением свойства
     * @param propName
     * @param propValue
     */
    Array.prototype.findElement = function (propName, propValue) {
        var result = null;
        this.some(function (el) {
            if (el && el[propName] === propValue) {
                result = el;
                return true;
            }
        });
        return result;
    };

    if (!String.prototype.startsWith) {
        String.prototype.startsWith = function (token) {
            return this.indexOf(token) === 0;
        };
    }

    // диалог jquery крадет фокус у инпута когда появляется, нам это не нужно
    _jquery2.default.ui.dialog.prototype._focusTabbable = function () {};

    function getBrowser() {
        var ua = navigator.userAgent,
            tem,
            M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
        if (/trident/i.test(M[1])) {
            tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
            return 'MSIE';
        }
        if (M[1] === 'Chrome') {
            tem = ua.match(/\bOPR\/(\d+)/);
            if (tem != null) {
                return { name: 'Opera', version: tem[1] };
            }
        }
        M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
        if ((tem = ua.match(/version\/(\d+)/i)) != null) {
            M.splice(1, 1, tem[1]);
        }
        return M[0];
    };

    function getBrowserVersion() {
        var ua = navigator.userAgent,
            tem,
            M = ua.match(/(opera|chrome|safari|firefox|msie|trident(?=\/))\/?\s*(\d+)/i) || [];
        if (/trident/i.test(M[1])) {
            tem = /\brv[ :]+(\d+)/g.exec(ua) || [];
            return tem[1] || '';
        }
        if (M[1] === 'Chrome') {
            tem = ua.match(/\bOPR\/(\d+)/);
            if (tem != null) {
                return { name: 'Opera', version: tem[1] };
            }
        }
        M = M[2] ? [M[1], M[2]] : [navigator.appName, navigator.appVersion, '-?'];
        if ((tem = ua.match(/version\/(\d+)/i)) != null) {
            M.splice(1, 1, tem[1]);
        }
        return M[1];
    };

    window.browser = getBrowser();
    window.browserVersion = parseInt(getBrowserVersion());
    window.addEventListener('popstate', AS.SERVICES.parseUrl);

    Error.stackTraceLimit = 50;

    var sync = Backbone.sync;
    Backbone.sync = function (method, model, options) {
        options.beforeSend = AS.FORMS.ApiUtils.addAuthHeader;
        sync(method, model, options);
    };
} /**
   * Created by exile on 24.03.16.
   */

/***/ }),

/***/ 383:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 384:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 385:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 386:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 388:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.calendarPopup = undefined;
exports.default = CalendarPopup;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _dateUtils = __webpack_require__(73);

var dateUtils = _interopRequireWildcard(_dateUtils);

var _domUtils = __webpack_require__(44);

var domUtils = _interopRequireWildcard(_domUtils);

var _i18n = __webpack_require__(14);

var _i18n2 = _interopRequireDefault(_i18n);

var _math = __webpack_require__(112);

var _math2 = _interopRequireDefault(_math);

var _PopupPane = __webpack_require__(128);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * компонент выбора даты
 * Created by user on 22.06.16.
 */

function CalendarPopup() {

    if (CalendarPopup.prototype._singletonInstance) {
        return CalendarPopup.prototype._singletonInstance;
    }

    CalendarPopup.prototype._singletonInstance = this;

    var updateRegionalSettings = function updateRegionalSettings(year) {
        var currentYear = new Date().getYear() + 1900;
        var minYear = year ? _math2.default.min(year, currentYear - 90) : currentYear - 90;
        var maxYear = year ? _math2.default.max(year, currentYear + 20) : currentYear + 20;

        _jquery2.default.datepicker.regional.current = {
            closeText: _i18n2.default.tr("Закрыть"),
            prevText: "&#x3C;" + _i18n2.default.tr("Пред"),
            nextText: _i18n2.default.tr("След") + "&#x3E;",
            currentText: _i18n2.default.tr("Сегодня"),
            monthNames: [_i18n2.default.tr("Январь"), _i18n2.default.tr("Февраль"), _i18n2.default.tr("Март"), _i18n2.default.tr("Апрель"), _i18n2.default.tr("Май"), _i18n2.default.tr("Июнь"), _i18n2.default.tr("Июль"), _i18n2.default.tr("Август"), _i18n2.default.tr("Сентябрь"), _i18n2.default.tr("Октябрь"), _i18n2.default.tr("Ноябрь"), _i18n2.default.tr("Декабрь")],
            monthNamesShort: [_i18n2.default.tr("Январь"), _i18n2.default.tr("Февраль"), _i18n2.default.tr("Март"), _i18n2.default.tr("Апрель"), _i18n2.default.tr("Май"), _i18n2.default.tr("Июнь"), _i18n2.default.tr("Июль"), _i18n2.default.tr("Август"), _i18n2.default.tr("Сентябрь"), _i18n2.default.tr("Октябрь"), _i18n2.default.tr("Ноябрь"), _i18n2.default.tr("Декабрь")],
            /*monthNamesShort: [i18n.tr("Янв"), i18n.tr("Фев"), i18n.tr("Мар"), i18n.tr("Апр"), i18n.tr("Май"), i18n.tr("Июн"),
             i18n.tr("Июл"), i18n.tr("Авг"), i18n.tr("Сен"), i18n.tr("Окт"), i18n.tr("Ноя"), i18n.tr("Дек")],*/
            dayNames: [_i18n2.default.tr("Воскресенье"), _i18n2.default.tr("Понедельник"), _i18n2.default.tr("Вторник"), _i18n2.default.tr("Среда"), _i18n2.default.tr("Четверг"), _i18n2.default.tr("Пятница"), _i18n2.default.tr("Суббота")],
            dayNamesShort: [_i18n2.default.tr("Вс"), _i18n2.default.tr("Пн"), _i18n2.default.tr("Вт"), _i18n2.default.tr("Ср"), _i18n2.default.tr("Чт"), _i18n2.default.tr("Пт"), _i18n2.default.tr("Сб")],
            dayNamesMin: [_i18n2.default.tr("Вс"), _i18n2.default.tr("Пн"), _i18n2.default.tr("Вт"), _i18n2.default.tr("Ср"), _i18n2.default.tr("Чт"), _i18n2.default.tr("Пт"), _i18n2.default.tr("Сб")],
            weekHeader: _i18n2.default.tr("Нед"),
            dateFormat: dateUtils.DATE_PICKER_FORMAT,
            firstDay: 1,
            isRTL: false,
            showMonthAfterYear: false,
            yearSuffix: "",
            yearRange: minYear + ":" + maxYear
        };
    };

    this.showDatePopup = function (value, anchor, input, handler) {

        // делаем это каждый раз потому что локаль с последнего показа могут изменить, и надо перестроить компонент с максимальным годоm
        var year = null;
        if (value) {
            year = value.getYear() + 1900;
        }
        updateRegionalSettings(year);

        var div = (0, _jquery2.default)("<div>", { tabindex: "0", class: "asf-datePopup" });
        div.click(function (event) {
            domUtils.cancelEvent(event);
        });

        div.mouseup(function (event) {
            domUtils.cancelEvent(event);
        });

        function dateChosen() {
            if (input) {
                input.focus();
            }
            handler(div.datepicker("getDate"));
            _PopupPane.popupPanel.hide();
        }

        div.datepicker({
            showOtherMonths: true,
            changeMonth: true,
            changeYear: true,
            dateFormat: dateUtils.DATE_PICKER_FORMAT,
            /* fix buggy IE focus functionality */
            fixFocusIE: false,
            onClose: function onClose(dateText, inst) {
                this.fixFocusIE = true;
                dateChosen();
            },
            onSelect: function onSelect() {
                dateChosen();
            },
            beforeShow: function beforeShow(input, inst) {
                if (input) {
                    input.blur();
                }
            }
        });

        div.datepicker("option", _jquery2.default.datepicker.regional.current);

        if (value) {
            div.datepicker("setDate", dateUtils.formatDate(value, "${yyyy}-${mm}-${dd}"));
        }

        var today = (0, _jquery2.default)("<div/>", { class: "asf-datePicker-Today" });
        today.html(_i18n2.default.tr("Сегодня"));
        today.click(function () {
            div.datepicker("setDate", dateUtils.formatDate(new Date(), "${yyyy}-${mm}-${dd}"));
            dateChosen();
        });

        div.append(today);

        _PopupPane.popupPanel.showComponent(div, anchor, 302, 292);
    };
};

var calendarPopup = exports.calendarPopup = new CalendarPopup();

/***/ }),

/***/ 389:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.daysChooserPopup = undefined;
exports.default = DaysChooserPopup;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _constants = __webpack_require__(2);

var _dateUtils = __webpack_require__(73);

var dateUtils = _interopRequireWildcard(_dateUtils);

var _domUtils = __webpack_require__(44);

var domUtils = _interopRequireWildcard(_domUtils);

var _PopupPane = __webpack_require__(128);

var _DropDownMenu = __webpack_require__(217);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * компонент выбора дней (дней недели, месяца, года)
 * Created by user on 22.06.16.
 */

function DaysChooserPopup() {

    if (DaysChooserPopup.prototype._singletonInstance) {
        return DaysChooserPopup.prototype._singletonInstance;
    }

    DaysChooserPopup.prototype._singletonInstance = this;

    this.showDaysPopup = function (type, selectedValues, anchor, handler) {

        var width = 286;
        var height = 0;

        var div = (0, _jquery2.default)("<div>", { class: "asf-dcPopup" });
        var contentDiv = (0, _jquery2.default)("<div>");
        contentDiv.css("width", "282px");
        contentDiv.css("padding", "2px");
        div.click(function (event) {
            domUtils.cancelEvent(event);
        });

        div.mouseup(function (event) {
            domUtils.cancelEvent(event);
        });

        function buildContent(values) {
            contentDiv.empty();
            values.forEach(function (value) {
                value.day = (0, _jquery2.default)('<div class="asf-dcDay ' + (value.selected ? 'asf-dcSelectedDay' : '') + ' ">' + value.title + '</div>');
                value.day.click(function () {
                    if (value.selected) {
                        value.selected = false;
                        value.day.removeClass("asf-dcSelectedDay");
                        selectedValues.remove(value.value);
                    } else {
                        value.selected = true;
                        (0, _jquery2.default)(value.day).addClass("asf-dcSelectedDay");
                        selectedValues.push(value.value);
                    }
                    handler(selectedValues);
                });
                contentDiv.append(value.day);
            });
        }

        var values = [];
        var i;
        var v;
        if (type === _constants.REPEAT_PERIOD_TYPE.week_days) {
            height = 32;
            div.css("width", width + "px");
            div.css("height", height + "px");
            for (i = 1; i < 8; i++) {
                v = i + ".0";
                values.push({ value: v, title: dateUtils.getWeekDayShortName(i), selected: selectedValues.indexOf(v) > -1 });
            }
        } else if (type === _constants.REPEAT_PERIOD_TYPE.month_days) {
            height = 136;
            div.css("width", width + "px");
            div.css("height", height + "px");
            for (i = 1; i < 32; i++) {
                v = i + ".0";
                values.push({ value: v, title: i + "", selected: selectedValues.indexOf(v) > -1 });
            }
        } else {
            height = 192;
            div.css("width", width + "px");
            div.css("height", height + "px");

            var navigatorBar = (0, _jquery2.default)("<div/>", { class: "asf-dcNavigatorBar" });
            var monthSelect = (0, _jquery2.default)('<div/>', { class: "asf-monthSelect asf-InlineBlock asf-cursorPointer" });

            var prev = (0, _jquery2.default)("<div/>", { class: "ns-calendarPrev" });
            var next = (0, _jquery2.default)("<div/>", { class: "ns-calendarNext" });

            prev.click(function () {
                monthSelect.setMonth(monthSelect.month - 1 === 0 ? 12 : monthSelect.month - 1);
            });

            next.click(function () {
                monthSelect.setMonth(monthSelect.month + 1 === 13 ? 1 : monthSelect.month + 1);
            });

            navigatorBar.append(prev);
            navigatorBar.append((0, _jquery2.default)("<div/>", { class: "asf-InlineBlock", style: "width:64px" }));
            navigatorBar.append(monthSelect);
            navigatorBar.append((0, _jquery2.default)("<div/>", { class: "asf-InlineBlock", style: "width:64px" }));
            navigatorBar.append(next);

            div.append(navigatorBar);

            monthSelect.setMonth = function (monthValue) {
                values = [];
                monthSelect.month = monthValue;
                monthSelect.html(dateUtils.getMonthName(monthValue));
                var daysCount = 32;
                if (monthValue === 2) {
                    daysCount = 30;
                } else if (monthValue === 4 || monthValue === 6 || monthValue === 9 || monthValue === 11) {
                    daysCount = 31;
                }
                for (var i = 1; i < daysCount; i++) {
                    var v = i + "." + monthValue;
                    values.push({ value: v, title: i + "", selected: selectedValues.indexOf(v) > -1 });
                }
                buildContent(values);
            };

            monthSelect.setMonth(new Date().getMonth() + 1);

            monthSelect.click(function () {
                var values = [];
                for (var i = 1; i < 13; i++) {
                    values.push({ value: i, title: dateUtils.getMonthName(i), selected: i === monthSelect.month });
                }

                _DropDownMenu.dropDownMenu.show(values, monthSelect, 60, monthSelect.setMonth, true);
            });
        }

        buildContent(values);

        div.append(contentDiv);

        _PopupPane.popupPanel.showComponent(div, anchor, width + 2, height + 2);
    };
};

var daysChooserPopup = exports.daysChooserPopup = new DaysChooserPopup();

/***/ }),

/***/ 390:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = DepartmentChooser;

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

var _i18n = __webpack_require__(14);

var _i18n2 = _interopRequireDefault(_i18n);

var _BasicChooserDialog = __webpack_require__(71);

var _BasicChooserDialog2 = _interopRequireDefault(_BasicChooserDialog);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function DepartmentChooser(multiSelect, locale) {

    var instance = this;

    var filterPositionID = null;
    var filterUserID = null;
    var filterDepartmentID = null;

    var dialog = new _BasicChooserDialog2.default();

    dialog.init({
        showListItemIcon: false,
        showListItemInfo: true,
        showListItemStatus: true,
        multiSelect: multiSelect,
        showSearchField: true,
        title: _i18n2.default.tr("Выбор подразделения"),
        placeHolderText: _i18n2.default.tr("Поиск подразделений"),
        showSelectedStack: true
    });

    this.correctListArray = function (departments) {
        if (!departments) {
            return [];
        }
        departments.forEach(function (department) {
            department.id = department.departmentId;
            department.name = department.departmentName;
            department.info = department.parentName;
        });
        return departments;
    };

    dialog.addTree("departments", function (parentNode, tree) {
        var departmentID = undefined;
        if (parentNode) {
            departmentID = parentNode.id;
        }
        api.getDepartments(departmentID, locale, function (departments) {
            dialog.setTreeNodeData("departments", departmentID, departments.departments, "departmentID", "departmentName", "hasChildDepartments");
        });
    }, function (parentNode, tree) {
        dialog.setListTitle(parentNode.name);
        dialog.showList("departments", parentNode, true);
    });

    dialog.addListType("departments", function (search, startRecord, recordsCount, selectedNodeId, stackId) {
        var pageNumber = startRecord / 30;
        if (startRecord % 30 > 0) {
            return;
        }
        api.getDepartmentsSuggestions(search, filterUserID, filterPositionID, filterDepartmentID, selectedNodeId, pageNumber, recordsCount, locale, true, function (departments) {
            dialog.setListData("departments", instance.correctListArray(departments), startRecord > 0);
        });
    }, function (search, selectedNodeId, stackId) {
        api.getDepartmentsSuggestions(search, filterUserID, filterPositionID, filterDepartmentID, selectedNodeId, 0, 0, locale, true, function (departments) {
            dialog.setSelectedItems(instance.correctListArray(departments));
        });
    });

    dialog.addStack("structure", _i18n2.default.tr("Орг. структура"), "departments", true, true, function () {});

    dialog.showStack("structure");

    this.setFilterUserID = function (newValue) {
        filterUserID = newValue;
    };

    this.setFilterPositionID = function (newValue) {
        filterPositionID = newValue;
    };

    this.setFilterDepartmentID = function (newValue) {
        filterDepartmentID = newValue;
    };

    this.setSelectedItems = function (newSelectedPersons) {
        dialog.setSelectedItems(instance.correctListArray(newSelectedPersons));
    };

    this.getSelectedItems = function () {
        return dialog.getSelectedItems();
    };

    this.showDialog = function () {
        dialog.show();
    };

    /**
     *
     * @param event SEE BasicChooserEvent
     * @param handler
     */
    this.on = function (event, handler) {
        dialog.on(event, handler);
    };
};

/***/ }),

/***/ 391:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = FileChooser;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

var _i18n = __webpack_require__(14);

var _i18n2 = _interopRequireDefault(_i18n);

var _BasicChooserDialog = __webpack_require__(71);

var _BasicChooserDialog2 = _interopRequireDefault(_BasicChooserDialog);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function FileChooser(multiSelect, showHomeFolder, mimeTypes) {

    var allowedTypes = [];

    var instance = this;

    this.correctListArray = function (files) {
        files.forEach(function (file) {
            file.id = file.identifier;
        });
        return files;
    };

    var dialog = new _BasicChooserDialog2.default();
    dialog.init({
        showListItemIcon: true,
        showListItemInfo: false,
        showListItemStatus: false,
        multiSelect: multiSelect,
        showSearchField: false,
        listItemIconSize: 16,
        title: _i18n2.default.getString("Выбор файла"),
        showListItemIconStyle: "ns-fileChooserIcon",
        showSelectedStack: false,
        lazyListLoading: false,
        canSelect: function canSelect(item) {
            return !item.is_folder;
        },
        dblClick: function dblClick(item) {
            dialog.selectTreeNode("folders", item.id);
            dialog.openTreeNode("folders", item.id);
        }
    });

    dialog.addTree("folders", function (parentNode, tree) {
        var parentID = undefined;
        if (parentNode) {
            parentID = parentNode.id;
        }
        api.getFolders(parentID, showHomeFolder, function (folders) {
            dialog.setTreeNodeData("folders", parentID, folders, "identifier", "name", "notEmpty");
        });
    }, function (parentNode, tree) {
        dialog.setListTitle(parentNode.name);
        dialog.showList("files", parentNode, true);
        dialog.openTreeNode("folders", parentNode.id);
    }, function () {
        return (0, _jquery2.default)("<div/>", { class: "ns-customTreeIcon ns-folderIcon" });
    });

    dialog.addListType("files", function (search, startRecord, recordsCount, selectedNodeId, stackId) {
        api.getFiles(selectedNodeId, mimeTypes, function (files) {
            dialog.setListData("files", instance.correctListArray(files), startRecord > 0);
        });
    }, function (search, selectedNodeId, stackId) {
        api.getFiles(selectedNodeId, function (files) {
            dialog.setSelectedItems(instance.correctListArray(files));
        });
    });

    dialog.addStack("folders", _i18n2.default.getString("Все папки"), "folders", true, true, function () {});

    dialog.showStack("folders");

    this.setAllowedTypes = function (newValue) {
        allowedTypes = newValue;
    };

    this.showDialog = function () {
        dialog.show();
    };

    this.setSelectedItems = function (newSelectedPersons) {
        dialog.setSelectedItems(instance.correctListArray(newSelectedPersons));
    };

    this.getSelectedItems = function () {
        return dialog.getSelectedItems();
    };

    /**
     *
     * @param event SEE BasicChooserEvent
     * @param handler
     */
    this.on = function (event, handler) {
        dialog.on(event, handler);
    };
};

/***/ }),

/***/ 392:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = LinkEditDialog;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _componentUtils = __webpack_require__(15);

var compUtils = _interopRequireWildcard(_componentUtils);

var _i18n = __webpack_require__(14);

var _i18n2 = _interopRequireDefault(_i18n);

var _BasicChooserDialog = __webpack_require__(71);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 *  диалог редактирования ссылки
 * Created by user on 30.04.16.
 */

function LinkEditDialog() {
    var instance = this;
    compUtils.makeBus(this);
    this.contentContainer = (0, _jquery2.default)('<div/>', { class: 'ns-linkEditDialogContent' });

    var CONTAINER_WIDTH = 400;
    var CONTAINER_HEIGHT = 255;

    var urlLabel = (0, _jquery2.default)('<div/>', { class: 'ns-boldLabel' });
    var urlInput = (0, _jquery2.default)('<input/>', { type: "text", class: 'ns-input ns-linkInput', placeholder: _i18n2.default.tr("Введите URL ссылки") });
    var titleLabel = (0, _jquery2.default)('<div/>', { class: 'ns-boldLabel' });
    var titleInput = (0, _jquery2.default)('<input/>', { type: "text", class: 'ns-input ns-linkInput', placeholder: _i18n2.default.tr("Введите надпись к ссылке") });
    var openInNewCheckBox = (0, _jquery2.default)('<input/>', { type: "checkbox", class: 'ns-checkBox' });
    var openInNewLabel = (0, _jquery2.default)('<label/>', { class: "ns-checkBoxLabel" });

    var button = (0, _jquery2.default)('<button>', { class: "ns-approveButton" }).button().html(_i18n2.default.tr("Готово"));

    urlLabel.html(_i18n2.default.tr("URL"));
    titleLabel.html(_i18n2.default.tr("Надпись"));

    openInNewLabel.append(openInNewCheckBox);
    openInNewLabel.append(_i18n2.default.tr("Открывать в отдельном окне"));

    instance.contentContainer.append(urlLabel);
    instance.contentContainer.append(urlInput);
    instance.contentContainer.append(titleLabel);
    instance.contentContainer.append(titleInput);
    instance.contentContainer.append(openInNewLabel);
    instance.contentContainer.append(button);

    urlLabel.css("margin-bottom", "5px");
    urlInput.css("margin-bottom", "7px");
    titleLabel.css("margin-bottom", "6px");
    titleInput.css("margin-bottom", "7px");
    openInNewLabel.css("margin-bottom", "20px");
    button.css("margin", "auto");
    button.css("display", "block");

    button.click(function () {
        instance.close();
    });

    this.showDialog = function () {
        (0, _jquery2.default)("body").append(this.contentContainer);
        (0, _jquery2.default)(this.contentContainer).dialog({
            modal: true,
            width: CONTAINER_WIDTH,
            height: CONTAINER_HEIGHT,
            title: _i18n2.default.tr("Настройки ссылки")
        });
    };

    this.setData = function (label, url, openInNew) {
        titleInput.val(label);
        urlInput.val(url);
        if (openInNew) {
            openInNewCheckBox.prop("checked", true);
        } else {
            openInNewCheckBox.prop("checked", false);
        }
    };

    this.getLabel = function () {
        return titleInput.val();
    };

    this.getUrl = function () {
        return urlInput.val();
    };

    this.getOpenInNew = function () {
        return openInNewCheckBox.prop("checked");
    };

    this.close = function () {
        instance.contentContainer.dialog("destroy");
        instance.contentContainer.detach();
        instance.trigger(_BasicChooserDialog.BasicChooserEvent.applyClicked);
    };
};

/***/ }),

/***/ 393:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = PositionChooser;

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

var _i18n = __webpack_require__(14);

var _i18n2 = _interopRequireDefault(_i18n);

var _BasicChooserDialog = __webpack_require__(71);

var _BasicChooserDialog2 = _interopRequireDefault(_BasicChooserDialog);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function PositionChooser(multiSelect, locale) {

    var instance = this;
    var showAll = true;
    var filterDepartmentID = null;
    var filterUserID = null;
    var showVacant;

    var dialog = new _BasicChooserDialog2.default();
    dialog.init({
        showListItemIcon: false,
        showListItemInfo: true,
        showListItemStatus: true,
        multiSelect: multiSelect,
        showSearchField: true,
        title: _i18n2.default.tr("Выбор должности"),
        placeHolderText: _i18n2.default.tr("Поиск должностей"),
        showSelectedStack: true
    });

    this.correctListArray = function (positions) {
        if (!positions) {
            return [];
        }
        positions.forEach(function (position) {
            if (position.transitional) {
                position.id = position.elementID + "," + position.periodID;
            } else {
                position.id = position.elementID;
            }

            position.name = position.elementName;
            position.info = position.departmentName;
        });
        return positions;
    };

    dialog.addTree("departments", function (parentNode, tree) {
        var departmentID = undefined;
        if (parentNode) {
            departmentID = parentNode.id;
        }
        api.getDepartments(departmentID, locale, function (departments) {
            dialog.setTreeNodeData("departments", departmentID, departments.departments, "departmentID", "departmentName", "hasChildDepartments");
        });
    }, function (parentNode, tree) {
        dialog.setListTitle(parentNode.name);
        dialog.showList("positions", parentNode, true);
    });

    dialog.addListType("positions", function (search, startRecord, recordsCount, selectedNodeId, stackId) {
        api.getPositions(search, filterUserID, filterDepartmentID, showVacant, showAll, selectedNodeId, startRecord, recordsCount, locale, function (positions) {
            dialog.setListData("positions", instance.correctListArray(positions), startRecord > 0);
        });
    }, function (search, selectedNodeId, stackId) {
        api.getPositions(search, filterUserID, filterDepartmentID, showVacant, showAll, selectedNodeId, 0, 0, locale, function (positions) {
            dialog.setSelectedItems(instance.correctListArray(positions));
        });
    });

    dialog.addStack("structure", _i18n2.default.tr("Орг. структура"), "departments", true, true, function () {});

    dialog.showStack("structure");

    this.setShowAll = function (newValue) {
        showAll = newValue;
    };

    this.setFilterDepartmentID = function (newValue) {
        filterDepartmentID = newValue;
    };

    this.setFilterUserID = function (newValue) {
        filterUserID = newValue;
    };

    this.setShowVacant = function (newValue) {
        showVacant = newValue;
    };

    this.setSelectedItems = function (newSelectedPersons) {
        dialog.setSelectedItems(instance.correctListArray(newSelectedPersons));
    };

    this.getSelectedItems = function () {
        return dialog.getSelectedItems();
    };

    this.showDialog = function () {
        dialog.show();
    };

    /**
     *
     * @param event SEE BasicChooserEvent
     * @param handler
     */
    this.on = function (event, handler) {
        dialog.on(event, handler);
    };
};

/***/ }),

/***/ 394:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = ProjectChooser;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

var _constants = __webpack_require__(2);

var _i18n = __webpack_require__(14);

var _i18n2 = _interopRequireDefault(_i18n);

var _BasicChooserDialog = __webpack_require__(71);

var _BasicChooserDialog2 = _interopRequireDefault(_BasicChooserDialog);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * диалог выбора проекта
 * Created by user on 28.04.16.
 */
function ProjectChooser(multiSelect) {

    var instance = this;

    this.correctListArray = function (objects) {
        objects.forEach(function (object) {
            if (object.elementType == _constants.OBJECT_TYPES.PLAN) {
                object.icon = "js/asforms/resources/project.16.png";
            } else {
                object.icon = "js/asforms/resources/portfolio.16.png";
            }
            object.id = object.itemID;
        });
        return objects;
    };

    this.correctTreeArray = function (objects) {

        var portfolios = [];
        objects.forEach(function (object) {
            object.notEmpty = object.hasChildren == "true";
            object.hasChildren = undefined;
            delete object.hasChildren;
            if (object.elementType == _constants.OBJECT_TYPES.PORTFOLIO) {
                portfolios.push(object);
            }
        });
        return objects;
    };

    var dialog = new _BasicChooserDialog2.default();
    dialog.init({
        showListItemIcon: true,
        showListItemInfo: false,
        showListItemStatus: false,
        multiSelect: multiSelect,
        showSearchField: false,
        listItemIconSize: 16,
        title: _i18n2.default.tr("Выбор портфеля или проекта"),
        showListItemIconStyle: "ns-fileChooserIcon",
        showSelectedStack: false,
        lazyListLoading: false
    });

    dialog.addTree("portfolios", function (parentNode, tree) {
        var parentNodeID = undefined;

        if (parentNode) {
            parentNodeID = parentNode.id;
        }

        var parentID = parentNodeID;

        if (parentID == "-1") {
            parentID = null;
        }
        api.getPortfolios(parentID, true, function (portfolio) {
            if (parentNodeID == undefined) {
                var data = [{ itemID: "-1", name: portfolio.name, hasChildren: "true" }];
                dialog.setTreeNodeData("portfolios", parentNodeID, instance.correctTreeArray(data), "itemID", "name", "notEmpty");
            } else {
                dialog.setTreeNodeData("portfolios", parentNodeID, instance.correctTreeArray(portfolio.items), "itemID", "name", "notEmpty");
            }
        });
    }, function (parentNode, tree) {
        dialog.setListTitle(parentNode.name);
        dialog.showList("projects", parentNode, true);
    }, function (node) {
        if (node.id == '-1') {
            return (0, _jquery2.default)("<div/>", { class: "ns-corePortfolioIcon ns-customTreeIcon" });
        } else {
            return (0, _jquery2.default)("<div/>", { class: "ns-portfolioIcon ns-customTreeIcon" });
        }
    });

    dialog.addListType("projects", function (search, startRecord, recordsCount, selectedNodeId, stackId) {

        if (selectedNodeId == "-1") {
            selectedNodeId = null;
        }

        api.getPortfolios(selectedNodeId, false, function (portfolio) {
            dialog.setListData("projects", instance.correctListArray(portfolio.items), startRecord > 0);
        });
    }, function (search, selectedNodeId, stackId) {

        if (selectedNodeId == "-1") {
            selectedNodeId = null;
        }

        api.getPortfolios(selectedNodeId, function (portfolio) {
            dialog.setSelectedItems(instance.correctListArray(portfolio.items));
        });
    });

    dialog.addStack("portfolios", _i18n2.default.tr("Общекорпоративное дерево"), "portfolios", true, true, function () {});

    dialog.showStack("portfolios");

    this.showDialog = function () {
        dialog.show();
    };

    this.setSelectedItems = function (newSelectedPersons) {
        dialog.setSelectedItems(instance.correctListArray(newSelectedPersons));
    };

    this.getSelectedItems = function () {
        return dialog.getSelectedItems();
    };

    /**
     *
     * @param event SEE BasicChooserEvent
     * @param handler
     */
    this.on = function (event, handler) {
        dialog.on(event, handler);
    };
};

/***/ }),

/***/ 395:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = RegistryLinkChooser;

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

var _constants = __webpack_require__(2);

var _i18n = __webpack_require__(14);

var _i18n2 = _interopRequireDefault(_i18n);

var _BasicChooserDialog = __webpack_require__(71);

var _BasicChooserDialog2 = _interopRequireDefault(_BasicChooserDialog);

var _ExtendedTable = __webpack_require__(113);

var _RegistryTable = __webpack_require__(218);

var _RegistryTable2 = _interopRequireDefault(_RegistryTable);

var _paginator = __webpack_require__(162);

var _paginator2 = _interopRequireDefault(_paginator);

var _paginator3 = __webpack_require__(164);

var _paginator4 = _interopRequireDefault(_paginator3);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function RegistryLinkChooser(multiSelect, registry) {

    var instance = this;
    var filterID = null;

    this.tableComponent = new _RegistryTable2.default(registry, 777);
    var tableComponent = this.tableComponent;
    this.paginatorComponent = new _paginator2.default({
        model: new _paginator4.default({
            currentPage: 0,
            totalPage: 0
        })
    });

    var dialog = new _BasicChooserDialog2.default();
    dialog.init({
        showListItemIcon: false,
        showListItemInfo: false,
        showListItemStatus: false,
        multiSelect: multiSelect,
        showSearchField: true,
        listItemIconSize: 16,
        title: _i18n2.default.tr("Выбор записи реестра"),
        placeHolderText: _i18n2.default.tr("Поиск записей"),
        showSelectedStack: false,
        lazyListLoading: false,
        width: 1000,
        height: 600,
        splitWidth: 982,
        treeWidth: 200,
        paginatorComponent: this.paginatorComponent
    });

    this.paginatorComponent.on("pageChanged", function (page) {
        instance.onPageChanged(page);
    });

    this.correctTreeArray = function (objects) {
        var registryRoot = {};
        registryRoot.isRoot = true;
        registryRoot.registryID = registry.registryID;
        registryRoot.id = registry.registryID;
        registryRoot.notEmpty = objects.length > 0;
        registryRoot.name = registry.name;
        registryRoot.children = objects;
        objects.forEach(function (object) {
            object.notEmpty = object.children.length > 0;
        });
        return [registryRoot];
    };

    api.getRegistryFilters(registry.registryID, true, "all", _constants.OPTIONS.locale, function (filters) {
        dialog.addTree("filters", function (parentNode, tree) {}, function (parentNode, tree) {
            dialog.openTreeNode("filters", parentNode.id);
            if (parentNode.id === registry.registryID) {
                tableComponent.loadData(0);
                filterID = null;
            } else {
                tableComponent.loadData(0, parentNode.id);
                filterID = parentNode.id;
            }
        }, null, instance.correctTreeArray(filters), true);
        dialog.selectTreeNode("filters", registry.registryID);
        dialog.showTree("filters");
    });

    this.showDialog = function () {
        dialog.show();
    };

    this.getSelectedItems = function () {
        return dialog.getSelectedItems();
    };

    /**
     *
     * @param event SEE BasicChooserEvent
     * @param handler
     */
    this.on = function (event, handler) {
        dialog.on(event, handler);
    };

    var paginatorComponent = this.paginatorComponent;

    this.tableComponent.on(_ExtendedTable.ExtendedTableEvent.dataLoaded, function () {
        paginatorComponent.setCurrentPage(tableComponent.getCurrentPage() + 1);
        var totalPage = tableComponent.getPagesCount();
        if (isNaN(totalPage)) {
            totalPage = 0;
        }
        if (totalPage === 0) {
            paginatorComponent.setCurrentPage(0);
        }
        paginatorComponent.setTotalPage(totalPage);
    });

    this.tableComponent.on(_ExtendedTable.ExtendedTableEvent.rowSelected, function (evt, rowIndex, data) {
        instance.selectedModel = data;
        dialog.trigger(_BasicChooserDialog.BasicChooserEvent.itemSelected, [data]);
    });

    this.tableComponent.dblclick(function () {
        if (instance.selectedModel) {
            dialog.trigger(_BasicChooserDialog.BasicChooserEvent.itemSelected, [instance.selectedModel]);
            dialog.close();
        }
    });

    this.onPageChanged = function (page) {
        this.tableComponent.loadData(page - 1, filterID);
    };

    dialog.setListComponent(this.tableComponent);
    dialog.setSearchBoxWidth(810);
};

/***/ }),

/***/ 396:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = TagItem;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _componentUtils = __webpack_require__(15);

var compUtils = _interopRequireWildcard(_componentUtils);

var _constants = __webpack_require__(2);

var _caretUtils = __webpack_require__(127);

var caretUtils = _interopRequireWildcard(_caretUtils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function TagItem(initValue, width, editContent) {
    var instance = this;

    this.value = initValue;
    var initTagName = initValue.tagName;

    compUtils.makeBus(instance);

    var tag = (0, _jquery2.default)('<div/>', { class: 'ns-tagItem' });

    var tagInput = (0, _jquery2.default)('<input/>', { type: "text", class: 'ns-tagItemInput' });
    tagInput.val(instance.value.tagName);
    tagInput.attr("readonly", "true");

    this.updateWidth = function () {
        if (width > 0) {
            var inputWidth = Math.max(Math.min(tagInput.val().getWidth("12px Arial") + 5, width - 25), 20);
            tagInput.css("width", inputWidth + "px");
        } else {
            var inputWidth = Math.max(Math.min(tagInput.val().getWidth("12px Arial") + 5), 20);
            tagInput.css("width", inputWidth + "px");
        }
    };

    this.valueUpdated = function () {
        if (tagInput.val().isEmpty()) {
            tagInput.val(initTagName);
        }
        instance.updateWidth();
        instance.value.tagName = tagInput.val();
        instance.trigger(_constants.EVENT_TYPE.tagEdit, [instance.value, instance]);
    };

    this.setEditable = function (editable) {
        if (editable) {
            tagDelete.css("display", "inline-block");
            tag.removeClass("ns-tagItemInputReadonly");
            tagInput.removeClass("ns-tagItemInputReadonly");
            if (editContent) {
                tag.on("dblclick", this.editTag);
            }
        } else {
            tagDelete.hide();
            tag.addClass("ns-tagItemInputReadonly");
            tagInput.addClass("ns-tagItemInputReadonly");
            tag.off("dblclick", this.editTag);
        }
    };

    this.getWidget = function () {
        return tag;
    };

    this.updateWidth();

    this.editTag = function () {
        tagInput.removeAttr("readonly");
        tagInput.addClass("ns-tagItemInputEditing");
        tagInput.focus();
        caretUtils.setCaretPosition(tagInput[0], tagInput.val().length);
        event.cancelBubble = true;
    };

    if (editContent) {
        tag.click(function (event) {
            event.cancelBubble = true;
            event.stopped = true;
            event.stopPropagation();
        });

        tagInput.keypress(function (event) {
            if (event.keyCode == _constants.KEY_CODES.ENTER) {
                tagInput.attr("readonly", "true");
                tagInput.removeClass("ns-tagItemInputEditing");
                instance.valueUpdated();
            }
        });

        tagInput.blur(function () {
            tagInput.attr("readonly", "true");
            tagInput.removeClass("ns-tagItemInputEditing");
        });
    }

    tag.append(tagInput);

    var tagDelete = (0, _jquery2.default)('<div/>', { class: "ns-tagDeleteImage" });
    tag.append(tagDelete);
    tagDelete.click(function () {
        if (!confirm(i18n.tr("Вы действительно хотите удалить этот тэг?"))) {
            return;
        }
        tag.detach();
        instance.trigger(_constants.EVENT_TYPE.tagDelete, [instance.value, instance]);
    });
};

/***/ }),

/***/ 397:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(global) {

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _underscore = __webpack_require__(7);

var _underscore2 = _interopRequireDefault(_underscore);

var _constants = __webpack_require__(2);

var _math = __webpack_require__(112);

var _math2 = _interopRequireDefault(_math);

__webpack_require__(155);

__webpack_require__(386);

__webpack_require__(382);

__webpack_require__(384);

__webpack_require__(383);

__webpack_require__(385);

var _as = __webpack_require__(380);

var _as2 = _interopRequireDefault(_as);

var _polyfills = __webpack_require__(381);

var _polyfills2 = _interopRequireDefault(_polyfills);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

window.math = _math2.default;

if (window.mobilePlayer) {
    _constants.OPTIONS.mobilePlayer = true;
}

global.AS = _as2.default;

global.$ = _jquery2.default;
global.jQuery = _jquery2.default;
global._ = _underscore2.default;

(0, _polyfills2.default)();
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(80)))

/***/ }),

/***/ 398:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _backbone = __webpack_require__(39);

var _backbone2 = _interopRequireDefault(_backbone);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var UploadFileModel = _backbone2.default.Model.extend({
    defaults: {
        'id': null,
        'name': '',
        'nodeId': null,
        'dataId': null
    },
    initialize: function initialize(options) {
        if (options) {
            this.nodeId = options.nodeId;
            this.dataId = options.dataId;
        }
    },

    getUrl: function getUrl() {
        "use strict";

        var url = "rest/api/storage/asffile/upload";
        url += this.nodeId ? 'nodeId=' + this.nodeId : '';
        url += this.dataId ? 'dataId=' + this.dataId : '';
        return url;
    },

    save: function save() {
        "use strict";
    }
});

exports.default = UploadFileModel;

/***/ }),

/***/ 399:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _underscore = __webpack_require__(7);

var _underscore2 = _interopRequireDefault(_underscore);

var _backbone = __webpack_require__(39);

var _backbone2 = _interopRequireDefault(_backbone);

var _i18n = __webpack_require__(14);

var _i18n2 = _interopRequireDefault(_i18n);

var _services = __webpack_require__(23);

var services = _interopRequireWildcard(_services);

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

var _logger = __webpack_require__(101);

var LOGGER = _interopRequireWildcard(_logger);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var fileUploadTemplate = _underscore2.default.template('\
        <input name="file" type="file" class="file-chooser-input"> \
        <div style="text-align:center; margin-top:30px"> \
        <button class="ns-approveButton ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" disabled="disabled"><%=i18n.tr("Загрузить")%></button> \
        </div> \
        ');

var FileUploadView = _backbone2.default.View.extend({
    template: fileUploadTemplate,
    mimeTypes: [],

    initialize: function initialize(options) {
        "use strict";

        this.title = _i18n2.default.tr("Выбор файла");
        this.mimeTypes = options.mimeTypes;
    },

    events: {
        'change input': 'fileChosen',
        'click button': 'submit'
    },

    fileChosen: function fileChosen() {
        "use strict";

        this.$('button').prop('disabled', false);
        this.model.set('name', this.getInput().val());
    },

    submit: function submit() {
        "use strict";

        var files = this.getInput().prop('files');
        if (_underscore2.default.isEmpty(files)) return;

        var file = _underscore2.default.first(files);

        var data = new FormData();
        data.append('file', file);

        var view = this;

        services.showWaitWindow();

        api.uploadFile(this.model.nodeId, this.model.dataId, data, function (data) {

            services.hideWaitWindow();

            view.model.set('id', data);
        }, function (data) {

            services.hideWaitWindow();

            LOGGER.logServer(data);
        });
    },

    show: function show() {
        "use strict";

        var dialog = this.dialog;
        if (dialog === void 0) {
            dialog = this.$el.dialog({
                modal: true,
                width: 400,
                height: 140,
                title: this.title
            });
            this.dialog = dialog;
        }

        dialog.show();

        var input = this.getInput();
        if (this.mimeTypes && this.mimeTypes.length > 0) {
            (0, _jquery2.default)(input).attr("accept", this.mimeTypes.arrayToString(","));
        }
    },

    remove: function remove() {
        "use strict";

        if (!this.$el.dialog('isOpen')) return;
        this.$el.dialog('close');
        this.$el.dialog('destroy');
    },

    getInput: function getInput() {
        return this.$('input');
    },

    getForm: function getForm() {
        "use strict";

        return this.$('form');
    },

    render: function render() {
        "use strict";

        this.$el.append(this.template());
        return this;
    }
});
exports.default = FileUploadView;

/***/ }),

/***/ 400:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.MobileDateView = MobileDateView;

var _view = __webpack_require__(11);

var _view2 = _interopRequireDefault(_view);

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _maskUtils = __webpack_require__(100);

var maskUtils = _interopRequireWildcard(_maskUtils);

var _componentUtils = __webpack_require__(15);

var compUtils = _interopRequireWildcard(_componentUtils);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _services = __webpack_require__(23);

var services = _interopRequireWildcard(_services);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Created by exile
 * Date: 03.02.16
 * Time: 16:25
 */
function MobileDateView(model, playerView) {

    _view2.default.call(this, this, model, playerView);

    this.initialWidth = 200;
    if (!model.asfProperty.config['time-Enable']) {
        this.initialWidth = 140;
    }

    _view2.default.call(this, this, model, playerView);
    var dateBox = (0, _jquery2.default)('<input/>', { type: 'text', class: 'asf-dateBox' });
    var timeBox = (0, _jquery2.default)('<input/>', { type: 'text', class: 'asf-timeBox' });

    var button = (0, _jquery2.default)("<button/>", { class: "asf-calendar-button" });

    this.container.append(dateBox);
    this.container.append(button);
    if (model.asfProperty.config['time-Enable']) {
        this.container.append(timeBox);
    }

    maskUtils.initInput({ asfProperty: { config: { 'input-mask': '####-##-##' } } }, dateBox);
    maskUtils.initInput({ asfProperty: { config: { 'input-mask': '##:##' } } }, timeBox);

    compUtils.initInput(model, this, dateBox, true);
    compUtils.initInput(model, this, timeBox, true);

    dataUtils.addAsFormId(dateBox, "date", model.asfProperty);
    dataUtils.addAsFormId(timeBox, "time", model.asfProperty);
    dataUtils.addAsFormId(button, "button", model.asfProperty);

    var instance = this;

    var enabled = true;

    dateBox.change(function () {
        var v = dateBox.val();
        if (v.match(/[0-9]{2}-[0-9]{2}-[0-9]{4}/)) {
            if (timeBox.val() == dateUtils.INPUT_TIME_MASK) {
                timeBox.val("00:00");
            }
        }
    });

    dateBox.keyup(function () {
        instance.checkValid();
    });

    timeBox.keyup(function () {
        instance.checkValid();
    });

    button.click(function () {
        instance.showDatePicker();
    });

    dateBox.click(function () {
        instance.showDatePicker();
    });

    timeBox.focus(function () {
        instance.showTimePicker();
    });

    this.showDatePicker = function () {
        if (enabled) {
            services.showDatePicker(dateUtils.parseDate(model.getValue()), instance.container, dateBox, function (value) {
                dateBox.val(dateUtils.formatDate(value, "${yyyy}-${mm}-${dd}"));
                model.setValueFromInput(instance.getValueForModel());
            });
        }
    };

    this.showTimePicker = function () {
        if (enabled) {
            services.showTimePicker(dateUtils.parseDate(model.getValue()), instance.container, dateBox, function (value) {
                timeBox.val(dateUtils.formatDate(value, "${HH}:${MM}"));
                model.setValueFromInput(instance.getValueForModel());
            });
        }
    };

    this.getValueForModel = function () {
        return dateBox.val() + " " + timeBox.val() + ":00";
    };

    this.unmarkInvalid = function () {
        dateBox.removeClass('asf-invalidInput');
        timeBox.removeClass('asf-invalidInput');
    };

    this.markInvalid = function () {
        dateBox.addClass('asf-invalidInput');
        timeBox.addClass('asf-invalidInput');
    };

    this.updateValueFromModel = function () {
        var date = model.getValue();
        if (date == instance.getValueForModel()) {
            return;
        }
        if (!date) {
            dateBox.val('____-__-__');
            timeBox.val("00:00");
        } else {
            dateBox.val(date.substring(0, 10));
            timeBox.val(date.substring(11, 16));
        }
    };

    this.setEnabled = function (newEnabled) {
        enabled = newEnabled;
        if (enabled) {
            dateBox.removeClass('asf-disabledInput');
            timeBox.removeClass('asf-disabledInput');
        } else {
            dateBox.addClass('asf-disabledInput');
            timeBox.addClass('asf-disabledInput');
        }
    };
    this.setEnabled(!dataUtils.isReadOnly(model));

    dateBox.prop("readonly", true);
    timeBox.prop("readonly", true);

    this.updateValueFromModel();
};

MobileDateView.prototype = Object.create(MobileDateView.prototype);
MobileDateView.prototype.constructor = MobileDateView;

/***/ }),

/***/ 401:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _underscore = __webpack_require__(7);

var _underscore2 = _interopRequireDefault(_underscore);

var _backbone = __webpack_require__(39);

var _backbone2 = _interopRequireDefault(_backbone);

var _i18n = __webpack_require__(14);

var _i18n2 = _interopRequireDefault(_i18n);

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var newFileTemplate = _underscore2.default.template('<input type="text" class="ns-input" style="width:100%" placeholder="<%=i18n.tr(\'Введите название файла\')%>" />' + '<div style="text-align:center; margin-top:20px">' + '<button class="ns-approveButton ui-button ui-widget ui-state-default ui-corner-all ui-button-text-only" disabled="disabled"><%=i18n.tr("Готово")%></button> ' + '</div> ');

var NewFileView = _backbone2.default.View.extend({
    template: newFileTemplate,

    initialize: function initialize(options) {
        this.nodeId = options.nodeId;
        this.dataId = options.dataId;
    },

    events: {
        'change input': 'textChanged',
        'textInput input': 'textChanged',
        'input input': 'textChanged',
        'click button': 'submit'
    },

    textChanged: function textChanged() {
        this.$('button').prop('disabled', this.isEmpty());
    },

    isEmpty: function isEmpty() {
        var fileName = this.getInput().val();
        return !_underscore2.default.isString(fileName) || _underscore2.default.isEmpty(fileName);
    },

    show: function show() {
        this.getInput().val('');

        var dialog = this.dialog;
        if (dialog === void 0) {
            dialog = this.$el.dialog({
                modal: true,
                title: _i18n2.default.getString('Создание файла'),
                width: 400,
                height: 140
            });
            this.dialog = dialog;
        }
        dialog.show();
    },

    submit: function submit() {
        "use strict";

        var fileName = this.getInput().val();
        var encodedFileName = encodeURIComponent(fileName).replace(/%20/g, '+');

        var data = {
            name: encodedFileName
        };

        if (_underscore2.default.isString(this.nodeId) && this.nodeId) {
            data['nodeId'] = this.nodeId;
        }
        if (_underscore2.default.isString(this.dataId) && this.dataId) {
            data['dataId'] = this.dataId;
        }

        var view = this;
        api.createHtd(data, function (data) {
            view.trigger('file:created', data.fileId);
            view.remove();
        });
    },

    remove: function remove() {
        "use strict";

        if (!this.$el.dialog('isOpen')) return;
        this.$el.dialog('close');
        this.$el.dialog('destroy');
    },

    getInput: function getInput() {
        return this.$('input');
    },

    render: function render() {
        this.$el.empty();
        this.$el.append(this.template());
        return this;
    }
});

exports.default = NewFileView;

/***/ }),

/***/ 402:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = ClearTableButton;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _i18n = __webpack_require__(14);

var _i18n2 = _interopRequireDefault(_i18n);

var _domUtils = __webpack_require__(44);

var domUtils = _interopRequireWildcard(_domUtils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function ClearTableButton(tableView, cell) {
    var instance = this;

    this.div = (0, _jquery2.default)("<div>", { class: "asf-building-clearPageButton", title: _i18n2.default.tr("Очистить страницу") });
    this.div.click(function (e) {
        if (!confirm(_i18n2.default.tr("Вы действительно хот��те очистить страницу?"))) {
            return;
        }
        tableView.clear();
        domUtils.cancelEvent(e);
    });

    this.destroy = function () {
        instance.div.empty();
        instance.div.detach();
    };

    this.hide = function () {
        instance.div.hide();
    };

    this.show = function () {
        instance.div.css("display", "");
    };

    if (tableView.model.isPage()) {
        cell.append(this.div);
    }
};

/***/ }),

/***/ 403:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = TableColumnRuler;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _domUtils = __webpack_require__(44);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function TableColumnRuler(tableModel, tableView, columnIndex) {
    var instance = this;

    this.div = (0, _jquery2.default)("<div>", { class: "asf-tableColumnRuler" });
    this.left = (0, _jquery2.default)('<div>', { class: "asf-InlineBlock asf-addLeftRowButton" });
    this.widthButton = (0, _jquery2.default)('<div>', { class: "asf-InlineBlock asf-addTableWidthRowButton" });
    this.right = (0, _jquery2.default)('<div>', { class: "asf-InlineBlock asf-addRightRowButton" });
    this.delete = (0, _jquery2.default)('<div>', { class: "asf-InlineBlock asf-deleteButton" });

    if (!tableModel.isPage()) {
        this.div.css("z-index", 1);
    }

    this.div.append(this.left);
    this.div.append(this.widthButton);
    this.div.append(this.delete);
    this.div.append(this.right);

    this.destroy = function () {
        instance.div.empty();
        instance.div.detach();
    };

    this.hide = function () {
        instance.div.hide();
    };

    this.show = function () {
        instance.div.css("display", "");
    };

    this.left.click(function (e) {
        tableView.addColumn(columnIndex);
        (0, _domUtils.cancelEvent)(e);
    });

    this.widthButton.click(function (e) {

        tableView.showWidthSettings(columnIndex);

        (0, _domUtils.cancelEvent)(e);
    });

    this.right.click(function (e) {
        tableView.addColumn(columnIndex + 1);
        (0, _domUtils.cancelEvent)(e);
    });

    this.delete.click(function (e) {
        tableView.deleteColumn(columnIndex);
        (0, _domUtils.cancelEvent)(e);
    });
    this.div.click(function (e) {
        tableModel.selection.selectColumn(columnIndex, e.ctrlKey);
        (0, _domUtils.cancelEvent)(e);
    });
};

/***/ }),

/***/ 404:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = TableRowRuler;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _domUtils = __webpack_require__(44);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function TableRowRuler(tableModel, tableView, rowIndex, span) {
    var instance = this;

    this.div = (0, _jquery2.default)("<div>", { class: "asf-tableRowRuler" });
    this.up = (0, _jquery2.default)('<div>', { class: "asf-InlineBlock asf-addUpRowButton" });
    this.down = (0, _jquery2.default)('<div>', { class: "asf-InlineBlock asf-addDownRowButton" });
    this.delete = (0, _jquery2.default)('<div>', { class: "asf-InlineBlock asf-deleteButton" });
    this.span = span;

    if (!tableModel.isPage()) {
        this.div.css("z-index", 1);
    }

    this.div.append(this.up);
    this.div.append(this.delete);
    this.div.append(this.down);

    this.destroy = function () {
        instance.div.empty();
        instance.div.detach();
    };

    this.hide = function () {
        instance.div.hide();
    };

    this.show = function () {
        instance.div.css("display", "");
    };

    this.up.click(function (e) {
        tableView.addRow(rowIndex);
        (0, _domUtils.cancelEvent)(e);
    });

    this.down.click(function (e) {
        tableView.addRow(rowIndex + 1);
        (0, _domUtils.cancelEvent)(e);
    });

    this.delete.click(function (e) {
        tableView.deleteRow(rowIndex);
        (0, _domUtils.cancelEvent)(e);
    });
    this.div.click(function (e) {
        tableModel.selection.selectRow(rowIndex, e.ctrlKey);
        (0, _domUtils.cancelEvent)(e);
    });
};

/***/ }),

/***/ 405:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = SelectTableButton;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _i18n = __webpack_require__(14);

var _i18n2 = _interopRequireDefault(_i18n);

var _domUtils = __webpack_require__(44);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function SelectTableButton(tableView, cell) {
    var instance = this;

    this.div = (0, _jquery2.default)("<div>", { class: "asf-building-selectTableOuterButton", title: _i18n2.default.tr("Выделить таблицу") });
    this.div.click(function (e) {
        var ownerTablePage = tableView.playerView.getOwnerPage(tableView.model.asfProperty.id);
        ownerTablePage.selectOneView(tableView);
        (0, _domUtils.cancelEvent)(e);
    });

    this.destroy = function () {
        instance.div.empty();
        instance.div.detach();
    };

    this.hide = function () {
        instance.div.hide();
    };

    this.show = function () {
        instance.div.css("display", "");
    };

    if (!tableView.model.isPage()) {
        cell.append(this.div);
    }
};

/***/ }),

/***/ 406:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


window.i18n = {

    i18nOptions: {
        detectLngQS: 'locale',
        cookieName: 'locale',
        resGetPath: 'locales/__ns__/__lng__/__ns__.json',
        fallbackOnEmpty: true
    },
    namespace: "",
    locale: "",
    defaultLocale: "",
    messages: null,
    setLocale: function setLocale(locale) {
        i18n.locale = locale;
        i18n.cookie.setCookie(i18n.i18nOptions.cookieName, locale);
    },
    _init: function _init(namespace, defaultLocale) {
        if (i18n.messages == null) {
            i18n.namespace = namespace;
            i18n.defaultLocale = defaultLocale;
            i18n.cookie.decorateString();

            var locale = i18n.detectLanguage();
            var url = i18n.i18nOptions.resGetPath.replace(/__lng__/g, locale).replace(/__ns__/g, i18n.namespace);
            i18n._ajax({
                url: url,
                dataType: "json",
                async: false,
                success: function success(data) {
                    i18n.messages = data;
                    i18n.setLocale(locale);
                },
                error: function error() {
                    console.log("error loading " + url);
                    i18n.setLocale(i18n.defaultLocale);
                }
            });
        }
    },
    _ajax: function _ajax(options) {

        // v0.5.0 of https://github.com/goloroden/http.js
        var getXhr = function getXhr(callback) {
            // Use the native XHR object if the browser supports it.
            if (window.XMLHttpRequest) {
                return callback(null, new XMLHttpRequest());
            } else if (window.ActiveXObject) {
                // In Internet Explorer check for ActiveX versions of the XHR object.
                try {
                    return callback(null, new ActiveXObject("Msxml2.XMLHTTP"));
                } catch (e) {
                    return callback(null, new ActiveXObject("Microsoft.XMLHTTP"));
                }
            }

            // If no XHR support was found, throw an error.
            return callback(new Error());
        };

        var encodeUsingUrlEncoding = function encodeUsingUrlEncoding(data) {
            if (typeof data === 'string') {
                return data;
            }

            var result = [];
            for (var dataItem in data) {
                if (data.hasOwnProperty(dataItem)) {
                    result.push(encodeURIComponent(dataItem) + '=' + encodeURIComponent(data[dataItem]));
                }
            }

            return result.join('&');
        };

        var utf8 = function utf8(text) {
            text = text.replace(/\r\n/g, '\n');
            var result = '';

            for (var i = 0; i < text.length; i++) {
                var c = text.charCodeAt(i);

                if (c < 128) {
                    result += String.fromCharCode(c);
                } else if (c > 127 && c < 2048) {
                    result += String.fromCharCode(c >> 6 | 192);
                    result += String.fromCharCode(c & 63 | 128);
                } else {
                    result += String.fromCharCode(c >> 12 | 224);
                    result += String.fromCharCode(c >> 6 & 63 | 128);
                    result += String.fromCharCode(c & 63 | 128);
                }
            }

            return result;
        };

        var base64 = function base64(text) {
            var keyStr = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=';

            text = utf8(text);
            var result = '',
                chr1,
                chr2,
                chr3,
                enc1,
                enc2,
                enc3,
                enc4,
                i = 0;

            do {
                chr1 = text.charCodeAt(i++);
                chr2 = text.charCodeAt(i++);
                chr3 = text.charCodeAt(i++);

                enc1 = chr1 >> 2;
                enc2 = (chr1 & 3) << 4 | chr2 >> 4;
                enc3 = (chr2 & 15) << 2 | chr3 >> 6;
                enc4 = chr3 & 63;

                if (isNaN(chr2)) {
                    enc3 = enc4 = 64;
                } else if (isNaN(chr3)) {
                    enc4 = 64;
                }

                result += keyStr.charAt(enc1) + keyStr.charAt(enc2) + keyStr.charAt(enc3) + keyStr.charAt(enc4);
                chr1 = chr2 = chr3 = '';
                enc1 = enc2 = enc3 = enc4 = '';
            } while (i < text.length);

            return result;
        };

        var mergeHeaders = function mergeHeaders() {
            // Use the first header object as base.
            var result = arguments[0];

            // Iterate through the remaining header objects and add them.
            for (var i = 1; i < arguments.length; i++) {
                var currentHeaders = arguments[i];
                for (var header in currentHeaders) {
                    if (currentHeaders.hasOwnProperty(header)) {
                        result[header] = currentHeaders[header];
                    }
                }
            }

            // Return the merged headers.
            return result;
        };

        var ajax = function ajax(method, url, options, callback) {
            // Adjust parameters.
            if (typeof options === 'function') {
                callback = options;
                options = {};
            }

            // Set default parameter values.
            options.cache = options.cache || false;
            options.data = options.data || {};
            options.headers = options.headers || {};
            options.jsonp = options.jsonp || false;
            options.async = options.async === undefined ? true : options.async;

            // Merge the various header objects.
            var headers = mergeHeaders({
                'accept': '*/*',
                'content-type': 'application/x-www-form-urlencoded;charset=UTF-8'
            }, ajax.headers, options.headers);

            // Encode the data according to the content-type.
            var payload;
            if (headers['content-type'] === 'application/json') {
                payload = JSON.stringify(options.data);
            } else {
                payload = encodeUsingUrlEncoding(options.data);
            }

            // Specially prepare GET requests: Setup the query string, handle caching and make a JSONP call
            // if neccessary.
            if (method === 'GET') {
                // Setup the query string.
                var queryString = [];
                if (payload) {
                    queryString.push(payload);
                    payload = null;
                }

                // Handle caching.
                if (!options.cache) {
                    queryString.push('_=' + new Date().getTime());
                }

                // If neccessary prepare the query string for a JSONP call.
                if (options.jsonp) {
                    queryString.push('callback=' + options.jsonp);
                    queryString.push('jsonp=' + options.jsonp);
                }

                // Merge the query string and attach it to the url.
                queryString = queryString.join('&');
                if (queryString.length > 1) {
                    if (url.indexOf('?') > -1) {
                        url += '&' + queryString;
                    } else {
                        url += '?' + queryString;
                    }
                }

                // Make a JSONP call if neccessary.
                if (options.jsonp) {
                    var head = document.getElementsByTagName('head')[0];
                    var script = document.createElement('script');
                    script.type = 'text/javascript';
                    script.src = url;
                    head.appendChild(script);
                    return;
                }
            }

            // Since we got here, it is no JSONP request, so make a normal XHR request.
            getXhr(function (err, xhr) {
                if (err) return callback(err);

                // Open the request.
                xhr.open(method, url, options.async);

                // Set the request headers.
                for (var header in headers) {
                    if (headers.hasOwnProperty(header)) {
                        xhr.setRequestHeader(header, headers[header]);
                    }
                }

                // Handle the request events.
                xhr.onreadystatechange = function () {
                    if (xhr.readyState === 4) {
                        var data = xhr.responseText || '';

                        // If no callback is given, return.
                        if (!callback) {
                            return;
                        }

                        // Return an object that provides access to the data as text and JSON.
                        callback(xhr.status, {
                            text: function text() {
                                return data;
                            },

                            json: function json() {
                                return JSON.parse(data);
                            }
                        });
                    }
                };

                // Actually send the XHR request.
                xhr.send(payload);
            });
        };

        // Define the external interface.
        var http = {
            authBasic: function authBasic(username, password) {
                ajax.headers['Authorization'] = 'Basic ' + base64(username + ':' + password);
            },

            connect: function connect(url, options, callback) {
                return ajax('CONNECT', url, options, callback);
            },

            del: function del(url, options, callback) {
                return ajax('DELETE', url, options, callback);
            },

            get: function get(url, options, callback) {
                return ajax('GET', url, options, callback);
            },

            head: function head(url, options, callback) {
                return ajax('HEAD', url, options, callback);
            },

            headers: function headers(_headers) {
                ajax.headers = _headers || {};
            },

            isAllowed: function isAllowed(url, verb, callback) {
                i18n.options(url, function (status, data) {
                    callback(data.text().indexOf(verb) !== -1);
                });
            },

            options: function options(url, _options, callback) {
                return ajax('OPTIONS', url, _options, callback);
            },

            patch: function patch(url, options, callback) {
                return ajax('PATCH', url, options, callback);
            },

            post: function post(url, options, callback) {
                return ajax('POST', url, options, callback);
            },

            put: function put(url, options, callback) {
                return ajax('PUT', url, options, callback);
            },

            trace: function trace(url, options, callback) {
                return ajax('TRACE', url, options, callback);
            }
        };

        var methode = options.type ? options.type.toLowerCase() : 'get';

        http[methode](options.url, options, function (status, data) {
            // file: protocol always gives status code 0, so check for data
            if (status === 200 || status === 0 && data.text()) {
                options.success(data.json(), status, null);
            } else {
                options.error(data.text(), status, null);
            }
        });
    },

    detectLanguage: function detectLanguage() {
        var detectedLng;

        // get from qs
        var qsParm = [];
        if (typeof window !== 'undefined') {
            (function () {
                var query = window.location.search.substring(1);
                var parms = query.split('&');
                for (var i = 0; i < parms.length; i++) {
                    var pos = parms[i].indexOf('=');
                    if (pos > 0) {
                        var key = parms[i].substring(0, pos);
                        var val = parms[i].substring(pos + 1);
                        qsParm[key] = val;
                    }
                }
            })();
            if (qsParm[i18n.i18nOptions.detectLngQS]) {
                detectedLng = qsParm[i18n.i18nOptions.detectLngQS];
                console.log("detected '" + detectedLng + "' locale from url");
            }
        }

        // get from cookie
        if (!detectedLng) {
            detectedLng = i18n.cookie.getCookie(i18n.i18nOptions.cookieName);
            console.log("detected '" + detectedLng + "' locale from cookie");
        }

        // get from navigator
        if (!detectedLng && typeof navigator !== 'undefined') {
            detectedLng = navigator.language ? navigator.language : navigator.userLanguage;
            console.log("detected '" + detectedLng + "' locale from navigator");
        }

        //fallback
        if (!detectedLng) {
            detectedLng = i18n.defaultLocale;
            console.log("fallback to '" + detectedLng + "' locale");
        }

        return detectedLng;
    },
    getString: function getString(key) {
        if (i18n.messages != null && key in i18n.messages && i18n.messages[key] !== "") return i18n.messages[key];else return key;
    },
    tr: function tr(key) {
        if (i18n.messages != null && key in i18n.messages && i18n.messages[key] !== "") return i18n.messages[key];else return key;
    },
    getStringPlural: function getStringPlural(key, plural, n) {
        if (i18n.messages != null && key in i18n.messages && i18n.messages[key] !== "") {
            var rule = i18n.plural.rules[i18n.locale];
            var suffix = rule.numbers[rule.plurals(n)];
            var realKey = suffix > 0 ? key + "_plural_" + suffix : key;
            return i18n.messages[realKey];
        } else {
            return key;
        }
    },
    getCurrentLocale: function getCurrentLocale() {
        return i18n.locale;
    },
    cookie: {
        decorateString: function decorateString() {
            if (typeof String.prototype.trimLeft !== "function") {
                String.prototype.trimLeft = function () {
                    return this.replace(/^\s+/, "");
                };
            }
            if (typeof String.prototype.trimRight !== "function") {
                String.prototype.trimRight = function () {
                    return this.replace(/\s+$/, "");
                };
            }
            if (typeof Array.prototype.map !== "function") {
                Array.prototype.map = function (callback, thisArg) {
                    for (var i = 0, n = this.length, a = []; i < n; i++) {
                        if (i in this) a[i] = callback.call(thisArg, this[i]);
                    }
                    return a;
                };
            }
        },
        getCookie: function getCookie(name) {
            var nameEQ = name + "=";
            var ca = document.cookie.split(';');
            for (var i = 0; i < ca.length; i++) {
                var c = ca[i];
                while (c.charAt(0) == ' ') {
                    c = c.substring(1, c.length);
                }if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
            }
            return null;
        },
        // уcтанавливает cookie
        setCookie: function setCookie(name, value, props) {
            props = props || {};
            var exp = props.expires;
            if (typeof exp == "number" && exp) {
                var d = new Date();
                d.setTime(d.getTime() + exp * 1000);
                exp = props.expires = d;
            }
            if (exp && exp.toUTCString) {
                props.expires = exp.toUTCString();
            }

            value = encodeURIComponent(value);
            var updatedCookie = name + "=" + value;
            for (var propName in props) {
                updatedCookie += "; " + propName;
                var propValue = props[propName];
                if (propValue !== true) {
                    updatedCookie += "=" + propValue;
                }
            }
            document.cookie = updatedCookie;
        }
    },
    plural: {
        rules: {
            "ach": {
                "name": "Acholi",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n > 1);
                }
            },
            "af": {
                "name": "Afrikaans",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "ak": {
                "name": "Akan",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n > 1);
                }
            },
            "am": {
                "name": "Amharic",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n > 1);
                }
            },
            "an": {
                "name": "Aragonese",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "ar": {
                "name": "Arabic",
                "numbers": [0, 1, 2, 3, 11, 100],
                "plurals": function plurals(n) {
                    return Number(n === 0 ? 0 : n == 1 ? 1 : n == 2 ? 2 : n % 100 >= 3 && n % 100 <= 10 ? 3 : n % 100 >= 11 ? 4 : 5);
                }
            },
            "arn": {
                "name": "Mapudungun",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n > 1);
                }
            },
            "ast": {
                "name": "Asturian",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "ay": {
                "name": 'Aymar\xE1',
                "numbers": [1],
                "plurals": function plurals(n) {
                    return 0;
                }
            },
            "az": {
                "name": "Azerbaijani",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "be": {
                "name": "Belarusian",
                "numbers": [1, 2, 5],
                "plurals": function plurals(n) {
                    return Number(n % 10 == 1 && n % 100 != 11 ? 0 : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2);
                }
            },
            "bg": {
                "name": "Bulgarian",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "bn": {
                "name": "Bengali",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "bo": {
                "name": "Tibetan",
                "numbers": [1],
                "plurals": function plurals(n) {
                    return 0;
                }
            },
            "br": {
                "name": "Breton",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n > 1);
                }
            },
            "bs": {
                "name": "Bosnian",
                "numbers": [1, 2, 5],
                "plurals": function plurals(n) {
                    return Number(n % 10 == 1 && n % 100 != 11 ? 0 : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2);
                }
            },
            "ca": {
                "name": "Catalan",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "cgg": {
                "name": "Chiga",
                "numbers": [1],
                "plurals": function plurals(n) {
                    return 0;
                }
            },
            "cs": {
                "name": "Czech",
                "numbers": [1, 2, 5],
                "plurals": function plurals(n) {
                    return Number(n == 1 ? 0 : n >= 2 && n <= 4 ? 1 : 2);
                }
            },
            "csb": {
                "name": "Kashubian",
                "numbers": [1, 2, 5],
                "plurals": function plurals(n) {
                    return Number(n == 1 ? 0 : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2);
                }
            },
            "cy": {
                "name": "Welsh",
                "numbers": [1, 2, 3, 8],
                "plurals": function plurals(n) {
                    return Number(n == 1 ? 0 : n == 2 ? 1 : n != 8 && n != 11 ? 2 : 3);
                }
            },
            "da": {
                "name": "Danish",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "de": {
                "name": "German",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "dz": {
                "name": "Dzongkha",
                "numbers": [1],
                "plurals": function plurals(n) {
                    return 0;
                }
            },
            "el": {
                "name": "Greek",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "en": {
                "name": "English",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "eo": {
                "name": "Esperanto",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "es": {
                "name": "Spanish",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "es_ar": {
                "name": "Argentinean Spanish",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "et": {
                "name": "Estonian",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "eu": {
                "name": "Basque",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "fa": {
                "name": "Persian",
                "numbers": [1],
                "plurals": function plurals(n) {
                    return 0;
                }
            },
            "fi": {
                "name": "Finnish",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "fil": {
                "name": "Filipino",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n > 1);
                }
            },
            "fo": {
                "name": "Faroese",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "fr": {
                "name": "French",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n > 1);
                }
            },
            "fur": {
                "name": "Friulian",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "fy": {
                "name": "Frisian",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "ga": {
                "name": "Irish",
                "numbers": [1, 2, 3, 7, 11],
                "plurals": function plurals(n) {
                    return Number(n == 1 ? 0 : n == 2 ? 1 : n < 7 ? 2 : n < 11 ? 3 : 4);
                }
            },
            "gd": {
                "name": "Scottish Gaelic",
                "numbers": [1, 2, 3, 20],
                "plurals": function plurals(n) {
                    return Number(n == 1 || n == 11 ? 0 : n == 2 || n == 12 ? 1 : n > 2 && n < 20 ? 2 : 3);
                }
            },
            "gl": {
                "name": "Galician",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "gu": {
                "name": "Gujarati",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "gun": {
                "name": "Gun",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n > 1);
                }
            },
            "ha": {
                "name": "Hausa",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "he": {
                "name": "Hebrew",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "hi": {
                "name": "Hindi",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "hr": {
                "name": "Croatian",
                "numbers": [1, 2, 5],
                "plurals": function plurals(n) {
                    return Number(n % 10 == 1 && n % 100 != 11 ? 0 : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2);
                }
            },
            "hu": {
                "name": "Hungarian",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "hy": {
                "name": "Armenian",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "ia": {
                "name": "Interlingua",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "id": {
                "name": "Indonesian",
                "numbers": [1],
                "plurals": function plurals(n) {
                    return 0;
                }
            },
            "is": {
                "name": "Icelandic",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n % 10 != 1 || n % 100 == 11);
                }
            },
            "it": {
                "name": "Italian",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "ja": {
                "name": "Japanese",
                "numbers": [1],
                "plurals": function plurals(n) {
                    return 0;
                }
            },
            "jbo": {
                "name": "Lojban",
                "numbers": [1],
                "plurals": function plurals(n) {
                    return 0;
                }
            },
            "jv": {
                "name": "Javanese",
                "numbers": [0, 1],
                "plurals": function plurals(n) {
                    return Number(n !== 0);
                }
            },
            "ka": {
                "name": "Georgian",
                "numbers": [1],
                "plurals": function plurals(n) {
                    return 0;
                }
            },
            "kk": {
                "name": "Kazakh",
                "numbers": [1],
                "plurals": function plurals(n) {
                    return 0;
                }
            },
            "km": {
                "name": "Khmer",
                "numbers": [1],
                "plurals": function plurals(n) {
                    return 0;
                }
            },
            "kn": {
                "name": "Kannada",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "ko": {
                "name": "Korean",
                "numbers": [1],
                "plurals": function plurals(n) {
                    return 0;
                }
            },
            "ku": {
                "name": "Kurdish",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "kw": {
                "name": "Cornish",
                "numbers": [1, 2, 3, 4],
                "plurals": function plurals(n) {
                    return Number(n == 1 ? 0 : n == 2 ? 1 : n == 3 ? 2 : 3);
                }
            },
            "ky": {
                "name": "Kyrgyz",
                "numbers": [1],
                "plurals": function plurals(n) {
                    return 0;
                }
            },
            "lb": {
                "name": "Letzeburgesch",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "ln": {
                "name": "Lingala",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n > 1);
                }
            },
            "lo": {
                "name": "Lao",
                "numbers": [1],
                "plurals": function plurals(n) {
                    return 0;
                }
            },
            "lt": {
                "name": "Lithuanian",
                "numbers": [1, 2, 10],
                "plurals": function plurals(n) {
                    return Number(n % 10 == 1 && n % 100 != 11 ? 0 : n % 10 >= 2 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2);
                }
            },
            "lv": {
                "name": "Latvian",
                "numbers": [0, 1, 2],
                "plurals": function plurals(n) {
                    return Number(n % 10 == 1 && n % 100 != 11 ? 0 : n !== 0 ? 1 : 2);
                }
            },
            "mai": {
                "name": "Maithili",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "mfe": {
                "name": "Mauritian Creole",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n > 1);
                }
            },
            "mg": {
                "name": "Malagasy",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n > 1);
                }
            },
            "mi": {
                "name": "Maori",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n > 1);
                }
            },
            "mk": {
                "name": "Macedonian",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n == 1 || n % 10 == 1 ? 0 : 1);
                }
            },
            "ml": {
                "name": "Malayalam",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "mn": {
                "name": "Mongolian",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "mnk": {
                "name": "Mandinka",
                "numbers": [0, 1, 2],
                "plurals": function plurals(n) {
                    return Number(0 ? 0 : n == 1 ? 1 : 2);
                }
            },
            "mr": {
                "name": "Marathi",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "ms": {
                "name": "Malay",
                "numbers": [1],
                "plurals": function plurals(n) {
                    return 0;
                }
            },
            "mt": {
                "name": "Maltese",
                "numbers": [1, 2, 11, 20],
                "plurals": function plurals(n) {
                    return Number(n == 1 ? 0 : n === 0 || n % 100 > 1 && n % 100 < 11 ? 1 : n % 100 > 10 && n % 100 < 20 ? 2 : 3);
                }
            },
            "nah": {
                "name": "Nahuatl",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "nap": {
                "name": "Neapolitan",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "nb": {
                "name": "Norwegian Bokmal",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "ne": {
                "name": "Nepali",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "nl": {
                "name": "Dutch",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "nn": {
                "name": "Norwegian Nynorsk",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "no": {
                "name": "Norwegian",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "nso": {
                "name": "Northern Sotho",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "oc": {
                "name": "Occitan",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n > 1);
                }
            },
            "or": {
                "name": "Oriya",
                "numbers": [2, 1],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "pa": {
                "name": "Punjabi",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "pap": {
                "name": "Papiamento",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "pl": {
                "name": "Polish",
                "numbers": [1, 2, 5],
                "plurals": function plurals(n) {
                    return Number(n == 1 ? 0 : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2);
                }
            },
            "pms": {
                "name": "Piemontese",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "ps": {
                "name": "Pashto",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "pt": {
                "name": "Portuguese",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "pt_br": {
                "name": "Brazilian Portuguese",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "rm": {
                "name": "Romansh",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "ro": {
                "name": "Romanian",
                "numbers": [1, 2, 20],
                "plurals": function plurals(n) {
                    return Number(n == 1 ? 0 : n === 0 || n % 100 > 0 && n % 100 < 20 ? 1 : 2);
                }
            },
            "ru": {
                "name": "Russian",
                "numbers": [1, 2, 5],
                "plurals": function plurals(n) {
                    return Number(n % 10 == 1 && n % 100 != 11 ? 0 : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2);
                }
            },
            "sah": {
                "name": "Yakut",
                "numbers": [1],
                "plurals": function plurals(n) {
                    return 0;
                }
            },
            "sco": {
                "name": "Scots",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "se": {
                "name": "Northern Sami",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "si": {
                "name": "Sinhala",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "sk": {
                "name": "Slovak",
                "numbers": [1, 2, 5],
                "plurals": function plurals(n) {
                    return Number(n == 1 ? 0 : n >= 2 && n <= 4 ? 1 : 2);
                }
            },
            "sl": {
                "name": "Slovenian",
                "numbers": [5, 1, 2, 3],
                "plurals": function plurals(n) {
                    return Number(n % 100 == 1 ? 1 : n % 100 == 2 ? 2 : n % 100 == 3 || n % 100 == 4 ? 3 : 0);
                }
            },
            "so": {
                "name": "Somali",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "son": {
                "name": "Songhay",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "sq": {
                "name": "Albanian",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "sr": {
                "name": "Serbian",
                "numbers": [1, 2, 5],
                "plurals": function plurals(n) {
                    return Number(n % 10 == 1 && n % 100 != 11 ? 0 : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2);
                }
            },
            "su": {
                "name": "Sundanese",
                "numbers": [1],
                "plurals": function plurals(n) {
                    return 0;
                }
            },
            "sv": {
                "name": "Swedish",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "sw": {
                "name": "Swahili",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "ta": {
                "name": "Tamil",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "te": {
                "name": "Telugu",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "tg": {
                "name": "Tajik",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n > 1);
                }
            },
            "th": {
                "name": "Thai",
                "numbers": [1],
                "plurals": function plurals(n) {
                    return 0;
                }
            },
            "ti": {
                "name": "Tigrinya",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n > 1);
                }
            },
            "tk": {
                "name": "Turkmen",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "tr": {
                "name": "Turkish",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n > 1);
                }
            },
            "tt": {
                "name": "Tatar",
                "numbers": [1],
                "plurals": function plurals(n) {
                    return 0;
                }
            },
            "ug": {
                "name": "Uyghur",
                "numbers": [1],
                "plurals": function plurals(n) {
                    return 0;
                }
            },
            "uk": {
                "name": "Ukrainian",
                "numbers": [1, 2, 5],
                "plurals": function plurals(n) {
                    return Number(n % 10 == 1 && n % 100 != 11 ? 0 : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? 1 : 2);
                }
            },
            "ur": {
                "name": "Urdu",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "uz": {
                "name": "Uzbek",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n > 1);
                }
            },
            "vi": {
                "name": "Vietnamese",
                "numbers": [1],
                "plurals": function plurals(n) {
                    return 0;
                }
            },
            "wa": {
                "name": "Walloon",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n > 1);
                }
            },
            "wo": {
                "name": "Wolof",
                "numbers": [1],
                "plurals": function plurals(n) {
                    return 0;
                }
            },
            "yo": {
                "name": "Yoruba",
                "numbers": [1, 2],
                "plurals": function plurals(n) {
                    return Number(n != 1);
                }
            },
            "zh": {
                "name": "Chinese",
                "numbers": [1],
                "plurals": function plurals(n) {
                    return 0;
                }
            }
        }
    }
};

/***/ }),

/***/ 407:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.Navigations = exports.Navigation = exports.ListView = exports.ListItem = undefined;

var _backbone = __webpack_require__(43);

var _backbone2 = _interopRequireDefault(_backbone);

var _underscore = __webpack_require__(7);

var _underscore2 = _interopRequireDefault(_underscore);

var _backbone3 = __webpack_require__(39);

var _backbone4 = _interopRequireDefault(_backbone3);

var _dataModel = __webpack_require__(283);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var ListItem = exports.ListItem = _backbone2.default.ItemView.extend({
    tagName: 'li',
    model: _dataModel.DataModel,
    className: 'list-item',
    events: {
        'dblclick': 'itemDoubleClicked',
        'click': 'itemClicked'
    },
    modelEvents: {
        'change': 'render',
        'reset': 'render'
    },
    onRender: function onRender() {
        if (this.model.isSelected()) {
            this.$el.addClass('selected');
        } else {
            this.$el.removeClass('selected');
        }
    },
    timer: 0,
    delay: 200,
    prevent: false,
    itemClicked: function itemClicked(event) {
        this.triggerMethod('item:clicked', this.model);
    },
    itemDoubleClicked: function itemDoubleClicked(event) {
        this.triggerMethod('item:doubleclicked', this.model);
    }
});

var ListView = exports.ListView = _backbone2.default.CompositeView.extend({
    className: 'list-container',
    ui: {
        toolbar: 'strong',
        list: '.list'
    },
    events: {
        "scroll @ui.list": "onScrollEvent"
    },
    childViewContainer: 'ul',
    templateHelpers: {
        getTitle: function getTitle() {
            return this.title;
        }
    },
    model: new _backbone4.default.Model({ title: '' }),

    getTemplate: function getTemplate() {
        return _underscore2.default.template('<div class="list-toolbar"><strong><%= getTitle()%></strong></div>' + '<ul class="list" ></ul>');
    },

    setTitle: function setTitle(title) {
        if (!_underscore2.default.isUndefined(title)) {
            this.model.set('title', title);
        }
    },
    onScrollEvent: function onScrollEvent() {
        console.log('event');
        var reachedEnd = this.$el.scrollTop() + this.$el.innerHeight() >= this.$el[0].scrollHeight;
        if (reachedEnd) {
            this.triggerMethod('onScrollReachedEnd', this);
        }
    }
});

/**
 * Элемент выборки навигации
 */
var Navigation = exports.Navigation = _backbone2.default.ItemView.extend({
    tagName: 'li',
    events: {
        'click': 'navItemClicked'
    },
    onRender: function onRender() {
        if (this.model.isSelected()) {
            this.$el.addClass('selected');
        } else {
            this.$el.removeClass('selected');
        }
    },
    navItemClicked: function navItemClicked() {
        var role = this.model.get('role');
        this.triggerMethod('navigation:clicked', role);

        this.$el.siblings().removeClass('selected');
        this.$el.addClass('selected');
    },
    attributes: function attributes() {
        return {
            'data-role': this.model.get('role')
        };
    },
    getTemplate: function getTemplate() {
        return _underscore2.default.template('<%=name%>');
    }
});

/**
 * Меню навигации
 */
var Navigations = exports.Navigations = _backbone2.default.CollectionView.extend({
    tagName: 'ul',
    className: 'chooser-navigation',
    childView: Navigation
});

/***/ }),

/***/ 408:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.TreeRoot = exports.TreeView = undefined;

var _backbone = __webpack_require__(43);

var _backbone2 = _interopRequireDefault(_backbone);

var _underscore = __webpack_require__(7);

var _underscore2 = _interopRequireDefault(_underscore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TreeView = exports.TreeView = _backbone2.default.CompositeView.extend({
    tagName: "li",
    childView: undefined,
    childViewContainer: 'ul',
    initialize: function initialize() {
        this.collection = this.model.get('nodes');
    },
    onRender: function onRender() {
        if (_underscore2.default.isUndefined(this.collection)) {
            this.$("ul:first").remove();
        }

        this.$el.toggleClass('selected', this.model.isSelected());
        this.$el.toggleClass('open', this.model.isOpened());
    },
    modelEvents: {
        'change': 'onModelChange'
    },
    events: {
        'click label': 'onTreeItemClick',
        'click i.node': 'onTreeItemToggled'
    },
    childEvents: {
        'treenode:clicked': 'onChildTreeNodeClicked',
        'treenode:show': 'onChildTreeNodeShow',
        'treenode:hide': 'onChildTreeNodeHide'
    },
    onModelChange: function onModelChange() {
        this.collection = this.model.get('nodes');
        this.render();
    },
    onTreeItemClick: function onTreeItemClick(event) {
        event.stopPropagation();
        this.triggerMethod('treenode:clicked', this.model);
    },
    onChildTreeNodeClicked: function onChildTreeNodeClicked(event, model) {
        this.triggerMethod('treenode:clicked', model);
    },
    onTreeItemToggled: function onTreeItemToggled(event) {
        event.stopPropagation();
        if (!this.$el.hasClass('open')) {
            this.triggerMethod('treenode:show', this.model);
        } else {
            this.triggerMethod('treenode:hide', this.model);
        }
    },
    onChildTreeNodeShow: function onChildTreeNodeShow(event, model) {
        this.triggerMethod('treenode:show', model);
    },
    onChildTreeNodeHide: function onChildTreeNodeHide(event, model) {
        this.triggerMethod('treenode:hide', model);
    },
    getTemplate: function getTemplate() {
        return _underscore2.default.template('<% if(hasChild) { %>' + '<i class="node"></i>' + '<% } else { %>' + '<i class=""></i>' + '<% } %>' + '<i class="tree-item-icon"></i>' + '<label><%= nodeName %></label>' + '<ul></ul>');
    }
});

var TreeRoot = exports.TreeRoot = _backbone2.default.CollectionView.extend({
    tagName: "ul",
    className: 'tree-navigation',
    childView: TreeView,

    setCurrentTreeNode: function setCurrentTreeNode(treeNode) {
        !_underscore2.default.isUndefined(this.currentTreeNode) && this.currentTreeNode.setSelected(false);
        this.currentTreeNode = treeNode;
        treeNode.setSelected(true);
    },

    getCurrentTreeNode: function getCurrentTreeNode() {
        return this.currentTreeNode;
    },

    fetchChildData: function fetchChildData(model, callback) {
        model.fetchChildFolders(function (collection) {
            if (collection.get('nodes').length == 0) {
                model.setHasChild(false);
            }
            !_underscore2.default.isUndefined(callback) && callback(collection);
        });
    },
    /**
     * Рекурсивно проверяет выбран ли дочерний элемент
     * @param collection коллекция
     * @returns {boolean}
     */
    hasSelectedChild: function hasSelectedChild(collection) {
        var self = this;
        if (!_underscore2.default.isEmpty(collection.findWhere({ selected: true }))) {
            return true;
        }
        return _underscore2.default.some(collection.models, function (model) {
            if (!_underscore2.default.isUndefined(model.get('nodes')) && self.hasSelectedChild(model.get('nodes'))) {
                return true;
            }
        });
    }
});

/***/ }),

/***/ 409:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function($) {

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.DepartmentNodeCollection = exports.TreeNodeCollection = exports.DepartmentNode = exports.TreeNode = undefined;

var _backbone = __webpack_require__(39);

var _backbone2 = _interopRequireDefault(_backbone);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var TreeNode = exports.TreeNode = _backbone2.default.Model.extend({
    defaults: {
        'selected': false,
        'hasChild': false,
        'opened': false
    },
    initialize: function initialize(options) {
        var nodes = this.get("nodes");
        if (nodes) {
            this.nodes = new TreeNodeCollection(nodes);
            this.unset("nodes");
        }
    },
    getID: function getID() {
        return this.get('nodeID');
    },
    setID: function setID(nodeID) {
        this.set('nodeID', nodeID);
    },
    setName: function setName(name) {
        this.set('nodeName', name);
    },
    getName: function getName() {
        return this.get('nodeName');
    },
    setHasChild: function setHasChild(hasChild) {
        this.set('hasChild', hasChild);
    },
    setSelected: function setSelected(selected) {
        this.set('selected', selected);
    },
    isSelected: function isSelected() {
        return this.get('selected');
    },
    setOpened: function setOpened(opened) {
        this.set('opened', opened);
    },
    isOpened: function isOpened() {
        return this.get('opened');
    }
});

var DepartmentNode = exports.DepartmentNode = TreeNode.extend({
    initialize: function initialize(options) {
        this.setID(options.departmentID);
        this.setName(options.departmentName);
        this.setHasChild(options.hasChildDepartments);
    },
    updateModelInfo: function updateModelInfo() {
        this.isRoot = true;
        this.fetch({ reset: true });
    },
    getChildDepartments: function getChildDepartments() {
        this.isRoot = false;
        this.fetch();
    },
    url: function url() {
        var page = this.page;
        var recordCount = 30;
        var params = {
            departmentID: this.getID(),
            onlyPosition: false,
            onlyDepartments: true,
            locale: 'ru' // AS.OPTIONS.locale
        };
        return '/Synergy/rest/api/departments/content?' + $.param(params);
    },
    parse: function parse(response) {
        var self = this;
        if (this.isRoot && !_.isEmpty(response.departments)) {
            this.set(response.departments[0]);
            this.initialize(response.departments[0]);
            this.setSelected(true);
        } else {
            this.set('nodes', new DepartmentNodeCollection(response.departments));
        }
        return this;
    }
});

var TreeNodeCollection = exports.TreeNodeCollection = _backbone2.default.Collection.extend({
    model: TreeNode
});

var DepartmentNodeCollection = exports.DepartmentNodeCollection = _backbone2.default.Collection.extend({
    model: DepartmentNode
});
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ }),

/***/ 410:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _chooser = __webpack_require__(161);

var _chooser2 = _interopRequireDefault(_chooser);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var AddressLinkChooser = _chooser2.default.extend({
    setSelectedModel: function setSelectedModel(model) {
        this.model = model;
        this.setValue(model.getValue());
    },

    getSelectedModelUUID: function getSelectedModelUUID() {
        return this.model.get('uuid');
    }
});
exports.default = AddressLinkChooser;

/***/ }),

/***/ 411:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});

var _backbone = __webpack_require__(43);

var _backbone2 = _interopRequireDefault(_backbone);

var _underscore = __webpack_require__(7);

var _underscore2 = _interopRequireDefault(_underscore);

var _link = __webpack_require__(280);

var _link2 = _interopRequireDefault(_link);

var _chooser = __webpack_require__(161);

var _chooser2 = _interopRequireDefault(_chooser);

var _chooser3 = __webpack_require__(163);

var _chooser4 = _interopRequireDefault(_chooser3);

var _addresslink_dialog_layout = __webpack_require__(284);

var _addresslink_dialog_layout2 = _interopRequireDefault(_addresslink_dialog_layout);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * Виджет для выборки адресной книги.
 * В конструктор можно передать:
 * -region: регион где виджет должен отображаться (обязательный)
 * -editMode: если true то режим редактирования, если false режим чтения. по умолчанию true (не обязательный)
 * -isEnabled: если true то пользователь можеть выбирать записи из диалога. по умолчанию true (не обязательный)
 * -isMandatory: Обязательное ли поле.
 *
 * Публичные методы:
 * -setEnabled(enabled)
 * -setEditMode(editMode)
 * -setMandatory(mandatory)
 * -getSelectedValue()
 * @type {void|*}
 */
exports.default = _backbone2.default.LayoutView.extend({
    regions: {
        linkContainer: '#link-container',
        chooserContainer: '#chooser-container',
        dialogContainer: '#dialog-container'
    },
    /* Регион где виджет будет отображаться */
    region: 'undefined',
    /* Режим редактирования/чтения */
    editMode: true,
    /*  */
    isEnabled: true,
    /* Обязательное ли поле */
    isMandatory: false,
    /**
     * Конструктор для класса
     * @param options объект,
     * который может хранить в себе регион для отображения компонента
     * хранить дополнительные настройки
     */
    initialize: function initialize(options) {
        this.applyArguments(options);
        this.region.show(this);

        this.initChooserComponent();
        this.initLinkComponent();
        this.setEditMode(this.editMode);
    },

    applyArguments: function applyArguments(options) {
        this.region = options.region;
        if (!_underscore2.default.isUndefined(options.editMode)) {
            this.editMode = options.editMode;
        }
        if (!_underscore2.default.isUndefined(options.isEnabled)) {
            this.isEnabled = options.isEnabled;
        }
    },

    /**
     * Инициализируем компонент ссылки которое будет отображаться в режиме чтения
     */
    initLinkComponent: function initLinkComponent() {
        this.linkComponent = new _link2.default({
            model: new _chooser4.default()
        });
        // слушаем нажатие на ссылку
        this.linkComponent.listenTo(this.linkComponent, 'link:clicked', function () {
            console.log('link clicked');
        });
        this.showChildView('linkContainer', this.linkComponent);
    },

    /**
     * Инициализирует компонент выборки.
     * При нажатии кнопки выборки `...` инициализирует диалог и показывает его
     *
     */
    initChooserComponent: function initChooserComponent() {
        var self = this;
        self.chooserComponent = new _chooser2.default({
            model: new _chooser4.default()
        });
        // Инициализируем диалоговое окно если было нажато на кнопку выбрать
        self.chooserComponent.listenTo(self.chooserComponent, 'chooser:button:clicked', function () {
            self.initChooserDialogComponent();
        });
        self.showChildView('chooserContainer', self.chooserComponent);
        self.chooserComponent.setEnabled(self.isEnabled);
    },

    /**
     * Инициализирует диалоговое окно в котором можно будет выбрать запись.
     */
    initChooserDialogComponent: function initChooserDialogComponent() {
        var self = this;
        this.dialogComponent = new _addresslink_dialog_layout2.default({ title: 'Abeke' });
        this.dialogComponent.listenTo(this.dialogComponent, 'dialog:submit:clicked', function (selectedModel) {
            self.setChooserValue(selectedModel);
        });
        this.dialogComponent.listenTo(this.dialogComponent, 'close:clicked', function () {
            var chooserModel = self.chooserComponent.getSelectedModel();
            if (_underscore2.default.isUndefined(chooserModel) || _underscore2.default.isNull(chooserModel)) {
                self.chooserComponent.setErrored(true);
            }
        });
        this.showChildView('dialogContainer', this.dialogComponent);
    },

    /**
     * Метод вызывается при нажатии на кнопку выбрать в диалоговом окне.
     * Проставляет выбранную модель для режима редактирования и отображения
     * @param model
     */
    setChooserValue: function setChooserValue(model) {
        this.chooserComponent.setSelectedModel(model);
        this.linkComponent.setSelectedModel(model);
    },

    /**
     * Задает режим представления. Режим редактирования или отображения
     * @param editMode
     */
    setEditMode: function setEditMode(editMode) {
        this.editMode = editMode;
        if (this.editMode) {
            this.chooserComponent.$el.show();
            this.linkComponent.$el.hide();
        } else {
            this.chooserComponent.$el.hide();
            this.linkComponent.$el.show();
        }
    },

    /**
     * Задает значение для редактируемости виджета
     * @param enabled Редактируемый ли виджет
     */
    setEnabled: function setEnabled(enabled) {
        this.chooserComponent.setEnabled(enabled);
    },
    /**
     * Заполняет значение для обязательного поля
     * @param mandatory обязательное ли поле: true/false
     */
    setMandatory: function setMandatory(mandatory) {
        this.isMandatory = mandatory;
    },

    /**
     * Возвращает выбранную модель виджета
     * @returns {*}
     */
    getSelectedModel: function getSelectedModel() {
        return this.chooserComponent.getSelectedModel();
    },

    /**
     * Шаблон для виджета
     */
    getTemplate: function getTemplate() {
        return _underscore2.default.template('<div id="link-container"></div>' + '<div id="chooser-container"></div>' + '<div id="dialog-container"></div>');
    }
});

/***/ }),

/***/ 412:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.OrganizationTableComponent = exports.OrganizationRowView = undefined;

var _backbone = __webpack_require__(43);

var _backbone2 = _interopRequireDefault(_backbone);

var _underscore = __webpack_require__(7);

var _underscore2 = _interopRequireDefault(_underscore);

var _i18n = __webpack_require__(14);

var _i18n2 = _interopRequireDefault(_i18n);

var _addresslink_user_table_component = __webpack_require__(286);

var _addresslink_models = __webpack_require__(166);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

var OrganizationRowView = exports.OrganizationRowView = _addresslink_user_table_component.UserRowView.extend({
    tagName: "tr",
    getTemplate: function getTemplate() {
        return _underscore2.default.template('<td><%= name %></td>' + '<td><%= address %></td>');
    }
});

var OrganizationTableComponent = exports.OrganizationTableComponent = _backbone2.default.CompositeView.extend({
    childView: OrganizationRowView,
    childViewContainer: "tbody",
    className: 'addresslink-table',
    initializeCollection: function initializeCollection() {
        this.collection = new _addresslink_models.Organizations();
    },
    fetchPage: function fetchPage(page, searchQuery, callback) {
        this.collection.fetchPage(page, searchQuery, function (collection) {
            !_underscore2.default.isUndefined(callback) && callback(collection.page, collection.totalPage, collection);
        });
    },
    getTemplate: function getTemplate() {
        return _underscore2.default.template('<table width="100%">' + '<thead>' + '<tr>' + '<th width="50%">' + _i18n2.default.tr("Организация") + '</th>' + '<th width="50%">' + _i18n2.default.tr("Адрес") + '</th>' + '</tr>' + '</thead>' + '<tbody></tbody>' + '</table>');
    }
});

/***/ }),

/***/ 44:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Created by user on 15.07.16.
 */



Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.cancelEvent = cancelEvent;
exports.stopPropagation = stopPropagation;
exports.cancelEventDispatch = cancelEventDispatch;
exports.notMovingClick = notMovingClick;
exports.isElementVisible = isElementVisible;
exports.isEventForInput = isEventForInput;
exports.addDialogCloseListener = addDialogCloseListener;
exports.getUrlParam = getUrlParam;
exports.absoluteOffset = absoluteOffset;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * отмена события
 * @param evt
 */
function cancelEvent(evt) {
    stopPropagation(evt);
    evt.preventDefault();
}

function stopPropagation(evt) {
    evt.cancelBubble = true;
    evt.stopped = true;
    evt.stopPropagation();
}

/**
 * отменяем распространение события для заданного элемента
 * @param element
 * @param event
 */
function cancelEventDispatch(element, event) {
    element.on(event, function (evt) {
        cancelEvent(evt);
    });
}

/**
 * добавляем лисенер на клик который сработает только в том случае если до этого не было move
 * @param element
 * @param handler
 */
function notMovingClick(element, handler) {
    var moving = false;
    element.mousedown(function () {
        moving = false;
    });
    element.mousemove(function () {
        moving = true;
    });

    element.mouseup(function () {
        if (moving) {
            return;
        }
        handler(arguments);
    });
}

function isElementVisible($el) {
    var winTop = (0, _jquery2.default)(window).scrollTop();
    var winBottom = winTop + (0, _jquery2.default)(window).height();
    var elTop = $el.offset().top;
    var elBottom = elTop + $el.height();
    return elBottom <= winBottom && elTop >= winTop;
}

function isEventForInput(event) {
    return (0, _jquery2.default)(event.target).is(":focus");
}

function addDialogCloseListener(dialog, region, element) {
    dialog.listenTo(dialog, 'close:clicked', function () {
        dialog.destroy();
        region.empty();
        element.destroy();
    });
};

function getUrlParam(name) {
    var results = new RegExp('[\?&]' + name + '=([^&#]*)').exec(window.location.href);
    if (!results) {
        return null;
    }
    return decodeURIComponent(results[1]);
};

function absoluteOffset(element) {
    var top = 0,
        left = 0;
    do {
        top += element.offsetTop || 0;
        left += element.offsetLeft || 0;
        element = element.offsetParent;
    } while (element);

    return {
        top: top,
        left: left
    };
};

/***/ }),

/***/ 5:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function($) {

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getComponentLocale = getComponentLocale;
exports.validateLocale = validateLocale;
exports.isReadOnly = isReadOnly;
exports.getBaseAsfData = getBaseAsfData;
exports.addAsFormId = addAsFormId;
exports.getAsFormId = getAsFormId;
exports.updateDefinition = updateDefinition;
exports.checkDefaultProperties = checkDefaultProperties;
exports.updateHeaderComponents = updateHeaderComponents;
exports.checkDefinitionProperties = checkDefinitionProperties;
exports.correctAsfData = correctAsfData;
exports.extractDataSources = extractDataSources;
exports.getDataSource = getDataSource;
exports.getDefaultDefinition = getDefaultDefinition;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _underscore = __webpack_require__(7);

var _underscore2 = _interopRequireDefault(_underscore);

var _constants = __webpack_require__(2);

var _dataUtils = __webpack_require__(5);

var dataUtils = _interopRequireWildcard(_dataUtils);

var _componentUtils = __webpack_require__(15);

var compUtils = _interopRequireWildcard(_componentUtils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * полезные функции для работы с asfData и asfDefinition
 * @type {{getComponentLocale: dataUtils.getComponentLocale, validateLocale: dataUtils.validateLocale, isReadOnly: dataUtils.isReadOnly, getBaseAsfData: dataUtils.getBaseAsfData, addAsFormId: dataUtils.addAsFormId, updateDefinitionDefaults: dataUtils.updateDefinitionDefaults, correctAsfData: dataUtils.correctAsfData}}
 */
function getComponentLocale(asfProperty) {
    var locale = _constants.OPTIONS.locale;
    if (asfProperty.config && asfProperty.config.locale) {
        locale = asfProperty.config.locale;
    }
    return locale;
}

function validateLocale(locale) {
    if (locale == 'kk') {
        return 'kz';
    }
    return locale;
}

function isReadOnly(model) {
    return model.asfProperty.config && model.asfProperty.config['read-only'];
}

/**
 * создание базового объекта asfdata все компоненты должны брать его, и потом дополнять своими свойствами если необходимо
 * @param asfProperty
 * @param blockNumber
 * @param value
 * @param key
 */
function getBaseAsfData(asfProperty, blockNumber, value, key) {
    var base = {
        id: blockNumber ? asfProperty.id + "-b" + blockNumber : asfProperty.id,
        type: asfProperty.type
    };

    if (asfProperty.label) {
        base.label = asfProperty.label;
    }

    if (!_underscore2.default.isUndefined(value) && !_underscore2.default.isNull(value)) {
        base.value = value;
    }

    if (!_underscore2.default.isUndefined(key) && !_underscore2.default.isNull(key)) {
        base.key = key;
    }
    return base;
}

function addAsFormId(element, role, asfProperty) {
    (0, _jquery2.default)(element).attr(_constants.CMP_DOM_ATTR, getAsFormId(role, asfProperty));
}

function getAsFormId(role, asfProperty) {
    if (asfProperty && asfProperty.id) {
        return asfProperty.type + "." + role + "." + asfProperty.id;
    } else if (asfProperty && asfProperty.type) {
        return asfProperty.type + "." + role;
    } else {
        return role;
    }
}

/**
 * вот здесь я правлю definition
 * 1) значения по умолчанию были разложены по Asfproperty потому как мне так удобнее создавать модели
 * 2) компоненты заголовков динамических таблиц я добавляю в свойства headerComponents рядом с components,
 *    а в components оставляю только компоненты таблицы, при этом если заголовок отображается значение
 *    свойства row у элементов components Уменьшается на 1
 * @param definition
 * @returns {*}
 */
function updateDefinition(definition) {

    definition.properties.forEach(function (prop) {
        checkDefaultProperties(prop);
    });

    return definition;
}

function checkDefaultProperties(asfProperty) {
    if (!asfProperty.config) {
        asfProperty.config = {};
    }
    if (!asfProperty.style) {
        asfProperty.style = {};
    }
    if (asfProperty.properties) {
        asfProperty.properties.forEach(function (prop) {
            checkDefaultProperties(prop);
        });
    }
}

function updateHeaderComponents(asfProperty) {
    if (!asfProperty) {
        return;
    }
    if (!asfProperty.config) {
        // бывают формы где это не прописано, и во избежание ошибок
        asfProperty.config = {};
    }
    if (asfProperty.type !== _constants.WIDGET_TYPE.table) {
        return;
    }
    if (!asfProperty.config || !asfProperty.config.isHaveHeaders || !asfProperty.config.appendRows) {
        asfProperty.layout.headerComponents = [];
    } else {

        var components = [];
        var headerComponents = [];

        asfProperty.layout.components.forEach(function (component) {
            if (component.row == 0) {
                headerComponents.push(component);
            } else {
                component.row = component.row - 1;
                components.push(component);
            }
        });

        asfProperty.layout.headerComponents = headerComponents;
        asfProperty.layout.components = components;
        asfProperty.layout.rows = asfProperty.layout.rows - 1;
    }
    dataUtils.updateHeaderComponents(asfProperty.properties);
}

function checkDefinitionProperties(properties) {
    var errors = [];
    var existingIds = [];
    properties.forEach(function (prop) {
        if (existingIds.indexOf(prop.id) > -1) {
            errors.push({ id: prop.id, errorCode: _constants.BUILDER_ERROR_TYPE.sameIds });
        } else {
            existingIds.push(prop.id);
        }

        if (prop.type != _constants.WIDGET_TYPE.table) {
            return;
        }

        if (prop.properties.length === 0) {
            errors.push({ id: prop.id, errorCode: _constants.BUILDER_ERROR_TYPE.emptyTable });
        } else {
            errors.push.apply(errors, dataUtils.checkDefinitionProperties(prop.properties));
        }
    });
    return errors;
}

/**
 * здесь я исправляю asfData потому как компоненты статической таблицы почему-то лежат вместе с компонентами страницы,
 * теперь они вставляются в отдельный эл��мент (причем в этот элемент вставляются все корневые свойства,
 * потому как сама модель возьмет и найдет нужные значения а остальные проигнорирует
 * @param asfData
 * @param pageModels
 * @returns {*}
 */
function correctAsfData(asfData, pageModels) {
    // таблицы у нас могут быть только одинарной вложенности
    pageModels.forEach(function (pageModel) {
        pageModel.modelBlocks.forEach(function (modelBlock) {
            modelBlock.forEach(function (model) {

                if (model.asfProperty.type != _constants.WIDGET_TYPE.table) {
                    return;
                }
                if (model.isStatic()) {
                    asfData.data.push({
                        id: model.asfProperty.id,
                        type: _constants.WIDGET_TYPE.table,
                        data: asfData.data
                    });
                    model.asfProperty.tableCounterData = [];
                } else {
                    var tableProperties = model.asfProperty.properties;
                    var tableCounterData = [];
                    asfData.data.forEach(function (data) {
                        if (data.type != _constants.WIDGET_TYPE.counter) {
                            return;
                        }
                        tableProperties.forEach(function (p) {
                            if (p.type != _constants.WIDGET_TYPE.counter || p.id != data.id) {
                                return;
                            }
                            p.data = $.extend(true, {}, data);
                            tableCounterData.push(_jquery2.default.extend({}, data));
                        });
                    });
                    model.asfProperty.tableCounterData = tableCounterData;
                }
            });
        });
    });
    return asfData;
}

function extractDataSources(definition) {
    var dataSources = [];

    if (definition.datasources) {
        dataSources.push.apply(dataSources, definition.datasources);
    }

    definition.properties.forEach(function (property) {
        dataUtils.getDataSource(property, dataSources);
    });

    return dataSources;
}

function getDataSource(property, dataSources) {
    if (property.dataSource) {
        dataSources.push(property.dataSource);
    }
    if (property.properties) {
        property.properties.forEach(function (property) {
            dataUtils.getDataSource(property, dataSources);
        });
    }
}

function getDefaultDefinition() {
    return {
        "config": {},
        "properties": [],
        "pointers": [],
        "collations": [],
        "format": 1,
        "layout": {
            "totalPages": 1,
            "pages": [{
                "page": 1,
                "columns": 1,
                "rows": 1,
                "components": []
            }]
        }
    };
}
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ }),

/***/ 52:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/* WEBPACK VAR INJECTION */(function(jQuery) {

Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.createTable = createTable;
exports.createRow = createRow;
exports.createCell = createCell;
exports.createTh = createTh;
exports.createCellForComp = createCellForComp;
exports.createHeaderCell = createHeaderCell;
exports.createModels = createModels;
exports.createViews = createViews;
exports.createRows = createRows;
exports.createASFTable = createASFTable;
exports.maybeChangeWidth = maybeChangeWidth;
exports.extractComponentIds = extractComponentIds;
exports.extractBlockNumbers = extractBlockNumbers;
exports.getModelWithId = getModelWithId;
exports.shallowFindView = shallowFindView;
exports.getViewWithId = getViewWithId;
exports.getViewsWithType = getViewsWithType;
exports.getComponentById = getComponentById;
exports.getPropertyById = getPropertyById;

var _underscore = __webpack_require__(7);

var _underscore2 = _interopRequireDefault(_underscore);

var _componentUtils = __webpack_require__(15);

var compUtils = _interopRequireWildcard(_componentUtils);

var _styleUtils = __webpack_require__(21);

var styleUtils = _interopRequireWildcard(_styleUtils);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * утилита для манипулирования таблицей
 * @type {{}}
 */
"use strict";

/**
 * создание таблички
 * @param className
 * @param style
 * @returns {*}
 */
function createTable(className, style) {
    var table = jQuery('<table><thead></thead><tbody></tbody></table>');
    table.addClass(className);
    table.attr("cellSpacing", "0");
    table.attr("cellPadding", "0");
    styleUtils.applyTableStyle(table, style);
    return table;
}

/**
 * создания ряда таблички согласно asfLayout, style
 * @param asfLayout
 * @param style
 * @returns {*}
 */
function createRow(asfLayout, style, fixed) {
    var row = jQuery('<tr/>');
    var c = 0;
    for (; c < asfLayout.columns; c++) {
        row.append(createCell(c, asfLayout, style, fixed));
    }
    return row;
}

/**
 * создание ячейки таблички согласно asfLaytou styee
 * @param column
 * @param asfLayout
 * @param style
 * @returns {*}
 */
function createCell(column, asfLayout, style, fixed) {
    var cell = jQuery('<td></td>');
    cell.addClass("asf-cell");
    styleUtils.applyBorder(cell, style);
    styleUtils.applyCellStyle(cell, column, asfLayout, fixed);
    styleUtils.applyColorStyle(cell, style);
    cell.append(jQuery('<div/>', { class: 'asf-emptyCell' }));
    return cell;
}

function createTh(column, asfLayout, style, fixed) {
    var cell = jQuery('<th></th>');
    styleUtils.applyCellStyle(cell, column, asfLayout, fixed);
    return cell;
}

function createCellForComp(comp, asfLayout, style, fixed) {
    if (comp === null) return;
    var $cell = createCell(comp.column, asfLayout, style, fixed);
    $cell.attr('rowspan', comp.rowspan);
    $cell.attr('colspan', comp.colspan);
    return $cell;
}

/**
 * создаем элемент заголовка таблицы
 * @param value значение лдя перевода
 * @param locale локаль
 * @param width ширина
 * @param minWidth минимальная ширина
 * @param bordered граница
 * @returns {*}
 */
function createHeaderCell(value, locale, width, bordered) {
    var cell = jQuery('<th>', { class: "asf-headerCell " + (bordered ? "asf-borderedCell" : "") });
    if (value) {
        compUtils.setTranslatedValue(value, locale, cell);
    }
    if (width) {
        cell.css("width", width);
    }
    return cell;
}

/**
 * создание моделей данных для блока таблицы (если таблица статическа то для всей таблицы)
 * для моделей данных динамической таблицы в asfProperty будут добавлены переменные ownerTableId - идентификатор таблицы, и blockTmpIndex - переданный параметр
 * @param tableModel
 * @param header - создать модель заголовка или обсновной части
 * @param tableBlockIndex сквозной временный индекс блока таблицы, никак не связан с тем что будет в -b, для статической таблицы всегда null
 * @returns {Array}
 */
function createModels(tableModel, header, tableBlockIndex) {

    var cmpAsfProperty;
    var model;
    var models = [];

    var components = tableModel.asfProperty.layout.components;
    if (header) {
        components = tableModel.asfProperty.layout.headerComponents;
        if (!components) {
            components = [];
        }
    }

    var tableId = null;
    var cmpBlockIndex = null;
    if (!tableModel.isPage() && !tableModel.isStatic()) {
        tableId = tableModel.asfProperty.id;
        cmpBlockIndex = tableBlockIndex + "";
    }

    components = _underscore2.default.filter(components, _underscore2.default.property('id'));
    components.forEach(function (cmpPosition) {

        var column = cmpPosition.column;
        var tableCellWidth = styleUtils.getCellWidth(tableModel.asfProperty.layout, column);

        var originalAsfProperty = compUtils.getPropertyWithId(tableModel.asfProperty.properties, cmpPosition.id, false);

        if (originalAsfProperty == null) return;

        cmpAsfProperty = jQuery.extend(true, {}, originalAsfProperty);

        if (!cmpAsfProperty.style) {
            cmpAsfProperty.style = {};
        }
        if (!cmpAsfProperty.style.width) {
            cmpAsfProperty.style.maxWidth = tableCellWidth;
        }

        cmpAsfProperty.ownerTableId = tableId;
        cmpAsfProperty.tableBlockIndex = cmpBlockIndex;
        model = compUtils.createModel(cmpAsfProperty, tableModel.playerModel);
        if (model) {
            models.push(model);
        }
    });

    models.tableBlockIndex = tableBlockIndex;

    return models;
}

/**
 * создание отображения для блока
 * @param tableModel
 * @param models
 * @param playerView
 * @param editable
 * @param building
 * @returns {Array}
 */
function createViews(tableModel, models, playerView, editable, isForBuilder) {
    var views = [];
    models.forEach(function (model) {
        views.push(compUtils.createView(model, playerView, editable, isForBuilder));
    });
    return views;
}

/**
 * создание рядов для блока
 * @param tableModel
 * @returns {Array}
 */
function createRows(tableModel, fixed) {
    var rows = [];
    var row;

    var rowsCount = tableModel.asfProperty.layout.rows;

    for (var rowIndex = 0; rowIndex < rowsCount; rowIndex++) {
        row = createRow(tableModel.asfProperty.layout, tableModel.asfProperty.style, fixed);
        rows.push(row);
    }
    return rows;
}

function createASFTable(model) {
    var style = "asf-table";
    if (model.isPage()) {
        style += " asf-page";
    }
    var table = createTable(style, model.asfProperty.style);

    if (model.asfProperty.config.fixedLayout) {
        maybeChangeWidth(table, model.asfProperty);
        table.addClass('asf-fixedLayout');

        if (!model.playerModel.building) {
            table.addClass("asf-playerView");
        }

        if (model.playerModel.format === 1) {
            table.css("table-layout", "fixed");
        }
    }

    if (model.playerModel.building) {
        table.addClass("asf-buildingView");
    }

    return table;
}

function maybeChangeWidth(table, asfProperty, minColumnWidth) {
    if (!minColumnWidth) {
        minColumnWidth = 0;
    }
    table.css('width', "");
    if (!asfProperty || !asfProperty.layout || !asfProperty.layout.config) return;

    var widths = _underscore2.default.map(asfProperty.layout.config, function (c) {
        if (!c.width) return null;
        return styleUtils.parseWidth(c.width);
    });

    var isFixed = widths.length === asfProperty.layout.columns && _underscore2.default.all(widths, function (width) {
        return width && width.unit === 'px';
    });

    if (!isFixed) return;

    table.css('width', 'auto');
}

function extractComponentIds(components) {
    components = components || [];
    return components.map(function (component) {
        return component.id;
    });
}

function extractBlockNumbers(asfData, ids) {
    var re = new RegExp('(.+)-b([0-9]+)$');

    var blocksNumbers = [];

    if (!asfData.data) {
        return blocksNumbers;
    }

    asfData.data.forEach(function (data) {
        var matches = re.exec(data.id);
        if (matches && jQuery.inArray(matches[2], blocksNumbers) == -1) {
            if (ids && jQuery.inArray(matches[1], ids) == -1) {
                return;
            }
            blocksNumbers[blocksNumbers.length] = matches[2];
        }
    });
    return blocksNumbers;
}

function getModelWithId(cmpId, models, tableId, blockIndex) {
    for (var i = 0; i < models.length; i++) {
        if (models[i].asfProperty.id == cmpId) {
            return models[i];
        } else if (models[i].getModelWithId) {
            var m = models[i].getModelWithId(cmpId, tableId, blockIndex);
            if (m) {
                return m;
            }
        }
    }
}

/**
 * метод ищет отображение с заданным айдишником именно среди массива переданных отображений (вглубь не смотрим и не надо этого трогать)
 * @param cmpId
 * @param views
 * @returns {*}
 */
function shallowFindView(cmpId, views) {
    try {
        for (var i = 0; i < views.length; i++) {
            var asfProperty = views[i].model.asfProperty;

            if (asfProperty.id === cmpId) {
                return views[i];
            }
        }
    } catch (e) {
        console.log("cannot find view", cmpId, views);
    }
}

/**
 * метод ищет отображения с заданным айдишником вглубь
 * @param cmpId
 * @param views
 * @param tableId
 * @param blockIndex
 * @returns {*}
 */
function getViewWithId(cmpId, views, tableId, blockIndex) {
    try {
        for (var i = 0; i < views.length; i++) {
            var asfProperty = views[i].model.asfProperty;

            if (asfProperty.id === cmpId && asfProperty.ownerTableId == tableId && asfProperty.tableBlockIndex == blockIndex) {
                return views[i];
            } else if (views[i].getViewWithId) {
                var v = views[i].getViewWithId(cmpId, tableId, blockIndex);
                if (v) {
                    return v;
                }
            }
        }
    } catch (e) {
        console.log(e);
    }
}

function getViewsWithType(type, views, tableId, blockIndex) {
    var result = [];
    views = views || [];
    views.forEach(function (view) {
        if (view.model.asfProperty.type === type) {
            result.push(view);
        }
        if (view.getViewsWithType) {
            var subviews = view.getViewsWithType(type, tableId, blockIndex);
            if (subviews.length > 0) {
                result = result.concat(subviews);
            }
        }
    });
    return result;
}

function getComponentById(layout, id) {
    var result = null;
    layout.components.some(function (component) {
        if (component.id === id) {
            result = component;
            return true;
        }
    });
    return result;
}

function getPropertyById(properties, id) {
    var result = null;
    properties.some(function (property) {
        if (property.id === id) {
            result = property;
            return true;
        }
    });
    return result;
}
/* WEBPACK VAR INJECTION */}.call(exports, __webpack_require__(1)))

/***/ }),

/***/ 599:
/***/ (function(module, exports) {

// removed by extract-text-webpack-plugin

/***/ }),

/***/ 65:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.getColumnIds = exports.getRowIds = exports.insertColumn = exports.insertRow = exports.deleteColumn = exports.deleteRow = exports.makeCompMatrix = exports.decColumn = exports.decRow = exports.eqCol = exports.eqRow = exports.columnLens = exports.rowLens = undefined;
exports.cellToComp = cellToComp;
exports.map2d = map2d;
exports.compToCell = compToCell;
exports.endCompToCell = endCompToCell;
exports.sortLayout = sortLayout;
exports.filterLayout = filterLayout;
exports.defaultSpans = defaultSpans;
exports.eqId = eqId;
exports.pointEq = pointEq;
exports.find = find;
exports.curryFind = curryFind;
exports.findFull = findFull;
exports.curryFindFull = curryFindFull;
exports.isMerged = isMerged;
exports.isInsidePoint = isInsidePoint;
exports.isInsidePointNoEqual = isInsidePointNoEqual;
exports.makeEnd = makeEnd;
exports.isInside = isInside;
exports.doesIntersect = doesIntersect;
exports.doesOnlyIntersect = doesOnlyIntersect;
exports.filterInside = filterInside;
exports.getIntersect = getIntersect;
exports.getInside = getInside;
exports.applyCompToMatrix = applyCompToMatrix;
exports.makeMatrix = makeMatrix;
exports.makeOnesMatrix = makeOnesMatrix;
exports.makeLayoutMatrix = makeLayoutMatrix;
exports.findMatrixComp = findMatrixComp;
exports.findTdIndex = findTdIndex;
exports.deleteOnLens = deleteOnLens;
exports.insertOnLens = insertOnLens;
exports.getLensIds = getLensIds;
exports.findById = findById;
exports.changeColumnWidth = changeColumnWidth;
exports.difference = difference;

var _utils = __webpack_require__(82);

var _underscore = __webpack_require__(7);

var _underscore2 = _interopRequireDefault(_underscore);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function cellToComp(cell) {
    if (!cell) return cell;
    return defaultSpans({
        row: cell[0],
        column: cell[1]
    });
}

function map2d(f) {
    return function (matrix) {
        return _underscore2.default.reduce(matrix, function (rows, row, ri) {
            var newRow = _underscore2.default.reduce(row, function (row, cell, coli) {
                row.push(f(cell, ri, coli));
                return row;
            }, []);

            rows.push(newRow);
            return rows;
        }, []);
    };
}

function compToCell(_ref) {
    var row = _ref.row,
        column = _ref.column;

    return [row, column];
}

function endCompToCell(comp) {
    return compToCell(makeEnd(comp));
}

function sortF(row, col) {
    return row * 10000 + col;
}

function sortLayout(layout) {
    layout.components = _underscore2.default.sortBy(layout.components, function (comp) {
        return sortF(comp.row, comp.column);
    });
}

function filterLayout(layout, ids) {
    layout.components = _underscore2.default.filter(layout.components, function (comp) {
        if (comp.id === undefined) return true;
        return ids.contains(comp.id);
    });
}

function defaultSpans() {
    var comp = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

    return _underscore2.default.defaults(comp, {
        rowspan: 1,
        colspan: 1
    });
}

var rowLens = exports.rowLens = {
    view: _underscore2.default.property('row'),
    update: function update(value, comp) {
        comp.row = value;
        return comp;
    },
    len: function len(layout) {
        return layout.rows;
    },
    updateLen: function updateLen(value, layout) {
        layout.rows = value;
        return layout;
    },
    span: function span(_span) {
        return _span.rowspan;
    },
    updateSpan: function updateSpan(value, comp) {
        comp.rowspan = value;
        return comp;
    }
};

var columnLens = exports.columnLens = {
    view: _underscore2.default.property('column'),
    update: function update(value, comp) {
        comp.column = value;
        return comp;
    },
    len: function len(layout) {
        return layout.columns;
    },
    updateLen: function updateLen(value, layout) {
        layout.columns = value;
        return layout;
    },
    span: function span(comp) {
        return comp.colspan;
    },
    updateSpan: function updateSpan(value, comp) {
        comp.colspan = value;
        return comp;
    }
};

columnLens.other = rowLens;
rowLens.other = columnLens;

var eqRow = exports.eqRow = _utils.eqLens.bind(null, rowLens);
var eqCol = exports.eqCol = _utils.eqLens.bind(null, columnLens);

function eqId(id) {
    return function (comp) {
        return id === comp.id;
    };
}

rowLens.eqValue = eqRow;
columnLens.eqValue = eqCol;

var decRow = exports.decRow = _utils.changeIf.bind(null, _utils.dec, rowLens);
var decColumn = exports.decColumn = _utils.changeIf.bind(null, _utils.dec, columnLens);

function pointEqActual(p1, p2) {
    if (!p1 || !p2) return false;
    return p1.row === p2.row && p1.column === p2.column;
}

function pointEq(p1) {
    return function (p2) {
        return pointEqActual(p1, p2);
    };
}

function find(toFind, layout) {
    return _underscore2.default.find(layout.components, pointEq(toFind));
}

function curryFind(layout) {
    return function (toFind) {
        return find(toFind, layout);
    };
}

function findFull(toFind, layout) {
    toFind = defaultSpans(toFind);
    return _underscore2.default.find(layout.components, function (comp) {
        if (!pointEqActual(comp, toFind)) return false;
        comp = defaultSpans(comp);
        return comp.rowspan === toFind.rowspan && comp.colspan === toFind.colspan;
    });
}

function curryFindFull(layout) {
    return function (toFind) {
        return findFull(toFind, layout);
    };
}

function isMerged(comp) {
    comp = defaultSpans(comp);
    return comp.rowspan > 1 || comp.colspan > 1;
}

function isInsidePointActual(container, point) {
    var insideCols = container.column <= point.column && point.column <= container.column + container.colspan - 1;
    var insideRows = container.row <= point.row && point.row <= container.row + container.rowspan - 1;

    return insideRows && insideCols;
}

function isInsidePoint(container) {
    defaultSpans(container);
    return function (point) {
        return isInsidePointActual(container, point);
    };
}

/**
 * внутри ли точка (исключая левую верхнюю вершину)
 */
function isInsidePointNoEqual(container) {
    defaultSpans(container);
    return function (point) {
        if (pointEqActual(container, point)) return false;

        return isInsidePointActual(container, point);
    };
}

function makeEnd(comp) {
    defaultSpans(comp);
    return {
        row: comp.row + comp.rowspan - 1,
        column: comp.column + comp.colspan - 1
    };
}

function isInside(container) {
    var isInside = isInsidePoint(container);
    return function (comp) {
        return isInside(comp) && isInside(makeEnd(comp));
    };
}

function doesIntersect(container) {
    var compsInside = _underscore2.default.flatten(makeCompMatrix(container));
    return function (comp) {
        return _underscore2.default.some(compsInside, isInsidePoint(comp));
    };
}

function doesOnlyIntersect(container) {
    var inside = isInside(container);
    var intersect = doesIntersect(container);

    return function (comp) {
        return !inside(comp) && intersect(comp);
    };
}

function filterInside(comp, layout) {
    sortLayout(layout);
    return _underscore2.default.filter(layout.components, isInside(comp));
}

function getIntersect(comp, layout) {
    return _underscore2.default.filter(layout.components, doesOnlyIntersect(comp));
}

function getInside(comp, layout) {
    return _underscore2.default.filter(layout.components, isInside(comp));
}

function applyCompToMatrix(matrix, comp) {
    defaultSpans(comp);

    var rowspan = _underscore2.default.max([1, comp.rowspan]);
    var colspan = _underscore2.default.max([1, comp.colspan]);

    for (var rowI = comp.row; rowI <= comp.row + rowspan - 1; rowI++) {
        for (var colI = comp.column; colI <= comp.column + colspan - 1; colI++) {
            matrix[rowI][colI] = null;
        }
    }matrix[comp.row][comp.column] = _underscore2.default.extend({}, comp);

    return matrix;
}

function makeMatrix(f, comp) {
    defaultSpans(comp);
    return _underscore2.default.reduce(_underscore2.default.range(comp.row, comp.row + comp.rowspan), function (rows, rowI) {
        var cells = _underscore2.default.map(_underscore2.default.range(comp.column, comp.column + comp.colspan), function (colI) {
            return f([rowI, colI]);
        });
        rows.push(cells);
        return rows;
    }, []);
}

var makeCompMatrix = exports.makeCompMatrix = makeMatrix.bind(null, cellToComp);

function makeOnesMatrix(rows, cols) {
    return makeMatrix(cellToComp, {
        row: 0, column: 0, rowspan: rows, colspan: cols
    });
}

function makeLayoutMatrix(layout) {
    var matrix = makeOnesMatrix(layout.rows, layout.columns);
    return _underscore2.default.reduce(layout.components, applyCompToMatrix, matrix);
}

function findMatrixComp(matrix, comp) {
    return _underscore2.default.reject(matrix[comp.row], _underscore2.default.isNull)[comp.column];
}

function findTdIndex(matrix, cell) {
    var matrixRow = matrix[cell[0]];
    if (!matrixRow) return null;
    matrixRow = _underscore2.default.reject(matrixRow, _underscore2.default.isNull);
    return _underscore2.default.findIndex(matrixRow, pointEq(cellToComp(cell)));
}

function deleteOnLens(lens, index, layout) {
    sortLayout(layout);
    var comps = layout.components;
    comps = _underscore2.default.reject(comps, lens.eqValue(index));

    var rowOrColumn = defaultSpans({
        row: 0, column: 0
    });
    lens.update(index, rowOrColumn);
    lens.other.updateSpan(lens.other.len(layout), rowOrColumn);

    var intersect = doesOnlyIntersect(rowOrColumn);
    _underscore2.default.chain(comps).filter(intersect).each(function (comp) {
        return lens.updateSpan(lens.span(comp) - 1, comp);
    });

    comps = _underscore2.default.map(comps, (0, _utils.changeIf)(_utils.dec, lens, (0, _utils.gt)(index)));

    layout.components = comps;
    lens.updateLen(lens.len(layout) - 1, layout);
    return layout;
}

var deleteRow = exports.deleteRow = deleteOnLens.bind(null, rowLens);
var deleteColumn = exports.deleteColumn = deleteOnLens.bind(null, columnLens);

function insertOnLens(lens, index, layout) {
    sortLayout(layout);

    var comps = layout.components;
    comps = _underscore2.default.map(comps, (0, _utils.changeIf)(_utils.inc, lens, (0, _utils.gte)(index)));

    lens.updateLen(lens.len(layout) + 1, layout);

    var rowOrColumn = defaultSpans({
        row: 0, column: 0
    });
    lens.update(index, rowOrColumn);
    lens.other.updateSpan(lens.other.len(layout), rowOrColumn);

    var intersect = doesOnlyIntersect(rowOrColumn);

    _underscore2.default.chain(comps).filter(intersect).each(function (comp) {
        return lens.updateSpan(lens.span(comp) + 1, comp);
    });

    if (lens === columnLens) {
        if (layout.config) {
            _underscore2.default.map(layout.config, (0, _utils.changeIf)(_utils.inc, lens, (0, _utils.gte)(index)));
        }
    }

    return layout;
}

var insertRow = exports.insertRow = insertOnLens.bind(null, rowLens);
var insertColumn = exports.insertColumn = insertOnLens.bind(null, columnLens);

function getLensIds(lens, index, layout) {
    return _underscore2.default.chain(layout.components).filter((0, _utils.eqLens)(lens, index)).map(_underscore2.default.property('id')).value();
}

var getRowIds = exports.getRowIds = getLensIds.bind(null, rowLens);
var getColumnIds = exports.getColumnIds = getLensIds.bind(null, columnLens);

function findById(id, layout) {
    return _underscore2.default.find(layout.components, eqId(id));
}

function changeColumnWidth(column, width, layout) {
    var config = layout.config;
    if (!config) {
        layout.config = config = [];
    }

    var columnConfig = _underscore2.default.find(config, eqCol(column));

    if (!columnConfig) {
        columnConfig = { column: column };
        config.push(columnConfig);
    }

    columnConfig.width = width;
}

function difference(layout1, layout2) {
    return _underscore2.default.reject(layout1.components, curryFindFull(layout2));
}

/***/ }),

/***/ 71:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.BasicChooserEvent = undefined;
exports.default = BasicChooserDialog;

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

var _constants = __webpack_require__(2);

var _componentUtils = __webpack_require__(15);

var compUtils = _interopRequireWildcard(_componentUtils);

var _i18n = __webpack_require__(14);

var _i18n2 = _interopRequireDefault(_i18n);

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _SplitPane = __webpack_require__(219);

var _SplitPane2 = _interopRequireDefault(_SplitPane);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

/**
 * базовый компонент выбора элементов имеющих иерархическую структуру
 * состоит их 3 основных областей
 * стеки
 * дерево
 * список (включает заголовок списка и элементы списка)
 * Created by exile on 25.04.16.
 */

var BasicChooserEvent = exports.BasicChooserEvent = {
    /**
     * стэк выделен
     */
    stackSelected: "stackSelected",
    /**
     * дерево выделено
     */
    treeSelected: "treeSelected",

    /**
     * выделение ноды дерева
     */
    treeNodeSelected: "treeNodeSelected",

    /**
     * выделение элемента списка
     */
    itemSelected: "itemSelected",

    /**
     * снятие выделения с элемента списка
     */
    itemDeselected: "itemDeselected",

    /**
     * отображение списка
     */
    listShowed: "listShowed",
    /**
     * перегрузка списка
     */
    reloadList: "reloadList",

    /**
     * нажатие на кнопку принять (или двойной клик)
     */
    applyClicked: "applyClicked"
};

function BasicChooserDialog() {

    var CONTAINER_WIDTH = 800;
    var CONTAINER_HEIGHT = 500;

    var SPLIT_WIDTH = 782;
    var SPLIT_HEIGHT = 350;
    var SPLIT_TOP = 44;

    var SPLIT_WITHOUT_SEARCH_HEIGHT = 385;
    var SPLIT_WITHOUT_SEARCH_TOP = 9;

    var GAP = 9;

    var LEFT_WIDTH = 258;
    var STACK_HEIGHT = 26;

    var instance = this;
    compUtils.makeBus(this);

    var countInPart = 30;

    var options = {
        multiSelect: false,
        showSearchField: true,
        showSelectedStack: true, // отображение стека и списка выделенных элементов
        showListItemIcon: false, // имеет ли элемент списка иконку
        showListItemIconStyle: "", // стиль иконки элемента списка
        listItemIconSize: 50, // размер иконки элемента списка
        showListItemInfo: false, // имеет ли элемент списка лейбл информации
        showListItemStatus: false, // имеет ли элемент списка лейбл статуса
        lazyListLoading: true, // включать ли ненивую подгрузку списка
        placeHolderText: _i18n2.default.tr("Поиск"), // плейсхолдер поле поиск
        title: _i18n2.default.getString("Выберите..."), // загловок диалога
        canSelect: null, // функция которая определяет может ли быть выбран элемент или нет
        dblClick: null // функция двоного клика по элементу
    };

    this.contentContainer = (0, _jquery2.default)('<div/>', { class: 'ns-basicChooserDialogContent' });

    var splitPane = new _SplitPane2.default();

    var stacksContainer = (0, _jquery2.default)('<div/>', { class: 'ns-basicChooserStacks' });
    var treeContainer = (0, _jquery2.default)('<div/>', { class: 'ns-basicChooserTree' });

    var listContainer = (0, _jquery2.default)('<div/>', { class: 'ns-basicChooserListContainer' });
    var listTitleContainer = (0, _jquery2.default)('<div/>', { class: 'ns-basicChooserListTitleContainer' });
    var listTitle = (0, _jquery2.default)('<div/>', { class: 'ns-basicChooserListTitle' });
    var selectAll = (0, _jquery2.default)('<input/>', { type: "checkbox", class: 'ns-basicChooserSelectAll' });
    var selectAllLabel = (0, _jquery2.default)('<label/>', { class: "ns-checkBoxLabel ns-basicChooserSelectAllContainer" });

    var searchBox = (0, _jquery2.default)('<input/>', {
        type: 'text',
        class: 'ns-input ns-basicChooserSearchInput',
        placeholder: options.placeHolderText
    });
    var button = (0, _jquery2.default)('<button>', { class: "ns-approveButton ns-basicChooserApplyButton" }).button().html(_i18n2.default.tr("Выбрать"));

    var searchImage = (0, _jquery2.default)('<div>', { class: "ns-basicChooserSearchIcon" });

    // деревья
    var trees = {};

    // стеки
    var stacks = {};

    // списки
    var lists = {};

    // выделенные элементы
    var selectedItems = [];

    /**
     * инициализация диалога
     *
     * multiSelect - множественный выбор (boolean)
     * showSearchField - о��ображать поле поиска (boolean)
     * showSelectedStack - отображение стека и списка выделенных элементов (boolean)
     * showListItemIcon - имеет ли элемент списка иконку (boolean)
     * showListItemIconStyle - стиль иконки элемента списка (boolean)
     * listItemIconSize - размер иконки элемента списка (integer)
     * showListItemInfo - имеет ли элемент списка лейбл информации (boolean)
     * showListItemStatus - имеет ли элемент списка лейбл статуса (boolean)
     * lazyListLoading - включать ли ненивую подгрузку списка (boolean)
     * placeHolderText - плейсхолдер поле поиск (string)
     * title - заголовок диалога (string)
     * canSelect - функция которая определяет может ли быть выбран элемент или нет (function)
     * dblClick - функция двойного клика по элементу (function)
     *
     * @param customOptions
     */
    this.init = function (customOptions) {
        var paginator;
        if (!_.isUndefined(customOptions.multiSelect)) {
            options.multiSelect = customOptions.multiSelect;
        }
        if (!_.isUndefined(customOptions.showSearchField)) {
            options.showSearchField = customOptions.showSearchField;
        }
        if (!_.isUndefined(customOptions.showSelectAll)) {
            options.showSelectAll = customOptions.showSelectAll;
        }
        if (!_.isUndefined(customOptions.showListItemIcon)) {
            options.showListItemIcon = customOptions.showListItemIcon;
        }
        if (!_.isUndefined(customOptions.showListItemInfo)) {
            options.showListItemInfo = customOptions.showListItemInfo;
        }
        if (!_.isUndefined(customOptions.showListItemStatus)) {
            options.showListItemStatus = customOptions.showListItemStatus;
        }
        if (!_.isUndefined(customOptions.title)) {
            options.title = customOptions.title;
        }
        if (!_.isUndefined(customOptions.listItemIconSize)) {
            options.listItemIconSize = customOptions.listItemIconSize;
        }
        if (!_.isUndefined(customOptions.showListItemIconStyle)) {
            options.showListItemIconStyle = customOptions.showListItemIconStyle;
        }
        if (!_.isUndefined(customOptions.showSelectedStack)) {
            options.showSelectedStack = customOptions.showSelectedStack;
        }
        if (!_.isUndefined(customOptions.placeHolderText)) {
            options.placeHolderText = customOptions.placeHolderText;
            searchBox.attr("placeholder", options.placeHolderText);
        }
        if (!_.isUndefined(customOptions.lazyListLoading)) {
            options.lazyListLoading = customOptions.lazyListLoading;
        }
        if (!_.isUndefined(customOptions.canSelect)) {
            options.canSelect = customOptions.canSelect;
        }

        if (!_.isUndefined(customOptions.dblClick)) {
            options.dblClick = customOptions.dblClick;
        }
        if (!_.isUndefined(customOptions.width)) {
            CONTAINER_WIDTH = customOptions.width;
        }
        if (!_.isUndefined(customOptions.splitWidth)) {
            SPLIT_WIDTH = customOptions.splitWidth;
        }
        if (!_.isUndefined(customOptions.treeWidth)) {
            LEFT_WIDTH = customOptions.treeWidth;
        }
        if (!_.isUndefined(customOptions.height)) {
            CONTAINER_HEIGHT = customOptions.height;
            SPLIT_HEIGHT = CONTAINER_HEIGHT - 150;
            SPLIT_WITHOUT_SEARCH_HEIGHT = SPLIT_HEIGHT + 35;
            listContainer.css("height", SPLIT_WITHOUT_SEARCH_HEIGHT + "px");
        }
        if (!_.isUndefined(customOptions.paginatorComponent)) {
            paginator = customOptions.paginatorComponent;
        }

        if (options.showSearchField) {
            instance.contentContainer.append(searchBox);
            instance.contentContainer.append(searchImage);
        } else {
            SPLIT_HEIGHT = SPLIT_WITHOUT_SEARCH_HEIGHT;
            SPLIT_TOP = SPLIT_WITHOUT_SEARCH_TOP;
            listContainer.css("height", SPLIT_WITHOUT_SEARCH_HEIGHT + "px");
        }

        if (paginator) {
            var page = (0, _jquery2.default)('<span/>', { class: 'paginator-container' });
            page.append(paginator.el);
            instance.contentContainer.append(page);
        }

        if (options.multiSelect) {
            selectAllLabel.append(selectAll);
            selectAllLabel.append(_i18n2.default.tr("Выбрать всех"));
        }

        selectAll.click(function () {
            if (!selectAll.listId) {
                return;
            }
            var list = lists[selectAll.listId];
            if (selectAll.prop("checked")) {
                list.selectAll();
            } else {
                instance.setSelectedItems([]);
            }
        });

        instance.contentContainer.append(splitPane.splitContainer);
        splitPane.splitContainer.addClass("ns-basicChooserSplit");
        splitPane.leftContainer.append(stacksContainer);
        splitPane.leftContainer.append(treeContainer);

        splitPane.minDividerPosition = 86;
        splitPane.maxDividerPosition = 605;

        listTitleContainer.append(listTitle);
        listTitleContainer.append(selectAllLabel);

        splitPane.rightContainer.append(listTitleContainer);
        splitPane.rightContainer.append(listContainer);

        instance.contentContainer.append(button);

        splitPane.setPixelSize(SPLIT_WIDTH, SPLIT_HEIGHT);
        splitPane.splitContainer.css("top", SPLIT_TOP + "px");

        splitPane.setDividerPosition(LEFT_WIDTH);

        treeContainer.css("width", "100%");
        treeContainer.css("height", SPLIT_HEIGHT - 2);

        if (options.showSelectedStack) {

            instance.addListType("selected", function () {
                instance.setListData("selected", selectedItems, false);
            });

            instance.addStack("selected", _i18n2.default.tr("Выбранные"), null, false, false, function () {
                instance.showList("selected", true);
            });
        }

        button.attr("disabled", "true");
        button.css("margin-top", CONTAINER_HEIGHT - 94 + "px");
        if (CONTAINER_WIDTH === 1000) {
            button.css("margin-left", "20%");
        }

        button.click(function () {
            instance.close();
        });

        instance.on(BasicChooserEvent.itemSelected, function (event, item) {
            if (options.multiSelect) {
                selectedItems.push(item);
            } else {
                if (selectedItems.length === 1) {
                    instance.trigger(BasicChooserEvent.itemDeselected, [selectedItems[0]]);
                }
                selectedItems = [item];
            }
            instance.updateButtonState();
        });

        instance.on(BasicChooserEvent.itemDeselected, function (event, item) {
            selectedItems.some(function (selectedItem, selectedIndex) {
                if (item.id == selectedItem.id) {
                    selectedItems.splice(selectedIndex, 1);

                    instance.updateButtonState();

                    selectAll.prop("checked", false);

                    return true;
                }
            });
        });

        instance.on(BasicChooserEvent.listShowed, function (event, listId) {
            searchBox.listId = listId;
            selectAll.listId = listId;
            (0, _jquery2.default)(selectAll).prop("checked", false);
        });

        instance.on(BasicChooserEvent.treeSelected, function (event, treeId) {
            searchBox.treeId = treeId;
            selectAllLabel.show();
        });

        instance.on(BasicChooserEvent.stackSelected, function (event, stackId) {
            if (stackId == 'selected') {
                selectAllLabel.hide();
            } else {
                selectAllLabel.show();
            }
        });

        searchBox.keypress(function (event) {
            if (event.keyCode == _constants.KEY_CODES.ENTER && searchBox.listId && searchBox.treeId) {
                lists[searchBox.listId].load(false);
            }
        });

        searchImage.click(function (event) {
            if (searchBox.listId && searchBox.treeId) {
                lists[searchBox.listId].load(false);
            }
        });

        if (options.lazyListLoading) {
            listContainer.scroll(function (event) {
                var list = lists[searchBox.listId];
                if (listContainer[0].scrollHeight - listContainer[0].scrollTop - listContainer.height() < 100) {
                    list.load(true);
                }
            });
        }

        splitPane.bus.on(_constants.COMPONENT_EVENTS.resized, function () {
            var leftWidth = splitPane.position;
            var rightWidth = splitPane.width - splitPane.position - splitPane.dividerSize;
            var height = splitPane.height;
            console.log(leftWidth + " " + rightWidth + " " + height);
        });
    };

    this.isSelectionValid = function () {
        var valid = selectedItems.length > 0;
        selectedItems.some(function (selectedItem) {
            if (options.canSelect && !options.canSelect(selectedItem)) {
                valid = false;
                return true;
            }
        });
        return valid;
    };

    this.updateButtonState = function () {

        if (instance.isSelectionValid()) {
            button.removeAttr("disabled");
        } else {
            button.attr("disabled", "true");
        }
    };

    /**
     * добавление дерева
     * @param treeId идентификатор
     * @param openHandler лисенер открытия ноды
     * @param selectHandler лисенер закрытия ноды
     * @param getIcon функция получения иконки (если таковая нужна)
     * @param data данные
     * @param autoOpen  автооткрытие нод
     */
    this.addTree = function (treeId, openHandler, selectHandler, getIcon, data, autoOpen) {
        var tree = (0, _jquery2.default)('<div/>', { class: 'ns-tree' });

        var closed = (0, _jquery2.default)("<div/>", { class: "ns-tree-node-closed" });
        var opened = (0, _jquery2.default)("<div/>", { class: "ns-tree-node-open" });
        tree.tree({
            data: _.isUndefined(data) ? [] : data,
            autoOpen: _.isUndefined(autoOpen) ? false : true,
            dragAndDrop: false,
            closedIcon: closed,
            openedIcon: opened,
            slide: false,
            onCreateLi: function onCreateLi(node, $li) {
                if (!getIcon) {
                    if ($li.find("a").length === 0) {
                        $li.find("span").addClass("ns-margin12Important");
                    }
                } else {
                    var icon = getIcon(node);
                    if ($li.find("a").length === 0) {
                        icon.css("margin-left", "12px");
                        $li.find("span").before(icon);
                    } else {
                        $li.find("a").after(icon);
                    }
                }
            }
        });

        tree.loadRoots = function () {
            openHandler(null);
        };
        tree.bind('tree.open', function (e) {
            var parent_node = e.node;
            if (openHandler) {
                openHandler(parent_node, tree);
            }
        });
        tree.bind('tree.select', function (e) {
            instance.trigger(BasicChooserEvent.treeSelected, [treeId]);
            var parent_node = e.node;
            if (!parent_node) {
                return;
            }
            tree.lastSelection = parent_node;
            instance.trigger(BasicChooserEvent.treeNodeSelected, [treeId, parent_node.id]);
            if (selectHandler) {
                selectHandler(parent_node, tree);
            }
        });

        instance.on(BasicChooserEvent.treeSelected, function (event, selectedTreeId) {
            var selected = treeId == selectedTreeId;
            if (tree.selected == selected) {
                return;
            }
            tree.selected = selected;
            if (!tree.selected) {
                return;
            }
            if (tree.lastSelection) {
                tree.tree('selectNode', null);
                tree.tree('selectNode', tree.lastSelection);
            } else {
                var tree_data = tree.tree('getTree');
                if (tree_data.length > 0) {
                    tree.tree('selectNode', tree_data[0]);
                } else {
                    tree.loadRoots();
                }
            }
        });
        trees[treeId] = tree;
    };

    /**
     * показать дерево
     * @param treeId
     */
    this.showTree = function (treeId) {
        var tree = trees[treeId];
        tree.selected = false;
        treeContainer.children().detach();
        treeContainer.append(tree);
        instance.trigger(BasicChooserEvent.treeSelected, [treeId]);
    };

    /**
     * вставка данных дерева
     * @param treeId идентификатор дерева
     * @param parentNodeId идентификатор ноды
     * @param data данные
     * @param idField  - поле в данных которое является идентификатором
     * @param nameField - поле в данных которое является наименованием
     * @param hasChildrenField - поле в данных которое определяет имеются ли дочерние элементы
     */
    this.setTreeNodeData = function (treeId, parentNodeId, data, idField, nameField, hasChildrenField) {
        var tree = trees[treeId];
        var parentNode = undefined;
        if (parentNodeId) {
            parentNode = tree.tree('getNodeById', parentNodeId);
            for (var i = parentNode.children.length - 1; i >= 0; i--) {
                if (parentNode.children[i]) {
                    tree.tree("removeNode", parentNode.children[i]);
                }
            }
        }

        var roots = [];
        data.forEach(function (item) {
            item.id = item[idField];
            item.label = item[nameField];

            var v = tree.tree('appendNode', item, parentNode);
            if (item[hasChildrenField]) {
                tree.tree('appendNode', { label: '', phantom: true }, v);
            }
            if (!parentNode) {
                roots.push(v);
            }
        });

        if (roots.length > 0) {
            roots.forEach(function (root) {
                if (root.children) {
                    tree.tree('openNode', root, false);
                }
            });
            tree.tree('selectNode', roots[0]);
        }
    };

    /**
     * выделение ноды дерева
     * @param treeId идентификатор дерева
     * @param nodeId идентификатор ноды
     */
    this.selectTreeNode = function (treeId, nodeId) {
        var tree = trees[treeId];
        var node = tree.tree('getNodeById', nodeId);
        if (node) {
            tree.tree('selectNode', node);
        }
    };

    /**
     * открытие ноды дерева
     * @param treeId идентификатор дерева
     * @param nodeId идентификатор ноды
     */
    this.openTreeNode = function (treeId, nodeId) {
        var tree = trees[treeId];
        var node = tree.tree('getNodeById', nodeId);
        if (node) {
            tree.tree('openNode', node, false);
        }
    };

    /**
     * добавление стека
     * @param stackId идентификатор стека
     * @param stackTitle наименование
     * @param treeId идентификатор дерева (которое должно отображаться для этого стека моджет пусть и null )
     * @param enableSearch есть ли поиск для этого стека
     * @param showSelectAll отображать ли галку "Выбрать всех"
     * @param selectHandler лисенер выделения стека
     */
    this.addStack = function (stackId, stackTitle, treeId, enableSearch, showSelectAll, selectHandler) {
        var stack = (0, _jquery2.default)('<div/>', { class: 'ns-basicChooserStack' });
        stack.html(stackTitle);
        stack.click(function () {
            instance.setListTitle(stackTitle);
            instance.trigger(BasicChooserEvent.stackSelected, [stackId]);
            selectHandler();
        });

        instance.on(BasicChooserEvent.stackSelected, function (event, selectedStackId) {
            if (selectedStackId != stackId) {
                stack.removeClass("ns-basicChooserStackSelected");
            } else {
                stack.addClass("ns-basicChooserStackSelected");
                if (enableSearch) {
                    searchBox.removeAttr("disabled");
                } else {
                    searchBox.attr("disabled", "disabled");
                    searchBox.valueOf("");
                }

                if (showSelectAll) {
                    selectAll.show();
                } else {
                    selectAll.hide();
                }
                if (treeId) {
                    instance.showTree(treeId);
                }
            }
        });

        instance.on(BasicChooserEvent.treeNodeSelected, function (event, selectedTreeId) {
            if (selectedTreeId != treeId) {
                stack.removeClass("ns-basicChooserStackSelected");
            }
        });

        stacks[stackId] = stack;
        stacksContainer.append(stack);

        treeContainer.css("height", SPLIT_HEIGHT - 2 - STACK_HEIGHT * Object.keys(stacks).length);
    };

    /**
     * переименование ��тека
     * @param stackId идентификатор стека
     * @param stackTitle наименование
     */
    this.renameStack = function (stackId, stackTitle) {
        stacks[stackId].html(stackTitle);
    };

    /**
     * отобразить стек
     * @param stackId идентификатор стека
     */
    this.showStack = function (stackId) {
        stacks[stackId].click();
    };

    /**
     * добавление типа списка
     * @param listId идентификатор списка
     * @param loadHandler лисенер загрузки
     * @param selectAllHandler лисенер загрузки всех элементов
     */
    this.addListType = function (listId, loadHandler, selectAllHandler) {
        var list = (0, _jquery2.default)('<div/>', { class: 'ns-basicChooserList' });
        list.load = function (append) {
            if (!list.treeId) {
                return;
            }

            if (!append) {
                list.loadedAll = false;
            }

            if (list.loading || list.loadedAll) {
                return;
            }

            list.loading = true;
            var selectedNode = trees[list.treeId].tree('getSelectedNode');
            var start = 0;
            if (append) {
                start = list.loadedCount;
            }
            loadHandler(searchBox.val(), start, countInPart, selectedNode ? selectedNode.id : null, list.stackId);
        };
        list.selectAll = function () {
            if (!list.treeId) {
                return;
            }
            var selectedNode = trees[list.treeId].tree('getSelectedNode');
            selectAllHandler(searchBox.val(), selectedNode ? selectedNode.id : null, list.stackId);
        };

        lists[listId] = list;
        instance.on(BasicChooserEvent.treeSelected, function (event, treeId) {
            list.treeId = treeId;
        });

        instance.on(BasicChooserEvent.stackSelected, function (event, stackId) {
            list.stackId = stackId;
        });
    };

    /**
     * отображение списка
     * @param listId идентификатор
     * @param reload перегружать содержимое
     */
    this.showList = function (listId, reload) {
        var list = lists[listId];
        if (reload) {
            list.load(false);
        }
        listContainer.children().detach();
        listContainer.append(list);
        instance.trigger(BasicChooserEvent.listShowed, [listId]);
    };

    this.setListComponent = function (cmp) {
        splitPane.rightContainer.children().detach();
        splitPane.rightContainer.addClass('registryTableContainerClass');
        splitPane.rightContainer.append(cmp.getWidget());

        searchBox.keypress(function (event) {
            if (event.keyCode == _constants.KEY_CODES.ENTER) {
                cmp.search(searchBox.val());
            }
        });

        searchImage.click(function (event) {
            cmp.search(searchBox.val());
        });
    };

    /**
     * установка длины компонента поиска в пикселях
     * @param width  ширина
     */
    this.setSearchBoxWidth = function (width) {
        searchBox.css("width", width + "px");
        searchImage.css("left", width - 17 + "px");
    };

    /**
     *
     * @param listId
     * @param data массив объектов, в котором должны быть элементы со свойствами id, name, info, status, statusColor, icon
     * @param append
     */
    this.setListData = function (listId, data, append) {
        var list = lists[listId];
        list.loading = false;
        list.loadedAll = data.length === 0;
        if (!append) {
            list.children().detach();
            list.loadedCount = 0;
        }
        data.forEach(function (item) {
            list.append(instance.buildListElement(item, list));
            list.loadedCount++;
        });
    };

    this.setListTitle = function (title) {
        listTitle.html(title);
    };

    this.setSelectedItems = function (newSelectedItems) {
        var oldSelectedItems = selectedItems.splice(0);
        selectedItems = [];
        oldSelectedItems.forEach(function (item) {
            instance.trigger(BasicChooserEvent.itemDeselected, item);
        });
        newSelectedItems.forEach(function (item) {
            instance.trigger(BasicChooserEvent.itemSelected, item);
        });
    };

    this.show = function () {
        (0, _jquery2.default)("body").append(this.contentContainer);
        (0, _jquery2.default)(this.contentContainer).dialog({
            modal: true,
            width: CONTAINER_WIDTH,
            height: CONTAINER_HEIGHT,
            resizable: false,
            title: options.title
        });
    };

    this.getSelectedItems = function () {
        return selectedItems;
    };

    this.isSelected = function (item) {
        return selectedItems.some(function (selectedItem) {
            if (item.id == selectedItem.id) {
                return true;
            }
        });
    };

    this.buildListElement = function (item, list) {
        var container = (0, _jquery2.default)('<div/>', { class: 'ns-basicChooserItem' });
        if (instance.isSelected(item)) {
            container.addClass("ns-basicChooserSelectedItem");
        }
        var name = (0, _jquery2.default)('<div/>');
        var info = (0, _jquery2.default)('<div/>', { class: 'ns-basicChooserItemInfo' });
        var status = (0, _jquery2.default)('<div/>', { class: 'ns-basicChooserItemStatus' });
        container.append(name);
        name.html(item.name);
        name.attr("title", item.name);

        if (options.showListItemStatus && item.status) {

            status.css("color", item.statusColor);
            status.html(item.status);
            status.attr("title", item.status);
            container.append(status);
            name.addClass("ns-basicChooserItemNameStatus");

            if (options.showListItemIcon) {
                name.css("max-width", 250 - options.listItemIconSize);
            }
        } else if (options.showListItemIcon) {
            name.css("max-width", "calc(100% - " + (options.listItemIconSize + 27) + "px)");
        }

        if (options.showListItemInfo) {
            info.html(item.info);
            info.attr("title", item.info);
            container.append(info);
            container.addClass("ns-basicChooserItemFull");
            name.addClass("ns-basicChooserItemName");
        } else {
            container.addClass("ns-basicChooserItemShort");
            name.addClass("ns-basicChooserItemNameShort");
        }

        if (options.showListItemIcon) {
            var imgDefault = (0, _jquery2.default)("<div/>", { class: "ns-basicChooserItemIcon " + options.showListItemIconStyle });
            container.append(imgDefault);

            if (item.icon) {
                var img = (0, _jquery2.default)("<div/>", { class: "ns-basicChooserItemIcon " + options.showListItemIconStyle });
                img.css("width", options.listItemIconSize);
                img.css("height", options.listItemIconSize);
                img.css("background-image", "url(" + api.getFullUrl(item.icon) + ")");
                container.append(img);
            }
            name.css("margin-left", options.listItemIconSize + GAP);
            info.css("margin-left", options.listItemIconSize + GAP);
            status.css("margin-left", options.listItemIconSize + GAP);
            info.css("max-width", "calc(100% - " + (options.listItemIconSize + GAP * 3) + "px)");
        }

        instance.on(BasicChooserEvent.itemSelected, function (event, selectedItem) {
            if (item.id == selectedItem.id) {
                container.addClass("ns-basicChooserSelectedItem");
            }
        });

        instance.on(BasicChooserEvent.itemDeselected, function (event, deselectedItem) {
            if (item.id == deselectedItem.id) {
                container.removeClass("ns-basicChooserSelectedItem");
            }
        });

        container.click(function () {
            if (instance.isSelected(item)) {
                if (options.multiSelect) {
                    instance.trigger(BasicChooserEvent.itemDeselected, [item]);
                }
            } else {
                instance.trigger(BasicChooserEvent.itemSelected, [item]);
            }
        });

        if (!options.multiSelect) {
            container.dblclick(function () {
                if (instance.isSelectionValid()) {
                    instance.close();
                }
            });
        }
        if (options.dblClick) {
            container.dblclick(function () {
                options.dblClick(item);
            });
        }
        return container;
    };

    this.close = function () {

        instance.contentContainer.dialog("destroy");
        instance.contentContainer.detach();
        instance.trigger(BasicChooserEvent.applyClicked);
    };
};

/***/ }),

/***/ 72:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.default = TagArea;

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _componentUtils = __webpack_require__(15);

var compUtils = _interopRequireWildcard(_componentUtils);

var _constants = __webpack_require__(2);

var _TagItem = __webpack_require__(396);

var _TagItem2 = _interopRequireDefault(_TagItem);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

/**
 * тэговый инпут
 * @param css стиль
 * @param width ширина (может быть 0)
 * @param multiValue принимает много значений
 * @param editContent разрешать редактировать тэг
 * @param canAddRandomText разрешать ввод произвольного текста
 * @param suggestHandler подсказки
 * @constructor
 */
function TagArea(css, width, multiValue, editContent, canAddRandomText, suggestHandler, createRandom) {

    var instance = this;
    compUtils.makeBus(this);

    var tagsContainer = (0, _jquery2.default)('<div/>', { class: 'ns-tagContainer' });
    if (css) {
        tagsContainer.css(css);
    }

    var tags = [];

    var suggestionInput = (0, _jquery2.default)('<input/>', { type: "text", class: 'ns-tagSuggestionInput' });

    var editable = true;

    suggestionInput.keyup(function (event) {
        if (event.keyCode == _constants.KEY_CODES.ENTER) {
            if (canAddRandomText && suggestionInput.val().trim() !== "") {
                if (createRandom) {
                    createRandom(suggestionInput.val());
                } else {
                    instance.addValue({ tagName: suggestionInput.val() }, true);
                }
                suggestionInput.val("");
            }
        } else {
            var value = suggestionInput.val();
            var inputWidth = value.getWidth("12px Arial") + 10;
            suggestionInput.css("width", inputWidth + "px");
            suggestHandler(value, suggestionInput);
        }
    });

    this.focus = function () {
        suggestionInput.focus();
    };

    tagsContainer.click(function () {
        suggestionInput.focus();
    });

    this.updateView = function (values) {
        tagsContainer.children().detach();
        tags = [];

        if (values && values instanceof Array) {
            values.forEach(function (value, index) {
                instance.addValue(value);
            });
        }
        this.updateSuggestInput();
    };

    this.addValue = function (value, updateSuggestionInput) {
        var tag = new _TagItem2.default(value, width, editContent);
        tag.setEditable(editable);
        tags.push(tag);
        tag.on(_constants.EVENT_TYPE.tagDelete, function () {
            tags.splice(tags.indexOf(tag), 1);
            instance.trigger(_constants.EVENT_TYPE.tagDelete);
            instance.updateSuggestInput();
        });

        tag.on(_constants.EVENT_TYPE.tagEdit, function () {
            instance.trigger(_constants.EVENT_TYPE.tagEdit);
        });
        tagsContainer.append(tag.getWidget());

        if (updateSuggestionInput) {
            instance.updateSuggestInput();
        }
        this.markValidity(true);
    };

    this.updateSuggestInput = function () {
        if (suggestHandler && (tags.length == 0 || multiValue)) {
            tagsContainer.append(suggestionInput);
        }
    };

    if (suggestHandler) {
        tagsContainer.append(suggestionInput);
    }

    this.markValidity = function (valid) {
        if (valid) {
            tagsContainer.removeClass('ns-invalidInput');
        } else {
            tagsContainer.addClass('ns-invalidInput');
        }
    };

    this.getValues = function () {
        var values = [];
        tags.forEach(function (tag) {
            values.push(tag.value);
        });
        return values;
    };

    this.setValues = function (newValues) {
        this.updateView(newValues);
    };

    this.setVisible = function (visible) {
        if (visible) {
            tagsContainer.css("display", "inline-block");
        } else {
            tagsContainer.hide();
        }
    };

    this.setEditable = function (newEnabled) {
        editable = newEnabled;
        if (editable) {
            tagsContainer.removeClass("ns-disabledInput");
            suggestionInput.css("display", "inline-block");
        } else {
            tagsContainer.addClass("ns-disabledInput");
            suggestionInput.hide();
        }

        tags.forEach(function (tag) {
            tag.setEditable(editable);
        });
    };

    this.getWidget = function () {
        return tagsContainer;
    };
};

/***/ }),

/***/ 73:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.SIGN_LIST_FORMAT = exports.RESOLUTION_LIST_FORMAT = exports.EXECUTION_LIST_FORMAT = exports.DATE_FORMAT_FULL = exports.DATE_FORMAT = exports.INPUT_TIME_MASK = exports.INPUT_DATE_MASK = exports.DATE_PICKER_FORMAT = undefined;
exports.validateDateString = validateDateString;
exports.isEmpty = isEmpty;
exports.parseDate = parseDate;
exports.needDictionary = needDictionary;
exports.formatDate = formatDate;
exports.getFormattedValue = getFormattedValue;
exports.getMonthName = getMonthName;
exports.getMonthShortName = getMonthShortName;
exports.getMonthPossessiveName = getMonthPossessiveName;
exports.getWeekDayName = getWeekDayName;
exports.getWeekDayShortName = getWeekDayShortName;

var _constants = __webpack_require__(2);

function validateDateString(dateStr) {
    if (!dateStr) {
        return null;
    }
    if (dateStr.match(/^[0-9]{4}\-[0-9]{2}\-[0-9]{2}$/)) {
        dateStr += " 00:00:00";
    }
    if (!dateStr.match(/^[0-9]{4}\-[0-9]{2}\-[0-9]{2} [0-9]{2}:[0-9]{2}:[0-9]{2}$/)) {
        return null;
    }
    return dateStr;
} /**
   * утилиты для манипулирования полем ввода даты и форматами даты
   * @type {{parseDate: dateUtils.parseDate, formatDate: dateUtils.formatDate, getFormattedValue: dateUtils.getFormattedValue, getMonthName: dateUtils.getMonthName, getMonthShortName: dateUtils.getMonthShortName, getMonthPossessiveName: dateUtils.getMonthPossessiveName, getWeekDayName: dateUtils.getWeekDayName, getWeekDayShortName: dateUtils.getWeekDayShortName, DATE_PICKER_FORMAT: string, INPUT_DATE_MASK: string, INPUT_TIME_MASK: string, DATE_FORMAT: string, DATE_FORMAT_FULL: string, EXECUTION_LIST_FORMAT: string, RESOLUTION_LIST_FORMAT: string, SIGN_LIST_FORMAT: string}}
   */

/**
 * валидируем строку которую передали,
 * если соответствует формату yyyy-MM-dd HH:mm:ss  возвращаем ее же
 * если соответствует формату yyyy-MM-dd добавляем 00:00:00 и возвращаем
 * если что-то другое возвращаем null
 * @param dateStr
 */
function isEmpty(dateStr) {
    if (!dateStr) {
        return true;
    }
    if (dateStr && dateStr.startsWith("____-__-__")) {
        return true;
    }
}

/**
 *
 * @param dateStr  дата в формате yyyy-MM-dd HH:mm
 * @returns {*}
 */
function parseDate(dateStr) {
    dateStr = validateDateString(dateStr);
    if (!dateStr) {
        return null;
    }

    var y = dateStr.substr(0, 4);
    var m = dateStr.substr(5, 2) - 1;
    var d = dateStr.substr(8, 2);
    var h = dateStr.substr(11, 2);
    var min = dateStr.substr(14, 2);

    if (m > 11 || m < 0) {
        return null;
    }
    if (m == 0 || m == 2 || m == 4 || m == 6 || m == 7 || m == 9 || m == 11) {
        if (d < 1 || d > 31) {
            return null;
        }
    } else if (m == 3 || m == 5 || m == 7 || m == 10) {
        if (d < 1 || d > 30) {
            return null;
        }
    } else if (m == 1) {
        if (y % 4 == 0 && y % 100 > 0) {
            if (d < 1 || d > 29) {
                return null;
            }
        } else {
            if (d < 1 || d > 28) {
                return null;
            }
        }
    }

    if (h > 23 || h < 0) {
        return null;
    }

    if (min < 0 || min > 59) {
        return null;
    }

    return new Date(y, m, d, h, min, 0);
}

function needDictionary(format) {
    return format && format.indexOf(_constants.DATE_FORMAT_PARTS.monthec) > -1;
}

function formatDate(date, format) {
    if (date == null || date == 'Invalid Date') {
        return "";
    }

    var parts = format.match(/(\$\{[^\}\{\$]+\})/g);
    if (!parts) {
        return format;
    }

    var result = format;
    parts.forEach(function (part) {
        var partValue = getFormattedValue(date, part);
        result = result.replace(part, partValue);
    });
    return result;
}

function getFormattedValue(date, part) {
    var f = part.match(/\$\{(.+)\}/)[1];

    switch (f) {
        case _constants.DATE_FORMAT_PARTS.yyyy:
            {
                return (date.getYear() + 1900).fixedInt(4);
            }
        case _constants.DATE_FORMAT_PARTS.yy:
            {
                return (date.getYear() + 1900).fixedInt(2);
            }
        case _constants.DATE_FORMAT_PARTS.mm:
            {
                return (date.getMonth() + 1).fixedInt(2);
            }
        case _constants.DATE_FORMAT_PARTS.m:
            {
                return date.getMonth() + 1;
            }
        case _constants.DATE_FORMAT_PARTS.month:
            {
                return getMonthName(date.getMonth() + 1).toLowerCase();
            }
        case _constants.DATE_FORMAT_PARTS.month_short:
            {
                return getMonthShortName(date.getMonth() + 1).toLowerCase();
            }
        case _constants.DATE_FORMAT_PARTS.monthed:
            {
                return getMonthPossessiveName(date.getMonth() + 1).toLowerCase();
            }
        case _constants.DATE_FORMAT_PARTS.dd:
            {
                return date.getDate().fixedInt(2);
            }
        case _constants.DATE_FORMAT_PARTS.d:
            {
                return date.getDate();
            }
        case _constants.DATE_FORMAT_PARTS.hh:
            {
                if (date.getHours() < 12) {
                    return date.getHours().fixedInt(2);
                } else {
                    return (date.getHours() - 12).fixedInt(2);
                }
            }
        case _constants.DATE_FORMAT_PARTS.h:
            {
                if (date.getHours() < 12) {
                    return date.getHours();
                } else {
                    return date.getHours() - 12;
                }
            }
        case _constants.DATE_FORMAT_PARTS.HH:
            {
                return date.getHours().fixedInt(2);
            }
        case _constants.DATE_FORMAT_PARTS.H:
            {
                return date.getHours();
            }
        case _constants.DATE_FORMAT_PARTS.MM:
            {
                return date.getMinutes().fixedInt(2);
            }
        case _constants.DATE_FORMAT_PARTS.M:
            {
                return date.getMinutes();
            }
        case _constants.DATE_FORMAT_PARTS.SS:
            {
                return date.getSeconds().fixedInt(2);
            }
        case _constants.DATE_FORMAT_PARTS.S:
            {
                return date.getSeconds();
            }
    }
    return part;
}

function getMonthName(month) {
    switch (month) {
        case 1:
            {
                return i18n.tr("Январь");
            }
        case 2:
            {
                return i18n.tr("Февраль");
            }
        case 3:
            {
                return i18n.tr("Март");
            }
        case 4:
            {
                return i18n.tr("Апрель");
            }
        case 5:
            {
                return i18n.tr("Май");
            }
        case 6:
            {
                return i18n.tr("Июнь");
            }
        case 7:
            {
                return i18n.tr("Июль");
            }
        case 8:
            {
                return i18n.tr("Август");
            }
        case 9:
            {
                return i18n.tr("Сентябрь");
            }
        case 10:
            {
                return i18n.tr("Октябрь");
            }
        case 11:
            {
                return i18n.tr("Ноябрь");
            }
        case 12:
            {
                return i18n.tr("Декабрь");
            }
    }
}

function getMonthShortName(month) {
    switch (month) {
        case 1:
            {
                return i18n.tr("янв");
            }
        case 2:
            {
                return i18n.tr("фев");
            }
        case 3:
            {
                return i18n.tr("мар");
            }
        case 4:
            {
                return i18n.tr("апр");
            }
        case 5:
            {
                return i18n.tr("май");
            }
        case 6:
            {
                return i18n.tr("июн");
            }
        case 7:
            {
                return i18n.tr("июл");
            }
        case 8:
            {
                return i18n.tr("авг");
            }
        case 9:
            {
                return i18n.tr("сен");
            }
        case 10:
            {
                return i18n.tr("окт");
            }
        case 11:
            {
                return i18n.tr("ноя");
            }
        case 12:
            {
                return i18n.tr("дек");
            }
    }
}

function getMonthPossessiveName(month) {
    switch (month) {
        case 1:
            {
                return i18n.tr("января");
            }
        case 2:
            {
                return i18n.tr("февраля");
            }
        case 3:
            {
                return i18n.tr("марта");
            }
        case 4:
            {
                return i18n.tr("апреля");
            }
        case 5:
            {
                return i18n.tr("мая");
            }
        case 6:
            {
                return i18n.tr("июня");
            }
        case 7:
            {
                return i18n.tr("июля");
            }
        case 8:
            {
                return i18n.tr("августа");
            }
        case 9:
            {
                return i18n.tr("сентября");
            }
        case 10:
            {
                return i18n.tr("октября");
            }
        case 11:
            {
                return i18n.tr("ноября");
            }
        case 12:
            {
                return i18n.tr("декабря");
            }
    }
}

function getWeekDayName(month) {
    switch (month) {
        case 1:
            {
                return i18n.tr("Понедельник");
            }
        case 2:
            {
                return i18n.tr("Вторник");
            }
        case 3:
            {
                return i18n.tr("Среда");
            }
        case 4:
            {
                return i18n.tr("Четверг");
            }
        case 5:
            {
                return i18n.tr("Пятница");
            }
        case 6:
            {
                return i18n.tr("Суббота");
            }
        case 7:
            {
                return i18n.tr("Воскресенье");
            }
    }
}

function getWeekDayShortName(dayNumber) {
    switch (dayNumber) {
        case 1:
            {
                return i18n.tr("Пн");
            }
        case 2:
            {
                return i18n.tr("Вт");
            }
        case 3:
            {
                return i18n.tr("Ср");
            }
        case 4:
            {
                return i18n.tr("Чт");
            }
        case 5:
            {
                return i18n.tr("Пт");
            }
        case 6:
            {
                return i18n.tr("Сб");
            }
        case 7:
            {
                return i18n.tr("Вс");
            }
    }
}

var DATE_PICKER_FORMAT = exports.DATE_PICKER_FORMAT = 'yy-mm-dd';
var INPUT_DATE_MASK = exports.INPUT_DATE_MASK = '##-##-####';
var INPUT_TIME_MASK = exports.INPUT_TIME_MASK = '## = ##';
var DATE_FORMAT = exports.DATE_FORMAT = '${yyyy}-${mm}-${dd} ${HH}:${MM}';
var DATE_FORMAT_FULL = exports.DATE_FORMAT_FULL = '${yyyy}-${mm}-${dd} ${HH}:${MM}:${SS}';
var EXECUTION_LIST_FORMAT = exports.EXECUTION_LIST_FORMAT = '${dd} ${month_short}; ${HH}:${MM}';
var RESOLUTION_LIST_FORMAT = exports.RESOLUTION_LIST_FORMAT = '${dd}-${mm}-${yy} ${HH}:${MM}';
var SIGN_LIST_FORMAT = exports.SIGN_LIST_FORMAT = '${HH}:${MM}:${SS} ${dd}.${mm}.${yy}';

/***/ }),

/***/ 8:
/***/ (function(module, exports, __webpack_require__) {

"use strict";
/**
 * Created by exile
 * Date: 20.01.16
 * Time: 16:25
 */


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.loadDefaultAsfData = loadDefaultAsfData;
exports.loadFormDefinition = loadFormDefinition;
exports.loadFormDefinitionByCode = loadFormDefinitionByCode;
exports.addAuthHeader = addAuthHeader;
exports.getFullUrl = getFullUrl;
exports.getImage = getImage;
exports.getHandler = getHandler;
exports.getFullUrlWithAuth = getFullUrlWithAuth;
exports.parseUrl = parseUrl;
exports.getDefaultAjaxParams = getDefaultAjaxParams;
exports.simpleAsyncGet = simpleAsyncGet;
exports.simpleAsyncPost = simpleAsyncPost;
exports.loadAsfData = loadAsfData;
exports.saveAsfData = saveAsfData;
exports.loadDictionary = loadDictionary;
exports.formatDate = formatDate;
exports.getDocAttribute = getDocAttribute;
exports.getDocumentIdentifier = getDocumentIdentifier;
exports.getDocLinkFormatted = getDocLinkFormatted;
exports.getDocumentBases = getDocumentBases;
exports.getProcessExecution = getProcessExecution;
exports.getResolutionsList = getResolutionsList;
exports.getSigns = getSigns;
exports.getSignHeaders = getSignHeaders;
exports.getUsersInfo = getUsersInfo;
exports.getUsers = getUsers;
exports.getUsersSuggestions = getUsersSuggestions;
exports.checkUsers = checkUsers;
exports.getPositionsInfo = getPositionsInfo;
exports.getPositions = getPositions;
exports.getPositionsSuggestions = getPositionsSuggestions;
exports.getDepartmentsInfo = getDepartmentsInfo;
exports.getDepartmentsSuggestions = getDepartmentsSuggestions;
exports.getDepartments = getDepartments;
exports.getGroups = getGroups;
exports.getAllGroups = getAllGroups;
exports.getOftenUsers = getOftenUsers;
exports.getCollatedData = getCollatedData;
exports.getFiles = getFiles;
exports.getFolders = getFolders;
exports.getRegistryFilters = getRegistryFilters;
exports.getPortfolios = getPortfolios;
exports.checkPositions = checkPositions;
exports.checkDepartments = checkDepartments;
exports.logError = logError;
exports.createHtd = createHtd;
exports.uploadFile = uploadFile;
exports.copyFileToForm = copyFileToForm;
exports.getFileInfo = getFileInfo;
exports.getProjectTreeItemInfo = getProjectTreeItemInfo;
exports.translate = translate;
exports.getDocMeaningContent = getDocMeaningContent;
exports.getAsfDataUUID = getAsfDataUUID;
exports.translateMultiple = translateMultiple;
exports.removeFile = removeFile;
exports.getRegistry = getRegistry;
exports.getRegistryData = getRegistryData;
exports.getRegistryFilterData = getRegistryFilterData;
exports.getAddressBookData = getAddressBookData;
exports.getCustomComponent = getCustomComponent;

var _constants = __webpack_require__(2);

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

var _logger = __webpack_require__(101);

var LOGGER = _interopRequireWildcard(_logger);

var _services = __webpack_require__(23);

var services = _interopRequireWildcard(_services);

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function loadDefaultAsfData(formID) {
    return simpleAsyncGet('rest/api/asforms/getDefaultContent?formID=' + formID);
}

function loadFormDefinition(uuid, version) {
    if (version) {
        return simpleAsyncGet('rest/api/asforms/form_ext?formID=' + uuid + "&version=" + version);
    } else {
        return simpleAsyncGet('rest/api/asforms/form_ext?formID=' + uuid);
    }
}

function loadFormDefinitionByCode(formCode, version) {
    if (version) {
        return simpleAsyncGet('rest/api/asforms/form_ext?formCode=' + formCode + "&version=" + version);
    } else {
        return simpleAsyncGet('rest/api/asforms/form_ext?formCode=' + formCode);
    }
}

function addAuthHeader(xhr) {
    if (_constants.OPTIONS.login) {
        xhr.setRequestHeader("Authorization", "Basic " + btoa(unescape(encodeURIComponent(_constants.OPTIONS.login + ":" + _constants.OPTIONS.password))));
    }
}

function getFullUrl(urlPart) {
    if (urlPart.startsWith("data:image/png;base64")) return urlPart;
    return _constants.OPTIONS.coreUrl + urlPart;
}

function getImage(url, handler) {
    var xhr = new XMLHttpRequest();
    xhr.open('GET', getFullUrl(url), true);
    addAuthHeader(xhr);
    xhr.responseType = 'arraybuffer';
    xhr.onload = function (e) {
        // response is unsigned 8 bit integer
        if (this.status === 200) {
            var responseArray = new Uint8Array(this.response);
            var b64encoded = btoa(new Uint8Array(responseArray).reduce(function (data, byte) {
                return data + String.fromCharCode(byte);
            }, ''));
            handler(b64encoded);
        }
    };

    xhr.send();
}

function getHandler(handler, hideWaitWindow, errorHandler) {
    return function (data) {
        if (hideWaitWindow) {
            services.hideWaitWindow();
        }
        if (data.errorCode && data.errorCode !== '0') {
            if (errorHandler) {
                errorHandler(data);
            } else {
                console.log(data);
            }
        } else {
            handler(data);
        }
    };
}

function getFullUrlWithAuth(urlPart) {
    var url = _constants.OPTIONS.coreUrl;
    if (browser !== 'MSIE') {
        if (url.startsWith("https")) {
            url = url.substring(8);
            url = "https://" + encodeURIComponent(_constants.OPTIONS.login) + ":" + encodeURIComponent(_constants.OPTIONS.password) + "@" + url;
        } else if (url.startsWith("http")) {
            url = url.substring(7);

            url = "http://" + encodeURIComponent(_constants.OPTIONS.login) + ":" + encodeURIComponent(_constants.OPTIONS.password) + "@" + url;
        }
    }

    return url + urlPart;
}

function parseUrl(url) {
    var a = document.createElement('a');
    a.href = url;
    return a;
}

function getDefaultAjaxParams(type, data, urlPart, callback, errorHandler) {
    if (!errorHandler) {
        errorHandler = function errorHandler(jqXHR, textStatus, errorThrown) {
            try {
                services.showErrorMessage(JSON.parse(jqXHR.responseText).errorMessage);
            } catch (e) {
                console.log(e);
            }
            var httpError = { status: textStatus, urlPart: urlPart, data: JSON.stringify(data), error: errorThrown };
            LOGGER.logServer(httpError);
        };
    }
    var ajaxParams = {
        type: type,
        url: getFullUrl(urlPart),
        success: callback,
        error: errorHandler,
        beforeSend: addAuthHeader,
        statusCode: {
            401: function _() {
                services.unAuthorized(urlPart);
            }
        }
    };
    return ajaxParams;
}

function simpleAsyncGet(urlPart, callback, dataType, data, errorHandler) {
    if (!dataType) {
        dataType = 'json';
    }

    var ajaxParams = getDefaultAjaxParams("GET", data, urlPart, callback, errorHandler);
    ajaxParams.data = data;
    ajaxParams.dataType = dataType;

    return _jquery2.default.ajax(ajaxParams).then(function (result) {
        return result;
    });
}

function simpleAsyncPost(urlPart, callback, dataType, data, contentType, errorHandler) {

    if (!dataType) {
        dataType = 'json';
    }

    var ajaxParams = getDefaultAjaxParams("POST", data, urlPart, callback, errorHandler);
    ajaxParams.data = data;
    ajaxParams.dataType = dataType;
    if (!_.isUndefined(contentType)) {
        ajaxParams.contentType = contentType;
    }
    return _jquery2.default.ajax(ajaxParams).then(function (result) {
        return result;
    });
}

function loadAsfData(uuid, version) {
    if (version) {
        return simpleAsyncGet('rest/api/asforms/data/' + uuid + "?version=" + version);
    } else {
        return simpleAsyncGet('rest/api/asforms/data/' + uuid);
    }
}

function saveAsfData(asfData, formUid, dataUid) {
    return simpleAsyncPost('rest/api/asforms/form/multipartdata', null, "text", {
        data: "\"data\" : " + JSON.stringify(asfData),
        form: formUid,
        uuid: dataUid
    });
}

function loadDictionary(code, locale, handler, errorHandler) {
    simpleAsyncGet('rest/api/dictionary/get_by_code?dictionaryCode=' + encodeURIComponent(code) + "&locale=" + locale, handler, null, {}, errorHandler);
}

function formatDate(date, format, locale) {
    return simpleAsyncGet('rest/api/formPlayer/formatDate?date=' + encodeURIComponent(date) + "&format=" + encodeURIComponent(format) + "&locale=" + locale, null, "text");
}

function getDocAttribute(dataUUID, field, locale, handler) {
    return simpleAsyncGet('rest/api/formPlayer/documentAttribute?dataUUID=' + encodeURIComponent(dataUUID) + "&field=" + encodeURIComponent(field) + "&locale=" + locale, handler, "text");
}

function getDocumentIdentifier(dataUUID, handler) {
    return simpleAsyncGet('rest/api/formPlayer/documentIdentifier?dataUUID=' + encodeURIComponent(dataUUID), handler, "text");
}

function getDocLinkFormatted(documentID, format, locale, handler) {
    return simpleAsyncGet('rest/api/formPlayer/documentLink?documentID=' + encodeURIComponent(documentID) + "&format=" + encodeURIComponent(format) + "&locale=" + locale, handler, "text");
}

function getDocumentBases(dataUUID, locale, handler) {
    return simpleAsyncGet('rest/api/formPlayer/getDocumentBases?dataUUID=' + encodeURIComponent(dataUUID) + "&locale=" + locale, handler);
}

function getProcessExecution(dataUUID, locale, handler) {
    return simpleAsyncGet('rest/api/formPlayer/getProcessExecution?dataUUID=' + encodeURIComponent(dataUUID) + "&locale=" + locale, handler);
}

function getResolutionsList(dataUUID, locale, handler) {
    return simpleAsyncGet('rest/api/formPlayer/getResolutions?dataUUID=' + encodeURIComponent(dataUUID) + "&locale=" + locale, handler);
}

function getSigns(dataUUID, type, locale, handler) {
    return simpleAsyncGet('rest/api/formPlayer/getSigns?dataUUID=' + encodeURIComponent(dataUUID) + "&type=" + type + "&locale=" + locale, handler);
}

function getSignHeaders(headers, type, locale, handler) {
    return simpleAsyncGet('rest/api/formPlayer/getSignHeaders?signConfig=' + encodeURIComponent(JSON.stringify(headers)) + "&type=" + type + "&locale=" + locale, handler);
}

function getUsersInfo(userIds, locale, handler) {
    var s = "locale=" + locale;
    userIds.forEach(function (userId) {
        s += "&userID=" + userId;
    });
    return simpleAsyncGet('rest/api/userchooser/getUserInfo?' + s, handler);
}

function getUsers(search, filterPositionId, filterDepartmentId, departmentID, groupID, showNoPosition, startRecord, recordsCount, showAll, showEmails, showGroups, locale, handler) {
    var orgParams = {
        startRecord: startRecord,
        recordsCount: recordsCount,
        showAll: showAll,
        filterPositionID: filterPositionId,
        filterDepartmentID: filterDepartmentId,
        departmentID: departmentID,
        groupID: groupID,
        showNoPosition: showNoPosition,
        search: search,
        showEmailContacts: showEmails,
        showGroups: showGroups,
        locale: locale
    };
    services.showWaitWindow();
    return simpleAsyncGet('rest/api/userchooser/search_ext?' + _jquery2.default.param(orgParams), getHandler(handler, true));
}

function getUsersSuggestions(search, filterPositionId, filterDepartmentId, showNoPosition, showAll, showEmails, locale, handler) {
    var orgParams = {
        startRecord: 0,
        recordsCount: 30,
        showAll: showAll,
        filterPositionID: filterPositionId,
        filterDepartmentID: filterDepartmentId,
        showNoPosition: showNoPosition,
        search: search,
        showEmailContacts: showEmails,
        showGroups: false,
        locale: locale
    };

    return simpleAsyncGet('rest/api/userchooser/search_ext?' + _jquery2.default.param(orgParams), handler);
}

function checkUsers(userIds, filterPositionId, filterDepartmentId, showNoPosition, showAll, locale, handler) {
    var s = "&locale=" + _constants.OPTIONS.locale;
    userIds.forEach(function (userId) {
        s += "&userToCheck=" + userId;
    });
    var orgParams = {
        showAll: showAll,
        filterPositionID: filterPositionId,
        filterDepartmentID: filterDepartmentId,
        showNoPosition: showNoPosition,
        locale: locale
    };
    return simpleAsyncGet('rest/api/userchooser/checkUsers?' + _jquery2.default.param(orgParams) + s, handler);
}

function getPositionsInfo(posIds, locale, handler) {
    var s = "locale=" + locale;
    posIds.forEach(function (posId) {
        s += "&positionID=" + posId;
    });
    return simpleAsyncGet('rest/api/userchooser/getPositionInfo?' + s, handler);
}

function getPositions(search, filterUserId, filterDepartmentId, showVacant, showAll, departmentID, startRecord, recordsCount, locale, handler) {
    var orgParams = {
        locale: locale,
        showAll: showAll,
        showOnlyVacant: showVacant,
        startRecord: startRecord,
        recordsCount: recordsCount,
        departmentID: departmentID,
        filterUserID: filterUserId,
        filterDepartmentID: filterDepartmentId,
        search: search
    };
    services.showWaitWindow();
    return simpleAsyncGet('rest/api/userchooser/search_pos?' + _jquery2.default.param(orgParams), getHandler(handler, true));
}

function getPositionsSuggestions(search, filterUserId, filterDepartmentId, showVacant, showAll, locale, handler) {
    var orgParams = {
        locale: locale,
        showAll: showAll,
        showOnlyVacant: showVacant,
        startRecord: 0,
        recordsCount: 30,
        filterUserID: filterUserId,
        filterDepartmentID: filterDepartmentId,
        search: search
    };

    return simpleAsyncGet('rest/api/userchooser/search_pos?' + _jquery2.default.param(orgParams), handler);
}

function getDepartmentsInfo(depIds, locale, handler) {
    var s = "locale=" + locale;
    depIds.forEach(function (depId) {
        s += "&departmentID=" + depId;
    });
    return simpleAsyncGet('rest/api/userchooser/getDepartmentInfo?' + s, handler);
}

function getDepartmentsSuggestions(search, filterUserId, filterPositionId, filterDepartmentID, departmentID, pageNumber, recordsCount, locale, showWaitWindow, handler) {
    if (!departmentID) {
        departmentID = "32b36e0c-4621-4c96-99e1-3826284c363e";
    }
    var orgParams = {
        locale: locale,
        pageNumber: pageNumber,
        countInPart: recordsCount,
        filterUserID: filterUserId,
        filterPositionID: filterPositionId,
        filterDepartmentID: filterDepartmentID,
        departmentID: departmentID,
        search: search,
        showChildDepartments: true
    };
    if (showWaitWindow) {
        services.showWaitWindow();
        handler = getHandler(handler, true);
    }
    return simpleAsyncGet('rest/api/userchooser/search_dept?' + _jquery2.default.param(orgParams), handler);
}

function getDepartments(departmentId, locale, handler) {
    if (!departmentId) {
        departmentId = "32b36e0c-4621-4c96-99e1-3826284c363e";
    }
    var params = {
        departmentID: departmentId,
        onlyPosition: false,
        onlyDepartments: true,
        locale: locale
    };
    services.showWaitWindow();
    return simpleAsyncGet('rest/api/departments/content?' + _jquery2.default.param(params), getHandler(handler, true));
}

function getGroups(parentGroupId, start, count, locale, handler) {
    var params = {
        parentGroupID: parentGroupId,
        right: 0,
        startRecords: start,
        recordsCount: count,
        loadAutoGroups: true,
        locale: locale
    };
    services.showWaitWindow();
    return simpleAsyncGet('rest/api/groups/content?' + _jquery2.default.param(params), getHandler(handler, true));
}

function getAllGroups(search, parentGroupId, start, count, locale, handler) {
    var params = {
        search: search,
        parentGroupID: parentGroupId,
        loadAllHierarchy: true,
        right: 0,
        startRecord: start,
        recordsCount: count,
        loadAutoGroups: true,
        locale: locale
    };
    services.showWaitWindow();
    return simpleAsyncGet('rest/api/groups/find?' + _jquery2.default.param(params), getHandler(handler, true));
}

function getOftenUsers(search, filterPositionId, filterDepartmentId, departmentID, showNoPosition, startRecord, recordsCount, showAll, showEmails, locale, handler) {
    var orgParams = {
        startRecord: startRecord,
        recordsCount: recordsCount,
        showAll: showAll,
        filterPositionID: filterPositionId,
        filterDepartmentID: filterDepartmentId,
        departmentID: departmentID,
        showNoPosition: showNoPosition,
        search: search,
        showEmailContacts: showEmails,
        locale: locale
    };

    return simpleAsyncGet('rest/api/userchooser/getOftenChosenUsers?' + _jquery2.default.param(orgParams), handler);
}

function getCollatedData(recordID, dataUUID, formId, collationGroup, version, handler) {
    var params = {
        registryDocumentId: recordID,
        dataUUID: dataUUID,
        formId: formId,
        collationGroup: collationGroup,
        version: version
    };
    return simpleAsyncGet('rest/api/asforms/form/collate?' + _jquery2.default.param(params), handler, 'text');
}

function getFiles(parentId, mimes, handler) {
    var orgParams = {
        parentID: parentId
    };

    var mimeParam = "";
    mimes.forEach(function (mime) {
        mimeParam += "&mimeType=" + encodeURIComponent(mime);
    });
    services.showWaitWindow();
    return simpleAsyncGet('rest/api/formPlayer/listFiles?' + _jquery2.default.param(orgParams) + mimeParam, getHandler(handler, true));
}

function getFolders(parentId, showHomeFolder, handler) {
    var params = { parentID: parentId, locale: 'ru', getOnlyFolders: true, showHomeFolder: showHomeFolder };
    services.showWaitWindow();
    return simpleAsyncGet('rest/api/formPlayer/listFiles?' + _jquery2.default.param(params), getHandler(handler, true));
}

function getRegistryFilters(registryID, loadIcon, type, locale, handler) {
    var params = { registryID: registryID, locale: locale, getIcon: loadIcon, type: type };
    services.showWaitWindow();
    return simpleAsyncGet('rest/api/registry/filters?' + _jquery2.default.param(params), getHandler(handler, true));
}

function getPortfolios(parentID, onlyFolders, handler) {
    var orgParams = {
        itemID: parentID,
        onlyFolders: onlyFolders
    };
    services.showWaitWindow();
    return simpleAsyncGet('rest/api/projects/get_project_tree?' + _jquery2.default.param(orgParams), getHandler(handler, true));
}

function checkPositions(search, filterUserId, filterDepartmentId, showVacant, showAll, locale, handler) {
    var orgParams = {
        locale: locale,
        showAll: showAll,
        showOnlyVacant: showVacant,
        filterUserID: filterUserId,
        filterDepartmentID: filterDepartmentId,
        search: search
    };

    return simpleAsyncGet('rest/api/userchooser/checkPositions?' + _jquery2.default.param(orgParams), handler);
}

function checkDepartments(search, filterUserId, filterPositionId, filterDepartmentID, locale, handler) {
    var orgParams = {
        locale: locale,
        filterUserID: filterUserId,
        filterPositionID: filterPositionId,
        filterDepartmentID: filterDepartmentID,
        search: search,
        showChildDepartments: true
    };

    return simpleAsyncGet('rest/api/userchooser/checkDepartments?' + _jquery2.default.param(orgParams), handler);
}

function logError(error, userId, formId, asfDataId) {

    var orgParams = {
        error: JSON.stringify(error),
        userId: userId,
        formId: formId,
        asfDataId: asfDataId
    };

    return simpleAsyncPost('rest/api/formPlayer/log?', null, "text", orgParams, undefined, function () {});
}

function createHtd(data, handler) {
    services.showWaitWindow();
    simpleAsyncPost('rest/api/storage/asffile/createHtd', getHandler(handler, true), "json", JSON.stringify(data), 'application/json; charset=utf8');
}

function uploadFile(nodeId, dataId, data, handler, errorHandler) {
    var url = "rest/api/asffile?type=attachment";
    url += nodeId ? '&node=' + nodeId : '';
    url += dataId ? '&data=' + dataId : '';

    return _jquery2.default.ajax({
        beforeSend: addAuthHeader,
        url: getFullUrl(url),
        data: data,
        cache: false,
        contentType: false,
        processData: false,
        type: 'POST',
        success: handler,
        error: errorHandler,
        statusCode: {
            401: function _() {
                services.unAuthorized();
            }
        }
    });
}
function copyFileToForm(data, handler) {
    simpleAsyncPost('rest/api/storage/asffile/copy', handler, "json", JSON.stringify(data), 'application/json; charset=utf8');
}
function getFileInfo(id, handler, errorHandler) {
    simpleAsyncGet('rest/api/storage/description?elementID=' + id + "&mobile=false", getHandler(handler, false, errorHandler), null, null, errorHandler);
}
function getProjectTreeItemInfo(actionID, handler) {
    simpleAsyncGet('rest/api/projects/get_project_tree_item_info?actionID=' + actionID, getHandler(handler));
}
function translate(value, locale, handler) {
    simpleAsyncGet('rest/api/formPlayer/translate?value=' + encodeURIComponent(value) + "&locale=" + locale, handler, "text");
}
function getDocMeaningContent(registryID, asfDataUUID, handler) {
    simpleAsyncGet('rest/api/formPlayer/getDocMeaningContent?registryID=' + registryID + "&asfDataUUID=" + asfDataUUID, handler, "text");
}
function getAsfDataUUID(documentID, handler) {
    simpleAsyncGet('rest/api/formPlayer/getAsfDataUUID?documentID=' + documentID, handler, "text");
}
function translateMultiple(values, locale, handler) {
    var params = "";
    values.forEach(function (value) {
        params += "&value=" + encodeURIComponent(value);
    });
    simpleAsyncGet('rest/api/formPlayer/translateMultiple?locale=' + locale + params, handler);
}

function removeFile(elementID, handler) {
    var orgParams = {
        locale: _constants.OPTIONS.locale
    };
    simpleAsyncPost("rest/api/storage/remove?" + _jquery2.default.param(orgParams), handler, null, { elementID: elementID });
}
function getRegistry(registryID, handler) {
    var orgParams = {
        registryID: registryID,
        locale: _constants.OPTIONS.locale
    };
    simpleAsyncGet("rest/api/registry/info?" + _jquery2.default.param(orgParams), handler);
}

function getRegistryData(registryID, pageNumber, countInPart, searchText, sortCmpID, sortDesc, handler) {
    var params = {
        registryID: registryID,
        pageNumber: pageNumber,
        countInPart: countInPart,
        searchString: searchText,
        sortCmpID: sortCmpID,
        sortDesc: sortDesc
    };

    services.showWaitWindow();

    simpleAsyncGet('rest/api/registry/data_ext?' + _jquery2.default.param(params), getHandler(handler, true));
}

function getRegistryFilterData(registryID, filterID, pageNumber, countInPart, searchText, sortCmpID, sortDesc, handler) {
    var params = {
        registryID: registryID,
        filterID: filterID,
        pageNumber: pageNumber,
        countInPart: countInPart,
        searchString: searchText,
        sortCmpID: sortCmpID,
        sortDesc: sortDesc
    };

    services.showWaitWindow();

    simpleAsyncGet('rest/api/registry/data_ext?' + _jquery2.default.param(params), getHandler(handler, true));
}

function getAddressBookData(itemType, search, page, handler) {
    var params = {
        itemType: itemType,
        search: search,
        page: page
    };
    simpleAsyncGet('rest/api/addressbook/items?' + _jquery2.default.param(params), handler);
}

function getCustomComponent(code, handler) {
    var params = {
        code: code
    };
    simpleAsyncGet('rest/api/formPlayer/getCustomComponent?' + _jquery2.default.param(params), handler);
}

/***/ }),

/***/ 81:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.USER_CHOOSER_SUFFIXES = undefined;
exports.default = UserChooser;

var _apiUtils = __webpack_require__(8);

var api = _interopRequireWildcard(_apiUtils);

var _i18n = __webpack_require__(14);

var _i18n2 = _interopRequireDefault(_i18n);

var _BasicChooserDialog = __webpack_require__(71);

var _BasicChooserDialog2 = _interopRequireDefault(_BasicChooserDialog);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function _interopRequireWildcard(obj) { if (obj && obj.__esModule) { return obj; } else { var newObj = {}; if (obj != null) { for (var key in obj) { if (Object.prototype.hasOwnProperty.call(obj, key)) newObj[key] = obj[key]; } } newObj.default = obj; return newObj; } }

/**
 * суффиксы для разных типов объектов которые можно засунуть в юзерчузер
 */
var USER_CHOOSER_SUFFIXES = exports.USER_CHOOSER_SUFFIXES = {
    group: 'g',
    contact: 'contact',
    text: 'text'
};

function UserChooser(multiSelect, showGroups, locale) {

    var instance = this;
    var showAll = true;
    var filterDepartmentID = null;
    var filterPositionID = null;
    var showNoPosition = true;

    var dialog = new _BasicChooserDialog2.default();
    dialog.init({
        showListItemIcon: true,
        showListItemInfo: true,
        showListItemStatus: true,
        listItemIconSize: 32,
        multiSelect: multiSelect,
        showSearchField: true,
        title: _i18n2.default.tr("Выбор пользователя"),
        placeHolderText: _i18n2.default.tr("Поиск пользователей"),
        showListItemIconStyle: "ns-userChooserIcon",
        showSelectedStack: multiSelect
    });

    this.correctListArray = function (users) {
        if (!users) {
            return [];
        }
        users.forEach(function (user) {
            user.id = user.personID;
            user.name = user.personName;
            user.info = user.positionName;

            if (user.personID.indexOf(USER_CHOOSER_SUFFIXES.group) === 0) {
                user.icon = "js/asforms/resources/group.png";
            } else if (user.personID.indexOf(USER_CHOOSER_SUFFIXES.contact) === 0) {
                user.icon = "js/asforms/resources/unknown.png";
            } else if (user.personID.indexOf(USER_CHOOSER_SUFFIXES.text) === 0) {
                user.icon = "js/asforms/resources/unknown.png";
            } else {
                user.icon = "rest/formPlayerIconService/photo?max_width=32&max_height=32&inscribe=false&personID=" + user.personID;
            }

            if (user.customFields && user.customFields.calendarStatusLabel) {
                user.status = user.customFields.calendarStatusLabel;
                user.statusColor = user.customFields.calendarColor;
            }
        });
        return users;
    };

    dialog.addTree("departments", function (parentNode, tree) {

        var departmentID = undefined;
        if (parentNode) {
            departmentID = parentNode.id;
        }
        api.getDepartments(departmentID, locale, function (departments) {
            dialog.setTreeNodeData("departments", departmentID, departments.departments, "departmentID", "departmentName", "hasChildDepartments");
        });
    }, function (parentNode, tree) {
        dialog.setListTitle(parentNode.name);
        dialog.showList("users", true);
    });

    dialog.addListType("users", function (search, startRecord, recordsCount, selectedNodeId, stackId) {
        if (stackId === "groups") {
            if (selectedNodeId == "-1") {
                selectedNodeId = "0";
            }
            api.getUsers(search, filterPositionID, filterDepartmentID, null, selectedNodeId, showNoPosition, startRecord, recordsCount, showAll, false, false, locale, function (users) {
                dialog.setListData("users", instance.correctListArray(users), startRecord > 0);
            });
        } else {
            api.getUsers(search, filterPositionID, filterDepartmentID, selectedNodeId, 0, showNoPosition, startRecord, recordsCount, showAll, false, false, locale, function (users) {
                dialog.setListData("users", instance.correctListArray(users), startRecord > 0);
            });
        }
    }, function (search, selectedNodeId, stackId) {
        api.getUsers(search, filterPositionID, filterDepartmentID, selectedNodeId, 0, showNoPosition, 0, -1, showAll, false, false, locale, function (users) {
            dialog.setSelectedItems(instance.correctListArray(users));
        });
    });

    dialog.addListType("often_chosen", function (search, startRecord, recordsCount, selectedNodeId, stackId) {
        console.log("loading " + selectedNodeId + " " + stackId);
        api.getOftenUsers(search, filterPositionID, filterDepartmentID, selectedNodeId, showNoPosition, startRecord, recordsCount, showAll, false, locale, function (users) {
            dialog.setListData("often_chosen", instance.correctListArray(users), startRecord > 0);
        });
    }, function (search, selectedNodeId, stackId) {
        api.getOftenUsers(search, filterPositionID, filterDepartmentID, selectedNodeId, showNoPosition, 0, -1, showAll, false, locale, function (users) {
            dialog.setSelectedItems(instance.correctListArray(users));
        });
    });

    dialog.addStack("often_chosen", _i18n2.default.tr("Часто выбираемые"), null, true, true, function () {
        dialog.showList("often_chosen", true);
    });

    dialog.addTree("groups", function (parentNode, tree) {
        if (parentNode) {

            var groupId = parentNode.id;
            if (groupId == '-1') {
                groupId = "0";
            }

            api.getGroups(groupId, 0, 0, locale, function (groups) {
                dialog.setTreeNodeData("groups", parentNode.id, groups.array, "groupID", "name", "innerGroupsCount");
            });
        } else {
            setTimeout(function () {
                dialog.setTreeNodeData("groups", null, [{
                    groupID: "-1",
                    name: _i18n2.default.tr("Группы"),
                    innerGroupsCount: 1
                }], "groupID", "name", "innerGroupsCount");
            }, 100);
        }
    }, function (parentNode, tree) {
        dialog.setListTitle(parentNode.name);
        if (showGroups && multiSelect) {
            dialog.showList("groups", parentNode, true);
        } else {
            dialog.showList("users", parentNode, true);
        }
    });

    if (showGroups) {
        if (multiSelect) {
            dialog.addListType("groups", function (search, startRecord, recordsCount, selectedNodeId) {
                if (selectedNodeId === "-1") {
                    api.getAllGroups(search, selectedNodeId, startRecord, recordsCount, locale, function (groups) {
                        groups.array.forEach(function (group) {
                            group.personID = "g-" + group.groupID;
                            group.personName = group.name;
                            group.positionName = group.parentName;
                        });

                        dialog.setListData("groups", instance.correctListArray(groups.array), startRecord > 0);
                    });
                } else {
                    api.getUsers(search, filterPositionID, filterDepartmentID, null, selectedNodeId, showNoPosition, startRecord, recordsCount, showAll, false, true, locale, function (users) {
                        dialog.setListData("groups", instance.correctListArray(users), startRecord > 0);
                    });
                }
            }, function (search, selectedNodeId) {
                if (selectedNodeId === "-1") {
                    api.getAllGroups(search, selectedNodeId, 0, 0, locale, function (groups) {
                        groups.array.forEach(function (group) {
                            group.personID = "g-" + group.groupID;
                            group.personName = group.name;
                            group.positionName = group.parentName;
                        });

                        dialog.setSelectedItems(instance.correctListArray(groups.array));
                    });
                } else {
                    api.getUsers(search, filterPositionID, filterDepartmentID, null, selectedNodeId, showNoPosition, 0, 0, showAll, false, true, locale, function (users) {
                        dialog.setSelectedItems(instance.correctListArray(users));
                    });
                }
            });
        }
        dialog.addStack("groups", _i18n2.default.tr("Группы пользователей"), "groups", true, true, function () {});
    }

    dialog.addStack("structure", _i18n2.default.tr("Орг. структура"), "departments", true, true, function () {});

    dialog.showStack("structure");

    this.setShowAll = function (newValue) {
        showAll = newValue;
    };

    this.setFilterDepartmentID = function (newValue) {
        filterDepartmentID = newValue;
    };

    this.setFilterPositionID = function (newValue) {
        filterPositionID = newValue;
    };

    this.setShowNoPosition = function (newValue) {
        showNoPosition = newValue;
    };

    this.setSelectedItems = function (newSelectedPersons) {
        dialog.setSelectedItems(instance.correctListArray(newSelectedPersons));
    };

    this.getSelectedItems = function () {
        return dialog.getSelectedItems();
    };

    this.showDialog = function () {
        dialog.show();
    };

    /**
     *
     * @param event SEE BasicChooserEvent
     * @param handler
     */
    this.on = function (event, handler) {
        dialog.on(event, handler);
    };
};

/***/ }),

/***/ 82:
/***/ (function(module, exports, __webpack_require__) {

"use strict";


Object.defineProperty(exports, "__esModule", {
    value: true
});
exports.inc = inc;
exports.dec = dec;
exports.gt = gt;
exports.gte = gte;
exports.changeIf = changeIf;
exports.eqLens = eqLens;
exports.eqLensObj = eqLensObj;
exports.every = every;
exports.curryContains = curryContains;
exports.updateLensF = updateLensF;
exports.contains = contains;
exports.reverse = reverse;
exports.clone = clone;
exports.insertEnteredData = insertEnteredData;

var _underscore = __webpack_require__(7);

var _underscore2 = _interopRequireDefault(_underscore);

var _jquery = __webpack_require__(1);

var _jquery2 = _interopRequireDefault(_jquery);

function _interopRequireDefault(obj) { return obj && obj.__esModule ? obj : { default: obj }; }

function inc(v) {
    return v + 1;
}
function dec(v) {
    return v - 1;
}
function gt(v1) {
    return function (v) {
        return v > v1;
    };
}

function gte(v1) {
    return function (v) {
        return v >= v1;
    };
}

function changeIf(change, lens, pred) {
    return function (obj) {
        var value = lens.view(obj);
        if (!pred(value)) return obj;
        return lens.update(change(value), obj);
    };
}

function eqLens(lens, value) {
    return function (obj) {
        return _underscore2.default.isEqual(lens.view(obj), value);
    };
}

function eqLensObj(lens, obj) {
    return function (obj2) {
        return _underscore2.default.isEqual(lens.view(obj), lens.view(obj2));
    };
}

function every(fs) {
    return function (obj) {
        return _underscore2.default.chain(fs).map(function (f) {
            return f(obj);
        }).every().value();
    };
}

function curryContains(list) {
    return function (obj) {
        return _underscore2.default.contains(list, obj);
    };
}

function updateLensF(view, update) {
    return function (f, obj) {
        return update(f(view(obj)), obj);
    };
}

function contains(list) {
    return function (v) {
        return _underscore2.default.contains(list, v);
    };
}

function reverse(arr) {
    return _underscore2.default.reduceRight(arr, function (mem, obj) {
        mem.push(obj);
        return mem;
    }, []);
}

function clone(obj) {
    return _jquery2.default.extend(true, {}, obj);
}

/**
 * вставить значение в указанное место если значение подходит под маску
 * @param enterData значение
 * @param caretPosition полоджение курсора
 * @param splitMask маска
 * @param textBox тектовое поле
 */
function insertEnteredData(enterData, caretPosition, splitMask, textBox) {

    var value = textBox.val();

    // запрещает печатать что-либо длинее маски
    if (enterData !== null && caretPosition === splitMask.length) {
        return;
    }

    // проверяем если данные такой же длины как маска - пропускаем
    if (enterData && enterData.length > 0) {
        if (maskUtils.isValidSymbol(enterData, splitMask[caretPosition])) {
            value = value.substring(0, caretPosition) + enterData + value.substring(caretPosition + 1);
            caretPosition++;
        }

        while (caretPosition < splitMask.length) {
            if (!maskUtils.isStaticSymbol(splitMask[caretPosition])) {
                break;
            }
            caretPosition++;
        }

        textBox.val(value);

        caretUtils.setCaretPosition(textBox[0], caretPosition);
    }
}

/***/ })

},[397]);


// WEBPACK FOOTER //
// form.player.js