let UTILS = {
  createField: function(fieldData) {
    let field = {};
    for (let key in fieldData) field[key] = fieldData[key];
    return field;
  },
  getValue: function(data, cmpID) {
    data = data.data ? data.data : data;
    for(let i = 0; i < data.length; i++)
    if (data[i].id === cmpID) return data[i];
    return null;
  },
  setValue: function(asfData, cmpID, data) {
    let field = this.getValue(asfData, cmpID);
    if(field) {
      for (let key in data) {
        if(key === 'id' || key === 'type') continue;
        field[key] = data[key];
      }
      return field;
    } else {
      asfData = asfData.data ? asfData.data : asfData;
      field = this.createField(data);
      field.id = cmpID;
      asfData.push(field);
      return field;
    }
  }
};

function replaceData(data, replaceValue) {
  data.forEach(item => {
    AS.SERVICES.showWaitWindow();
    try {
      AS.FORMS.ApiUtils.loadAsfData(item.dataUUID)
      .then(asfData => {
        UTILS.setValue(asfData, 'base_rate_tarif', replaceValue);
        AS.FORMS.ApiUtils.saveAsfData(asfData.data, asfData.form, asfData.uuid)
        .then(res => {
          AS.SERVICES.hideWaitWindow();
          console.log('Данные сохранены, dataUUID: ' + res);
        });
      });
    } catch (e) {
      console.log('error', e, item);
    } finally {
      AS.SERVICES.hideWaitWindow();
    }
  });
}

let button = $('<button>')
.css({
  'border-radius': '5px',
  'border': 'none',
  'background-color': '#f47172',
  'color': '#ffffff',
  'font-family': 'arial, tahoma, sans-serif',
  'font-size': '12pt',
  'font-weight': 'bold',
  'text-shadow': '#000000 0 0 2px',
  'cursor': 'pointer',
  'padding': '10px'
})
.hover(function() {
  $(this).css("background-color", "#b05253");
}, function() {
  $(this).css("background-color", "#f47172");
})
.text('изменение базового тарифа')
.on('click', () => {
  try {
    let replaceValue = model.playerModel.getModelWithId('base_rate_tarif');
    let text_laboratory_name = model.playerModel.getModelWithId('text_laboratory_name');
    if(replaceValue && replaceValue.getValue() && replaceValue.getValue() != '' && text_laboratory_name) {
      AS.SERVICES.showWaitWindow();
      let msg = $('<div id="custom-msg" style="font-size: 34px;font-weight: bold;color: red;background-color: rgba(0, 0, 0, 0.9);position: fixed;top: 10px;width: 100%;height: 10%;display: grid;align-content: center;">Подождите, идет поиск данных...</div>');
      $('#mngmnt_wait_div table td').append(msg);
      text_laboratory_name = text_laboratory_name.getAsfData();
      AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/data_ext?registryCode=reestr_issledovanii_po_gos_zakazu&loadData=false&field=text_laboratory_name&key=${text_laboratory_name.key}&condition=CONTAINS`)
      .then(res => {
        AS.SERVICES.hideWaitWindow();
        msg.remove();
        if(res.count == 0) {
          alert(`По лаборатории "${text_laboratory_name.value}" не найдено записей в реестре "Реестр исследовании по гос.заказу".`);
        } else {
          let confirmResult = confirm(`По лаборатории "${text_laboratory_name.value}" найдено ${res.count} записей в реестре "Реестр исследовании по гос.заказу". Запустить изменение базового тарифа на ${replaceValue.getValue()}?`);
          if(confirmResult) replaceData(res.data, replaceValue.getAsfData());
        }
      });
    } else {
      alert('Не заполнено поле base_rate_tarif');
    }
  } catch (e) {
    AS.SERVICES.hideWaitWindow();
    alert('Произошла какая-то неведомая ошибка');
    console.log('::: ERROR :::', e);
  }
});

view.container.append(button);
