let dashboardURL = window.location.origin + '/kibana/app/kibana#/dashboard/covid19?embed=true';
let refreshPeriodInSecond = 120; // 2 минуты
let defaultParam = `&_g=(refreshInterval:(display:Off,pause:!f,value:${refreshPeriodInSecond*1000}),time:(from:now-4h,mode:quick,to:now))`;
let newURL = dashboardURL + defaultParam;

let container = $('.dashboard-container');
let periodBlock = $('<div>', {class: 'period-container'});
let startInput = $('<input class="custom-input-date" type="date" name="start-date" min="2020-01-01">');
let stopInput = $('<input class="custom-input-date" type="date" name="stop-date" min="2020-01-01" disabled>');
let button = $('<button id="changed-date" class="uk-button uk-button-default uk-button-small" uk-icon="check" title="Применить"></button>');
let iFrame = $(`<iframe id="dashboard-frame" src="${newURL}" width="100%" height="100%">`);


periodBlock
.append(startInput)
.append('<span style="color: white;"> - </span>')
.append(stopInput)
.append(button)
.append('<div class="covid19-logo">COVID 19</div>');

container.append(periodBlock).append(iFrame);

//refreshInterval:(value:${refreshPeriodInSecond*1000}), - пока убрал, из-за того что огромные периоды выбирают

button.css('margin-bottom', '3px');
button.on('click', e => {
  e.preventDefault();
  e.target.blur();
  let start = startInput.val();
  let stop = stopInput.val();
  if (start == '' || stop == '') return;
  let param = `&_g=(time:(from:'${start}T00:00:00.000Z',mode:absolute,to:'${stop}T23:59:59.000Z'))`;
  newURL = dashboardURL + param;
  iFrame.attr('src', newURL);
});

startInput.on('change', e => {
  let val = startInput.val();
  if (val == '') {
    stopInput.attr('min', '2020-01-01');
    stopInput.attr('disabled', 'disabled');
  } else {
  	stopInput.val(val);
    stopInput.attr('min', val);
    stopInput.attr('disabled', null);
  }
});
