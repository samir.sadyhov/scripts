function duplicateInit(value) {
  if(!editable || !view.playerView.editable) return;
  if(!value) return;
  let tableModel = model.playerModel.getModelWithId(model.asfProperty.ownerTableId);
  tableModel.modelBlocks.forEach(row => {
    let tmpModel = model.playerModel.getModelWithId(model.asfProperty.id, model.asfProperty.ownerTableId, row.tableBlockIndex);
    if(tmpModel && !tmpModel.getValue()) tmpModel.setValue(value);
  });
}

model.on("valueChange", (_1, _2, value) => duplicateInit(value));
