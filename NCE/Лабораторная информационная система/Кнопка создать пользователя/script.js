class CreateUser {

  constructor() {
    this.lastname = null;
    this.firstname = null;
    this.patronymic = null;
    this.user_in_system = null;
  }

  createButton(){
    return $('<button id="nce-button-createuser" class="activity-button uk-button uk-button-default">')
    .text('Создать нового пользователя');
  }

  isCreateUser(){
    if(!this.lastname || !this.firstname) return false;
    if(this.lastname && this.firstname && this.user_in_system.getValue().length > 0) return false;
    return true;
  }

  randomInteger () {
    return Math.round(9999.5 + Math.random() * 99001);
  }

  createNewUser(){
    AS.SERVICES.showWaitWindow();
    
    let data = {
      email: model.playerModel.getModelWithId('crm_form_contact_main_email').getValue(),
      lastname: this.lastname,
      firstname: this.firstname,
      patronymic: this.patronymic,
      hasAccess: false,
      hasPointersBookAccess: false,
      hasPrivateFolder: true,
      hasStrategyAccess: false,
      isAdmin: false,
      isChancellery: false,
      isConfigurator: false,
      pointersCode: `${this.lastname}_${this.firstname}_${this.randomInteger()}`
    };

    AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/filecabinet/user/save", null, null, data, "application/x-www-form-urlencoded", null)
    .then(res => {
      if(res.errorCode == "13") {
        AS.SERVICES.showErrorMessage(`Ошибка создания пользователя<br>${res.errorMessage}`);
      } else {
        this.userID = res.userID;
        window.showMessage(i18n.tr("Пользователь создан"));

        //Назначение на должность
        AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/positions/appoint?positionID=${model.positionID}&userID=${this.userID}`).then(result => {
          if(result.errorCode != "0") {
            AS.SERVICES.showErrorMessage(`Ошибка назначения на должность<br>${result.errorMessage}`);
          }
          this.user_in_system.setValue([{
            personID: this.userID,
            personName: `${this.lastname} ${this.firstname} ${this.patronymic}`,
            tagName: `${this.lastname} ${this.firstname} ${this.patronymic}`
          }]);
          AS.SERVICES.hideWaitWindow();
        });

      }
    });
  }

  init(user) {
    this.lastname = user.lastname;
    this.firstname = user.firstname;
    this.patronymic = user.patronymic;
    this.user_in_system = user.user_in_system;

    if(!this.isCreateUser()) return;
    if($('#nce-button-createuser').length > 0) return;
    let button = this.createButton().click(() => this.createNewUser());
    if(editable) {
      button.css({
        'position': 'relative',
        'left': '50px'
      });
    }
    view.container.append(button);
  }

}


(function(){
	try {
    let modelLastname = model.playerModel.getModelWithId('crm_form_contact_main_lastName');
    let modelFirstname = model.playerModel.getModelWithId('crm_form_contact_main_firstName');
    let modelPatronymic = model.playerModel.getModelWithId('crm_form_contact_main_patronymic');

    let buttonUserCreat = new CreateUser();

    buttonUserCreat.init({
      lastname: modelLastname.getValue(),
      firstname: modelFirstname.getValue(),
      patronymic: modelPatronymic.getValue(),
      user_in_system: model.playerModel.getModelWithId('user_in_system')
    });

    //Подписываемся на изменения полей ФИО
    [modelLastname, modelFirstname, modelPatronymic].forEach(m => m.on("valueChange", () => {
      buttonUserCreat.init({
        lastname: modelLastname.getValue(),
        firstname: modelFirstname.getValue(),
        patronymic: modelPatronymic.getValue(),
        user_in_system: model.playerModel.getModelWithId('user_in_system')
      })
    }));

  } catch (e) {
		console.error(e);
	}
})();
