let UTILS = {
  createField: function(fieldData) {
    let field = {};
    for (let key in fieldData) field[key] = fieldData[key];
    return field;
  },
  getValue: function(data, cmpID) {
    data = data.data ? data.data : data;
    for(let i = 0; i < data.length; i++)
    if (data[i].id === cmpID) return data[i];
    return null;
  },
  setValue: function(asfData, cmpID, data) {
    let field = this.getValue(asfData, cmpID);
    if(field) {
      for (let key in data) {
        if(key === 'id' || key === 'type') continue;
        field[key] = data[key];
      }
      return field;
    } else {
      asfData = asfData.data ? asfData.data : asfData;
      field = this.createField(data);
      field.id = cmpID;
      asfData.push(field);
      return field;
    }
  }
};

function searchDocs() {
  let reportUrl = `rest/api/report/do?reportCode=search_doc&fileName=1.xml`;
  return $.when(AS.FORMS.ApiUtils.simpleAsyncPost(reportUrl, null, 'text')).then(data => parseResult(data));
}

function parseResult(xml) {
  let result = [];
  $($.parseXML(xml)).find('textContent').each((i, item) => {
    let row = $(item).text().split(':::');
    result.push({
      documentID: row[0],
      asfDataID: row[1],
      etalon: row[2],
      cmp_key: row[3],
      cmp_id: row[4]
    })
  });
  return result;
}

function createTable(data) {
	let container = $('<div>', {class: 'table-container'});
  let table = $('<table>', {class: 'search-result'});
  let thead = $('<thead>');
  let body = $('<tbody>');
  let tr = $('<tr>');

  for (let key in data[0]) {
    tr.append($('<th>').text(key))
  }

  data.forEach(row => {
    let tr = $('<tr>');
    for (let key in row) {
      tr.append($('<td>').text(row[key]))
    }
    body.append(tr);
  });

  thead.append(tr);
  table.append(thead).append(body);
  container.append(table);
  return container;
}

function showDialog(table, data) {
  let modal = $('<div>').append(table);
  $("body").append(modal);
  modal.dialog({
    modal: true,
    width: 800,
    height: 400,
    title: "Список найденых документов - " + data.length,
    buttons: [{
        text: "Закрыть",
        click: function() {
          $(this).dialog("close");
        }
      },
      {
        text: "Заменить значения",
        click: function() {
          replaceData(data);
        }
      }
    ],
    close: function() {
      $(this).remove();
    }
  });
}

function replaceData(data) {
  data.forEach(item => {
    AS.SERVICES.showWaitWindow();
    AS.FORMS.ApiUtils.loadAsfData(item.asfDataID)
    .then(asfData => {
      let table = UTILS.getValue(asfData, 'dynamic_table');
      let field = UTILS.getValue(table, item.cmp_id);
      field.key = item.etalon;
      field.value = item.etalon;
      AS.FORMS.ApiUtils.saveAsfData(asfData.data, asfData.form, asfData.uuid)
      .then(res => {
        AS.SERVICES.hideWaitWindow();
        window.showMessage('Данные сохранены, documentID: ' + item.documentID);
      });
    });
  });
}

let button = $('<button>', {class: 'custom-button'})
.text('замена весового коэффициента')
.on('click', () => {
  AS.SERVICES.showWaitWindow();
  let msg = $('<div id="custom-msg" style="font-size: 34px;font-weight: bold;color: red;background-color: rgba(0, 0, 0, 0.9);position: fixed;top: 10px;width: 100%;height: 10%;display: grid;align-content: center;">Подождите, идет поиск данных...</div>');
  $('#mngmnt_wait_div table td').append(msg);
  searchDocs().then(result => {
    AS.SERVICES.hideWaitWindow();
    msg.remove();
    if(result.length == 0) {
      window.showMessage('Данные не найдены');
      return;
    }
    let table = createTable(result);
    showDialog(table, result);
  });
});


view.container.append(button);
