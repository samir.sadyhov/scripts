let cmps = [];
cmps.push('oblast');
cmps.push('rayon');
cmps.push('locality');
cmps.push('crm_form_account_main_adress');


function collectData(){
  let result = [];
  cmps.forEach(cmp => {
    let val = model.playerModel.getModelWithId(cmp).getTextValue();
    if(val) {
      if(typeof val == "string") result.push(val);
      if(typeof val == "object") {
        if(!val.length && val.value) result.push(val.value);
        else result.push(val.join(' '));
      }
    }
  });
  return result.length > 0 ? result.join(' ') : null;
}


model.playerModel.on(AS.FORMS.EVENT_TYPE.dataLoad, () => {
  model.setValue(collectData());
  cmps.forEach(cmp => {
    model.playerModel.getModelWithId(cmp).on('valueChange', () => {
      model.setValue(collectData());
    })
  });
});
