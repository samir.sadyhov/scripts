/* Настройки:
model.dictCode = "spravochnik_spisok_naselennyh_punktov"; // код справочника
model.columnCodeKey = "spravochnik_spisok_naselennyh_punktov_key"; // код столбца значения (value)
model.columnCodeValue = "spravochnik_spisok_naselennyh_punktov_value"; // код столбца для отображения (title)
model.columnCodeFilter = "spravochnik_spisok_raionov_key"; // код столбца фильтрации значений
model.filterCmpID = "rayon"; // компонент выпадающий список от которого
                            // будет идти фильтрация (можно использовать этот же компонент)
*/
let input = $(`<input class="asf-dropdown-input uk-select" data-asformid="custom.input.${model.asfProperty.id}" readonly>`);
let button = $(`<button class="asf-dropdown-button" data-asformid="custom.button.${model.asfProperty.id}">`).hide();
let label = $(view.container).children(".asf-label");
view.container.append(input).append(button);

if (editable) {
  label.hide();
  input.show();
  if(window.location.href.indexOf('customers') == -1) button.show();
} else {
  label.show();
  button.hide();
  input.hide();
}

model.getAsfData = function(blockNumber){
  if(model.getValue())
  return AS.FORMS.ASFDataUtils.getBaseAsfData(model.asfProperty, blockNumber, model.getValue().value , model.getValue().key);
  return AS.FORMS.ASFDataUtils.getBaseAsfData(model.asfProperty, blockNumber);
};
model.setAsfData = function(asfData){
  if(!asfData || !asfData.value || !asfData.key) return;
  model.setValue({key: asfData.key, value: asfData.value});
};

view.updateValueFromModel = function () {
  if (model.getValue()) {
    label.text(model.getValue().value);
    input.val(model.getValue().value);
    view.unmarkInvalid();
  } else {
    label.text("");
    input.val("");
  }
};
view.updateValueFromModel();

let nce_kato = new NCEdropdownKATO();
nce_kato.init();
button.on('click', () => nce_kato.showDropDown());
input.on('click', () => nce_kato.showDropDown());

model.playerModel.getModelWithId(model.filterCmpID, model.asfProperty.ownerTableId, model.asfProperty.tableBlockIndex).on("valueChange", () => {
  model.setValue(null);
  nce_kato.purgeItems();
  nce_kato.init();
});

function NCEdropdownKATO(){
  let items = null;

  this.init = () => {
    AS.SERVICES.showWaitWindow();

    let setDefaultValue = () => {
      if(model.getValue() && model.getValue().key !== "0") return;
      if(items && items.length > 0) model.setAsfData({key: items[0].value, value: items[0].title});
      else model.setValue(null);
    }

    if(items) {
      AS.SERVICES.hideWaitWindow();
      setDefaultValue();
    } else {
      let keyColumnID = null;
      let valueColumnID = null;

      $.when(AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/dictionaries/${model.dictCode}?getItems=false`))
      .then(res => {
        let filterValue = model.playerModel.getModelWithId(model.filterCmpID, model.asfProperty.ownerTableId, model.asfProperty.tableBlockIndex).getAsfData().key;
        keyColumnID = res.columns[model.columnCodeKey].id;
        valueColumnID = res.columns[model.columnCodeValue].id;
        let filterColumnID = res.columns[model.columnCodeFilter].id;
        return AS.FORMS.ApiUtils.simpleAsyncPost(`rest/api/dictionary/record/findDictionaryItems?orgid=${filterValue}&orici=${filterColumnID}`);
      })
      .then(response => {
        //возвращаем отфильтрованный список населенных пунктов
        items = response.map(item => {
          return {title: item.values[valueColumnID].value, value: item.values[keyColumnID].value};
        })
        .sort((a, b) => {
          if(a.title > b.title) return 1;
          if(a.title < b.title) return -1;
          return 0;
        });
        AS.SERVICES.hideWaitWindow();
        setDefaultValue();
      });
    }
  }

  this.showDropDown = () => {
    let getTitle = value => items.filter(region => region.value === value)[0].title;
    AS.SERVICES.showDropDown(items, input, 100, item => model.setAsfData({key: item, value: getTitle(item)}));
  }

  this.purgeItems = () => items = null;
}
