model.dialogWidth = model.dialogWidth || 600;
model.dialogHeight = model.dialogHeight || 500;

if(!editable) {
  let textView = jQuery('<div>', {class : "asf-label"});
  view.container.append(textView);
  view.updateValueFromModel = function(){
    if(model.getValue()) textView.html(model.getValue().name);
  };
} else {
  let inputView = jQuery('<div>', {class : "ns-tagContainer", style : "width:calc(100% - 30px)"});
  let button = jQuery('<button>', {class : "asf-browseButton"});
  view.container.append(inputView);
  view.container.append(button);

  view.updateValueFromModel = function(){
    if(model.getValue()) inputView.html(model.getValue().name);
  };

  button.on('click', () => {
    let treeChooser = new NceTreeChooser();
    treeChooser.initDialog();
    treeChooser.showDialog();
    treeChooser.on(AS.FORMS.BasicChooserEvent.applyClicked, function(){
      let selected = treeChooser.getSelectedValue();
      console.log("selected: ", selected);
      let value = { name : selected.text, id : selected.id, info : selected.value};
      model.setValue(value);
    });
  });
}

function NceTreeChooser(){
  let instance = this;
  let selectedValue = null;
  let regions = null;
  let dictID = {
    'nce_dict_picture_list_of_areas': null, //Справочник Список областей
    'spravochnik_spisok_raionov': null, //Справочник Список районов
    'spravochnik_spisok_naselennyh_punktov': null //Справочник Список населенных пунктов
  };
  let dialogContainer = jQuery('<div>');
  let treeContainer = jQuery('<div>', {
    style: "overflow: auto; width: " + (model.dialogWidth - 10) + "px; height: " + (model.dialogHeight - 120) + "px;"
  });
  let selectButton = jQuery('<button>', {class : "ns-approveButton", style: "margin-top: 20px;"}).button().html(i18n.tr("Выбрать"));
  selectButton.attr("disabled", "true");
  dialogContainer.append(treeContainer);
  dialogContainer.append(selectButton);
  AS.FORMS.ComponentUtils.makeBus(this);

  this.initDialog = function() {
    $.when(AS.FORMS.ApiUtils.simpleAsyncGet('rest/api/dictionaries'))
    .then(dictionaries => {
      //ищем айди справочников, в дальнейшем понадобиться при фильтрации значений
      dictionaries.forEach(x => {
        for(let key in dictID) if(x.code == key) dictID[key] = x.id;
      });
      //загружаем справочник областей
      return AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/dictionary/get_by_code?dictionaryCode=nce_dict_picture_list_of_areas&locale=${AS.OPTIONS.locale}`);
    })
    .then(response => {
      let keyColumnID = response.columns.filter(x => x.code === "nce_dict_picture_list_of_areas_key")[0].columnID;
      let valueColumnID = response.columns.filter(x => x.code === "nce_dict_picture_list_of_areas_value")[0].columnID;
      //возвращаем отфильтрованный список областей
      regions = response.items.map(item => {
        let key = item.values.filter(x => x.columnID === keyColumnID)[0].value;
        let value = item.values.filter(x => x.columnID === valueColumnID)[0].translation;
        return {key: key, id: key, value: value, text: value, children: [], parent: 'root'};
      })
      .sort((a, b) => {
        if(a.text > b.text) return 1;
        if(a.text < b.text) return -1;
        return 0;
      });
      //загружаем справочник районов
      return AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/dictionary/get_by_code?dictionaryCode=spravochnik_spisok_raionov&locale=${AS.OPTIONS.locale}`);
    })
    .then(response => {
      let keyColumnID = response.columns.filter(x => x.code === "spravochnik_spisok_raionov_key")[0].columnID;
      let valueColumnID = response.columns.filter(x => x.code === "spravochnik_spisok_raionov_value")[0].columnID;
      let parentColumnID = response.columns.filter(x => x.code === "nce_dict_picture_list_of_areas_key")[0].columnID;
      //возвращаем отфильтрованный список районов
      return response.items.map(item => {
        let key = item.values.filter(x => x.columnID === keyColumnID)[0].value;
        let parent = item.values.filter(x => x.columnID === parentColumnID)[0].value;
        let value = item.values.filter(x => x.columnID === valueColumnID)[0].value;
        return {key: key, id: key, value: value, text: value, children: [''], parent: parent};
      })
      .sort((a, b) => {
        if(a.text > b.text) return 1;
        if(a.text < b.text) return -1;
        return 0;
      });
    })
    .then(areas => {
      regions.forEach(region => {
        region.children = areas.filter(area => area.parent === region.id);
      });
      $(treeContainer).jstree({core: {
        data: [{id: "root", text: "Республика Казахстан", state: {opened: true}, children: regions}]
      }});
    });
  };

  $(treeContainer).on("select_node.jstree", function(evt, data) {
    selectedValue = data.node.original;
    if(selectedValue.id != 'root'){
      $(selectButton).prop('disabled', false);
    } else {
      $(selectButton).prop('disabled', true);
    }
  });

  this.showDialog = () => {
    dialogContainer.dialog({
      width: model.dialogWidth,
      height: model.dialogHeight,
      modal: true,
      resizable: false,
      title: 'Выбор населенного пункта'
    });
  };

  selectButton.on('click', event => {
    dialogContainer.dialog( "destroy" );
    dialogContainer.detach();
    instance.trigger(AS.FORMS.BasicChooserEvent.applyClicked);
  });

  this.getSelectedValue = function() {
    return selectedValue;
  };
}

model.getAsfData = function(blockNumber){
  if(model.getValue()) {
    let result = AS.FORMS.ASFDataUtils.getBaseAsfData(model.asfProperty, blockNumber, model.getValue().name , model.getValue().id);
    result.valueID = model.getValue().info;
    return result;
  } else {
    return AS.FORMS.ASFDataUtils.getBaseAsfData(model.asfProperty, blockNumber);
  }
};

model.setAsfData = function(asfData){
  if(!asfData || !asfData.value) {
    return;
  }
  let value = { name : asfData.value, id : asfData.key, info : asfData.valueID};
  model.setValue(value);
};

view.updateValueFromModel();
