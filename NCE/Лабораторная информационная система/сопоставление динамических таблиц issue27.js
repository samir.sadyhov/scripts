let matchingTable = {
  table: {from: 'table', to: 'table'},
  cmpIds: [
    {from: 'listbox_chart', to: 'listbox_chart'},
    {from: 'text_number_samples', to: 'text_number_samples'},
    {from: 'reglink_name_researh', to: 'reglink_name_researh', children: [
      {from: 'research_method', to: 'research_method'},
      {from: 'num_price', to: 'num_price'},
      {from: 'reglink_service_name', to: 'reglink_service_name'}
    ]}
  ]
};

let currentValue = model.getValue();

setTimeout(() => {
  currentValue = model.getValue();
}, 500);

const getTableBlocksCount = data => {
  data = data.data ? data.data : data;
  data = data.filter(row => row.type !== 'label');
  data = data[data.length - 1].id;
  return data.slice(data.indexOf('-b') + 2) * 1;
}

const getAsfData = (data, cmpID) => {
  data = data.data ? data.data : data;
  for(let i = 0; i < data.length; i++)
  if (data[i].id === cmpID) return data[i];
  return null;
}

const setChildrenFields = (childrenModel, documentID, matching) => {
  $.when(AS.FORMS.ApiUtils.getAsfDataUUID(documentID))
  .then(uuid => AS.FORMS.ApiUtils.loadAsfData(uuid))
  .then(asfData => {
    matching.forEach(id => {
      let tmpModel = childrenModel.playerModel.getModelWithId(id.to, childrenModel.asfProperty.ownerTableId, childrenModel.asfProperty.tableBlockIndex);
      let tmpField = getAsfData(asfData, id.from);
      if(tmpModel && tmpField) {
        if(tmpField.hasOwnProperty('key')) {
          tmpModel.setValue(tmpField.key);
        } else {
          tmpModel.setValue(tmpField.value);
        }
      }
    });
  });
}

const createTableRow = documentID => {
  AS.SERVICES.showWaitWindow();
  $.when(AS.FORMS.ApiUtils.getAsfDataUUID(documentID))
  .then(uuid => AS.FORMS.ApiUtils.loadAsfData(uuid))
  .then(asfData => {
    let tableFromData = getAsfData(asfData, matchingTable.table.from);
    let tableBlocks = getTableBlocksCount(tableFromData);
    let tableToModel = model.playerModel.getModelWithId(matchingTable.table.to);

    for(let i = 1; i < tableBlocks; i++) {
      let tableRow = tableToModel.createRow();
      matchingTable.cmpIds.forEach(item => {
        let tmpField = getAsfData(tableFromData, `${item.from}-b${i}`);
        let tmpModel = tableRow.filter(x => x.asfProperty.id === item.to)[0];
        if(tmpModel && tmpField) {
          if(tmpField.hasOwnProperty('key')) {
            tmpModel.setValue(tmpField.key);
          } else {
            tmpModel.setValue(tmpField.value);
          }
        }
        if(item.hasOwnProperty('children') && item.children.length > 0) {
          setChildrenFields(tmpModel, tmpField.key, item.children);
        }
      });
    }

  })
  .fail(err => console.log(err))
  .always(() => AS.SERVICES.hideWaitWindow());
}

const matchingTableInit = value => {
  if(!editable || !view.playerView.editable) return;
  if(!value) return;
  if(currentValue == value) return;
  currentValue = value;
  createTableRow(value);
}

setTimeout(() => {
  model.on("valueChange", (_1, _2, value) => matchingTableInit(value));
}, 1000);
