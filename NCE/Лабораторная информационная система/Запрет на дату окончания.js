function changeFunction(_1, _2, value){
  if(!editable) return;
  if(!AS.FORMS.DateUtils.parseDate(value)) return;

  AS.FORMS.ApiUtils.simpleAsyncGet('rest/api/registry/data_ext?registryCode=data_sdachi_otcheta&pageNumber=0&countInPart=1&fields=date_end')
  .then(res => {
    if(res.recordsCount == 0) return;
    let selectDate = AS.FORMS.DateUtils.parseDate(value);
    let date_end = AS.FORMS.DateUtils.parseDate(res.result[0].fieldKey.date_end);
    if(selectDate.getFullYear() < date_end.getFullYear()
    || selectDate.getMonth() < date_end.getMonth()) {
      view.markInvalid();
      model.trigger(AS.FORMS.EVENT_TYPE.markInvalid);
      AS.SERVICES.showErrorMessage("Счет-реест на указанную дату закрыть, измените дату");
      model.setValue(null);
    }
  });
}

model.off(AS.FORMS.EVENT_TYPE.valueChange, changeFunction);
model.on(AS.FORMS.EVENT_TYPE.valueChange, changeFunction);
