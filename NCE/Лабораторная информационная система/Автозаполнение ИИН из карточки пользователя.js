function getValue(data, cmpID) {
  data = data.data ? data.data : data;
  for(let i = 0; i < data.length; i++)
  if (data[i].id === cmpID) return data[i];
  return null;
}

function setUserIIN(userID){
  AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/personalrecord/forms/${userID}`).then(cards => {
    let cardUUID = cards.filter(item => item.formCode == "kartochka_pol_zovatelya_iin")[0]['data-uuid'];
    return AS.FORMS.ApiUtils.loadAsfData(cardUUID);
  }).then(asfData => {
    let iin = getValue(asfData, 'iin');
    if(iin && iin.value) iin = iin.value;
    model.playerModel.getModelWithId('iin').setValue(iin);
  });
}

model.playerModel.on(AS.FORMS.EVENT_TYPE.dataLoad, function(){
  setUserIIN(model.getValue()[0].personID);
});

model.on("valueChange", function(evt, thisModel, value) {
  setUserIIN(value[0].personID);
});
