(function(){
	try {
    let modelDocumentID = model.playerModel.getModelWithId('documentid');
    let modelDataUUID = model.playerModel.getModelWithId('datauuid');

    if(!modelDataUUID.getValue() && !modelDocumentID.getValue()) {
      AS.FORMS.ApiUtils.getDocumentIdentifier(model.playerModel.asfDataId)
      .then(docID => {
        modelDocumentID.setValue(docID);
        modelDataUUID.setValue(model.playerModel.asfDataId);
        AS.FORMS.ApiUtils.saveAsfData(model.playerModel.getAsfData().data, model.playerModel.formId, model.playerModel.asfDataId)
        .then(res => console.log(res));
      });
    }

  } catch (e) {
		console.error(e);
	}
})();
