let matching = [];
matching.push({from: 'research_method', to: 'research_method'});
matching.push({from: 'num_price', to: 'num_price'});
matching.push({from: 'reglink_service_name', to: 'reglink_service_name'});
matching.push({from: 'text_ laboratory_name', to: 'text_ laboratory_name'});
matching.push({from: 'listbox_chart', to: 'listbox_chart'});

let currentValue = model.getValue();

setTimeout(() => {
  currentValue = model.getValue();
}, 500);

const getAsfData = (data, cmpID) => {
  data = data.data ? data.data : data;
  for(let i = 0; i < data.length; i++)
  if (data[i].id === cmpID) return data[i];
  return null;
}

const clearFields = () => {
  matching.forEach(id => {
    let tmpModel = model.playerModel.getModelWithId(id.to, model.asfProperty.ownerTableId, model.asfProperty.tableBlockIndex);
    if (tmpModel) tmpModel.setValue(null);
  });
}

const setFields = (documentID) => {
  AS.SERVICES.showWaitWindow();
  $.when(AS.FORMS.ApiUtils.getAsfDataUUID(documentID))
  .then(uuid => AS.FORMS.ApiUtils.loadAsfData(uuid))
  .then(asfData => {
    matching.forEach(id => {
      let tmpModel = model.playerModel.getModelWithId(id.to, model.asfProperty.ownerTableId, model.asfProperty.tableBlockIndex);
      let tmpField = getAsfData(asfData, id.from);
      if(tmpModel && tmpField) {
        if(tmpField.hasOwnProperty('key')) {
          tmpModel.setValue(tmpField.key);
        } else {
          tmpModel.setValue(tmpField.value);
        }
      }
    });
  })
  .fail(err => console.log(err))
  .always(() => AS.SERVICES.hideWaitWindow());
}

const matchingInit = value => {
  if(!editable || !view.playerView.editable) return;
  if(!value) {
    clearFields();
    currentValue = value;
    return;
  }
  if(currentValue == value) return;
  currentValue = value;
  clearFields();
  setFields(value);
}

setTimeout(() => {
  model.on("valueChange", (_1, _2, value) => matchingInit(value));
}, 1000);
