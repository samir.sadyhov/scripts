class Result {
  constructor(container) {
    this.container = view.container;
    this.tableBlockIndex = model.asfProperty.tableBlockIndex;
    this.dataUUID = model.playerModel.getModelWithId('doc_datauuid', 'result_table', this.tableBlockIndex).getValue();
    this.documentID = model.playerModel.getModelWithId('doc_id', 'result_table', this.tableBlockIndex).getValue();
  }

  showModalPlayer() {
    let modal = $('<div class="nce-modal-player">');

    let player = AS.FORMS.createPlayer();
    player.view.setEditable(false);
    player.showFormData(null, null, this.dataUUID);
    player.view.appendTo(modal);

    $("body").append(modal);
    modal.dialog({
        modal: true,
        width: model.width || 1000,
        height: model.height || 600,
        resizable: false,
        title: 'Просмотр документа',
        show: {
          effect: 'fade',
          duration: 300
        },
        hide: {
          effect: 'fade',
          duration: 300
        },
        buttons: [
          {
            text: i18n.tr("Закрыть"),
            click: function() {
              $(this).dialog("close");
            }
          }
        ],
        close: function() {
          player.destroy();
          $(this).remove();
        }
    });
  }

  createButton(){
    return jQuery('<div id="nce-button-show">')
    .text("Просмотреть результат")
    .click(() => this.showModalPlayer());
  }

  init(){
    if(!this.dataUUID && !this.documentID) return;    
    if(this.dataUUID) {
      this.container.append(this.createButton());
    } else if(this.documentID) {
      AS.FORMS.ApiUtils.getAsfDataUUID(this.documentID)
      .then(dataUUID => {
        this.dataUUID = dataUUID;
        this.container.append(this.createButton());
      });
    }
  }
}

(function(){
	try {
    let showResult = new Result();
    showResult.init();
  } catch (e) {
		console.error(e);
	}
})();
