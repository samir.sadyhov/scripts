let UTILS = {
  createField: function(fieldData) {
    let field = {};
    for (let key in fieldData) field[key] = fieldData[key];
    return field;
  },
  getValue: function(data, cmpID) {
    data = data.data ? data.data : data;
    for(let i = 0; i < data.length; i++)
    if (data[i].id === cmpID) return data[i];
    return null;
  },
  setValue: function(asfData, cmpID, data) {
    let field = this.getValue(asfData, cmpID);
    if(field) {
      for (let key in data) {
        if(key === 'id' || key === 'type') continue;
        field[key] = data[key];
      }
      return field;
    } else {
      asfData = asfData.data ? asfData.data : asfData;
      field = this.createField(data);
      field.id = cmpID;
      asfData.push(field);
      return field;
    }
  }
};

function replaceData(items, dateValue) {
  items.forEach(item => {
    AS.SERVICES.showWaitWindow();
    try {
      AS.FORMS.ApiUtils.loadAsfData(item.dataUUID)
      .then(asfData => {
        let dateField = UTILS.setValue(asfData, 'research_end_date', {
          key: dateValue,
          value: dateValue
        });
        AS.FORMS.ApiUtils.saveAsfData(asfData.data, asfData.form, asfData.uuid)
        .then(res => console.log('сохранение данных, uuid', res, dateField));
      });
    } catch (e) {
      console.log('error', e, item);
    } finally {
      AS.SERVICES.hideWaitWindow();
    }
  });
}

let button = $('<button>')
.css({
  'border-radius': '5px',
  'border': 'none',
  'background-color': '#f47172',
  'color': '#ffffff',
  'font-family': 'arial, tahoma, sans-serif',
  'font-size': '12pt',
  'font-weight': 'bold',
  'text-shadow': '#000000 0 0 2px',
  'cursor': 'pointer',
  'padding': '10px'
})
.hover(function() {
  $(this).css("background-color", "#b05253");
}, function() {
  $(this).css("background-color", "#f47172");
})
.text('«Заявка по гос заказу», замена даты')
.on('click', () => {
  try {
    let replaceValue = model.playerModel.getModelWithId('research_end');
    if(AS.FORMS.DateUtils.parseDate(replaceValue.getValue())) {
      AS.SERVICES.showWaitWindow();
      let msg = $('<div id="custom-msg" style="font-size: 34px;font-weight: bold;color: red;background-color: rgba(0, 0, 0, 0.9);position: fixed;top: 10px;width: 100%;height: 10%;display: grid;align-content: center;">Подождите, идет поиск данных...</div>');
      $('#mngmnt_wait_div table td').append(msg);
      AS.FORMS.ApiUtils.simpleAsyncGet('rest/api/registry/data_ext?registryCode=reestr_registratsiya_ob_ekta&loadData=false&field=status&key=7&condition=NOT_CONTAINS&field1=research_end_date&key1=2020-02-01 00:00:00&condition1=MORE_OR_EQUALS&field2=research_end_date&key2=2020-02-29 23:59:59&condition2=LESS_OR_EQUALS')
      .then(res => {
        AS.SERVICES.hideWaitWindow();
        msg.remove();
        if(res.data.length == 0) {
          window.showMessage('Данные не найдены');
          return;
        }
        if(confirm(`Найдено ${res.count} записей, запустить замену даты на ${AS.FORMS.DateUtils.validateDateString(replaceValue.getValue())}?`)) {
          replaceData(res.data, AS.FORMS.DateUtils.validateDateString(replaceValue.getValue()));
        }
      });
    } else {
      AS.SERVICES.showErrorMessage('Заполните поле "Дата окончания"');
    }
  } catch (e) {
    AS.SERVICES.hideWaitWindow();
    AS.SERVICES.showErrorMessage('Произошла ошибка');
    console.log(e);
  }
});


view.container.append(button);
