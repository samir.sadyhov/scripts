class FinishWork {
  constructor() {
    this.check = false;
    this.actions = null;
    this.api = AS.FORMS.ApiUtils;
  }

  getActionIds(processes, userID) {
    let result = [];
    function search(p) {
      p.forEach(process => {
        if (!process.finished && process.responsibleUserID === userID) result.push(process);
        if (process.subProcesses.length > 0) search(process.subProcesses);
      });
    }
    search(processes);
    return result.map(x => x.actionID);
  }

  init(){
    if(this.check) return;
    $.when(this.api.getDocumentIdentifier(model.playerModel.asfDataId))
    .then(docID => this.api.simpleAsyncGet(`rest/api/workflow/get_execution_process?documentID=${docID}`))
    .then(processes => {
      this.actions = this.getActionIds(processes, AS.OPTIONS.currentUser.userId || model.playerModel.currentUser.userid);
      this.check = true;
      this.addButton();
    });
  }

  finishWork(){
    if(!model.playerModel.isValid()) {
      alert("Данные на форме не корректны. Пожалуйста проверьте данные и попробуйте завершить работу повторно.");
      return;
    }
    let data = {
      workID: this.actions[0],
      completionForm: "COMMENT",
      comment: "Работа завершена"
    }
    $.when(this.api.simpleAsyncPost('rest/api/workflow/work/set_result', null, null, data, "application/x-www-form-urlencoded; charset=utf-8"))
    .then(result => {
      if(result && result.errorCode == "0") {
        alert("Работа завершена");
        view.container.empty();
      } else {
        alert("При завершении работы возникла ошибка");
        console.log(result);
      }
    })
    .fail(err => {
      alert("При завершении работы возникла ошибка")
      console.log(err);
    });
  }

  addButton(){
    if(!this.check) {
      this.init();
      return;
    }
    if(this.actions && this.actions.length > 0) {
      let button = $('<button id="nce-button-finishwork">').text("Завершить").click(() => this.finishWork());
      view.container.append($('<div style="justify-content: center; display: flex;">').append(button));
    }
  }
}

(function(){
	try {
    let nceFinishWork = new FinishWork();
    nceFinishWork.init();
  } catch (e) {
		console.error(e);
	}
})();
