function loadUserSignKey(userId, success) {
  var cert = null;
  var endInfo = null;
  var alg = null;
  var certId = null;

  jQuery.when(jQuery.get("https://local.arta.pro:8389/?TYPE=INFO")).then(function(data){
    if (data === null) return "NO KEY";
    cert = data.CERT;
    endInfo = data.KEY_CN;
    return AS.FORMS.ApiUtils.simpleAsyncPost("rest/sign/verificationkey", null, "text", {uuid : userId, pemCer : cert, edsInfo : endInfo});

  }).then(function(result){
    if(result === null || result.indexOf("::::") === -1) {
      if(result === "CERT REVOKED") {
        showMessage("Сертификат отозван. Выберите в Synergy Agent действующий сертификат", "Ошибка");
      } else if (result === "CERT END"){
        showMessage("Сертификат просрочен. Выберите в Synergy Agent действующий сертификат", "Ошибка");
      } else if (result === "NO KEY") {
        showMessage("Проверьте, запущен ли SynergyAgent и выбран ли в нем ключ", "Ошибка");
      } else {
        showMessage("Произошла ошибка проверки ключа", "Ошибка");
      }
      return;
    }
    var args = result.split("::::");
    alg = args[1];
    certId = args[0];
    success(certId, cert, alg);
  }).fail(function(error){
    console.log(error);
    showMessage("Произошла ошибка проверки ключа. Проверьте, запущен ли SynergyAgent и выбран ли в нем ключ", "Ошибка");
  });
}

function signDocument(documentId, certId, cert, alg){
  let deferred = new $.Deferred();
  let rawdata = null;
  let signedData = null;
  $.when(AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/docflow/doc/document_info?documentID=${documentId}`))
  .then(documentInfo => {
    rawdata = documentInfo.rawdata;
    return $.post("https://local.arta.pro:8389/", {TYPE : 'SIGN', 'DATA' : rawdata, 'ALG' : alg});
  }).then(data => {
    signedData = data.signedData;
    let sendData = {
      documentID: documentId,
      rawdata : rawdata,
      signdata : signedData,
      certificate : cert,
      certID : certId
    };
    return AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/docflow/sign", null, "text", sendData);
  }).then(() => {
    deferred.resolve(true);
  }).fail(error => {
    deferred.reject();
  });
  return deferred;
}

function sign(){
  try {
    AS.FORMS.ApiUtils.getDocumentIdentifier(model.playerModel.asfDataId).then(docID => {
      loadUserSignKey(AS.OPTIONS.currentUser.userId || model.playerModel.currentUser.userid, function (certId, cert, alg) {
        let deferreds = [];
        let deferred1 = signDocument(docID, certId, cert, alg);
        deferreds.push(deferred1);
        jQuery.when.apply(jQuery, deferreds)
        .then(() => {
          showMessage('Документ подписан', 'Подпись');
          $('#nce-button-sign').hide();
        })
        .fail(err => console.log(err));
      });
    });
  } catch (e) {
    console.log(e);;
  }
}

function createSignButton(){
  if($('#nce-button-sign').length > 0) return;
  return jQuery('<div id="nce-button-sign">')
  .text("Подписать с ЭЦП")
  .click(() => sign());
}

function searchSign(signList){
  if(signList.length == 0) return false;
  for(let i = 0; i < signList.length; i++)
  if(signList[i].userID == AS.OPTIONS.currentUser.userId || model.playerModel.currentUser.userid) return true;
  return false;
}

function showMessage(msg, title) {
  let container = $('<div>', {class: 'nce-msg-container'});
  let titleBox = $('<div>', {class: 'nce-msg-title'}).text(title || 'Сообщение');
  let msgBox = $('<div>').html(msg);
  let close = $('<div>', {class: 'nce-msg-close'})
  .on('click', () => {
    container.fadeToggle('normal');
    setTimeout(() => container.remove(), 500);
  });
  container.append(titleBox).append(close).append(msgBox);
  $('body').append(container);
  container.fadeToggle('normal');
  setTimeout(() => {
    container.fadeToggle('normal');
    setTimeout(() => container.remove(), 500);
  }, 8000);
}

(function(){
	try {
    AS.FORMS.ApiUtils.getSigns(model.playerModel.asfDataId, -1)
    .then(res => {
      if(!searchSign(res)) {
        let bc = $('<div style="justify-content: center; display: flex; margin-top: 30px;">').append(createSignButton());
        view.container.append(bc);
      }
    });
  } catch (e) {
		console.error(e);
	}
})();
