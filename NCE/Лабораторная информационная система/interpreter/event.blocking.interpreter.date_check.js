var result = true;
var message = "ok";

try {
  let resultSearch = API.httpGetMethod('rest/api/registry/data_ext?registryCode=data_sdachi_otcheta&pageNumber=0&countInPart=1&fields=date_end');
  if(resultSearch.recordsCount == 0) throw new Error('Не найдена запись в реестре "Дата сдачи отчета"');

  let currentFormData = API.getFormData(dataUUID);
  let research_end_date = UTILS.getValue(currentFormData, "research_end_date");
  if(!research_end_date.hasOwnProperty('key')) throw new Error('Поле "Дата окончания" не заполнено');

  research_end_date = UTILS.parseDateTime(research_end_date.key);
  let date_end = UTILS.parseDateTime(resultSearch.result[0].fieldKey.date_end);

  if(research_end_date.getTime() < date_end.getTime()) {
    date_end = UTILS.formatDate(date_end, true);
    UTILS.setValue(currentFormData, "research_end_date", {key: date_end, value: date_end});
    API.saveFormData(currentFormData.form, currentFormData.uuid, currentFormData.data);
    message = '"Дата окончания": ' + date_end;
  } else {
    message = 'Поле "Дата окончания" не изменено';
  }

} catch (err) {
  log.error(err.message);
  message = err.message;
}
