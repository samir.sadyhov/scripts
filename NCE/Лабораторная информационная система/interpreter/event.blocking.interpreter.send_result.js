var result = true;
var message = "ok";

try {

  function fail(formData){
    UTILS.setValue(formData, "status", {key: "0", value: "Ошибка"});
    API.saveFormData(formData.form, formData.uuid, formData.data);
    throw new Error("Ссылка на Реестр сводных заявок пустая");
  }

  let currentFormData = API.getFormData(dataUUID);
  let consolidated_app = UTILS.getValue(currentFormData, "link_zay");

  if(!consolidated_app) fail(currentFormData);
  if(!consolidated_app.key) fail(currentFormData);

  let consolidatedAppData = API.getFormData(API.getAsfDataId(consolidated_app.key));
  let result_table = UTILS.getValue(consolidatedAppData, "result_table");

  if(!result_table) {
    result_table = UTILS.createField({
      id: "result_table",
      type: "appendable_table",
      key: "",
      data: []
    });
    consolidatedAppData.data.push(result_table);
  }

  result_table.data.push(UTILS.createField({
    id: "doc_id-b" + UTILS.getTableBlockIndex(result_table, "doc_id"),
    type: "textbox",
    value: documentID
  }));
  result_table.data.push(UTILS.createField({
    id: "doc_datauuid-b" + UTILS.getTableBlockIndex(result_table, "doc_datauuid"),
    type: "textbox",
    value: dataUUID
  }));

  let consolidatedAppDescription = API.getFormDescription(consolidatedAppData.form);

  let matching = [];
  matching.push({from: 'doc_num', to: 'doc_num'}); //Наименование объекта
  matching.push({from: 'doc_date', to: 'doc_date'}); //Дата выезда
  matching.push({from: 'doc_name', to: 'doc_name'}); //Адрес объекта
  matching.push({from: 'file_protokol', to: 'file_protokol'}); //Статус
  matching.push({from: 'status', to: 'status_result'}); //Статус

  matching.forEach(function(id){
    let fromData = UTILS.getValue(currentFormData, id.from);

    let field = UTILS.createField({
      id: id.to + "-b" + UTILS.getTableBlockIndex(result_table, id.to),
      type: UTILS.getCmpType(consolidatedAppDescription, id.to)
    });

    for(let key in fromData) {
      if(key === 'id' || key === 'type') continue;
      field[key] = fromData[key];
    }

    result_table.data.push(field);
  });

  API.saveFormData(consolidatedAppData.form, consolidatedAppData.uuid, consolidatedAppData.data);

} catch (err) {
  log.error(err.message);
  message = err.message;
}
