function searchTableBlockIndex(data, key){
  let res = null;
  data = data.data ? data.data : data;
  data.forEach(function(item) {
    if(item.key && item.key === key) res = item.id.substring(item.id.indexOf('-b'));
  });
  return res;
}

var result = true;
var message = "ok";

try {

  let currentFormData = API.getFormData(dataUUID);

  let consolidated_app = UTILS.getValue(currentFormData, "link_zay");

  if(!consolidated_app) throw new Error("Ссылка на Реестр сводных заявок пустая");
  if(!consolidated_app.key) throw new Error("Ссылка на Реестр сводных заявок пустая");

  let consolidatedAppData = API.getFormData(API.getAsfDataId(consolidated_app.key));

  let table_plan = UTILS.getValue(consolidatedAppData, "table_plan");
  let tableBlockIndex = searchTableBlockIndex(table_plan, documentID);

  if(!tableBlockIndex) throw new Error("В таблице График выездов ОПП не найдено записи с ссылкой на реестр Графика отбора проб");

  let matching = [];
  matching.push({from: 'name_object', to: 'name_object'}); //Наименование объекта
  matching.push({from: 'date', to: 'date'}); //Дата выезда
  matching.push({from: 'adress_object', to: 'adress_object'}); //Адрес объекта
  matching.push({from: 'status', to: 'status'}); //Статус

  matching.forEach(function(id){
    let fromData = UTILS.getValue(currentFormData, id.from);
    let cmpTo = id.to + tableBlockIndex;
    UTILS.setValue(table_plan, cmpTo, fromData);
  });

  API.saveFormData(consolidatedAppData.form, consolidatedAppData.uuid, consolidatedAppData.data);

} catch (err) {
  log.error(err.message);
  message = err.message;
}
