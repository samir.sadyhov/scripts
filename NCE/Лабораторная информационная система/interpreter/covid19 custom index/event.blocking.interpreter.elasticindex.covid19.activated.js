var result = true;
var message = "ok";

let ELASTIC_HOST = '192.168.1.182:9200'; //localhost

function checkElasticServer(){
  let client = new org.apache.commons.httpclient.HttpClient();
  let get = new org.apache.commons.httpclient.methods.GetMethod("http://" + ELASTIC_HOST);
  client.executeMethod(get);
  get.getResponseBodyAsString();
  get.releaseConnection();
}

function putInIndex(data, elasticKey) {
  let client = new org.apache.commons.httpclient.HttpClient();
  let put = new org.apache.commons.httpclient.methods.PutMethod("http://"+ELASTIC_HOST+"/custom_index_covid/covid19/" + encodeURIComponent(elasticKey));
  let body = new org.apache.commons.httpclient.methods.StringRequestEntity(JSON.stringify(data), "application/json", "UTF-8");
  put.setRequestEntity(body);
  client.executeMethod(put);
  let resp = put.getResponseBodyAsString();
  put.releaseConnection();
  return JSON.parse(resp);
}

function formatDate(date) {
  date = date.split(' ');
  return date[0] + 'T' + date[1] + '.000Z';
}

try {
  checkElasticServer(); // В случае недоступности сервера вернет ошибку:
                        // "В соединении отказано (Connection refused)"
                        // дальнейшее выполнение БП будет невозможно

  let currentFormData = API.getFormData(dataUUID);
  let covid_19 = UTILS.getValue(currentFormData, 'covid_19');
  let status = UTILS.getValue(currentFormData, 'status');

  if(!covid_19) throw new Error("Нет данных по типу заявки || Пропускаем заявку");
  if(covid_19.value != '1') throw new Error("Заявка на коронавирус COVID-19 - НЕТ || Пропускаем заявку");

  if(!status) throw new Error("Не определен статус заявки || Пропускаем заявку");
  if(status.key != '7') throw new Error("Статус заявки - " + status.value + ' || Пропускаем заявку');

  let indexData = {asfDataID: dataUUID};
  indexData.covid_19 = Number(covid_19.value);
  indexData.status = Number(status.key);
  indexData.deleted = 0;

  let fields = [];
  fields.push({id: 'contact_copy2'}); //ФИО пациента
  fields.push({id: 'person_id'}); //ИИН
  fields.push({id: 'sex', key: true}); //ПОЛ
  fields.push({id: 'birthdate', date: true}); //Дата рождения
  fields.push({id: 'work_place'}); //Место работы
  fields.push({id: 'country_of_arrival'}); //Страна прибытия
  fields.push({id: 'status_patient', key: true}); //Статус пациента
  fields.push({id: 'survey_status', key: true}); //Статус обследования
  fields.push({id: 'Full_Name_patient'}); //ФИО больного
  fields.push({id: 'Full_Name_Contact'}); //ФИО БК
  fields.push({id: 'Full_Name_potential_Contact'}); //ФИО ПК
  fields.push({id: 'selection_date_time', date: true}); //Дата и время отбора проб
  fields.push({id: 'selected_user'}); //Кем отобран материал (ФИО, орг.)
  fields.push({id: 'research_start_date', date: true}); //Дата начала исследований
  fields.push({id: 'research_end_date', date: true}); //Дата окончания исследования
  fields.push({id: 'diagnosis'}); //Диагноз
  fields.push({id: 'code_print'}); //Штрих код
  fields.push({id: 'clinic_code'}); //Номер пробы от поликлиники
  fields.push({id: 'specialist'}); //Кем проведено исследование
  fields.push({id: 'personal_card_name_company_ru1'}); //Филиал
  fields.push({id: 'personal_card_name_department_ru1'}); //Отделение

  fields.forEach(function(field) {
    let tmpData = UTILS.getValue(currentFormData, field.id);
    if(field.date) {
      if(tmpData.key) {
        indexData[field.id + '_date'] = formatDate(tmpData.key);
        indexData[field.id + '_key'] = tmpData.key;
        indexData[field.id + '_value'] = tmpData.value;
      }
    } else {
      if(tmpData) tmpData = field.key ? tmpData.key : tmpData.value;
      indexData[field.id] = tmpData || '';
    }
  });

  let addressField = ['Oblast', 'rayon', 'locality', 'address_copy1']; //Адрес проживания
  indexData.address = '';
  addressField.forEach(function(field) {
    let tmpData = UTILS.getValue(currentFormData, field);
    if(tmpData && tmpData.value) indexData.address += tmpData.value + " ";
  });

  let dynamic_table = UTILS.getValue(currentFormData, 'dynamic_table');
  fields = [];
  fields.push({id: 'tarificator_research_code'}); //Код исследования
  fields.push({id: 'services_amount'}); //Количество исследований
  fields.push({id: 'mismatch_services_amount'}); //Количество положительных результатов

  fields.forEach(function(field) {
    let tableBlockIndex = UTILS.getTableBlockIndex(dynamic_table, field.id) - 1;
    let tmpData = UTILS.getValue(dynamic_table, field.id + '-b' + tableBlockIndex);
    if(tmpData) tmpData = field.key ? tmpData.key : tmpData.value;
    if(field.id == 'tarificator_research_code') {
      indexData[field.id] = tmpData || '';
    } else {
      indexData[field.id] = Number(tmpData) || 0;
    }
  });

  // log.info(":::::: INDEX DATA ::::::", indexData);
  let resultAddIndex = putInIndex(indexData, "covid19-" + dataUUID);
  log.info(":::::: RESULT ADD INDEX ::::::", resultAddIndex);

} catch (err) {
  log.error('[ INDEX COVID19 ERROR ] ' + err.message);
  message = err.message;
}
