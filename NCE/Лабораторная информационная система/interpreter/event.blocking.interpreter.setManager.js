var result = true;
var message = "ok";
var userCardCode = 'nce_forma_personal_card';
var departmentCardCode = 'nce_forma_subdivision_card_copy2';

function getTableValues(tableData, cmpID) {
  tableData = tableData.data ? tableData.data : tableData;
  return tableData.map(function(item) {
    if(item.id.slice(0, item.id.indexOf('-b')) === cmpID) {
      if(item.hasOwnProperty('key')) return item.key;
      if(item.hasOwnProperty('value')) return item.value;
    }
  })
  .filter(function(value) {return value}).sort().uniq();
}

function compareArr(a1, a2) {
  if (!Array.isArray(a1) && !Array.isArray(a2)) return false;
  let count = 0;
  for (let i = 0; i < a1.length; i++)
  if (a2.indexOf(a1[i]) !== -1) count++;
  return count == a1.length;
}

function getWorkLoad(params){
  let client = API.getHttpClient();
  let post = new org.apache.commons.httpclient.methods.PostMethod("http://127.0.0.1:8080/Synergy/rest/person/workload/m");
  post.setRequestBody(JSON.stringify(params));
  post.setRequestHeader("Content-type", "application/json");
  client.executeMethod(post);
  let resp = post.getResponseBodyAsString();
  post.releaseConnection();
  return JSON.parse(resp);
}

function getUsers(departments, reglink_service_name, contract_individual_entity){
  let users = [];
  let filterUsers = [];
  let filterDepartment = [];

  departments.forEach(function(departmentID) {
    let deparmentCard = API.httpGetMethod('rest/api/departments/get_cards?departmentID=' + departmentID);
    deparmentCard = deparmentCard.filter(function(x){ return x.formCode == departmentCardCode})[0];
    let deparmentCardData = API.getFormData(deparmentCard['data-uuid']);
    let table_reglink_service_name = UTILS.getValue(deparmentCardData, "table_reglink_service_name");
    if(table_reglink_service_name && UTILS.getTableBlockIndex(table_reglink_service_name, 'reglink_service_name') != 1) {
      let department_service_name = getTableValues(table_reglink_service_name, 'reglink_service_name');
      // Сравнивает список услуг
      if(compareArr(reglink_service_name, department_service_name)) filterDepartment.push(departmentID);
    }
  });

  //Достаем всех юзеров по подходящим департаментам
  filterDepartment.forEach(function(departmentID) {
    let userDep = API.httpGetMethod('rest/api/userchooser/search_ext?recordsCount=5000&showAll=true&departmentID=' + departmentID);
    userDep.forEach(function(user) {users.push(user)});
  });


  //Фильтруем юзеров по данным в карточке
  users.forEach(function(item) {
    let userCard = API.httpGetMethod('rest/api/personalrecord/forms/' + item.personID);
    userCard = userCard.filter(function(x){ return x.formCode == userCardCode})[0];
    let userCardData = API.getFormData(userCard['data-uuid']);
    let table_user_service_name = UTILS.getValue(userCardData, "table_reglink_service_name");
    if(table_user_service_name && UTILS.getTableBlockIndex(table_user_service_name, 'reglink_service_name') != 1) {
      let user_service_name = getTableValues(table_user_service_name, 'reglink_service_name');
      let userCie = UTILS.getValue(userCardData, "contract_individual_entity");
      //2.1.2 Данные по виду клиента совпадают по полям contract_individual_entity сводной заявки;
      if(userCie && userCie.value == contract_individual_entity.value) {
        // 2.1.2 Список услуг в поле reglink_service_name таблицы table_reglink_service_name
        // совпадает с услугами из сводной заявки по полю reglink_service_name таблицы table;
        if(compareArr(reglink_service_name, user_service_name)) filterUsers.push({
          userID: item.personID,
          fullName: item.personName
        });
      }
    }
  });

  return filterUsers;
}

function getFullName(userID) {
  let person = API.httpGetMethod('rest/api/filecabinet/user/' + userID);
  return (
    person.firstname ? (
      person.lastname ? (
        person.patronymic ?
        person.lastname + ' ' + person.firstname.substr(0, 1).toUpperCase() + '. ' + person.patronymic.substr(0, 1).toUpperCase() + '.' :
        person.lastname + ' ' + person.firstname.substr(0, 1).toUpperCase() + '.'
      ) : (person.patronymic ? person.firstname + ' ' + person.patronymic.substr(0, 1).toUpperCase() + '.' : '')
    ) : 'Anonymous'
  );
}

try {
  let currentFormData = API.getFormData(dataUUID);

  let contract_individual_entity = UTILS.getValue(currentFormData, "contract_individual_entity");
  if(!contract_individual_entity.hasOwnProperty('value'))
  throw new Error('Не определен тип пользователя');

  let table_object_location = UTILS.getValue(currentFormData, "table_object_location");
  if(UTILS.getTableBlockIndex(table_object_location, 'oblast') == 1)
  throw new Error('Не заполнена таблица "Место оказания услуг" [cmp_id: table_object_location]');

  let table = UTILS.getValue(currentFormData, "table");
  if(UTILS.getTableBlockIndex(table, 'reglink_service_name') == 1)
  throw new Error('Не заполнена таблица "Вид услуги" [cmp_id: table]');

  let oblast = getTableValues(table_object_location, 'oblast');
  let rayon = getTableValues(table_object_location, 'rayon');
  let reglink_service_name = getTableValues(table, 'reglink_service_name');
  let filterUsers;

  //ID департамента ЦА
  let CADepartmentID = ['58edc25d-9f61-435a-9c6c-1efed9a40a4b', '3c3e863a-b774-4ff4-8990-c3d2fe68bbdb'];

  /*
    Условие 1: Если в сводной заявке в таблице table_object_location
    в поле oblast во всех значениях выбран один вид области.
  */
  if(oblast.length === 1) {
    /*
      1.Система находит карточку подразделения Форма - карточка подразделения nce_forma_subdivision_card_copy2
      у которого значение в поле oblast совпадает со значением oblast сводной заявки.
    */
    let departments = API.httpGetMethod('rest/api/departments/get_by_field_value?formCode=' + encodeURIComponent(departmentCardCode)
    + '&fieldName=oblast&value=' + encodeURIComponent(oblast[0]));

    if(departments.length > 0) {
      filterUsers = getUsers(departments, reglink_service_name, contract_individual_entity);
    } else { //если не найдено департаментов ищем по ЦА
      filterUsers = getUsers(CADepartmentID, reglink_service_name, contract_individual_entity);
    }
  } else { //если несколько областей или ваще не указано то ищем по ЦА
    filterUsers = getUsers(CADepartmentID, reglink_service_name, contract_individual_entity);
  }

  //Формируем данные для запроса нагруженности
  let query = filterUsers.map(function(user){
    return {
      userID: user.userID,
      startDate: UTILS.formatDate(new Date()),
      finishDate: UTILS.formatDate(new Date())
    }
  });
  //Возвращает список значений нагрузки для пользователя
  let workloadResult = getWorkLoad(query);

  let Users = [];
  for (key in workloadResult) {
    let user = {
      userID: key,
      load: 0,
      fullName: filterUsers.filter(function(x){ if(x.userID == key) return x; })[0].fullName
    };
    workloadResult[key].forEach(function(item) {
      user.load += parseFloat(item.value);
    });
    Users.push(user);
  }

  //сортируем по нагруженности
  Users.sort(function(a, b) {
    return a.load - b.load;
  });

  //ставим самого не нагруженного юзера в поле manager
  if(Users.length > 0) {
    UTILS.setValue(currentFormData, 'manager', {
      type: "entity",
      key: Users[0].userID,
      value: getFullName(Users[0].userID) //Users[0].fullName
    });
  } else {
    let managerDep = API.httpGetMethod('rest/api/departments/get?departmentID=' + CADepartmentID[0]).manager.managerID;
    UTILS.setValue(currentFormData, 'manager', {
      type: "entity",
      key: managerDep,
      value: getFullName(managerDep)
    });
  }  

  //сохраняем данные
  API.saveFormData(currentFormData.form, currentFormData.uuid, currentFormData.data);

} catch (err) {
  log.error(err.message);
  message = err.message;
}
