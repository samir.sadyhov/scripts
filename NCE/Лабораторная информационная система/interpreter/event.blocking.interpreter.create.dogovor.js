var result = true;
var message = "ok";

function createNewField(formData, formDescription, item) {
  let tmp = UTILS.getValue(formData, item.from);
  if(!tmp) return null;
  if(!UTILS.getCmpType(formDescription, item.to)) return null;
  let field = UTILS.createField({
    id: item.to,
    type: UTILS.getCmpType(formDescription, item.to)
  });
  for(let key in tmp) {
    if(key === 'id' || key === 'type') continue;
    field[key] = tmp[key];
  }
  return field;
}

function createDogovor(registryCode, data){
  let client = API.getHttpClient();
  let post = new org.apache.commons.httpclient.methods.PostMethod("http://127.0.0.1:8080/Synergy/rest/api/registry/create_doc_rcc");
  post.setRequestBody(JSON.stringify({"registryCode": registryCode, "data": data}));
  post.setRequestHeader("Content-type", "application/json; charset=utf-8");
  client.executeMethod(post);
  let resp = post.getResponseBodyAsString();
  post.releaseConnection();
  return JSON.parse(resp);
}

function failed() {
  let currentFormData = API.getFormData(dataUUID);
  UTILS.setValue(currentFormData, "status", {key: '0', value: 'Ошибка'});
  API.saveFormData(currentFormData.form, currentFormData.uuid, currentFormData.data);
}

try {
  let currentFormData = API.getFormData(dataUUID);
  let contract_individual_entity = UTILS.getValue(currentFormData, "contract_individual_entity");
  if(!contract_individual_entity.hasOwnProperty('value')) throw new Error('Не определен тип пользователя');

  // Если пользователь является ФЛ, то переходим к этапу 2.1.1.
  if(contract_individual_entity.value == '1') {
    let dogovorFormDescription = API.httpGetMethod('rest/api/asforms/form_ext?formCode=nce_form_contract_for_PE');
    let dogovorData = [];
    let matching = [];

    // 1. Поля из сводной заявки
    matching.push({from: 'account', to: 'org_name_zakazchk'});
    matching.push({from: 'contact', to: 'contact'});
    matching.push({from: 'manager', to: 'res_user'});
    matching.push({from: 'author', to: 'fio_zakazchik_1ru'});
    matching.push({from: 'transport_type_fl', to: 'list__departure_specialists_ru'});
    matching.push({from: 'date_contract_term', to: 'date_conditions_ru'});
    matching.push({from: 'table', to: 'table'});
    matching.push({from: 'table_object_location', to: 'table_objects'});
    matching.push({from: 'table_training', to: 'table_training'});
    matching.push({from: 'signing', to: 'signing_contract'});

    matching.forEach(function(item){
      let field = createNewField(currentFormData, dogovorFormDescription, item);
      if(field) dogovorData.push(field);
    });

    dogovorData.push(UTILS.createField({
      id: "link_zay",
      type: "reglink",
      key: documentID,
      valueID: documentID
    }));

    /*
      2. Из реестра Контакты по полю contact форма customers_form_contact
      реестр customers_registry_contacts для договора ФЛ подтягиваем следующие поля:
    */
    let contact = UTILS.getValue(currentFormData, "contact");
    if(contact && contact.hasOwnProperty('key')) {
      let contactFormData = API.getFormData(API.getAsfDataId(contact.key));

      matching = [];
      matching.push({from: 'crm_form_contact_main_fullName', to: 'fio_zakazchik_ru'});
      matching.push({from: 'crm_form_contact_main_iin', to: 'iin_zakazchik_ru'});
      matching.push({from: 'crm_form_contact_main_phone', to: 'tel_zakazchik_ru'});
      matching.push({from: 'crm_form_contact_main_fullName', to: 'fio_zakazchik_2ru'});
      matching.push({from: 'crm_form_contact_main_fullName', to: 'user_zakaz_ru'});
      matching.push({from: 'full_address', to: 'adress_zakazchik_ru'});

      matching.forEach(function(item){
        let field = createNewField(contactFormData, dogovorFormDescription, item);
        if(field) dogovorData.push(field);
      });
    }


    /*
      3. Из реестра Аккаунта по полю org_name_zakazchk форма customers_form_account
      реестр customers_registry_accounts для договора ФЛ подтягиваем следующие поля:
    */
    let org_name_zakazchk = UTILS.getValue(currentFormData, "account");
    if(org_name_zakazchk && org_name_zakazchk.hasOwnProperty('key')){
      let org_name_zakazchkFormData = API.getFormData(API.getAsfDataId(org_name_zakazchk.key));

      /*
        В постановке каша или че, указано брать с поля org_name_zakazchk,
        но такого поля в сводной заявке нет, оно есть в договоре, но мы то создаем договор,
        а в договор оно заполняется со сводной заявки значением поля account,
        так что хер с ним, попробуем подставить вместо org_name_zakazchk поле account,
        если что потом исправим за дополнительные часы, мне то че, мне же платите ))
        p.s. сколько можно говорить пишите внимательней постановки, чесное слово, кровь уже из глаз хлещет
      */
      matching = [];
      matching.push({from: 'nce_form_account_lastName', to: 'user_zakaz_ru'}); // это поле выше уже заполнялось, перезапишется
      matching.push({from: 'nce_form_account_lastName', to: 'fio_zakazchik_2ru'}); // это поле выше уже заполнялось, перезапишется
      matching.push({from: 'crm_form_account_main_bin', to: 'iin_zakazchik_ru'}); // это поле выше уже заполнялось, перезапишется
      matching.push({from: 'crm_form_account_bankName', to: 'bankName_zakazchik_ru'});
      matching.push({from: 'crm_form_account_bankBIK', to: 'bik_zakazchik_ru'});
      matching.push({from: 'crm_form_account_bankKBE', to: 'kbe_zakazchil_ru'});
      matching.push({from: 'crm_form_account_bankIIK', to: 'bankIIK_zakazchik_ru'});
      matching.push({from: 'crm_form_account_main_phone', to: 'tel_zakazchik_ru'}); // это поле выше уже заполнялось, перезапишется
      matching.push({from: 'full_address', to: 'adress_zakazchik_ru'}); // это поле выше уже заполнялось, перезапишется
      matching.push({from: 'full_name', to: 'fio_zakazchik_ru'}); // это поле выше уже заполнялось, перезапишется

      matching.forEach(function(item){
        let field = createNewField(org_name_zakazchkFormData, dogovorFormDescription, item);
        if(field) dogovorData.push(field);
      });
    }


    /*
      4. Из карточки подразделения данного пользователя res_user
      Форма - карточка подразделения пользователя nce_forma_subdivision_card_copy2
      подтягиваем следующие поля:
    ------------------------------------------------------------------------------------------------
      поля res_user нет на форме сводной заявки, данное поле есть в договоре которое заполняется
      полем manager сводной заявки, пункт 1 выше "matching.push({from: 'manager', to: 'res_user'});"
      так что пока подставлю сюда manager
    */
    let res_user = UTILS.getValue(currentFormData, "manager"); //res_user
    if(res_user && res_user.hasOwnProperty('key')) {
      let userInfo = API.httpGetMethod('rest/api/filecabinet/user/' + res_user.key);
      if(userInfo && userInfo.hasOwnProperty('positions') && userInfo.positions.length > 0) {
        let deparmentCard = API.httpGetMethod('rest/api/departments/get_cards?departmentID=' + userInfo.positions[0].departmentID);
        deparmentCard = deparmentCard.filter(function(x){ return x.formCode == 'nce_forma_subdivision_card_copy2'})[0];
        let deparmentCardData = API.getFormData(deparmentCard['data-uuid']);

        matching = [];
        matching.push({from: 'listbox_city', to: 'list_name_city_ru'});
        matching.push({from: 'personal_card_fullName', to: 'user_ispol_ru'});
        matching.push({from: 'personal_card_document', to: 'personal_card_document'});
        matching.push({from: 'personal_card_number', to: 'num_doverenosti_ru'});
        matching.push({from: 'personal_card_date', to: 'date_doverenosti_ru'});
        matching.push({from: 'personal_card_name_company_ru', to: 'filial_ru'});
        matching.push({from: 'personal_card_name_company_kz', to: 'filial_kz'});
        matching.push({from: 'personal_card_name_department_ru', to: 'district_office_ru'});
        matching.push({from: 'personal_card_name_department_kz', to: 'district_office_kz'});
        matching.push({from: 'personal_card_adress_ru', to: 'adress_ispolnitel_ru'});
        matching.push({from: 'personal_card_adress_kz', to: 'adress_ispolnitel_kz'});
        matching.push({from: 'personal_card_bin', to: 'bin_ispolnitel_ru'});
        matching.push({from: 'personal_card_iikkz', to: 'iik_ispolnitel_ru'});
        matching.push({from: 'bank_ispolnitel_ru', to: 'bank_ispolnitel_ru'});
        matching.push({from: 'bank_ispolnitel_kz', to: 'bank_ispolnitel_kz'});
        matching.push({from: 'personal_card_bik', to: 'bik_ispolnitel_ru'});
        matching.push({from: 'personal_card_kbe', to: 'kbe_ispolnitel_ru'});
        matching.push({from: 'personal_card_phone', to: 'tel_ispolnitel_ru'});

        matching.forEach(function(item){
          let field = createNewField(deparmentCardData, dogovorFormDescription, item);
          if(field) dogovorData.push(field);
        });
      }
    }

    let newDogovor = createDogovor("nce_registry_contracts_for_PE", dogovorData);
    if(newDogovor.errorCode != '0') {
      throw new Error('Произошла ошибка при создании договора\n' + 'errorMessage: ' + newDogovor.errorMessage);
    }

    // API.httpGetMethod('rest/api/registry/activate_doc?documentID=' + newDogovor.documentID);
    UTILS.setValue(currentFormData, 'contract_individual', {
      type: "reglink",
      key: newDogovor.documentID,
      valueID: newDogovor.documentID
    });
    API.saveFormData(currentFormData.form, currentFormData.uuid, currentFormData.data);
    message = 'Договор успешно создан\ndocumentID: ' + newDogovor.documentID + '\ndataUUID: ' + newDogovor.dataID;
    log.info('Договор успешно создан || documentID: ' + newDogovor.documentID + ' || dataUUID: ' + newDogovor.dataID);
  }


  // Если пользователь является ЮЛ, то переходим к этапу 2.2.1.
  if(contract_individual_entity.value == '2') {
    let dogovorFormDescription = API.httpGetMethod('rest/api/asforms/form_ext?formCode=nce_form_contract_for_PE_UL');
    let dogovorData = [];
    let matching = [];

    // 1. Поля из сводной заявки
    matching.push({from: 'account', to: 'org_name_zakazchk'});
    matching.push({from: 'manager', to: 'res_user'});
    matching.push({from: 'author', to: 'fio_zakazchik_1ru'});
    matching.push({from: 'object_type', to: 'object_type'});
    matching.push({from: 'date_contract_term', to: 'date_conditions_ru'});
    matching.push({from: 'table', to: 'table'});
    matching.push({from: 'table_object_location', to: 'table_objects'});
    matching.push({from: 'table_training', to: 'table_training'});
    matching.push({from: 'contract_individual_entity', to: 'contract_individual_entity'});
    matching.push({from: 'signing', to: 'signing_contract'});

    matching.forEach(function(item){
      let field = createNewField(currentFormData, dogovorFormDescription, item);
      if(field) dogovorData.push(field);
    });

    dogovorData.push(UTILS.createField({
      id: "link_zay",
      type: "reglink",
      key: documentID,
      valueID: documentID
    }));

    /*
      2. Из реестра Аккаунта по полю org_name_zakazchk
      форма customers_form_account реестр customers_registry_accounts
      для договора ЮЛ подтягиваем следующие поля:

      таже проблема что и в пункте 3 при создании договора ФЛ,
      вместо поля org_name_zakazchk подставим account
    */
    let accounts = UTILS.getValue(currentFormData, "account");
    if(accounts && accounts.hasOwnProperty('key')) {
      let accountsFormData = API.getFormData(API.getAsfDataId(accounts.key));

      matching = [];
      matching.push({from: 'crm_form_account_company_type', to: 'company_type_ru1'});
      matching.push({from: 'nce_form_account_number', to: 'nce_form_account_number_ru'});
      matching.push({from: 'nce_form_account_document', to: 'nce_form_account_document_ru'});
      matching.push({from: 'crm_form_account_bankName', to: 'bankName_zakazchik_ru'});
      matching.push({from: 'crm_form_account_bankBIK', to: 'bik_zakazchik_ru'});
      matching.push({from: 'crm_form_account_bankKBE', to: 'kbe_zakazchil_ru'});
      matching.push({from: 'crm_form_account_bankIIK', to: 'bankIIK_zakazchik_ru'});
      matching.push({from: 'full_address', to: 'adress_zakazchik_ru'});
      matching.push({from: 'crm_form_account_main_phone', to: 'tel_zakazchik_ru'});
      matching.push({from: 'crm_form_account_company_type', to: 'company_type_ru'});
      matching.push({from: 'crm_form_account_main_name', to: 'main_name_ru'});
      matching.push({from: 'crm_form_account_main_bin', to: 'bin_zakazchik_ru'});
      matching.push({from: 'crm_form_account_main_name', to: 'org_name_zakazchik_ru'});
      matching.push({from: 'nce_form_account_lastName', to: 'account_lastName_ru'});
      matching.push({from: 'nce_form_account_lastName', to: 'fio_zakazchik_2ru'});
      matching.push({from: 'crm_form_account_main_email', to: 'crm_form_account_main_email'});

      matching.forEach(function(item){
        let field = createNewField(accountsFormData, dogovorFormDescription, item);
        if(field) dogovorData.push(field);
      });
    }

    /*
      3. В договоре по полю res_user из личной карточки пользователя
      Форма - личная карточка nce_forma_personal_card подтягиваем следующие поля:
      ------------------------------------------------------------------------------------------------
        поля res_user нет на форме сводной заявки, данное поле есть в договоре которое заполняется
        полем manager сводной заявки, пункт 1 выше "matching.push({from: 'manager', to: 'res_user'});"
        так что пока подставлю сюда manager
    */
    let res_user = UTILS.getValue(currentFormData, "manager"); //res_user
    if(res_user && res_user.hasOwnProperty('key')) {
      let userCard = API.httpGetMethod('rest/api/personalrecord/forms/' + res_user.key);
      userCard = userCard.filter(function(x){ return x.formCode == 'nce_forma_personal_card'})[0];
      let userCardData = API.getFormData(userCard['data-uuid']);

      matching = [];
      matching.push({from: 'iin', to: 'iin'});
      matching.forEach(function(item){
        let field = createNewField(userCardData, dogovorFormDescription, item);
        if(field) dogovorData.push(field);
      });

      let userInfo = API.httpGetMethod('rest/api/filecabinet/user/' + res_user.key);
      if(userInfo && userInfo.hasOwnProperty('positions') && userInfo.positions.length > 0) {
        let deparmentCard = API.httpGetMethod('rest/api/departments/get_cards?departmentID=' + userInfo.positions[0].departmentID);
        deparmentCard = deparmentCard.filter(function(x){ return x.formCode == 'nce_forma_subdivision_card_copy2'})[0];
        let deparmentCardData = API.getFormData(deparmentCard['data-uuid']);

        matching = [];
        matching.push({from: 'listbox_city', to: 'list_name_city_ru'});
        matching.push({from: 'personal_card_fullName', to: 'user_ispol_ru'});
        matching.push({from: 'personal_card_document', to: 'personal_card_documen'});
        matching.push({from: 'personal_card_number', to: 'num_doverenosti_ru'});
        matching.push({from: 'personal_card_date', to: 'date_doverenosti_ru'});
        matching.push({from: 'personal_card_name_company_ru', to: 'filial_ru'});
        matching.push({from: 'personal_card_name_company_kz', to: 'filial_kz'});
        matching.push({from: 'personal_card_name_department_ru', to: 'district_office_ru'});
        matching.push({from: 'personal_card_name_department_kz', to: 'district_office_kz'});
        matching.push({from: 'personal_card_adress_ru', to: 'adress_ispolnitel_ru'});
        matching.push({from: 'personal_card_adress_kz', to: 'adress_ispolnitel_kz'});
        matching.push({from: 'personal_card_bin', to: 'bin_ispolnitel_ru'});
        matching.push({from: 'personal_card_iikkz', to: 'iik_ispolnitel_ru'});
        matching.push({from: 'bank_ispolnitel_ru', to: 'bank_ispolnitel_ru'});
        matching.push({from: 'bank_ispolnitel_kz', to: 'bank_ispolnitel_kz'});
        matching.push({from: 'personal_card_bik', to: 'bik_ispolnitel_ru'});
        matching.push({from: 'personal_card_kbe', to: 'kbe_ispolnitel_ru'});
        matching.push({from: 'personal_card_phone', to: 'tel_ispolnitel_ru'});

        matching.forEach(function(item){
          let field = createNewField(deparmentCardData, dogovorFormDescription, item);
          if(field) dogovorData.push(field);
        });
      }

    }


    let newDogovor = createDogovor("nce_registry_contracts_for_LE", dogovorData);
    if(newDogovor.errorCode != '0') {
      throw new Error('Произошла ошибка при создании договора\n' + 'errorMessage: ' + newDogovor.errorMessage);
    }

    // API.httpGetMethod('rest/api/registry/activate_doc?documentID=' + newDogovor.documentID); //активация документа
    UTILS.setValue(currentFormData, 'contract_entity', {
      type: "reglink",
      key: newDogovor.documentID,
      valueID: newDogovor.documentID
    });
    API.saveFormData(currentFormData.form, currentFormData.uuid, currentFormData.data);
    message = 'Договор успешно создан\ndocumentID: ' + newDogovor.documentID + '\ndataUUID: ' + newDogovor.dataID;
    log.info('Договор успешно создан || documentID: ' + newDogovor.documentID + ' || dataUUID: ' + newDogovor.dataID);

  }

} catch (err) {
  log.error(err.message);
  message = err.message;

  // 3. Обнаружение ошибок. || 3.2. Устанавливаем статус «Ошибка» в поле статус на сводной заявке
  failed();
}
