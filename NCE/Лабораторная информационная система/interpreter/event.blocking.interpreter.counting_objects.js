var result = true;
var message = "ok";

try {

  let currentFormData = API.getFormData(dataUUID);
  let documentInfo = API.httpGetMethod('rest/api/docflow/doc/document_info?documentID=' + documentID);
  let registryInfo = API.httpGetMethod('rest/api/registry/info?registryID=' + documentInfo.registryID);

  let oblastCmpID = 'oblast';
  let rayonCmpmID = 'rayon';
  let resultCmp = 'number_of_subject';

  if(registryInfo.code == 'reestr_obekta') {
    oblastCmpID = 'region';
    rayonCmpmID = 'district';
    resultCmp = 'number_of_objects';
  }

  let oblast = UTILS.getValue(currentFormData, oblastCmpID);
  let rayon = UTILS.getValue(currentFormData, rayonCmpmID);

  if(!oblast.hasOwnProperty('key')) throw new Error('Не выбрана область');
  if(!rayon.hasOwnProperty('key')) throw new Error('Не выбран район');

  //Поиск филиала в реестре "Реестр обхват рынка карточка"
  let urlSearch = 'rest/api/registry/data_ext?registryCode=reestr_obhvat_rynka_card&countInPart=1&loadData=false';
  urlSearch += '&field=oblast&condition=EQUALS&key=' + oblast.key + '&field=rayon&condition=EQUALS&key=' + rayon.key;
  let filialCard = API.httpGetMethod(urlSearch);

  if(filialCard.count == 0) throw new Error('Не найдена карточка филиала');

  //Поиск записей в текущем реестре
  urlSearch = 'rest/api/registry/data_ext?registryID=' + registryInfo.registryID + '&countInPart=1&loadData=false';
  urlSearch += '&field=' + oblastCmpID + '&condition=EQUALS&key=' + oblast.key;
  urlSearch += '&field=' + rayonCmpmID + '&condition=EQUALS&key=' + rayon.key;
  let countObjects = API.httpGetMethod(urlSearch).count;

  let filialFormData = API.getFormData(filialCard.data[0].dataUUID);
  UTILS.setValue(filialFormData, resultCmp, {key: String(countObjects), value: String(countObjects)});

  if(registryInfo.code == 'reestr_obekta') {
    let objectsType = [];
    let vidObektaDict = API.httpGetMethod('rest/api/dictionaries/spravochnik_vid_obekta_dlya_klienta');
    for (let key in vidObektaDict.items) {
      objectsType.push({
        name: vidObektaDict.items[key].vid_obekta_dlya_klienta_value.value,
        id: vidObektaDict.items[key].vid_obekta_dlya_klienta_key.value
      });
    }

    let table_object_type1 = UTILS.getValue(filialFormData, 'table-9z3lr6');

    if(!table_object_type1) {
      table_object_type1 = UTILS.createField({
        id: "table-9z3lr6",
        type: "appendable_table",
        data: []
      });
      filialFormData.data.push(table_object_type1);
    } else {
      table_object_type1.data = [];
    }

    objectsType.forEach(function(obj){
      urlSearch = 'rest/api/registry/data_ext?registryID=' + registryInfo.registryID + '&countInPart=1&loadData=false';
      urlSearch += '&field=' + oblastCmpID + '&condition=EQUALS&key=' + oblast.key;
      urlSearch += '&field=' + rayonCmpmID + '&condition=EQUALS&key=' + rayon.key;
      urlSearch += '&field=object_type1&condition=EQUALS&key=' + obj.id;
      let countObjectsType = API.httpGetMethod(urlSearch).count;

      let tableBlockIndex = UTILS.getTableBlockIndex(table_object_type1, 'object_type1');

      table_object_type1.data.push(
        UTILS.createField({
          id: "object_type1-b" + tableBlockIndex,
          type: "listbox",
          value: obj.name,
          key: obj.id
        })
      );

      table_object_type1.data.push(
        UTILS.createField({
          id: "number_of_objects1-b" + tableBlockIndex,
          type: "numericinput",
          value: String(countObjectsType),
          key: String(countObjectsType)
        })
      );
    });
  }

  API.saveFormData(filialFormData.form, filialFormData.uuid, filialFormData.data);

} catch (err) {
  log.error(err.message);
  message = err.message;
}
