function getValue(data, cmpID) {
  data = data.data ? data.data : data;
  for(let i = 0; i < data.length; i++)
  if (data[i].id === cmpID) return data[i];
  return null;
}

function blockTable(){
  if(!editable) return;

  let research_end_date = model.playerModel.getModelWithId('research_end_date').getAsfData();
  let status = model.playerModel.getModelWithId('status').getAsfData();
  if(!AS.FORMS.DateUtils.parseDate(research_end_date.key)) return;
  if(status.key != '7') return;

  let docID = '9f6d1f15-20d5-433b-9545-aed26a0609e1'; //документ в котором хранится дата для проверки

  AS.FORMS.ApiUtils.getAsfDataUUID(docID)
  .then(uuid => AS.FORMS.ApiUtils.loadAsfData(uuid))
  .then(asfData => {
    let date_end = AS.FORMS.DateUtils.parseDate(getValue(asfData, 'date_end').key);
    research_end_date = AS.FORMS.DateUtils.parseDate(research_end_date.key);

    if(research_end_date.getTime() < date_end.getTime()){
      view.setEnabled(false);
      view.viewBlocks.forEach(row => {
        row.views.forEach(item => item.setEnabled(false));
      });
    }

  });
}

blockTable();
