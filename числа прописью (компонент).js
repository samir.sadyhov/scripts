/*
model.settings = [];
model.settings.push({
  fieldNumber: 'number1',
  fieldResult: 'textbox1',
  locale: 'ru'
});

model.settings.push({
  fieldNumber: 'number2',
  fieldResult: 'textbox2',
  locale: 'kk'
});
*/

const translate = {
  kk: {
    "один": "бір",
    "два": "екі",
    "три": "үш",
    "четыре": "төрт",
    "пять": "бес",
    "шесть": "алты",
    "семь": "жеті",
    "восемь": "сегіз",
    "девять": "тоғыз",
    "десять": "он",
    "одиннадцать": "он бір",
    "двенадцать": "он екі",
    "тринадцать": "он үш",
    "четырнадцать": "он төрт",
    "пятнадцать": "он бес",
    "шестнадцать": "он алты",
    "семнадцать": "он жеті",
    "восемнадцать": "он сегіз",
    "девятнадцать": "он тоғыз",
    "двадцать": "жиырма",
    "тридцать": "отыз",
    "сорок": "қырық",
    "пятьдесят": "елу",
    "шестьдесят": "алпыс",
    "семьдесят": "жетпіс",
    "восемьдесят": "сексен",
    "девяносто": "тоқсан",
    "сто": "жүз",
    "двести": "екі жүз",
    "триста": "үш жүз",
    "четыреста": "төрт жүз",
    "пятьсот": "бес жүз",
    "шестьсот": "алты жүз",
    "семьсот": "жеті жүз",
    "восемьсот": "сегіз жүз",
    "девятьсот": "тоғыз жүз",
    "тысяча": "бір мың",
    "тысячи": "мыңдағандар",
    "тысяч": "мың",
    "миллион": "миллион",
    "миллиона": "миллион",
    "миллионов": "миллион",
    "миллиард": "миллиард",
    "миллиарда": "миллиард",
    "миллиардов": "миллиард",
    "триллион": "триллион",
    "триллиона": "триллион",
    "триллионов": "триллион",
    "квадриллион": "квадриллион",
    "квадриллиона": "квадриллион",
    "квадриллионов": "квадриллион",
    "квинтиллион": "квинтиллион",
    "квинтиллиона": "квинтиллион",
    "квинтиллионов": "квинтиллион",
    "одна": "біреуі",
    "две": "екі"
  },
  en: {
    "один": "one",
    "два": "two",
    "три": "three",
    "четыре": "four",
    "пять": "five",
    "шесть": "six",
    "семь": "seven",
    "восемь": "eight",
    "девять": "nine",
    "десять": "ten",
    "одиннадцать": "eleven",
    "двенадцать": "twelve",
    "тринадцать": "thirteen",
    "четырнадцать": "fourteen",
    "пятнадцать": "fifteen",
    "шестнадцать": "sixteen",
    "семнадцать": "seventeen",
    "восемнадцать": "eighteen",
    "девятнадцать": "nineteen",
    "двадцать": "twenty",
    "тридцать": "thirty",
    "сорок": "forty",
    "пятьдесят": "fifty",
    "шестьдесят": "sixty",
    "семьдесят": "seventy",
    "восемьдесят": "eighty",
    "девяносто": "ninety",
    "сто": "one hundred",
    "двести": "two hundred",
    "триста": "three hundred",
    "четыреста": "four hundred",
    "пятьсот": "five hundred",
    "шестьсот": "six hundred",
    "семьсот": "seven hundred",
    "восемьсот": "eight hundred",
    "девятьсот": "nine hundred",
    "тысяча": "one thousand",
    "тысячи": "thousands",
    "тысяч": "thousands",
    "миллион": "million",
    "миллиона": "million",
    "миллионов": "millions",
    "миллиард": "billion",
    "миллиарда": "billion",
    "миллиардов": "billions",
    "триллион": "trillion",
    "триллиона": "trillion",
    "триллионов": "trillions",
    "квадриллион": "quadrillion",
    "квадриллиона": "quadrillion",
    "квадриллионов": "quadrillion",
    "квинтиллион": "quintillion",
    "квинтиллиона": "quintillion",
    "квинтиллионов": "quintillion",
    "одна": "one",
    "две": "two"
  }
};

const trans = (value, locale) => {
  if (locale == 'ru') return value;
  return translate[locale][value];
}

const getDeclinationNumber = (n, arr) => {
  const d = arr[0], e = arr[1], f = arr[2];
  return n % 10 == 1 && n % 100 != 11 ? d : n % 10 >= 2 && n % 10 <= 4 && (n % 100 < 10 || n % 100 >= 20) ? e : f;
}

const numberToText = (c, locale) => {
  try {
    let result = "";

    for (let d = {
        0: {
          1: trans("один", locale),
          2: trans("два", locale),
          3: trans("три", locale),
          4: trans("четыре", locale),
          5: trans("пять", locale),
          6: trans("шесть", locale),
          7: trans("семь", locale),
          8: trans("восемь", locale),
          9: trans("девять", locale),
          10: trans("десять", locale),
          11: trans("одиннадцать", locale),
          12: trans("двенадцать", locale),
          13: trans("тринадцать", locale),
          14: trans("четырнадцать", locale),
          15: trans("пятнадцать", locale),
          16: trans("шестнадцать", locale),
          17: trans("семнадцать", locale),
          18: trans("восемнадцать", locale),
          19: trans("девятнадцать", locale),
          20: trans("двадцать", locale),
          30: trans("тридцать", locale),
          40: trans("сорок", locale),
          50: trans("пятьдесят", locale),
          60: trans("шестьдесят", locale),
          70: trans("семьдесят", locale),
          80: trans("восемьдесят", locale),
          90: trans("девяносто", locale),
          100: trans("сто", locale),
          200: trans("двести", locale),
          300: trans("триста", locale),
          400: trans("четыреста", locale),
          500: trans("пятьсот", locale),
          600: trans("шестьсот", locale),
          700: trans("семьсот", locale),
          800: trans("восемьсот", locale),
          900: trans("девятьсот", locale)
        },
        1: {
          1: trans("одна", locale),
          2: trans("две", locale)
        }
      }, i = {
        0: ["", "", ""],
        1: [trans("тысяча", locale), trans("тысячи", locale), trans("тысяч", locale)],
        2: [trans("миллион", locale), trans("миллиона", locale), trans("миллионов", locale)],
        3: [trans("миллиард", locale), trans("миллиарда", locale), trans("миллиардов", locale)],
        4: [trans("триллион", locale), trans("триллиона", locale), trans("триллионов", locale)],
        5: [trans("квадриллион", locale), trans("квадриллиона", locale), trans("квадриллионов", locale)],
        6: [trans("квинтиллион", locale), trans("квинтиллиона", locale), trans("квинтиллионов", locale)]
      }, j = (("" + c).match(/(\d{1,3})(?=((\d{3})*([^\d]|$)))/g) || []).reverse(), e = 0; e < j.length; e++) {

        let b = "";
        for (let f = d[e], c = j[e], g = 0; g < c.length; g++) {
          let a = c.substr(g);
          if (f && f[a] || d[0][a]) {
            b = b + " " + (f && f[a] || d[0][a]);
            break;
          } else {
            a = +c.substr(g, 1) * Math.pow(10, a.length - 1), +a in d[0] && (b = b + " " + d[0][a]);
          }
        }

        b && (b = b + " " + getDeclinationNumber(+c, i[e] || i[0]));
        result = b + result;
    }

    return result.trim() || "";

  } catch (err) {
    console.log('ERROR numberToText', err);
  }
}

if(editable) {
  const {settings = []} = model;
  settings.forEach(item => {
    const {fieldNumber, fieldResult, locale} = item;
    const modelFieldNumber = model.playerModel.getModelWithId(fieldNumber);
    const modelFieldResult = model.playerModel.getModelWithId(fieldResult);

    if(modelFieldNumber) {
      modelFieldNumber.on('valueChange', (_1, _2, value) => {
        if(modelFieldResult) {
          const textValue = numberToText(Number(value) || 0, locale);
          modelFieldResult.setValue(textValue);
        }
      });
    }
  });
}
