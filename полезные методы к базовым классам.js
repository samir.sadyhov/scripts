/**
 * Created by exile on 24.03.16.
 */

// здесь будут добавлены всякие полезные методы к базовым классом, ибо так удобнее
String.prototype.endsWith = function(pattern) {
    var d = this.length - pattern.length;
    return d >= 0 && this.lastIndexOf(pattern) === d;
};

String.prototype.isEmpty = function () {
    return !this.match(/\S/);
};

String.prototype.formatThousands = function (delimiter) {
    return this.toString().replace(/\B(?=(\d{3})+(?!\d))/g, delimiter);
};

String.prototype.getWidth = function(font) {
    // re-use canvas object for better performance
    var canvas = String.prototype.canvas || (String.prototype.canvas = document.createElement("canvas"));
    var context = canvas.getContext("2d");
    context.font = font;
    var metrics = context.measureText(this);
    return metrics.width;
};


Number.prototype.round = function(digits) {
    digits = Math.floor(digits);
    if (isNaN(digits) || digits === 0) {
        return Math.round(this);
    }
    if (digits < 0 || digits > 16) {
        throw 'RangeError: Number.round() digits argument must be between 0 and 16';
    }
    var multiplier = Math.pow(10, digits);
    return Math.round(this * multiplier) / multiplier;
};

Number.prototype.fixed = function(digits) {
    digits = Math.floor(digits);
    if (isNaN(digits) || digits === 0) {
        return Math.round(this).toString();
    }
    var parts = this.round(digits).toString().split('.');
    var fraction = parts.length === 1 ? '' : parts[1];
    if (digits > fraction.length) {
        fraction += new Array(digits - fraction.length + 1).join('0');
    }
    return parts[0] + '.' + fraction;
};

Number.prototype.fixedInt = function(digits) {
    var result = this.toString();
    while(result.length < digits) {
        result="0"+result;
    }
    result = result.substr(result.length-digits);
    return result;
};

Date.prototype.isValid = function () {
    // An invalid date object returns NaN for getTime() and NaN is the only
    // object not strictly equal to itself.
    return this.getTime() === this.getTime();
};


Array.prototype.isEmpty = function () {
    return this.length === 0;
};


Array.prototype.arrayToString = function (delimiter) {
    var v = "";
    var sign = "";
    this.forEach(function(item){
        v+=sign+item;
        sign = delimiter;
    });
    return v;
};


Array.prototype.equals = function (anotherArray) {
    if (this === anotherArray) return true;
    if (anotherArray == null) return false;
    if (this.length != anotherArray.length) return false;

    // If you don't care about the order of the elements inside
    // the array, you should sort both arrays here.

    for (var i = 0; i < this.length; ++i) {
        if (!_.isEqual(this[i], anotherArray[i])) {
            return false;
        }
    }
    return true;
};

/**
 * содержится ли элемент в массиве
 * @param element
 * @returns {boolean}
 */
Array.prototype.contains = function (element) {
    return this.indexOf(element) > -1;
};

/**
 * удаление элемента из массива
 * @param element
 */
Array.prototype.remove = function (element) {
    return this.splice(this.indexOf(element), 1);
};

/**
 * добавление элемента в массиве если такого там нет
 * @param element
 */
Array.prototype.pushUnique = function (element) {
    if(this.contains(element)) {
        return;
    }
    this.push(element);
};

/**
 * найти элемент с указанным ззначением свойства
 * @param propName
 * @param propValue
 */
Array.prototype.findElement = function (propName, propValue) {
    var result = null;
    this.some(function(el){
        if(el && el[propName] === propValue) {
            result = el;
            return true;
        }
    });
    return result;
};
