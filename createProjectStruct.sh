#!/bin/bash

PROJECT_PATH=$1$2
PROJECT_NAME=$2

echo "path: $PROJECT_PATH"
echo "name: $PROJECT_NAME"

mkdir $PROJECT_PATH
mkdir $PROJECT_PATH/constructor
mkdir $PROJECT_PATH/constructor/css
mkdir $PROJECT_PATH/constructor/js
mkdir $PROJECT_PATH/constructor/js/pages
mkdir $PROJECT_PATH/constructor/components
mkdir $PROJECT_PATH/configurator


touch $PROJECT_PATH/constructor/js/index.js
touch $PROJECT_PATH/constructor/js/pages/auth.js

touch $PROJECT_PATH/constructor/css/style.css

echo "AS.SERVICES.showWaitWindow = Cons.showLoader;" >> $PROJECT_PATH/constructor/js/index.js
echo "AS.SERVICES.hideWaitWindow = Cons.hideLoader;" >> $PROJECT_PATH/constructor/js/index.js
