let API = {
  getHttpClient: function(){
    let client = new org.apache.commons.httpclient.HttpClient();
    let creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(login, password);
    client.getParams().setAuthenticationPreemptive(true);
    client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
    return client;
  },
  httpGetMethod: function(methods, type) {
    let client = this.getHttpClient();
    let get = new org.apache.commons.httpclient.methods.GetMethod("http://127.0.0.1:8080/Synergy/" + methods);
    get.setRequestHeader("Content-type", "application/json");
    client.executeMethod(get);
    let resp = get.getResponseBodyAsString();
    get.releaseConnection();
    return type == 'text' ? resp : JSON.parse(resp);
  },
  httpPostMethod: function(methods, params, contentType) {
    let client = this.getHttpClient();
    let post = new org.apache.commons.httpclient.methods.PostMethod("http://127.0.0.1:8080/Synergy/" + methods);
    if(contentType) post.setRequestBody(JSON.stringify(params));
    else for(let key in params) post.addParameter(key, params[key]);
    post.setRequestHeader("Content-type", contentType || "application/x-www-form-urlencoded; charset=utf-8");
    let resp = client.executeMethod(post);
    post.releaseConnection();
    return resp;
  },
  getFormData: function(asfDataId) {
    return this.httpGetMethod("rest/api/asforms/data/" + asfDataId)
  },
  saveFormData: function(formID, asfDataId, asfData) {
    return this.httpPostMethod("rest/api/asforms/form/multipartdata", {
      form: formID,
      uuid: asfDataId,
      data: "\"data\":" + JSON.stringify(asfData)
    });
  },
  sendNotification: function(body) {
    return this.httpPostMethod("rest/api/notifications/send", body, "application/json; charset=utf-8");
  }
};

API.getAsfDataId = function(documentID) {
  return this.httpGetMethod("rest/api/formPlayer/getAsfDataUUID?documentID=" + documentID, 'text');
};

API.getDocumentID = function(asfDataId) {
  return this.httpGetMethod("rest/api/formPlayer/documentIdentifier?dataUUID=" + asfDataId, 'text');
};

API.getProcesses = function(documentID) {
  return this.httpGetMethod("rest/api/workflow/get_execution_process?documentID=" + documentID);
};

API.getWorkCompletionData = function(workID) {
  return this.httpGetMethod("rest/api/workflow/work/get_completion_data?workID=" + workID);
};

API.getFormDescription = function(formID) {
  return this.httpGetMethod("rest/api/asforms/form/" + formID + "?isMobile=false");
};

API.getSynergyCalendar = function(start, finish) {
  return this.httpGetMethod("rest/api/settings/calendar?date_start=" + start + "&date_finish=" + finish);
};
API.getFormForResult = function(formCode, workID) {
  return this.httpGetMethod("rest/api/workflow/work/get_form_for_result?formCode=" + formCode + "&workID=" + workID);
};
API.finishWork = function(actionID, file_identifier) {
  return this.httpPostMethod("rest/api/workflow/work/set_result", {
      workID: actionID,
      completionForm: "FORM",
      type: 'work',
      file_identifier: file_identifier
  });
};

let log = {
  parse: function(args) {
    let result = [];
    for (let x in args) result.push(JSON.stringify(args[x], null, 4));
    return documentID + '\n' + result.join('\n');
  },
  info: function() {
    console.info(this.parse(arguments));
  },
  error: function() {
    console.error(this.parse(arguments));
  }
}

let UTILS = {
  createField: function(fieldData) {
    let field = {};
    for (let key in fieldData) field[key] = fieldData[key];
    return field;
  },
  getValue: function(data, cmpID) {
    data = data.data ? data.data : data;
    for(let i = 0; i < data.length; i++)
    if (data[i].id === cmpID) return data[i];
  },
  setValue: function(asfData, cmpID, data) {
    let field = this.getValue(asfData, cmpID);
    for (let key in data) {
      if(key === 'id' || key === 'type') continue;
      field[key] = data[key];
    }
    return field;
  },
  getTableBlockIndex: function(data, cmp) {
    let res = 0;
    data = data.data ? data.data : data;
    data.forEach(function(item) {
      if (item.id.slice(0, item.id.indexOf('-b')) === cmp) res++;
    });
    return res === 0 ? 1 : ++res;
  }
};

UTILS.formatDate = function(datetime, time) {
  let result = datetime.getFullYear() + '-' +
    ('0' + (datetime.getMonth() + 1)).slice(-2) + '-' +
    ('0' + datetime.getDate()).slice(-2) + ' ' +
    ('0' + datetime.getHours()).slice(-2) + ':' +
    ('0' + datetime.getMinutes()).slice(-2) + ':' +
    ('0' + datetime.getSeconds()).slice(-2);
  return time ? result : result.substring(0, result.indexOf(' '));
};

UTILS.getCurrentDateParse = function() {
  return formatDate(new Date(), true); /*yyyy-mm-dd HH:MM:SS*/
};

UTILS.parseDateTime = function(datetime /*yyyy-mm-dd HH:MM:SS*/) {
  datetime = datetime.split(/\D/);
  return new Date(datetime[0], datetime[1] - 1, datetime[2], datetime[3] || 0, datetime[4] || 0, datetime[5] || 0);
};

UTILS.getPeriod = function() {
  let f = new Date();
  f.setMonth(f.getMonth() + 2);
  return {
    start: formatDate(new Date()),
    finish: formatDate(f)
  };
};

UTILS.getActionIds = function(processes, formCode) {
  let result = [];

  function search(p) {
    p.forEach(function(process) {
      if (process.completionFormCode == formCode && process.finished) {
        result.push(process);
      }
      if (process.subProcesses.length > 0) search(process.subProcesses);
    });
  }
  search(processes);

  return result.sort(function(a, b) {
    return UTILS.parseDateTime(b.finished) - UTILS.parseDateTime(a.finished);
  }).map(function(item) {
    return {actionID: item.actionID, finished: item.finished};
  });
};

UTILS.getCmpType = function(formDescription, cmpID) {
  let result = null;
  for (let i = 0; i < formDescription.properties.length; i++) {
    if (formDescription.properties[i].id === cmpID) {
      result = formDescription.properties[i].type;
      break;
    }
    if (formDescription.properties[i].type === "table") {
      result = this.getCmpType(formDescription.properties[i], cmpID) || result;
    }
  }
  return result;
};

UTILS.getParams = function(str){
  return str.split('$').filter(function(item) {
    if (item.indexOf('{') !== -1) return item;
  }).map(function(item) {
    return item.substring(item.indexOf('{') + 1, item.indexOf('}'));
  });
};

//выпиливыние из массива повторяющихся елементов
Array.prototype.uniq = function() {
  return this.filter(function(v, i, a){ return i == a.indexOf(v) });
}

let regExp = /[^a-zA-ZА-Яа-яЁё]/gi; //выпиливаем лишнии символы и цифры str.replace(regExp, '')

let result = true;
let message = "ok";

try {

  log.info('START', {documentID: documentID, dataUUID: dataUUID});
  throw new Error('Не найдена форма завершения');

} catch (err) {
  log.error(err.message);
  message = err.message;
}
