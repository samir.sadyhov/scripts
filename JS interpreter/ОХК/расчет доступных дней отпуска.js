var form = platform.getFormsManager().getFormData(dataUUID);
form.load();
// Карточка “ Отпуска сотрудника ”
var card = platform.getCardsManager().getUserCard('6516b0cf-5e6f-4e21-8eda-2aea6423c6ee', form.getValue("user"));
card.load();

// функция вывода даты в формате похожий на 'Wed Jan 11 2015 10:40:50 GMT+0600'
// на входе принимает дату в формате 'YYYY-MM-DD HH:mm:ss'
function getDateUTC(date) {
  var result = new Date(
    parseInt(date.substring(0, 4), 10),
    parseInt(date.substring(5, 7), 10) - 1,
    parseInt(date.substring(8, 10), 10),
    parseInt(date.substring(11, 13), 10),
    parseInt(date.substring(14, 16), 10),
    parseInt(date.substring(17), 10));
  return result;
}

// сумма значений строк, столбца таблицы
function getSumRows(table, cmp) {
  var result = 0, n = 0;
  for (var i = 0; i < card.getRowsCount(table); i++) {
    n = Number(card.getNumericValue(table, cmp, i));
    n = isNaN(n) ? 0 : n;
    result = result + n;
  } return result;
}

// общий стаж дней на выбранную дату начала отпуска
var FullExperience = Math.floor((getDateUTC(form.getValue("date_start")).getTime() - getDateUTC(card.getValue("day_start")).getTime()) / 86400000);

// дни без учета стажа работы
var WithoutExperience = getSumRows("cmp-8bctv8", "cmp-7mfg88");

// дни отпуска за весь период работы
var VacationDays = getSumRows("table", "days");

// дни отзыва за весь период работы
var RestVacationDays = getSumRows("table", "rest_for_the_period");

// доступные дни отпуска
var AvailableDays = Math.floor((FullExperience - WithoutExperience) / 30 * 2.5 - VacationDays + RestVacationDays);

// Записываем остаток за период на форму
form.setValue("days_rest_from_last_vacation", AvailableDays - form.getValue("days"));
form.setValue("result", AvailableDays);

//Записываем сумму дней за все отпуски на карточку
//card.setValue("days_pay", VacationDays);

// Проверяем не превышает ли
if (+form.getValue("days") <= AvailableDays) {
  form.setValue("is_ok", "1");
} else {
  form.setValue("is_ok", "0");
}

form.save();
card.save();
var result = true;
var message = "OK";
