var form = platform.getFormsManager().getFormData(dataUUID);
form.load();

var synergyUser = "1";
var synergyPass = "1";
var synergyURL = "http://192.168.4.6:8080/Synergy";

// id папки в хранилище, узнать через aisuite
var folder = "496506d8-bc53-4773-912d-76ba60fdff5c";

// получение текущей даты в формате "деньМесяцГод_часыМинутыСекунды" для наименования файла отчета
var date = new Date();
var values = [date.getDate(), date.getMonth() + 1, date.getMinutes(), date.getSeconds()];
for (var id in values) {
  values[id] = values[id].toString().replace(/^([0-9])$/, '0$1');
}
var newDate = (values[0] + values[1] + date.getFullYear() + '_' + date.getHours() + values[2] + values[3]);

// параметры для отчета
var reportID = "c3c3f178-6879-45a6-9fdf-38e12d0acfd3";
var paramReport = form.getValue("local");

// формирования отчета в папку хранилища
var post = new org.apache.commons.httpclient.methods.PostMethod(synergyURL + "/rest/api/report/do?fileName=" + newDate + ".pdf&reportID=" + reportID + "&parentIdentifier=" + folder + "&inline=false&locale=ru&local_dictionary=" + paramReport);
var client = new org.apache.commons.httpclient.HttpClient();
var creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(synergyUser, synergyPass);
client.getParams().setAuthenticationPreemptive(true);
client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
post.setRequestHeader("Content-type", "application/json");
var status = client.executeMethod(post);
var fileReportID = eval("(" + post.getResponseBodyAsString() + ")").identifier;
post.releaseConnection();

form.setValue('fileReport', fileReportID);
form.setValue('docID', documentID);

// добавление к работе файла отчета с хранилища
post = new org.apache.commons.httpclient.methods.PostMethod(synergyURL + "/rest/api/storage/copy");
post.addParameter("fileID", fileReportID);
post.addParameter("documentID", documentID);
client = new org.apache.commons.httpclient.HttpClient();
creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(synergyUser, synergyPass);
client.getParams().setAuthenticationPreemptive(true);
client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
post.setRequestHeader("Content-type", "application/x-www-form-urlencoded");
status = client.executeMethod(post);
var newFileID = eval("(" + post.getResponseBodyAsString() + ")").fileID;
post.releaseConnection();

form.save();
var result = true;
var message = "ID файла в приложении: " + newFileID;
