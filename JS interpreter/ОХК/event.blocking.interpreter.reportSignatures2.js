var form = platform.getFormsManager().getFormData(dataUUID);
form.load();

var synergyUser = "Administrator";
var synergyPass = "qazwsx123";
var synergyURL = "http://localhost:8080/Synergy";

// id папки в хранилище, узнать через aisuite
var folder = "1a6abaca-23a0-4cba-97b7-0fdb97c6f650";

// reportID можно узнать через API /rest/api/report/list
var reportID = "027f4f81-16e6-4f70-b33c-cf6a3b3f7ed3";

// параметры для отчета
var ReportDocID = documentID;
var ReportMonth = form.getValue("month");
var ReportYear = form.getValue("year");

// получение текущей даты в формате "деньМесяцГод_часыМинутыСекунды" для наименования файла отчета
var date = new Date();
var values = [date.getDate(), date.getMonth() + 1, date.getMinutes(), date.getSeconds()];
for (var id in values) {
  values[id] = values[id].toString().replace(/^([0-9])$/, '0$1');
}
var newDate = (values[0] + values[1] + date.getFullYear() + '_' + date.getHours() + values[2] + values[3]);

// формирования отчета в папку хранилища
var post = new org.apache.commons.httpclient.methods.PostMethod(synergyURL + "/rest/api/report/do?fileName=" + newDate + ".xls&reportID=" + reportID + "&parentIdentifier=" + folder + "&inline=false&locale=ru&month=" + ReportMonth + "&year=" + ReportYear + "&docid=" + ReportDocID);
var client = new org.apache.commons.httpclient.HttpClient();
var creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(synergyUser, synergyPass);
client.getParams().setAuthenticationPreemptive(true);
client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
post.setRequestHeader("Content-type", "application/json; charset=utf-8");
var status = client.executeMethod(post);
var fileReportID = eval("(" + post.getResponseBodyAsString() + ")").identifier;
post.releaseConnection();

// добавление к работе файла отчета с хранилища
post = new org.apache.commons.httpclient.methods.PostMethod(synergyURL + "/rest/api/storage/copy");
post.addParameter("fileID", fileReportID);
post.addParameter("documentID", documentID);
client = new org.apache.commons.httpclient.HttpClient();
creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(synergyUser, synergyPass);
client.getParams().setAuthenticationPreemptive(true);
client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
post.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=utf-8");
status = client.executeMethod(post);
var newFileID = eval("(" + post.getResponseBodyAsString() + ")").fileID;
post.releaseConnection();

form.save();
var result = true;
var message = "ID файла в приложении: " + newFileID;
