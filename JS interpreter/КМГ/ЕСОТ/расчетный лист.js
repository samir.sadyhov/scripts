var form = platform.getFormsManager().getFormData (dataUUID);
form.load();

var synergyUser = "Administrator";
var synergyPass = "$ystemAdm1n";
var synergyURL = "http://127.0.0.1:8080/Synergy/";

// функция для исполнения api с типом get
function getAPIresult(api, param) {
  var APIString = '';
  param == null ? APIString = synergyURL + api : APIString = synergyURL + api + param;
  var get = new org.apache.commons.httpclient.methods.GetMethod(APIString);
  var client = new org.apache.commons.httpclient.HttpClient();
  var creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(synergyUser, synergyPass);
  client.getParams().setAuthenticationPreemptive(true);
  client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
  get.setRequestHeader("Content-type", "application/json");
  var status = client.executeMethod(get);
  var result = get.getResponseBodyAsString();
  return result;
  get.releaseConnection();
}

// object to string
function obj2Str(obj) {
  var val, output = "";
  if (obj) {
    output += '{"';
    for (var i in obj) {
      val = obj[i];
      output += i + '":"' + val + '","';
    }
    output = output.substring(0, output.length - 2) + "}";
  }
  return output;
}

// реестр удержаний
var uderj = {
  registryID: 'e7427549-fc6d-4fca-9f99-bf66e05f7f82',
  formID: '0e32a9bf-ff89-42c0-b67e-eac9e5efc310',
  cmpUser: 'user',
  cmpDateS: 'date_start',
  cmpDataF: 'date_finish',
  cmpType: 'type',
  cmpSum: 'summa',
  cmpDoc: 'doc'
};

// json текущей формы
var rsFormData = getAPIresult("rest/api/asforms/data/", dataUUID);
rsFormData = eval("(" + rsFormData + ")").data;

// получаем фио пользователя с текущей формы
var rsUserFIO = null;
for (var i = 0; i < rsFormData.length; i++) {
  if (rsFormData[i].id == 'user') {
    rsUserFIO = rsFormData[i].value;
    break;
  }
}

// получаем год месяц (ггггмм)
var currentDate = 0;
var rsMonth = null;
for (var i = 0; i < rsFormData.length; i++) {
  if (rsFormData[i].id == 'year') {
    currentDate = rsFormData[i].value;
    break;
  }
}
for (var i = 0; i < rsFormData.length; i++) {
  if (rsFormData[i].id == 'month') {
    var tM=rsFormData[i].key;
    if (tM.length<10) tM='0'+tM;
    currentDate+=tM;
    rsMonth=rsFormData[i].key;
    break;
  }
}

// Формирование поисковой строки
var paramSearch = '{"query": "where uuid=\''+uderj.formID+'\' and `'+uderj.cmpUser+'.TEXT` = \''+rsUserFIO+'\' and \''+currentDate+'\' BETWEEN DATE_FORMAT(`'+uderj.cmpDateS+'.DATE`, \'%Y%m\')  and DATE_FORMAT(`'+uderj.cmpDataF+'.DATE`, \'%Y%m\') order by `date_start.DATE`", "parameters": ["'+uderj.cmpUser+'.TEXT", "'+uderj.cmpDateS+'.DATE", "'+uderj.cmpDataF+'.DATE"], "showDeleted": "false", "searchInRegistry": "true"}';

// Поиск записей в реестре удержаний
var APIString = synergyURL + "rest/api/asforms/search/advanced";
var post = new org.apache.commons.httpclient.methods.PostMethod(APIString);
var client = new org.apache.commons.httpclient.HttpClient();
var creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(synergyUser, synergyPass);
client.getParams().setAuthenticationPreemptive(true);
client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
post.setRequestHeader("Content-type", "application/json; charset=utf-8");
post.setRequestBody(paramSearch);
var status = client.executeMethod(post);
var deductionRecord = post.getResponseBodyAsString();
deductionRecord = eval("(" + deductionRecord + ")");

var tmpUd=false;
if (deductionRecord.length>0) tmpUd=true;

var MRP=2121;
var MZP=22859;
var last_zp=259468.00;
last_zp= last_zp.toFixed(2);

var card=platform.getCardsManager().getUserCard('6e7ee767-7f9b-4f53-b7d9-04a6091d066f', form.getValue("user"));  //Карточка “Личные данные” -берем МТС,ЧТС и табельный номер
card.load();
var salary= Number(card.getValue('MTS'));
if(isNaN(salary)) {salary=0;}
var CHTS=card.getValue('CHTS');
var Save_MTS=Number(card.getValue('constSalary'));
if(isNaN(Save_MTS)) {Save_MTS=0;}
var Save_CHTS=card.getValue('save_CHTS');
var t_number=card.getValue('t_number');

var dataU='973233fa-d1b1-4069-bd1a-d860deee44dc';                                             //Табель за декабрь
var form1 = platform.getFormsManager().getFormData (dataU);
form1.load();
var days= String(form1.getNumericValue("table","work_days",0));          //кол-во дней для 1 строки таблицы
var hours=String(form1.getNumericValue("table","all_hours",0));                        //кол-во часов для 1 строки таблицы
var overtime_hours=form1.getNumericValue("table","overtime",0);            //кол-во сверх часов для 1 строки таблицы
var night_hours=form1.getNumericValue("table","night_hours",0);               //кол-во ночных часов для 1 строки таблицы
var festive_hours=form1.getNumericValue("table","festive_hours",0);               //кол-во праздничных часов для 1 строки таблицы
var output_hours=form1.getNumericValue("table","output_hours",0);               //кол-во выходных часов для 1 строки таблицы

//var all_sum_ud=parseFloat(OPV)+parseFloat(IPN);
//var sum_finish=parseFloat(all_sum)-parseFloat(all_sum_ud);

var Desition1 = getAPIresult("rest/api/asforms/data/", dataUUID);
Desition1=String(Desition1);
var ss=Desition1.indexOf("table");
var st=Desition1.indexOf("[",ss);
var fin=Desition1.indexOf("]",ss);
var p1=Desition1.substring(0, st+1);
var p2=Desition1.substring(st+1, fin);
var p3=Desition1.substring(fin, Desition1.length);

Desition1 = eval("(" + Desition1 + ")");
for (var i = 0; i < Desition1.data.length; i++) {
  if (Desition1.data[i].id == 'table') {
    Desition1 = Desition1.data[i];
    break;
  }
}

var ii=1;       //параметр для номера строки в таблице
Desition1.data.push({id:"name-b"+ii,type:"textbox",value:"1. НАЧИСЛЕНИЯ"},{id:"day-b"+ii,type:"textbox",value:""},{id:"hour-b"+ii,type:"textbox",value:""},{id:"value-b"+ii,type:"textbox",value:""});                                                                     //НАЧИСЛЕНО
ii++;
Desition1.data.push({id:"name-b"+ii,type:"textbox",value:"Количество отработанных дней"},{id:"day-b"+ii,type:"textbox",value:days},{id:"hour-b"+ii,type:"textbox",value:""},{id:"value-b"+ii,type:"textbox",value:""});                                       //кол-во отработанных дней
ii++;
Desition1.data.push({id:"name-b"+ii,type:"textbox",value:"Количество отработанных часов"},{id:"day-b"+ii,type:"textbox",value:""},{id:"hour-b"+ii,type:"textbox",value:hours},{id:"value-b"+ii,type:"textbox",value:""});                          //кол-во отработанных часов
ii++;
var sum=parseFloat(salary);
sum=sum.toFixed(2);
Desition1.data.push({id:"name-b"+ii,type:"textbox",value:"Оплата по тарифу"},{id:"day-b"+ii,type:"textbox",value:""},{id:"hour-b"+ii,type:"textbox",value:""},{id:"value-b"+ii,type:"textbox",value:sum});                                                                                                      //МТС
ii++;
var Save_MTS2=Save_MTS.toFixed(2);
Desition1.data.push({id:"name-b"+ii,type:"textbox",value:"Сохраняемая часть к тарифной ставке"},{id:"day-b"+ii,type:"textbox",value:""},{id:"hour-b"+ii,type:"textbox",value:""},{id:"value-b"+ii,type:"textbox",value:Save_MTS2});                                                                                                      //МТС
ii++;
var sum=parseFloat(salary)+parseFloat(Save_MTS);               //sum-для всего начислено
var sum2 = sum.toFixed(2);

////////////////////////
///// ВЫПЛАТЫ    START//
////////////////////////

var userCards = getAPIresult("rest/api/personalrecord/forms/", form.getValue("user"));
userCards = String(userCards).replace(/form-uuid/g, "formUuid");
userCards = userCards.replace(/data-uuid/g, "dataUuid");
userCards = eval("(" + userCards + ")");

// Ищем UUID карточки пользователя (выплаты)
var cardViplatyID = null;
for (var ic = 0; ic < userCards.length; ic++) {
  if (userCards[ic].formUuid == '085bfdcb-3663-43ca-99d2-96e25a2fc813') {
    cardViplatyID = userCards[ic].dataUuid;
    break;
  }
}

if (cardViplatyID) { // если нашлась карточка достаем данные
  var card2=getAPIresult("rest/api/asforms/data/", cardViplatyID);
  card2 = eval("(" + card2 + ")").data[1].data;

  if (card2.length > 2) { // если таблица не пустая
    var des = null;
    var koefDop=0;
    for (var jc = 0; jc < card2.length; jc++) {
      if (card2[jc].type == 'reglink') {
        des = card2[jc].key;
        koefDop = card2[jc+1].key;

        var asfDataID = eval("(" + getAPIresult("rest/api/docflow/doc/document_info?documentID=", des) + ")");
        var asfDataID = asfDataID.asfDataID; // id записи в реестре Выплат
        var Desition = getAPIresult("rest/api/asforms/data/get?dataUUID=", asfDataID); // данные из реестра Выплат (json строка)

        var desJson = String(Desition);
        desJson=desJson.replace(/\n/g, '');
        desJson = eval("(" + desJson + ")")[0].data; // данные из реестра Выплат (json объект)
        var typePeriod = null; // Периодичность выплаты:
        var vipMonth = null; // Если выплата ежегодная, укажите месяц:
        for (var jj = 0; jj < desJson.length; jj++) {
          if (desJson[jj].id == 'type') {
            typePeriod = desJson[jj].value;
            break;
          }
        }
        if (typePeriod == '2') {
          for (var jj = 0; jj < desJson.length; jj++) {
            if (desJson[jj].id == 'month') {
              vipMonth = desJson[jj].key;
              break;
            }
          }
        }

        st = Desition.indexOf('"data"', 0);
        var kk = Desition.indexOf("premium_name");
        kk = Desition.indexOf("value", kk);
        fin = Desition.indexOf("}", kk);
        var str = Desition.substring(kk + 8, fin - 1);
        str = String(str);
        var time = 0;
        var dopl = false;
        if (str == "Доплаты за сверхурочную работу") {
          time = overtime_hours;
          dopl = true;
        }
        if (str == "Доплаты за работу в праздничные и выходные дни") {
          time = festive_hours + output_hours;
          dopl = true;
        }
        if (str == "Доплаты за работу в ночное время") {
          time = night_hours;
          dopl = true;
        }


        if (typePeriod == '1') { //если выплата ежемесячная
          if (dopl) {
            st = Desition.indexOf('"data"', 0);
            kk = Desition.indexOf("premium_type");
            kk = Desition.indexOf("key", kk);
            fin = Desition.indexOf("}", kk);
            var str2 = Desition.substring(kk + 6, fin - 1); //нашли номер формулы
            str2 = parseInt(str2);
            if (str2 == 1) str2 = time * CHTS * koefDop; //Если формула с кодом 1 - t * K * чТС
            if (str2 == 2) str2 = salary * koefDop; //Если формула с кодом 2 -K * мТС(мДО)
            if (str2 == 3) str2 = MRP * koefDop; //Если формула с кодом 3 - К * МРП
            if (str2 == 4) str2 = salary * koefDop; //Если формула с кодом 4 - K * мТС(мДО)
            sum = sum + parseFloat(str2); //sum-для всего начислено
            str2 = str2.toFixed(2);
            sum2 = sum.toFixed(2);

            time = parseFloat(time);
            Desition1.data.push({id: "name-b" + ii, type: "textbox", value: str}, {id: "day-b" + ii, type: "textbox", value: ""}, {id: "hour-b" + ii, type: "textbox",value: time}, {id: "value-b" + ii, type: "textbox", value: str2});
            ii++;
          } else {
            st = Desition.indexOf('"data"', 0);
            kk = Desition.indexOf("premium_type");
            kk = Desition.indexOf("key", kk);
            fin = Desition.indexOf("}", kk);
            var str2 = Desition.substring(kk + 6, fin - 1); //нашли номер формулы
            str2 = parseInt(str2);
            if (str2 == 1) str2 = time * CHTS * koefDop; //Если формула с кодом 1 - t * K * чТС
            if (str2 == 2) str2 = salary * koefDop; //Если формула с кодом 2 -K * мТС(мДО)
            if (str2 == 3) str2 = MRP * koefDop; //Если формула с кодом 3 - К * МРП
            if (str2 == 4) str2 = salary * koefDop; //Если формула с кодом 4 - K * мТС(мДО)
            sum = sum + parseFloat(str2); //sum-для всего начислено
            str2 = str2.toFixed(2);
            sum2 = sum.toFixed(2);

            Desition1.data.push({id: "name-b" + ii, type: "textbox", value: str}, {id: "day-b" + ii, type: "textbox", value: ""}, {id: "hour-b" + ii, type: "textbox", value: ""}, {id: "value-b" + ii, type: "textbox", value: str2});
            ii++;
          }
        }
        if (typePeriod == '2' && vipMonth == rsMonth) { // если выплата ежегодная сравниваем месяц из расчетного
          if (dopl) {
            st = Desition.indexOf('"data"', 0);
            kk = Desition.indexOf("premium_type");
            kk = Desition.indexOf("key", kk);
            fin = Desition.indexOf("}", kk);
            var str2 = Desition.substring(kk + 6, fin - 1); //нашли номер формулы
            str2 = parseInt(str2);
            if (str2 == 1) str2 = time * CHTS * koefDop; //Если формула с кодом 1 - t * K * чТС
            if (str2 == 2) str2 = salary * koefDop; //Если формула с кодом 2 -K * мТС(мДО)
            if (str2 == 3) str2 = MRP * koefDop; //Если формула с кодом 3 - К * МРП
            if (str2 == 4) str2 = salary * koefDop; //Если формула с кодом 4 - K * мТС(мДО)
            sum = sum + parseFloat(str2); //sum-для всего начислено
            str2 = str2.toFixed(2);
            sum2 = sum.toFixed(2);

            time = parseFloat(time);
            Desition1.data.push({id: "name-b" + ii, type: "textbox", value: str}, {id: "day-b" + ii, type: "textbox", value: ""}, {id: "hour-b" + ii, type: "textbox",value: time}, {id: "value-b" + ii, type: "textbox", value: str2});
            ii++;
          } else {
            st = Desition.indexOf('"data"', 0);
            kk = Desition.indexOf("premium_type");
            kk = Desition.indexOf("key", kk);
            fin = Desition.indexOf("}", kk);
            var str2 = Desition.substring(kk + 6, fin - 1); //нашли номер формулы
            str2 = parseInt(str2);
            if (str2 == 1) str2 = time * CHTS * koefDop; //Если формула с кодом 1 - t * K * чТС
            if (str2 == 2) str2 = salary * koefDop; //Если формула с кодом 2 -K * мТС(мДО)
            if (str2 == 3) str2 = MRP * koefDop; //Если формула с кодом 3 - К * МРП
            if (str2 == 4) str2 = salary * koefDop; //Если формула с кодом 4 - K * мТС(мДО)
            sum = sum + parseFloat(str2); //sum-для всего начислено
            str2 = str2.toFixed(2);
            sum2 = sum.toFixed(2);

            Desition1.data.push({id: "name-b" + ii, type: "textbox", value: str}, {id: "day-b" + ii, type: "textbox", value: ""}, {id: "hour-b" + ii, type: "textbox", value: ""}, {id: "value-b" + ii, type: "textbox", value: str2});
            ii++;
          }
        } // if (card2[jc].type == 'reglink')
      }
    } // конец цикла for (var jc = 0; jc < card2.length; jc++)
  } // if (card2.length > 2)
} //if (cardViplatyID)

////////////////////////
///// ВЫПЛАТЫ      END//
////////////////////////

Desition1.data.push({id:"name-b"+ii,type:"textbox",value:"Всего начислено"},{id:"day-b"+ii,type:"textbox",value:""},{id:"hour-b"+ii,type:"textbox",value:""},{id:"value-b"+ii,type:"textbox",value: sum2}); // Всего начислено
ii++;



Desition1.data.push({id:"name-b"+ii,type:"textbox",value:"2. УДЕРЖАНИЯ"},{id:"day-b"+ii,type:"textbox",value:""},{id:"hour-b"+ii,type:"textbox",value:""},{id:"value-b"+ii,type:"textbox",value:""});                                                                                   //Удержано
ii++;
var OPV=sum*0.1;
OPV=OPV.toFixed(2);
var IPN=(parseFloat(sum)-parseFloat(OPV)-parseFloat(MZP))*0.1;
IPN=IPN.toFixed(2);
Desition1.data.push({id:"name-b"+ii,type:"textbox",value:"Пенсионные выплаты"},{id:"day-b"+ii,type:"textbox",value:""},{id:"hour-b"+ii,type:"textbox",value:""},{id:"value-b"+ii,type:"textbox",value:OPV});
ii++;
Desition1.data.push({id:"name-b"+ii,type:"textbox",value:"Подоходный налог"},{id:"day-b"+ii,type:"textbox",value:""},{id:"hour-b"+ii,type:"textbox",value:""},{id:"value-b"+ii,type:"textbox",value:IPN});
ii++;

// добавляем удержания из реестра удержаний (если есть)
// salary - зарплата
var sumUderj=0;
if (tmpUd) {
  for (var dr = 0; dr < deductionRecord.length; dr++) {
    var uderjData = getAPIresult("rest/api/asforms/data/", deductionRecord[dr].dataUUID);
    uderjData = eval("(" + uderjData + ")").data;

    var typeUderj = '0';
    for (var i = 0; i < uderjData.length; i++) {
      if (uderjData[i].id == uderj.cmpType) {
        typeUderj = uderjData[i].value;
        break;
      }
    }
    var razmerUderj = 0;
    for (var i = 0; i < uderjData.length; i++) {
      if (uderjData[i].id == uderj.cmpSum) {
        razmerUderj = parseFloat(uderjData[i].key);
        break;
      }
    }
    var vidUderj='';
    for (var i = 0; i < uderjData.length; i++) {
      if (uderjData[i].id == uderj.cmpDoc) {
        vidUderj = uderjData[i].value;
        break;
      }
    }
    if (typeUderj == '1') {
      razmerUderj = parseFloat(salary) / 100 * razmerUderj;
    }
    sumUderj=parseFloat(sumUderj)+parseFloat(razmerUderj);
    razmerUderj=razmerUderj.toFixed(2);
    Desition1.data.push({id:"name-b"+ii,type:"textbox",value:vidUderj},{id:"day-b"+ii,type:"textbox",value:""},{id:"hour-b"+ii,type:"textbox",value:""},{id:"value-b"+ii,type:"textbox",value:razmerUderj});
    ii++;

  }
}

var tt=parseFloat(OPV)+parseFloat(IPN)+parseFloat(sumUderj);
tt=tt.toFixed(2);
Desition1.data.push({id:"name-b"+ii,type:"textbox",value:"Удержано"},{id:"day-b"+ii,type:"textbox",value:""},{id:"hour-b"+ii,type:"textbox",value:""},{id:"value-b"+ii,type:"textbox",value:tt});
ii++;
Desition1.data.push({id:"name-b"+ii,type:"textbox",value:"3. ВЫПЛАЧЕНО"},{id:"day-b"+ii,type:"textbox",value:""},{id:"hour-b"+ii,type:"textbox",value:""},{id:"value-b"+ii,type:"textbox",value:""});
ii++;
Desition1.data.push({id:"name-b"+ii,type:"textbox",value:"№4424  пл.п.№440/4424 Зар.плата за 29.11.2015г."},{id:"day-b"+ii,type:"textbox",value:""},{id:"hour-b"+ii,type:"textbox",value:""},{id:"value-b"+ii,type:"textbox",value:last_zp});
ii++;
Desition1.data.push({id:"name-b"+ii,type:"textbox",value:"Выплачено"},{id:"day-b"+ii,type:"textbox",value:""},{id:"hour-b"+ii,type:"textbox",value:""},{id:"value-b"+ii,type:"textbox",value:last_zp});
ii++;
//var dolg=parseFloat(sum)-parseFloat(OPV)-parseFloat(IPN);
var dolg=parseFloat(sum)-parseFloat(tt);
dolg=dolg.toFixed(2);
Desition1.data.push({id:"name-b"+ii,type:"textbox",value:"Итого к выдаче"},{id:"day-b"+ii,type:"textbox",value:""},{id:"hour-b"+ii,type:"textbox",value:""},{id:"value-b"+ii,type:"textbox",value:dolg});

var table = '';
for (var i=0; i < Desition1.data.length; i++)
{
  table+=obj2Str(Desition1.data[i])+',';
}
table=table.substring(0, table.length-1);
table=p1+table+p3;
//ss=table.indexOf("data");
table=table.substring(table.indexOf("data") - 1, table.length - 1);
form.setValue("test3",table);

form.save();
form1.save();

//сохранение данных по форме
var post = new org.apache.commons.httpclient.methods.PostMethod(synergyURL + "rest/api/asforms/data/save");
post.addParameter("formUUID", "a9c69ef3-cd41-4d85-89f2-b7cf1f0efbf5");
post.addParameter("uuid", dataUUID);
post.addParameter("data", table);
var client = new org.apache.commons.httpclient.HttpClient();
var creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(synergyUser, synergyPass);
client.getParams().setAuthenticationPreemptive(true);
client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
post.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=utf-8");
var status = client.executeMethod(post);
post.releaseConnection();




var form = platform.getFormsManager().getFormData (dataUUID);
form.load();
var organization='АО Озенмунайгаз';
var posit=form.getValue('position');

var card3 = getAPIresult("rest/api/positions/get_cards?positionID=", posit);
card3 = String(card3).replace(/form-uuid/g, "formUuid");
card3 = card3.replace(/data-uuid/g, "dataUuid");
card3 = eval("(" + card3 + ")");

// Ищем UUID нужной карточки должности
var cardUUID = '';
for (var i = 0; i < card3.length; i++) {
  if (card3[i].formUuid == 'b355996d-caa6-4ad2-a998-d7588dad14ba') {
    cardUUID = card3[i].dataUuid;
    break;
  }
}

var form_card = platform.getFormsManager().getFormData (cardUUID);
form_card.load();
var type=form_card.getValue("type");
if (type=='1')   //рабочий
{
   form.setValue("razr",form_card.getValue("discharge_work"));     //разряд
   form.setValue("gred",form_card.getValue("grade_work"));   //грейд
   form.setValue("categ",form_card.getValue("category_work"));    //категория

}
if (type=='2')   //служащие
{
   form.setValue("gred",form_card.getValue("grade_official"));   //грейд
   form.setValue("categ",form_card.getValue("category_official"));    //категория
}
form_card.save();

form.setValue("organization",organization);
form.setValue("salary",salary);
form.setValue("MTS",salary);
form.setValue("t_number",t_number);
form.setValue("CHTS",CHTS);
form.setValue("Save_MTS",Save_MTS);
form.setValue("Save_CHTS",Save_CHTS);
form.setValue("MRZP",MZP);
form.setValue("dolg_dep",last_zp);
var new_zp=parseFloat(last_zp)+parseFloat(dolg);
new_zp=new_zp.toFixed(2);
form.setValue("god_doh",new_zp);
form.setValue("god_pens",OPV);
form.setValue("god_pod",IPN);


form.setValue("IPN",IPN);
form.save();

var result=true;
var message = "ОК";
