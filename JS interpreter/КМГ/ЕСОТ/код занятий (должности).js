var form = platform.getFormsManager().getFormData(dataUUID);
form.load();

// общие настройки
var synergy = {
  login: "Administrator",
  password: "$ystemAdm1n",
  url: "http://127.0.0.1:8080/Synergy"
};

var posFormCode = "Карточка_должности";
var fieldCode = "link_official";
var codePosition = form.getValue("code_position"); // Код занятий (код должности)

function changedFieldValue(uuid, field, value) {
  var card = platform.getFormsManager().getFormData(uuid);
  card.load();
  card.setValue(field, value);
  card.save();
}

// Формирование поисковой строки
var paramSearch = '{"query": "where code=\''+posFormCode+'\' and `'+fieldCode+'.DATE` = \''+documentID+'\'", "parameters": ["'+fieldCode+'.DATE"], "showDeleted": "false", "searchInRegistry": "false"}';

// Поиск карточек по ссылке (documentID)
var APIString = synergy.url + "/rest/api/asforms/search/advanced";
var post = new org.apache.commons.httpclient.methods.PostMethod(APIString);
var client = new org.apache.commons.httpclient.HttpClient();
var creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(synergy.login, synergy.password);
client.getParams().setAuthenticationPreemptive(true);
client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
post.setRequestHeader("Content-type", "application/json; charset=utf-8");
post.setRequestBody(paramSearch);
var status = client.executeMethod(post);
var uuids = post.getResponseBodyAsString();
uuids = eval("(" + uuids + ")");
post.releaseConnection();

if (uuids.length > 0) {
  for (var i = 0; i < uuids.length; i++) {
    // сохраняем код должности в карточку
    changedFieldValue(uuids[i].dataUUID, 'code_position', codePosition);
  }
}

var result = true;
var message = "ОК";
