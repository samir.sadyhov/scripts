var form = platform.getFormsManager().getFormData(dataUUID);
form.load();

// общие настройки
var synergy = {
  login: "Administrator",
  password: "$ystemAdm1n",
  url: "http://127.0.0.1:8080/Synergy"
};

// получение результата api функции
function runAPIRequestGET(api, param) {
  var APIString = '';
  param == null ? APIString = synergy.url + api : APIString = synergy.url + api + param;
  var get = new org.apache.commons.httpclient.methods.GetMethod(APIString);
  var client = new org.apache.commons.httpclient.HttpClient();
  var creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(synergy.login, synergy.password);
  client.getParams().setAuthenticationPreemptive(true);
  client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
  get.setRequestHeader("Content-type", "application/json; charset=utf-8");
  var status = client.executeMethod(get);
  var result = get.getResponseBodyAsString();
  get.releaseConnection();
  return result;
}


// post.addParameter("lastname", Url.encode(user.lastname));

// изменение юзера
function modificationUser(user) {
  var result = null;
  var APIString = synergy.url + '/rest/api/filecabinet/user/save';
  var post = new org.apache.commons.httpclient.methods.PostMethod(APIString);
  post.addParameter("lastname", user.lastname);
  post.addParameter("firstname", user.firstname);
  post.addParameter("patronymic", user.patronymic);
  post.addParameter("pointersCode", user.pointersCode);
  post.addParameter("isConfigurator", user.configurator);
  post.addParameter("isAdmin", user.admin);
  post.addParameter("email", user.mail);
  post.addParameter("jid", user.jid);
  post.addParameter("hasAccess", user.access);
  post.addParameter("hasPointersBookAccess", user.pointers);
  post.addParameter("hasStrategyAccess", user.pointers);
  post.addParameter("userID", user.userID);

  var client = new org.apache.commons.httpclient.HttpClient();
  var creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(synergy.login, synergy.password);
  client.getParams().setAuthenticationPreemptive(true);
  client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
  post.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=utf-8");
  var status = client.executeMethod(post);
  result = post.getResponseBodyAsString();
  post.releaseConnection();
  return result;
}

// Функция проверки записей в таблице
function isHaveARow(form, table) {
  try {
    form.getRowsCount(table);
    return true;
  } catch (err) {
    return false;
  }
}

// Url.encode, Url.decode
var Url = {
  // публичная функция для кодирования URL
  encode : function (string) {
    return escape(this._utf8_encode(string));
  },

  // публичная функция для декодирования URL
  decode : function (string) {
    return this._utf8_decode(unescape(string));
  },

  // приватная функция для кодирования URL
  _utf8_encode : function (string) {
    string = string.replace(/\r\n/g,"\n");
    var utftext = "";

    for (var n = 0; n < string.length; n++) {

      var c = string.charCodeAt(n);

      if (c < 128) {
        utftext += String.fromCharCode(c);
      }
      else if((c > 127) && (c < 2048)) {
        utftext += String.fromCharCode((c >> 6) | 192);
        utftext += String.fromCharCode((c & 63) | 128);
      }
      else {
        utftext += String.fromCharCode((c >> 12) | 224);
        utftext += String.fromCharCode(((c >> 6) & 63) | 128);
        utftext += String.fromCharCode((c & 63) | 128);
      }

    }

    return utftext;
  },

  // приватная функция для декодирования URL
  _utf8_decode : function (utftext) {
    var string = "";
    var i = 0;
    var c = c1 = c2 = 0;

    while ( i < utftext.length ) {

      c = utftext.charCodeAt(i);

      if (c < 128) {
        string += String.fromCharCode(c);
        i++;
      }
      else if((c > 191) && (c < 224)) {
        c2 = utftext.charCodeAt(i+1);
        string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
        i += 2;
      }
      else {
        c2 = utftext.charCodeAt(i+1);
        c3 = utftext.charCodeAt(i+2);
        string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
        i += 3;
      }

    }

    return string;
  }

}



var cardFormID = runAPIRequestGET('/rest/api/asforms/form_ext?formCode=', Url.encode("Штатная_расстановка111")); // получаем formID по коду формы
cardFormID = eval("(" + cardFormID + ")").uuid;

var table = 't';
var currentUserData = [];
var newUserData = [];

if (isHaveARow(form, table)) {
  for (var i = 0; i < form.getRowsCount(table)+5; i++) {
    if (form.getValue(table, 'user', i)) {
      // получаем текущии данные о пользователе
      var currentUser = runAPIRequestGET('/rest/api/filecabinet/user/', form.getValue(table, 'user', i));
      currentUser = eval("(" + currentUser + ")");

      // Массив пользователй на изменения
      currentUserData.push(
        {
          userid: currentUser.userid,
          tabNum: form.getValue(table, 'tab_num', i),
          lastname: currentUser.lastname,
          firstname: currentUser.firstname,
          patronymic: currentUser.patronymic,
          code: currentUser.code,

          configurator: currentUser.configurator,
          admin: currentUser.admin,
          mail: currentUser.mail,
          jid: currentUser.jid,
          access: currentUser.access,
          pointers: currentUser['pointers-access']
        }
      );
      // Массив изменяемых данных
      newUserData.push(
        {
          udo: {
            number: form.getValue(table, 'numberofudlichnosti', i),
            date: form.getValue(table, 'dateofud', i),
            organ: form.getValue(table, 'organofud', i)
          },
          pasport: {
            number: form.getValue(table, 'numberofpassport', i),
            date: form.getValue(table, 'dateofpassport', i),
            organ: form.getValue(table, 'organofpassport', i)
          },
          user: {
            lastname: form.getValue(table, 'user_lastname', i),
            firstname: form.getValue(table, 'user_name', i),
            patronymic: form.getValue(table, 'user_middle', i)
          }
        }
      );

    }
  }
}

var resultUserSave = [];

if (currentUserData.length > 0) {
  currentUserData.forEach(function(data, index) {

    if (data) {

      // записываем значения в карточку пользователя
      var card = platform.getCardsManager().getUserCard(cardFormID, data.userid);
      card.load();
      // удостоверение
      card.setValue("numberofudlichnosti", newUserData[index].udo.number);
      card.setValue("dateofud", newUserData[index].udo.date);
      card.setValue("organofud", newUserData[index].udo.organ);
      // паспорт
      card.setValue("numberofpassport", newUserData[index].pasport.number);
      card.setValue("dateofpassport", newUserData[index].pasport.date);
      card.setValue("organofpassport", newUserData[index].pasport.organ);
      card.save();

      // Изменение данных юзера в орг.структуре
      var tmpUser = {
        lastname: newUserData[index].user.lastname,
        firstname: newUserData[index].user.firstname,
        patronymic: newUserData[index].user.patronymic,
        pointersCode: data.code,
        configurator: data.configurator,
        admin: data.admin,
        mail: data.mail,
        jid: data.jid,
        access: data.access,
        pointers: data.pointers,
        userID: data.userid
      };
      var tmpRes = modificationUser(tmpUser);
      resultUserSave.push(eval("(" + tmpRes + ")"));

    }

  });
}

form.save();
var result = true;
