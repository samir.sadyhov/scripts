var form = platform.getFormsManager().getFormData(dataUUID);
form.load();

// общие настройки
var synergy = {
  login: "Administrator",
  password: "$ystemAdm1n",
  url: "http://127.0.0.1:8080/Synergy"
};

// номер и дата регистрации
var regNumber = form.getValue('number');
var regDate = form.getValue('date');

// Формирование поисковой строки
var searchParam = '{"query": "where dataUUID = (SELECT getAsfDataID(docID) FROM register_docs WHERE !defective AND deleted IS NULL AND DATE(date)=DATE(\'' + regDate + '\') AND number=\'' + regNumber + '\')", "parameters": [], "showDeleted": "false", "searchInRegistry": "true"}';

// если указать айди журнала будет быстрей
// registerid=\'8c4d024a-4bd5-4ddb-9ee2-f5f061f48f22\' AND

// поиск приказа который нужно отменить
var APIString = synergy.url + "/rest/api/asforms/search/advanced";
var post = new org.apache.commons.httpclient.methods.PostMethod(APIString);
var client = new org.apache.commons.httpclient.HttpClient();
var creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(synergy.login, synergy.password);
client.getParams().setAuthenticationPreemptive(true);
client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
post.setRequestHeader("Content-type", "application/json; charset=utf-8");
post.setRequestBody(searchParam);
var status = client.executeMethod(post);
var resultSearch = post.getResponseBodyAsString();
resultSearch = eval("(" + resultSearch + ")");

// Если маршрут записи завершен успешно (запись активирована)
if (resultSearch[0].registryRecordStatus == 'STATE_SUCCESSFUL') {
  var prikaz = platform.getFormsManager().getFormData(resultSearch[0].dataUUID);
  prikaz.load();
  // записываем в нужное поле 1
  prikaz.setValue('cancel', '1');
  prikaz.save();
}

form.save();
var result = true;
var message = 'OK';
