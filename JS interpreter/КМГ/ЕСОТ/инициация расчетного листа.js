var form = platform.getFormsManager().getFormData(dataUUID);
form.load();

// общие настройки
var synergy = {
  login: "Administrator",
  password: "$ystemAdm1n",
  url: "http://127.0.0.1:8080/Synergy",
  apiGetData: "/rest/api/asforms/data/",
  apiSaveData: "/rest/api/asforms/data/save",
  apiUserData: "/rest/api/filecabinet/user/",
  apiUserSearch: "/rest/api/userchooser/search",
  apiCreateDoc: "/rest/api/registry/create_doc?registryID=",
  apiActivateDoc: "/rest/api/registry/activate_doc?dataUUID=",
  apiDepInfo: "/rest/api/departments/get?departmentID="
};
// получение результата api функции
function runAPIRequestGET(api, param) {
  var APIString = '';
  param == null ? APIString = synergy.url + api : APIString = synergy.url + api + param;
  var get = new org.apache.commons.httpclient.methods.GetMethod(APIString);
  var client = new org.apache.commons.httpclient.HttpClient();
  var creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(synergy.login, synergy.password);
  client.getParams().setAuthenticationPreemptive(true);
  client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
  get.setRequestHeader("Content-type", "application/json; charset=utf-8");
  var status = client.executeMethod(get);
  var result = get.getResponseBodyAsString();
  get.releaseConnection();
  return result;
}
// сохранение данных по форме
function runAPIRequestPOST(api, form, dataUUID, data) {
  var APIString = synergy.url + api;
  var post = new org.apache.commons.httpclient.methods.PostMethod(APIString);
  post.addParameter("formUUID", form);
  post.addParameter("uuid", dataUUID);
  post.addParameter("data", data);
  var client = new org.apache.commons.httpclient.HttpClient();
  var creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(synergy.login, synergy.password);
  client.getParams().setAuthenticationPreemptive(true);
  client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
  post.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=utf-8");
  var status = client.executeMethod(post);
  var result = post.getResponseBodyAsString();
  post.releaseConnection();
  return result;
}

// ID реестра \Расчетный лист\
var regID = "a3eb22c0-9851-4214-a802-2fb598f551dd";
// ID формы \Расчетный лист\
var regFormID = "a9c69ef3-cd41-4d85-89f2-b7cf1f0efbf5";
// Получаем месяц год с формы
var yearKey = Number(form.getValue('year'));
var yearValue='';
switch (yearKey) {
  case 1:
    yearValue='2015';
    break;
  case 2:
    yearValue='2016';
    break;
  case 3:
    yearValue='2017';
    break;
}

var month = Number(form.getValue('month'));
var monthName='';
switch (month) {
  case 1:
    monthName='Январь';
    break;
  case 2:
    monthName='Февраль';
    break;
  case 3:
    monthName='Март';
    break;
  case 4:
    monthName='Апрель';
    break;
  case 5:
    monthName='Май';
    break;
  case 6:
    monthName='Июнь';
    break;
  case 7:
    monthName='Июль';
    break;
  case 8:
    monthName='Август';
    break;
  case 9:
    monthName='Сентябрь';
    break;
  case 10:
    monthName='Октябрь';
    break;
  case 11:
    monthName='Ноябрь';
    break;
  case 12:
    monthName='Декабрь';
    break;
}

var tmpStr = ['"O', '"О', '"_', 'з""'];

// массив пользователей
var ArrUsers = [];
// получаем ID департамента
var initDepID = form.getValue('department');
// получаем ID руководителя департамента
var managerID = runAPIRequestGET(synergy.apiDepInfo, initDepID);
managerID = String(managerID);
managerID=managerID.replace(new RegExp(tmpStr[0],'g'),'O');
managerID=managerID.replace(new RegExp(tmpStr[1],'g'),'О');
managerID=managerID.replace(new RegExp(tmpStr[2],'g'),'_');
managerID=managerID.replace(new RegExp(tmpStr[3],'g'),'з"');
managerID = eval("(" + managerID + ")");
managerID = managerID.manager.managerID;

// получаем всех подчиненных
var usersDep = runAPIRequestGET(synergy.apiUserSearch, "?showAll=false&userID="+managerID);
usersDep = eval("(" + usersDep + ")");

// формируем массив данных пользователей
for (var i = 0; i < usersDep.length; i++) {
  var userInfo = runAPIRequestGET(synergy.apiUserData, usersDep[i].userID);
  userInfo = String(userInfo);
  userInfo=userInfo.replace(new RegExp(tmpStr[0],'g'),'O');
  userInfo=userInfo.replace(new RegExp(tmpStr[1],'g'),'О');
  userInfo=userInfo.replace(new RegExp(tmpStr[2],'g'),'_');
  userInfo=userInfo.replace(new RegExp(tmpStr[3],'g'),'з"');
  userInfo = eval("(" + userInfo + ")");
  ArrUsers.push({
    userid: usersDep[i].userID,
    fio: usersDep[i].name,
    positionID: userInfo.positions[0].positionID,
    positionName: userInfo.positions[0].positionName,
    departmentID: userInfo.positions[0].departmentID,
    departmentName: userInfo.positions[0].departmentName
  });
}

if (ArrUsers.length > 0) {
  for (var j = 0; j < ArrUsers.length; j++) {
    // создаем запись в реестре
    var newAsfDataID = runAPIRequestGET(synergy.apiCreateDoc, regID);
    newAsfDataID = eval("(" + newAsfDataID + ")");
    // получаем ID записи
    newAsfDataID=newAsfDataID.dataUUID;
    // формируем JSON строку
    var newData = '"data":[{"id":"cmp-3mppwb","type":"label","label":"Расчетный лист за","value":" "},{"id":"cmp-wq5py4","type":"label","label":" ","value":" "},{"id":"month","type":"listbox","value":"'+monthName+'","key":"'+month+'"},{"id":"cmp-s23cy7","type":"label","label":" ","value":" "},{"id":"year","type":"listbox","value":"'+yearValue+'","key":"'+yearKey+'"},{"id":"cmp-m2p0kt","type":"label","label":" ","value":" "},{"id":"cmp-x5aeb7","type":"label","label":"Работник","value":" "},{"id":"user","type":"entity","value":"'+ArrUsers[j].fio+'","key":"'+ArrUsers[j].userid+'","formatVersion":"V1"},{"id":"cmp-gcwb2e","type":"label","label":"Разряд","value":" "},{"id":"razr","type":"textbox"},{"id":"tarif_stavka","type":"label","label":"Месячная тарифная ставка с учетом режима работы","value":" "},{"id":"MTS","type":"textbox"},{"id":"cmp-dt4exl","type":"label","label":"Долг предприятия:","value":" "},{"id":"dolg_dep","type":"textbox"},{"id":"cmp-c9taqs","type":"label","label":"Табельный номер","value":" "},{"id":"t_number","type":"textbox"},{"id":"cmp-b3aos2","type":"label","label":"Грейд","value":" "},{"id":"gred","type":"textbox"},{"id":"cmp-hen1ns","type":"label","label":"Часовая тарифная ставка","value":" "},{"id":"CHTS","type":"textbox"},{"id":"cmp-vbul4d","type":"label","label":"Годовой доход:","value":" "},{"id":"god_doh","type":"textbox"},{"id":"cmp-ir8ssg","type":"label","label":"Должность","value":" "},{"id":"position","type":"entity","value":"'+ArrUsers[j].positionName+'","key":"'+ArrUsers[j].positionID+'"},{"id":"cmp-sppkh1","type":"label","label":"Категория","value":" "},{"id":"categ","type":"textbox"},{"id":"part_stavka","type":"label","label":"Сохраняемая часть к месячной тарифной ставке","value":" "},{"id":"Save_MTS","type":"textbox"},{"id":"cmp-7kinb9","type":"label","label":"Годовые пенс.:","value":" "},{"id":"god_pens","type":"textbox"},{"id":"sdfgh","type":"label","label":"Подразделение","value":" "},{"id":"department","type":"entity","value":"'+ArrUsers[j].departmentName+'","key":"'+ArrUsers[j].departmentID+'"},{"id":"cmp-75wfo6","type":"label","label":"МРМЗП","value":" "},{"id":"MRZP","type":"textbox"},{"id":"chas_stavka","type":"label","label":"Часовая ставка сохраняемемой части","value":" "},{"id":"Save_CHTS","type":"textbox"},{"id":"cmp-tbtdgf","type":"label","label":"Годовой под. налог:","value":" "},{"id":"god_pod","type":"textbox"},{"id":"cmp-sry7uw","type":"label","label":" ","value":" "},{"id":"table","type":"appendable_table","key":"","data":[{"id":"cmp-wg8zya","type":"label","label":"Вид ","value":" "},{"id":"cmp-hgkj15","type":"label","label":"Дни","value":" "},{"id":"cmp-hjt4uc","type":"label","label":"Часы","value":" "},{"id":"cmp-iw2kgs","type":"label","label":"Сумма","value":" "}]}]';
    // сохраняем данные в новой записи реестра
    var resultFormSave = runAPIRequestPOST(synergy.apiSaveData, regFormID, newAsfDataID, newData);
    // запускаем активацию
    var resultActivateDoc = runAPIRequestGET(synergy.apiActivateDoc, newAsfDataID);
  }
}

form.save();
var result = true;
var message = 'OK';
