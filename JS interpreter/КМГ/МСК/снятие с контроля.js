var formPlan = platform.getFormsManager().getFormData(dataUUID);
formPlan.load();

// Функция проверки записей в таблице
function isHaveARow(form, table) {
  try {
    form.getRowsCount(table);
    return true;
  } catch (err) {
    return false;
  }
}

if (isHaveARow(formPlan, 'appeal')) {
  for (var i = 0; i < formPlan.getRowsCount('appeal'); i++) {
    // Получаем статус вопроса (контроль)
    var sign = formPlan.getValue('appeal', 'sign', i);
    var asfDataUUID = formPlan.getValue('appeal', 'linkAsfDataId', i);
    var res = 0, v = 0;
    var resMsg = '';

    // Проверяем, снят ли вопрос с контроля
    if ((sign == '[null]') && (asfDataUUID != '1')) {
      // Если вопрос снят, берем дополнительные данные (ссылки на вопрос)
      var cmpID = formPlan.getValue('appeal', 'linkCmpID', i);
      cmpID = Number(cmpID) - 1;
      var tableName = formPlan.getValue('appeal', 'TableName', i);

      // И снимаем вопрос с контроля в реестре 02 или 03
      var formProblem = platform.getFormsManager().getFormData(asfDataUUID);
      formProblem.load();

      if (tableName == 'table02') {
        formProblem.setValue('table02', 'desicion', cmpID, '1');
      } else {
        formProblem.setValue('table03', 'desicion', cmpID, '1');
      }
      formProblem.save();

      // помечаем снятый с контроля вопрос, чтоб в будущем по этому вопросу ничего не делать
      formPlan.setValue('appeal', 'linkAsfDataId', i, '1');
      res++;
    }

    // подсчитывает кол-во оставшихся на контроле вопросов
    if (sign != '[null]') { v++; }
  }
}

// записывает значение оставшихся на контроле вопросов на форму
formPlan.setValue("count", parseInt(v));

formPlan.save();
var result = true;
if (res > 0) {
  resMsg = "Снятых с контроля вопросов: " + res;
} else {
  resMsg = "Не было снято с контроля";
}

var message = resMsg;
