// инициирующие параметры

// Настройки
var synergyUser = "Administrator";
var synergyPass = "$ystemAdm1n";
var synergyURL = "http://127.0.0.1:8080/Synergy";

// var DST preiod (in days)
var reportDSTPeriod = 30; // 30 days by default

// var Qst preiod (in days)
var reportQSTPeriod = 7;

// реестры
var Registries = {
  // реестр "социальная напряженность"
  CH: { uuid: '46e16e23-af5f-46eb-a792-47679012ee87', formid: '17da8e58-fb6c-4534-9ce1-21d0f59ccdbc'},

  // реестр "Анкета"
  Anketa : {uuid: '6b9edece-ad7f-4478-aba4-80d456e7fea4', formid: '64bc2160-39d1-4ccb-b703-633f088c079f'},

  // реестр "Диаграмма"
  DIAG : {uuid: '58795b10-85b4-4584-834b-06066004712d', formid: 'ddfc820e-5967-4b8f-9619-f776b5c597b2'},

  // реестр "Вспомогательные коэффициенты"
  VSP_coef : {uuid: 'df566883-7820-4842-a543-204f3d38208f', formid: 'f4c3c114-a9a7-4827-824f-ba9cb5b17884'},

  // реестр "Оценка уровня трудовой дисциплины"
  OUTD : {uuid: '51df80a6-902d-4149-8847-477a2221c855', formid: 'c0e790d7-e7cd-4562-9e17-a21342da5250'},

  // реестр "Обращение по поводу проблем"
  OPPP : {uuid: '37934c40-268b-4ef5-9546-18c815c0d591', formid: 'a59576f8-93a0-4d5a-8398-2bf363176b84'},

  // реестр "Отчет по производственным премиям"
  OPPPr : {uuid: '4fc0dc1b-a640-4ec8-89d9-9201437e8e95', formid: 'e379f5b8-f242-4a12-b100-cf2e06679429'},

  // реестр "Отчет по соц выплатам"
  OPPPs : {uuid: 'db0d891c-3a39-4f91-b067-8663eb1d4d4e', formid: '69fad48c-1465-4b0c-968b-7fb672797870'},

  // реестр "Отчет по повышению квалификации"
  Ppqv : {uuid: '677232fa-5c57-46a9-a9ae-0839280e82e3', formid: 'c487b1c6-1313-4757-951c-ee5e49a874e8'},

  // реестр "Отчет по социально-экономическим показателям"
  S : {uuid: '8078c6d6-341d-430d-a2f9-774464b22ed0', formid: '3a631f8e-6a89-4e84-af8d-4b7ecf36f0f0'},

  // реестр "Отчет по социально-экономическим показателям"
  PDES : {uuid: '61191fa2-037e-44a4-a786-79f11d8cd101', formid: 'cc440126-6489-46d9-aa50-47b4a9249aa2'},

  // реестр "Настройки компаний"
  CompanySettings : {uuid: 'd9b4245b-7be7-4a6d-b085-0b7943653a96', formid: '09cd8a0b-049b-4ed4-94a4-a0eef3f13993'}
};

var CH_codes = new Array (
  { "code_local":"Pe1",
    "code_remote":"P(e)1",
    "code_description":"Доля респондентов, набравшие менее 2 баллов",
    "code_result":"P(e)1_directory", // код справочника для интерпретации результатов
    "code_questionnaire":"Анкета \"Уровень социальной фрустрированности респондента\"",
    "code_questionnaire_key":"1",
    "code_background":"Фоновый",
    "code_background_key":"1"},

  { "code_local":"Pe2",
    "code_remote":"P(e)2",
    "code_description":"Доля респондентов, набравшие от 2-х до 2,9 баллов",
    "code_result":"P(e)2_directory",
    "code_questionnaire":"Анкета \"Уровень социальной фрустрированности респондента\"",
    "code_questionnaire_key":"1",
    "code_background":"Фоновый",
    "code_background_key":"1"},

  { "code_local":"Pe3",
    "code_remote":"P(e)3",
    "code_description":"Доля респондентов, набравшие 3 балла и более",
    "code_result":"P(e)3_directory",
    "code_questionnaire":"Анкета \"Уровень социальной фрустрированности респондента\"",
    "code_questionnaire_key":"1",
    "code_background":"Фоновый",
    "code_background_key":"1"},

  { "code_local":"Pakt",
    "code_remote":"Р(akt)",
    "code_description":"Доля работников, хотя бы в одной из ситуации выбравших гражданскую активность",
    "code_result":"P(akt)_directory",
    "code_questionnaire":"Анкета \"Готовность к конфликту\"",
    "code_questionnaire_key":"2",
    "code_background":"Фоновый",
    "code_background_key":"1"},

  { "code_local":"Рind",
    "code_remote":"Р(ind)",
    "code_description":"Доля работников, хотя бы в одной из ситуации выбравших реакцию индивидуального противодействия",
    "code_result":"P(ind)_directory",
    "code_questionnaire":"Анкета \"Готовность к конфликту\"",
    "code_questionnaire_key":"2",
    "code_background":"Фоновый",
    "code_background_key":"1"},

  { "code_local":"Pcrim",
    "code_remote":"P(crim)",
    "code_description":"Доля работников, хотя бы в двух ситуациях выбравших девиантное поведение/криминальную активность",
    "code_result":"P(crim)_directory",
    "code_questionnaire":"Анкета \"Готовность к конфликту\"",
    "code_questionnaire_key":"2",
    "code_background":"Фоновый",
    "code_background_key":"1"},

  { "code_local":"Рout",
    "code_remote":"Р(out)",
    "code_description":"Доля работников, хотя бы в двух ситуациях выбравших стратегию ухода из фрустрирующей ситуации",
    "code_result":"P(out)_directory",
    "code_questionnaire":"Анкета \"Готовность к конфликту\"",
    "code_questionnaire_key":"2",
    "code_background":"Фоновый",
    "code_background_key":"1"},

  { "code_local":"Рmax",
    "code_remote":"Р(max)",
    "code_description":"Доля работников, в более чем половине ситуаций выбравших присоединение к реакции большинства",
    "code_result":"P(max)_directory",
    "code_questionnaire":"Анкета \"Готовность к конфликту\"",
    "code_questionnaire_key":"2",
    "code_background":"Фоновый",
    "code_background_key":"1"},

  { "code_local":"Р0",
    "code_remote":"Р(0)",
    "code_description":"Доля работников, в более чем половине ситуаций выбравших отсутствие реакции",
    "code_result":"P(0)_directory",
    "code_questionnaire":"Анкета \"Готовность к конфликту\"",
    "code_questionnaire_key":"2",
    "code_background":"Фоновый",
    "code_background_key":"1"},

  { "code_local":"Рaag",
    "code_remote":"Р(aag)",
    "code_description":"Доля работников, в более чем половине ситуаций выбравших аутоагрессивное поведение",
    "code_result":"P(aag)_directory",
    "code_questionnaire":"Анкета \"Готовность к конфликту\"",
    "code_questionnaire_key":"2",
    "code_background":"Фоновый",
    "code_background_key":"1"},

  { "code_local":"Pconnect",
    "code_remote":"P(connect)",
    "code_description":"Доля респондентов, имеющий экстремально высокие показатели неформальных связей",
    "code_result":"P(connect)_directory",
    "code_questionnaire":"Анкета \"Связанность респондентов в коллективе\"",
    "code_questionnaire_key":"3",
    "code_background":"Фоновый",
    "code_background_key":"1"},

  { "code_local":"X",
    "code_remote":"X",
    "code_description":"Нормированное среднее значение связанности работников в коллективе.",
    "code_result":"X_directory",
    "code_questionnaire":"Анкета \"Связанность респондентов в коллективе\"",
    "code_questionnaire_key":"3",
    "code_background":"Фоновый",
    "code_background_key":"1"},

  { "code_local":"Z",
    "code_remote":"Z",
    "code_description":"Общий средний нормированный показатель неудовлетворенности",
    "code_result":"Z_directory",
    "code_questionnaire":"Анкета \"Неудовлетворенность работников различными аспектами работы на предприятии\"",
    "code_questionnaire_key":"4",
    "code_background":"Нефоновый",
    "code_background_key":"0"},

  { "code_local":"Psat",
    "code_remote":"P(sat)",
    "code_description":"Общая доля проблем, вызывающих максимальную неудовлетворенность",
    "code_result":"P(sat)_directory",
    "code_questionnaire":"Анкета \"Неудовлетворенность работников различными аспектами работы на предприятии\"",
    "code_questionnaire_key":"4",
    "code_background":"Нефоновый",
    "code_background_key":"0"},

  { "code_local":"S",
    "code_remote":"S",
    "code_description":"Средняя оценка стабильности социально-экономических показателей",
    "code_result":"S_directory", // код справочника для интерпретации результатов
    "code_questionnaire":"Отчет \"О социально-экономических показателях\"",
    "code_questionnaire_key":"6",
    "code_background":"Фоновый",
    "code_background_key":"1"},

  { "code_local":"Dst",
    "code_remote":"Dst",
    "code_description":"Стандартизированное количество нарушений за отчетный период (месяц)",
    "code_result":"Dst_directory",
    "code_questionnaire":"Отчет \"Уровень социальной фрустрированности респондента\"",
    "code_questionnaire_key":"1",
    "code_background":"Фоновый",
    "code_background_key":"1"},

  { "code_local":"Qst",
    "code_remote":"Qst",
    "code_description":"Стандартизированное количество обращений о проблемах/вопросах",
    "code_result":"Qst_directory",
    "code_questionnaire":"Отчет по уровню трудовой дисциплины",
    "code_questionnaire_key":"5",
    "code_background":"Нефоновый",
    "code_background_key":"0"},

  { "code_local":"Pq",
    "code_remote":"P(q)",
    "code_description":"Доля нерешенных проблем/обращений/вопросов",
    "code_result":"P(q)_directory",
    "code_questionnaire":"Отчет \"По обращениям и вопросам работников\"",
    "code_questionnaire_key":"5",
    "code_background":"Нефоновый",
    "code_background_key":"0"},

  { "code_local":"Pdes",
    "code_remote":"P(des)",
    "code_description":"Стандартизированное количество (относительно среднего показателя за предыдущий отчетный период) работников, получивших травму/заболевание",
    "code_result":"P(des)_directory",
    "code_questionnaire":"Отчет \"О социально-психологических показателях\"",
    "code_questionnaire_key":"6",
    "code_background":"Фоновый",
    "code_background_key":"1"},

  { "code_local":"Ppp",
    "code_remote":"P(pp)",
    "code_description":"Доля работников (с позициями не выше самого низшего управленческого звена), получивших производственные премии",
    "code_result":"P(pp)_directory",
    "code_questionnaire":"Отчет \"О социально-психологических показателях\"",
    "code_questionnaire_key":"6",
    "code_background":"Фоновый",
    "code_background_key":"1"},

  { "code_local":"Ppqv",
    "code_remote":"P(pqv)",
    "code_description":"Доля работников, участвовавших в повышении квалификации",
    "code_result":"P(pqv)_directory",
    "code_questionnaire":"Отчет \"О социально-психологических показателях\"",
    "code_questionnaire_key":"6",
    "code_background":"Фоновый",
    "code_background_key":"1"},

  { "code_local":"Pss",
    "code_remote":"P(ss)",
    "code_description":"Доля работников, получивших социальные выплаты",
    "code_result":"P(ss)_directory",
    "code_questionnaire":"Отчет \"О социально-психологических показателях\"",
    "code_questionnaire_key":"6",
    "code_background":"Фоновый",
    "code_background_key":"1"}
);

var CH_props = ["value", "coef", "date", "result"];

// -- Вспомогательные коэффициенты prototype
var VSP_coef = function() {};
VSP_coef.prototype.parameter_M = Number(0);
VSP_coef.prototype.parameter_N = Number(0);
VSP_coef.prototype.parameter_T1 = Number(0);
VSP_coef.prototype.parameter_T2 = Number(0);
VSP_coef.prototype.parameter_T3 = Number(0);

VSP_coef.prototype.parameter_M_date = "no date";
VSP_coef.prototype.parameter_N_date = "no date";
VSP_coef.prototype.parameter_T1_date = "no date";
VSP_coef.prototype.parameter_T2_date = "no date";
VSP_coef.prototype.parameter_T3_date = "no date";

VSP_coef.prototype.parameter_M_duration = Number(0);
VSP_coef.prototype.parameter_N_duration = Number(0);
VSP_coef.prototype.parameter_T1_duration = Number(0);
VSP_coef.prototype.parameter_T2_duration = Number(0);
VSP_coef.prototype.parameter_T3_duration = Number(0);


// -- CH prototype
var protoCH = function() {};
protoCH.prototype.company = "no company";
protoCH.prototype.date = "no date";
protoCH.prototype.CH = Number(0);
protoCH.prototype.log = "";

// make default settings
var today = new Date();
var month = Number( today.getMonth() + 1 );
if (month < 10) { month = '0' + month;}
var currentdate =  ('0'+today.getDate()).slice(-2) + '.' + (month) + '.' + today.getFullYear();

function parseDate(date, day) {
  if (day) date.setDate(date.getDate() - day);
  var month = Number(date.getMonth() + 1);
  if (month < 10) month = '0' + month;

  return date.getFullYear() + '-' + month + '-' + ('0' + date.getDate()).slice(-2);
}

for (var ic = 0; ic < CH_codes.length; ic++) {
  eval ("protoCH.prototype." + CH_codes[ic].code_local + "_value = Number(0);");
  eval ("protoCH.prototype." + CH_codes[ic].code_local + "_coef = Number(0);");
  eval ("protoCH.prototype." + CH_codes[ic].code_local + "_date = '" + currentdate + "';");
}

// recalc CH value. Returns Number() of CH
protoCH.prototype.recalcCH = function(){
  var ret = 0;
  for (var icr = 0; icr < CH_codes.length; icr++) {
    var tmp_value = eval("this." + CH_codes[icr].code_local + "_value");
    var tmp_coef  = eval("this." + CH_codes[icr].code_local + "_coef");
    if (typeof tmp_value == "undefined"|| tmp_value == "undefined") { tmp_value = 0; }
    if (typeof tmp_coef == "undefined" || tmp_coef == "undefined") { tmp_coef = 0; }
    ret = ret + Number(tmp_value) * Number(tmp_coef);
  }
  // перевод итогового значения СН из -8,75..20,75 в 0..100
  ret = (Number(ret) + 8.75) * 200/59;
  return ret;
};

// recalc data for DIAGRAMM
protoCH.prototype.recalcDIAG = function(){
    var ret = [0, 0];
    var dataFON = 0;
    var dataQST = 0;

    for (var icd = 0; icd < CH_codes.length; icd++) {
        var tmp_mod = ( Number(eval("this." + CH_codes[icd].code_local + "_value")) * Number(eval("this." + CH_codes[icd].code_local + "_coef")) );

        if ( Number(tmp_mod) < 0 ) { tmp_mod = tmp_mod * -1; }

        if (CH_codes[icd].code_background_key == "1") {
            // фоновый
            dataFON = Number(dataFON) + tmp_mod;
        } else {
            // не фоновый
            dataQST = Number(dataQST) + tmp_mod;
        }
    }
    ret = [ ( Number(dataFON) * 100 / ( Number(dataFON) + Number(dataQST) ) ), ( Number(dataQST) * 100 / ( Number(dataFON) + Number(dataQST) ) )];
    return ret;
};
protoCH.prototype.setLog = function(str){
    this.log = this.log + str + "\n";
    return true;
};
protoCH.prototype.getLog = function(){
    return this.log;
};

// Url.encode, Url.decode
var Url = {
    // публичная функция для кодирования URL
    encode : function (string) {
        return escape(this._utf8_encode(string));
    },

    // публичная функция для декодирования URL
    decode : function (string) {
        return this._utf8_decode(unescape(string));
    },

    // приватная функция для кодирования URL
    _utf8_encode : function (string) {
        string = string.replace(/\r\n/g,"\n");
        var utftext = "";

        for (var n = 0; n < string.length; n++) {

            var c = string.charCodeAt(n);

            if (c < 128) {
                utftext += String.fromCharCode(c);
            }
            else if((c > 127) && (c < 2048)) {
                utftext += String.fromCharCode((c >> 6) | 192);
                utftext += String.fromCharCode((c & 63) | 128);
            }
            else {
                utftext += String.fromCharCode((c >> 12) | 224);
                utftext += String.fromCharCode(((c >> 6) & 63) | 128);
                utftext += String.fromCharCode((c & 63) | 128);
            }

        }

        return utftext;
    },

    // приватная функция для декодирования URL
    _utf8_decode : function (utftext) {
        var string = "";
        var i = 0;
        var c = c1 = c2 = 0;

        while ( i < utftext.length ) {

            c = utftext.charCodeAt(i);

            if (c < 128) {
                string += String.fromCharCode(c);
                i++;
            }
            else if((c > 191) && (c < 224)) {
                c2 = utftext.charCodeAt(i+1);
                string += String.fromCharCode(((c & 31) << 6) | (c2 & 63));
                i += 2;
            }
            else {
                c2 = utftext.charCodeAt(i+1);
                c3 = utftext.charCodeAt(i+2);
                string += String.fromCharCode(((c & 15) << 12) | ((c2 & 63) << 6) | (c3 & 63));
                i += 3;
            }

        }

        return string;
    }

};

// functions
function setCurrentForm(name, value){
    var currentForm = platform.getFormsManager().getFormData(dataUUID);
    currentForm.load();
    currentForm.setValue(name, value);
    currentForm.save();
    return true;
}

// returns  returnRequest.status
//          returnRequest.responseBody
//          returnRequest.json
function runAPIRequestGET(RegistryID, FormID, APIString, contenttype) {
    if (typeof contenttype == "undefined") { contenttype = "application/json; charset=utf-8"; }
    var get = new org.apache.commons.httpclient.methods.GetMethod(APIString);
    var client = new org.apache.commons.httpclient.HttpClient();

    var creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(synergyUser, synergyPass);
    client.getParams().setAuthenticationPreemptive(true);
    client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);

    get.setRequestHeader("Content-type", contenttype);
    get.setRequestHeader("formUUID", FormID);
    get.setRequestHeader("registrryUUID", RegistryID);

    var returnRequest = {};
    returnRequest.status = client.executeMethod(get);
    returnRequest.responseBody = get.getResponseBodyAsString();

    returnRequest.json = eval("(" + returnRequest.responseBody + ")");
    get.releaseConnection();

    return returnRequest;
}

// returns  returnRequest.status
//          returnRequest.responseBody
//          returnRequest.json
function runAPIRequestPOST(RegistryID, FormID, APIString, data, contenttype, newUUID) {

    if (typeof contenttype == "undefined") { contenttype = "application/x-www-form-urlencoded; charset=utf-8"; }

    var post = new org.apache.commons.httpclient.methods.PostMethod(APIString);
    var client = new org.apache.commons.httpclient.HttpClient();

    var creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(synergyUser, synergyPass);
    client.getParams().setAuthenticationPreemptive(true);
    client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);

    post.setRequestHeader("Content-type", contenttype);
    post.setRequestHeader("formUUID", FormID);
    post.addParameter("formUUID", FormID);

    if (typeof newUUID == "undefined") {
        post.setRequestHeader("uuid", RegistryID);
        post.setRequestBody( data );
    } else {
        post.addParameter("uuid", newUUID);
        post.setRequestBody( "data=" + data );
    }

    var returnRequest = {};
    returnRequest.status = client.executeMethod(post);
    returnRequest.responseBody = post.getResponseBodyAsString();

    returnRequest.json = eval("(" + returnRequest.responseBody + ")");
    post.releaseConnection();

    return returnRequest;
}

// функция поиска в массиве
// searchJSON(json, string, returnKey)
function searchJSON(json, string, returnKey, returnData) {
    var ret;
    if (typeof returnKey == "undefined") { returnKey=false ;}
    if (typeof returnData == "undefined") { returnData=false ;}
    if (typeof json == "undefined") { ret = 0; }
    else {
        for (var i = 0; i < json.length; i++) {
            if ( json[i].id == string ) {
                if (json[i].value === null) { json[i].value = ""; }
                if (returnKey) {
                    ret = json[i].key;
                } else if (returnData) {
                    ret = json[i].data;
                } else {
                    ret = json[i].value;
                }
                break;
            }
        }
    }
    return escape(ret);
}

function getCompanyes() {
  var ret = {};
  var api = synergyURL + "/rest/api/asforms/search?formUUID=" + Registries.CompanySettings.formid + "&search=" + Url.encode("ДА") + "&field=necessity&type=exact&getDocIds=true";
  var searchCompany = runAPIRequestGET(Registries.CompanySettings.uuid, Registries.CompanySettings.formid, api, "text/plain");
  for (var i = 0; i < searchCompany.json.length; i++) {
    var tmp = runAPIRequestGET(Registries.CompanySettings.uuid, Registries.CompanySettings.formid, synergyURL + "/rest/api/asforms/data/get?dataUUID=" + searchCompany.json[i].dataUUID);
    ret[i] = { 'company': unescape(searchJSON(tmp.json[0].data, "settings_name")), dataUUID: searchCompany.json[i].dataUUID, documentID: searchCompany.json[i].documentID };
  }
  return ret;
}

function getTotalRespondents(company) {
  var yearSearch = new Date().getFullYear();
  var api = synergyURL + "/rest/api/asforms/search/advanced"; //?formUUID=" + Registries.Anketa.formid + "&uuid=" + Registries.Anketa.uuid;
  var totalRespondents = 0;

  for (var i = 0; i < 3; i++) {
    // var data = '{ "query": "where uuid = \'' + Registries.Anketa.formid + '\' and YEAR(`date.DATE`) = ' + yearSearch + ' and `company.TEXT` like \'%' + company + '%\' and (`frustrated_point.TEXT` like \'1%\' or `frustrated_point.TEXT` like \'2%\' or `frustrated_point.TEXT` like \'3%\' or `frustrated_point.TEXT` like \'4%\')", "parameters": [ "company.TEXT", "frustrated_point.TEXT", "date.DATE" ], "showDeleted": "false", "searchInRegistry": "true", "registryRecordStatus": ["STATE_SUCCESSFUL"] }';
    var data = '{ "query": "where uuid = \'' + Registries.Anketa.formid + '\' and YEAR(`date.DATE`) = ' + yearSearch + ' and `company.TEXT` like \'%' + company + '%\' ", "parameters": [ "company.TEXT", "date.DATE" ], "showDeleted": "false", "searchInRegistry": "true", "registryRecordStatus": ["STATE_SUCCESSFUL"] }';
    totalRespondents = runAPIRequestPOST( Registries.Anketa.uuid, Registries.Anketa.formid, api, data, "application/json; charset=utf-8" ).json;
    if (totalRespondents.length > 0) {
      totalRespondents = totalRespondents.length;
      break;
    } else {
      yearSearch = yearSearch - 1;
    }
  }
  return Number(totalRespondents); // return 100;
}


// !!! МОЖНО УДАЛИТЬ ПОСЛЕ НАПИСАНИЯ ВСЕХ ФУНКЦИЙ ПЕРЕСЧЕТА
//for (var i = 0; i < CH_codes.length; i++) { eval("function calc" + CH_codes[i].code_local + "(company, totalRespondents) { return 0; }"); }

var calcPe1 = function(company, totalRespondents) {
  var yearSearch = new Date().getFullYear();
  var ret = 0;
  var api = synergyURL + "/rest/api/asforms/search/advanced"; //?formUUID=" + Registries.Anketa.formid + "&uuid=" + Registries.Anketa.uuid;

  for (var i = 0; i < 3; i++) {
    var data = '{ "query": "where uuid = \'' + Registries.Anketa.formid + '\' and YEAR(`date.DATE`) = ' + yearSearch + ' and `company.TEXT` like \'%' + company + '%\' and (`frustrated_point.TEXT` like \'1%\') ", "parameters": [ "company.TEXT", "frustrated_point.TEXT", "date.DATE" ], "showDeleted": "false", "searchInRegistry": "true", "registryRecordStatus": ["STATE_SUCCESSFUL"] }';
    ret = runAPIRequestPOST( Registries.Anketa.uuid, Registries.Anketa.formid, api, data, "application/json; charset=utf-8" );
    if (ret.json.length > 0) {
      break;
    } else {
      yearSearch = yearSearch - 1;
    }
  }

  ret = ret.json.length / totalRespondents;
  return Number(ret);//.toFixed(3);
};

var calcPe2 = function(company, totalRespondents) {
  var yearSearch = new Date().getFullYear();
  var ret = 0;
  var api = synergyURL + "/rest/api/asforms/search/advanced"; //?formUUID=" + Registries.Anketa.formid + "&uuid=" + Registries.Anketa.uuid;

  for (var i = 0; i < 3; i++) {
    var data = '{ "query": "where uuid = \'' + Registries.Anketa.formid + '\' and YEAR(`date.DATE`) = ' + yearSearch + ' and `company.TEXT` like \'%' + company + '%\' and (`frustrated_point.TEXT` like \'2%\')", "parameters": [ "company.TEXT", "frustrated_point.TEXT", "date.DATE" ], "showDeleted": "false", "searchInRegistry": "true", "registryRecordStatus": ["STATE_SUCCESSFUL"] }';
    ret = runAPIRequestPOST( Registries.Anketa.uuid, Registries.Anketa.formid, api, data, "application/json; charset=utf-8" );
    if (ret.json.length > 0) {
      break;
    } else {
      yearSearch = yearSearch - 1;
    }
  }

  ret = ret.json.length / totalRespondents;
  return Number(ret);//.toFixed(3);
};

var calcPe3 = function(company, totalRespondents) {
  var yearSearch = new Date().getFullYear();
  var ret = 0;
  var api = synergyURL + "/rest/api/asforms/search/advanced"; //?formUUID=" + Registries.Anketa.formid + "&uuid=" + Registries.Anketa.uuid;

  for (var i = 0; i < 3; i++) {
    var data = '{ "query": "where uuid = \'' + Registries.Anketa.formid + '\' and YEAR(`date.DATE`) = ' + yearSearch + ' and `company.TEXT` like \'%' + company + '%\' and (`frustrated_point.TEXT` like \'3%\' or `frustrated_point.TEXT` like \'4%\')", "parameters": [ "company.TEXT", "frustrated_point.TEXT", "date.DATE" ], "showDeleted": "false", "searchInRegistry": "true", "registryRecordStatus": ["STATE_SUCCESSFUL"] }';
    ret = runAPIRequestPOST( Registries.Anketa.uuid, Registries.Anketa.formid, api, data, "application/json; charset=utf-8" );
    if (ret.json.length > 0) {
      break;
    } else {
      yearSearch = yearSearch - 1;
    }
  }

  ret = ret.json.length / totalRespondents;
  return Number(ret);//.toFixed(3);
};

var calcPakt = function(company, totalRespondents) {
  var yearSearch = new Date().getFullYear();
  var ret = 0;
  var api = synergyURL + "/rest/api/asforms/search/advanced"; //?formUUID=" + Registries.Anketa.formid + "&uuid=" + Registries.Anketa.uuid;
  var tmp = "а";

  for (var i = 0; i < 3; i++) {
    var data = '{ "query": "where uuid = \'' + Registries.Anketa.formid + '\' and YEAR(`date.DATE`) = ' + yearSearch + ' and `company.TEXT` like \'%' + company + '%\' and (`conflict_otvet1.TEXT` like \'' + tmp + '%\' or `conflict_otvet2.TEXT` like \'' + tmp + '%\' or `conflict_otvet3.TEXT` like \'' + tmp + '%\' or `conflict_otvet4.TEXT` like \'' + tmp + '%\' or `conflict_otvet5.TEXT` like \'' + tmp + '%\' or `conflict_otvet6.TEXT` like \'' + tmp + '%\')", "parameters": [ "company.TEXT", "conflict_otvet1.TEXT", "conflict_otvet2.TEXT", "conflict_otvet3.TEXT", "conflict_otvet4.TEXT", "conflict_otvet5.TEXT", "conflict_otvet6.TEXT", "date.DATE" ], "showDeleted": "false", "searchInRegistry": "true", "registryRecordStatus": ["STATE_SUCCESSFUL"] }';

    ret = runAPIRequestPOST( Registries.Anketa.uuid, Registries.Anketa.formid, api, data, "application/json; charset=utf-8" );
    if (ret.json.length > 0) {
      break;
    } else {
      yearSearch = yearSearch - 1;
    }
  }

  if (totalRespondents === 0) {
    ret = 0;
  } else {
    ret = ret.json.length / totalRespondents;
  }
  // if (ret > 1) ret = 1;

  return Number(ret);//.toFixed(3);
};

var calcРind = function(company, totalRespondents) {
  var yearSearch = new Date().getFullYear();
  var ret = 0;
  var api = synergyURL + "/rest/api/asforms/search/advanced"; //?formUUID=" + Registries.Anketa.formid + "&uuid=" + Registries.Anketa.uuid;
  var tmp = "е";

  for (var i = 0; i < 3; i++) {
    var data = '{ "query": "where uuid = \'' + Registries.Anketa.formid + '\' and YEAR(`date.DATE`) = ' + yearSearch + ' and `company.TEXT` like \'%' + company + '%\' and (`conflict_otvet1.TEXT` like \'' + tmp + '%\' or `conflict_otvet2.TEXT` like \'' + tmp + '%\' or `conflict_otvet3.TEXT` like \'' + tmp + '%\' or `conflict_otvet4.TEXT` like \'' + tmp + '%\' or `conflict_otvet5.TEXT` like \'' + tmp + '%\' or `conflict_otvet6.TEXT` like \'' + tmp + '%\')", "parameters": [ "company.TEXT", "conflict_otvet1.TEXT", "conflict_otvet2.TEXT", "conflict_otvet3.TEXT", "conflict_otvet4.TEXT", "conflict_otvet5.TEXT", "conflict_otvet6.TEXT", "date.DATE" ], "showDeleted": "false", "searchInRegistry": "true", "registryRecordStatus": ["STATE_SUCCESSFUL"] }';

    ret = runAPIRequestPOST( Registries.Anketa.uuid, Registries.Anketa.formid, api, data, "application/json; charset=utf-8" );
    if (ret.json.length > 0) {
      break;
    } else {
      yearSearch = yearSearch - 1;
    }
  }

  if (totalRespondents === 0) {
    ret = 0;
  } else {
    ret = ret.json.length / totalRespondents;
  }
  if (ret > 1) ret = 1;

  return Number(ret);//.toFixed(3);
};

var calcPcrim = function(company, totalRespondents) {
  var yearSearch = new Date().getFullYear();
  var api = synergyURL + "/rest/api/asforms/search/advanced"; //?formUUID=" + Registries.Anketa.formid + "&uuid=" + Registries.Anketa.uuid;
  var tmp = "б";
  var col = 2;
  var col_act = 0;

  for (var i = 0; i < 3; i++) {
    var data = '{ "query": "where uuid = \'' + Registries.Anketa.formid + '\' and YEAR(`date.DATE`) = ' + yearSearch + ' and `company.TEXT` like \'%' + company + '%\' and (`conflict_otvet1.TEXT` like \'' + tmp + '%\' or `conflict_otvet2.TEXT` like \'' + tmp + '%\' or `conflict_otvet3.TEXT` like \'' + tmp + '%\' or `conflict_otvet4.TEXT` like \'' + tmp + '%\' or `conflict_otvet5.TEXT` like \'' + tmp + '%\' or `conflict_otvet6.TEXT` like \'' + tmp + '%\')", "parameters": [ "company.TEXT", "conflict_otvet1.TEXT", "conflict_otvet2.TEXT", "conflict_otvet3.TEXT", "conflict_otvet4.TEXT", "conflict_otvet5.TEXT", "conflict_otvet6.TEXT", "date.DATE" ], "showDeleted": "false", "searchInRegistry": "true", "registryRecordStatus": ["STATE_SUCCESSFUL"] }';
    var total = runAPIRequestPOST( Registries.Anketa.uuid, Registries.Anketa.formid, api, data, "application/json; charset=utf-8" ).json;

    if (total.length > 0) {
      for (var it = 0; it < total.length; it++) {
        var tmp2 = runAPIRequestGET(Registries.Anketa.uuid, Registries.Anketa.formid, synergyURL + "/rest/api/asforms/data/get?dataUUID=" + total[it].dataUUID).json[0];
        var tmp_col = 0;
        for (var ico = 1; ico <= 6; ico++) {
          if ( unescape(searchJSON(tmp2.data, "conflict_otvet"+ico)).substring(0, 1) == tmp) tmp_col++;
        }
        if ( Number(tmp_col) >= Number(col) ) col_act++;
      }
      break;
    } else {
      yearSearch = yearSearch - 1;
    }

  }

  var ret = 0;
  if (totalRespondents === 0) {
    ret = 0;
  } else {
    ret = Number(col_act) / totalRespondents;
  }
  if (ret > 1) ret = 1;

  return ret;//.toFixed(3);
};

var calcРout = function(company, totalRespondents) {
  var yearSearch = new Date().getFullYear();
  var api = synergyURL + "/rest/api/asforms/search/advanced"; //?formUUID=" + Registries.Anketa.formid + "&uuid=" + Registries.Anketa.uuid;
  var tmp = "в";
  var col = 2;
  var col_act = 0;

  for (var i = 0; i < 3; i++) {
    var data = '{ "query": "where uuid = \'' + Registries.Anketa.formid + '\' and YEAR(`date.DATE`) = ' + yearSearch + ' and `company.TEXT` like \'%' + company + '%\' and (`conflict_otvet1.TEXT` like \'' + tmp + '%\' or `conflict_otvet2.TEXT` like \'' + tmp + '%\' or `conflict_otvet3.TEXT` like \'' + tmp + '%\' or `conflict_otvet4.TEXT` like \'' + tmp + '%\' or `conflict_otvet5.TEXT` like \'' + tmp + '%\' or `conflict_otvet6.TEXT` like \'' + tmp + '%\')", "parameters": [ "company.TEXT", "conflict_otvet1.TEXT", "conflict_otvet2.TEXT", "conflict_otvet3.TEXT", "conflict_otvet4.TEXT", "conflict_otvet5.TEXT", "conflict_otvet6.TEXT", "date.DATE" ], "showDeleted": "false", "searchInRegistry": "true", "registryRecordStatus": ["STATE_SUCCESSFUL"] }';
    var total = runAPIRequestPOST( Registries.Anketa.uuid, Registries.Anketa.formid, api, data, "application/json; charset=utf-8" ).json;

    if (total.length > 0) {
      for (var it = 0; it < total.length; it++) {
        var tmp2 = runAPIRequestGET(Registries.Anketa.uuid, Registries.Anketa.formid, synergyURL + "/rest/api/asforms/data/get?dataUUID=" + total[it].dataUUID).json[0];
        var tmp_col = 0;
        for (var ico = 1; ico <= 6; ico++) {
          if ( unescape(searchJSON(tmp2.data, "conflict_otvet"+ico)).substring(0, 1) == tmp) tmp_col++;
        }
        if ( Number(tmp_col) >= Number(col) ) col_act++;
      }
      break;
    } else {
      yearSearch = yearSearch - 1;
    }

  }

  var ret = 0;
  if (totalRespondents === 0) {
    ret = 0;
  } else {
    ret = Number(col_act) / totalRespondents;
  }
  if (ret > 1) ret = 1;

  return ret;//.toFixed(3);
};

var calcРmax = function(company, totalRespondents) {
  var yearSearch = new Date().getFullYear();
  var api = synergyURL + "/rest/api/asforms/search/advanced"; //?formUUID=" + Registries.Anketa.formid + "&uuid=" + Registries.Anketa.uuid;
  var tmp = "г";
  var col = 4;
  var col_act = 0;

  for (var i = 0; i < 3; i++) {
    var data = '{ "query": "where uuid = \'' + Registries.Anketa.formid + '\' and YEAR(`date.DATE`) = ' + yearSearch + ' and `company.TEXT` like \'%' + company + '%\' and (`conflict_otvet1.TEXT` like \'' + tmp + '%\' or `conflict_otvet2.TEXT` like \'' + tmp + '%\' or `conflict_otvet3.TEXT` like \'' + tmp + '%\' or `conflict_otvet4.TEXT` like \'' + tmp + '%\' or `conflict_otvet5.TEXT` like \'' + tmp + '%\' or `conflict_otvet6.TEXT` like \'' + tmp + '%\')", "parameters": [ "company.TEXT", "conflict_otvet1.TEXT", "conflict_otvet2.TEXT", "conflict_otvet3.TEXT", "conflict_otvet4.TEXT", "conflict_otvet5.TEXT", "conflict_otvet6.TEXT", "date.DATE" ], "showDeleted": "false", "searchInRegistry": "true", "registryRecordStatus": ["STATE_SUCCESSFUL"] }';
    var total = runAPIRequestPOST( Registries.Anketa.uuid, Registries.Anketa.formid, api, data, "application/json; charset=utf-8" ).json;

    if (total.length > 0) {
      for (var it = 0; it < total.length; it++) {
        var tmp2 = runAPIRequestGET(Registries.Anketa.uuid, Registries.Anketa.formid, synergyURL + "/rest/api/asforms/data/get?dataUUID=" + total[it].dataUUID).json[0];
        var tmp_col = 0;
        for (var ico = 1; ico <= 6; ico++) {
          if ( unescape(searchJSON(tmp2.data, "conflict_otvet"+ico)).substring(0, 1) == tmp) tmp_col++;
        }
        if ( Number(tmp_col) >= Number(col) ) col_act++;
      }
      break;
    } else {
      yearSearch = yearSearch - 1;
    }

  }

  var ret = 0;
  if (totalRespondents === 0) {
    ret = 0;
  } else {
    ret = Number(col_act) / totalRespondents;
  }
  if (ret > 1) ret = 1;

  return ret;//.toFixed(3);
};

var calcР0 = function(company, totalRespondents) {
  var yearSearch = new Date().getFullYear();
  var api = synergyURL + "/rest/api/asforms/search/advanced"; //?formUUID=" + Registries.Anketa.formid + "&uuid=" + Registries.Anketa.uuid;
  var tmp = "ж";
  var col = 4;
  var col_act = 0;

  for (var i = 0; i < 3; i++) {
    var data = '{ "query": "where uuid = \'' + Registries.Anketa.formid + '\' and YEAR(`date.DATE`) = ' + yearSearch + ' and `company.TEXT` like \'%' + company + '%\' and (`conflict_otvet1.TEXT` like \'' + tmp + '%\' or `conflict_otvet2.TEXT` like \'' + tmp + '%\' or `conflict_otvet3.TEXT` like \'' + tmp + '%\' or `conflict_otvet4.TEXT` like \'' + tmp + '%\' or `conflict_otvet5.TEXT` like \'' + tmp + '%\' or `conflict_otvet6.TEXT` like \'' + tmp + '%\')", "parameters": [ "company.TEXT", "conflict_otvet1.TEXT", "conflict_otvet2.TEXT", "conflict_otvet3.TEXT", "conflict_otvet4.TEXT", "conflict_otvet5.TEXT", "conflict_otvet6.TEXT", "date.DATE" ], "showDeleted": "false", "searchInRegistry": "true", "registryRecordStatus": ["STATE_SUCCESSFUL"] }';
    var total = runAPIRequestPOST( Registries.Anketa.uuid, Registries.Anketa.formid, api, data, "application/json; charset=utf-8" ).json;

    if (total.length > 0) {
      for (var it = 0; it < total.length; it++) {
        var tmp2 = runAPIRequestGET(Registries.Anketa.uuid, Registries.Anketa.formid, synergyURL + "/rest/api/asforms/data/get?dataUUID=" + total[it].dataUUID).json[0];
        var tmp_col = 0;
        for (var ico = 1; ico <= 6; ico++) {
          if ( unescape(searchJSON(tmp2.data, "conflict_otvet"+ico)).substring(0, 1) == tmp) tmp_col++;
        }
        if ( Number(tmp_col) >= Number(col) ) col_act++;
      }
      break;
    } else {
      yearSearch = yearSearch - 1;
    }

  }

  var ret = 0;
  if (totalRespondents === 0) {
    ret = 0;
  } else {
    ret = Number(col_act) / totalRespondents;
  }
  if (ret > 1) ret = 1;

  return ret;//.toFixed(3);
};

var calcРaag = function(company, totalRespondents) {
  var yearSearch = new Date().getFullYear();
  var api = synergyURL + "/rest/api/asforms/search/advanced"; //?formUUID=" + Registries.Anketa.formid + "&uuid=" + Registries.Anketa.uuid;
  var tmp = "д";
  var col = 4;
  var col_act = 0;

  for (var i = 0; i < 3; i++) {
    var data = '{ "query": "where uuid = \'' + Registries.Anketa.formid + '\' and YEAR(`date.DATE`) = ' + yearSearch + ' and `company.TEXT` like \'%' + company + '%\' and (`conflict_otvet1.TEXT` like \'' + tmp + '%\' or `conflict_otvet2.TEXT` like \'' + tmp + '%\' or `conflict_otvet3.TEXT` like \'' + tmp + '%\' or `conflict_otvet4.TEXT` like \'' + tmp + '%\' or `conflict_otvet5.TEXT` like \'' + tmp + '%\' or `conflict_otvet6.TEXT` like \'' + tmp + '%\')", "parameters": [ "company.TEXT", "conflict_otvet1.TEXT", "conflict_otvet2.TEXT", "conflict_otvet3.TEXT", "conflict_otvet4.TEXT", "conflict_otvet5.TEXT", "conflict_otvet6.TEXT", "date.DATE" ], "showDeleted": "false", "searchInRegistry": "true", "registryRecordStatus": ["STATE_SUCCESSFUL"] }';
    var total = runAPIRequestPOST( Registries.Anketa.uuid, Registries.Anketa.formid, api, data, "application/json; charset=utf-8" ).json;

    if (total.length > 0) {
      for (var it = 0; it < total.length; it++) {
        var tmp2 = runAPIRequestGET(Registries.Anketa.uuid, Registries.Anketa.formid, synergyURL + "/rest/api/asforms/data/get?dataUUID=" + total[it].dataUUID).json[0];
        var tmp_col = 0;
        for (var ico = 1; ico <= 6; ico++) {
          if ( unescape(searchJSON(tmp2.data, "conflict_otvet"+ico)).substring(0, 1) == tmp) tmp_col++;
        }
        if ( Number(tmp_col) >= Number(col) ) col_act++;
      }
      break;
    } else {
      yearSearch = yearSearch - 1;
    }

  }

  var ret = 0;
  if (totalRespondents === 0) {
    ret = 0;
  } else {
    ret = Number(col_act) / totalRespondents;
  }
  if (ret > 1) ret = 1;

  return ret;//.toFixed(3);
};

var calcPconnect = function(company, totalRespondents) {
  var yearSearch = new Date().getFullYear();
  var api = synergyURL + "/rest/api/asforms/search/advanced"; //?formUUID=" + Registries.Anketa.formid + "&uuid=" + Registries.Anketa.uuid;
  var ret = 0;

  for (var i = 0; i < 3; i++) {
    var data = '{ "query": "where uuid = \'' + Registries.Anketa.formid + '\' and YEAR(`date.DATE`) = ' + yearSearch + ' and `company.TEXT` like \'%' + company + '%\' and ( (`connect_otvet1.TEXT` like \'б%\' or `connect_otvet1.TEXT` like \'в%\' or `connect_otvet1.TEXT` like \'г%\') and ( `connect_otvet2.TEXT` like \'б%\' or `connect_otvet2.TEXT` like \'в%\' ) )", "parameters": [ "company.TEXT", "connect_otvet1.TEXT", "connect_otvet2.TEXT", "date.DATE" ], "showDeleted": "false", "searchInRegistry": "true", "registryRecordStatus": ["STATE_SUCCESSFUL"] }';
    ret = runAPIRequestPOST( Registries.Anketa.uuid, Registries.Anketa.formid, api, data, "application/json; charset=utf-8" );
    if (ret.json.length > 0) {
      break;
    } else {
      yearSearch = yearSearch - 1;
    }
  }

  ret = ret.json.length / totalRespondents;

  return Number(ret);//.toFixed(3);
};

var calcX = function(company, totalRespondents) {
  var yearSearch = new Date().getFullYear();
  var api = synergyURL + "/rest/api/asforms/search/advanced"; //?formUUID=" + Registries.Anketa.formid + "&uuid=" + Registries.Anketa.uuid;
  var rang = 0;
  var rangsX = [
    [0, 0],
    [2, 2], // а
    [3, 3], // б
    [4, 4], // в
    [2, 0], // г
    [1, 0]  // д
  ];

  for (var i = 0; i < 3; i++) {
    var data = '{ "query": "where uuid = \'' + Registries.Anketa.formid + '\' and YEAR(`date.DATE`) = ' + yearSearch + ' and `company.TEXT` like \'%' + company + '%\' and (`frustrated_point.TEXT` like \'1%\' or `frustrated_point.TEXT` like \'2%\' or `frustrated_point.TEXT` like \'3%\' or `frustrated_point.TEXT` like \'4%\')", "parameters": [ "company.TEXT", "frustrated_point.TEXT", "date.DATE" ], "showDeleted": "false", "searchInRegistry": "true", "registryRecordStatus": ["STATE_SUCCESSFUL"] }';
    var total = runAPIRequestPOST( Registries.Anketa.uuid, Registries.Anketa.formid, api, data, "application/json; charset=utf-8" ).json;

    if (total.length > 0) {
      for (var it = 0; it < total.length; it++) {
        var tmp2 = runAPIRequestGET(Registries.Anketa.uuid, Registries.Anketa.formid, synergyURL + "/rest/api/asforms/data/get?dataUUID=" + total[it].dataUUID).json[0];

        if      (unescape(searchJSON(tmp2.data, "connect_otvet1", true)) == "1") { eval("rang = rang + rangsX[" + unescape(searchJSON(tmp2.data, "connect_otvet1", true)) + "][0]"); }
        else if (unescape(searchJSON(tmp2.data, "connect_otvet1", true)) == "2") { eval("rang = rang + rangsX[" + unescape(searchJSON(tmp2.data, "connect_otvet1", true)) + "][0]"); }
        else if (unescape(searchJSON(tmp2.data, "connect_otvet1", true)) == "3") { eval("rang = rang + rangsX[" + unescape(searchJSON(tmp2.data, "connect_otvet1", true)) + "][0]"); }
        else if (unescape(searchJSON(tmp2.data, "connect_otvet1", true)) == "4") { eval("rang = rang + rangsX[" + unescape(searchJSON(tmp2.data, "connect_otvet1", true)) + "][0]"); }
        else if (unescape(searchJSON(tmp2.data, "connect_otvet1", true)) == "5") { eval("rang = rang + rangsX[" + unescape(searchJSON(tmp2.data, "connect_otvet1", true)) + "][0]"); }

        if      (unescape(searchJSON(tmp2.data, "connect_otvet2", true)) == "1") { eval("rang = rang + rangsX[" + unescape(searchJSON(tmp2.data, "connect_otvet2", true)) + "][1]"); }
        else if (unescape(searchJSON(tmp2.data, "connect_otvet2", true)) == "2") { eval("rang = rang + rangsX[" + unescape(searchJSON(tmp2.data, "connect_otvet2", true)) + "][1]"); }
        else if (unescape(searchJSON(tmp2.data, "connect_otvet2", true)) == "3") { eval("rang = rang + rangsX[" + unescape(searchJSON(tmp2.data, "connect_otvet2", true)) + "][1]"); }
        else if (unescape(searchJSON(tmp2.data, "connect_otvet2", true)) == "4") { eval("rang = rang + rangsX[" + unescape(searchJSON(tmp2.data, "connect_otvet2", true)) + "][1]"); }
        else if (unescape(searchJSON(tmp2.data, "connect_otvet2", true)) == "5") { eval("rang = rang + rangsX[" + unescape(searchJSON(tmp2.data, "connect_otvet2", true)) + "][1]"); }
      }
      break;
    } else {
      yearSearch = yearSearch - 1;
    }

  }

  return rang / 2 / 4 / totalRespondents;
};


var calcZ = function(company, totalRespondents) {
  var yearSearch = new Date().getFullYear();
  var api = synergyURL + "/rest/api/asforms/search/advanced"; //?formUUID=" + Registries.Anketa.formid + "&uuid=" + Registries.Anketa.uuid;
  var rangsZ = [4, 3, 2, 1, 0];
  var ball = 0;

  for (var i = 0; i < 3; i++) {
    var data = '{ "query": "where uuid = \'' + Registries.Anketa.formid + '\' and YEAR(`date.DATE`) = ' + yearSearch + ' and `company.TEXT` like \'%' + company + '%\' and (`frustrated_point.TEXT` like \'1%\' or `frustrated_point.TEXT` like \'2%\' or `frustrated_point.TEXT` like \'3%\' or `frustrated_point.TEXT` like \'4%\')", "parameters": [ "company.TEXT", "frustrated_point.TEXT", "date.DATE" ], "showDeleted": "false", "searchInRegistry": "true", "registryRecordStatus": ["STATE_SUCCESSFUL"] }';
    var total = runAPIRequestPOST( Registries.Anketa.uuid, Registries.Anketa.formid, api, data, "application/json; charset=utf-8" ).json;

    if (total.length > 0) {
      for (var it = 0; it < total.length; it++) {
        var tmp2 = runAPIRequestGET(Registries.Anketa.uuid, Registries.Anketa.formid, synergyURL + "/rest/api/asforms/data/get?dataUUID=" + total[it].dataUUID).json[0];
        for (var icz = 1; icz <= 12; icz++) {
          if (searchJSON(tmp2.data, "problem_otvet"+icz, true) == "0") { eval("ball = ball + rangsZ[" + searchJSON(tmp2.data, "problem_otvet"+icz, true) + "]");}
          if (searchJSON(tmp2.data, "problem_otvet"+icz, true) == "1") { eval("ball = ball + rangsZ[" + searchJSON(tmp2.data, "problem_otvet"+icz, true) + "]");}
          if (searchJSON(tmp2.data, "problem_otvet"+icz, true) == "2") { eval("ball = ball + rangsZ[" + searchJSON(tmp2.data, "problem_otvet"+icz, true) + "]");}
          if (searchJSON(tmp2.data, "problem_otvet"+icz, true) == "3") { eval("ball = ball + rangsZ[" + searchJSON(tmp2.data, "problem_otvet"+icz, true) + "]");}
          if (searchJSON(tmp2.data, "problem_otvet"+icz, true) == "4") { eval("ball = ball + rangsZ[" + searchJSON(tmp2.data, "problem_otvet"+icz, true) + "]");}
        }
      }
      break;
    } else {
      yearSearch = yearSearch - 1;
    }

  }

  return ball / 4 / 12 / totalRespondents ; //( Number(col_act) / totalRespondents );//.toFixed(3);
};

var calcPsat = function(company, totalRespondents) {
  var api = synergyURL + "/rest/api/asforms/search/advanced"; //?formUUID=" + Registries.Anketa.formid + "&uuid=" + Registries.Anketa.uuid;
  var ret = 0;
  var Psat = 0;

  for (var ip = 1; ip <= 12; ip++) {
    var yearSearch = new Date().getFullYear();

    for (var i = 0; i < 3; i++) {
      var data = '{ "query": "where uuid = \'' + Registries.Anketa.formid + '\' and YEAR(`date.DATE`) = ' + yearSearch + ' and `company.TEXT` like \'%' + company + '%\' and (`problem_otvet' + ip + '.INT` like \'0\' and `block_importance' + ip + '.INT` like \'1\')", "parameters": [ "company.TEXT", "problem_otvet' + ip + '.INT", "block_importance' + ip + '.INT", "date.DATE" ], "showDeleted": "false", "searchInRegistry": "true", "registryRecordStatus": ["STATE_SUCCESSFUL"] }';
      var blockTotal = runAPIRequestPOST( Registries.Anketa.uuid, Registries.Anketa.formid, api, data, "application/json; charset=utf-8" ).json;
      if (blockTotal.length > 0) {
        if ( (blockTotal.length / totalRespondents * 100) > 10 ) ret++;
        break;
      } else {
        yearSearch = yearSearch - 1;
      }

      //log.setLog("blockTotal" + ip + " = " + blockTotal.length + " / " + totalRespondents + " = " + (blockTotal.length / totalRespondents * 100) + "%");
    }

  }

  return ret / 12;
};

// uses reportDSTPeriod var
// uses currentVSP_coef.parameter_M
var calcDst = function(company, totalRespondents) {
  var ret = 0;
  var currentMonth = new Date().getMonth() + 1;
  var api = synergyURL + "/rest/api/asforms/search/advanced";

  var data = '{ "query": "where uuid = \'' + Registries.OUTD.formid + '\' and MONTH(`date.DATE`) = ' + currentMonth + ' and `company.TEXT` = \'' + company + '\'", "parameters": [ "company.TEXT", "date.DATE" ], "showDeleted": "false", "searchInRegistry": "true" }';
  var outd_all = runAPIRequestPOST(Registries.OUTD.uuid, Registries.OUTD.formid, api, data, "application/json; charset=utf-8" ).json;
  var tmp_allcount = 0;

  for (var io = 0; io < outd_all.length; io++) {
    api = synergyURL + "/rest/api/asforms/data/get?dataUUID=" + outd_all[io].dataUUID;
    var tmp_ret = runAPIRequestGET( Registries.OUTD.uuid, Registries.OUTD.formid, api).json[0];
    var tmp_count = Number( searchJSON(tmp_ret.data, "count") );
    if ( typeof tmp_count == "undefined" || isNaN(tmp_count)) tmp_count = 0;
    tmp_allcount = Number(tmp_allcount) + Number(tmp_count);
  }

  if ( tmp_allcount === 0 ) {
    ret = 0;
  } else if (tmp_allcount > 0 && tmp_allcount <= (Number(currentVSP_coef.parameter_M)/2) ) {
    ret = 0.25;
  } else if (tmp_allcount > (Number(currentVSP_coef.parameter_M)/2) && tmp_allcount < Number(currentVSP_coef.parameter_M)) {
    ret = 0.5;
  } else if (tmp_allcount >= Number(currentVSP_coef.parameter_M) && tmp_allcount <= (Number(currentVSP_coef.parameter_M)*1.15) ) {
    ret = 0.75;
  } else if (tmp_allcount > Number(currentVSP_coef.parameter_M)*1.15) {
    ret = 1;
  } else {
    ret = 0;
  }

  return ret;
};

// uses reportQSTPeriod var
// uses currentVSP_coef.parameter_N
var calcQst = function(company, totalRespondents) {
  var startDate = parseDate(new Date(), reportQSTPeriod);
  var stopDate = parseDate(new Date());
  var ret = 0;
  var api = synergyURL + "/rest/api/asforms/search/advanced";

  var data = '{ "query": "where uuid = \'' + Registries.OPPP.formid + '\' and DATE(`date.DATE`) BETWEEN DATE(\'' + startDate + '\') and DATE(\'' + stopDate + '\') and `company.TEXT` = \'' + company + '\'", "parameters": [ "company.TEXT", "date.DATE" ], "showDeleted": "false", "searchInRegistry": "true" }';
  var outd_all = runAPIRequestPOST(Registries.OPPP.uuid, Registries.OPPP.formid, api, data, "application/json; charset=utf-8" ).json;
  var tmp_allcount = 0;

  for (var io = 0; io < outd_all.length; io++) {
    api = synergyURL + "/rest/api/asforms/data/get?dataUUID=" + outd_all[io].dataUUID;
    var tmp_ret = runAPIRequestGET( Registries.OPPP.uuid, Registries.OPPP.formid, api).json[0];
    var tmp_count = Number( searchJSON(tmp_ret.data, "count") );
    if ( typeof tmp_count == "undefined" || isNaN(tmp_count)) tmp_count = 0;
    tmp_allcount = Number(tmp_allcount) + Number(tmp_count);
  }

  if ( tmp_allcount === 0 ) {
    ret = 0;
  } else if (tmp_allcount > 0 && tmp_allcount <= (Number(currentVSP_coef.parameter_N)/2) ) {
    ret = 0.25;
  } else if (tmp_allcount > (Number(currentVSP_coef.parameter_N)/2) && tmp_allcount < Number(currentVSP_coef.parameter_N)) {
    ret = 0.5;
  } else if (tmp_allcount >= Number(currentVSP_coef.parameter_N) && tmp_allcount <= (Number(currentVSP_coef.parameter_N)*1.15) ) {
    ret = 0.75;
  } else if (tmp_allcount > Number(currentVSP_coef.parameter_N)*1.15) {
    ret = 1;
  } else {
    ret = 0;
  }

  return ret;
};

var calcPq = function(company, totalRespondents) {
    var ret = 1;

    //settings_pmRegistryID
    //settings_pmFormID

    var api = synergyURL + "/rest/api/asforms/search?formUUID=" + Registries.CompanySettings.formid + "&search=" + Url.encode(company) + "&field=settings_name&type=exact&getDocIds=true";
    var searchCompany = runAPIRequestGET(Registries.CompanySettings.uuid, Registries.CompanySettings.formid, api, "text/plain");
    var tmp = runAPIRequestGET(Registries.CompanySettings.uuid, Registries.CompanySettings.formid, synergyURL + "/rest/api/asforms/data/get?dataUUID=" + searchCompany.json[0].dataUUID);
    var settings_pmRegistryID = unescape(searchJSON(tmp.json[0].data, "settings_pmRegistryID"));
    var settings_pmFormID = unescape(searchJSON(tmp.json[0].data, "settings_pmFormID"));

    //  log.setLog("settings_pmRegistryID = " + settings_pmRegistryID);
    //  log.setLog("settings_pmFormID = " + settings_pmFormID);

    api = synergyURL + "/rest/api/asforms/search?formUUID=" + settings_pmFormID + "&search=" + Url.encode(company) + "&field=company&type=exact&getDocIds=true";
    var lastPM = runAPIRequestGET(settings_pmRegistryID, settings_pmFormID, api, "text/plain");
    tmp = runAPIRequestGET(settings_pmRegistryID, settings_pmFormID, synergyURL + "/rest/api/asforms/data/get?dataUUID=" + lastPM.json[0].dataUUID);
    var active_count = unescape(searchJSON(tmp.json[0].data, "count"));
    if (typeof active_count == "undefined" || isNaN(active_count)) active_count = 0;
    //log.setLog("count = " + active_count);
    //30b6328e-9848-4d3f-84e2-b5ed99964b3c

    var PM_data = tmp.json[0].data;
    var appeal = {};
    for (var pmI = 0; pmI < PM_data.length; pmI++) {
        if ( PM_data[pmI].id == "appeal" ) {
            appeal = PM_data[pmI].data;
        }
    }

    var appealUnresolved = 0;
    for (var pmJ = 0; pmJ < appeal.length; pmJ++) {
        if ( appeal[pmJ].id.indexOf("control-b") != -1 ) {
            //log.setLog("id = " + appeal[pmJ].id + ", value = " + appeal[pmJ].value + ", key = " + appeal[pmJ].key);
            if (appeal[pmJ].key == "0") { appealUnresolved++; }
        }
    }
    if (active_count === 0) {
        ret = 0;
    } else {
        ret = appealUnresolved / active_count;
    }

    if ( ret > 1) {
        ret = 1;
    }

    if (isNaN(ret)) ret = 0;

    return ret;
};

var calcS = function(company, totalRespondents) {
    // выбираем последний отчет
    // считываем значения (value) вариантов ответа
    // вычисляем среднее арифметическое (суммируем и делим на 3)
    var ret = 0;

    var api = synergyURL + "/rest/api/asforms/search?formUUID=" + Registries.S.formid + "&search=" + Url.encode(company) + "&field=company&type=exact";
    // выбираем все записи по компании
    var tmp_SUUIDS = runAPIRequestGET(Registries.S.uuid, Registries.S.formid, api, "text/plain").json;
    var tmp_sortdates = [];
    var tmp_hashdates = {};
    for (var isuid = 0; isuid < tmp_SUUIDS.length; isuid++) {
        api = synergyURL + "/rest/api/asforms/data/get?dataUUID=" + tmp_SUUIDS[isuid];
        var tmp_obj = runAPIRequestGET(Registries.S.uuid, Registries.S.formid, api, "text/plain").json[0].data;
        var tmp_date = searchJSON( tmp_obj, "date");
        tmp_date = (Number( tmp_date.substring(0,4) )-1)*365 + (Number( tmp_date.substring(5,7) )-1)*30 + Number(tmp_date.substring(8,10));
        tmp_sortdates.push(tmp_date);
        tmp_hashdates[tmp_date] = tmp_obj;
    }
    if ( tmp_SUUIDS.length > 0 ) {
        tmp_sortdates.sort();
        ret = (Number(searchJSON(tmp_hashdates[tmp_sortdates[tmp_sortdates.length-1].toString()], "line1_column1", true)) + Number(searchJSON(tmp_hashdates[tmp_sortdates[tmp_sortdates.length-1].toString()], "line2_column1", true)) + Number(searchJSON(tmp_hashdates[tmp_sortdates[tmp_sortdates.length-1].toString()], "line3_column1", true)))/3;
    }
    return ret;
};

var calcPdes = function(company, totalRespondents) {
    //    выбираю последний отчет
    var ret = 0;

    var api = synergyURL + "/rest/api/asforms/search?formUUID=" + Registries.PDES.formid + "&search=" + Url.encode(company) + "&field=company&type=exact";
    // выбираем все записи по компании
    var tmp_PDESUUIDS = runAPIRequestGET(Registries.PDES.uuid, Registries.PDES.formid, api, "text/plain").json;
    var tmp_sortdates = [];
    var tmp_hashdates = {};
    for (var isuid = 0; isuid < tmp_PDESUUIDS.length; isuid++) {
        api = synergyURL + "/rest/api/asforms/data/get?dataUUID=" + tmp_PDESUUIDS[isuid];
        var tmp_obj = runAPIRequestGET(Registries.PDES.uuid, Registries.PDES.formid, api, "text/plain").json[0].data;
        var tmp_date = searchJSON( tmp_obj, "date");
        tmp_date = (Number( tmp_date.substring(0,4) )-1)*365 + (Number( tmp_date.substring(5,7) )-1)*30 +  Number(tmp_date.substring(8,10));
        tmp_sortdates.push(tmp_date);
        tmp_hashdates[tmp_date] = tmp_obj;
    }
    if ( tmp_PDESUUIDS.length > 0 ) {
        tmp_sortdates.sort();
        //ret = (Number() + Number(searchJSON(tmp_hashdates[tmp_sortdates[tmp_sortdates.length-1].toString()], "line2_column1", true)) + Number(searchJSON(tmp_hashdates[tmp_sortdates[tmp_sortdates.length-1].toString()], "line3_column1", true)))/3;
        //    если есть смертельные по ним считаю
        //    если есть тяжелые травмы ( и нет смертельны) по ним считаю
        //    если нет, то считаю по легким

        // количество Легких = searchJSON(tmp_hashdates[tmp_sortdates[tmp_sortdates.length-1].toString()], "line1_column1")
        // количество тяжелых = searchJSON(tmp_hashdates[tmp_sortdates[tmp_sortdates.length-1].toString()], "line2_column1")
        // количество смертельных = searchJSON(tmp_hashdates[tmp_sortdates[tmp_sortdates.length-1].toString()], "line3_column1")
        var local_coef = 0;
        var local_type = 0;
        var local_sum = 0;
        if ( Number(searchJSON(tmp_hashdates[tmp_sortdates[tmp_sortdates.length-1].toString()], "line3_column1")) > 0 ) {
            // есть смертельные
            local_coef = Number(currentVSP_coef.parameter_T3);
            local_sum = Number(searchJSON(tmp_hashdates[tmp_sortdates[tmp_sortdates.length-1].toString()], "line3_column1"));
        } else if ( Number(searchJSON(tmp_hashdates[tmp_sortdates[tmp_sortdates.length-1].toString()], "line2_column1")) > 0 ) {
            // есть тяжелые
            local_coef = Number(currentVSP_coef.parameter_T2);
            local_sum = Number(searchJSON(tmp_hashdates[tmp_sortdates[tmp_sortdates.length-1].toString()], "line2_column1"));
        } else if ( Number(searchJSON(tmp_hashdates[tmp_sortdates[tmp_sortdates.length-1].toString()], "line1_column1")) > 0 ) {
            // есть легкие
            local_coef = Number(currentVSP_coef.parameter_T1);
            local_sum = Number(searchJSON(tmp_hashdates[tmp_sortdates[tmp_sortdates.length-1].toString()], "line1_column1"));
        } else {
            // все нули
            local_coef = 0;
            local_sum = 0;
        }

        if ( local_sum === 0 ) {
            ret = 0;
        } else if ( local_sum > 0 && local_sum < (local_coef/2) ) {
            ret = 0.25;
        } else if (local_sum >= (local_coef/2) && local_sum < local_coef) {
            ret = 0.5;
        } else if (local_sum >= local_coef && local_sum < (local_coef*1.15) ) {
            ret = 0.75;
        } else if (local_sum >= local_coef*1.15 ) {
            ret = 1;
        }
    }

    return ret;
};


// OPPPr.uuid OPPPr.formid
// CompanySettings.uuid, CompanySettings.formid
var calcPpp = function(company, totalRespondents) {
    var ret = 0;
    var api = synergyURL + "/rest/api/asforms/search?formUUID=" + Registries.CompanySettings.formid + "&search=" + Url.encode(company) + "&field=settings_name&type=exact";
    api = synergyURL + "/rest/api/asforms/data/get?dataUUID=" + runAPIRequestGET(Registries.CompanySettings.uuid, Registries.CompanySettings.formid, api, "text/plain").json[0];
    var totalCount = searchJSON( runAPIRequestGET(Registries.CompanySettings.uuid, Registries.CompanySettings.formid, api, "text/plain").json[0].data, "people");

    var currentMonth = new Date().getMonth() + 1;
    api = synergyURL + "/rest/api/asforms/search/advanced";
    var data = '{ "query": "where uuid = \'' + Registries.OPPPr.formid + '\' and MONTH(`date.DATE`) = ' + currentMonth + ' and `company.TEXT` = \'' + company + '\'", "parameters": [ "company.TEXT", "date.DATE" ], "showDeleted": "false", "searchInRegistry": "true" }';

    var tmp_Ppp = runAPIRequestPOST(Registries.OPPPr.uuid, Registries.OPPPr.formid, api, data, "application/json; charset=utf-8" ).json;
    var Ppp = 0;
    for (var ipp = 0; ipp < tmp_Ppp.length; ipp++) {
        api = synergyURL + "/rest/api/asforms/data/get?dataUUID=" + tmp_Ppp[ipp].dataUUID;
        Ppp = Ppp + Number( searchJSON( runAPIRequestGET(Registries.OPPPr.uuid, Registries.OPPPr.formid, api, "text/plain").json[0].data, "Ppp") );
    }

    if (totalCount === 0) {
        ret = 0;
    } else {
        ret = Number(Ppp) / Number(totalCount);
    }

    if ( ret > 1) {
        ret = 1;
    }

    return ret;
};

var calcPss = function(company, totalRespondents) {
    var ret = 0;
    var api = synergyURL + "/rest/api/asforms/search?formUUID=" + Registries.CompanySettings.formid + "&search=" + Url.encode(company) + "&field=settings_name&type=exact";
    api = synergyURL + "/rest/api/asforms/data/get?dataUUID=" + runAPIRequestGET(Registries.CompanySettings.uuid, Registries.CompanySettings.formid, api, "text/plain").json[0];
    var totalCount = searchJSON( runAPIRequestGET(Registries.CompanySettings.uuid, Registries.CompanySettings.formid, api, "text/plain").json[0].data, "people");

    var currentMonth = new Date().getMonth() + 1;
    api = synergyURL + "/rest/api/asforms/search/advanced";
    var data = '{ "query": "where uuid = \'' + Registries.OPPPs.formid + '\' and MONTH(`date.DATE`) = ' + currentMonth + ' and `company.TEXT` = \'' + company + '\'", "parameters": [ "company.TEXT", "date.DATE" ], "showDeleted": "false", "searchInRegistry": "true" }';

    var tmp_Pss = runAPIRequestPOST(Registries.OPPPs.uuid, Registries.OPPPs.formid, api, data, "application/json; charset=utf-8" ).json;
    var Pss = 0;
    for (var ipp = 0; ipp < tmp_Pss.length; ipp++) {
        api = synergyURL + "/rest/api/asforms/data/get?dataUUID=" + tmp_Pss[ipp].dataUUID;
        Pss = Pss + Number( searchJSON( runAPIRequestGET(Registries.OPPPs.uuid, Registries.OPPPs.formid, api, "text/plain").json[0].data, "Pss") );
    }

    if (totalCount === 0) {
        ret = 0;
    } else {
        ret = Number(Pss) / Number(totalCount);
    }

    if ( ret > 1) {
        ret = 1;
    }
    return ret;
};

var calcPpqv = function(company, totalRespondents) {
    var ret = 0;
    var api = synergyURL + "/rest/api/asforms/search?formUUID=" + Registries.CompanySettings.formid + "&search=" + Url.encode(company) + "&field=settings_name&type=exact";
    api = synergyURL + "/rest/api/asforms/data/get?dataUUID=" + runAPIRequestGET(Registries.CompanySettings.uuid, Registries.CompanySettings.formid, api, "text/plain").json[0];
    var totalCount = searchJSON( runAPIRequestGET(Registries.CompanySettings.uuid, Registries.CompanySettings.formid, api, "text/plain").json[0].data, "people");


    var currentMonth = new Date().getMonth() + 1;
    api = synergyURL + "/rest/api/asforms/search/advanced";
    var data = '{ "query": "where uuid = \'' + Registries.Ppqv.formid + '\' and MONTH(`date.DATE`) = ' + currentMonth + ' and `company.TEXT` = \'' + company + '\'", "parameters": [ "company.TEXT", "date.DATE" ], "showDeleted": "false", "searchInRegistry": "true" }';

    var tmp_Ppqv = runAPIRequestPOST(Registries.Ppqv.uuid, Registries.Ppqv.formid, api, data, "application/json; charset=utf-8" ).json;
    var Ppqv = 0;
    for (var ipp = 0; ipp < tmp_Ppqv.length; ipp++) {
        api = synergyURL + "/rest/api/asforms/data/get?dataUUID=" + tmp_Ppqv[ipp].dataUUID;
        Ppqv = Ppqv + Number( searchJSON( runAPIRequestGET(Registries.Ppqv.uuid, Registries.Ppqv.formid, api, "text/plain").json[0].data, "Ppqv") );
    }
    if (totalCount === 0) {
        ret = 0;
    } else {
        ret = Number(Ppqv) / Number(totalCount);
    }

    if ( ret > 1) {
        ret = 1;
    }

    return ret;
};



function getLastCH(company) {
    var ret = new protoCH();

    ret.company = company;

    // find dataUUID of lastCH entry for company
    var api = synergyURL + "/rest/api/asforms/search?formUUID=" + Registries.CH.formid + "&sortField=date&sortDir=asc&search=" + Url.encode(company) + "&field=company&type=exact";
    //log.setLog(api);
    var filteredCHs = runAPIRequestGET(Registries.CH.uuid , Registries.CH.formid, api, "text/plain").json;
    var tmpArray = [];
    var tmpHash = {};
    for (var ifch = 0; ifch < filteredCHs.length; ifch++) {
        var tmpCH = runAPIRequestGET(Registries.CH.uuid, Registries.CH.formid, synergyURL + "/rest/api/asforms/data/get?dataUUID=" + filteredCHs[ifch]);
        tmp = searchJSON( tmpCH.json[0].data, "date" );
        tmp = tmp.split('.')[2] + tmp.split('.')[1] + tmp.split('.')[0];
        tmpArray.push( tmp );
        tmpHash[tmp] = filteredCHs[ifch];
    }

    tmpArray.sort();

    // getdata from lastCH dataUUID
    var lastCHDATA = runAPIRequestGET(Registries.CH.uuid, Registries.CH.formid, synergyURL + "/rest/api/asforms/data/get?dataUUID=" + tmpHash[tmpArray[tmpArray.length-1].toString()]);

    //log.setLog("3. " + synergyURL + "/rest/api/asforms/data/get?dataUUID=" + tmpHash[tmpArray[tmpArray.length-1].toString()]);
    ret.CH = searchJSON( lastCHDATA.json[0].data, "CH_value");
    ret.date = searchJSON( lastCHDATA.json[0].data, "date");

    var tmp="";
    for (var ic = 0; ic < CH_codes.length; ic++) {
        for (var ip = 0; ip < CH_props.length; ip++) {
            tmp =  searchJSON(lastCHDATA.json[0].data, CH_codes[ic].code_remote + "_" + CH_props[ip]);
            if (tmp === "" || typeof tmp == "undefined") {
                tmp = 0;
            }
            eval("ret." + CH_codes[ic].code_local + "_" + CH_props[ip] +" = \"" + tmp + "\";");
        }
    }

    return ret;
}

// применить изменения СН
function makeChanges(lastCH, newCH, companyes) {
    var ret = {};

    var data = '"data": [';

    data = data + '{"id":"company","type":"reglink","value":"' + companyes.company + '","key":"' + companyes.documentID + '","valueID":"' + companyes.documentID + '","userID":""}, ';
    data = data + '{"id":"date","type":"date","value":"' + newCH.date + '","key":"' + newCH.date.substring(6,10) + '-' + newCH.date.substring(3,5) + '-' + newCH.date.substring(0,2) + ' 00:00:00"},';
    data = data + '{"id":"CH_value","type":"textbox","value":"' + newCH.CH + '"}';

    for (var icg = 0; icg < CH_codes.length; icg++) {
        // name
        data = data + ', {"id":"' + CH_codes[icg].code_remote + '_name","type":"textbox","label":" ","value":"' + CH_codes[icg].code_remote + '"}, ';

        // value
        if ( eval("newCH." + CH_codes[icg].code_local + "_value") == eval("lastCH." + CH_codes[icg].code_local + "_value") ) {
            //log.setLog("Переменная " + CH_codes[icg].code_local + " без изменений. - " + eval("newCH." + CH_codes[icg].code_local + "_value") );
            data = data + '{"id":"' + CH_codes[icg].code_remote + '_value","type":"textbox","label":" ","value":"' + eval("lastCH." + CH_codes[icg].code_local + "_value") + '"}, ';

            // date
            var resultdate = eval("lastCH." + CH_codes[icg].code_local + "_date;");
            if (resultdate.substring(4,5)=="-") { resultdate = resultdate.substring(8,10) + '.' + resultdate.substring(5,7) + '.'+ resultdate.substring(0,4); }
            data = data + '{"id":"' + CH_codes[icg].code_remote + '_date","type":"date","label":" ","value":"' + resultdate + '","key":"' + resultdate.substring(6,10) + '-' + resultdate.substring(3,5) + '-' + resultdate.substring(0,2) + ' 00:00:00' + '"},';
        } else {
            log.setLog("Переменная " + CH_codes[icg].code_local + " изменилась. старое значение - " + eval("lastCH." + CH_codes[icg].code_local + "_value") + ", новое значение - " + eval("newCH." + CH_codes[icg].code_local + "_value"));
            data = data + '{"id":"' + CH_codes[icg].code_remote + '_value","type":"textbox","label":" ","value":"' + eval("newCH." + CH_codes[icg].code_local + "_value") + '"}, ';

            // date
            var resultdate = "";
            resultdate = eval("newCH." + CH_codes[icg].code_local + "_date;");
            //    log.setLog(">>> " + resultdate + " <<< - " + eval("newCH." + CH_codes[icg].code_local + "_value;"));
            if (resultdate.substring(4,5) == "-") { resultdate = resultdate.substring(8,10) + '.' + resultdate.substring(5,7) + '.' + resultdate.substring(0,4); }
            //    if (resultdate.substring(4,5) == ".") { resultdate = resultdate.substring(0,2) + ".0" + resultdate.substring(3,9); }

            data = data + '{"id":"' + CH_codes[icg].code_remote + '_date","type":"date","label":" ","value":"' + resultdate + '","key":"' + resultdate.substring(6,10) + '-' + resultdate.substring(3,5) + '-' + resultdate.substring(0,2) + ' 00:00:00' + '"},';
        }

        // coef
        data = data + '{"id":"' + CH_codes[icg].code_remote + '_coef","type":"textbox","label":" ","value":"' + eval("newCH." + CH_codes[icg].code_local + "_coef") + '"}, ';

        // description
        data = data + '{"id":"' + CH_codes[icg].code_remote + '_description","type":"textbox","label":" ","value":"' + CH_codes[icg].code_description.replace(/\"/g, "\\\"") + '"},';

        // result
        data = data + '{"id":"' + CH_codes[icg].code_remote + '_result","type":"textbox","label":" ","value":"' + CH_codes[icg].code_result + '"},';

        // questionarie
        data = data + '{"id":"' + CH_codes[icg].code_remote + '_questionnaire","type":"listbox","value":"' + CH_codes[icg].code_questionnaire.replace(/\"/g, "\\\"") + '","key":"' + CH_codes[icg].code_questionnaire_key + '"},';

        // background
        data = data + '{"id":"' + CH_codes[icg].code_remote + '_background","type":"listbox","value":"' + CH_codes[icg].code_background + '","key":"' + CH_codes[icg].code_background_key + '"}';

    }

    data = data + ']';

    var newUUID = runAPIRequestGET(Registries.CH.uuid , Registries.CH.formid, synergyURL + "/rest/api/registry/create_doc?registryID=" + Registries.CH.uuid);
    var retPost = runAPIRequestPOST(Registries.CH.uuid , Registries.CH.formid, synergyURL + "/rest/api/asforms/data/save?formUUID=" + Registries.CH.formid + "&uuid=" + newUUID.json.dataUUID, data, "application/x-www-form-urlencoded; charset=utf-8", newUUID.json.dataUUID);
    var retAct = runAPIRequestGET(Registries.CH.uuid , Registries.CH.formid, synergyURL + "/rest/api/registry/activate_doc?dataUUID=" + newUUID.json.dataUUID);
    return retPost.responseBody;
}

function setLastCoef(lastCH, currentCH) {
    for (var icq = 0; icq < CH_codes.length; icq++) {
        eval( "currentCH." + CH_codes[icq].code_local + "_coef = lastCH." + CH_codes[icq].code_local + "_coef;");
    }
    return currentCH;
}

function makeChangesDIAG(currentCH, companyes) {
    var ret = "";
    var tmp = currentCH.recalcDIAG();

    var data = '"data": [';

    data = data + '{"id":"company","type":"reglink","value":"' + companyes.company + '","key":"' + companyes.documentID + '","valueID":"' + companyes.documentID + '","userID":""}, ';
    var date = currentCH.date;
    if ( date.substring(4,5) == ".") {
        date = date.substring(0,2) + ".0" + date.substring(3,9);
    }
    data = data + '{"id":"date","type":"date","value":"' + date + '","key":"' + date.substring(6,10) + '-' + date.substring(3,5) + '-' + date.substring(0,2) + ' 00:00:00"},';
    data = data + '{"id":"background","type":"textbox","value":"' + tmp[0] + '"},';
    data = data + '{"id":"circulation","type":"textbox","value":"' + tmp[1] + '"}';
    data = data + ']';

    var newUUID = runAPIRequestGET(Registries.DIAG.uuid , Registries.DIAG.formid, synergyURL + "/rest/api/registry/create_doc?registryID=" + Registries.DIAG.uuid);
    var retPost = runAPIRequestPOST(Registries.DIAG.uuid , Registries.DIAG.formid, synergyURL + "/rest/api/asforms/data/save?formUUID=" + Registries.DIAG.formid + "&uuid=" + newUUID.json.dataUUID, data, "application/x-www-form-urlencoded; charset=utf-8", newUUID.json.dataUUID);
    var retAct = runAPIRequestGET(Registries.DIAG.uuid , Registries.DIAG.formid, synergyURL + "/rest/api/registry/activate_doc?dataUUID=" + newUUID.json.dataUUID);

    ret = "Фон " + tmp[0].toFixed(2) + "%, По анкетам " + tmp[1].toFixed(2) + "%"; // (Результат: " + retPost.responseBody + ") (Активация: " + retAct.responseBody + ")";

    return ret;
}

// получение вспомогательных коэффициентов
function getVSP_coefs(company) {
  var ret = new VSP_coef();
// http://192.168.1.60:8080/Synergy/rest/api/asforms/search?formUUID=f4c3c114-a9a7-4827-824f-ba9cb5b17884&search=%D0%A2%D0%9E%D0%9E%20%D0%9C%D1%83%D0%BD%D0%B0%D0%B9%D1%82%D0%B5%D0%BB%D0%B5%D0%BA%D0%BE%D0%BC&field=company&type=exact&showDeleted=false&searchInRegistry=true&sortField=parameter_M_date&sortDir=desc&recordsCount=1
  var api = synergyURL + "/rest/api/asforms/search?formUUID=" + Registries.VSP_coef.formid + "&search=" + Url.encode(company) + "&field=company&type=exact&showDeleted=false&searchInRegistry=true&sortField=parameter_M_date&sortDir=desc&recordsCount=1";
  api = synergyURL + "/rest/api/asforms/data/get?dataUUID=" + runAPIRequestGET(Registries.VSP_coef.uuid , Registries.VSP_coef.formid, api, "text/plain").json[0];
  var tmpVSP = runAPIRequestGET(Registries.VSP_coef.uuid , Registries.VSP_coef.formid, api);

  ret.parameter_M = searchJSON( tmpVSP.json[0].data, "parameter_M" );
  ret.parameter_N = searchJSON( tmpVSP.json[0].data, "parameter_N" );
  ret.parameter_T1 = searchJSON( tmpVSP.json[0].data, "parameter_T1" );
  ret.parameter_T2 = searchJSON( tmpVSP.json[0].data, "parameter_T2" );
  ret.parameter_T3 = searchJSON( tmpVSP.json[0].data, "parameter_T3" );

  ret.parameter_M_date = searchJSON( tmpVSP.json[0].data, "parameter_M_date" );
  ret.parameter_N_date = searchJSON( tmpVSP.json[0].data, "parameter_N_date" );
  ret.parameter_T1_date = searchJSON( tmpVSP.json[0].data, "parameter_T1_date" );
  ret.parameter_T2_date = searchJSON( tmpVSP.json[0].data, "parameter_T2_date" );
  ret.parameter_T3_date = searchJSON( tmpVSP.json[0].data, "parameter_T3_date" );

  ret.parameter_M_duration = searchJSON( tmpVSP.json[0].data, "parameter_M_duration" );
  ret.parameter_N_duration = searchJSON( tmpVSP.json[0].data, "parameter_N_duration" );
  ret.parameter_T1_duration = searchJSON( tmpVSP.json[0].data, "parameter_T1_duration" );
  ret.parameter_T2_duration = searchJSON( tmpVSP.json[0].data, "parameter_T2_duration" );
  ret.parameter_T3_duration = searchJSON( tmpVSP.json[0].data, "parameter_T3_duration" );

  ret.parameter_M_text = searchJSON( tmpVSP.json[0].data, "parameter_M_text" );
  ret.parameter_N_text = searchJSON( tmpVSP.json[0].data, "parameter_N_text" );
  ret.parameter_T1_text = searchJSON( tmpVSP.json[0].data, "parameter_T1_text" );
  ret.parameter_T2_text = searchJSON( tmpVSP.json[0].data, "parameter_T2_text" );
  ret.parameter_T3_text = searchJSON( tmpVSP.json[0].data, "parameter_T3_text" );

  return ret;
}


// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
// !!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!


// starting ...

var log = new protoCH(); // используется только для логов

var companyes = getCompanyes();
log.setLog("Активные компании: ");


for (var key in companyes) {
    log.setLog( Number(Number(key) + 1) + ". <b>" + companyes[key].company + "</b>");
}


for (var ic in companyes) {
    // для каждой компании
    var totalRespondents = getTotalRespondents(companyes[ic].company);
    var currentCH = new protoCH();
    log.setLog(" ");
    log.setLog("<hr>");
    log.setLog(" ");
    log.setLog("Компания <b>\"" + companyes[ic].company + "\"</b> (Всего респондентов: <i>" + totalRespondents + "</i>)");

    // настройка коэффициентов для текущей компании
    // currentVSP_coef.parameter_M ...
    var currentVSP_coef = getVSP_coefs(companyes[ic].company);

    for (var i = 0; i < CH_codes.length; i++) {
        eval("currentCH." + CH_codes[i].code_local + "_value = calc" + CH_codes[i].code_local + "(companyes[ic].company, totalRespondents);");
        eval("log.setLog(\"Расчет коэффициента <i>" + CH_codes[i].code_local + "</i>... <b>\" + currentCH." + CH_codes[i].code_local + "_value + \"</b>\");");
    }


    log.setLog("Поиск последних данных по СН для компании \"" + companyes[ic].company + "\"...");
    lastCH = getLastCH(companyes[ic].company);
    currentCH = setLastCoef(lastCH, currentCH);
    currentCH.CH = currentCH.recalcCH();
    currentCH.date =  currentdate;

    log.setLog("Предыдущий показатель СН: " + lastCH.CH + ", дата: " + lastCH.date);
    log.setLog("Текущий показатель СН: " + currentCH.CH + ", дата: " + currentCH.date);

    if (Number(lastCH.CH).toFixed(3) == Number(currentCH.CH).toFixed(3)) {
        log.setLog("Показатель СН не изменился, обновления не требуется");
    } else {
        log.setLog("Показатель СН изменился, требуется обновление");
        var status = eval("(" + makeChanges(lastCH, currentCH, companyes[ic]) + ")");
        if (status.errorCode == "0") {
            status = "OK";
        } else {
            status = "ErrorCode: " + status.errorCode + ", " + status.errorMessage;
        }
        log.setLog( "Обновление показателя СН: " +  status);
        // обновление данных диаграмм
        log.setLog( "Обновление данных диаграмм: " + makeChangesDIAG(currentCH, companyes[ic]) );
    }

    // log.setLog("Обновление вспомогательных коэффициентов: " + updateVSP_coef(companyes[ic], currentVSP_coef).toString());

}


//-- ending
setCurrentForm("recalc_log", log.getLog());
var result = true;
var message = "Ok";
