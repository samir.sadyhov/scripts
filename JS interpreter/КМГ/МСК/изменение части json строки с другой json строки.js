var form = platform.getFormsManager().getFormData(dataUUID);
form.load();

var synergyUser = "Administrator";
var synergyPass = "$ystemAdm1n";
var synergyURL = "http://127.0.0.1:8080/Synergy/";

// функция для исполнения api с типом get
function getAPIresult(api, param) {
  var APIString = ''; //10
  param == null ? APIString = synergyURL + api : APIString = synergyURL + api + param;
  var get = new org.apache.commons.httpclient.methods.GetMethod(APIString);
  var client = new org.apache.commons.httpclient.HttpClient();
  var creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(synergyUser, synergyPass);
  client.getParams().setAuthenticationPreemptive(true);
  client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
  get.setRequestHeader("Content-type", "application/json");
  var status = client.executeMethod(get);
  var result = get.getResponseBodyAsString();
  return result;
  get.releaseConnection();
}

//получение данных по текущей форме, ищем часть строки с 3-ей "[" и первой "]"
var parData = getAPIresult("rest/api/asforms/data/get?dataUUID=", dataUUID);

var target = "[", // цель поиска
  pos = 0,
  i = 0,
  st = 0;

while (true) {
  i++;
  var foundPos = parData.indexOf(target, pos);
  if (i == 3) st = foundPos;
  if (foundPos == -1) break;
  pos = foundPos + 1; // продолжить поиск со следующей
}

var fin = parData.indexOf("]", pos);
var newStr = parData.substring(st + 1, fin);

var des = form.getValue("link");
var asfDataID = eval("(" + getAPIresult("rest/api/docflow/doc/document_info?documentID=", des) + ")");
var formID = asfDataID.formID;
asfDataID = asfDataID.asfDataID;
var Desition = getAPIresult("rest/api/asforms/data/get?dataUUID=", asfDataID);

//получение данных по форме, на которую есть ссылка, ищем часть строки с 3-ей "[" и первой "]"
target = "["; // цель поиска
pos = 0; i = 0; st = 0;

while (true) {
  i++;
  var foundPos = Desition.indexOf(target, pos);
  if (i == 3) st = foundPos;
  if (foundPos == -1) break;
  pos = foundPos + 1; // продолжить поиск со следующей
}

fin = Desition.indexOf("]", pos);
var newStr1 = Desition.substring(st + 1, fin);

//замена строк
Desition = Desition.replace(newStr1, newStr);
st = Desition.indexOf('"data"', 0);

//получение данных по форме, ищем часть строки с data до 2-ой "]"
target = "]"; // цель поиска
pos = 0; i = 0; fin = 0;

while (true) {
  i++;
  var foundPos = Desition.indexOf(target, pos);
  if (i == 2) fin = foundPos;
  if (foundPos == -1) break;
  pos = foundPos + 1; // продолжить поиск со следующей
}

Desition = Desition.substring(st, fin + 1);

//сохранение данных по форме
var post = new org.apache.commons.httpclient.methods.PostMethod(synergyURL + "rest/api/asforms/data/save");
post.addParameter("formUUID", formID);
post.addParameter("uuid", asfDataID);
post.addParameter("data", Desition);
var client = new org.apache.commons.httpclient.HttpClient();
var creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(synergyUser, synergyPass);
client.getParams().setAuthenticationPreemptive(true);
client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
post.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=utf-8");
var status = client.executeMethod(post);
post.releaseConnection();

form.save();
var result = true;
var message = "OK";
