var form = platform.getFormsManager().getFormData(dataUUID);
form.load();

var Pe1=Number(form.getValue("P(e)1_value"))*100;
if (isNaN(Pe1)) {Pe1=0;}
var Pe2=Number(form.getValue("P(e)2_value"))*100;
if (isNaN(Pe2)) {Pe2=0;}
var Pe3=Number(form.getValue("P(e)3_value"))*100;
if (isNaN(Pe3)) {Pe3=0;}
var Pakt=Number(form.getValue("Р(akt)_value"))*100;
if (isNaN(Pakt)) {Pakt=0;}
var Pind=Number(form.getValue("Р(ind)_value"))*100;
if (isNaN(Pind)) {Pind=0;}
var Pcrim=Number(form.getValue("P(crim)_value"))*100;
if (isNaN(Pcrim)) {Pcrim=0;}
var Pout=Number(form.getValue("Р(out)_value"))*100;
if (isNaN(Pout)) {Pout=0;}
var Pmax=Number(form.getValue("Р(max)_value"))*100;
if (isNaN(Pmax)) {Pmax=0;}
var P0=Number(form.getValue("Р(0)_value"))*100;
if (isNaN(P0)) {P0=0;}
var Paag=Number(form.getValue("Р(aag)_value"))*100;
if (isNaN(Paag)) {Paag=0;}
var Pconnect=Number(form.getValue("P(connect)_value"))*100;
if (isNaN(Pconnect)) {Pconnect=0;}
var X=Number(form.getValue("X_value"))*100;
if (isNaN(X)) {X=0;}
var Z=Number(form.getValue("Z_value"))*100;
if (isNaN(Z)) {Z=0;}
var Psat=Number(form.getValue("P(sat)_value"))*12;
if (isNaN(Psat)) {Psat=0;}
var Pq=Number(form.getValue("P(q)_value"))*100;
if (isNaN(Pq)) {Pq=0;}
var Ppp=Number(form.getValue("P(pp)_value"))*100;
if (isNaN(Ppp)) {Ppp=0;}
var Ppqv=Number(form.getValue("P(pqv)_value"))*100;
if (isNaN(Ppqv)) {Ppqv=0;}
var Pss=Number(form.getValue("P(ss)_value"))*100;
if (isNaN(Pss)) {Pss=0;}

form.setValue("P(e)1_display", Pe1);
form.setValue("P(e)2_display", Pe2);
form.setValue("P(e)3_display", Pe3);
form.setValue("Р(akt)_display", Pakt);
form.setValue("Р(ind)_display", Pind);
form.setValue("P(crim)_display", Pcrim);
form.setValue("Р(out)_display", Pout);
form.setValue("Р(max)_display", Pmax);
form.setValue("Р(0)_display", P0);
form.setValue("Р(aag)_display", Paag);
form.setValue("P(connect)_display", Pconnect);
form.setValue("X_display", X);
form.setValue("Z_display", Z);
form.setValue("P(sat)_display", Psat);
form.setValue("P(q)_display", Pq);
form.setValue("P(pp)_display", Ppp);
form.setValue("P(pqv)_display", Ppqv);
form.setValue("P(ss)_display", Pss);

form.setValue("S_display", form.getValue("S_value"));
form.setValue("Dst_display", form.getValue("Dst_value"));
form.setValue("Qst_display", form.getValue("Qst_value"));
form.setValue("P(des)_display", form.getValue("P(des)_value"));

form.save();
var result = true;
var message = 'OK';
