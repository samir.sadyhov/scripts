var form = platform.getFormsManager().getFormData(dataUUID);
form.load();

var synergyUser = "Administrator";
var synergyPass = "$ystemAdm1n";
var synergyURL = "http://127.0.0.1:8080/Synergy/";

// функция для исполнения api с типом get
function getAPIresult(api, param) {
  var APIString = '';
  param == null ? APIString = synergyURL + api : APIString = synergyURL + api + param;
  var get = new org.apache.commons.httpclient.methods.GetMethod(APIString);
  var client = new org.apache.commons.httpclient.HttpClient();
  var creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(synergyUser, synergyPass);
  client.getParams().setAuthenticationPreemptive(true);
  client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
  get.setRequestHeader("Content-type", "application/json");
  var status = client.executeMethod(get);
  var result = get.getResponseBodyAsString();
  result =  eval("(" + result + ")");
  return result;
  get.releaseConnection();
}

var des=form.getValue("link");
var num=form.getNumericValue("number");
var asfDataID = getAPIresult("rest/api/docflow/doc/document_info?documentID=", des);
var formID=asfDataID.formID;
asfDataID=asfDataID.asfDataID;
var Desition = getAPIresult("rest/api/asforms/data/get?dataUUID=", asfDataID);

// поиск строки которую нужно изменить
var t=0, i2=0, d2 = 0;
for (var i = 0; i < Desition[0].data.length; i++) {
  if (Desition[0].data[i].id == "table02") {
    for (var j = 0; j < Desition[0].data[i].data.length; j++) {
      for (var d=0; d<j; d++) {
        if ((Desition[0].data[i].data[j].id == "number-b" + (d+1)) && (Desition[0].data[i].data[j].value==num)) {
          t=1;
          d2=d+1;
          i2=i;
          break;
        } if (t==1) break;
      } if (t==1) break;
    }
  }
}

// изменение данных
t=0;
for (var j=0; j < Desition[0].data[i2].data.length; j++) {
   if (Desition[0].data[i2].data[j].id == "desicion-b" + d2) {
       Desition[0].data[i2].data[j].value="решен";
       Desition[0].data[i2].data[j].key="1";
       t=1;
   }
   if (t==1) break;
}


Desition=Desition[0].data;

// перевод из объекта в строку
Desition = uneval(Desition);
var r = /\\u([\d\w]{4})/gi;
Desition = Desition.replace(r, function (match, grp) {
return String.fromCharCode(parseInt(grp, 16)); } );

//формирование правильного формата data для передачи в api  "rest/api/asforms/data/save"
Desition = 'data:' + unescape(Desition);
Desition=Desition.replace(/id/g, '"id"');
Desition=Desition.replace(/type/g, '"type"');
Desition=Desition.replace(/label/g, '"label"');
Desition=Desition.replace(/valueID/g, '"valueID"');
Desition=Desition.replace(/value/g, '"value"');
Desition=Desition.replace(/key/g, '"key"');
Desition=Desition.replace(/username/g, '"username"');
Desition=Desition.replace(/userID/g, '"userID"');
Desition=Desition.replace(/""value"ID"/g, '"valueID"');
Desition=Desition.replace(/""label""/g, '"label"');
Desition=Desition.replace(/data:/g, '"data": ');

//сохранение данных по форме
var post = new org.apache.commons.httpclient.methods.PostMethod(synergyURL + "rest/api/asforms/data/save");
post.addParameter("formUUID", formID);
post.addParameter("uuid", asfDataID);
post.addParameter("data", Desition);
var client = new org.apache.commons.httpclient.HttpClient();
var creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(synergyUser, synergyPass);
client.getParams().setAuthenticationPreemptive(true);
client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
post.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=utf-8");
var status = client.executeMethod(post);
//var errorMsg = eval("(" + post.getResponseBodyAsString() + ")").errorMessage;
post.releaseConnection();

//form.setValue("test", errorMsg);

form.save();
var result=true;
var message = "Изменение статуса работы № " + num + " на решено, успешно.";
