var form = platform.getFormsManager().getFormData(dataUUID);
form.load();

// общие настройки
var synergy = {
  login: "1",
  password: "1",
  url: "http://127.0.0.1:8080/Synergy",
  apiGetData: "/rest/api/asforms/data/",
  apiSaveData: "/rest/api/asforms/data/save",
  apiUserData: "/rest/api/filecabinet/user/"
};

// наименование компонентов на форме приказа
var prikaz = {
  position: 'fromPositionID',
  userid: 'userID',
  manager: 'manager'
};

// наименование компонентов на карточке подразделения
var depCard = {
  formID: '1a37986a-a3c7-4541-ac42-12bf327eb53a',
  table: 'dekretTable',
  userid: 'user',
  statusName: 'status',
  finishDate: 'finishDate'
};


// получение результата api функции
function runAPIRequestGET(api, param) {
  var APIString = '';
  param == null ? APIString = synergy.url + api : APIString = synergy.url + api + param;
  var get = new org.apache.commons.httpclient.methods.GetMethod(APIString);
  var client = new org.apache.commons.httpclient.HttpClient();
  var creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(synergy.login, synergy.password);
  client.getParams().setAuthenticationPreemptive(true);
  client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
  get.setRequestHeader("Content-type", "application/json; charset=utf-8");
  var status = client.executeMethod(get);
  var result = get.getResponseBodyAsString();
  get.releaseConnection();
  return result;
}

// сохранение данных по форме
function runAPIRequestPOST(api, form, dataUUID, data) {
  var APIString = synergy.url + api;
  var post = new org.apache.commons.httpclient.methods.PostMethod(APIString);
  post.addParameter("formUUID", form);
  post.addParameter("uuid", dataUUID);
  post.addParameter("data", data);
  var client = new org.apache.commons.httpclient.HttpClient();
  var creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(synergy.login, synergy.password);
  client.getParams().setAuthenticationPreemptive(true);
  client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
  post.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=utf-8");
  var status = client.executeMethod(post);
  var result = post.getResponseBodyAsString();
  post.releaseConnection();
  return result;
}


// object to string
function obj2Str(obj) {
  var val, output = "";
  if (obj) {
    output += '{"';
    for (var i in obj) {
      val = obj[i];
      output += i + '":"' + val + '","';
    }
    output = output.substring(0, output.length - 2) + "}";
  }
  return output;
}

var resultManager = 0;

// получаем ID пользователя
var userID = form.getValue(prikaz.userid);
// получаем данные по пользователю
var userInfo = runAPIRequestGET(synergy.apiUserData, userID);
userInfo = eval("(" + userInfo + ")");
var userLastname = userInfo.lastname; //достаем фамилию пользователя

// проверяем есть ли у пользователя должность
if (userInfo.positions.length == 0) {
  // если нет, значит скорее всего это руководитель в д\о
  // надо найти в каком подразделении он был руководителем, достать оттуда карточку и выпилить его строку
  resultManager = 1;

  // формирование поисковой строки
  var dataSearch = '{ "query": "where `user-b1.TEXT` like \'' + userLastname + '%\'';
  var tmpSearch1 = '", "parameters": ["user-b1.TEXT"';
  for (var kk = 2; kk < 20; kk++) {
    dataSearch += ' or `user-b' + kk + '.TEXT` like \'' + userLastname + '%\'';
    tmpSearch1 += ', "user-b' + kk + '.TEXT"';
  }
  dataSearch = dataSearch + tmpSearch1 + '], "showDeleted": "false", "searchInRegistry": "false" }'

  // поиск карточки подразделения
  var APIString = synergy.url + "/rest/api/asforms/search/advanced";
  var post = new org.apache.commons.httpclient.methods.PostMethod(APIString);
  var client = new org.apache.commons.httpclient.HttpClient();
  var creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(synergy.login, synergy.password);
  client.getParams().setAuthenticationPreemptive(true);
  client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
  post.setRequestHeader("Content-type", "application/json; charset=utf-8");
  post.setRequestBody(dataSearch);
  var status = client.executeMethod(post);
  var resultPost = post.getResponseBodyAsString();

  // сохраняем в переменную uuid карточки подразделения
  var depCardUUID = eval("(" + resultPost + ")");
  if (depCardUUID.length > 0) {
    depCardUUID = depCardUUID[0].dataUUID
  } else {
    depCardUUID = '0';
  }

  // вытаскиваем карточку подразделения
  var cardDataJson = runAPIRequestGET(synergy.apiGetData, depCardUUID);

  if (cardDataJson != '{}') { // проверяем нормальная ли карточка
    var tt = 0;
    cardDataJson = eval("(" + cardDataJson + ")");

    for (var i = 0; i < cardDataJson.data.length; i++) { // ищем таблицу в карточке
      if (cardDataJson.data[i].id == 'dekretTable') {
        var tmpi = i;
        break;
      }
    }
    for (var i = 0; i < cardDataJson.data[tmpi].data.length; i++) { // ищем в таблице юзера из приказа
      if (cardDataJson.data[tmpi].data[i].key == userID) {
        cardDataJson.data[tmpi].data.splice(i, i); // если есть такой юзер выпилить его строку из таблицы
        tt = 1;
        break;
      }
    }
    if (tt == 1) { // если из таблицы выпилили юзера, надо перезаписать карточку

      // формирование json строки для передачи в апи
      var cardData = '"data":[{"id":"cmp-w4ypt6","type":"label","label":"Декретный отпуск","value":" "},';
      cardData += '{"id":"' + depCard.table + '","type":"appendable_table","key":"","data":[';
      for (var i = 0; i < cardDataJson.data[1].data.length; i++) {
        cardData += obj2Str(cardDataJson.data[1].data[i]) + ',';
      }
      cardData = cardData.substring(0, cardData.length - 1) + ']}]';
      // Сохраняем данные в карточке подразделения
      var resultFormSave = runAPIRequestPOST(synergy.apiSaveData, depCard.formID, depCardUUID, cardData);
      resultFormSave = eval("(" + resultFormSave + ")");
    }
  }
} else { // иначе записываем на форму результат 0
  resultManager = 0;
}

form.setValue(prikaz.manager, resultManager);

form.save();
var result = true;
var message = 'OK';
