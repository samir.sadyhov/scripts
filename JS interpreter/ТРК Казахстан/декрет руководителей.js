var form = platform.getFormsManager().getFormData(dataUUID);
form.load();

// общие настройки
var synergy = {
  login: "administrator",
  password: "1",
  url: "http://127.0.0.1:8080/Synergy",
  apiPos: "/rest/api/positions/get?positionID=",
  apiDep: "/rest/api/departments/get?departmentID=",
  apiCardDep: "/rest/api/departments/get_cards?departmentID=",
  apiRegID: "/rest/api/asforms/data/document?dataUUID=",
  apiDict: "/rest/api/dictionary/get_by_code?dictionaryCode=",
  apiPosVacate: "/rest/api/positions/discharge",
  apiGetData: "/rest/api/asforms/data/",
  apiSaveData: "/rest/api/asforms/data/save",
  apiUserData: "/rest/api/filecabinet/user/"
};

// наименование компонента на форме приказа
var prikaz = {
  position: 'pos_rus',
  userid: 'userID',
  finishDate: 'date2_rus'
};

// наименование компонентов на карточке подразделения
var depCard = {
  formID: '919f0259-8f29-470c-b482-53294874d1d8',
  table: 'dekretTable',
  userid: 'user',
  statusName: 'status',
  finishDate: 'finishDate'
};

// код справочника
var codDicitionary = 'registryStatus';
// код столбца справочника где хранятся статусы
var codColumn = 'status';


// получение результата api функции
function runAPIRequestGET(api, param) {
  var APIString = '';
  param == null ? APIString = synergy.url + api : APIString = synergy.url + api + param;
  var get = new org.apache.commons.httpclient.methods.GetMethod(APIString);
  var client = new org.apache.commons.httpclient.HttpClient();
  var creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(synergy.login, synergy.password);
  client.getParams().setAuthenticationPreemptive(true);
  client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
  get.setRequestHeader("Content-type", "application/json; charset=utf-8");
  var status = client.executeMethod(get);
  var result = get.getResponseBodyAsString();
  get.releaseConnection();
  return result;
}

// //сохранение данных по форме
function runAPIRequestPOST(api, form, dataUUID, data) {
  var APIString = synergy.url + api;
  var post = new org.apache.commons.httpclient.methods.PostMethod(APIString);
  post.addParameter("formUUID", form);
  post.addParameter("uuid", dataUUID);
  post.addParameter("data", data);
  var client = new org.apache.commons.httpclient.HttpClient();
  var creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(synergy.login, synergy.password);
  client.getParams().setAuthenticationPreemptive(true);
  client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
  post.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=utf-8");
  var status = client.executeMethod(post);
  var result = post.getResponseBodyAsString();
  post.releaseConnection();
  return result;
}


// Функция проверки записей в таблице
function isHaveARow(form, table) {
  try {
    form.getRowsCount(table);
    return true;
  } catch (err) {
    return false;
  }
}

// object to string
function obj2Str(obj) {
  var val, output = "";
  if (obj) {
    output += '{"';
    for (var i in obj) {
      val = obj[i];
      output += i + '":"' + val + '","';
    }
    output = output.substring(0, output.length - 2) + "}";
  }
  return output;
}

// получаем ID должности
var positionID = form.getValue(prikaz.position);
// получаем ID пользователя
var userID = form.getValue(prikaz.userid);
// получаем дату завершения отпуска
var finishDateKey = form.getValue(prikaz.finishDate);
var finishDateValue = finishDateKey.substring(0, finishDateKey.indexOf(' ', 0));

// Получаем ФИО пользователя
var userFIO = runAPIRequestGET(synergy.apiUserData, userID);
userFIO = userFIO.substring(0, userFIO.indexOf('positions', 0)-4)+'}';
userFIO = eval("(" + userFIO + ")");
var userLastname = userFIO.lastname;
var userFirstname = userFIO.firstname;
var userPatronymic = userFIO.patronymic;
userFIO = userLastname + ' ' + userFirstname.substring(0,1)+'.';
if (userPatronymic.length > 1) {
userFIO+=' '+userPatronymic.substring(0,1)+'.';
}

// Получаем ID текущего реестра
var registryID = runAPIRequestGET(synergy.apiRegID, dataUUID);
registryID = registryID.substring(registryID.indexOf('registryID', 0)).substring(13,49);

// Получаем значения справочника
var dictionary = runAPIRequestGET(synergy.apiDict, codDicitionary);
dictionary = eval("(" + dictionary + ")");

//----------------------------------------------------------------------------------------
// Получаем статус отпуска для пользователя
//----------------------------------------------------------------------------------------
var statusUser = null;
// поиск ID столбца статуса
for (var i = 0; i < dictionary.columns.length; i++) {
  if (dictionary.columns[i].code == codColumn) {
    var column1 = dictionary.columns[i].columnID;
    break;
  }
}
// поиск номера записи в массиве по id реестра
var tmp = null;
for (var i = 0; i < dictionary.items.length; i++) {
  for (var j = 0; j < dictionary.items[i].values.length; j++) {
    if (dictionary.items[i].values[j].value == registryID) {
      tmp = i;
      break;
    }
  }
}
// находим статус
if (tmp) {
  for (var i = 0; i < dictionary.items[tmp].values.length; i++) {
    if (dictionary.items[tmp].values[i].columnID == column1) {
      statusUser = dictionary.items[tmp].values[i].value;
      break;
    }
  }
} else {
  statusUser = null;
}
//----------------------------------------------------------------------------------------

// Получаем ID департамента по должности руководителя
var posJson = runAPIRequestGET(synergy.apiPos, positionID);
posJson = eval("(" + posJson + ")");
var departmentID = posJson.parentDepartmentID;

// Получаем данные подразделения
var depData = runAPIRequestGET(synergy.apiDep, departmentID);
depData = eval("(" + depData + ")");
var managerID = depData.manager.positionID;

// Проверяем, если должность указанная на форме это должность руководителя, то выполняем необходимые действия
if (positionID == managerID) {

// Получаем карточки подразделения
var depJson = '';
depJson = runAPIRequestGET(synergy.apiCardDep, departmentID);
depJson = String(depJson).replace(/form-uuid/g, "formUuid");
depJson = depJson.replace(/data-uuid/g, "dataUuid");
depJson = eval("(" + depJson + ")");

// Ищем UUID нужной карточки подразделения
var cardUUID = '';
for (var i = 0; i < depJson.length; i++) {
  if (depJson[i].formUuid == depCard.formID) {
    cardUUID = depJson[i].dataUuid;
    break;
  }
}
var resultFormSave = '0';
var resultVacate = '0';
var resMsg='Руководитель';

// ##########################################################################

// 1. вытаскиваем json карточки подразделения
var cardDataJson = runAPIRequestGET(synergy.apiGetData, cardUUID);
//cardDataJson = cardDataJson.toString();
cardDataJson = eval("(" + cardDataJson + ")");

var tmpStatus = 0, tmpFinish = 0;
var updateUser = 'NO';
var createUser = 'NO';
var createTable = 'NO';
var tmp1 = 0;

// 2. проверяем есть ли записи в таблице
if (cardDataJson.data.length == 0) {
  createTable = 'YES';
} else {
  for (var i = 0; i < cardDataJson.data[1].data.length; i++) {
    var tmpid = cardDataJson.data[1].data[i].id.substring(0, depCard.userid.length);
    if (tmpid == depCard.userid) {
      tmp1++;
    }
    if (tmp1 > 0) {
      createTable = 'NO';
      break;
    } else {
      createTable = 'YES';
    }
  }
}

// 3. если таблица пустая то создаем новую запись
if (createTable == 'YES') {
  // 3.1. начало формы, с наименованием
  var cardData = '"data":[{"id":"cmp-w4ypt6","type":"label","label":"Декретный отпуск","value":" "},';
  // 3.2. добавляем начало таблицы
  cardData += '{"id":"' + depCard.table + '","type":"appendable_table","key":"","data":[';
  // 3.3. формируем заголовок таблицы из 3-х столбцов
  //for (var i = 0; i < 3; i++) {
  //  cardData += '{"id": "' + cardDataJson.data[1].data[i].id + '", "type": "' + cardDataJson.data[1].data[i].type + '", "label": "' + cardDataJson.data[1].data[i].label + '", "value": "' + cardDataJson.data[1].data[i].value + '"},';
  //}
  cardData+='{"id":"cmp-48f5rb","type":"label","label":"ФИО","value":" "},{"id":"cmp-p2hpo7","type":"label","label":"Статус","value":" "},{"id":"cmp-yds4gu","type":"label","label":"Дата выхода","value":" "},';
  // 3.4. добавляем строку в таблицу
  cardData += '{"id":"' + depCard.userid + '-b1","type":"entity","value":"' + userFIO + '","key":"' + userID + '","formatVersion":"V1"},{"id":"' + depCard.statusName + '-b1","type":"listbox","value":"' + statusUser + '","key":"' + statusUser + '"},{"id":"' + depCard.finishDate + '-b1","type":"date","value":"' + finishDateValue + '","key":"' + finishDateKey + '"}]}]';
  // 3.5. Увольнение сотрудника
  var resultVacate = runAPIRequestGET(synergy.apiPosVacate, '?positionID='+positionID+'&userID='+userID);
  resultVacate = eval("(" + resultVacate + ")");
  resultVacate = resultVacate.errorMessage;

  // 4. иначе проверяем необходимость создания или изменения юзера
} else {
  tmp1 = 0;
  for (var i = 0; i < cardDataJson.data[1].data.length; i++) {
    var tmpid = cardDataJson.data[1].data[i].id.substring(0, depCard.userid.length);
    if (cardDataJson.data[1].data[i].key == userID) {tmp1++;}
  }
  if (tmp1 > 0) {
    // нужно изменить статус и дату выходу у юзера
    updateUser = 'YES';
  } else {
    // нужно создать новую запись в таблице по данному юзеру
    createUser = 'YES';
  }
}

// 5. если юзер есть в таблице, меняем статус и дату выхода
if (updateUser == 'YES') {
  for (var i = 0; i < cardDataJson.data[1].data.length; i++) {
    var tmpid = cardDataJson.data[1].data[i].id.substring(0, depCard.userid.length);
    // находим наименования компонентов на форме для изменения
    if (cardDataJson.data[1].data[i].key == userID) {
      var tmpStatusName = depCard.statusName + cardDataJson.data[1].data[i].id.substring(depCard.userid.length);
      var tmpFinishDate = depCard.finishDate + cardDataJson.data[1].data[i].id.substring(depCard.userid.length);

      for (var j = 0; j < cardDataJson.data[1].data.length; j++) {
        // изменение статуса
        if (statusUser) {
          if (cardDataJson.data[1].data[j].id == tmpStatusName) {
            cardDataJson.data[1].data[j].key = statusUser;
            cardDataJson.data[1].data[j].value = statusUser;
            tmpStatus = 1;
          }
        } else {
          tmpStatus = 1;
        }

        // изменение даты выхода
        if (cardDataJson.data[1].data[j].id == tmpFinishDate) {
          cardDataJson.data[1].data[j].key = finishDateKey;
          cardDataJson.data[1].data[j].value = finishDateValue;
          tmpFinish = 1;
        }
      }
      if (tmpStatus == 1 && tmpFinish == 1) {

        // формирование json строки для передачи в апи
        var cardData = '"data":[{"id":"cmp-w4ypt6","type":"label","label":"Декретный отпуск","value":" "},';
        cardData += '{"id":"' + depCard.table + '","type":"appendable_table","key":"","data":[';
        for (var i=0; i < cardDataJson.data[1].data.length; i++) {
          cardData+=obj2Str(cardDataJson.data[1].data[i])+',';
        }
        cardData=cardData.substring(0, cardData.length-1) + ']}]';

        createUser = 'NO';
        createTable = 'NO';
        updateUser = 'NO';
        resultVacate = 'Нет необходимости';
        break;
      }
    }
  }
} else if (createUser == 'YES') { // 6. если юзера нет в таблице, добавляем новую запись
  // 6.1. поиск номера последний записи в таблице
  for (var i = 0; i < cardDataJson.data[1].data.length; i++) {
  var tmpid = cardDataJson.data[1].data[i].id;
  }
  tmpid = '-b' + (Number(tmpid.substring(tmpid.indexOf('-b', 0)+2))+1);
  // 6.2. добавление новой записи в таблицу
  cardDataJson.data[1].data.push({id:depCard.userid+tmpid,type:"entity",value:userFIO,key:userID,formatVersion:"V1"},{id:depCard.statusName+tmpid,type:"listbox",value:statusUser,key:statusUser},{id:depCard.finishDate+tmpid,type:"date",value:finishDateValue,key:finishDateKey});

  // формирование json строки для передачи в апи
  var cardData = '"data":[{"id":"cmp-w4ypt6","type":"label","label":"Декретный отпуск","value":" "},';
  cardData += '{"id":"' + depCard.table + '","type":"appendable_table","key":"","data":[';
  for (var i=0; i < cardDataJson.data[1].data.length; i++) {
    cardData+=obj2Str(cardDataJson.data[1].data[i])+',';
  }
  cardData=cardData.substring(0, cardData.length-1) + ']}]';

  // 6.3.  снимаем пользователя с должности
  var resultVacate = runAPIRequestGET(synergy.apiPosVacate, '?positionID='+positionID+'&userID='+userID);
  resultVacate = eval("(" + resultVacate + ")");
  resultVacate = resultVacate.errorMessage;
}

// ##########################################################################


// 7. Сохраняем данные в карточке подразделения
var resultFormSave = runAPIRequestPOST(synergy.apiSaveData, depCard.formID, cardUUID, cardData);
resultFormSave = eval("(" + resultFormSave + ")");
if (resultFormSave.errorCode == 0) {
  resultFormSave = 'Действие выполнено';
} else {
  resultFormSave = 'error ' + resultVacate.errorCode;
}


} else {
  var resMsg='Специалист';
  var resultFormSave = 'Нет необходимости';
  var resultVacate = 'Нет необходимости';
}

form.save();
var result = true;
var message = 'Тип должности: ' + resMsg + '\nЗапись в карточку подразделения: ' + resultFormSave + '\nСнятие с должности: ' + resultVacate;
