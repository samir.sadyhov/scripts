// json парсинг
var newJson = {
  string: function(str) {
    str = uneval(str);
    var r = /\\u([\d\w]{4})/gi;
    str = str.replace(r, function(match, grp) {
      return String.fromCharCode(parseInt(grp, 16));
    });
    //формирование правильного формата data для передачи в api  "rest/api/asforms/data/save"
    str = unescape(str);
    str = str.replace(/id/g, '"id"');
    str = str.replace(/type/g, '"type"');
    str = str.replace(/label/g, '"label"');
    str = str.replace(/valueID/g, '"valueID"');
    str = str.replace(/value/g, '"value"');
    str = str.replace(/key/g, '"key"');
    str = str.replace(/username/g, '"username"');
    str = str.replace(/userID/g, '"userID"');
    str = str.replace(/""value"ID"/g, '"valueID"');
    str = str.replace(/""label""/g, '"label"');
    str = str.replace(/data:/g, '"data": ');
    return str;
  },
  parse: function(str) {
    return eval("(" + str + ")");
  }
};

// наименование компонентов на карточке подразделения
var depCard = {
  formID: '1a37986a-a3c7-4541-ac42-12bf327eb53a',
  table: 'dekretTable',
  userid: 'user',
  statusName: 'status',
  finishDate: 'finishDate'
};
var userID = "5d3f79f2-c433-416c-ba2e-6eaf83381768";
var finishDate = '2016-05-30';
var statusUser = 'Отпуск по беременности и родам';

// Получаем ФИО пользователя
var userFIO = '{"userid": "5d3f79f2-c433-416c-ba2e-6eaf83381768",  "lastname": "Кривошипов",  "firstname": "Поршень",  "patronymic": "Коленвалович",  "mail": "",  "jid": "",  "code": "krivoshipov_porshen",  "access": "false",  "hr-manager": "false",  "admin": "false", "configurator": "false", "pointers-access": "false",  "positions": [{"positionID" : "16c72834-631e-4d50-a0be-925aa4cd02c5", "departmentID" : "845773f9-da04-4df2-9c67-8752f73fbe12", "positionName" : "Директор департамента", "departmentName" : "Департамент внедрения" }] , "modified": "2016-05-23 16:56:13" }';

userFIO = userFIO.substring(0, userFIO.indexOf('positions', 0) - 4) + '}';
userFIO = newJson.parse(userFIO);
var userLastname = userFIO.lastname;
var userFirstname = userFIO.firstname;
var userPatronymic = userFIO.patronymic;
userFIO = userLastname + ' ' + userFirstname.substring(0, 1) + '.';
if (userPatronymic.length > 1) {
  userFIO += ' ' + userPatronymic.substring(0, 1) + '.';
}

var cardUUID = "d3d80fb9-51df-4191-9f35-5095246243a4"

//var cardDataJson = '{"uuid":"d3d80fb9-51df-4191-9f35-5095246243a4", "version":4, "form":"1a37986a-a3c7-4541-ac42-12bf327eb53a", "formVersion":1, "modified":"2016-05-23 17:38:45.0", "nodeUUID":"0c643518-1728-46bc-8357-848b30a7e183", "data":[{"id":"cmp-w4ypt6","type":"label","label":"Декретный отпуск","value":" "},{"id":"dekretTable","type":"appendable_table","key":"","data":[{"id":"cmp-48f5rb","type":"label","label":"ФИО","value":" "},{"id":"cmp-p2hpo7","type":"label","label":"Статус","value":" "},{"id":"cmp-yds4gu","type":"label","label":"Дата выхода","value":" "},{"id":"user-b2","type":"entity","value":"Иванов И. И.","key":"f4f8c653-475b-4196-97e5-b61ee9c4241b","formatVersion":"V1"},{"id":"status-b2","type":"listbox","value":"В отпуске по уходу за ребенком","key":"В отпуске по уходу за ребенком"},{"id":"finishDate-b2","type":"date","value":"2016-05-10","key":"2016-05-10 00:00:00"},{"id":"user-b3","type":"entity","value":"Петров П. П.","key":"99472efa-0c9f-4369-98d8-bf9cc2896225","formatVersion":"V1"},{"id":"status-b3","type":"listbox","value":"Отпуск по беременности и родам","key":"Отпуск по беременности и родам"},{"id":"finishDate-b3","type":"date","value":"2016-05-19","key":"2016-05-19 00:00:00"},{"id":"user-b4","type":"entity","value":"Кривошипов П. К.","key":"5d3f79f2-c433-416c-ba2e-6eaf83381768","formatVersion":"V1"},{"id":"status-b4","type":"listbox","value":"В декрете","key":"В декрете"},{"id":"finishDate-b4","type":"date","value":"2016-05-23","key":"2016-05-23 00:00:00"}]}]}';

//var cardDataJson = '{"uuid":"d3d80fb9-51df-4191-9f35-5095246243a4", "version":3, "form":"1a37986a-a3c7-4541-ac42-12bf327eb53a", "formVersion":1, "modified":"2016-05-23 17:20:28.0", "nodeUUID":"0c643518-1728-46bc-8357-848b30a7e183", "data":[{"id":"cmp-w4ypt6","type":"label","label":"Декретный отпуск","value":" "},{"id":"dekretTable","type":"appendable_table","key":"","data":[{"id":"cmp-48f5rb","type":"label","label":"ФИО","value":" "},{"id":"cmp-p2hpo7","type":"label","label":"Статус","value":" "},{"id":"cmp-yds4gu","type":"label","label":"Дата выхода","value":" "}]}]}';

var cardDataJson = '{"uuid":"d3d80fb9-51df-4191-9f35-5095246243a4", "version":4, "form":"1a37986a-a3c7-4541-ac42-12bf327eb53a", "formVersion":1, "modified":"2016-05-23 17:38:45.0", "nodeUUID":"0c643518-1728-46bc-8357-848b30a7e183", "data":[{"id":"cmp-w4ypt6","type":"label","label":"Декретный отпуск","value":" "},{"id":"dekretTable","type":"appendable_table","key":"","data":[{"id":"cmp-48f5rb","type":"label","label":"ФИО","value":" "},{"id":"cmp-p2hpo7","type":"label","label":"Статус","value":" "},{"id":"cmp-yds4gu","type":"label","label":"Дата выхода","value":" "},{"id":"user-b2","type":"entity","value":"Иванов И. И.","key":"f4f8c653-475b-4196-97e5-b61ee9c4241b","formatVersion":"V1"},{"id":"status-b2","type":"listbox","value":"В отпуске по уходу за ребенком","key":"В отпуске по уходу за ребенком"},{"id":"finishDate-b2","type":"date","value":"2016-05-10","key":"2016-05-10 00:00:00"},{"id":"user-b3","type":"entity","value":"Петров П. П.","key":"99472efa-0c9f-4369-98d8-bf9cc2896225","formatVersion":"V1"},{"id":"status-b3","type":"listbox","value":"Отпуск по беременности и родам","key":"Отпуск по беременности и родам"},{"id":"finishDate-b3","type":"date","value":"2016-05-19","key":"2016-05-19 00:00:00"},{"id":"user-b4","type":"entity","value":"Кривошипов П. К.","key":"5d3f79f2-c433-416c-ba2e-6eaf83381768","formatVersion":"V1"},{"id":"status-b4","type":"listbox","value":"В декрете","key":"В декрете"},{"id":"finishDate-b4","type":"date","value":"2016-05-23","key":"2016-05-23 00:00:00"}]}]}';

cardDataJson = newJson.parse(cardDataJson);
var tmpStatus = 0,
  tmpFinish = 0;
var updateUser = 'NO';
var createUser = 'NO';
var createTable = 'NO';
var tmp1 = 0;

// проверяем есть ли записи в таблице
for (var i = 0; i < cardDataJson.data[1].data.length; i++) {
  var tmpid = cardDataJson.data[1].data[i].id.substring(0, depCard.userid.length);
  if (tmpid == depCard.userid) {
    tmp1++;
  }
  if (tmp1 > 0) {
    createTable = 'NO';
    break;
  } else {
    createTable = 'YES';
  }
}

if (createTable == 'YES') {
  // если пустая то создаем
  console.log('create table');

  // создаем запись в таблице

  // 1. начало формы, с наименованием
  var cardData = '"data":[{"id":"cmp-w4ypt6","type":"label","label":"Декретный отпуск","value":" "},';
  // 2. добавляем начало таблицы
  cardData += '{"id":"' + depCard.table + '","type":"appendable_table","key":"","data":[';
  // 3. формируем заголовок таблицы из 3-х столбцов
  for (var i = 0; i < 3; i++) {
    cardData += '{"id": "' + cardDataJson.data[1].data[i].id + '", "type": "' + cardDataJson.data[1].data[i].type + '", "label": "' + cardDataJson.data[1].data[i].label + '", "value": "' + cardDataJson.data[1].data[i].value + '"},';
  }
  // 4. добавляем строку в таблицу
  cardData += '{"id":"' + depCard.userid + '-b1","type":"entity","value":"' + userFIO + '","key":"' + userID + '","formatVersion":"V1"},{"id":"' + depCard.statusName + '-b1","type":"listbox","value":"' + statusUser + '","key":"' + statusUser + '"},{"id":"' + depCard.finishDate + '-b1","type":"date","value":"' + finishDate + '","key":"' + finishDate + ' 00:00:00"}]}]';

  // 5. Увольнение сотрудника
  //var resultVacate = runAPIRequestGET(synergy.apiPosVacate, '?positionID='+positionID+'&userID='+userID);
  //resultVacate = eval("(" + resultVacate + ")");
  //resultVacate = resultVacate.errorMessage;

  // иначе проверяем необходимость создания или изменения юзера
} else {
  tmp1 = 0;
  for (var i = 0; i < cardDataJson.data[1].data.length; i++) {
    var tmpid = cardDataJson.data[1].data[i].id.substring(0, depCard.userid.length);
    if (cardDataJson.data[1].data[i].key == userID) {
      tmp1++;
    }
  }
  if (tmp1 > 0) {
    // нужно изменить статус и дату выходу у юзера
    updateUser = 'YES';
  } else {
    // нужно создать новую запись в таблице по данному юзеру
    createUser = 'YES';
  }
}

if (updateUser == 'YES') {
  console.log('update User');
  for (var i = 0; i < cardDataJson.data[1].data.length; i++) {
    var tmpid = cardDataJson.data[1].data[i].id.substring(0, depCard.userid.length);
    // находим наименования компонентов на форме для изменения
    if (cardDataJson.data[1].data[i].key == userID) {
      var tmpStatusName = depCard.statusName + cardDataJson.data[1].data[i].id.substring(depCard.userid.length);
      var tmpFinishDate = depCard.finishDate + cardDataJson.data[1].data[i].id.substring(depCard.userid.length);

      for (var j = 0; j < cardDataJson.data[1].data.length; j++) {
        // изменение статуса
        if (cardDataJson.data[1].data[j].id == tmpStatusName) {
          cardDataJson.data[1].data[j].key = statusUser;
          cardDataJson.data[1].data[j].value = statusUser;
          tmpStatus = 1;
        }
        // изменение даты выхода
        if (cardDataJson.data[1].data[j].id == tmpFinishDate) {
          cardDataJson.data[1].data[j].key = finishDate + ' 00:00:00';
          cardDataJson.data[1].data[j].value = finishDate;
          tmpFinish = 1;
        }
      }
      if (tmpStatus == 1 && tmpFinish == 1) {
        var cardData = cardDataJson.data;
        cardData = '"data":' + newJson.string(cardData);
        createUser = 'NO';
        createTable = 'NO';
        updateUser = 'NO';
        break;
      }
    }
  }
} else if (createUser == 'YES') {
  console.log('create USER');
// поиск номера последний записи в таблице
for (var i = 0; i < cardDataJson.data[1].data.length; i++) {
var tmpid = cardDataJson.data[1].data[i].id;
}
tmpid = '-b' + (Number(tmpid.substring(tmpid.indexOf('-b', 0)+2))+1);
// добвление новой записи в таблицу
cardDataJson.data[1].data.push({id:depCard.userid+tmpid,type:"entity",value:userFIO,key:userID,formatVersion:"V1"},{id:depCard.statusName+tmpid,type:"listbox",value:statusUser,key:statusUser},{id:depCard.finishDate+tmpid,type:"date",value:finishDate,key:finishDate + " 00:00:00"});
cardDataJson = cardDataJson.data;
var cardData = '"data":' +  newJson.string(cardDataJson);

}


console.log(cardData);
console.log('Обновить данные юзера? ' + updateUser + '\nСоздать юзера? ' + createUser + '\nСоздать таблицу и добавить юзера? ' + createTable);
