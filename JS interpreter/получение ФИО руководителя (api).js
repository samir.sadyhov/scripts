var form = platform.getFormsManager().getFormData(dataUUID);
form.load();

var synergy = {
  login: "Administrator",
  password: "qazwsx123",
  url: "http://127.0.0.1:8080/Synergy",
  apiUser: "/rest/api/filecabinet/user/",
  apiDep: "/rest/api/departments/get?departmentID="
};

// получаем ID автора
var userid = form.getValue("autor");

// получение результата api функции как json объект
function getAPIresult(api, param) {
  var APIString = '';
  param == null ? APIString = synergy.url + api : APIString = synergy.url + api + param;
  var get = new org.apache.commons.httpclient.methods.GetMethod(APIString);
  var client = new org.apache.commons.httpclient.HttpClient();
  var creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(synergy.login, synergy.password);
  client.getParams().setAuthenticationPreemptive(true);
  client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
  get.setRequestHeader("Content-type", "application/json; charset=utf-8");
  var status = client.executeMethod(get);
  var result = get.getResponseBodyAsString();
  result =  eval("(" + result + ")");
  return result;
  get.releaseConnection();
}

// получение ID департамента автора
var depID = getAPIresult(synergy.apiUser, userid);
depID = depID.positions[0].departmentID;

// получение ID руководителя департамента
var bossID = getAPIresult(synergy.apiDep, depID);
bossID = bossID.manager.managerID;

// получение ФИО руководителя департамента
var bossFIO = getAPIresult(synergy.apiUser, bossID);
bossFIO = bossFIO.lastname + ' ' + bossFIO.firstname + ' ' + bossFIO.patronymic;

// запись ФИО руководителя департамента на форму
form.setValue('cheef', bossFIO);

form.save();
var result = true;
var message = bossFIO + " красаучик есьжи!";
