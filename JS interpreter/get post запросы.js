var synergy = {
  login: "1",
  password: "1",
  url: "http://127.0.0.1:8080/Synergy",
  apiPos: "/rest/api/positions/get?positionID=",
  apiDep: "/rest/api/departments/get?departmentID=",
  apiCardDep: "/rest/api/departments/get_cards?departmentID=",
  apiRegID: "/rest/api/asforms/data/document?dataUUID=",
  apiDict: "/rest/api/dictionary/get_by_code?dictionaryCode=",
  apiPosVacate: "/rest/api/positions/discharge",
  apiGetData: "/rest/api/asforms/data/",
  apiSaveData: "/rest/api/asforms/data/save",
  apiUserData: "/rest/api/filecabinet/user/"
};

// получение результата api функции
function runAPIRequestGET(api, param) {
  var APIString = '';
  param == null ? APIString = synergy.url + api : APIString = synergy.url + api + param;
  var get = new org.apache.commons.httpclient.methods.GetMethod(APIString);
  var client = new org.apache.commons.httpclient.HttpClient();
  var creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(synergy.login, synergy.password);
  client.getParams().setAuthenticationPreemptive(true);
  client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
  get.setRequestHeader("Content-type", "application/json; charset=utf-8");
  var status = client.executeMethod(get);
  var result = get.getResponseBodyAsString();
  get.releaseConnection();
  return result;
}

// //сохранение данных по форме
function runAPIRequestPOST(api, form, dataUUID, data) {
  var APIString = synergy.url + api;
  var post = new org.apache.commons.httpclient.methods.PostMethod(APIString);
  post.addParameter("formUUID", form);
  post.addParameter("uuid", dataUUID);
  post.addParameter("data", data);
  var client = new org.apache.commons.httpclient.HttpClient();
  var creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(synergy.login, synergy.password);
  client.getParams().setAuthenticationPreemptive(true);
  client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
  post.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=utf-8");
  var status = client.executeMethod(post);
  var result = post.getResponseBodyAsString();
  post.releaseConnection();
  return result;
}
