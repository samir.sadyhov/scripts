var form = platform.getFormsManager().getFormData(dataUUID);
form.load();

// общие настройки
var synergy = {
  login: "kamyrov",
  password: "123456",
  url: "http://127.0.0.1:8080/Synergy/"
};

// настройки справочника
var dictionary = {
  code: 'dolzhnost',
  columns: {
    ru: '91b3fd92-762c-4dfc-b44d-43fb350e57ae',
    kz: 'dddcbd7b-5a0c-4e05-9ddf-b7d46bc3444a',
    en: '17177168-5f74-4161-894a-b72c73511ece'
  }
};

var department = form.getValue('kafedra');
var position = form.getValue('dolzhnost');

// Получаем значения справочника
var get = new org.apache.commons.httpclient.methods.GetMethod(synergy.url + 'rest/api/dictionary/get_by_code?dictionaryCode=' + dictionary.code);
var client = new org.apache.commons.httpclient.HttpClient();
var creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(synergy.login, synergy.password);
client.getParams().setAuthenticationPreemptive(true);
client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
get.setRequestHeader("Content-type", "application/json; charset=utf-8");
var status = client.executeMethod(get);
var dictionaryData = get.getResponseBodyAsString();
get.releaseConnection();

dictionaryData = eval("(" + dictionaryData + ")");
dictionaryData = dictionaryData.items;

// поиск строки в справочнике
var items = false;
for (var dd = 0; dd < dictionaryData.length; dd++) {
  for (var dd2 = 0; dd2 < dictionaryData[dd].values.length; dd2++) {
    if (dictionaryData[dd].values[dd2].columnID == dictionary.columns.ru) {
      if (dictionaryData[dd].values[dd2].value == position) {
        items = dictionaryData[dd].itemID;
        break;
      }
    }
  }
}

// получение значений справочника
var tmpRu = null, tmpKz = null, tmpEn = null;
if (items) {
  for (var dd = 0; dd < dictionaryData.length; dd++) {
    if (dictionaryData[dd].itemID == items) {
      for (var dd2 = 0; dd2 < dictionaryData[dd].values.length; dd2++) {
        if (dictionaryData[dd].values[dd2].columnID == dictionary.columns.ru) {
          tmpRu = dictionaryData[dd].values[dd2].value;
        }
      }
    }
    if (dictionaryData[dd].itemID == items) {
      for (var dd2 = 0; dd2 < dictionaryData[dd].values.length; dd2++) {
        if (dictionaryData[dd].values[dd2].columnID == dictionary.columns.kz) {
          tmpKz = dictionaryData[dd].values[dd2].value;
        }
      }
    }
    if (dictionaryData[dd].itemID == items) {
      for (var dd2 = 0; dd2 < dictionaryData[dd].values.length; dd2++) {
        if (dictionaryData[dd].values[dd2].columnID == dictionary.columns.en) {
          tmpEn = dictionaryData[dd].values[dd2].value;
        }
      }
    }
  }
}
if (!tmpRu) tmpRu='не найдено значение на русском';
if (!tmpKz) tmpKz='не найдено значение на казахском';
if (!tmpEn) tmpEn='не найдено значение на английском';

// создание должности в орг.структуре
var post = new org.apache.commons.httpclient.methods.PostMethod(synergy.url + 'rest/api/positions/save?locale=ru');
var client = new org.apache.commons.httpclient.HttpClient();
var creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(synergy.login, synergy.password);
client.getParams().setAuthenticationPreemptive(true);
client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
post.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=utf-8");
post.addParameter("locale", "ru");
post.addParameter("nameRu", tmpRu);
post.addParameter("nameKz", tmpKz);
post.addParameter("nameEn", tmpEn);
post.addParameter("departmentID", department);
post.addParameter("positionType", '2');
post.addParameter("pointersCode", guid());
var status = client.executeMethod(post);
var errorMsg = post.getResponseBodyAsString();
post.releaseConnection();
errorMsg = eval("(" + errorMsg + ")");

// генератор айдишнеков
function guid(){
  function chr4(){
    return Math.random().toString(16).slice(-4);
  }
  return chr4() + chr4() +
    '-' + chr4() +
    '-' + chr4() +
    '-' + chr4() +
    '-' + chr4() + chr4() + chr4();
}

form.save();
var result = true;
var message = errorMsg.errorMessage + '\npositionID: '+errorMsg.positionID;
