var form = platform.getFormsManager().getFormData(dataUUID);
form.load();

// общие настройки
var synergy = {
  login: "1",
  password: "1",
  url: "http://127.0.0.1:8080/Synergy"
};

// АКТ классификационного освидетельствования судна
var akt = {
    formID: '1999da42-4590-494c-bf87-6d2d4274af4d',
    regNumber: 'reg_nomer',
    date: 'data',
    dateE: {pred: 'pred_data_ezh', sled: 'sled_data_ezh'},
    dateO: {pred: 'pred_data_och', sled: 'sled_data_och'},
    dateS: {pred: 'pred_data_dok', sled: 'sled_data_dok'},
    dateK: {pred: 'pred_data_klass', sled: 'sled_data_klass'}
};

// получение результата api функции
function runAPIRequestGET(api, param) {
  var APIString = '';
  param == null ? APIString = synergy.url + api : APIString = synergy.url + api + param;
  var get = new org.apache.commons.httpclient.methods.GetMethod(APIString);
  var client = new org.apache.commons.httpclient.HttpClient();
  var creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(synergy.login, synergy.password);
  client.getParams().setAuthenticationPreemptive(true);
  client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
  get.setRequestHeader("Content-type", "application/json; charset=utf-8");
  var status = client.executeMethod(get);
  var result = get.getResponseBodyAsString();
  get.releaseConnection();
  return result;
}

// функция преобразования дат
// на входе принимает дату вида "2016-01-01 00:00:00" и прибавляет t лет
function getDateKey(dateStr, t) {
  // 2016-01-01 00:00:00
  return (Number(dateStr.substring(0,4))+Number(t))+dateStr.substring(4);
}

// Получение текущей даты в формате "2016-01-01 00:00:00"
var today = new Date();
var month = Number( today.getMonth() + 1 );
if (month < 10) { month = '0' + month;}
var currentDate =  today.getFullYear() + '-' + (month) + '-' + ('0'+today.getDate()).slice(-2) + ' 00:00:00';

// получаем дату освидетельствования
var dateOsvid = form.getValue(akt.date);

// Получаем "Регистровый №", будет осуществлен поиск предыдущей записи по этому значению
var regNumber = form.getValue(akt.regNumber);

// Формирование поисковой строки
var paramSearch = '{"query": "where uuid = \''+akt.formID+'\' and `'+akt.regNumber+'.TEXT` = \''+regNumber+'\' order by `'+akt.date+'.DATE` desc", "parameters": ["'+akt.regNumber+'.TEXT", "'+akt.date+'.DATE"], "showDeleted": "false", "searchInRegistry": "true", "recordsCount": "10"}';

// Поиск предыдущей записи по судну
var APIString = synergy.url + "/rest/api/asforms/search/advanced";
var post = new org.apache.commons.httpclient.methods.PostMethod(APIString);
var client = new org.apache.commons.httpclient.HttpClient();
var creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(synergy.login, synergy.password);
client.getParams().setAuthenticationPreemptive(true);
client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
post.setRequestHeader("Content-type", "application/json; charset=utf-8");
post.setRequestBody(paramSearch);
var status = client.executeMethod(post);
var previousRecord = post.getResponseBodyAsString();
previousRecord = eval("(" + previousRecord + ")");

var resMsg = 'Нет записи по данному судну';

if (previousRecord.length >= 2) {

  for (var i = 0; i < previousRecord.length; i++) {
    if (previousRecord[i].registryRecordStatus == 'STATE_SUCCESSFUL') {
      previousRecord = previousRecord[i].dataUUID;
      break;
    }
  }

  var previousDataOnTheShip = runAPIRequestGET("/rest/api/asforms/data/", previousRecord);
  previousDataOnTheShip = eval("(" + previousDataOnTheShip + ")");

  for (var i = 0; i < previousDataOnTheShip.data.length; i++) {
    if (previousDataOnTheShip.data[i].id == akt.dateE.sled) {
      form.setValue(akt.dateE.pred, previousDataOnTheShip.data[i].key); // Предыдущая дата Ежегодное
      form.setValue(akt.dateE.sled, getDateKey(dateOsvid, 1)); //Следующая дата Ежегодное (+ 1 год)
    }
    if (previousDataOnTheShip.data[i].id == akt.dateO.sled) {
      form.setValue(akt.dateO.pred, previousDataOnTheShip.data[i].key); // Предыдущая дата Очередное
      if (previousDataOnTheShip.data[i].key >= currentDate) {
        form.setValue(akt.dateO.sled, previousDataOnTheShip.data[i].key); // Следующая дата Очередное (если след.дата больше текущей даты)
      } else {
        form.setValue(akt.dateO.sled, getDateKey(previousDataOnTheShip.data[i].key, 5)); // Следующая дата Очередное (+ 5 лет)
      }
    }
    if (previousDataOnTheShip.data[i].id == akt.dateS.sled) {
      form.setValue(akt.dateS.pred, previousDataOnTheShip.data[i].key); // Предыдущая дата На слипе
      if (previousDataOnTheShip.data[i].key >= currentDate) {
        form.setValue(akt.dateS.sled, previousDataOnTheShip.data[i].key); // Следующая дата На слипе (если след.дата больше текущей даты)
      } else {
        form.setValue(akt.dateS.sled, getDateKey(previousDataOnTheShip.data[i].key, 5)); // Следующая дата На слипе (+ 5 лет)
      }
    }
    if (previousDataOnTheShip.data[i].id == akt.dateK.sled) {
      form.setValue(akt.dateK.pred, previousDataOnTheShip.data[i].key); // Предыдущая дата Классификационное
      form.setValue(akt.dateK.sled, getDateKey(dateOsvid, 5)); // Следующая дата Классификационное (+ 5 лет)
    }
  }
  resMsg = 'Ok';
}

form.save();
var result = true;
var message = resMsg;
