var form = platform.getFormsManager().getFormData(dataUUID);
form.load();

// общие настройки
var synergy = {
  login: "1",
  password: "1",
  url: "http://127.0.0.1:8080/Synergy"
};

// АКТ очередного освидетельствования судна
var akt = {
    formID: 'd2e602ce-7cb7-4ce6-a505-89bce82b5e4a',
    regNumber: 'reg_nomer',
    date: 'data_osvid',
    dateE: {pred: 'pred_data_ezhegod', sled: 'sled_data_ezhegodnoe'},
    dateO: {pred: 'pred_data_ocherednoe', sled: 'sled_data_ochered'},
    dateS: {pred: 'pred_data__dok', sled: 'sled_data_dok'},
    dateK: {pred: 'pred_data_klassific', sled: 'sled_data_klass'}
};

// АКТ ежегодного освидетельствования судна
var aktYear = {
    formID: '8052dc2b-abc6-49e2-b92a-da2ea170b3e3',
    regNumber: 'reg_nomer',
    date: 'year1',
    dateE: {pred: 'ezh1', sled: 'ezh2'},
    dateO: {pred: 'och1', sled: 'och2'},
    dateS: {pred: 'slip1', sled: 'slip2'},
    dateK: {pred: 'klas1', sled: 'klas2'}
};

// получение результата api функции
function runAPIRequestGET(api, param) {
  var APIString = '';
  param == null ? APIString = synergy.url + api : APIString = synergy.url + api + param;
  var get = new org.apache.commons.httpclient.methods.GetMethod(APIString);
  var client = new org.apache.commons.httpclient.HttpClient();
  var creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(synergy.login, synergy.password);
  client.getParams().setAuthenticationPreemptive(true);
  client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
  get.setRequestHeader("Content-type", "application/json; charset=utf-8");
  var status = client.executeMethod(get);
  var result = get.getResponseBodyAsString();
  get.releaseConnection();
  return result;
}

// функция преобразования дат
// на входе принимает дату вида "2016-01-01 00:00:00" и прибавляет t лет
function getDateKey(dateStr, t) {
  // 2016-01-01 00:00:00
  return (Number(dateStr.substring(0,4))+Number(t))+dateStr.substring(4);
}

// Получение текущей даты в формате "2016-01-01 00:00:00"
var today = new Date();
var month = Number( today.getMonth() + 1 );
if (month < 10) { month = '0' + month;}
var currentDate =  today.getFullYear() + '-' + (month) + '-' + ('0'+today.getDate()).slice(-2) + ' 00:00:00';

// получаем дату освидетельствования
var dateOsvid = form.getValue(akt.date);

// Получаем "Регистровый №", будет осуществлен поиск предыдущей записи по этому значению
var regNumber = form.getValue(akt.regNumber);

// Формирование поисковой строки (акт ежегодного освидетельствования судна)
var paramSearchYear = '{"query": "where uuid = \''+aktYear.formID+'\' and `'+akt.regNumber+'.TEXT` = \''+regNumber+'\' order by `'+aktYear.date+'.DATE` desc", "parameters": ["'+akt.regNumber+'.TEXT", "'+aktYear.date+'.DATE"], "showDeleted": "false", "searchInRegistry": "true", "recordsCount": "10"}';

// Поиск последней записи по судну из акта ежегодного освидетельствования судна
var APIString = synergy.url + "/rest/api/asforms/search/advanced";
var post = new org.apache.commons.httpclient.methods.PostMethod(APIString);
var client = new org.apache.commons.httpclient.HttpClient();
var creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(synergy.login, synergy.password);
client.getParams().setAuthenticationPreemptive(true);
client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
post.setRequestHeader("Content-type", "application/json; charset=utf-8");
post.setRequestBody(paramSearchYear);
var status = client.executeMethod(post);
var previousRecordYear = post.getResponseBodyAsString();
previousRecordYear = eval("(" + previousRecordYear + ")");

var yearDate=null;
if (previousRecordYear.length >= 2) {
  for (var i = 0; i < previousRecordYear.length; i++) {
    if (previousRecordYear[i].registryRecordStatus == 'STATE_SUCCESSFUL') {
      previousRecordYear = previousRecordYear[i].dataUUID;
      break;
    }
  }
  var previousDataYear = runAPIRequestGET("/rest/api/asforms/data/", previousRecordYear);
  previousDataYear = eval("(" + previousDataYear + ")");
  for (var i = 0; i < previousDataYear.data.length; i++) {
    if (previousDataYear.data[i].id == aktYear.dateK.sled) {
      yearDate=previousDataYear.data[i].key; // вытаскиваем дату след. классифик. из последнего акта ежегодного освидетельствования судна
    }
  }
}



// Формирование поисковой строки
var paramSearch = '{"query": "where uuid = \''+akt.formID+'\' and `'+akt.regNumber+'.TEXT` = \''+regNumber+'\' order by `'+akt.date+'.DATE` desc", "parameters": ["'+akt.regNumber+'.TEXT", "'+akt.date+'.DATE"], "showDeleted": "false", "searchInRegistry": "true", "recordsCount": "10"}';

// Поиск предыдущей записи по судну
var APIString = synergy.url + "/rest/api/asforms/search/advanced";
var post = new org.apache.commons.httpclient.methods.PostMethod(APIString);
var client = new org.apache.commons.httpclient.HttpClient();
var creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(synergy.login, synergy.password);
client.getParams().setAuthenticationPreemptive(true);
client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);
post.setRequestHeader("Content-type", "application/json; charset=utf-8");
post.setRequestBody(paramSearch);
var status = client.executeMethod(post);
var previousRecord = post.getResponseBodyAsString();
previousRecord = eval("(" + previousRecord + ")");

var resMsg = 'Нет записи по данному судну';

if (previousRecord.length >= 2) {

  for (var i = 0; i < previousRecord.length; i++) {
    if (previousRecord[i].registryRecordStatus == 'STATE_SUCCESSFUL') {
      previousRecord = previousRecord[i].dataUUID;
      break;
    }
  }

  var previousDataOnTheShip = runAPIRequestGET("/rest/api/asforms/data/", previousRecord);
  previousDataOnTheShip = eval("(" + previousDataOnTheShip + ")");

  for (var i = 0; i < previousDataOnTheShip.data.length; i++) {
    if (previousDataOnTheShip.data[i].id == akt.dateE.sled) {
      form.setValue(akt.dateE.pred, previousDataOnTheShip.data[i].key); // Предыдущая дата Ежегодное
      if (yearDate) {
        form.setValue(akt.dateE.sled, getDateKey(yearDate, 1)); //Следующая дата Ежегодное (+ 1 год)
      }
    }
    if (previousDataOnTheShip.data[i].id == akt.dateO.sled) {
      form.setValue(akt.dateO.pred, previousDataOnTheShip.data[i].key); // Предыдущая дата Очередное
      form.setValue(akt.dateO.sled, getDateKey(dateOsvid, 5)); // Следующая дата Очередное (+ 5 лет)
    }
    if (previousDataOnTheShip.data[i].id == akt.dateS.sled) {
      form.setValue(akt.dateS.pred, previousDataOnTheShip.data[i].key); // Предыдущая дата На слипе
      form.setValue(akt.dateS.sled, getDateKey(dateOsvid, 5)); // Следующая дата На слипе (+ 5 лет)
    }
    if (previousDataOnTheShip.data[i].id == akt.dateK.sled) {
      form.setValue(akt.dateK.pred, previousDataOnTheShip.data[i].key); // Предыдущая дата Классификационное
      if (yearDate) {
        form.setValue(akt.dateK.sled, yearDate); // Следующая дата Классификационное
      }
    }
  }
  resMsg = 'Ok';
}

form.save();
var result = true;
var message = resMsg;
