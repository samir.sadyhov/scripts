function init(){
  let api = AS.FORMS.ApiUtils;
  let userID = AS.OPTIONS.currentUser.userId;

  AS.SERVICES.showWaitWindow();

  jQuery.when(api.simpleAsyncGet(`/rest/api/filecabinet/user/${userID}?getGroups=false&locale=${AS.OPTIONS.locale}`))
  .then(currentUser => {
    if(currentUser.positions && currentUser.positions.length > 0) {
      return api.simpleAsyncGet(`/rest/api/departments/get?departmentID=${currentUser.positions[0].departmentID}`);
    }
  })
  .then(userDep => {
    if(userDep.manager && userDep.manager.managerID) {
      return api.simpleAsyncGet(`/rest/api/userchooser/getUserInfo?userID=${userDep.manager.managerID}`)
    }
  })
  .then(headUser => {
    model.setValue(headUser[0].personName);
    AS.SERVICES.hideWaitWindow();
  })
  .fail(error => {
    console.warn(error);
    AS.SERVICES.hideWaitWindow();
  });
}

model.playerModel.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
  init();
});
