var HOSTNAME = 'http://192.168.2.151:3000';

function statusChange(uuid, status) {
  var client = new org.apache.commons.httpclient.HttpClient();
  var get = new org.apache.commons.httpclient.methods.GetMethod(HOSTNAME + '/api/forms/status_change?status=' + status + '&uuid=' + uuid);
  get.setRequestHeader("Content-type", "application/json; charset=utf-8");
  client.executeMethod(get);
  var resp = get.getResponseBodyAsString();
  get.releaseConnection();
  return JSON.parse(resp);
}

var result = true;
var message = "ok";

try {
  var resultChange = statusChange(dataUUID, 1);
  if(resultChange.status == 200) {
    message = "Статус услуги изменен";
  } else {
    message = "Ошибка при смене статуса: " + resultChange.errorMessage;
  }
} catch (err) {
  message = err;
}
