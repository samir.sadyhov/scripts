function getInfoNobd(iin, success, fail) {
  AS.SERVICES.showWaitWindow();
  let filter = {iin: iin};
  jQuery.ajax({
    crossDomain: true,
    url: 'https://nobd-api.iac.kz/api/public/info/search/personal_data?filter=' + JSON.stringify(filter),
    dataType: 'json',
    success: function(res) {
      success(res);
    },
    error: function(e){
      fail(e);
    }
  });
}

function testIIN(iin) {
  if(!iin) return false;
  return iin.match(/^\d+$/);
}

function validIIIN(iin) {
  let s = 0;
  for (let i = 0; i < 11; i++) {
    s = s + (i + 1) * iin[i];
  }
  let k = s % 11;
  if (k === 10) {
    s = 0;
    for (let i = 0; i < 11; i++) {
      let t = (i + 3) % 11;
      if (t === 0) t = 11;
      s = s + t * iin[i];
    }
    k = s % 11;
    if (k === 10) return false;

    return (k === Number(iin.substring(11, 12)));
  }
  return (k === Number(iin.substring(11, 12)));
}

function getDate(iin) {
  let year = iin.substring(0, 2);
  let ep = Number(iin.substring(6, 7));
  switch (ep) {
    case 1:
    case 2:
      year = '18' + year;
      break;
    case 3:
    case 4:
      year = '19' + year;
      break;
    case 5:
    case 6:
      year = '20' + year;
      break;
  }
  return new Date(year, (iin.substring(2, 4) - 1), (iin.substring(4, 6)));
}

function getGender(iin) {
  let key = iin.substring(6, 7);
  if (key % 2 == 0) {
    return {value: 'женский', key: '4'};
  } else {
    return {value: 'мужской', key: '3'};
  }
}

function getAge(iin) {
  let birthDay = getDate(iin);
  return ((new Date().getTime() - birthDay) / (24 * 3600 * 365.25 * 1000)) | 0;
}

function init() {
  let age_baby = model.playerModel.getModelWithId('age_baby');
  let sex_baby = model.playerModel.getModelWithId('sex_baby');
  let date_birth_baby = model.playerModel.getModelWithId('date_birth_baby');
  let fio_baby = model.playerModel.getModelWithId('fio_baby');

  let iin = model.getValue();

  if (!testIIN(iin)) return;

  if(!validIIIN(iin)) {
    alert('Введен не верный ИИН');
    if(age_baby) age_baby.setValue(null);
    if(sex_baby) sex_baby.setValue('');
    if(date_birth_baby) date_birth_baby.setValue(null);
    if(fio_baby) fio_baby.setValue(null);
    model.setValue(null);
    return;
  }

  let age = getAge(iin);
  let gender = getGender(iin);

  if(age_baby) age_baby.setValue(age + '');
  if(sex_baby) sex_baby.setValue(gender.key);

  let birthDay = AS.FORMS.DateUtils.formatDate(getDate(iin), '${yyyy}-${mm}-${dd} ${HH}:${MM}:${SS}');
  if(date_birth_baby && birthDay) date_birth_baby.setValue(birthDay);

  getInfoNobd(iin, function(res){
    if(res) {
      res = res.result[0];
      if(fio_baby) fio_baby.setValue(res.lastName + ' ' + res.firstName + ' ' + res.middleName);
    }
    AS.SERVICES.hideWaitWindow();
  }, function(err){
    console.error(err);
    AS.SERVICES.hideWaitWindow();
  });

}

model.on(AS.FORMS.EVENT_TYPE.valueChange, function () {
  init();
});
