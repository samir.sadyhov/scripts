function init(){

  try {
    let qrCodeTXTModel = model.playerModel.getModelWithId('qr-code-txt');
    let viewIMG = view.playerView.getViewWithId(model.img_cmp);
    let modelIMG = model.playerModel.getModelWithId(model.img_cmp);

    if(qrCodeTXTModel.getValue()) {
      let base64string = qrCodeTXTModel.getValue();
      modelIMG.setValue(`data:image/png;base64,${base64string}`);
      viewIMG.container.first().find('img').attr("src", `data:image/png;base64,${base64string}`);
      modelIMG.asfProperty.config.url = `data:image/png;base64,${base64string}`;
      modelIMG.asfProperty.data.value = `data:image/png;base64,${base64string}`;
    } else {

      let settings = {
        "url": `${window.location.origin}/api/qrcode/generate`,
        "method": "POST",
        "headers": {
          "Content-Type": "application/json; charset=utf-8"
        },
        "data": JSON.stringify({
          "text": `${window.location.origin}/#/showNotify?uuid=${model.playerModel.asfDataId}`,
          "width": model.width || 100
        })
      };

      $.ajax(settings).done(result => {
        if (result.status == 200) {
          modelIMG.setValue(result.response);
          viewIMG.container.first().find('img').attr("src", result.response);
          modelIMG.asfProperty.config.url = result.response;
          modelIMG.asfProperty.data.value = result.response;

          qrCodeTXTModel.setValue(result.response.split(',')[1]);
          jQuery.when(AS.FORMS.ApiUtils.saveAsfData(model.playerModel.getAsfData().data, model.playerModel.formId, model.playerModel.asfDataId)).then(function (res) {
            console.log(res);
          }, function (err) {
            console.log(err);
          });

        } else {
          console.log(result);
        }
      });

    }

  } catch (e) {
    console.log(e);
  }

}

init();

model.playerModel.on(AS.FORMS.EVENT_TYPE.formShow, function() {
  init();
});
