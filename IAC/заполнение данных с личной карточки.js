function getUserInfo(userid){
  let result = null;
  jQuery.ajax({
    async: false,
    url: window.location.origin + '/api/user/search?userid=' + userid,
    dataType: 'json',
    success: function(res) {result = res;}
  });
  return result;
}

function init() {
  try {
    let value = model.getValue();
    if (value && value.length > 0) {

      let userInfo = getUserInfo(value[0].personID);
      if(userInfo && userInfo.status == 200 && userInfo.response.length > 0) {
        userInfo = userInfo.response[0];

        let iinModel = model.playerModel.getModelWithId('IIN');
        let lastNameModel = model.playerModel.getModelWithId('LastName');
        let firstNameModel = model.playerModel.getModelWithId('FirstName');
        let patronymicModel = model.playerModel.getModelWithId('MiddleName');
        let birthDateModel = model.playerModel.getModelWithId('Date_of_Birth');
        let emailModel = model.playerModel.getModelWithId('email');

        if(iinModel) iinModel.setValue(userInfo.iin);
        if(lastNameModel) lastNameModel.setValue(userInfo.lastname);
        if(firstNameModel) firstNameModel.setValue(userInfo.firstname);
        if(patronymicModel) patronymicModel.setValue(userInfo.patronymic);
        if(birthDateModel) birthDateModel.setValue(userInfo.birthDate);
        if(emailModel) emailModel.setValue(userInfo.email);

        //адресные данные
        let address_country = model.playerModel.getModelWithId('address_country'); //Место проживания: Страна*
        let address_region = model.playerModel.getModelWithId('address_region'); //Область*
        let address_area = model.playerModel.getModelWithId('address_area'); //Район*
        let address_locality = model.playerModel.getModelWithId('address_locality'); //Город
        let address_street = model.playerModel.getModelWithId('address_street'); //Улица*
        let address_house = model.playerModel.getModelWithId('address_house'); //Номер дома*

        if(address_country) address_country.setValue(userInfo.Country);
        if(address_region) address_region.setValue(userInfo.Districts);
        if(address_area) address_area.setValue((userInfo.City ? userInfo.Region : ''));
        if(address_locality) address_locality.setValue(userInfo.City ? userInfo.City : userInfo.Region);
        if(address_street) address_street.setValue(userInfo.Street);
        if(address_house) address_house.setValue(userInfo.Building + (userInfo.Flat ? 'кв. ' + userInfo.Flat : ''));
      }

    }
  } catch (e) {
    console.error(e);
  }
}

model.on(AS.FORMS.EVENT_TYPE.valueChange, function () {
  init();
});

model.playerModel.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
  init();
});
