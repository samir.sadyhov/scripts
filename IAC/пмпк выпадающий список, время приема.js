// model.cmpids = {
//     date: 'appointment_date',
//     organization: 'organization'
// }
// model.registries = {
//     schedule: 'grafik_raboty_pmpk',
//     current: 'showcase_registry_req2,showcase_registry_req3'
// }

'use strict';

let input = $('<div class="asf-dropdown-input">');
let button = $('<button class="asf-dropdown-button">');
let label = $(view.container).children(".asf-label");

view.container.append(input).append(button);

if (editable) {
  label.hide();
  input.show();
  button.show();
} else {
  label.show();
  button.hide();
  input.hide();
}

model.getAsfData = function(blockNumber){
  if(model.getValue()) {
    var result = AS.FORMS.ASFDataUtils.getBaseAsfData(model.asfProperty, blockNumber, model.getValue() , model.getValue());
    result.valueID = model.getValue() ;
    return result;
  } else {
    return AS.FORMS.ASFDataUtils.getBaseAsfData(model.asfProperty, blockNumber);
  }
};

model.setAsfData = function(asfData){
  if(!asfData || !asfData.value) {
    return;
  }
  model.setValue(asfData.value);
};

model.getSpecialErrors = function() {
  return;
};

view.updateValueFromModel = function () {
  if (model.getValue()) {
    label.text(model.getValue());
    input.text(model.getValue());
  } else {
    label.text("");
    input.text("");
  }
};

model.on(AS.FORMS.EVENT_TYPE.valueChange, function () {
  view.updateValueFromModel();
});

model.on(AS.FORMS.EVENT_TYPE.dataLoad, function () {
  view.updateValueFromModel();
});

view.markInvalid = function(){
   label.css("background-color", "#aa3344");
   input.css("background-color", "#aa3344");
};

view.unmarkInvalid = function(){
    input.css("background-color", "");
    label.css("background-color", "");
};

view.updateValueFromModel();


function getItems(organizationID, date, day){
  let result = [];

  let url = window.location.origin + '/api/pmpk/getTimeWork' +
  '?schedulecode=' + model.registries.schedule +
  '&orgcmpid=' + model.cmpids.organization +
  '&orgid=' + organizationID +
  '&day=' + day +
  '&curRegCode=' + model.registries.current +
  '&curDateCmp=' + model.cmpids.date +
  '&curDayCmp=' + model.asfProperty.id +
  '&curDate=' + date;

  $.ajax({
    async: false,
    url: url,
    dataType: 'json',
    success: function(res) {result = res.response;}
  });

  return result;
}

let selectDateModel = model.playerModel.getModelWithId(model.cmpids.date);
let selectOrgModel = model.playerModel.getModelWithId(model.cmpids.organization);

function init(){

  let items = [];
  let selectDate;
  let selectOrg;
  let selectDayNumber;

  if(selectDateModel.getValue() && AS.FORMS.DateUtils.parseDate(selectDateModel.getValue())) {
    selectDate = selectDateModel.getValue();
    selectOrg = selectOrgModel.getValue();
    selectDayNumber = AS.FORMS.DateUtils.parseDate(selectDate).getDay();
    items = getItems(selectOrg, selectDate, selectDayNumber);
  }

  button.on('click', function(){
    AS.SERVICES.showDropDown(items, input, 100, function(item){
      model.setValue(item);
    });
  });

  input.on('click', function(){
    AS.SERVICES.showDropDown(items, input, 100, function(item){
      model.setValue(item);
    });
  });
}

init();

selectOrgModel.on(AS.FORMS.EVENT_TYPE.valueChange, function () {
  selectDateModel.setValue(null);
  model.setValue(null);
  init();
});

selectDateModel.on(AS.FORMS.EVENT_TYPE.valueChange, function () {
  if(selectDateModel.getValue() && AS.FORMS.DateUtils.parseDate(selectDateModel.getValue())){
    let currDate = new Date();
    currDate.setDate(currDate.getDate() - 1);
    currDate = currDate.getTime();
    let selectDate = AS.FORMS.DateUtils.parseDate(selectDateModel.getValue()).getTime();
    if(selectDate < currDate) {
      alert("Вы не можете выбрать дату меньше текущей");
      selectDateModel.setValue(null);
    }
  }
  model.setValue(null);
  init();
});
