const eventShow = {type: 'set_hidden', hidden: false};
const eventHide = {type: 'set_hidden', hidden: true};

let tableContainer = $('.nlc-table-container');

let Paginator = {
  container: $('<div class="nlc-pt-container">'),
  paginator: $('<div class="nlc-pt-paginator">'),
  pContent: $('<div class="nlc-pt-paginator-content">'),
  bPrevious: $('<button class="nlc-pt-previous" disabled="disabled" title="Назад">'),
  bNext: $('<button class="nlc-pt-next" disabled="disabled" title="Вперед">'),
  label: $('<label>'),
  input: $('<input type="text">'),
  countInPart: 0,
  rows: 0,
  currentPage: 1,
  pages: 0,

  init: function(){
    $('.nlc-pt-container').remove();
    $('.nlc-registry-container').after(this.container);
    this.pContent.append(this.label).append(this.input);
    this.paginator.append(this.bPrevious).append(this.pContent).append(this.bNext);
    this.container.append(this.paginator);
    this.reset();

    this.bNext.click(() => {
      this.currentPage++;
      this.update();
      registryTable.createBody();
    });

    this.bPrevious.click(() => {
      this.currentPage--;
      this.update();
      registryTable.createBody();
    });

    this.label.on('dblclick', e => {
      e.preventDefault();
      this.input.show();
      this.label.hide();

      this.input.on('blur', () => {
        this.label.show();
        this.input.hide();
      });

      this.input.val("" + this.currentPage);

      this.input.on('keypress', e => {
        if (e.which != 8 && e.which != 0 && e.which != 46 && (e.which < 48 || e.which > 57)) return false;
      });

      this.input.on('keydown', e => {
        if(e.which === 13) {
          let inputVal = +this.input.val();
          if(inputVal && inputVal != this.currentPage && inputVal <= this.pages && inputVal >= 1){
            this.currentPage = inputVal;
            this.update();
            registryTable.createBody();
          }
          this.label.show();
          this.input.hide();
          this.input.off();
        }
      });
      this.input.focus();
    });

  },

  update: function(){
    this.pages = Math.ceil(this.rows / this.countInPart),
    this.label.text(this.currentPage + ' / ' + this.pages);

    if(this.pages == 0) {
      this.bPrevious.attr('disabled', 'disabled');
      this.bNext.attr('disabled', 'disabled');
    } else {
      if(this.currentPage == 1) {
        this.bPrevious.attr('disabled', 'disabled');
        if (this.currentPage == this.pages) {
          this.bNext.attr('disabled', 'disabled');
        } else {
          this.bNext.removeAttr('disabled');
        }
      } else {
        this.bPrevious.removeAttr('disabled');
        if (this.currentPage == this.pages) {
          this.bNext.attr('disabled', 'disabled');
        } else {
          this.bNext.removeAttr('disabled');
        }
      }
    }
  },

  reset: function(){
    this.countInPart = 15;
    this.rows = 15;
    this.currentPage = 1;
    this.pages = 0;
  }
}

let registryTable = {
  registryInfo: null,
  registryCode: null,
  registryID: null,
  registryName: '',
  registryRights: [],
  filterCode: null,
  formCode: null,
  formPlayerID: null,

  searchString: null,
  filterSearchUrl: null,

  allRights: [],

  sortCmpID: null,
  sortDesc: false,
  searchField: null,
  searchValue: null,
  heads: [],
  fields: [],

  registryTable: null,
  tHead: null,
  tBody: null,

  getNextFieldNumber: function(url) {
    let p = url.substring(url.indexOf('?') + 1).split('&');
    p = p.map(x => {
      x = x.split('=');
      if(x[0].indexOf('field') !== -1 && x[0] !== 'fields') return x[0];
    }).filter(x => x).sort();
    if(p.length) return Number(p[p.length - 1].substring(5)) + 1;
    return '';
  },

  getUrl: function(all){
    let url = `api/registry/data_ext?registryCode=${this.registryCode}`;
    if(!all) url += `&pageNumber=${Paginator.currentPage - 1}&countInPart=${Paginator.countInPart}`;
    if(this.filterCode) url+=`&filterCode=${this.filterCode}`;
    if(this.fields && this.fields.length > 0) this.fields.forEach(field => url+=`&fields=${field}`);
    if(this.sortCmpID) url+=`&sortCmpID=${this.sortCmpID}&sortDesc=${this.sortDesc}`;
    if(this.searchString) url += `&searchString=${this.searchString}`;
    if(this.filterSearchUrl) {
      url += this.filterSearchUrl;
      if(this.searchField && this.searchValue) {
        let next = this.getNextFieldNumber(this.filterSearchUrl);
        url+=`&field${next}=${this.searchField}&condition${next}=CONTAINS&value${next}=${this.searchValue}`;
      }
    } else {
      if(this.searchField && this.searchValue) url+=`&field=${this.searchField}&condition=CONTAINS&value=${this.searchValue}`;
    }
    return url;
  },

  createHeader: function() {
    this.tHead.empty();

    let tr = $('<tr>');
    let me = this;

    this.heads.forEach((item, i) => {
      let tmp_th = $('<th>', {class: "nlc-pt-header", headid: i});
      let tmp_label = $('<label class="columnLabel">').text(item.name);

      tmp_th.append(tmp_label);
      tr.append(tmp_th);
    });

    tr.append('<th class="nlc-pt-header" style="width: 45px;">');

    this.tHead.append(tr);
    this.createBody();
  },

  createBody: function() {
    rest.synergyGet(this.getUrl(), data => {
      this.tBody.empty();
      if(data.errorCode && data.errorCode != 0) {
        Paginator.rows = 0;
      } else {
        data.result.forEach(item => this.tBody.append(this.createRow(item)));
        Paginator.rows = data.recordsCount;
      }
      Paginator.update();
      tableContainer.scrollTop(0);

      if(this.searchString) {
        let labelText = `По вашему запросу найдено ${data.recordsCount} результатов`;
        fire({
          type: 'change_label',
          text: localizedText(labelText, labelText, labelText, labelText)
        }, 'label-search-result');
        fire({type: 'set_hidden', hidden: false}, 'label-search-result');
      } else {
        fire({type: 'set_hidden', hidden: true}, 'label-search-result');
      }

    });
  },

  removeRegistryRow: function(uuid) {
    UIkit.modal.confirm('Вы действительно хотите удалить запись реестра?').then(() => {
      Cons.showLoader();
      try {
        rest.synergyGet(`api/registry/delete_doc?dataUUID=${uuid}`, res => {
          if(res.errorCode != '0') throw new Error(res.errorMessage);
          showMessage("Запись реестра удалена", "success");
          this.createBody();
          Cons.hideLoader();
        });
      } catch (err) {
        Cons.hideLoader();
        showMessage("Произошла ошибка при удалении записи реестра", "error");
        console.log(error);
      }
    }, () => null);
  },

  createRow: function(dataRow) {
    let tr = $('<tr>');

    this.heads.forEach(item => {
      let td = $('<td>');
      if (dataRow.fieldValue.hasOwnProperty(item.cmpParent)) {
        let title = dataRow.fieldValue[item.cmpParent];
        td.append(`<span class="mobile-table-header">${item.name}</span>`);
        if(item.hasOwnProperty('progress') && item.progress) {
          td.append(this.getProgressBar(dataRow.fieldValue[item.cmpParent]));
        } else {
          td.append(`<span class="data-value parent">${dataRow.fieldValue[item.cmpParent]}</span>`);
          if(item.child && dataRow.fieldValue.hasOwnProperty(item.child.cmp)) {
            td.append(`<span class="data-value child">${item.child.prefix} ${dataRow.fieldValue[item.child.cmp]}</span>`);
            title += ` ${item.child.prefix} ${dataRow.fieldValue[item.child.cmp]}`;
          }
        }
        td.attr('title', title);
      }
      tr.append(td);
    });

    let tdAction = $('<td style="vertical-align: middle; text-align: end;">');
    tdAction.append('<span id="remove-row" title="Удалить" uk-icon="icon: trash"></span>');
    tdAction.append('<span id="open-row" title="Открыть" uk-icon="icon: push"></span>');
    tr.append(tdAction);

    tr.on('click', e => {
      let id = $(e.target).closest('span').attr('id');

      if(id) {
        if(id == 'remove-row') this.removeRegistryRow(dataRow.dataUUID);
        if(id == 'open-row') {
          const {openDocumentInWindow} = Cons.getAppStore();
          openDocumentInWindow(dataRow.dataUUID);
        }
      } else {
        this.tBody.find('tr').removeAttr('selected');
        tr.attr('selected', true);
        if(this.allRights.indexOf('rr_read') !== -1) {
          if(this.formPlayerID) fire({type: 'show_form_data', dataId: dataRow.dataUUID}, this.formPlayerID);
          if($('#panel-data').length) fire(eventShow, 'panel-data');
        } else {
          showMessage('У вас нет прав на просмотр этого документа', 'warning');
          if($('#panel-data').length) fire(eventHide, 'panel-data');
        }
      }

    });

    return tr;
  },

  getProgressBar: function(value) {
    let color = value < 100 ? '#597EF7' : 'rgba(26, 160, 83, 0.5)';
    let br = value < 100 ? '50px 0px 0px 50px' : '50px';
    let container = $('<div>', {class: 'progress-container'});
    let label = $('<div>', {style: 'color: #232D42; font-size: 16px; line-height: 175%;'}).text(`${value}%`);
    let progress = $('<div>', {style: 'width: 100px; height: 10px; background: #E9ECEF; box-shadow: inset 0px 4px 8px rgb(0 0 0 / 10%); border-radius: 8px;'});
    let bar = $('<div>', {style: `width: ${value}%; background: ${color}; border-radius: ${br}; height: 10px;`});
    progress.append(bar);
    container.append(label).append(progress);
    return container;
  },

  renderSortPanel: function() {
    let panel = $('.panel-sort-row');
    let labelSort = $('<div>', {class: 'reg-sort-label'}).text('Сортировать по:');
    let dropdownContainer = $('<div>');
    let buttonMenu = $('<div>', {class: 'reg-sort-button', id: 'reg-sort-cmp-name'}).text('Не выбрано');
    let dropdown = $('<div uk-dropdown="mode: click"></div>');
    let ul = $('<ul>', {class: 'uk-nav uk-dropdown-nav'});

    dropdownContainer.append(buttonMenu).append(dropdown);
    dropdown.append(ul);
    panel.append(labelSort).append(dropdownContainer);

    this.heads.forEach(head => {
      let li = $('<li>');
      let a = $(`<a href="#">${head.name}</a>`);
      li.append(a);
      ul.append(li);

      a.on('click', e => {
        e.preventDefault();
        e.target.blur();
        this.sortDesc = !this.sortDesc;
        this.sortCmpID = head.cmpParent;

        buttonMenu.text(head.name);

        if(this.sortDesc) {
          buttonMenu.addClass('desc');
          buttonMenu.removeClass('asc');
        } else {
          buttonMenu.addClass('asc');
          buttonMenu.removeClass('desc');
        }

        this.createBody();
      });

    });
  },

  reset: function(){
    this.filterRights = [];
    this.allRights = [];
    this.filterName = null;
    this.filterCode = null;
    this.filterID = null;
    this.sortCmpID = null;
    this.sortDesc = false;
    this.searchField = null;
    this.searchValue = null;
    this.filterSearchUrl = null;
    this.formPlayerID = null;

    this.registryTable = $('<table class="nlc-table uk-table uk-table-small uk-table-divider uk-table-responsive">');
    this.tHead = $('<thead>');
    this.tBody = $('<tbody>');
    this.registryTable.append(this.tHead).append(this.tBody);
    tableContainer.empty().append(this.registryTable);
  },

  init: function(params){
    this.reset();
    rest.synergyGet(`api/registry/info?code=${params.registryCode}`, info => {
      let registryList = Cons.getAppStore().registryList;
      this.registryInfo = info;
      this.heads = params.heads;

      params.heads.forEach(x => {
        this.fields.push(x.cmpParent);
        if(x.child) this.fields.push(x.child.cmp);
      });

      this.registryID = info.registryID;
      this.registryName = info.name;
      this.registryCode = params.registryCode;
      this.allRights = registryList.find(x => x.registryCode == this.registryCode).rights;
      this.formCode = info.formCode;

      Cons.setAppStore({formCode: info.formCode});
      Cons.setAppStore({registryInfo: {
        registryName: info.name,
        registryID: info.registryID,
        registryCode: params.registryCode
      }});

      if(params.formPlayerID) this.formPlayerID = params.formPlayerID;
      if(params.filterCode) this.filterCode = params.filterCode;
      if(params.searchString) this.searchString = params.searchString;

      this.createHeader();
      this.renderSortPanel();
      Paginator.init();
    });
  }
}

tableContainer.on('renderNewTable', e => {
  if(!e.hasOwnProperty('eventParam')) return;
  registryTable.init(e.eventParam);
});

tableContainer.on('updateTableBody', e => {
  if(!registryTable.registryCode) return;
  if(e.hasOwnProperty('eventParam')) {
    let param = e.eventParam;
    if(param.filterCode && param.filterCode !== "") {
      registryTable.filterCode = param.filterCode;
    } else {
      registryTable.filterCode = null;
    }
    if(e.eventParam.hasOwnProperty('filterSearchUrl')) {
      registryTable.filterSearchUrl = null;
    }
  }
  Paginator.currentPage = 1;
  registryTable.createBody();
});

tableContainer.on('searchInRegistry', e => {
  if(!e.hasOwnProperty('eventParam')) return;
  if(e.eventParam.searchString && e.eventParam.searchString !== "") {
    registryTable.searchString = e.eventParam.searchString;
  } else {
    registryTable.searchString = null;
  }
  Paginator.currentPage = 1;
  registryTable.createBody();
});

tableContainer.on('filterRegistryRows', e => {
  if(!e.hasOwnProperty('eventParam')) return;
  if(e.eventParam.filterSearchUrl && e.eventParam.filterSearchUrl !== "") {
    registryTable.filterSearchUrl = e.eventParam.filterSearchUrl;
  } else {
    registryTable.filterSearchUrl = null;
    return;
  }
  Paginator.currentPage = 1;
  registryTable.createBody();
});
