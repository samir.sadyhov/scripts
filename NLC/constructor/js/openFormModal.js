const createSynergyPlayer = (dataUUID, editable = false) => {
  let player = AS.FORMS.createPlayer();
  if($(document).width() < 781) player.model.showView('mobile');
  player.view.setEditable(editable);
  player.showFormData(null, null, dataUUID);
  return new Promise(resolve => {
    AS.FORMS.ApiUtils.getDocumentIdentifier(dataUUID)
    .then(documentID => AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/docflow/doc/document_info?documentID=${documentID}`))
    .then(documentInfo => {
      player.documentID = documentInfo.documentID;
      player.registryID = documentInfo.registryID;
      player.registryName = documentInfo.registryName;
      player.formName = documentInfo.formName;
      player.formID = documentInfo.formID;
      player.formCode = documentInfo.formCode;
      player.documentInfo = documentInfo;
      resolve(player);
    })
  });
}

const activateDoc = dataUUID => {
  return AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/activate_doc?dataUUID=${dataUUID}`);
}

const saveFormData = formPlayer => {
  return new Promise(resolve => {
    let asfData = formPlayer.model.getAsfData();

    let data = {
      data: '"data" : ' + JSON.stringify(asfData.data),
      form: asfData.form,
      uuid: asfData.uuid
    };
    AS.FORMS.ApiUtils.simpleAsyncPost('rest/api/asforms/form/multipartdata', res => {
      AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/modify_doc?dataUUID=${asfData.uuid}`)
      .then(res => {
        formPlayer.model.hasChanges = false;
        resolve(res);
      })
    }, "text", data, "application/x-www-form-urlencoded; charset=UTF-8", err => {
      showMessage(JSON.parse(err.responseText).errorMessage, 'error');
      resolve(null);
    });
  });
}

const checkSavedForm = (formPlayer, handler) => {
  if(formPlayer.model.hasChanges) {
    UIkit.modal.confirm('Документ был изменен. Сохранить произведенные изменения?').then(() => {
      if(!formPlayer.model.isValid()) {
        showMessage("Заполните обязательные поля!", "error");
      } else {
        Cons.showLoader();
        saveFormData(formPlayer).then(result => {
          Cons.hideLoader();
          showMessage('<svg width="35" height="35" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><polyline fill="none" stroke="#fff" stroke-width="1.1" points="4,10 8,15 17,4"></polyline></svg> Успешно', 'success');
          handler();
        });
      }
    }, handler);
  } else {
    handler();
  }
}

const printForm = content => {
  var css = '<link rel="stylesheet" href="synergyForms.css" type="text/css" />';
  var WinPrint = window.open();
  WinPrint.document.write('');
  WinPrint.document.write(css);
  WinPrint.document.write(content.innerHTML);
  WinPrint.document.write('');
  WinPrint.document.close();
  WinPrint.focus();
  WinPrint.print();
}

const openDocumentInWindow = (dataUUID, editable = false, activate = false) => {
  let documentWindow = $('<div>', {class: 'exp-window-document'});
  let header = $('<div>', {class: 'exp-window-header'});
  let body = $('<div>', {class: 'exp-window-body'});
  let content = $('<div>', {class: 'exp-window-content'});
  let formPanel = $('<div>', {class: 'exp-window-form-panel'})
  let buttonsHeader = $('<div>', {class: 'exp-window-header-buttons'});
  let closeButton = $('<span class="exp-window-close-button" uk-icon="icon: close; ratio: 2"></span>');
  let panelActions = $('<div>', {class: 'exp-window-form-panel-actions'});
  let panelLogo = $('<div>', {class: 'exp-window-header-logo'});

  // let printButton = $('<span class="exp-window-print-button" uk-icon="icon: print; ratio: 1.5"></span>');
  let editButton = $('<span class="exp-window-edit-button" uk-icon="icon: file-edit; ratio: 1.5"></span>');
  let viewButton = $('<span class="exp-window-view-button" uk-icon="icon: file-text; ratio: 1.5"></span>').hide();
  let saveButton = $('<button class="exp-window-save-button uk-button uk-button-primary">Сохранить</button>');

  panelActions.append(editButton).append(viewButton).append(saveButton); //.append(printButton)

  content.append(panelActions).append(formPanel);
  buttonsHeader.append(closeButton);
  header.append(panelLogo).append(buttonsHeader);
  body.append(content);
  documentWindow.append(header).append(body);

  if(editable) {
    editButton.hide();
    // printButton.hide();
    viewButton.show();
  }

  createSynergyPlayer(dataUUID, editable).then(player => {
    formPanel.append(player.view.container);
    $('body').append(documentWindow).addClass('_lock');

    panelLogo.append(`<span>${player.formName}</span>`);

    setTimeout(() => {
      player.model.hasChanges = false;
    }, 1000);

    closeButton.on('click', e => {
      checkSavedForm(player, () => {
        player.destroy();
        documentWindow.fadeOut({
          complete: function() {
            documentWindow.remove();
          }
        });
        $('body').removeClass('_lock');
      });
    });

    editButton.on('click', e => {
      e.preventDefault();
      e.target.blur();
      e.stopPropagation();
      player.view.setEditable(true);
      editButton.hide();
      viewButton.show();
      // printButton.hide();
    });

    viewButton.on('click', e => {
      e.preventDefault();
      e.target.blur();
      e.stopPropagation();
      player.view.setEditable(false);
      editButton.show();
      viewButton.hide();
      // printButton.show();
    });

    saveButton.on('click', e => {
      e.preventDefault();
      e.target.blur();
      e.stopPropagation();

      function complet(){
        Cons.hideLoader();
        showMessage('<svg width="35" height="35" viewBox="0 0 20 20" xmlns="http://www.w3.org/2000/svg"><polyline fill="none" stroke="#fff" stroke-width="1.1" points="4,10 8,15 17,4"></polyline></svg> Данные успешно сохранены', 'success');
        player.destroy();
        documentWindow.fadeOut({
          complete: function() {
            documentWindow.remove();
          }
        });
        $('body').removeClass('_lock');
      }

      if(!player.model.isValid()) {
        showMessage("Заполните обязательные поля!", "error");
      } else {
        Cons.showLoader();
        saveFormData(player).then(result => {
          if(activate) {
            activateDoc(dataUUID).then(res => complet());
          } else {
            complet();
          }
        });
      }

    });

    // printButton.on('click', e => {
    //   e.preventDefault();
    //   e.target.blur();
    //   e.stopPropagation();
    //   printForm(player.view.container[0]);
    // });

  });

}

Cons.setAppStore({openDocumentInWindow: openDocumentInWindow});
