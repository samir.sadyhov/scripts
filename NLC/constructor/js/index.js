const getFullName = person => {
  const {firstname, lastname, patronymic} = person;
  const fio = lastname.substr(0, 1).toUpperCase() + lastname.substr(1) + ' ' + firstname.substr(0, 1).toUpperCase() + '.';
  return patronymic ? fio + ' ' + patronymic.substr(0, 1).toUpperCase() + '.' : fio;
}

const goPage = code => {
  let event = {
    type: 'goto_page',
    pageCode: code,
    pageParams: []
  }
  fire(event, 'button-problem');
}

const createItemNav = item => {
  return $(`<li><a href="#"><span class="uk-margin-small-right" uk-icon="icon: ${item.icon}"></span>${item.name}</a></li>`)
  .on('click', e => {
    e.preventDefault();
    switch (item.action.type) {
      case 'goPage': goPage(item.action.data); break;
      case 'logout': $nlc.logout(); break;
    }
  });
}

const createMenuNav = (data, panelHover) => {
  $('.custom-dropdown-menu').remove();
  let menuContainer = $(`<div class="custom-dropdown-menu" style="display: none">`);
  let nav = $('<ul class="uk-nav uk-dropdown-nav">');
  data.forEach(item => nav.append(createItemNav(item)));
  menuContainer.append(nav);
  $('body').append(menuContainer);
  panelHover.hover(e => {
    UIkit.dropdown(menuContainer).show();
  });
}

const $nlc = {
  userGroups: Cons.getAppStore().userGroups,

  logout: function() {
    AS.apiAuth.setCredentials(null, null);
    AS.OPTIONS.currentUser = {};
    Cons.logout();
    window.location.reload();
  },

  setProfileData: function() {
    let photoURL = `../Synergy/load?userid=${AS.OPTIONS.currentUser.userid}`;
    $('#profileName span').text(getFullName(AS.OPTIONS.currentUser));
    if(AS.OPTIONS.currentUser.positions.length) {
      $('#profilePosition span').text(AS.OPTIONS.currentUser.positions[0].positionName);
    }
    $('#profileImage').attr('src', photoURL);
  },

  initDropdownMenuNav: function() {
    createMenuNav([
      // {name: 'Личная карточка', icon: 'user', action: {type: 'goPage', data: 'user_page'}},
      {name: 'Выход', icon: 'sign-out', action: {type: 'logout'}}
    ], $('#panel-profile-menu'));
  }
}

window.$nlc = $nlc;

//Иконка приложения
if(!$('link[rel="icon"]').length) {
  $('head').append('<link rel="icon" href="data:image/svg+xml;base64,PHN2ZyB3aWR0aD0iMTYwIiBoZWlnaHQ9IjE2MCIgdmlld0JveD0iMCAwIDE2MCAxNjAiIGZpbGw9Im5vbmUiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyI+CjxtYXNrIGlkPSJtYXNrMCIgc3R5bGU9Im1hc2stdHlwZTphbHBoYSIgbWFza1VuaXRzPSJ1c2VyU3BhY2VPblVzZSIgeD0iMCIgeT0iMCIgd2lkdGg9IjE2MCIgaGVpZ2h0PSIxNjAiPgo8Y2lyY2xlIGN4PSI4MCIgY3k9IjgwIiByPSI4MCIgZmlsbD0iI0M0QzRDNCIgZmlsbC1vcGFjaXR5PSIwLjUiLz4KPC9tYXNrPgo8ZyBtYXNrPSJ1cmwoI21hc2swKSI+CjxwYXRoIGZpbGwtcnVsZT0iZXZlbm9kZCIgY2xpcC1ydWxlPSJldmVub2RkIiBkPSJNOTQuMDc0NCAtMC4yNDYzMDdDMTE4LjAyIDE2LjY0NDMgMTMxLjcxMSAyNy42OTM1IDE1Ni4yOTcgNDguMzk1N0wxMzEuMTExIDIuMjIyODNMOTQuMDc0NCAtMC4yNDYzMDdaTTE1OS41MDggNjEuNDgyMkMxMDMuOTIyIDM3LjEyNzQgNzYuMDE3NCAyNy4xNDg3IDMxLjM1OTYgMTUuMDYyNUw1Ny41MzI0IDEuNDgyMjFDOTguNjI0MyAyMC40NzEzIDEyMS4wNDkgMzIuMjM1NiAxNTguMjczIDU4LjI3MjNMMTU5LjUwOCA2MS40ODIyWk0xMy44MjczIDMxLjExMTFDNjQuNjkxNSA0MC40OTM4IDEwMS43MjkgNDkuMTM1OCAxNjQuMTk4IDcwLjEyMzVMMTYxLjIzNSA3MS4zNThDMTMwLjA3MiA3MC4yNTExIDExMS4yOTggNjcuMjU0NiA3Ny4wMzcxIDYwLjQ5MzhDNDcuODExNSA1NC43MTggMzEuOTY2OSA1My4zMDM3IDMuOTUwNzIgNTEuNjA0OUwxMy44MjczIDMxLjExMTFaTTg5LjM4MzkgNzEuMzU4OUM2MS4yOTUgNjQuNDQ3MiAzOS41NDI2IDY0LjE2NTcgLTcuMTU5MyA2OC4zOTZWOTIuODQwNEMyNS40MTcgODUuMDY4NiA0My40MjE2IDgyLjI4MDQgNzQuMzIyMiA4NC42OTIzQzEzMC41ODkgOTAuOTExOCAxNDcuOTE2IDg4LjYwNzIgMTYyLjk2NCA3OC4wMjU2QzEzNS4wNzkgODIuMDU1MyAxMTguOTU1IDgwLjQ4ODMgODkuMzgzOSA3MS4zNTg5Wk05NC4zMjA5IDk2Ljc5MDVDNjEuMTQwNSA5Mi4zNzg2IDM5LjgzMzkgOTUuNTQwMyAtNi4xNzI5NyAxMDYuNDJMLTUuMTg1MzIgMTA3LjkwMkw4LjM5NDkzIDEyOC42NDJDNDAuOTM4MyAxMTIuODEzIDU5Ljk4NyAxMTEuMDU5IDk0LjMyMDkgMTExLjExMkMxMjQuNjE3IDExMy4xNDMgMTM5LjM2OSAxMDcuMTg5IDE2Mi43MTYgODcuMTYwOUMxMzguOTc3IDk4LjI2NjMgMTI0LjQ2NCAxMDEuNTE2IDk0LjMyMDkgOTYuNzkwNVpNMTUuMDY0IDE0MC43NDFDNDUuODgyMiAxMjUuNDkyIDYzLjQ2NjUgMTE5Ljc4OSA5Ni4wNTE3IDEyMC45ODhDMTIzLjM1MSAxMjEuNjc3IDE0MC4yMTggMTE0LjQzIDE2NC40NDcgOTQuNTY4MUMxNDMuNDU0IDExOC41ODEgMTI4LjkyMyAxMzMuNzc3IDEwMC4wMDIgMTM2LjA1Qzc0LjA4NTcgMTM2LjY3OSA2MC4wMzUzIDE0MS4xOTggMzUuNTU3OCAxNTQuMzIxTDE1LjA2NCAxNDAuNzQxWk0xMjIuNzE5IDE0MC4yNDhDODIuMjIwMyAxNDcuOTg1IDY3LjA1MDYgMTUxLjgzOCA1OC41MjEgMTU3LjUzMkw2Mi40NzE3IDE2My45NTFMMTQyLjQ3MiAxNTkuNzU0TDE1MS4zNjEgMTE3LjI4NUMxMzcuMTI5IDEzMy4xOTEgMTMwLjg4NiAxMzguMTc1IDEyMi43MTkgMTQwLjI0OFoiIGZpbGw9IiNDNEM0QzQiLz4KPC9nPgo8L3N2Zz4K">');
}
