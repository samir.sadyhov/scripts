pageHandler("appeal_register_page", () => {
  $nlc.setProfileData();
  $nlc.initDropdownMenuNav();

  const formPlayerID = 'formPlayer-appeal';
  const formPlayerCreateID = 'formPlayer-create-appeal';
  let asform;
  let asformCreate;
  let editable = false;

  let tableHeads = [];
  tableHeads.push({
    name: 'Дата',
    cmpParent: 'lawyers_form_client_appeal_date',
    child: null
  });

  tableHeads.push({
    name: 'Тип обращения',
    cmpParent: 'lawyers_form_client_appeal_type',
    child: null
  });

  tableHeads.push({
    name: 'Статус',
    cmpParent: 'lawyers_form_client_appeal_status',
    child: null
  });

  let eventParam = {
    registryCode: 'lawyers_registry_client_appeal',
    formPlayerID: formPlayerID,
    heads: tableHeads
  };

  let searchValue = Cons.getAppStore().searchValueAppeal;
  if(searchValue && searchValue !== "") {
    $('.search-input').val(searchValue);
    eventParam.searchString = searchValue;
  }

  $('.nlc-table-container').trigger({
    type: 'renderNewTable',
    eventParam: eventParam
  });

  $('.search-input').off().on('keyup', e => {
    if (e.keyCode === 13) {
      e.preventDefault();
      Cons.setAppStore({searchValueAppeal: $('.search-input').val()});
      $('.nlc-table-container').trigger({
        type: 'searchInRegistry',
        eventParam: {
          searchString: $('.search-input').val()
        }
      });
    }
  });

  $('#button-form-edittable').off().on('click', e => {
    editable = !editable;
    fire({type: 'set_form_editable', editable: editable}, formPlayerID);
    fire({type: 'set_hidden', hidden: !editable}, 'button-save');
  });

  if(!Cons.getAppStore().appeal_register_page_listeners) {

    addListener('loaded_form_data', formPlayerCreateID, e => {
      asformCreate = e;
    });

    addListener('loaded_form_data', formPlayerID, e => {
      asform = e;
    });

    addListener("button_click", "button-save", e => {
      let valid = !asform.model.getErrors().length;
      if (valid) {
        fire({type: "set_disabled", disabled: true, cmpID: "button-save"}, "button-save");

        fire({
          type: 'save_form_data',
          success: (dataId, documentId) => {
            showMessage('Изменения сохранены', 'success');

            editable = !editable;
            fire({type: 'set_form_editable', editable: editable}, formPlayerID);
            fire({type: 'set_hidden', hidden: !editable}, 'button-save');
            fire({type: "set_disabled", disabled: false, cmpID: "button-save"}, "button-save");

            if($('#panel-data').length) fire({type: 'set_hidden', hidden: true}, 'panel-data');
            $('.nlc-table-container').trigger({type: 'updateTableBody'});

          },
          error: (status, error) => {
            console.error("FAILED! ", error);
            showMessage('Ошибка сохранения данных', 'error');
          }
        }, formPlayerID);

      } else {
        showMessage('Заполните обязательные поля', 'error');
      }
    });

    addListener("button_click", "button-create", e => {
      let valid = !asformCreate.model.getErrors().length;
      if (valid) {
        fire({type: "set_disabled", disabled: true, cmpID: "button-create"}, "button-create");
        fire({
          type: "create_form_data",
          registryCode: eventParam.registryCode,
          activate: true,
          success: (id, docid) => {
            fire({type: 'set_hidden', hidden: true}, 'modal-panel-create-appeal');
            if($('#panel-data').length) fire({type: 'set_hidden', hidden: true}, 'panel-data');
            $('.nlc-table-container').trigger({type: 'updateTableBody'});
            showMessage('Запись создана', 'success');
          },
          error: (st, err) => {
            console.log("failed" + err.toString());
            showMessage('Ошибка сохранения данных', 'error');
          }
        }, formPlayerCreateID);
      } else {
        showMessage('Заполните обязательные поля', 'error');
      }
    });

    addListener("button_click", "button-close-modal", e => {
      UIkit.modal.confirm('Закрыть документ без сохранения изменений?')
      .then(
        () => fire({type: 'set_hidden', hidden: true}, 'modal-panel-create-appeal'),
        () => console.log('cancel')
      );
    });

    addListener('out_click', 'modal-panel-create-appeal', e => {
      UIkit.modal.confirm('Закрыть документ без сохранения изменений?')
      .then(
        () => fire({type: 'set_hidden', hidden: true}, 'modal-panel-create-appeal'),
        () => console.log('cancel')
      );
    });

    Cons.setAppStore({appeal_register_page_listeners: true});
  }

});
