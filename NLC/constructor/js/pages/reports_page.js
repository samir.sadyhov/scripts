Array.prototype.uniq = function() {
  return this.filter((v, i, a) => i == a.indexOf(v));
}

const getTableHeader = heads => {
  const thead = $('<thead>');
  const tr = $('<tr>');
  thead.append(tr);
  heads.forEach(text => tr.append(`<th>${text}</th>`));
  return thead;
}

const getTable = heads => {
  const table = $('<table>', {class: 'uk-table uk-table-striped uk-table-responsive', style: 'width: auto;'});
  table.append(getTableHeader(heads));
  return table;
}

const getSelectBlock = (id, name, data) => {
  const container = $('<div>', {class: "uk-margin"});
  const controls = $('<div>', {class: "uk-form-controls"});
  const select = $('<select>', {class: "uk-select", id: id});

  container.append(`<label class="uk-form-label" for="${id}">${name}</label>`);
  container.append(controls);
  controls.append(select);
  data.forEach(x => select.append(`<option value="${x.value}">${x.title}</option>`));

  return {container, select};
}

const dictClientType = [
  {title: 'Юридическое лицо', value: '1'},
  {title: 'Физическое лицо', value: '2'}
];

const dictThemeType = [
  {title: 'Уголовное судопроизводство', value: '1'},
  {title: 'Гражданское судопроизводство', value: '2'},
  {title: 'Административное судопроизводство', value: '3'},
  {title: 'Исполнительное производство', value: '4'},
  {title: 'Правовая консультация', value: '5'},
  {title: 'Иное (тэг)', value: '6'}
];

const dictAppealType = [
  {title: 'Ходатайство', value: '1'},
  {title: 'Жалоба', value: '2'},
  {title: 'Жалоба в Верховный суд', value: '3'},
  {title: 'Адвокатский запрос', value: '4'},
  {title: 'Запрос данных', value: '5'},
  {title: 'Обращение', value: '6'}
];

const drawReportCase = () => {
  const {registryCode, reportData, reportContainer} = Reports;
  const rows = reportData.map(x => x.caseStepKey).uniq().sort();
  const table = getTable(['Стадии:', 'Все', 'Завершено', 'Производство']);
  const tbody = $('<tbody>');
  const form = $('<form>', {class: "uk-form-horizontal"});

  const clientType = getSelectBlock('nlc-client-type', 'Тип клиента', dictClientType);
  const themeType = getSelectBlock('nlc-theme-type', 'Предмет', dictThemeType);

  const drawBody = () => {
    const clienTypeKey = clientType.select.val();
    const clientTypeValue = clienTypeKey;
    const caseThemeKey = themeType.select.val();
    const caseTypeValue = caseThemeKey;
    tbody.empty();

    rows.forEach(step => {
      const stepName = reportData.find(x => x.caseStepKey == step).caseStepValue;
      const tr = $('<tr>');
      const allApp = reportData.filter(x => x.caseStepKey == step && x.clienTypeKey == clienTypeKey && x.caseThemeKey == caseThemeKey).length;
      const finishApp = reportData.filter(x => x.caseStepKey == step && x.clienTypeKey == clienTypeKey && x.caseThemeKey == caseThemeKey && x.caseStatusKey == '0').length;
      const workApp = allApp - finishApp;

      tbody.append(tr);
      tr.append(`<td>${stepName}</td>`);
      tr.append(`<td>${allApp}</td>`);
      tr.append(`<td>${finishApp}</td>`);
      tr.append(`<td>${workApp}</td>`);
    });

    const allAppTotal = reportData.filter(x => x.clienTypeKey == clienTypeKey && x.caseThemeKey == caseThemeKey).length;
    const finishAppTotal = reportData.filter(x => x.clienTypeKey == clienTypeKey && x.caseThemeKey == caseThemeKey && x.caseStatusKey == '0').length;
    const workAppTotal = allAppTotal - finishAppTotal;
    const trTotal = $('<tr></tr>');
    tbody.append(trTotal);
    trTotal.append(`<td style="font-weight: bold;">Всего:</td>`);
    trTotal.append(`<td style="font-weight: bold;">${allAppTotal}</td>`);
    trTotal.append(`<td style="font-weight: bold;">${finishAppTotal}</td>`);
    trTotal.append(`<td style="font-weight: bold;">${workAppTotal}</td>`);
  }

  form.append(clientType.container);
  form.append(themeType.container);
  table.append(tbody);
  reportContainer.append(form);
  reportContainer.append(table);

  clientType.select.on('change', e => {
    drawBody();
  });

  themeType.select.on('change', e => {
    drawBody();
  });

  drawBody();
}

const drawReportContract = () => {
  const {registryCode, reportData, reportContainer} = Reports;
  const rows = reportData.map(x => x.caseThemeKey).uniq().sort();
  const table = getTable(['Предмет', 'Все', 'Завершён', 'Производство']);
  const tbody = $('<tbody>');
  const form = $('<form>', {class: "uk-form-horizontal"});

  const clientType = getSelectBlock('nlc-client-type', 'Тип клиента', dictClientType);

  const drawBody = () => {
    const clienTypeKey = clientType.select.val();
    const clientTypeValue = clienTypeKey;
    tbody.empty();

    rows.forEach(theme => {
      const themeName = reportData.find(x => x.caseThemeKey == theme).caseThemeValue;
      const tr = $('<tr>');
      const allApp = reportData.filter(x => x.caseThemeKey == theme && x.clienTypeKey == clienTypeKey).length;
      const finishApp = reportData.filter(x => x.caseThemeKey == theme && x.clienTypeKey == clienTypeKey && x.caseStatusKey == '0').length;
      const workApp = allApp - finishApp;

      tbody.append(tr);
      tr.append(`<td>${themeName}</td>`);
      tr.append(`<td>${allApp}</td>`);
      tr.append(`<td>${finishApp}</td>`);
      tr.append(`<td>${workApp}</td>`);
    });

    const allAppTotal = reportData.filter(x => x.clienTypeKey == clienTypeKey).length;
    const finishAppTotal = reportData.filter(x => x.clienTypeKey == clienTypeKey && x.caseStatusKey == '0').length;
    const workAppTotal = allAppTotal - finishAppTotal;
    const trTotal = $('<tr></tr>');
    tbody.append(trTotal);
    trTotal.append(`<td style="font-weight: bold;">Всего:</td>`);
    trTotal.append(`<td style="font-weight: bold;">${allAppTotal}</td>`);
    trTotal.append(`<td style="font-weight: bold;">${finishAppTotal}</td>`);
    trTotal.append(`<td style="font-weight: bold;">${workAppTotal}</td>`);
  }

  form.append(clientType.container);
  table.append(tbody);
  reportContainer.append(form);
  reportContainer.append(table);

  clientType.select.on('change', e => {
    drawBody();
  });

  drawBody();
}

const drawReportLawyers = () => {
  const {registryCode, reportData, reportContainer} = Reports;
  const rows = reportData.map(x => x.lawyerID).uniq().sort();

  const reportBlock = $('<div>', {style: 'width: 100%; display: flex;'});

  const tableLawyers = getTable(['ФИО:', 'Нагрузка<br>Все / Производство']);
  const tbodyLawyers = $('<tbody>');

  const tableLawyerInfo = getTable(['Вид судопроизводства', 'Все', 'Производство']);
  const tbodyLawyerInfo = $('<tbody>');

  tableLawyerInfo.css({
    'margin-left': '10%',
    'height': 'fit-content'
  });

  const drawTableLawyerInfo = lawyerID => {
    const lawyerData = reportData.filter(x => x.lawyerID == lawyerID);
    const themes = lawyerData.map(x => x.caseThemeKey).filter(x => x != 'null').uniq().sort();

    let allAppTotal = 0;
    let workAppTotal = 0;

    tbodyLawyerInfo.empty();
    themes.forEach(theme => {
      const themeName = lawyerData.find(x => x.caseThemeKey == theme).caseThemeValue;

      const tr = $('<tr>');
      const allApp = lawyerData.filter(x => x.caseThemeKey == theme).length;
      const workApp = lawyerData.filter(x => x.caseThemeKey == theme && x.caseStatusKey == '1').length;

      allAppTotal += allApp;
      workAppTotal += workApp;

      tbodyLawyerInfo.append(tr);
      tr.append(`<td>${themeName}</td>`);
      tr.append(`<td>${allApp}</td>`);
      tr.append(`<td>${workApp}</td>`);
    });

    const trTotal = $('<tr></tr>');
    tbodyLawyerInfo.append(trTotal);
    trTotal.append(`<td style="font-weight: bold;">Всего:</td>`);
    trTotal.append(`<td style="font-weight: bold;">${allAppTotal}</td>`);
    trTotal.append(`<td style="font-weight: bold;">${workAppTotal}</td>`);
  }

  rows.forEach(lawyerID => {
    const fio = reportData.find(x => x.lawyerID == lawyerID).lawyerFIO;
    const tr = $('<tr>', {class: 'tr-hover'});
    const allApp = reportData.filter(x => x.lawyerID == lawyerID && x.caseThemeKey != "null").length;
    const workApp = reportData.filter(x => x.lawyerID == lawyerID && x.caseStatusKey == '1').length;

    tbodyLawyers.append(tr);
    tr.append(`<td>${fio}</td>`);
    tr.append(`<td>${allApp} / ${workApp}</td>`);

    tr.on('click', e => {
      if(tr.hasClass('selected')) return;

      tbodyLawyers.find('tr').removeClass('selected');
      tr.addClass('selected');

      drawTableLawyerInfo(lawyerID);
    });
  });

  tableLawyers.append(tbodyLawyers);
  tableLawyerInfo.append(tbodyLawyerInfo);
  reportBlock.append(tableLawyers);
  reportBlock.append(tableLawyerInfo);
  reportContainer.append(reportBlock);
}

const drawReportAppeals = () => {
  const {registryCode, reportData, reportContainer} = Reports;
  const rows = reportData.map(x => x.appealStepKey).filter(x => x != "null").uniq().sort();
  const table = getTable(['Тип обращений', '%, доля', 'Положительно', 'Частично положительный', 'Отказ', 'Нет ответа']);
  const tbody = $('<tbody>');
  const form = $('<form>', {class: "uk-form-horizontal"});
  const lawyers = reportData.map(x => x.appealLawyerKey).filter(x => x != "null").uniq();
  const lawyersDict = lawyers.map(lawyerID => {
    return {
      title: reportData.find(x => x.appealLawyerKey == lawyerID).appealLawyerValue,
      value: lawyerID
    }
  });
  const selectLawyers = getSelectBlock('nlc-appeal-lawyers', 'Адвокат', lawyersDict);
  const themeType = getSelectBlock('nlc-appeal-theme', 'Предмет', dictThemeType);
  const appealType = getSelectBlock('nlc-appeal-type', 'Тип обращения', dictAppealType);

  const drawBody = () => {
    const lawyerID = selectLawyers.select.val();
    const appealThemeKey = themeType.select.val();
    const appealTypeKey = appealType.select.val();
    tbody.empty();

    let positivAppTotal = 0;
    let partPositivAppTotal = 0;
    let refusAppTotal = 0;
    let noAnswerAppTotal = 0;

    rows.forEach(step => {
      const stepName = reportData.find(x => x.appealStepKey == step).appealStepValue;
      const tr = $('<tr>');

      //Положительно
      const positivApp = reportData.filter(x =>
        x.appealStepKey == step &&
        x.appealLawyerKey == lawyerID &&
        x.appealThemeKey == appealThemeKey &&
        x.appealTypeKey == appealTypeKey &&
        x.appealStatusKey == '1'
      ).length;

      //Частично положительный
      const partPositivApp = reportData.filter(x =>
        x.appealStepKey == step &&
        x.appealLawyerKey == lawyerID &&
        x.appealThemeKey == appealThemeKey &&
        x.appealTypeKey == appealTypeKey &&
        x.appealStatusKey == '2'
      ).length;

      //Отказ
      const refusApp = reportData.filter(x =>
        x.appealStepKey == step &&
        x.appealLawyerKey == lawyerID &&
        x.appealThemeKey == appealThemeKey &&
        x.appealTypeKey == appealTypeKey &&
        x.appealStatusKey == '3'
      ).length;

      //Нет ответа
      const noAnswerApp = reportData.filter(x =>
        x.appealStepKey == step &&
        x.appealLawyerKey == lawyerID &&
        x.appealThemeKey == appealThemeKey &&
        x.appealTypeKey == appealTypeKey &&
        x.appealStatusKey == '4'
      ).length;

      const percent = Math.round((positivApp + partPositivApp) / (positivApp + partPositivApp + refusApp + noAnswerApp) * 10000) / 100 || 0;

      positivAppTotal += positivApp;
      partPositivAppTotal += partPositivApp;
      refusAppTotal += refusApp;
      noAnswerAppTotal += noAnswerApp;

      tbody.append(tr);
      tr.append(`<td>${stepName}</td>`);
      tr.append(`<td>${percent}%</td>`);
      tr.append(`<td>${positivApp}</td>`);
      tr.append(`<td>${partPositivApp}</td>`);
      tr.append(`<td>${refusApp}</td>`);
      tr.append(`<td>${noAnswerApp}</td>`);
    });

    const percentAll = Math.round((positivAppTotal + partPositivAppTotal) / (positivAppTotal + partPositivAppTotal + refusAppTotal + noAnswerAppTotal) * 10000) / 100 || 0;
    const trTotal = $('<tr>');
    tbody.append(trTotal);
    trTotal.append(`<td style="font-weight: bold;">Всего</td>`);
    trTotal.append(`<td style="font-weight: bold;">${percentAll}%</td>`);
    trTotal.append(`<td style="font-weight: bold;">${positivAppTotal}</td>`);
    trTotal.append(`<td style="font-weight: bold;">${partPositivAppTotal}</td>`);
    trTotal.append(`<td style="font-weight: bold;">${refusAppTotal}</td>`);
    trTotal.append(`<td style="font-weight: bold;">${noAnswerAppTotal}</td>`);
  }

  selectLawyers.select.on('change', e => {
    drawBody();
  });

  themeType.select.on('change', e => {
    drawBody();
  });

  appealType.select.on('change', e => {
    drawBody();
  });

  drawBody();

  form.append(selectLawyers.container);
  form.append(themeType.container);
  form.append(appealType.container);
  table.append(tbody);
  reportContainer.append(form);
  reportContainer.append(table);
}

const Reports = {
  reportList: null,
  registries: null,
  registryCode: null,
  reglistContainer: null,
  reportContainer: null,
  reportData: null,

  parseReportData(xmlText) {
    const parser = new DOMParser();
    const xmlDoc = parser.parseFromString(xmlText, "text/xml");
    const textContent = [...xmlDoc.getElementsByTagName("textContent")];
    const result = [];

    textContent.forEach((node, key) => {
      const tmp = {};
      node = node.childNodes[0].nodeValue.split('||');
      node.forEach(x => {
        x = x.split(':');
        tmp[x[0]] = x[1];
      });
      result.push(tmp);
    });
    return result;
  },

  getReportList(){
    return new Promise(resolve => {
      rest.synergyGet('api/report/list', res => resolve(res), err => resolve(null));
    });
  },

  getRegistryList(){
    return new Promise(resolve => {
      rest.synergyGet('api/registry/list', res => resolve(res), err => resolve(null));
    });
  },

  getReport(reportParam) {
    return new Promise((resolve, reject) => {
      fetch(`${window.location.origin}/Synergy/rest/api/report/do?${reportParam}`, {
        method: 'POST',
        headers: {
          'Authorization': "Basic " + btoa(Cons.creds.login + ":" + Cons.creds.password)
        }
      })
      .then(response => response.text())
      .then(result => resolve(result))
      .catch(error => reject(error));
    });
  },

  getReportData(registry){
    Cons.showLoader();
    this.registryCode = registry.registryCode;
    const report = this.reportList.find(x => x.objectCode == this.registryCode);
    if(report) {
      this.getReport(`reportID=${report.reportID}&fileName=1.xml`)
      .then(res => this.parseReportData(res))
      .then(res => {
        this.reportData = res;
        this.renderReportContent();
        Cons.hideLoader();
      });

    } else {
      Cons.hideLoader();
      this.reportContainer.fadeOut(600, () => this.reportContainer.empty());
      showMessage('Отчет в разработке', 'info');
    }
  },

  renderReportContent(){
    this.reportContainer.empty();
    this.reportContainer.fadeIn(600);
    switch (this.registryCode) {
      case 'lawyers_registry_client_case': drawReportCase(); break;
      case 'lawyers_registry_client_contract': drawReportContract(); break;
      case 'lawyers_registry_client_lawyer': drawReportLawyers(); break;
      case 'lawyers_registry_client_appeal': drawReportAppeals(); break;
    }
  },

  renderRegistryList(){
    const ul = $('<ul>');
    this.reglistContainer.empty();
    this.reglistContainer.append(ul);
    this.registries.forEach(registry => {
      const li = $('<li>');
      const a = $(`<a class="nlc-report-nav-item" href="#">${registry.registryName}</a>`);
      li.append(a);
      ul.append(li);
      a.on('click', e => {
        e.preventDefault();
        e.target.blur();
        if(a.hasClass('selected')) return;

        $('.nlc-report-nav-item').removeClass('selected');
        a.addClass('selected');
        this.getReportData(registry);
      });
    });
    return this;
  },

  init(){
    Cons.showLoader();

    this.reglistContainer = $('#nlc-registries');
    this.reportContainer = $('#nlc-report');
    this.reportContainer.hide();

    this.getRegistryList().then(res => {
      if(res) {
        this.registries = res.find(x => x.regGroupName == 'registry_reports').consistOf;
        return this.getReportList();
      } else {
        Cons.hideLoader();
      }
    }).then(res => {
      if(res) {
        this.reportList = res.filter(x => x.objectType == '65536' && this.registries.find(reg => reg.registryCode == x.objectCode));
        return this.renderRegistryList();
      } else {
        Cons.hideLoader();
      }
    }).then(res => {
      Cons.hideLoader();
    });
  }

}

pageHandler("reports_page", () => {
  $nlc.setProfileData();
  $nlc.initDropdownMenuNav();
  Reports.init();
});
