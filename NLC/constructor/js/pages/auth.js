const parseRegistryList = list => {
  let newList = [];

  function s(data) {
    data.forEach(item => {
      if (item.consistOf && item.consistOf.length > 0) {
        s(item.consistOf);
      } else {
        newList.push(item);
      }
    });
  }
  s(list);

  return newList;
}

pageHandler('auth', () => {
  if (Cons.getAppStore().auth_page_listener) return;

  addListener('auth_success', 'button-auth', authed => {
    const {login, password} = authed.creds;

    Cons.creds.login = login;
    Cons.creds.password = password;
    AS.apiAuth.setCredentials(login, password);
    AS.OPTIONS.login = login;
    AS.OPTIONS.password = password;
    AS.OPTIONS.currentUser = authed.data.person;

    Cons.setAppStore({userGroups: authed.data.groups});

    rest.synergyGet(`api/registry/list`, registryList => {
      Cons.setAppStore({registryList: parseRegistryList(registryList)});
    });
  });

  Cons.setAppStore({auth_page_listener: true});
});
