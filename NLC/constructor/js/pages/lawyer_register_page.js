pageHandler("lawyer_register_page", () => {
  $nlc.setProfileData();
  $nlc.initDropdownMenuNav();

  const formPlayerID = 'formPlayer-lawyer';
  const formPlayerCreateID = 'formPlayer-create-lawyer';
  let asform;
  let asformCreate;
  let editable = false;

  let tableHeads = [];
  tableHeads.push({
    name: 'АДВОКАТ',
    cmpParent: 'lawyers_form_lawyer_lastname',
    child: {
      cmp: 'lawyers_form_lawyer_iin',
      prefix: 'ИИН:'
    }
  });

  tableHeads.push({
    name: 'КОНТАКТЫ',
    cmpParent: 'lawyers_form_lawyer_phone',
    child: null
  });

  let eventParam = {
    registryCode: 'lawyers_registry_client_lawyer',
    formPlayerID: formPlayerID,
    heads: tableHeads
  };

  let searchValue = Cons.getAppStore().searchValueLawyer;
  if(searchValue && searchValue !== "") {
    $('.search-input').val(searchValue);
    eventParam.searchString = searchValue;
  }

  $('.nlc-table-container').trigger({
    type: 'renderNewTable',
    eventParam: eventParam
  });

  $('.search-input').off().on('keyup', e => {
    if (e.keyCode === 13) {
      e.preventDefault();
      Cons.setAppStore({searchValueLawyer: $('.search-input').val()});
      $('.nlc-table-container').trigger({
        type: 'searchInRegistry',
        eventParam: {
          searchString: $('.search-input').val()
        }
      });
    }
  });

  $('#button-form-edittable').off().on('click', e => {
    editable = !editable;
    fire({type: 'set_form_editable', editable: editable}, formPlayerID);
    fire({type: 'set_hidden', hidden: !editable}, 'button-save');
  });

  if(!Cons.getAppStore().lawyers_register_page_listeners) {
    addListener('loaded_form_data', formPlayerCreateID, e => {
      asformCreate = e;
    });

    addListener('loaded_form_data', formPlayerID, e => {
      asform = e;
      let lastname = asform.model.getModelWithId('lawyers_form_lawyer_lastname');
      let firstname = asform.model.getModelWithId('lawyers_form_lawyer_firstname');
      let patronymic = asform.model.getModelWithId('lawyers_form_lawyer_partonomyc');
      let labelText = '';
      if(lastname && lastname.getValue()) labelText = lastname.getValue();
      if(firstname && firstname.getValue()) labelText += ` ${firstname.getValue()}`;
      if(patronymic && patronymic.getValue()) labelText += ` ${patronymic.getValue()}`;
      fire({
        type: 'change_label',
        text: localizedText(labelText, labelText, labelText, labelText)
      }, 'document-label');
    });

    addListener("button_click", "button-save", e => {
      let valid = !asform.model.getErrors().length;
      if (valid) {
        fire({type: "set_disabled", disabled: true, cmpID: "button-save"}, "button-save");

        fire({
          type: 'save_form_data',
          success: (dataId, documentId) => {
            showMessage('Изменения сохранены', 'success');

            editable = !editable;
            fire({type: 'set_form_editable', editable: editable}, formPlayerID);
            fire({type: 'set_hidden', hidden: !editable}, 'button-save');
            fire({type: "set_disabled", disabled: false, cmpID: "button-save"}, "button-save");

            if($('#panel-data').length) fire({type: 'set_hidden', hidden: true}, 'panel-data');
            $('.nlc-table-container').trigger({type: 'updateTableBody'});

          },
          error: (status, error) => {
            console.error("FAILED! ", error);
            showMessage('Ошибка сохранения данных', 'error');
          }
        }, formPlayerID);

      } else {
        showMessage('Заполните обязательные поля', 'error');
      }
    });

    addListener("button_click", "button-create", function() {
      let valid = !asformCreate.model.getErrors().length;
      if (valid) {
        fire({type: "set_disabled", disabled: true, cmpID: "button-create"}, "button-create");
        fire({
          type: "create_form_data",
          registryCode: eventParam.registryCode,
          activate: true,
          success: (id, docid) => {
            fire({type: 'set_hidden', hidden: true}, 'modal-panel-create-lawyer');
            if($('#panel-data').length) fire({type: 'set_hidden', hidden: true}, 'panel-data');
            $('.nlc-table-container').trigger({type: 'updateTableBody'});
            showMessage('Запись создана', 'success');
          },
          error: (st, err) => {
            console.log("failed" + err.toString());
            showMessage('Ошибка сохранения данных', 'error');
          }
        }, formPlayerCreateID);
      } else {
        showMessage('Заполните обязательные поля', 'error');
      }
    });

    addListener("button_click", "button-close-modal", e => {
      UIkit.modal.confirm('Закрыть документ без сохранения изменений?')
      .then(
        () => fire({type: 'set_hidden', hidden: true}, 'modal-panel-create-lawyer'),
        () => console.log('cancel')
      );
    });

    addListener('out_click', 'modal-panel-create-lawyer', e => {
      UIkit.modal.confirm('Закрыть документ без сохранения изменений?')
      .then(
        () => fire({type: 'set_hidden', hidden: true}, 'modal-panel-create-lawyer'),
        () => console.log('cancel')
      );
    });

    Cons.setAppStore({lawyers_register_page_listeners: true});
  }

});
