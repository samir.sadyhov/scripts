/*
model.tabs = [];
model.tabs.push({name: '', blocks: [], accordion: [
  {titleID: '', open: true, blocks: []}
]});

@param name - наименование таба
@param blocks - массив, перечисление ID компонентов которые будут входить в данный блок.

@param accordion.titleID - ID компонента на форме с типом label для заголовка блока
@param accordion.open - true/false, открытый или закрытый блок
@param accordion.blocks - массив, перечисление ID компонентов которые будут входить в данный блок.
*/

let parentContainer = view.playerView.container;
let tabContainer = $('<div>', {class: 'tab-container'});
let tab = $('<div>', {class: 'tab'});
let tabsData = [];

tabContainer.append(tab);
$(view.container).append(tabContainer);

if(model.hasOwnProperty('tabs')) tabsData = model.tabs;

const toggleAcc = (el, block) => {
  el = el[0];
  el.classList.toggle("active");
  block.open = $(el).hasClass('active');
  let panel = el.nextElementSibling;
  if (panel.style.maxHeight) panel.style.maxHeight = null;
  else panel.style.maxHeight = "initial";
}

tabsData.forEach((item, i) => {
  let tabButton = $('<button>', {class: 'tablinks'}).text(item.name);
  let tabcontent = $('<div>', {class: 'tabcontent'});

  tab.append(tabButton);
  tabContainer.append(tabcontent);

  tabButton.on('click', e => {
    tabContainer.find('.tablinks').removeClass('active');
    tabButton.addClass('active');
    tabContainer.find('.tabcontent').removeClass('active');
    tabcontent.addClass('active');
  });

  if(i == 0) {
    tabButton.addClass('active');
    tabcontent.addClass('active');
  }

  if(item.hasOwnProperty('blocks')) {
    item.blocks.forEach(block => {
      let tabData = $(parentContainer).find(`[data-asformid $= ".container.${block}"]`);
      tabData.parent().parent().hide();
      tabcontent.append(tabData.detach());
    });
  }

  if(item.hasOwnProperty('accordion')) {
    let accordionContainer = $('<div class="custom-accordion">');
    tabcontent.append(accordionContainer);

    item.accordion.forEach(acc => {
      let block = $('<div class="accordion-block">')
      let title = $(parentContainer).find(`[data-asformid="label.label.${acc.titleID}"]`);
      let button = $(`<button class="accordion-button">${title.text()}</button>`);
      let body = $('<div class="accordion-body">');

      title.parent().parent().parent().hide();
      accordionContainer.append(block);
      block.append(button).append(body);
      button.on('click', () => toggleAcc(button, acc));

      acc.blocks.forEach(accblock => {
        let cc = $(parentContainer).find(`[data-asformid $= ".container.${accblock}"]`);
        cc.parent().parent().hide();
        body.append(cc.detach());
      });

      body.append('<div class="accordion-footer"></div>');
      if(acc.open) toggleAcc(button, acc);

    });
  }

});
