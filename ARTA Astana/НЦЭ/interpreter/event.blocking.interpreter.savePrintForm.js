var result = true;
var message = "ok";

try {
  let fileName = "printFormFile.pdf";
  let PDFFile = API.getPrintFilePDF(dataUUID);

  if(PDFFile.length == 0) throw new Error('Ошибка получения PDF версии формы');

  let tmpFile = API.startUpload();
  let uploadPartResult = API.uploadPart(tmpFile.file, PDFFile);
  let asfFileAddResult = API.addFileInForm(dataUUID, fileName, tmpFile.file, true);

  if(asfFileAddResult && asfFileAddResult.hasOwnProperty('fileId')) {
    let currentFormData = API.getFormData(dataUUID);
    let attachmentsTable = UTILS.getValue(currentFormData, 'attachments');

    if(attachmentsTable && attachmentsTable.hasOwnProperty('data')) {
      let oldFile = false;
      for(let i = 0; i < attachmentsTable.data.length; i++) {
        let item = attachmentsTable.data[i];
        if(item.type == 'file' && item.value == fileName && item.key == asfFileAddResult.fileId) {
          oldFile = true;
          break;
        }
      }
      if(oldFile) {
        message = 'Файл перезаписан';
      } else {
        let resultSaveFile = API.appendTable(dataUUID, 'attachments', [{
          id: 'file',
          type: 'file',
          value: fileName,
          key: asfFileAddResult.fileId
        }]);

        if(resultSaveFile.errorCode != '0') throw new Error('Ошибка сохранения файла на форме appendTable');
        message = 'Файл успешно добавлен';
      }
    } else {
      let resultSaveFile = API.appendTable(dataUUID, 'attachments', [{
        id: 'file',
        type: 'file',
        value: fileName,
        key: asfFileAddResult.fileId
      }]);

      if(resultSaveFile.errorCode != '0') throw new Error('Ошибка сохранения файла на форме appendTable');
      message = 'Файл успешно добавлен';
    }

  } else {
    throw new Error('Ошибка загрузки файла addFileInForm');
  }

} catch (err) {
  log.error(err.message);
  result = false;
  message = err.message;
}
