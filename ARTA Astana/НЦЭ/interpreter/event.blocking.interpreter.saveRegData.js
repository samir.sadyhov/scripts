function customFormatDate(datetime){
  datetime = datetime.split(/\D/);
  return datetime[2] + '.' + datetime[1] + '.' + datetime[0] + ' ' + datetime[3] + ':' + datetime[4];
}

var result = true;
var message = "ok";

try {

  let currentDocInfo = API.getDocumentInfo(documentID);
  let asfData = [];

  asfData.push({
    "id": "docDate",
    "type": "date",
    "value": customFormatDate(currentDocInfo.regDate),
    "key": currentDocInfo.regDate
  });

  asfData.push({
    "id": "docNo",
    "type": "textbox",
    "value": currentDocInfo.number
  });

  let saveResult = API.mergeFormData({uuid: dataUUID, data: asfData});
  if(saveResult && saveResult.errorCode == '0') {
    message = 'Номер: ' + currentDocInfo.number + ' || Дата: ' + customFormatDate(currentDocInfo.regDate);
  } else {
    message = 'Произошла ошибка сохранения';
  }

} catch (err) {
  log.error(err.message);
  message = err.message;
}
