view.container.css('overflow', 'auto');

const getValue = (data, cmpID) => {
  data = data.data ? data.data : data;
  return data.find(x => x.id == cmpID);
}

const parseAsfValue = asfDataValue => {
  if(!asfDataValue) return null;
  const {type, value = '', key = ''} = asfDataValue;
  return key ? {type, value, key} : {type, value};
}

const parseAsfTable = asfTable => {
  const result = [];
  try {
    if(!asfTable.hasOwnProperty('data')) return result;

    const data = asfTable.data.filter(x => x.type != 'label');
    if(!data.length) return result;

    const ids = data.map(x => x.id.slice(0, x.id.indexOf('-b'))).uniq();
    let tbi =  data.slice(-1)[0].id;
    tbi = Number(tbi.slice(tbi.indexOf('-b') + 2));

    for(let i = 1; i <= tbi; i++) {
    	const item = {};
      ids.forEach(key => {
        const parseValue = parseAsfValue(getValue(asfTable, `${key}-b${i}`));
        if(parseValue) item[key] = parseValue;
      });
      result.push(item);
    }

    return result;
  } catch (err) {
    console.log('ERROR parseAsfTable', err);
    return result;
  }
}

Array.prototype.uniq = function() {
  return this.filter(function(v, i, a){ return i == a.indexOf(v) });
}

const total = {
  data: {
    numericinput_kolvo: 0,
    textbox_fot: 0,
    textbox_social_contributions: 0,
    textbox_electricity: 0,
    textbox_materialy: 0,
    textbox_total_direct_costs: 0,
    numericinput_overheads: 0,
    numericinput_administrative_expenses: 0,
    numericinput_general_expenses: 0,
    numericinput_total_cost: 0,
    numericinput_profitability_indicator: 0,
    numericinput_profitability_indicator_material: 0,
    numericinput_profitability_material: 0,
    numericinput_profitability: 0,
    numericinput_cost_profitability: 0,
    numericinput_total_no_nds: 0,
    numericinput_serving_and_cleaning: 0,
    numericinput_cost_of_services: 0,
    numericinput_nds: 0,
    numericinput_total_nds: 0
  },

  sumData: function(){
    this.tableData = parseAsfTable(model.getAsfData()[0]);

    for(const cmp_id in this.data) {
      this.data[cmp_id] = 0;

      for(let i = 0; i < this.tableData.length; i++){
        const item = this.tableData[i];
        if(item.hasOwnProperty(cmp_id) && item[cmp_id].hasOwnProperty('key')) {
          const value = Number(item[cmp_id].key);
          if(!isNaN(Number(value))) this.data[cmp_id] += value;
        }
      }
    }
  },

  renderTotal: function(){
    for(const cmp_id in this.data) {
      const value = this.data[cmp_id];
      const totalCell = this.tfoot.find(`#total_${cmp_id}`);
      const trFor1unitCell = this.tfoot.find(`#for1unit_${cmp_id}`);

      if(totalCell && totalCell.length) totalCell.text(value.toFixed(2));
      if(trFor1unitCell && trFor1unitCell.length && this.data.numericinput_kolvo > 0) trFor1unitCell.text((value / this.data.numericinput_kolvo).toFixed(2));
    }
  },

  renderFooter: function(){
    this.tfoot = $('<tfoot>');
    this.table.append(this.tfoot);

    const trTotal = $('<tr>');
    const trFor1unit = $('<tr>');

    trTotal.append('<td class="asf-cell asf-borderedCell asf-label" style="text-align: center; font-weight: bold;" colspan="2">Итого</td>');
    trFor1unit.append('<td class="asf-cell asf-borderedCell asf-label" style="text-align: center; font-weight: bold;" colspan="2">На 1 единицу</td>');

    for(const cmp_id in this.data) {
      trTotal.append(`<td class="asf-cell asf-borderedCell asf-label" id="total_${cmp_id}" style="text-align: center; font-weight: bold;"></td>`);
      trFor1unit.append(`<td class="asf-cell asf-borderedCell asf-label" id="for1unit_${cmp_id}" style="text-align: center; font-weight: bold;"></td>`);
    }

    trTotal.append('<td class="asf-cell asf-borderedCell asf-label">', '<td class="asf-cell asf-borderedCell">');
    trFor1unit.append('<td class="asf-cell asf-borderedCell asf-label">', '<td class="asf-cell asf-borderedCell">');

    this.tfoot.append(trTotal, trFor1unit);
  },

  addCmpListeners: function(block){
    const {tableBlockIndex} = block;
    const tableID = model.asfProperty.id;
    for(const cmp_id in this.data) {
      const cmpModel = model.playerModel.getModelWithId(cmp_id, tableID, tableBlockIndex);
      if(cmpModel) {
        cmpModel.on('valueChange', () => {
          this.sumData();
          this.renderTotal();
        });
      }
    }
  },

  addListeners: function(){
    const {modelBlocks} = model;
    const tableID = model.asfProperty.id;

    for(let i = 0; i < modelBlocks.length; i++) this.addCmpListeners(modelBlocks[i]);

    model.on('tableRowAdd', (_1, _2, block) => {
      this.addCmpListeners(block);

      modelBlocks.forEach((item,i) => {
        const numModel = model.playerModel.getModelWithId("numericinput_counter", tableID, item.tableBlockIndex);
        if(numModel) numModel.setValue(String(++i));
      });
    });

    model.on('tableRowDelete', () => {
      this.sumData();
      this.renderTotal();

      modelBlocks.forEach((item,i) => {
        const numModel = model.playerModel.getModelWithId("numericinput_counter", tableID, item.tableBlockIndex);
        if(numModel) numModel.setValue(String(++i));
      });
    });
  },

  init: function(){
    this.table = view.container.find('table');
    this.renderFooter();

    const timerID = setInterval(() => {
      this.tableData = model.getAsfData();
      if(this.tableData && this.tableData.length == 1 && this.tableData[0].hasOwnProperty('data')) {

        clearInterval(timerID);
        this.sumData();
        this.renderTotal();

        if(editable) this.addListeners();
      }
    }, 500);
  }
}

total.init();
