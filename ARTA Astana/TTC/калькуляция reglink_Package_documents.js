const matching = [];
matching.push({in_field: 'date_left_for_repairs_vu36', out_field: 'date_of_data_collection_from'});
matching.push({in_field: 'date_left_for_repairs_vu36', out_field: 'date_of_data_collection_before'});
matching.push({in_field: 'textbox_repair_point_vybra', out_field: 'textbox_repair_point'});
matching.push({in_field: 'textbox_owner_carriage_vybra', out_field: 'textbox_wagon_owner'});
matching.push({in_field: 'textbox_type_of_repair_vybra', out_field: 'textbox_type_of_repair'});
matching.push({in_field: 'textbox_symptom_so_vybra', out_field: 'textbox_symptom_so'});
matching.push({in_field: 'textbox_document_number_com_pos', out_field: 'textbox_contract_number'});
matching.push({in_field: 'textbox_сar_type_vybra', out_field: 'table_aggregated_list_of_expenses.textbox_type_carriage'});
matching.push({in_field: 'textbox_carriage_number_vybra', out_field: 'table_aggregated_list_of_expenses.textbox_number_wagon'});


const getAsfDataUUID = async documentID => {
  return new Promise(async resolve => {
    AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/formPlayer/getAsfDataUUID?documentID=${documentID}`,
      resolve, 'text',  null,
      err => {
        console.log(`ERROR [ getAsfDataUUID ]: ${JSON.stringify(err)}`);
        resolve(null);
      }
    );
  });
}

const setField = (from, modelTo, collation_type = null, label = null) => {
  if(from) {
    switch (from.type) {
      case 'textbox':
      case 'textarea':
        if(modelTo && from.hasOwnProperty('value') && from.value) {
          const currentValue = modelTo.getValue();

          switch (collation_type) {
            case 'PREFIX':
              if(currentValue && currentValue != '') {
                if(label && label != '') {
                  modelTo.setValue(`${label} ${from.value}`);
                } else {
                  modelTo.setValue(`${currentValue} ${from.value}`);
                }
              } else {
                if(label && label != '') {
                  modelTo.setValue(`${label} ${from.value}`);
                } else {
                  modelTo.setValue(from.value);
                }
              }
              break;
            case 'POSTFIX':
              if(currentValue && currentValue != '') {
                if(label && label != '') {
                  modelTo.setValue(`${from.value} ${label}`);
                } else {
                  modelTo.setValue(`${from.value} ${currentValue}`);
                }
              } else {
                if(label && label != '') {
                  modelTo.setValue(`${from.value} ${label}`);
                } else {
                  modelTo.setValue(from.value);
                }
              }
              break;
            default: modelTo.setValue(from.value);
          }

        } else {
          if(modelTo) modelTo.setValue(null);
        }
        break;
      case 'check':
        if(modelTo && from.hasOwnProperty('values')) {
          modelTo.setValue(from.values);
        } else {
          if(modelTo) modelTo.setValue(null);
        }
        break;
      case 'entity':
        if(modelTo && from.hasOwnProperty('key') && from.key && from.key !== 'null') {
          switch (modelTo.asfProperty.config.entity) {
            case "users":
              modelTo.setValue({
                personID: from.key,
                personName: from.value
              });
              break;
            case "departments":
              modelTo.setValue({
                departmentId: from.key,
                departmentName: from.value
              });
              break;
            case "positions":
              modelTo.setValue({
                elementID: from.key,
                elementName: from.value
              });
              break;
          }
        } else {
          if(modelTo) modelTo.setValue(null);
        }
        break;
      default:
        if(modelTo && from.hasOwnProperty('key') && from.key) {
          modelTo.setValue(from.key);
        } else {
          if(modelTo) modelTo.setValue(null);
        }
    }
  } else {
    if(modelTo) modelTo.setValue(null);
  }
}

const removeRow = tableModel => {
  for(const numb in tableModel.getBlockNumbers()) tableModel.removeRow(numb);
}

const getTableBlockIndex = tableModel => {
  const {modelBlocks} = tableModel;
  if(modelBlocks.length) {
    const block = modelBlocks[modelBlocks.length - 1];
    return block.tableBlockIndex;
  } else {
    const toBlock = tableModel.createRow();
    return toBlock.tableBlockIndex;
  }
}

const clearFields = () => {
  matching.forEach(item => {
    const {out_field} = item;
    if(out_field.includes('.')) {
      const tableID = out_field.split('.')[0];
      const tableModel = model.playerModel.getModelWithId(tableID);
      if(tableModel) removeRow(tableModel);
    } else {
      const modelOutField = model.playerModel.getModelWithId(out_field);
      if(modelOutField) modelOutField.setValue(null);
    }
  });
}

const matchingInit = async documentID => {
  if(!editable) return;

  clearFields();

  try {
    if(documentID) {
      AS.SERVICES.showWaitWindow();

      const fromDataUUID = await getAsfDataUUID(documentID);
      const fromAsfData = await AS.FORMS.ApiUtils.loadAsfData(fromDataUUID);

      AS.SERVICES.hideWaitWindow();

      matching.forEach(async item => {
        const {in_field, out_field} = item;
        const from = fromAsfData.data.find(x => x.id === in_field);

        if(out_field.includes('.')) {
          const tableID = out_field.split('.')[0];
          const fieldTableID = out_field.split('.')[1];
          const tableModel = model.playerModel.getModelWithId(tableID);
          if(tableModel) {
            const tableBlockIndex = getTableBlockIndex(tableModel);
            const tableModelOutField = model.playerModel.getModelWithId(fieldTableID, tableID, tableBlockIndex);
            setField(from, tableModelOutField);
          }
        } else {
          const modelOutField = model.playerModel.getModelWithId(out_field);
          setField(from, modelOutField);
        }
      });
    } else {
      clearFields();
    }
  } catch (err) {
    AS.SERVICES.hideWaitWindow();
    console.log('Произошла ошибка при сопоставлении полей', err.message);
  }
}

if(editable) model.on('valueChange', (_1, _2, value) => matchingInit(value));
