//сопоставление статических полей
//тут только стат. поля, как источник так и приемник
// для сопоставления данных в дин.таблицу, параметры ниже (tableFillParams)
const matching = [];
matching.push({in_field: 'date_left_for_repairs_vu36', out_field: 'date_of_data_collection_from'});
matching.push({in_field: 'date_left_for_repairs_vu36', out_field: 'date_of_data_collection_before'});
matching.push({in_field: 'textbox_repair_point_vybra', out_field: 'textbox_repair_point'});
matching.push({in_field: 'textbox_owner_carriage_vybra', out_field: 'textbox_wagon_owner'});
matching.push({in_field: 'textbox_type_of_repair_vybra', out_field: 'textbox_type_of_repair'});
matching.push({in_field: 'textbox_symptom_so_vybra', out_field: 'textbox_symptom_so'});
matching.push({in_field: 'textbox_document_number_com_pos', out_field: 'textbox_contract_number'});

//параметры заполнения дин.таблицы
// outTableID - айди таблицы приемник
// cmpIDs [
// in_field - источник, может быть записано как поле с дин.таблцы
// out_field - поле приемник в таблице outTableID
// ]
const tableFillParams = [];
tableFillParams.push({
  outTableID: 'table_rejected_parts',
  cmpIDs: [
    {in_field: 'textbox_carriage_number_vybra', out_field: 'numericinput_carriage_number'}, //Номер вагона
    {in_field: 'textbox_сar_type_vybra', out_field: 'textbox_type_carriage'}, //Род вагона
    {in_field: 'textbox_type_of_repair_vybra', out_field: 'textbox_kind_repair'}, //Вид ремонта
    {in_field: 'table_rejected_parts.textbox_name_detail', out_field: 'textbox_name_of_parts'}, //Наименование детали
    {in_field: 'table_rejected_parts.numericinput_missing_details', out_field: 'numericinput_missing_details'} //Отсутствовало деталей
  ]
});

const getAsfDataUUID = async documentID => {
  return new Promise(async resolve => {
    AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/formPlayer/getAsfDataUUID?documentID=${documentID}`,
      resolve, 'text',  null,
      err => {
        console.log(`ERROR [ getAsfDataUUID ]: ${JSON.stringify(err)}`);
        resolve(null);
      }
    );
  });
}

const getValue = (data, cmpID) => {
  data = data.data ? data.data : data;
  return data.find(x => x.id == cmpID);
}

const parseAsfValue = asfDataValue => {
  if(!asfDataValue) return null;
  const {type, value = '', key = ''} = asfDataValue;
  return key ? {type, value, key} : {type, value};
}

const parseAsfTable = asfTable => {
  const result = [];
  try {
    if(!asfTable.hasOwnProperty('data')) return result;

    const data = asfTable.data.filter(x => x.type != 'label');
    if(!data.length) return result;

    const ids = data.map(x => x.id.slice(0, x.id.indexOf('-b'))).uniq();
    let tbi =  data.slice(-1)[0].id;
    tbi = Number(tbi.slice(tbi.indexOf('-b') + 2));

    for(let i = 1; i <= tbi; i++) {
    	const item = {};
      ids.forEach(key => {
        const parseValue = parseAsfValue(getValue(asfTable, `${key}-b${i}`));
        if(parseValue) item[key] = parseValue;
      });
      result.push(item);
    }

    return result;
  } catch (err) {
    console.log('ERROR parseAsfTable', err);
    return result;
  }
}

Array.prototype.uniq = function() {
  return this.filter(function(v, i, a){ return i == a.indexOf(v) });
}

const setField = (from, modelTo, collation_type = null, label = null) => {
  if(from) {
    switch (from.type) {
      case 'textbox':
      case 'textarea':
        if(modelTo && from.hasOwnProperty('value') && from.value) {
          const currentValue = modelTo.getValue();

          switch (collation_type) {
            case 'PREFIX':
              if(currentValue && currentValue != '') {
                if(label && label != '') {
                  modelTo.setValue(`${label} ${from.value}`);
                } else {
                  modelTo.setValue(`${currentValue} ${from.value}`);
                }
              } else {
                if(label && label != '') {
                  modelTo.setValue(`${label} ${from.value}`);
                } else {
                  modelTo.setValue(from.value);
                }
              }
              break;
            case 'POSTFIX':
              if(currentValue && currentValue != '') {
                if(label && label != '') {
                  modelTo.setValue(`${from.value} ${label}`);
                } else {
                  modelTo.setValue(`${from.value} ${currentValue}`);
                }
              } else {
                if(label && label != '') {
                  modelTo.setValue(`${from.value} ${label}`);
                } else {
                  modelTo.setValue(from.value);
                }
              }
              break;
            case 'SPLICE':
              if(currentValue && currentValue != '') {
                modelTo.setValue(`${currentValue}, ${from.value}`);
              } else {
                modelTo.setValue(from.value);
              }
              break;
            default: modelTo.setValue(from.value);
          }

        } else {
          if(modelTo) modelTo.setValue(null);
        }
        break;
      case 'check':
        if(modelTo && from.hasOwnProperty('values')) {
          modelTo.setValue(from.values);
        } else {
          if(modelTo) modelTo.setValue(null);
        }
        break;
      case 'entity':
        if(modelTo && from.hasOwnProperty('key') && from.key && from.key !== 'null') {
          switch (modelTo.asfProperty.config.entity) {
            case "users":
              modelTo.setValue({
                personID: from.key,
                personName: from.value
              });
              break;
            case "departments":
              modelTo.setValue({
                departmentId: from.key,
                departmentName: from.value
              });
              break;
            case "positions":
              modelTo.setValue({
                elementID: from.key,
                elementName: from.value
              });
              break;
          }
        } else {
          if(modelTo) modelTo.setValue(null);
        }
        break;
      default:
        if(modelTo && from.hasOwnProperty('key') && from.key) {
          modelTo.setValue(from.key);
        } else {
          if(modelTo) modelTo.setValue(null);
        }
    }
  } else {
    if(modelTo) modelTo.setValue(null);
  }
}

const removeRow = tableModel => {
  for(const numb in tableModel.getBlockNumbers()) tableModel.removeRow(numb);
}

const clearFields = () => {
  matching.forEach(item => {
    const {out_field} = item;
    const modelOutField = model.playerModel.getModelWithId(out_field);
    if(modelOutField) modelOutField.setValue(null);
  });

  tableFillParams.forEach(item => {
    const {outTableID} = item;
    const tableModel = model.playerModel.getModelWithId(outTableID);
    if(tableModel) removeRow(tableModel);
  });
}

const checkInTablesData = inTablesData => {
  for(const tableID in inTablesData) {
    if(inTablesData[tableID].data.length) {
      return true;
      break;
    }
  }
  return false;
}

const matchingHandler = async documentID => {
  try {
    AS.SERVICES.showWaitWindow();

    const fromDataUUID = await getAsfDataUUID(documentID);
    const fromAsfData = await AS.FORMS.ApiUtils.loadAsfData(fromDataUUID);

    AS.SERVICES.hideWaitWindow();

    //сопоставление статических полей
    matching.forEach(item => {
      const {in_field, out_field} = item;
      const from = fromAsfData.data.find(x => x.id === in_field);

      const modelOutField = model.playerModel.getModelWithId(out_field);
      setField(from, modelOutField, 'SPLICE');
    });

    //сопоставление полей в дин.таблицу(ы)
    tableFillParams.forEach(item => {
      const {outTableID, cmpIDs} = item;
      const tableModel = model.playerModel.getModelWithId(outTableID);

      if(tableModel) {
        //парсинг данных по стат полям
        const statFields = cmpIDs
        .filter(x => !x.in_field.includes('.'))
        .map(x => ({...x, value: getValue(fromAsfData, x.in_field)}));

        //парсинг данных по дин.таблицам
        const inTablesData = {};
        for(let i = 0; i < cmpIDs.length; i++) {
          const {in_field, out_field} = cmpIDs[i];
          if(!in_field.includes('.')) continue;

          const tableID = in_field.split('.')[0];
          if(!inTablesData.hasOwnProperty(tableID)) inTablesData[tableID] = {};
          if(!inTablesData[tableID].hasOwnProperty('data')) {
            inTablesData[tableID]['data'] = parseAsfTable(getValue(fromAsfData, tableID));
          }
          if(!inTablesData[tableID].hasOwnProperty('fields')) inTablesData[tableID]['fields'] = [];
          inTablesData[tableID]['fields'].push({in_field: in_field.split('.')[1], out_field});
        }

        //заполнение дин.таблицы приемника
        if(checkInTablesData(inTablesData)) { //проверка есть ли данные в дин.таблицах источника
          for(const inTable in inTablesData) {
            const {data, fields} = inTablesData[inTable];
            data.forEach(fromBlock => {
              const newBlock = tableModel.createRow();

              fields.forEach(x => {
                const from = fromBlock[x.in_field];
                const tableModelOutField = model.playerModel.getModelWithId(x.out_field, outTableID, newBlock.tableBlockIndex);
                setField(from, tableModelOutField);
              });

              statFields.forEach(x => {
                const tableModelOutField = model.playerModel.getModelWithId(x.out_field, outTableID, newBlock.tableBlockIndex);
                setField(x.value, tableModelOutField);
              });
            });
          };
        } else {
          const newBlock = tableModel.createRow();
          statFields.forEach(x => {
            const tableModelOutField = model.playerModel.getModelWithId(x.out_field, outTableID, newBlock.tableBlockIndex);
            setField(x.value, tableModelOutField);
          });
        }
      }

    });
  } catch (err) {
    AS.SERVICES.hideWaitWindow();
    console.log('Произошла ошибка при сопоставлении полей', err.message);
  }
}

const matchingInit = async documentIDs => {
  if(!editable) return;
  if(!documentIDs) {
    clearFields();
    return;
  }

  if(Array.isArray(documentIDs)) {
    if(!documentIDs.length) {
      clearFields();
      return;
    }
    clearFields();
    documentIDs.forEach(async documentID => {
      matchingHandler(documentID);
    });
  } else {
    clearFields();
    matchingHandler(documentIDs);
  }
}

if(editable) model.on('valueChange', (_1, _2, value) => matchingInit(value));
