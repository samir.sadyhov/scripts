const fileDownload = (id, filename) => {
  let requestUrl = AS.FORMS.ApiUtils.getFullUrl(`rest/api/storage/file/get?identifier=${id}`);
  let xhr = new XMLHttpRequest();
  xhr.open('GET', requestUrl, true);
  AS.FORMS.ApiUtils.addAuthHeader(xhr);
  xhr.responseType = 'arraybuffer';
  xhr.onload = function () {
    if(this.status === 200) {
      let type = xhr.getResponseHeader('Content-Type');
      let blob = new Blob([this.response], {type: type});
      if(typeof window.navigator.msSaveBlob !== 'undefined') {
        window.navigator.msSaveBlob(blob, filename);
      } else {
        let URL = window.URL || window.webkitURL;
        let downloadUrl = URL.createObjectURL(blob);
        if(filename) {
          let a = document.createElement("a");
          if(typeof a.download === 'undefined') {
            window.location = downloadUrl;
          } else {
            a.href = downloadUrl;
            a.download = filename;
            document.body.appendChild(a);
            a.click();
          }
        } else {
          window.location = downloadUrl;
        }
        setTimeout(() => {URL.revokeObjectURL(downloadUrl)}, 100);
      }
    }
  };
  xhr.send();
}

const getFileModels = () => {
  let result = [];
  model.playerModel.models[0].modelBlocks[0].forEach(block => {
    if(block.asfProperty.type === "file") result.push(block);
    if(block.asfProperty.type === "table") {
      if(block.modelBlocks.length > 0) {
        block.modelBlocks.forEach(row => {
          row.forEach(tableBlock => {
            if(tableBlock.asfProperty.type === "file") result.push(tableBlock);
          });
        });
      }
    }
  });
  return result;
};

const initFileDownloader = () => {
  let fileModels = getFileModels();

  fileModels.forEach(fileModel => {
    let props = fileModel.asfProperty;
    let tmpView = view.playerView.getViewWithId(props.id, props.ownerTableId, props.tableBlockIndex);

    //отключаем все стандартные события
    setTimeout(() => {
      $(`[data-asformid="file.filename.${props.id}"]`).off();
      fileModel.on('valueChange', () => {
        $(`[data-asformid="file.filename.${props.id}"]`).off();
        return null;
      });
    }, 100);

    //вешаем свое событие
    if(tmpView) tmpView.container.on('click', () => {
      if(!fileModel.value) return;
      if(!fileModel.value.hasOwnProperty('identifier')) return;
      fileDownload(fileModel.value.identifier, fileModel.value.name);
    });

  });
};

if(window.location.href.indexOf('Synergy') === -1) initFileDownloader();
