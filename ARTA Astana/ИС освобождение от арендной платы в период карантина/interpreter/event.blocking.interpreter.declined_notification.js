var result = true;
var message = "ok";

try {

  let currentFormData = API.getFormData(dataUUID);
  let tenant_mail = UTILS.getValue(currentFormData, 'textbox_tenant_mail');
  let reg_num = UTILS.getValue(currentFormData, 'textbox_app_reg_num').value || '';
  let reg_date = UTILS.getValue(currentFormData, 'date_app_reg_date').value || '';

  // отправка письма
  if(tenant_mail && tenant_mail.hasOwnProperty('value') && tenant_mail.value != '') {

    let sendResult = API.sendNotification({
      header: "Уведомление об отказе в погашении арендной платы. Заявка №" + reg_num + " от " + reg_date,
      message: "Ваша заявка рассмотрена. Вам отказано в погашении арендной платы",
      emails: [tenant_mail.value]
    });

    if(sendResult.errorCode != 0) throw new Error('Произошла ошибка отправки уведомления');
    log.info(sendResult);
    message = 'Уведомление успешно отправлено';

  } else {
    throw new Error('Не найден email получателя');
  }

} catch (err) {
  log.error(err);
  message = err.message;
}
