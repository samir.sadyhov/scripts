var result = true;
var message = "ok";

function customFormatDate(datetime){
  datetime = datetime.split(/\D/);
  return datetime[2] + '.' + datetime[1] + '.' + datetime[0] + ' ' + datetime[3] + ':' + datetime[4];
}

try {
  let currentFormData = API.getFormData(dataUUID);
  let docInfo = API.getDocumentInfo(documentID);

  let matching = [];
  matching.push({from: 'entity_author', to: 'entity_author', type: 'entity'});
  matching.push({from: 'textarea_organization', to: 'textarea_organization', type: 'textarea'});
  matching.push({from: 'textbox_bin_iin', to: 'textbox_bin_iin', type: 'textbox'});
  matching.push({from: 'textbox_address', to: 'textbox_address', type: 'textbox'});
  matching.push({from: 'textbox_phone', to: 'textbox_phone', type: 'textbox'});
  matching.push({from: 'textbox_mail', to: 'textbox_mail', type: 'textbox'});
  matching.push({from: 'textbox_tenant_mail', to: 'textbox_tenant_mail', type: 'textbox'});  
  matching.push({from: 'numericinput_debt_count', to: 'numericinput_debt_count', type: 'numericinput'});

  let notifyAsfData = [];

  matching.forEach(function(id) {
    let fromData = UTILS.getValue(currentFormData, id.from);
    if(fromData && fromData.value) {
      let field = UTILS.createField({id: id.to, type: id.type});
      for(let key in fromData) {
        if(key === 'id' || key === 'type') continue;
        field[key] = fromData[key];
      }
      notifyAsfData.push(field);
    }
  });

  notifyAsfData.push(UTILS.createField({
    id: "reglink_application",
    type: "reglink",
    key: documentID,
    valueID: documentID,
    value: API.getDocMeaningContent(documentID)
  }));

  notifyAsfData.push(UTILS.createField({
    id: "textbox_app_reg_num",
    type: "textbox",
    value: docInfo.number
  }));

  notifyAsfData.push(UTILS.createField({
    id: "date_app_reg_date",
    type: "date",
    key: docInfo.createDate,
    value: customFormatDate(docInfo.createDate)
  }));

  let result = API.createDocRCC('reestr_uvedomlenii_o_pogashenii_arendnoi_platy', notifyAsfData);
  if(result.errorCode != 0) throw new Error("Не удалось создать документ.\n" + result.errorMessage);

  result = API.activateDoc(result.documentID);
  if(result.errorCode != 0) throw new Error("Ошибка активации документа");

  message = "Запись создана\ndocumentID: " + result.documentID;

} catch (err) {
  log.error(err);
  message = err.message;
}
