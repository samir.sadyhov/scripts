const getCurrentDateFormated = () => {
  const currntDate = new Date();
  return currntDate.getFullYear() + '-' + ('0' + (currntDate.getMonth() + 1)).slice(-2) + '-' + ('0' + currntDate.getDate()).slice(-2);
}

const generateBase64Data = () => {
  const bin = model.playerModel.getModelWithId('textbox_bin').getValue();
  const date = getCurrentDateFormated();
  return btoa(escape(encodeURIComponent(bin + ":" + date)));
}

const button = $('<button>', {class: 'ec_button'}).text('Сгенерировать данные для QR');
view.container.append(button);

button.on('click', e => {
  e.preventDefault();
  e.target.blur();
  const base64data = generateBase64Data();
  model.playerModel.getModelWithId('base_64').setValue(base64data);

  AS.SERVICES.showWaitWindow();
  AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/asforms/data/merge", res => {
    AS.SERVICES.hideWaitWindow();
    model.playerModel.hasChanges = false;
    button.fadeOut();
  }, null, JSON.stringify({
    uuid: model.playerModel.asfDataId,
    data: [model.playerModel.getModelWithId('base_64').getAsfData()]
  }), "application/json; charset=utf-8", err => {
    console.log('error', err.responseJSON);
    AS.SERVICES.hideWaitWindow()
  });
});
