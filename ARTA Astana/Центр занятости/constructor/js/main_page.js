const getUrlParameter = sParam => {
  let sPageURL = window.location.search.substring(1), sURLVariables = sPageURL.split('&'), sParameterName;
  for (let i = 0; i < sURLVariables.length; i++) {
    sParameterName = sURLVariables[i].split('=');
    if (sParameterName[0] === sParam)
    return sParameterName[1] === undefined ? true : decodeURIComponent(sParameterName[1]);
  }
}

const searchInRegistry = params => {
  return new Promise((resolve, reject) => {
    rest.synergyGet(`api/registry/data_ext?${params}`,
      res => resolve(res),
      err => reject(err.responseJSON.errorMessage));
  });
}

const searchOrganization = bin => {
  let params = $.param({
    registryCode: 'registry_organizations',
    field: 'textbox_bin',
    condition: 'TEXT_EQUALS',
    value: bin,
    countInPart: 1
  });
  ['textbox_url', 'base_64'].forEach(x => params += `&fields=${x}`);

  return new Promise(resolve => {
    searchInRegistry(params).then(res => {
      res.recordsCount == 0 ? resolve(null) : resolve(res.result);
    }).catch(err => {
      resolve(null);
      console.log('searchOrganization ERROR', err);
    });
  });
}

const goErrorPage = errorMessage => {
  fire({
    type: 'goto_page',
    pageCode: 'error_page',
    pageParams: [
      {
        pageParamName: 'errorMessage',
        value: errorMessage
      }
    ]
  }, 'root-panel');
}

pageHandler('main_page', () => {
  const paramBin = getUrlParameter('bin');
  const base64data = getUrlParameter('g');

  if(!paramBin && !base64data) {
    goErrorPage('Неверная ссылка');
  }

  if(paramBin) {
    fire({
      type: 'goto_page',
      pageCode: 'qr_code_page',
      pageParams: [
        {
          pageParamName: 'bin',
          value: paramBin
        }
      ]
    }, 'root-panel');
  }

  if(base64data) {
    const decodeParams = btoa(encodeURIComponent(decodeURIComponent(unescape(atob(base64data)))));
    const urlParams = decodeURIComponent(atob(decodeParams)).split(':');
    const bin = urlParams[0];

    searchOrganization(bin).then(res => {
      if(!res) goErrorPage(`Не найдено данных по организации с БИН: ${bin}`);

      if(base64data == res[0].fieldValue.base_64) {
        open(res[0].fieldValue.textbox_url, '_self');
      } else {
        goErrorPage(`Ошибка проверки данных по QR коду, переход не возможен`);
      }
    });
  }

});
