const getPageParamValue = paramName => {
  const {paramValues, params, pageParams} = Cons.getCurrentPage();
  const paramID = params.find(x => x.name == paramName).id;
  return paramValues.find(x => x.pageParamID == paramID || x.pageParamName == paramName).value;
}

pageHandler('error_page', () => {
  const errorMessage = getPageParamValue('errorMessage');
  fire({type: 'change_label', text: localizedText(errorMessage, errorMessage, errorMessage, errorMessage)}, 'errorLabel');
});
