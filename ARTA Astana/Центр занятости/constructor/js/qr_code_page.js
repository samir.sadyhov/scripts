const getPageParamValue = paramName => {
  const {paramValues, params, pageParams} = Cons.getCurrentPage();
  const paramID = params.find(x => x.name == paramName).id;
  return paramValues.find(x => x.pageParamID == paramID || x.pageParamName == paramName).value;
}

const searchInRegistry = params => {
  return new Promise((resolve, reject) => {
    rest.synergyGet(`api/registry/data_ext?${params}`,
      res => resolve(res),
      err => reject(err.responseJSON.errorMessage));
  });
}

const searchOrganization = bin => {
  let params = $.param({
    registryCode: 'registry_organizations',
    field: 'textbox_bin',
    condition: 'TEXT_EQUALS',
    value: bin,
    countInPart: 1
  });
  ['textbox_url', 'base_64'].forEach(x => params += `&fields=${x}`);

  return new Promise(resolve => {
    searchInRegistry(params).then(res => {
      res.recordsCount == 0 ? resolve(null) : resolve(res.result);
    }).catch(err => {
      resolve(null);
      console.log('searchOrganization ERROR', err);
    });
  });
}

const goErrorPage = errorMessage => {
  fire({
    type: 'goto_page',
    pageCode: 'error_page',
    pageParams: [
      {
        pageParamName: 'errorMessage',
        value: errorMessage
      }
    ]
  }, 'root-panel');
}

pageHandler('qr_code_page', () => {
  const bin = getPageParamValue('bin');

  searchOrganization(bin).then(res => {
    if(!res) goErrorPage(`Не найдено данных по организации с БИН: ${bin}`);

    const base64data = res[0].fieldValue.base_64;
    const qrCodeURL = `${window.location.origin}/login_qr/?g=${base64data}`;
    const qrCodeIMG = $("<div>").html("<img src='https://chart.googleapis.com/chart?chs=400x400&cht=qr&chl=" + encodeURIComponent(qrCodeURL) + "'>");

    $('#qrCodeContainer').append(qrCodeIMG);
  });
});
