let input = $('#textInput_car_number');
let buttonFindIcon = $('#panel-search span');
let buttonFind = $('#button_find');
let tbody = $('.table-result tbody');

tbody.empty();

const emptyValidator = input => {
  if(input && input.text && input.text !== '') {
    fire({type: 'input_highlight', error: false}, input.code);
    return true;
  } else {
    fire({type: 'input_highlight', error: true}, input.code);
    return false;
  }
}

const inputSearchValidator = input => {
  if(emptyValidator(input)) {
    if(input.text.length < 3) {
      fire({type: 'input_highlight', error: true}, input.code);
      return false;
    }
    if(input && !/[^A-Za-zа-яА-ЯёЁ0-9]/.test(input.text)) {
      fire({type: 'input_highlight', error: false}, input.code);
      return true;
    } else {
      fire({type: 'input_highlight', error: true}, input.code);
      return false;
    }
  }
  return false;
}

const parseResult = (data, docName) => {
  let result = [];
  data.forEach(item => {
    let tmp = {};
    let fieldValue = item.fieldValue;
    tmp.dataUUID = item.dataUUID;
    tmp.docName = docName;
    tmp.docNumber = fieldValue.hasOwnProperty('counter_number') ? fieldValue.counter_number : '';
    tmp.carNumber = '';
    tmp.perevozchik = fieldValue.hasOwnProperty('textbox_name_carrier') ? fieldValue.textbox_name_carrier : '';
    if (fieldValue.hasOwnProperty('textbox_bus_number')) {
      tmp.carNumber = fieldValue.textbox_bus_number;
    } else if (fieldValue.hasOwnProperty('textbox_car_number')) {
      tmp.carNumber = fieldValue.textbox_car_number;
    }
    result.push(tmp);
  });
  return result;
}

const getDocumentWindow = player => {
  let window = $('<div>', {class: 'window-document'});
  let header = $('<div>', {class: 'window-header uk-modal-header'});
  let body = $('<div>', {class: 'window-body'});
  let buttonsHeader = $('<div>', {class: 'window-header-buttons'});
  let closeButton = $('<span class="window-close-button" uk-icon="icon: close; ratio: 1.5">Закрыть </span>');

  header.append(`<h3 style="overflow: hidden; white-space: nowrap; width: calc(100% - 30px);">${player.documentName}</h3>`).append(buttonsHeader);
  buttonsHeader.append(closeButton);
  body.append(player.view.container);
  window.append(header).append(body);

  closeButton.on('click', e => {
    window.fadeOut();
    setTimeout(() => {
      player.destroy();
      window.remove();
    }, 600);
  });

  return window;
}

const openDocument = (dataUUID, documentName) => {
  let player = AS.FORMS.createPlayer();
  player.view.setEditable(false);
  player.showFormData(null, null, dataUUID);
  player.documentName = documentName;
  let window = getDocumentWindow(player);
  $('.root-panel').append(window);
}

const tableFill = data => {
  tbody.empty();
  if(data.length) {
    $('.result-empty').fadeOut();
    data.forEach(item => {
      let tr = $('<tr>');
      tr.append(`<td>${item.docName}</td>`)
      .append(`<td>${item.docNumber}</td>`)
      .append(`<td>${item.carNumber}</td>`)
      .append(`<td>${item.perevozchik}</td>`);
      tbody.append(tr);

      tr.on('click', e => {
        openDocument(item.dataUUID, item.docName);
      });
    });
  } else {
    $('.result-empty').fadeIn();
  }
}

const get_registry_route_list_passenger = searchString => {
  return new Promise(resolve => {
    try {
      let url = 'rest/api/registry/data_ext?registryCode=registry_route_list_passenger&loadData=true&groupTerm=and';
      url += '&field=status&condition=CONTAINS&key=2&term1=or';

      url += `&field1=textbox_bus_number&condition1=TEXT_EQUALS&value1=${searchString}`;
      // url += `&field1=textbox_bus_number&condition1=START&value1=${searchString}`;
      // url += `&field1=textbox_bus_number&condition1=END&value1=${searchString}`;

      url += `&field1=textbox_car_number&condition1=TEXT_EQUALS&value1=${searchString}`;
      // url += `&field1=textbox_car_number&condition1=START&value1=${searchString}`;
      // url += `&field1=textbox_car_number&condition1=END&value1=${searchString}`;

      url += '&fields=counter_number';
      url += '&fields=textbox_bus_number';
      url += '&fields=textbox_car_number';
      url += '&fields=textbox_name_carrier';

      AS.FORMS.ApiUtils.simpleAsyncGet(url).then(res => {
        if(res.recordsCount > 0) {
          resolve(res.result);
        } else {
          resolve([]);
        }
      });
    } catch (e) {
      console.error('ERROR: get_registry_route_list_passenger', e);
      resolve([]);
    }
  });
}

const get_registry_route_liast_and_ttn = searchString => {
  return new Promise(resolve => {
    try {
      let url = 'rest/api/registry/data_ext?registryCode=registry_route_liast_and_ttn&loadData=true&groupTerm=and';
      url += '&field=status&condition=CONTAINS&key=2&term1=or';
      url += `&field1=textbox_car_number&condition1=TEXT_EQUALS&value1=${searchString}`;
      // url += `&field1=textbox_car_number&condition1=START&value1=${searchString}`;
      // url += `&field1=textbox_car_number&condition1=END&value1=${searchString}`;
      url += '&fields=counter_number';
      url += '&fields=textbox_car_number';
      url += '&fields=textbox_name_carrier';

      AS.FORMS.ApiUtils.simpleAsyncGet(url).then(res => {
        if(res.recordsCount > 0) {
          resolve(res.result);
        } else {
          resolve([]);
        }
      });
    } catch (e) {
      console.error('ERROR: get_registry_route_liast_and_ttn', e);
      resolve([]);
    }
  });
}

const search = () => {
  const inputSearch = getCompByCode('textInput_car_number');
  if(!inputSearchValidator(inputSearch)) {
    showMessage('Для поиска данных по авто необходимо ввести три и более символа', 'error');
  }
  let resultSearch = [];
  get_registry_route_list_passenger(inputSearch.text).then(res => {
    let arr = parseResult(res, 'Путевой лист для перевозки пассажиров и багажа');
    resultSearch = resultSearch.concat(arr);
    return get_registry_route_liast_and_ttn(inputSearch.text);
  }).then(res => {
    let arr = parseResult(res, 'Путевой лист для перевозки грузов');
    resultSearch = resultSearch.concat(arr);
  }).then(() => {
    tableFill(resultSearch);
  });
}

buttonFindIcon.off().on('click', e => {
  e.preventDefault();
  e.target.blur();
  search();
});

buttonFind.off().on('click', e => {
  e.preventDefault();
  e.target.blur();
  search();
});

input.off().on('input', e => {
  input.val(input.val().replace(/[^A-Za-zа-яА-ЯёЁ0-9]/g, '').slice(0, 10));
});

input.off().on('keyup', e => {
  if($('#panel-search .cons-input-wrapper').hasClass('cons-color-red')) {
    $('#panel-search .cons-input-wrapper').removeClass('cons-color-red');
  }
  if(e.keyCode === 13) {
    e.preventDefault();
    e.target.blur();
    search();
  }
});
