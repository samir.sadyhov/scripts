const emailRegex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
const parentDepartmentCode = 'department_system_users';
const groupCode = 'user';
let panelTabs = $('.panel-tabs');
let alertContainer = $('.alert_container');
let translations;
let components = [];

const t = (msg, l) => {
  if(!translations) return msg;
  let res = translations.find(x => x.ru == msg || x.kk == msg);
  if(l) {
    return res ? res[l] : msg;
  } else {
    return res ? res[AS.OPTIONS.locale] : msg;
  }
}

const localized = () => components.forEach(component => component.trigger('change_locale'));

const getTemplateEmail = (fio, login, password) => {
  const theme = t('Данные для входа в систему "Система управления транспортными документами"');
  const subject = `${t('Уважаемый(ая)')} ${fio}!<br><br>
  ${t('Ваши данные для входа в систему "Система управления транспортными документами"')}<br>
  <br>${t('Логин:')} <b>${login}</b>
  <br>${t('Пароль:')} <b>${password}</b>
  <br><br>${t('Для входа в систему перейдите по')} <a href="${window.location.origin}/sutd">ссылке</a>`;
  return {theme, subject};
}

const panelData = [];
panelData.push({
  id: '2',
  name: 'Личные данные руководителя',
  blocks: [
    {id: 'boss-mail', type: 'text', placeholder: 'введите e-mail первого руководителя'},
    {id: 'boss-iin', type: 'text', placeholder: 'введите иин первого руководителя'}
  ]
});
panelData.push({
  id: '3',
  name: 'Личные данные',
  blocks: [
    {id: 'user-mail', type: 'text', placeholder: 'введите e-mail'},
    {id: 'user-iin', type: 'text', placeholder: 'введите иин'}
  ]
});
panelData.push({
  id: '4',
  name: 'ФИО',
  blocks: [
    {id: 'user-lastname', type: 'text', placeholder: 'введите фамилию'},
    {id: 'user-firstname', type: 'text', placeholder: 'введите имя'},
    {id: 'user-patronymic', type: 'text', placeholder: 'введите отчество'}
  ]
});
panelData.push({
  id: '5',
  name: 'Авторизационные данные',
  blocks: [
    {id: 'input-password', type: 'password', placeholder: 'введите пароль'},
    {id: 'input-password-confirm', type: 'password', placeholder: 'подтвердите пароль'}
  ]
});

let regData = {
  hideStep2: false,
  regOrgData: null,
  statGovOrgData: null,

  imBoss: false,
  imFL: false,

  user: {
    lastname: null,
    firstname: null,
    patronymic: null,
    email: null,
    iin: null
  },

  boss: {
    email: null,
    iin: null
  },

  role: {
    consignee: null, //грузополучатель
    carrier: null, //перевозчик
    shipper: null //грузоотправитель
  },

  type_transportation: {
    shipping: null, //Перевозка груза
    passengers: null //Перевозка пассажиров и багажа
  },

  password: null
}

//если поле не заполнено - выделить его как невалидное
const emptyValidator = input => {
  if(input.val() !== '') {
    input.removeClass('uk-form-danger');
    return true;
  } else {
    input.addClass('uk-form-danger');
    return false;
  }
}

//валидация введенного email
const emailValidator = input => {
  if(emptyValidator(input)) {
    if(emailRegex.test(input.val())) {
      input.removeClass('uk-form-danger');
      return true;
    } else {
      input.addClass('uk-form-danger');
      return false;
    }
  }
  return false;
}

//валидация бин/иин
const binValidator = input => {
  if(emptyValidator(input)) {
    if(input.val().length != 12) {
      input.addClass('uk-form-danger');
      return false;
    }
    if(/^\d+$/.test(input.val())) {
      input.removeClass('uk-form-danger');
      return true;
    } else {
      input.addClass('uk-form-danger');
      return false;
    }
  }
  return false;
}

const stepValidator = () => {
  let result = true;
  let activeTabID = $('.tab._active').attr('id');
  activeTabID = Number(activeTabID.slice(11));
  if(regData.imBoss && regData.statGovOrgData) changeUserFio(regData.statGovOrgData.fio);

  switch (activeTabID) {
    case 1:
      let roleChecked = 0;
      ['#carrier', '#shipper', '#consignee'].forEach(cmp => {
        if($(cmp).prop('checked')) roleChecked++;
      });

      if(roleChecked === 0) {
        $('.system_role').addClass('uk-form-danger');
        result = false;
      }

      if($('#carrier').prop('checked')) {
        if(!$('#shipping').prop('checked') && !$('#passengers').prop('checked')) {
          $('.type_transportation').addClass('uk-form-danger');
          result = false;
        }
        if(!$('#fiz_l').prop('checked') && !binValidator($('#bin_org'))) {
          result = false;
        }
      }

      if($('#shipper').prop('checked')) {
        if(!$('#fiz_l').prop('checked') && !binValidator($('#bin_org'))) {
          result = false;
        }
      }

      if(!$('#fiz_l').prop('checked') && !regData.regOrgData && !regData.statGovOrgData) {
        showAlert(t('Просьба проверить корректность введенного БИН и выполнить поиск.'), 'warning');
        result = false;
      }

      break;
    case 2:
      if(!emailValidator($('#boss-mail'))) result = false;
      if(!binValidator($('#boss-iin'))) result = false;
      break;
    case 3:
      if(!emailValidator($('#user-mail'))) result = false;
      if(!binValidator($('#user-iin'))) result = false;
      break;
    case 4:
      if(!emptyValidator($('#user-lastname'))) result = false;
      if(!emptyValidator($('#user-firstname'))) result = false;
      break;
    case 5:
      if(!emptyValidator($('#input-password'))) result = false;
      if(!emptyValidator($('#input-password-confirm'))) result = false;
      break;
  }
  return result;
}

const changeUserFio = strFio => {
  strFio = api.parseFIO(strFio);
  regData.user.lastname = strFio.lastname;
  regData.user.firstname = strFio.firstname;
  regData.user.patronymic = strFio.patronymic;
  $('#user-lastname').val(strFio.lastname);
  $('#user-firstname').val(strFio.firstname);
  $('#user-patronymic').val(strFio.patronymic || '');
}

const toTranslit = text => {
  return text.replace(/([а-яё])|([\s_-])|([^a-z\d])/gi,
    function (all, ch, space, words, i) {
      if (space || words) return space ? '_' : '';
      var code = ch.charCodeAt(0),
      index = code == 1025 || code == 1105 ? 0 :
      code > 1071 ? code - 1071 : code - 1039,
      t = ['yo', 'a', 'b', 'v', 'g', 'd', 'e', 'zh', 'z', 'i', 'y', 'k', 'l', 'm', 'n', 'o',
      'p', 'r', 's', 't', 'u', 'f', 'h', 'c', 'ch', 'sh', 'shch', '', 'y', '', 'e', 'yu', 'ya'];
      return t[index];
    });
}

const addIndicator = () => {
  $('#step-indicator').append(`<span class="step active" tabid="1"></span>`);
  for(let i = 1; i < $('.tab').length; i++)
  $('#step-indicator').append(`<span class="step" tabid="${i+1}"></span>`);
}

const switchIndicator = num => {
  num = num - 1;
  if(num == 0 ) {
    $('.step').removeClass('active').removeClass('finish');
    $('.step:eq(0)').addClass('active');
  } else {
    $('.step').removeClass('active').removeClass('finish');
    for(let i = 0; i < num; i++)
    $(`.step:eq(${i})`).removeClass('active').addClass('finish');
    $(`.step:eq(${num})`).addClass('active');
  }
}

const getTabContainer = (id, name) => {
  let stepBlock = $(`<div class="step_name">${t(name)}</div>`);
  stepBlock.on('change_locale', e => stepBlock.text(t(name)));
  components.push(stepBlock);
  return $(`<div id="panel-step-${id}" class="tab">`).append(stepBlock);
}

const getLegend = name => {
  let legend = $(`<legend>${t(name)}</legend>`);
  legend.on('change_locale', e => legend.text(t(name)));
  components.push(legend);
  return legend;
}

const getLabel = (id, name) => {
  let label = $(`<label for="${id}">${t(name)}</label>`);
  label.on('change_locale', e => label.text(t(name)));
  components.push(label);
  return label;
}

const getCheckBoxComponent = (id, label) => {
  let container = $('<div>', {class: 'checkbox_container'});
  let input = $(`<input type="checkbox" id="${id}">`);
  container.append(input).append(getLabel(id, label));
  return container;
}

const getInputBlock = (id, type, plh) => {
  let container = $('<div>', {class: 'uk-margin'});
  let input = $(`<input id="${id}" class="uk-input custom-input" type="${type}" placeholder="${t(plh)}" autocomplete="new-password">`);
  container.append(input);
  input.on('input', e => {
    if(input.hasClass('uk-form-danger')) input.removeClass('uk-form-danger');
  });
  input.on('change_locale', e => input.attr('placeholder', t(plh)));
  components.push(input);
  return container;
}

const renderApp = () => {
  let panel = getTabContainer('1', 'Данные по организации');

  let fieldset = $('<fieldset>', {class: 'system_role'});
  fieldset.append(getLegend('Выберите роль в системе'))
  .append(getCheckBoxComponent('consignee', 'грузополучатель'))
  .append(getCheckBoxComponent('carrier', 'перевозчик'))
  .append(getCheckBoxComponent('shipper', 'грузоотправитель'));

  let fieldsetFl = $('<fieldset>', {class: 'type_transportation'}).hide();
  fieldsetFl.append(getLegend('Выберите вид перевозки'))
  .append(getCheckBoxComponent('shipping', 'Перевозка груза'))
  .append(getCheckBoxComponent('passengers', 'Перевозка пассажиров и багажа'));

  let inputbinorg = $(`<input id="bin_org" class="uk-input custom-input" type="text" placeholder="${t('введите БИН/ИИН организации')}">`);
  let findbutton = $(`<div id="find-button" class="custom_button _dark">${t('Поиск')}</div>`);
  let orgData = $('<div id="org_data">')
  .append($('<div class="search_org">').append(inputbinorg).append(findbutton))
  .append(getCheckBoxComponent('leader', 'Я являюсь руководителем'));

  inputbinorg.on('change_locale', e => {
    inputbinorg.attr('placeholder', t('введите БИН/ИИН организации'));
  });
  components.push(inputbinorg);

  panel.addClass('_active')
  .append(fieldset)
  .append(fieldsetFl)
  .append(getCheckBoxComponent('fiz_l', 'Я являюсь физическим лицом').hide())
  .append(orgData);

  panelTabs.append(panel);

  panelData.forEach(item => {
    let tmpPanel = getTabContainer(item.id, item.name);
    item.blocks.forEach(block => tmpPanel.append(getInputBlock(block.id, block.type, block.placeholder)));
    panelTabs.append(tmpPanel);
  });

  addIndicator();
}

const showHideTabs = params => {
  let {action, num, activeTabID} = params;

  if(action == 'next') {
    if(regData.hideStep2 && num === 1) num++;
    if(regData.imBoss && num === 3) num++;
    if(num === 5) return;
    $(`#${activeTabID}`).removeClass('_active');
    $(`#panel-step-${num+1}`).addClass('_active');
    if(regData.hideStep2) {
      if(num === 2) $('#button-prev').show();
    } else {
      if(num === 1) $('#button-prev').show();
    }
    if(num === 4) $('#button-next').addClass('reg-button').text(t('Зарегистрироваться'));
    switchIndicator(num + 1);
  } else {
    if(regData.hideStep2 && num === 3) num--;
    if(regData.imBoss && num === 5) num--;

    $(`#${activeTabID}`).removeClass('_active');
    if(num-1 != 0) $(`#panel-step-${num-1}`).addClass('_active');
    if(num === 2) $('#button-prev').hide();
    if(num === 5 || (regData.imBoss && num === 4))
    $('#button-next').removeClass('reg-button').text(t('Далее'));

    switchIndicator(num - 1);
  }
}

const switchTabs = action => {
  const activeTabID = $('.tab._active').attr('id');
  if(!activeTabID) return;
  const num = Number(activeTabID.slice(11));
  if(isNaN(num) || num < 1) return;
  showHideTabs({action, num, activeTabID});
}

const showAlert = (msg, type, add) => {
  let container = $('<div uk-alert>');
  container.append('<a class="uk-alert-close" uk-close></a>');
  container.append(`<p>${msg}</p>`);
  switch (type) {
    case 'success':
      container.addClass('uk-alert-success');
      break;
    case 'warning':
      container.addClass('uk-alert-warning');
      break;
    case 'danger':
      container.addClass('uk-alert-danger');
      break;
    default: container.addClass('uk-alert-primary');

  }
  if(add) {
    alertContainer.append(container);
  } else {
    alertContainer.empty().append(container);
  }
}

const clearCheck = () => {
  const cmps = ['#carrier', '#shipper', '#consignee', '#shipping', '#passengers'];
  cmps.forEach(cmp => $(cmp).prop('checked', false).trigger('change'));
}

const disableCheck = () => {
  const cmps = ['#carrier', '#shipper', '#consignee', '#shipping', '#passengers'];
  cmps.forEach(cmp => $(cmp).prop('disabled', true));
}

const setRoles = keys => {
  clearCheck();
  keys = keys.split(', ');
  keys.forEach(key => {
    switch (key) {
      case '1':
        $('#consignee').prop('checked', true).trigger('change');
        break;
      case '2':
        $('#shipper').prop('checked', true).trigger('change');
        break;
      case '3':
        $('#carrier').prop('checked', true).trigger('change');
        break;
    }
  });
}

const setTransportType = keys => {
  keys = keys.split(', ');
  keys.forEach(key => {
    switch (key) {
      case '1':
        $('#shipping').prop('checked', true).trigger('change');
        break;
      case '2':
        $('#passengers').prop('checked', true).trigger('change');
        break;
    }
  });
}

const hideShowStep2 = () => {
  let flCheck = $('#fiz_l');
  let leaderCheck = $('#leader');
  if(flCheck.prop('checked') || leaderCheck.prop('checked')) {
    $('.step[tabid="2"]').fadeOut(200);
    regData.hideStep2 = true;
  } else {
    $('.step[tabid="2"]').fadeIn(200);
    regData.hideStep2 = false;
  }
}

const generatePassword = () => {
  let charSet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-+_';
  return Array.apply(null, Array(6)).map(() => charSet.charAt(Math.random() * charSet.length)).join('');
}

const api = {
  searchOrg: function(){
    return new Promise(async resolve => {
      try {
        const inputBin = $('#bin_org');
        if(!binValidator(inputBin)) {
          showMessage(t('Введите корректный БИН/ИИН организации'), 'error');
        } else {
          let url = 'api/registry/data_ext?registryCode=registry_organizations';
          url += `&field=textbox_bin_iin&condition=TEXT_EQUALS&value=${inputBin.val()}`;
          url += '&fields=textbox_bin_iin&fields=textbox_email&fields=textbox_name_organization&fields=textbox_fio';
          url += '&fields=check_roles&fields=check_transportation_type';
          rest.synergyGet(url, res => {
            if(res.recordsCount > 0) {
              resolve(res.result[0]);
            } else {
              resolve(null);
            }
          });
        }
      } catch (e) {
        console.log(e);
        resolve(null);
      }
    });
  },

  searchOrgInStatGov: function(){
    return new Promise(async resolve => {
      try {
        const inputBin =$('#bin_org');
        if(!binValidator(inputBin)) {
          showMessage(t('Введите корректный БИН/ИИН организации'), 'error');
        } else {
          fetch(`../statgov/rest/api/juridical?bin=${inputBin.val()}&lang=ru`)
          .then(res => res.json())
          .then(res => {
            if(res.success) {
              resolve(res.obj);
            } else {
              resolve(null);
            }
          })
          .catch(err => {
            console.log(err);
            resolve(null);
          });
        }
      } catch (e) {
        console.log(e);
        resolve(null);
      }
    });
  },

  getDepartmentID: function(){
    return new Promise(async resolve => {
      try {
        let url = `api/departments/search?pointer_code=${parentDepartmentCode}&deleted=false&recordsCount=1&searchTypePC=exact`;
        rest.synergyGet(url, res => {
          if(res.length > 0) {
            resolve(res[0]);
          } else {
            resolve(null);
          }
        });
      } catch (e) {
        console.log(e);
        resolve(null);
      }
    });
  },

  getDepartmentInfo: function(departmentID){
    return new Promise(async resolve => {
      try {
        rest.synergyGet(`api/departments/get?departmentID=${departmentID}`, res => resolve(res));
      } catch (e) {
        console.log(e);
        resolve(null);
      }
    });
  },

  addDepartment: function(data){
    return new Promise(async resolve => {
      try {
        rest.synergyPost('api/departments/save?locale=ru', data, "application/x-www-form-urlencoded; charset=UTF-8", function (response) {
          if(response.errorCode == 0) {
            resolve(response);
          } else {
            console.log(response);
            resolve(null);
          }
        }, function (err) {
            console.error(err);
            resolve(null);
        });
      } catch (e) {
        console.log(e);
        resolve(null);
      }
    });
  },

  addPosition: function(departmentID, name, pointersCode){
    return new Promise(async resolve => {
      try {
        let data = {
          departmentID: departmentID,
          nameEn: name,
          nameKz: t(name, 'kk'),
          nameRu: t(name, 'ru'),
          pointersCode: pointersCode
        }
        rest.synergyPost('api/positions/save?locale=ru', data, "application/x-www-form-urlencoded; charset=UTF-8", function (response) {
          if(response.errorCode == 0) {
            resolve(response);
          } else {
            console.log(response);
            resolve(null);
          }
        }, function (err) {
            console.error(err);
            resolve(null);
        });
      } catch (e) {
        console.log(e);
        resolve(null);
      }
    });
  },

  searchPosition: function(){
    return new Promise(async resolve => {
      try {
        let code = 'employee_' + $('#bin_org').val();
        let url = `api/positions/search?pointer_code=${code}&deleted=false&recordsCount=1&searchTypePC=exact`;
        rest.synergyGet(url, res => {
          if(res.length > 0) {
            resolve(res[0]);
          } else {
            resolve(null);
          }
        });
      } catch (e) {
        console.log(e);
        resolve(null);
      }
    });
  },

  positionsAppoint: function(positionID, userID){
    return new Promise(async resolve => {
      try {
        rest.synergyGet(`api/positions/appoint?positionID=${positionID}&userID=${userID}`, res => {
          if(res.errorCode == 0) {
            resolve(res);
          } else {
            console.log(res);
            resolve(null);
          }
        });
      } catch (e) {
        console.log(e);
        resolve(null);
      }
    });
  },

  addUser: function(userData){
    return new Promise(async resolve => {
      try {
        let data = {
          login: userData.email,
          password: userData.password,
          // email: userData.email,
          lastname: userData.lastname,
          firstname: userData.firstname,
          pointersCode: `IIN${userData.iin}`, //issues/49
          isConfigurator: false,
          isAdmin: false,
          isChancellery: false
        };
        if(userData.patronymic) data.patronymic = userData.patronymic;

        rest.synergyPost('api/filecabinet/user/save', data, "application/x-www-form-urlencoded; charset=UTF-8", function (response) {
          if(response.errorCode == 0) {
            resolve(response);
          } else {
            console.log(response);
            resolve(null);
          }
        }, function (err) {
            console.error(err);
            resolve(null);
        });
      } catch (e) {
        console.log(e);
        resolve(null);
      }
    });
  },

  addUserGroup: function(userID){
    return new Promise(async resolve => {
      try {
        rest.synergyGet(`api/storage/groups/add_user?groupCode=${groupCode}&userID=${userID}`, res => resolve(res));
      } catch (e) {
        console.log(e);
        resolve(null);
      }
    });
  },

  createDocRCC: function(registryCode, asfData, sendToActivation = false){
    return new Promise(async resolve => {
      try {
        let settings = {
          url: `${window.location.origin}/Synergy/rest/api/registry/create_doc_rcc`,
          method: "POST",
          headers: {
            "Authorization": "Basic " + btoa(Cons.creds.login + ":" + Cons.creds.password),
            "Content-Type": "application/json; charset=utf-8"
          },
          data: JSON.stringify({
            registryCode: registryCode,
            data: asfData,
            sendToActivation: sendToActivation
          }),
        };
        $.ajax(settings).done(response => {
          if(response.errorCode == 0) {
            resolve(response);
          } else {
            console.log(response);
            resolve(null);
          }
        }).fail(err => {
          console.error(err);
          resolve(null);
        });
      } catch (e) {
        console.log(e);
        resolve(null);
      }
    });
  },

  searchUser: function(param){
    return new Promise(async resolve => {
      try {
        rest.synergyGet(`api/filecabinet/user/checkExistence?${jQuery.param(param)}`, res => resolve(res.result == "true"));
      } catch (e) {
        console.log(e);
        resolve(null);
      }
    });
  },

  getUserCard: function(iin){
    return new Promise(async resolve => {
      try {
        rest.synergyGet(`api/registry/data_ext?registryCode=registry_user_profile&field=textbox_iin&condition=TEXT_EQUALS&value=${iin}&fields=textbox_email&fields=entity_user_uuid&fields=textbox_iin&countInPart=1`, res => resolve(res));
      } catch (e) {
        console.log(e);
        resolve(null);
      }
    });
  },

  sendNotification: function(body){
    return new Promise(async resolve => {
      try {
        $.ajax({
          url: `${window.location.origin}/Synergy/rest/api/notifications/send`,
          method: "POST",
          headers: {
            "Authorization": "Basic " + btoa(Cons.creds.login + ":" + Cons.creds.password),
            "Content-Type": "application/json; charset=utf-8"
          },
          data: JSON.stringify(body),
        }).done(response => {
          if(response.errorCode == 0) {
            resolve(response);
          } else {
            console.log(response);
            resolve(null);
          }
        }).fail(err => {
          console.error(err);
          resolve(null);
        });
      } catch (e) {
        console.log(e);
        resolve(null);
      }
    });
  },

  parseFIO: function(text){
    text = text.split(' ');
    return {
      lastname: text[0] || null,
      firstname: text[1] || null,
      patronymic: text[2] || null
    }
  },

  getRoles: function(){
    let keys = [];
    let values = [];
    for(let key in regData.role) {
      if(regData.role[key]) {
        keys.push(regData.role[key].key);
        values.push(regData.role[key].value);
      }
    }
    return {keys, values};
  },

  getTypeTransport: function(){
    let keys = [];
    let values = [];
    for(let key in regData.type_transportation) {
      if(regData.type_transportation[key]) {
        keys.push(regData.type_transportation[key].key);
        values.push(regData.type_transportation[key].value);
      }
    }
    return {keys, values};
  },

  getTranslations: function(){
    return new Promise(resolve => {
      if(Cons.getAppStore().sutd_translations) {
        resolve(Cons.getAppStore().sutd_translations);
      } else {
        rest.synergyGet(`api/dictionaries/sutd_translations?getColumns=false`, res => {
          let result = [];
          for (let key in res.items) {
            if(res.items[key].Ru.value)
            result.push({
              ru: res.items[key].Ru.value,
              kk: res.items[key].Kk.value
            });
          }
          resolve(result);
        });
      }
    });
  }

}

const parseDate = date => {
  if(!date) return null;
  let key = date.substring(0, date.indexOf('.')).replace('T', ' ');
  let value = key.split(' ')[0].split('-');
  value = [value[2], value[1], value[0]].join('.');
  return {key, value};
}

const registrationContinue = () => {
  let newData = {};

  //1.2. Организация не найдена в системе. Но найдена в stat.gov.kz
  if(regData.statGovOrgData) {
    Cons.showLoader();
    newData = {};

    try {
      //Поиск родительского подразделения
      api.getDepartmentID().then(departmentID => {

        let data = {
          nameEn: regData.statGovOrgData.name,
          nameKz: regData.statGovOrgData.name,
          nameRu: regData.statGovOrgData.name,
          parentDepartmentID: departmentID,
          pointersCode: toTranslit(regData.statGovOrgData.name),
          positionNameEn: "Руководитель",
          positionNameKz: "Руководитель",
          positionNameRu: "Руководитель"
        }

        //Создаем новое подразделение
        return api.addDepartment(data);

      }).then(res => {
        if(!res) throw new Error(t('Ошибка при создании организации'));
        newData.departmentID = res.departmentID;

        //добавляем должность Сотрудники
        return api.addPosition(newData.departmentID, "Сотрудники", `employee_${regData.statGovOrgData.bin}`);

      }).then(employee => {
        newData.employee = employee;

        //issues/49 Закомментировать создание должностей Водители и Медицинские работники. в эти должности пока не назначаются пользователи

        // let promises = [];
        // promises.push(api.addPosition(newData.departmentID, "Медицинские работники", `medical_worker_${regData.statGovOrgData.bin}`));
        // promises.push(api.addPosition(newData.departmentID, "Водители", `driver_${regData.statGovOrgData.bin}`));
        //
        // //добавляем остальные должности
        // return Promise.all(promises);

        return employee;

      }).then(positions => {
        newData.fioBoss = api.parseFIO(regData.statGovOrgData.fio);

        newData.passwordBoss = regData.password;
        if(!regData.imBoss) password = generatePassword();

        //создаем руководителя этого подразделения
        return api.addUser({
          lastname: newData.fioBoss.lastname,
          firstname: newData.fioBoss.firstname,
          patronymic: newData.fioBoss.patronymic,
          email: regData.boss.email,
          iin: regData.boss.iin,
          password: newData.passwordBoss
        });

      }).then(boss => {
        if(!boss) throw new Error(t('Ошибка при создании руководителя организации'));
        newData.bossUserID = boss.userID;

        //получение информации о созданном подразделении
        return api.getDepartmentInfo(newData.departmentID);

      }).then(dep => {
        newData.depInfo = dep;
        newData.bossPositionID = dep.manager.positionID;

        //назначаем руководителя подразделения
        return api.positionsAppoint(newData.bossPositionID, newData.bossUserID);

      }).then(res => {
        if(!res) throw new Error(t('Ошибка при назначении руководителя организации'));

        //добавление польователя в группу
        return api.addUserGroup(newData.bossUserID);
      }).then(res => {
        let roles = api.getRoles();
        let regDate = parseDate(regData.statGovOrgData.registerDate);

        let orgAsfData = [];
        orgAsfData.push({id: "check_roles", type: "check", values: roles.values, keys: roles.keys});
        orgAsfData.push({id: "numericinput_okedCode", type: "numericinput", key: regData.statGovOrgData.okedCode, value: regData.statGovOrgData.okedCode});
        orgAsfData.push({id: "textbox_bin_iin", type: "textbox", value: regData.statGovOrgData.bin});
        orgAsfData.push({id: "textbox_name_organization", type: "textbox", value: newData.depInfo.nameRu});
        orgAsfData.push({id: "textbox_okedName", type: "textbox", value: regData.statGovOrgData.okedName});
        orgAsfData.push({id: "textbox_katoId", type: "textbox", value: regData.statGovOrgData.katoId});
        orgAsfData.push({id: "textbox_katoAddress", type: "textbox", value: regData.statGovOrgData.katoAddress});
        orgAsfData.push({id: "textbox_fio", type: "textbox", value: regData.statGovOrgData.fio});
        orgAsfData.push({id: "textbox_email", type: "textbox", value: regData.boss.email});
        orgAsfData.push({id: "entity_signer", type: "entity", value: regData.statGovOrgData.fio, key: newData.bossUserID});
        orgAsfData.push({id: "entity_boss", type: "entity", value: regData.statGovOrgData.fio, key: newData.bossUserID});
        orgAsfData.push({id: "entity_organization", type: "entity", value: newData.depInfo.nameRu, key: newData.depInfo.departmentID});
        orgAsfData.push({id: "entity_department", type: "entity", value: newData.depInfo.nameRu, key: newData.depInfo.departmentID});
        orgAsfData.push({id: "custom_kato", type: "custom", key: regData.statGovOrgData.katoCode});
        orgAsfData.push({id: "listbox_individual", type: "listbox", key: "2", value: "Нет"});

        if(regDate) orgAsfData.push({id: "date_register_date", type: "date", key: regDate.key, value: regDate.value});

        if(regData.type_transportation.shipping || regData.type_transportation.passengers) {
          let tt = api.getTypeTransport();
          orgAsfData.push({id: "check_transportation_type", type: "check", values: tt.values, keys: tt.keys});
        }

        if(regData.statGovOrgData.secondOkeds) {
          orgAsfData.push({id: "numericinput_secondOkeds", type: "textbox", value: regData.statGovOrgData.secondOkeds});
        }

        //создание записи в реестре организаций
        return api.createDocRCC('registry_organizations', orgAsfData, true);

      }).then(rccOrg => {
        if(!rccOrg) throw new Error(t('Ошибка при создании записи в реестре организации'));
        newData.rccOrg = rccOrg;

        let roles = api.getRoles();
        let userAsfData = [];
        userAsfData.push({id: "check_roles", type: "check", values: roles.values, keys: roles.keys});
        userAsfData.push({id: "textbox_lastname", type: "textbox", value: newData.fioBoss.lastname});
        userAsfData.push({id: "textbox_firstname", type: "textbox", value: newData.fioBoss.firstname});
        userAsfData.push({id: "textbox_patronymic", type: "textbox", value: newData.fioBoss.patronymic});
        userAsfData.push({id: "textbox_iin", type: "textbox", value: regData.boss.iin});
        userAsfData.push({id: "textbox_email", type: "textbox", value: regData.boss.email});
        userAsfData.push({id: "entity_user_uuid", type: "entity", value: regData.statGovOrgData.fio, key: newData.bossUserID});

        if(regData.type_transportation.shipping || regData.type_transportation.passengers) {
          let tt = api.getTypeTransport();
          userAsfData.push({id: "check_transportation_type", type: "check", values: tt.values, keys: tt.keys});
        }

        //создание записи в реестре профилей юзеров
        return api.createDocRCC('registry_user_profile', userAsfData, true);

      }).then(rccBoss => {
        if(!rccBoss) throw new Error(t('Ошибка при создании карточки руководителя'));
        newData.rccBoss = rccBoss;

        if(regData.imBoss) {
          let emlData = getTemplateEmail(regData.statGovOrgData.fio, regData.boss.email, newData.passwordBoss);

          api.sendNotification({
            header: emlData.theme,
            message: emlData.subject,
            emails: [regData.boss.email]
          }).then(res => {
            Cons.hideLoader();
            showAlert(t("Регистрация в системе прошла успешно"), 'success', true);
            setTimeout(() => {
              fire({type: 'goto_page', pageCode: 'auth'}, comp.code);
            }, 2000);
          });
        } else {

          //создаем пользователя
          api.addUser({
            lastname: regData.user.lastname,
            firstname: regData.user.firstname,
            patronymic: regData.user.patronymic,
            email: regData.user.email,
            iin: regData.user.iin,
            password: regData.password
          }).then(user => {
            if(!user) throw new Error(t('Ошибка при создании сотрудника организации'));

            newData.userID = user.userID;
            //добавление польователя в группу
            return api.addUserGroup(newData.userID);

          }).then(res => {

            //назначаем на должность сотрудника
            return api.positionsAppoint(newData.employee.positionID, newData.userID);

          }).then(res => {
            if(!res) throw new Error(t('Ошибка при назначении сотрудника на должность'));

            let roles = api.getRoles();
            let fio = [regData.user.lastname, regData.user.firstname].join(' ');
            let userAsfData = [];
            userAsfData.push({id: "check_roles", type: "check", values: roles.values, keys: roles.keys});
            userAsfData.push({id: "textbox_lastname", type: "textbox", value: regData.user.lastname});
            userAsfData.push({id: "textbox_firstname", type: "textbox", value: regData.user.firstname});
            userAsfData.push({id: "textbox_patronymic", type: "textbox", value: regData.user.patronymic});
            userAsfData.push({id: "textbox_iin", type: "textbox", value: regData.user.iin});
            userAsfData.push({id: "textbox_email", type: "textbox", value: regData.user.email});
            userAsfData.push({id: "entity_user_uuid", type: "entity", value: fio, key: newData.userID});

            if(regData.type_transportation.shipping || regData.type_transportation.passengers) {
              let tt = api.getTypeTransport();
              userAsfData.push({id: "check_transportation_type", type: "check", values: tt.values, keys: tt.keys});
            }

            //создание записи в реестре профилей юзеров
            return api.createDocRCC('registry_user_profile', userAsfData, true);

          }).then(res => {
            if(!res) throw new Error(t('Ошибка при создании карточки пользователя'));

            let fio = [regData.user.lastname, regData.user.firstname, regData.user.patronymic].join(' ').trim();
            let emlData = getTemplateEmail(fio, regData.user.email, regData.password);

            api.sendNotification({
              header: emlData.theme,
              message: emlData.subject,
              emails: [regData.user.email]
            }).then(res => {
              Cons.hideLoader();
              showAlert(t("Регистрация в системе прошла успешно"), 'success', true);
              setTimeout(() => {
                fire({type: 'goto_page', pageCode: 'auth'}, comp.code);
              }, 2000);
            });

          }).catch(e => {
            showMessage(e.message, 'error');
            console.error('ERROR: ' + e.message);
            Cons.hideLoader();
          });

        }

      }).catch(e => {
        showMessage(e.message, 'error');
        console.error('ERROR: ' + e.message);
        Cons.hideLoader();
      });

    } catch (e) {
      showMessage(t('Произошла ошибка при регистрации'), 'error');
      console.error('ERROR: ' + e.message);
      Cons.hideLoader();
    }
  } else if (regData.regOrgData) { //1.3. Организация найдена в системе.
    Cons.showLoader();
    newData = {};

    try {

      //создаем пользователя
      api.addUser({
        lastname: regData.user.lastname,
        firstname: regData.user.firstname,
        patronymic: regData.user.patronymic,
        email: regData.user.email,
        iin: regData.user.iin,
        password: regData.password
      }).then(user => {
        if(!user) throw new Error(t('Ошибка при создании пользователя'));

        newData.userID = user.userID;

        //добавление польователя в группу
        return api.addUserGroup(newData.userID);
      })
      .then(res => api.searchPosition()) //поиск должности по коду
      .then(positionID => {
        if(!positionID) throw new Error(t('Не найдена должность для назначения'));
        newData.positionID = positionID;

        //назначаем на должность сотрудника
        return api.positionsAppoint(newData.positionID, newData.userID);

      }).then(res => {
        if(!res) throw new Error(t('Ошибка при назначении сотрудника на должность'));

        let fio = [regData.user.lastname, regData.user.firstname].join(' ');
        let roles = api.getRoles();
        let userAsfData = [];
        userAsfData.push({id: "check_roles", type: "check", values: roles.values, keys: roles.keys});
        userAsfData.push({id: "textbox_lastname", type: "textbox", value: regData.user.lastname});
        userAsfData.push({id: "textbox_firstname", type: "textbox", value: regData.user.firstname});
        userAsfData.push({id: "textbox_patronymic", type: "textbox", value: regData.user.patronymic});
        userAsfData.push({id: "textbox_iin", type: "textbox", value: regData.user.iin});
        userAsfData.push({id: "textbox_email", type: "textbox", value: regData.user.email});
        userAsfData.push({id: "entity_user_uuid", type: "entity", value: fio, key: newData.userID});

        if(regData.type_transportation.shipping || regData.type_transportation.passengers) {
          let tt = api.getTypeTransport();
          userAsfData.push({id: "check_transportation_type", type: "check", values: tt.values, keys: tt.keys});
        }

        //создание записи в реестре профилей юзеров
        return api.createDocRCC('registry_user_profile', userAsfData, true);

      }).then(res => {
        if(!res) throw new Error(t('Ошибка при создании карточки пользователя'));

        let fio = [regData.user.lastname, regData.user.firstname, regData.user.patronymic].join(' ').trim();
        let emlData = getTemplateEmail(fio, regData.user.email, regData.password);

        api.sendNotification({
          header: emlData.theme,
          message: emlData.subject,
          emails: [regData.user.email]
        }).then(res => {
          Cons.hideLoader();
          showAlert(t("Регистрация в системе прошла успешно"), 'success', true);
          setTimeout(() => {
            fire({type: 'goto_page', pageCode: 'auth'}, comp.code);
          }, 2000);
        });
      }).catch(e => {
        showMessage(e.message, 'error');
        console.error('ERROR: ' + e.message);
        Cons.hideLoader();
      });

    } catch (e) {
      showMessage(t('Произошла ошибка при регистрации'), 'error');
      console.error('ERROR: ' + e.message);
      Cons.hideLoader();
    }
  } else if (regData.imFL) { //2. Пользователь является физическим лицом

    Cons.showLoader();
    newData = {};

    try {

      //Поиск родительского подразделения
      api.getDepartmentID().then(departmentID => {

        let orgNameFL = [regData.user.lastname, regData.user.firstname, regData.user.iin].join('_');

        let data = {
          nameEn: orgNameFL,
          nameKz: orgNameFL,
          nameRu: orgNameFL,
          parentDepartmentID: departmentID,
          pointersCode: toTranslit(orgNameFL),
          positionNameEn: "Руководитель",
          positionNameKz: t("Руководитель", 'kk'),
          positionNameRu: "Руководитель"
        }

        //Создаем новое подразделение
        return api.addDepartment(data);

      }).then(res => {
        if(!res) throw new Error(t('Ошибка при создании организации'));
        newData.departmentID = res.departmentID;

        //получение информации о созданном подразделении
        return api.getDepartmentInfo(newData.departmentID);

      }).then(dep => {
        newData.depInfo = dep;
        newData.bossPositionID = dep.manager.positionID;

        //создаем пользователя
        return api.addUser({
          lastname: regData.user.lastname,
          firstname: regData.user.firstname,
          patronymic: regData.user.patronymic,
          email: regData.user.email,
          iin: regData.user.iin,
          password: regData.password
        });

      }).then(boss => {
        if(!boss) throw new Error(t('Ошибка при создании пользователя'));
        newData.bossUserID = boss.userID;

        //добавления пользователя в группу
        return api.addUserGroup(newData.bossUserID);

      })
      .then(res => api.positionsAppoint(newData.bossPositionID, newData.bossUserID))
      .then(res => {
        if(!res) throw new Error(t('Ошибка при назначении сотрудника на должность'));

        let fio = [regData.user.lastname, regData.user.firstname].join(' ');
        let roles = api.getRoles();
        let userAsfData = [];
        userAsfData.push({id: "check_roles", type: "check", values: roles.values, keys: roles.keys});
        userAsfData.push({id: "textbox_lastname", type: "textbox", value: regData.user.lastname});
        userAsfData.push({id: "textbox_firstname", type: "textbox", value: regData.user.firstname});
        userAsfData.push({id: "textbox_patronymic", type: "textbox", value: regData.user.patronymic});
        userAsfData.push({id: "textbox_iin", type: "textbox", value: regData.user.iin});
        userAsfData.push({id: "textbox_email", type: "textbox", value: regData.user.email});
        userAsfData.push({id: "entity_user_uuid", type: "entity", value: fio, key: newData.bossUserID});

        if(regData.type_transportation.shipping || regData.type_transportation.passengers) {
          let tt = api.getTypeTransport();
          userAsfData.push({id: "check_transportation_type", type: "check", values: tt.values, keys: tt.keys});
        }

        //создание записи в реестре профилей юзеров
        return api.createDocRCC('registry_user_profile', userAsfData, true);

      }).then(res => {
        if(!res) throw new Error(t('Ошибка при создании карточки пользователя'));

        let userName = [regData.user.lastname, regData.user.firstname].join(' ');
        let roles = api.getRoles();
        let orgAsfData = [];
        orgAsfData.push({id: "check_roles", type: "check", values: roles.values, keys: roles.keys});
        orgAsfData.push({id: "textbox_name_organization", type: "textbox", value: newData.depInfo.nameRu});
        orgAsfData.push({id: "listbox_individual", type: "listbox", key: "1", value: "Да"});
        orgAsfData.push({id: "entity_signer", type: "entity", value: userName, key: newData.bossUserID});
        orgAsfData.push({id: "entity_boss", type: "entity", value: userName, key: newData.bossUserID});
        orgAsfData.push({id: "textbox_email", type: "textbox", value: regData.user.email});

        if(regData.type_transportation.shipping || regData.type_transportation.passengers) {
          let tt = api.getTypeTransport();
          orgAsfData.push({id: "check_transportation_type", type: "check", values: tt.values, keys: tt.keys});
        }

        //создание записи в реестре организаций
        return api.createDocRCC('registry_organizations', orgAsfData, true);

      }).then(rccOrg => {
        if(!rccOrg) throw new Error(t('Ошибка при создании записи в реестре организации'));

        let fio = [regData.user.lastname, regData.user.firstname, regData.user.patronymic].join(' ').trim();
        let emlData = getTemplateEmail(fio, regData.user.email, regData.password);

        api.sendNotification({
          header: emlData.theme,
          message: emlData.subject,
          emails: [regData.user.email]
        }).then(res => {
          Cons.hideLoader();
          showAlert(t("Регистрация в системе прошла успешно"), 'success', true);
          setTimeout(() => {
            fire({type: 'goto_page', pageCode: 'auth'}, comp.code);
          }, 2000);
        });

      }).catch(e => {
        showMessage(e.message, 'error');
        console.error('ERROR: ' + e.message);
        Cons.hideLoader();
      });

    } catch (e) {
      showMessage(t('Произошла ошибка при регистрации'), 'error');
      console.error('ERROR: ' + e.message);
      Cons.hideLoader();
    }

  }
}

const registration = () => {
  if($('#input-password').val().length < 3) {
    $('#input-password').addClass('uk-form-danger');
    showMessage(t('Минимальная длина пароля должна составлять 3 символа'), 'error');
    return;
  }

  if($('#input-password').val() != $('#input-password-confirm').val()) {
    $('#input-password-confirm').addClass('uk-form-danger');
    showMessage(t('Пароли не совпадают'), 'error');
    return;
  }

  if(regData.imBoss) {
    api.searchUser({login: regData.boss.email}).then(user => {
      if(user) {
        showMessage(`${t('В системе уже имеется учетная запись с таким email.')} ${t('Попробуйте восстановить учетную запись через страницу "Забыли пароль?"')}`, 'error');
      } else {
        api.searchUser({code: `IIN${regData.boss.iin}`}).then(searchIin => {
          if(searchIin) {
            api.getUserCard(regData.boss.iin).then(data => {
              if(data.recordsCount > 0) {
                data = data.result[0];
                if(data.hasOwnProperty('fieldValue') && data.fieldValue.hasOwnProperty('textbox_email') && data.fieldValue.textbox_email) {
                  showMessage( `${t('В системе уже зарегистрирована учетная запись с таким ИИН.')} ${t('К учетной записи привязана электронная почта')} ${data.fieldValue.textbox_email}. ${t('Попробуйте восстановить доступ к почте на странице "Забыли пароль?"')}`, 'error');
                } else {
                  showMessage(`${t('В системе уже зарегистрирована учетная запись с таким ИИН.')} ${t('Попробуйте восстановить доступ к почте на странице "Забыли пароль?"')}`, 'error');
                }
              } else {
                showMessage(`${t('В системе уже зарегистрирована учетная запись с таким ИИН.')} ${t('Попробуйте восстановить доступ к почте на странице "Забыли пароль?"')}`, 'error');
              }
            });
          } else {
            registrationContinue();
          }
        });
      }
    });
  } else {
    if(regData.boss.email) {
      let boss;
      api.searchUser({login: regData.boss.email}).then(res => {
        boss = res;
        return api.searchUser({login: regData.user.email});
      }).then(user => {
        if(boss) {
          showMessage(t(`В системе уже зарегистрирована учетная запись для руководителя с таким адресом электронной почты. Попробуйте восстановить доступ к учетной записи через страницу "Забыли пароль?".`), 'error');
        } else if (user) {
          showMessage(t(`В системе уже зарегистрирована учетная запись для сотрудника с таким адресом электронной почты. Попробуйте восстановить доступ к учетной записи через страницу "Забыли пароль?"`), 'error');
        } else {
          let bosIIN;
          api.searchUser({code: `IIN${regData.boss.iin}`}).then(bosIIN => {
            bosIIN = res;
            return api.searchUser({code: `IIN${regData.user.iin}`});
          }).then(userIIN => {
            if(bosIIN) {
              api.getUserCard(regData.boss.iin).then(data => {
                if(data.recordsCount > 0) {
                  data = data.result[0];
                  if(data.hasOwnProperty('fieldValue') && data.fieldValue.hasOwnProperty('textbox_email') && data.fieldValue.textbox_email) {
                    showMessage( `${t('В системе уже зарегистрирована учетная запись для руководителя с таким ИИН.')} ${t('К учетной записи привязана электронная почта')} ${data.fieldValue.textbox_email}. ${t('Попробуйте восстановить доступ к почте на странице "Забыли пароль?"')}`, 'error');
                  } else {
                    showMessage(`${t('В системе уже зарегистрирована учетная запись для руководителя с таким ИИН.')} ${t('Попробуйте восстановить доступ к почте на странице "Забыли пароль?"')}`, 'error');
                  }
                } else {
                  showMessage(`${t('В системе уже зарегистрирована учетная запись для руководителя с таким ИИН.')} ${t('Попробуйте восстановить доступ к почте на странице "Забыли пароль?"')}`, 'error');
                }
              });
            } else if (userIIN) {
              api.getUserCard(regData.user.iin).then(data => {
                if(data.recordsCount > 0) {
                  data = data.result[0];
                  if(data.hasOwnProperty('fieldValue') && data.fieldValue.hasOwnProperty('textbox_email') && data.fieldValue.textbox_email) {
                    showMessage( `${t('В системе уже зарегистрирована учетная запись для сотрудника с таким ИИН.')} ${t('К учетной записи привязана электронная почта')} ${data.fieldValue.textbox_email}. ${t('Попробуйте восстановить доступ к почте на странице "Забыли пароль?"')}`, 'error');
                  } else {
                    showMessage(`${t('В системе уже зарегистрирована учетная запись для сотрудника с таким ИИН.')} ${t('Попробуйте восстановить доступ к почте на странице "Забыли пароль?"')}`, 'error');
                  }
                } else {
                  showMessage(`${t('В системе уже зарегистрирована учетная запись для сотрудника с таким ИИН.')} ${t('Попробуйте восстановить доступ к почте на странице "Забыли пароль?"')}`, 'error');
                }
              });
            } else {
              registrationContinue();
            }
          });
        }
      });
    } else {
      api.searchUser({login: regData.user.email}).then(user => {
        if(user) {
          showMessage(`${t('В системе уже имеется учетная запись с таким email.')} ${t('Попробуйте восстановить учетную запись через страницу "Забыли пароль?"')}`, 'error');
        } else {
          api.searchUser({code: `IIN${regData.user.iin}`}).then(searchIin => {
            if(searchIin) {
              api.getUserCard(regData.user.iin).then(data => {
                if(data.recordsCount > 0) {
                  data = data.result[0];
                  if(data.hasOwnProperty('fieldValue') && data.fieldValue.hasOwnProperty('textbox_email') && data.fieldValue.textbox_email) {
                    showMessage( `${t('В системе уже зарегистрирована учетная запись с таким ИИН.')} ${t('К учетной записи привязана электронная почта')} ${data.fieldValue.textbox_email}. ${t('Попробуйте восстановить доступ к почте на странице "Забыли пароль?"')}`, 'error');
                  } else {
                    showMessage(`${t('В системе уже зарегистрирована учетная запись с таким ИИН.')} ${t('Попробуйте восстановить доступ к почте на странице "Забыли пароль?"')}`, 'error');
                  }
                } else {
                  showMessage(`${t('В системе уже зарегистрирована учетная запись с таким ИИН.')} ${t('Попробуйте восстановить доступ к почте на странице "Забыли пароль?"')}`, 'error');
                }
              });
            } else {
              registrationContinue();
            }
          });
        }
      });
    }
  }
}

const hideShowFLcheck = () => {
  if($('#consignee').prop('checked') && !$('#carrier').prop('checked') && !$('#shipper').prop('checked')) {
    $('#fiz_l').parent().show();
  } else {
    $('#fiz_l').parent().hide();
    $('#fiz_l').prop('checked', false).trigger('change');
  }
}

const initListeners = () => {
  let buttonNext = $('#button-next');
  buttonNext.text(t(buttonNext.text()));
  buttonNext.on('click', e => {
    if(stepValidator()) {
      if(buttonNext.hasClass('reg-button')) {
        registration();
      } else {
        switchTabs('next');
      }
    } else {
      showMessage(t('Введите обязательные поля'), 'error');
    }
  });
  buttonNext.on('change_locale', e => {
    buttonNext.text(t(buttonNext.text()));
  });
  components.push(buttonNext);

  let prevButton = $('#button-prev');
  prevButton.text(t(prevButton.text()));
  prevButton.on('click', e => {
    switchTabs('prev');
  });
  prevButton.on('change_locale', e => {
    prevButton.text(t(prevButton.text()));
  });
  components.push(prevButton);

  $('#consignee').on('change', e => {
    $('.system_role').removeClass('uk-form-danger');
    if(e.target.checked) {
      hideShowFLcheck();
      regData.role.consignee = {value: '1', key: 'Грузополучатель'};
    } else {
      regData.role.consignee = null;
    }
  });

  $('#shipper').on('change', e => {
    $('.system_role').removeClass('uk-form-danger');
    hideShowFLcheck();
    if(e.target.checked) {
      regData.role.shipper = {value: '2', key: 'Грузоотправитель'};
    } else {
      regData.role.shipper = null;
    }
  });

  $('#carrier').on('change', e => {
    $('.system_role').removeClass('uk-form-danger');
    hideShowFLcheck();
    if(e.target.checked) {
      $('.type_transportation').show();
      regData.role.carrier = {value: '3', key: 'Перевозчик'};
    } else {
      $('.type_transportation').hide();
      regData.role.carrier = null;
    }
  });

  $('#shipping').on('change', e => {
    $('.type_transportation').removeClass('uk-form-danger');
    if(e.target.checked) {
      regData.type_transportation.shipping = {value: '1', key: 'Перевозка груза'};
    } else {
      regData.type_transportation.shipping = null;
    }
  });

  $('#passengers').on('change', e => {
    $('.type_transportation').removeClass('uk-form-danger');
    if(e.target.checked) {
      regData.type_transportation.passengers = {value: '2', key: 'Перевозка пассажиров и багажа'};
    } else {
      regData.type_transportation.passengers = null;
    }
  });

  $('#fiz_l').on('change', e => {
    if(e.target.checked) {
      $('#org_data').fadeOut(200);
      $('#bin_org').val('');
      regData.regOrgData = null;
      regData.statGovOrgData = null;
      alertContainer.empty();
      regData.imFL = true;
      $('#leader').prop('checked', false).trigger('change');
    } else {
      $('#org_data').fadeIn(200);
      regData.imFL = false;
    }
    hideShowStep2();
  });

  $('#leader').on('change', e => {
    if(e.target.checked) {
      regData.imBoss = true;
      $('.step[tabid="4"]').fadeOut(200);
    } else {
      regData.imBoss = false;
      $('.step[tabid="4"]').fadeIn(200);
    }
    hideShowStep2();
  });

  let inputBin = $('#bin_org');
  inputBin.on('input', e => {
    inputBin.val(inputBin.val().replace(/[^\d]/g,'').slice(0,12));
    inputBin.removeClass('uk-form-danger');
  });

  let inputUserIin = $('#user-iin');
  inputUserIin.on('input', e => {
    inputUserIin.val(inputUserIin.val().replace(/[^\d]/g,'').slice(0,12));
    regData.user.iin = inputUserIin.val();
    if(regData.imBoss) regData.boss.iin = inputUserIin.val();
    inputUserIin.removeClass('uk-form-danger');
  });

  let inputBossIin = $('#boss-iin');
  inputBossIin.on('input', e => {
    inputBossIin.val(inputBossIin.val().replace(/[^\d]/g,'').slice(0,12));
    regData.boss.iin = inputBossIin.val();
    inputBossIin.removeClass('uk-form-danger');
  });

  let userMailInput = $('#user-mail');
  userMailInput.on('input', e => {
    userMailInput.val(userMailInput.val().replace(/[^a-zA-Z\d.@\-_]/g,''));
    regData.user.email = userMailInput.val();
    if(regData.imBoss) regData.boss.email = userMailInput.val();
    userMailInput.removeClass('uk-form-danger');
  });

  let bossMailInput = $('#boss-mail');
  bossMailInput.on('input', e => {
    bossMailInput.val(bossMailInput.val().replace(/[^a-zA-Z\d.@\-_]/g,''));
    regData.boss.email = bossMailInput.val();
    bossMailInput.removeClass('uk-form-danger');
  });

  $('#input-password').on('input', e => {
    regData.password = $('#input-password').val();
    $('#input-password').removeClass('uk-form-danger');
  });
  $('#user-lastname').on('input', e => {
    regData.user.lastname = $('#user-lastname').val();
    $('#user-lastname').removeClass('uk-form-danger');
  });
  $('#user-firstname').on('input', e => {
    regData.user.firstname = $('#user-firstname').val();
    $('#user-firstname').removeClass('uk-form-danger');
  });
  $('#user-patronymic').on('input', e => {
    regData.user.patronymic = $('#user-patronymic').val();
    $('#user-patronymic').removeClass('uk-form-danger');
  });

  let searchOrgButton = $('#find-button');
  searchOrgButton.on('click', e => {
    api.searchOrg().then(res => {
      if(res) {
        regData.regOrgData = res;
        alertContainer.empty();
        if(res.fieldValue.hasOwnProperty('textbox_email')) bossMailInput.val(res.fieldValue.textbox_email);
        if(res.fieldValue.hasOwnProperty('textbox_name_organization') && res.fieldValue.hasOwnProperty('textbox_bin_iin'))
        showAlert(`${t('БИН:')} ${res.fieldValue.textbox_bin_iin}<br>${t('Наименование организации:')} ${res.fieldValue.textbox_name_organization}`, 'primary', true);
        if(res.fieldKey.hasOwnProperty('check_roles')) setRoles(res.fieldKey.check_roles);
        if(res.fieldKey.hasOwnProperty('check_transportation_type')) setTransportType(res.fieldKey.check_transportation_type);
        disableCheck();
        $('#leader').parent().hide();
        $('.step[tabid="2"]').fadeOut(200);
        regData.hideStep2 = true;
      } else {
        regData.regOrgData = null;
        userMailInput.val('');
        bossMailInput.val('');
        api.searchOrgInStatGov().then(statRes => {
          if(statRes) {
            regData.statGovOrgData = statRes;
            showAlert(`${t('Организация с таким БИН не была найдена в системе')}.<br>${t('Будет создана новая организация в системе на основании данных из stat.gov.kz')}`, 'danger');
            showAlert(`${t('БИН:')} ${statRes.bin}<br>${t('Наименование организации:')} ${statRes.name}`, 'primary', true);
          } else {
            regData.statGovOrgData = null;
            showAlert(`${t('Организация с таким БИН не была найдена в системе и в системе stat.gov.kz.')}<br>${t('Просьба проверить корректность введенного БИН')}`, 'danger');
          }
        });
      }
    });

  });

  searchOrgButton.on('change_locale', e => {
    searchOrgButton.text(t('Поиск'));
  });
  components.push(searchOrgButton);

}

function localeClickerRegistration() {
  localized();
}

Cons.setAppStore({
  localeClickerRegistration: {
    localeClickerRegistration
  }
});

api.getTranslations().then(res => {
  translations = res;
  Cons.setAppStore({sutd_translations: res});
  renderApp();
  initListeners();
});
