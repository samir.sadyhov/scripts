let translations = Cons.getAppStore().sutd_translations;
let button = $('.email-bt[type="button"]');
let textCall = $('<div class="text-call">');
let buttonName = $('<span>');

const t = (msg, l) => {
  if(!translations) return msg;
  let res = translations.find(x => x.ru == msg || x.kk == msg);
  if(l) {
    return res ? res[l] : msg;
  } else {
    return res ? res[AS.OPTIONS.locale] : msg;
  }
}

textCall.append(buttonName);
button.append(textCall);

button.attr('title', t('Обратная связь'));
buttonName.html(t('Обратная связь').replace(' ', '<br>'));

button.on('click', e => {
  e.preventDefault();
  e.target.blur();
  fire({type: 'set_hidden', hidden: false}, 'panel_feedback');
});


function localeClickerFeedback() {
  button.attr('title', t('Обратная связь'));
  buttonName.html(t('Обратная связь').replace(' ', '<br>'));
}

Cons.setAppStore({
  localeClickerFeedback: {
    localeClickerFeedback
  }
});
