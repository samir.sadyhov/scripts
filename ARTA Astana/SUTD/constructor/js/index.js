$('[value="en"]').hide();

const hideShowComp = () => {
  switch (Cons.getCurrentPage().code) {
    case 'auth':
      if(AS.OPTIONS.locale == 'kk') {
        $('#button_manual_kz').show();
        $('#button_manual').hide();
      } else {
        $('#button_manual').show();
        $('#button_manual_kz').hide();
      }
      break;
  }
}

const getTranslations = () => {
  return new Promise(resolve => {
    if(Cons.getAppStore().sutd_translations) {
      resolve(Cons.getAppStore().sutd_translations);
    } else {
      rest.synergyGet(`api/dictionaries/sutd_translations?getColumns=false`, res => {
        let result = [];
        for (let key in res.items) {
          if(res.items[key].Ru.value)
          result.push({ru: res.items[key].Ru.value, kk: res.items[key].Kk.value});
        }
        resolve(result);
      });
    }
  });
}

const localized = () => {
  let store = Cons.getAppStore();
  console.log(store);
  // такс этa фигня нужна для страницы регистрации
  if (store && store.localeClickerRegistration && store.localeClickerRegistration.localeClickerRegistration) {
    store.localeClickerRegistration.localeClickerRegistration();
  }
  // такс этa фигня нужна для кнопки обратной связи
  if (store && store.localeClickerFeedback && store.localeClickerFeedback.localeClickerFeedback) {
    store.localeClickerFeedback.localeClickerFeedback();
  }
}

setTimeout(() => {
  hideShowComp();
}, 200);

getTranslations().then(res => {
  Cons.setAppStore({sutd_translations: res});
});

if($("#localeSelector").length) {
  AS.OPTIONS.locale = $('#localeSelector').val();

  $("#localeSelector").off().on('change', function() {
    AS.OPTIONS.locale = this.value;
    hideShowComp();
    localized();
  });
}
