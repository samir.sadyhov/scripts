Array.prototype.equals = function(array, strict = true) {
  if(!array) return false;
  if(this.length != array.length) return false;
  for(let i = 0; i < this.length; i++) {
    if(this[i] instanceof Array && array[i] instanceof Array) {
      if(!this[i].equals(array[i], strict)) return false;
    } else if(strict && this[i] != array[i]) {
      return false;
    } else if(!strict) {
      return this.sort().equals(array.sort(), true);
    }
  }
  return true;
}

const strToArr = (str, sep) => str.replace( /\s/g, "").split(sep).sort();

const eventShow = {type: 'set_hidden', hidden: false};
const eventHide = {type: 'set_hidden', hidden: true};

const getTableBlockIndex = tableData => {
  let id = tableData[tableData.length - 1].id;
  return Number(id.slice(id.indexOf('-b') + 2));
}

const parseAsfItem = item => {
  switch (item.type) {
    case "check":
      return item.values
      break;
    case "listbox":
      return item.key
      break;
    case "textbox":
      return item.value
      break;
    default:
    return null;
  }
}

const getPortalSettings = () => {
  const rows = [
    'check_roles',
    'check_refers_role',
    'check_transportation_type',
    'check_type_transport',
    'listbox_workflow',
    'listbox_ttn',
    'listbox_ttn_create',
    'listbox_pl_cargo',
    'listbox_pl_cargo_create',
    'listbox_pl_passengers',
    'listbox_pl_passengers_create',
    'listbox_arm_inspector',
    'listbox_drivers',
    'listbox_med_employees',
    'listbox_trailer'
  ];
  return new Promise(resolve => {
    try {
      AS.FORMS.ApiUtils.simpleAsyncGet('rest/api/registry/data_ext?registryCode=registry_portal_settings&countInPart=1&loadData=false')
      .then(res => AS.FORMS.ApiUtils.loadAsfData(res.data[0].dataUUID))
      .then(asfData => {
        let settings = [];
        let table = asfData.data.find(x => x.id == 'table_view_pages').data;
        for(let i = 1; i <= getTableBlockIndex(table); i++) {
          let param = {};
          rows.forEach(rowID => {
            let tmp = table.find(x => x.id == `${rowID}-b${i}`);
            param[rowID] = parseAsfItem(tmp);
          });
          settings.push(param);
        }
        resolve(settings);
      });
    } catch (e) {
      resolve(null);
    }
  });
}

const parseSetting = settings => {
  return new Promise(resolve => {
    try {
      let portal_settings;
      let userGroups = Cons.getAppStore().userGroups;

      if(userGroups.find(x => x.groupCode === "Inspektory")) {
        portal_settings = settings.find(x => x.check_roles.indexOf('1') !== -1);
        if(portal_settings) {
          for (let key in portal_settings) {
            if (key.indexOf('listbox') !== -1)
            portal_settings[key] == '1' ?  portal_settings[key] = true : portal_settings[key] = false;
          }
          resolve(portal_settings);
        } else {
          resolve(false);
        }
      } else if(userGroups.find(x => x.groupCode === "user")) {
        let url = 'rest/api/registry/data_ext?registryCode=registry_user_profile&countInPart=1';
        url += `&field=entity_user_uuid&condition=CONTAINS&key=${AS.OPTIONS.currentUser.userid}`;
        url += '&fields=entity_user_uuid&fields=check_roles&fields=check_transportation_type&fields=check_bus_or_taxi';
        url += '&fields=textbox_iin'
        AS.FORMS.ApiUtils.simpleAsyncGet(url).then(res => {
          if(res && res.recordsCount > 0) {
            portal_settings = settings.filter(x => x.check_roles.indexOf('2') !== -1);
            let userRoles = strToArr(res.result[0].fieldKey.check_roles, ',');
            let userTransportationType = [null];
            if(res.result[0].fieldKey.check_transportation_type)
            userTransportationType = strToArr(res.result[0].fieldKey.check_transportation_type, ',');

            portal_settings = portal_settings.find(x =>
              x.check_refers_role.equals(userRoles) &&
              x.check_transportation_type.equals(userTransportationType)
            );
            if(portal_settings) {
              for (let key in portal_settings) {
                if (key.indexOf('listbox') !== -1)
                portal_settings[key] == '1' ?  portal_settings[key] = true : portal_settings[key] = false;
              }
              if(res.result[0].fieldKey.check_bus_or_taxi) {
                portal_settings.check_bus_or_taxi = strToArr(res.result[0].fieldKey.check_bus_or_taxi, ',');
              } else {
                portal_settings.check_bus_or_taxi = [null];
              }
              if(res.result[0].fieldValue.hasOwnProperty('textbox_iin')) {
                portal_settings.user_iin = res.result[0].fieldValue.textbox_iin;
              } else {
                portal_settings.user_iin = null;
              }
              resolve(portal_settings);
            } else {
              resolve(false);
            }
          } else {
            resolve(false);
          }
        });
      } else if(userGroups.find(x => x.groupCode === "Admin")) {
        resolve(false);
      }
    } catch (e) {
      resolve(false);
      console.log(e);
    }
  });
}

const hideShowComponents = () => {
  let pageCode = Cons.getCurrentPage().code;
  if(pageCode == 'auth') return;

  let settings = Cons.getAppStore().portal_settings;
  if(settings === undefined) {
    setTimeout(() => {
      hideShowComponents();
    }, 1000);
  }

  if(!settings) {
    if($('#button_workflow').length) fire(eventShow, 'button_workflow');
    if($('#button_ttn').length) fire(eventShow, 'button_ttn');
    if($('#buttn_pl_cargo').length) fire(eventShow, 'buttn_pl_cargo');
    if($('#button_pl_passenger').length) fire(eventShow, 'button_pl_passenger');
    if($('#button_create').length) fire(eventShow, 'button_create');
  } else {
    for(let key in settings) {
      if(settings[key]) {
        switch (key) {
          case 'listbox_workflow':
            if($('#button_workflow').length) fire(eventShow, 'button_workflow');
            break;
          case 'listbox_ttn':
            if($('#button_ttn').length) fire(eventShow, 'button_ttn');
            break;
          case 'listbox_pl_cargo':
            if($('#buttn_pl_cargo').length) fire(eventShow, 'buttn_pl_cargo');
            break;
          case 'listbox_pl_passengers':
            if($('#button_pl_passenger').length) fire(eventShow, 'button_pl_passenger');
            break;

          case 'listbox_ttn_create':
            if(pageCode == 'page_ttn') if($('#button_create').length) fire(eventShow, 'button_create');
            break;
          case 'listbox_pl_cargo_create':
            if(pageCode == 'page_pl_cargo') if($('#button_create').length) fire(eventShow, 'button_create');
            break;
          case 'listbox_pl_passengers_create':
            if(pageCode == 'page_pl_passenger') if($('#button_create').length) fire(eventShow, 'button_create');
            break;
          case 'listbox_arm_inspector':
            if(pageCode == 'page_cabinet') if($('#arm_inspector').length) fire(eventShow, 'arm_inspector');
            break;
        }
      }
    }
  }
}

const goPage = settings => {
  let event = {type: 'goto_page'};
  if(!settings) {
    event.pageCode = 'page_workflow';
  } else if (settings.listbox_workflow) {
    event.pageCode = 'page_workflow';
  } else if (settings.listbox_ttn) {
    event.pageCode = 'page_ttn';
  } else if (settings.listbox_pl_cargo) {
    event.pageCode = 'page_pl_cargo';
  } else if (settings.listbox_pl_passengers) {
    event.pageCode = 'page_pl_passenger';
  } else if (settings.listbox_arm_inspector) {
    event.pageCode = 'inspectors_arm';
  } else {
    event.pageCode = 'page_workflow';
  }
  fire(event, 'button_auth');
}

pageHandler('auth', () => {
  if (Cons.getAppStore().auth_page_listener) return;

  AS.apiAuth.setCredentials(Cons.creds.login, Cons.creds.password);
  AS.OPTIONS.login = Cons.creds.login;
  AS.OPTIONS.password = Cons.creds.password;

  addListener('auth_success', 'button_auth', authed => {
    Cons.showLoader();
    const {login, password} = authed.creds;
    Cons.setAppStore({userGroups: authed.data.groups});
    AS.apiAuth.setCredentials(login, password);
    AS.OPTIONS.login = login;
    AS.OPTIONS.password = password;
    AS.OPTIONS.currentUser = authed.data.person;

    getPortalSettings()
    .then(allSettings => parseSetting(allSettings))
    .then(portal_settings => {
      Cons.setAppStore({portal_settings: portal_settings});
      Cons.hideLoader();
      goPage(portal_settings);
    });

  });
  Cons.setAppStore({auth_page_listener: true});
});

AS.SERVICES.showWaitWindow = () => Cons.showLoader();
AS.SERVICES.hideWaitWindow = () => Cons.hideLoader();

hideShowComponents();
