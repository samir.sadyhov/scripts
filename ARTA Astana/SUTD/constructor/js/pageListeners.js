const eventShow = {type: 'set_hidden', hidden: false};
const eventHide = {type: 'set_hidden', hidden: true};

const checkRouteActive = uuid => {
  return new Promise(resolve => {
    try {
      AS.FORMS.ApiUtils.getDocumentIdentifier(uuid)
      .then(docID => AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/docflow/document_actions?documentID=${docID}`))
      .then(res => {
        res = res.find(x => x.operation == 'RUN');
        if(res) {
          resolve(false);
        } else {
          resolve(true);
        }
      });
    } catch (e) {
      console.log('checkRouteActive error', e);
      resolve(false);
    }
  })
}

pageHandler('page_info_ttn', () => {
  const formPlayer = getCompByCode('formPlayer-1');
  checkRouteActive(formPlayer.dataId).then(result => {
    if(result) {
      fire(eventHide, 'button-edit');
      fire(eventHide, 'button-send');
    } else {
      fire(eventShow, 'button-edit');
      fire(eventShow, 'button-send');
    }
  });
});

pageHandler('show_pl_cargo', () => {
  const formPlayer = getCompByCode('formPlayer-1');
  checkRouteActive(formPlayer.dataId).then(result => {
    if(result) {
      fire(eventHide, 'button-edit');
      fire(eventHide, 'button-send');
    } else {
      fire(eventShow, 'button-edit');
      fire(eventShow, 'button-send');
    }
  });
});

pageHandler('page_info_pl_passenger', () => {
  const formPlayer = getCompByCode('formPlayer-1');
  checkRouteActive(formPlayer.dataId).then(result => {
    if(result) {
      fire(eventHide, 'button-edit');
      fire(eventHide, 'button-send');
    } else {
      fire(eventShow, 'button-edit');
      fire(eventShow, 'button-send');
    }
  });
});
