//если поле не заполнено - выделить его как невалидное

const glob_login = "guest_user";
const glob_pass = "mKoBViaP";

// const glob_login = Cons.creds.login;
// const glob_pass = Cons.creds.password;

const emptyValidator = (input) => {
    if (input && input.text && input.text !== '') {
        fire({ type: 'input_highlight', error: false }, input.code);
        return true;
    } else {
        fire({ type: 'input_highlight', error: true }, input.code);
        return false;
    }
}

// registryValidator
const registryValidator = (registryClass) => {
    if ($("." + registryClass).length == 1) {
        $(".registry-error-handler").removeClass("ns-invalidInput");
        return true;
    } else {
        $(".registry-error-handler").addClass("ns-invalidInput");
        return false;
    }
}

//валидация введенного email
const emailValidator = (input) => {
    if (emptyValidator(input)) {
        let regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
        if (input && regex.test(input.text)) {
            fire({ type: 'input_highlight', error: false }, input.code);
            return true;
        } else {
            fire({ type: 'input_highlight', error: true }, input.code);
            return false;
        }
    }
    return false;
}

//метод поиска юзера, кастомное апи
const searchUser = async (email) => {
    const res = await fetch(`${window.origin}/crowdsourcing/rest/api/user/search?email=${email}`, {
        headers: {
            'Authorization': "Basic " + btoa(Cons.creds.login + ":" + Cons.creds.password)
        }
    });
    return await res.json();
    return true;
}


const setDropDownAddListener = () => {

    if (Cons.getAppStore().drop_down_roles_menu) return
    Cons.setAppStore({ drop_down_roles_menu: true });

    addListener('loaded_form_data', 'formPlayer-drop-down-roles-menu', event => {
        const playerModel = event.model;
        const playerView = event.view;
        const dataUUID = event.model.asfDataId;

        let drop_down_menu_value = playerModel.getModelWithId("system_roles_dictionary").getValue();

        if (drop_down_menu_value && drop_down_menu_value[0]) {
            console.log(drop_down_menu_value);
            $("#" + drop_down_menu_value[0]).addClass("drop-down-form-visible");
            $("#" + drop_down_menu_value[0]).removeClass("uk-hidden");
            $("#" + drop_down_menu_value[0]).attr("drop-down-value", drop_down_menu_value[0]);
            $("#" + drop_down_menu_value[0]).css({ "display": "" });
        }

        playerModel.getModelWithId("system_roles_dictionary").on(AS.FORMS.EVENT_TYPE.valueChange, function () {
            //console.log(playerModel.getModelWithId("system_roles_dictionary").getValue());
            drop_down_menu_value = playerModel.getModelWithId("system_roles_dictionary").getValue();

            if ($(".drop-down-form-visible").length > 0) {
                $(".drop-down-form-visible").css({ "display": "none" });
                $(".drop-down-form-visible").removeClass("drop-down-form-visible");
            }

            if (drop_down_menu_value && drop_down_menu_value[0]) {
                $("#" + drop_down_menu_value[0]).addClass("drop-down-form-visible");
                $("#" + drop_down_menu_value[0]).attr("drop-down-value", drop_down_menu_value[0]);
                $("#" + drop_down_menu_value[0]).removeClass("uk-hidden");
                $("#" + drop_down_menu_value[0]).css({ "display": "" });
            }
        });
    });
}

const getDepartmentsList = async function () {
    return new Promise(async function (resolve) {
        try {
            $.ajax({
                "url": window.origin + "/Synergy/rest/api/departments/list",
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "Authorization": ("Basic " + btoa(glob_login + ":" + glob_pass))
                },
            }).done(function (response) {
                resolve(response);
            });
        } catch (err) {
            resolve({
                result: [],
                recordsCount: 0
            });
        }
    });
}

const getAsfDataUUID = async function (documentID) {
    return new Promise(async function (resolve) {
        try {
            $.ajax({
                "url": window.origin + "/Synergy/rest/api/formPlayer/getAsfDataUUID?documentID=" + documentID,
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "Authorization": ("Basic " + btoa(glob_login + ":" + glob_pass))
                },
            }).done(function (response) {
                resolve(response);
            });
        } catch (err) {
            resolve("");
        }
    });
}

const getDataByDataUUID = async function (dataUUID) {
    return new Promise(async function (resolve) {
        try {
            $.ajax({
                "url": window.origin + "/Synergy/rest/api/asforms/data/" + dataUUID,
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "Authorization": ("Basic " + btoa(glob_login + ":" + glob_pass))
                },
            }).done(function (response) {
                resolve(response);
            });
        } catch (err) {
            resolve("");
        }
    });
}

const mergeAuthorData = async function (entityData) {
    return new Promise(async function (resolve) {
        $.ajax({
            "url": window.origin + "/Synergy/rest/api/asforms/data/merge",
            "method": "POST",
            "timeout": 0,
            "headers": {
                "Authorization": ("Basic " + btoa(glob_login + ":" + glob_pass)),
                "Content-Type": "application/json",
            },
            "data": JSON.stringify(entityData),
        }).done(function (response) {
            console.log(response);
            resolve(response);
        });
    });
}

const getPositionsList = async function (departmentID) {
    return new Promise(async function (resolve) {
        try {
            $.ajax({
                "url": window.origin + "/Synergy/rest/api/departments/content?departmentID=" + departmentID,
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "Authorization": ("Basic " + btoa(glob_login + ":" + glob_pass))
                },
            }).done(function (response) {
                resolve(response);
            });
        } catch (err) {
            resolve({
                result: [],
                recordsCount: 0
            });
        }
    });
}

// Выполняем скрипт только в странице регистрации
pageHandler('registration', () => {
    setDropDownAddListener();
    if (Cons.getAppStore().button_registration_listener) return;
    Cons.setAppStore({ button_registration_listener: true });

    // Выполняем действие только при нажатии на кнопку register
    addListener('button_click', 'register', () => {

        // Получаем все нужные компоненты
        let fname = getCompByCode('fname');
        let lname = getCompByCode('lname');
        let email = getCompByCode('email');
        let passwordConfrim = getCompByCode('passwordConfirm');
        let password = getCompByCode('password');
        let registry = $(".drop-down-form-visible").attr("drop-down-value") + "-registry-chooser";

        //обязательные поля заполнены, email корректен
        let success = emptyValidator(fname) &
            emptyValidator(lname) &
            emptyValidator(password) &
            emptyValidator(passwordConfrim) &
            emailValidator(email) &
            registryValidator(registry);

        if (!success) {
            //отображаем сообщение с типом “ошибка”
            showMessage('Не все поля формы регистрации были заполнены', 'error');
            return;
        }

        //поиск юзера в системе
        searchUser(email.text).then(user => {
            if (!user && user.errorCode == 0) { // добавил восклицательный знак
                showMessage(localizedText(`Пользователь с адресом электронной почты ${user.result.email} уже зарегистрирован в системе`, `Пользователь с адресом электронной почты ${user.result.email} уже зарегистрирован в системе`, `${user.result.email} электрондық пошта мекенжайы бар пайдаланушы жүйеде тіркелген`, `Пользователь с email (${user.result.email}) уже зарегистрирован в системе`), 'error');
            } else {
                AS.SERVICES.showWaitWindow();
                if (password.text === passwordConfrim.text) {
                    // Собираем данные в один объект
                    let data = {
                        login: email.text,
                        password: password.text,
                        //email: email.text,
                        firstname: fname.text,
                        lastname: lname.text,
                        pointersCode: lname.text + '_' + fname.text,
                        isConfigurator: false,
                    };
                    let userID;
                    // Отправляем запрос в Synergy
                    rest.synergyPost('api/filecabinet/user/save', data, "application/x-www-form-urlencoded; charset=UTF-8", function (success) {
                        // Обрабатываем успешный ответ (Выводим сообщение);
                        if (success && success.errorCode && success.errorCode === '0') {
                            userID = success.userID;

                            getSystemRolesDictionary().then(function (data) {
                                let system_roles = getColumns(data);
                                //текущие значение кода панели
                                let currentPanelCode = $(".drop-down-form-visible").attr("drop-down-value");
                                // находим из данных справочника код департамента выбраный пользователем
                                let system_role = system_roles.filter(x => x.panelCode == currentPanelCode);
                                if (system_role && system_role[0]) {
                                    getDepartmentsList().then(function (depList) {
                                        for (let i = 0; i < depList.length; i++) {
                                            if (depList[i] && depList[i].pointersCode == system_role[0].departmentCode) {
                                                getPositionsList(depList[i].departmentID).then(function (posList) {

                                                    let documentID = $("." + registry).attr("documentID");
                                                    getAsfDataUUID(documentID).then(function (dataUUID) {
                                                        getDataByDataUUID(dataUUID).then(function (form_data) {

                                                            let entityObject = form_data.data.filter(x => x.id == system_role[0].entityCode);

                                                            console.log(system_role[0].entityCode);
                                                            console.log(entityObject);

                                                            if (entityObject && entityObject[0] && entityObject[0].value && entityObject[0].key) {
                                                                entityObject[0].value = entityObject[0].value + "," + fname.text + " " + lname.text[0] + ".";
                                                                entityObject[0].key = entityObject[0].key + ";" + userID;
                                                            } else {
                                                                entityObject[0].value = fname.text + " " + lname.text[0] + ".";
                                                                entityObject[0].key = userID;
                                                            }


                                                            mergeAuthorData({ "uuid": dataUUID, "data": entityObject }).then(function () {
                                                                let position = posList.positions.filter(x => x.pointerCode == system_role[0].positionCode);
                                                                if (!position) {
                                                                    showMessage("Проверьте справочник системных ролей, проблема с кодом должности, пользователь создан но должность не присвоена", "error");
                                                                    return;
                                                                }
                                                                let positionID = position[0].positionID;
                                                                //назначаем на должность из справочника
                                                                rest.synergyGet(`api/positions/appoint?positionID=${positionID}&userID=${userID}`, res => {
                                                                    //добавление в группу "Пользователи"
                                                                    //rest.synergyGet(`api/storage/groups/add_user?groupCode=users&userID=${userID}`, res => {
                                                                    showMessage(localizedText('Регистрация прошла успешно', '', '', ''), 'success');

                                                                    showMessage(localizedText('Открытие авторизационной страницы Synergy через 5 секунд', '', '', ''), 'info');
                                                                    fire({ type: "goto_page", pageCode: "auth", pageParams: [] }, "register");
                                                                    AS.SERVICES.hideWaitWindow();
                                                                    //setTimeout(function () {
                                                                        //window.location.href = window.location.origin + "/Synergy";
                                                                        //window.open(window.location.origin + "/Synergy");
                                                                    //}, 2000)
                                                                    //});
                                                                });
                                                            });
                                                        });
                                                    });
                                                });
                                            } else {
                                                // AS.SERVICES.hideWaitWindow();
                                                // showMessage("Код должности неверен в справочнике 'Роли системы'", "error");
                                            }
                                        }
                                    });
                                } else {
                                    AS.SERVICES.hideWaitWindow();
                                    showMessage("Ошибка нахождения системной роли пользователя", "error");
                                }
                            });
                        } else if (success && success.errorCode && success.errorCode !== '0') {
                            AS.SERVICES.hideWaitWindow();
                            showMessage(success.errorMessage, 'error');
                        } else {
                            AS.SERVICES.hideWaitWindow();
                            console.error(success);
                        }
                    }, function (err) {
                        AS.SERVICES.hideWaitWindow();
                        console.error(err);
                    });
                } else {
                    showMessage(localizedText('Пароли не совпадают', 'Пароли не совпадают', 'Пароли не совпадают', 'Пароли не совпадают'), 'error');
                }
            }
        });

    });
});



// парсим справочник
function getColumns(data) {
    let id;
    let name;
    let panelCode;
    let departmentCode;
    let positionCode;
    let entityCode;

    data.columns.forEach(x => {
        switch (x.code) {
            case 'id':
                id = x.columnID;
                break;
            case 'name':
                name = x.columnID;
                break;
            case 'panelCode':
                panelCode = x.columnID;
                break;
            case 'departmentCode':
                departmentCode = x.columnID;
                break;
            case 'positionCode':
                positionCode = x.columnID;
                break;
            case 'entityCode':
                entityCode = x.columnID;
                break;
            default:
        }
    });

    let columnsArray = [];  //массив с объектами словаря
    for (let i = 0; i < data.items.length; i++) {
        let system_roles = {};

        for (let j = 0; j < data.items[i].values.length; j++) {
            switch (data.items[i].values[j].columnID) {
                case id:
                    system_roles.id = data.items[i].values[j].value;
                    break;
                case name:
                    system_roles.name = data.items[i].values[j].value;
                    break;
                case panelCode:
                    system_roles.panelCode = data.items[i].values[j].value;
                    break;
                case departmentCode:
                    system_roles.departmentCode = data.items[i].values[j].value;
                    break;
                case positionCode:
                    system_roles.positionCode = data.items[i].values[j].value;
                    break;
                case entityCode:
                    system_roles.entityCode = data.items[i].values[j].value;
                    break;
            }
        }

        columnsArray.push(system_roles);
    }

    return columnsArray;
}

// let glob_login = "1";
// let glob_pass = "1";
const getSystemRolesDictionary = async function () {
    return new Promise(async function (resolve) {
        try {
            $.ajax({
                "url": window.origin + "/Synergy/rest/api/dictionary/get_by_code?dictionaryCode=dictionary_roles",
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "Authorization": ("Basic " + btoa(glob_login + ":" + glob_pass))
                },
            }).done(function (response) {
                resolve(response);
            });
        } catch (err) {
            resolve({
                dictionary_code: null
            });
        }
    });
}
