const showFormPlayer = dataUUID => fire({type: 'show_form_data', dataId: dataUUID}, 'formPlayer_work');
const eventShow = {type: 'set_hidden', hidden: false};
const eventHide = {type: 'set_hidden', hidden: true};
const settings = Cons.getAppStore().portal_settings;

const checkWorkLabel = procIDArr => {
  fire(eventHide, 'button_sign');
  fire(eventHide, 'button_complete_work');
  fire(eventHide, 'button_familiarize');

  if(procIDArr && procIDArr[0]) {
    switch (procIDArr[0].work_state_label) {
      case "agreement": fire(eventShow, 'button_sign'); break;
      case "acquaintance": fire(eventShow, 'button_familiarize'); break;
    }
  }
}

const getUserWork = (processes, userID, typeID) => {
  let result = [];
  function search(p) {
    p.forEach(function(process) {
      if (process.responsibleUserID == userID && !process.finished && process.typeID == typeID) result.push(process);
      if (process.subProcesses.length > 0) search(process.subProcesses);
    });
  }
  search(processes);
  return result;
}

const getModalDialog = (title, body, buttonName, buttonAction) => {
  let dialog = $('<div class="uk-flex-top" uk-modal="bg-close: false; esc-close: false">');
  let md = $('<div class="uk-modal-dialog uk-margin-auto-vertical">');
  let modalBody = $('<div class="uk-modal-body" uk-overflow-auto>');
  let footer = $('<div class="uk-modal-footer uk-text-right">');
  let buttonClose = $(`<button class="uk-modal-close-default" type="button" uk-close></button>`)
  .css({'color': '#fff'})
  .hover(function() {
    $(this).css("color", "red");
  }, function() {
    $(this).css("color", "#fff");
  });

  modalBody.append(body);
  dialog.append(md);
  md.append(buttonClose)
  .append(`<div class="uk-modal-header" style="background: #0a043c; padding: 10px 30px;"><h4 style="color: #fff;">${title}</h4></div>`)
  .append(modalBody).append(footer);

  if(buttonName) {
    let actionButton = $('<button class="uk-button uk-margin-left button-active" type="button">');
    actionButton.html(buttonName).on('click', buttonAction);
    footer.append(actionButton);
  }
  return dialog;
}

const complete_work_action = body => {
  return new Promise(async resolve => {
    try {
      AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/workflow/work/set_result", result => {
        resolve(result);
      }, null, body, "application/x-www-form-urlencoded; charset=UTF-8", err => {
        console.log(err);
        resolve({errorCode: 120, erroMessage: "Error : cant complete work"});
      });
    } catch (e) {
      console.error(e);
      resolve({errorCode: 120, erroMessage: "Error : cant complete work"});
    }
  });
}

const finishWorkForm = (workID, formCode, type) => {
  try {
    let url = `rest/api/workflow/work/get_form_for_result?formCode=${formCode}&workID=${workID}`;
    AS.FORMS.ApiUtils.simpleAsyncGet(url).then(res => {
      if(res.errorCode && res.errorCode != '0') throw new Error(res.errorMessage);

      let player = AS.FORMS.createPlayer();
      player.view.setEditable(true);
      player.showFormData(null, null, res.dataUUID);

      player.model.on(AS.FORMS.EVENT_TYPE.dataLoad, () => {
        if(player.model.getModelWithId('entity_complete_user'))
        player.model.getModelWithId('entity_complete_user').setValue({
          personID: AS.OPTIONS.currentUser.userid,
          personName: AS.OPTIONS.currentUser.lastname + " " + AS.OPTIONS.currentUser.firstname
        });
        if(type) {
          if(player.model.getModelWithId('radio_work_type'))
          player.model.getModelWithId('radio_work_type').setValue([type]);
        }
      });

      let dialog = getModalDialog("Форма завершения", player.view.container, "Завершить", () => {
        Cons.showLoader();
        try {
          if(!player.model.isValid()) throw new Error('Заполните обязательные поля');
          player.saveFormData(saveResult => {
            let body = {
              workID: workID,
              completionForm: "FORM",
              type: 'work',
              file_identifier: res.file_identifier
            };
            complete_work_action(body).then(result => {
              Cons.hideLoader();
              if(result.errorCode == 0) {
                UIkit.modal(dialog).hide();
                showMessage("Работа успешно завершена", "success");
                fire(eventHide, 'button_complete_work');
                fire(eventHide, 'button_make_mark_time');
                fire(eventHide, 'button_make_mark_comeback');
                fire(eventHide, 'button_correct_station_employee');
                fire({type: 'goto_page', pageCode: 'page_workflow'}, 'button_edit');
              } else {
                showMessage(result.errorMessage, "error");
              }
            });
          });
        } catch (e) {
          Cons.hideLoader();
          showMessage(e.message, 'error');
        }
      });

      UIkit.modal(dialog).show();
      dialog.on('hidden', () => dialog.remove());
    });
  } catch (e) {
    showMessage(e.message, 'error');
    console.log(e);
  }
}

const finishWorkComment = actionID => {
  if($("#textInput_comment_work") && $("#textInput_comment_work").val().trim()) {
    Cons.showLoader();
    let body = {
      workID: actionID,
      completionForm: "COMMENT",
      comment: $("#textInput_comment_work").val()
    };
    complete_work_action(body).then(result => {
      Cons.hideLoader();
      if(result.errorCode == 0) {
        showMessage("Работа успешно завершена", "success");
        fire(eventHide, "panel_complete_work");
        fire(eventHide, 'button_complete_work');
        fire(eventHide, 'button_edit');
        fire({type: 'goto_page', pageCode: 'page_workflow'}, 'button_edit');
      } else {
        showMessage(result.errorMessage, "error");
      }
    });
  } else {
    showMessage('Ввод комментария при завершение работы : обязателен', 'info');
    return;
  }
}

const acquaintance_agree_doc = async (docID, procInstID, signal) => {
  return new Promise(async resolve => {
    Cons.showLoader();
    try {
      let finish_process = {
        procInstID: procInstID,
        addSignature: false,
        signal: signal
      };
      const finishProcess = await new Promise((resolve, reject) => {
        AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/workflow/finish_process", result => {
          if(result.errorCode && result.errorCode != '0') reject(result);
          resolve(result);
        }, null, finish_process, "application/x-www-form-urlencoded; charset=UTF-8", err => {
          console.log(err);
          reject(err);
        });
      });

      resolve(finishProcess);
    } catch (err) {
      showMessage('Error: could not acquaintance the work', 'error');
    }
    Cons.hideLoader();
  });
}


const sign_doc = async (docID, procInstID, signal) => {
  return new Promise(async resolve => {
    Cons.showLoader();
    try {
      const data = await new Promise((resolve, reject) => rest.custom('https://local.arta.pro:8389/?TYPE=INFO', {}, resolve, reject));
      if(!data) throw Error('Укажите ЭЦП ключ!');

      const {CERT: pemCer, KEY_CN: edsInfo, KEY_SERIALNUMBER} = data;

      if(!settings.user_iin) throw Error('В личной карточке не указан ИИН. Не возможно проверить ключ. Подписание не возможно.');
      if(`IIN${settings.user_iin}` !== KEY_SERIALNUMBER) throw Error('Выбранный сертификат ЭЦП не соответствует учетной записи, под которой Вы авторизовались в системе. Подписание не возможно.');

      const urlencoded = new URLSearchParams();
      const headers = new Headers();

      urlencoded.append('uuid', AS.OPTIONS.currentUser.userid);
      urlencoded.append('pemCer', pemCer);
      urlencoded.append('edsInfo', edsInfo);
      headers.append('Authorization', `Basic ${btoa(AS.OPTIONS.login + ':' + AS.OPTIONS.password)}`);

      // Верификация ЭЦП
      const verificationkey = await new Promise((resolve, reject) => {
        fetch('/Synergy/rest/sign/verificationkey', {
            headers,
            method: 'POST',
            body: urlencoded
          })
          .then(response => response.text())
          .then(resolve)
          .catch(reject);
      });

      const alg = verificationkey.split('::::')[1];
      const {rawdata} = await new Promise((resolve, reject) => {
        AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/docflow/doc/document_info?documentID=${docID}`).then(resolve);
      });

      // Подписание контрольной суммы
      const sign = await new Promise((resolve, reject) => rest.custom(`https://local.arta.pro:8389/?TYPE=SIGN&DATA=${encodeURIComponent(rawdata)}&ALG=${alg}`, {}, resolve, reject));

      const {
        signedData: signData,
        dataForSign: dataForSign,
        certificate: certificate
      } = sign;
      const certID = verificationkey.split('::::')[0];

      let finish_process = {
        rawdata: dataForSign,
        signdata: signData,
        certificate: certificate,
        certID: certID,
        procInstID: procInstID,
        addSignature: true,
        signal: signal,
        comment: $("#textInput_comment_sign").val()
      };
      const finishProcess = await new Promise((resolve, reject) => {
        AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/workflow/finish_process", result => {
          if(result.errorCode && result.errorCode != '0') reject(result);
          resolve(result);
        }, null, finish_process, "application/x-www-form-urlencoded; charset=UTF-8", err => {
          console.log(err);
          reject(err);
        });
      });
      showMessage('Документ успешно подписан с помощью ЭЦП ключа', 'success');

      resolve(finishProcess);
    } catch (err) {
      /**
       * Если не удалось получить ЭЦП или произошла ошибка
       * То кидаем Клиенту сообщение чтобы Клиент запустил Synergy Agent и выбрал ЭЦП ключ.
       */
      showMessage(err.name === 'Error' ? err.message : `Не найдены данные ЭЦП. Пожалуйста проверьте выбрано ли ЭЦП. Инструкция по установке Synergy Agent и его настройка тут <a target="_blank" rel="noopener noreferrer" href="${window.location.origin}/synergy-static/sa_manual.pdf">Инструкция.</a>`, 'error');
    }
    Cons.hideLoader();
  });
}

const got_agree_click = (docID, procInstID) => {
  if($("#got_agree").length) {
    $("#got_agree").off().on('click', async () => {
      sign_doc(docID, procInstID, "got_agree").then(result => {
        showMessage("Работа успешно согласована", "info");
        fire(eventHide, "panel_sign");
        fire(eventHide, 'button_sign');
        $("#got_agree").off();
        fire({type: 'goto_page', pageCode: 'page_workflow'}, 'button_sign');
      });
    });
  } else {
    setTimeout(got_agree_click, 600, docID, procInstID);
  }
}

const acquaintance_agree_click = (docID, procInstID) => {
  if($("#complete_familiarize").length) {
    $("#complete_familiarize").off().on('click', async () => {
      acquaintance_agree_doc(docID, procInstID, "got_agree").then(result => {
        showMessage("Вы успешно ознакомились с документом", "info");
        fire(eventHide, "panel_familiarize");
        fire(eventHide, 'button_familiarize');
        $("#complete_familiarize").off();
      });
    });
  } else {
    setTimeout(got_agree_click, 600, docID, procInstID);
  }
}

const got_refuse_click = (docID, procInstID) => {
  if ($("#got_refuse").length) {
    $("#got_refuse").off().on('click', async () => {
      if($("#textInput_comment_sign") && $("#textInput_comment_sign").val().trim()) {
        sign_doc(docID, procInstID, "got_refuse").then(result => {
          showMessage("Работа не согласована", "info");
          fire(eventHide, "panel_sign");
          fire(eventHide, 'button_sign');
          $("#got_refuse").off();
          fire({type: 'goto_page', pageCode: 'page_workflow'}, 'button_sign');
        });
      } else {
        showMessage('Ввод комментария при не согласование работы : обязателен', 'info');
        return;
      }
    });
  } else {
    setTimeout(got_refuse_click, 600, docID, procInstID);
  }
}

const updateToWorkPage = async param => {
  let {actionID, dataUUID, documentID} = param;
  if (!dataUUID) {
    console.error("Error: no dataUUID of document");
    return;
  }

  AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/workflow/works_by_id?workID=${actionID}`).then(actionsData => {
    if($('#formPlayer_work').length) showFormPlayer(dataUUID);

    if (actionsData && actionsData.length) {
      let procIDArr = [];

      for(let i = 0; i < actionsData.length; i++) {
        if(actionsData[i] && actionsData[i].procInstID)
        procIDArr.push({procInstID: actionsData[i].procInstID, work_state_label: actionsData[i].work_state_label});
      }

      checkWorkLabel(procIDArr);

      if(procIDArr[0] && procIDArr[0].work_state_label === "progress") {
        rest.synergyGet(`api/workflow/get_execution_process?documentID=${documentID}`, res => {
          let processes = getUserWork(res, AS.OPTIONS.currentUser.userid, 'ASSIGNMENT_ITEM');
          let processesAgr = getUserWork(res, AS.OPTIONS.currentUser.userid, 'AGREEMENT_ITEM');
          if(processes.length > 0) {
            switch (processes[0].code) {
              case 'make_mark_shipper':
                fire(eventShow, 'button_make_mark_time');
                $("#button_make_mark_time").off().on('click', async () => finishWorkForm(actionID, 'completion_form_consignee', '1'));
                break;
              case 'make_mark_consignee':
                fire(eventShow, 'button_make_mark_time');
                $("#button_make_mark_time").off().on('click', async () => finishWorkForm(actionID, 'completion_form_consignee', '2'));
                break;
              case 'make_mark_car_arrival_time':
                fire(eventShow, 'button_make_mark_comeback');
                $("#button_make_mark_comeback").off().on('click', async () => finishWorkForm(actionID, 'completion_form_waybill', '1'));
                break;
              case 'correct_station_employee':
                fire(eventShow, 'button_correct_station_employee');
                $("#button_correct_station_employee").off().on('click', async () => finishWorkForm(actionID, 'form_complete_bus_station_dispatcher', '1'));
                break;
              case 'modify':
                fire(eventShow, 'button_complete_work');
                fire(eventShow, 'button_edit');
                $("#button_complete_work").off().on('click', e => {
                  fire(eventShow, 'panel_complete_work')
                  setTimeout(() => {
                    $("#complete_work").off().on('click', async () => finishWorkComment(actionID));
                  }, 500);
                });
                break;
              case 'control_medical_check_1':
                fire(eventShow, 'button_complete_work');
                $("#button_complete_work").off().on('click', e => {
                  fire(eventShow, 'panel_complete_work')
                  setTimeout(() => {
                    $("#complete_work").off().on('click', async () => finishWorkComment(actionID));
                  }, 500);
                });
                break;
              case 'control_medical_check_2':
                fire(eventShow, 'button_complete_work');
                $("#button_complete_work").off().on('click', e => {
                  fire(eventShow, 'panel_complete_work')
                  setTimeout(() => {
                    $("#complete_work").off().on('click', async () => finishWorkComment(actionID));
                  }, 500);
                });
                break;
              case 'confirm':
                fire(eventShow, 'button_complete_work');
                $("#button_complete_work").off().on('click', e => {
                  fire(eventShow, 'panel_complete_work')
                  setTimeout(() => {
                    $("#complete_work").off().on('click', async () => finishWorkComment(actionID));
                  }, 500);
                });
                break;
            }
          }
          if(processesAgr.length > 0) {
            switch (processesAgr[0].code) {
              case 'agree_carrier':
                let buttonSignCar = $("#button_sign");
                if(buttonSignCar.length) {
                  fire(eventShow, 'button_sign');
                  buttonSignCar.off().on('click', e => {
                    e.preventDefault();
                    e.target.blur();
                    fire(eventShow, 'panel_sign');
                  });
                }
                break;
            }
          }
        }, err => {
          console.log(err);
        });
      }

      $("#button_sign").off().on('click', async () => {
        if(procIDArr[0] && procIDArr[0].procInstID) {
          setTimeout(got_refuse_click, 600, documentID, procIDArr[0].procInstID);
          setTimeout(got_agree_click, 600, documentID, procIDArr[0].procInstID);
        } else {
          showMessage("Error: no procInstID", "error");
        }
      });

      $("#button_familiarize").off().on('click', async () => {
        if(procIDArr[0] && procIDArr[0].procInstID) {
          setTimeout(acquaintance_agree_click, 600, documentID, procIDArr[0].procInstID);
        } else {
          showMessage("Error: no procInstID", "error");
        }
      });
    }
  });
}


pageHandler('page_workflow', () => {
  let store = Cons.getAppStore();

  if(!store.workAddListener) {
    Cons.setAppStore({workAddListener: true});
    addListener('worklist_item_click', 'workList', e => {
      AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/workflow/work/${e.actionID}/document`).then(doc => {
        if(doc && doc.documentID) {
          AS.FORMS.ApiUtils.getAsfDataUUID(doc.documentID).then(dataUUID => {
            updateToWorkPage({
              actionID: e.actionID,
              dataUUID: dataUUID,
              documentID: doc.documentID
            });
          });
        } else {
          console.log("Error: no documentID of work");
        }

      });
    });
  }
});
