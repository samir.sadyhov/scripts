/* Настроки
  pageCode - Код страницы где надо добавить слушатель на отправку формы
  playerID -  код компонента проигрывателя форм
  buttonID - код кнопки создания записи реестра
  saveButton - код кнопки сохранить
*/
const options = [];
options.push({pageCode: 'page_create_ttn_record', playerID: 'formPlayer_ttn', buttonID: 'button_create', saveButton: 'button-save-ttn'});
options.push({pageCode: 'page_create_pl_cargo_record', playerID: 'formPlayer-1', buttonID: 'button_create', saveButton: 'button-save-pl-cargo'});
options.push({pageCode: 'page_create_pl_passenger_record', playerID: 'formPlayer-1', buttonID: 'button_create', saveButton: 'button-save-pl-passenger'});
options.push({pageCode: 'show_pl_cargo', playerID: 'formPlayer-1', buttonID: 'button-send', saveButton: 'button-save'});
options.push({pageCode: 'page_info_ttn', playerID: 'formPlayer-1', buttonID: 'button-send', saveButton: 'button-save'});
options.push({pageCode: 'page_info_pl_passenger', playerID: 'formPlayer-1', buttonID: 'button-send', saveButton: 'button-save'});

// Проверка формы и отправка по маршруту форм на странице личный кабинет

options.push({pageCode: 'page_cabinet', playerID: 'formPlayer_profile', buttonID: 'button_create_test', saveButton: 'button_save_form'});
options.push({pageCode: 'page_cabinet', playerID: 'formPlayer_medical_worker', buttonID: 'button_save_medical_worker', saveButton: 'button_save_form'});
options.push({pageCode: 'page_cabinet', playerID: 'formPlayer_drivers', buttonID: 'button_save_drivers', saveButton: 'button_save_form'});
options.push({pageCode: 'page_cabinet', playerID: 'formPlayer_cargo', buttonID: 'button_save_cargo', saveButton: 'button_save_form'});
options.push({pageCode: 'page_cabinet', playerID: 'formPlayer_bus', buttonID: 'button_save_bus', saveButton: 'button_save_form'});
options.push({pageCode: 'page_cabinet', playerID: 'formPlayer_taxi', buttonID: 'button_save_taxi', saveButton: 'button_save_form'});
options.push({pageCode: 'page_cabinet', playerID: 'formPlayer_trailer', buttonID: 'button_save_trailer', saveButton: 'button_save_form'});
options.push({pageCode: 'page_cabinet', playerID: 'formPlayer_organization', buttonID: 'button_save_organization', saveButton: 'button_save_form'});
options.push({pageCode: 'page_cabinet', playerID: 'formPlayer_route_template', buttonID: 'button_save_route_template', saveButton: 'button_save_form'});
options.push({pageCode: 'page_cabinet', playerID: 'formPlayer_organization_cards', buttonID: null, saveButton: 'button_save_organization_cards'});

// Сохранение данных по форме

options.push({pageCode: 'bus', playerID: 'formPlayer_view', buttonID: 'button_hide', saveButton: 'button_save'});
options.push({pageCode: 'drivers', playerID: 'formPlayer_view', buttonID: 'button_hide', saveButton: 'button_save'});
options.push({pageCode: 'truck', playerID: 'formPlayer_view', buttonID: 'button_hide', saveButton: 'button_save'});
//options.push({pageCode: 'show_medical_workers', playerID: 'formPlayer_view', buttonID: 'button_hide', saveButton: 'button_save'});
options.push({pageCode: 'taxi', playerID: 'formPlayer_view', buttonID: 'button_hide', saveButton: 'button_save'});
options.push({pageCode: 'trailer', playerID: 'formPlayer_view', buttonID: 'button_hide', saveButton: 'button_save'});
options.push({pageCode: 'organizations', playerID: 'formPlayer_view', buttonID: 'button_hide', saveButton: 'button_save'});

//Дефолтное сообщение
const defaultMsgError = {
  ru: 'Просим заполнить все обязательные поля выделенные красным цветом и повторить попытку',
  kk: 'Қызыл түспен белгіленген барлық қажетті өрістерді толтырып, қайталап көріңіз',
  en: ''
}

/*
Для получения кастомного сообщения например с компонента выпадающий список,
в конфигураторе, на форме в скрипте нужного компонента, необходимо добавить к моделе метод проверки getSpecialErrors
например:
model.getSpecialErrors = function () {
  if (model.getValue()[0] == "0") {
    let customMsg = {
      ru: "Выберите регион",
      kk: "Аймақты таңдаңыз",
      en: "Выберите регион"
    };
    return {
      id: model.asfProperty.id,
      errorCode: window.location.href.indexOf('Synergy') !== -1 ? AS.FORMS.INPUT_ERROR_TYPE.emptyValue : customMsg
    };
  }
};
*/
const getErrorMessage = errors => {
  let msg = defaultMsgError[AS.OPTIONS.locale];
  errors.forEach(x => {
    if(!AS.FORMS.INPUT_ERROR_TYPE.hasOwnProperty(x.errorCode)) msg = x.errorCode[AS.OPTIONS.locale];
  });
  return msg;
}

//сохранение данных по форме
const saveFormData = (formPlayer, activate, success, error) => {
  let data = {
    data: '"data" : ' + JSON.stringify(formPlayer.model.getAsfData().data),
    form: formPlayer.model.formId,
    uuid: formPlayer.model.asfDataId
  };
  AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/asforms/form/multipartdata", res => {
    if(activate) {
      AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/activate_doc?dataUUID=${data.uuid}`).then(res => success(res));
    } else {
      success(res);
    }
  }, "text", data, "application/x-www-form-urlencoded; charset=UTF-8", err => {
    error(err.responseJSON.errorMessage);
  });
}

//создание записи реестра
const createFormData = (formPlayer, activate, success, error) => {
  let asfData = formPlayer.model.getAsfData();
  let registryCode = formPlayer.registryCode;
  let body = {"registryCode": registryCode, "data": asfData.data};
  if(activate) body.sendToActivation = activate;
  AS.FORMS.ApiUtils.simpleAsyncPost('rest/api/registry/create_doc_rcc', res => {
    if(res.errorCode != '0') error(res.errorMessage);
    success(res);
  }, null, JSON.stringify(body), "application/json; charset=utf-8", err => {
    error(err.responseJSON.errorMessage);
  });
}

const modifyDoc = formPlayer => {
  let asfData = formPlayer.model.getAsfData();
  return new Promise(async resolve => {
    try {
      rest.synergyGet(`api/registry/modify_doc?dataUUID=${asfData.uuid}`, res => resolve(res));
    } catch (e) {
      resolve(null);
    }
  });
}

//отправка формы
const sendApp = (formPlayer, activate, success, error) => {
  if(!formPlayer.dataId) {
    createFormData(formPlayer, activate, success, error);
  } else {
    saveFormData(formPlayer, activate, success, error);
  }
}

const searchUser = param => {
  return new Promise(async resolve => {
    try {
      rest.synergyGet(`api/filecabinet/user/checkExistence?${jQuery.param(param)}`, res => resolve(res.result == "true"));
    } catch (e) {
      console.log(e);
      resolve(null);
    }
  });
}

const checkAndSendApp = (formPlayer, opt) => {
  let iin = formPlayer.model.getModelWithId('textbox_iin').getValue();
  let email = formPlayer.model.getModelWithId('textbox_email').getValue();
  searchUser({code: `IIN${iin}`}).then(checkIIN => {
    searchUser({login: email}).then(checkEmail => {
      if(checkIIN) {
        showMessage(localizedText(`Пользователь с ИИН ${iin} уже зарегистрирован в системе`, `Пользователь с ИИН ${iin} уже зарегистрирован в системе`, `Пользователь с ИИН ${iin} уже зарегистрирован в системе`, `Пользователь с ИИН ${iin} уже зарегистрирован в системе`), 'error');
        return;
      }

      if(checkEmail) {
        showMessage(localizedText(`В системе уже имеется учетная запись с таким email.`, `В системе уже имеется учетная запись с таким email.`, `В системе уже имеется учетная запись с таким email.`, `В системе уже имеется учетная запись с таким email.`), 'error');
        return;
      }

      Cons.showLoader();
      try {
        sendApp(formPlayer, true, result => {
          showMessage(localizedText('Запись успешно создана', 'Запись успешно создана', 'Жазба сәтті құрылды', 'Запись успешно создана'), 'success');
          fire({type: 'set_hidden', hidden: true}, opt.buttonID);
          fire({type: 'set_form_editable', editable: false}, formPlayer.code);
          Cons.hideLoader();
        }, (err) => {
          Cons.hideLoader();
          showMessage(err, 'error');
        });
      } catch (e) {
        Cons.hideLoader();
        console.log(e.message, e);
        showMessage(localizedText('Произошла ошибка при создании записи.', 'Произошла ошибка при создании записи.', 'Жазбаны құру кезінде қате пайда болды.', 'Произошла ошибка при создании записи.'), 'error');
      }

    });
  });
}

options.forEach(opt => {
  pageHandler(opt.pageCode, () => {
    let formPlayer = null;

    addListener("loaded_form_data", opt.playerID, e => formPlayer = getCompByCode(opt.playerID));

    if(!opt.buttonID && !opt.saveButton) return;

    if (Cons.getAppStore()[`${opt.pageCode}_${opt.buttonID || opt.saveButton}_listener`]) return;

    if(opt.buttonID) {
      addListener("button_click", opt.buttonID, e => {
        if(!formPlayer.model.isValid()) {
          showMessage(getErrorMessage(formPlayer.model.getErrors()), 'error');
        } else {
          if(['button_save_drivers', 'button_save_medical_worker'].indexOf(opt.buttonID) != -1) {
            checkAndSendApp(formPlayer, opt);
          } else {
            Cons.showLoader();
            try {
              sendApp(formPlayer, true, result => {
                showMessage(localizedText('Запись успешно создана', 'Запись успешно создана', 'Жазба сәтті құрылды', 'Запись успешно создана'), 'success');
                fire({type: 'set_hidden', hidden: true}, opt.buttonID);
                fire({type: 'set_form_editable', editable: false}, formPlayer.code);
                Cons.hideLoader();
              }, (err) => {
                Cons.hideLoader();
                showMessage(err, 'error');
              });
            } catch (e) {
              Cons.hideLoader();
              console.log(e.message, e);
              showMessage(localizedText('Произошла ошибка при создании записи.', 'Произошла ошибка при создании записи.', 'Жазбаны құру кезінде қате пайда болды.', 'Произошла ошибка при создании записи.'), 'error');
            }
          }

        }
      });
    }

    if(opt.saveButton) {
      addListener("button_click", opt.saveButton, e => {
        Cons.showLoader();
        try {
          sendApp(formPlayer, false, result => {
            if(opt.saveButton == 'button_save_organization_cards') {
              modifyDoc(formPlayer).then(res => {
                showMessage(localizedText('Запись успешно сохранена', 'Запись успешно сохранена', 'Жазба сәтті сақталды', 'Запись успешно сохранена'), 'success');
                Cons.hideLoader();
              });
            } else {
              showMessage(localizedText('Запись успешно сохранена', 'Запись успешно сохранена', 'Жазба сәтті сақталды', 'Запись успешно сохранена'), 'success');
              Cons.hideLoader();
            }

          }, (err) => {
            Cons.hideLoader();
            showMessage(err, 'error');
          });
        } catch (e) {
          Cons.hideLoader();
          console.log(e.message);
          showMessage(localizedText('Произошла ошибка при сохранении записи.', 'Произошла ошибка при сохранении записи.', 'Жазбаны сақтау кезінде қате пайда болды.', 'Произошла ошибка при сохранении записи.'), 'error');
        }
      });
    }

    let s = {};
    s[`${opt.pageCode}_${opt.buttonID || opt.saveButton}_listener`] = true;

    Cons.setAppStore(s);
  });
});
