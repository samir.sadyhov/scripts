const eventShow = {type: 'set_hidden', hidden: false};
const eventHide = {type: 'set_hidden', hidden: true};

const fileDownload = (id, filename) => {
  let requestUrl = AS.FORMS.ApiUtils.getFullUrl(`rest/api/storage/file/get?identifier=${id}`);
  let xhr = new XMLHttpRequest();
  xhr.open('GET', requestUrl, true);
  AS.FORMS.ApiUtils.addAuthHeader(xhr);
  xhr.responseType = 'arraybuffer';
  xhr.onload = function () {
    if(this.status === 200) {
      let type = xhr.getResponseHeader('Content-Type');
      let blob = new Blob([this.response], {type: type});
      if(typeof window.navigator.msSaveBlob !== 'undefined') {
        window.navigator.msSaveBlob(blob, filename);
      } else {
        let URL = window.URL || window.webkitURL;
        let downloadUrl = URL.createObjectURL(blob);
        if(filename) {
          let a = document.createElement("a");
          if(typeof a.download === 'undefined') {
            window.location = downloadUrl;
          } else {
            a.href = downloadUrl;
            a.download = filename;
            document.body.appendChild(a);
            a.click();
          }
        } else {
          window.location = downloadUrl;
        }
        setTimeout(() => {URL.revokeObjectURL(downloadUrl)}, 100);
      }
    }
  };
  xhr.send();
}

const toggleAcc = el => {
  el = el[0];
  el.classList.toggle("active");
  let panel = el.nextElementSibling;
  if (panel.style.maxHeight) panel.style.maxHeight = null;
  else panel.style.maxHeight = "initial";
}

const createAccordion = (accordionData, accordionContainer) => {
  accordionContainer.empty();

  accordionData.forEach(item => {
    let block = $('<div class="accordion-block">')
    let button = $(`<button class="accordion-button">${item.title}</button>`);
    let body = $('<div class="accordion-body">');

    accordionContainer.append(block);
    block.append(button).append(body);
    button.on('click', () => toggleAcc(button));

    if(item.hasOwnProperty('fileID')) {
      let fileLink = $(`<a href="#" src="${item.fileID}">${item.body}</a>`);

      fileLink.on('click', e => {
        e.preventDefault();
        e.target.blur();
        fileDownload(item.fileID, item.body);
      });

      body.html(fileLink);
    } else {
      body.html(item.body);
    }

    body.append('<div class="accordion-footer"></div>');
  });
}

const initFAQ = () => {
  let registryApi =
      AS.OPTIONS.locale == 'kk'
        ? 'api/registry/data_ext?registryCode=faq_registry&fields=answer_kz&fields=quest_kz'
        : 'api/registry/data_ext?registryCode=faq_registry&fields=answer&fields=Quest';
  rest.synergyGet(registryApi, res => {
    let accData = res.result.map(x => {
      return {
        title: AS.OPTIONS.locale == 'kk' ? x.fieldValue.quest_kz : x.fieldValue.Quest,
        body: AS.OPTIONS.locale == 'kk' ? x.fieldValue.answer_kz : x.fieldValue.answer
      }
    });
    createAccordion(accData, $('#tab-faq'));
  });
}

const initInstruction = () => {
  rest.synergyGet(`api/registry/data_ext?registryCode=reg_manual&fields=file_manual&fields=name_category`, res => {
    let accData = res.result.map(x => {
      return {
        title: x.fieldValue.name_category,
        body: x.fieldValue.file_manual,
        fileID: x.fieldKey.file_manual
      }
    });
    createAccordion(accData, $('#tab-instructions'));
  });
}

pageHandler('help_page', () => {
  initFAQ();
  initInstruction();

  $('#button-faq').off().on('click', e => {
    e.preventDefault();
    e.target.blur();
    $('#button-faq').addClass('active');
    $('#button-instructions').removeClass('active');
    fire(eventShow, 'tab-faq');
    fire(eventHide, 'tab-instructions');
  });

  $('#button-instructions').off().on('click', e => {
    e.preventDefault();
    e.target.blur();
    $('#button-instructions').addClass('active');
    $('#button-faq').removeClass('active');
    fire(eventShow, 'tab-instructions');
    fire(eventHide, 'tab-faq');
  });

});

pageHandler('faq_no_auth', () => {
  initFAQ();
  initInstruction();

  $('#button-faq').off().on('click', e => {
    e.preventDefault();
    e.target.blur();
    $('#button-faq').addClass('active');
    $('#button-instructions').removeClass('active');
    fire(eventShow, 'tab-faq');
    fire(eventHide, 'tab-instructions');
  });

  $('#button-instructions').off().on('click', e => {
    e.preventDefault();
    e.target.blur();
    $('#button-instructions').addClass('active');
    $('#button-faq').removeClass('active');
    fire(eventShow, 'tab-instructions');
    fire(eventHide, 'tab-faq');
  });

});
