const eventShow = {type: 'set_hidden', hidden: false};
const eventHide = {type: 'set_hidden', hidden: true};

const emptyValidator = (input) => {
  if (input && input.text && input.text !== '') {
    fire({type: 'input_highlight', error: false}, input.code);
    return true;
  } else {
    fire({type: 'input_highlight', error: true}, input.code);
    return false;
  }
}

const sendNotification = async (body) => {
  const res = await fetch(`${window.origin}/Synergy/rest/api/notifications/send`, {
    method: 'POST',
    headers: {
      'Authorization': "Basic " + btoa(Cons.creds.login + ":" + Cons.creds.password),
      'Content-Type': 'application/json;charset=utf-8'
    },
    body: JSON.stringify(body)
  });
  return await res.json();
}

const loadAsfData = uuid => {
  return new Promise(async resolve => {
    try {
      rest.synergyGet(`api/asforms/data/${uuid}`, res => resolve(res), err => resolve(null));
    } catch (e) {
      resolve(null);
    }
  });
}

const searchInRegistry = (url) => {
  return new Promise(async resolve => {
    try {
      rest.synergyGet(url, res => res.count > 0 ? resolve(res.data[0].dataUUID) : resolve(null), err => resolve(null));
    } catch (e) {
      console.log(e);
      resolve(null);
    }
  });
}

const getUserCardUUID = () => {
  let settings = Cons.getAppStore().portal_settings;
  let url = 'api/registry/data_ext?loadData=false';
  if(settings && settings.listbox_arm_inspector) {
    url += '&registryCode=registry_inspector';
    url += `&field=entity_userid&condition=CONTAINS&key=${AS.OPTIONS.currentUser.userid}`;
  } else {
    url += '&registryCode=registry_user_profile';
    url += `&field=entity_user_uuid&condition=CONTAINS&key=${AS.OPTIONS.currentUser.userid}`;
  }
  return searchInRegistry(url);
}

const getDepartmentCardUUID = () => {
  let url = 'api/registry/data_ext?registryCode=registry_organizations&loadData=false';
  url += `&field=entity_department&condition=CONTAINS&key=${AS.OPTIONS.currentUser.positions[0].departmentID}`;
  return searchInRegistry(url);
}

const hideShowButtons = depCardID => {
  let settings = Cons.getAppStore().portal_settings;
  if(!settings) return;
  if(!depCardID) return;

  loadAsfData(depCardID).then(asfData => {
    let boss = asfData.data.find(x => x.id == 'entity_boss');
    let responsible = asfData.data.find(x => x.id == 'entity_responsible');

    if((boss && boss.key == AS.OPTIONS.currentUser.userid) || (responsible && responsible.key.indexOf(AS.OPTIONS.currentUser.userid) !== -1)) {

      if($('#button_drivers').length) fire(eventShow, 'button_drivers');

      if(settings.check_refers_role.indexOf('3') !== -1) {
        if($('#button_show_medical_worker').length) fire(eventShow, 'button_show_medical_worker');
        if($('#button_route_templates').length) fire(eventShow, 'button_route_templates');

        if(settings.check_transportation_type.indexOf('1') !== -1) {
          if($('#button_trailer').length) fire(eventShow, 'button_trailer');
          if($('#button_cargo').length) fire(eventShow, 'button_cargo');
        }

        if(settings.check_transportation_type.indexOf('2') !== -1) {
          if(settings.check_bus_or_taxi.indexOf('1') !== -1 && $('#button_bus').length) fire(eventShow, 'button_bus');
          if(settings.check_bus_or_taxi.indexOf('2') !== -1 && $('#button_taxi').length) fire(eventShow, 'button_taxi');
        }
      }

    }
  });
}

const changePassword = () => {
  let currentPassInput = getCompByCode('textInput_current_pass');
  let newPassInput = getCompByCode('textInput_new_pass');
  let confirmPassInput = getCompByCode('textInput_confirm_pass');

  if(!emptyValidator(currentPassInput)) {
    showMessage('Не заполнено поле Текущий пароль', 'error');
    return;
  }
  if(!emptyValidator(newPassInput)) {
    showMessage('Не заполнено поле Новый пароль', 'error');
    return;
  }if(!emptyValidator(confirmPassInput)) {
    showMessage('Не заполнено поле Подтверждение пароля', 'error');
    return;
  }

  $.when($.ajax({
    type: "GET",
    url: `${AS.OPTIONS.coreUrl}rest/api/person/auth`,
    beforeSend: function(xhr){
      xhr.setRequestHeader("Authorization", "Basic " + btoa(unescape(encodeURIComponent(AS.OPTIONS.login + ":" + currentPassInput.text))));
    },
    statusCode: {
      401: function () {
        fire({type: 'input_highlight', error: true}, currentPassInput.code);
        showMessage('Неверный текущий пароль', 'error');
        return;
      },
      500: function () {
        fire({type: 'input_highlight', error: true}, currentPassInput.code);
        showMessage('Не введен текущий пароль', 'error');
        return;
      }
    }
  })).then(user => {
    let data = {
      actionCode: 'CHANGE_PASSWORD',
      locale: AS.OPTIONS.locale,
      login: AS.OPTIONS.login,
      password: newPassInput.text,
      passwordConfirm: confirmPassInput.text
    };
    AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/filecabinet/user/changeCredentials", res => {
      if(res && res.errorCode == 0) {
        sendNotification({
          header: 'Изменение пароля',
          message: 'Ваш пароль был изменен.<br><br>Новый пароль для входа <b>' + newPassInput.text + '</b>',
          emails: [AS.OPTIONS.login]
        }).then(result => {
          showMessage('Параметры авторизации успешно изменены', 'success');
          fire({type: "goto_page", pageCode: "auth", pageParams: []}, "button_save");
        });
      } else {
        showMessage('Произошла ошибка при изменении параметров авторизации', 'error');
        console.log('ERROR', res);
      }
    }, null, data, "application/x-www-form-urlencoded; charset=UTF-8", err => {
      showMessage(err.responseJSON.errorMessage, 'error');
      fire({type: 'input_highlight', error: true}, newPassInput.code);
      console.log('err', err.responseJSON);
    });
  });
}

pageHandler('page_cabinet', () => {
  let regex = /[\s\^а-яА-Я]/g;
  ['#textInput_current_pass', '#textInput_new_pass', '#textInput_confirm_pass'].forEach(id => {
    let input = $(id);
    input.off().on('input', () => {
      input.val(input.val().replace(regex,''));
      input.parent().removeClass('cons-color-red');
    });
  });

  getUserCardUUID().then(cardUUID => {
    if(cardUUID) fire({type: 'show_form_data', dataId: cardUUID}, 'formPlayer_profile');
  });

  getDepartmentCardUUID().then(cardUUID => {
    if(cardUUID) fire({type: 'show_form_data', dataId: cardUUID}, 'formPlayer_organization_cards');
    if(AS.OPTIONS.currentUser.positions[0].type == 1) fire(eventShow, 'button_organization');
    hideShowButtons(cardUUID);
  });

  if(Cons.getAppStore().button_save_pass_listener) return;
  Cons.setAppStore({button_save_pass_listener: true});
  addListener('button_click', 'button_save', (e) => {
    changePassword();
  });

});
