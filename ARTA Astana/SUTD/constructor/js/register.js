const eventShow = {type: 'set_hidden', hidden: false};
const eventHide = {type: 'set_hidden', hidden: true};
const emailRegex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;

//если поле не заполнено - выделить его как невалидное
const emptyValidator = input => {
  if(input && input.text && input.text !== '') {
    fire({type: 'input_highlight', error: false}, input.code);
    return true;
  } else {
    fire({type: 'input_highlight', error: true}, input.code);
    return false;
  }
}

//валидация введенного email
const emailValidator = input => {
  if(emptyValidator(input)) {
    if (input && emailRegex.test(input.text)) {
      fire({type: 'input_highlight', error: false}, input.code);
      return true;
    } else {
      fire({type: 'input_highlight', error: true}, input.code);
      return false;
    }
  }
  return false;
}

//валидация бин/иин
const binValidator = input => {
  if(emptyValidator(input)) {
    if(input.text.length != 12) return false;
    if(input && /^\d+$/.test(input.text)) {
      fire({type: 'input_highlight', error: false}, input.code);
      return true;
    } else {
      fire({type: 'input_highlight', error: true}, input.code);
      return false;
    }
  }
  return false;
}

const transliterate = text => {
  return text
    .replace(/\u0401/g, 'YO').replace(/\u0419/g, 'I').replace(/\u0426/g, 'TS')
    .replace(/\u0423/g, 'U').replace(/\u041A/g, 'K').replace(/\u0415/g, 'E')
    .replace(/\u041D/g, 'N').replace(/\u0413/g, 'G').replace(/\u0428/g, 'SH')
    .replace(/\u0429/g, 'SCH').replace(/\u0417/g, 'Z').replace(/\u0425/g, 'H')
    .replace(/\u042A/g, '').replace(/\u0451/g, 'yo').replace(/\u0439/g, 'i')
    .replace(/\u0446/g, 'ts').replace(/\u0443/g, 'u').replace(/\u043A/g, 'k')
    .replace(/\u0435/g, 'e').replace(/\u043D/g, 'n').replace(/\u0433/g, 'g')
    .replace(/\u0448/g, 'sh').replace(/\u0449/g, 'sch').replace(/\u0437/g, 'z')
    .replace(/\u0445/g, 'h').replace(/\u044A/g, "'").replace(/\u0424/g, 'F')
    .replace(/\u042B/g, 'I').replace(/\u0412/g, 'V').replace(/\u0410/g, 'a')
    .replace(/\u041F/g, 'P').replace(/\u0420/g, 'R').replace(/\u041E/g, 'O')
    .replace(/\u041B/g, 'L').replace(/\u0414/g, 'D').replace(/\u0416/g, 'ZH')
    .replace(/\u042D/g, 'E').replace(/\u0444/g, 'f').replace(/\u044B/g, 'i')
    .replace(/\u0432/g, 'v').replace(/\u0430/g, 'a').replace(/\u043F/g, 'p')
    .replace(/\u0440/g, 'r').replace(/\u043E/g, 'o').replace(/\u043B/g, 'l')
    .replace(/\u0434/g, 'd').replace(/\u0436/g, 'zh').replace(/\u044D/g, 'e')
    .replace(/\u042F/g, 'Ya').replace(/\u0427/g, 'CH').replace(/\u0421/g, 'S')
    .replace(/\u041C/g, 'M').replace(/\u0418/g, 'I').replace(/\u0422/g, 'T')
    .replace(/\u042C/g, "'").replace(/\u0411/g, 'B').replace(/\u042E/g, 'YU')
    .replace(/\u044F/g, 'ya').replace(/\u0447/g, 'ch').replace(/\u0441/g, 's')
    .replace(/\u043C/g, 'm').replace(/\u0438/g, 'i').replace(/\u0442/g, 't')
    .replace(/\u044C/g, "'").replace(/\u0431/g, 'b').replace(/\u044E/g, 'yu');
}

const addIndicator = () => {
  $('#step-indicator').append('<span class="step active"></span>');
  for(let i = 1; i < $('.tab').length; i++)
  $('#step-indicator').append('<span class="step"></span>');
}

const switchIndicator = num => {
  num = num - 1;
  if(num == 0 ) {
    $('.step').removeClass('active').removeClass('finish');
    $('.step:eq(0)').addClass('active');
  } else {
    $('.step').removeClass('active').removeClass('finish');
    for(let i = 0; i < num; i++)
    $(`.step:eq(${i})`).removeClass('active').addClass('finish');
    $(`.step:eq(${num})`).addClass('active');
  }
}

const searchOrg = () => {
  return new Promise(async resolve => {
    try {
      const inputBin = getCompByCode('textInput_bin_iin');
      if(!binValidator(inputBin)) {
        showMessage('Введите корректный БИН/ИИН организации', 'error');
      } else {
        let url = 'api/registry/data_ext?registryCode=registry_organizations';
        url += `&field=textbox_bin_iin&condition=TEXT_EQUALS&value=${inputBin.text}`;
        url += '&fields=check_roles&fields=textbox_email&fields=textbox_name_organization';
        rest.synergyGet(url, res => {
          if(res.recordsCount > 0) {
            resolve(res.result[0]);
          } else {
            resolve(null);
          }
        });
      }
    } catch (e) {
      console.log(e);
      resolve(null);
    }
  });
}
const searchOrgInStatGov = () => {
  return new Promise(async resolve => {
    try {
      const inputBin = getCompByCode('textInput_bin_iin');
      if(!binValidator(inputBin)) {
        showMessage('Введите корректный БИН/ИИН организации', 'error');
      } else {
        fetch(`../statgov/rest/api/juridical?bin=${inputBin.text}&lang=ru`)
        .then(res => res.json())
        .then(res => {
          if(res.success) {
            resolve(res.obj);
          } else {
            resolve(null);
          }
        })
        .catch(err => {
          console.log(err);
          resolve(null);
        });
      }
    } catch (e) {
      console.log(e);
      resolve(null);
    }
  });
}

const showAlert = (el, msg) => {
  let container = $('<div class="uk-alert-danger" uk-alert>');
  container.append('<a class="uk-alert-close" uk-close></a>');
  container.append(`<p>${msg}</p>`);
  el.empty().append(container);
}

const showHideTabs = params => {
  let {action, num, activeTabID} = params;
  if(action == 'next') {
    if(num === 4) return;
    fire(eventHide, activeTabID);
    fire(eventShow, `panel-step-${num+1}`);
    if(num === 1) fire(eventShow, 'button-prev');
    if(num === 3) $('#button-next').addClass('button-active').text('Зарегистрироваться');
    switchIndicator(num + 1);
  } else {
    fire(eventHide, activeTabID);
    if(num-1 != 0) fire(eventShow, `panel-step-${num-1}`);
    if(num === 2) fire(eventHide, 'button-prev');
    if(num === 4) $('#button-next').removeClass('button-active').text('Далее');
    switchIndicator(num - 1);
  }
}

const switchTabs = action => {
  const activeTabID = $('.tab:not(.uk-hidden)').attr('data-cy');
  if(!activeTabID) return;
  const num = Number(activeTabID.slice(11));
  if(isNaN(num) || num < 1) return;
  showHideTabs({action, num, activeTabID});
}

pageHandler('registration', () => {

  let inputBinIin = $('#textInput_bin_iin');
  inputBinIin.off().on('input', e => {
    inputBinIin.val(inputBinIin.val().replace(/[^\d]/g,'').slice(0,12));
  });

  let mailInput = $('#email_boss');
  mailInput.off().on('input', e => {
    mailInput.val(mailInput.val().replace(/[^a-zA-Z\d.@\-_]/g,''));
  });

  let searchOrgButton = $('#button_find_org');
  searchOrgButton.off().on('click', e => {
    e.preventDefault();
    e.target.blur();
    let regOrgData = null;
    let statGovOrgData = null;

    searchOrg().then(res => {
      if(res) {
        regOrgData = res;
        $('#alert-step1').empty();
        if(regOrgData.fieldValue.hasOwnProperty('textbox_email')) mailInput.val(regOrgData.fieldValue.textbox_email);
      } else {
        regOrgData = null;
        mailInput.val('');
        searchOrgInStatGov().then(statRes => {
          if(statRes) {
            statGovOrgData = statRes;
            showAlert($('#alert-step1'), 'Организация с таким БИН не была найдена в системе. Будет создана новая организация в системе на основании данных из stat.gov.kz');
          } else {
            statGovOrgData = null;
            showAlert($('#alert-step1'), 'Организация с таким БИН не была найдена в системе и в системе stat.gov.kz. Просьба проверить корректность введенного БИН');
          }
          console.log('regOrgData', regOrgData);
          console.log('statGovOrgData', statGovOrgData);
        });
      }
    });

  });

  $('#button-next').off().on('click', e => {
    e.preventDefault();
    e.target.blur();
    switchTabs('next');
  });

  $('#button-prev').off().on('click', e => {
    e.preventDefault();
    e.target.blur();
    switchTabs('prev');
  });

  // if(!emailValidator(getCompByCode('email_boss'))) {
  //   showMessage('Введен не корректный email адрес', 'error');
  //   return;
  // }

  addIndicator();

});
