var result = true;
var message = 'ok';

try {
  let currentFormData = API.getFormData(dataUUID);
  let roles = UTILS.getValue(currentFormData, 'check_roles');
  let transportType = UTILS.getValue(currentFormData, 'check_transportation_type');
  let busOrTaxi = UTILS.getValue(currentFormData, 'check_bus_or_taxi');
  let department = UTILS.getValue(currentFormData, 'entity_department');

  if(!department || !department.hasOwnProperty('key')) throw new Error('Не заполнено поле Организация пользователя');

  let urlSearch = 'rest/api/registry/data_ext?registryCode=registry_user_profile';
  urlSearch += '&loadData=false&field=entity_organization&condition=CONTAINS&key=' + department.key;

  let searchResult = API.httpGetMethod(urlSearch);

  if(searchResult.count == 0) throw new Error('Не найдено карточек пользователей для организации ' + department.value);

  let changeUserCardCount = 0;

  searchResult.data.forEach(function(item){
    let userFormData = API.getFormData(item.dataUUID);
    let userRoles = UTILS.getValue(userFormData, 'check_roles');
    let userTransportType = UTILS.getValue(userFormData, 'check_transportation_type');
    let userBusOrTaxi = UTILS.getValue(userFormData, 'check_bus_or_taxi');
    if(JSON.stringify(roles) != JSON.stringify(userRoles) ||
       JSON.stringify(transportType) != JSON.stringify(userTransportType) ||
       JSON.stringify(busOrTaxi) != JSON.stringify(userBusOrTaxi)) {
      UTILS.setValue(userFormData, 'check_roles', roles);
      UTILS.setValue(userFormData, 'check_transportation_type', transportType);
      UTILS.setValue(userFormData, 'check_bus_or_taxi', busOrTaxi);
      API.mergeFormData(userFormData);
      changeUserCardCount++;
    }
  });

  message = 'Обновлено карточек пользователей: ' + changeUserCardCount;

} catch (err) {
  log.error(err.message);
  message = err.message;
}
