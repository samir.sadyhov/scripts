var result = true;
var message = "ok";

function getActionIds(processes, code, formCode) {
  let result = [];
  function search(p) {
    p.forEach(function(x) {
      if (x.completionFormCode == formCode && x.code == code && x.finished) result.push(x);
      if (x.subProcesses.length > 0) search(x.subProcesses);
    });
  }
  search(processes);

  return result.sort(function(a, b) {
    return UTILS.parseDateTime(b.finished) - UTILS.parseDateTime(a.finished);
  }).map(function(item) {
    return {actionID: item.actionID, finished: item.finished};
  });
};

try {
  let currentFormData = API.getFormData(dataUUID);
  let processes = API.getProcesses(documentID);
  let actions = getActionIds(processes, 'make_mark_consignee', 'completion_form_consignee');

  let completionForms = actions.map(function(item) {
    return API.getWorkCompletionData(item.actionID).result.dataUUID;
  }).filter(function(dataUUID){ return dataUUID != null});

  if(completionForms.length == 0) throw new Error('Не найдена форма завершения');

  completionForms.forEach(function(completionFormDataUUID){
    let completionFormData = API.getFormData(completionFormDataUUID);

    let entity_complete_user = UTILS.getValue(completionFormData, 'entity_complete_user');
    if(!entity_complete_user || !entity_complete_user.hasOwnProperty('key')) throw new Error('Не заполнен автор формы завершения');

    let table_cargo_info = UTILS.getValue(currentFormData, 'table_cargo_info');
    if(!table_cargo_info || !table_cargo_info.hasOwnProperty('data')) throw new Error('Не найдено данных в таблице table_cargo_info');

    let users = [];
    table_cargo_info.data.forEach(function(x) {
      if(x.id.slice(0, x.id.indexOf('-b')) === 'entity_consignee_signer' && x.hasOwnProperty('key')) users.push(x);
    });

    users = users.filter(function(x) {
      if(x.key === entity_complete_user.key) return x;
    });

    let time_arrival = UTILS.getValue(completionFormData, 'time_arrival');
    let time_departures = UTILS.getValue(completionFormData, 'time_departures');

    users.forEach(function(x){
      let index = x.id.slice(x.id.indexOf('-b'));
      UTILS.setValue(table_cargo_info, 'time_arrival_consignee' + index, time_arrival);
      UTILS.setValue(table_cargo_info, 'time_departures_consignee' + index, time_departures);
    });
  });

  API.mergeFormData(currentFormData);

} catch (err) {
  log.error(err);
  message = err.message;
}
