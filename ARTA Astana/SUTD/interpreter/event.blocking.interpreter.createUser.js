function fetchUserFullName(lastname, firstname, patronymic) {
  lastname = lastname ? lastname : null;
  firstname = firstname ? ' ' + firstname.charAt(0) + '.' : null;
  patronymic = patronymic ? ' ' + patronymic.charAt(0) + '.' : null;
  return lastname + firstname + patronymic;
}

function createNewUser(params) {
  let client = API.getHttpClient();
  let post = new org.apache.commons.httpclient.methods.PostMethod("http://127.0.0.1:8080/Synergy/rest/api/filecabinet/user/save");
  for(let key in params) post.addParameter(key, params[key]);
  post.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=utf-8");
  let resp = client.executeMethod(post);
  resp = JSON.parse(post.getResponseBodyAsString());
  post.releaseConnection();
  return resp;
}

function searchPosition(departmentID){
  let res = API.httpGetMethod("rest/api/departments/content?departmentID=" + departmentID);
  res = res.positions.filter(function(x){
    if(x.pointerCode.substring(0,8) == 'employee') return x;
  });
  return res.length ? res[0].positionID : null;
}

function generatePassword() {
  let charSet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-+_';
  return Array.apply(null, Array(8)).map(function() {
    return charSet.charAt(Math.random() * charSet.length);
  }).join('');
}

var result = true;
var message = 'ok';

try {
  let currentFormData = API.getFormData(dataUUID);

  let textbox_lastname = UTILS.getValue(currentFormData, "textbox_lastname");
  let textbox_firstname = UTILS.getValue(currentFormData, "textbox_firstname");
  let textbox_patronymic = UTILS.getValue(currentFormData, "textbox_patronymic");
  let textbox_email = UTILS.getValue(currentFormData, "textbox_email");
  let textbox_iin = UTILS.getValue(currentFormData, "textbox_iin");
  let organization = UTILS.getValue(currentFormData, "entity_organization");

  if(!textbox_firstname || !textbox_firstname.hasOwnProperty('value')) throw new Error('Не заполнено имя пользователя');
  if(!textbox_lastname || !textbox_lastname.hasOwnProperty('value')) throw new Error('Не заполнена фамилия пользователя');
  if(!textbox_email || !textbox_email.hasOwnProperty('value')) throw new Error('Не заполнен email пользователя');
  if(!textbox_iin || !textbox_iin.hasOwnProperty('value')) throw new Error('Не заполнен ИИН');
  if(!organization || !organization.hasOwnProperty('key')) throw new Error('Не выбрана организация');

  if(!textbox_patronymic || !textbox_patronymic.hasOwnProperty('value')) {
    textbox_patronymic = '';
  } else {
    textbox_patronymic = textbox_patronymic.value;
  }

  let positionID = searchPosition(organization.key);
  if(!positionID) throw new Error('Не найдена должность для назначения');

  let newPassword = generatePassword();

  let user = createNewUser({
    lastname: textbox_lastname.value,
    firstname: textbox_firstname.value,
    patronymic: textbox_patronymic,
    login: textbox_email.value,
    password: newPassword,
    pointersCode: 'IIN' + textbox_iin.value
  });

  if(user.errorCode == '13') throw new Error(user.errorMessage);
  message = user.errorMessage;

  let fullName = fetchUserFullName(textbox_lastname.value, textbox_firstname.value, textbox_patronymic.value);
  UTILS.setValue(currentFormData, "entity_userid", {value: fullName, key: user.userID});
  API.mergeFormData(currentFormData);

  //Добавление юзера в группу
  let apiRes = API.httpGetMethod('rest/api/storage/groups/add_user?groupCode=user&userID=' + user.userID);
  message += '\n' + apiRes.message;

  //Назначение на должность
  apiRes = API.httpGetMethod('rest/api/positions/appoint?positionID=' + positionID + '&userID=' + user.userID);
  message += '\n' + apiRes.errorMessage;

  // отправка письма
  apiRes = API.sendNotification({
    header: 'Уведомление о создании новой учетной записи в системе.',
    message: 'Для вас была создана учетная запись для входа в систему.<br>Ваш логин <b>' + textbox_email.value + '</b> <br>Ваш пароль <b>' + newPassword + '</b><br>После входа в систему необходимо сменить пароль.<br>Ссылка для перехода в систему <a href="http://esutd.gov.kz/sutd/">esutd.gov.kz</a>',
    emails: [textbox_email.value]
  });

  if(apiRes.errorCode != 0) {
    message += '\nПроизошла ошибка отправки уведомления';
  } else {
    message = '\nУведомление успешно отправлено';
  }

} catch (err) {
  log.error(err, err.message);
  result = false;
  message = err.message;
}
