var result = true;
var message = 'ok';

try {
  let currentFormData = API.getFormData(dataUUID);

  let user = UTILS.getValue(currentFormData, "entity_user");
  let position = UTILS.getValue(currentFormData, "entity-position");

  if(!user || !user.hasOwnProperty('key')) throw new Error('Не выбран пользователь для назначения');
  if(!position || !position.hasOwnProperty('key')) throw new Error('Не выбрана должность для назначения');

  let res = API.httpGetMethod('rest/api/positions/appoint?userID=' + user.key + "&positionID=" + position.key);

  if(res.errorCode != '0') throw new Error(res.errorMessage);
  message = res.errorMessage;

} catch (err) {
  result = false;
  message = err.message;
}
