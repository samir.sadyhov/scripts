var result = true;
var message = 'ok';

try {

  let currentFormData = API.getFormData(dataUUID);
  let documentIDs = UTILS.getValue(currentFormData, "reglink_select_ttn");

  if(!documentIDs || !documentIDs.hasOwnProperty('key')) throw new Error("поле 'Выбор ТТН*' не заполнено");

  let matching = [];
  matching.push({from: 'reglink_select_car', to: 'reglink_select_car'});
  matching.push({from: 'textbox_car_model', to: 'textbox_car_model'});
  matching.push({from: 'textbox_car_number', to: 'textbox_car_number'});
  matching.push({from: 'radio_trailer', to: 'radio_trailer'});
  matching.push({from: 'table_trailer', to: 'table_trailer'});

  documentIDs = documentIDs.key.split(';');
  documentIDs.forEach(function(docID) {
    let ttnFormData = API.getFormData(API.getAsfDataId(docID));
    matching.forEach(function(id) {
      let fromData = UTILS.getValue(currentFormData, id.from);
      if(fromData) UTILS.setValue(ttnFormData, id.to, fromData);
    });
    API.mergeFormData(ttnFormData);
  });

} catch (err) {
  log.error(err, err.message);
  message = err.message;
}
