var result = true;
var message = "ok";

function getActionIds(processes, code, formCode) {
  let result = [];
  function search(p) {
    p.forEach(function(x) {
      if (x.completionFormCode == formCode && x.code == code && x.finished) result.push(x);
      if (x.subProcesses.length > 0) search(x.subProcesses);
    });
  }
  search(processes);

  return result.sort(function(a, b) {
    return UTILS.parseDateTime(b.finished) - UTILS.parseDateTime(a.finished);
  }).map(function(item) {
    return {actionID: item.actionID, finished: item.finished};
  });
};

try {
  let processes = API.getProcesses(documentID);
  let actions = getActionIds(processes, 'make_mark_author', 'completion_form_waybill');

  let completionFormDataUUID = actions.map(function(item) {
    return API.getWorkCompletionData(item.actionID).result.dataUUID;
  }).filter(function(dataUUID){ return dataUUID != null})[0];

  if(!completionFormDataUUID) throw new Error('Не найдена форма завершения');

  let completionFormData = API.getFormData(completionFormDataUUID);
  let currentFormData = API.getFormData(dataUUID);

  let matching = [];
  matching.push({from: 'time_arrival', to: 'date-arrival-final-destination'});

  matching.forEach(function(id) {
    let fromData = UTILS.getValue(completionFormData, id.from);
    if(fromData && fromData.value) UTILS.setValue(currentFormData, id.to, fromData);
  });

  API.mergeFormData(currentFormData);

} catch (err) {
  log.error(err);
  message = err.message;
}
