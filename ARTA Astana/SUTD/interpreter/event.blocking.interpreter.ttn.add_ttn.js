var result = true;
var message = 'ok';

try {
  let currentFormData = API.getFormData(dataUUID);
  let waybill = UTILS.getValue(currentFormData, "custom_waybill");

  if(!waybill || !waybill.hasOwnProperty('key')) throw new Error('Не выбран пперевозчик');

  let waybillFormData = API.getFormData(API.getAsfDataId(waybill.key));
  let select_ttn = UTILS.getValue(waybillFormData, "reglink_select_ttn");

  if(!select_ttn) {
    select_ttn = {
      id: "reglink_select_ttn",
      type: "reglink",
      key: documentID,
      valueID: documentID,
      value: API.getDocMeaningContent(documentID)
    }
  } else if (!select_ttn.hasOwnProperty('key')) {
    select_ttn.key = documentID;
    select_ttn.valueID = documentID;
    select_ttn.value = API.getDocMeaningContent(documentID);
  } else {
    if(select_ttn.key.indexOf(documentID) == -1) {
      select_ttn.key += ';' + documentID;
      select_ttn.valueID += ';' + documentID;
      select_ttn.value += ';' + API.getDocMeaningContent(documentID);
    }
  }

  UTILS.setValue(waybillFormData, "check_exist_cargo", {
    type: "check",
    values: [null],
    keys: [null]
  });

  API.mergeFormData(waybillFormData);

} catch (err) {
  result = false;
  message = err.message;
}
