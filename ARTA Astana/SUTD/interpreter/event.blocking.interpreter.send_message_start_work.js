var result = true;
var message = "ok";
let emailTheme = "Уведомление о необходимости пройти медицинское освидетельствование перед началом смены.";
let emailBody = "Прошу пройти в медицинский пункт для прохождения медицинского освидетельствования перед началом рабочей смены.";

try {
  let currentFormData = API.getFormData(dataUUID);
  let table_drivers = UTILS.getValue(currentFormData, 'table_drivers');

  if(!table_drivers.hasOwnProperty('data')) throw new Error('Не заполнена таблица Сведения о водителях');

  let emails = [];
  let emptyEmail = [];

  table_drivers.data.forEach(function(item) {
    if (item.id.slice(0, item.id.indexOf('-b')) === 'entity_userid') {
      if (item.hasOwnProperty('key')) {
        let index = item.id.slice(item.id.indexOf('-b') + 2);
        let email = table_drivers.data.filter(function(x){if(x.id == "textbox_email-b" + index) return x;})[0];
        if(email.hasOwnProperty('value')) {
          emails.push(email.value);
        } else {
          emptyEmail.push(item.key);
        }
      }
    }
  });

  if(emails.length == 0) throw new Error('Не найдены email-ы для отправки сообщений');

  // отправка письма
  let sendResult = API.sendNotification({
    header: emailTheme,
    message: emailBody,
    emails: emails
  });

  if(sendResult.errorCode != 0) throw new Error('Произошла ошибка отправки уведомлений');
  message = 'Уведомления успешно отправлены';

  if(emptyEmail.length > 0) {
    emptyEmail.forEach(function(userid){
      message += "\nНе найден email получателя для пользователя userID: " + userid;
    });
  }

} catch (err) {
  log.error(err.message);
  message = err.message;
}
