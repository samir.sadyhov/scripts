let orgModel = model.playerModel.getModelWithId('entity_organization');
let organization = orgModel.getValue();

if(organization && organization.length > 0) {
  organization = organization[0];

  const urlSearch = `rest/api/registry/data_ext?registryCode=registry_organizations&loadData=false&field=entity_organization&condition=CONTAINS&key=${organization.departmentId}`;

  AS.FORMS.ApiUtils.simpleAsyncGet(urlSearch).then(res => {
    if(res.count > 0) return AS.FORMS.ApiUtils.loadAsfData(res.data[0].dataUUID);
  }).then(orgAsfData => {
    const orgData = {
      check_roles: orgAsfData.data.find(x => x.id == 'check_roles'),
      check_transportation_type: orgAsfData.data.find(x => x.id == 'check_transportation_type'),
      check_bus_or_taxi: orgAsfData.data.find(x => x.id == 'check_bus_or_taxi')
    }
    for(let key in orgData) {
      let tmpModel = model.playerModel.getModelWithId(key);
      if(tmpModel && orgData[key].hasOwnProperty('values')) tmpModel.setValue(orgData[key].values);
    }
  });

}
