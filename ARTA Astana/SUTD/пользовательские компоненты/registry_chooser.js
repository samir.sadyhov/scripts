// Инструкция по использованию
// 1. добавить данный пользовательский компонент на форму
// 2. в поле код скрипта компонента прописать код реестра и дополнительные параметры

// model.registryCode = 'registryCode'; //код реестра, обязательно
// model.filterCode = 'filterCode'; //код фильтра, если не передать будут отображены все записи реестра
// model.multiple = true; //Позволять мультивыбор
// model.openDialog = true; //Открывать в диалоге

// Сопоставление полей
// model.matching = [];
// model.matching.push({from: 'cmp_from', to: 'cmp_to'});

//Для сопоставления полей динамических таблиц
// model.matching.push({
//   from: 'cmp_from',
//   to: 'cmp_to',
//   tables: [                            - перечисление компонентов таблиц
//     {from: 'cmp_from', to: 'cmp_to'},
//     {from: 'cmp_from2', to: 'cmp_to2'}
//   ]
// });

// Формат данных
// {id : "id",
// type : "custom",
// value : "значащее содержимое документов через запятую",
// keys : [массив выбранных идентификаторов],
// key : "первый идентификатор если он есть",
// valueID : "хэш fnv32 отсортированных по возрастанию идентификаторов скленных через пробел"}
//

AS.FORMS.hash = function(str, asString, seed) {
  var i, l, hval = (seed === undefined) ? 0x811c9dc5 : seed;
  for (i = 0, l = str.length; i < l; i++) {
    hval ^= str.charCodeAt(i);
    hval += (hval << 1) + (hval << 4) + (hval << 7) + (hval << 8) + (hval << 24);
  }
  if (asString) {
    // Convert to 8 digit hex string
    return ("0000000" + (hval >>> 0).toString(16)).substr(-8);
  }
  return hval >>> 0;
};


// ИНициалиация модели
model.onOnce = function(event, name, handler) {
  if (!model.handlers) model.handlers = {};
  if (model.handlers[event + "_" + name]) model.off(event, model.handlers[event + "_" + name]);
  model.handlers[event + "_" + name] = handler;
  model.on(event, handler);
}

if (!model.selectedDocuments) {
  model.selectedDocuments = [];
  model.textValue = "";
}

model.updateTextView = function() {
  if (!model.registry) return;

  model.selectedDocuments = [];
  model.textValue = "";

  if (!model.getSelectedIds() || model.getSelectedIds().length == 0) {
    model.textValue = "";
    model.asfDataId = null;
    model.trigger(AS.FORMS.EVENT_TYPE.dataLoad, [model]);
    return;
  }

  var asfDataPromises = [];
  model.getSelectedIds().forEach(function(documentId) {
    asfDataPromises.push(AS.FORMS.ApiUtils.simpleAsyncGet('rest/api/formPlayer/getAsfDataUUID?documentID=' + documentId, null, "text"));
    model.selectedDocuments.push({
      documentId: documentId
    });
  });

  jQuery.when.apply(jQuery, asfDataPromises)
    .then(function() {
      var docMeaningPromises = [];

      for (var i = 0; i < arguments.length; i++) {
        model.selectedDocuments[i].asfDataId = arguments[i];
        docMeaningPromises.push(AS.FORMS.ApiUtils.simpleAsyncGet('rest/api/formPlayer/getDocMeaningContent?registryID=' + model.registry.registryID + "&asfDataUUID=" + arguments[i], null, "text"));
      }

      jQuery.when.apply(jQuery, docMeaningPromises).then(function() {
        var sign = "";
        for (var i = 0; i < arguments.length; i++) {
          model.selectedDocuments[i].meaning = arguments[i];
          if (model.selectedDocuments[i].meaning == '') {
            model.selectedDocuments[i].meaning = i18n.tr('Документ');
          }
          model.textValue = model.textValue + sign + model.selectedDocuments[i].meaning;
          sign = ", ";
        }
        model.trigger(AS.FORMS.EVENT_TYPE.dataLoad, [model]);
      });

    });
};

model.getTextValue = function() {
  return model.textValue;
};

model.setAsfData = function(asfData) {
  if (asfData.keys || asfData.key) {
    if (!asfData.keys) {
      asfData.keys = [asfData.key];
    }
    model.setValue(asfData.keys);
  } else {
    model.setValue([]);
  }
};

model.getSelectedIds = function() {
  return model.value;
};

model.getValue = function() {
  if (model.multiple) {
    return model.value;
  } else {
    if (model.value.length == 0) {
      return null;
    } else {
      return model.value[0];
    }
  }
};

model.getAsfData = function(blockNumber) {
  var valueID = "";
  if (model.value) {
    var array = model.value.slice().sort();
    valueID = AS.FORMS.hash(array.join(" "));
  }

  var asfData = AS.FORMS.ASFDataUtils.getBaseAsfData(model.asfProperty, blockNumber, model.textValue, null);
  asfData.keys = model.value;
  if (asfData.keys && asfData.keys.length > 0) {
    asfData.key = asfData.keys[0];
  }
  asfData.valueID = valueID;
  return asfData;
};

const showDialog = (title, content) => {
  let modal = $('<div>').append(content);
  $("body").append(modal);
  modal.dialog({
    modal: true,
    width: 800,
    height: 600,
    title: title,
    buttons: [{
      text: "Закрыть",
      click: function() {
        $(this).dialog("close");
      }
    }],
    close: function() {
      $(this).remove();
    }
  });
}

const opendDocument = documentID => {
  if (!model.openDialog) {
    AS.SERVICES.showDocument(documentID);
  } else {
    AS.FORMS.ApiUtils.getAsfDataUUID(documentID).then(dataUUID => {
      AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/formPlayer/getDocMeaningContent?&documentId=${documentID}`, null, "text")
        .then(meaning => {
          let player = AS.FORMS.createPlayer();
          player.view.setEditable(false);
          player.showFormData(null, null, dataUUID);
          showDialog(meaning, player.view.container);
        });
    });
  }
}

const clearFields = () => {
  model.matching.forEach(id => {
    let tmpModel = model.playerModel.getModelWithId(id.to, model.asfProperty.ownerTableId, model.asfProperty.tableBlockIndex);
    if (tmpModel) tmpModel.setValue(null);
  });
}

const getTableBlockIndex = (data, cmp) => {
  let res = 0;
  data = data.data ? data.data : data;
  data.forEach(function(item) {
    if (item.id.slice(0, item.id.indexOf('-b')) === cmp) res++;
  });
  return res === 0 ? 1 : ++res;
}

const setField = (from, modelTo) => {
  if (from) {
    switch (from.type) {
      case 'textbox':
      case 'textarea':
        if (modelTo && from.hasOwnProperty('value') && from.value) {
          modelTo.setValue(from.value);
        } else {
          if (modelTo) modelTo.setValue(null);
        }
        break;
      case 'check':
        if (modelTo && from.hasOwnProperty('values')) {
          modelTo.setValue(from.values);
        } else {
          if (modelTo) modelTo.setValue(null);
        }
        break;
      case 'entity':
        if (modelTo && from.hasOwnProperty('key') && from.key && from.key !== 'null') {
          switch (modelTo.asfProperty.config.entity) {
            case "users":
              modelTo.setValue({
                personID: from.key,
                personName: from.value
              });
              break;
            case "departments":
              modelTo.setValue({
                departmentId: from.key,
                departmentName: from.value
              });
              break;
            case "positions":
              modelTo.setValue({
                elementID: from.key,
                elementName: from.value
              });
              break;
          }
        } else {
          if (modelTo) modelTo.setValue(null);
        }
        break;
      case "appendable_table": break;
      default:
        if (modelTo && from.hasOwnProperty('key') && from.key) {
          modelTo.setValue(from.key);
        } else {
          if (modelTo) modelTo.setValue(null);
        }
    }
  } else {
    if (modelTo) modelTo.setValue(null);
  }
}

const setTableFields = (tableFrom, tableModel, m) => {
  let tbx = getTableBlockIndex(tableFrom, m[0].from);

  //Чистим таблицу перед заполнением
  for(let numb in tableModel.getBlockNumbers()) tableModel.removeRow(numb);

  setTimeout(() => {
    for(let i = 1; i < tbx; i++) {
      let tableBlock = tableModel.createRow();
      m.forEach(item => {
        let tmpFrom = tableFrom.data.find(x => x.id === `${item.from}-b${i}`);
        let tmpTo = model.playerModel.getModelWithId(item.to, tableModel.asfProperty.id, tableBlock.tableBlockIndex);
        setField(tmpFrom, tmpTo);
      });
    }
  }, 500);
}

const matchingInit = docID => {
  if (!editable) return;
  if (!docID) {
    clearFields();
    return;
  }

  AS.SERVICES.showWaitWindow();
  try {
    AS.FORMS.ApiUtils.getAsfDataUUID(docID)
      .then(uuid => AS.FORMS.ApiUtils.loadAsfData(uuid))
      .then(asfData => {
        model.matching.forEach(item => {
          let from = asfData.data.find(x => x.id === item.from);
          let modelTo = model.playerModel.getModelWithId(item.to, model.asfProperty.ownerTableId, model.asfProperty.tableBlockIndex);
          if(item.hasOwnProperty('tables')) {
            setTableFields(from, modelTo, item.tables);
          } else {
            setField(from, modelTo);
          }
        });
        AS.SERVICES.hideWaitWindow();
      });
  } catch (e) {
    AS.SERVICES.hideWaitWindow();
  }
}

model.onOnce(AS.FORMS.EVENT_TYPE.valueChange, "updateTextView", function() {
  model.updateTextView();
  if (!model.multiple && model.matching) matchingInit(model.getValue());
});

// ИНициализация отображения
view.init = function() {
  let tagArea = new AS.FORMS.TagArea({width: "calc(100% - 30px)"}, 0, model.multiple, false, false);
  let chooserButton = $('<button>', {class: 'asf-browseButton'});

  let fileNameLabel = jQuery('<div>', {
    style: "font-family: arial, tahoma, sans-serif; font-size: 12px; align-self: center; word-wrap: break-word; padding-right: 2px; padding-left: 1px;"
  });

  tagArea.on("tagDelete", function() {
    let documentIds = [];
    tagArea.getValues().forEach(function(tagItem) {
      documentIds.push(tagItem.documentId);
    });
    model.setValueFromInput(documentIds);
  });

  if (editable) {
    let div = jQuery('<div>');
    div.append(tagArea.getWidget());
    div.append(chooserButton);
    view.container.prepend(div);
  } else {
    view.container.prepend(fileNameLabel);
  }

  chooserButton.click(function() {
    let selectedIds = [];
    if (model.getSelectedIds()) selectedIds = model.getSelectedIds();
    let chooser = new RegistryChooser(model.registry, model.multiple, model.filterIds, selectedIds, selected => {
      let documentIds = [];
      selected.forEach(item => documentIds.push(item.documentId));
      model.setValueFromInput(documentIds);
    });
    chooser.show();
  });

  AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/info?code=${model.registryCode}`).then(reg => {
    model.registry = reg;
    model.registry.registryID = reg.registryID;
    model.updateTextView();
  });

  view.updateValueFromModel = function() {
    fileNameLabel.html("");
    tagArea.setValues([]);

    if (model.getSelectedIds() && model.getSelectedIds().length > 0) {
      model.selectedDocuments.forEach(function(selectedDocument) {
        selectedDocument.tagName = selectedDocument.meaning;

        let docComponent = jQuery("<div>", {
          style: "font-family: arial, tahoma, sans-serif; font-size: 12px; " +
            "align-self: center; word-wrap: break-word; padding-right: 2px;" +
            "padding-left: 1px; cursor : pointer; text-decoration: underline;" +
            "line-height:20px"
        }).html(selectedDocument.meaning);
        docComponent.click(function() {
          opendDocument(selectedDocument.documentId);
        });

        fileNameLabel.append(docComponent);
      });

      tagArea.setValues(model.selectedDocuments);
    }
  };

  view.updateValueFromModel();

  model.on("dataLoad", function() {
    view.updateValueFromModel();
  });
};

if (model.registryCode) {
  view.init();
} else {
  model.on("loadComponentAdditionalInfo", function() {
    view.init();
  });
}

// ТАБЛИЦА
var MultiTableEvent = {
  dataClicked: "dataClicked",
  dataSelected: "dataSelected",
  dataDeselected: "dataDeselected",
  columnSortClicked: "columnSortClicked",
  inited: "inited",
  colResized: "colResized",
  colResizeStarted: "colResizeStarted",
  dataLoaded: "dataLoaded"
};

var MultiTable = function(multiChoose, initialSelect) {
  var instance = this;
  instance.bus = jQuery({});

  this.trigger = function(eventType, args) {
    instance.bus.trigger(eventType, args);
  };
  this.on = function(eventType, handler) {
    instance.bus.on(eventType, handler);
  };
  this.off = function(eventType, handler) {
    instance.bus.off(eventType, handler);
  };

  var headerRowHeight = 32;
  var rowHeight = 26;
  var width = 500;
  var headerRow = null;
  var rows = [];
  var selectedData = [];
  var idField = "id";
  var columns = [];

  var container = jQuery("<div>", {
    style: "font-family: DroidSans, arial, serif;font-size: 14px;color: #606060; display: inline-block;text-align: left;",
    tabIndex: "0"
  });


  initialSelect.forEach(item => selectedData.push(item));

  this.init = function(options, newColumns) {
    instance.clear();
    headerRow = new MultiTableHeaderRow(instance, -1);
    container.append(headerRow.getWidget());

    columns = newColumns;

    headerRowHeight = options["headerRowHeight"] || headerRowHeight;
    rowHeight = options["rowHeight"] || rowHeight;
    width = options["width"] || width;
    idField = options["idField"] || idField;

    instance.trigger(MultiTableEvent.inited, []);
  };


  instance.on(MultiTableEvent.dataClicked, function(evt, data) {

    if (multiChoose) {
      if (!instance.isSelected(data)) {
        selectedData.push(data);
        instance.trigger(MultiTableEvent.dataSelected, data);
      } else {
        let index = -1;
        selectedData.forEach(function(candidate, candidateIndex) {
          if (candidate[idField] == data[idField]) index = candidateIndex;
        });
        selectedData.splice(index, 1);
        instance.trigger(MultiTableEvent.dataDeselected, data);
      }
    } else {
      while (selectedData.length > 0) {
        let d = selectedData[0];
        let i = selectedData.indexOf(d);
        selectedData.splice(i, 1);
        instance.trigger(MultiTableEvent.dataDeselected, d);
      }

      selectedData.push(data);
      instance.trigger(MultiTableEvent.dataSelected, data);
    }
    container.focus();
  });



  this.isSelected = function(data) {
    let selected = false;
    selectedData.forEach(function(item) {
      if (item[idField] == data[idField]) {
        selected = true;
      }
    });
    return selected;
  };

  this.getSelectedData = function() {
    return selectedData;
  };

  this.getColumns = function() {
    return columns;
  };

  this.getHeaderRowHeight = function() {
    return headerRowHeight;
  };

  this.getRowHeight = function() {
    return rowHeight;
  };

  this.clear = function() {
    container.empty();
  };

  this.setData = function(data, append) {
    if (!append) {
      rows.forEach(function(row) {
        row.getWidget().detach();
        row.getWidget().empty();
      });
      rows = [];
    }

    let rowsCount = rows.length;

    data.forEach(function(data, dataIndex) {
      let row = new MultiTableRow(instance, data, dataIndex + rowsCount);
      rows.push(row);
      container.append(row.getWidget());
    });

    instance.trigger(MultiTableEvent.dataLoaded, []);
  };

  this.setSelectedData = function(newSelectedData) {
    while (selectedData.length > 0) {
      let d = selectedData[0];
      let i = selectedData.indexOf(d);
      selectedData.splice(i, 1);
      instance.trigger(MultiTableEvent.dataDeselected, d);
    }

    if (!newSelectedData) return;

    newSelectedData.forEach(function(newSelectData) {
      selectedData.push(newSelectData);
      instance.trigger(MultiTableEvent.dataSelected, newSelectData);
    });
  };

  this.getWidget = function() {
    return container;
  };

};

var MultiTableHeaderRow = function(table, rowIndex) {
  var headerRow = jQuery("<div>", {
    style: "border-bottom: 1px solid #d6d6d6;background: #fbfbfb;white-space: nowrap;"
  });
  headerRow.css("height", table.getHeaderRowHeight() + "px");
  headerRow.css("line-height", table.getHeaderRowHeight() + "px");
  var cells = [];

  table.on(MultiTableEvent.inited, function() {
    cells = [];
    headerRow.empty();
    var columns = table.getColumns();
    columns.forEach(function(column, columnIndex) {
      var headerCell = new MultiTableHeaderCell(table, column, columnIndex);
      cells.push(headerCell);
      headerRow.append(headerCell.getWidget());
    });
  });


  this.getWidget = function() {
    return headerRow;
  };


  let startX = -1;
  let movingColumn = null;
  let movingColumnIndex = -1;
  table.on(MultiTableEvent.colResizeStarted, function(evt, screenX, columnIndex) {
    startX = screenX;
    movingColumn = table.getColumns()[columnIndex];
    movingColumnIndex = columnIndex;
  });

  headerRow.mousemove(function(evt) {
    if (!movingColumn) return;

    let targetWidth = movingColumn.width - (startX - evt.screenX);
    if (movingColumn.minWidth) {
      targetWidth = Math.max(movingColumn.minWidth, targetWidth);
    }

    if (startX === -1) return;

    movingColumn.width = targetWidth;
    table.trigger(MultiTableEvent.colResized, [movingColumn, movingColumnIndex]);

    startX = evt.screenX;
  });

  headerRow.mouseup(function() {
    startX = -1;
    movingColumn = null;
    movingColumnIndex = -1;
  });

  headerRow.mouseout(function() {
    startX = -1;
    movingColumn = null;
    movingColumnIndex = -1;
  });


};

var MultiTableHeaderCell = function(table, columnOption, columnIndex) {
  var header = jQuery("<div>", {
    style: "padding-left: 12px;display: inline-block;border-right: 1px solid #d6d6d6;overflow: hidden;white-space: nowrap;text-overflow: ellipsis;height: 100%;box-sizing: border-box;",
    title: columnOption.name
  });
  header.css("width", columnOption.width + "px");

  var text = jQuery("<div>", {
    style: "display: inline-block;width: calc(100% - 12px);overflow: hidden;white-space: nowrap;text-overflow: ellipsis;height: 100%;"
  });

  text.html(columnOption.name);

  var movingAnchor = jQuery("<div>", {
    style: "cursor: ew-resize;width: 12px;height: 100%;display: inline-block;"
  });
  var sortImage = jQuery("<div>", {
    style: "margin-left: 9px;margin-bottom: 9px;"
  });
  sortImage.hide();
  var sortAsc = null;

  header.append(text);
  header.append(sortImage);
  header.append(movingAnchor);


  this.getWidget = function() {
    return header;
  };

  this.cancelEventDispatch = function(element, event) {
    element.on(event, function(evt) {
      evt.cancelBubble = true;
      evt.stopped = true;
      evt.stopPropagation();
      evt.preventDefault();
    });
  };

  this.notMovingClick = function(element, handler) {
    var moving = false;
    element.mousedown(function() {
      moving = false;
    });
    element.mousemove(function() {
      moving = true;
    });

    element.mouseup(function() {
      if (moving) {
        return;
      }
      handler(arguments);
    });
  };


  this.cancelEventDispatch(movingAnchor, 'mouseout');
  this.cancelEventDispatch(header, 'mouseout');
  this.cancelEventDispatch(text, 'mouseout');

  /**  resizing header  **/
  movingAnchor.mousedown(function(evt) {
    table.trigger(MultiTableEvent.colResizeStarted, [evt.screenX, columnIndex]);
    evt.cancelBubble = true;
    evt.stopped = true;
    evt.stopPropagation();
  });

  table.on(MultiTableEvent.colResized, function(evt, column) {
    if (columnOption === column) {
      header.css("width", columnOption.width + "px");
    }
  });

  /**  sorting header  **/
  if (columnOption.sortable) {

    this.notMovingClick(header, function() {
      sortAsc = !sortAsc;
      table.trigger(MultiTableEvent.columnSortClicked, [columnIndex, sortAsc, columnOption]);
    });


    table.on(MultiTableEvent.columnSortClicked, function(event, columnIndex, sortAsc, column) {
      if (columnOption === column) {
        text.css("width", "calc(100% - 31px)");
        sortImage.css("display", "inline-block");
        if (sortAsc) {
          sortImage.addClass("ns-tableSortDescIcon");
          sortImage.removeClass("ns-tableSortAscIcon");
        } else {
          sortImage.removeClass("ns-tableSortDescIcon");
          sortImage.addClass("ns-tableSortAscIcon");
        }
      } else {
        text.css("width", "");
        sortAsc = null;
        sortImage.hide();
      }
    });
  }

};

var MultiTableRow = function(table, data, rowIndex) {
  var row = jQuery("<div>", {
    style: "border-bottom: 1px dotted #d6d6d6;white-space: nowrap;"
  });
  if (rowIndex % 2 > 0) {
    row.css("background", "#fbfbfb");
  }
  row.css("height", table.getRowHeight() + "px");
  row.css("line-height", table.getRowHeight() + "px");
  var cells = [];

  var columns = table.getColumns();

  columns.forEach(function(column) {
    var cell = new MultiTableCell(column.width, data[column.id]);
    cells.push(cell);
    row.append(cell.getWidget());
  });

  var selected = false;
  var hover = false;

  this.validateBackground = function() {
    if (selected) {
      if (hover) {
        row.css("background", "#e7e8b1");
      } else {
        row.css("background", "#deefff");
      }
    } else if (hover) {
      row.css("background", "#f5f5dc");
    } else {
      if (rowIndex % 2 > 0) {
        row.css("background", "#fbfbfb");
      } else {
        row.css("background", "");
      }
    }
  };


  var instance = this;

  table.on(MultiTableEvent.colResized, function(evt, column, columnIndex) {
    cells[columnIndex].setWidth(column.width);
  });

  row.click(function() {
    table.trigger(MultiTableEvent.dataClicked, [data]);
  });

  table.on(MultiTableEvent.dataSelected, function() {
    selected = table.isSelected(data);
    instance.validateBackground();
  });

  table.on(MultiTableEvent.dataDeselected, function() {
    selected = table.isSelected(data);
    instance.validateBackground();
  });

  this.getWidget = function() {
    return row;
  };

  selected = table.isSelected(data);

  this.validateBackground(false, false);
};

var MultiTableCell = function(width, value) {
  var cell = jQuery("<div>", {
    style: "padding-left: 12px; padding-right: 12px;display: inline-block; border-right: 1px dotted #d6d6d6; overflow: hidden; white-space: nowrap; text-overflow: ellipsis; height: 100%; box-sizing: border-box;",
    title: value
  });
  cell.text(value);
  this.getWidget = function() {
    return cell;
  };
  this.setWidth = function(width) {
    cell.css("width", width + "px");
  };
  this.setWidth(width);
};


// ТАБЛИЦА РЕЕСТРА

var RegistryTable = function(registry, multiChoose, filterIds, selectedData) {
  var columns = [];
  var recordsCount = 0;
  var currentPage = 0;
  var search = null;
  var sortCmpId = null;
  var sortAsc = null;
  var countInPage = 15;
  var instance = this;
  this.filterID = null;

  var width = 780;

  registry.columns.forEach(function(col) {
    if (col.visible != 1) {
      return;
    }
    col.id = col.columnID;
    col.name = col.label;
    columns.push(col);
  });

  var columnWidth = 150;
  if (columns.length <= 5) {
    columnWidth = (width - 1) / columns.length;
  }

  columns = columns.sort(function(item1, item2) {
    var number1 = item1.order;
    var number2 = item2.order;

    if (number1 === number2) {
      if (item1.name < item2.name) {
        return -1;
      } else if (item1.name > item2.name) {
        return 1;
      }
    } else {
      if (number1 === 0) {
        return 1;
      } else if (number2 === 0) {
        return -1;
      } else if (number1 < number2) {
        return -1;
      } else {
        return 1;
      }
    }
    return 0;
  });

  columns.forEach(function(column) {
    column.width = columnWidth;
    column.minWidth = 35;
  });

  var table = new MultiTable(multiChoose, selectedData);
  table.init({
    idField: 'documentId'
  }, columns);

  this.dblclick = function(handler) {
    table.getWidget().dblclick(handler);
  };

  table.on(MultiTableEvent.columnSortClicked, function(event, columnIndex, sortAsc, columnOption) {
    instance.sort(columnOption.id, sortAsc);
  });

  this.addDataClicked = function(handler) {
    table.on(MultiTableEvent.dataClicked, handler);
  };

  this.search = function(newSearch) {
    search = newSearch;
    this.loadData(0, this.filterID);
  };

  this.sort = function(cmpId, asc) {
    sortCmpId = cmpId;
    sortAsc = asc;
    this.loadData(0, this.filterID);
  };

  this.parseData = function(data) {
    var registryData = [];
    data.result.forEach(function(item) {
      var mergedItem = item.fieldValue;
      mergedItem.documentId = item.documentID;
      mergedItem.asfDataId = item.dataUUID;
      mergedItem.uuid = item.documentID;
      registryData.push(mergedItem);
    });
    return registryData;
  };

  this.loadData = function(pageNumber) {
    currentPage = pageNumber;
    var params = {
      registryID: registry.registryID,
      pageNumber: pageNumber,
      countInPart: countInPage,
      searchString: search,
      sortCmpID: sortCmpId,
      sortDesc: sortAsc
    };
    if (model.filterCode) params.filterCode = model.filterCode;
    if (model.additionalParams) {
      params = Object.assign(params, model.additionalParams);
    }
    if (filterIds) {
      params.pageNumber = 0;
      params.countInPart = 0;
    }

    AS.SERVICES.showWaitWindow();
    AS.FORMS.ApiUtils.simpleAsyncGet('rest/api/registry/data_ext?' + $.param(params), function(data) {
      data = instance.filterData(data, pageNumber);
      recordsCount = data.recordsCount;
      table.setData(instance.parseData(data));
      AS.SERVICES.hideWaitWindow();
    });
  };

  this.selectAll = function() {
    if (!multiChoose) return;
    var params = {
      registryID: registry.registryID,
      pageNumber: 0,
      countInPart: 0,
      searchString: search,
      sortCmpID: sortCmpId,
      sortDesc: sortAsc
    };
    if (model.filterCode) params.filterCode = model.filterCode;
    AS.SERVICES.showWaitWindow();
    AS.FORMS.ApiUtils.simpleAsyncGet('rest/api/registry/data_ext?' + $.param(params), function(data) {
      data = instance.filterData(data, 0);
      table.setSelectedData(instance.parseData(data));
      AS.SERVICES.hideWaitWindow();
    });
  };

  this.filterData = function(data, pageNumber) {
    if (!filterIds) return data;

    var result = {
      recordsCount: 0,
      result: []
    };

    var start = pageNumber * countInPage;
    var finish = start + countInPage;

    data.result.forEach(function(record) {
      if (filterIds.indexOf(record.documentID) > -1) {
        if (!pageNumber || (result.recordsCount >= start && result.recordsCount < finish)) {
          result.result.push(record);
        }
        result.recordsCount++;
      }
    });

    return result;
  }

  this.getCurrentPage = function() {
    return currentPage;
  };

  this.getRecordsCount = function() {
    return recordsCount;
  };

  this.getPagesCount = function() {
    var pagesCount = Math.floor(recordsCount / countInPage);
    if (recordsCount % countInPage) {
      pagesCount++;
    }
    return pagesCount;
  };

  this.getSelectedData = function() {
    return table.getSelectedData();
  };

  this.loadData(0, this.filterID);

  this.on = function(event, handler) {
    table.on(event, handler);
  };

  this.getSelectedData = function() {
    return table.getSelectedData();
  };

  this.getWidget = function() {
    return table.getWidget();
  };
};


var RegistryChooser = function(registry, multiChoose, filterIds, selectedIds, handler) {
  var instance = this;

  this.container = jQuery("<div>");

  var searchInput = jQuery("<input>", {
    style: "color: #606060; height: 32px;box-sizing: border-box; " +
      "border: #e2e2e2 1px solid;border-radius: 4px;padding-left: 9px;" +
      "vertical-align:top; width: 480px; margin-right:20px",
    placeHolder: i18n.tr("Поиск")
  });

  var prevPage = jQuery("<div>", {
    style: "height: 32px; width: 34px;border: 1px solid #d6d6d6;" +
      "border-top-left-radius: 4px; border-bottom-left-radius: 4px; display: inline-block; " +
      "box-sizing: border-box;vertical-align:top;vertical-align: middle;font-size: 20px;" +
      "padding-left: 5px; line-height: 30px; color: #afafaf; user-select: none;" +
      `background: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAAPJJREFUeNqk069KREEUx/HPLgomsVl8AXEFQSwWEU3qC1iEYzAZNvoeFg1ykti1KIJPYBDcIJr8EzQZTIJwLRN0Ee69eNpw5vtl5ndmOlVV+U+NNNkUEXPYQz8zX3/2ug3gaZxiAxPD/W4NPINLjGM1M+8aXyEierjAWIGvG2cQEbMFHsVKZt40DrHA52W5nJmDxlOIiAWc4QuLmflYF/JwiPOYxDs+moz4lyAzD9BHD1cRMVUn6Pz1EiNiF/u4xVpmvrQSFMkODotkPTOfWwmKZBtHGBTJUytBkWziGPdYysy3Vn8hM0+whQd8tj5BXX0PAB+FV2HorGSFAAAAAElFTkSuQmCC') no-repeat center;`
  });

  var nextPage = jQuery("<div>", {
    style: "height: 32px; width: 34px;border: 1px solid #d6d6d6;" +
      "border-top-right-radius: 4px; border-bottom-right-radius: 4px; display: inline-block;" +
      "box-sizing: border-box;vertical-align: middle;font-size: 20px;" +
      "padding-left: 5px; line-height: 30px; color: #afafaf; user-select: none;" +
      `background: url('data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQCAYAAAAf8/9hAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKT2lDQ1BQaG90b3Nob3AgSUNDIHByb2ZpbGUAAHjanVNnVFPpFj333vRCS4iAlEtvUhUIIFJCi4AUkSYqIQkQSoghodkVUcERRUUEG8igiAOOjoCMFVEsDIoK2AfkIaKOg6OIisr74Xuja9a89+bN/rXXPues852zzwfACAyWSDNRNYAMqUIeEeCDx8TG4eQuQIEKJHAAEAizZCFz/SMBAPh+PDwrIsAHvgABeNMLCADATZvAMByH/w/qQplcAYCEAcB0kThLCIAUAEB6jkKmAEBGAYCdmCZTAKAEAGDLY2LjAFAtAGAnf+bTAICd+Jl7AQBblCEVAaCRACATZYhEAGg7AKzPVopFAFgwABRmS8Q5ANgtADBJV2ZIALC3AMDOEAuyAAgMADBRiIUpAAR7AGDIIyN4AISZABRG8lc88SuuEOcqAAB4mbI8uSQ5RYFbCC1xB1dXLh4ozkkXKxQ2YQJhmkAuwnmZGTKBNA/g88wAAKCRFRHgg/P9eM4Ors7ONo62Dl8t6r8G/yJiYuP+5c+rcEAAAOF0ftH+LC+zGoA7BoBt/qIl7gRoXgugdfeLZrIPQLUAoOnaV/Nw+H48PEWhkLnZ2eXk5NhKxEJbYcpXff5nwl/AV/1s+X48/Pf14L7iJIEyXYFHBPjgwsz0TKUcz5IJhGLc5o9H/LcL//wd0yLESWK5WCoU41EScY5EmozzMqUiiUKSKcUl0v9k4t8s+wM+3zUAsGo+AXuRLahdYwP2SycQWHTA4vcAAPK7b8HUKAgDgGiD4c93/+8//UegJQCAZkmScQAAXkQkLlTKsz/HCAAARKCBKrBBG/TBGCzABhzBBdzBC/xgNoRCJMTCQhBCCmSAHHJgKayCQiiGzbAdKmAv1EAdNMBRaIaTcA4uwlW4Dj1wD/phCJ7BKLyBCQRByAgTYSHaiAFiilgjjggXmYX4IcFIBBKLJCDJiBRRIkuRNUgxUopUIFVIHfI9cgI5h1xGupE7yAAygvyGvEcxlIGyUT3UDLVDuag3GoRGogvQZHQxmo8WoJvQcrQaPYw2oefQq2gP2o8+Q8cwwOgYBzPEbDAuxsNCsTgsCZNjy7EirAyrxhqwVqwDu4n1Y8+xdwQSgUXACTYEd0IgYR5BSFhMWE7YSKggHCQ0EdoJNwkDhFHCJyKTqEu0JroR+cQYYjIxh1hILCPWEo8TLxB7iEPENyQSiUMyJ7mQAkmxpFTSEtJG0m5SI+ksqZs0SBojk8naZGuyBzmULCAryIXkneTD5DPkG+Qh8lsKnWJAcaT4U+IoUspqShnlEOU05QZlmDJBVaOaUt2ooVQRNY9aQq2htlKvUYeoEzR1mjnNgxZJS6WtopXTGmgXaPdpr+h0uhHdlR5Ol9BX0svpR+iX6AP0dwwNhhWDx4hnKBmbGAcYZxl3GK+YTKYZ04sZx1QwNzHrmOeZD5lvVVgqtip8FZHKCpVKlSaVGyovVKmqpqreqgtV81XLVI+pXlN9rkZVM1PjqQnUlqtVqp1Q61MbU2epO6iHqmeob1Q/pH5Z/YkGWcNMw09DpFGgsV/jvMYgC2MZs3gsIWsNq4Z1gTXEJrHN2Xx2KruY/R27iz2qqaE5QzNKM1ezUvOUZj8H45hx+Jx0TgnnKKeX836K3hTvKeIpG6Y0TLkxZVxrqpaXllirSKtRq0frvTau7aedpr1Fu1n7gQ5Bx0onXCdHZ4/OBZ3nU9lT3acKpxZNPTr1ri6qa6UbobtEd79up+6Ynr5egJ5Mb6feeb3n+hx9L/1U/W36p/VHDFgGswwkBtsMzhg8xTVxbzwdL8fb8VFDXcNAQ6VhlWGX4YSRudE8o9VGjUYPjGnGXOMk423GbcajJgYmISZLTepN7ppSTbmmKaY7TDtMx83MzaLN1pk1mz0x1zLnm+eb15vft2BaeFostqi2uGVJsuRaplnutrxuhVo5WaVYVVpds0atna0l1rutu6cRp7lOk06rntZnw7Dxtsm2qbcZsOXYBtuutm22fWFnYhdnt8Wuw+6TvZN9un2N/T0HDYfZDqsdWh1+c7RyFDpWOt6azpzuP33F9JbpL2dYzxDP2DPjthPLKcRpnVOb00dnF2e5c4PziIuJS4LLLpc+Lpsbxt3IveRKdPVxXeF60vWdm7Obwu2o26/uNu5p7ofcn8w0nymeWTNz0MPIQ+BR5dE/C5+VMGvfrH5PQ0+BZ7XnIy9jL5FXrdewt6V3qvdh7xc+9j5yn+M+4zw33jLeWV/MN8C3yLfLT8Nvnl+F30N/I/9k/3r/0QCngCUBZwOJgUGBWwL7+Hp8Ib+OPzrbZfay2e1BjKC5QRVBj4KtguXBrSFoyOyQrSH355jOkc5pDoVQfujW0Adh5mGLw34MJ4WHhVeGP45wiFga0TGXNXfR3ENz30T6RJZE3ptnMU85ry1KNSo+qi5qPNo3ujS6P8YuZlnM1VidWElsSxw5LiquNm5svt/87fOH4p3iC+N7F5gvyF1weaHOwvSFpxapLhIsOpZATIhOOJTwQRAqqBaMJfITdyWOCnnCHcJnIi/RNtGI2ENcKh5O8kgqTXqS7JG8NXkkxTOlLOW5hCepkLxMDUzdmzqeFpp2IG0yPTq9MYOSkZBxQqohTZO2Z+pn5mZ2y6xlhbL+xW6Lty8elQfJa7OQrAVZLQq2QqboVFoo1yoHsmdlV2a/zYnKOZarnivN7cyzytuQN5zvn//tEsIS4ZK2pYZLVy0dWOa9rGo5sjxxedsK4xUFK4ZWBqw8uIq2Km3VT6vtV5eufr0mek1rgV7ByoLBtQFr6wtVCuWFfevc1+1dT1gvWd+1YfqGnRs+FYmKrhTbF5cVf9go3HjlG4dvyr+Z3JS0qavEuWTPZtJm6ebeLZ5bDpaql+aXDm4N2dq0Dd9WtO319kXbL5fNKNu7g7ZDuaO/PLi8ZafJzs07P1SkVPRU+lQ27tLdtWHX+G7R7ht7vPY07NXbW7z3/T7JvttVAVVN1WbVZftJ+7P3P66Jqun4lvttXa1ObXHtxwPSA/0HIw6217nU1R3SPVRSj9Yr60cOxx++/p3vdy0NNg1VjZzG4iNwRHnk6fcJ3/ceDTradox7rOEH0x92HWcdL2pCmvKaRptTmvtbYlu6T8w+0dbq3nr8R9sfD5w0PFl5SvNUyWna6YLTk2fyz4ydlZ19fi753GDborZ752PO32oPb++6EHTh0kX/i+c7vDvOXPK4dPKy2+UTV7hXmq86X23qdOo8/pPTT8e7nLuarrlca7nuer21e2b36RueN87d9L158Rb/1tWeOT3dvfN6b/fF9/XfFt1+cif9zsu72Xcn7q28T7xf9EDtQdlD3YfVP1v+3Njv3H9qwHeg89HcR/cGhYPP/pH1jw9DBY+Zj8uGDYbrnjg+OTniP3L96fynQ89kzyaeF/6i/suuFxYvfvjV69fO0ZjRoZfyl5O/bXyl/erA6xmv28bCxh6+yXgzMV70VvvtwXfcdx3vo98PT+R8IH8o/2j5sfVT0Kf7kxmTk/8EA5jz/GMzLdsAAAAgY0hSTQAAeiUAAICDAAD5/wAAgOkAAHUwAADqYAAAOpgAABdvkl/FRgAAAOtJREFUeNqk061KhFEUheFnBhEsYhuDGgVxJonBKBq9A0HYGEyCl2E0GMTgjnZBEQXBZjBMMAhehMFk+iwnfPgzztHVVlgv6xzW7jRN4z+aaJuImMURDjNzOA6g+8nPYAsXEbFUDcjMZ2xiGrcRsVzbQGY+FMgUbiKiXwUokEdsYLJABlWAAhlivdjrnyDdUfUy8wlraMqfrFYBit7wih5WqgARMYc79HGQmScjh/RN+AoD7Gfm8a9LbIXncVnCe5l5OtaUS3ihhPvYzcyzUc/stI8pInq4xyK2M/O8donveMHOOOEvDf6ijwEA/0NKYA/mAUIAAAAASUVORK5CYII=') no-repeat center;`
  });

  var pageNumber = jQuery("<div>", {
    style: "display: inline-block;border: #e2e2e2 1px solid;width: 80px;" +
      "height: 32px;box-sizing: border-box;vertical-align:top;text-align: center; line-height: 32px;"
  });

  var selectedButton = jQuery("<div>", {
    style: "padding-left : 20px; padding-right:20px; height: 30px;" +
      "line-height: 30px;border:#55cc95 1px solid; border-radius: 4px; cursor: pointer;" +
      "text-align: center; color: #ffffff; background-color:#55cc95; font-family: DroidSansBold;" +
      "font-size: 14px; width:130px; margin:0 auto"
  });

  var selectAll = jQuery('<input/>', {
    type: "checkbox",
    style: 'display: inline-block ; vertical-align: middle;'
  });
  var selectAllLabel = jQuery('<label/>', {
    style: "color: #606060; font-family: DroidSans, arial, serif; " +
      "font-size: 13px; display: inline-block; margin-right:30px"
  });

  selectAllLabel.append(selectAll);
  selectAllLabel.append(jQuery("<span>").html(i18n.tr("Выбрать все")));

  selectedButton.html(i18n.tr("Выбрать"));

  var tableScroll = jQuery("<div>", {
    style: "width: 780px; height : 460px; border: 1px solid #efefef; overflow : auto; margin-top: 5px; margin-bottom: 5px;"
  });

  var headerPanel = jQuery("<div>");
  headerPanel.append(searchInput);
  if (multiChoose) {
    headerPanel.append(selectAllLabel);
  } else {
    searchInput.css("width", "610px");
  }
  headerPanel.append(prevPage);
  headerPanel.append(pageNumber);
  headerPanel.append(nextPage);

  var selectedData = [];
  selectedIds.forEach(function(selectedId) {
    selectedData.push({
      documentId: selectedId
    });
  });

  var registryTable = new RegistryTable(registry, multiChoose, filterIds, selectedData);

  registryTable.addDataClicked(function() {
    selectAll.removeAttr("checked");
  });

  var currentPage = 1;

  registryTable.on(MultiTableEvent.dataLoaded, function() {
    currentPage = registryTable.getCurrentPage() + 1;
    var totalPage = registryTable.getPagesCount();
    if (isNaN(totalPage)) {
      totalPage = 0;
    }
    if (totalPage === 0) {
      currentPage = 0;
    }
    pageNumber.html(currentPage + "/" + totalPage);
  });

  registryTable.dblclick(function() {
    if (multiChoose) return;
    if (registryTable.getSelectedData().length == 0) return;

    instance.close(true);
  });

  selectedButton.click(function() {
    instance.close(true);
  });

  this.close = function(executeHandler) {
    if (executeHandler === true) {
      handler(registryTable.getSelectedData());
    }
    instance.container.dialog("destroy");
  };

  prevPage.click(function() {
    if (currentPage <= 1) return;
    currentPage = currentPage - 1;
    registryTable.loadData(currentPage - 1);
  });

  nextPage.click(function() {
    if (currentPage >= registryTable.getPagesCount()) return;
    currentPage = currentPage + 1;
    registryTable.loadData(currentPage - 1);
  });

  selectAll.click(function() {
    registryTable.selectAll();
  });

  searchInput.keypress(function(event) {
    if (event.keyCode == 13) registryTable.search(searchInput.val());
  });

  this.show = function() {
    instance.container.dialog({
      width: 800,
      height: 600,
      title: i18n.tr("Выбор записей реестра: ") + registry.name,
      modal: true,
      close: function() {
        instance.close(false);
      }
    });
  };

  tableScroll.append(registryTable.getWidget());

  this.container.append(headerPanel);
  this.container.append(tableScroll);
  this.container.append(selectedButton);
};
