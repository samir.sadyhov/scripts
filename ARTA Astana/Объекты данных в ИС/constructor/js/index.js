if($('.menu__icon').length) {
  let openMenu = false;
  let xDown = null;
  let yDown = null;

  const hideShowMenu = () => {
    if($('.filter-window').css('display') == 'block') return;

    if(!openMenu) {
      openMenu = !openMenu;
      $('.menu-container').css({'display': 'flex'});
      setTimeout(() => {
        $('.menu__icon').addClass('_active');
        $('.menu-container').addClass('_open');
        $('body').addClass('lock');
      }, 0);
    } else {
      openMenu = !openMenu;
      $('.menu__icon').removeClass('_active');
      $('.menu-container').removeClass('_open');
      $('body').removeClass('lock');
      setTimeout(() => {
        $('.menu-container').css({'display': 'none'});
      }, 400);
    }
  }

  const handleTouchStart = evt => {
    xDown = evt.touches[0].clientX;
    yDown = evt.touches[0].clientY;
  }

  const handleTouchMove = evt => {
    if ( ! xDown || ! yDown ) return;
    let xUp = evt.touches[0].clientX;
    let yUp = evt.touches[0].clientY;
    let xDiff = xDown - xUp;
    let yDiff = yDown - yUp;

    if(Math.abs(xDiff) > Math.abs(yDiff)) {
      if (xDiff > 0) {
        //left swipe
        if(openMenu) hideShowMenu();
      } else {
        //right swipe
        if(!openMenu) hideShowMenu();
      }
    }
    xDown = null;
    yDown = null;
  }

  $('.menu__icon').off().on('click', e => {
    e.preventDefault();
    hideShowMenu();
  });

  $('.menu-container').off().on('click', e => {
    e.preventDefault();
    if($(e.target).hasClass('menu-container')) hideShowMenu();
    if($(e.target).hasClass('menu-button')) hideShowMenu();
  });

  document.addEventListener('touchstart', handleTouchStart, false);
  document.addEventListener('touchmove', handleTouchMove, false);
}

//Иконка приложения
if(!$('link[rel="icon"]').length) {
  $('head').append('<link rel="icon" href="data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIGVuYWJsZS1iYWNrZ3JvdW5kPSJuZXcgMCAwIDI0IDI0IiBoZWlnaHQ9IjI0cHgiIHZpZXdCb3g9IjAgMCAyNCAyNCIgd2lkdGg9IjI0cHgiIGZpbGw9IiMyNmQwY2UiPjxyZWN0IGZpbGw9Im5vbmUiIGhlaWdodD0iMjQiIHdpZHRoPSIyNCIvPjxwYXRoIGQ9Ik0yMSw2LjVjLTEuNjYsMC0zLDEuMzQtMywzYzAsMC4wNywwLDAuMTQsMC4wMSwwLjIxbC0yLjAzLDAuNjhjLTAuNjQtMS4yMS0xLjgyLTIuMDktMy4yMi0yLjMyVjUuOTEgQzE0LjA0LDUuNTcsMTUsNC40LDE1LDNjMC0xLjY2LTEuMzQtMy0zLTNTOSwxLjM0LDksM2MwLDEuNCwwLjk2LDIuNTcsMi4yNSwyLjkxdjIuMTZjLTEuNCwwLjIzLTIuNTgsMS4xMS0zLjIyLDIuMzJMNS45OSw5LjcxIEM2LDkuNjQsNiw5LjU3LDYsOS41YzAtMS42Ni0xLjM0LTMtMy0zcy0zLDEuMzQtMywzczEuMzQsMywzLDNjMS4wNiwwLDEuOTgtMC41NSwyLjUyLTEuMzdsMi4wMywwLjY4IGMtMC4yLDEuMjksMC4xNywyLjY2LDEuMDksMy42OWwtMS40MSwxLjc3QzYuODUsMTcuMDksNi40NCwxNyw2LDE3Yy0xLjY2LDAtMywxLjM0LTMsM3MxLjM0LDMsMywzczMtMS4zNCwzLTMgYzAtMC42OC0wLjIyLTEuMy0wLjYtMS44bDEuNDEtMS43N2MxLjM2LDAuNzYsMy4wMiwwLjc1LDQuMzcsMGwxLjQxLDEuNzdDMTUuMjIsMTguNywxNSwxOS4zMiwxNSwyMGMwLDEuNjYsMS4zNCwzLDMsM3MzLTEuMzQsMy0zIHMtMS4zNC0zLTMtM2MtMC40NCwwLTAuODUsMC4wOS0xLjIzLDAuMjZsLTEuNDEtMS43N2MwLjkzLTEuMDQsMS4yOS0yLjQsMS4wOS0zLjY5bDIuMDMtMC42OGMwLjUzLDAuODIsMS40NiwxLjM3LDIuNTIsMS4zNyBjMS42NiwwLDMtMS4zNCwzLTNTMjIuNjYsNi41LDIxLDYuNXogTTMsMTAuNWMtMC41NSwwLTEtMC40NS0xLTFjMC0wLjU1LDAuNDUtMSwxLTFzMSwwLjQ1LDEsMUM0LDEwLjA1LDMuNTUsMTAuNSwzLDEwLjV6IE02LDIxIGMtMC41NSwwLTEtMC40NS0xLTFjMC0wLjU1LDAuNDUtMSwxLTFzMSwwLjQ1LDEsMUM3LDIwLjU1LDYuNTUsMjEsNiwyMXogTTExLDNjMC0wLjU1LDAuNDUtMSwxLTFzMSwwLjQ1LDEsMWMwLDAuNTUtMC40NSwxLTEsMSBTMTEsMy41NSwxMSwzeiBNMTIsMTVjLTEuMzgsMC0yLjUtMS4xMi0yLjUtMi41YzAtMS4zOCwxLjEyLTIuNSwyLjUtMi41czIuNSwxLjEyLDIuNSwyLjVDMTQuNSwxMy44OCwxMy4zOCwxNSwxMiwxNXogTTE4LDE5IGMwLjU1LDAsMSwwLjQ1LDEsMWMwLDAuNTUtMC40NSwxLTEsMXMtMS0wLjQ1LTEtMUMxNywxOS40NSwxNy40NSwxOSwxOCwxOXogTTIxLDEwLjVjLTAuNTUsMC0xLTAuNDUtMS0xYzAtMC41NSwwLjQ1LTEsMS0xIHMxLDAuNDUsMSwxQzIyLDEwLjA1LDIxLjU1LDEwLjUsMjEsMTAuNXoiLz48L3N2Zz4K">')
}
