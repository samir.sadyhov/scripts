Array.prototype.uniq = function() {
  return this.filter((v, i, a) => i == a.indexOf(v));
}

const dicts = {
  uniqueness: {
    1: 'round_icon uniq_one',
    2: 'round_icon uniq_two',
    3: 'round_icon uniq_three',
    4: 'round_icon uniq_four'
  },
  filling: {
    1: 'round_icon fill_one',
    2: 'round_icon fill_two',
    3: 'round_icon fill_three'
  },
  operations: {
    1: 'operation_icon operation_one',
    2: 'operation_icon operation_two',
    3: 'operation_icon operation_three',
    4: 'operation_icon operation_four'
  },
  duplication: {
    1: '#AA60EE',
    2: '#FFFFFF',
    3: '#7DD9FF'
  }
}

const getSystem = data => {
  const result = [];
  data.result.forEach(item => {
    if(item.fieldKey.hasOwnProperty('reglink_name_system')) {
      result.push(item.fieldKey.reglink_name_system);
    }
  });
  return result.uniq();
}

const getSystemPart = (data, parentID) => {
  const result = [];
  data.result.forEach(item => {
    if(item.fieldKey.hasOwnProperty('reglink_name_system') && item.fieldKey.hasOwnProperty('reglink_name_system_part')) {
      if(item.fieldKey.reglink_name_system == parentID) result.push(item.fieldKey.reglink_name_system_part);
    }
  });
  return result.uniq();
}

const parseDate = data => {
  const result = {};
  if(!data) return result;

  const system = getSystem(data);

  system.forEach(parentID => {
    result[parentID] = {};
    const children = getSystemPart(data, parentID);
    children.forEach(childID => {
      const childData = [];
      result[parentID][childID] = childData;
      data.result.forEach(item => {
        if(item.fieldKey.hasOwnProperty('reglink_name_system') && item.fieldKey.hasOwnProperty('reglink_name_system_part')) {
          if(item.fieldKey.reglink_name_system == parentID && item.fieldKey.reglink_name_system_part == childID) {
            const isData = {};
            childData.push(isData);
            isData.dataUUID = item.dataUUID;
            for(let key in item.fieldValue) {
              isData[key] = {};
              isData[key].value = item.fieldValue[key];
              if(item.fieldKey.hasOwnProperty(key)) {
                isData[key].key = item.fieldKey[key];
              }
            }
          }
        }
      });
    });
  });

  return result;
}

const getOrgData = orgID => {
  let url = `api/registry/data_ext?registryCode=registry_data_objects_in_it&field=listbox_gov&condition=TEXT_EQUALS&key=${orgID}`;
  ['reglink_name_system', 'reglink_name_system_part', 'listbox_uniqueness', 'textbox_data_type', 'listbox_filling_method', 'listbox_operations_performed', 'listbox_duplication_level', 'textbox_data_objects'].forEach(field => url += `&fields=${field}`);
  return new Promise((resolve, reject) => {
    rest.synergyGet(url, res => res.recordsCount > 0 ? resolve(res) : resolve(null), err => reject(err));
  });
}

const getModalDialog = (title, body, buttonName, buttonAction) => {
  let dialog = $('<div class="uk-flex-top" uk-modal>');
  let md = $('<div class="uk-modal-dialog uk-margin-auto-vertical">');
  let modalBody = $('<div class="uk-modal-body" uk-overflow-auto>');
  let footer = $('<div class="uk-modal-footer uk-text-right">');

  modalBody.append(body);
  footer.append('<button class="uk-button uk-button-default uk-modal-close" type="button">Закрыть</button>');
  dialog.append(md);
  md.append(`<div class="uk-modal-header"><h3>${title}</h3></div>`)
  .append(modalBody).append(footer);

  if(buttonName) {
    let actionButton = $('<button class="uk-button uk-button-primary uk-margin-left" type="button">');
    actionButton.html(buttonName).on('click', buttonAction);
    footer.append(actionButton);
  }

  return dialog;
}

const openFormData = (uuid, title) => {
  const player = AS.FORMS.createPlayer();
  player.view.setEditable(false);
  player.showFormData(null, null, uuid);

  const dialog = getModalDialog(title, player.view.container);

  UIkit.modal(dialog).show();
  dialog.on('hidden', () => dialog.remove());
}

const changeOrgNameLabel = (tRu, tKz) => fire({type: 'change_label', text: localizedText(tRu, tRu, tKz, tRu)}, 'organization_name');

const getISContainer = name => {
  const container = $('<div>', {class: 'is_container'});
  const title = $('<span>', {class: 'is_title'}).text(name);
  container.append(title);
  return container;
}

const getParentBlock = param => {
  const block = $('<div>', {class: 'parent_model_container'});
  const body = $('<div>', {class: 'parent_body', style: `background: ${dicts.duplication[param.listbox_duplication_level.key]}`});
  const name = $('<span>', {class: 'parent_name'});
  const icons = $('<div>', {class: 'parent_icons'});
  const footer = $('<span>', {class: 'parent_footer'});

  let systemName = 'Информация';

  if(param.hasOwnProperty('textbox_data_objects')) name.text(param.textbox_data_objects.value || '');
  if(param.hasOwnProperty('textbox_data_type')) footer.text(param.textbox_data_type.value || '');
  if(param.hasOwnProperty('reglink_name_system_part')) systemName = param.reglink_name_system_part.value || '';

  icons.append($('<div>', {class: dicts.uniqueness[param.listbox_uniqueness.key]}));
  icons.append($('<div>', {class: dicts.filling[param.listbox_filling_method.key]}));
  icons.append($('<div>', {class: dicts.operations[param.listbox_operations_performed.key]}));

  body.append(name).append(icons);
  block.append(body).append(footer);

  block.on('click', e => {
    openFormData(param.dataUUID, systemName);
  });

  return block;
}

const getBlockModule = (param) => {
  const block = $('<div>', {class: 'model_container'});
  const title = $('<span>', {class: '_title'});
  block.append(title);
  title.text(param[0].reglink_name_system_part.value);
  param.forEach(item => block.append(getParentBlock(item)));
  return block;
}

const renderModels = orgID => {
  const modelContainer = $('.model_content');
  modelContainer.empty();
  modelContainer.removeClass('hidden');

  Cons.showLoader();

  let resultApi;

  getOrgData(orgID).then(res => {
    resultApi = res;
    return parseDate(res);
  }).then(data => {

    for(let key in data) {
      const systemName = resultApi.result.find(x => x.fieldKey.reglink_name_system == key).fieldValue.reglink_name_system;
      const isContainer = getISContainer(systemName);
      modelContainer.append(isContainer);
      for(let param in data[key]) isContainer.append(getBlockModule(data[key][param]));
    }

    Cons.hideLoader();
  }).catch(err => {
    Cons.hideLoader();
    console.log('ERROR', err);
  });
}

const getmenuItem = item => {
  const button = $('<button>', {class: 'menu-button'});
  button.text(item.name);
  button.on('click', e => {
    e.preventDefault();
    e.target.blur();
    $('#org_list .menu-button').removeClass('selected');
    button.addClass('selected');
    changeOrgNameLabel(item.name, item.name);
    renderModels(item.id);
  });
  return button;
}

pageHandler('model_is_page', () => {
  const {organizations} = Cons.getAppStore();
  const container = $('#org_list');
  organizations.forEach(org => container.append(getmenuItem(org)));
});
