const getDictionaryOrganizations = () => {
  return new Promise(resolve => {
    rest.synergyGet('api/dictionaries/government_bodies',
      res => resolve(parseOrgDict(res.items)),
      err => resolve([])
    );
  });
}

const parseOrgDict = items => {
  return Object.values(items).map(item => {
    const res = {};
    for(let key in item) res[key] = item[key].value;
    return res;
  }).sort((a, b) => a.id - b.id);
}

pageHandler('auth_page', () => {
  if (!Cons.getAppStore().auth_page_listener) {

    addListener('auth_success', 'button-auth', authed => {
      const {login, password} = authed.creds;

      Cons.creds.login = login;
      Cons.creds.password = password;
      AS.apiAuth.setCredentials(login, password);
      AS.OPTIONS.login = login;
      AS.OPTIONS.password = password;
      AS.OPTIONS.currentUser = authed.data.person;

      Cons.setAppStore({userGroups: authed.data.groups});

      getDictionaryOrganizations().then(organizations => {
        Cons.setAppStore({organizations: organizations});
        fire({type: 'goto_page', pageCode: 'model_is_page'}, 'button-auth');
      });
    });

    Cons.setAppStore({auth_page_listener: true});
  }
});
