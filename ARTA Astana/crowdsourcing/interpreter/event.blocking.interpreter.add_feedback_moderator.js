var result = true;
var message = "ok";

try {

  let urlSearch = 'rest/api/registry/data_ext?registryCode=crowdsourcing_registry_settings&pageNumber=0&countInPart=1&fields=entity_feedback_moderator';

  let res = API.httpGetMethod(urlSearch);
  res = res.result[0];

  let data = [];
  data.push({
    id: 'entity_moderator',
    type: 'entity',
    value: res.fieldValue['entity_feedback_moderator'],
    key: res.fieldKey['entity_feedback_moderator']
  });

  API.mergeFormData({uuid: dataUUID, data: data});

} catch (err) {
  log.error(err.message);
  message = err.message;
}
