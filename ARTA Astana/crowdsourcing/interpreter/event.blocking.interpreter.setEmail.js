var result = true;
var message = "ok";

function getUserInfo(userID) {
  let client = API.getHttpClient();
  let get = new org.apache.commons.httpclient.methods.GetMethod("http://127.0.0.1:8080/crowdsourcing/rest/api/user/get/" + userID);
  get.setRequestHeader("Content-type", "application/json");
  client.executeMethod(get);
  let resp = get.getResponseBodyAsString();
  get.releaseConnection();
  return JSON.parse(resp);
}

try {

  let currentFormData = API.getFormData(dataUUID);
  let entity_author = UTILS.getValue(currentFormData, 'entity_author');

  if(!entity_author || !entity_author.hasOwnProperty('key')) throw new Error('не заполнено поле entity_author');

  let userInfo = getUserInfo(entity_author.key);
  UTILS.setValue(currentFormData, 'textbox_tenant_mail', {value: userInfo.result.email});
  API.mergeFormData(currentFormData);

} catch (err) {
  log.error(err.message);
  message = err.message;
}
