pageHandler('auth', () => {
  if (Cons.getAppStore().auth_page_listener) return;

  addListener('auth_success', 'button-1', authed => {
    const {login, password} = authed.creds;

    AS.apiAuth.setCredentials(login, password);
    AS.OPTIONS.login = login;
    AS.OPTIONS.password = password;
    AS.OPTIONS.currentUser = authed.data.person;

    if(['Unknown', 'bbee3615-9c7b-4493-9179-2b1df91634da'].indexOf(AS.OPTIONS.currentUser.userid) !== -1) {
      fire({type: 'goto_page', pageCode: 'mobile'}, 'button-1');
    } else {
      alert('Доступ в систему запрещен');
    }

  });
  Cons.setAppStore({auth_page_listener: true});
});
