const eventShow = {type: 'set_hidden', hidden: false};
const eventHide = {type: 'set_hidden', hidden: true};

const getRandomValue = (min, max) => {
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

const customFormatDate = datetime => {
  datetime = datetime.split(/\D/);
  return datetime[2] + '.' + datetime[1] + '.' + datetime[0] + ' ' + datetime[3] + ':' + datetime[4];
}

const formatDate = datetime => {
  return datetime.getFullYear() + '-' +
    ('0' + (datetime.getMonth() + 1)).slice(-2) + '-' +
    ('0' + datetime.getDate()).slice(-2) + ' ' +
    ('0' + datetime.getHours()).slice(-2) + ':' +
    ('0' + datetime.getMinutes()).slice(-2) + ':' +
    ('0' + datetime.getSeconds()).slice(-2);
}

const searchInRegistry = (pageNumber, numSearch) => {
  return new Promise(resolve => {
    let url = 'rest/api/registry/data_ext?registryCode=reg_gifts7';
    url += '&fields=numericinput_number&fields=textbox_name'
    url += '&field=listbox_ask_phone&condition=CONTAINS&key=2';
    if(numSearch) {
      url += `&field1=numericinput_number&condition1=EQUALS&key1=${numSearch}&countInPart=1`;
    } else {
      url += `&pageNumber=${pageNumber}&countInPart=1`
    }

    try {
      AS.FORMS.ApiUtils.simpleAsyncGet(url, res => {
        resolve(res);
      });
    } catch (e) {
      resolve(null);
    }
  });
}

const activateDoc = dataUUID => {
  return new Promise(resolve => {
    try {
      AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/activate_doc?dataUUID=${dataUUID}`, res => {
        resolve(res);
      });
    } catch (e) {
      resolve(null);
    }
  });
}

const mergeFormData = (uuid, data) => {
  let body = {uuid, data};
  return new Promise(resolve => {
    try {
      AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/asforms/data/merge", res => {
        resolve(res);
      }, null, JSON.stringify(body), "application/json; charset=utf-8", err => {
        resolve(err.responseJSON);
      });
    } catch (e) {
      resolve(null);
    }
  });
}

const animationClass = [
  'uk-animation-shake'
];

pageHandler('contest_19', () => {
  if(['Unknown', 'bbee3615-9c7b-4493-9179-2b1df91634da'].indexOf(AS.OPTIONS.currentUser.userid) !== -1) {
    fire(eventShow, 'panel-random');

    let rangeStartInput = $('#range_start');
    let rangeStopInput = $('#range_stop');
    let buttonQuantity = $('#button_quantity');
    let winner = false;
    let randItems = [];
    let winnerNumber = 1;

    $('#winner_user span').empty();

    function selectWinner(){
      let randomItem = getRandomValue(rangeStartInput.val(), rangeStopInput.val());
      if(randItems.indexOf(randomItem) !== -1) {
        selectWinner();
      } else {
        randItems.push(randomItem);
        searchInRegistry(0, randomItem).then(res => {
          if(res.recordsCount == 0) {
            selectWinner();
          } else {
            winner = true;
            let userFIO = res.result[0].fieldValue.textbox_name || 'Unknown';
            let userNumber = parseInt(res.result[0].fieldValue.numericinput_number) || '';

            $('#participant_count span').empty()
            .append(`<div class="${animationClass[0]}">${randomItem}</div>`);

            fire(eventShow, 'panel_for_winner');
            $('#winner_user span').append(`<div style="margin-top: 20px;">${winnerNumber}) ${userNumber} ${userFIO}</div>`);

            winnerNumber++;

            activateDoc(res.result[0].dataUUID).then(activateResult => {
              buttonQuantity.removeClass('uk-disabled');
              buttonQuantity.css('opacity', '1');
            });
          }
        });
      }
    }

    buttonQuantity.addClass('uk-disabled');
    buttonQuantity.css('opacity', '0.3');

    rangeStartInput.off().on('input', e => {
      rangeStartInput.val(rangeStartInput.val().replace(/[^0-9]/g, ''));
      if(rangeStartInput.val() != '' && rangeStopInput.val() != '') {
        buttonQuantity.removeClass('uk-disabled');
        buttonQuantity.css('opacity', '1');
      } else {
        if(!buttonQuantity.hasClass('uk-disabled')) {
          buttonQuantity.addClass('uk-disabled');
          buttonQuantity.css('opacity', '0.3');
        }
      }
    });

    rangeStopInput.off().on('input', e => {
      rangeStopInput.val(rangeStopInput.val().replace(/[^0-9]/g, ''));
      if(rangeStartInput.val() != '' && rangeStopInput.val() != '') {
        buttonQuantity.removeClass('uk-disabled');
        buttonQuantity.css('opacity', '1');
      } else {
        if(!buttonQuantity.hasClass('uk-disabled')) {
          buttonQuantity.addClass('uk-disabled');
          buttonQuantity.css('opacity', '0.3');
        }
      }
    });

    buttonQuantity.off().on('click', e => {
      e.preventDefault;
      e.target.blur();

      buttonQuantity.addClass('uk-disabled');
      buttonQuantity.css('opacity', '0.3');

      let timerId = setInterval(() => {
        if(winner) {
          clearInterval(timerId);
          winner = false;
        } else {
          $('#participant_count span').empty()
          .append(`<div class="${animationClass[0]} uk-animation-fast">${getRandomValue(rangeStartInput.val(), rangeStopInput.val())}</div>`);
        }
      }, 100);

      setTimeout(() => {
        selectWinner();
      }, getRandomValue(15000, 20000));

    });
  }

});
