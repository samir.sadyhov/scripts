const buttonText = {
  ru: 'Обратная связь',
  kk: 'Кері байланыс',
  en: ''
}
let button = $('.email-bt[type="button"]');
let textCall = $('<div class="text-call">');
let buttonName = $('<span>');

textCall.append(buttonName);
button.append(textCall);

button.attr('title', buttonText[AS.OPTIONS.locale]);
buttonName.html(buttonText[AS.OPTIONS.locale].replace(' ', '<br>'));

button.on('click', e => {
  e.preventDefault();
  e.target.blur();
  fire({type: 'set_hidden', hidden: false}, 'panel_feedback');
});


function localeClickerFeedback() {
  button.attr('title', buttonText[AS.OPTIONS.locale]);
  buttonName.html(buttonText[AS.OPTIONS.locale].replace(' ', '<br>'));
}

Cons.setAppStore({
  localeClickerFeedback: {
    localeClickerFeedback
  }
});
