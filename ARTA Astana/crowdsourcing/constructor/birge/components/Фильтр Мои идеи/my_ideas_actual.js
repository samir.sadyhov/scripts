const container = $('#' + comp.code);

let glob_locale_array_index = 0;
const glob_locale_array = ["Данные успешно доработаны", "Деректер сәтті толықтырылды",
    "Работа успешно завершена", "Жұмыс сәтті аяқталды",
    "Возникли неполадки при завершение работы", "Жұмысты аяқтау кезінде ақаулар пайда болды",
    "Найдено более 1 незавершенной работы, невозможно определить какую завершить", "1-ден астам аяқталмаған жұмыс табылды, қайсысын аяқтау керектігін анықтау мүмкін емес",
    "Ошибка получения хода выполнения работы", "Жұмыс барысын алу қателігі",
    "Не получен идентификатор работы для ее завершения", "Жұмысты аяқтауға қажетті жумыс идентификаторы алынбады",
    "Ознакомиться", "Танысу",
    "Доработать", "Толықтыру",
    "Ошибка создания кнопки перехода", "Шарлау түймесін жасау кезінде қате пайда болды",
    "Проблема получения авторизационных данных пользователя, обратитесь к разработчику", "Пайдаланушының авторизациясы туралы деректерді алу кезінде қиындық туындады, администратормен байланысыңыз",
    "Произошла ошибка при сохранение данных", "Деректерді сақтау кезінде қате пайда болды",
    "Не все поля формы были заполнены. Пожалуйста заполните все поля формы", "Форманың барлық өрістері толтырылмаған. Барлық өрістерді толтыруыңызды өтінеміз."];
const tableContainer = container.find('table').parent();

const body = container.find('tbody');

const buildBodyExt = (url, paramsObj) => {
    for (let i in paramsObj) {
        if (paramsObj[i]) {
            url += `&${i}=${paramsObj[i]}`;
        }
    }

    return url;
};

const clearBody = () => body.empty();

let globalIdeaStatus = "";
let globalUserID = "";


// 1) добавил дополнительный параметр в "Принимаемые параметры" страницы Подробнее о проекте
// 2) добавил дополнительный передаваемый параметр в кейс кнопки "button_show_more" на странице "Главная страница"
// 3) Добавил в ресурсы в скрипт passport.js скрипт ниже
// 4) Проблемы: арта все еще не решила баг со множественным созданием listenerov из за чего создается множественное количество слушателей // TODO in future

//////////////////////////////////////////////////////////////////////////////////////////

let getMyIdeas = async function (userID, locale) {
    return new Promise(async function (resolve) {
        try {
            let url = [];

            url.push('fields=entity_author');
            url.push('fields=listbox_status_idea');
            url.push('fields=reglink_project');
            url.push('fields=textbox_idea_name');
            url.push('fields=numericinput_voted_count');
            url.push('fields=idea_status');
            url.push('field=entity_author');
            url.push('condition=TEXT_EQUALS');
            url.push(`key=${userID}`);
            url.push('locale=' + locale);

            $.ajax({
                "url": window.origin + "/Synergy/rest/api/registry/data_ext?" + buildBodyExt(url.join('&'), {
                    registryCode: "registry_idea"
                }),
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "Authorization": ("Basic " + btoa(Cons.creds.login + ":" + Cons.creds.password))
                    //"Authorization": ("Basic MTox")
                },
            }).done(function (response) {
                resolve(response);
                console.log(response);
            });
        } catch (err) {
            resolve({
                result: [],
                recordsCount: 0
            });
        }
    });
}

const getAuthPerson = async function () {
    return new Promise(async function (resolve) {
        try {
            $.ajax({
                "url": window.origin + "/Synergy/rest/api/person/auth?getGroups=false",
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "Authorization": ("Basic " + btoa(Cons.creds.login + ":" + Cons.creds.password))
                },
            }).done(function (response) {
                resolve(response);
            });
        } catch (err) {
            resolve({
                userid: ""
            });
        }
    });
}

const getFormJson = async function (dataUUID) {
    return new Promise(async function (resolve) {
        try {
            $.ajax({
                "url": window.origin + "/Synergy/rest/api/asforms/data/" + dataUUID,
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "Authorization": ("Basic " + btoa(Cons.creds.login + ":" + Cons.creds.password))
                },
            }).done(function (response) {
                resolve(response);
            });
        } catch (err) {
            resolve({
                data: [],
                uuid: dataUUID
            });
        }
    });
}

function parseActionsList(actionsList) {
    let actionId = [];
    console.log(actionsList);
    if (actionsList) {
        for (let i = 0; i < actionsList.length; i++) {
            if (actionsList[i].typeID == "ASSIGNMENT_ITEM" && !actionsList[i].finished) {
                actionId.push(actionsList[i].actionID);
            }
        }
    }

    return actionId;
}


const finishWork = async function (dataUUID) {
    return new Promise(async function (resolve) {
        AS.SERVICES.showWaitWindow();
        getDocumentIdentifierByDataUUID(dataUUID).then(function (documentID) {
            if (documentID) {
                getExecutionProcess(documentID).then(function (actionsList) {
                    let actionId = parseActionsList(actionsList);
                    if (actionId.length == 1) {
                        setWorkProgress(actionId[0], "100").then(function (response) {
                            if (response.errorCode == "0") {
                                resolve("success");
                                showMessage(glob_locale_array[2 + glob_locale_array_index], "success");
                            } else {
                                showMessage(glob_locale_array[4 + glob_locale_array_index], "error");
                            }
                            AS.SERVICES.hideWaitWindow();
                        });
                    } else if (actionId.length > 1) {
                        AS.SERVICES.hideWaitWindow();
                        showMessage(glob_locale_array[6 + glob_locale_array_index], 'info');
                    } else {
                        AS.SERVICES.hideWaitWindow();
                        showMessage(glob_locale_array[8 + glob_locale_array_index], 'error');
                    }
                });
            } else {
                AS.SERVICES.hideWaitWindow();
                showMessage(glob_locale_array[10 + glob_locale_array_index], 'error');
            }
        });
    })
}

const getDocumentIdentifierByDataUUID = async function (dataUUID) {
    return new Promise(async function (resolve) {
        try {
            $.ajax({
                "url": window.origin + "/Synergy/rest/api/formPlayer/documentIdentifier?dataUUID=" + dataUUID,
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "Authorization": ("Basic " + btoa(Cons.creds.login + ":" + Cons.creds.password))
                },
            }).done(function (response) {
                resolve(response);
            });
        } catch (err) {
            resolve("");
        }
    });
}

const getExecutionProcess = async function (documentID) {
    return new Promise(async function (resolve) {
        try {
            $.ajax({
                "url": window.origin + "/Synergy/rest/api/workflow/get_execution_process?documentID=" + documentID,
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "Authorization": ("Basic " + btoa(Cons.creds.login + ":" + Cons.creds.password))
                },
            }).done(function (response) {
                resolve(response);
            });
        } catch (err) {
            resolve("");
        }
    });
}

const setWorkProgress = async function (workID, progress) {
    return new Promise(async function (resolve) {
        try {
            $.ajax({
                "url": window.origin + "/Synergy/rest/api/workflow/work/set_progress?workID=" + workID + "&progress=" + progress,
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "Authorization": ("Basic " + btoa(Cons.creds.login + ":" + Cons.creds.password))
                },
            }).done(function (response) {
                resolve(response);
            });
        } catch (err) {
            resolve({
                errorCode: "100"
            });
        }
    });
}

const setTableRows = Rows => {
    clearBody();
    let tr;
    let th;
    let button;
    //Rows.sort((a, b) => b.numericinput_voted_count - a.numericinput_voted_count);
    Rows.forEach(x => {
        tr = $('<tr>');
        th = $('<td>');
        th.addClass('uk-cursor-pointer');
        th.text(x.entity_author);
        th.css("text-align", "center");
        tr.append(th);

        th = $('<td>');
        th.addClass('uk-cursor-pointer');
        th.text(x.listbox_status_idea);
        th.css("text-align", "center");
        tr.append(th);

        th = $('<td>');
        th.addClass('uk-cursor-pointer');
        th.text(x.reglink_project);
        th.css("text-align", "center");
        tr.append(th);

        th = $('<td>');
        th.addClass('uk-cursor-pointer');
        th.text(x.textbox_idea_name);
        th.css("text-align", "center");
        tr.append(th);

        th = $('<td>');
        th.addClass('uk-cursor-pointer');
        if (x.numericinput_voted_count) {
            th.text(x.numericinput_voted_count);
        } else {
            th.text("__");
        }
        th.css("text-align", "center");
        tr.append(th);

        th = $('<td>');
        th.addClass('uk-cursor-pointer');
        try {
            button = $('<button>');
            button.addClass('uk-button margined uk-button-primary ideas-form crowdsourcing-button');
            button.attr({ "dataUUID": x.dataUUID });
            button.attr({ "idea_status": x.idea_status });

            if (x.idea_status != "2") {
                button.text(glob_locale_array[12 + glob_locale_array_index]);
            } else {
                button.text(glob_locale_array[14 + glob_locale_array_index]);
            }
            th.append(button);
        } catch (err) {
            showMessage(glob_locale_array[16 + glob_locale_array_index], 'error');
        }
        th.css("text-align", "center");
        tr.append(th);
        body.append(tr);
    });

    $(".ideas-form").off();
    $(".ideas-form").click(async function () {
        fire(event = {
            type: 'set_hidden',
            hidden: false
        }, 'panel_modal');
        let dataUUID = $(this).attr("dataUUID");
        let idea_status = $(this).attr("idea_status");
        globalIdeaStatus = idea_status;
        setTimeout(function () {
            fire({
                type: 'show_form_data',
                dataId: dataUUID,
            }, 'formPlayer_ideas');

            //changeHeaderText("Просмотр идеи");

            fire(event = {
                type: 'set_hidden',
                hidden: false
            }, 'formPlayer_ideas');

            if (idea_status != "2") {
                fire(event = {
                    type: 'set_hidden',
                    hidden: true
                }, 'button_send');

                fire({
                    type: 'set_form_editable',
                    editable: false,
                }, 'formPlayer_ideas');
            } else {
                fire(event = {
                    type: 'set_hidden',
                    hidden: false
                }, 'button_send');

                fire({
                    type: 'set_form_editable',
                    editable: true,
                }, 'formPlayer_ideas');
            }
        }, 600);

    });
};

function showPanels(show, labelText) {
  if($('#panel-date-voting').length) fire({type: 'set_hidden', hidden: !show}, 'panel-date-voting');
  if($('#panel-ideas-list').length) fire({type: 'set_hidden', hidden: !show}, 'panel-ideas-list');
  if($('#panel-suggest-project').length) fire({type: 'set_hidden', hidden: show}, 'panel-suggest-project');
  if($('#label-date-voting').length) fire({type: 'change_label', text: localizedText(labelText, labelText, labelText, labelText)}, 'label-date-voting');
}

function setMyIdeasTable(locale) {
    getAuthPerson().then(function (response) {
        if (response.userid) {
            globalUserID = response.userid
            getMyIdeas(response.userid, locale).then(function (myIdeasList) {
                let rows = [];
                for (let i = 0; i < myIdeasList.result.length; i++) {
                    if (myIdeasList.result[i] && myIdeasList.result[i].fieldValue) {
                        rows.push({
                            "entity_author": myIdeasList.result[i].fieldValue.entity_author,
                            "listbox_status_idea": myIdeasList.result[i].fieldValue.listbox_status_idea,
                            "reglink_project": myIdeasList.result[i].fieldValue.reglink_project,
                            "textbox_idea_name": myIdeasList.result[i].fieldValue.textbox_idea_name,
                            "numericinput_voted_count": +(myIdeasList.result[i].fieldValue.numericinput_voted_count),
                            "dataUUID": myIdeasList.result[i].dataUUID,
                            "idea_status": myIdeasList.result[i].fieldKey.listbox_status_idea
                        });
                    }
                }
                setTableRows(rows);
            });
        } else {
            showMessage(glob_locale_array[18 + glob_locale_array_index], error);
        }
    });
}

addListener('loaded_form_data', 'formPlayer_ideas', event => {
    const playerModel = event.model;
    const playerView = event.view;
    const dataUUID = event.model.asfDataId;

    if (globalIdeaStatus == "2") {
        playerView.getViewWithId("textarea_revision").setEnabled(false);
    }

    $('#button_send').off();
    $('#button_send').on('click', () => {
        if (!playerModel.getErrors().length) {
            fire({
                type: 'save_form_data',
                success: (dataId, documentId) => {
                    showMessage(glob_locale_array[0 + glob_locale_array_index], 'info', 3000);
                    finishWork(dataUUID).then(function () {
                        fire(event = {
                            type: 'set_hidden',
                            hidden: true
                        }, 'panel_modal');

                        setTimeout(setMyIdeasTable, 1500);
                    });
                },
                error: (status, error) => { showMessage(glob_locale_array[20 + glob_locale_array_index], 'info', 3000); }
            }, 'formPlayer_ideas');

        } else {
            showMessage(glob_locale_array[22 + glob_locale_array_index], "error");
        }
    });
});



if (glob_locale_array_index == 0) {
    setMyIdeasTable("ru");
} else {
    setMyIdeasTable("kz");
}

// function updateTable() {
//     let timerFix = setInterval(() => {
//         const store = Cons.getAppStore();
//         if (store && store.passport && store.passport.params && store.passport.params.docID && store.passport.params.dataUUID) {
//             clearInterval(timerFix);
//             let documentId = store.passport.params.docID;
//             let dataUUID = store.passport.params.dataUUID;
//             if (documentId && documentId[0] && documentId[0].value && dataUUID && dataUUID[0] && dataUUID[0].value) {
//                 setMyIdeasTable();
//             }
//         }
//     }, 600);
// }


// changeLabels depending on local-component
checkIfExists(0);
function checkIfExists(index) {
    if (index == 10) {
        alert("error happened no localeSelector component. Fix it");
        return;
    }
    //console.log("entered");
    //console.log("entered2");
    //console.log($("#localeSelector"));
    //$("#localeSelector").off();
    function localeClickerMyIdeas(value) {
        if (value == "kk") {
            global_locale_value = value;
            setElementsVisible(false);
        } else if (value == "ru") {
            global_locale_value = value;
            setElementsVisible(true);
        }
    }

    Cons.setAppStore({
        localeClickerMyIdeas: {
            localeClickerMyIdeas
        }
    });

    function setElementsVisible(hide) {
        let arr;
        if (hide) {
            arr = ["Автор идеи", "Статус", "Проект", "Наименование идеи", "Количество проголосовавших"];
            glob_locale_array_index = 0;
        } else {
            arr = ["Идея авторы", "Статус", "ЖОБАЛАР", "ИДЕЯНЫҢ АТАУЫ", "ДАУЫС БЕРГЕНДЕР САНЫ"];
            glob_locale_array_index = 1;
        }

        let header_children = $(".label-class").children();
        for (let i = 0; i < header_children.length; i++) {
            if (arr[i])
                $(header_children[i]).text(arr[i]);
        }
        if (hide) {
            setMyIdeasTable("ru");
        } else {
            setMyIdeasTable("kz");
        }
    }
}
