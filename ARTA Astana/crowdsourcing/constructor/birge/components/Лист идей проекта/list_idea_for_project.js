const container = $('#' + comp.code);

const tableContainer = container.find('table').parent();

let glob_locale_array_index = 0;
const glob_locale_array = ["Вы уже проголосовали за эту Идею", "Сіз бұл идеяға дауыс бердіңіз",
    "Ваш голос принят!", "Сіздің дауысыңыз қабылданды!",
    "Произошла ошибка при сохранение данных попробуйте позже", "Деректерді сақтау кезінде қате пайда болды, кейінірек қайталап көріңіз",
    "Вы уже проголосовали за эту Идею", "Сіз бұл идеяға дауыс бердіңіз",
    "Нельзя проголосовать от имени гостевого пользователя", "Қонақ пайдаланушының атынан дауыс беруге болмайды",
    "Ознакомиться", "Танысу",
    "Ошибка создание кнопки 'Ознакомиться'", "'Танысу' батырмасын құру қатесі",
    "Просмотр идеи", "Идеямен танысу",
    "Срок принятие голосов до ", "Дауыстарды қабылдаудың соңғы мерзімі",
    "дата не указана в проекте", "жобада дауыс беру күні көрсетілмеген",
    "Голосование завершено. Проект и предложенные идеи утверждены", "Дауыс беру аяқталды. Жоба мен ұсынылған идеялар мақұлданды"];

const body = container.find('tbody');
let subDivisionNameList = new Set();
let usedSubString = [];

const buildBodyExt = (url, paramsObj) => {
    for (let i in paramsObj) {
        if (paramsObj[i]) {
            url += `&${i}=${paramsObj[i]}`;
        }
    }

    return url;
};

const clearBody = () => body.empty();


// 1) добавил дополнительный параметр в "Принимаемые параметры" страницы Подробнее о проекте
// 2) добавил дополнительный передаваемый параметр в кейс кнопки "button_show_more" на странице "Главная страница"
// 3) Добавил в ресурсы в скрипт passport.js скрипт ниже
// 4) Проблемы: арта все еще не решила баг со множественным созданием listenerov из за чего создается множественное количество слушателей // TODO in future

let documentIDCode = getCurrentPage().params.filter(x => x.name == "documentID");
let dataUUIDCode = getCurrentPage().params.filter(x => x.name == "dataUUID");
if (documentIDCode && documentIDCode[0] && documentIDCode[0].id && dataUUIDCode && dataUUIDCode[0] && dataUUIDCode[0].id) {
    let params = { docID: Cons.getCurrentPage().paramValues.filter(x => x.pageParamID == documentIDCode[0].id), dataUUID: Cons.getCurrentPage().paramValues.filter(x => x.pageParamID == dataUUIDCode[0].id) };
    if (params && params.docID && params.dataUUID && params.docID.length && params.dataUUID.length) {
        Cons.setAppStore({
            passport: {
                params
            }
        });

    }

}

$("#button_suggest_idea").on('click', function () {
    setTimeout(function () {

        changeHeaderText("Предложить идею");
        if ($("#formPlayer_idea").length) {
            fire(event = {
                type: 'set_hidden',
                hidden: false
            }, 'formPlayer_idea');
            let timerFix = setInterval(() => {
                if ($("#button_send_idea").length) {
                    fire(event = {
                        type: 'set_hidden',
                        hidden: false
                    }, 'button_send_idea');
                    clearInterval(timerFix);
                }
            }, 100);

        }
    }, 600);
});

addListener('loaded_form_data', 'formPlayer_idea', event => {
    const playerModel = event.model;
    const dataUUID = event.model.asfDataId;

    let documentId = Cons.getAppStore().passport.params.docID;
    if (documentId && documentId[0] && documentId[0].value) {
        playerModel.getModelWithId("reglink_project").setValue(documentId[0].value);
    }

    let entity_voted = playerModel.getModelWithId("entity_voted").getValue();
    let listbox_status_idea = playerModel.getModelWithId("listbox_status_idea").getValue();

    let containsUserAsVoted = false;
    if (listbox_status_idea && listbox_status_idea[0] && (listbox_status_idea[0] == "5")) {
        getAuthPerson().then(function (userData) {
            if (userData.userid) {
                let containsUserAsVoted = entity_voted.find(x => x.personID == userData.userid);

                //console.log(containsUserAsVoted);
                //console.log(Cons.getAppStore().projectStatus.status);
                //console.log(userData.userid);

                if (containsUserAsVoted || Cons.getAppStore().projectStatus.status == "voting_finish") {
                    if (Cons.getAppStore().projectStatus.status == "voting_finish") {
                        fire(event = {
                            type: 'set_hidden',
                            hidden: true
                        }, 'button_vote_idea');
                    } else {
                        $("#button_vote_idea").off();
                        $("#button_vote_idea").click(function () {
                            showMessage(glob_locale_array[0 + glob_locale_array_index], 'info', 3000);
                        });
                    }
                } else if (userData.userid != "1") {
                    fire(event = {
                        type: 'set_hidden',
                        hidden: false
                    }, 'button_vote_idea');

                    $("#button_vote_idea").off();
                    $("#button_vote_idea").click(function () {
                        AS.SERVICES.showWaitWindow();
                        getFormJson(dataUUID).then(function (formData) {
                            formData = formData.data;
                            let entity_voted = formData.filter(x => x.id == "entity_voted");
                            let voteAmount = formData.filter(x => x.id == "numericinput_voted_count");

                            if (!entity_voted[0] || !entity_voted[0].key || !entity_voted[0].key.includes(userData.userid)) {
                                let entity_voted_key = [];
                                let entity_voted_value = [];
                                if (entity_voted && entity_voted[0] && entity_voted[0].key && entity_voted[0].value) {
                                    entity_voted_key = entity_voted[0].key.split(";");
                                    entity_voted_value = entity_voted[0].value.split(",");
                                }

                                entity_voted_key[entity_voted_key.length] = userData.userid;
                                let personName = "";
                                if (userData.lastname) {
                                    personName = personName + userData.lastname;
                                }

                                if (userData.firstname) {
                                    personName = personName + " " + userData.firstname[0] + ".";
                                }

                                if (userData.patronymic) {
                                    personName = personName + " " + userData.patronymic[0] + ".";
                                }
                                entity_voted_value[entity_voted_value.length] = personName;
                                let userModel = [];
                                for (let i = 0; i < entity_voted_key.length; i++) {
                                    userModel.push({ "personID": entity_voted_key[i], "personName": entity_voted_value[i] });
                                }


                                let votesAmount = 1;
                                if (voteAmount && voteAmount[0] && voteAmount[0].key) {
                                    votesAmount = (+voteAmount[0].key) + 1;
                                }

                                playerModel.getModelWithId("entity_voted").setValue(userModel);
                                playerModel.getModelWithId("numericinput_voted_count").setValue(votesAmount + "");

                                fire({
                                    type: 'save_form_data',
                                    success: (dataId, documentId) => {
                                        showMessage(glob_locale_array[2 + glob_locale_array_index], 'info', 3000);
                                        updateTable();
                                        fire(event = {
                                            type: 'set_hidden',
                                            hidden: true
                                        }, 'panel_modal');
                                    },
                                    error: (status, error) => { showMessage(glob_locale_array[4 + glob_locale_array_index], 'info', 3000); }
                                }, 'formPlayer_idea');
                            } else {
                                showMessage(glob_locale_array[6 + glob_locale_array_index], 'info', 3000);
                            }
                            AS.SERVICES.hideWaitWindow();
                        });
                    });
                } else {
                    fire(event = {
                        type: 'set_hidden',
                        hidden: false
                    }, 'button_vote_idea');

                    $("#button_vote_idea").off();
                    $("#button_vote_idea").click(function () {
                        showMessage(glob_locale_array[8 + glob_locale_array_index], 'error', 3000);
                    });
                }
            }
        });
    } else {
        fire(event = {
            type: 'set_hidden',
            hidden: true
        }, 'button_vote_idea');
    }
});

function changeHeaderText(headerLabel) {
    $(".uk-modal-title").text(headerLabel);
}

//////////////////////////////////////////////////////////////////////////////////////////

const getProjectIdeas = async function (documentID, status, locale) {
    return new Promise(async function (resolve) {
        try {
            let url = [];

            url.push('fields=date_send_idea');
            url.push('fields=textbox_idea_name');
            url.push('fields=numericinput_voted_count');
            url.push('fields=listbox_status_idea');
            url.push('field=reglink_project');
            url.push('condition=TEXT_EQUALS');
            url.push(`key=${documentID}`);
            url.push('locale=' + locale);
            let statusArray = ["5", "6", "7"];
            (statusArray || []).forEach((status, key) => {
                url.push(`field1=listbox_status_idea&condition1=EQUALS&key1=${status}`);

                if (key < (statusArray || []).length - 1) {
                    url.push(`term1=or`);
                }
            });

            $.ajax({
                "url": window.origin + "/Synergy/rest/api/registry/data_ext?" + buildBodyExt(url.join('&'), {
                    registryCode: "registry_idea"
                }),
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "Authorization": ("Basic " + btoa(Cons.creds.login + ":" + Cons.creds.password))
                },
            }).done(function (response) {
                resolve(response);
            });
        } catch (err) {
            resolve({
                result: [],
                recordsCount: 0
            });
        }
    });
}

const getAuthPerson = async function () {
    return new Promise(async function (resolve) {
        try {
            $.ajax({
                "url": window.origin + "/Synergy/rest/api/person/auth?getGroups=false",
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "Authorization": ("Basic " + btoa(Cons.creds.login + ":" + Cons.creds.password))
                },
            }).done(function (response) {
                resolve(response);
            });
        } catch (err) {
            resolve({
                userid: ""
            });
        }
    });
}

const getFormJson = async function (dataUUID) {
    return new Promise(async function (resolve) {
        try {
            $.ajax({
                "url": window.origin + "/Synergy/rest/api/asforms/data/" + dataUUID,
                "method": "GET",
                "timeout": 0,
                "headers": {
                    "Authorization": ("Basic " + btoa(Cons.creds.login + ":" + Cons.creds.password))
                },
            }).done(function (response) {
                resolve(response);
            });
        } catch (err) {
            resolve({
                data: [],
                uuid: dataUUID
            });
        }
    });
}

const setTableRows = Rows => {
    clearBody();
    let tr;
    let th;
    let button;
    Rows.sort((a, b) => b.numericinput_voted_count - a.numericinput_voted_count);
    console.log(Rows);
    console.log(Rows.length);

    // if (Rows.length < 5) {
    //     tableContainer.css('height', 'calc(100vh - 500px)');
    // } else if (Rows.length < 10) {
    //     tableContainer.css('height', 'calc(100vh - 400px)');
    // }else{
    //     tableContainer.css('height', 'calc(100vh - 200px)');
    // }

    Rows.forEach(x => {
        tr = $('<tr>');
        th = $('<td>');
        th.addClass('uk-cursor-pointer');
        th.text(x.date_send_idea);
        th.css("text-align", "center");
        tr.append(th);

        th = $('<td>');
        th.addClass('uk-cursor-pointer');
        th.text(x.textbox_idea_name);
        th.css("text-align", "center");
        tr.append(th);

        th = $('<td>');
        th.addClass('uk-cursor-pointer');
        if (x.numericinput_voted_count) {
            th.text(x.numericinput_voted_count);
        } else {
            th.text("__");
        }
        th.css("text-align", "center");
        tr.append(th);

        th = $('<td>');
        th.addClass('uk-cursor-pointer');
        try {
            button = $('<button>');
            button.text(glob_locale_array[10 + glob_locale_array_index]);
            button.addClass('crowdsourcing-button uk-button margined uk-button-primary ideas-form');
            button.attr({ "dataUUID": x.dataUUID });
            th.append(button);
        } catch (err) {
            //th.text(x.crm_form_deal_main_budget_0);
            showMessage(glob_locale_array[12 + glob_locale_array_index], info);
        }
        th.css("text-align", "center");
        tr.append(th);
        body.append(tr);
    });

    $(".ideas-form").off();
    $(".ideas-form").click(async function () {
        fire(event = {
            type: 'set_hidden',
            hidden: false
        }, 'panel_modal');
        let dataUUID = $(this).attr("dataUUID");

        let timerFix = setInterval(() => {
            if ($("#formPlayer_idea").length) {
                fire({
                    type: 'show_form_data',
                    dataId: dataUUID,
                }, 'formPlayer_idea');

                changeHeaderText(glob_locale_array[14 + glob_locale_array_index]);

                fire(event = {
                    type: 'set_hidden',
                    hidden: true
                }, 'button_send_idea');

                fire({
                    type: 'set_form_editable',
                    editable: false,
                }, 'formPlayer_idea');
                clearInterval(timerFix);
                setTimeout(function () {
                    fire(event = {
                        type: 'set_hidden',
                        hidden: false
                    }, 'formPlayer_idea');
                }, 200)
            }
        }, 100);

    });
};

const getProjetcStatus = async function (dataUUID, documentId, locale) {
    getFormJson(dataUUID).then(function (formData) {
        let projectStatus = formData.data.filter(x => x.id == "status");

        Cons.setAppStore({
            projectStatus: {
                status: projectStatus[0].key
            }
        });
        if (projectStatus && projectStatus[0] && projectStatus[0].key && (projectStatus[0].key == "on_voting" || projectStatus[0].key == "approved" || projectStatus[0].key == "voting_finish")) {

            getProjectIdeas(documentId, 5, locale).then(function (dataExt) {

                let rows = [];
                for (let i = 0; i < dataExt.result.length; i++) {
                    if (dataExt.result[i] && dataExt.result[i].fieldValue) {
                        rows.push({
                            "date_send_idea": dataExt.result[i].fieldValue.date_send_idea,
                            "textbox_idea_name": dataExt.result[i].fieldValue.textbox_idea_name,
                            "numericinput_voted_count": +(dataExt.result[i].fieldValue.numericinput_voted_count),
                            "dataUUID": dataExt.result[i].dataUUID,
                            "status": projectStatus[0].key
                        });
                    }
                }

                setTableRows(rows);
            });

            let data_voting_ideas = formData.data.filter(x => x.id == "date_voting_ideas");
            let labelText = glob_locale_array[16 + glob_locale_array_index];
            if (projectStatus[0].key == "on_voting") {
                if (data_voting_ideas && data_voting_ideas[0] && data_voting_ideas[0].value) {
                    labelText = labelText + data_voting_ideas[0].value;
                } else {
                    labelText = labelText + glob_locale_array[18 + glob_locale_array_index];
                }
            } else if (projectStatus[0].key == "approved" || projectStatus[0].key == "voting_finish") {
                labelText = glob_locale_array[20 + glob_locale_array_index];
            }
            showPanels(true, labelText);

        } else {
            showPanels(false, "");
        }
    });
}

function showPanels(show, labelText) {
  if($('#panel-date-voting').length) fire({type: 'set_hidden', hidden: !show}, 'panel-date-voting');
  if($('#panel-ideas-list').length) fire({type: 'set_hidden', hidden: !show}, 'panel-ideas-list');
  if($('#panel-suggest-project').length) fire({type: 'set_hidden', hidden: show}, 'panel-suggest-project');
  if($('#label-date-voting').length) fire({type: 'change_label', text: localizedText(labelText, labelText, labelText, labelText)}, 'label-date-voting');
}

function updateTable(locale) {
    let timerFix = setInterval(() => {
        const store = Cons.getAppStore();
        if (store && store.passport && store.passport.params && store.passport.params.docID && store.passport.params.dataUUID) {
            clearInterval(timerFix);
            let documentId = store.passport.params.docID;
            let dataUUID = store.passport.params.dataUUID;
            if (documentId && documentId[0] && documentId[0].value && dataUUID && dataUUID[0] && dataUUID[0].value) {
                getProjetcStatus(dataUUID[0].value, documentId[0].value, locale);
            }
        }
    }, 600);
}


if (glob_locale_array_index == 0) {
    updateTable("ru");
} else {
    updateTable("kz");
}

checkIfExists(0);
function checkIfExists(index) {
    if (index == 10) {
        alert("error happened no localeSelector component. Fix it");
        return;
    }

    //$("#localeSelector").off();

    function localeClicker(value) {
        if (value == "kk") {
            global_locale_value = value;
            setElementsVisible(false);
        } else if (value == "ru") {
            global_locale_value = value;
            setElementsVisible(true);
        }
    }

    Cons.setAppStore({
        localeClicker: {
            localeClicker
        }
    });


    function setElementsVisible(hide) {
        let arr;
        if (hide) {
            arr = ["ДАТА ПОДАЧИ ИДЕИ", "НАИМЕНОВАНИЕ ИДЕИ", "КОЛИЧЕСТВО ГОЛОСОВ", "ДЕЙСТВИЕ"];
            glob_locale_array_index = 0;
        } else {
            arr = ["ИДЕЯНЫ ҰСЫНУ КҮНІ", "ИДЕЯНЫҢ АТАУЫ", "ДАУЫС САНЫ", "ӘРЕКЕТ"];
            glob_locale_array_index = 1;
        }

        let header_children = $(".label-class").children();
        for (let i = 0; i < header_children.length; i++) {
            if (arr[i])
                $(header_children[i]).text(arr[i]);
        }

        if (hide) {
            updateTable("ru");
        } else {
            updateTable("kz");
        }
    }
}
