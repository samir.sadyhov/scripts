$('body>#crowdsourcing-menu-container').remove();

const mobileMenuItems = [
  {
    name: {ru: 'Проекты и идеи', kk: 'Жобалар мен идеялар'},
    icon: 'album',
    pageCode: 'main_page'
  },
  {
    name: {ru: 'Идеи на реализации', kk: 'Іске асыру идеялары'},
    icon: 'clock',
    pageCode: 'ideas_for_implementation'
  },
  {
    name: {ru: 'Новости', kk: 'Жаңалықтар'},
    icon: 'rss',
    pageCode: 'news'
  },
  {
    name: {ru: 'О нас', kk: 'Біз туралы'},
    icon: 'info',
    pageCode: 'about_us'
  }
];

const getMenuItem = (name, icon, pageCode) => {
  let li = $('<li>');
  let menuItem = $(`<a href="javascript:void(0);"><span class="uk-margin-small-right" uk-icon="icon: ${icon}"></span>${name[AS.OPTIONS.locale]}</a>`);
  menuItem.on('click', e => {
    $('body>#crowdsourcing-menu-container').remove();
    fire({type: 'goto_page', pageCode: pageCode}, comp.code);
  });
  return li.append(menuItem);
}

const logout = () => {
  AS.apiAuth.setCredentials(null, null);
  AS.OPTIONS.currentUser = {};
  Cons.logout();
  window.location.reload();
}

const renderMenu = () => {
  let mobileMenuContent = $('.crowdsourcing-mobile-nav');
  mobileMenuContent.empty();

  let menu = $('<ul class="uk-nav-default" uk-nav>');
  mobileMenuContent.append(menu);

  mobileMenuItems.forEach(item => {
    menu.append(getMenuItem(item.name, item.icon, item.pageCode));
  });

  if(AS.OPTIONS.currentUser.hasOwnProperty('lastname') && AS.OPTIONS.currentUser.lastname !== "Lastname") {
    menu.append(getMenuItem({ru: 'Мои идеи', kk: 'Менің идеяларым'}, 'star', 'my_ideas'));

    let exitItem = $(`<a href="javascript:void(0);"><span class="uk-margin-small-right" uk-icon="icon: sign-out"></span>${AS.OPTIONS.locale == 'kk' ? 'Шығу' : 'Выйти'}</a>`);

    exitItem.on('click', e => {
      e.preventDefault();
      e.target.blur();
      logout();
      fire({type: 'goto_page', pageCode: 'main_page'}, comp.code);
    });

    menu.append('<li class="uk-nav-divider"></li>').append($('<li>').append(exitItem));
  } else {
    menu.append('<li class="uk-nav-divider"></li>')
    .append(getMenuItem({ru: 'Войти', kk: 'Кіру'}, 'sign-in', 'auth'));
  }

}

setTimeout(() => {renderMenu()}, 500);

function localeClickerMobile() {
  renderMenu();
}

Cons.setAppStore({
  localeClickerMobile: {
    localeClickerMobile
  }
});
