//если поле не заполнено - выделить его как невалидное
const emptyValidator = (input) => {
  if (input && input.text && input.text !== '') {
    fire({type: 'input_highlight', error: false}, input.code);
    return true;
  } else {
    fire({type: 'input_highlight', error: true}, input.code);
    return false;
  }
}

//валидация введенного email
const emailValidator = (input) => {
  if (emptyValidator(input)) {
    let regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
    if (input && regex.test(input.text)) {
      fire({type: 'input_highlight', error: false}, input.code);
      return true;
    } else {
      fire({type: 'input_highlight', error: true}, input.code);
      return false;
    }
  }
  return false;
}

//метод поиска юзера, кастомное апи
const searchUser = async (email) => {
  const res = await fetch(`${window.origin}/crowdsourcing/rest/api/user/search?email=${email}`, {
    headers: {
      'Authorization': "Basic " + btoa(Cons.creds.login + ":" + Cons.creds.password)
    }
  });
  return await res.json();
}

const transliterate = text => {
  return text
    .replace(/\u0401/g, 'YO').replace(/\u0419/g, 'I').replace(/\u0426/g, 'TS')
    .replace(/\u0423/g, 'U').replace(/\u041A/g, 'K').replace(/\u0415/g, 'E')
    .replace(/\u041D/g, 'N').replace(/\u0413/g, 'G').replace(/\u0428/g, 'SH')
    .replace(/\u0429/g, 'SCH').replace(/\u0417/g, 'Z').replace(/\u0425/g, 'H')
    .replace(/\u042A/g, '').replace(/\u0451/g, 'yo').replace(/\u0439/g, 'i')
    .replace(/\u0446/g, 'ts').replace(/\u0443/g, 'u').replace(/\u043A/g, 'k')
    .replace(/\u0435/g, 'e').replace(/\u043D/g, 'n').replace(/\u0433/g, 'g')
    .replace(/\u0448/g, 'sh').replace(/\u0449/g, 'sch').replace(/\u0437/g, 'z')
    .replace(/\u0445/g, 'h').replace(/\u044A/g, "'").replace(/\u0424/g, 'F')
    .replace(/\u042B/g, 'I').replace(/\u0412/g, 'V').replace(/\u0410/g, 'a')
    .replace(/\u041F/g, 'P').replace(/\u0420/g, 'R').replace(/\u041E/g, 'O')
    .replace(/\u041B/g, 'L').replace(/\u0414/g, 'D').replace(/\u0416/g, 'ZH')
    .replace(/\u042D/g, 'E').replace(/\u0444/g, 'f').replace(/\u044B/g, 'i')
    .replace(/\u0432/g, 'v').replace(/\u0430/g, 'a').replace(/\u043F/g, 'p')
    .replace(/\u0440/g, 'r').replace(/\u043E/g, 'o').replace(/\u043B/g, 'l')
    .replace(/\u0434/g, 'd').replace(/\u0436/g, 'zh').replace(/\u044D/g, 'e')
    .replace(/\u042F/g, 'Ya').replace(/\u0427/g, 'CH').replace(/\u0421/g, 'S')
    .replace(/\u041C/g, 'M').replace(/\u0418/g, 'I').replace(/\u0422/g, 'T')
    .replace(/\u042C/g, "'").replace(/\u0411/g, 'B').replace(/\u042E/g, 'YU')
    .replace(/\u044F/g, 'ya').replace(/\u0447/g, 'ch').replace(/\u0441/g, 's')
    .replace(/\u043C/g, 'm').replace(/\u0438/g, 'i').replace(/\u0442/g, 't')
    .replace(/\u044C/g, "'").replace(/\u0431/g, 'b').replace(/\u044E/g, 'yu');
}


// Выполняем скрипт только в странице регистрации
pageHandler('registration', () => {

  if(Cons.getAppStore().button_registration_listener) return;
  Cons.setAppStore({button_registration_listener: true});

  // Выполняем действие только при нажатии на кнопку register
  addListener('button_click', 'register', () => {

    // Получаем все нужные компоненты
    let fname = getCompByCode('fname');
    let lname = getCompByCode('lname');
    let email = getCompByCode('email');
    let passwordConfrim = getCompByCode('passwordConfirm');
    let password = getCompByCode('password');

    //обязательные поля заполнены, email корректен
    let success = emptyValidator(fname) &
      emptyValidator(lname) &
      emptyValidator(password) &
      emptyValidator(passwordConfrim) &
      emailValidator(email);

    if (!success) {
      //отображаем сообщение с типом “ошибка”
      showMessage('Не все поля формы регистрации были заполнены', 'error');
      return;
    }

    //поиск юзера в системе
    searchUser(email.text).then(user => {
      if (user && user.errorCode == 0) {
        showMessage(localizedText(`Пользователь с адресом электронной почты ${user.result.email} уже зарегистрирован в системе`, `Пользователь с адресом электронной почты ${user.result.email} уже зарегистрирован в системе`, `${user.result.email} электрондық пошта мекенжайы бар пайдаланушы жүйеде тіркелген`, `Пользователь с email (${user.result.email}) уже зарегистрирован в системе`), 'error');
      } else {
        if (password.text === passwordConfrim.text) {
          // Собираем данные в один объект
          let data = {
            login: email.text,
            password: password.text,
            firstname: fname.text,
            lastname: lname.text,
            pointersCode: transliterate(lname.text + '_' + fname.text),
            isConfigurator: false,
          };
          let userID;
          // Отправляем запрос в Synergy
          rest.synergyPost('api/filecabinet/user/save', data, "application/x-www-form-urlencoded; charset=UTF-8", function(success) {
            // Обрабатываем успешный ответ (Выводим сообщение);
            if (success && success.errorCode && success.errorCode === '0') {
              userID = success.userID;
              let positionID = 'f414f3cb-4689-434a-8117-006aca39f76f';
              //назначаем на должность "Граждане"
              rest.synergyGet(`api/positions/appoint?positionID=${positionID}&userID=${userID}`, res => {
                //добавление в группу "Пользователи"
                rest.synergyGet(`api/storage/groups/add_user?groupCode=users&userID=${userID}`, res => {
                  showMessage(localizedText('Вы успешно зарегистрировались на платформе "Birge"!', 'Вы успешно зарегистрировались на платформе "Birge"!', 'Сіз "Birge" платформасында сәтті тіркелдіңіз!', 'Вы успешно зарегистрировались на платформе "Birge"!'), 'success');
                  fire({type: "goto_page", pageCode: "auth", pageParams: []}, "register");
                });
              });

            } else if (success && success.errorCode && success.errorCode !== '0') {
              showMessage(success.errorMessage, 'error');
            } else {
              console.error(success);
            }
          }, function(err) {
            console.error(err);
          });
        } else {
          showMessage(localizedText('Пароли не совпадают', 'Пароли не совпадают', 'Пароли не совпадают', 'Пароли не совпадают'), 'error');
        }
      }
    });

  });
});
