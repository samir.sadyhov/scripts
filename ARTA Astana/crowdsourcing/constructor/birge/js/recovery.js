const emptyValidator = (input) => {
  if (input && input.text && input.text !== '') {
    fire({type: 'input_highlight', error: false}, input.code);
    return true;
  } else {
    fire({type: 'input_highlight', error: true}, input.code);
    return false;
  }
}

//валидация введенного email
const emailValidator = (input) => {
  let regex = /^([a-zA-Z0-9_.+-])+\@(([a-zA-Z0-9-])+\.)+([a-zA-Z0-9]{2,4})+$/;
  if (input && regex.test(input.text)) {
    fire({type: 'input_highlight', error: false}, input.code);
    return true;
  } else {
    fire({type: 'input_highlight', error: true}, input.code);
    return false;
  }
}

const generatePassword = () => {
  let charSet = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789-+_';
  return Array.apply(null, Array(6)).map(() => charSet.charAt(Math.random() * charSet.length)).join('');
}

//метод поиска юзера, кастомное апи
const searchUser = async (email) => {
  const res = await fetch(`${window.origin}/crowdsourcing/rest/api/user/search?email=${email}`, {headers: {
    'Authorization': "Basic " + btoa(Cons.creds.login + ":" + Cons.creds.password)
  }});
  return await res.json();
}

//метод установки пароля, кастомное апи
const setPassword = async (userID, newPass) => {
  const res = await fetch(`${window.origin}/crowdsourcing/rest/api/user/set/password?password=${newPass}&userID=${userID}`, {
    method: 'POST',
    headers: {
      'Authorization': "Basic " + btoa(Cons.creds.login + ":" + Cons.creds.password),
      'Content-Type': 'application/json;charset=utf-8'
    }
  });
  return await res.json();
}

const sendNotification = async (body) => {
  const res = await fetch(`${window.origin}/Synergy/rest/api/notifications/send`, {
    method: 'POST',
    headers: {
      'Authorization': "Basic " + btoa(Cons.creds.login + ":" + Cons.creds.password),
      'Content-Type': 'application/json;charset=utf-8'
    },
    body: JSON.stringify(body)
  });
  return await res.json();
}

const t = msg => msg[AS.OPTIONS.locale];

pageHandler('Password_recovery', () => {
  let ie = $('#mail');
  ie.off();
  ie.on('input', () => ie.val(ie.val().replace(/[^A-Za-z0-9_.+-\@]/g,'')));

  if(Cons.getAppStore().button_recovery_pass_listener) return;
  Cons.setAppStore({button_recovery_pass_listener: true});

  addListener('button_click', 'button_recovery_pass', (e) => {
    let emailInput = getCompByCode('mail');

    if(!emptyValidator(emailInput)) {
      showMessage(localizedText('Поле e-mail не заполнено', 'Поле e-mail не заполнено', 'Электрондық пошта өрісі толтырылмаған', 'Поле e-mail не заполнено'), 'error');
      return;
    }
    if(!emailValidator(emailInput)) {
      showMessage(localizedText('Введено не корректное значение.', 'Введено не корректное значение.', 'Қате мән енгізілді.', 'Введено не корректное значение.'), 'error');
      return;
    }

    try {
      let newPass = generatePassword();
      let user = null;

      //Поиск юзера в базе по email
      searchUser(emailInput.text).then(res => {
        if(res && res.errorCode === 0) {
          user = res.result;
          //установка нового пароля
          return setPassword(user.userID, newPass);

        } else {
          throw new Error(t({ru: 'Пользователь с таким e-mail не найден. Проверьте корректность введенных данных', kk: 'Мұндай электрондық поштасы бар пайдаланушы табылмады. Енгізілген деректердің дұрыстығын тексеріңіз'}));
        }

      }).then(res => {
        if(res && res.errorCode === 0) {
          //отправка уведомления на почту
          return sendNotification({
            header: 'Құпия сөзді қалпына келтіру. Сброс пароля',
            message: 'Сіздің құпия сөзіңіз қалпына келтірілді. Сіздің жаңа кіру құпия сөзіңіз <b>' + newPass + '</b>. Ваш пароль был сброшен. Ваш новый пароль для входа <b>' + newPass + '</b>',
            emails: [user.email]
          });
        } else {
          throw new Error(res.errorMessage);
        }
      }).then(res => {
        if(res && res.errorCode === 0) {
          showMessage(localizedText('Уведомление отправлено', 'Уведомление отправлено', 'Хабарлама жіберілді', 'Уведомление отправлено'), 'info');
          //если все успешно переходим на страницу авторизации
          fire({type: "goto_page", pageCode: "auth", pageParams: []}, "button_recovery_pass");
        } else {
          throw new Error(res.errorMessage);
        }
      }).catch(err => {
        showMessage(err.message, 'error');
      });

    } catch (err) {
      console.log(err);
      showMessage(localizedText('Произошла ошибка при сбросе пароля', 'Произошла ошибка при сбросе пароля', 'Құпия сөзді қалпына келтіру кезінде қате пайда болды', 'Произошла ошибка при сбросе пароля'), 'error');
    }
  });

});
