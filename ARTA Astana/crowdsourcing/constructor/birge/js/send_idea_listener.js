pageHandler("passport", () => {

  let formPlayerId = "formPlayer_idea"; // id компонента проигрывателя форм
  let buttonID = "button_send_idea"; //код кнопки для отправки формы
  let registryCode = "registry_idea"; //код реестра в котором создается запись
  let find = ["textbox_idea_name", "textarea_description"]; //список полей формы которые необходимо сделать обязательными

  //Сообщение при ошибке проверки полей
  let msgError = {
    ru: 'Не все поля формы были заполнены. Пожалуйста заполните все поля формы.',
    kz: 'Форманың барлық өрістері толтырылмаған. Барлық өрістерді толтыруыңызды өтінеміз.'
  };

  let fields = {};
  let asform = null;

  addListener("loaded_form_data", formPlayerId, event => asform = event);

  if (Cons.getAppStore().button_send_app_listener) return;

  addListener("button_click", buttonID, function() {

    find.forEach(elem => {
      fields[elem] = {};
      fields[elem].model = asform.model.getModelWithId(elem);

      fields[elem].model.getSpecialErrors = function() {
        if (!fields[elem].model.getValue()) {
          return {
            id: fields[elem].model.asfProperty.id,
            errorCode: AS.FORMS.INPUT_ERROR_TYPE.wrongValue
          }
        };
        return 0;
      };
    });

    let valid = !asform.model.getErrors().length;
    if (valid) {
      fire({type: "set_disabled", disabled: true, cmpID: buttonID}, buttonID);
      fire({
        type: "create_form_data",
        registryCode: registryCode,
        activate: true,
        success: (id, docid) => {
          console.log("success");
        },
        error: (st, err) => {
          console.log("failed" + err.toString());
        }
      }, formPlayerId);
    } else {
      showMessage(localizedText(msgError.ru, msgError.ru, msgError.kz, msgError.ru), 'error');
    }
  });

  Cons.setAppStore({button_send_app_listener: true});
});
