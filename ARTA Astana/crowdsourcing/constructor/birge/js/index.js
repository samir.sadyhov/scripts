const getSettings = async () => {
  if(Cons.getAppStore().appSettings) return Cons.getAppStore().appSettings;
  let res = await fetch(`${window.origin}/crowdsourcing/rest/api/unsecured/get-settings`);
  if(!res.ok) return false;
  res = await res.json();
  if(res.errorCode !== 0) return false;
  Cons.setAppStore({appSettings: res.result});
  return res.result;
}

const getCountIdeas = async () => {
  if(Cons.getAppStore().count_idea) return Cons.getAppStore().count_idea;
  let res = await fetch(`${window.origin}/crowdsourcing/rest/api/unsecured/get-countIdeas`);
  if(!res.ok) return 0;
  res = await res.json();
  if(res.errorCode !== 0) return 0;
  Cons.setAppStore({count_idea: res.result});
  return res.result;
}

const setSvgImg = (cmpID, imgID) => {
  fetch(`${window.origin}/crowdsourcing/rest/api/unsecured/image?identifier=${imgID}`)
  .then(res => res.text()).then(svg => $(`#${cmpID}`).empty().html(svg));
}

const setSvgBackground = (imgID) => {
  fetch(`${window.origin}/crowdsourcing/rest/api/unsecured/image?identifier=${imgID}`)
  .then(res => res.text())
  .then(svg => $('.background-banner').css('background', `url('data:image/svg+xml,${encodeURIComponent(svg)}') 50% 100%/50% auto no-repeat, #f6f8fc`));
}

const setAkimatname = () => {
  getSettings().then(s => $('#label-akimat-name > span').text(s.name[AS.OPTIONS.locale]));
}

const setContacts = () => {
  getSettings().then(s => {
    let contacts = `<span class="contact-head">${AS.OPTIONS.locale == 'kk' ? 'Байланыс:' : 'Контакты:'}</span>` +
      `<span class="contact-item"><a href="tel:${s.phone[AS.OPTIONS.locale]}">${s.phone[AS.OPTIONS.locale]}</a></span>` +
      `<span class="contact-item">E-mail: <a href="mailto:${s.email[AS.OPTIONS.locale]}">${s.email[AS.OPTIONS.locale]}</a></span>` +
      `<span class="contact-item">${s.address[AS.OPTIONS.locale]}</span>`;
    $('#panel-contacts').empty().html(contacts);
  });
}

const setDescriptionProject = () => {
  getSettings().then(s => $('#project-description').empty().html(s.description[AS.OPTIONS.locale]));
}


const setRulesIdea = () => {
  getSettings().then(s => {
    let html = s.info[AS.OPTIONS.locale];
    let linkText = AS.OPTIONS.locale == 'kk' ? '<a id="go_rules">модерация ережелерін</a>' : '<a id="go_rules">правилами модерации</a>';
    html = html.replace('${MODER_RULES_LINK}', linkText);
    $('#rules-idea').empty().html(html);
    $('#rules-idea #go_rules').on('click', () => {
      fire({type: 'goto_page', pageCode: 'moderation_rules'}, 'rules-idea');
    });
  });
}

const hideShowComp = () => {
  getSettings().then(appSettings => {
    switch (Cons.getCurrentPage().code) {
      case 'about_us':
        setSvgImg('about-img', appSettings.images['bannerabout_'+AS.OPTIONS.locale]);
        $('#about-description').empty().html(appSettings.about[AS.OPTIONS.locale]);
        break;
      case 'moderation_rules':
        $('#rules-description').empty().html(appSettings.moderation_rules[AS.OPTIONS.locale]);
        break;
      case 'newsdescription':
        if(AS.OPTIONS.locale == 'kk') {
          $('#label_head_news').hide();
          $('#label_category').hide();
          $('#label_text_news').hide();
          $('#label_head_news_kz').show();
          $('#label_category_kz').show();
          $('#label_text_news_kz').show();
        } else {
          $('#label_head_news').show();
          $('#label_category').show();
          $('#label_text_news').show();
          $('#label_head_news_kz').hide();
          $('#label_category_kz').hide();
          $('#label_text_news_kz').hide();
        }
        break;
      case 'passport':
        if(AS.OPTIONS.locale == 'kk') {
          $('#label_project_name').hide();
          $('#label_goal_project').hide();
          $('#label_description_project').hide();
          $('#label_project_name_kz').show();
          $('#label_goal_project_kz').show();
          $('#label_description_project_kz').show();
        } else {
          $('#label_project_name').show();
          $('#label_goal_project').show();
          $('#label_description_project').show();
          $('#label_project_name_kz').hide();
          $('#label_goal_project_kz').hide();
          $('#label_description_project_kz').hide();
        }
        break;
      case 'details_idea_implemented':
        if(AS.OPTIONS.locale == 'kk') {
          $('#label_head_implementation').hide();
          $('#label_category').hide();
          $('#label_text_implementation').hide();
          $('#label_head_implementation_kz').show();
          $('#label_category_kz').show();
          $('#label_text_implementation_kz').show();
        } else {
          $('#label_head_implementation_kz').hide();
          $('#label_category_kz').hide();
          $('#label_text_implementation_kz').hide();
          $('#label_head_implementation').show();
          $('#label_category').show();
          $('#label_text_implementation').show();
        }
        break;
    }
  });
}

const localized = () => {
  let store = Cons.getAppStore();
  // такс этa фигня нужна для страницы подробнее 'passport'
  if (store && store.localeClicker && store.localeClicker.localeClicker) {
    store.localeClicker.localeClicker(AS.OPTIONS.locale);
  }
  // такс этa фигня нужна для страницы Мои идеи
  if (store && store.localeClickerMyIdeas && store.localeClickerMyIdeas.localeClickerMyIdeas) {
    store.localeClickerMyIdeas.localeClickerMyIdeas(AS.OPTIONS.locale);
  }
  // такс этa фигня нужна для мобильного меню
  if (store && store.localeClickerMobile && store.localeClickerMobile.localeClickerMobile) {
    store.localeClickerMobile.localeClickerMobile();
  }
  // такс этa фигня нужна для кнопки обратной связи
  if (store && store.localeClickerFeedback && store.localeClickerFeedback.localeClickerFeedback) {
    store.localeClickerFeedback.localeClickerFeedback();
  }
}

function checkIfExists(getLabels) {
  if ($("#localeSelector").length) {
    // добавляем классы для языковых лейблов
    let arr = [];

    for (let i = 0; i < getLabels.length; i++) {
      arr[i] = [];
      parsedData($("#panel_iterator").children(), 0, getLabels[i], arr[i]);
    }

    function parsedData(array, index, val, arrayToWriteTo) {
      if (index == 10) {
        return;
      }
      for (let i = 0; i < array.length; i++) {
        if (arrayToWriteTo.filter(x => x == -1).length) {
          return;
        }
        arrayToWriteTo[index] = i;
        if (val == $(array[i]).attr("id")) {
          arrayToWriteTo[index + 1] = -1;
          return 1;
        }
        let res = parsedData($(array[i]).children(), index + 1, val, arrayToWriteTo);
        if (res == 1) {
          break;
        }
      }
    }

    fuckingTimerBugs(0);

    function fuckingTimerBugs(count) {
      if (count == 10) {
        return;
      }
      if ($("#panel_iterator").parent().children().length > 1) {
        let iterator_parent = $("#panel_iterator").parent().children();
        for (i = 1; i < iterator_parent.length; i++) {
          runThrowParsef($(iterator_parent[i]).children(), 0, arr[0], "ru_labels");
          runThrowParsef($(iterator_parent[i]).children(), 0, arr[1], "kz_labels");

          if ($('.ru_labels').length == 0) {
            runThrowParsef($(iterator_parent[i]).children().children(), 0, arr[0], "ru_labels");
            runThrowParsef($(iterator_parent[i]).children().children(), 0, arr[1], "kz_labels");
          }
        }
        $("#localeSelector").trigger('change');

        function runThrowParsef(array, index, arrayWithIndexes, label_class) {
          if (index == 10) {
            return;
          }

          for (let i = 0; i < array.length; i++) {
            if (i == arrayWithIndexes[index]) {
              if (arrayWithIndexes[index + 1] == -1) {
                $(array[i]).addClass(label_class);
                if (label_class == "kz_labels") {
                  $(array[i]).hide();
                  $(array[i]).removeClass("uk-hidden");
                }
                if (label_class == "ru_labels") {
                  $(array[i]).hide();
                  $(array[i]).removeClass("uk-hidden");
                }
                return;
              }
              runThrowParsef($(array[i]).children(), index + 1, arrayWithIndexes, label_class);
            }
          }
        }

      } else {
        setTimeout(fuckingTimerBugs, 800, count + 1);
      }
    }

    $("#localeSelector").off();
    $("#localeSelector").on('change', function() {
      AS.OPTIONS.locale = this.value;
      document.cookie = "ConstructorLocale=kk; path=/;";

      setContacts();
      setDescriptionProject();
      setRulesIdea();
      setAkimatname();

      if (this.value == "kk") {
        global_locale_value = this.value;
        setElementsVisible(false);
      } else if (this.value == "ru") {
        global_locale_value = this.value;
        setElementsVisible(true);
      }

      localized();
    });

    function setElementsVisible(hide) {
      if (hide) {
        $('.ru_labels').show();
        $('.kz_labels').hide();
      } else {
        $('.kz_labels').show();
        $('.ru_labels').hide();
      }
    }
  } else {
    setTimeout(checkIfExists, 200);
  }
}

getSettings().then(appSettings => {
  $('#localeSelector>[value="en"]').hide();
  $('#localeSelector>[value="kk"]').text('KZ');
  $('#localeSelector>[value="ru"]').text('RU');
  AS.OPTIONS.locale = $('#localeSelector').val();
  hideShowComp();
  try {
    setSvgImg('header-logo', appSettings.images.logo_header);

    switch (Cons.getCurrentPage().code) {
      case 'main_page':
        setSvgImg('banner-img', appSettings.images.banner_main);
        showVoteinfo(appSettings.vote_info.radio_start_vote);
        break;
      case 'vote_for_an_idea':
        setIdeaDates(appSettings.vote_info.date_start_vote, appSettings.vote_info.date_stop_vote);
        break;
      case 'auth':
      case 'registration':
      case 'Password_recovery':
        setSvgBackground(appSettings.images.banner_main);
        break;
      default: setSvgImg('banner-img', appSettings.images.banner);
    }

    setSvgImg('footer-logo', appSettings.images.logo_footer);
    setDescriptionProject();
    setContacts();
    getCountIdeas().then(ideas => $('#label-idea-count > span').text(ideas));
    setRulesIdea();
    setAkimatname();
  } catch (e) {
    console.log(e.message);
  }
});

const setIdeaDates = (dateStart, dateStop) => {
  fire({
    type: 'change_label',
    text: localizedText(dateStart, dateStart, dateStart, dateStart),
  }, 'date_start_vote');

  fire({
    type: 'change_label',
    text: localizedText(dateStop, dateStop, dateStop, dateStop),
  }, 'date_stop_vote');
}

const showVoteinfo = (radio_button_value) => {
  fire({
    type: 'set_hidden',
    hidden: Boolean(radio_button_value - 1)
  }, 'button_vote_ideas');

  fire({
    type: 'set_hidden',
    hidden: Boolean(radio_button_value - 1)
  }, 'label_vote_info');
}


$("#localeSelector").off().on('change', function() {
  AS.OPTIONS.locale = this.value;
  hideShowComp();
  setContacts();
  setDescriptionProject();
  setRulesIdea();
  setAkimatname();
  localized();
  if (Cons.getCurrentPage().code == "vote_for_an_idea") {
    if (Cons.getAppStore() && Cons.getAppStore().parseMyIdeas && Cons.getAppStore().parseMyIdeas.parseMyIdeasData) {
      Cons.getAppStore().parseMyIdeas.parseMyIdeasData();
    }
  }
});

$('#header-logo').off().on('click', e => {
  fire({type: 'goto_page', pageCode: 'main_page'}, 'header-logo');
});

if (Cons.getCurrentPage().code == "main_page") {
  checkIfExists(["label_project_name", "label_project_name_kz"]);
} else if (Cons.getCurrentPage().code == "news") {
  checkIfExists(["label_category", "label_category_kz"]);
  checkIfExists(["label_news_name", "label_news_name_kz"]);
} else if (Cons.getCurrentPage().code == "ideas_for_implementation") {
  checkIfExists(["label_project_name", "label_project_name_kz"]);
} else if (Cons.getCurrentPage().code == "my_ideas") {
  localized();
} else if (Cons.getCurrentPage().code == "passport") {
  localized();
}

window.history.pushState({page: 1}, "", "");
window.onpopstate = function(event) {
  window.history.pushState({page: 1}, "", "");
  fire({type: 'goto_page', pageCode: 'main_page'}, 'header-logo');
  return;
}
