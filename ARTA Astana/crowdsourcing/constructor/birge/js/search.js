/**
 * Сюда добавляем колонки которые мы хотим сравнивать
 * добавляем как на примере, внутрь квадратных скобок, добавляем фигурные и в них добавляем name, variant
 * name - компонент в форме
 * variant - ключ или значение
 */
const getColumns = () => {
  return [
    {name: 'project_name', variant: 'value'},
    {name: 'goal_project', variant: 'value'},
    {name: 'description_project', variant: 'value'}
  ];
}

const search = () => {
  //получаем значение поля поиска
  let searchWrap = getCompByCode('search_field').text;
  let params = '';
  //формируем параметры для события изменения данных в итераторе
  if (searchWrap && searchWrap !== '') {
      params += '&groupTerm=or'; // закоментировать если не нужно склеивания групп условий
      getColumns().map((col, i) => {
          let index = i === 0 ? '' : i;
          params += `&field${index}=${col.name}&condition${index}=CONTAINS&${col.variant + '' + index}=${searchWrap}`;
      });
  }
  /*генерируем событие Конструктора, которое изменяет содержимое итетатора с кодом `panel-1`*/
  fire({type: 'change_repeater_search_params', params: params}, 'panel_iterator');

  if(searchWrap != '') {
    fire({
      type: 'change_label',
      text: localizedText("Результаты поиска","Результаты поиска","Іздеу нәтижесі","Результаты поиска"),
    }, 'label-theme');
    fire({
      type: 'change_label',
      text: localizedText("Хотите найти что-то еще?","Хотите найти что-то еще?","Тағы бір нәрсе тапқыңыз келе ме?","Хотите найти что-то еще?"),
    }, 'label-akimat-name');
  } else {
    fire({
      type: 'change_label',
      text: localizedText("Темы для вашей идеи!","Темы для вашей идеи!","Сіздің идеяңызға арналған тақырыптар!","Темы для вашей идеи!"),
    }, 'label-theme');
    let s = Cons.getAppStore().appSettings;
    fire({
      type: 'change_label',
      text: localizedText(s.name.ru,s.name.ru,s.name.kk,s.name.ru),
    }, 'label-akimat-name');
  }
}

/*выполнять скрипты в ресурсе, когда пользователь находится  на странице “Главная”*/
pageHandler('main_page', () => {

  addListener('button_click', 'search_button', e => {
    search();
  });

  $('#search_field').keyup(e => {
    if(e.keyCode == 13) {
      e.preventDefault();
      search();
    }
  });

});
