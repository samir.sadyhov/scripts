const hideShowButtons = () => {
  const pages = ['newsdescription', 'about_us', 'news', 'moderation_rules', 'passport', 'main_page'];
  if(pages.indexOf(Cons.getCurrentPage().code) === -1) return;
  if(AS.OPTIONS.login == '$key') {
    fire({type: 'set_hidden', hidden: false}, 'button_my_ideas');
    fire({type: 'set_hidden', hidden: false}, 'button_sign_out');
    fire({type: 'set_hidden', hidden: true}, 'button_sign_in');
  } else {
    fire({type: 'set_hidden', hidden: true}, 'button_my_ideas');
    fire({type: 'set_hidden', hidden: true}, 'button_sign_out');
    fire({type: 'set_hidden', hidden: false}, 'button_sign_in');
  }
}

const logOut = () => {
  AS.apiAuth.setCredentials(null, null);
  AS.OPTIONS.currentUser = {};
  Cons.logout();
  if(Cons.getCurrentPage().code === 'main_page') {
    window.location.reload();
  } else {
    fire({type: 'goto_page', pageCode: 'main_page'}, 'button_sign_out');
  }
}

pageHandler('auth', () => {
  if (Cons.getAppStore().auth_page_listener) return;

  addListener('auth_success', 'button_auth', authed => {
    const moduleID = 'nur-sultan';
    const {login, password} = authed.creds;

    AS.apiAuth.setCredentials(login, password);
    AS.OPTIONS.login = login;
    AS.OPTIONS.password = password;
    AS.OPTIONS.currentUser = authed.data.person;

    AS.FORMS.ApiUtils.simpleAsyncGet('rest/api/person/generate_auth_key?moduleID=' + moduleID)
    .then(access_token => {
      AS.apiAuth.setCredentials('$key', access_token.key);
      AS.OPTIONS.login = '$key';
      AS.OPTIONS.password = access_token.key;
      authed.creds.login = '$key';
      authed.creds.password = access_token.key;
      fire({type: 'goto_page', pageCode: 'main_page'}, 'button_auth');
    });
  });

  Cons.setAppStore({auth_page_listener: true});
});


$('#button_sign_out').off().on('click', e => {
  logOut();
});

const setImageApiKey = () => {
  const app = Cons.getCurrentApp();
  const apiKey = btoa(unescape(encodeURIComponent(app.defaultUser + ":" + app.defaultUserPassword)));
  let d = new Date();
  d.setDate(d.getDate() + 1);
  document.cookie ='constructor_image_api_key=' + apiKey + '; expires=' + d + '; path=/;'
}

setImageApiKey();
setTimeout(() => {
  setImageApiKey();
}, 0);
setTimeout(() => {
  setImageApiKey();
  hideShowButtons();
}, 200);
setTimeout(() => {
  setImageApiKey();
}, 500);

AS.SERVICES.showWaitWindow = () => Cons.showLoader();
AS.SERVICES.hideWaitWindow = () => Cons.hideLoader();
