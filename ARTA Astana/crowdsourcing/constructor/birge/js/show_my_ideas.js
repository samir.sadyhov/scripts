pageHandler("my_ideas", () => {
  if (Cons.getAppStore().my_ideas_registry_item_click_listener) return;

  let row;

  addListener('registry_item_click', 'registry_appeal', e => {
    row = e;
    fire({type: 'set_hidden', hidden: false}, 'panel_appeal');
  });

  addListener('set_hidden', 'panel_appeal', event => {
    if(!event.hidden) fire({type: 'show_form_data', dataId: row.dataUUID}, 'formPlayer_appeal_view');
  });

  Cons.setAppStore({my_ideas_registry_item_click_listener: true});
});
