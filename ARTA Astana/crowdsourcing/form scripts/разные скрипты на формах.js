//заполнение емайла юзера (форма Идея form_idea)
const setUserEmail = val => {
  if(!editable) return;
  if(!val || !val.length) return;
  if(!val[0].hasOwnProperty('personID')) return;
  try {
    AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/filecabinet/user/${val[0].personID}`).then(user => {
      model.playerModel.getModelWithId('textbox_tenant_mail').setValue(user.mail);
    });
  } catch (e) {
    console.log(e);
  }
}
model.on('valueChange', (_1, _2, value) => setUserEmail(value));
