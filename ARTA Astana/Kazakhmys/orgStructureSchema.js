let isHiddenIndividual = false

const getStructureJsonData = (dataSource, options = {
    isHiddenIndividual: false
}) => {
    const images = {
        baseImageRoot: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAACXBIWXMAAAsTAAALEwEAmpwYAAAJAUlEQVR4nO1daYwURRh9K8JyCSoICEgURTnkMHiheBFuxCugIV5EI8oPXIgiIoJDOFTkEIkGVBIPxChIREQ5RMSooKjsGhRBRYIIe6AIcgi4jKn4Kql8qZrumZ3Z3Zmul1Qy21X9VU+9ra6vXn1VA3h4eHh4eHh4eHh4eHikgj5MHlWMNgDeBhBnWg2gQ1U/VBRRH0AMwD8GGTodAzAbwKlV/ZBRwEkA7gNQYhBQDuBFpnLjegnL1qjqh85VXAZgg+gNG3hd4yIA60SZH/z4kl60BPAagBNGI/8G4C4AeY57BgLYLohZBqB1mp8tUqjLceKw0aiHeK1OiPtrASgAcMC4/yjHl1Mq4flzBnkAbgewy2hI1TsWAGiRgr0WvNfsYcr2HQl6mIeB9eJV8yWAbmlooW60ZdpWdXkEIG6kR9P8X5xHm2YdHiEJ+R5AGRswPw2tlk9bZbTtCQmJOJMavM+ih7WTXlWqUF7Xz/S0zqVtT0gKhGhcB6AIwMcAOiE82gL4AMAWAH2N656QChKiZ+mql+xhr2kCN06je1tK1/dkke8JSQMhZmM/BaAYwBjONzRUww8DsBvAPABnwA5PSBoJ0WgPYBWAzQB6MW3mNZWXCJ6QDBCicQNlku38HAaekAwSApZNtrz3skIi7gmpXoh7QqpOVn/dIhrGPSGVizp0Vw84ZPW4J6RyoIS92wDssKyD6/SrJ6Ty8Klo/G8BXMO0yULOJgBdM+BldRX1RRZmY4+jFKJRgzPsUlFOBS28DKBpGghpSltmIISa7UcWuhG2UAJ/WMgeYOjODIbymMTsBzDaUj4MIbV4736xlDsNQANEGOZgfR4D3LYBGBwiAC7O9JOjvIuQnmLtQwfTBUkrkYDNe+rDMJ0VANpZ7unHHiWJWSkaVRLSnmXMexQxvTP4/bIOLndWKrSNRX5N5peJBj5uKLqaEK0EHzXK/WlRhT1CuLONAbzAgXa4JeJQNfxcAP/Szhyj/BdMxbw2x6ivkW/95HqIREcAawB8B6CHJb8zgG8MO6r810zqs4IXD1MkZCiARQDOsZS/BcAvAN6xRBzKMcP2d+TnGakQEuMEUY0PUxnVbqI2gLGWfE9IBglRqTmAVxize6clFutM5u/iuvrEJHqIj05MgRCNpx1R7RqXMtrwcEg7OjrRZivSMvsbIQmJiXjeV9l7TKj/+CEALrbYac57VE/6PYStSEFJ6eMBHBTzh7CExJn+BvAYxxMXYhQM1VgzhWNNfX4+kqStnMRgi8z+FoD5IQiZz7KSmO30vGy4h2WUPiahPLjFFluDEAHYdjApL+rqJLyjWIIdUyqt5VxEQsv4nwDoYsm/FkChsOUqmxOYZ8yizT1+psyeDCGuPYVx1mP7D1ez+/sZ3TjXEiin80uFLVvZrIfZYM8BaGgpM4EDvNKnJAE1mafKSDSkTddYBIuMP5MNP8qoT+abMv8+R9mshf5iewFsBTDA4XEtp4LbzyBEq7rLWUZiAG3udRDSyXFwgA62/pF1uPJNolXZ/sgBmA3Vlw38Ib+0RD9+8T+YNEESbWlDR6+bXph81R1n49rq68/6FOEXJMg3iXHZyhrIhlJdfyRfG7MsG/lV/oMARjheKbN470jxinMRMtmo71lK8bK+Ucyf6XieUXx16TqOOZ49K+B6t2vZvJgDaqKN/DVYptgx0CYiJGbI+Cs5J3nAIePP48A/LITMH6etrEPQYNuZLmshXVCXW7rW4daGJUSWK+KGH4kudHtdz1MgXmE5u94xiPFXSnY/m2kRrw0KuDcVQnRa7JD5Ez3PR1EgBJQuxvFVUMbPtQPuqQdgCaWYwhCEFFpkmyOUU5QtKfM8bnmeWFQISSaeKo8HCezkPKVlEhPMlpZDA+IUH20yf04tdGWCkEu4Vv4VgCsS3Bf0dzfakK+x9QxJSmTHE4L/N3TqBamhQn5JhRDQxt2MbjFJecITEtxDxrOxpjgODRjId/1Y410fE0u+qoxEPucrqSwHRPqVFWMPWcJAh5stZVob+auZXEERCjfx8IAltO0JSZKQGD/3YEjQGiPMx4TOL3KEDV1I99UMK0rFfc466AdfYYk+rAgh4Gx6uBEIJ4Pf8iwekyrzvCPwLhKEHBPhmwUhpOywhMgTGkqog8kTGsBrI1hmtkXTSpUQW13VGu3YO8wBU54xUlFCZF0ygLo3r7mCtytCSNYGa1/PtQuTmPcdsrer4SeFdA50Xe8xbeW1IEwyVguDCDFFxjjrOR9ZhloMOPhLSNkzxEqibICGxoadWBJ1vckUFOGu7E+nBP8Q3eogQqY6NvxMd6yKVms0sZynW2rI3roB9Ja2kgzN+PXa/B4+T5MUJphqS9xLlrOBbRJ+VkajqAiRpUy2TZ+xNBFyFSNf1vE5Kjrjt30XJWRmJW4N2Ba9I82E3EthUb1ybBjNxm1lsdOKeYnuzer1kkQRjQd5rU4GZvxFlFIm8ixg+SwTjPxpTBN5bYLljOB6dArKRJxX1kNL4wtEpEk8zYTosxsXUsIfYplE6vxyJi3zu5YB9DNn9cQxLOJMTyZxT9j5zJXcafWZCNbWuNwRKa/Kfg5go2UZIDKEnODZis1C3JPMBDOPe0x2h7Df2FAGCizeVCQIeUbsog06NCDVGX8D7iNRY8EjQubP587dMu7qdZ0VHwlCwJnwMuHFbEtwhF+qEgy4YriUhxLcyKQ+vytWEyNNiDxYwCRmleVnjWwN34HLtmGdg16UXrbycxhEjhDzYIFSx6EBkhCtCB/PUNAFok6IxumiobXMP4aa1GTHiUKekAyjowhg08KlPEHIE1LJGMg1ckmAPrjfE1IFyOdPUBxgMn/awhNShWhmmeB5QqoZ4kzqPPjuGfCyutN2ZL2sZLFTyDALKSJWlBAtRJrxwqoujwDUpYwuf3bPJqmHIURL9YeEvZhF1vdI8ocp9SE2QdHvQT9UqfaSeGTwp1slIUEHIXhUEPpnk4qNBi5nD2pqENKIqoAZArTXIcF7pAGugwM2Mu3LhZCfbEQbi8xvptUWZdmjEtBTzCnCRkB6ZBD64ADzoAIPDw8PDw8PDw8PD6Qd/wEexWxFyCTAfwAAAABJRU5ErkJggg==',
        baseImageCompany: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAACXBIWXMAAAsTAAALEwEAmpwYAAAJnElEQVR4nO1dCawVNRQ9/wMiH/iiyKokiuCGC8gSNCqKuKCBqHGPIuKCBANCRCEIjKIoajSoUUncFde4oRIVVFwAF8QVRVBZ/Xz4hqjsomIaT2Nz7axv3nv/velJJpn3btuZ6ZlO29PbFnBwcHBwcHBwKDIOBfA2D3XuUCTsAWAagB0AdvL4C8DjAFo7VgqHRgBGANhgEPEHD/1b2UYyrEMecQKAr4yMV8ccAIcA2B/Aa8L2PYDTHCPpoxOA50RmLwNwtiVsPwDfiLCzAXRxxOSOpgA8ANuMzN3E/xoHxGvET9av4rOm6pzdHDHxUQlgEIC1lgq7TYx0WpKEP410fiFZDRwx0dALwALxyfkYQO8cMrAbgPdEmosAHOtI8cdeLAF/G5m2hiWlIqWMGwDgJ0HMqwD2dcT8hyoA1wHYaGTSFgC3AmiWh4xqwuv9brle86wTo97Y5ZY3dp8CXLu9pUT+DOAK1mGZwhEA3hdEfAbgmCLcS08A88S9fALgKGQAbQE8xBaTfvhaAJcW+a2sBDAYQI1o1T2CMsUuAMYA+M144O0AbgNQjfqD5gDuEqWl7DCQvWrzIWcC6Iz6h9MB/ABgXTkTYhKxBMCpqH84EMAsAN8B6E8lIBOE3EfZvL6gJYB7WSKGA2jI/zNByDQe6yld6IcvBhqyeasq8ekAWgl7JghRDwmO6inJ/GvK6YXGiVSD3/JRgE+kbJ8ZQjTOYAX6EoD9CiThv8IGxsAQ+5tZIGSmpYmrJPSxAOryKF1UA5jKa1zLJniQvXFWPlkr+c0eYukEtmMnTEkXl6TUSazktWrYGW0bw54JQjxDMvGTSZT9gxRkFCXhz6cEcmQCe2YIkaKin5A4IMQeJuGv9pHtw+yZJcSUwdf5SO1VIfY4aUl7WF2VSULSeqsH5KG0ZZoQOXT7sc9wrbR3Y33kNxSr66skQ7WZI+Rc9pDbWFo+l7Hl82CIvYbnskXWhnH97FGQOUI8Cnmq7X+NT9/gdtrHWOyepcRpib+OcXOR9TNJiEdvQ/VtXwqgqyVuZ3Yol7IukPE1BjBMUln/NKFCZ5YQjWkhdcxJABZT0jjYiH8w/1vMMHGh43/LQ50f5Aj5P0HNLR6GDelkvY560zKej0jgVL07XwIzvvZ2XMfSZqrUKnxZl5CzOJY+jB6EkpDx9DDUdjmG8QYPdR4HDZhmLcdmbPH3BHC/8Has87mXsmr26ok2X7EekZ+0pw173wiVehj6Mq2oE3sOA/COGGD70nIvZdcPUTL8j2xt2TL8THbsBvvYw9ARwIu8hkorLs60eDu+wHTLtmPYUIwgeiGNgCiEKPnkFn5uxgHYFcmxK9MwvSu3ApiSJ+/KghOiFNYLA8S9SezcVSQgRMW5mH7Bj9JDMS2otB6zeDum6X9ctGbv5gCZpJOQSaIS0ptxFlBqKSUP/bxjbwAzAgi5gaVEiYZPWBwNKgz7mhBCWjGN1SElL02oa1zEe9PP+DfvQwmi9QZK6p7AmU7mG+SXoU05rn59yCyqk33ig3F3ss5Q1y8k1P3dxDpFP+smNt1zqbdSgZrzt0IQ8SyHSHOtpBES/2EAz7NFpvo5hca+vL757MuLVbd0DZmh5LGi1riaw7RdfTJ4GPsANn3LFt78fRyALwDMDYifT4wU+VBwQqaLXq2SHi4XsrckpAEd1dQcwgfoPSgzeJ5htzmyBZUwlf5Qxrc5wuUDrXivtWJ6Q8EJMb3Z7/CZ5TqJFbzUnFoAuJMzZCdaMrgF7etFDznqJ8+MPypPCwk0Ytrr6TnfQqjFRSNkckiL63U6WytnZomOYjDKExk8Occ66AA6U6ft7N2f4zqz6LBt3k/RCfFiPMDrzCQ/eILgNHruIBlLmIFB1w/DAcYLZiO4ZAiRRfxOFnGJvsKeFiFRr+8H8xM4OuATWFKEyEpwLSv4BgH2eSk3m3X605n+0BBp3WyERGkklCQhGocDeBfA5wD6WOxdOZKn5iFGJWR0QHq29OeyqayazBJ9mFacZnRJE6JxFmXu5yP4T0X5hM1hxyxKevr6Zvh9jI6mbZGbsicElBrGUzKfTFnChqh1ShPKKnURl2kyw9fxPIkUUzaEaOwVIhbGreRzlWaQdUKknD5fyOkyw6ZaCJgaEF7J+z3gj8wQ0jLm8krgAw0yBpzaGRnWnv/9ZRkAMweoZAZPYhy/AazMEDKBw6BJhlSbcci0jsLlIp5PoWTjhYSXJegOwz5WLIiWGUI8vpW5OB10pEfK04ajQVAG6vAXiPvwDPuHAU4WZU+IZ/TGl6S03q6XY6WdRkez5AnRv+cbjmvKSS0JZLqqT3FPgCPdOOFo5wgRGWG6dh6fAiFeiKvpbsKVVA3BuhJiyUiZMVHh94abztRqbV8JbVfjMY6QFD8VXkg694Sk218Qluk6pBCEeOJ30xCPw7IgpDbCFLJCEXKumNIm7eND7rekCbGtnXh0kQmBWKTgZct1ngtYxKCkCangG7lSePA9A6BDEQmR96bmH8rw2r6KXojSXpKEaDSh58hm42Y28+aqLA9axR77iz72OAiLp2dI+YUvq36IhCoVTwkP8VV8E/WDnsf/TPt5KRLSE8A5Id710xN615ccIRqqHlko6peNYo6FPDam2MraSo2qu4/XyELDnglCwBbMpWzRyMyv5fJIQ4TdS3Adz8e7fgjnbzxkkVEqDfuKrBCiUc01erfzkBP7q/NAiGfM6J3Jpq4NzTlcfEpAemVHiEbngIn9+SIkSQZnhpAgpEnIKC5O1sXHPpRqr982SI4Qg5BZCdb2lRmoJpBeRTVX6Vh3W0rMR4ZdXs8RYtmWaHiMtX39MlAvlLzDx7veXEjZdKpzhBjr+e40jq+561pSQjQOoge+X/ibs9bKSrIg/k7jeJmuO364kUecDDPDZ6YfkhSNLdsSbaOvVbVost5K27YYa//2E2sFO0Jy2ABmLTubQ8S2evqo4TIclRHSfpgdQ6X6uhISA90pc/hJLvMs2xZ9GnHboh4k5MqUPllHi51Gy+KTZUMFBciVQpQ8n7YK+l6tFsMAM0SFHgVJCOlA/y8pqpYtIRpVVGtNWd9EFSvsLWIYYGIMT/Y4hOj7CRp2cMC/pSLpRpVRCSnmtn8li14JFooJI6RbPdr2r6Q3O66NuNlxUM/fbXZchO3AJSFuO/A8oxO9TsxPzjJjLqFJSD8uQ2uGnU2vR4eUcTLdTM3MVrL8kzzeELak6wM7JNixbX1Ax3MDpRy55LlDHrEHK+wdloq/dT4v7BBtGCCNiUQODg4ODg4OyDj+AZ5PjgeYv1SwAAAAAElFTkSuQmCC',
        baseImageIndividual: 'data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAAGQAAABkCAYAAABw4pVUAAAACXBIWXMAAAsTAAALEwEAmpwYAAAKSElEQVR4nO1dB4xVRRQ9y8KCDVBWQUTFDiqxYVfsoCgqYq+xBDX2SOxlETUqhGhsqLEHoxKMvZeIYsMKKrrYgKXIB0VBRaSYm5zRm8m8/8r/7+97/89JXvbtnz7nzcydOzN3AA8PDw+PCqMDH48M4AgA3/ORd49WQi8ALwFYaT1vAdjGs1I5NAK4C8AyRUKBj/l/Gf2IX4+U0BbAUKvi/wFwDyu+M4CbAPyt3H8FcCmABs9KebEfgClW1/Q6gK0dfjcH8Lzl91sAB3tSSsdmAJ60KrcZwFERwu4P4Esr7GsAtvLExMfqAJoALFGVuYi/tY8RTzsAFwBYqOJZCuA2AJ08MeFoA+BkAHNVBS4H8AiAriVUYBeSoAWB+SSr3hPjxs4A3re6mA8A7FLGCtsOwNtWGp8C6OdJ+R892AJWqEqayZZSl1JFDQLwg0XMcwA2qmViVqVIukhVyh8UXWUMSRurMP3fVfp/Mv01UGOQL/Qnxxe6YSvkpbujhc5KuYVmBtsDeMci4hMAe7R2xgDsCOA9K28fAdgVVYhGh5RTyKCUU8eWMUflcwVbUDdUCex+ekkO+uk1mEc9D/qdZck97HFiE+QHG7B16DLkHnqG/GrOVBdbMc9Lq5GQ4eyfZ1NDuzayizXZZc3lODeiGgkRPZSrsKJWz5rqZrb10TRVMyEGWwB4AcA3AAai9bEvgC8AvAGgj+VWE4RoNflXVI33RuWxCVX804qo9WuKEK0mL3C+0rEC+VqNeZrPv8V2rdQcIbaa/OcUJ41m8jcrxoSvZgnRKpYJVKvsifKrRz6MqRKpeUK0EvJHTih7IjnWY2tIqtb3hDjU5D8nUM2XElbDE1KGr9wsQpXauqqWEJGgnuWM/fAS1j724NgygWNNsfGnXGr9qiREdhIOAXA9J4QyG17A+cctAI7j9lCZLYdB/JzOOO7jBoiufJ9Ntyjx1DQhLilLRM6DAFwBYByA77ikOxHACRHi7kgy5/G5JaU5TM0Q4kInzt5lifeciGFG8EkLNU2IXosQdcZVCEdTgvjjwBOiurQpFFmLwRNSgRZisA6AzwHcjGB4QipIiFlD+YCSmkt68oRUmBAz2Iv0da+DFE9IKxACqj7e5Iy9PiIhEuY6AAciOfygHqKfeomLSrKOEkSIqFZOAdACYEaJH4QnJARyRG08Z/wdHIT0Zfc2CcBuZejSPCERUM+uS07djmKldefmhJk8j2jGGk9ISmOIi5QHedDzcyowr+fyrIYnpEKEgK1gPDdxB6nZPSEVJCRKhXtCHIS8kuJuxaYUCVmbea8a5eJS6yD/RUpczTIh7ZjXX639ybmH2aGod5B/w3WQrBIykHnUeX6BZalaQqaWsZBNZSJEVixftPI6tcwfT6uhs+PQi7E90p5dwlCeVjL2SlqLkLC85hr1rOh5qnDLihxBkN/GkJihCXcrJiVE0jrTkdcxGT8uEQvPWk1eZtLbRgi3Df1+BmCvChCyNyeTVW1rqzNNJpkCDksQxzCGHRdjP1UcQnoybk1E0rxmHkerAo4tospwYTX6LfDLLaYKSULISG6E0PEvVvn9K0Zec4OHWLhFHAxlt+GjVPadGLDbsI5uM+l3PVXBUcKHEVLH/VrLi8SvjQa0hKSVG7RRVnyettx24Y5zMS6zk/p9J/72oWVkpsmq4KDwQf6Txq+7sKC0coO+qjAiLQWdy2hhS3qI76c4vsamkAWnh6lqD/LfvYT4Z6tyrGA8Oq3c4GpVkPWL+FudKnPxd0OAnD+Iff1lDncJfyPdL7cWqDrwtwL92LvdtbukYaM987TEYTzNpJUbGFtXkyP43ZiD6FO0tzvY4edixhfkvrEK/xqf76mKFzcbg+n+VER3E7/uxoLykjk0KjsmxfZPaUyh0bJ9rNOvffj+BU/G7kuSX3ecjgXdp/KRdxsmvskJ3U36mhjXSd1M4QSV2aiTuhv4mBnz2RQKlnH7qJ6xa/c7eRYxbEzown1ccxm2PqY7HOnbtoGlW8wkxjKTC2Oo1nd2dG+DacRsHg99tnNsmDOHQs9TRgc0IRLmfPq5jWE0wtyLYU1+EIaUxRkzfPDf1zNfza7jiMlzrP68C5V6fbgo9DWAAY6wveku59v7K0L687eXA868D2CcEnbLBGU14bV5wChqoYpiV5W5U2OGvY8tQWMyRWhQEmrmsTQxkKzt+T5Ht2ZlalwMJh/iSEf7d0lXYbDD91JlFuVkpjBCyezrxgw7iAOkxu2WXqmB/xeo/hjJ92F0a+DK3kUOk+KdHP7jICi8zFl+YbkfQMbwMTMmZ/qS7EJcaPXjR/JrtNFVaWevDDmyJm5nWEfe4iBK+JeZF+nCMoNuSg80LeEhf1GzHG8dP/jFIfW042B/eIhRgT1DDoWGIWr44Sz38ixZyZaBc7qlanicp5+i4jSG0fjKURkHcXuo6TKO4bE3k94GfJ9Ot7jKQRP+p4jhB6pyH4AMQbqdayiuavu3w2mbNwymRTSo38ZQvaLDP0xR1077Wvbxf3JSKr/FwaqU0AosR9TwXVTvEOXYXcUh+qvHLFX2DB53DvvaJlpf2TPsNkz4VUiay1BMW5rfeJGtI0p6oJ/jGOaxEP1bEKaxnK4xLzPYXQ305nlXibIuXErpygyoCziQmriMvsqFk3hmBNzpPonp7YBg7EA/k5hGUjzK8snYlmm04dig7d/K4Hd/wFfem303ONESvZSOazSAu+H+yr+0WpdJe5YjvW78bRbnTKUaFzhXlS8X1lY7sl/XquzfAFziUKs3k4wLOIbYko9ok20cyhZULO0C07uE7zeX0WbwjqpcWlLMPDbluKC7Mel/D1N+RnFNRVTexzoqd5FDFH6f5jrC0n6L11LIeznRwGWEldSL5Q4HOK4hMrZ8+3EgLwSs0DVb+qe9uQMySrfTn6SkAWMvXk4J5xJt2fcuUKT8Q1X4X9RHufCEZf/klRi6s840FZ6GycDRLMOSvO9yFDn+Dmsf10q2GJdK+zJ2a0ZKmhFTNzU1pU1wx6i8l/M2oMxtxh7oUH0b8XacQ0schgcDNl+Uip4q33HzlGnsT0WdJkbb8m3k5HALyv1xzfWdRZE3DRjxXiaYVYX2AddbGFG1hWvrIo0luRBMBIo0YCRImcBWJdblOsNyRcwcTuiErLUSxNmWonM5DZy1oWChr/kT3VzVoi/1XLobm04VSRJMYNdYDhg1jc7bxASLdLlDHWfBMy01/1heuRcHI7m4VQp6MG37mr/jq2E/cKWv2RvC8ytJ4K/ZS+Eiyh7c/hMX/iLKhFe12rvcXWiJcaOnv6q1ApcZj+fCVDH4y4xTuO57ccB136KGvzUgHn/dd5mxGQ2b6W6s2bo5p1/A2oq56cfWFOTpVrnMYj/urteVKzP8rSmt/aFajuyIfN7yG7QD0qMESPdzodphaGyV3EoVxwC+27ZYLkzBFouHQiPX4/U9vGbw10cL7i7RmoRHTPSi8cyV1W4sIG8YwuMEP0RYj/eoEDrk7QCnh4eHB7KHfwEqXCB8k+aWbQAAAABJRU5ErkJggg=='
    }

    const colors = {
        defaultColor: '#ffffff',
        yellowColor: '#ffff9f',
        greenColor: '#94df7f',
        blueColor: '#d6e6ff',
        redColor: '#ff958f',
    }

    const isHiddenIndividual = options?.isHiddenIndividual ? true : false

    let idCounter = 1

    const individualMap = new Map()

    const TYPE = {
        INDIVIDUAL_ITEM: 'INDIVIDUAL_ITEM',
        INDIVIDUAL_RESULT: 'INDIVIDUAL_RESULT'
    }

    const transformData = (node, parentId = null) => {
        let currentId = idCounter

        const { uuid } = node.current
        const childs = node.childrens ?? []
        const asfDataComponentList = node.current.asfDataComponentList || []
        const name = asfDataComponentList.find(item => item.id === 'kzm_textbox_name_abbrev')?.value || i18n.tr('Компания')
        const isResidence = (Number(asfDataComponentList.find(item => item.id === 'kzm_listbox_residence')?.key) ?? -1) === 1
        const isKzmStatus = (Number(asfDataComponentList.find(item => item.id === 'kzm_listbox_status')?.key) ?? -1) === 3
        const isIndividual = (Number(asfDataComponentList.find(item => item.id === 'kzm_listbox_type')?.key) ?? -1) === 1
        const dolyPercent = asfDataComponentList.find(item => item.id === 'kzm_procent_doly')?.key ?? ''

        const color = (() => {
            if(isResidence) {
                return colors.greenColor
            } else if(isKzmStatus) {
                return colors.yellowColor
            } else {
                return colors.blueColor
            }
        })()

        const image = images.baseImageCompany

        const tree = []

        if(isIndividual && isHiddenIndividual) {
            return tree
        }

        if(parentId && parentId === currentId) {
            parentId--;
        }

        if(isIndividual) {
            const rest = individualMap.get(parentId)

            const individualItem = {
                type: TYPE.INDIVIDUAL_ITEM,
                uuid,
                name,
                parentId,
                image,
                color,
                percent: dolyPercent
            }

            const individualData = rest ? [...rest, individualItem] : [individualItem]

            const individualItemList = individualData.filter(({ type }) => type === TYPE.INDIVIDUAL_ITEM)
            const individualResultFound = individualData.find(({ type }) => type === TYPE.INDIVIDUAL_RESULT)


            const individualPercentAll = String(individualItemList.reduce((acc, x) => acc + (!isNaN(parseFloat(x.percent)) ? parseFloat(x.percent) : 0), 0).toFixed(8))

            const individualResult = individualResultFound ? {
                ...individualResultFound,
                percent: individualPercentAll
            } : {
                type: TYPE.INDIVIDUAL_RESULT,
                uuid: null,
                name: i18n.tr('Физическое лицо'),
                parentId,
                image: images.baseImageIndividual,
                color: colors.redColor,
                percent: individualPercentAll
            }

            individualItemList.push(individualResult)

            individualMap.set(parentId, individualItemList)
            return tree
        }

        currentId = idCounter++

        console.log(currentId, name, isResidence, isKzmStatus, isIndividual, dolyPercent, color, image, parentId)

        tree.push({
            id: currentId,
            uuid,
            name,
            parentId,
            image,
            color,
            percent: dolyPercent
        })

        childs.forEach(child => {
            tree.push(...transformData(child, currentId))
        })

        return tree
    }

    const result = [
        {
            id: idCounter,
            uuid: null,
            name: i18n.tr('Конечный Бенефициар'),
            image: images.baseImageRoot,
            color: colors.defaultColor,
            percent: null
        },
        ...transformData(dataSource, idCounter++)
    ]

    result.push(...[...Array.from(individualMap).map(([_, values], key) => {
        const { type, ...rest } = values.find(({type}) => type === TYPE.INDIVIDUAL_RESULT)

        return {
            id: result.length + key + 1,
            ...rest
        }
    })])

    return result
}

function exportChartAsPdf(chart) {
    chart.exportImg({
        save: false,
        full: true,
        onLoad: (base64) => {
            var pdf = new jspdf.jsPDF();
            var img = new Image();
            img.src = base64;
            img.onload = function () {
                pdf.addImage(
                    img,
                    'JPEG',
                    5,
                    5,
                    595 / 3,
                    ((img.height / img.width) * 595) / 3
                );
                pdf.save('chart.pdf');
            };
        },
    });
}

pageHandler('corp_page', async () => {
    jQuery('button#button-org-structure-schema').off().on('click', async () => {
        try {
            Cons.showLoader()
            const queryParams = new URLSearchParams({
                registryCode: 'reestr_kontragenta',
                countInPart: 1,
                pageNumber: 0,
                filterCode: 'otnositsya_k_gk__kazahmys_',
                fields: 'kzm_reglink_subsidiaryCompany',
                sortCmpID: 'kzm_reglink_subsidiaryCompany',
                sortDesc: false,
            })

            const promiseGetFetch = (url) => new Promise((resolve, reject) => rest.synergyGet(url, resolve, reject))

            const {result, recordsCount} = await promiseGetFetch(`api/registry/data_ext?${queryParams.toString()}`)

            if (!recordsCount) {
                Cons.showMessage(i18n.tr('Данные не найдены. Проверьте реестр на существование вышестоящей записи'), 'danger')
                return
            }

            const {documentID} = result[0]

            const structureTreeUpdate = async () => {
                const treeStructure = await promiseGetFetch(`../../structure-tree/rest/api/tree?documentID=${documentID}`)
                console.log('treeStructure', treeStructure)
                return getStructureJsonData(treeStructure ?? [], { isHiddenIndividual })
            }

            const dataStructure = await structureTreeUpdate()

            const btnClasses = 'class="uk-button margined uk-button-default btn-login fonts" '
            const btnStyles = 'style="color: rgb(34, 34, 34);z-index: 0;box-sizing: border-box;direction: ltr;display: flex;align-items: center;justify-content: center;margin: 2px;padding: 0 10px;font-size: 14px;font-weight: normal;width: auto;height: 40px;text-wrap: nowrap;"'
            const dialog = UTILS.getFullModalDialog(i18n.tr('Организационная структура'), `<div class="uk-modal-body"><style>
#chart {
    width: 100%;
    height: 100%;
}
.node {
    cursor: pointer;
}
.uk-modal-body {
    width: 100%;
    height: 100%;
    background: #fff;
}
.uk-modal-header {
    height: 55px !important;
    display: flex;
    justify-content: left;
    align-items: center;
}
</style>
<div style="display: flex;width:100%;flex-wrap: wrap;">
<button name="chart-action-zoom" ${btnClasses}${btnStyles}>${i18n.tr('Увеличить масштаб')}</button>
<button name="chart-action-zoom-out" ${btnClasses}${btnStyles}>${i18n.tr('Уменьшить масштаб')}</button>
<button name="chart-action-expand-all" ${btnClasses}${btnStyles}>${i18n.tr('Раскрыть все')}</button>
<button name="chart-action-collapse-all" ${btnClasses}${btnStyles}>${i18n.tr('Скрыть все')}</button>
<button name="chart-action-compact-out" ${btnClasses}${btnStyles}>${i18n.tr('Убрать компактность')}</button>
<button name="chart-action-compact" ${btnClasses}${btnStyles}>${i18n.tr('Компаковать')}</button>
<button name="chart-action-layout-top" ${btnClasses}${btnStyles}>${i18n.tr('Отрисовать по верхнему краю')}</button>
<button name="chart-action-layout-right" ${btnClasses}${btnStyles}>${i18n.tr('Отрисовать по правому краю')}</button>
<button name="chart-action-layout-left" ${btnClasses}${btnStyles}>${i18n.tr('Отрисовать по левому краю')}</button>
<button name="chart-action-layout-bottom" ${btnClasses}${btnStyles}>${i18n.tr('Отрисовать по нижнему краю')}</button>
<button name="chart-action-fit" ${btnClasses}${btnStyles}>${i18n.tr('Выровнить структуру по центру')}</button>
<button name="chart-action-export-image" ${btnClasses}${btnStyles}>${i18n.tr('Экспортировать текущий')}</button>
<button name="chart-action-export-image-full" ${btnClasses}${btnStyles}>${i18n.tr('Экспортировать полностью')}</button>
<button name="chart-action-export-svg" ${btnClasses}${btnStyles}>${i18n.tr('Экспортировать как svg')}</button>
<button name="chart-action-export-pdf" ${btnClasses}${btnStyles}>${i18n.tr('Экспортировать как pdf')}</button>
<button name="chart-action-hide-invididual" ${btnClasses}${btnStyles}>${i18n.tr((isHiddenIndividual ? 'Показать' : 'Скрыть') + ' Физические лица')}</button>
</div>
<div id="OrgChart"></div></div>`)

            UIkit.modal(dialog).show()

            dialog.on('hidden', () => dialog.remove())

            dialog.on('shown', () => {
                let chart = null

                jQuery('button[name="chart-action-zoom"]').off().on('click', () => chart && chart.zoomIn())
                jQuery('button[name="chart-action-zoom-out"]').off().on('click', () => chart && chart.zoomOut())
                jQuery('button[name="chart-action-expand-all"]').off().on('click', () => chart && chart.expandAll().fit())
                jQuery('button[name="chart-action-collapse-all"]').off().on('click', () => chart && chart.collapseAll().fit())
                jQuery('button[name="chart-action-compact-out"]').off().on('click', () => chart && chart.compact(false).render().fit())
                jQuery('button[name="chart-action-compact"]').off().on('click', () => chart && chart.compact(true).render().fit())
                jQuery('button[name="chart-action-layout-top"]').off().on('click', () => chart && chart.layout('top').render().fit())
                jQuery('button[name="chart-action-layout-right"]').off().on('click', () => chart && chart.layout('right').render().fit())
                jQuery('button[name="chart-action-layout-left"]').off().on('click', () => chart && chart.layout('left').render().fit())
                jQuery('button[name="chart-action-layout-bottom"]').off().on('click', () => chart && chart.layout('bottom').render().fit())
                jQuery('button[name="chart-action-fit"]').off().on('click', () => chart && chart.fit())
                jQuery('button[name="chart-action-export-image"]').off().on('click', () => chart && chart.exportImg())
                jQuery('button[name="chart-action-export-image-full"]').off().on('click', () => chart && chart.exportImg({full:true}))
                jQuery('button[name="chart-action-export-svg"]').off().on('click', () => chart && chart.exportSvg())
                jQuery('button[name="chart-action-export-pdf"]').off().on('click', () => chart && exportChartAsPdf(chart))
                jQuery('button[name="chart-action-hide-invididual"]').off().on('click', function() {
                    $(this).text(i18n.tr((isHiddenIndividual ? 'Показать' : 'Скрыть') + ' Физические лица'))

                    isHiddenIndividual = !isHiddenIndividual

                    if(chart) {
                        chart.data(dataStructure).render()
                        chart.expandAll().fit()
                    }
                })

                chart = new d3.OrgChart()
                    .nodeHeight((d) => 75)
                    .childrenMargin((d) => 40)
                    .compactMarginBetween((d) => 40)
                    .compactMarginPair((d) => 150)
                    .neighbourMargin((a, b) => 20)
                    .onNodeClick(async (d, i, arr) => {
                        if(!d.data.uuid) {
                            return
                        }

                        const player = UTILS.getSynergyPlayer(d.data.uuid, true)

                        const dialogTitle = d.data.name
                        const dialogButton = i18n.tr("Сохранить")

                        const dialog = await UTILS.getModalDialog(dialogTitle, player.view.container, dialogButton, async (e) => {
                            e.stopPropagation()
                            e.preventDefault()

                            try {
                                Cons.showLoader()

                                if (!player.model.isValid()) {
                                    showMessage(i18n.tr('Заполните обязательные поля'), 'danger')
                                    return
                                }

                                player.saveFormData(async () => {
                                    showMessage(i18n.tr('Данные сохранены', 'success'))
                                    UIkit.modal(dialog).hide()
                                    player.destroy()

                                    if(chart) {
                                        const dataStructure = await structureTreeUpdate()
                                        chart.data(dataStructure).render()
                                        chart.expandAll().fit()
                                    }
                                })
                            } catch (err) {
                                console.error(err)
                                UIkit.modal(dialog).hide()
                                player.destroy()
                                showMessage(err.message, 'error')
                            } finally {
                                Cons.hideLoader()
                            }
                        })

                        dialog.find('.uk-modal-dialog').css({
                            width: '100%',
                        })

                        dialog.find('.uk-modal-body').css({
                            'max-height': undefined,
                            padding: '0 25px'
                        })

                        dialog.on('hidden', () => {
                            dialog.remove()
                        })

                        UIkit.modal(dialog, {stack: true}).show()
                    })
                    .nodeContent(function (d, i, arr, state) {
                        const color = d.data.color ?? '#ECECEC75'
                        const imageDiffVert = 25 + 2
                        return `
                    <div style='width:${
                            d.width
                        }px;padding-top:${imageDiffVert - 2}px;padding-left:1px;padding-right:1px'>
                        <div style="font-family: 'Inter', sans-serif;background-color:${color};margin-left:-1px;width:${d.width - 2}px;height:${d.height - imageDiffVert}px;border-radius:10px;border: ${d.data._highlighted || d.data._upToTheRootHighlighted ? '5px solid #E27396"' : '1px solid #E4E2E9"'} >
                            <div style="background-color:#ffffff;border: 2px solid ${color};margin-top:${-imageDiffVert + 5}px;margin-left:${15}px;border-radius:100px;width:40px;height:40px;" ></div>
                            <div style="margin-top:${
                            -imageDiffVert - 10
                        }px;">   <img src="${d.data.image}" style="margin-left:${20}px;border-radius:100px;width:32px;height:32px;" /></div>
                            <div style="font-size:10px;overflow: hidden;text-overflow: ellipsis;display: -webkit-box;-webkit-line-clamp: 1;-webkit-box-orient: vertical;white-space: normal;color:#08011E;text-align:center;margin-top:2px">  ${
                            d.data.name
                        } </div>
                            ${d.data.percent ? `<div style="color:#716E7B;margin-left:20px;margin-top:3px;font-size:10px;"> ${
                            d.data.percent
                        } </div>` : ''}

                        </div>
                `
                    })
                    .container('#OrgChart')
                    .data(dataStructure)
                    .render()

                chart.expandAll().fit()
                console.log('chart',chart)
            })
        } catch (e) {
            console.error(e)
            Cons.showMessage(i18n.tr('Произошла непредвиденная ошибка. Повторите попытку позже.'), 'danger')
        } finally {
            Cons.hideLoader()
        }
    })
})
