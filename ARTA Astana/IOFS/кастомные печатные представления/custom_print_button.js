/*
model.printFormat = ['PDF', 'DOC']; //список форматов для скачивания печатного предствления
model.reportCode = 'ishodyaschee_pismo' //код отчета (печатного представления)
*/

function initCustomButton(){
  if(!model.reportCode) return; //если не указан код отчета, ниче не делаем
  $('.iofs-print-button').remove(); //грохаем кнопки если уже есть
  let originPrintButton = $('[src="light/images/simple.button/dark.gray.slim/print.png"]').closest('table').parent().closest('table');
  originPrintButton.hide(); //прячем оригинальную кнопку

  AS.FORMS.ApiUtils.getSigns(model.playerModel.asfDataId, 1).then(signs => { //ищем утверждения
    let director = model.playerModel.getModelWithId('entity_user'); //берем юзера указанного на форме

    if(director && director.getValue()) {
      director = director.getValue();
      let dd = signs.filter(x => x.name == director[0].personName); //есть ли утверждение директора
      if(!dd.length) return; //если нет ниче не делаем

      let container = originPrintButton.closest('tr');
      let td = $('<td align="left" style="vertical-align: top;">');
      let customPrintButton = $('<button>', {class: "iofs-print-button"});
      let items = model.printFormat ? model.printFormat.map(x => ({title: x, value: x.toLowerCase()})) : [{title: 'PDF', value: 'pdf'}]; //формруем нужный(правильный) список для синержевского dropDown

      container.append(td.append(customPrintButton)); //вставляем свою кнопку
      customPrintButton.on('click', e => {
        e.preventDefault();
        e.target.blur();
        AS.SERVICES.showDropDown(items, customPrintButton, 100, value => print(value)); //Открываем синержевский dropDown
      });
    }
  });
}

function print(format) {
  let currentDocumentID = '';
  let regNumber = '';
  let regDate = '';
  AS.SERVICES.showWaitWindow(); //Показываем спинер
  AS.FORMS.ApiUtils.getDocumentIdentifier(model.playerModel.asfDataId) //Получаем documentID по uuid данных по форме
  .then(docID => {
    currentDocumentID = docID;
    return AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/docflow/doc/document_info?documentID=${docID}`) //получаем инфу по документу
  })
  .then(info => {
    regNumber = info.number;
    regDate = info.regDate;
    return AS.FORMS.ApiUtils.simpleAsyncGet('rest/api/report/list') //получаем информацию о доступных текущему пользователю отчетов
  })
  .then(reports => {
    let reportParam = reports.filter(x => x.code == model.reportCode)[0]; //фильтруем список и получаем параметры нужного нам отчета
    let reportURL = "formreport?reportid=" + reportParam.reportID
    + "&inline=false&filename=" + (reportParam.defaultName.split('.')[0] + '.' + format)
    + "&documentID=" + currentDocumentID
    + "&regNumber=" + regNumber
    + "&regDate=" + regDate
    + "&locale=" + AS.OPTIONS.locale; //формируем урл отчета
    window.open(reportURL); //открываем отчет
    AS.SERVICES.hideWaitWindow(); //прячем спинер
  });
}

if (!editable) {
  //Инициализируем кнопку если форма в режиме просмотра
  initCustomButton();
} else {
  //Иначе грохаем если есть
  $('.iofs-print-button').remove();
}
