const setNewValue = value => {
  if(!editable) return;
  if(!value) return;
  AS.FORMS.ApiUtils.loadDictionary(model.asfProperty.dataSource.dict, AS.OPTIONS.locale, data => {
    let unit_code = data.columns.find(x => x.code == 'unit_code').columnID;
    let id = data.columns.find(x => x.code == 'id').columnID;
    data.items.forEach(item => {
      item.values.forEach(x => {
        if(x.columnID == id && x.value == value[0]) {
          item.values.forEach(val => {
            if(val.columnID == unit_code)
            model.playerModel.getModelWithId('listbox_unit_1', model.asfProperty.ownerTableId, model.asfProperty.tableBlockIndex).setValue(val.value);
          });
        }
      });
    });
  });
}
model.on('valueChange', (_1, _2, value) => setNewValue(value));
