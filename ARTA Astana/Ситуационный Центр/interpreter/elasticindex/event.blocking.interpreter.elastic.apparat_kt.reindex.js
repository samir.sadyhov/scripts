var result = true;
var message = "ok";

let ELASTIC_HOST = 'localhost:9200';

function checkElasticServer(){
  let client = new org.apache.commons.httpclient.HttpClient();
  let get = new org.apache.commons.httpclient.methods.GetMethod("http://" + ELASTIC_HOST);
  client.executeMethod(get);
  get.getResponseBodyAsString();
  get.releaseConnection();
}

function putInIndex(data, elasticKey) {
  let client = new org.apache.commons.httpclient.HttpClient();
  let put = new org.apache.commons.httpclient.methods.PutMethod("http://"+ELASTIC_HOST+"/kt_apparat/kt/" + encodeURIComponent(elasticKey));
  let body = new org.apache.commons.httpclient.methods.StringRequestEntity(JSON.stringify(data), "application/json", "UTF-8");
  put.setRequestEntity(body);
  client.executeMethod(put);
  let resp = put.getResponseBodyAsString();
  put.releaseConnection();
  return JSON.parse(resp);
}

function formatDate(date) {
  date = date.split(' ');
  return date[0] + 'T' + date[1] + '.000Z';
}

function getUrlSearch(id){
  let fields = ['date_create_data', 'numericinput_all_apparate', 'numericinput_serviceable_count', 'formula_not_serviceable'];
  let url = 'rest/api/registry/data_ext?registryCode=reestr_kt_apparatov&field=listbox_organizations&key=' + id;
  url += '&condition=EQUALS&sortCmpID=date_create_data&sortDesc=false&countInPart=1';
  fields.forEach(function(field){url += '&fields=' + field});
  return url;
}

try {
  checkElasticServer(); // В случае недоступности сервера вернет ошибку:
                        // "В соединении отказано (Connection refused)"
                        // дальнейшее выполнение БП будет невозможно

  let organization = [];
  let dict = API.httpGetMethod('rest/api/dictionaries/meditsinskie_organizatsii');

  for (let key in dict.items) {
    if (dict.items[key].id.value == "0") continue;
    organization.push({name: dict.items[key].name.value, id: dict.items[key].id.value});
  }

  organization.forEach(function(org){
    let searchResult = API.httpGetMethod(getUrlSearch(org.id));
    if(searchResult.recordsCount > 0) {
      let indexData = {organizationID: org.id, organizationName: org.name};
      let fieldKey = searchResult.result.length > 0 ? searchResult.result[0].fieldKey : null;
      let fieldValue = searchResult.result.length > 0 ? searchResult.result[0].fieldValue : null;

      indexData.date_create_key = fieldKey ? fieldKey.date_create_data : '';
      indexData.date_create_value = fieldValue ? fieldValue.date_create_data : '';
      indexData.date_create_date = fieldKey ?  formatDate(fieldKey.date_create_data) : '';

      indexData.all_apparate = fieldKey ? Number(fieldKey.numericinput_all_apparate) : 0;
      indexData.serviceable = fieldKey ? Number(fieldKey.numericinput_serviceable_count) : 0;
      indexData.not_serviceable = fieldKey ? Number(fieldKey.formula_not_serviceable) : 0;

      let resultAddIndex = putInIndex(indexData, "organization_" + org.id);
      log.info(":::::: RESULT ADD INDEX ::::::", resultAddIndex);
    }
  });

} catch (err) {
  log.error('[ INDEX kt_apparat ] ' + err.message);
  message = err.message;
}
