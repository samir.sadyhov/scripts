var result = true;
var message = "ok";

let ELASTIC_HOST = 'localhost:9200';

function checkElasticServer(){
  let client = new org.apache.commons.httpclient.HttpClient();
  let get = new org.apache.commons.httpclient.methods.GetMethod("http://" + ELASTIC_HOST);
  client.executeMethod(get);
  get.getResponseBodyAsString();
  get.releaseConnection();
}

function putInIndex(data, elasticKey) {
  let client = new org.apache.commons.httpclient.HttpClient();
  let put = new org.apache.commons.httpclient.methods.PutMethod("http://"+ELASTIC_HOST+"/stat_of_places/mo/" + encodeURIComponent(elasticKey));
  let body = new org.apache.commons.httpclient.methods.StringRequestEntity(JSON.stringify(data), "application/json", "UTF-8");
  put.setRequestEntity(body);
  client.executeMethod(put);
  let resp = put.getResponseBodyAsString();
  put.releaseConnection();
  return JSON.parse(resp);
}

function formatDate(date) {
  date = date.split(' ');
  return date[0] + 'T' + date[1] + '.000Z';
}

try {
  checkElasticServer(); // В случае недоступности сервера вернет ошибку:
                        // "В соединении отказано (Connection refused)"
                        // дальнейшее выполнение БП будет невозможно

  let currentFormData = API.getFormData(dataUUID);
  let organization = UTILS.getValue(currentFormData, 'listbox_organization');

  if(organization.key == "0") throw new Error("Не выбрана организация, пропускаем запись");

  let indexData = {
    organizationID: organization.key,
    organizationName: organization.value
  };

  let date_create = UTILS.getValue(currentFormData, 'date_create_data');
  let place_count = UTILS.getValue(currentFormData, 'numericinput_place_count');
  let place_count_prov = UTILS.getValue(currentFormData, 'numericinput_place_count_prov');
  let place_used = UTILS.getValue(currentFormData, 'numericinput_place_used');
  let place_used_prov = UTILS.getValue(currentFormData, 'numericinput_place_used_prov');
  let formula_free = UTILS.getValue(currentFormData, 'formula_free');
  let formula_free_prov = UTILS.getValue(currentFormData, 'formula_free_prov');

  indexData.date_create_key = date_create.key;
  indexData.date_create_value = date_create.value;
  indexData.date_create_date = formatDate(date_create.key);

  indexData.place_count = Number(place_count.key);
  indexData.place_count_prov = Number(place_count_prov.key);
  indexData.place_count_sum = indexData.place_count + indexData.place_count_prov;

  indexData.place_used = Number(place_used.key);
  indexData.place_used_prov = Number(place_used_prov.key);
  indexData.place_used_sum = indexData.place_used + indexData.place_used_prov;

  indexData.formula_free = Number(formula_free.key);
  indexData.formula_free_prov = Number(formula_free_prov.key);
  indexData.formula_free_sum = indexData.formula_free + indexData.formula_free_prov;

  let resultAddIndex = putInIndex(indexData, "organization_" + organization.key);
  log.info(":::::: RESULT ADD INDEX ::::::", resultAddIndex);

} catch (err) {
  log.error('[ INDEX stat_of_places ] ' + err.message);
  message = err.message;
}
