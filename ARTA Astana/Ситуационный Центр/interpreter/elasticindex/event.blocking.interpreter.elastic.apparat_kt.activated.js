var result = true;
var message = "ok";

let ELASTIC_HOST = 'localhost:9200';

function checkElasticServer(){
  let client = new org.apache.commons.httpclient.HttpClient();
  let get = new org.apache.commons.httpclient.methods.GetMethod("http://" + ELASTIC_HOST);
  client.executeMethod(get);
  get.getResponseBodyAsString();
  get.releaseConnection();
}

function putInIndex(data, elasticKey) {
  let client = new org.apache.commons.httpclient.HttpClient();
  let put = new org.apache.commons.httpclient.methods.PutMethod("http://"+ELASTIC_HOST+"/kt_apparat/kt/" + encodeURIComponent(elasticKey));
  let body = new org.apache.commons.httpclient.methods.StringRequestEntity(JSON.stringify(data), "application/json", "UTF-8");
  put.setRequestEntity(body);
  client.executeMethod(put);
  let resp = put.getResponseBodyAsString();
  put.releaseConnection();
  return JSON.parse(resp);
}

function formatDate(date) {
  date = date.split(' ');
  return date[0] + 'T' + date[1] + '.000Z';
}

try {
  checkElasticServer(); // В случае недоступности сервера вернет ошибку:
                        // "В соединении отказано (Connection refused)"
                        // дальнейшее выполнение БП будет невозможно

  let currentFormData = API.getFormData(dataUUID);
  let organization = UTILS.getValue(currentFormData, 'listbox_organizations');

  if(organization.key == "0") throw new Error("Не выбрана организация, пропускаем запись");

  let indexData = {
    organizationID: organization.key,
    organizationName: organization.value
  };

  let date_create = UTILS.getValue(currentFormData, 'date_create_data');

  let all_apparate = UTILS.getValue(currentFormData, 'numericinput_all_apparate');
  let serviceable = UTILS.getValue(currentFormData, 'numericinput_serviceable_count');
  let not_serviceable = UTILS.getValue(currentFormData, 'formula_not_serviceable');

  indexData.date_create_key = date_create.key;
  indexData.date_create_value = date_create.value;
  indexData.date_create_date = formatDate(date_create.key);

  indexData.all_apparate = Number(all_apparate.key) || 0;
  indexData.serviceable = Number(serviceable.key) || 0;
  indexData.not_serviceable = Number(not_serviceable.key) || 0;

  let resultAddIndex = putInIndex(indexData, "organization_" + organization.key);
  log.info(":::::: RESULT ADD INDEX ::::::", resultAddIndex);

} catch (err) {
  log.error('[ INDEX kt_apparat ] ' + err.message);
  message = err.message;
}
