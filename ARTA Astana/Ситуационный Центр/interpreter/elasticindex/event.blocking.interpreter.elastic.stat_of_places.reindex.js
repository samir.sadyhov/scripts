var result = true;
var message = "ok";

let ELASTIC_HOST = 'localhost:9200';

function checkElasticServer(){
  let client = new org.apache.commons.httpclient.HttpClient();
  let get = new org.apache.commons.httpclient.methods.GetMethod("http://" + ELASTIC_HOST);
  client.executeMethod(get);
  get.getResponseBodyAsString();
  get.releaseConnection();
}

function putInIndex(data, elasticKey) {
  let client = new org.apache.commons.httpclient.HttpClient();
  let put = new org.apache.commons.httpclient.methods.PutMethod("http://"+ELASTIC_HOST+"/stat_of_places/mo/" + encodeURIComponent(elasticKey));
  let body = new org.apache.commons.httpclient.methods.StringRequestEntity(JSON.stringify(data), "application/json", "UTF-8");
  put.setRequestEntity(body);
  client.executeMethod(put);
  let resp = put.getResponseBodyAsString();
  put.releaseConnection();
  return JSON.parse(resp);
}

function formatDate(date) {
  date = date.split(' ');
  return date[0] + 'T' + date[1] + '.000Z';
}

function getUrlSearch(id){
  let fields = ['date_create_data', 'numericinput_place_count', 'numericinput_place_used','formula_free','numericinput_place_count_prov','numericinput_place_used_prov','formula_free_prov'];
  let url = 'rest/api/registry/data_ext?registryCode=reestr_mest_v_mo&field=listbox_organization&key=' + id;
  url += '&condition=EQUALS&sortCmpID=date_create_data&sortDesc=false&countInPart=1';
  fields.forEach(function(field){url += '&fields=' + field});
  return url;
}

try {
  checkElasticServer(); // В случае недоступности сервера вернет ошибку:
                        // "В соединении отказано (Connection refused)"
                        // дальнейшее выполнение БП будет невозможно

  let organization = [];
  let dict = API.httpGetMethod('rest/api/dictionaries/meditsinskie_organizatsii');

  for (let key in dict.items) {
    if (dict.items[key].id.value == "0") continue;
    organization.push({
      name: dict.items[key].name.value,
      id: dict.items[key].id.value
    });
  }

  organization.forEach(function(org){
    let searchResult = API.httpGetMethod(getUrlSearch(org.id));
    let indexData = {organizationID: org.id, organizationName: org.name};
    let fieldKey = searchResult.result.length > 0 ? searchResult.result[0].fieldKey : null;
    let fieldValue = searchResult.result.length > 0 ? searchResult.result[0].fieldValue : null;

    indexData.date_create_key = fieldKey ? fieldKey.date_create_data : '';
    indexData.date_create_value = fieldValue ? fieldValue.date_create_data : '';
    indexData.date_create_date = fieldKey ?  formatDate(fieldKey.date_create_data) : '';

    indexData.place_count = fieldKey ? Number(fieldKey.numericinput_place_count) : 0;
    indexData.place_count_prov = fieldKey ? Number(fieldKey.numericinput_place_count_prov) : 0;
    indexData.place_count_sum = indexData.place_count + indexData.place_count_prov;

    indexData.place_used = fieldKey ? Number(fieldKey.numericinput_place_used) : 0;
    indexData.place_used_prov = fieldKey ? Number(fieldKey.numericinput_place_used_prov) : 0;
    indexData.place_used_sum = indexData.place_used + indexData.place_used_prov;

    indexData.formula_free = fieldKey ? Number(fieldKey.formula_free) : 0;
    indexData.formula_free_prov = fieldKey ? Number(fieldKey.formula_free_prov) : 0;
    indexData.formula_free_sum = indexData.formula_free + indexData.formula_free_prov;

    let resultAddIndex = putInIndex(indexData, "organization_" + org.id);
    log.info(":::::: RESULT ADD INDEX ::::::", resultAddIndex);

  });

} catch (err) {
  log.error('[ INDEX stat_of_places ] ' + err.message);
  message = err.message;
}
