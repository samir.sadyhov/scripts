var result = true;
var message = "ok";

function customFormatDate(datetime){
  datetime = datetime.split(/\D/);
  return datetime[2] + '.' + datetime[1] + '.' + datetime[0] + ' ' + datetime[3] + ':' + datetime[4];
}

try {
  let currentFormData = API.getFormData(dataUUID);
  let currentDate = UTILS.getCurrentDateParse();
  let organization = UTILS.getValue(currentFormData, 'listbox_organization');

  UTILS.setValue(currentFormData, 'date_create_data', {
    value: customFormatDate(currentDate),
    key: currentDate
  });

  if(organization && organization.key != "0") {
    let urlSearch = 'rest/api/registry/data_ext?registryCode=registry_information_on_the_total_number_of_hospital_beds&countInPart=1'
    + '&field=listbox_organization'
    + '&key=' + organization.key
    + '&condition=EQUALS'
    + '&fields=numericinput_place_count&fields=numericinput_place_count_prov';

    let result = API.httpGetMethod(urlSearch);

    if(result.recordsCount != '0') {
      result = result.result[0].fieldKey;

      UTILS.setValue(currentFormData, "numericinput_place_count", {
        key: result['numericinput_place_count'],
        value: result['numericinput_place_count']
      });

      UTILS.setValue(currentFormData, "numericinput_place_count_prov", {
        key: result['numericinput_place_count_prov'],
        value: result['numericinput_place_count_prov']
      });
    } else {
      message = "Не найдено информации по " + organization.value;
    }

  } else {
    message = "Не выбрана организация";
  }

  API.saveFormData(currentFormData);

} catch (err) {
  log.error(err.message);
  message = err.message;
}
