var result = true;
var message = "ok";

function customFormatDate(datetime){
  datetime = datetime.split(/\D/);
  return datetime[2] + '.' + datetime[1] + '.' + datetime[0] + ' ' + datetime[3] + ':' + datetime[4];
}

try {
  let currentFormData = API.getFormData(dataUUID);
  let currentDate = UTILS.getCurrentDateParse();
  let organization = UTILS.getValue(currentFormData, 'listbox_organizations');

  UTILS.setValue(currentFormData, 'date_create_data', {
    value: customFormatDate(currentDate),
    key: currentDate
  });

  if(organization && organization.key != "0") {
    let urlSearch = 'rest/api/registry/data_ext?registryCode=registry_ivl_count&countInPart=1'
    + '&field=listbox_organizations'
    + '&condition=EQUALS'
    + '&key=' + organization.key
    + '&fields=numericinput_all_apparate';

    let result = API.httpGetMethod(urlSearch);

    if(result.recordsCount != '0') {
      result = result.result[0].fieldKey;

      UTILS.setValue(currentFormData, "numericinput_all_apparate", {
        key: result['numericinput_all_apparate'],
        value: result['numericinput_all_apparate']
      });
    } else {
      message = "Не найдено информации по " + organization.value;
    }
  } else {
    message = "Не выбрана организация";
  }

  API.saveFormData(currentFormData);

} catch (err) {
  log.error(err.message);
  message = err.message;
}
