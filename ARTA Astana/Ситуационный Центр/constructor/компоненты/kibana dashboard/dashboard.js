let dashboardURL = window.location.origin + '/kibana/app/kibana#/dashboard/Статистика-по-МО?embed=true';
let refreshPeriodInSecond = 60; // 1 минута
let defaultParam = `&_g=(refreshInterval:(display:Off,pause:!f,value:${refreshPeriodInSecond*1000}))`;
let newURL = dashboardURL + defaultParam;

let container = $('.dashboard-container');
let iFrame = $(`<iframe id="dashboard-frame" src="${newURL}" width="100%" height="100%">`);

container.append(iFrame);

setTimeout(() => {
  let iFrameHead = $('#dashboard-frame').contents().find("head");
  let iFrameCSS = `
  <style type="text/css">
    .c3-axis-x .tick text {
      fill: #000 !important;
      font: 9px sans-serif;
      word-spacing: -2.3px;
    }
    .gridster li,
    .gridster li:hover {
      border: none !important;
    }
  </style>`;
  $(iFrameHead).append(iFrameCSS);
}, 1000);
