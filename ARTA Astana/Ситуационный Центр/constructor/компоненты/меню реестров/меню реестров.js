let registryList = getCompByCode("registry-list");
let regMenu = $('.registry-menu');
regMenu.empty();

const getSettings = registryCode => settings.find(x => x.registryCode == registryCode);

const getTableBlockIndex = tableData => {
  let id = tableData[tableData.length - 1].id;
  return Number(id.slice(id.indexOf('-b') + 2));
}

const getErrorMessage = errors => {
  let msg = 'Не заполнены обязательные поля';
  errors.forEach(x => {
    if(!AS.FORMS.INPUT_ERROR_TYPE.hasOwnProperty(x.errorCode)) msg = x.errorCode;
  });
  return msg;
}

const drawRegMenu = () => {
  settings.forEach(item => {
    if(item.show) {
      let button = $(`<button class="registry-menu-item" registrycode="${item.registryCode}">${item.labelName}</button>`);
      button.attr('title', item.labelName);

      button.on('click', e => {
        if(registryList.registryCode == item.registryCode) return;
        $('.registry-menu-item').removeClass('active');
        button.addClass('active');
        $('#label-registry span').text(item.labelName);
        fire({type: 'registry_code_change', code: item.registryCode}, registryList.code);
        fire({type: 'registry_page_change', page: 0}, registryList.code);
        registryList.registryCode = item.registryCode;
      });

      regMenu.append(button);
    }
  });

  let paramRegCode = getPageParam("registryCode");
  let info = getSettings(paramRegCode);
  $(`[registrycode="${paramRegCode}"]`).addClass('active');
  $('#label-registry span').text(info.labelName);

  Cons.hideLoader();
}


Cons.showLoader();
let settings = Cons.getAppStore().settings;
if(!settings) {
  settings = [];
  AS.FORMS.ApiUtils.simpleAsyncGet('rest/api/registry/data_ext?registryCode=nastroiki_dlya_portala&countInPart=1&loadData=false')
  .then(res => AS.FORMS.ApiUtils.loadAsfData(res.data[0].dataUUID))
  .then(asfData => {
    let table = asfData.data.find(x => x.id == 'form_and_reg_table').data;
    let rows = ['labelName', 'formCode', 'registryCode', 'show', 'listbox_editable'];
    for(let i = 1; i <= getTableBlockIndex(table); i++) {
      let param = {};
      rows.forEach(rowID => {
        let tmp = table.find(x => x.id == `${rowID}-b${i}`);
        param[rowID] = tmp.value;
      });
      settings.push(param);
    }
    settings = settings.filter(x => x.show == 'Да');
    Cons.setAppStore({settings: settings});
    drawRegMenu();
  });
} else {
  drawRegMenu();
}

const getModalDialog = (title, player, editable) => {
  let dialog = $('<div class="uk-modal-container" uk-modal="bg-close: false">');
  let md = $('<div class="uk-modal-dialog uk-margin-auto-vertical">');
  let modalHeader = $(`<div class="uk-modal-header" style="background: #1e87f0; padding: 10px 30px;"><h4 style="color: #fff;">${title}</h4></div>`);
  let modalBody = $('<div class="uk-modal-body" uk-overflow-auto>');
  let footer = $('<div class="uk-modal-footer uk-text-right">');

  md.append('<button class="uk-modal-close-default" type="button" style="color: #fff;" uk-close></button>').append(modalHeader);

  if(editable) {
    let actionButtonContainer = $('<div class="uk-text-right" style="margin: 10px 30px;">');
    let saveButton = $('<button class="uk-button uk-button-default uk-button-small uk-margin-small-right" type="button"><span uk-icon="icon: check"></span> Сохранить</button>');
    saveButton.hide();
    saveButton.on('click', e => {
      if (!player.model.isValid()) {
        showMessage('Заполните обязательные поля','error');
        return;
      }
      Cons.showLoader();
      player.saveFormData(uuid => {
        Cons.hideLoader();
        showMessage('Данные сохранены','success');
        UIkit.modal(dialog).hide();
      });
    });

    let editButton = $('<button class="uk-button uk-button-default uk-button-small uk-margin-small-right" type="button"><span uk-icon="icon: pencil"></span> Редактировать</button>');
    editButton.on('click', e => {
      player.view.setEditable(!player.view.editable);
      if(player.view.editable) {
        editButton.html('<span uk-icon="icon: file-text"></span> Просмотр');
        saveButton.show();
      } else {
        editButton.html('<span uk-icon="icon: pencil"></span> Редактировать');
        saveButton.hide();
      }
    });

    actionButtonContainer.append(saveButton).append(editButton);
    md.append(actionButtonContainer);
  }

  modalBody.append(player.view.container);
  footer.append('<button class="uk-button uk-button-primary uk-modal-close" type="button">Закрыть</button>');
  dialog.append(md);
  md.append(modalBody).append(footer);
  return dialog;
}

if(!Cons.getAppStore().registry_item_click){
  addListener('registry_item_click', registryList.code, e => {
    let opt = getSettings(registryList.registryCode);
    let player = AS.FORMS.createPlayer();
    player.view.setEditable(false);
    player.showFormData(null, null, e.dataUUID);
    let dialog = getModalDialog($('#label-registry span').text(), player, opt.listbox_editable == 'Да');
    UIkit.modal(dialog).show();
    dialog.on('hidden', () => dialog.remove());
  });
  Cons.setAppStore({registry_item_click: true});
}
