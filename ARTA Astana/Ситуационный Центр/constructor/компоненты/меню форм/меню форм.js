let formPlayer = getCompByCode("formPlayer-1");
let buttonSend = $('#button-send-app');
let formMenu = $('.form-menu');
formMenu.empty();

const getSettings = formCode => settings.find(x => x.formCode == formCode);

const getTableBlockIndex = tableData => {
  let id = tableData[tableData.length - 1].id;
  return Number(id.slice(id.indexOf('-b') + 2));
}

const getErrorMessage = errors => {
  let msg = 'Не заполнены обязательные поля';
  errors.forEach(x => {
    if(!AS.FORMS.INPUT_ERROR_TYPE.hasOwnProperty(x.errorCode)) msg = x.errorCode;
  });
  return msg;
}

const drawFormMenu = () => {
  settings.forEach(item => {
    if(item.show) {
      let button = $(`<button class="form-menu-item">${item.labelName}</button>`);
      button.attr('title', item.labelName);

      button.on('click', e => {
        if(formPlayer.model.formCode == item.formCode) return;
        Cons.showLoader();
        $('.form-menu-item').removeClass('active');
        button.addClass('active');
        $('#label-form span').text(item.labelName);
        fire({type: 'show_form', formCode: item.formCode}, 'formPlayer-1');
        setTimeout(() => {Cons.hideLoader()}, 500);
      });

      formMenu.append(button);
    }
  });
  $($('.form-menu-item')[0]).click();
  Cons.hideLoader();
}

const sendApp = registryCode => {
  let body = {
    registryCode: registryCode,
    sendToActivation: true,
    data: formPlayer.model.getAsfData().data
  };
  AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/registry/create_doc_rcc", res => {
    if(res.errorCode != '0') throw new Error(res.errorMessage);
    setTimeout(() => {
      Cons.hideLoader();
      showMessage("Запись успешно создана", 'success');
      fire({
        type: 'goto_page',
        pageCode: 'registries',
        pageParams: [
          {pageParamID: 'bb4be837-4cc7-49f4-aec9-bb2e0381a987', pageParamName: 'registryCode', value: registryCode}
        ]
      }, comp.code);
    }, 100)
  }, null, JSON.stringify(body), "application/json; charset=utf-8", err => {
    throw new Error(err.responseText);
  });
}

const checkRegistryEntry = (regCode, cmpMO, msg) => {
  let org = formPlayer.model.getModelWithId(cmpMO).getValue();
  let urlSearch = `api/registry/data_ext?registryCode=${regCode}&countInPart=1&field=${cmpMO}&condition=EQUALS&key=${org[0]}&loadData=false`;
  rest.synergyGet(urlSearch, res => {
    if(res.count > 0) {
      Cons.hideLoader();
      UIkit.modal.alert(msg);
    } else {
      sendApp(regCode);
    }
  });
}

Cons.showLoader();
let settings = Cons.getAppStore().settings;
if(!settings) {
  settings = [];
  AS.FORMS.ApiUtils.simpleAsyncGet('rest/api/registry/data_ext?registryCode=nastroiki_dlya_portala&countInPart=1&loadData=false')
  .then(res => AS.FORMS.ApiUtils.loadAsfData(res.data[0].dataUUID))
  .then(asfData => {
    let table = asfData.data.find(x => x.id == 'form_and_reg_table').data;
    let rows = ['labelName', 'formCode', 'registryCode', 'show', 'listbox_editable'];
    for(let i = 1; i <= getTableBlockIndex(table); i++) {
      let param = {};
      rows.forEach(rowID => {
        let tmp = table.find(x => x.id == `${rowID}-b${i}`);
        param[rowID] = tmp.value;
      });
      settings.push(param);
    }
    settings = settings.filter(x => x.show == 'Да');
    Cons.setAppStore({settings: settings});
    drawFormMenu();
  });
} else {
  drawFormMenu();
}

buttonSend.on('click', e => {
  if(!formPlayer.model.isValid()) {
    showMessage(getErrorMessage(formPlayer.model.getErrors()),'error');
  } else {
    Cons.showLoader();
    try {
      let opt = getSettings(formPlayer.model.formCode);

      switch (opt.registryCode) {
        case "registry_ivl_count":
          checkRegistryEntry(opt.registryCode, 'listbox_organizations', 'По данной МО уже имеется запись. Пройдите в блок "Реестры" в "Количество аппаратов ИВЛ" и внесите изменения в существующую запись по данной МО');
          break;
        case "registry_information_on_the_total_number_of_hospital_beds":
          checkRegistryEntry(opt.registryCode, 'listbox_organization', 'По данной МО уже имеется запись. Пройдите в блок "Реестры" в "Информация по общему количеству койко мест в стационарах" и внесите изменения в существующую запись по данной МО');
          break;
        case "registry_information_on_the_daily_capacity_of_the_laboratory":
          checkRegistryEntry(opt.registryCode, 'listbox_organizations', 'По данной Лаборатории уже имеется запись. Пройдите в блок "Реестры" в "Информация по суточной мощности лаборатории" и внесите изменения в существующую запись по данной Лаборатории');
          break;
        default:
          sendApp(opt.registryCode);
      }

    } catch (e) {
      Cons.hideLoader();
      console.log(e.message, e);
      showMessage("Произошла ошибка при создании документа", 'error');
    }
  }
});
