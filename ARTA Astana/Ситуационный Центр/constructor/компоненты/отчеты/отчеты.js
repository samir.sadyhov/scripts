let Reports = {
  reportList: null,
  registries: null,
  reportURL: null,
  reportName: null,
  listValues: {},
  dicts: ['meditsinskie_organizatsii', 'meditsinskie_organizatsii__polikliniki_i_chastnye_kliniki_', 'drugs', 'lekarstvennaya_forma_preparata', 'edinitsa_izmereniya_preparatov'],

  getSystemReportExcel(registryID) {
    let regInfo = this.registries.find(x => x.registryID == registryID);
    window.open(`${window.location.origin}/Synergy/rest/reg/load/xls?r=${registryID}&l=ru&f=&s=&u=${AS.OPTIONS.currentUser.userid}&fn=${regInfo.registryCode}`);
  },

  reportDownload() {
    Cons.showLoader();
    try {
      let me = this;
      let xhr = new XMLHttpRequest();
      xhr.open('POST', me.reportURL, true);
      xhr.setRequestHeader("Authorization", "Basic " + btoa(unescape(encodeURIComponent(Cons.creds.login + ":" + Cons.creds.password))));
      xhr.responseType = 'arraybuffer';
      xhr.onload = function () {
        if (this.status === 200) {
          let type = xhr.getResponseHeader('Content-Type');
          let blob = new Blob([this.response], {type: type});
          if (typeof window.navigator.msSaveBlob !== 'undefined') {
            window.navigator.msSaveBlob(blob, me.reportName);
          } else {
            let URL = window.URL || window.webkitURL;
            let downloadUrl = URL.createObjectURL(blob);
            if (me.reportName) {
              let a = document.createElement("a");
              if (typeof a.download === 'undefined') {
                window.location = downloadUrl;
              } else {
                a.href = downloadUrl;
                a.download = me.reportName;
                document.body.appendChild(a);
                a.click();
              }
            } else {
              window.location = downloadUrl;
            }
            setTimeout(function () {
              URL.revokeObjectURL(downloadUrl);
            }, 100);
          }
          Cons.hideLoader();
        } else {
          Cons.hideLoader();
          console.log(this.status, this);
          showMessage('Произошла ошибка при формировании отчета','error');
        }
      };
      xhr.send();
    } catch (e) {
      console.log(e);
      Cons.hideLoader();
      showMessage('Произошла ошибка при формировании отчета','error');
    }
  },

  getParamRow(param) {
    let paramRow = $('<div class="uk-margin-small">');
    let label = $(`<label class="uk-form-label fonts" for="${param.code}">${param.label}</label>`);
    let controls = $('<div class="uk-form-controls">');

    switch (param.type) {
      case "java.util.Date":
        let inputDate = $(`<input class="uk-input fonts" id="${param.code}" type="date">`);
        inputDate.val(new Date().toISOString().substring(0, 10));
        controls.append(inputDate);
        break;
      case "java.util.List":
        let p = param.code.split('.');
        let dictCode = p[0];
        let dictValue = p[1];
        let dictKey = p[2].split('_')[0];

        let scrollable = $(`<div id="${param.code}" class="uk-panel uk-panel-scrollable">`);
        let ul = $('<ul class="uk-list">');

        scrollable.append(ul);
        controls.append(scrollable);

        this.listValues[param.code] = [];
        rest.synergyGet(`api/dictionaries/${dictCode}`, dict => {
          let items = [];
          for (let key in dict.items) {
            if (this.dicts.indexOf(dictCode) !== -1 && dict.items[key][dictKey].value == "0") continue;
            items.push({value: dict.items[key][dictValue].value, key: dict.items[key][dictKey].value});
          }

          items = items.sort((a, b) => {
            a = a.value.toUpperCase();
            b = b.value.toUpperCase();
            if (a > b) return 1;
            if (a < b) return -1;
            return 0;
          });

          items.forEach(item => {
            let li = $('<li>');
            let checkbox = $('<input/>').addClass('uk-checkbox').attr('type', 'checkbox');
            let label = $('<label>')
            .addClass('fonts dict-menu-item')
            .append(checkbox)
            .append(`<sapn> ${item.value}</span>`);

            checkbox.on('change', e => {
              if(e.target.checked) {
                if(this.listValues[param.code].indexOf(item.key) === -1)
                this.listValues[param.code].push(item.key);
              } else {
                let index = this.listValues[param.code].indexOf(item.key);
                if(index !== -1) this.listValues[param.code].splice(index, 1);
              }
            });

            ul.append(li.append(label));
          });
        });
        break;
      default:
        controls.append(`<input class="uk-input fonts" id="${param.code}" type="text" placeholder="${param.label}">`);
    }

    paramRow.append(label).append(controls);
    return paramRow;
  },

  getReport(report){
    this.reportURL = `${window.location.origin}/Synergy/rest/api/report/do?reportID=${report.reportID}`;
    this.reportName = report.defaultName;

    if(report.params && report.params.length > 0) {
      let dialog = $('<div uk-modal>');
      let md = $('<div>', {class: 'uk-modal-dialog'});
      let modalBody = $('<div class="uk-modal-body" uk-overflow-auto>');
      let footer = $('<div class="uk-modal-footer uk-text-right">')
      let button = $('<button class="uk-button uk-button-primary" type="button">Принять</button>');
      dialog.append(md);
      footer.append('<button class="uk-button uk-button-default uk-modal-close uk-margin-right" type="button">Отмена</button>');
      footer.append(button);
      md.append(`<div class="uk-modal-header"><h3>${report.nameru}</h3></div>`).append(modalBody).append(footer);

      report.params.forEach(param => modalBody.append(this.getParamRow(param)));

      UIkit.modal(dialog).show();
      dialog.on('hidden', () => dialog.remove());

      button.on('click', () => {
        report.params.forEach(param => {
          let val = "";
          switch (param.type) {
            case "java.util.Date":
              val = $(`[id="${param.code}"]`).val();
              let time = " 00:00:00";
              if(!val) val = new Date().toISOString().substring(0, 10);
              if(param.code == "start") time = " 00:00:00";
              if(param.code == "stop") time = " 23:59:59";
              val += time;
              break;
            case "java.util.List": val = this.listValues[param.code].join(','); break;
            default: val = $(`[id="${param.code}"]`).val() || "";
          }
          this.reportURL += `&${param.code}=${val}`;
        });
        this.reportDownload();
        UIkit.modal(dialog).hide();
      });

    } else {
      this.reportDownload();
    }
  },

  initSystemReportMenu() {
    let dialog = $('<div uk-modal>');
    let md = $('<div>', {class: 'uk-modal-dialog'});
    let modalBody = $('<div class="uk-modal-body" uk-overflow-auto>');
    let footer = $('<div class="uk-modal-footer uk-text-right">')
    let button = $('<button class="uk-button uk-button-primary" type="button">Принять</button>');
    dialog.append(md);
    footer.append('<button class="uk-button uk-button-default uk-modal-close uk-margin-right" type="button">Отмена</button>');
    footer.append(button);
    md.append('<div class="uk-modal-header"><h3>Выгрузка записей реестра в Excel</h3></div>').append(modalBody).append(footer);

    let rowSelect = $('<div class="uk-margin">');
    let label = $('<label class="uk-form-label" for="registry-select">Список реестров</label>');
    let controls = $('<div class="uk-form-controls">');
    let select = $('<select class="uk-select" id="registry-select">');

    this.registries.forEach(item => select.append(`<option value="${item.registryID}">${item.registryName}</option>`));

    rowSelect.append(label).append(controls.append(select));
    modalBody.append(rowSelect);

    UIkit.modal(dialog).show();
    dialog.on('hidden', () => {
      dialog.remove();
    });
    button.on('click', () => {
      this.getSystemReportExcel(select.val());
      UIkit.modal(dialog).hide();
    });

  },

  createReportMenu() {
    let reportsMenu = $('.report-menu ul');
    reportsMenu.empty();

    if(this.registries) {
      let linkGetExcel = $('<li><a href="#">Выгрузка в Excel</a></li>');
      linkGetExcel.on('click', e => this.initSystemReportMenu());
      reportsMenu.append(linkGetExcel);
      reportsMenu.append('<li class="uk-nav-divider"></li>');
    }

    this.reportList.forEach(report => {
      let item = $(`<li><a href="#">${report.nameru}</a></li>`);
      item.on('click', e => this.getReport(report));
      reportsMenu.append(item);
    });
  },

  init() {
    rest.synergyGet('api/registry/list', regList => {
      this.registries = regList.find(x => x.regGroupName == "103").consistOf;
      rest.synergyGet('api/report/list', res => {
        this.reportList = res.filter(x => x.objectType == 65536);
        this.createReportMenu();
      });
    });
  }
}

Reports.init();
