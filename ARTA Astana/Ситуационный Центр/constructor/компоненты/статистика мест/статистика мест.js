let tableContainer = $('.table-container');
let table = $('.statistic-table');
let head = table.find('thead');
let body = table.find('tbody');
let interval = 1000 * 60; //интервал обновления данных
let defaultValue = "Нет данных"; //Дефолтное значение если нет данных по МО

let total = {
  place_count: 0,
  place_used: 0
}

const getCurrentDateParse = () => {
  let d = new Date();
  return d.getFullYear() + '-' + ('0' + (d.getMonth() + 1)).slice(-2) + '-' + ('0' + d.getDate()).slice(-2);
}

const tableSort = () => {
  let sortedRows = Array.from(table_sort.rows).slice(1)
  .sort((rowA, rowB) => Number($(rowA).attr('sort-value')) > Number($(rowB).attr('sort-value')) ? -1 : 1);
  table_sort.tBodies[0].append(...sortedRows);
}

let statistic = {
  organization: [],
  registryCode: 'reestr_mest_v_mo',

  fields: [
    {code: 'listbox_organization', name: 'Наименование МО', bold: false, border: false},
    {code: 'date_create_data', name: 'Дата обновления', bold: false, border: true},
    {code: 'numericinput_place_count', name: 'Всего мест в инфекционном', bold: false, border: false},
    {code: 'numericinput_place_used', name: 'Занято в инфекционном', bold: false, border: false},
    {code: 'formula_free', name: 'Свободно в инфекционном', bold: true, border: true},
    {code: 'numericinput_quarantine_beds', name: 'Карантинные койки', bold: true, border: true}
  ],

  createHeader: function(){
    let tr = $('<tr>');
    head.empty().append(tr);
    this.fields.forEach(x => {
      let th = $(`<th cmpid="${x.code}" class="uk-text-center uk-text-top">${x.name}</th>`);
      if(x.border) th.css('border-right', '1px solid #fff');
      tr.append(th)
    });
  },

  createBlock: function(orgid, field, value){
    let td = $('<td>').attr('orgid', orgid);
    let card = $('<div class="uk-card uk-card-default uk-card-small">');
    let cardBody = $('<div class="uk-text-center uk-card-body">').text(value).attr('field-code', field.code);
    if(field.bold) {
      if(value != defaultValue) cardBody.addClass('uk-text-bold');
      if(value == defaultValue || value < 1) {
        cardBody.css({
          'color': '#fff',
          'background-color': '#FF0000'
        });
      } else {
        cardBody.css({
          'color': '#fff',
          'background-color': '#008000'
        });
      }
    }
    if(field.border) td.css('border-right', '1px solid #e5e5e5');
    card.append(cardBody);
    td.append(card);
    return td;
  },

  getUrlSearch: function(id){
    let url = `api/registry/data_ext?registryCode=${this.registryCode}&field=listbox_organization&key=${id}&condition=EQUALS&sortCmpID=date_create_data&sortDesc=false&countInPart=1`;
    this.fields.forEach(field => url += `&fields=${field.code}`);
    return url;
  },

  totalUpdate: function(code, fieldValue, resDate) {
    let currDate = getCurrentDateParse();
    resDate = resDate.split(' ')[0];

    if(resDate == currDate) {
      switch (code) {
        case 'numericinput_place_count': total.place_count += Number(fieldValue) || 0; break;
        case 'numericinput_place_used': total.place_used += Number(fieldValue) || 0; break;
      }
    }

    let percentInf = (total.place_count == 0 ? 0 : (Number(total.place_used * 100 / total.place_count) || 0).toFixed(1))  + '%';

    $('#total_count').text(total.place_count);
    $('#total_percent').text(percentInf);
  },

  init: function(){
    Cons.showLoader();
    this.createHeader();
    rest.synergyGet("api/dictionaries/meditsinskie_organizatsii", dict => {
      for (let key in dict.items) {
        if (dict.items[key].id.value == "0") continue;
        this.organization.push({
          name: dict.items[key].name.value,
          id: dict.items[key].id.value
        });
      }
      this.organization.sort((a, b) => a.id - b.id);
      Cons.hideLoader();

      body.empty();

      total.place_count = 0;
      total.place_used = 0;

      this.organization.forEach(org => {
        let tr = $('<tr>');
        body.append(tr);
        tr.append(`<td orgid="${org.id}">${org.name}</td>`);
        rest.synergyGet(this.getUrlSearch(org.id), res => {
          org.fieldValue = res.recordsCount > 0 ? res.result[0].fieldValue : null;

          let sortValue = -1;
          if(org.fieldValue) {
            sortValue = Number(org.fieldValue['numericinput_place_count']);
            if(sortValue > 0) sortValue += new Date(res.result[0].fieldKey['date_create_data']).getTime();
          }
          tr.attr('sort-value', sortValue);

          for(let i = 0; i < this.fields.length; i++){
            if(this.fields[i].code == 'listbox_organization') continue;
            let value = org.fieldValue ? org.fieldValue[this.fields[i].code] : defaultValue;
            tr.append(this.createBlock(org.id, this.fields[i], value));

            if(res.recordsCount > 0 && res.result[0].fieldKey.hasOwnProperty('date_create_data'))
            this.totalUpdate(this.fields[i].code, value, res.result[0].fieldKey['date_create_data']);
          }

          tableSort();
        });
      });

    });
  },

  update: function(){
    console.log(new Date().toLocaleString(), 'update data');

    total.place_count = 0;
    total.place_used = 0;

    this.organization.forEach(org => {
      rest.synergyGet(this.getUrlSearch(org.id), res => {
        org.fieldValue = res.recordsCount > 0 ? res.result[0].fieldValue : null;

        let sortValue = -1;
        if(org.fieldValue) {
          sortValue = Number(org.fieldValue['numericinput_place_count']);
          if(sortValue > 0) sortValue += new Date(res.result[0].fieldKey['date_create_data']).getTime();
        }
        $('[orgid="${org.id}"]').parent().attr('sort-value', sortValue);

        for(let i = 0; i < this.fields.length; i++){
          if(this.fields[i].code == 'listbox_organization') continue;
          let value = org.fieldValue ? org.fieldValue[this.fields[i].code] : defaultValue;
          let field = $(`[orgid="${org.id}"] [field-code="${this.fields[i].code}"]`);
          field.text(value);
          if(this.fields[i].bold) {
            field.removeClass('uk-text-bold');
            if(value != defaultValue) field.addClass('uk-text-bold');
            if(value == defaultValue || value < 1) {
              field.css({
                'color': '#fff',
                'background-color': '#FF0000'
              });
            } else {
              field.css({
                'color': '#fff',
                'background-color': '#008000'
              });
            }
          }

          if(res.recordsCount > 0 && res.result[0].fieldKey.hasOwnProperty('date_create_data'))
          this.totalUpdate(this.fields[i].code, value, res.result[0].fieldKey['date_create_data']);
        }

        tableSort();
      });
    });
  }

};


statistic.init();

let timerId = setTimeout(function run() {
  if(Cons.getCurrentPage().code != "stat_of_places_in_med_org") {
    clearTimeout(timerId);
    return;
  }
  statistic.update();
  timerId = setTimeout(run, interval);
}, interval);
