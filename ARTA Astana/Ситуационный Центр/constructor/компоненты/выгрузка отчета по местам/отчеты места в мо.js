const getSystemReportExcel = (registryID, fileName) => window.open(`${window.location.origin}/Synergy/rest/reg/load/xls?r=${registryID}&l=ru&f=&s=&u=${AS.OPTIONS.currentUser.userid}&fn=${fileName}`);

const getParamDate = param => {
  let paramRow = $('<div class="uk-margin">');
  let label = $(`<label class="uk-form-label" for="${param.code}">${param.label}</label>`);
  let controls = $('<div class="uk-form-controls">');
  let input = $(`<input class="uk-input" id="${param.code}" type="date">`);
  input.val(new Date().toISOString().substring(0, 10));
  paramRow.append(label).append(controls.append(input));
  return paramRow;
}

const reportDownload = (defaultName, requestUrl) => {
  Cons.showLoader();
  try {
    let xhr = new XMLHttpRequest();
    xhr.open('POST', requestUrl, true);
    xhr.setRequestHeader("Authorization", "Basic " + btoa(unescape(encodeURIComponent(Cons.creds.login + ":" + Cons.creds.password))));
    xhr.responseType = 'arraybuffer';
    xhr.onload = function () {
      if (this.status === 200) {
        let filename = defaultName;
        let type = xhr.getResponseHeader('Content-Type');
        let blob = new Blob([this.response], {type: type});
        if (typeof window.navigator.msSaveBlob !== 'undefined') {
          window.navigator.msSaveBlob(blob, filename);
        } else {
          let URL = window.URL || window.webkitURL;
          let downloadUrl = URL.createObjectURL(blob);
          if (filename) {
            let a = document.createElement("a");
            if (typeof a.download === 'undefined') {
              window.location = downloadUrl;
            } else {
              a.href = downloadUrl;
              a.download = filename;
              document.body.appendChild(a);
              a.click();
            }
          } else {
            window.location = downloadUrl;
          }
          setTimeout(function () {
            URL.revokeObjectURL(downloadUrl);
          }, 100);
        }
        Cons.hideLoader();
      } else {
        Cons.hideLoader();
        console.log(this.status, this);
        showMessage('Произошла ошибка при формировании отчета','error');
      }
    };
    xhr.send();
  } catch (e) {
    console.log(e);
    Cons.hideLoader();
    showMessage('Произошла ошибка при формировании отчета','error');
  }
}

const getReport = report => {
  let url = `${window.location.origin}/Synergy/rest/api/report/do?reportID=${report.reportID}`;

  let dialog = $('<div uk-modal>');
  let md = $('<div>', {class: 'uk-modal-dialog'});
  let modalBody = $('<div class="uk-modal-body" uk-overflow-auto>');
  let footer = $('<div class="uk-modal-footer uk-text-right">')
  let button = $('<button class="uk-button uk-button-primary" type="button">Принять</button>');
  dialog.append(md);
  footer.append('<button class="uk-button uk-button-default uk-modal-close uk-margin-right" type="button">Отмена</button>');
  footer.append(button);
  md.append(`<div class="uk-modal-header"><h3>${report.nameru}</h3></div>`).append(modalBody).append(footer);

  let startDate = getParamDate({code: "start", label: "Период с"});
  let stopDate = getParamDate({code: "stop", label: "Период по"});

  let rowSelect = $('<div class="uk-margin">');
  let label = $('<label class="uk-form-label" for="selectmo">Медицинская организация</label>');
  let controls = $('<div class="uk-form-controls">');
  let select = $('<select class="uk-select" id="selectmo">').attr('multiple', 'multiple');
  rowSelect.append(label).append(controls.append(select));

  rest.synergyGet("api/dictionaries/meditsinskie_organizatsii", dict => {
    for (let key in dict.items) {
      if (dict.items[key].id.value == "0") continue;
      select.append(`<option value="${dict.items[key].id.value}">${dict.items[key].name.value}</option>`);
    }
    modalBody.append(rowSelect);
    modalBody.append(startDate);
    modalBody.append(stopDate);
  });

  UIkit.modal(dialog).show();
  dialog.on('hidden', () => {
    dialog.remove();
  });
  button.on('click', () => {
    let start = startDate.find('#start').val();
    let stop = stopDate.find('#stop').val();
    let mo = select.val() ? select.val().join(',') : '';

    if(start == '') start = new Date().toISOString().substring(0, 10);
    if(stop == '') stop = new Date().toISOString().substring(0, 10);

    url += `&meditsinskie_organizatsii.name.id_dictionary=${mo}`;
    url += `&start=${start} 00:00:00`;
    url += `&stop=${stop} 23:59:59`;

    reportDownload(report.defaultName, url);
    UIkit.modal(dialog).hide();
  });
}

rest.synergyGet('api/registry/info?code=reestr_mest_v_mo', info => {
  let reportsMenu = $('.report-menu');
  reportsMenu.empty();

  let linkGetExcel = $('<li><a href="#">Выгрузка в Excel</a></li>');
  linkGetExcel.on('click', e => getSystemReportExcel(info.registryID, info.name));
  reportsMenu.append(linkGetExcel);
  reportsMenu.append('<li class="uk-nav-divider"></li>');

  rest.synergyGet('api/report/list', res => {
    res = res.filter(x => x.code == 'otchet_po_mestam_v_razreze');
    res.forEach(report => {
      let item = $(`<li><a href="#">${report.nameru}</a></li>`);
      item.on('click', e => getReport(report));
      reportsMenu.append(item);
    });
  });

});
