// Удалить бронирование сырья

var result = true;
var message = "ok";

function customFormatDate(datetime){
  datetime = datetime.split(/\D/);
  return datetime[2] + '.' + datetime[1] + '.' + datetime[0] + ' ' + datetime[3] + ':' + datetime[4];
}

function getSummRaw(tableData, docID) {
  let summ = 0;
  tableData.forEach(function(x){
    if(x.hasOwnProperty('key') && x.key == docID) {
      let tableBlockIndex = x.id.slice(x.id.indexOf('-b') + 2);
      summ += Number(UTILS.getValue(tableData, 'amount-b'+tableBlockIndex).key) || 0;
    }
  });
  return summ;
}

try {

  let currentFormData = API.getFormData(dataUUID);
  let currentDate = UTILS.getCurrentDateParse();
  let rawTable = UTILS.getValue(currentFormData, 'pharm_form_raw_request_table');

  let docIDs = [];

  rawTable.data.forEach(function(item){
    if (item.id.slice(0, item.id.indexOf('-b')) === 'raw') {
      if(item.hasOwnProperty('key')) docIDs.push(item.key);
    }
  });

  docIDs.uniq().forEach(function(docID){
    let rawFormData = API.getFormData(API.getAsfDataId(docID));
    let summRaw = getSummRaw(rawTable.data, docID);
    let raw_booked = UTILS.getValue(rawFormData, 'pharm_form_raw_booked');
    raw_booked = raw_booked.hasOwnProperty('key') ? Number(raw_booked.key) : 0;
    summRaw = raw_booked - summRaw;
    UTILS.setValue(rawFormData, 'pharm_form_raw_booked', {value: String(summRaw), key: String(summRaw)});
    UTILS.setValue(rawFormData, 'pharm_form_raw_changedate', {key: currentDate, value: customFormatDate(currentDate)});
    log.info(API.mergeFormData(rawFormData))
  });

} catch (err) {
  log.error(err);
  message = err.message;
}
