/*
Подсчёт полученного сырья по факту перехода заявки в статус "Выполнена":

При появлении новой заявки на получение сырья со статусом Выполнена система должна автоматически произвести следующие действия:
• Определить подразделение, к которому принадлежит автор заявки со статусом Выполнена (поле pharm_form_raw_request_author_dep заявки);
• Произвести поиск наименований сырья из заявки в журнале учёта сырья и готовой продукции нужного подразделения (подразделение, которому принадлежит журнал, указано в поле pharm_form_acc_logbook_department журнала):
• Если наименование сырья из заявки уже существует в журнале учёта сырья и готовой продукции нужного подразделения, увеличить значение поля В наличии на значение поля Количество из заявки;
• Если наименования сырья из заявки ещё нет в журнале учёта сырья и готовой продукции нужного подразделения, создать запись (Вид записи = Сырьё) и увеличить значение поля В наличии на значение поля Количество из заявки.
*/

var result = true;
var message = "ok";

function getSummRaw(tableData, docID) {
  let summ = 0;
  tableData.forEach(function(x){
    if(x.hasOwnProperty('key') && x.key == docID) {
      let tableBlockIndex = x.id.slice(x.id.indexOf('-b') + 2);
      summ += Number(UTILS.getValue(tableData, 'amount-b'+tableBlockIndex).key) || 0;
    }
  });
  return summ;
}

function getTableValue(tableData, docID, cmp) {
  let res;
  tableData.forEach(function(x){
    if(x.hasOwnProperty('key') && x.key == docID) {
      let tableBlockIndex = x.id.slice(x.id.indexOf('-b') + 2);
      res = UTILS.getValue(tableData, cmp + '-b' + tableBlockIndex);
    }
  });
  return res;
}

function customFormatDate(datetime){
  datetime = datetime.split(/\D/);
  return datetime[2] + '.' + datetime[1] + '.' + datetime[0] + ' ' + datetime[3] + ':' + datetime[4];
}

function getDepartmentCard(departmentID){
  let card = API.httpGetMethod('rest/api/departments/get_cards?departmentID=' + departmentID);
  card = card.filter(function(x){ return x.formCode == 'pharm_card_depcard'})[0];
  if(card) card = API.getFormData(card['data-uuid']);
  return card;
}

try {

  let currentFormData = API.getFormData(dataUUID);
  let author_dep = UTILS.getValue(currentFormData, 'pharm_form_raw_request_author_dep');

  let depCard = getDepartmentCard(author_dep.key);
  if(!depCard) throw new Error('Не найдена карточка подразделения');

  let regCode = UTILS.getValue(depCard, 'pharm_card_depcard_acc_logbook');
  if(!regCode) throw new Error('Не найден код реестра журнала учёта сырья и ГП');
  if(!regCode.hasOwnProperty('value') || regCode.value == '')
  throw new Error('Не найден код реестра журнала учёта сырья и ГП');

  let docIDs = [];
  let urlSearch = "";
  let rawTable = UTILS.getValue(currentFormData, 'pharm_form_raw_request_table');

  rawTable.data.forEach(function(item){
    if (item.id.slice(0, item.id.indexOf('-b')) === 'raw') {
      if(item.hasOwnProperty('key')) docIDs.push(item.key);
    }
  });

  docIDs.uniq().forEach(function(docID){
    urlSearch = "rest/api/registry/data_ext?registryCode=" + regCode.value + "&countInPart=1&loadData=false";
    urlSearch += "&field=pharm_form_acc_logbook_department&condition=TEXT_EQUALS&key=" + author_dep.key;
    urlSearch += "&field1=pharm_form_acc_logbook_type&condition1=CONTAINS&value1=1"
    urlSearch += "&field2=pharm_form_acc_logbook_raw&condition2=CONTAINS&key2=" + docID;

    let resultApi = API.httpGetMethod(urlSearch);
    let rawFormData = API.getFormData(API.getAsfDataId(docID));
    let measure = UTILS.getValue(rawFormData, 'pharm_form_raw_measure');
    let specifications = UTILS.getValue(rawFormData, 'pharm_form_raw_specifications');
    let summRaw = getSummRaw(rawTable.data, docID);
    let currentDate = UTILS.getCurrentDateParse();
    let productMeasure = getTableValue(rawTable.data, docID, 'measure');
    let shortname = getTableValue(rawTable.data, docID, 'shortname');

    if(resultApi.count == 0) {
      // log.info('Создаем новую запись в реестре [Журнал учёта сырья и заготовок/изделий]');
      let data = [];

      data.push({id: "pharm_form_acc_logbook_type", type: "radio", value: "1", key: "Сырьё"});
      data.push({id: "pharm_form_acc_logbook_department", type: "entity", value: author_dep.value, key: author_dep.key});
      data.push({id: "pharm_form_acc_logbook_raw", type: "reglink", key: docID, valueID: docID, value: API.getDocMeaningContent(docID)});
      data.push({id: "pharm_form_acc_logbook_instock", type: "numericinput", value: String(summRaw), key: String(summRaw)});
      data.push({id: "pharm_form_acc_logbook_raw_measure", type: "listbox", value: measure.value, key: measure.key});
      data.push({id: "pharm_form_acc_logbook_changedate", type: "date", value: customFormatDate(currentDate), key: currentDate});
      data.push({id: "pharm_form_acc_logbook_product_measure", type: "listbox", value: productMeasure.value, key: productMeasure.key});
      data.push({id: "pharm_form_acc_logbook_raw_shortname", type: "textbox", value: shortname.value});

      if(specifications.hasOwnProperty('value')) {
        data.push({id: "pharm_form_acc_logbook_specifications", type: "textarea", value: specifications.value});
      }

      resultApi = API.httpPostMethod("rest/api/registry/create_doc_rcc", {
        registryCode: regCode.value,
        sendToActivation: true,
        data: data
      }, "application/json; charset=utf-8");

    } else {
      // log.info('Изменяем значения поля [pharm_form_acc_logbook_instock] в реестре [Журнал учёта сырья и заготовок/изделий]');

      let accFormData = API.getFormData(resultApi.data[0].dataUUID);
      let instock = UTILS.getValue(accFormData, 'pharm_form_acc_logbook_instock');

      if(instock && instock.hasOwnProperty('key')) {
        let newValue = Number(instock.key) + summRaw;
        instock.key = String(newValue);
        instock.value = String(newValue);
      } else {
        instock = {id: "pharm_form_acc_logbook_instock", type: "numericinput", value: String(summRaw), key: String(summRaw)}
      }

      UTILS.setValue(accFormData, 'pharm_form_acc_logbook_changedate', {value: customFormatDate(currentDate), key: currentDate});
      UTILS.setValue(accFormData, 'pharm_form_acc_logbook_raw_measure', {value: measure.value, key: measure.key});
      if(specifications.hasOwnProperty('value')) {
        UTILS.setValue(accFormData, 'pharm_form_acc_logbook_specifications', {value: specifications.value});
      }

      API.mergeFormData(accFormData);
    }

  });

} catch (err) {
  log.error(err);
  message = err.message;
}
