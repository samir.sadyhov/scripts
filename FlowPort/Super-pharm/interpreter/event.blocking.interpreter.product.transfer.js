/*Передача готовой продукции:

При появлении новой записи в журнале учёта передачи готовой продукции любого цеха со статусом Подтверждена система должна автоматически произвести следующие действия:

--- Уменьшить значение поля В наличии на значение поля Количество из записи журнала учёта передачи готовой продукции подразделения сотрудника, указанного в поле Передал;

1. Определить подразделение из поля Принял (наименование подразделения) записи журнала учёта передачи готовой продукции со статусом Выполнена:

** Если передача происходит на склад:
- При передаче ГП на склад делать проверку по значению полей:
Наименование,
Партия сырья,
Серия,
Партия,
Дата изготовления (месяц, год),
Срок годности (месяц, год).
Если запись с такими значениями уже есть на складе готовой продукции, следует увеличить количество В наличии на значение Передано из запускаемой записи журнала учёта передачи ГП. Если на складе готовой продукции такой записи нет, то следует создать новую запись и установить количество В наличии как значение Передано из запускаемой записи журнала учёта передачи ГП

** Если передача происходит не на склад:
- Произвести поиск наименований готовой продукции из записи в журнале учёта сырья и готовой продукции нужного подразделения:
- Если наименование готовой продукции из записи журнала учёта передачи готовой продукции уже существует в журнале учёта сырья и готовой продукции нужного подразделения, увеличить значение поля В наличии на значение поля Количество из записи журнала учёта передачи готовой продукции;
- Если наименования готовой продукции из записи журнала учёта передачи готовой продукции ещё нет в журнале учёта сырья и готовой продукции нужного подразделения, создать запись (Вид записи = Готовая продукция) и увеличить значение поля В наличии на значение поля Количество из записи журнала учёта передачи готовой продукции.
*/

var result = true;
var message = "ok";

function customFormatDate(datetime){
  datetime = datetime.split(/\D/);
  return datetime[2] + '.' + datetime[1] + '.' + datetime[0] + ' ' + datetime[3] + ':' + datetime[4];
}

function getDepartmentCard(departmentID){
  let card = API.httpGetMethod('rest/api/departments/get_cards?departmentID=' + departmentID);
  card = card.filter(function(x){ return x.formCode == 'pharm_card_depcard'})[0];
  if(card) card = API.getFormData(card['data-uuid']);
  return card;
}

try {

  let currentFormData = API.getFormData(dataUUID);

  let receiver_dep = UTILS.getValue(currentFormData, 'pharm_form_fin_logbook_receiver_dep');
  if(!receiver_dep || !receiver_dep.hasOwnProperty('key')) throw new Error('Не выбрано подразделение в поле принял');

  let depCard = getDepartmentCard(receiver_dep.key);
  if(!depCard) throw new Error('Не найдена карточка подразделения');

  let product_table = UTILS.getValue(currentFormData, 'pharm_form_transfer_logbook_product_table');
  if(!product_table || !product_table.hasOwnProperty('data')) throw new Error('Таблица "Заготовка/изделие" пустая');

  let warehouse = UTILS.getValue(depCard, 'pharm_card_depcard_warehouse');
  let currentDate = UTILS.getCurrentDateParse();
  let products = [];

  product_table.data.forEach(function(item){
    if (item.id.slice(0, item.id.indexOf('-b')) === 'product') {
      if(item.hasOwnProperty('key')) {
        let accFormData = API.getFormData(API.getAsfDataId(item.key));
        let productDocID = UTILS.getValue(accFormData, 'pharm_form_acc_logbook_product');
        if(productDocID && productDocID.hasOwnProperty('key')) {
          let producFormData = API.getFormData(API.getAsfDataId(productDocID.key));
          let tableBlockIndex = item.id.slice(item.id.indexOf('-b'));
          products.push({
            currentDate: {key: currentDate, value: customFormatDate(currentDate)},
            accFormDocID: item.key,
            accFormData: accFormData,
            docID: productDocID.key,
            name: UTILS.getValue(producFormData, 'pharm_form_product_name').value || '', //Наименование
            shortname: UTILS.getValue(producFormData, 'pharm_form_product_shortname').value || '', //Краткое наименование
            stock_number: UTILS.getValue(product_table, 'raw_shortname' + tableBlockIndex).value || '', //Партия сырья
            amount_sent: Number(UTILS.getValue(product_table, 'amount_sent' + tableBlockIndex).key) || 0, //Количество в наличии - Передано
            series: UTILS.getValue(product_table, 'series' + tableBlockIndex).value || '', //Серия
            batch: UTILS.getValue(product_table, 'batch' + tableBlockIndex).value || '', //Партия
            prodmonth: UTILS.getValue(product_table, 'prodmonth' + tableBlockIndex), //Дата изготовления месяц
            prodyear: UTILS.getValue(product_table, 'prodyear' + tableBlockIndex), //Дата изготовления год
            expmonth: UTILS.getValue(product_table, 'expmonth' + tableBlockIndex), //Срок годности месяц
            expyear: UTILS.getValue(product_table, 'expyear' + tableBlockIndex), //Срок годности год
            product_set: UTILS.getValue(producFormData, 'pharm_form_product_set'), //Розница/Комплект
            specifications: UTILS.getValue(producFormData, 'pharm_form_product_specifications').value || '', //Техническая характеристика
            measure: UTILS.getValue(producFormData, 'pharm_form_product_measure'), //Единица измерения
            regcert: UTILS.getValue(producFormData, 'pharm_form_product_regcert').value || '', //№ рег. удостоверения
            quantity: UTILS.getValue(producFormData, 'pharm_form_product_quantity').value || '', //Количество в коробке
            price: Number(UTILS.getValue(producFormData, 'pharm_form_product_price').key) || 0 //Цена за минимальную ед.
          });
        }
      }
    }
  });

  if(products.length == 0) throw new Error('Таблица "Заготовка/изделие" пустая');

  if(warehouse && warehouse.value == "1") {
    //передача на склад

    let urlSearch = "";
    products.forEach(function(product){
      //поиск в реестре "Склад готовой продукции"
      urlSearch = "rest/api/registry/data_ext?registryCode=pharm_registry_products&countInPart=1&loadData=false";
      urlSearch += "&field=pharm_form_product_name&condition=TEXT_EQUALS&value=" + encodeURIComponent(product.name);
      urlSearch += "&field1=pharm_form_product_raw_shortname&condition1=TEXT_EQUALS&value1=" + encodeURIComponent(product.stock_number);
      urlSearch += "&field2=pharm_form_product_series&condition2=TEXT_EQUALS&value2=" + encodeURIComponent(product.series);
      urlSearch += "&field3=pharm_form_product_batch&condition3=TEXT_EQUALS&value3=" + encodeURIComponent(product.batch);
      urlSearch += "&field4=pharm_form_product_prodmonth&condition4=CONTAINS&key4=" + product.prodmonth.key;
      urlSearch += "&field5=pharm_form_product_prodyear&condition5=CONTAINS&key5=" + product.prodyear.key;
      urlSearch += "&field6=pharm_form_product_expmonth&condition6=CONTAINS&key6=" + product.expmonth.key;
      urlSearch += "&field7=pharm_form_product_expyear&condition7=CONTAINS&key7=" + product.expyear.key;

      let res = API.httpGetMethod(urlSearch);
      if(res.hasOwnProperty('errorCode') && res.errorCode != '0') throw new Error(res.errorMessage);
      if(res.count != 0) {
        //Запись найдена, увеличиваем Количество в наличии (pharm_form_product_instock)
        let skladFormData = API.getFormData(res.data[0].dataUUID);
        let raw_instock = UTILS.getValue(skladFormData, 'pharm_form_product_instock');
        raw_instock = raw_instock.hasOwnProperty('key') ? Number(raw_instock.key) : 0;
        raw_instock += product.amount_sent;
        UTILS.setValue(skladFormData, 'pharm_form_product_instock', {value: String(raw_instock), key: String(raw_instock)});
        API.mergeFormData(skladFormData);
      } else {
        //Запись не найдена, создаем новую
        let data = [];
        data.push({id: "pharm_form_product_productlink", type: "reglink", key: product.docID, valueID: product.docID, value: product.name});
        data.push({id: "pharm_form_product_name", type: "textarea", value: product.name});
        data.push({id: "pharm_form_product_shortname", type: "textbox", value: product.shortname});
        data.push({id: "pharm_form_product_raw_shortname", type: "textbox", value: product.stock_number});
        data.push({id: "pharm_form_product_instock", type: "numericinput", value: String(product.amount_sent), key: String(product.amount_sent)});
        data.push({id: "pharm_form_product_series", type: "textbox", value: product.series});
        data.push({id: "pharm_form_product_batch", type: "textbox", value: product.batch});
        data.push({id: "pharm_form_product_prodmonth", type: "listbox", value: product.prodmonth.value, key: product.prodmonth.key});
        data.push({id: "pharm_form_product_prodyear", type: "listbox", value: product.prodyear.value, key: product.prodyear.key});
        data.push({id: "pharm_form_product_expmonth", type: "listbox", value: product.expmonth.value, key: product.expmonth.key});
        data.push({id: "pharm_form_product_expyear", type: "listbox", value: product.expyear.value, key: product.expyear.key});
        data.push({id: "pharm_form_product_set", type: "radio", value: product.product_set.value, key: product.product_set.key});
        data.push({id: "pharm_form_product_specifications", type: "textarea", value: product.specifications});
        data.push({id: "pharm_form_product_measure", type: "listbox", value: product.measure.value, key: product.measure.key});
        data.push({id: "pharm_form_product_regcert", type: "textbox", value: product.regcert});
        data.push({id: "pharm_form_product_quantity", type: "textbox", value: product.quantity});
        data.push({id: "pharm_form_product_price", type: "numericinput", value: String(product.price), key: String(product.price)});

        res = API.httpPostMethod("rest/api/registry/create_doc_rcc", {
          registryCode: 'pharm_registry_products',
          sendToActivation: true,
          data: data
        }, "application/json; charset=utf-8");
      }
    });
  } else {
    //передача не на склад
    let regCode = UTILS.getValue(depCard, 'pharm_card_depcard_acc_logbook');
    if(!regCode) throw new Error('Не найден код реестра [Журнал учёта сырья и заготовок/изделий]');
    if(!regCode.hasOwnProperty('value') || regCode.value == '')
    throw new Error('Не найден код реестра [Журнал учёта сырья и заготовок/изделий]');

    let urlSearch = "";
    products.forEach(function(product){

      urlSearch = "rest/api/registry/data_ext?registryCode=" + regCode.value + "&countInPart=1&loadData=false";
      urlSearch += "&field=pharm_form_acc_logbook_department&condition=TEXT_EQUALS&key=" + receiver_dep.key;
      urlSearch += "&field1=pharm_form_acc_logbook_type&condition1=CONTAINS&value1=2"
      urlSearch += "&field2=pharm_form_acc_logbook_product&condition2=CONTAINS&key2=" + product.docID;

      let res = API.httpGetMethod(urlSearch);
      if(res.hasOwnProperty('errorCode') && res.errorCode != '0') throw new Error('registryCode ' + regCode.value + ' || ' + res.errorMessage);
      if(res.count != 0) {
        //Запись найдена, увеличиваем В наличии (pharm_form_acc_logbook_instock)
        let accFormData2 = API.getFormData(res.data[0].dataUUID);

        let instock = UTILS.getValue(accFormData2, 'pharm_form_acc_logbook_instock');
        if(instock && instock.hasOwnProperty('key')) {
          let newValue = Number(instock.key) + product.amount_sent;
          instock.key = String(newValue);
          instock.value = String(newValue);
        } else {
          instock = {
            id: "pharm_form_acc_logbook_instock",
            type: "numericinput",
            value: String(product.amount_sent),
            key: String(product.amount_sent)
          }
        }

        UTILS.setValue(accFormData2, 'pharm_form_acc_logbook_changedate', product.currentDate);
        UTILS.setValue(accFormData2, 'pharm_form_acc_logbook_product_measure', {value: product.measure.value, key: product.measure.key});
        UTILS.setValue(accFormData2, 'pharm_form_acc_logbook_specifications', {value: product.specifications});

        API.mergeFormData(accFormData2);
      } else {
        log.info('Создаем новую запись в реестре [Журнал учёта сырья и заготовок/изделий]');
        let data = [];

        data.push({id: "pharm_form_acc_logbook_type", type: "radio", value: "2", key: "Заготовка/изделие"});
        data.push({id: "pharm_form_acc_logbook_department", type: "entity", value: receiver_dep.value, key: receiver_dep.key});
        data.push({id: "pharm_form_acc_logbook_product", type: "reglink", key: product.docID, valueID: product.docID, value: product.name});
        data.push({id: "pharm_form_acc_logbook_instock", type: "numericinput", value: String(product.amount_sent), key: String(product.amount_sent)});
        data.push({id: "pharm_form_acc_logbook_product_measure", type: "listbox", value: product.measure.value, key: product.measure.key});
        data.push({id: "pharm_form_acc_logbook_changedate", type: "date", value: product.currentDate.key, key: product.currentDate.value});
        data.push({id: "pharm_form_acc_logbook_specifications", type: "textarea", value: product.specifications});
        data.push({id: "pharm_form_acc_logbook_series", type: "textbox", value: product.series});
        data.push({id: "pharm_form_acc_logbook_batch", type: "textbox", value: product.batch});
        data.push({id: "pharm_form_acc_logbook_raw_shortname", type: "textbox", value: product.stock_number});

        res = API.httpPostMethod("rest/api/registry/create_doc_rcc", {
          registryCode: regCode.value,
          sendToActivation: true,
          data: data
        }, "application/json; charset=utf-8");
      }
    });
  }

  //Уменьшить значение поля
  products.forEach(function(product) {
    let instock = UTILS.getValue(product.accFormData, 'pharm_form_acc_logbook_instock');
    if(instock && instock.hasOwnProperty('key')) {
      let newValue = Number(instock.key) - product.amount_sent;
      instock.key = String(newValue);
      instock.value = String(newValue);
      API.mergeFormData(product.accFormData);
    }
  });

} catch (err) {
  log.error(err, err.message);
  result = false;
  message = err.message;
}
