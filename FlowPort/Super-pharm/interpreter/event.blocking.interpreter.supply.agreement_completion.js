var result = true;
var message = "ok";

function filterProcess(processes, formCode) {
  let result = [];
  function search(p) {
    p.forEach(function(process) {
      if (process.completionFormCode == formCode && process.finished) result.push(process);
      if (process.subProcesses.length > 0) search(process.subProcesses);
    });
  }
  search(processes);
  return result.sort(function(a, b) {
    return UTILS.parseDateTime(b.finished) - UTILS.parseDateTime(a.finished);
  });
};

try {
  let processes = API.getProcesses(documentID);
  processes = filterProcess(processes, 'buch');

  let completionFormDataUUID = processes.map(function(item) {
    return API.getWorkCompletionData(item.actionID).result.dataUUID;
  }).filter(function(dataUUID){ return dataUUID != null})[0];

  if(!completionFormDataUUID) throw new Error('Не найдена форма завершения');

  let matching = [];
  matching.push({from: 'pharm_form_supply_wcf_result', to: 'pharm_form_supply_result'});
  matching.push({from: 'pharm_form_supply_wcf_comment', to: 'pharm_form_supply_comment'});

  let completionFormData = API.getFormData(completionFormDataUUID);
  let currentFormData = API.getFormData(dataUUID);

  matching.forEach(function(id) {
    let fromData = UTILS.getValue(completionFormData, id.from);
    if(fromData) UTILS.setValue(currentFormData, id.to, fromData);
  });

  API.mergeFormData(currentFormData);

} catch (err) {
  log.error(err);
  message = err.message;
}
