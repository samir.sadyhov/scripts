//Накладная на приём сырья на склад

var result = true;
var message = "ok";

try {

  let currentFormData = API.getFormData(dataUUID);
  let table = UTILS.getValue(currentFormData, 'pharm_form_waybill_main_table');
  let materials = [];

  table.data.forEach(function(item){
    if (item.id.slice(0, item.id.indexOf('-b')) === 'raw') {
      if(item.hasOwnProperty('key')) {
        let rawFormData = API.getFormData(API.getAsfDataId(item.key));
        let tableBlockIndex = item.id.slice(item.id.indexOf('-b'));
        materials.push({
          docID: item.key,
          name: UTILS.getValue(rawFormData, 'pharm_form_raw_name').value || '', //Наименование
          specifications: UTILS.getValue(rawFormData, 'pharm_form_raw_specifications').value, //Техническая характеристика
          measure: UTILS.getValue(rawFormData, 'pharm_form_raw_measure'), //Единица измерения
          country: UTILS.getValue(rawFormData, 'pharm_form_raw_country'), //Страна
          series: UTILS.getValue(rawFormData, 'pharm_form_raw_series').value || '', //Серия
          batch: UTILS.getValue(rawFormData, 'pharm_form_raw_batch').value || '', //Партия
          stock_number: UTILS.getValue(table, 'stock_number' + tableBlockIndex).value || '', //Партия сырья
          amount: Number(UTILS.getValue(table, 'amount2' + tableBlockIndex).key) || 0, //В наличии
          expmonth: UTILS.getValue(table, 'expmonth' + tableBlockIndex), //Срок годности месяц
          expyear: UTILS.getValue(table, 'expyear' + tableBlockIndex) //Срок годности год
        });
      }
    }
  });

  if(materials.length === 0) {
    message = 'Таблица сырья пустая';
  } else {
    let urlSearch = "";
    materials.forEach(function(item){
      urlSearch = "rest/api/registry/data_ext?registryCode=pharm_registry_raw_material&countInPart=1&loadData=false";
      urlSearch += "&field=pharm_form_raw_name&condition=TEXT_EQUALS&value=" + encodeURIComponent(item.name);
      urlSearch += "&field1=pharm_form_raw_shortname&condition1=TEXT_EQUALS&value1=" + encodeURIComponent(item.stock_number);
      urlSearch += "&field2=pharm_form_raw_expmonth&condition2=CONTAINS&key2=" + item.expmonth.key;
      urlSearch += "&field3=pharm_form_raw_expyear&condition3=CONTAINS&key3=" + item.expyear.key;

      let result = API.httpGetMethod(urlSearch);
      if(result.count != 0) {
        //Запись найдена, увеличиваем pharm_form_raw_instock
        let skladFormData = API.getFormData(result.data[0].dataUUID);
        let raw_instock = UTILS.getValue(skladFormData, 'pharm_form_raw_instock');
        raw_instock = raw_instock.hasOwnProperty('key') ? Number(raw_instock.key) : 0;
        raw_instock += item.amount;
        UTILS.setValue(skladFormData, 'pharm_form_raw_instock', {value: String(raw_instock), key: String(raw_instock)});
        log.info(API.mergeFormData(skladFormData));
      } else {
        //Запись не найдена, создаем новую
        let data = [];

        data.push({id: "pharm_form_raw_rawlink", type: "reglink", key: item.docID, valueID: item.docID, value: item.name});
        data.push({id: "pharm_form_raw_name", type: "textarea", value: item.name});
        data.push({id: "pharm_form_raw_specifications", type: "textarea", value: item.specifications});
        data.push({id: "pharm_form_raw_shortname", type: "textbox", value: item.stock_number});
        data.push({id: "pharm_form_raw_series", type: "textbox", value: item.series});
        data.push({id: "pharm_form_raw_batch", type: "textbox", value: item.batch});
        data.push({id: "pharm_form_raw_booked", type: "numericinput", value: "0", key: "0"});
        data.push({id: "pharm_form_raw_instock", type: "numericinput", value: String(item.amount), key: String(item.amount)});
        data.push({id: "pharm_form_raw_expmonth", type: "listbox", value: item.expmonth.value, key: item.expmonth.key});
        data.push({id: "pharm_form_raw_expyear", type: "listbox", value: item.expyear.value, key: item.expyear.key});
        data.push({id: "pharm_form_raw_measure", type: "listbox", value: item.measure.value, key: item.measure.key});
        data.push({id: "pharm_form_raw_country", type: "listbox", value: item.country.value, key: item.country.key});

        result = API.httpPostMethod("rest/api/registry/create_doc_rcc", {
          registryCode: 'pharm_registry_raw_material',
          sendToActivation: true,
          data: data
        }, "application/json; charset=utf-8");

        log.info(result);
      }
    });
  }

  UTILS.setValue(currentFormData, 'pharm_form_waybill_status', {value: 'Выполнена', key: '1'});
  API.mergeFormData(currentFormData);

} catch (err) {
  log.error(err);
  message = err.message;
}
