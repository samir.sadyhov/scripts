var result = true;
var message = "ok";

function customFormatDate(datetime){
  datetime = datetime.split(/\D/);
  return datetime[2] + '.' + datetime[1] + '.' + datetime[0] + ' ' + datetime[3] + ':' + datetime[4];
}

function getSummProduct(tableData, docID) {
  let summ = 0;
  tableData.forEach(function(x){
    if(x.hasOwnProperty('key') && x.key == docID) {
      let tableBlockIndex = x.id.slice(x.id.indexOf('-b') + 2);
      summ += Number(UTILS.getValue(tableData, 'amount-b'+tableBlockIndex).key) || 0;
    }
  });
  return summ;
}

try {

  let currentFormData = API.getFormData(dataUUID);
  let currentDate = UTILS.getCurrentDateParse();
  let productTable = UTILS.getValue(currentFormData, 'pharm_form_control_logbook_product_table');

  let docIDs = [];

  productTable.data.forEach(function(item){
    if (item.id.slice(0, item.id.indexOf('-b')) === 'product') {
      if(item.hasOwnProperty('key')) docIDs.push(item.key);
    }
  });

  docIDs.uniq().forEach(function(docID){
    let productFormData = API.getFormData(API.getAsfDataId(docID));
    let summ = getSummProduct(productTable.data, docID);
    let instock = UTILS.getValue(productFormData, 'pharm_form_acc_logbook_instock');
    instock = instock.hasOwnProperty('key') ? Number(instock.key) : 0;
    summ = instock - summ;
    UTILS.setValue(productFormData, 'pharm_form_acc_logbook_instock', {value: String(summ), key: String(summ)});
    UTILS.setValue(productFormData, 'pharm_form_acc_logbook_changedate', {key: currentDate, value: customFormatDate(currentDate)});
    API.mergeFormData(productFormData);
  });

} catch (err) {
  log.error(err);
  message = err.message;
}
