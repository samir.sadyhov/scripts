/*
Подсчёт сырья и готовой продукции на складе цеха по факту выработки продукции:

При появлении новой записи в журнале выработки готовой продукции любого цеха со статусом Подтверждена система должна автоматически произвести следующие действия:
• Произвести поиск наименований сырья из записи в журнале учёта сырья и готовой продукции подразделения, создавшего эту запись, и уменьшить значение поля В наличии на значение поля Количество из записи в журнале учёта сырья и готовой продукции;
• Произвести поиск наименований готовой продукции из записи в журнале учёта сырья и готовой продукции подразделения, создавшего эту запись, и увеличить значение поля В наличии на значение поля Количество из записи в журнале учёта сырья и готовой продукции.
*/

var result = true;
var message = "ok";

function customFormatDate(datetime){
  datetime = datetime.split(/\D/);
  return datetime[2] + '.' + datetime[1] + '.' + datetime[0] + ' ' + datetime[3] + ':' + datetime[4];
}

function getSummRaw(data, tableID, docID, amountID) {
  let summ = 0;
  let table = UTILS.getValue(data, tableID);
  if(table && table.hasOwnProperty('data'))
  table.data.forEach(function(x){
    if(x.hasOwnProperty('key') && x.key == docID) {
      let tableBlockIndex = x.id.slice(x.id.indexOf('-b'));
      summ += Number(UTILS.getValue(table.data, amountID + tableBlockIndex).key) || 0;
    }
  });
  return summ;
}

function getDepartmentCard(departmentID){
  let card = API.httpGetMethod('rest/api/departments/get_cards?departmentID=' + departmentID);
  card = card.filter(function(x){ return x.formCode == 'pharm_card_depcard'})[0];
  if(card) card = API.getFormData(card['data-uuid']);
  return card;
}

function getDocLinks(data, tableID, linkID) {
  let docs = [];
  let table = UTILS.getValue(data, tableID);
  if(table && table.hasOwnProperty('data'))
  table.data.forEach(function(x){
    if (x.id.slice(0, x.id.indexOf('-b')) === linkID) if(x.hasOwnProperty('key')) docs.push(x.key);
  });
  return docs.uniq();
}

try {

  let currentFormData = API.getFormData(dataUUID);
  let author_dep = UTILS.getValue(currentFormData, 'pharm_form_fin_logbook_author_dep');

  let depCard = getDepartmentCard(author_dep.key);
  if(!depCard) throw new Error('Не найдена карточка подразделения');

  let regCode = UTILS.getValue(depCard, 'pharm_card_depcard_acc_logbook');
  if(!regCode) throw new Error('Не найден код реестра журнала учёта сырья и ГП');
  if(!regCode.hasOwnProperty('value') || regCode.value == '')
  throw new Error('Не найден код реестра журнала учёта сырья и ГП');

  //Сырье
  let docIDs = getDocLinks(currentFormData, 'pharm_form_fin_logbook_raw_table', 'raw');
  docIDs.forEach(function(docID){
    let accFormData = API.getFormData(API.getAsfDataId(docID));
    let summRaw = getSummRaw(currentFormData, 'pharm_form_fin_logbook_raw_table', docID, 'amount_spend');
    let instock = UTILS.getValue(accFormData, 'pharm_form_acc_logbook_instock');
    if(instock && instock.hasOwnProperty('key')) {
      let newValue = Number(instock.key) - summRaw;
      instock.key = String(newValue);
      instock.value = String(newValue);
      log.info(API.mergeFormData(accFormData));
    }
  });

  //Продукция
  let urlSearch = "";
  docIDs = getDocLinks(currentFormData, 'pharm_form_fin_logbook_product_table', 'product');
  docIDs.forEach(function(docID){
    urlSearch = "rest/api/registry/data_ext?registryCode=" + regCode.value + "&countInPart=1&loadData=false";
    urlSearch += "&field=pharm_form_acc_logbook_department&condition=TEXT_EQUALS&key=" + author_dep.key;
    urlSearch += "&field1=pharm_form_acc_logbook_type&condition1=CONTAINS&value1=2"
    urlSearch += "&field2=pharm_form_acc_logbook_product&condition2=CONTAINS&key2=" + docID;


    let summRaw = getSummRaw(currentFormData, 'pharm_form_fin_logbook_product_table', docID, 'amount_produced');
    let currentDate = UTILS.getCurrentDateParse();

    let result = API.httpGetMethod(urlSearch);
    if(result.count != 0) {
      let accFormData = API.getFormData(result.data[0].dataUUID);
      let instock = UTILS.getValue(accFormData, 'pharm_form_acc_logbook_instock');
      UTILS.setValue(accFormData, 'pharm_form_acc_logbook_changedate', {
        type: "date",
        value: customFormatDate(currentDate),
        key: currentDate
      });
      if(instock && instock.hasOwnProperty('key')) {
        let newValue = Number(instock.key) + summRaw;
        instock.key = String(newValue);
        instock.value = String(newValue);
        log.info(API.mergeFormData(accFormData));
      }
    } else {
      log.info('Создаем новую запись в реестре [Журнал учёта сырья и ГП]');

      let producFormData = API.getFormData(API.getAsfDataId(docID));
      let measure = UTILS.getValue(producFormData, 'pharm_form_product_measure');
      let series = UTILS.getValue(producFormData, 'pharm_form_product_series');
      let shortname = UTILS.getValue(producFormData, 'pharm_form_product_shortname');
      let specifications = UTILS.getValue(producFormData, 'pharm_form_product_specifications');
      let data = [];

      data.push({
        id: "pharm_form_acc_logbook_type",
        type: "radio",
        value: "2",
        key: "Готовая продукция"
      });

      data.push({
        id: "pharm_form_acc_logbook_department",
        type: "entity",
        value: author_dep.value,
        key: author_dep.key
      });

      data.push({
        id: "pharm_form_acc_logbook_product",
        type: "reglink",
        key: docID,
        valueID: docID,
        value: API.getDocMeaningContent(docID)
      });

      data.push({
        id: "pharm_form_acc_logbook_instock",
        type: "numericinput",
        value: String(summRaw),
        key: String(summRaw)
      });

      data.push({
        id: "pharm_form_acc_logbook_product_measure",
        type: "listbox",
        value: measure.value,
        key: measure.key
      });

      data.push({
        id: "pharm_form_acc_logbook_changedate",
        type: "date",
        value: customFormatDate(currentDate),
        key: currentDate
      });

      if(series.hasOwnProperty('value')) {
        data.push({
          id: "pharm_form_acc_logbook_series",
          type: "textbox",
          value: series.value
        });
      }

      if(shortname.hasOwnProperty('value')) {
        data.push({
          id: "pharm_form_acc_logbook_shortname",
          type: "textbox",
          value: shortname.value
        });
      }

      if(specifications.hasOwnProperty('value')) {
        data.push({
          id: "pharm_form_acc_logbook_specifications",
          type: "textarea",
          value: specifications.value
        });
      }

      result = API.createDocRCC(regCode.value, data);
      result = API.activateDoc(result.documentID);
      log.info("Запись создана [ documentID: " + result.documentID + " ]");
    }
  });

} catch (err) {
  log.error(err);
  message = err.message;
}
