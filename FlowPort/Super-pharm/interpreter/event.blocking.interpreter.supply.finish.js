/*
Форма Поставка. При завершении маршрута должен сработать БП, который уменьшит значение полей В наличии всех записей реестра Склад сырья, которые были задействованы в завершённой записи реестра поставок, на указанное в записи реестра поставок количество
*/

var result = true;
var message = "ok";

function customFormatDate(datetime){
  datetime = datetime.split(/\D/);
  return datetime[2] + '.' + datetime[1] + '.' + datetime[0] + ' ' + datetime[3] + ':' + datetime[4];
}

function getSummRaw(data, tableID, docID, amountID) {
  let summ = 0;
  let table = UTILS.getValue(data, tableID);
  if(table && table.hasOwnProperty('data'))
  table.data.forEach(function(x){
    if(x.hasOwnProperty('key') && x.key == docID) {
      let tableBlockIndex = x.id.slice(x.id.indexOf('-b'));
      summ += Number(UTILS.getValue(table.data, amountID + tableBlockIndex).key) || 0;
    }
  });
  return summ;
}

function getDocLinks(data, tableID, linkID) {
  let docs = [];
  let table = UTILS.getValue(data, tableID);
  if(table && table.hasOwnProperty('data'))
  table.data.forEach(function(x){
    if (x.id.slice(0, x.id.indexOf('-b')) === linkID) if(x.hasOwnProperty('key')) docs.push(x.key);
  });
  return docs.uniq();
}

try {
  let currentFormData = API.getFormData(dataUUID);

  let docIDs = getDocLinks(currentFormData, 'pharm_form_supply_table_product', 'productlink');
  docIDs.forEach(function(docID){
    let accFormData = API.getFormData(API.getAsfDataId(docID));
    let summRaw = getSummRaw(currentFormData, 'pharm_form_supply_table_product', docID, 'amount');
    let instock = UTILS.getValue(accFormData, 'pharm_form_product_instock');
    if(instock && instock.hasOwnProperty('key')) {

      //уменьшение значения "Количество в наличии"
      let newValue = Number(instock.key) - summRaw;
      instock.key = String(newValue);
      instock.value = String(newValue);

      //установка даты изменения
      let currentDate = UTILS.getCurrentDateParse();
      UTILS.setValue(accFormData, 'pharm_form_product_changedate', {
        type: "date",
        value: customFormatDate(currentDate),
        key: currentDate
      });

      //сохранение данных
      API.mergeFormData(accFormData);
    }
  });

} catch (err) {
  log.error(err);
  message = err.message;
}
