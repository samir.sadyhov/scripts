var result = true;
var message = "ok";

function customFormatDate(datetime){
  datetime = datetime.split(/\D/);
  return datetime[2] + '.' + datetime[1] + '.' + datetime[0] + ' ' + datetime[3] + ':' + datetime[4];
}

try {
  let currentFormData = API.getFormData(dataUUID);
  let currentDate = UTILS.getCurrentDateParse();

  UTILS.setValue(currentFormData, 'pharm_form_fin_logbook_savedate', {value: customFormatDate(currentDate), key: currentDate});
  message = customFormatDate(currentDate);

  API.saveFormData(currentFormData);

} catch (err) {
  log.error(err);
  message = err.message;
}
