var result = true;
var message = "ok";

function customFormatDate(datetime){
  datetime = datetime.split(/\D/);
  return datetime[2] + '.' + datetime[1] + '.' + datetime[0] + ' ' + datetime[3] + ':' + datetime[4];
}

function getTableValue(table, cmp) {
  return table.filter(function(x){if(x.id === cmp) return x})[0] || null;
}

try {
  let currentFormData = API.getFormData(dataUUID);
  let table = UTILS.getValue(currentFormData, 'pharm_form_control_logbook_product_table');
  table = table.data.filter(function(row) {if(row.type !== 'label') return row});

  let n = table[table.length - 1].id;
  n = n.slice(n.indexOf('-b') + 2) * 1;

  let currentDate = UTILS.getCurrentDateParse();

  for(let i = 1; i <= n; i++) {
    let productName = getTableValue(table, 'product-b' + i);
    let shortname = getTableValue(table, 'shortname-b' + i);
    let batch = getTableValue(table, 'batch-b' + i);
    let series = getTableValue(table, 'series-b' + i);
    let expmonth = getTableValue(table, 'expmonth-b' + i);
    let expyear = getTableValue(table, 'expyear-b' + i);
    let quality = getTableValue(table, 'quality-b' + i);
    let certificate = getTableValue(table, 'certificate-b' + i);
    let measure = getTableValue(table, 'measure-b' + i);

    productName ? productName = productName.value.split('-')[0] : productName = '';
    shortname ? shortname = shortname.value : shortname = '';
    batch ? batch = batch.value : batch = '';
    series ? series = series.value : series = '';

    let urlSearch = 'rest/api/registry/data_ext?registryCode=pharm_registry_products';
    urlSearch += '&countInPart=1&loadData=false';
    urlSearch += '&field=pharm_form_product_name&condition=TEXT_EQUALS&value=' + encodeURIComponent(productName);
    urlSearch += '&field1=textarea-hfvk2w&condition1=TEXT_EQUALS&value1=' + encodeURIComponent(shortname);
    urlSearch += '&field2=pharm_form_product_batch&condition2=TEXT_EQUALS&value2=' + encodeURIComponent(batch);
    urlSearch += '&field3=pharm_form_product_series&condition3=TEXT_EQUALS&value3=' + encodeURIComponent(series);
    urlSearch += '&field4=pharm_form_product_expmonth&condition4=CONTAINS&key4=' + expmonth.key;
    urlSearch += '&field5=pharm_form_product_expyear&condition5=CONTAINS&key5=' + expyear.key;

    //даты изготовления нет в исходниках
    // urlSearch += '&field4=pharm_form_product_prodmonth&condition4=CONTAINS&key4=04';
    // urlSearch += '&field5=pharm_form_product_prodyear&condition5=CONTAINS&key5=2020';

    let result = API.httpGetMethod(urlSearch);
    if(result.count != 0) {
      //Запись найдена
      let skladFormData = API.getFormData(result.data[0].dataUUID);
      UTILS.setValue(skladFormData, 'pharm_form_product_quality', quality);
      UTILS.setValue(skladFormData, 'pharm_form_product_certificate', certificate);
      UTILS.setValue(skladFormData, 'pharm_form_product_changedate', {key: currentDate, value: customFormatDate(currentDate)});
      result = API.mergeFormData(skladFormData);
      log.info(result);
    } else {
      //Запись не найдена, создаем новую
      let data = [];
      data.push({id: "pharm_form_product_name", type: "textarea", value: productName});
      data.push({id: "pharm_form_product_shortname", type: "textbox", value: shortname});
      data.push({id: "pharm_form_product_batch", type: "textbox", value: batch});
      data.push({id: "pharm_form_product_series", type: "textbox", value: series});
      data.push({id: "pharm_form_product_measure", type: "listbox", value: measure.value, key: measure.key});
      data.push({id: "pharm_form_product_expmonth", type: "listbox", value: expmonth.value, key: expmonth.key});
      data.push({id: "pharm_form_product_expyear", type: "listbox", value: expyear.value, key: expyear.key});
      data.push({id: "pharm_form_product_quality", type: "radio", value: quality.value, key: quality.key});
      data.push({id: "pharm_form_product_certificate", type: "radio", value: certificate.value, key: certificate.key});
      data.push({id: "pharm_form_product_changedate", type: "date", key: currentDate, value: customFormatDate(currentDate)});

      result = API.httpPostMethod("rest/api/registry/create_doc_rcc", {
        registryCode: 'pharm_registry_products',
        sendToActivation: true,
        data: data
      }, "application/json; charset=utf-8");
      log.info(result);
    }

  }

} catch (err) {
  log.error(err);
  message = err.message;
}
