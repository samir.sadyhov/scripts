let tableCmpID = 'pharm_form_fin_logbook_raw_table';

const getInfoOnRaw = () => {
  let result = [];
  let table = model.playerModel.getModelWithId(tableCmpID);
  table.modelBlocks.forEach(item => {
    let raw = model.playerModel.getModelWithId("raw", tableCmpID, item.tableBlockIndex);
    let shortname = model.playerModel.getModelWithId("shortname", tableCmpID, item.tableBlockIndex);
    if(raw.getValue() && shortname.getValue()) result.push(`${raw.getTextValue()} - ${shortname.getValue()}`);
  });
  return result.sort().join('\n');
}

const setRMB = () => {
  let raw_shortname = model.playerModel.getModelWithId('raw_shortname');
  setTimeout(() => {
    if(model.getValue()) {
      let info = getInfoOnRaw();
      raw_shortname.setValue(info);
    } else {
      raw_shortname.setValue(null);
    }    
  }, 200);
}

if(editable) {
  let rawTable = model.playerModel.getModelWithId(tableCmpID);

  model.on('valueChange', () => setRMB());

  rawTable.modelBlocks.forEach(item => {
    model.playerModel.getModelWithId("raw", tableCmpID, item.tableBlockIndex).on('valueChange', () => setRMB());
  });

  rawTable.on('tableRowAdd', (event, pModel, block) => {
    model.playerModel.getModelWithId("raw", tableCmpID, block.tableBlockIndex).on('valueChange', () => setRMB());
  });

  rawTable.on('tableRowDelete', () => setRMB());
}
