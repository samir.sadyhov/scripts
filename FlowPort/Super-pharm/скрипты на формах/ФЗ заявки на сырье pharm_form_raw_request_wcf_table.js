AS.SERVICES.showWaitWindow();
setTimeout(() => {
  AS.FORMS.ApiUtils.getDocumentIdentifier(model.playerModel.asfDataId)
  .then(docID => AS.FORMS.ApiUtils.getAsfDataUUID(docID))
  .then(uuid => AS.FORMS.ApiUtils.loadAsfData(uuid))
  .then(asfData => {
    model.setAsfData(asfData.data.find(x => x.id === 'pharm_form_raw_request_table'));
    $('[data-asformid="table.addRowButton.pharm_form_raw_request_wcf_table"]').hide();
    $('[data-asformid="table.removeRowButton.pharm_form_raw_request_wcf_table"]').hide();
    AS.SERVICES.hideWaitWindow();
  });
}, 100);
