//Накладная на приём сырья на склад
//pharm_form_waybill_main_table.raw
const sourceID = 'pharm_form_raw_measure';
const receiverID = 'measure';
const setMeasure = docID => {
  if(!editable) return;
  if(!docID) return;
  AS.FORMS.ApiUtils.getAsfDataUUID(docID)
  .then(uuid => AS.FORMS.ApiUtils.loadAsfData(uuid))
  .then(asfData => {
    let source = asfData.data.find(x => x.id === sourceID);
    if(source && source.hasOwnProperty('key')) {
      model.playerModel.getModelWithId(receiverID, model.asfProperty.ownerTableId, model.asfProperty.tableBlockIndex).setValue(source.key);
    }
  });
}
model.on('valueChange', (_1, _2, value) => setMeasure(value));

//Журнал учёта передачи готовой продукции - 1
//pharm_form_transfer_logbook_product_table.product
const sourceID = 'pharm_form_acc_logbook_product_measure';
const receiverID = 'product_measure';

const setMeasure = docID => {
  if(!editable) return;
  if(!docID) return;

  AS.FORMS.ApiUtils.getAsfDataUUID(docID)
  .then(uuid => AS.FORMS.ApiUtils.loadAsfData(uuid))
  .then(asfData => {
    let source = asfData.data.find(x => x.id === sourceID);
    let instock = asfData.data.find(x => x.id === 'pharm_form_acc_logbook_instock');
    if(source && source.hasOwnProperty('key')) {
      model.playerModel.getModelWithId(receiverID, model.asfProperty.ownerTableId, model.asfProperty.tableBlockIndex).setValue(source.key);
    }
    if(instock && instock.hasOwnProperty('key')) {
      model.playerModel.getModelWithId('amount_produced', model.asfProperty.ownerTableId, model.asfProperty.tableBlockIndex).setValue(instock.key);
    } else {
      model.playerModel.getModelWithId('amount_produced', model.asfProperty.ownerTableId, model.asfProperty.tableBlockIndex).setValue('0');
    }
  });
}
model.on('valueChange', (_1, _2, value) => setMeasure(value));


//Журнал выработки готовой продукции - 1
//pharm_form_fin_logbook_raw_table.raw
const sourceID = 'pharm_form_acc_logbook_raw_measure';
const receiverID = 'raw_measure';
const setMeasure = docID => {
  if(!editable) return;
  if(!docID) return;
  AS.FORMS.ApiUtils.getAsfDataUUID(docID)
  .then(uuid => AS.FORMS.ApiUtils.loadAsfData(uuid))
  .then(asfData => {
    let source = asfData.data.find(x => x.id === sourceID);
    if(source && source.hasOwnProperty('key')) {
      model.playerModel.getModelWithId(receiverID, model.asfProperty.ownerTableId, model.asfProperty.tableBlockIndex).setValue(source.key);
    }
  });
}
model.on('valueChange', (_1, _2, value) => setMeasure(value));

//Журнал выработки готовой продукции - 1
//pharm_form_fin_logbook_product_table.product
const sourceID = 'pharm_form_product_measure';
const receiverID = 'product_measure';
const setMeasure = docID => {
  if(!editable) return;
  if(!docID) return;
  AS.FORMS.ApiUtils.getAsfDataUUID(docID)
  .then(uuid => AS.FORMS.ApiUtils.loadAsfData(uuid))
  .then(asfData => {
    let source = asfData.data.find(x => x.id === sourceID);
    if(source && source.hasOwnProperty('key')) {
      model.playerModel.getModelWithId(receiverID, model.asfProperty.ownerTableId, model.asfProperty.tableBlockIndex).setValue(source.key);
    }
  });
}
model.on('valueChange', (_1, _2, value) => setMeasure(value));


//Журнал учёта выработки заготовок/изделий
//таблица Затраченное сырье
const matching = [];
matching.push({from: 'pharm_form_acc_logbook_product_measure', to: 'raw_measure'}); //Единица измерения
matching.push({from: 'pharm_form_acc_logbook_raw_shortname', to: 'shortname'}); //Партия сырья
matching.push({from: 'pharm_form_acc_logbook_series', to: 'series'}); //Серия
matching.push({from: 'pharm_form_acc_logbook_batch', to: 'batch'}); //Партия

const setMeasure = docID => {
  if(!editable) return;
  if(!docID) return;

  AS.FORMS.ApiUtils.getAsfDataUUID(docID)
  .then(uuid => AS.FORMS.ApiUtils.loadAsfData(uuid))
  .then(asfData => {

    matching.forEach(item => {
      let from = asfData.data.find(x => x.id === item.from);
      if(from) {
        let modelTo = model.playerModel.getModelWithId(item.to, model.asfProperty.ownerTableId, model.asfProperty.tableBlockIndex);
        switch (from.type) {
          case 'textbox':
          case 'textarea':
            if(modelTo && from.hasOwnProperty('value')) modelTo.setValue(from.value);
            break;
          default:
            if(modelTo && from.hasOwnProperty('key')) modelTo.setValue(from.key);
        }
      }
    });

  });
}

model.on('valueChange', (_1, _2, value) => setMeasure(value));


//Журнал учёта выработки заготовок/изделий (формы 11, 14, 21)
//таблица Заготовка/изделие
const matching = [];
matching.push({from: 'pharm_form_product_measure', to: 'product_measure'}); //Единица измерения
matching.push({from: 'pharm_form_product_batch', to: 'batch'}); //Партия
matching.push({from: 'pharm_form_product_shortname', to: 'shortname'}); //Краткое наименование
matching.push({from: 'pharm_form_product_series', to: 'series'}); //Серия

const setMeasure = docID => {
  if(!editable) return;
  if(!docID) return;

  AS.FORMS.ApiUtils.getAsfDataUUID(docID)
  .then(uuid => AS.FORMS.ApiUtils.loadAsfData(uuid))
  .then(asfData => {

    matching.forEach(item => {
      let from = asfData.data.find(x => x.id === item.from);
      if(from) {
        let modelTo = model.playerModel.getModelWithId(item.to, model.asfProperty.ownerTableId, model.asfProperty.tableBlockIndex);
        switch (from.type) {
          case 'textbox':
          case 'textarea':
            if(modelTo && from.hasOwnProperty('value')) modelTo.setValue(from.value);
            break;
          default:
            if(modelTo && from.hasOwnProperty('key')) modelTo.setValue(from.key);
        }
      }
    });

  });
}

model.on('valueChange', (_1, _2, value) => setMeasure(value));

let tableCmpID = 'pharm_form_fin_logbook_raw_table';

const getInfoOnRaw = () => {
  let result = [];
  let table = model.playerModel.getModelWithId(tableCmpID);
  table.modelBlocks.forEach(item => {
    let raw = model.playerModel.getModelWithId("raw", tableCmpID, item.tableBlockIndex);
    let shortname = model.playerModel.getModelWithId("shortname", tableCmpID, item.tableBlockIndex);
    if(raw.getValue() && shortname.getValue()) result.push(`${raw.getTextValue()} - ${shortname.getValue()}`);
  });
  return result.sort().join('\n');
}

const setRMB = () => {
  let raw_shortname = model.playerModel.getModelWithId('raw_shortname', model.asfProperty.ownerTableId, model.asfProperty.tableBlockIndex);
  setTimeout(() => {
    if(model.getValue()) {
      let info = getInfoOnRaw();
      raw_shortname.setValue(info);
    } else {
      raw_shortname.setValue(null);
    }    
  }, 200);
}

if(editable) {
  let rawTable = model.playerModel.getModelWithId(tableCmpID);

  model.on('valueChange', () => setRMB());

  rawTable.modelBlocks.forEach(item => {
    model.playerModel.getModelWithId("raw", tableCmpID, item.tableBlockIndex).on('valueChange', () => setRMB());
  });

  rawTable.on('tableRowAdd', (event, pModel, block) => {
    model.playerModel.getModelWithId("raw", tableCmpID, block.tableBlockIndex).on('valueChange', () => setRMB());
  });

  rawTable.on('tableRowDelete', () => setRMB());
}



//Журнал учёта передачи заготовок/изделий
//таблица Заготовка/изделие
const matching = [];
matching.push({from: 'pharm_form_acc_logbook_product_measure', to: 'product_measure'});
matching.push({from: 'pharm_form_acc_logbook_raw_shortname', to: 'raw_shortname'});
matching.push({from: 'pharm_form_acc_logbook_shortname', to: 'shortname'});
matching.push({from: 'pharm_form_acc_logbook_series', to: 'series'});
matching.push({from: 'pharm_form_acc_logbook_batch', to: 'batch'});
matching.push({from: 'pharm_form_acc_logbook_instock', to: 'amount_produced'});

const setMeasure = docID => {
  if(!editable) return;
  if(!docID) return;

  AS.FORMS.ApiUtils.getAsfDataUUID(docID)
  .then(uuid => AS.FORMS.ApiUtils.loadAsfData(uuid))
  .then(asfData => {

    matching.forEach(item => {
      let from = asfData.data.find(x => x.id === item.from);
      if(from) {
        let modelTo = model.playerModel.getModelWithId(item.to, model.asfProperty.ownerTableId, model.asfProperty.tableBlockIndex);
        switch (from.type) {
          case 'textbox':
          case 'textarea':
            if(modelTo && from.hasOwnProperty('value')) modelTo.setValue(from.value);
            break;
          default:
            if(modelTo && from.hasOwnProperty('key')) modelTo.setValue(from.key);
        }
      }
    });

  });
}

model.on('valueChange', (_1, _2, value) => setMeasure(value));


//Реестр поставок (pharm_registry_supply), форма "Поставка" (pharm_form_supply)
//таблица Заготовка/изделие
const matching = [];
matching.push({from: 'pharm_form_product_measure', to: 'measure'}); //Ед.измерения
// matching.push({from: 'ddd', to: 'country'}); //Страна
matching.push({from: 'pharm_form_product_series', to: 'series'}); //Серия
matching.push({from: 'pharm_form_product_batch', to: 'batch'}); //Партия
matching.push({from: 'pharm_form_product_prodmonth', to: 'prodmonth'}); //Месяц изготовления
matching.push({from: 'pharm_form_product_prodyear', to: 'prodyear'}); //Год изготовления
matching.push({from: 'pharm_form_product_expmonth', to: 'expmonth'}); //Срок годности (месяц)
matching.push({from: 'pharm_form_product_expyear', to: 'expyear'}); //Срок годности (год)

const setMeasure = docID => {
  if(!editable) return;
  if(!docID) return;

  AS.FORMS.ApiUtils.getAsfDataUUID(docID)
  .then(uuid => AS.FORMS.ApiUtils.loadAsfData(uuid))
  .then(asfData => {

    matching.forEach(item => {
      let from = asfData.data.find(x => x.id === item.from);
      if(from) {
        let modelTo = model.playerModel.getModelWithId(item.to, model.asfProperty.ownerTableId, model.asfProperty.tableBlockIndex);
        switch (from.type) {
          case 'textbox':
          case 'textarea':
            if(modelTo && from.hasOwnProperty('value')) modelTo.setValue(from.value);
            break;
          default:
            if(modelTo && from.hasOwnProperty('key')) modelTo.setValue(from.key);
        }
      }
    });

  });
}

model.on('valueChange', (_1, _2, value) => setMeasure(value));
