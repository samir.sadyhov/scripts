let raw = model.playerModel.getModelWithId('raw', model.asfProperty.ownerTableId, model.asfProperty.tableBlockIndex);
let rawTable = model.playerModel.getModelWithId(model.asfProperty.ownerTableId);
let instock = 0;

const getValue = (data, cmpID) => {
  data = data.data ? data.data : data;
  for(let i = 0; i < data.length; i++)
  if (data[i].id === cmpID) return data[i];
  return null;
};

const getCurrentTableCount = docID => {
  let tableAsfData = rawTable.getAsfData()[0];
  let summ = 0;
  tableAsfData.data.forEach(x => {
    if(x.hasOwnProperty('key') && x.key == docID) {
      let index = x.id.slice(x.id.indexOf('-b'));
      summ += Number(getValue(tableAsfData, `amount_spend${index}`).key) || 0;
    }
  });
  return summ;
};

const setFreeRaw = () => {
  let dostupno = instock - getCurrentTableCount(raw.getValue());
  view.container.attr('title', `Доступно не более ${dostupno}`);
  view.container.find('input').attr('placeholder', `Не более ${dostupno}`);
};

const updateParams = docID => {
  if(!editable) return;
  model.setValue(null);
  if(docID) {
    AS.FORMS.ApiUtils.getAsfDataUUID(raw.getValue())
    .then(uuid => AS.FORMS.ApiUtils.loadAsfData(uuid))
    .then(asfData => {
      asfData.data.forEach(x => {
        if(x.id == 'pharm_form_acc_logbook_instock') instock = x.hasOwnProperty('key') ? (x.key > 0 ? Number(x.key) : 0) : 0;
      });
      setFreeRaw();
    });
  } else {
    instock = 0;
    view.container.attr('title', '');
    view.container.find('input').attr('placeholder', i18n.tr('Введите число'));
  }
};

const checkCount = count => {
  if(!editable) return;
  if(!count) return;

  let docID = raw.getValue();
  if(!docID) {
    alert("Не выбрано наименование сырья");
    model.setValue(null);
    return;
  }

  if(Number(count) > instock) {
    alert(`Вы не можете забронировать больше доступного. Доступно ${instock}.`);
    model.setValue(null);
    return;
  }

  let cc = getCurrentTableCount(docID);
  if(cc > instock) {
    model.setValue(null);
    instock = instock - getCurrentTableCount(docID)
    alert(`Вы не можете забронировать больше доступного. Доступно ${instock}.`);
    return;
  }
};

raw.on('valueChange', (_1, _2, docID) => updateParams(docID));
model.on('valueChange', (_1, _2, count) => checkCount(count));
