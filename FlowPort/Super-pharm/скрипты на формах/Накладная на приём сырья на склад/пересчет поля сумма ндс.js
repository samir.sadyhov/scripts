let pharm_form_waybill_VAT = model.playerModel.getModelWithId('pharm_form_waybill_VAT');
let cmps = ['raw', 'amount1', 'amount2', 'unit_price'];

const recalc = () => {
  if(!editable) return;

  let nds = pharm_form_waybill_VAT.getValue();

  // Количество отпущено
  let amount2 = model.playerModel.getModelWithId('amount2', model.asfProperty.ownerTableId, model.asfProperty.tableBlockIndex);
  // Цена за единицу, KZT
  let unit_price = model.playerModel.getModelWithId('unit_price', model.asfProperty.ownerTableId, model.asfProperty.tableBlockIndex);
  // Сумма с НДС, KZT
  let sum = model.playerModel.getModelWithId('sum', model.asfProperty.ownerTableId, model.asfProperty.tableBlockIndex);

  let kol = Number(amount2.getValue()) || 0;
  let price = Number(unit_price.getValue()) || 0;

  if(!nds || nds[0] == 'Нет') {
    model.setValue(null);
    sum.setValue(String(kol * price));
    return;
  }

  sum.setValue(String(kol * price));
  model.setValue(String(kol * price * 12 / 112));
}

cmps.forEach(cmp => {
  let tmp = model.playerModel.getModelWithId(cmp, model.asfProperty.ownerTableId, model.asfProperty.tableBlockIndex);
  if(tmp) tmp.on('valueChange', () => recalc());
});

if(editable) pharm_form_waybill_VAT.on('valueChange', (_1, _2, value) => recalc());
