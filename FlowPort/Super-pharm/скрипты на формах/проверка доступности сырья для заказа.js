let raw = model.playerModel.getModelWithId('raw', model.asfProperty.ownerTableId, model.asfProperty.tableBlockIndex);
let rawTable = model.playerModel.getModelWithId(model.asfProperty.ownerTableId);
let pharm_form_raw_instock = 0;
let pharm_form_raw_booked = 0;
let raw_measure = '1';
let part = null;

const getValue = (data, cmpID) => {
  data = data.data ? data.data : data;
  for(let i = 0; i < data.length; i++)
  if (data[i].id === cmpID) return data[i];
  return null;
};

const getCurrentTableCount = docID => {
  let tableAsfData = rawTable.getAsfData()[0];
  let summ = 0;
  tableAsfData.data.forEach(x => {
    if(x.hasOwnProperty('key') && x.key == docID) {
      let index = x.id.slice(x.id.indexOf('-b'));
      summ += Number(getValue(tableAsfData, `amount${index}`).key) || 0;
    }
  });
  return summ;
};

const setFreeRaw = () => {
  let dostupno = pharm_form_raw_instock - pharm_form_raw_booked - getCurrentTableCount(raw.getValue());
  view.container.attr('title', `Доступно не более ${dostupno}`);
  view.container.find('input').attr('placeholder', `Не более ${dostupno}`);
};

const updateParams = docID => {
  if(!editable) return;
  model.setValue(null);
  if(docID) {
    AS.FORMS.ApiUtils.getAsfDataUUID(raw.getValue())
    .then(uuid => AS.FORMS.ApiUtils.loadAsfData(uuid))
    .then(asfData => {
      asfData.data.forEach(x => {
        if(x.id == 'pharm_form_raw_instock') pharm_form_raw_instock = x.hasOwnProperty('key') ? (x.key > 0 ? Number(x.key) : 0) : 0;
        if(x.id == 'pharm_form_raw_booked') pharm_form_raw_booked = x.hasOwnProperty('key') ? (x.key > 0 ? Number(x.key) : 0) : 0;
        if(x.id == 'pharm_form_raw_measure') raw_measure = x.hasOwnProperty('key') ? x.key : '1';
        if(x.id == 'pharm_form_raw_shortname') part = x.hasOwnProperty('value') ? x.value : null;
        model.playerModel.getModelWithId('measure', model.asfProperty.ownerTableId, model.asfProperty.tableBlockIndex).setValue(raw_measure);
        model.playerModel.getModelWithId('shortname', model.asfProperty.ownerTableId, model.asfProperty.tableBlockIndex).setValue(part);
      });
      setFreeRaw();
    });
  } else {
    pharm_form_raw_instock = 0;
    pharm_form_raw_booked = 0;
    raw_measure = '1';
    model.playerModel.getModelWithId('measure', model.asfProperty.ownerTableId, model.asfProperty.tableBlockIndex).setValue(raw_measure);
    model.playerModel.getModelWithId('shortname', model.asfProperty.ownerTableId, model.asfProperty.tableBlockIndex).setValue(null);
    view.container.attr('title', '');
    view.container.find('input').attr('placeholder', i18n.tr('Введите число'));
  }
};

const checkCount = count => {
  if(!editable) return;
  if(!count) return;

  let docID = raw.getValue();
  if(!docID) {
    alert("Не выбрано наименование сырья");
    model.setValue(null);
    return;
  }

  let dostupno = pharm_form_raw_instock - pharm_form_raw_booked;
  if(Number(count) > dostupno) {
    alert(`Вы не можете выдать больше доступного. Доступно ${dostupno}.`);
    model.setValue(null);
    return;
  }

  let cc = getCurrentTableCount(docID);
  if(cc > dostupno) {
    model.setValue(null);
    dostupno = dostupno - getCurrentTableCount(docID)
    alert(`Вы не можете выдать больше доступного. Доступно ${dostupno}.`);
    return;
  }
};

updateParams(raw.getValue());
raw.on('valueChange', (_1, _2, docID) => updateParams(docID));
model.on('valueChange', (_1, _2, count) => checkCount(count));
