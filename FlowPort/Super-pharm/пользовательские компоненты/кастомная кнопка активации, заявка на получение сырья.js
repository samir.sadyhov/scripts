let rawTable = model.playerModel.getModelWithId('pharm_form_raw_request_table');

const getValue = (asfData, id) => asfData.data.find(x => x.id === id);

const getCurrentTableCount = docID => {
  let tableAsfData = rawTable.getAsfData()[0];
  let summ = 0;
  tableAsfData.data.forEach(x => {
    if(x.hasOwnProperty('key') && x.key == docID) {
      let index = x.id.slice(x.id.indexOf('-b'));
      summ += Number(getValue(tableAsfData, `amount${index}`).key) || 0;
    }
  });
  return summ;
};

Array.prototype.uniq = function() {
  return this.filter(function(v, i, a){ return i == a.indexOf(v) });
}

AS.FORMS.ApiUtils.getDocumentIdentifier(model.playerModel.asfDataId).then(documentID => {
  let oldButton = $(`[data-button="APPROVE"][synergytest="DocumentCardRegistrySendButton"][document_id="${documentID}"]`);
  $('.super-pharm-button').remove();

  if(oldButton && oldButton.length) {
    let td = oldButton.parent();
    let newButton = $('<button>').addClass('super-pharm-button').text('Запустить');

    newButton.css({
      'width': '100%',
      'border': '1px solid #49b785',
      'border-radius': '4px',
      'background': '#49b785',
      'color': '#fff',
      'line-height': '30px',
      'overflow': 'hidden',
      'cursor': 'pointer',
      'text-align': 'center',
      'white-space': 'nowrap',
      'font-family': 'DroidSansBold',
      'font-size': '14px'
    }).hover(function() {
      $(this).css({
        'background': '#55cc95',
        'border-color': '#55cc95'
      });
    }, function() {
      $(this).css({
        'background': '#49b785',
        'border-color': '#49b785'
      });
    })

    newButton.on('click', e => {
      if(!model.playerModel.isValid()) {
        AS.SERVICES.showErrorMessage('Заполните обязательные поля');
        return;
      }

      let docIDs = [];
      let errors = 0;
      rawTable.getAsfData()[0].data.forEach(item => {
        if (item.id.slice(0, item.id.indexOf('-b')) === 'raw')
        if(item.hasOwnProperty('key')) docIDs.push(item.key);
      });

      AS.SERVICES.showWaitWindow();

      docIDs.uniq().forEach((docID, i) => {
        let instock = 0;
        let booked = 0;
        let dostupno = 0;
        let summRaw = getCurrentTableCount(docID);

        AS.FORMS.ApiUtils.getAsfDataUUID(docID)
        .then(uuid => AS.FORMS.ApiUtils.loadAsfData(uuid))
        .then(asfData => {
          asfData.data.forEach(x => {
            if(x.id == 'pharm_form_raw_instock') instock = x.hasOwnProperty('key') ? (x.key > 0 ? Number(x.key) : 0) : 0;
            if(x.id == 'pharm_form_raw_booked') booked = x.hasOwnProperty('key') ? (x.key > 0 ? Number(x.key) : 0) : 0;
          });

          dostupno = instock - booked;
          if(summRaw > dostupno) errors++;

          if(docIDs.length === (i+1)) {
            if(errors > 0) {
              AS.SERVICES.hideWaitWindow();
              AS.SERVICES.showErrorMessage('Вы не можете отправить заявку, так как введенное количество превышает доступное сырье');
              return;
            } else {
              AS.FORMS.ApiUtils.saveAsfData(model.playerModel.getAsfData().data, model.playerModel.formId, model.playerModel.asfDataId)
              .then(dataUUID => {
                AS.FORMS.ApiUtils.simpleAsyncGet(`rest/api/registry/activate_doc?dataUUID=${dataUUID}`).then(res => {
                  if(res.errorCode == 0) {
                    newButton.remove();
                    $('td > img[src*="edit.png"]').click();
                    $('td > img[src*="close.png"]').click();
                    setTimeout(() => {
                      $('body > div.gwt-DialogBox > div > table > tbody > tr.background-gray > td > table > tbody > tr:nth-child(2) > td > table > tbody > tr > td:nth-child(3) > table > tbody > tr > td.buttonCenterOldGray').click()
                    }, 100);
                    AS.SERVICES.hideWaitWindow();
                    window.showMessage('Документ успешно отправлен');
                  } else {
                    AS.SERVICES.hideWaitWindow();
                    AS.SERVICES.showErrorMessage('Произошла ошибка отправки документа. Обратитесь к администратору.');
                    console.log(res);
                  }
                });
              });
            }
          }
        });
      });
    });
    oldButton.hide();
    td.append(newButton);
  }
});
