let matching = []; //Сопоставление из выбранной записи в текущую
matching.push({from: 'dlitelnost', to: 'dlitelnost'});
matching.push({from: 'date_start', to: 'date_start'});
matching.push({from: 'date-_finish', to: 'date_finish'});
matching.push({from: 'comment', to: 'comment'});
matching.push({from: 'table-15xc94.file', to: 'file'}); // тут указывается таблица и компонент откуда брать значение, таблица.компонент
matching.push({from: 'resurs', to: 'resurs'});
matching.push({from: 'technica', to: 'technika'});
matching.push({from: ['numericinput-94u8ko', 'listbox-usk83j'], to: 'obem'}); // тут указывается если сразу несколько компонентов надо объеденить
matching.push({from: 'summa', to: 'cena'});

function getValue(data, id){
  if(Array.isArray(id)) {
    return id.map(itemId => {
      let tmp = data.data.filter(x => x.id === itemId)[0];
      if(tmp && tmp.value) return tmp.value;
    }).join(' ');
  }

  if(id.indexOf('.') !== -1) {
    let tableID = id.split('.')[0];
    let cmpID = id.split('.')[1];
    let tableData = data.data.filter(x => x.id === tableID)[0].data;
    if(Array.isArray(tableData)) {
      return tableData.map((itemId, i) => {
        let tmp = tableData.filter(x => x.id === cmpID+'-b'+(i+1))[0];
        if(tmp && tmp.value) return tmp.value;
      }).join('; ')+';';
    } else {
      return '';
    }
  }

  let tmp = data.data.filter(x => x.id === id)[0];
  if(tmp && tmp.value) return tmp.value;
}

model.matching = function(docID){
  if(!docID) return;
  let api = AS.FORMS.ApiUtils;
  let tableBlockIndex = model.asfProperty.tableBlockIndex;
  let tableName = model.asfProperty.ownerTableId;

  $.when(api.getAsfDataUUID(docID)).then(uuid => {
    return api.loadAsfData(uuid);
  }).then(asfData => {
    matching.forEach(id => {
      model.playerModel.getModelWithId(id.to, tableName, tableBlockIndex).setValue(getValue(asfData, id.from));
    });
  }).fail(err => console.log(err));
}

model.on("valueChange", function(_1, _2, value) {
  model.matching(value);
});
