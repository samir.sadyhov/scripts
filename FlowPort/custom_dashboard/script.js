new Vue({
  el: '#custom_dashboard',
  data: function() {
    return {
      appShow: false,
      date_period: "",
      optionurl: "",
      urlDashboard: "",
      h: "800px"
    }
  },
  mounted() {

    this.optionurl = `${window.origin}/kibana/app/kibana#/dashboard/Обращения?embed=true&_g=(time:(from:now-1y,mode:quick,to:now))`;
    this.urlDashboard = this.optionurl;

    setInterval(() => {
      let tsdFrame = $("iframe[src*='/Synergy/resources/wait/wait.white.gif#custom_dashboards']");
      if (tsdFrame.length > 0) {
        this.h = (tsdFrame.height() - 60) + 'px'
        if(!this.appShow) {
            tsdFrame.after($('#custom_dashboard'));
            tsdFrame.hide();
            this.appShow = true;
        }
      } else if (tsdFrame.length == 0) {
        this.appShow = false;
        this.date_period = "";
        this.urlDashboard = this.optionurl;
      }
    }, 500);


  },
  methods: {
    setPeriod() {
        this.urlDashboard = `${this.optionurl.split('?')[0]}?embed=true&_g=(time:(from:'${this.date_period[0]}',mode:absolute,to:'${this.date_period[1]}'))`
    },
  }
})
