// function getCommentsList(){
//   return $.when(AS.FORMS.ApiUtils.simpleAsyncPost(`rest/api/report/do?reportCode=comments_list&fileName=1.csv&documentID=${documentID}`, null, 'text'))
//   .then(data => {
//     return data.split('||\n').map(row => {
//       row = row.split(':::');
//       return {
//         author: row[0],
//         comment: row[1],
//         created: row[2],
//         deleted: row[3].split('.').length === 1 ? null : row[3]
//       }
//     }).filter(item => item.comment);
//   });
// }

function getCommentsList(){
  let reportUrl = `rest/api/report/do?reportCode=comments_list&fileName=1.html&documentID=${getDocID()}`;
  return $.when(AS.FORMS.ApiUtils.simpleAsyncPost(reportUrl, null, 'text')).then(data => parseResult(data));
}

function parseResult(html){
  html = $(html);
  html.find('img').attr("src", "");
  let table = html.find('table').attr("class", "custom-comment-table");
  table.find("tr").eq(0).remove();
  table.find("span").attr("class", "custom-comment-text");
  return table;
}

function reportDownload(){
  $.when(AS.FORMS.ApiUtils.simpleAsyncGet("rest/api/report/list"))
  .then(res => res.filter(item => item.code == "comments_list")[0])
  .then(report => {
    let reportURL = `formreport?reportid=${report.reportID}&inline=false&filename=comments.xls&documentID=${getDocID()}`;
    AS.SERVICES.showWaitWindow();
    $.when(window.open(reportURL)).then(() => AS.SERVICES.hideWaitWindow());
  });
}

function getDocID() {
  return window.location.href.split("#")[1].split('&').filter(x => x.indexOf('document_identifier') !== -1)[0].split('=')[1];
}

function showDialog(comments) {
  let modal = $('<div>')
  .attr("class", "custom-comment-modal")
  .append(comments);

  $("body").append(modal);
  modal.dialog({
      modal: true,
      width: 660,
      height: 600,
      resizable: true,
      title: i18n.tr("Комментарии к документу"),
      show: {
        effect: 'fade',
        duration: 500
      },
      hide: {
        effect: 'fade',
        duration: 500
      },
      buttons: [
        {
          text: i18n.tr("Скачать"),
          icon: "ui-icon-print",
          click: function() {
            reportDownload();
          }
        },
        {
          text: i18n.tr("Закрыть"),
          icon: "ui-icon-check",
          click: function() {
            $(this).dialog("close") ;
          }
        }
      ],
      close: function() {
        $(this).remove();
      }
  });
}

let customCommentButtonIsShow = false;

function addCommentButton(){
  if(window.location.hash.indexOf("submodule=common&action=open_document") == -1) {
    $('.custom-comment-button').remove();
    customCommentButtonIsShow = false;
    return;
  }

  let sendButton = $('[synergytest="DocumentCardSendButton"]');
  if(!sendButton[0]) return;
  if(customCommentButtonIsShow) return;

  let commentButton = $('<button>')
  .attr("class", "custom-comment-button")
  .text(i18n.tr("Комментарии"))
  .on('click', () => {
    getCommentsList().then(comments => showDialog(comments));
  });

  sendButton.parent().append(commentButton);

  let blockfiles = sendButton.parent().parent().parent().parent().parent().parent().parent().find('div.border-box-input');
  blockfiles.css({"height": "auto"});
  blockfiles.parent().css({"height": blockfiles.parent().height() - 35 + "px"});

  customCommentButtonIsShow = true;
}

(function () {
  setInterval(addCommentButton, 1000);
})();

/*
<style>
  .custom-comment-text {
    font-family: Arial !important;
    font-size: 14px !important;
  }

  .custom-comment-button {
    margin-top: 10px;
    width: 100%;
    height: 25px;
    border-radius: 5px;
    border: 1px solid #9bc537;
    background-color: #9bc537;
    color: #ffffff;
    font-family: arial, tahoma, sans-serif;
    font-size: 9pt;
    font-weight: bold;
    text-shadow: #000000 0 0 2px;
    cursor: pointer;
  }

  .custom-comment-button:hover {
    background-color: #82AA22;
    border-color: #82AA22;
  }

  .custom-comment-modal {
    margin: 20px;
  }

</style>

*/
