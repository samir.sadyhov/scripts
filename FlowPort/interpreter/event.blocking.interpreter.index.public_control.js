let ELASTIC_HOST = 'http://localhost:9200';
let INDEX_NAME = 'public_control_registry';
let INDEX_KEY = 'pc';

let client = new org.apache.commons.httpclient.HttpClient();
let creds = new org.apache.commons.httpclient.UsernamePasswordCredentials(login, password);
client.getParams().setAuthenticationPreemptive(true);
client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);

function getFormData(dataUUID) {
    let get = new org.apache.commons.httpclient.methods.GetMethod("http://127.0.0.1:8080/Synergy/rest/api/asforms/data/" + dataUUID);
    get.setRequestHeader("Content-type", "application/json");
    client.executeMethod(get);
    let resp = get.getResponseBodyAsString();
    get.releaseConnection();
    return JSON.parse(resp);
}

function putInIndex(data, elasticKey) {
    let put = new org.apache.commons.httpclient.methods.PutMethod(ELASTIC_HOST+"/"+INDEX_NAME+"/"+INDEX_KEY+"/" + encodeURIComponent(elasticKey));
    let body = new org.apache.commons.httpclient.methods.StringRequestEntity(JSON.stringify(data), "application/json", "UTF-8");
    put.setRequestEntity(body);
    client.executeMethod(put);
    put.releaseConnection();
}

function searchInJson(data, cmpID) {
  let result = {value: '', key: ''};
  data = data.data ? data.data : data;
  for (let i = 0; i < data.length; i++) {
    if (data[i].id == cmpID) {
      result = data[i];
      break;
    }
  }
  return result;
}

let result = true;
let message = "ok";

try {

  let asfData = getFormData(dataUUID);

  let indexData = {
    asfDataID: dataUUID,
    number: searchInJson(asfData, 'number').value || "",
    date_create: searchInJson(asfData, 'date_create').key.split(' ')[0] || "",
    decision_date: searchInJson(asfData, 'decision_date').key.split(' ')[0] || "",

    type_value: searchInJson(asfData, 'type').value || "",
    type_key: searchInJson(asfData, 'type').key || "",
    category_value: searchInJson(asfData, 'category').value || "",
    category_key: searchInJson(asfData, 'category').key || "",
    problem_value: searchInJson(asfData, 'problem').value || "",
    problem_key: searchInJson(asfData, 'problem').key || "",
    fakultet_value: searchInJson(asfData, 'fakultet').value || "",
    fakultet_key: searchInJson(asfData, 'fakultet').key || "",
    status_value: searchInJson(asfData, 'status').value || "",
    status_key: searchInJson(asfData, 'status').key || "",
    problem_duration_value: searchInJson(asfData, 'problem_duration').key || "",
    problem_duration_key: searchInJson(asfData, 'problem_duration').value || "",

    deleted: 0
  };

  let elasticKey = INDEX_NAME + "-" + dataUUID;
  putInIndex(indexData, elasticKey);

  message = 'index data added\n[key: ' + elasticKey + ']';

} catch (err) {
  message = 'ERROR: ' + err.message;
}
