let client = new org.apache.commons.httpclient.HttpClient();
let creds = new org.apache.commons.httpclient.UsernamePasswordCredentials('1', '1');
client.getParams().setAuthenticationPreemptive(true);
client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);


function getFormData(asfDataId) {
  let get = new org.apache.commons.httpclient.methods.GetMethod("http://127.0.0.1:8080/Synergy/rest/api/asforms/data/" + asfDataId);
  get.setRequestHeader("Content-type", "application/json");
  client.executeMethod(get);
  let resp = get.getResponseBodyAsString();
  get.releaseConnection();
  return JSON.parse(resp);
}

function getAsfDataId(documentID) {
  let get = new org.apache.commons.httpclient.methods.GetMethod("http://127.0.0.1:8080/Synergy/rest/api/formPlayer/getAsfDataUUID?documentID=" + documentID);
  get.setRequestHeader("Content-type", "application/json");
  client.executeMethod(get);
  let resp = get.getResponseBodyAsString();
  get.releaseConnection();
  return resp;
}

function getDocumentID(asfDataId) {
  let get = new org.apache.commons.httpclient.methods.GetMethod("http://127.0.0.1:8080/Synergy/rest/api/formPlayer/documentIdentifier?dataUUID=" + asfDataId);
  get.setRequestHeader("Content-type", "application/json");
  client.executeMethod(get);
  let resp = get.getResponseBodyAsString();
  get.releaseConnection();
  return resp;
}

function saveFormData(form, uuid, data) {
  let post = new org.apache.commons.httpclient.methods.PostMethod("http://127.0.0.1:8080/Synergy/rest/api/asforms/form/multipartdata");
  post.addParameter("form", form);
  post.addParameter("uuid", uuid);
  post.addParameter("data", "\"data\":" + JSON.stringify(data));
  post.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=utf-8");
  client.executeMethod(post);
  let resp = post.getResponseBodyAsString();
  post.releaseConnection();
  return resp;
}

function searchInJson(data, cmpID) {
  let result = null;
  let tmpData = data.data ? data.data : data;
  for (let i = 0; i < tmpData.length; i++) {
    if (tmpData[i].id == cmpID) {
      result = tmpData[i];
      break;
    }
  }
  return result;
}

function createRow(type, id, value, key, valueID) {
  let result = {id: id, type: type};
  if (value) result.value = value;
  if (key) result.key = key;
  if (valueID) result.valueID = valueID;
  return result;
}

function coundIDS(data, cmp) {
  let res = 0;
  data.forEach(function(item) {
    if (item.id.slice(0, item.id.indexOf('-b')) === cmp) res++;
  });
  return res === 0 ? 1 : ++res;
}

function getCurParseDate() {
  let today = new Date();
  let month = Number(today.getMonth() + 1);
  if (month < 10) month = '0' + month;
  return ('0' + today.getDate()).slice(-2) + '.' + month + '.' + today.getFullYear();
}

let result = true;
let message = "ok";

try {

  let tableID = "table-ysyn2p";
  let currentDocumentID = getDocumentID(dataUUID)+"";
  let docLinkID = searchInJson(getFormData(dataUUID), "reglink-rbn3ni").key+"";
  let linkAsfDataID = getAsfDataId(docLinkID);
  let linkAsfData = getFormData(linkAsfDataID);

  let tableData = searchInJson(linkAsfData, tableID);
  tableData.data.push(
    createRow("reglink",
    "reglink-d102xk-b" + coundIDS(tableData.data, "reglink-d102xk"),
    "Документ", currentDocumentID, currentDocumentID)
  );
  tableData.data.push(
    createRow("textbox", "textbox-63w7ii-b" + coundIDS(tableData.data, "textbox-63w7ii"), getCurParseDate())
  );

  let saveresult = saveFormData(linkAsfData.form, linkAsfData.uuid, linkAsfData.data);

  message += ' ::: asfDataId: ' + saveresult;

} catch (e) {
  message = 'ERROR ::: ' + e.message;
}
