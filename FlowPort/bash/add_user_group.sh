# код группы куда надо добавить всех юзеров, передается параметром в скрипт
GROUP_CODE=$1

# mySQL настройки
mysqlUser="root"
mysqlPass="root"
mysqlDB="synergy"
mysqlHost="localhost"

# вспомогательные настройки
logFile="./add_user_group.log"
scriptName=${0##*/}
tmpsql=$(mktemp)


function now_time () {
  date +"%Y-%m-%d %H:%M:%S"
}
function logging () {
  echo -e "`now_time` #$1# [$2] $3" >> $logFile
}

# Получение ID группы
# $1 group code
function getGroupID () {
  local tmp=$(mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use $mysqlDB; set names utf8; SELECT id FROM groups WHERE code = '$1';" 2>/dev/null)
  echo `echo $tmp | cut -d " " -f2`
}

# $1 groupid
# $2 userid
function addUserToGroup () {
  local tmp=$(mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use $mysqlDB; set names utf8; INSERT INTO usergroup (groupid, useridentifier) VALUES ($1, '$2');" 2>/dev/null)
  echo `echo $tmp | cut -d " " -f2`
  tmp=$(mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use $mysqlDB; set names utf8; INSERT INTO realusergroup (groupid, useridentifier) VALUES ($1, '$2');" 2>/dev/null)
  echo `echo $tmp | cut -d " " -f2`
}

echo '' >> $logFile
logging $scriptName START "Начало работы скрипта"

groupID=$(getGroupID $GROUP_CODE)

echo '' >> $logFile
logging $scriptName INFO "groupID: $groupID"

echo '' >> $logFile
logging $scriptName INFO "Поиск пользователей для добавления в группу: $GROUP_CODE"

mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use $mysqlDB; set names utf8;
SELECT userid FROM users
WHERE userid NOT IN (SELECT useridentifier FROM realusergroup WHERE groupid = $groupID)
AND finished IS NULL;" > $tmpsql 2>/dev/null

if [ -s $tmpsql ];
then
  countStr=`cat $tmpsql | wc -l`

  for i in `seq 2 $countStr`;
  do
    userid=`cat $tmpsql | sed -n $i'p' | cut -f1`

    logging $scriptName INFO "userid: $userid"
    logging $scriptName addUserToGroup "$(addUserToGroup $groupID $userid)"

  done
else
  logging $scriptName INFO "не найдено новых пользователей для добавления в группу"
fi

echo '' >> $logFile
logging $scriptName STATUS "Удаление временных файлов"
rm -rf $tmpsql
logging $scriptName END "Завершение работы скрипта"
exit 0
