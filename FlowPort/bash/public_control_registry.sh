#!/bin/bash

source $(dirname $(readlink -e $0))/utils.class
scriptName=${0##*/}
tmpsql=$(mktemp)

indexName='public_control_registry'
indexType='pb'

echo '' >> $logFile
logging $scriptName START "Начало работы скрипта"
logging $scriptName INFO "Собираем данные для индекса $indexName"

mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8;
SELECT rg.asfDataID,
IFNULL(a.cmp_data, '') number,
IFNULL(DATE(b.cmp_key), '') date_create,
IFNULL(DATE(c.cmp_key), '') decision_date,

IFNULL(d.cmp_data, '') type_value, IFNULL(d.cmp_key, '') type_key,
IFNULL(e.cmp_data, '') category_value, IFNULL(e.cmp_key, '') category_key,
IFNULL(f.cmp_data, '') problem_value, IFNULL(f.cmp_key, '') problem_key,
IFNULL(g.cmp_data, '') fakultet_value, IFNULL(g.cmp_key, '') fakultet_key,
IFNULL(h.cmp_data, '') status_value, IFNULL(h.cmp_key, '') status_key,
IFNULL(i.cmp_key, '') problem_duration_value, IFNULL(i.cmp_data, '') problem_duration_key


FROM registries r
LEFT JOIN registry_documents rg ON rg.registryID = r.registryID
LEFT JOIN asf_data_index a ON a.uuid = rg.asfDataID AND a.cmp_id='number'
LEFT JOIN asf_data_index b ON b.uuid = rg.asfDataID AND b.cmp_id='date_create'
LEFT JOIN asf_data_index c ON c.uuid = rg.asfDataID AND c.cmp_id='decision_date'

LEFT JOIN asf_data_index d ON d.uuid = rg.asfDataID AND d.cmp_id='type'
LEFT JOIN asf_data_index e ON e.uuid = rg.asfDataID AND e.cmp_id='category'
LEFT JOIN asf_data_index f ON f.uuid = rg.asfDataID AND f.cmp_id='problem'
LEFT JOIN asf_data_index g ON g.uuid = rg.asfDataID AND g.cmp_id='fakultet'
LEFT JOIN asf_data_index h ON h.uuid = rg.asfDataID AND h.cmp_id='status'
LEFT JOIN asf_data_index i ON i.uuid = rg.asfDataID AND i.cmp_id='problem_duration'


WHERE r.code = 'public_control_registry' AND rg.active IN ('Y', 'P')
AND rg.deleted IS NULL;" > $tmpsql 2>/dev/null

if [ -s $tmpsql ];
then
  # Количество строк в файле tmpsql
  countStr=`cat $tmpsql | wc -l`

  for i in `seq 2 $countStr`;
  do
    asfDataID=`cat $tmpsql | sed -n $i'p' | cut -f1`
    number=`cat $tmpsql | sed -n $i'p' | cut -f2`
    date_create=`cat $tmpsql | sed -n $i'p' | cut -f3`
    decision_date=`cat $tmpsql | sed -n $i'p' | cut -f4`
    type_value=`cat $tmpsql | sed -n $i'p' | cut -f5`
    type_key=`cat $tmpsql | sed -n $i'p' | cut -f6`
    category_value=`cat $tmpsql | sed -n $i'p' | cut -f7`
    category_key=`cat $tmpsql | sed -n $i'p' | cut -f8`
    problem_value=`cat $tmpsql | sed -n $i'p' | cut -f9`
    problem_key=`cat $tmpsql | sed -n $i'p' | cut -f10`
    fakultet_value=`cat $tmpsql | sed -n $i'p' | cut -f11`
    fakultet_key=`cat $tmpsql | sed -n $i'p' | cut -f12`
    status_value=`cat $tmpsql | sed -n $i'p' | cut -f13`
    status_key=`cat $tmpsql | sed -n $i'p' | cut -f14`
    problem_duration_value=`cat $tmpsql | sed -n $i'p' | cut -f15`
    problem_duration_key=`cat $tmpsql | sed -n $i'p' | cut -f16`

    echo '' >> $logFile
    logging $scriptName INFO "asfDataID: $asfDataID"

    iData='{"asfDataID": "'$asfDataID'", "number": "'$number'",'
    iData+='"date_create": "'$date_create'", "decision_date": "'$decision_date'",'
    iData+='"type_value": "'$type_value'", "type_key": "'$type_key'",'
    iData+='"category_value": "'$category_value'", "category_key": "'$category_key'",'
    iData+='"problem_value": "'$problem_value'", "problem_key": "'$problem_key'",'
    iData+='"fakultet_value": "'$fakultet_value'", "fakultet_key": "'$fakultet_key'",'
    iData+='"status_value": "'$status_value'", "status_key": "'$status_key'",'
    iData+='"problem_duration_value": "'$problem_duration_value'", "problem_duration_key": "'$problem_duration_key'",'
    iData+='"deleted": 0}'
    logging $scriptName RESULT "iData: $iData"

    elasticKey=$indexName"-"$asfDataID
    logging $scriptName RESULT "elasticKey: $elasticKey"

    logging $scriptName INFO "Обновление индекса"
    logging $scriptName INFO "$elasticHost/$indexName/$indexType/$elasticKey"
    addIndexData "$elasticHost/$indexName/$indexType/$elasticKey" "$iData"
    logging $scriptName RESULT "`cat $tmpfile`"

  done
else
  logging $scriptName INFO "нет новых данных"
fi

echo '' >> $logFile
logging $scriptName STATUS "Удаление временных файлов"
rm -rf $tmpsql
rm -rf $tmpfile
logging $scriptName END "Завершение работы скрипта"
exit 0
