function isValid()
{
	if (document.getElementById("login").value == null || document.getElementById("login").value.length == 0)
	{	
		alert("Enter login");
		document.getElementById("login").focus();
		return false;
	}
	if (document.getElementById("pswd").value == null || document.getElementById("pswd").value.length == 0)
	{
		alert("Enter password");
		document.getElementById("pswd").focus();
		return false;
	}
	return true;
}
		var locale_servlet = "ru";
		var buttonURL = "images/enter/enter.ru.png";
		var buttonPressedURL = "images/enter/enter.ru.pressed.png";
		function readCookie(name) 
		{
			var nameEQ = name + "=";
			var ca = document.cookie.split(';');
			for(var i=0;i < ca.length;i++) {
			 var c = ca[i];
			 while (c.charAt(0)==' ') c = c.substring(1,c.length);
			 if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length,c.length);
		 }
		 return null;
	   }
	   var ssessionID= readCookie('JSESSIONID');
	   $.cookie('JSESSIONID', ssessionID, { expires: 7, path: '/' });
	   function useLocale(){
		var locale_cookie = readCookie("management_locale");
				try{
					ruStyle = "link";
					kzStyle = "link";
					enStyle = "link";
					if (!locale_cookie){
						locale_cookie = locale_servlet;
					}
					if (locale_cookie == "kz"){
						kzStyle = "linkSelected";
						buttonURL = "images/enter/enter.kz.png";
						buttonPressedURL = "images/enter/enter.kz.pressed.png";
					} else if (locale_cookie == "en"){
						enStyle = "linkSelected";
						buttonURL = "images/enter/enter.en.png";
						buttonPressedURL = "images/enter/enter.en.pressed.png";
					} else {
						ruStyle = "linkSelected";
					}
				}catch (e){
					alert(e);
				}
	  }
	  function langbtnset(loc)
	  {
			if (loc == "kz")
			{
				$('#lngKZ').attr("class","btn btn-primary");
				$('#lngRU').attr("class","btn btn-default");
				$('#lngEN').attr("class","btn btn-default");
			} 
			else if (loc == "en")
			{
				$('#lngKZ').attr("class","btn btn-default");
				$('#lngRU').attr("class","btn btn-default");
				$('#lngEN').attr("class","btn btn-primary");
			}
			else 
			{
				$('#lngKZ').attr("class","btn btn-default");
				$('#lngRU').attr("class","btn btn-primary");
				$('#lngEN').attr("class","btn btn-default");
			}	  
	  }
	  function followLink(loc)
	  {
		langbtnset(loc);
		newCookie("management_locale", loc, 0);
		window.location = "?locale="+loc;		
	  }	  
	  function disablePlaceholder(e){
		e.removeAttribute("placeHolder");
	  }
	  function enablePlaceholder(e, s){
		e.setAttribute("placeHolder", s);
	  }
	  function saveLogin() {
		newCookie("recoveryLogin", encodeURIComponent(document.getElementById('login').value), 0);
	  }
	  function recoverPassword(){
		saveLogin();
		window.location = "recoverPassword";
	  }

var locale_cookie = readCookie("management_locale");
var me_rememb = readCookie("mbo_user_remember__Synergy");
	if (me_rememb=='true'){
	  $("#remember_me").attr("checked",true);
	}
	if (locale_cookie == "kz")
	{
		langbtnset(locale_cookie);
		$('#auth').text("Авторизациалау");
		$('#login').attr("placeholder","Логин");
		$('#login_label').text("Логин");
		$('#pswd').attr("placeholder","Құпия сөз");
		$('#password_label').text("Құпия сөз");
		$('#enter_label').text("Кіру");
		$('#remember_me_label').text("Еске сақтау");		
	}
	else if (locale_cookie == "en")
	{
		langbtnset(locale_cookie);
		$('#auth').text("Authorization");
		$('#login').attr("placeholder","Login");
		$('#login_label').text("Login");
		$('#pswd').attr("placeholder","Password");
		$('#password_label').text("Password");
		$('#enter_label').text("Enter");
		$('#remember_me_label').text("Remember");	
	} else 
	{
		langbtnset(locale_cookie);
		$('#auth').text("Авторизация");
		$('#login').attr("placeholder","Логин");
		$('#login_label').text("Логин");
		$('#pswd').attr("placeholder","Пароль");
		$('#password_label').text("Пароль");
		$('#enter_label').text("Войти");
		$('#remember_me_label').text("Запомнить");
	}
//$.cookie('management_locale', locale_cookie);
function change_show_password()
{
	if ($('#pswd').attr("type")==="password")
	{
		$('#pswd').attr("type","text");
		$('#password_show_icon').attr("class","glyphicon glyphicon-eye-open");
	}
	else
	{
		$('#pswd').attr("type","password");
		$('#password_show_icon').attr("class","glyphicon glyphicon-eye-close");
	}
}
