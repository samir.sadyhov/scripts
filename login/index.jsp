<!DOCTYPE html>
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="ru">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<meta http-equiv="content-type" content="text/html; charset=utf-8" />
	<meta name='yandex-verification' content='6f4950d6841e80cd' />
	<title>SYNERGY.ENU</title>
	<%@ page contentType="text/html;charset=UTF-8"%>
	<%@ page session="true" %>
	<link rel="shortcut icon" href="static/images/favicon.ico" >
	<link rel="stylesheet" href="static/css/bootstrap.min.css" media="screen">
	<link rel="stylesheet" href="static/css/bootswatch.css">
	<link rel="stylesheet" href="static/css/b_menu.css">
	<link rel="stylesheet" href="static/css/font-awesome.min.css" media="screen">
	<script language="javascript" src="static/js/jquery.js"></script>
	<script language="javascript" src="static/js/bootstrap.min.js"></script>
	<script language="javascript" src="static/js/bootswatch.js"></script>
	<script language="javascript" src="static/js/common.js"></script>
	<script language="javascript" src="static/js/jquery.cookie.js"></script> 
</head>
<body>
<div class="navbar navbar-default navbar-fixed-top">
		<div class="container-fluid">
			<div class="navbar-header">			
				<a href="https://doc.enu.kz/" class="navbar-brand"><i class="glyphicon glyphicon-file" aria-hidden="true"></i> SYNERGY.ENU</a>			
				<button class="navbar-toggle" type="button" data-toggle="collapse" data-target="#navbar-main">
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
					<span class="icon-bar"></span>
				</button>			
			</div>
			<div class="navbar-collapse collapse" id="navbar-main">
				<ul class="nav navbar-nav">
					<li class="dropdown">
						<a class="dropdown-toggle" data-toggle="dropdown" href="#" id="download"><span class="glyphicon glyphicon-th-large"></span> Проекты<span class="caret"></span></a>
							<ul class="dropdown-menu" aria-labelledby="download">										
								<li><a href="http://enu.kz/" title="Официальный сайт университета - ENU.KZ"> <span class="glyphicon glyphicon-heart"></span> ENU.KZ</a></li>
								<li><a href="http://srvrk.enu.kz/" title="Совет ректоров высших учебных заведений Республики Казахстан"> <span class="fa fa-graduation-cap"></span> Совет ректоров РК</a></li>
								<li class="dropdown-submenu">
									<a tabindex="-1" href="#"><span class="fa fa-th-large"></span> Сайты факультетов</a>
										<ul class="dropdown-menu">
											<li><a href="http://asf.enu.kz/">Архитектурно-строительный факультет</a></li>
											<li><a href="http://fen.enu.kz/">Факультет естественных наук</a></li>
											<li><a href="http://fjp.enu.kz/">Факультет журналистики и политологии</a></li>
											<li><a href="http://fit.enu.kz/">Факультет информационных технологий</a></li>
											<li><a href="http://hist.enu.kz/">Факультет истории</a></li>
											<li><a href="http://fmo.enu.kz/">Факультет международных отношений</a></li>
											<li><a href="http://mmf.enu.kz/">Механико-математический факультет</a></li>
											<li><a href="http://fsn.enu.kz/">Факультет социальных наук</a></li>
											<li><a href="http://tef.enu.kz/">Транспортно-энергетический факультет</a></li>
											<li><a href="http://ftf.enu.kz/">Физико-технический факультет</a></li>
											<li><a href="http://ff.enu.kz/">Филологический факультет</a></li>
											<li><a href="http://ef.enu.kz/">Экономический факультет</a></li>
											<li><a href="http://yur.enu.kz/">Юридический факультет</a></li>
											<li class="divider"></li>
											<li><a href="http://military.enu.kz/">Военная кафедра</a></li>
										</ul>
								</li>
								<li class="dropdown-submenu">
									<a tabindex="-1" href="#"><span class="fa fa-th-large"></span> Библиотека</a>
										<ul class="dropdown-menu">
											<li><a href="http://lib.enu.kz/" title="">LIB.ENU - сайт библиотеки</a></li>
											<li><a href="http://enulib.enu.kz/" title="">ENULIB.ENU - электронная библиотека</a></li>
											<li><a href="http://catalog.enu.kz/" title="Библиотечный онлайн каталог - KABIS">CATALOG - библиотечный каталог</a></li>
											<li><a href="http://repository.enu.kz/" title="База данных научных статьей - DSpace">REPOSITORY - база данных научных статьей</a></li>
											<li><a href="http://emedia.enu.kz">emedia.enu.kz</a></li>
										</ul>
								</li>
								<li class="divider"></li>							
								<li><a href="http://edu.enu.kz/" title="Система управления учебным процессом - Platonus"><span class="glyphicon glyphicon-briefcase"></span> PLATONUS.ENU</a></li>
								<li><a href="http://doc.enu.kz/" title="Система электронного документооборота - SYNERGY"><span class="glyphicon glyphicon-file"></span> SYNERGY.ENU</a></li>
								<li><a href="http://mail.enu.kz/" title="Система корпоративной электронной почты - WEBMAIL"><span class="glyphicon glyphicon-envelope"></span> MAIL.ENU</a></li>
								<li><a href="http://my.enu.kz/"><span class="fa fa-university"></span> MY.ENU</a></li>
								<li><a href="http://tel.enu.kz/"><span class="fa fa-phone"></span> TEL.ENU</a></li>
								<li class="divider"></li>
								<li><a href="http://moodle.enu.kz/" title="Образовательный портал - MOODLE">MOODLE.ENU</a></li>
								<li><a href="http://anketa.enu.kz/" title="Система анкетирования - ANKETA.ENU">ANKETA.ENU (students)</a></li>
							</ul>
					</li>					
				</ul>
				<ul class="nav navbar-nav navbar-right">
						<li class="dropdown">		
							<a class="dropdown-toggle" data-toggle="dropdown" href="#" id="download"><span class="glyphicon glyphicon-globe"></span> Язык<span class="caret"></span></a>
							<ul class="dropdown-menu" aria-labelledby="download">
								<li><a href="#lang_kz" onclick="followLink('kz')">KZ-Қазақ</a></li>
								<li><a href="#lang_ru" onclick="followLink('ru')">RU-Русский</a></li>
								<li><a href="#lang_en" onclick="followLink('en')">EN-English</a></li>
							</ul>
						</li>
						<li><a href="#notice" data-toggle="modal" data-target="#m_notice"><span class="badge" id="p_notice_count"></span> Объявление</a></li>
						<li><a href="#contacts" data-toggle="modal" data-target="#m_contacts"><span class="glyphicon glyphicon-earphone"></span> Контакты</a></li>
						<li><a href="#about" data-toggle="modal" data-target="#m_about"><span class="glyphicon glyphicon-question-sign"></span> О проекте</a></li>
					</ul>
			</div>	
		</div>
	</div>
</br>
<div class="container">	
	<center>
		<div class="row-fluid">
			<div class="col-md-6 col-md-offset-3">
				<form class="form-signin" role="form" action="/Synergy/loginservlet" method="post" ID="loginForm">
					<input type="hidden" name="secfield" value="0"/>
					<input type="hidden" name="role" value="ienu"/>
					<div class="panel panel-default">
						<div class="panel-heading"><div class="logoenu"></div></div>
						<div class="panel-heading" id="p_auth"><span id="auth">Авторизация</span></div>
						<div class="panel-body">								
								<div style="width: 100%;  padding: 2px" class="input-group">
									<span style="width:110px;" class="input-group-addon"><span class="glyphicon glyphicon-user"></span> <span id="login_label">Логин</span>:</span>
									<input type="text" class="form-control" placeholder="Логин" name="login" id="login" required autofocus>
								</div>
								<div id="result_check_iin"></div>
								<div style="width: 100%;  padding: 2px" class="input-group">
									<span style="width:110px;" class="input-group-addon"><span class="glyphicon glyphicon-lock"></span> <span id="password_label">Пароль</span>:</span>
									<input style="height:38px" type="password" class="form-control" placeholder="Пароль" name="pswd" id="pswd" required>
									<div class="input-group-btn">
										<button type="button" style="width:50px;height:38px" class="btn btn-success" onclick="change_show_password()"><span class="glyphicon glyphicon-eye-close" id="password_show_icon"></span> </button>
									</div>
								</div>
								<button class="btn btn-lg btn-info btn-block" type="submit" id="submit"><span class="glyphicon glyphicon-log-in"></span> <span id="enter_label">Войти</span></button>
						</div>
						<div class="panel-footer">
							<center>
								<div class="btn-group" role="group" aria-label="langs">
								  <button type="button" class="btn btn-default" id="lngKZ" onclick="followLink('kz')">Қазақша</button>
								  <button type="button" class="btn btn-primary" id="lngRU" onclick="followLink('ru')">Русский</button>
								  <button type="button" class="btn btn-default" id="lngEN" onclick="followLink('en')">English</button>
								</div>
							</center>
						</div>
					</div>
				</form>
			</div>
		</div>
	</center>
</div>
    <div class="container">
		<nav>
		  <ul class="pagination pagination-sm">        
			<li><a href="http://www.enu.kz/ru/blog-rectora/" onclick="pageTracker._link(this.href); return false;"><span class="glyphicon glyphicon-user"></span> Rector</a></li>
			<li><a href="http://www.enu.kz/ru/info/novosti-enu/rss/"><span class="glyphicon glyphicon-bell"></span> RSS</a></li>
			<li><a href="https://twitter.com/ENUofficial"><span class="glyphicon glyphicon-bullhorn"></span> Twitter</a></li>
			<li><a href="http://www.facebook.com/gumilyov.enu"><span class="glyphicon glyphicon-thumbs-up"></span> Facebook</a></li>
			<li class="pull-right"><a href="#top"><span class="glyphicon glyphicon-arrow-up"></span> Вверх</a></li>
		  </ul>
		</nav>
			<p>© Arta Software, 2005–2016, <a href="http://arta.pro/">arta.pro</a></br>Все права защищены. Все торговые марки являются собственностью их правообладателей.</p>
			<p>
				<link rel="stylesheet" href="static/css/enu_style.css">	 
				<script language="javascript" src="static/js/enu_script.js"></script>
				<!-- Yandex.Metrika informer -->
				<a href="https://metrika.yandex.ru/stat/?id=23934448&amp;from=informer"
				target="_blank" rel="nofollow"><img src="//bs.yandex.ru/informer/23934448/2_1_FFFFFFFF_EFEFEFFF_0_uniques"
				style="width:80px; height:31px; border:0;" alt="Яндекс.Метрика" title="Яндекс.Метрика: данные за сегодня (уникальные посетители)" onclick="try{Ya.Metrika.informer({i:this,id:23934448,lang:'ru'});return false}catch(e){}"/></a>
				<!-- /Yandex.Metrika informer -->

				<!-- Yandex.Metrika counter -->
				<script type="text/javascript">
				(function (d, w, c) {
					(w[c] = w[c] || []).push(function() {
						try {
							w.yaCounter23934448 = new Ya.Metrika({id:23934448,
									webvisor:true,
									clickmap:true,
									trackLinks:true,
									accurateTrackBounce:true,
									trackHash:true});
						} catch(e) { }
					});

					var n = d.getElementsByTagName("script")[0],
						s = d.createElement("script"),
						f = function () { n.parentNode.insertBefore(s, n); };
					s.type = "text/javascript";
					s.async = true;
					s.src = (d.location.protocol == "https:" ? "https:" : "http:") + "//mc.yandex.ru/metrika/watch.js";

					if (w.opera == "[object Opera]") {
						d.addEventListener("DOMContentLoaded", f, false);
					} else { f(); }
				})(document, window, "yandex_metrika_callbacks");
				</script>
				<noscript><div><img src="//mc.yandex.ru/watch/23934448" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
				<!-- /Yandex.Metrika counter -->
				<div class="modal fade" id="m_notice" tabindex="-1" role="dialog" aria-labelledby="m_notice_ModalLabel">
				  <div class="modal-dialog" role="document">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="m_notice_ModalLabel">Объявления</h4>
					  </div>
					  <div class="modal-body">
						<div id="p_notice_content"></div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
					  </div>
					</div>
				  </div>
				</div>
				<div class="modal fade" id="m_contacts" tabindex="-1" role="dialog" aria-labelledby="m_contacts_ModalLabel">
				  <div class="modal-dialog" role="document">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="m_contacts_ModalLabel">Контакты</h4>
					  </div>
					  <div class="modal-body">
						<div id="p_contacts_content"></div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
					  </div>
					</div>
				  </div>
				</div>
				<div class="modal fade" id="m_about" tabindex="-1" role="dialog" aria-labelledby="m_about_ModalLabel">
				  <div class="modal-dialog" role="document">
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
						<h4 class="modal-title" id="m_about_ModalLabel">О проекте</h4>
					  </div>
					  <div class="modal-body">
						<div id="p_about_content"></div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Закрыть</button>
					  </div>
					</div>
				  </div>
				</div>
				<script src="//my.enu.kz/js/enu_my_script.js"></script>
			</p>
    </div>
	
</body>
</html>
