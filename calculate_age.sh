#!/bin/bash

# Проверяем, передан ли аргумент
if [ -z "$1" ]; then
  echo "Использование: $0 <год рождения>"
  exit 1
fi

# Проверяем, что введен корректный год
if ! [[ $1 =~ ^[0-9]{4}$ ]]; then
  echo "Ошибка: введите год рождения в формате YYYY (например, 1980)"
  exit 1
fi

# Получаем текущий год
current_year=$(date +%Y)

# Вычисляем возраст
birth_year=$1
if [ $birth_year -gt $current_year ]; then
  echo "Ошибка: год рождения не может быть больше текущего года ($current_year)"
  exit 1
fi

age=$((current_year - birth_year))

echo "Ваш возраст: $age лет"
