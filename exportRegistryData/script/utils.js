var utils = {
  /* Данная функция создаёт кроссбраузерный объект XMLHTTP */
  getXmlHttp: function() {
    var xmlhttp;
    try {
      xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
    } catch (e) {
      try {
        xmlhttp = new ActiveXObject("Microsoft.XMLHTTP");
      } catch (E) {
        xmlhttp = false;
      }
    }
    if (!xmlhttp && typeof XMLHttpRequest!='undefined') {
      xmlhttp = new XMLHttpRequest();
    }
    return xmlhttp;
  },

  makeBaseAuth: function(user, pswd){
    var token = user + ':' + pswd;
    var hash = "";
    if (btoa) {
      hash = btoa(token);
    }
    return "Basic " + hash;
  },

  runAPIRequestGET: function(address, api, login, pass){
    var result = null;
    var url = address + api;
    var xmlhttp = utils.getXmlHttp();
    if (xmlhttp) {
      xmlhttp.open("GET", url, false);
      xmlhttp.setRequestHeader("Content-type", "application/json; charset=utf-8");
      xmlhttp.setRequestHeader('Authorization', utils.makeBaseAuth(login, pass));
      xmlhttp.send();
      result = JSON.parse(xmlhttp.response);
    }
    return result;
  },

  searchInRegistry: function(address, login, pass, formCode){
    var result = null;
    var searchData =
    JSON.stringify({
      "query": "where code = \'" + formCode + "\'",
      "showDeleted": "false",
      "searchInRegistry": "true",
      "registryRecordStatus": ["STATE_SUCCESSFUL"]
    });
    var url = address + 'rest/api/asforms/search/advanced';
    var xmlhttp = utils.getXmlHttp();
    if (xmlhttp) {
      xmlhttp.open("POST", url, false);
      xmlhttp.setRequestHeader("Content-type", "application/json; charset=utf-8");
      xmlhttp.setRequestHeader('Authorization', utils.makeBaseAuth(login, pass));
      xmlhttp.send(searchData);
      result = JSON.parse(xmlhttp.responseText);
    }
    return result;
  },

  /*saveData: function(address, login, pass, formUUID, uuid, data){
    var result = null;
    var sendData = "formUUID=" + formUUID + "&uuid=" + uuid + "&data=" + encodeURIComponent('"data":'+JSON.stringify(data.data));
    var url = address + 'rest/api/asforms/data/save';
    var xmlhttp = utils.getXmlHttp();
    if (xmlhttp) {
      xmlhttp.open("POST", url, false);
      xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=utf-8");
      xmlhttp.setRequestHeader('Authorization', utils.makeBaseAuth(login, pass));
      xmlhttp.send(sendData);
      result = JSON.parse(xmlhttp.responseText);
    }
    return result;
  },*/
  
  saveData: function(address, login, pass, formUUID, uuid, data){
    var result = null;
    var sendData = "form=" + formUUID + "&uuid=" + uuid + "&data=" + encodeURIComponent('"data":'+JSON.stringify(data.data));
    var url = address + 'rest/api/asforms/form/multipartdata';
    var xmlhttp = utils.getXmlHttp();
    if (xmlhttp) {
      xmlhttp.open("POST", url, false);
      xmlhttp.setRequestHeader("Content-type", "application/x-www-form-urlencoded; charset=utf-8");
      xmlhttp.setRequestHeader('Authorization', utils.makeBaseAuth(login, pass));
      xmlhttp.send(sendData);
      result = JSON.parse(xmlhttp.responseText);
    }
    return result;
  },

  msgShow: function(msg, type) {
    var c1 = '#FF9999';
    var c2 = 'rgba(255, 153, 153, 0.9)';
    if (type == 'info') {
      c1 = '#B5D5FF';
      c2 = 'rgba(181, 213, 255, 0.9)';
    }
    var msgBox = $('<div/>');
    msgBox.css({
      'position': 'fixed',
      'left': '50%',
      'margin-right': '-50%',
      'transform': 'translate(-50%)',
      'top': '20px',
      'color': 'black',
      'font-size': '14px',
      'font-weight': 'bold',
      'text-align': 'center',
      'padding': '20px',
      'outline': 'none',
      'border-radius': '3px',
      'background': c1,
      'background-color': c2,
      'display': 'none',
      'z-index': '999'
    });
    $('body').append(msgBox);
    msgBox.html(msg).fadeToggle('normal');
    setTimeout(function() {
      msgBox.fadeToggle('normal');
      setTimeout(function() {
        msgBox.remove();
      }, 1000);
    }, 5000);
  },

  //Скрывает ждущее окошко АМ
  hideWaitMngmnt: function() {
    try {
      document.getElementById("mngmnt_wait_div").style.width = "0";
      document.getElementById("mngmnt_wait_div").style.height = "0";
      document.getElementById("mngmnt_wait_div").style.visibility = "hidden";
    } catch (e) {
      console.log(e);
    }
  },

  //Показывает ждущее окошко АМ
  showWaitMngmnt:  function() {
    try {
      document.getElementById("mngmnt_wait_div").style.width = "100%";
      document.getElementById("mngmnt_wait_div").style.height = "100%";
      document.getElementById("mngmnt_wait_div").style.visibility = "visible";
      document.getElementById("ww").innerHTML = "<img src='image/wait.gif'>";
    } catch (e) {
      console.log(e);
    }
  }

};

function runExportData() {
  var d = {
    formCode: $('#form_code').val(),
    server: {
      one: {
        address: $('#s_one_address').val() + '/Synergy/',
        login: $('#s_one_login').val(),
        password: $('#s_one_password').val()
      },
      two: {
        address: $('#s_two_address').val() + '/Synergy/',
        login: $('#s_two_login').val(),
        password: $('#s_two_password').val(),
        registryID: $('#registry_id').val()
      }
    }
  };

  utils.showWaitMngmnt();

  setTimeout(function(){
    try {
      jQuery.when(searchAndExport(d)).then(function(){
        utils.hideWaitMngmnt();
      });
    } catch (e) {
      console.log(e);
      utils.msgShow("ОШИБКА В ВВЕДЕННЫХ ДАННЫХ\nПодробности смотрите в консоле браузера");
      utils.hideWaitMngmnt();
    }
  }, 0);

}

function searchAndExport(d) {
  console.log(d);

  var p = [];
  var inputs = $('input');
  for (var i = 0; i < inputs.length; i++) {
    p.push($(inputs[i]).val());
    if ($(inputs[i]).val() === "") {
      $(inputs[i]).parent().css("background-color", "#FF9999");
    } else {
      $(inputs[i]).parent().css("background-color", "");
    }
  }

  if(p.indexOf("") !== -1) {
    utils.msgShow("Заполните все поля");
    return;
  }

  var dataUUIDs = utils.searchInRegistry(d.server.one.address, d.server.one.login, d.server.one.password, d.formCode);

  if (!dataUUIDs || dataUUIDs.length < 1) {
    utils.msgShow("Не найдены записи по форме");
    return;
  }

  dataUUIDs.forEach(function(res){
    var tmpData = utils.runAPIRequestGET(d.server.one.address, "rest/api/asforms/data/" + res.dataUUID, d.server.one.login, d.server.one.password);

    if (tmpData) {
      // создаем запись реестра
      var uuidCreated = utils.runAPIRequestGET(d.server.two.address, "rest/api/registry/create_doc?registryID=" + d.server.two.registryID, d.server.two.login, d.server.two.password);
      if (uuidCreated.errorCode == "0") {
        uuidCreated = uuidCreated.dataUUID;
        console.log("запись реестра создана");
        console.log(uuidCreated);
      } else {
        console.log("!! запись реестра не создана !!");
        console.log(uuidCreated);
        setTimeout(function(){
          uuidCreated = null;
        }, 0);
      }

      if (uuidCreated) {
        // получаем ID формы
        var fromIdS2 = utils.runAPIRequestGET(d.server.two.address, "rest/api/asforms/form_ext?version=0&formCode=" + d.formCode, d.server.two.login, d.server.two.password);
        console.log("данные по форме по коду формы");
        console.log(fromIdS2);
        // сохраняем данные по форме
        var saveResult = utils.saveData(d.server.two.address, d.server.two.login, d.server.two.password, fromIdS2.uuid, uuidCreated, tmpData);
        console.log("сохраняем данные по форме");
        console.log(saveResult);
        // активируем запись
        var activateDoc = utils.runAPIRequestGET(d.server.two.address, "rest/api/registry/activate_doc?dataUUID=" + uuidCreated, d.server.two.login, d.server.two.password);
        console.log("активируем запись");
        console.log(activateDoc);

      }

    }

  });

  setTimeout(function(){
    utils.msgShow("Экспорт данных завершен", "info");
    console.log("Экспорт данных завершен");
  }, 0);

}
