var apartmentInfo = model.playerModel.getModelWithId('apartment_info');

function getFlatInfo(){
  if(!apartmentInfo) return [];

  let result = [];

  apartmentInfo.modelBlocks.forEach(function(modelBlock){
    let tableBlockIndex = modelBlock[0].asfProperty.tableBlockIndex;

    if (model.playerModel.getModelWithId('house', apartmentInfo.asfProperty.id, tableBlockIndex).getValue() &&
        model.playerModel.getModelWithId('flat', apartmentInfo.asfProperty.id, tableBlockIndex).getValue()) {
      let flat_info = model.playerModel.getModelWithId('house', apartmentInfo.asfProperty.id, tableBlockIndex).getValue();
      flat_info += ', секция ' + model.playerModel.getModelWithId('section', apartmentInfo.asfProperty.id, tableBlockIndex).getValue();
      flat_info += ', кв. ' + model.playerModel.getModelWithId('flat', apartmentInfo.asfProperty.id, tableBlockIndex).getValue();
      flat_info += ', ' + model.playerModel.getModelWithId('floor', apartmentInfo.asfProperty.id, tableBlockIndex).getValue() + ' этаж';
      flat_info += ', ' + model.playerModel.getModelWithId('flat_sq', apartmentInfo.asfProperty.id, tableBlockIndex).getValue()+ ' кв.м.';

      let flat_cost = model.playerModel.getModelWithId('flat_cost', apartmentInfo.asfProperty.id, tableBlockIndex).getValue();

      result.push({addr: flat_info, cost: flat_cost});
    }
  });

  return result;
}

function getParkingInfo(){
  if(!apartmentInfo) return [];

  let result = [];

  apartmentInfo.modelBlocks.forEach(function(modelBlock){
    let tableBlockIndex = modelBlock[0].asfProperty.tableBlockIndex;

    if (model.playerModel.getModelWithId('parking', apartmentInfo.asfProperty.id, tableBlockIndex).getValue()) {
      let parking_info = model.playerModel.getModelWithId('house', apartmentInfo.asfProperty.id, tableBlockIndex).getValue();
      parking_info += ', машиноместо ' + model.playerModel.getModelWithId('parking', apartmentInfo.asfProperty.id, tableBlockIndex).getValue();

      let parking_cost = model.playerModel.getModelWithId('parking_cost', apartmentInfo.asfProperty.id, tableBlockIndex).getValue();

      result.push({addr: parking_info, cost: parking_cost});
    }
  });

  return result;
}

function removeRowTable() {
  if (model.modelBlocks.length > 0) {
    model.modelBlocks.forEach(function(modelBlock, index){
      model.removeRow(index);
    });
    if (model.modelBlocks.length > 0) removeRowTable();
  }
}

function init() {
  removeRowTable();
  let data = getFlatInfo().concat(getParkingInfo());

  data.forEach(function(item, index){
    let row = model.createRow();
    row[0].setValue((index + 1) + '');
    row[1].setValue(item.addr);
    row[4].setValue(item.cost);
    row[5].setValue(item.cost);
  });
}

model.playerModel.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
  init();
});
