<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
<style>
  #slider {
    float: left;
    clear: left;
    width: 300px;
    margin: 5px 15px;
    border-color: #729fcf;
    border: 1px solid #729fcf;
    border-radius: 5px;
  }

  .ui-slider-range {
    background: #729fcf;
  }
  .ui-slider-handle {
    border-color: #729fcf;
  }

  #custom-handle {
    width: 3em;
    height: 1.6em;
    top: 50%;
    margin-top: -.8em;
    text-align: center;
    line-height: 1.6em;
    border: 1px solid #729fcf;
    border-radius: 5px;
    background-color: #f9f9f9;
  }

</style>
<div id="slider">
  <div id="custom-handle" class="ui-slider-handle"></div>
</div>
<div innerId = 'label'></div>
<input innerId = 'input' type="text" class="asf-textBox" style="text-align: left; font-family: Arial, sans-serif; font-size: 12px;">

'use strict';

model.getAsfData = function(blockNumber){
  if(model.getValue()) {
    return AS.FORMS.ASFDataUtils.getBaseAsfData(model.asfProperty, blockNumber, model.getValue());
  } else {
    return AS.FORMS.ASFDataUtils.getBaseAsfData(model.asfProperty, blockNumber);
  }
};

model.setAsfData = function(asfData){
  if(!asfData || !asfData.value) {
    return;
  }
  model.setValue(asfData.value+'');
};

model.getSpecialErrors = function() {
  return;
};

var label = jQuery(view.container).children("[innerId='label']");
var input = jQuery(view.container).children("[innerId='input']");
var percentSlide = $('#slider');
var handle = $("#custom-handle");

if (editable) {
  label.hide();
  percentSlide.show();
  handle.show();
  input.hide();
} else {
  label.show();
  percentSlide.hide();
  handle.hide();
  input.hide();
}

// метод обновления отображения согласно изменившимся данным
view.updateValueFromModel = function () {
  let val = model.getValue() || input.val();
  if (model.getValue()) {
    label.html(val);
    input.val(val);
  } else {
    label.html(model.min+"");
    input.val(model.min+"");
  }
};

// подписываемся на событие модели об изменении, чтобы записать в label и input актуальные данные
model.on(AS.FORMS.EVENT_TYPE.valueChange, function () {
  view.updateValueFromModel();
});

// подписываем на событие подгрузки для актуализации label и input
model.on(AS.FORMS.EVENT_TYPE.dataLoad, function () {
  view.updateValueFromModel();
});

/**
 * метод помечает поле как неправильно заполненное
 */
view.markInvalid = function(){
   label.css("background-color", "#aa3344");
   input.css("background-color", "#aa3344");
};
/**
 * метод убирает пометку неправильно заполненного поля
 */
view.unmarkInvalid = function(){
    input.css("background-color", "");
    label.css("background-color", "");
};

function recalc(val){
  if(model.sum && model.result) {
    let sumValue = parseFloat(model.playerModel.getModelWithId(model.sum).getValue());
    if(isNaN(sumValue)) sumValue = 0;
    let percent = parseInt(val);
    let tmpValue = sumValue / 100 * percent;
    model.playerModel.getModelWithId(model.result).setValue(tmpValue.toFixed(2) + '');
  }
}

// view.updateValueFromModel();
setTimeout(function(){
  view.updateValueFromModel();
  $("#slider").slider({
    orientation: "horizontal",
    animate: true,
    range: "min",
    value: model.getValue() || model.min,
    min: model.min,
    max: model.max,
    step: model.step,

    create: function() {
      let value = model.getValue() || input.val();
      handle.text(value + "%");
      recalc(value);
    },
    slide: function(event, ui) {
      handle.text(ui.value + "%");
    },
    change: function(event, ui) {
      recalc(ui.value);
      model.setValue(ui.value);
    }
  });
}, 10);



// recalc($("#slider").slider("value"));
