const url_api = "https://allatrack-tfa.tk";

const LOCALE = AS.OPTIONS.locale;

const TRANSLATIONS = {
  ru: {
    send_code: "Отправить код",
    invalid_code: "Введён неверный код, попробуйте ещё раз через 10 секунд",
    number_confirmation: "Подтверждение номера",
    failed_send: "Не удалось отправить код, повторите попытку позже",
    phone_confirmed: "Номер подтвержден",
    phone_not_confirmed: "Номер не подтвержден"
  },
  kk: {
    send_code: "Кодты жіберіңіз",
    invalid_code: "Жарамсыз код енгізілді, 10 секундтан кейін қайталап енгізіңіз",
    number_confirmation: "Нөмірді растау",
    failed_send: "Код жіберілмеді, кейінірек қайталап көріңіз.",
    phone_confirmed: "Нөмірі расталған",
    phone_not_confirmed: "Нөмірі расталмады"
  },
  en: {
    send_code: "Send code",
    invalid_code: "Invalid code entered, try again in 10 seconds",
    number_confirmation: "Number confirmation",
    failed_send: "Failed to send code, please try again later.",
    phone_confirmed: "Number confirmed",
    phone_not_confirmed: "Number not confirmed"
  }
};

function trans(value) {
  return TRANSLATIONS[LOCALE][value]
}

function makeRandomHexString() {
  let text = ''
  let possible = '1234567890abcdef'
  for (let i = 0; i < 17; i++) {
    text += possible.charAt(Math.floor(Math.random() * possible.length))
  }
  return text
}

function decimalToHexString(number) {
  if (number < 0) {
    number = 0xFFFFFFFF + number + 1
  }
  return number.toString(16).toLowerCase()
}

function getEstimatedValue(web) {
  let arr = [97, 77, 105, 83, 77, 85, 118, 89, 109, 80, 71, 122, 57, 114];
  if (web) arr = [115, 103, 100, 102, 104, 100, 109, 103, 100, 107, 102, 103, 106, 107]; //Web API
  let res = ''
  arr.forEach(function(el) {
    res = res + String.fromCharCode(el)
  })
  return res
}

async function getApiKey(url, body, phoneNUmber, web) {
  let secret = await getEstimatedValue(web)
  let keys = Object.keys(body)
  let strBody = ''
  keys.forEach(function(key) {
    if (strBody !== '') {
      strBody = `${strBody};${key}:${body[key]}`
    } else {
      strBody = `${key}:${body[key]}`
    }
  })
  strBody = strBody + ';'
  return SparkMD5.hash(url + '::body::' + decimalToHexString(CRC32.str(strBody)) + '::key::' + secret + '::phone_number::' + phoneNUmber).toString(CryptoJS.enc.Hex) + makeRandomHexString()
}


async function user_reg(phone, cb) {
  let path = '/v1/api/users';
  let url = `${url_api}${path}`;
  let data = {
    phone_number: phone,
    lang: "ru"
  };
  const apiKey = await getApiKey(path, data, phone);

  let settings = {
    crossDomain: true,
    url: url,
    method: "POST",
    headers: {
      "Content-Type": "application/json; charset=utf-8",
      "Accept": "application/json",
      "api-key": apiKey
    },
    data: JSON.stringify(data)
  }

  $.ajax(settings)
    .done(function(response) {
      cb(response);
    })
    .fail(function(jqXHR, textStatus) {
      cb(false, jqXHR.responseText);
    });
}

//проверка существует ли юзер
async function web_verify_user(phone, cb) {
  let path = '/v1/api/web/verify/user';
  let url = `${url_api}${path}`;
  let data = {
    phone_number: phone,
    service: 'kaztel',
    client_timestamp: new Date().getTime() / 1000,
    event: 'login',
    lang: 'ru'
  };
  const apiKey = await getApiKey(path, data, phone, true);

  let settings = {
    crossDomain: true,
    url: url,
    method: "POST",
    headers: {
      "Content-Type": "application/json; charset=utf-8",
      "Accept": "application/json",
      "api-key": apiKey
    },
    data: JSON.stringify(data)
  }

  $.ajax(settings)
    .done(function(response) {
      cb(response);
    })
    .fail(function(jqXHR, textStatus) {
      cb(false, jqXHR.responseText);
    });

}

//отправка кода
async function web_code(phone, cb) {
  let path = '/v1/api/web/code';
  let url = `${url_api}${path}`;
  let data = {
    phone_number: phone,
    service: 'kaztel',
    method: 'sms',
    client_timestamp: (new Date()).getTime() / 1000,
    event: 'login',
    resend: false,
    lang: 'ru'
  };
  const apiKey = await getApiKey(path, data, phone, true);

  let settings = {
    crossDomain: true,
    url: url,
    method: "POST",
    headers: {
      "Content-Type": "application/json; charset=utf-8",
      "Accept": "application/json",
      "api-key": apiKey
    },
    data: JSON.stringify(data)
  }

  $.ajax(settings)
    .done(function(response) {
      cb(response);
    })
    .fail(function(jqXHR, textStatus) {
      cb(false, jqXHR.responseText);
    });
}

//проверка кода
async function web_verify_code(phone, code, cb) {
  let path = '/v1/api/web/verify/code';
  let url = `${url_api}${path}`;
  let data = {
    phone_number: phone,
    code: Number(code),
    service: 'kaztel',
    method: 'sms',
    client_timestamp: (new Date()).getTime() / 1000,
    event: 'login',
    lang: 'ru'
  };
  const apiKey = await getApiKey(path, data, phone, true);

  let settings = {
    crossDomain: true,
    url: url,
    method: "POST",
    headers: {
      "Content-Type": "application/json; charset=utf-8",
      "Accept": "application/json",
      "api-key": apiKey
    },
    data: JSON.stringify(data)
  }

  $.ajax(settings)
    .done(function(response) {
      cb(response);
    })
    .fail(function(jqXHR, textStatus) {
      cb(false, jqXHR.responseText);
    });
}

function setStatusInForm(){
  model.playerModel.getModelWithId('phone_number_verification').setValue("2");
}

function showDialogCode(phone) {
  let codeDialog = $('<div style="text-align: center;">');
  let inputCode = $('<input type="text">')
    .css({
      "border": "1px solid #49b785",
      "border-radius": "6px",
      "margin": "0.5em 0",
      "padding": "5px 15px",
      "background": "#FFF"
    });
  let sendButton = $('<button>')
    .css({
      "padding": "0px 1em",
      "margin": "0.5em 1em",
      "font-weight": "bold",
      "background-color": "#49b785",
      "border-color": "#49b785",
      "color": "#ffffff",
      "height": "30px",
      "line-height": "30px",
      "border": "1px solid",
      "border-radius": "6px",
      "overflow": "hidden",
      "text-align": "center",
      "white-space": "nowrap",
      "cursor": "pointer",
      "min-width": "200px"
    })
    .hover(function() {
      $(this).css("background-color", "#55cc95");
    }, function() {
      $(this).css("background-color", "#49b785");
    })
    .html(trans("send_code"))
    .click(function() {
      web_verify_code(phone, inputCode.val(), function(res, err) {
        if (res) {
          alert(trans("phone_confirmed"));
          setStatusInForm();
          $(codeDialog).dialog("close");
        } else {
          fail(err, trans("invalid_code"));
          $(sendButton).prop('disabled', true).html("10");

          let tSec = 9;
          let timer = setInterval(function() {
            $(sendButton).html(tSec--);
          }, 1000);

          setTimeout(function() {
            clearInterval(timer);
            $(sendButton).prop('disabled', false).html(trans("send_code"));
          }, 9000)

        }
      })

    });

  codeDialog.append(inputCode).append(sendButton);

  codeDialog.dialog({
    modal: true,
    resizable: false,
    show: {
      effect: 'fade',
      duration: 500
    },
    hide: {
      effect: 'fade',
      duration: 500
    },
    title: trans("number_confirmation"),
    close: function() {
      $(this).remove();
    }
  });
}

function fail(errMsg, userMsg) {
  if(userMsg){
    alert(userMsg);
  }
  console.error(errMsg);
}

let phoneView = view.playerView.getViewWithId('phone');
let phoneModel = model.playerModel.getModelWithId('phone');
let statusModel = model.playerModel.getModelWithId('refuse_desicion_status');
let phoneVerifyModel = model.playerModel.getModelWithId('phone_number_verification');


function init(){
  const phone = phoneModel.getValue();

  web_verify_user(phone, function(verify, errorVerify) {
    if (verify) {
      web_code(phone, function(code, codeErr) {
        if (code) {
          showDialogCode(phone);
        } else {
          fail(codeErr, trans("failed_send"));
        }
      });
    } else {
      fail(errorVerify)
      fail("Регистрируем нового пользователя");
      user_reg(phone, function(user, regErr) {
        if (user) {
          web_verify_user(phone, function(verify2, errorVerify2) {
            if (verify2) {
              web_code(phone, function(code, codeErr) {
                if (code) {
                  showDialogCode(phone);
                } else {
                  fail(codeErr, trans("failed_send"));
                }
              });
            } else {
              fail(errorVerify2)
            }
          });
        } else {
          fail(regErr);
        }
      });
    }
  });
}

function msgPhoneVerify(verify){
  if(statusModel.getAsfData().key == 0) return;
  
  let msgContainer = $('<div>').css({
    "font-weight": "bold",
    "border-radius": "5px",
    "margin": "5px",
    "padding": "5px 10px",
    "display": "inline-block"
  });
  //если подвержден
  if(phoneVerifyModel.getAsfData().key == 2) {
    $(msgContainer).css({
      "border": "1px solid green",
      "color": "green"
    }).html(trans("phone_confirmed"));
  } else {
    $(msgContainer).css({
      "border": "1px solid orange",
      "color": "orange"
    }).html(trans("phone_not_confirmed"));
  }
  phoneView.container.append(msgContainer);
}

if (editable) {
  if(statusModel.getAsfData().key == 4 && phoneVerifyModel.getAsfData().key == 1) {
    let button = jQuery('<div>')
    .css({
      "padding": "0px 1em",
      "font-weight": "bold",
      "background-color": "#4f5988",
      "border-color": "#4f5988",
      "color": "#ffffff",
      "min-width": "5em",
      "height": "30px",
      "line-height": "30px",
      "border": "1px solid",
      "border-radius": "6px",
      "overflow": "hidden",
      "text-align": "center",
      "white-space": "nowrap",
      "display": "inline-block",
      "cursor": "pointer"
    })
    .hover(function() {
      $(this).css("background-color", "#8890b3");
    }, function() {
      $(this).css("background-color", "#4f5988");
    })
    .html(trans("number_confirmation"))
    .click(init);

    phoneView.container.append(button);

  } else {
    msgPhoneVerify();
  }
} else {
  msgPhoneVerify();
}



/*
1. Кнопка для подтверждения номера доступна только для заявки в статусе На доработке
2. Для заявки в статусе В работе, Отказано, Одобрено показывать такой же блок (как кнопка, но не интерактивный), зелёный с текстом "Номер подтверждён", оранжевый с текстом "Номер не подтверждён"
3. На форме должен быть скрытый компонент (любой), содержащий признак подтверждения номера (да/нет)
4. Если введён неверный код подтверждения, вывести сообщение "Введён неверный код, попробуйте ещё раз через 10 секунд", закрыть окно ввода номера и дать возможность снова нажать на кнопку Подтверждение номера через 10 секунд (отобразить обратный отсчёт, после которого снова станет доступна кнопка "Подтверждение номера")
*/
