var TABLE_TOTAL = {

  getTableTotalRow: function(config) {
    var colspan = config.summaryColSpan;

    var result = '<tr id="table_total_row">'+
    '<td class="asf-cell asf-borderedCell" style="background: #F8F8FF;" colspan='+colspan+'><div class="asf-label" style="text-align: right; font-family: Verdana, sans-serif; font-size: 14px; font-weight: bold;">'+config.totalLabel+'</div></td>';

    config.summaryIds.forEach(function(id) {
      result+='<td class="asf-cell asf-borderedCell" style="background: #F8F8FF;"><div class="asf-label" id="table_total_'+id+'" style="text-align: center; font-family: Verdana, sans-serif; font-size: 14px; font-weight: bold;">0</div></td>';
    });

    for (var i = 0; i < config.endTotalRow; i++){
      result+='<td class="asf-cell asf-borderedCell" style="background: #F8F8FF;"></td>';
    }
    return result+'</tr>';
  },

  addTableTotalRow: function(config) {
    $('#table_total_row').remove();
    $('div[data-asformid="table.container.'+config.tableId+'"]>table tr:last').after(TABLE_TOTAL.getTableTotalRow(config));
  },

  countingTotal: function(tableModel, config) {
    var days = {};
    config.summaryIds.forEach(function(id) {
      days[id] = 0;
    });
    for(var cmp in days) {
      tableModel.modelBlocks.forEach(function(modelBlock){
        var tableBlockIndex = modelBlock[0].asfProperty.tableBlockIndex;
        var n = parseFloat(tableModel.getModelWithId(cmp, config.tableId, tableBlockIndex).getValue());
        if(_.isNaN(n)) n = 0;
        days[cmp] = days[cmp] + n;
      });
    }
    for(var n in days) {
      $('#table_total_'+n).text(days[n]);
    }
  },

  initTotalRow: function(playerModel, playerView, config) {
    var tableModel = playerModel.getModelWithId(config.tableId);
    var tableView = playerView.getViewWithId(config.tableId);

    if(!tableModel) return;

    try {
      var thiz = this;
      thiz.addTableTotalRow(config);
      thiz.countingTotal(tableModel, config);

      if(playerView.editable) {
        tableView.getViewBlocks().forEach(function(viewBlock, index){
          var tableBlockIndex = viewBlock.views[0].model.asfProperty.tableBlockIndex;
          config.summaryIds.forEach(function(cmp) {
            playerModel.getModelWithId(cmp, config.tableId, tableBlockIndex).on("valueChange", function() {
              thiz.countingTotal(tableModel, config);
            });
          });
        });
        tableModel.on('tableRowDelete',function(event, cmodel, cview){
          thiz.countingTotal(tableModel, config);
        });
        tableModel.on('tableRowAdd',function(event, modelTab, modelRow){
          var tableBlockIndex = modelRow[0].asfProperty.tableBlockIndex;
          config.summaryIds.forEach(function(cmp) {
            playerModel.getModelWithId(cmp, config.tableId, tableBlockIndex).on("valueChange", function() {
              thiz.countingTotal(tableModel, config);
            });
          });
          thiz.addTableTotalRow(config);
          thiz.countingTotal(tableModel, config);
        });
      }
    } catch (e) {
      console.log(e);
    }
  }
};


$(function() {
  if(!model.summaryConfig) return;
  model.summaryConfig.forEach(function(config){
    TABLE_TOTAL.initTotalRow(model.playerModel, view.playerView, config);
  });
});

/* Пример настроек компонента
model.summaryConfig = [];

model.summaryConfig.push(
    {
        tableId : "investment_table",
        summaryColSpan : 1,
        summaryIds : [
            "bd", "bd_part", "inv1", "inv1_part", "inv2", "inv2_part", "inv3", "inv3_part"
            ],
        endTotalRow: 0,
        totalLabel: "Итого:"
    }
);
*/
