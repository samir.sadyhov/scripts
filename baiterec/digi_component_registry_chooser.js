/**
 * обновить текстовое представление записи реестра
 */

'use strict';

model.updateTextView = function () {
    if (!model.getValue()) {
        model.textValue = "";
        model.asfDataId = null;
        model.trigger(AS.FORMS.EVENT_TYPE.dataLoad, [model]);
        return;
    }
    AS.FORMS.ApiUtils.getAsfDataUUID(model.getValue(), function (newAsfDataId) {
        model.asfDataId = newAsfDataId;
        if(!registry || !registry.registryID) return;
        AS.FORMS.ApiUtils.getDocMeaningContent(registry.registryID, newAsfDataId, function (text) {
            if (text === null || text === '') {
                model.textValue = i18n.tr('Документ');
            } else {
                model.textValue = text;
            }
            model.trigger(AS.FORMS.EVENT_TYPE.dataLoad, [model]);
        });
    });
};

AS.userFilterCmp = model.userFilterCmp;

/**
 * получить тесктовое представление записи реестра
 * @returns {string|string|*}
 */
model.getTextValue = function () {
    return model.textValue;
};

// подписываемся на событие модели об изменении содержания, чтобы подгрузить дополнительные данные
model.on(AS.FORMS.EVENT_TYPE.valueChange, function () {
    model.updateTextView();
});

/**
 * метод реализовывает вставку asfData
 * @param asfData
 */
model.setAsfData = function (asfData) {
    model.setValue(asfData.key);
};

/**
 * метод реализовывает получение данных компонента для сохранения
 * @param blockNumber
 * @returns {*}
 */
model.getAsfData = function (blockNumber) {
    return AS.FORMS.ASFDataUtils.getBaseAsfData(model.asfProperty, blockNumber, model.textValue, model.value);
};


/* инициализация отображения */


/**
 * реестр
 * @type {object}
 */
var registry = null;
/**
 * видимые колонки реестра
 * @type {Array}
 */
var registryColumns = [];


/**
 * поле ввода для поиска записей реестра
 * @type {XMLList|*}
 */
var input = jQuery(view.container).children("[innerId='name']");
/**
 * поле для отображения выбранной записи реестра
 * @type {XMLList|*}
 */
var textView = jQuery(view.container).children("[innerId='textView']");
/**
 * кнопка добавления записи
 * @type {XMLList|*}
 */
var addIcon = jQuery(view.container).children("[innerId='add']");
/**
 * кнопка выбора записи из реестра
 * @type {XMLList|*}
 */
var browseIcon = jQuery(view.container).children("[innerId='browse']");

/**
 * кнопка выбора записи из реестра
 * @type {XMLList|*}
 */
var editIcon = jQuery(view.container).children("[innerId='edit']");
/**
 * кнопка удаления текущей выбранной записи
 * @type {XMLList|*}
 */
var deleteIcon = jQuery(view.container).children("[innerId='delete']");

// кнопку удаления текущей выбраннйо записи скрываем
deleteIcon.hide();
editIcon.hide();

AS.SERVICES.showRegisterLinkDialog = function(registry, handler) {
    var registerDialog = new AS.FORMS.RegistryLinkChooser(false, registry);
    registerDialog.showDialog();

    //registerDialog.listenTo(registerDialog, 'registrylink:submit:clicked', function (registerModel, columnCollection) {
    //    registerDialog.destroy();
    //    handler(registerModel.uuid);
    //});
    registerDialog.on(AS.FORMS.BasicChooserEvent.applyClicked, function(){
        handler(registerDialog.getSelectedItems()[0].uuid);
    });
}

// по нажатию на кнопку "выбрать из реестра" открываем стандартный диалог выбра записи реестра
browseIcon.click(function () {
    AS.userFilterCmp = model.userFilterCmp;
    AS.SERVICES.showRegisterLinkDialog(registry, function (documentId) {
        model.setValue(documentId);
        collateSelfLinksData(model.selfLinks);
        collateDataFromRecord(documentId, model.collateMap);
    });
});



// по нажатию на кнопку "создать" открываем форму создания записи реестра
addIcon.click(function () {
    if (!registry.rr_create) {
        alert("У вас нет прав на создание записей данного реестра");
        return;
    }

    if (!view.width || view.width === undefined || view.width === null) {
        view.width = 800;
    }
    if (!view.height || view.height === undefined || view.height === null) {
        view.height = 400;
    }
    var createPlayerDiv = jQuery("<div>");
    createPlayerDiv.css("width", view.width + "px");
    createPlayerDiv.css("height", view.height + "px");


    createPlayerDiv.css("border", "1px solid #afafaf");

    var saveButton = jQuery("<button>", {class: "ns-approveButton ns-basicChooserApplyButton"});
    saveButton.button();
    saveButton.html(i18n.tr("Создать"));
    saveButton.css("margin", "auto");
    saveButton.css("display", "block");
    saveButton.css("margin-top", "10px");
    saveButton.css("margin-bottom", "10px");

    var player = AS.FORMS.createPlayer();

    player.view.setEditable(true);
    AS.SERVICES.showWaitWindow();

    AS.FORMS.ApiUtils.simpleAsyncGet("rest/api/registry/create_doc?registryID=" + registry.registryID, function (result) {
        if (result.errorCode != 0) {
            AS.SERVICES.hideWaitWindow();
            alert(i18n.tr("Во время создания записи произошли ошибки. Обратитесь к администратору"));
            return;
        }
        player.model.asfDataId = result.dataUUID;
        player.showFormData(null, null, result.dataUUID);
        AS.SERVICES.hideWaitWindow();

        player.view.appendTo(createPlayerDiv);

        createPlayerDiv.append(saveButton);


        createPlayerDiv.dialog({
            width: view.width,
            height: view.height,
            modal: true
        });

        collateLinksData(player, model.links);
        collateCurrentData(player, model.cmpMap, model.asfProperty.tableBlockIndex);

        if(model.tableMap) replaceTableValue(player, model.tableMap);
    });

    saveButton.click(function () {
        var valid = player.model.isValid();
        if (!valid) {
            alert(i18n.tr("Введите все обязательные поля"));
            return;
        }

        AS.SERVICES.showWaitWindow();
        player.saveFormData(function (result) {
            AS.SERVICES.hideWaitWindow();
            if (_.isUndefined(result)) {
                alert(i18n.tr("Во время сохранения данных по форме произошли ошибки. Обратитесь к администратору"));
                return;
            };

            AS.FORMS.ApiUtils.getDocumentIdentifier(result, function (documentID) {
                model.setValue(documentID);
                collateDataFromRecord(documentID, model.collateMap);
            });

            if (model.activate) {
                AS.FORMS.ApiUtils.simpleAsyncGet("rest/api/registry/activate_doc?dataUUID=" + result, function (result) {
                    if (result.errorCode != 0) {
                        alert(i18n.tr("Во время активации произошла ошибка. Обратитесь к администратору"));
                    }
                });
            }
            createPlayerDiv.dialog("destroy");
        });

        if (view.createOnly) {
            deleteIcon.hide();
        }
    });


});

editIcon.click(function(){

    if (!registry.rr_edit) {
        alert("У вас нет прав на редактирование записей данного реестра");
        return;
    }

    if (!view.width || view.width === undefined || view.width === null) {
        view.width = 800;
    }
    if (!view.height || view.height === undefined || view.height === null) {
        view.height = 400;
    }
    var editPlayerDiv = jQuery("<div>");
    editPlayerDiv.css("width", view.width + "px");
    editPlayerDiv.css("height", view.height + "px");


    editPlayerDiv.css("border", "1px solid #afafaf");

    var saveButton = jQuery("<button>", {class: "ns-approveButton ns-basicChooserApplyButton"});
    saveButton.button();
    saveButton.html(i18n.tr("Сохранить"));
    saveButton.css("margin", "auto");
    saveButton.css("display", "block");
    saveButton.css("margin-top", "10px");
    saveButton.css("margin-bottom", "10px");

    var player = AS.FORMS.createPlayer();

    player.view.setEditable(true);

    player.view.appendTo(editPlayerDiv);
    editPlayerDiv.append(saveButton);

    editPlayerDiv.dialog({
        width: view.width,
        height: view.height,
        modal: true
    });

    AS.FORMS.ApiUtils.getAsfDataUUID(model.getValue(), function (identifier) {
        player.showFormData(null, null, identifier);
    });

    saveButton.click(function () {
        var valid = player.model.isValid();
        if (!valid) {
            alert(i18n.tr("Введите все обязательные поля"));
            return;
        }


        player.saveFormData(function (result) {
            AS.SERVICES.hideWaitWindow();
            if (_.isUndefined(result)) {
                alert(i18n.tr("Во время сохранения данных по форме произошли ошибки. Обратитесь к администратору"));
                return;
            }
            collateDataFromRecord(model.getValue(), model.collateMap);
            editPlayerDiv.dialog("destroy");
        });
    });


});


// по нажатию на кнопку удалить  - удаляем выбранное значение
deleteIcon.click(function(){
    model.setValue(null);
});

/* Показать документ встроенными средствами Synergy */

/*
 textView.click(function(){
 AS.SERVICES.showDocument(model.value);
 });
 */

// по нажатию на текстовое отображение  - открываем запись реестра на просмотр
textView.click(function () {

    var createPlayerDiv = jQuery("<div>");
    if (!view.width || view.width === undefined || view.width === null) {
        view.width = 800;
    }
    if (!view.height || view.height === undefined || view.height === null) {
        view.height = 400;
    }
    createPlayerDiv.css("width", view.width + "px");
    createPlayerDiv.css("height", view.height + "px");

    var editButton = jQuery('<div class="edit"></div>');

    var saveButton = jQuery("<button>", {class: "ns-approveButton ns-basicChooserApplyButton"});
    saveButton.button();
    saveButton.html(i18n.tr("Сохранить"));
    saveButton.css("margin", "auto");
    saveButton.css("display", "block");
    saveButton.css("margin-top", "10px");
    saveButton.css("margin-bottom", "10px");

    createPlayerDiv.css("border", "1px solid #afafaf");

    var player = AS.FORMS.createPlayer();

    player.view.setEditable(false);
    player.showFormData(null, null, model.asfDataId, 0);
    player.view.appendTo(createPlayerDiv);
    createPlayerDiv.append(saveButton);
    saveButton.hide();

    createPlayerDiv.dialog({
        width: view.width,
        height: view.height,
        modal: true
    });

    saveButton.click(function () {
        var valid = player.model.isValid();
        if (!valid) {
            alert(i18n.tr("Введите все обязательные поля"));
            return;
        }
        AS.SERVICES.showWaitWindow();
        player.saveFormData(function (result) {
            AS.SERVICES.hideWaitWindow();
            if (_.isUndefined(result)) {
                alert(i18n.tr("Во время сохранения данных по форме произошли ошибки. Обратитесь к администратору"));
                return;
            }
            // ;
            createPlayerDiv.dialog("destroy");

            AS.FORMS.ApiUtils.getDocumentIdentifier(result, function (documentID) {
                model.setValue(documentID);
                collateDataFromRecord(documentID, model.collateMap);
            });

        });

    });
});


// скрываем или отображаем поля ввода в зависимости от того режим чтения это или редактирования
if (editable) {
    //textView.hide();
} else {
    input.hide();
    addIcon.hide();
    editIcon.hide();
    browseIcon.hide();
    deleteIcon.hide();
}
addIcon.text('+' + i18n.tr('Создать'));
editIcon.text('✎' + i18n.tr('Редактировать'));
browseIcon.text('...'+i18n.tr('Выбрать из реестра'));
deleteIcon.text('✕'+i18n.tr('Удалить'));


// реализовываем метод обновления отображения согласно изменившимся данным модели
view.updateValueFromModel = function () {


    input.hide();
    textView.hide();
    input.hide();
    addIcon.hide();
    editIcon.hide();
    browseIcon.hide();
    deleteIcon.hide();

    input.val("");

    if (model.getValue()) {

        textView.show();
        textView.html(model.getTextValue());
        if(view.playerView.editable && !view.watchOnly) {
            if (view.editInput) {
                editIcon.css("display", "inline-block");
            }
            if(!view.createOnly) {
                browseIcon.css("display", "inline-block");
                deleteIcon.css("display", "inline-block");
            }
        }
    } else {
        input.show();
        if(view.playerView.editable && !view.watchOnly) {
            if(!view.createOnly) {
                browseIcon.css("display", "inline-block");
            }

            if (registry && registry.rr_create && !view.createHide) {
                addIcon.css("display", "inline-block");
            }
        }
    }

    if (view.disableInput || view.watchOnly) {
        input.prop("readonly", true);
        input.addClass("asf-disabledInput");
    } else {
        input.removeProp("readonly");
        input.removeClass("asf-disabledInput");
    }
};



// подписываем на событие подгрузки дополнительных данных значения
model.on(AS.FORMS.EVENT_TYPE.dataLoad, function () {
    view.updateValueFromModel();
});

var collateDataFromRecord = function(documentId, collateMap, handler) {
  if (collateMap && collateMap.size !== 0) {
    /*сопоставление из записи реестра в текущие данные формы*/
    AS.FORMS.ApiUtils.getAsfDataUUID(documentId, function (identifier) {
      jQuery.when(AS.FORMS.ApiUtils.loadAsfData(identifier))
      .then(function (asfData) {
        for (var key in collateMap) {
          if (collateMap.hasOwnProperty(key)) {
            asfData.data.forEach(function (item) {
              if (item.id === key) {
                var m = model.playerModel.getModelWithId(collateMap[key]);
                if(m) {
                  if (item.type == "appendable_table" || item.type == "check" || item.type == "radio") {
                    m.setAsfData(item);
                  } else {
                    if (item.key && item.key !== null && item.key !== '') {
                      m.setValue(item.key + "");
                    } else if (item.value) {
                      m.setValue(item.value + "");
                    }
                  }
                }
              }
            });
          }
        }
        if(handler) {
          handler();
        }
      });
    });
  }
};


model.collateData = function(handler){
    collateDataFromRecord(model.getValue(), model.collateMap, handler);
    collateSelfLinksData(model.selfLinks);
};

var collateCurrentData = function (player, map, tableBlockIndex) {
    player.model.on(AS.FORMS.EVENT_TYPE.dataLoad, function () {
        var data = model.playerModel.getAsfData();
        // console.log(map);
        if(map){
            map.forEach(function (map) {
                if (map.fromTable) {
                    data.data.forEach(function (item) {

                        if (item.id === map.fromTable) {

                            replaceValue(player, item.data, map.from + '-b' + 1, map.to);
                        }
                    });
                } else {
                    replaceValue(player, data.data, map.from, map.to);
                }
            });
        }
    });
};

var pmodel = model.playerModel;
var collateLinksData = function (player, links) {
    player.model.on(AS.FORMS.EVENT_TYPE.dataLoad, function () {
        var regFgzs = player.model.getModelWithId('reg_fgzs');
        if (regFgzs !== null) {
            // console.log(pmodel.nodeId);
            regFgzs.setValue(location.hash.split('document_identifier=')[1]);
            // console.log(regFgzs);
        }
        // console.log(links);
        if(links){
            links.forEach(function (link) {
                var registries = link.from.split(".");
                var model = pmodel.getModelWithId(registries.shift());
                AS.FORMS.ApiUtils.getAsfDataUUID(model.getValue(), function (dataUUID) {
                    jQuery.when(AS.FORMS.ApiUtils.loadAsfData(dataUUID)).then(function (data) {
                        if(link.valueCmp && link.toCmp) {
                            replaceValue(player, data.data, link.valueCmp, link.toCmp);
                        } else {
                            replaceValuesByCollation(registries, player, data.data, link);
                        }
                    });
                });
            });}

    });
};


function getData(data, id){
    var result = null;
    data.data.forEach(function(d){
        if(d.id == id){
            result = d;
        }
    });
    return result;
}


var collateSelfLinksData = function (selfLinks) {
    // console.log(selfLinks);
    if(selfLinks){
        AS.FORMS.ApiUtils.getAsfDataUUID(model.getValue(), function (dataUUID) {
            jQuery.when(AS.FORMS.ApiUtils.loadAsfData(dataUUID)).then(function (data) {
                selfLinks.forEach(function (selfLink) {

                    var fromDocumentData = null;
                    data.data.forEach(function(d){
                        if(d.id == selfLink.from) {
                            fromDocumentData = d;
                        }
                    });

                    if(!fromDocumentData) return;

                    AS.FORMS.ApiUtils.getAsfDataUUID(fromDocumentData.key, function (dataUUID) {
                        jQuery.when(AS.FORMS.ApiUtils.loadAsfData(dataUUID)).then(function (formData) {

                            for (var key in selfLink.collation) {
                                if (!selfLink.collation.hasOwnProperty(key)) {
                                    return;
                                }
                                var fromData = getData(formData, key);
                                if(fromData) {
                                    var targetModel = model.playerModel.getModelWithId(selfLink.collation[key]);
                                    if(targetModel) {
                                        targetModel.setAsfData(fromData);
                                    }
                                }

                            }

                        });
                    });
                });
            });
        });
    }
};

var replaceValuesByCollation = function(registries, player, data, link) {
    if (registries.length > 0) {
        AS.FORMS.ApiUtils.getAsfDataUUID(getValueById(data, registries.shift()), function (dataUUID) {
            jQuery.when(AS.FORMS.ApiUtils.loadAsfData(dataUUID)).then(function (data) {
                replaceValuesByCollation(registries, player, data.data, link);
            });
        });
    } else {
        for (var property in link.collation) {
            if (link.collation.hasOwnProperty(property)) {
                replaceValue(player, data, property, link.collation[property]);
            }
        }
    }
};

var getValueById = function(data, cmpId) {
    var result = null;
    data.forEach(function (item) {
        if (item.id === cmpId) {
            result = item.key;
            return;
        }
    });
    return result;
};

//{from: {tableid: 'table-f1', cmpid: 'entity-users1'}, to: {tableid: 'table-f2', cmpid: 'entity-users2'}});
var replaceTableValue = function (player, map){
    player.model.on(AS.FORMS.EVENT_TYPE.dataLoad, function () {
        map.forEach(function(param){
            var fromTableModel = model.playerModel.getModelWithId(param.from.tableid);
            var toTableModel = player.model.getModelWithId(param.to.tableid);
            fromTableModel.modelBlocks.forEach(function(row){
                row.forEach(function(cmp){
                    if(cmp.asfProperty.id == param.from.cmpid) {
                        var newRow = toTableModel.createRow();
                        newRow.forEach(function(toCmp){
                            if (toCmp.asfProperty.id == param.to.cmpid){
                                toCmp.setValue(cmp.getValue());
                            }
                        });
                    }
                });
            });
        });
    });
};

var replaceValue = function (player, data, fromCmp, toCmp) {
  data.forEach(function (item) {
    if (item.id === fromCmp) {
      var m = player.model.getModelWithId(toCmp);

      if (m !== null) {
        if (item.type === "entity" && m.asfProperty.type !== "entity") {
          m.setValue(item.value + "");
        } else if (item.type === "appendable_table" && m.asfProperty.type !== "appendable_table") {
          m.setAsfData(item);
        } else {
          if (item.key && item.key !== null && item.key !== '') {
            if (m.asfProperty.type == "entity") {
              if (m.asfProperty.config.entity === 'positions') {
                m.setValue([{elementID: item.key + "", elementName: item.value, tagName: item.value}]);
              } else {
                m.setValue([{personID: item.key + "", personName: item.value}]);
              }
            } else {
              m.setValue(item.key + "");
            }
          } else if (item.value) {
            m.setValue(item.value + "");
          }
        }
      }
    }
  });
};

/**
 * если нет прав создания записи реестра, то кнопки создать не должно быть видно
 */


/**
 * инициализируем компонент (получаем реестр, колонки)
 */
function initComponent(userFilterCmp) {
    AS.FORMS.ApiUtils.simpleAsyncGet('rest/api/registry/info?code=' + model.code, function (reg) {
        registry = reg;

        if(!registry.columns || registry.columns.length == 0) return;

        registry.registryID = reg.registryID;

        registryColumns = [];
        registry.columns.forEach(function (col) {
            if (col.visible != 1) {
                return;
            }
            registryColumns.push(col);
        });

        registryColumns = registryColumns.sort(function (item1, item2) {
            var number1 = item1.order;
            var number2 = item2.order;
            if (number1 === number2) {
                if (item1.name < item2.name) {
                    return -1;
                } else if (item1.name > item2.name) {
                    return 1;
                }
            } else {
                if (number1 === 0) {
                    return 1;
                } else if (number2 === 0) {
                    return -1;
                } else if (number1 < number2) {
                    return -1;
                } else {
                    return 1;
                }
            }
            return 0;
        });
        if (userFilterCmp && !model.getValue()) {
            var p = {
                field: userFilterCmp,
                key: AS.OPTIONS.currentUser.userId,
                condition: 'TEXT_EQUALS',
                registryID: registry.registryID,
                pageNumber: 0,
                countInPart: 1,
                searchString: ""};

            AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/registry/data_ext_post", function (data) {
                var jsonData = JSON.parse(data);
                if (jsonData.result.length >= 1) {
                    model.setValue(jsonData.result[0].documentID);
                    collateDataFromRecord(jsonData.result[0].documentID, model.collateMap);
                    collateSelfLinksData(model.selfLinks);
                }
            }, "text", p);
        }

        model.updateTextView();
        view.updateValueFromModel();

    });

}

// при вводе пользователя отображаем первые 10 результатов поиска
input.on("input", function () {
    if (view.createOnly) {
        return;
    }
    var search = input.val();
    if (search.length === 0 || !registry) {
        AS.SERVICES.showDropDown([]);
        return;
    }

    if (!AS.userFilterCmp) {
        AS.FORMS.ApiUtils.getRegistryData(registry.registryID, 0, 10, search, null, null, function (foundData) {
            var values = [];
            foundData.result.forEach(function (record) {
                var value = {value: record.documentID};
                var label = "";

                registryColumns.forEach(function (column) {
                    if (record.fieldValue[column.columnID] !== undefined) {
                        label += record.fieldValue[column.columnID] + " - ";
                    }
                });

                value.title = label;
                values.push(value);
            });

            AS.SERVICES.showDropDown(values, input, null, function (selectedValue) {
                model.setValue(selectedValue);
                view.updateValueFromModel();
            });

        });
    } else {
        var p = {
            field: AS.userFilterCmp,
            key: AS.OPTIONS.currentUser.userId,
            condition: 'TEXT_EQUALS',
            registryID: registry.registryID,
            pageNumber: pageNumber,
            countInPart: countInPage,
            searchString: search,
            sortCmpID: sortCmpId,
            sortDesc: sortAsc };

        AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/registry/data_ext_post", function (data) {
            AS.SERVICES.hideWaitWindow();
            var jsonData = JSON.parse(data);

            var values = [];
            jsonData.result.forEach(function (record) {
                var value = {value: record.documentID};
                var label = "";

                registryColumns.forEach(function (column) {
                    if (record.fieldValue[column.columnID] !== undefined) {
                        label += record.fieldValue[column.columnID] + " - ";
                    }
                });

                value.title = label;
                values.push(value);
            });

            AS.SERVICES.showDropDown(values, input, null, function (selectedValue) {
                model.setValue(selectedValue);
                view.updateValueFromModel();
            });
        }, "text", p);
    }


});


setTimeout(function () {
  initComponent(model.userFilterCmp);
}, 0);


AS.SERVICES.getParameterByName = function(name, url) {
    if (!url) {
        url = window.location.href;
    }
    name = name.replace(/[\[\]]/g, "\\$&");
    var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
        results = regex.exec(url);
    if (!results) return null;
    if (!results[2]) return '';
    return decodeURIComponent(results[2].replace(/\+/g, " "));
};

AS.COMPONENTS.RegistryTable = function(registry, width) {
    var columns = [];

    var recordsCount = 0;
    var currentPage = 0;
    var search = null;
    var sortCmpId = null;
    var sortAsc = null;

    var countInPage = 15;

    var instance = this;

    if (registry && registry.columns) {
      registry.columns.forEach(function(col){
          if(col.visible != 1) {
              return;
          }
          col.id = col.columnID;
          col.name = col.label;
          columns.push(col);
      });
    }

    var columnWidth = 150;
    if(columns.length <= 5) {
        columnWidth = width/columns.length;
    }

    columns = columns.sort(function(item1, item2){
        var number1 = item1.order;
        var number2 = item2.order;


        if(number1 === number2) {
            if(item1.name < item2.name) {
                return -1;
            } else if(item1.name > item2.name) {
                return 1;
            }
        } else {
            if(number1 === 0 ) {
                return 1;
            } else if(number2 === 0 ) {
                return -1;
            } else if(number1 < number2) {
                return -1;
            } else {
                return 1;
            }
        }
        return 0;
    });

    columns.forEach(function(column){
        column.width = columnWidth;
        column.minWidth = 35;
        column.sortable = true;
    });


    var table = new AS.COMPONENTS.ExtendedTable();
    table.init({}, columns);

    this.dblclick = function(handler){
        table.getWidget().dblclick(handler);
    };

    table.on(AS.COMPONENTS.ExtendedTableEvent.columnSortClicked, function(event, columnIndex, sortAsc, columnOption){
        instance.sort(columnOption.id, sortAsc);
    });


    this.search = function(newSearch) {
        search = newSearch;
        this.loadData(0);
    };

    this.sort = function(cmpId, asc){
        sortCmpId = cmpId;
        sortAsc = asc;
        this.loadData(0);
    };

    this.loadData = function(pageNumber){
        currentPage = pageNumber;
        if (model.customFilter) {
          var p = {
              field: model.customFilter.id,
              key: model.customFilter.value,
              condition: model.customFilter.eq,
              registryID: registry.registryID,
              pageNumber: pageNumber,
              countInPart: countInPage,
              searchString: search,
              sortCmpID: sortCmpId,
              sortDesc: sortAsc };

          AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/registry/data_ext_post", function (data) {
              AS.SERVICES.hideWaitWindow();
              var jsonData = JSON.parse(data);
              recordsCount = jsonData.recordsCount;

              var registryData = [];

              jsonData.result.forEach(function(item){

                  var mergedItem = item.fieldValue;
                  mergedItem.documentId = item.documentID;
                  mergedItem.asfDataUUID = item.dataUUID;
                  mergedItem.uuid = item.documentID;
                  mergedItem.id = item.dataUUID;

                  registryData.push(mergedItem);
              });
              table.setData(registryData);
          }, "text", p);
        } else {
          if (!AS.userFilterCmp) {
              AS.FORMS.ApiUtils.getRegistryData(registry.registryID, pageNumber, countInPage, search, sortCmpId, sortAsc,  function(data){
                  recordsCount = data.recordsCount;

                  var registryData = [];

                  data.result.forEach(function(item){

                      var mergedItem = item.fieldValue;
                      mergedItem.documentId = item.documentID;
                      mergedItem.asfDataUUID = item.dataUUID;
                      mergedItem.uuid = item.documentID;
                      mergedItem.id = item.dataUUID;

                      registryData.push(mergedItem);
                  });
                  table.setData(registryData);
              });
          } else {
              var p = {
                  field: AS.userFilterCmp,
                  key: AS.OPTIONS.currentUser.userId,
                  condition: 'TEXT_EQUALS',
                  registryID: registry.registryID,
                  pageNumber: pageNumber,
                  countInPart: countInPage,
                  searchString: search,
                  sortCmpID: sortCmpId,
                  sortDesc: sortAsc
                };

              AS.FORMS.ApiUtils.simpleAsyncPost("rest/api/registry/data_ext_post", function (data) {
                  AS.SERVICES.hideWaitWindow();
                  var jsonData = JSON.parse(data);
                  recordsCount = jsonData.recordsCount;

                  var registryData = [];

                  jsonData.result.forEach(function(item){

                      var mergedItem = item.fieldValue;
                      mergedItem.documentId = item.documentID;
                      mergedItem.asfDataUUID = item.dataUUID;
                      mergedItem.uuid = item.documentID;
                      mergedItem.id = item.dataUUID;

                      registryData.push(mergedItem);
                  });

                 table.setData(registryData);
              }, "text", p);
          }
        }

    };

    this.getCurrentPage = function(){
        return currentPage;
    };

    this.getRecordsCount = function() {
        return recordsCount;
    };

    this.getPagesCount = function(){
        var pagesCount = Math.floor(recordsCount/countInPage);
        if(recordsCount%countInPage) {
            pagesCount++;
        }
        return pagesCount;
    };

    this.getSelectedData = function() {
        return table.getSelectedData();
    };

    this.loadData(0);

    this.on = function(event, handler){
        table.on(event, handler);
    };

    this.getSelectedData = function(){
        return table.getSelectedData();
    };

    this.getWidget = function(){
        return table.getWidget();
    };

};


AS.FORMS.RegistryLinkChooser = function(multiSelect, registry){
  var instance = this;
    var filterID = null;

    this.tableComponent = new AS.COMPONENTS.RegistryTable(registry, 777);
    var tableComponent = this.tableComponent;
    this.paginatorComponent = new AS.COMPONENTS.Paginator({
        model: new AS.Entities.PaginatorModel({
            currentPage: 0,
            totalPage: 0
        })
    });

    var dialog = new AS.FORMS.BasicChooserDialog();
    dialog.init({
        showListItemIcon: false,
        showListItemInfo: false,
        showListItemStatus: false,
        multiSelect: multiSelect,
        showSearchField: true,
        listItemIconSize: 16,
        title: i18n.tr("Выбор записи реестра"),
        placeHolderText: i18n.tr("Поиск записей"),
        showSelectedStack: false,
        lazyListLoading: false,
        width: 1000,
        height: 600,
        splitWidth: 982,
        treeWidth: 200,
        paginatorComponent: this.paginatorComponent
    });

    this.paginatorComponent.on("pageChanged", function (page) {
        instance.onPageChanged(page);
    });

    this.correctTreeArray = function (objects) {
        var registryRoot = {};
        registryRoot.isRoot = true;
        registryRoot.registryID = registry.registryID;
        registryRoot.id = registry.registryID;
        registryRoot.notEmpty = objects.length > 0;
        registryRoot.name = registry.name;
        registryRoot.children = objects;
        objects.forEach(function (object) {
            object.notEmpty = object.children.length > 0;
        });
        return [registryRoot];
    };


    AS.FORMS.ApiUtils.getRegistryFilters(registry.registryID, true, "all", AS.OPTIONS.locale, function (filters) {
        dialog.addTree("filters", function (parentNode, tree) {
        }, function (parentNode, tree) {
            dialog.openTreeNode("filters", parentNode.id);
            if (parentNode.id === registry.registryID) {
                tableComponent.loadData(0);
                filterID = null;
            } else {
                tableComponent.loadData(0, parentNode.id);
                filterID = parentNode.id;
            }

        }, null, instance.correctTreeArray(filters), true);
        dialog.selectTreeNode("filters", registry.registryID);
        dialog.showTree("filters");
    });

    this.showDialog = function () {
        dialog.show();
    };

    this.getSelectedItems = function () {
        return dialog.getSelectedItems();
    };

    /**
     *
     * @param event SEE BasicChooserEvent
     * @param handler
     */
    this.on = function (event, handler) {
        dialog.on(event, handler);
    };

    var paginatorComponent = this.paginatorComponent;

    this.tableComponent.on(AS.COMPONENTS.ExtendedTableEvent.dataLoaded, function () {
        paginatorComponent.setCurrentPage(tableComponent.getCurrentPage() + 1);
        var totalPage = tableComponent.getPagesCount();
        if (isNaN(totalPage)) {
            totalPage = 0;
        }
        if (totalPage === 0) {
            paginatorComponent.setCurrentPage(0);
        }
        paginatorComponent.setTotalPage(totalPage);
    });

    this.tableComponent.on(AS.COMPONENTS.ExtendedTableEvent.rowSelected, function (evt, rowIndex, data) {
        instance.selectedModel = rowIndex;
        //какого-то хуя в data нет нихуя, зато есть в rowIndex, из-за этого была вся хуйня
        dialog.trigger(AS.FORMS.BasicChooserEvent.itemSelected, [rowIndex]);

    });

    this.tableComponent.dblclick(function () {
        if (instance.selectedModel) {
            dialog.trigger(AS.FORMS.BasicChooserEvent.itemSelected, [instance.selectedModel]);
            dialog.close();
        }
    });

    this.onPageChanged = function (page) {
        this.tableComponent.loadData(page - 1, filterID);
    };

    dialog.setListComponent(this.tableComponent);
    dialog.setSearchBoxWidth(810);
};
