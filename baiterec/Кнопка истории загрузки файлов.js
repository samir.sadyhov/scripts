(function(){

  if (AS.OPTIONS.locale == 'en') {
    var buttonName = 'History';
  } else if (AS.OPTIONS.locale == 'ru') {
    var buttonName = 'История изменений';
  } else {
    var buttonName = 'Өзгерістердің тарихы';
  }

  var button = jQuery('<button>', {class : "el-button el-button--success el-button--small is-plain"}).text(buttonName);
  view.container.append(button);

  button.click(function(){
    showHistoryData();
  });

  let showHistoryData = function(){
    let playerModel = model.playerModel;
    let historyTableId = playerModel.historyTableId;
    let historyTable = playerModel.getModelWithId(historyTableId);

    var content = jQuery('<div>');
    var table = jQuery("<table class='el-table el-table-bordered'></table>");
    content.append(table);
    var headerRow = jQuery("<thead>");
    table.prepend(headerRow);
    headerRow.append(jQuery("<tr><td>" + buttonName + "</td></tr>"));

    historyTable.modelBlocks.forEach(function(block){
      var value = block[0].value;
      var row = jQuery("<tr>");
      table.append(row);
      var nameTd = jQuery("<td>");
      nameTd.text(value);
      row.append(nameTd);
    });

    jQuery("body").append(content);
    jQuery(content).dialog({
      modal: true,
      width: 640,
      height: 480,
      resizable: false,
      show: {
        effect: 'fade',
        duration: 500
      },
      hide: {
        effect: 'fade',
        duration: 500
      },
      title: buttonName,
      buttons: {'OK': function(){
        $(this).dialog("close");
      }},
      close: function() {
        $(this).remove();
      }
    });

  }

}());
