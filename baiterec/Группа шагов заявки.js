<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<style>
  .panel-heading button {
   display: block;
   background: none;
   border: none;
   width: 100%;
   padding: 10px 15px;
    font-size: 13px;
    font-family: Tahoma, sans-serif;
    font-weight: bold;
    color: #333;
  }
  .panel-heading {
  	padding: 0;
  }
.ui-accordion-header {
    margin: 0 !important;
    padding: 10px 15px !important;
    border-top-left-radius: 3px;
    border-top-right-radius: 3px;
    color: #333;
    background-color: #f5f5f5;
    border-bottom: 1px solid #ddd;
    font-size: 14px;
    font-family: Tahoma, sans-serif;
    font-weight: bolder;
}

.ui-accordion-header-active {
    color: white;
    background-color: #5ED35C;
     border: none;
}

.ui-accordion-content {
    /*border: 1px solid #e0e0e0;*/
    border-top: none;
}

  #accordion_steps h3 {
      padding: 5px 25px;
  }

  .panel-collapse {
      padding: 10px;
  }
  #accordion_steps.ui-accordion .ui-accordion-content {
    padding: 10px !important;
}

#accordion_steps .asf-container {
    width: calc(100% - 22px) !important;
}
.closeActions {
    text-align: right;
    padding: 10px 0;
    cursor: pointer;
}

#closeActions a {
    color: #333;
    font-family: Tahoma, sans-serif;
    font-weight: bold;
    }

.hidden {
    display: none !important;
}
</style>

<div id='closeActions' class ='closeActions'><a id="openAll" >Развернуть все</a> | <a id="closeAll">Свернуть все</a></div>
<div id="accordion_steps" class="panel panel-default">
</div>

$( function() {

  if (AS.OPTIONS.locale == 'en') {
    var expand = 'Expand';
    var collapse = 'Collapse';
  } else if (AS.OPTIONS.locale == 'ru') {
    var expand = 'Развернуть все';
    var collapse = 'Свернуть все';
  } else {
    var expand = 'Барлығын кеңейтіңіз';
    var collapse = 'Барлығын тасалау';
  }
    // debugger;
    // if (model.isCreated) return
  model.isCreated = true;
  let acc = $("#accordion_steps" );
  if (acc.length === 0) return;
//   let acc = $('body').find('#accordion_steps');
  let collapsible = model.style && model.style === 'collapsible';
  if (model.steps !== undefined && model.steps.length > 0) {
        if (!collapsible) {
            $('#closeActions')[0].classList.add('hidden');
        }
        for (let i = 0; i < model.steps.length; i++) {
            let step = model.steps[i];
            let title;
            let span = document.createElement("span");
            if (step.id) {
                span.id = step.id;
            }
            span.classList.add('panel-default');
            span = $(span);
            span.appendTo(acc);

            if (step.title) {
                title = step.title;
            } else if (step.titleId) {
                title = $('body').find('[data-asformid="label.label.' + step.titleId +'"]');
            }
            if (title.length > 0) {
                title.detach();
                if (collapsible) {
                     span.append('<div class="panel-heading"><button type="button" data-toggle="collapse" data-target="#collapse'+i +'">' + title.text() + '</button></div>');
                } else {
                    span.append('<h3>' + title.text() + '</h3>');
                }
            } else {
                span.append('<h3></h3>');
            }
                    //Array.isArray(step.container)


            let content = $('body').find('[data-asformid="table.container.' + step.container +'"]');
            if (content.length > 0) {
                if (collapsible) {
                    // debugger
                    let panel = document.createElement("div");
                    panel.id = 'collapse'+ i;
                    panel.classList.add('panel-collapse');
                    panel.classList.add('collapse');
                    if (model.expandAll && model.expandAll === true) {
                        panel.classList.add('in');
                    }
                    let c = content.detach();
                    $(panel)
                      .append(c)
                      .appendTo(span);

                } else {
                    content.detach().appendTo(span);
                }
            } else {
                acc.append('<div></div>');
            }
        }
  } else {
      let stepNamePrefix = 'table.container.cmp-step';
      if (model.stepNamePrefix !== undefined) {
          stepNamePrefix =  'table.container.' + model.stepNamePrefix;
      }
      let search = '[data-asformid^="' + stepNamePrefix +'"]';
      let forms = $(search);
      if (!forms) return;
      for (let i = 0; i<forms.length; i++) {
        let step = $(forms[i]);
        let value= forms[i].attributes['data-asformid'].nodeValue;
        let num = value.substr(stepNamePrefix.length);
        let title = i+1;
        let titleLabel = step.find('[data-asformid="label.label.h' + num +'"]');
        if (titleLabel.length > 0) {
            title = titleLabel.text();
        }
        acc.append('<h3>' + title + '</h3>');
        step.detach().appendTo(acc);
      }
  }

  if (collapsible) {
      $('#closeAll').text(collapse)
      .click(function () {
          $('.panel-collapse.in').collapse("hide");
      });
      $('#openAll').text(expand)
      .click(function () {
          $('.panel-collapse').collapse("show");
      });
  } else {
      acc.accordion({
        heightStyle: "content",
        collapsible: true
      });
  }

});
