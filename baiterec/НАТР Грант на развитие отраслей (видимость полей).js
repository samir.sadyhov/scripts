view.setVisible(false);

function disabledCustomComponent(cmp) {
  var cc = view.playerView.getViewWithId(cmp);
  if (cc) {
    cc.setEnabled(false);
    ['add', 'edit', 'browse', 'delete'].forEach(function(atr){
      cc.container.find('[innerid="'+atr+'"]').hide();
    });
  }
}

setTimeout(function(){

  //скрыть шаги если заявка открыта не в synergy
  var stepsUr = ['step21', 'step22', 'step23', 'step24', 'step25'];
  if (window.location.href.indexOf('Synergy') == -1) {
    stepsUr.forEach(function(id){
      $('#'+id).hide();
    });
  } else { //если открыто в synergy

    if(AS.OPTIONS.currentUser.userId !== "97d88b75-5595-4179-975a-b267794aa4f6") {

      // заблокировать все поля на изменение
      ['textbox-8ytjz1', 'listbox-3nmorc', 'textbox-x7xrnz', 'textbox-lvxf7k', 'cmp-tnrmme', 'date-vi8ues', 'cmp-isyjm5', 'cmp-psxi49', 'cmp-e8ppva', 'cmp-wlwwa1', 'textbox-jndu4e', 'textbox-umfh8c', 'textbox-ckbfzb', 'textarea-bp673a', 'cmp-jswv84', 'cmp-pm6iej', 'email', 'cmp-avvpwh', 'listbox-eccm14', 'numericinput-3gy32x', 'numericinput-35po14', 'numericinput-nv5s2s', 'textbox-b95qri', 'textbox-8gtbbc', 'textbox-osif9i', 'listbox-69kou5', 'textbox-3iei8x', 'textbox-2l4108', 'textbox-zlst4p', 'textbox-8khur0', 'textbox-7fldt7', 'textbox-lk8f6i', 'cmp-jswv84_copy1', 'cmp-pm6iej_copy1', 'email_copy1', 'cmp-avvpwh_copy1', 'textbox-fyvewu', 'textarea-4ld8ps', 'textarea-o5an0u', 'textbox-r840xz', 'textbox-8jgy2k', 'textbox-opaeop', 'textbox-xa8w6d', 'listbox-eaxqrt', 'volume', 'program_name', 'project', 'sum', 'numericinput-ux78f8', 'textbox-8gtt2d', 'textbox-8gtt2d_copy1', 'textbox-8gtt2d_copy2', 'textbox-8gtt2d_copy3', 'check-via4mn', 'other', 'check-vxazxu', 'check-vxazxu_copy1', 'check-vxazxu_copy3', 'check-vxazxu_copy2', 'check-vxazxu_copy4', 'check-vxazxu_copy5', 'numericinput-hf9ol2', 'textbox-y8hinf'].forEach(function(cmp){
        var component = view.playerView.getViewWithId(cmp);
        if (component) component.setEnabled(false);
      });

      // блокируем динамические таблицы
      ['table-t1j145', 'composition'].forEach(function(cmp){
        var tableView = view.playerView.getViewWithId(cmp);
        if(tableView){
          tableView.setEnabled(false);
          tableView.getViewBlocks().forEach(function(viewBlock, index){
            viewBlock.views.forEach(function(component){
              component.setEnabled(false);
            });
          });
        }
      });

      // скрыть блок загрузки файлов
      ['table-doc1', 'table-doc2', 'table-doc3', 'table-doc4', 'table-doc5', 'table-doc6', 'table-doc7', 'table-doc8', 'table-doc9', 'table-doc10', 'table-doc11'].forEach(function(cmp){
        var tableModel = model.playerModel.getModelWithId(cmp);
        if (tableModel) {
          $('[data-asformid="table.container.'+cmp+'"]')
          .find('[data-asformid="custom.container.'+tableModel.asfProperty.properties[0].id+'"]')
          .parent().parent().hide();
          $('[data-asformid="table.removeRowButton.'+cmp+'"]').hide();
        }
      });

    }


    //Назначить исполнителя
    if (AS.FORMS.ComponentUtils.hasRouteItem('set_executor')) {
      ['step22', 'step23', 'step24', 'step25'].forEach(function(id){
        $('#'+id).hide();
      });
    }

    //Проверить и принять решение о регистрации
    if (AS.FORMS.ComponentUtils.hasRouteItem('decide')) {
      ['step21', 'step23', 'step24', 'step25'].forEach(function(id){
        $('#'+id).hide();
      });
      if (view.playerView.getViewWithId('label-oupk6m_copy1')) view.playerView.getViewWithId('label-oupk6m_copy1').setVisible(false);
      if (view.playerView.getViewWithId('custom-f5ebfs_copy1')) view.playerView.getViewWithId('custom-f5ebfs_copy1').setVisible(false);
    }

    //Подготовить уведомление об отказе в регистрации
    if (AS.FORMS.ComponentUtils.hasRouteItem('do_not_register')) {
      ['step21', 'step23', 'step24', 'step25'].forEach(function(id){
        $('#'+id).hide();
      });
      if (view.playerView.getViewWithId('exp_decision')) view.playerView.getViewWithId('exp_decision').setEnabled(false);
    }

    //Статус предварительная экспертиза
    if (AS.FORMS.ComponentUtils.hasRouteItem('pred_expertiza_status')) {
      ['step21', 'step23', 'step24', 'step25'].forEach(function(id){
        $('#'+id).hide();
      });
      if (view.playerView.getViewWithId('reg_decision')) view.playerView.getViewWithId('reg_decision').setEnabled(false);
      disabledCustomComponent('custom-f5ebfs_copy1');
    }

    //Указать результат предварительно экспертизы
    if (AS.FORMS.ComponentUtils.hasRouteItem('pred_expertiza_result')) {
      ['step21', 'step24', 'step25'].forEach(function(id){
        $('#'+id).hide();
      });
      if (view.playerView.getViewWithId('label-oupk6m')) view.playerView.getViewWithId('label-oupk6m').setVisible(false);
      if (view.playerView.getViewWithId('custom-f5ebfs')) view.playerView.getViewWithId('custom-f5ebfs').setVisible(false);

      if (view.playerView.getViewWithId('reg_decision')) view.playerView.getViewWithId('reg_decision').setEnabled(false);
      disabledCustomComponent('custom-f5ebfs_copy1');
    }

    //Сформировать уведомление о необходимости доработки
    if (AS.FORMS.ComponentUtils.hasRouteItem('creat_notify_rework')) {
      ['step21', 'step24', 'step25'].forEach(function(id){
        $('#'+id).hide();
      });
      if (view.playerView.getViewWithId('reg_decision')) view.playerView.getViewWithId('reg_decision').setEnabled(false);
      if (view.playerView.getViewWithId('exp_decision')) view.playerView.getViewWithId('exp_decision').setEnabled(false);
      disabledCustomComponent('custom-f5ebfs_copy1');
    }

    //Статус предварительная экспертиза
    if (AS.FORMS.ComponentUtils.hasRouteItem('pred_expertiza_status2')) {
      ['step21', 'step24', 'step25'].forEach(function(id){
        $('#'+id).hide();
      });
      if (view.playerView.getViewWithId('reg_decision')) view.playerView.getViewWithId('reg_decision').setEnabled(false);
      if (view.playerView.getViewWithId('exp_decision')) view.playerView.getViewWithId('exp_decision').setEnabled(false);
      disabledCustomComponent('custom-f5ebfs_copy1');
      disabledCustomComponent('custom-f5ebfs');
    }

    // Статус оставить без рассмотрения
    // Указать результат повторной предварительной экспертизы
    if (AS.FORMS.ComponentUtils.hasRouteItem('bez_rassmotreniya_status') ||
        AS.FORMS.ComponentUtils.hasRouteItem('pred_expertiza_result2')) {
      ['step21', 'step25'].forEach(function(id){
        $('#'+id).hide();
      });
      if (view.playerView.getViewWithId('reg_decision')) view.playerView.getViewWithId('reg_decision').setEnabled(false);
      if (view.playerView.getViewWithId('exp_decision')) view.playerView.getViewWithId('exp_decision').setEnabled(false);
      disabledCustomComponent('custom-f5ebfs_copy1');
      disabledCustomComponent('custom-f5ebfs');
    }

    // Статус тех. Экспертиза
    // Статус фин. Экспертиза
    // Статус ожидание решения
    if (AS.FORMS.ComponentUtils.hasRouteItem('teh_expertiza_status') ||
        AS.FORMS.ComponentUtils.hasRouteItem('fin_expertiza_status') ||
        AS.FORMS.ComponentUtils.hasRouteItem('ojidanie_resheniya_status')) {
      ['step21', 'step25'].forEach(function(id){
        $('#'+id).hide();
      });
      if (view.playerView.getViewWithId('reg_decision')) view.playerView.getViewWithId('reg_decision').setEnabled(false);
      if (view.playerView.getViewWithId('exp_decision')) view.playerView.getViewWithId('exp_decision').setEnabled(false);
      if (view.playerView.getViewWithId('add_exp_decision')) view.playerView.getViewWithId('add_exp_decision').setEnabled(false);
      disabledCustomComponent('custom-f5ebfs_copy1');
      disabledCustomComponent('custom-f5ebfs');
    }

    // Сформировать ответ и установить статус
    if (AS.FORMS.ComponentUtils.hasRouteItem('gen_response_set_status')) {
      $('#step21').hide();
      if (view.playerView.getViewWithId('reg_decision')) view.playerView.getViewWithId('reg_decision').setEnabled(false);
      if (view.playerView.getViewWithId('exp_decision')) view.playerView.getViewWithId('exp_decision').setEnabled(false);
      if (view.playerView.getViewWithId('add_exp_decision')) view.playerView.getViewWithId('add_exp_decision').setEnabled(false);
      disabledCustomComponent('custom-f5ebfs_copy1');
      disabledCustomComponent('custom-f5ebfs');
    }

  }

}, 500);
