AS.FORMS.bus.on(AS.FORMS.EVENT_TYPE.formShow, function(event, playerModel, playerView) {
  var cmpTable = 'supportive_measures_table';
  var cmps = {dic1: 'supportive_measures', dic2: 'measures_add', result: 'ceelll'};
  var tableModel = playerModel.getModelWithId(cmpTable);

  var setJoinString = function() {
    var result = '';
    tableModel.modelBlocks.forEach(function(modelBlock, index){
      var tableBlockIndex = modelBlock[0].asfProperty.tableBlockIndex;
      var d1 = tableModel.getModelWithId(cmps.dic1, cmpTable, tableBlockIndex).getTextValues();
      var d2 = tableModel.getModelWithId(cmps.dic2, cmpTable, tableBlockIndex).getTextValues();
      if(d1 && d2) {
        if(result !== '') {
          result += ', ' + d1 + (d2 == '' ? '' : ' - ' + d2);
        } else {
          result = d1 + (d2 == '' ? '' : ' - ' + d2);
        }
      }
    });
    playerModel.getModelWithId(cmps.result).setValue(result);
  };

  setJoinString();

  tableModel.on('tableRowAdd', function(evt, modelTab, modelRow) {
    var tableBlockIndex = modelRow[0].asfProperty.tableBlockIndex;
    playerModel.getModelWithId(cmps.dic1, cmpTable, tableBlockIndex).on("valueChange", function() {
      setJoinString();
    });
    playerModel.getModelWithId(cmps.dic2, cmpTable, tableBlockIndex).on("valueChange", function() {
      setJoinString();
    });
  });

  tableModel.on('tableRowDelete', function(evt, rowModel, rowIndex, removedRow) {
    setJoinString();
  });
});
