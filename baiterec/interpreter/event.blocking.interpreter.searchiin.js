let client = new org.apache.commons.httpclient.HttpClient();
let creds = new org.apache.commons.httpclient.UsernamePasswordCredentials('administrator', 'ubhkzylf916');
client.getParams().setAuthenticationPreemptive(true);
client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);

function getDocumentID(uuid) {
  let get = new org.apache.commons.httpclient.methods.GetMethod("http://127.0.0.1:8080/Synergy/rest/api/formPlayer/documentIdentifier?dataUUID=" + uuid);
  get.setRequestHeader("Content-type", "application/json");
  client.executeMethod(get);
  let resp = get.getResponseBodyAsString();
  get.releaseConnection();
  return String(resp);
}

function searchAppByIIN(iin, payment_method) {
  let query = "http://127.0.0.1:8080/Synergy/rest/api/registry/data_ext?registryCode=bd_zayavlenie_po_expo&loadData=true&term=AND" +
  "&field=iinbin&condition=TEXT_EQUALS&value=" + iin +
  "&field1=radio-je3ut&condition1=TEXT_EQUALS&value1=" + payment_method +
  "&field2=refuse_desicion_status&condition2=NOT_CONTAINS&key2=3" +
  "&field3=refuse_desicion_status&condition3=NOT_CONTAINS&key3=0";

  let get = new org.apache.commons.httpclient.methods.GetMethod(query);
  get.setRequestHeader("Content-type", "application/json");
  client.executeMethod(get);
  let recordsCount = Number(JSON.parse(get.getResponseBodyAsString()).recordsCount);
  get.releaseConnection();
  return recordsCount;
}

function createDocRCC(data) {
  let post = new org.apache.commons.httpclient.methods.PostMethod("http://127.0.0.1:8080/Synergy/rest/api/registry/create_doc_rcc");
  post.setRequestHeader("Content-type", "application/json; charset=utf-8");
  post.setRequestBody(data);
  client.executeMethod(post);
  let resp = post.getResponseBodyAsString();
  post.releaseConnection();
  return JSON.parse(resp);
}

function getRandomDate() {
  let minLimit = 30 * 60 * 1000; // 30 minute
  let maxLimit = 24 * 60 * 60 * 1000; // 24 hour
  let d = new Date(new Date().getTime() + (Math.floor(Math.random() * (maxLimit + 1 - minLimit)) + minLimit)); // new date + random period
  let month = ('0' + Number(d.getMonth() + 1)).slice(-2);
  let time = ('0'+d.getHours()).slice(-2) + ':' + ('0'+d.getMinutes()).slice(-2) + ':' + ('0'+d.getSeconds()).slice(-2)
  return d.getFullYear() + '-' + month + '-' +('0'+d.getDate()).slice(-2) + ' ' + time; // return date format yyyy-mm-dd HH:MM:SS
}

function changeStatusExpo() {
  let expoDocID = String(getDocumentID(dataUUID));
  let randomDateRefused = getRandomDate();
  let data = {
    "registryCode": "bd_ekspo_smena_statusa_kvartiry_po_vremeni",
    "data": [
      {"id":"documentID","type":"textbox","value": expoDocID},
      {"id":"date_of_status_change","type":"date","value": randomDateRefused, "key": randomDateRefused}
    ]
  };

  return createDocRCC(JSON.stringify(data));
}

function chekAge(iin) {
  let year = iin.substring(0, 2);
  let ep = Number(iin.substring(6, 7));
  switch (ep) {
    case 1:
    case 2:
      year = '18' + year;
      break;
    case 3:
    case 4:
      year = '19' + year;
      break;
    case 5:
    case 6:
      year = '20' + year;
      break;
  }
  let birthDay = new Date(year, (iin.substring(2, 4) - 1), (iin.substring(4, 6)));
  return ((new Date().getTime() - birthDay.getTime()) / (24 * 3600 * 365.25 * 1000)) | 0;
}


function validIIIN(iin) {
  if (!iin) return false;
  if (iin.length != 12) return false;
  if (!(/^\d+$/.test(iin))) return false;
  if (chekAge(iin) > 100) return false;

  let s = 0;
  for (let i = 0; i < 11; i++) {
    s = s + (i + 1) * iin[i];
  }
  let k = s % 11;
  if (k === 10) {
    s = 0;
    for (let i = 0; i < 11; i++) {
      let t = (i + 3) % 11;
      if (t === 0) t = 11;
      s = s + t * iin[i];
    }
    k = s % 11;
    if (k === 10) return false;
    return (k === Number(iin.substring(11, 12)));
  }
  return (k === Number(iin.substring(11, 12)));
}

let result = true;
let message = "ok";
let currentForm = platform.getFormsManager().getFormData(dataUUID);
currentForm.load();

try {

  let iinbin = String(currentForm.getValue('iinbin'));


  if(!validIIIN(iinbin)) {
    let expoRes = changeStatusExpo();
    currentForm.setValue("refuse_desicion_status", "3");
    currentForm.save();
    result = false;
    message = "Маршрут прерван. {Причина: 'Поле ИИН заполнено неверно', ИИН: '" + iinbin + "'}";

    if(expoRes && expoRes.errorCode == '0') {
      message += "\ndocumentID: " + expoRes.documentID + " dataUUID: " + expoRes.dataID
    } else {
      message += "expoRes.errorCode: " + expoRes.errorCode;
    }

  } else {
    let payment_method = String(currentForm.getValue('radio-je3ut'));

    if(payment_method != '2' && payment_method != '3') {
      result = true;
      message = "Статус: OK {payment_method: " + payment_method + "}";
    } else {
      let apps = searchAppByIIN(iinbin, payment_method);
      if(payment_method == '2' && apps > 1) {
        //БВУ
        let expoRes = changeStatusExpo();
        currentForm.setValue("refuse_desicion_status", "3");
        currentForm.save();
        result = false;
        message = "Маршрут прерван. {Причина: 'У заявителя с ИИНом ("+iinbin+"), указанным в данной заявке, уже есть активные заявки на рассмотрении'}";
        message += "\n{payment_method: "+payment_method+", apps: "+apps+"}"
        if(expoRes && expoRes.errorCode == '0') {
          message += "\ndocumentID: " + expoRes.documentID + " dataUUID: " + expoRes.dataID
        } else {
          message += "expoRes.errorCode: " + expoRes.errorCode;
        }
      } else if(payment_method == '3' && apps > 1) {
        //Рассрочка
        let expoRes = changeStatusExpo();
        currentForm.setValue("refuse_desicion_status", "3");
        currentForm.save();
        result = false;
        message = "Маршрут прерван. {Причина: 'У заявителя с ИИНом ("+iinbin+"), указанным в данной заявке, уже есть активные заявки на рассмотрении'}";
        message += "\n{payment_method: "+payment_method+", apps: "+apps+"}"
        if(expoRes && expoRes.errorCode == '0') {
          message += "\ndocumentID: " + expoRes.documentID + " dataUUID: " + expoRes.dataID
        } else {
          message += "expoRes.errorCode: " + expoRes.errorCode;
        }
      } else {
        result = true;
        message = "Статус: OK {payment_method: "+payment_method+", apps: "+apps+"}";
      }

    }

  }

} catch (err) {
  let expoRes = changeStatusExpo();
  currentForm.setValue("refuse_desicion_status", "3");
  currentForm.save();
  result = false;
  message += err.toString();
}
