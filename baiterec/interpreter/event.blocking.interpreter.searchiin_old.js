function getDocumentID(uuid) {
  let client = new org.apache.commons.httpclient.HttpClient();
  let creds = new org.apache.commons.httpclient.UsernamePasswordCredentials('administrator', 'ubhkzylf916');
  client.getParams().setAuthenticationPreemptive(true);
  client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);

  let get = new org.apache.commons.httpclient.methods.GetMethod("http://127.0.0.1:8080/Synergy/rest/api/formPlayer/documentIdentifier?dataUUID=" + uuid);
  get.setRequestHeader("Content-type", "application/json");
  client.executeMethod(get);
  let resp = get.getResponseBodyAsString();
  get.releaseConnection();
  return resp+"";
}

function searchAppByIIN(iin) {
  let client = new org.apache.commons.httpclient.HttpClient();
  let creds = new org.apache.commons.httpclient.UsernamePasswordCredentials('administrator', 'ubhkzylf916');
  client.getParams().setAuthenticationPreemptive(true);
  client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);

  let query = "http://127.0.0.1:8080/Synergy/rest/api/registry/data_ext?registryCode=bd_zayavlenie_po_expo&loadData=true&term=AND" +
  "&field=iinbin&condition=TEXT_EQUALS&value=" + iin +
  "&field1=radio-je3ut&condition1=TEXT_EQUALS&value1=3" +
  "&field2=refuse_desicion_status&condition2=NOT_CONTAINS&key2=3" +
  "&field3=refuse_desicion_status&condition3=NOT_CONTAINS&key3=0";

  let get = new org.apache.commons.httpclient.methods.GetMethod(query);
  get.setRequestHeader("Content-type", "application/json");
  client.executeMethod(get);
  let result = JSON.parse(get.getResponseBodyAsString()).recordsCount > 1 ? "1" : "0";
  get.releaseConnection();
  return result;
}

function changeStatusExpo() {
  let documentID = getDocumentID(dataUUID);
  let query = JSON.stringify({id: documentID, status: 1});
  let client = new org.apache.commons.httpclient.HttpClient();
  let creds = new org.apache.commons.httpclient.UsernamePasswordCredentials('arta@gmail.com', 'EvhX5bCZ4VkM1CS3Y9Ag');
  client.getParams().setAuthenticationPreemptive(true);
  client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);

  let post = new org.apache.commons.httpclient.methods.PostMethod("http://expo.baiterek.gov.kz/api/flat/change_status");
  post.setRequestHeader("Content-type", "application/json; charset=utf-8");
  post.setRequestBody(query);
  client.executeMethod(post);
  let resp = post.getResponseBodyAsString();
  post.releaseConnection();
  return resp+"";
}

function chekAge(iin) {
  let year = iin.substring(0, 2);
  let ep = Number(iin.substring(6, 7));
  switch (ep) {
    case 1:
    case 2:
      year = '18' + year;
      break;
    case 3:
    case 4:
      year = '19' + year;
      break;
    case 5:
    case 6:
      year = '20' + year;
      break;
  }
  let birthDay = new Date(year, (iin.substring(2, 4) - 1), (iin.substring(4, 6)));
  return ((new Date().getTime() - birthDay.getTime()) / (24 * 3600 * 365.25 * 1000)) | 0;
}


function validIIIN(iin) {
  if (!iin) return false;
  if (iin.length != 12) return false;
  if (!(/^\d+$/.test(iin))) return false;
  if (chekAge(iin) > 100) return false;

  let s = 0;
  for (let i = 0; i < 11; i++) {
    s = s + (i + 1) * iin[i];
  }
  let k = s % 11;
  if (k === 10) {
    s = 0;
    for (let i = 0; i < 11; i++) {
      let t = (i + 3) % 11;
      if (t === 0) t = 11;
      s = s + t * iin[i];
    }
    k = s % 11;
    if (k === 10) return false;
    return (k === Number(iin.substring(11, 12)));
  }
  return (k === Number(iin.substring(11, 12)));
}

let result = true;
let message = "ok";
let currentForm = platform.getFormsManager().getFormData(dataUUID);
currentForm.load();

try {

  let iinbin = currentForm.getValue('iinbin')+"";

  if(!validIIIN(iinbin)) {
    let expoRes = changeStatusExpo();
    currentForm.setValue("refuse_desicion_status", "3");
    currentForm.save();
    result = false;
    message = "Маршрут прерван. {Причина: 'Поле ИИН заполнено неверно', ИИН: '" + iinbin + "'}\n" + expoRes;
  } else {
    let sposob = currentForm.getValue('radio-je3ut')+"";
    let sposobP = sposob == '3' || sposob == 'реализация в рассрочку';

    if(!sposobP) {
      result = true;
      message = "OK {способ реализации: " + sposob + "}";
    } else {
      let apps = searchAppByIIN(iinbin);
      if(apps == "1") {
        let expoRes = changeStatusExpo();
        currentForm.setValue("refuse_desicion_status", "3");
        currentForm.save();
        result = false;
        message = apps+": Маршрут прерван. {Причина: 'У заявителя с ИИНом ("+iinbin+"), указанным в данной заявке, уже есть активные заявки на рассмотрении'}\n" + expoRes;
      } else {
        result = true;
        message = apps+": OK {Статус: 'в системе нет заявок в рассрочку с ИИН: " + iinbin + "'}";
      }

    }

  }

} catch (err) {
  let expoRes = changeStatusExpo();
  currentForm.setValue("refuse_desicion_status", "3");
  currentForm.save();
  result = false;
  message = err;
}
