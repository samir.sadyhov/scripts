function changeStatus(data) {
  let client = new org.apache.commons.httpclient.HttpClient();
  let creds = new org.apache.commons.httpclient.UsernamePasswordCredentials('arta@gmail.com', 'EvhX5bCZ4VkM1CS3Y9Ag');
  client.getParams().setAuthenticationPreemptive(true);
  client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);

  let post = new org.apache.commons.httpclient.methods.PostMethod("http://expo.baiterek.gov.kz/api/flat/change_status");
  post.setRequestHeader("Content-type", "application/json; charset=utf-8");
  post.setRequestBody(data);
  client.executeMethod(post);
  let resp = post.getResponseBodyAsString();
  post.releaseConnection();
  return String(resp);
}

let result = true;
let message = "ok";
let currentForm = platform.getFormsManager().getFormData(dataUUID);
currentForm.load();

try {

  let expoDocID = String(currentForm.getValue('documentID'));
  let query = JSON.stringify({id: expoDocID, status: 1});
  let res = changeStatus(query);

  if(res.indexOf('responseCode') !== -1) {
    res = JSON.parse(res);
    if(res.responseCode == 0) {
      message = "responseCode: " + res.responseCode + ", status: OK";
      result = true;
    } else {
      message = "responseCode: " + res.responseCode + ", status: ERROR";
      result = false;
    }
  } else {
    message = res;
    result = false;
  }

  currentForm.setValue("status_changed", "1");
  currentForm.save();

} catch (err) {
  currentForm.setValue("status_changed", "1");
  currentForm.save();
  message = err.toString();
  result = false;
}
