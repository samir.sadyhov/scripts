let client = new org.apache.commons.httpclient.HttpClient();
let creds = new org.apache.commons.httpclient.UsernamePasswordCredentials('administrator', 'ubhkzylf916');
client.getParams().setAuthenticationPreemptive(true);
client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);

function getDocumentID(uuid) {
  let get = new org.apache.commons.httpclient.methods.GetMethod("http://127.0.0.1:8080/Synergy/rest/api/formPlayer/documentIdentifier?dataUUID=" + uuid);
  get.setRequestHeader("Content-type", "application/json");
  client.executeMethod(get);
  let resp = get.getResponseBodyAsString();
  get.releaseConnection();
  return String(resp);
}

function createDocRCC(data) {
  let post = new org.apache.commons.httpclient.methods.PostMethod("http://127.0.0.1:8080/Synergy/rest/api/registry/create_doc_rcc");
  post.setRequestHeader("Content-type", "application/json; charset=utf-8");
  post.setRequestBody(data);
  client.executeMethod(post);
  let resp = post.getResponseBodyAsString();
  post.releaseConnection();
  return JSON.parse(resp);
}

function getRandomDate() {
  let minLimit = 30 * 60 * 1000; // 30 minute
  let maxLimit = 24 * 60 * 60 * 1000; // 24 hour
  let d = new Date(new Date().getTime() + (Math.floor(Math.random() * (maxLimit + 1 - minLimit)) + minLimit)); // new date + random period
  let month = ('0' + Number(d.getMonth() + 1)).slice(-2);
  let time = ('0'+d.getHours()).slice(-2) + ':' + ('0'+d.getMinutes()).slice(-2) + ':' + ('0'+d.getSeconds()).slice(-2)
  return d.getFullYear() + '-' + month + '-' +('0'+d.getDate()).slice(-2) + ' ' + time; // return date format yyyy-mm-dd HH:MM:SS
}

function changeStatusExpo() {
  let expoDocID = String(getDocumentID(dataUUID));
  let randomDateRefused = getRandomDate();
  let data = {
    "registryCode": "bd_ekspo_smena_statusa_kvartiry_po_vremeni",
    "data": [
      {"id":"documentID","type":"textbox","value": expoDocID},
      {"id":"date_of_status_change","type":"date","value": randomDateRefused, "key": randomDateRefused}
    ]
  };

  return createDocRCC(JSON.stringify(data));
}

function searchInJson(data, cmpID) {
  var result = null;
  var tmpData = data.data ? data.data : data;
  for (var i = 0; i < tmpData.length; i++) {
    if (tmpData[i].id == cmpID) {
      result = tmpData[i];
      break;
    }
  }
  return result;
}

let result = true;
let message = "ok";

try {

  let expoRes = changeStatusExpo();
  if(expoRes && expoRes.errorCode == '0') {
    message = "Создана очередь на снятие брони" +
    "\ndocumentID: " + expoRes.documentID +
    "\ndataUUID: " + expoRes.dataID +
    "\ndate_of_status_change: " + searchInJson(expoRes, 'date_of_status_change').key
  } else {
    message = "Ошибка создания очереди на снятие брони\n" + JSON.stringify(expoRes);
  }

} catch (err) {
  message = err.toString();
}
