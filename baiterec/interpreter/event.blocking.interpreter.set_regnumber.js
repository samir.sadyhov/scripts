var client = new org.apache.commons.httpclient.HttpClient();
var creds = new org.apache.commons.httpclient.UsernamePasswordCredentials('administrator', 'ubhkzylf916');
client.getParams().setAuthenticationPreemptive(true);
client.getState().setCredentials(org.apache.commons.httpclient.auth.AuthScope.ANY, creds);


function getRegistryId(asfDataId) {
  var get = new org.apache.commons.httpclient.methods.GetMethod("http://127.0.0.1:8080/Synergy/rest/api/asforms/data/document?dataUUID=" + asfDataId);
  get.setRequestHeader("Content-type", "application/json");
  client.executeMethod(get);
  var resp = get.getResponseBodyAsString();
  get.releaseConnection();
  return JSON.parse(resp)[asfDataId].registryID;
}

function getRegistryInfo(registryID) {
  var get = new org.apache.commons.httpclient.methods.GetMethod("http://127.0.0.1:8080/Synergy/rest/api/registry/info?registryID=" + registryID);
  get.setRequestHeader("Content-type", "application/json");
  client.executeMethod(get);
  var resp = get.getResponseBodyAsString();
  get.releaseConnection();
  return JSON.parse(resp);
}

function getRegistryCodeFromDataUUID(asfDataId) {
  return getRegistryInfo(getRegistryId(asfDataId)).code;
}

function searchInRegistryServices(value) {
  var url = "http://127.0.0.1:8080/Synergy/rest/api/registry/data_ext?registryCode=" + encodeURIComponent('Реестр_услуг') + "&field=catalogue_code_services&condition=TEXT_EQUALS&value=" + encodeURIComponent(value) + "&pageNumber=0&countInPart=1&loadData=false";
  var get = new org.apache.commons.httpclient.methods.GetMethod(url);
  get.setRequestHeader("Content-type", "application/json");
  client.executeMethod(get);
  var result = JSON.parse(get.getResponseBodyAsString());
  get.releaseConnection();
  if (result.count === 0) {
    return null;
  }
  return result.data[0];
}

function getFormData(asfDataId) {
  var get = new org.apache.commons.httpclient.methods.GetMethod("http://127.0.0.1:8080/Synergy/rest/api/asforms/data/" + asfDataId);
  get.setRequestHeader("Content-type", "application/json");
  client.executeMethod(get);
  var resp = get.getResponseBodyAsString();
  get.releaseConnection();
  return JSON.parse(resp);
}

/*
return
{
    "errorCode": "0",
    "documentID": "32a785df-090c-4926-ba82-c1765e028156",
    "dataUUID": "93ecf5f0-6a36-4db6-be6f-91df7ca1b277",
    "asfNodeID": "7561a620-3d72-11e8-8267-00505602002b"
}
*/
function createDoc(registryCode) {
  var get = new org.apache.commons.httpclient.methods.GetMethod("http://127.0.0.1:8080/Synergy/rest/api/registry/create_doc?registryCode=" + encodeURIComponent(registryCode));
  get.setRequestHeader("Content-type", "application/json");
  client.executeMethod(get);
  var resp = get.getResponseBodyAsString();
  get.releaseConnection();
  return JSON.parse(resp);
}

function activateDoc(documentID) {
  var get = new org.apache.commons.httpclient.methods.GetMethod("http://127.0.0.1:8080/Synergy/rest/api/registry/activate_doc?documentID=" + documentID);
  get.setRequestHeader("Content-type", "application/json");
  client.executeMethod(get);
  var resp = get.getResponseBodyAsString();
  get.releaseConnection();
  return JSON.parse(resp);
}

function searchInJson(data, cmpID) {
  var result = null;
  var tmpData = data.data ? data.data : data;
  for (var i = 0; i < tmpData.length; i++) {
    if (tmpData[i].id == cmpID) {
      result = tmpData[i];
      break;
    }
  }
  return result;
}


var result = true;
var message = "ok";

try {
  // regnumber в реестре услуг поле с кодом реестра номеров
  // renumber cmp_id в реестре номеров
  // в заявке сохранять номер в поле regnumber

  var currenRegistryCode = getRegistryCodeFromDataUUID(dataUUID);
  var resultSearch = searchInRegistryServices(currenRegistryCode);

  if (!resultSearch) {
    result = false;
    message = "не найдено записи с кодом реестра " + currenRegistryCode;
  } else {
    var numberRegistryCode = getFormData(resultSearch.dataUUID);
    numberRegistryCode = searchInJson(numberRegistryCode, 'regnumber');

    if (numberRegistryCode && numberRegistryCode.value) {
      numberRegistryCode = numberRegistryCode.value;

      var newRowNumberData = getFormData(createDoc(numberRegistryCode).dataUUID);
      var newNumber = searchInJson(newRowNumberData, 'renumber');

      if (newNumber && newNumber.value) {
        var currentForm = platform.getFormsManager().getFormData(dataUUID);
        currentForm.load();
        currentForm.setValue("regnumber", newNumber.value);
        currentForm.save();
        result = true;
        message = "Зарегестрированно за номером: " + newNumber.value;
      }

    } else {
      result = false;
      message = "не найдено ссылки на реестр номеров";
    }
  }

} catch (err) {
  result = false;
  message = err.message;
}
