<!--<script src="//cdn.rawgit.com/satazor/SparkMD5/master/spark-md5.min.js"></script>-->
<style>
    .file-uploader {
        border: 2px dashed #e0e0e0 !important;
        text-align: center;
        cursor: pointer;
		position: relative;
		font-size: 20px;
		padding: 30px;
    }
    .upload-input {
        display: none !important;
        font-size: 10px
    }
    .hidden {
        display: none !important;
    }
    .asf-link {
        color: #06f !important;
        text-decoration: underline !important;
    }
</style>

<div id='files-panel'></div>

// Класс для загрузки файла в синержи

class BFileUploader { //QuerySingleFileUploader

    constructor(file, onFileUploaded, onUploadCancel) {
        this.fileReader = new FileReader();
        this.file = file;
        this.onFileUploaded = onFileUploaded;
        this.onUploadCancel = onUploadCancel;
        this.api = AS.FORMS.ApiUtils;
        this.urlStartUpload = "rest/api/storage/start_upload";
        this.urlPartUload = "rest/api/storage/upload_part";

        this.size = 0;
        this.sent = 0;
        this.lastSentIndex = 0;
        this.hash = null;
        this.stop = false;
        this.uploadFile = null;
        this.part = 1024 * 1024;
        this.progress = 0;

        this.playerModel = model.playerModel;
        this.tableFilesModel = this.playerModel.getModelWithId(model.asfProperty.ownerTableId);

        let instance = this;
        this.fileReader.onloadend = function(e) {
            if (e.target.readyState == 2){
                var result = e.target.result;
                var b64encoded = btoa(
                    new Uint8Array(result).
                    reduce((data, byte) => data + String.fromCharCode(byte), ''));
                var data = new FormData();
                data.append('body', b64encoded);
                instance.uploadPart(instance.uploadFile, data, function(){
                    instance.partLoaded();
                }, function(){
                    instance.retryAction('loadPart');
                });
            }
        }
    }

    uploadPart (uploadFile, data, handler, errorHandler){
        var url = "rest/api/storage/upload_part?file="+encodeURIComponent(uploadFile);
        let api = this.api;
        return jQuery.ajax({
            beforeSend : api.addAuthHeader,
            url: api.getFullUrl(url),
            data: data,
            cache: false,
            contentType: false,
            processData: false,
            type: 'POST',
            success: function(){
                handler();
            },
            error: function(){
                errorHandler(arguments);
            },
            statusCode: {
                401: function() {
                    AS.SERVICES.unAuthorized();
                }
            }
        });
    }

    getCookie (name) {
        var matches = document.cookie.match(new RegExp(
            "(?:^|; )" + name.replace(/([\.$?*|{}\(\)\[\]\\\/\+^])/g, '\\$1') + "=([^;]*)"
        ));
        return matches ? decodeURIComponent(matches[1]) : undefined;
    }

    setCookie(name, value, options) {
        options = options || {};
        let expires = options.expires;
        if (typeof expires == "number" && expires) {
            let d = new Date();
            d.setTime(d.getTime() + expires * 1000);
            expires = options.expires = d;
        }
        if (expires && expires.toUTCString) {
            options.expires = expires.toUTCString();
        }
        value = encodeURIComponent(value);
        let updatedCookie = name + "=" + value;
        for (var propName in options) {
            updatedCookie += "; " + propName;
            let propValue = options[propName];
            if (propValue !== true) {
                updatedCookie += "=" + propValue;
            }
        }
        document.cookie = updatedCookie;
    }

    deleteCookie(name) {
        document.cookie = name + '=; expires=Thu, 01 Jan 1970 00:00:01 GMT;';
    }

    stopLoading (){
        this.stop = true;
        this.onUploadCancel(this);
    }

    loadingStopped() {
        console.log('cancel loading file ' + this.fileName);
        this.loading = false;
        this.stop = false;
        this.file = null;
        this.size = 0;
        this.sent = 0;
        this.fileName = null;
        this.uploadFile = null;
        this.hash = null;
        this.loading = false;
    }

    loadPart() {
    //   var url = this.urlPartUload + "?file="+encodeURIComponent(this.uploadFile);
        if(this.stop) {
            this.loadingStopped();
            return;
        }
        this.lastSentIndex = Math.min(this.sent + this.part, this.size);
        let blob = this.file.slice(this.sent, this.lastSentIndex);
        this.fileReader.readAsArrayBuffer(blob);
    }

    // TEST
    retryAction(actionName) {
      let instance = this;
      if(this.stop) {
        this.loadingStopped();
      }
      setTimeout(function(){
        instance[actionName]();
      }, 30000);
    }
    // TEST

    partLoaded(){
        this.sent = this.lastSentIndex;
        var progressCookie = {uploadFile : this.uploadFile, sent : this.sent};
        this.setCookie("file" + this.hash, JSON.stringify(progressCookie), {expires : 24 * 60 * 60});
        this.setCookie("fileloading" + this.hash, new Date().getTime(), {expires : 10 * 60});

        let progress = 0;
        if(this.size > 0) {
            progress = parseInt(this.sent * 100/this.size);
        }
        if(progress === 100){
            progress = 99;  // 100 будет когда файл добавится
        }

        let me = this;
        let cancelButton = jQuery("<div>").html("X");
        if (me.playerModel.img && me.playerModel.img.deleted) {
            cancelButton = jQuery("<img>", {src: me.playerModel.img.deleted});
        }

        cancelButton.attr('title', 'Отмена загрузки');
        cancelButton.css({
          'width': '17px',
          'height': '17px',
          'cursor': 'pointer'
        });
        cancelButton.on('click', function(){
          me.stopLoading();
        });
        this.nameLabel.text(this.fileName+" ("+progress+"%)").append(cancelButton);

        if(this.sent >= this.size) {
            this.onFileUploaded(this);
            this.loading = false;
        } else {
            this.loadPart();
        }
    }

    startFileUploading () {
        let me = this;
        this.api.simpleAsyncGet(this.urlStartUpload, function(result){
            if(result.errorCode != '0') {
                console.log(result);
                alert("Ошибка при загрузке файла " + me.file.name);
                return;
            }
            me.uploadFile = result.file;
            me.loadPart();
        }, undefined, undefined, function(){
            console.log(arguments);
            // TODO error
            alert("Ошибка при загрузке файла " + me.file.name);
        });
    }

    calculateHash (file, successHandler, errorHandler){
        var blobSlice = File.prototype.slice || File.prototype.mozSlice || File.prototype.webkitSlice,
            chunkSize = 2097152,                             // Read in chunks of 2MB
            chunks = Math.ceil(file.size / chunkSize),
            currentChunk = 0,
            spark = new SparkMD5.ArrayBuffer(),
            fileReader = new FileReader();
        fileReader.onload = function (e) {
            console.log('read chunk nr', currentChunk + 1, 'of', chunks);
            spark.append(e.target.result);                   // Append array buffer
            currentChunk++;

            if (currentChunk < chunks) {
                loadNext();
            } else {
                successHandler(spark.end());
            }
        };
        fileReader.onerror = function (evt) {
            errorHandler(evt);
        };
       function loadNext() {
            var start = currentChunk * chunkSize,
                end = ((start + chunkSize) >= file.size) ? file.size : start + chunkSize;
            fileReader.readAsArrayBuffer(blobSlice.call(file, start, end));
        }
        loadNext();
    }

    uploadFileStart(file){
        let instance = this;
        let api = this;
        instance.loading = true;
        instance.stop = false;
        instance.sent = 0;
        instance.file = file;
        instance.size = file.size;
        instance.fileName = file.name;
        this.playerModel.isLoading = false;

        this.file.slice =  this.file.slice || this.file.webkitSlice || this.file.mozSlice;

        instance.row = this.tableFilesModel.createRow();
        instance.nameLabel = view.playerView.getViewWithId(this.row[0].asfProperty.id, model.asfProperty.ownerTableId, this.row[0].asfProperty.tableBlockIndex).container.find('.asf-link');

        instance.nameLabel.text(instance.fileName+" (0%)");

        if(editable) instance.nameLabel.off();

        instance.calculateHash(file, function(hash){
            instance.hash = hash;
            console.log("FILE HASH --  " + hash);

            var loadedInfo = api.getCookie("file"+instance.hash);
            var loadingInfo = api.getCookie("fileloading"+instance.hash);
            if(loadedInfo) {
                try {
                    var progress = JSON.parse(loadedInfo);

                    if(loadingInfo && progress.send < instance.size) {
                        var time = parseInt(loadingInfo);
                        // если прошлая загрузка не была завершена,
                        // и где то в другой вкладке идет или шла загрузка этого файла (в последние 10 минут) то нам надо грузить данные в новый файл
                        // чтобы не было конфликтов и мы меняем hash файла
                        //
                        if(new Date().getTime() - time < 10 *60 * 1000) {

                            console.log("Another loading process for this file active. Starting new Load", progress);

                            instance.hash = SparkMD5.hash(instance.hash+" "+Math.random());
                            instance.startFileUploading();
                        }
                    }

                    instance.uploadFile = progress.uploadFile;

                    console.log("loading will be continued. loaded info ", progress);

                    instance.lastSentIndex = progress.sent;
                    instance.partLoaded();

                    return;
                } catch (e){
                    alert('Ошибка при загрузке файла');
                    console.log(e);
                }
            }

            instance.startFileUploading();

        }, function(evt){
            alert("Произошла ошибка");
        });
    }

    start() {
        console.log('Start upload ' + this.file.name);
        this.uploadFileStart(this.file);
    }
}

//***************************************************
$(function(player, context) {

    player.Uploader =  function(player) {
        window.addEventListener("dragover",function(e){
            e = e || event;
            e.preventDefault();
        },false);

        window.addEventListener("drop",function(e){
            e = e || event;
            e.preventDefault();
        },false);

        let lang = AS.OPTIONS.locale;
        let api = AS.FORMS.ApiUtils;
        let me = this;
        eval(model.asfProperty.config.script);


        if(lang == 'kk'){
          var mess = '... немесе тінтуірмен басқа орынға апарыңыз ';
          var mess2 = 'Жүктеу үшін файлдарды таңдап алыңыз';
          var mess3 = 'Құжатты бекіту:';
          var mess4 = 'Құжат атауы:';
          var mess5 = 'Құжат сипаттамасы';
          var mess6 = 'Құжат тобы';
        } else if (lang == 'ru') {
          var mess = '... или перетащите мышью';
          var mess2 = 'Выберите файл';
          var mess3 = 'Прикрепить документ:';
          var mess4 = 'Наименование документа';
          var mess5 = 'Описание документа';
          var mess6 = 'Группа документа';
        }
        else {
          var mess = '... or drag with the mouse';
          var mess2 = 'Select files to upload';
          var mess3 = 'Attach document:';
          var mess4 = 'Title of the document';
          var mess5 = 'Document description';
          var mess6 = 'Document group';
        }

        let container = view.container;
        let panel = container.find('#files-panel');
        panel.attr('title', mess2 + '\n' + mess);
        let playerModel = model.playerModel;
        let tableID = model.asfProperty.ownerTableId;
        let tableHistoryModel = null;
        if (playerModel.historyTableId) tableHistoryModel = playerModel.getModelWithId(playerModel.historyTableId);

        // Скрывать дроп панель если мы не в режиме редактирования. Отключено на время отладки
        if (!model.debug) {
            if (!editable && panel.length >0 ) {
                panel[0].classList.add('hidden');
            }
        }

        this.drop = jQuery('<div class="file-uploader">' + mess2 + '<br>' + mess + '</div>');
        this.input = jQuery('<input id="file" type="file" multiple class="upload-input">');
        panel.append(this.drop).append(this.input);
        this.table = playerModel.getModelWithId(tableID);
        jQuery('div[data-asformid="table.addRowButton.'+tableID+'"]').hide();

        this.getDocumentID = function(){
          if (!playerModel.documentID) {
            var url = 'rest/api/formPlayer/documentIdentifier?dataUUID=' + encodeURIComponent(playerModel.asfDataId);
            var request = jQuery.ajax({
              url: api.getFullUrl(url),
              type: "GET",
              beforeSend: api.addAuthHeader,
              async: false
            });
            playerModel.documentID = request.responseText;
          }
          return playerModel.documentID;
        };

        this.getFileID = function(value) {
          if(!value || value == '') return null;
          var s = value.split('&');
          var file_identifier;
          for (var i = 0; i < s.length; i++) {
            // file_identifier = s[i].split('file_identifier=');
            file_identifier = s[i].split('identifier=');
            if (file_identifier.length > 1) return file_identifier[1];
          }
        };

        this.getFileURL = function(identifier){
          // return '#' + jQuery.param({
          //   submodule: 'common',
          //   action: 'open_file',
          //   file_identifier: identifier
          // });
          //  var token = "Basic " + btoa(unescape(encodeURIComponent(AS.OPTIONS.login+":"+AS.OPTIONS.password)));
          // var token = "Basic " + btoa(unescape(encodeURIComponent(AS.OPTIONS.login+":6lqP9l5gt34VhrbWacKESvQd")));
          let token = "Basic JHNlc3Npb246NmxxUDlsNWd0MzRWaHJiV2FjS0VTdlFk";
          return AS.OPTIONS.coreUrl.replace('/Synergy', '').replace('/Configurator', '')
            + "baiterek/file?identifier=" + identifier +"&token=" + token;
        };

        this.addHistoryData = function(fileName, del){
          if (!tableHistoryModel) return;
          let hRow = tableHistoryModel.createRow();
          let currDate = AS.FORMS.DateUtils.formatDate(new Date(), '${dd}.${mm}.${yyyy} ${HH}:${MM}:${SS}');
          if (del) {
            hRow[0].setValue(currDate + ': файл "' + fileName + '" удален');
          } else {
            hRow[0].setValue(currDate + ': файл "' + fileName + '" добавлен');
          }
          $('div[data-asformid="table.removeRowButton.'+playerModel.historyTableId+'"]').parent().hide();
        };

        this.replaceRemoveRowButton = function() {
          var removeRowButton = jQuery('div[data-asformid="table.removeRowButton.'+tableID+'"]');
          removeRowButton.attr('title', 'Удалить файл');

          if (playerModel.img && playerModel.img.deleted) {
            removeRowButton.css({
            'background': 'url('+playerModel.img.deleted+') no-repeat center',
            'width': '20px',
            'height': '20px',
            'padding-left': '0px'
            });
          }

          removeRowButton.parent().parent().css({'border-bottom': '1px dashed #e0e0e0'});
          // var attachImg = jQuery("<span class='attach-img'>").append(jQuery("<img>", {src: playerModel.img.attachment}));
          // $('.attach-img').remove();
          // removeRowButton.parent().parent().find('.asf-link').prepend(attachImg);
        };
        this.replaceRemoveRowButton();

        // Враппер для обработки клика на область загрузки для активации окна для выбора файлов
        this.drop.click(function () {
          me.input.trigger('click');
        });

        this.onFileUploaded = function(uploadInstance){
          me.replaceRemoveRowButton();
          let documentID = me.getDocumentID();
          let data = {docID : documentID, path: 'ase:workContainer', fileName : uploadInstance.fileName, filePath : uploadInstance.uploadFile};
          jQuery.when(api.simpleAsyncPost('rest/api/storage/document/'+documentID+'/attachment/create', null, null, data)).then(function(result){
            var errorCode = result.errorCode || result.result;
            var errorMessage = result.errorMessage || result.message;
            if(errorCode != '0') {
              // uploadInstance.tableFilesModel.removeRow(uploadInstance.row.tableBlockIndex);
              alert('Ошибка при присоединении файла к заявке');
              throw new Error("file cannot be added ::: " + errorMessage);
            } else {
              uploadInstance.row[0].setValue({value: me.getFileURL(result.id), key: uploadInstance.fileName + '; true'});
              me.addHistoryData(uploadInstance.fileName);
              playerModel.isLoading = true;
              uploadInstance.deleteCookie("file"+uploadInstance.hash);
              uploadInstance.deleteCookie("fileloading"+uploadInstance.hash);
              console.log('Файл: "' + uploadInstance.fileName + '" загружен');
            }
          });
        };

        this.onUploadCancel = function(uploadInstance){
          uploadInstance.tableFilesModel.modelBlocks.forEach(function(modelBlock, index){
            if(modelBlock.tableBlockIndex == uploadInstance.row.tableBlockIndex) {
              uploadInstance.tableFilesModel.removeRow(index);
            }
          });
          playerModel.isLoading = true;
        };

        this.table.on('tableRowDelete', function(evt, tableModel, rowIndex, removedRow) {
          if(removedRow.length > 0 && removedRow[0][0].value) {
            let fileID = me.getFileID(removedRow[0][0].value);
            if (fileID && fileID != '' && fileID != undefined && fileID != 'undefined') {
              jQuery.when(api.simpleAsyncGet("rest/api/docflow/doc/attachment/remove?fileUUID=" + fileID)).then(function(result){
                var errorCode = result.errorCode || result.result;
                if(errorCode == '0') me.addHistoryData(removedRow[0][0].label, true);
              });
            } // else {
            //   throw new Error("cannot find file ::: fileID = " + fileID);
            // }
          }
        });

        // Проверка расширения файла на допустимость аплоада
        this.validExt = function(fileName){
          if (!model.fileFormat) return true;
          var format = fileName.substring(fileName.lastIndexOf(".")+1).toLowerCase();
          if(~model.fileFormat.toLowerCase().split(';').indexOf(format)) return true;
          return false;
        };

        // Старт загрузки файла
        this.startSingleUpload = function(file){
            let uploader = new BFileUploader(file, me.onFileUploaded, me.onUploadCancel);
            uploader.start();
        };

        // Старт загрузки массива файлов
        this.uploadFiles = function(files){
            let invalid = '';
            for(let i=0; i < files.length; i++) {
                let file = files[i];
                let fileName = file.name;
                if(!me.validExt(fileName)) {
                    invalid += (fileName + '; ');
                }  else {
                    me.startSingleUpload(file);
                }
            }
        };

        // Обработчик выбора файлов в input
        this.input.on("change", function(evt){
            me.uploadFiles(evt.target.files);
        });

        // Обработчик drop-а файлов в области загрузки (HTML5)
        this.drop[0].ondrop = function(evt) {
            evt.preventDefault();
            me.uploadFiles(evt.dataTransfer.files);
        };

    }
    let uploader = new player.Uploader(player);
});
