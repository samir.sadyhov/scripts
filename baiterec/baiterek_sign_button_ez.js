(function(){

  if (window.location.href.indexOf('Synergy') == -1) return;

  var api = AS.FORMS.ApiUtils;
  var my_requests = "podpis_etsp";
  var appNumber = model.playerModel.getModelWithId("app_num").getValue();

  if (AS.OPTIONS.locale == 'en') {
    var buttonName = 'Check sign';
  } else if (AS.OPTIONS.locale == 'ru') {
    var buttonName = 'Проверка подписи';
  } else {
    var buttonName = 'Қолтаңбаны тексеру';
  }

  var button = jQuery('<div>')
  .css({
    "width": "200px",
    "font-weight": "bold",
    "background-color": "#49b785",
    "border-color": "#49b785",
    "color": "#ffffff",
    "min-width": "150px",
    "height": "30px",
    "line-height": "30px",
    "border": "1px solid",
    "border-radius": "6px",
    "overflow": "hidden",
    "text-align": "center",
    "white-space": "nowrap",
    "display": "inline-block",
    "cursor": "pointer"
  })
  .hover(function() {
    $(this).css("background-color", "#55cc95");
  }, function() {
    $(this).css("background-color", "#49b785");
  })
  .html(buttonName)
  .click(function(){
    showInnerSigns();
  });

  view.container.append(button);

  var fail = function (errMsg, userMsg) {
    AS.SERVICES.hideWaitWindow();
    if(userMsg){
      AS.SERVICES.showErrorMessage(userMsg);
    } else {
      AS.SERVICES.showErrorMessage(i18n.tr('Ошибка проверки ЭЦП'));
    }
    console.log(errMsg);
  };

  var showSigns = function(signinfo){
    try {
      var dateSign = AS.FORMS.DateUtils.formatDate(new Date(signinfo.dateSign*1), '${dd}.${mm}.${yyyy} ${HH}:${MM}:${SS}');
      var fromDate = AS.FORMS.DateUtils.formatDate(new Date(signinfo.fromDate*1), '${dd}.${mm}.${yyyy} ${HH}:${MM}:${SS}');
      var toDate = AS.FORMS.DateUtils.formatDate(new Date(signinfo.toDate*1), '${dd}.${mm}.${yyyy} ${HH}:${MM}:${SS}');
      var allInfoCert = JSON.parse(signinfo.allInfoCert);

      var table = jQuery("<table class='asf-table' border = '1' style = 'margin: 5px'></table>");
      var headerRow = jQuery("<thead>");
      table.prepend(headerRow);
      headerRow.append(jQuery("<th style='text-align:center; padding: 6px'>" + i18n.tr("Ключ") + "</th>"));
      headerRow.append(jQuery("<th style='text-align:center; padding: 6px'>" + i18n.tr("Значение") + "</th>"));

      var tableBody = jQuery("<tbody>");

      tableBody.append(jQuery("<tr>")
      .append(jQuery("<td align='right' style='width: 50%; margin: 2px; padding: 6px; font-weight: bold'>").text(i18n.tr("Дата подписи")))
      .append(jQuery("<td style='padding: 6px'>").text(dateSign)));

      tableBody.append(jQuery("<tr>")
      .append(jQuery("<td align='right' style='width: 50%; margin: 2px; padding: 6px; font-weight: bold'>").text(i18n.tr("Срок действия сертификата")))
      .append(jQuery("<td style='padding: 6px'>").text(fromDate + " - " + toDate)));

      for (key in allInfoCert) {
        if (key != "CERT") {
          var keyTd = jQuery("<td align='right' style='width: 50%; margin: 2px; padding: 6px; font-weight: bold'>").text(key.replace(/KEY_/g,""));
          var valueTd = jQuery("<td style='padding: 6px'>").text(allInfoCert[key]);
          tableBody.append(jQuery("<tr>").append(keyTd).append(valueTd));
        }
      }

      table.append(tableBody);

      jQuery("body").append(table);
      jQuery(table).dialog({
          modal: true,
          width: 600,
          height: 400,
          resizable: false,
          title: i18n.tr("Проверка подписи"),
          show: {
            effect: 'fade',
            duration: 500
          },
          hide: {
            effect: 'fade',
            duration: 500
          },
          buttons: {'OK': function(){
            $(this).dialog("close");
          }},
          close: function() {
            $(this).remove();
          }
      });

      AS.SERVICES.hideWaitWindow();
    } catch (e) {
      fail(e);
    }
  };

  var showInnerSigns = function() {
    AS.SERVICES.showWaitWindow();

    try {
      let url = 'rest/api/registry/data_ext?registryCode=' + encodeURIComponent(my_requests) + '&condition=TEXT_EQUALS&field=app_num&value=' + encodeURIComponent(appNumber);

      api.simpleAsyncGet(url, function (res) {
        if (!res || !res.result || !res.result[0] || !res.result[0].documentID) {
          fail(res, 'Не найдено подписи');
          return;
        }

        var signDocumentID = res.result[0].documentID;
        var signDataUUID = res.result[0].dataUUID;

        api.simpleAsyncGet('rest/api/formPlayer/getSigns?type=-1&dataUUID=' + signDataUUID, function (signinfo) {
          if (!signinfo || signinfo.length === 0) {
            fail(signinfo, 'Не найдено подписи');
            return;
          }

          api.simpleAsyncPost('rest/sign/testSigns', function(result){
            showSigns(result);
          }, undefined, {"keyID": signinfo[signinfo.length - 1].certID, "docID": signDocumentID}, "application/x-www-form-urlencoded; charset=utf-8", function (e) {
            fail(e);
          });

        }, undefined, undefined, function (e) {
          fail(e);
        });

      }, undefined, undefined, function (e) {
        fail(e);
      });
    } catch (e) {
      fail(e);
    }
  };
}());
