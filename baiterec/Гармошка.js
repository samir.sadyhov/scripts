<style>
.ui-accordion-header {
    margin: 0 !important;
    padding: 10px 15px !important;
    border-top-left-radius: 3px;
    border-top-right-radius: 3px;
    color: #333;
    background-color: #f5f5f5;
    border-bottom: 1px solid #ddd;
    font-size: 14px;
    font-family: Tahoma, sans-serif;
    font-weight: bolder;
}

.ui-accordion-header-active {
    color: white;
    background-color: #5ED35C;
     border: none;
}

.ui-accordion-content {
    /*border: 1px solid #e0e0e0;*/
    border-top: none;
}

  #accordion_steps h3 {
      padding: 5px 25px;
  }

  .panel-collapse {
      padding: 10px;
  }
  #accordion_steps.ui-accordion .ui-accordion-content {
    padding: 10px !important;
}

#accordion_steps .asf-container {
    width: calc(100% - 22px) !important;
}
.closeActions {
    text-align: right;
    padding: 0;
    cursor: pointer;
}

#closeActions a {
    color: red !important;
    height: 24px !important;
    width: 24px !important;
    }

.hidden {
    display: none !important;
}
.arta-panel {
    border: 1px solid #e0e0e0;
    border-top: none;
}

.arta-panel-header {
    color: #333;
    background-color: #f5f5f5;
    border-bottom: 1px solid #ddd;
    border-top: 1px solid #ddd;
}

.arta-active {
}

.arta-active:after {
    content: "\2796" !important;

}

.arta-panel-header:after {
    content: '\02795';
    float: right;
    margin-left: 5px;
    font-size: 10px;
    font-weight: lighter;
}
.arta-panel-header {
   display: block;
   width: 100%;
   padding: 8px 15px;
    font-size: 13px;
    font-family: Tahoma, sans-serif;
    font-weight: bold;
    color: #333;
    border-left: none;
    border-right: none;
    cursor: pointer
  }

.arta-panel-collapse {
    background-color: white;
    /*max-height: 0;*/
    height: auto;
    overflow: hidden;
    transition: height 0.2s ease-out;
    padding: 10px !important;
}

.arta-collapse-active {
    display: block;
    padding: 10px !important;
    /*height: auto;*/
}
.arta-collapse-hide {
    display: none;
    padding: 0 10px !important;
    /*height: 0;*/
}

.arta-collapsing {
  height: 0;
  overflow: hidden;
  transition: height 0.35s ease;
}

</style>

<div id='closeActions' class ='closeActions'>
    <a id="openAll" ><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M4 15h16v-2H4v2zm0 4h16v-2H4v2zm0-8h16V9H4v2zm0-6v2h16V5H4z"/></svg></a>
    <a id="closeAll"><svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"><path d="M4 18h17v-6H4v6zM4 5v6h17V5H4z"/></svg></a>
    </div>
<div id="accordion_steps" class="arta-panel">
</div>


$( function() {
//   debugger;
  let acc = $("#accordion_steps" );
  if (acc.length === 0) return;
  let collapsible = true;

  if (model.steps !== undefined && model.steps.length > 0) {
        for (let i = 0; i < model.steps.length; i++) {
            let step = model.steps[i];
            let title;
            let span = document.createElement("span");
            if (step.id) {
                span.id = step.id;
            }
            // span.classList.add('panel-default');
            span = $(span)
            span.appendTo(acc);

            if (step.title) {
                title = step.title
            } else if (step.titleId) {
                title = $('body').find('[data-asformid="label.label.' + step.titleId +'"]');
            }
            if (title.length > 0) {
                title.detach();
                span.append('<button class="arta-panel-header arta-active" target="#collapse'+i +'">' + title.text() + '</button>');
            } else {
                span.append('<button class="arta-panel-header arta-active" target="#collapse'+i +'"> Не найден заголовок ' + step.title +'</button>');
            }

            let containers = [];
            if (Array.isArray(step.container)) {
                containers = step.container;
                // debugger
            } else {
               containers.push(step.container);
            }

            let panel = document.createElement("div");
            panel.id = 'collapse'+ i;
            panel.classList.add('arta-panel-collapse');
            panel.classList.add('arta-collapse-active');
            panel = $(panel);
            for (let idx = 0; idx < containers.length; idx++) {
               let content = $('body').find('[data-asformid="table.container.' + containers[idx] +'"]');
               if (content.length > 0) {
                    let c = content.detach();
                    panel.append(c)
               }
            }
            panel.appendTo(span)
        }
    }

    var collapser = function(panelHeader) {
            panelHeader.classList.toggle("arta-active");
            var panel = panelHeader.nextElementSibling;
            panel.classList.toggle("arta-collapse-hide");
            panel.classList.toggle("arta-collapse-active");
    };

  var switcher = function(isCollapse) {
    var tmp = document.getElementsByClassName("arta-panel-header");
    for (let i = 0; i < tmp.length; i++) {
        let panel = tmp[i].nextElementSibling;
        if (isCollapse) {
            if (panel.className.indexOf('arta-collapse-hide') === -1) {
                tmp[i].classList.toggle("arta-active");
                panel.classList.toggle("arta-collapse-hide");
                panel.classList.toggle("arta-collapse-active");
            }
        } else {
            if (panel.className.indexOf('arta-collapse-active') === -1) {
                tmp[i].classList.toggle("arta-active");
                panel.classList.toggle("arta-collapse-hide");
                panel.classList.toggle("arta-collapse-active");
            }
        }
    }
  };
  if (collapsible) {
      $('#closeAll').click(function () {
        switcher(false);
      })
      $('#openAll').click(function () {
        switcher(true);
      })

    var panels = document.getElementsByClassName("arta-panel-header");
    var i;
    for (i = 0; i < panels.length; i++) {
      panels[i].addEventListener("click", function() {
        collapser(this);
      });
      if (!panels[i].style.maxHeight) {
        var p = panels[i].nextElementSibling;
      }

    }

  } else {
      acc.accordion({
        heightStyle: "content",
        collapsible: true
      });
  }

});
