// model.processCode = "voting";
// model.userIDs = ["437711d7-d5f8-417d-a933-af07196a8558", "1"];
// model.buttonName = {ru: 'Прервать голосование', kk: 'Дауыс беруді үзу', en: 'Interrupt the voting'};

(function(){
  'use strict';

  if (window.location.href.indexOf('Synergy') === -1) return;
  if (model.userIDs.indexOf(AS.OPTIONS.currentUser.userId) === -1) return;

  var api = AS.FORMS.ApiUtils;

  AS.SERVICES.showMessage = function(message) {
    window.showMessage(message);
  };

  var button = jQuery('<div>')
  .css({
    "width": "200px",
    "font-weight": "bold",
    "background-color": "#49b785",
    "border-color": "#49b785",
    "color": "#ffffff",
    "min-width": "150px",
    "height": "30px",
    "line-height": "30px",
    "border": "1px solid",
    "border-radius": "6px",
    "overflow": "hidden",
    "text-align": "center",
    "white-space": "nowrap",
    "display": "inline-block",
    "cursor": "pointer"
  })
  .hover(function() {
    $(this).css("background-color", "#55cc95");
  }, function() {
    $(this).css("background-color", "#49b785");
  })
  .html(model.buttonName[AS.OPTIONS.locale])
  .click(function(){
    showDialog();
  });

  view.container.append(button);

  var fail = function (errMsg, userMsg) {
    AS.SERVICES.hideWaitWindow();
    if(userMsg){
      AS.SERVICES.showErrorMessage(userMsg);
    } else {
      AS.SERVICES.showErrorMessage(i18n.tr('Во время прерывания голосования произошла ошибка. Обратитесь к администратору.'));
    }
    console.error(errMsg);
  };

  var showDialog = function(){
    var dialog = $('<div>')
    .append('<textarea id="comment" rows="4" cols="50" name="comment" placeholder="Введите комментарий..."></textarea>')
    .dialog({
      modal: true,
      width: 390,
      height: 200,
      resizable: false,
      title: model.buttonName[AS.OPTIONS.locale],
      show: {
        effect: 'fade',
        duration: 500
      },
      hide: {
        effect: 'fade',
        duration: 500
      },
      buttons: {
        'Прервать': function(){
          let c = $(this).children().first().val();
          $(this).dialog("close");
          stopProcess(c);
        },
        'Отмена': function(){
          $(this).dialog("close");
        }
      },
      close: function() {
        $(this).remove();
      }
    });
  };

  var getActionIds = function (processes, code) {
    var result = [];

    function search(p) {
      p.forEach(function(process) {
        if (process.code == code && !process.finished) {
          result.push(process.actionID);
        }
        if (process.subProcesses.length > 0) search(process.subProcesses);
      });
    }
    search(processes);
    return result;
  };

  var checkStop = function(){
    try {
      let label_stopProcess = model.playerModel.getModelWithId('label_stopProcess');
      if(label_stopProcess) {
        label_stopProcess.setValue('2');
        jQuery.when(api.saveAsfData(model.playerModel.getAsfData().data, model.playerModel.formId, model.playerModel.asfDataId)).then(function (res) {
          console.log(res);
        }, function (err) {
          console.log(err);
        });
      }
    } catch (e) {
      console.log(e);
    }
  }

  var stopProcess = function(comment){
    AS.SERVICES.showWaitWindow();
    try {
      api.getDocumentIdentifier(model.playerModel.asfDataId, function(documentID){

        api.simpleAsyncGet("rest/api/workflow/get_execution_process?documentID=" + documentID, function(processes){
            if(processes.length === 0) {
              fail(processes, 'Нет запущенных процессов по документу');
              return;
            }

            var actionIds = getActionIds(processes, model.processCode);

            if (actionIds.length === 0) {
              fail(actionIds, 'Не найдено работ для завершения');
              return;
            }

            checkStop();

            actionIds.forEach(function(actionId){
              var settings = {
                  "async": false,
                  "crossDomain": true,
                  "url": api.getFullUrl("rest/api/workflow/work/set_result"),
                  "method": "POST",
                  "beforeSend": api.addAuthHeader,
                  "headers": {
                      "Content-Type": "application/x-www-form-urlencoded; charset=utf-8"
                  },
                  "data": {
                      "workID": actionId,
                      "completionForm": "COMMENT",
                      "comment": comment
                  }
              };
              $.ajax(settings).done(function (result) {
                  if (result.errorCode == 13) {
                      fail(result, result.errorMessage);
                  } else {
                      AS.SERVICES.showMessage('Работа <b>' + result.work.name + '</b> завершена');
                      console.log(result);
                  }
              });
            });

            AS.SERVICES.hideWaitWindow();

        }, undefined, undefined, function (e) {
          fail(e);
        });

      });

    } catch (e) {
      fail(e);
    }

  };

}());
