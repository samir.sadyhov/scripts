(function(){
  'use strict';

  if (window.location.href.indexOf('Synergy') === -1) return;
  if (['b9de3578-b585-46ab-bc1c-0730f1648eba', '1'].indexOf(AS.OPTIONS.currentUser.userId) !== -1) return;

  let api = AS.FORMS.ApiUtils;

  jQuery.when(api.simpleAsyncGet(`rest/api/groups/find?search=${encodeURIComponent('Клиенты')}`))
  .then(res => {
    api.simpleAsyncGet(`rest/api/filecabinet/user/${AS.OPTIONS.currentUser.userId}?getGroups=true`, user => {
      console.log({'group': res.array[0], 'user': user});

      if(user.groups.find(x => x.groupID == res.array[0].groupID)) {
        window.location.href = 'https://digital.baiterek.gov.kz/login';
      }

    });
  }).fail(error => {
    console.error(error);
  });

}());
