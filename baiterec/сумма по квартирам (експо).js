model.playerModel.on(AS.FORMS.EVENT_TYPE.dataLoad, function() {
  var tableModel = model.playerModel.getModelWithId('apartment_info');

  function sumCost(){
    var costValue = 0;
    tableModel.modelBlocks.forEach(function(modelBlock){
      var tableBlockIndex = modelBlock[0].asfProperty.tableBlockIndex;
      var tmpValue = model.playerModel.getModelWithId('flat_cost', tableModel.asfProperty.id, tableBlockIndex);

      if(tmpValue) {
        tmpValue = parseFloat(tmpValue.getValue());
        if(isNaN(tmpValue)) tmpValue = 0;
        costValue+=tmpValue;
      }
    });
    model.setValue(costValue + '');
  }

  tableModel.modelBlocks.forEach(function(modelBlock){
    var tableBlockIndex = modelBlock[0].asfProperty.tableBlockIndex;
    model.playerModel.getModelWithId('flat_cost', tableModel.asfProperty.id, tableBlockIndex).on("valueChange", function(evt, cModel, value) {
      sumCost();
    });
  });
  tableModel.on('tableRowAdd', function(evt, modelTab, modelRow) {
    var tableBlockIndex = modelRow[0].asfProperty.tableBlockIndex;
    model.playerModel.getModelWithId('flat_cost', tableModel.asfProperty.id, tableBlockIndex).on("valueChange", function(evt, cModel, value) {
      sumCost();
    });
    sumCost();
  });
  tableModel.on('tableRowDelete', function(evt, rowModel, rowBlock, removedRow) {
    sumCost();
  });
  sumCost();
});
