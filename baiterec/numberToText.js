function numberToText(c) {
  function k(b, c) {
    var d = c[0],
      e = c[1],
      f = c[2];
    return b % 10 == 1 && b % 100 != 11 ? d : b % 10 >= 2 && b % 10 <= 4 && (b % 100 < 10 || b % 100 >= 20) ? e : f;
  }
  for (var d = {
      0: {
        1: "один",
        2: "два",
        3: "три",
        4: "четыре",
        5: "пять",
        6: "шесть",
        7: "семь",
        8: "восемь",
        9: "девять",
        10: "десять",
        11: "одиннадцать",
        12: "двенадцать",
        13: "тринадцать",
        14: "четырнадцать",
        15: "пятнадцать",
        16: "шестнадцать",
        17: "семнадцать",
        18: "восемнадцать",
        19: "девятнадцать",
        20: "двадцать",
        30: "тридцать",
        40: "сорок",
        50: "пятьдесят",
        60: "шестьдесят",
        70: "семьдесят",
        80: "восемьдесят",
        90: "девяносто",
        100: "сто",
        200: "двести",
        300: "триста",
        400: "четыреста",
        500: "пятьсот",
        600: "шестьсот",
        700: "семьсот",
        800: "восемьсот",
        900: "девятьсот"
      },
      1: {
        1: "одна",
        2: "две"
      }
    }, i = {
      0: ["", "", ""],
      1: ["тысяча", "тысячи", "тысяч"],
      2: ["миллион", "миллиона", "миллионов"],
      3: ["миллиард", "миллиарда", "миллиардов"],
      4: ["триллион", "триллиона", "триллионов"],
      5: ["квадриллион", "квадриллиона", "квадриллионов"],
      6: ["квинтиллион", "квинтиллиона", "квинтиллионов"]
    }, h = "", j = (("" + c).match(/(\d{1,3})(?=((\d{3})*([^\d]|$)))/g) || []).reverse(), e = 0; e < j.length; e++) {
    for (var f = d[e], c = j[e], b = "", g = 0; g < c.length; g++)
      if (a = c.substr(g), f && f[a] || d[0][a]) {
        b = b + " " + (f && f[a] || d[0][a]);
        break;
      } else a = +c.substr(g, 1) * Math.pow(10, a.length - 1), +a in d[0] && (b = b + " " + d[0][a]);
    b && (b = b + " " + k(+c, i[e] || i[0]));
    h = b + h
  }
  return h || ""
}

function init(){
  try {
    var numberInput = model.playerModel.getModelWithId(model.conf.numberInput);
    if(numberInput) {
      let textOutInt = model.playerModel.getModelWithId(model.conf.textOutInt);
      let textOutFract = null;
      if(model.conf.textOutFract) textOutFract = model.playerModel.getModelWithId(model.conf.textOutFract);

      function reverseString(str) {
        return (str === '') ? '' : reverseString(str.substr(1)) + str.charAt(0);
      }

      let setResult = function(value){
        value = parseFloat(value).toFixed(2) + '';
        value = value.split('.') || value.split(',');
        if(value.length > 1) {
          let fract = reverseString(reverseString(value[1])*1 + '');
          fract = fract == '0' ? '' : fract;
          if (fract.length === 1) fract = fract + '0';
          console.log(fract);
          if (textOutFract) {
            textOutInt.setValue(numberToText(value[0] == 0 ? '' : value[0]));
            textOutFract.setValue(numberToText(fract));
          } else {
            textOutInt.setValue(numberToText(value[0] == 0 ? '' : value[0]) + ', ' + numberToText(fract));
          }
        } else {
          textOutInt.setValue(numberToText(value[0] == 0 ? '' : value[0]));
        }
      };

      if(numberInput.getValue()) setResult(numberInput.getValue());
      numberInput.on("valueChange", function(evt, thisModel, value) {
        setResult(value);
      });
    }
  } catch (e) {
    console.log(e);
  }
}
init();
model.playerModel.on(AS.FORMS.EVENT_TYPE.dataLoad, init);

//настройки на форме
/*
model.conf = {
  numberInput: 'summa_k_oplate1',
  textOutInt: 'summa_propisiyu1'
  textOutFract: 'summa_propisiyu2'
};
*/
