#!/bin/bash

mysqlUser="root"
mysqlPass="root"
mysqlDB="arta"
mysqlHost="127.0.0.1"

# вспомогательные настройки
logFile="./expo.log"
scriptName=${0##*/}

#parking OR flat
type=$1

#список квартир
FILE=$2

# Функция получения текущего времени
function now_time () {
  date +"%Y-%m-%d %H:%M:%S"
}
# Функция логирования
function logging () {
  echo -e "`now_time` #$1# [$2] $3" >> $logFile
}

# проверка статуса квартиры
# $1 flat_id
function getStatusFlat () {
  local tmp=$(mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use $mysqlDB; set names utf8; SELECT status FROM flats WHERE id = $1;" 2>/dev/null)
  echo `echo $tmp | cut -d " " -f2`
}
# проверка статуса парковки
# $1 parking_id
function getStatusParking () {
  local tmp=$(mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use $mysqlDB; set names utf8; SELECT status FROM parkings WHERE id = $1;" 2>/dev/null)
  echo `echo $tmp | cut -d " " -f2`
}

# смена статуса квартиры
# $1 flat_id
# $2 status
function changeStatusFlat () {
  local tmp=$(mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use $mysqlDB; UPDATE flats SET status = $2 WHERE id = $1;" 2>/dev/null)
  echo `echo $tmp | cut -d " " -f2`
}
# смена статуса парковки
# $1 parking_id
# $2 status
function changeStatusParking () {
  local tmp=$(mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use $mysqlDB; UPDATE parkings SET status = $2 WHERE id = $1;" 2>/dev/null)
  echo `echo $tmp | cut -d " " -f2`
}

# удаление квартиры из резерва
# $1 flat_id
function deleteFlatReserv () {
  local tmp=$(mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use $mysqlDB; DELETE FROM flat_reserves WHERE flat_id = $1;" 2>/dev/null)
  echo `echo $tmp | cut -d " " -f2`
}
# удаление паркинга из резерва
# $1 parking_id
function deleteParkingReserv () {
  local tmp=$(mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use $mysqlDB; DELETE FROM parking_reserves WHERE parking_id = $1;" 2>/dev/null)
  echo `echo $tmp | cut -d " " -f2`
}

echo '' >> $logFile
logging $scriptName START "Начало работы скрипта"

if [[ -f $FILE && -s $FILE ]]; then
  if [[ $type == flat ]]; then

    while IFS=: read -r flat_id;
    do
      status=$(getStatusFlat $flat_id)
      echo '' >> $logFile
      logging $scriptName INFO "flat_id: $flat_id || status: $status"
      if [ $status != 0 ]; then
        logging $scriptName changeStatusFlat "OK $(changeStatusFlat $flat_id 0)"
        logging $scriptName deleteFlatReserv "OK $(deleteFlatReserv $flat_id)"
      else
        logging $scriptName INFO "пропускаем"
      fi
    done < $FILE

  elif [[ $type == parking ]]; then

    while IFS=: read -r parking_id;
    do
      status=$(getStatusParking $parking_id)
      echo '' >> $logFile
      logging $scriptName INFO "parking_id: $parking_id || status: $status"
      if [ $status != 0 ]; then
        logging $scriptName changeStatusParking "OK $(changeStatusParking $parking_id 0)"
        logging $scriptName deleteParkingReserv "OK $(deleteParkingReserv $parking_id)"
      else
        logging $scriptName INFO "пропускаем"
      fi
    done < $FILE

  else
    echo '' >> $logFile
    logging $scriptName ERROR "передан не верный параметр type: $type || доступно только: flat или parking"
    exit 13
  fi
else
  echo '' >> $logFile
  logging $scriptName ERROR "не передан список"
  exit 13
fi


echo '' >> $logFile
logging $scriptName END "Завершение работы скрипта"
exit 0
