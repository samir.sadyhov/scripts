#!/bin/bash

source $(dirname $(readlink -e $0))/utils.class
scriptName=${0##*/}
tmpsql=$(mktemp)

indexName='natr_grant_v2'
indexType='ng'

echo '' >> $logFile
logging $scriptName START "Начало работы скрипта"
logging $scriptName INFO "Собираем данные для индекса $indexName"


mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8;
SELECT asfDataID, usluga, sendDate, statusKey, statusValue, addStatusKey, addStatusValue,
IFNULL(regionName, 'Акмолинская область') regionName, IFNULL(regionKey, 1) regionKey,
IFNULL(ST_GeoHash(SUBSTRING_INDEX(regionPoint, ',', -1), SUBSTRING_INDEX(regionPoint, ',', 1), 12), 'v93r55ztchws') regionGeoHash

FROM (
SELECT rg.asfDataID, natr.usluga, DATE(a.cmp_key) sendDate,
b.cmp_data statusValue, b.cmp_key statusKey,
c.cmp_data addStatusValue, c.cmp_key*1 addStatusKey,
d.cmp_data regionName, d.cmp_key regionKey, getRegionGeoPoint(d.cmp_key) regionPoint

FROM (
SELECT 'natr_grant_na_kommertsializatsiu_fl_v2' code, 'Грант на коммерциализацию ФЛ' usluga
UNION SELECT 'natr_grant_na_kommertsializatsiu_ul_v2', 'Грант на коммерциализацию ЮЛ'
UNION SELECT 'natr_grant_na_razvitie_otraslei_v2', 'Грант на развитие отраслей'
UNION SELECT 'natr_grant_na_razvitie_predpriyatii_v2', 'Грант на развитие предприятий'
) natr

LEFT JOIN registries r ON r.code = natr.code
JOIN registry_documents rg ON rg.registryID = r.registryID
LEFT JOIN asf_data ad ON ad.uuid = rg.asfDataID
JOIN asf_data_index a ON a.uuid = rg.asfDataID AND a.cmp_id = 'send_date'
LEFT JOIN asf_data_index b ON b.uuid = rg.asfDataID AND b.cmp_id = 'refuse_desicion_status'
LEFT JOIN asf_data_index c ON c.uuid = rg.asfDataID AND c.cmp_id = 'add_status'
LEFT JOIN asf_data_index d ON d.uuid = rg.asfDataID AND d.cmp_id = 'cmp-psxi49'

WHERE rg.deleted IS NULL AND b.cmp_key != 0
AND DATE(ad.modified) = DATE(NOW())
) t2;" > $tmpsql 2>/dev/null


if [ -s $tmpsql ];
then
  # Количество строк в файле tmpsql
  countStr=`cat $tmpsql | wc -l`

  for i in `seq 2 $countStr`;
  do
    asfDataID=`cat $tmpsql | sed -n $i'p' | cut -f1`
    usluga=`cat $tmpsql | sed -n $i'p' | cut -f2`
    sendDate=`cat $tmpsql | sed -n $i'p' | cut -f3`
    statusKey=`cat $tmpsql | sed -n $i'p' | cut -f4`
    statusValue=`cat $tmpsql | sed -n $i'p' | cut -f5`
    addStatusKey=`cat $tmpsql | sed -n $i'p' | cut -f6`
    addStatusValue=`cat $tmpsql | sed -n $i'p' | cut -f7`
    regionName=`cat $tmpsql | sed -n $i'p' | cut -f8`
    regionKey=`cat $tmpsql | sed -n $i'p' | cut -f9`
    regionGeoHash=`cat $tmpsql | sed -n $i'p' | cut -f10`

    echo '' >> $logFile
    logging $scriptName INFO "asfDataID: $asfDataID"

    iData='{"usluga": "'$usluga'", "sendDate": "'$sendDate'",'
    iData+='"statusKey": '$statusKey', "statusValue": "'$statusValue'",'
    iData+='"addStatusKey": '$addStatusKey', "addStatusValue": "'$addStatusValue'",'
    iData+='"regionName": "'$regionName'", "regionKey": "'$regionKey'", "regionGeoHash": "'$regionGeoHash'"}'
    logging $scriptName RESULT "iData: $iData"

    elasticKey=$indexName"-"$asfDataID
    logging $scriptName RESULT "elasticKey: $elasticKey"

    logging $scriptName INFO "Обновление индекса"
    logging $scriptName INFO "$elasticHost/$indexName/$indexType/$elasticKey"
    addIndexData "$elasticHost/$indexName/$indexType/$elasticKey" "$iData"
    logging $scriptName RESULT "`cat $tmpfile`"

  done
else
  logging $scriptName INFO "нет новых данных"
fi

echo '' >> $logFile
logging $scriptName STATUS "Удаление временных файлов"
rm -rf $tmpsql
rm -rf $tmpfile
logging $scriptName END "Завершение работы скрипта"
exit 0
