#!/bin/bash

source $(dirname $(readlink -e $0))/utils.class
scriptName=${0##*/}
tmpsql="/tmp/holding_stat.sql"

indexName='holding_stat'
indexType='hs'

echo '' >> $logFile
logging $scriptName START "Начало работы скрипта"


indexName1=$indexName"_1"
logging $scriptName INFO "Собираем данные для индекса $indexName1"

registryID=$(getRegistryId 'nuh_zayavitel')

logging $scriptName INFO "НУХ Заявитель [$registryID]"

mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8;
SELECT rg.asfDataID FROM registry_documents rg
LEFT JOIN asf_data ad ON ad.uuid=rg.asfDataID
WHERE rg.deleted IS NULL AND rg.registryID='$registryID'
AND DATE(ad.modified) = DATE(NOW())
ORDER BY DATE(ad.modified) DESC;" > $tmpsql

if [ -s $tmpsql ];
then
  # Количество строк в файле tmpsql
  countStr=`cat $tmpsql | wc -l`

  for i in `seq 2 $countStr`;
  do
    tmpuuid=`cat $tmpsql | sed -n $i'p' | cut -f1`
    echo '' >> $logFile
    logging $scriptName INFO "asfDataID: $tmpuuid"

    getAsfData "$tmpuuid"
    jsonString=`cat $tmpfile`
    asfData=$(getJsonObject "$jsonString" "data")

    tip_spisokObj=$(searchInJsonArr "$asfData" "tip_spisok")
    tip_spisokKey=$(getJsonObject "$tip_spisokObj" "key")

    if [[ ("$tip_spisokKey" != "null") && ("$tip_spisokKey" == "\"1\"") ]]; then
      data_ulObj=$(searchInJsonArr "$asfData" "data_ul")
      data_ulKey=$(getJsonObject "$data_ulObj" "key")

      if [ "$data_ulKey" != "null" ]; then
        data_ulKey=$(echo $data_ulKey | cut -c 2- | rev | cut -c 2- | rev )
        ulUUID=$(getDataUUID $data_ulKey)
        getAsfData "$ulUUID"
        ulData=`cat $tmpfile`
        ulData=$(getJsonObject "$ulData" "data")

        stranaObj=$(searchInJsonArr "$ulData" "strana")
        stranaValue=$(getJsonObject "$stranaObj" "value")
        to_pathObj=$(searchInJsonArr "$ulData" "to_path")
        to_pathValue=$(getJsonObject "$to_pathObj" "value")

        if [ "$stranaValue" != "null" ]; then
          stranaValue=$(echo $stranaValue | cut -c 2- | rev | cut -c 2- | rev )
        else
          stranaValue=""
        fi
        if [ "$to_pathValue" != "null" ]; then
          to_pathValue=$(echo $to_pathValue | cut -c 2- | rev | cut -c 2- | rev )
        else
          to_pathValue=""
        fi
        if [ "$stranaValue" == "004" ]; then
          stranaValue="Афганистан"
        fi

        iData='{"strana": "'$stranaValue'", "to_path": "'$to_pathValue'"}'
        logging $scriptName RESULT "iData: $iData"

        elasticKey=$indexName"-1-"$tmpuuid
        logging $scriptName RESULT "elasticKey: $elasticKey"

        logging $scriptName INFO "Обновление индекса"
        logging $scriptName INFO "$elasticHost/$indexName1/$indexType/$elasticKey"
        addIndexData "$elasticHost/$indexName1/$indexType/$elasticKey" "$iData"
        logging $scriptName RESULT "`cat $tmpfile`"
      fi
    fi

  done
else
  logging $scriptName INFO "нет новых данных в реестрах"
fi


indexName2=$indexName"_2"
echo '' >> $logFile
logging $scriptName INFO "Собираем данные для индекса $indexName2"

registryID=$(getRegistryId 'nuh_pul_proektov__chastnye_dannye_o_proekte')

logging $scriptName INFO "НУХ Пул проектов. Частные данные о проекте [$registryID]"

mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8;
SELECT rg.asfDataID FROM registry_documents rg
LEFT JOIN asf_data ad ON ad.uuid=rg.asfDataID
WHERE rg.deleted IS NULL AND rg.registryID='$registryID'
AND DATE(ad.modified) = DATE(NOW())
ORDER BY DATE(ad.modified) DESC;" > $tmpsql

if [ -s $tmpsql ];
then
  # Количество строк в файле tmpsql
  countStr=`cat $tmpsql | wc -l`

  for i in `seq 2 $countStr`;
  do
    tmpuuid=`cat $tmpsql | sed -n $i'p' | cut -f1`
    echo '' >> $logFile
    logging $scriptName INFO "asfDataID: $tmpuuid"

    getAsfData "$tmpuuid"
    jsonString=`cat $tmpfile`
    asfData=$(getJsonObject "$jsonString" "data")

    table=$(searchInJsonArr "$asfData" "supportive_measures_table")
    tableData=$(getJsonObject "$table" "data")
    tabLength=$(getRowsCount "$tableData" "supportive_measures") #кол-во строк в дин.таблице

    tmpDataSM="["
    tmpDataMA="["
    prov=false

    for ((j=1; j<$tabLength; j++))
    do
      supportive_measuresObj=$(searchInJsonArr "$tableData" "supportive_measures-b$j")
      smId=$(getJsonObject "$supportive_measuresObj" "id")
      smKey=$(getJsonObject "$supportive_measuresObj" "key")
      smValue=$(getJsonObject "$supportive_measuresObj" "value")

      smId=$(echo $smId | cut -c 2- | rev | cut -c 2- | rev )
      smKey=$(echo $smKey | cut -c 2- | rev | cut -c 2- | rev )
      smValue=$(echo $smValue | cut -c 2- | rev | cut -c 2- | rev )

      if [ "$smValue" != "ul" ]; then
        prov=true
        tmpDataSM+='{"id": "'$smId'", "key": "'$smKey'", "value": "'$smValue'"},'
      fi

      measures_addObj=$(searchInJsonArr "$tableData" "measures_add-b$j")
      maId=$(getJsonObject "$measures_addObj" "id")
      maKey=$(getJsonObject "$measures_addObj" "key")
      maValue=$(getJsonObject "$measures_addObj" "value")

      maId=$(echo $maId | cut -c 2- | rev | cut -c 2- | rev )
      maKey=$(echo $maKey | cut -c 2- | rev | cut -c 2- | rev )
      maValue=$(echo $maValue | cut -c 2- | rev | cut -c 2- | rev )

      if [ "$maValue" != "ul" ]; then
        prov=true
        tmpDataMA+='{"id": "'$maId'", "key": "'$maKey'", "value": "'$maValue'"},'
      fi


    done

    if [ $prov == true ]; then
      tmpDataSM=$(echo $tmpDataSM | rev | cut -c 2- | rev )
      tmpDataSM+="]"
      tmpDataMA=$(echo $tmpDataMA | rev | cut -c 2- | rev )
      tmpDataMA+="]"

      iData='{"supportive_measures": '$tmpDataSM', "measures_add": '$tmpDataMA'}'
      logging $scriptName RESULT "iData: $iData"

      elasticKey=$indexName"-2-"$tmpuuid

      logging $scriptName RESULT "elasticKey: $elasticKey"
      logging $scriptName INFO "Обновление индекса"
      logging $scriptName INFO "$elasticHost/$indexName2/$indexType/$elasticKey"
      addIndexData "$elasticHost/$indexName2/$indexType/$elasticKey" "$iData"
      logging $scriptName RESULT "`cat $tmpfile`"
    else
      logging $scriptName INFO "пустая запись, пропускаем"
    fi

  done
else
  logging $scriptName INFO "нет новых данных в реестре"
fi



indexName3=$indexName"_3"
logging $scriptName INFO "Собираем данные для индекса $indexName3"

registryID=$(getRegistryId 'nuh_pul_investorov__chastnye_dannye_o_proekte')

logging $scriptName INFO "НУХ Пул инвесторов. Частные данные о проекте [$registryID]"

mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8;
SELECT rg.asfDataID FROM registry_documents rg
LEFT JOIN asf_data ad ON ad.uuid=rg.asfDataID
WHERE rg.deleted IS NULL AND rg.registryID='$registryID'
AND DATE(ad.modified) = DATE(NOW())
ORDER BY DATE(ad.modified) DESC;" > $tmpsql

if [ -s $tmpsql ];
then
  # Количество строк в файле tmpsql
  countStr=`cat $tmpsql | wc -l`

  for i in `seq 2 $countStr`;
  do
    tmpuuid=`cat $tmpsql | sed -n $i'p' | cut -f1`
    echo '' >> $logFile
    logging $scriptName INFO "asfDataID: $tmpuuid"

    getAsfData "$tmpuuid"
    jsonString=`cat $tmpfile`
    asfData=$(getJsonObject "$jsonString" "data")

    type_appObj=$(searchInJsonArr "$asfData" "type_app")
    type_appValue=$(getJsonObject "$type_appObj" "value")

    if [ "$type_appValue" != "null" ]; then
      type_appValue=$(echo $type_appValue | cut -c 2- | rev | cut -c 2- | rev )

      iData='{"type_app": "'$type_appValue'"}'
      logging $scriptName RESULT "iData: $iData"

      elasticKey=$indexName"-3-"$tmpuuid
      logging $scriptName RESULT "elasticKey: $elasticKey"

      logging $scriptName INFO "Обновление индекса"
      logging $scriptName INFO "$elasticHost/$indexName3/$indexType/$elasticKey"
      addIndexData "$elasticHost/$indexName3/$indexType/$elasticKey" "$iData"
      logging $scriptName RESULT "`cat $tmpfile`"
    else
      logging $scriptName RESULT "Пустая запись, пропускаем"
    fi

  done
else
  logging $scriptName INFO "нет новых данных в реестрах"
fi



indexName4=$indexName"_4"
echo '' >> $logFile
logging $scriptName INFO "Собираем данные для индекса $indexName4"

registryID=$(getRegistryId 'nuh_pul_investorov__zayavka')

logging $scriptName INFO "НУХ Пул инвесторов. Заявка [$registryID]"

mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8;
SELECT rg.asfDataID FROM registry_documents rg
LEFT JOIN asf_data ad ON ad.uuid=rg.asfDataID
WHERE rg.deleted IS NULL AND rg.registryID='$registryID'
AND DATE(ad.modified) = DATE(NOW())
ORDER BY DATE(ad.modified) DESC;" > $tmpsql

if [ -s $tmpsql ];
then
  # Количество строк в файле tmpsql
  countStr=`cat $tmpsql | wc -l`

  for i in `seq 2 $countStr`;
  do
    tmpuuid=`cat $tmpsql | sed -n $i'p' | cut -f1`
    echo '' >> $logFile
    logging $scriptName INFO "asfDataID: $tmpuuid"

    getAsfData "$tmpuuid"
    jsonString=`cat $tmpfile`
    asfData=$(getJsonObject "$jsonString" "data")

    table=$(searchInJsonArr "$asfData" "cmp-wbw93u")
    tableData=$(getJsonObject "$table" "data")
    tabLength=$(getRowsCount "$tableData" "communication_type") #кол-во строк в дин.таблице

    tmpData="["
    prov=false

    for ((j=1; j<$tabLength; j++))
    do
      communication_typeObj=$(searchInJsonArr "$tableData" "communication_type-b$j")
      ctId=$(getJsonObject "$communication_typeObj" "id")
      ctKey=$(getJsonObject "$communication_typeObj" "key")
      ctValue=$(getJsonObject "$communication_typeObj" "value")

      ctId=$(echo $ctId | cut -c 2- | rev | cut -c 2- | rev )
      ctKey=$(echo $ctKey | cut -c 2- | rev | cut -c 2- | rev )
      ctValue=$(echo $ctValue | cut -c 2- | rev | cut -c 2- | rev )

      if [ "$communication_typeValue" != "null" ]; then
        prov=true
        tmpData+='{"id": "'$ctId'", "key": "'$ctKey'", "value": "'$ctValue'"},'
      fi
    done

    if [ $prov == true ]; then
      tmpData=$(echo $tmpData | rev | cut -c 2- | rev )
      tmpData+="]"

      logging $scriptName INFO "communication_type: $tmpData"

      iData='{"communication_type": '$tmpData'}'
      logging $scriptName RESULT "iData: $iData"

      elasticKey=$indexName"-4-"$tmpuuid

      logging $scriptName RESULT "elasticKey: $elasticKey"
      logging $scriptName INFO "Обновление индекса"
      logging $scriptName INFO "$elasticHost/$indexName4/$indexType/$elasticKey"
      addIndexData "$elasticHost/$indexName4/$indexType/$elasticKey" "$iData"
      logging $scriptName RESULT "`cat $tmpfile`"
    else
      logging $scriptName INFO "пустая запись, пропускаем"
    fi

  done
else
  logging $scriptName INFO "нет новых данных в реестре"
fi



echo '' >> $logFile
logging $scriptName STATUS "Удаление временных файлов"
rm -rf $tmpsql
rm -rf $tmpfile
logging $scriptName END "Завершение работы скрипта"
exit 0
