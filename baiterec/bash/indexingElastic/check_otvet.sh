#!/bin/bash

source $(dirname $(readlink -e $0))/utils.class
scriptName=${0##*/}
tmpsql=$(mktemp)

asStandart='{"id":"answer_status","type":"listbox","value":"","key":"0"}'
asNoAnswer='{"id":"answer_status","type":"listbox","value":"нет ответа","key":"1"}'
asYesAnswer='{"id":"answer_status","type":"listbox","value":"есть ответ","key":"2"}'

echo '' >> $logFile
logging $scriptName START "Начало работы скрипта"
logging $scriptName INFO "Собираем данные по заявкам, ожидающих ответ от заявителя"

mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8;
SELECT registries.formid, rg.asfDataID, rg.documentID, w.actionID, processes.topProcInstID,
a.cmp_key desicionStatus, IF(DATE(NOW()) > DATE(b.cmp_key), 1, 0) delayStatus

FROM route_items ri
LEFT JOIN processes ON processes.parentProcInstID = ri.procInstID
LEFT JOIN registry_processes rp ON rp.procInstID = processes.topProcInstID
LEFT JOIN processes pp ON pp.topProcInstID = processes.topProcInstID
LEFT JOIN actions w ON w.actionID = pp.objectID
LEFT JOIN registry_documents rg ON rg.asfDataID = rp.asfDataID
LEFT JOIN registries ON registries.registryID = rg.registryID
JOIN asf_data_index a ON a.uuid = rg.asfDataID AND a.cmp_id='refuse_desicion_status'
JOIN asf_data_index b ON b.uuid = rg.asfDataID AND b.cmp_id='answer_date'

WHERE ri.code = 'waiting_answer'
AND rg.deleted IS NULL
AND rg.active != 'N'
AND a.cmp_key IN (1, 4)
AND w.actionID IS NOT NULL
AND w.finished IS NULL
AND processes.actorID IS NOT NULL
AND processes.finished IS NULL
AND pp.finished IS NULL

GROUP BY w.actionID;" > $tmpsql 2>/dev/null


if [ -s $tmpsql ];
then
  # Количество строк в файле tmpsql
  countStr=`cat $tmpsql | wc -l`

  for i in `seq 2 $countStr`;
  do
    formid=`cat $tmpsql | sed -n $i'p' | cut -f1`
    asfDataID=`cat $tmpsql | sed -n $i'p' | cut -f2`
    documentID=`cat $tmpsql | sed -n $i'p' | cut -f3`
    actionID=`cat $tmpsql | sed -n $i'p' | cut -f4`
    topProcInstID=`cat $tmpsql | sed -n $i'p' | cut -f5`
    desicionStatus=`cat $tmpsql | sed -n $i'p' | cut -f6`
    delayStatus=`cat $tmpsql | sed -n $i'p' | cut -f7`


    if [[ ("$desicionStatus" == "1") && ("$delayStatus" == "0") ]]; then
      logging $scriptName INFO "есть ответ, меняем статус в заявке\n actionID: [$actionID]"
      echo '' >> $logFile

      logging $scriptName STATUS "Берем текущие данные по форме"
      getAsfData $asfDataID
      newData=`cat $tmpfile`

      newData=$(echo $newData | sed "s/$asStandart/$asYesAnswer/")
      newData=$(echo $newData | sed "s/$asNoAnswer/$asYesAnswer/")
      newData=$(echo $newData | sed "s/$asYesAnswer/$asYesAnswer/")
      newData=$(echo ${newData#*data})
      newData=$(echo ${newData%]*})
      newData='"data'$newData']'

      logging $scriptName JSON "Итоговый JSON\n`echo $newData`"
      echo '' >> $logFile

      logging $scriptName STATUS "Сохранение данных"
      saveMultipartdata $formid $asfDataID "$newData" $tmpfile
      logging $scriptName RESULT "`cat $tmpfile`"

      echo '' >> $logFile
      logging $scriptName STATUS "Завершаем работу заявителя"
      setWorkComplet $actionID "ответ предоставлен"
      logging $scriptName RESULT "`cat $tmpfile`"


    elif [[ ("$desicionStatus" == "4") && ("$delayStatus" == "1") ]]; then
      logging $scriptName INFO "нет ответа, меняем статус в заявке\n actionID: [$actionID]"
      echo '' >> $logFile

      logging $scriptName STATUS "Берем текущие данные по форме"
      getAsfData $asfDataID
      newData=`cat $tmpfile`

      newData=$(echo $newData | sed "s/$asStandart/$asNoAnswer/")
      newData=$(echo $newData | sed "s/$asNoAnswer/$asNoAnswer/")
      newData=$(echo $newData | sed "s/$asYesAnswer/$asNoAnswer/")
      newData=$(echo ${newData#*data})
      newData=$(echo ${newData%]*})
      newData='"data'$newData']'

      logging $scriptName JSON "Итоговый JSON\n`echo $newData`"
      echo '' >> $logFile

      logging $scriptName STATUS "Сохранение данных"
      saveMultipartdata $formid $asfDataID "$newData" $tmpfile
      logging $scriptName RESULT "`cat $tmpfile`"

      echo '' >> $logFile
      logging $scriptName STATUS "Завершаем работу заявителя"
      setWorkComplet $actionID "время на предоставление ответа просрочено"
      logging $scriptName RESULT "`cat $tmpfile`"

    else
      logging $scriptName INFO "еще есть время на предоставление ответа\n actionID: [$actionID]"
    fi

  done
else
  logging $scriptName INFO "нет заявок ожидающих ответ заявителя"
fi

echo '' >> $logFile
logging $scriptName STATUS "Удаление временных файлов"
rm -rf $tmpsql
rm -rf $tmpfile
logging $scriptName END "Завершение работы скрипта"
exit 0
