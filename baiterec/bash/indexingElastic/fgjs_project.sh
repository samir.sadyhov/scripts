#!/bin/bash

source $(dirname $(readlink -e $0))/utils.class
scriptName=${0##*/}
tmpsql=$(mktemp)
tmpsql2=$(mktemp)

indexName='fgjs_project'
indexType='fp'

echo '' >> $logFile
logging $scriptName START "Начало работы скрипта"
logging $scriptName INFO "Собираем данные для индекса $indexName"

registryID=$(getRegistryId 'ФГЖС:_реестр_для_заполнения_заявки_ФГЖС')
dogovorRegistryID=$(getRegistryId 'fgzhs_zakluchennye_dogovora')

mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8;
SELECT rg.asfDataID, rg.documentid FROM registry_documents rg
LEFT JOIN asf_data ad ON ad.uuid=rg.asfDataID
WHERE rg.deleted IS NULL AND rg.registryID = '$registryID'
AND DATE(ad.modified) = DATE(NOW());" > $tmpsql 2>/dev/null

if [ -s $tmpsql ];
then
  # Количество строк в файле tmpsql
  countStr=`cat $tmpsql | wc -l`

  for i in `seq 2 $countStr`;
  do
    zayavAsfDataID=`cat $tmpsql | sed -n $i'p' | cut -f1`
    zayavDocumentid=`cat $tmpsql | sed -n $i'p' | cut -f2`
    echo '' >> $logFile
    logging $scriptName INFO "asfDataID: [$zayavAsfDataID] :: documentID: [$zayavDocumentid]"

    getAsfData "$zayavAsfDataID"
    jsonString=`cat $tmpfile`
    asfData=$(getJsonObject "$jsonString" "data")

    send_dateObj=$(searchInJsonArr "$asfData" "send_date")
    send_dateKey=$(getJsonObject "$send_dateObj" "key")

    if [ "$send_dateKey" != "null" ]; then
      send_dateKey=$(echo $send_dateKey | cut -c 2- | rev | cut -c 2- | rev )
      send_dateKey=$(date.parse $send_dateKey)

      statusObj=$(searchInJsonArr "$asfData" "refuse_desicion_status")
      statusKey=$(getJsonObject "$statusObj" "key")

      if [ "$statusKey" != "null" ]; then
        zayavkaStatus="Поступило"
        statusKey=$(echo $statusKey | cut -c 2- | rev | cut -c 2- | rev )
        statusValue=$(getJsonObject "$statusObj" "value")
        statusValue=$(echo $statusValue | cut -c 2- | rev | cut -c 2- | rev )


        # Общая_Информация_о_проекте
        projecktData=$(searchInJsonArr "$asfData" "projeck")
        projecktData=$(getJsonObject "$projecktData" "key")
        projecktData=$(echo $projecktData | cut -c 2- | rev | cut -c 2- | rev )
        projecktData=$(getDataUUID $projecktData)

        getAsfData "$projecktData"
        projecktData=`cat $tmpfile`
        projecktData=$(getJsonObject "$projecktData" "data")

        projecktStoimost=$(searchInJsonArr "$projecktData" "stoimost")
        projecktStoimost=$(getJsonObject "$projecktStoimost" "key")
        if [ "$projecktStoimost" != "null" ]; then
          projecktStoimost=$(echo $projecktStoimost | cut -c 2- | rev | cut -c 2- | rev )
        else
          projecktStoimost=0
        fi


        #atributy_fgzhs_psd
        filesData=$(searchInJsonArr "$asfData" "files")
        filesData=$(getJsonObject "$filesData" "key")
        filesData=$(echo $filesData | cut -c 2- | rev | cut -c 2- | rev )
        filesData=$(getDataUUID $filesData)
        getAsfData "$filesData"
        filesData=`cat $tmpfile`
        filesData=$(getJsonObject "$filesData" "data")

        documentInfo24Data=$(searchInJsonArr "$filesData" "document_info24")
        documentInfo24Data=$(getJsonObject "$documentInfo24Data" "key")
        documentInfo24Data=$(echo $documentInfo24Data | cut -c 2- | rev | cut -c 2- | rev )
        documentInfo24Data=$(getDataUUID $documentInfo24Data)
        getAsfData "$documentInfo24Data"
        documentInfo24Data=`cat $tmpfile`
        documentInfo24Data=$(getJsonObject "$documentInfo24Data" "data")

        totalUsefulFloorArea=$(searchInJsonArr "$documentInfo24Data" "total_useful_floor_area")
        totalUsefulFloorArea=$(getJsonObject "$totalUsefulFloorArea" "key")
        if [ "$totalUsefulFloorArea" != "null" ]; then
          totalUsefulFloorArea=$(echo $totalUsefulFloorArea | cut -c 2- | rev | cut -c 2- | rev )
        else
          totalUsefulFloorArea=0
        fi

        flatsQuantity=$(searchInJsonArr "$documentInfo24Data" "flats_quantity")
        flatsQuantity=$(getJsonObject "$flatsQuantity" "key")
        if [ "$flatsQuantity" != "null" ]; then
          flatsQuantity=$(echo $flatsQuantity | cut -c 2- | rev | cut -c 2- | rev )
        else
          flatsQuantity=0
        fi

        odobrSum=0
        if [ "$statusKey" == "2" ]; then
          zayavkaStatus="Одобренно"
          odobrSum=$projecktStoimost
        fi

        # Заключенные договора
        # 0 - нет договора, 1 - договор заключен
        dogovorStatus=0
        dogovorDate="2000/01/01"
        dogovorSum=0
        squareDogovor=0
        flatsDogovor=0

        mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8;
        SELECT rg.asfdataid, rg.documentid, d.cmp_key dateDogovor, s.cmp_key sumDogovor,
        k.cmp_key squareDogovor, f.cmp_key flatsDogovor
        FROM registry_documents rg
        LEFT JOIN asf_data_index l ON l.uuid=rg.asfDataId AND l.cmp_id='app'
        LEFT JOIN asf_data_index d ON d.uuid=rg.asfDataId AND d.cmp_id='date'
        LEFT JOIN asf_data_index s ON s.uuid=rg.asfDataId AND s.cmp_id='sum'
        LEFT JOIN asf_data_index k ON k.uuid=rg.asfDataId AND k.cmp_id='square'
        LEFT JOIN asf_data_index f ON f.uuid=rg.asfDataId AND f.cmp_id='flats'
        WHERE rg.registryID = '$dogovorRegistryID' AND rg.deleted IS NULL
        AND l.cmp_key = '$zayavDocumentid';" > $tmpsql2 2>/dev/null

        if [ -s $tmpsql2 ];
        then
          dogovorDate=`cat $tmpsql2 | sed -n 2p | cut -f3`
          dogovorDate=$(date.parse $dogovorDate)
          dogovorSum=`cat $tmpsql2 | sed -n 2p | cut -f4`
          squareDogovor=`cat $tmpsql2 | sed -n 2p | cut -f5`
          flatsDogovor=`cat $tmpsql2 | sed -n 2p | cut -f6`
          dogovorStatus=1
          zayavkaStatus="Договора"
        fi

        iData='{"send_date": "'$send_dateKey'", "status": '$statusKey', "statusName": "'$statusValue'", "zayavkaStatus": "'$zayavkaStatus'",'
        iData+='"projecktStoimost": '$projecktStoimost', "odobrSum": '$odobrSum','
        iData+='"totalUsefulFloorArea": '$totalUsefulFloorArea', "flatsQuantity": '$flatsQuantity','
        iData+='"dogovorStatus": '$dogovorStatus', "dogovorDate": "'$dogovorDate'",'
        iData+='"dogovorSum": '$dogovorSum', "squareDogovor": '$squareDogovor', "flatsDogovor": '$flatsDogovor'}'

        logging $scriptName RESULT "iData: $iData"

        elasticKey=$indexName"-"$zayavAsfDataID
        logging $scriptName RESULT "elasticKey: $elasticKey"

        logging $scriptName INFO "Обновление индекса"
        logging $scriptName INFO "$elasticHost/$indexName/$indexType/$elasticKey"
        addIndexData "$elasticHost/$indexName/$indexType/$elasticKey" "$iData"
        logging $scriptName RESULT "`cat $tmpfile`"
      else
        logging $scriptName RESULT "Не указан статус, пропускаем"
      fi

    else
      logging $scriptName RESULT "Заявка не отправлена, пропускаем"
    fi

  done
else
  logging $scriptName INFO "нет новых данных в реестре"
fi


echo '' >> $logFile
logging $scriptName STATUS "Удаление временных файлов"
rm -rf $tmpsql
rm -rf $tmpsql2
rm -rf $tmpfile
logging $scriptName END "Завершение работы скрипта"
exit 0
