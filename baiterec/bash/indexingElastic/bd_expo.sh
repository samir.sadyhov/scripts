#!/bin/bash

source $(dirname $(readlink -e $0))/utils.class
scriptName=${0##*/}
tmpsql=$(mktemp)

indexName='bd_expo'
indexType='be'

echo '' >> $logFile
logging $scriptName START "Начало работы скрипта"
logging $scriptName INFO "Собираем данные для индекса $indexName"

mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8;
SELECT rg.asfDataID, DATE(a.cmp_key) sendDate,

CASE b.cmp_key
WHEN 1 THEN 2
WHEN 2 THEN 3
WHEN 3 THEN 0
WHEN 4 THEN 2
END statusKey,

CASE b.cmp_key
WHEN 1 THEN 'Забронировано'
WHEN 2 THEN 'Продано'
WHEN 3 THEN 'Отказано'
WHEN 4 THEN 'Забронировано'
END statusValue,

c.cmp_data sredstvaKey, c.cmp_key sredstvaValue,
SUM(IF(k1.cmp_data != '' AND k1.cmp_data IS NOT NULL, 1, 0)) house,
SUM(IF(k2.cmp_data != '' AND k1.cmp_data IS NOT NULL, 1, 0)) parking,

CASE b.cmp_key
WHEN 1 THEN 'В работе'
WHEN 2 THEN 'Одобрено'
WHEN 3 THEN 'Отказано'
WHEN 4 THEN 'На доработке'
ELSE 'Черновик'
END statusZayavki,

DATE(ad.modified) modified,

IFNULL(SUM(k3.cmp_key), 0) flat_cost,
IFNULL(SUM(k4.cmp_key), 0) parking_cost,
ROUND(IFNULL(SUM(k3.cmp_key), 0)/1000000, 1) flat_costMillion,
ROUND(IFNULL(SUM(k4.cmp_key), 0)/1000000, 1) parking_costMillion

FROM registry_documents rg
LEFT JOIN asf_data ad ON ad.uuid = rg.asfDataID
LEFT JOIN asf_data_index a ON a.uuid = rg.asfDataID AND a.cmp_id='send_date'
LEFT JOIN asf_data_index b ON b.uuid = rg.asfDataID AND b.cmp_id='refuse_desicion_status'
LEFT JOIN asf_data_index c ON c.uuid = rg.asfDataID AND c.cmp_id='radio-je3ut'
LEFT JOIN asf_data_index k1 ON k1.uuid = rg.asfDataID AND k1.cmp_id LIKE 'house-%'
LEFT JOIN asf_data_index k2 ON k2.uuid = rg.asfDataID AND k2.cmp_id = CONCAT('parking-b', k1.cmp_row)
LEFT JOIN asf_data_index k3 ON k3.uuid = rg.asfDataID AND k3.cmp_id = CONCAT('flat_cost-b', k1.cmp_row)
LEFT JOIN asf_data_index k4 ON k4.uuid = rg.asfDataID AND k4.cmp_id = CONCAT('parking_cost-b', k1.cmp_row)

WHERE rg.registryID = getRegistryIdByCode('bd_zayavlenie_po_expo')
AND rg.deleted IS NULL AND b.cmp_key != 0
GROUP BY rg.asfDataID;" > $tmpsql 2>/dev/null

if [ -s $tmpsql ];
then
  # Количество строк в файле tmpsql
  countStr=`cat $tmpsql | wc -l`

  for i in `seq 2 $countStr`;
  do
    asfDataID=`cat $tmpsql | sed -n $i'p' | cut -f1`
    sendDate=`cat $tmpsql | sed -n $i'p' | cut -f2`
    statusKey=`cat $tmpsql | sed -n $i'p' | cut -f3`
    statusValue=`cat $tmpsql | sed -n $i'p' | cut -f4`
    sredstvaKey=`cat $tmpsql | sed -n $i'p' | cut -f5`
    sredstvaValue=`cat $tmpsql | sed -n $i'p' | cut -f6`
    house=`cat $tmpsql | sed -n $i'p' | cut -f7`
    parking=`cat $tmpsql | sed -n $i'p' | cut -f8`
    statusZayavki=`cat $tmpsql | sed -n $i'p' | cut -f9`
    modified=`cat $tmpsql | sed -n $i'p' | cut -f10`
    flat_cost=`cat $tmpsql | sed -n $i'p' | cut -f11`
    parking_cost=`cat $tmpsql | sed -n $i'p' | cut -f12`
    flat_costMillion=`cat $tmpsql | sed -n $i'p' | cut -f13`
    parking_costMillion=`cat $tmpsql | sed -n $i'p' | cut -f14`

    echo '' >> $logFile
    logging $scriptName INFO "asfDataID: $asfDataID"

    iData='{"sendDate": "'$sendDate'", "modified": "'$modified'",'
    iData+='"statusZayavki": "'$statusZayavki'", "statusKey": '$statusKey', "statusValue": "'$statusValue'",'
    iData+='"sredstvaKey": '$sredstvaKey', "sredstvaValue": "'$sredstvaValue'",'
    iData+='"flat_costMillion": '$flat_costMillion', "parking_costMillion": '$parking_costMillion','
    iData+='"house": '$house', "flat_cost": '$flat_cost','
    iData+='"parking": '$parking', "parking_cost": '$parking_cost'}'
    logging $scriptName RESULT "iData: $iData"

    elasticKey=$indexName"-"$asfDataID
    logging $scriptName RESULT "elasticKey: $elasticKey"

    logging $scriptName INFO "Обновление индекса"
    logging $scriptName INFO "$elasticHost/$indexName/$indexType/$elasticKey"
    addIndexData "$elasticHost/$indexName/$indexType/$elasticKey" "$iData"
    logging $scriptName RESULT "`cat $tmpfile`"

  done
else
  logging $scriptName INFO "нет новых данных"
fi

echo '' >> $logFile
logging $scriptName STATUS "Удаление временных файлов"
rm -rf $tmpsql
rm -rf $tmpfile
logging $scriptName END "Завершение работы скрипта"
exit 0
