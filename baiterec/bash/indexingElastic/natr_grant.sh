#!/bin/bash

source $(dirname $(readlink -e $0))/utils.class
scriptName=${0##*/}
tmpsql=$(mktemp)
tmpsql2=$(mktemp)
tmpsql3=$(mktemp)

indexName='natr_grant'
indexType='ng'

rInfo='[
  {
    "zayavRegCode": "grant_na_razvitie_otraslei",
    "grantName": "Грант на развитие отраслей",
    "grantVid": "1",
    "odobrDate": "industry_date",
    "odobrLink": "industry_app",
    "odobrSum": "industry_sum"
  },
  {
    "zayavRegCode": "grant_na_razvitie_deistvuuschih_predpriyatii",
    "grantName": "Грант на развитие действующих предприятий",
    "grantVid": "2",
    "odobrDate": "enterprises_date",
    "odobrLink": "enterprises_app",
    "odobrSum": "enterprises_sum"
  },
  {
    "zayavRegCode": "grant_na_kommertsializatsiu_ur_litsa",
    "grantName": "Грант на коммерциализацию (юр.лица)",
    "grantVid": "3",
    "odobrDate": "comm_ul_date",
    "odobrLink": "comm_ul_app",
    "odobrSum": "comm_ul_sum"
  },
  {
    "zayavRegCode": "grant_na_kommertsializatsiu_fiz_litsa",
    "grantName": "Грант на коммерциализацию (физ.лица)",
    "grantVid": "4",
    "odobrDate": "comm_fl_date",
    "odobrLink": "comm_fl_app",
    "odobrSum": "comm_fl_sum"
  }
]'

odobrRegistryID=$(getRegistryId 'natr_odobrennye_zayavki')
dogovorRegistryID=$(getRegistryId 'natr_zakluchennye_dogovora')

echo '' >> $logFile
logging $scriptName START "Начало работы скрипта"
logging $scriptName INFO "Собираем данные для индекса $indexName"

rInfoLength=$(getRowsCount "$rInfo" "zayavRegCode")

for ((i=0; i<$rInfoLength; i++))
do
  tmpObj=$(echo $rInfo | jq '.['$i']')

  zayavRegCode=$(getJsonObject "$tmpObj" "zayavRegCode")
  zayavRegCode=$(echo $zayavRegCode | cut -c 2- | rev | cut -c 2- | rev )
  zayavRegistryID=$(getRegistryId "$zayavRegCode")

  grantVid=$(getJsonObject "$tmpObj" "grantVid")
  grantVid=$(echo $grantVid | cut -c 2- | rev | cut -c 2- | rev )
  grantName=$(getJsonObject "$tmpObj" "grantName")
  grantName=$(echo $grantName | cut -c 2- | rev | cut -c 2- | rev )
  odobrDateCmpId=$(getJsonObject "$tmpObj" "odobrDate")
  odobrDateCmpId=$(echo $odobrDateCmpId | cut -c 2- | rev | cut -c 2- | rev )
  odobrLinkCmpId=$(getJsonObject "$tmpObj" "odobrLink")
  odobrLinkCmpId=$(echo $odobrLinkCmpId | cut -c 2- | rev | cut -c 2- | rev )
  odobrSumCmpId=$(getJsonObject "$tmpObj" "odobrSum")
  odobrSumCmpId=$(echo $odobrSumCmpId | cut -c 2- | rev | cut -c 2- | rev )

  echo '' >> $logFile
  logging $scriptName INFO "Поиск данных в реестре [$grantName]"

  mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8;
  SELECT rg.asfDataID, rg.documentid FROM registry_documents rg
  LEFT JOIN asf_data ad ON ad.uuid=rg.asfDataID WHERE rg.deleted IS NULL
  AND rg.registryID = '$zayavRegistryID'
  AND DATE(ad.modified) = DATE(NOW());" > $tmpsql 2>/dev/null

  if [ -s $tmpsql ];
  then
    # Количество строк в файле tmpsql
    countStr=`cat $tmpsql | wc -l`

    for j in `seq 2 $countStr`;
    do
      zayavAsfDataID=`cat $tmpsql | sed -n $j'p' | cut -f1`
      zayavDocumentid=`cat $tmpsql | sed -n $j'p' | cut -f2`

      getAsfData "$zayavAsfDataID"
      jsonString=`cat $tmpfile`
      asfData=$(getJsonObject "$jsonString" "data")

      send_dateObj=$(searchInJsonArr "$asfData" "send_date")
      send_dateKey=$(getJsonObject "$send_dateObj" "key")

      if [ "$send_dateKey" != "null" ]; then
        send_dateKey=$(echo $send_dateKey | cut -c 2- | rev | cut -c 2- | rev )
        send_dateKey=$(date.parse $send_dateKey)

        #client_data
        client_dataObj=$(searchInJsonArr "$asfData" "client_data")
        client_dataKey=$(getJsonObject "$client_dataObj" "key")
        grant_sumValue="0"
        if [ "$client_dataKey" != "null" ]; then
          client_dataKey=$(echo $client_dataKey | cut -c 2- | rev | cut -c 2- | rev )
          clUUID=$(getDataUUID $client_dataKey)
          getAsfData "$clUUID"
          clData=`cat $tmpfile`
          clData=$(getJsonObject "$clData" "data")

          grant_sumObj=$(searchInJsonArr "$clData" "grant_sum")
          grant_sumValue=$(getJsonObject "$grant_sumObj" "key")

          if [ "$grant_sumValue" != "null" ]; then
            grant_sumValue=$(echo $grant_sumValue | cut -c 2- | rev | cut -c 2- | rev)
            grant_sumValue=$(echo "scale=3;$grant_sumValue/1000" | bc)
          else
            grant_sumValue="0"
          fi
        fi

        odobrDate="2000/01/01"
        odobrSum=0
        dogovorDate="2000/01/01"
        dogovorSum=0
        status=1
        statusName="Поступило"

        mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8;
        SELECT rg.asfdataid, rg.documentid, d.cmp_key dateOdobr, ROUND(s.cmp_key/1000, 3) sumOdobr
        FROM registry_documents rg
        LEFT JOIN asf_data_index l ON l.uuid=rg.asfDataId AND l.cmp_id='$odobrLinkCmpId'
        LEFT JOIN asf_data_index d ON d.uuid=rg.asfDataId AND d.cmp_id='$odobrDateCmpId'
        LEFT JOIN asf_data_index s ON s.uuid=rg.asfDataId AND s.cmp_id='$odobrSumCmpId'
        WHERE rg.registryID = '$odobrRegistryID' AND rg.deleted IS NULL
        AND l.cmp_key = '$zayavDocumentid';" > $tmpsql2 2>/dev/null

        if [ -s $tmpsql2 ];
        then
          odobrAsfDataID=`cat $tmpsql2 | sed -n 2p | cut -f1`
          odobrDocumentID=`cat $tmpsql2 | sed -n 2p | cut -f2`
          odobrDate=`cat $tmpsql2 | sed -n 2p | cut -f3`
          odobrDate=$(date.parse $odobrDate)
          odobrSum=`cat $tmpsql2 | sed -n 2p | cut -f4`
          status=2
          statusName="Одобренно"

          mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8;
          SELECT rg.asfdataid, rg.documentid, d.cmp_key dateDogovor, ROUND(s.cmp_key/1000, 3) sumDogovor
          FROM registry_documents rg
          LEFT JOIN asf_data_index l ON l.uuid=rg.asfDataId AND l.cmp_id='sek_app'
          LEFT JOIN asf_data_index d ON d.uuid=rg.asfDataId AND d.cmp_id='sek_date'
          LEFT JOIN asf_data_index s ON s.uuid=rg.asfDataId AND s.cmp_id='sek_sum'
          WHERE rg.registryID = '$dogovorRegistryID' AND rg.deleted IS NULL
          AND l.cmp_key = '$odobrDocumentID';" > $tmpsql3 2>/dev/null

          if [ -s $tmpsql3 ];
          then
            dogovorDate=`cat $tmpsql3 | sed -n 2p | cut -f3`
            dogovorDate=$(date.parse $dogovorDate)
            dogovorSum=`cat $tmpsql3 | sed -n 2p | cut -f4`
            status=3
            statusName="Договора"
          fi

        fi

        iData='{"grantName": "'$grantName'", "dateGrant": "'$send_dateKey'", "sumGrant": '$grant_sumValue','
        iData+='"dateOdobr": "'$odobrDate'", "sumOdobr": '$odobrSum','
        iData+='"dateDogovor": "'$dogovorDate'", "sumDogovor": '$dogovorSum','
        iData+='"statusName": "'$statusName'", "status": '$status'}'
        logging $scriptName RESULT "iData: $iData"

        elasticKey=$indexName"-"$zayavAsfDataID
        logging $scriptName RESULT "elasticKey: $elasticKey"

        logging $scriptName INFO "Обновление индекса"
        logging $scriptName INFO "$elasticHost/$indexName/$indexType/$elasticKey"
        addIndexData "$elasticHost/$indexName/$indexType/$elasticKey" "$iData"
        logging $scriptName RESULT "`cat $tmpfile`"

      else
        logging $scriptName RESULT "Пустая запись, пропускаем"
      fi
    done
  else
    logging $scriptName INFO "нет новых данных в реестре [$grantName]"
  fi
done

echo '' >> $logFile
logging $scriptName STATUS "Удаление временных файлов"
rm -rf $tmpsql
rm -rf $tmpsql2
rm -rf $tmpsql3
rm -rf $tmpfile
logging $scriptName END "Завершение работы скрипта"
exit 0
