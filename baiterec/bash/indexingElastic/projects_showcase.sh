#!/bin/bash

source $(dirname $(readlink -e $0))/utils.class
scriptName=${0##*/}
tmpsql="/tmp/projects_showcase.sql"

indexName='projects_showcase'
indexType='ps'

echo '' >> $logFile
logging $scriptName START "Начало работы скрипта"
logging $scriptName INFO "Собираем данные для индекса $indexName"

registryID1=$(getRegistryId 'kppf_ps_project_publish_notification')
registryID2=$(getRegistryId 'НУХ_Байтерек:_Уведомление_о_публикации_проекта_на_Витрине_проектов')

logging $scriptName INFO "Реестры:\nКППФ ПС: Уведомление о публикации проекта на Витрине проектов [$registryID1]\nНУХ Байтерек: Уведомление о публикации проекта на Витрине проектов [$registryID2]"

mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8;
SELECT rg.asfDataID FROM registry_documents rg
LEFT JOIN asf_data ad ON ad.uuid=rg.asfDataID
WHERE rg.deleted IS NULL AND rg.registryID IN ('$registryID1', '$registryID2')
AND DATE(ad.modified) = DATE(NOW());" > $tmpsql

if [ -s $tmpsql ];
then
  # Количество строк в файле tmpsql
  countStr=`cat $tmpsql | wc -l`

  for i in `seq 2 $countStr`;
  do
    tmpuuid=`cat $tmpsql | sed -n $i'p' | cut -f1`
    echo '' >> $logFile
    logging $scriptName INFO "asfDataID: $tmpuuid"

    getAsfData "$tmpuuid"
    jsonString=`cat $tmpfile`
    asfData=$(getJsonObject "$jsonString" "data")

    nomer_uvedObj=$(searchInJsonArr "$asfData" "nomer_uved")
    nomer_uvedValue=$(getJsonObject "$nomer_uvedObj" "value")

    if [ "$nomer_uvedValue" != "null" ]; then
      nomer_uvedValue=$(echo $nomer_uvedValue | cut -c 2- | rev | cut -c 2- | rev )

      iData='{"nomer_uvedValue": "'$nomer_uvedValue'"}'
      logging $scriptName RESULT "iData: $iData"

      elasticKey=$indexName"-"$tmpuuid
      logging $scriptName RESULT "elasticKey: $elasticKey"

      logging $scriptName INFO "Обновление индекса"
      logging $scriptName INFO "$elasticHost/$indexName/$indexType/$elasticKey"
      addIndexData "$elasticHost/$indexName/$indexType/$elasticKey" "$iData"
      logging $scriptName RESULT "`cat $tmpfile`"
    else
      logging $scriptName RESULT "Пустая запись, пропускаем"
    fi

  done
else
  logging $scriptName INFO "нет новых данных в реестрах"
fi



echo '' >> $logFile
logging $scriptName STATUS "Удаление временных файлов"
rm -rf $tmpsql
rm -rf $tmpfile
logging $scriptName END "Завершение работы скрипта"
exit 0
