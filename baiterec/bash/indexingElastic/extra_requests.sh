#!/bin/bash

source $(dirname $(readlink -e $0))/utils.class
scriptName=${0##*/}
tmpsql="/tmp/extra_requests.sql"

indexName='extra_requests'
indexType='er'

rInfo='[
  {"code": "Запрос_дополнительных_документов", "organization": "ТОО \"Kazakhstan Project Preparation Fund\""},
  {"code": "bd_pay_add_doc_notification", "organization": "АО \"Байтерек девелопмент\""},
  {"code": "БД_Уведомление_о_запросе_дополнительных_документов", "organization": "АО \"Байтерек девелопмент\""},
  {"code": "kppf_ps_add_doc_request", "organization": "ТОО \"Kazakhstan Project Preparation Fund\""},
  {"code": "Уведомление_о_запросе_дополнительных_документов_НУХ", "organization": "АО \"Национальный Управляющий Холдинг \"Байтерек\""},
  {"code": "Уведомление_о_запросе_дополнительных_документов", "organization": "АО \"Национальный Управляющий Холдинг \"Байтерек\""}
]'

echo '' >> $logFile
logging $scriptName START "Начало работы скрипта"
logging $scriptName INFO "Собираем данные для индекса $indexName"

rInfoLength=$(getRowsCount "$rInfo" "code")

for ((i=0; i<$rInfoLength; i++))
do
  tmpObj=$(echo $rInfo | jq '.['$i']')
  code=$(getJsonObject "$tmpObj" "code")
  organization=$(getJsonObject "$tmpObj" "organization")
  code=$(echo $code | cut -c 2- | rev | cut -c 2- | rev )
  organization=$(echo $organization | cut -c 2- | rev | cut -c 2- | rev )
  registryID=$(getRegistryId $code)

  echo '' >> $logFile
  logging $scriptName INFO "registryCode: [$code]\nregistryID: [$registryID]\norganization: [$organization]"

  mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8;
  SELECT rg.asfDataID FROM registry_documents rg
  LEFT JOIN asf_data ad ON ad.uuid=rg.asfDataID
  WHERE rg.deleted IS NULL AND rg.registryID = '$registryID'
  AND DATE(ad.modified) = DATE(NOW());" > $tmpsql

  if [ -s $tmpsql ];
  then
    # Количество строк в файле tmpsql
    countStr=`cat $tmpsql | wc -l`

    for j in `seq 2 $countStr`;
    do
      tmpuuid=`cat $tmpsql | sed -n $j'p' | cut -f1`
      echo '' >> $logFile
      logging $scriptName INFO "asfDataID: $tmpuuid"

      iData='{"organization_value": "'$organization'", "organization_key": "'$code'", "count": "1"}'
      logging $scriptName RESULT "iData: $iData"

      elasticKey=$indexName"-"$tmpuuid
      logging $scriptName RESULT "elasticKey: $elasticKey"
      logging $scriptName INFO "Обновление индекса"
      logging $scriptName INFO "$elasticHost/$indexName/$indexType/$elasticKey"
      addIndexData "$elasticHost/$indexName/$indexType/$elasticKey" "$iData"
      logging $scriptName RESULT "`cat $tmpfile`"
    done
  else
    logging $scriptName INFO "нет новых данных в реесте"
  fi

done

echo '' >> $logFile
logging $scriptName STATUS "Удаление временных файлов"
rm -rf $tmpsql
rm -rf $tmpfile
logging $scriptName END "Завершение работы скрипта"
exit 0
