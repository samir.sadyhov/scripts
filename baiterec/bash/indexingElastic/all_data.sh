#!/bin/bash

source $(dirname $(readlink -e $0))/utils.class
scriptName=${0##*/}
tmpsql=$(mktemp)

indexName='all_data'
indexType='ad'

echo '' >> $logFile
logging $scriptName START "Начало работы скрипта"
logging $scriptName INFO "Собираем данные для индекса $indexName"


echo '' >> $logFile
logging $scriptName INFO "=================== БД ==================="

mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8;
SELECT 'БД' name, asfDataID, sendDate, statusKey, statusValue,
'Акмолинская область' regionName, 1 regionKey, 'v93r55ztchws' regionGeoHash,
summa summaZ, summa summaO

FROM (SELECT rg.asfDataID, DATE(a.cmp_key) sendDate,
IF(b.cmp_key != 2, 1, 2) statusKey,
IF(b.cmp_key != 2, 'В работе', 'Одобрено') statusValue,
'Акмолинская область' regionName, 1 regionKey, 'v93r55ztchws' regionGeoHash,

IFNULL((SELECT a2.cmp_key FROM registry_documents rg2
JOIN asf_data_index a2 ON a2.uuid=rg2.asfDataId AND a2.cmp_id='total_sum'
WHERE rg2.documentid = c.cmp_key), 0)*1 summa

FROM registries r
LEFT JOIN registry_documents rg ON rg.registryID=r.registryID
LEFT JOIN asf_data ad ON ad.uuid=rg.asfDataID
JOIN asf_data_index a ON a.uuid=rg.asfDataId AND a.cmp_id='send_date'
JOIN asf_data_index b ON b.uuid=rg.asfDataId AND b.cmp_id='refuse_desicion_status'
LEFT JOIN asf_data_index c ON c.uuid=rg.asfDataId AND c.cmp_id='object_info'
WHERE rg.deleted IS NULL AND r.code IN (
'bd_nurly_zher',
'bd_zayavka_na_finansirovanie_zhil_proektov_pryamoe_finansirovanie',
'bd_zayavka_na_finansirovanie_zhil_proektov_sredstva_mfo',
'bd_zayavka_na_finansirovanie_zhil_proektov_sob_sredstva')
AND DATE(ad.modified) = DATE(NOW())
) t

UNION

SELECT 'БД' name, asfDataID, sendDate, statusKey, IF(statusKey = 1, 'В работе', 'Одобрено') statusValue,
'Акмолинская область' regionName, 1 regionKey, 'v93r55ztchws' regionGeoHash, 0 summaZ, 0 summaO

FROM (SELECT rg.asfDataID, DATE(a.cmp_key) sendDate, IF(b.cmp_key = 2, 2, 1) statusKey

FROM registries r
LEFT JOIN registry_documents rg ON rg.registryID=r.registryID
LEFT JOIN asf_data ad ON ad.uuid=rg.asfDataID
JOIN asf_data_index a ON a.uuid=rg.asfDataId AND a.cmp_id='send_date'
LEFT JOIN asf_data_index b ON b.uuid=rg.asfDataId AND b.cmp_id='refuse_desicion_status'

WHERE rg.deleted IS NULL AND b.cmp_key IN (1, 2, 4)
AND r.code IN (
'bd_zayavlenie_po_expo',
'bd_priobretenie_zhilya',
'bd_sots_proekty',
'bd_pay_main_service')
AND DATE(ad.modified) = DATE(NOW())
) t;" > $tmpsql 2>/dev/null

if [ -s $tmpsql ];
then
  # Количество строк в файле tmpsql
  countStr=`cat $tmpsql | wc -l`

  for i in `seq 2 $countStr`;
  do
    asfDataID=`cat $tmpsql | sed -n $i'p' | cut -f2`
    organization=`cat $tmpsql | sed -n $i'p' | cut -f1`
    sendDate=`cat $tmpsql | sed -n $i'p' | cut -f3`
    statusKey=`cat $tmpsql | sed -n $i'p' | cut -f4`
    statusValue=`cat $tmpsql | sed -n $i'p' | cut -f5`
    regionName=`cat $tmpsql | sed -n $i'p' | cut -f6`
    regionKey=`cat $tmpsql | sed -n $i'p' | cut -f7`
    regionGeoHash=`cat $tmpsql | sed -n $i'p' | cut -f8`
    amount_r=`cat $tmpsql | sed -n $i'p' | cut -f9`
    amount_a=`cat $tmpsql | sed -n $i'p' | cut -f10`

    echo '' >> $logFile
    logging $scriptName INFO "asfDataID: $asfDataID"

    iData='{"organization": "'$organization'", "sendDate": "'$sendDate'",'
    iData+='"statusKey": '$statusKey', "statusValue": "'$statusValue'",'
    iData+='"regionName": "'$regionName'", "regionKey": "'$regionKey'", "regionGeoHash": "'$regionGeoHash'",'
    iData+='"amount_r": '$amount_r', "amount_a": '$amount_a'}'
    logging $scriptName RESULT "iData: $iData"

    elasticKey=$indexName"-"$asfDataID
    logging $scriptName RESULT "elasticKey: $elasticKey"

    logging $scriptName INFO "Обновление индекса"
    logging $scriptName INFO "$elasticHost/$indexName/$indexType/$elasticKey"
    addIndexData "$elasticHost/$indexName/$indexType/$elasticKey" "$iData"
    logging $scriptName RESULT "`cat $tmpfile`"

  done
else
  logging $scriptName INFO "нет новых данных"
fi


echo '' >> $logFile
logging $scriptName INFO "=================== НУХ ==================="

mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8;
SELECT 'НУХ' name, asfDataID, sendDate, statusKey, statusValue, IFNULL(regionName, 'Акмолинская область') regionName, IFNULL(regionKey, 1) regionKey,
IFNULL(ST_GeoHash(SUBSTRING_INDEX(regionPoint, ',', -1), SUBSTRING_INDEX(regionPoint, ',', 1), 12), 'v93r55ztchws') regionGeoHash, 0 summaZ, 0 summaO
FROM (SELECT asfDataID, sendDate, statusKey, statusValue, regionName, regionKey, getRegionGeoPoint(regionKey) regionPoint
FROM (SELECT rg.asfDataID, DATE(a.cmp_key) sendDate, IF(b.cmp_key != 2, 1, 2) statusKey,  IF(b.cmp_key != 2, 'В работе', 'Одобрено') statusValue,
(SELECT rr.cmp_data FROM registry_documents rg2
LEFT JOIN asf_data_index rr ON rr.uuid=rg2.asfDataID AND rr.cmp_id='region'
WHERE rg2.documentID=c.cmp_key) regionName,
(SELECT rr.cmp_key FROM registry_documents rg2
LEFT JOIN asf_data_index rr ON rr.uuid=rg2.asfDataID AND rr.cmp_id='region'
WHERE rg2.documentID=c.cmp_key) regionKey
FROM registries r
LEFT JOIN registry_documents rg ON rg.registryID=r.registryID
LEFT JOIN asf_data ad ON ad.uuid=rg.asfDataID
JOIN asf_data_index a ON a.uuid=rg.asfDataId AND a.cmp_id='send_date'
JOIN asf_data_index b ON b.uuid=rg.asfDataId AND b.cmp_id='refuse_desicion_status'
LEFT JOIN asf_data_index c ON c.uuid=rg.asfDataID AND c.cmp_id='chast_project'
WHERE rg.deleted IS NULL AND r.code IN ('nuh_pul_proektov__zayavka', 'nuh_pul_investorov__zayavka')
AND DATE(ad.modified) = DATE(NOW())
) t ) t2;" > $tmpsql 2>/dev/null

if [ -s $tmpsql ];
then
  # Количество строк в файле tmpsql
  countStr=`cat $tmpsql | wc -l`

  for i in `seq 2 $countStr`;
  do
    asfDataID=`cat $tmpsql | sed -n $i'p' | cut -f2`
    organization=`cat $tmpsql | sed -n $i'p' | cut -f1`
    sendDate=`cat $tmpsql | sed -n $i'p' | cut -f3`
    statusKey=`cat $tmpsql | sed -n $i'p' | cut -f4`
    statusValue=`cat $tmpsql | sed -n $i'p' | cut -f5`
    regionName=`cat $tmpsql | sed -n $i'p' | cut -f6`
    regionKey=`cat $tmpsql | sed -n $i'p' | cut -f7`
    regionGeoHash=`cat $tmpsql | sed -n $i'p' | cut -f8`
    amount_r=`cat $tmpsql | sed -n $i'p' | cut -f9`
    amount_a=`cat $tmpsql | sed -n $i'p' | cut -f10`

    echo '' >> $logFile
    logging $scriptName INFO "asfDataID: $asfDataID"

    iData='{"organization": "'$organization'", "sendDate": "'$sendDate'",'
    iData+='"statusKey": '$statusKey', "statusValue": "'$statusValue'",'
    iData+='"regionName": "'$regionName'", "regionKey": "'$regionKey'", "regionGeoHash": "'$regionGeoHash'",'
    iData+='"amount_r": '$amount_r', "amount_a": '$amount_a'}'
    logging $scriptName RESULT "iData: $iData"

    elasticKey=$indexName"-"$asfDataID
    logging $scriptName RESULT "elasticKey: $elasticKey"

    logging $scriptName INFO "Обновление индекса"
    logging $scriptName INFO "$elasticHost/$indexName/$indexType/$elasticKey"
    addIndexData "$elasticHost/$indexName/$indexType/$elasticKey" "$iData"
    logging $scriptName RESULT "`cat $tmpfile`"

  done
else
  logging $scriptName INFO "нет новых данных"
fi


echo '' >> $logFile
logging $scriptName INFO "=================== ФГЖС ==================="

mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8;
SELECT 'ФГЖС' name, asfDataID, sendDate, statusKey, statusValue,
'Акмолинская область' regionName, 1 regionKey, 'v93r55ztchws' regionGeoHash,
summa summaZ, summa summaO

FROM (SELECT rg.asfDataID, DATE(a.cmp_key) sendDate,
IF(b.cmp_key != 2, 1, 2) statusKey,
IF(b.cmp_key != 2, 'В работе', 'Одобрено') statusValue,

IFNULL((SELECT a2.cmp_key FROM registry_documents rg2
JOIN asf_data_index a2 ON a2.uuid=rg2.asfDataId AND a2.cmp_id='stoimost'
WHERE rg2.documentid = c.cmp_key), 0)*1 summa

FROM registries r
LEFT JOIN registry_documents rg ON rg.registryID=r.registryID
LEFT JOIN asf_data ad ON ad.uuid=rg.asfDataID
JOIN asf_data_index a ON a.uuid=rg.asfDataId AND a.cmp_id='send_date'
JOIN asf_data_index b ON b.uuid=rg.asfDataId AND b.cmp_id='refuse_desicion_status'
LEFT JOIN asf_data_index c ON c.uuid=rg.asfDataId AND c.cmp_id='projeck'

WHERE rg.deleted IS NULL AND r.code IN ('ФГЖС:_реестр_для_заполнения_заявки_ФГЖС')
AND DATE(ad.modified) = DATE(NOW())
) t;" > $tmpsql 2>/dev/null

if [ -s $tmpsql ];
then
  # Количество строк в файле tmpsql
  countStr=`cat $tmpsql | wc -l`

  for i in `seq 2 $countStr`;
  do
    asfDataID=`cat $tmpsql | sed -n $i'p' | cut -f2`
    organization=`cat $tmpsql | sed -n $i'p' | cut -f1`
    sendDate=`cat $tmpsql | sed -n $i'p' | cut -f3`
    statusKey=`cat $tmpsql | sed -n $i'p' | cut -f4`
    statusValue=`cat $tmpsql | sed -n $i'p' | cut -f5`
    regionName=`cat $tmpsql | sed -n $i'p' | cut -f6`
    regionKey=`cat $tmpsql | sed -n $i'p' | cut -f7`
    regionGeoHash=`cat $tmpsql | sed -n $i'p' | cut -f8`
    amount_r=`cat $tmpsql | sed -n $i'p' | cut -f9`
    amount_a=`cat $tmpsql | sed -n $i'p' | cut -f10`

    echo '' >> $logFile
    logging $scriptName INFO "asfDataID: $asfDataID"

    iData='{"organization": "'$organization'", "sendDate": "'$sendDate'",'
    iData+='"statusKey": '$statusKey', "statusValue": "'$statusValue'",'
    iData+='"regionName": "'$regionName'", "regionKey": "'$regionKey'", "regionGeoHash": "'$regionGeoHash'",'
    iData+='"amount_r": '$amount_r', "amount_a": '$amount_a'}'
    logging $scriptName RESULT "iData: $iData"

    elasticKey=$indexName"-"$asfDataID
    logging $scriptName RESULT "elasticKey: $elasticKey"

    logging $scriptName INFO "Обновление индекса"
    logging $scriptName INFO "$elasticHost/$indexName/$indexType/$elasticKey"
    addIndexData "$elasticHost/$indexName/$indexType/$elasticKey" "$iData"
    logging $scriptName RESULT "`cat $tmpfile`"

  done
else
  logging $scriptName INFO "нет новых данных"
fi


echo '' >> $logFile
logging $scriptName INFO "=================== КППФ ==================="

mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8;
SELECT 'КППФ' name, asfDataID, sendDate, statusKey, statusValue, IFNULL(regionName, 'Акмолинская область') regionName, IFNULL(regionKey, 1) regionKey,
IFNULL(ST_GeoHash(SUBSTRING_INDEX(regionPoint, ',', -1), SUBSTRING_INDEX(regionPoint, ',', 1), 12), 'v93r55ztchws') regionGeoHash, 0 summaZ, 0 summaO
FROM (SELECT asfDataID, sendDate, statusKey, statusValue, regionName, regionKey, getRegionGeoPoint(regionKey) regionPoint
FROM (SELECT rg.asfDataID, DATE(a.cmp_key) sendDate, IF(b.cmp_key != 2, 1, 2) statusKey,  IF(b.cmp_key != 2, 'В работе', 'Одобрено') statusValue,
(SELECT rr.cmp_data FROM registry_documents rg2
LEFT JOIN asf_data_index rr ON rr.uuid=rg2.asfDataID AND rr.cmp_id='region'
WHERE rg2.documentID=c.cmp_key) regionName,
(SELECT rr.cmp_key FROM registry_documents rg2
LEFT JOIN asf_data_index rr ON rr.uuid=rg2.asfDataID AND rr.cmp_id='region'
WHERE rg2.documentID=c.cmp_key) regionKey
FROM registries r
LEFT JOIN registry_documents rg ON rg.registryID=r.registryID
LEFT JOIN asf_data ad ON ad.uuid=rg.asfDataID
JOIN asf_data_index a ON a.uuid=rg.asfDataId AND a.cmp_id='send_date'
JOIN asf_data_index b ON b.uuid=rg.asfDataId AND b.cmp_id='refuse_desicion_status'
LEFT JOIN asf_data_index c ON c.uuid=rg.asfDataID AND c.cmp_id='chastnaia_info_o_proekte'
WHERE rg.deleted IS NULL AND r.code IN ('kppf_ps_main_service_project')
AND DATE(ad.modified) = DATE(NOW())
) t ) t2;" > $tmpsql 2>/dev/null

if [ -s $tmpsql ];
then
  # Количество строк в файле tmpsql
  countStr=`cat $tmpsql | wc -l`

  for i in `seq 2 $countStr`;
  do
    asfDataID=`cat $tmpsql | sed -n $i'p' | cut -f2`
    organization=`cat $tmpsql | sed -n $i'p' | cut -f1`
    sendDate=`cat $tmpsql | sed -n $i'p' | cut -f3`
    statusKey=`cat $tmpsql | sed -n $i'p' | cut -f4`
    statusValue=`cat $tmpsql | sed -n $i'p' | cut -f5`
    regionName=`cat $tmpsql | sed -n $i'p' | cut -f6`
    regionKey=`cat $tmpsql | sed -n $i'p' | cut -f7`
    regionGeoHash=`cat $tmpsql | sed -n $i'p' | cut -f8`
    amount_r=`cat $tmpsql | sed -n $i'p' | cut -f9`
    amount_a=`cat $tmpsql | sed -n $i'p' | cut -f10`

    echo '' >> $logFile
    logging $scriptName INFO "asfDataID: $asfDataID"

    iData='{"organization": "'$organization'", "sendDate": "'$sendDate'",'
    iData+='"statusKey": '$statusKey', "statusValue": "'$statusValue'",'
    iData+='"regionName": "'$regionName'", "regionKey": "'$regionKey'", "regionGeoHash": "'$regionGeoHash'",'
    iData+='"amount_r": '$amount_r', "amount_a": '$amount_a'}'
    logging $scriptName RESULT "iData: $iData"

    elasticKey=$indexName"-"$asfDataID
    logging $scriptName RESULT "elasticKey: $elasticKey"

    logging $scriptName INFO "Обновление индекса"
    logging $scriptName INFO "$elasticHost/$indexName/$indexType/$elasticKey"
    addIndexData "$elasticHost/$indexName/$indexType/$elasticKey" "$iData"
    logging $scriptName RESULT "`cat $tmpfile`"

  done
else
  logging $scriptName INFO "нет новых данных"
fi


echo '' >> $logFile
logging $scriptName INFO "=================== НАТР ==================="

mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8;
SELECT 'НАТР' name, asfDataID, sendDate, statusKey, IF(statusKey != 2, 'В работе', 'Одобрено') statusValue,
'Акмолинская область' regionName, 1 regionKey, 'v93r55ztchws' regionGeoHash, summaZ, summaO

FROM (SELECT rg.asfDataID, DATE(a.cmp_key) sendDate,

IF((SELECT l.cmp_key FROM registries r2 LEFT JOIN registry_documents rg2 ON rg2.registryID=r2.registryID
LEFT JOIN asf_data_index l ON l.uuid=rg2.asfDataId AND l.cmp_id IN ('industry_app', 'enterprises_app', 'comm_ul_app', 'comm_fl_app')
WHERE r2.code = 'natr_odobrennye_zayavki' AND rg2.deleted IS NULL AND l.cmp_key=rg.documentID LIMIT 1) IS NOT NULL, 2, 1) statusKey,

IFNULL((SELECT a2.cmp_key FROM registry_documents rg2
JOIN asf_data_index a2 ON a2.uuid=rg2.asfDataId AND a2.cmp_id='grant_sum'
WHERE rg2.documentid = c.cmp_key), 0)*1 summaZ,

IFNULL((SELECT SUM(s.cmp_key) FROM registries r2 LEFT JOIN registry_documents rg2 ON rg2.registryID=r2.registryID
LEFT JOIN asf_data_index l ON l.uuid=rg2.asfDataId AND l.cmp_id IN ('industry_app', 'enterprises_app', 'comm_ul_app', 'comm_fl_app')
LEFT JOIN asf_data_index s ON s.uuid=rg2.asfDataId AND s.cmp_id IN ('industry_sum', 'enterprises_sum', 'comm_ul_sum', 'comm_fl_sum')
WHERE r2.code = 'natr_odobrennye_zayavki' AND rg2.deleted IS NULL AND l.cmp_key=rg.documentID LIMIT 1), 0)*1 summaO

FROM registries r
LEFT JOIN registry_documents rg ON rg.registryID=r.registryID
LEFT JOIN asf_data ad ON ad.uuid=rg.asfDataID
JOIN asf_data_index a ON a.uuid=rg.asfDataId AND a.cmp_id='send_date'
LEFT JOIN asf_data_index c ON c.uuid=rg.asfDataID AND c.cmp_id='client_data'
WHERE rg.deleted IS NULL AND r.code IN (
'grant_na_razvitie_otraslei',
'grant_na_razvitie_deistvuuschih_predpriyatii',
'grant_na_kommertsializatsiu_ur_litsa',
'grant_na_kommertsializatsiu_fiz_litsa')
AND DATE(ad.modified) = DATE(NOW())
) t

UNION

SELECT 'НАТР' name, asfDataID, sendDate, statusKey, IF(statusKey = 1, 'В работе', 'Одобрено') statusValue,
'Акмолинская область' regionName, 1 regionKey, 'v93r55ztchws' regionGeoHash, t.summ summaZ, IF(statusKey = 1, 0, t.summ) summaO

FROM (SELECT rg.asfDataID, DATE(a.cmp_key) sendDate, IF(b.cmp_key = 2, 2, 1) statusKey, c.cmp_data * 1 summ

FROM registries r
LEFT JOIN registry_documents rg ON rg.registryID=r.registryID
LEFT JOIN asf_data ad ON ad.uuid=rg.asfDataID
JOIN asf_data_index a ON a.uuid=rg.asfDataId AND a.cmp_id='send_date'
LEFT JOIN asf_data_index b ON b.uuid=rg.asfDataId AND b.cmp_id='refuse_desicion_status'
LEFT JOIN asf_data_index c ON c.uuid=rg.asfDataId AND c.cmp_id='textbox-x7xrnz'

WHERE rg.deleted IS NULL AND b.cmp_key IN (1, 2, 4)
AND r.code IN (
'natr_grant_na_kommertsializatsiu_ul_v2',
'natr_grant_na_kommertsializatsiu_fl_v2',
'natr_grant_na_razvitie_predpriyatii_v2',
'natr_grant_na_razvitie_otraslei_v2')
AND DATE(ad.modified) = DATE(NOW())
) t;" > $tmpsql 2>/dev/null


if [ -s $tmpsql ];
then
  # Количество строк в файле tmpsql
  countStr=`cat $tmpsql | wc -l`

  for i in `seq 2 $countStr`;
  do
    asfDataID=`cat $tmpsql | sed -n $i'p' | cut -f2`
    organization=`cat $tmpsql | sed -n $i'p' | cut -f1`
    sendDate=`cat $tmpsql | sed -n $i'p' | cut -f3`
    statusKey=`cat $tmpsql | sed -n $i'p' | cut -f4`
    statusValue=`cat $tmpsql | sed -n $i'p' | cut -f5`
    regionName=`cat $tmpsql | sed -n $i'p' | cut -f6`
    regionKey=`cat $tmpsql | sed -n $i'p' | cut -f7`
    regionGeoHash=`cat $tmpsql | sed -n $i'p' | cut -f8`
    amount_r=`cat $tmpsql | sed -n $i'p' | cut -f9`
    amount_a=`cat $tmpsql | sed -n $i'p' | cut -f10`

    echo '' >> $logFile
    logging $scriptName INFO "asfDataID: $asfDataID"

    iData='{"organization": "'$organization'", "sendDate": "'$sendDate'",'
    iData+='"statusKey": '$statusKey', "statusValue": "'$statusValue'",'
    iData+='"regionName": "'$regionName'", "regionKey": "'$regionKey'", "regionGeoHash": "'$regionGeoHash'",'
    iData+='"amount_r": '$amount_r', "amount_a": '$amount_a'}'
    logging $scriptName RESULT "iData: $iData"

    elasticKey=$indexName"-"$asfDataID
    logging $scriptName RESULT "elasticKey: $elasticKey"

    logging $scriptName INFO "Обновление индекса"
    logging $scriptName INFO "$elasticHost/$indexName/$indexType/$elasticKey"
    addIndexData "$elasticHost/$indexName/$indexType/$elasticKey" "$iData"
    logging $scriptName RESULT "`cat $tmpfile`"

  done
else
  logging $scriptName INFO "нет новых данных"
fi



echo '' >> $logFile
logging $scriptName INFO "=================== КЭГ ==================="

mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8;
SELECT 'КЭГ' name, asfDataID, sendDate, statusKey, IF(statusKey = 1, 'В работе', 'Одобрено') statusValue,
'Акмолинская область' regionName, 1 regionKey, 'v93r55ztchws' regionGeoHash, 0 summaZ, 0 summaO
FROM (SELECT rg.asfDataID, DATE(a.cmp_key) sendDate, IF(b.cmp_key = 2, 2, 1) statusKey
FROM registries r
LEFT JOIN registry_documents rg ON rg.registryID=r.registryID
LEFT JOIN asf_data ad ON ad.uuid=rg.asfDataID
JOIN asf_data_index a ON a.uuid=rg.asfDataId AND a.cmp_id='send_date'
LEFT JOIN asf_data_index b ON b.uuid=rg.asfDataId AND b.cmp_id='refuse_desicion_status'
WHERE rg.deleted IS NULL AND b.cmp_key IN (1, 2, 4)
AND r.code IN (
'keg_strahovanie_eksportnogo_kredita_v2',
'ke_strahovanie_finansovogo_lizinga',
'ke_strahovanie_avansovogo_platezha',
'ke_strahovanie_bankovskoi_platezhnoi_garantii',
'ke_strahovanie_zaima',
'ke_hodaistvo_bnf')
AND DATE(ad.modified) = DATE(NOW())
) t;" > $tmpsql 2>/dev/null


if [ -s $tmpsql ];
then
  # Количество строк в файле tmpsql
  countStr=`cat $tmpsql | wc -l`

  for i in `seq 2 $countStr`;
  do
    asfDataID=`cat $tmpsql | sed -n $i'p' | cut -f2`
    organization=`cat $tmpsql | sed -n $i'p' | cut -f1`
    sendDate=`cat $tmpsql | sed -n $i'p' | cut -f3`
    statusKey=`cat $tmpsql | sed -n $i'p' | cut -f4`
    statusValue=`cat $tmpsql | sed -n $i'p' | cut -f5`
    regionName=`cat $tmpsql | sed -n $i'p' | cut -f6`
    regionKey=`cat $tmpsql | sed -n $i'p' | cut -f7`
    regionGeoHash=`cat $tmpsql | sed -n $i'p' | cut -f8`
    amount_r=`cat $tmpsql | sed -n $i'p' | cut -f9`
    amount_a=`cat $tmpsql | sed -n $i'p' | cut -f10`

    echo '' >> $logFile
    logging $scriptName INFO "asfDataID: $asfDataID"

    iData='{"organization": "'$organization'", "sendDate": "'$sendDate'",'
    iData+='"statusKey": '$statusKey', "statusValue": "'$statusValue'",'
    iData+='"regionName": "'$regionName'", "regionKey": "'$regionKey'", "regionGeoHash": "'$regionGeoHash'",'
    iData+='"amount_r": '$amount_r', "amount_a": '$amount_a'}'
    logging $scriptName RESULT "iData: $iData"

    elasticKey=$indexName"-"$asfDataID
    logging $scriptName RESULT "elasticKey: $elasticKey"

    logging $scriptName INFO "Обновление индекса"
    logging $scriptName INFO "$elasticHost/$indexName/$indexType/$elasticKey"
    addIndexData "$elasticHost/$indexName/$indexType/$elasticKey" "$iData"
    logging $scriptName RESULT "`cat $tmpfile`"

  done
else
  logging $scriptName INFO "нет новых данных"
fi


echo '' >> $logFile
logging $scriptName STATUS "Удаление временных файлов"
rm -rf $tmpsql
rm -rf $tmpfile
logging $scriptName END "Завершение работы скрипта"
exit 0
