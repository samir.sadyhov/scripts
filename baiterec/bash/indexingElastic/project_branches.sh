#!/bin/bash

source $(dirname $(readlink -e $0))/utils.class
scriptName=${0##*/}
tmpsql="/tmp/project_branches.sql"

indexName='project_branches'
indexType='pb'

echo '' >> $logFile
logging $scriptName START "Начало работы скрипта"
logging $scriptName INFO "Собираем данные для индекса $indexName"

echo '' >> $logFile
logging $scriptName INFO "Реестр: КППФ ПС: Частные данные о проекте"
registryID=$(getRegistryId 'kppf_ps_project_partial_data')

mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8;
SELECT rg.asfDataID FROM registry_documents rg
LEFT JOIN asf_data ad ON ad.uuid=rg.asfDataID
WHERE rg.deleted IS NULL AND rg.registryID='$registryID'
AND DATE(ad.modified) = DATE(NOW());" > $tmpsql

if [ -s $tmpsql ];
then
  # Количество строк в файле tmpsql
  countStr=`cat $tmpsql | wc -l`

  for i in `seq 2 $countStr`;
  do
    tmpuuid=`cat $tmpsql | sed -n $i'p' | cut -f1`
    echo '' >> $logFile
    logging $scriptName INFO "asfDataID: $tmpuuid"

    getAsfData "$tmpuuid"
    jsonString=`cat $tmpfile`
    asfData=$(getJsonObject "$jsonString" "data")

    gpiirObj=$(searchInJsonArr "$asfData" "gpiir")
    gpiirValue=$(getJsonObject "$gpiirObj" "value")
    okedObj=$(searchInJsonArr "$asfData" "oked")
    okedValue=$(getJsonObject "$okedObj" "value")
    project_costObj=$(searchInJsonArr "$asfData" "project_cost")
    project_costValue=$(getJsonObject "$project_costObj" "value")

    if [ "$gpiirValue" != "null" ]; then
      gpiirValue=$(echo $gpiirValue | cut -c 2- | rev | cut -c 2- | rev | sed 's/\\/\\\\/g;s/\"/\\"/g;s/\«/\\"/g;s/'"'"'/\\"/g;s/\»/\\"/g')
      okedValue=$(echo $okedValue | cut -c 2- | rev | cut -c 2- | rev | sed 's/\\/\\\\/g;s/\"/\\"/g;s/\«/\\"/g;s/'"'"'/\\"/g;s/\»/\\"/g')
      project_costValue=$(echo $project_costValue | cut -c 2- | rev | cut -c 2- | rev | sed 's/ //g' )
      if [ "$project_costValue" == "-" ]; then
        project_costValue="0"
      fi
      project_costValue=$(( $project_costValue*1 ))

      iData='{"project_cost": "'$project_costValue'", "gpiir": "'$gpiirValue'", "oked": "'$okedValue'"}'
      logging $scriptName RESULT "iData: $iData"

      elasticKey="project_branches-"$tmpuuid
      logging $scriptName RESULT "elasticKey: $elasticKey"

      logging $scriptName INFO "Обновление индекса"
      logging $scriptName INFO "$elasticHost/$indexName/$indexType/$elasticKey"
      addIndexData "$elasticHost/$indexName/$indexType/$elasticKey" "$iData"
      logging $scriptName RESULT "`cat $tmpfile`"
    else
      logging $scriptName RESULT "Пустая запись, пропускаем"
    fi

  done
else
  logging $scriptName INFO "нет новых данных в реестре КППФ ПС: Частные данные о проекте"
fi



echo '' >> $logFile
logging $scriptName INFO "Реестр: НУХ Пул проектов. Частные данные о проекте"
registryID=$(getRegistryId 'nuh_pul_proektov__chastnye_dannye_o_proekte')

mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8;
SELECT rg.asfDataID FROM registry_documents rg
LEFT JOIN asf_data ad ON ad.uuid=rg.asfDataID
WHERE rg.deleted IS NULL AND rg.registryID='$registryID'
AND DATE(ad.modified) = DATE(NOW())" > $tmpsql

if [ -s $tmpsql ];
then
  # Количество строк в файле tmpsql
  countStr=`cat $tmpsql | wc -l`

  for i in `seq 2 $countStr`;
  do
    tmpuuid=`cat $tmpsql | sed -n $i'p' | cut -f1`
    echo '' >> $logFile
    logging $scriptName INFO "asfDataID: $tmpuuid"

    getAsfData "$tmpuuid"
    jsonString=`cat $tmpfile`
    asfData=$(getJsonObject "$jsonString" "data")
    okedObj=$(searchInJsonArr "$asfData" "oked")
    okedValue=$(getJsonObject "$okedObj" "value")
    project_costObj=$(searchInJsonArr "$asfData" "project_cost")
    project_costValue=$(getJsonObject "$project_costObj" "value")

    if [ "$okedValue" != "null" ]; then
      okedValue=$(echo $okedValue | cut -c 2- | rev | cut -c 2- | rev | sed 's/\\/\\\\/g;s/\"/\\"/g;s/\«/\\"/g;s/'"'"'/\\"/g;s/\»/\\"/g')
      project_costValue=$(echo $project_costValue | cut -c 2- | rev | cut -c 2- | rev | sed 's/ //g' )
      if [ "$project_costValue" == "-" ]; then
        project_costValue="0"
      fi
      project_costValue=$(( $project_costValue*1 ))

      iData='{"project_cost": "'$project_costValue'", "gpiir": "", "oked": "'$okedValue'"}'
      logging $scriptName RESULT "iData: $iData"

      elasticKey="project_branches-"$tmpuuid
      logging $scriptName RESULT "elasticKey: $elasticKey"

      logging $scriptName INFO "Обновление индекса"
      logging $scriptName INFO "$elasticHost/$indexName/$indexType/$elasticKey"
      addIndexData "$elasticHost/$indexName/$indexType/$elasticKey" "$iData"
      logging $scriptName RESULT "`cat $tmpfile`"
    else
      logging $scriptName RESULT "Пустая запись, пропускаем"
    fi

  done
else
  logging $scriptName INFO "нет новых данных в реестре НУХ Пул проектов. Частные данные о проекте"
fi


echo '' >> $logFile
logging $scriptName STATUS "Удаление временных файлов"
rm -rf $tmpsql
rm -rf $tmpfile
logging $scriptName END "Завершение работы скрипта"
exit 0
