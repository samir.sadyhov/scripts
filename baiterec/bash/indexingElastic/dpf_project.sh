#!/bin/bash

source $(dirname $(readlink -e $0))/utils.class
scriptName=${0##*/}
tmpsql=$(mktemp)

indexName='dpf_project'
indexType='dp'

echo '' >> $logFile
logging $scriptName START "Начало работы скрипта"
logging $scriptName INFO "Собираем данные для индекса $indexName"

registryID=$(getRegistryId 'nuh_pul_proektov__zayavka')

mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8;
SELECT rg.asfDataID FROM registry_documents rg
LEFT JOIN asf_data ad ON ad.uuid=rg.asfDataID
WHERE rg.deleted IS NULL AND rg.registryID = '$registryID'
AND DATE(ad.modified) = DATE(NOW());" > $tmpsql 2>/dev/null

if [ -s $tmpsql ];
then
  # Количество строк в файле tmpsql
  countStr=`cat $tmpsql | wc -l`

  for i in `seq 2 $countStr`;
  do
    tmpuuid=`cat $tmpsql | sed -n $i'p' | cut -f1`
    echo '' >> $logFile
    logging $scriptName INFO "asfDataID: $tmpuuid"

    getAsfData "$tmpuuid"
    jsonString=`cat $tmpfile`
    asfData=$(getJsonObject "$jsonString" "data")

    send_dateObj=$(searchInJsonArr "$asfData" "send_date")
    send_dateKey=$(getJsonObject "$send_dateObj" "key")

    if [ "$send_dateKey" != "null" ]; then
      send_dateKey=$(echo $send_dateKey | cut -c 2- | rev | cut -c 2- | rev )
      send_dateKey=$(date.parse $send_dateKey)

      statusObj=$(searchInJsonArr "$asfData" "refuse_desicion_status")
      statusKey=$(getJsonObject "$statusObj" "key")

      if [ "$statusKey" != "null" ]; then
        statusKey=$(echo $statusKey | cut -c 2- | rev | cut -c 2- | rev )
        statusValue=$(getJsonObject "$statusObj" "value")
        statusValue=$(echo $statusValue | cut -c 2- | rev | cut -c 2- | rev )

        projInfoObj=$(searchInJsonArr "$asfData" "chast_project")
        projInfoKey=$(getJsonObject "$projInfoObj" "key")
        projInfoKey=$(echo $projInfoKey | cut -c 2- | rev | cut -c 2- | rev )
        projInfoKey=$(getDataUUID $projInfoKey)

        getAsfData "$projInfoKey"
        jsonProjInfo=`cat $tmpfile`
        projInfoData=$(getJsonObject "$jsonProjInfo" "data")

        okedObj=$(searchInJsonArr "$projInfoData" "oked1")
        okedKey=$(getJsonObject "$okedObj" "key")
        okedValue=$(getJsonObject "$okedObj" "value")

        if [ "$okedKey" == "null" ]; then
          okedKey=""
        else
          okedKey=$(echo $okedKey | cut -c 2- | rev | cut -c 2- | rev )
        fi
        if [ "$okedValue" == "null" ]; then
          okedValue="Не указан"
        else
          okedValue=$(echo $okedValue | cut -c 2- | rev | cut -c 2- | rev )
        fi

        gpiirObj=$(searchInJsonArr "$projInfoData" "gpiir")
        gpiirKey=$(getJsonObject "$gpiirObj" "key")
        gpiirValue=$(getJsonObject "$gpiirObj" "value")

        if [ "$gpiirKey" == "null" ]; then
          gpiirKey=""
        else
          gpiirKey=$(echo $gpiirKey | cut -c 2- | rev | cut -c 2- | rev )
        fi
        if [ "$gpiirValue" == "null" ]; then
          gpiirValue="Не указан"
        else
          gpiirValue=$(echo $gpiirValue | cut -c 2- | rev | cut -c 2- | rev )
        fi

        regionObj=$(searchInJsonArr "$projInfoData" "region")
        regionKey=$(getJsonObject "$regionObj" "key")
        regionValue=$(getJsonObject "$regionObj" "value")

        regionLat="48.548043"
        regoinLon="66.904544"

        if [ "$regionKey" == "null" ]; then
          regionKey=""
        else
          regionKey=$(echo $regionKey | cut -c 2- | rev | cut -c 2- | rev )
          regionLat=$(getRegionLatLon "$regionKey" 2)
          regoinLon=$(getRegionLatLon "$regionKey" 3)
        fi
        if [ "$regionValue" == "null" ]; then
          regionValue="Не указан"
        else
          regionValue=$(echo $regionValue | cut -c 2- | rev | cut -c 2- | rev )
        fi

        regionPoint=$(getGeoHash "$regoinLon" "$regionLat")

        supportiveTable=$(searchInJsonArr "$projInfoData" "supportive_measures_table")
        supportiveTable=$(getJsonObject "$supportiveTable" "data")

        supportiveObj=$(searchInJsonArr "$supportiveTable" "supportive_measures-b1")
        supportiveKey=$(getJsonObject "$supportiveObj" "key")
        supportiveValue=$(getJsonObject "$supportiveObj" "value")

        if [ "$supportiveKey" == "null" ]; then
          supportiveKey=""
        else
          supportiveKey=$(echo $supportiveKey | cut -c 2- | rev | cut -c 2- | rev )
        fi
        if [ "$supportiveValue" == "null" ]; then
          supportiveValue="Не указан"
        else
          supportiveValue=$(echo $supportiveValue | cut -c 2- | rev | cut -c 2- | rev )
        fi

        iData='{"send_date": "'$send_dateKey'", "status": "'$statusKey'", "statusName": "'$statusValue'",'
        iData+='"okedKey": "'$okedKey'", "okedValue": "'$okedValue'",'
        iData+='"gpiirKey": "'$gpiirKey'", "gpiirValue": "'$gpiirValue'",'
        iData+='"supportiveKey": "'$supportiveKey'", "supportiveValue": "'$supportiveValue'",'
        iData+='"regionKey": "'$regionKey'", "regionValue": "'$regionValue'", "regionPoint": "'$regionPoint'"}'

        logging $scriptName RESULT "iData: $iData"

        elasticKey=$indexName"-"$tmpuuid
        logging $scriptName RESULT "elasticKey: $elasticKey"

        logging $scriptName INFO "Обновление индекса"
        logging $scriptName INFO "$elasticHost/$indexName/$indexType/$elasticKey"
        addIndexData "$elasticHost/$indexName/$indexType/$elasticKey" "$iData"
        logging $scriptName RESULT "`cat $tmpfile`"
      else
        logging $scriptName RESULT "Не указан статус, пропускаем"
      fi

    else
      logging $scriptName RESULT "Заявка не отправлена, пропускаем"
    fi

  done
else
  logging $scriptName INFO "нет новых данных в реестре"
fi


echo '' >> $logFile
logging $scriptName STATUS "Удаление временных файлов"
rm -rf $tmpsql
rm -rf $tmpfile
logging $scriptName END "Завершение работы скрипта"
exit 0
