#!/bin/bash

source $(dirname $(readlink -e $0))/utils.class
scriptName=${0##*/}
tmpsql=$(mktemp)

indexName='apps_stat'
indexType='as'

# type: 0 - рабочих дней; 1 - календарных дней;
rInfo='[
  {
    "code": "ФГЖС:_реестр_для_заполнения_заявки_ФГЖС",
    "organization": "ФГЖС",
    "srok": "15",
    "type": "0",
    "serviceName": "Гарантия на завершение жилищного строительства"
  },
  {
    "code": "bd_main_service_project",
    "organization": "БД",
    "srok": "10",
    "type": "0",
    "serviceName": "Рассмотрение проектов жилищного строительства на предмет осуществления финансирования"
  },
  {
    "code": "bd_pay_main_service",
    "organization": "БД",
    "srok": "10",
    "type": "0",
    "serviceName": "Рассмотрение документов по оплате по актам выполненных работ"
  },
  {
    "code": "bd_zayavlenie_po_expo",
    "organization": "БД",
    "srok": "15",
    "type": "1",
    "serviceName": "Приобретение объектов недвижимости в МЖК на территории ЭКСПО"
  },
  {
    "code": "bd_priobretenie_zhilya",
    "organization": "БД",
    "srok": "3",
    "type": "0",
    "serviceName": "Приобретение жилья по программе «Нұрлы жер» в регионах Казахстана"
  },
  {
    "code": "bd_nurly_zher",
    "organization": "БД",
    "srok": "10",
    "type": "0",
    "serviceName": "Финансирование проектов жилищного строительства через размещение обусловленных вкладов АО \"Байтерек девелопмент\" в рамках программы жилищного строительства \"Нұрлы жер\""
  },
  {
    "code": "bd_zayavka_na_finansirovanie_zhil_proektov_sob_sredstva",
    "organization": "БД",
    "srok": "10",
    "type": "0",
    "serviceName": "Финансирование жилищных инвестиционных проектов АО \"Байтерек девелопмент\" в городах Астана, Алматы. Обусловленное финансирование проектов через БВУ за счет собственных средств общества"
  },
  {
    "code": "bd_zayavka_na_finansirovanie_zhil_proektov_sredstva_mfo",
    "organization": "БД",
    "srok": "10",
    "type": "0",
    "serviceName": "Финансирование жилищных инвестиционных проектов АО \"Байтерек девелопмент\" в городах Астана, Алматы. Обусловленное финансирование проектов через БВУ за счет средств МФО"
  },
  {
    "code": "bd_zayavka_na_finansirovanie_zhil_proektov_pryamoe_finansirovanie",
    "organization": "БД",
    "srok": "10",
    "type": "0",
    "serviceName": "Финансирование жилищных инвестиционных проектов АО \"Байтерек девелопмент\" в городах Астана, Алматы. Реализация проектов за счет средств МФО посредством прямого финансирования"
  },
  {
    "code": "bd_sots_proekty",
    "organization": "БД",
    "srok": "10",
    "type": "0",
    "serviceName": "Финансирование инвестиционных социально-ориентированных проектов АО \"Байтерек девелопмент\""
  },
  {
    "code": "bd_pay_main_service",
    "organization": "БД",
    "srok": "10",
    "type": "0",
    "serviceName": "Рассмотрение документов по оплате по актам выполненных работ"
  },
  {
    "code": "kppf_ps_main_service_project",
    "organization": "KPPF",
    "srok": "7",
    "type": "0",
    "serviceName": "Подать проектное предложение"
  },
  {
    "code": "nuh_pul_proektov__zayavka",
    "organization": "НУХ \"Байтерек\"",
    "srok": "15",
    "type": "1",
    "serviceName": "Подача заявления на финансирование проекта"
  },
  {
    "code": "nuh_pul_investorov__zayavka",
    "organization": "НУХ \"Байтерек\"",
    "srok": "12",
    "type": "1",
    "serviceName": "Зарегистрироваться как инвестор"
  },
  {
    "code": "grant_na_razvitie_otraslei",
    "organization": "НАТР",
    "srok": "100",
    "type": "0",
    "serviceName": "Предоставление инновационных грантов на технологическое развитие отраслей"
  },
  {
    "code": "grant_na_razvitie_deistvuuschih_predpriyatii",
    "organization": "НАТР",
    "srok": "100",
    "type": "0",
    "serviceName": "Предоставление инновационных грантов на технологическое развитие действующих предприятий"
  },
  {
    "code": "grant_na_kommertsializatsiu_fiz_litsa",
    "organization": "НАТР",
    "srok": "83",
    "type": "0",
    "serviceName": "Предоставление инновационных грантов на коммерциализацию технологий (для физ. лиц)"
  },
  {
    "code": "grant_na_kommertsializatsiu_ur_litsa",
    "organization": "НАТР",
    "srok": "83",
    "type": "0",
    "serviceName": "Предоставление инновационных грантов на коммерциализацию технологий (для юр. лиц)"
  },
  {
    "code": "natr_grant_na_kommertsializatsiu_ul_v2",
    "organization": "НАТР",
    "srok": "83",
    "type": "0",
    "serviceName": "Предоставление инновационных грантов на коммерциализацию технологий (для юр. лиц)"
  },
  {
    "code": "natr_grant_na_kommertsializatsiu_fl_v2",
    "organization": "НАТР",
    "srok": "83",
    "type": "0",
    "serviceName": "Предоставление инновационных грантов на коммерциализацию технологий (для физ. лиц)"
  },
  {
    "code": "natr_grant_na_razvitie_predpriyatii_v2",
    "organization": "НАТР",
    "srok": "83",
    "type": "0",
    "serviceName": "Предоставление инновационных грантов на технологическое развитие действующих предприятий"
  },
  {
    "code": "natr_grant_na_razvitie_otraslei_v2",
    "organization": "НАТР",
    "srok": "83",
    "type": "0",
    "serviceName": "Предоставление инновационных грантов на технологическое развитие отраслей"
  },
  {
    "code": "keg_strahovanie_eksportnogo_kredita_v2",
    "organization": "KazakhExport",
    "srok": "20",
    "type": "1",
    "serviceName": "Страхование кредита экспортера (Страхование отсрочки платежа по экспортному контракту)"
  },
  {
    "code": "ke_strahovanie_finansovogo_lizinga",
    "organization": "KazakhExport",
    "srok": "35",
    "type": "0",
    "serviceName": "Добровольное страхование финансового лизинга"
  },
  {
    "code": "ke_strahovanie_avansovogo_platezha",
    "organization": "KazakhExport",
    "srok": "28",
    "type": "1",
    "serviceName": "Страхование ответственности экспортера по возврату авансового платежа"
  },
  {
    "code": "ke_strahovanie_bankovskoi_platezhnoi_garantii",
    "organization": "KazakhExport",
    "srok": "35",
    "type": "1",
    "serviceName": "Страхование банковских платежных гарантий"
  },
  {
    "code": "ke_strahovanie_zaima",
    "organization": "KazakhExport",
    "srok": "35",
    "type": "1",
    "serviceName": "Добровольное страхование займов"
  },
  {
    "code": "ke_hodaistvo_bnf",
    "organization": "KazakhExport",
    "srok": "28",
    "type": "1",
    "serviceName": "Экспортное торговое финансирование"
  }
]'

echo '' >> $logFile
logging $scriptName START "Начало работы скрипта"
logging $scriptName INFO "Собираем данные для индекса $indexName"

rInfoLength=$(getRowsCount "$rInfo" "code")

for ((i=0; i<$rInfoLength; i++))
do
  tmpObj=$(echo $rInfo | jq '.['$i']')

  code=$(getJsonObject "$tmpObj" "code")
  code=$(echo $code | cut -c 2- | rev | cut -c 2- | rev )

  organization=$(getJsonObject "$tmpObj" "organization")
  organization=$(echo $organization | cut -c 2- | rev | cut -c 2- | rev )

  srok=$(getJsonObject "$tmpObj" "srok")
  srok=$(echo $srok | cut -c 2- | rev | cut -c 2- | rev )

  serviceName=$(getJsonObject "$tmpObj" "serviceName")
  serviceName=$(echo $serviceName | cut -c 2- | rev | cut -c 2- | rev )

  type=$(getJsonObject "$tmpObj" "type")
  type=$(echo $type | cut -c 2- | rev | cut -c 2- | rev )

  registryID=$(getRegistryId $code)

  echo '' >> $logFile
  logging $scriptName INFO "registryCode: [$code]\nregistryID: [$registryID]\norganization: [$organization]"

  mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8;
  SELECT rg.asfDataID FROM registry_documents rg
  LEFT JOIN asf_data ad ON ad.uuid=rg.asfDataID
  LEFT JOIN asf_data_index b ON b.uuid = rg.asfDataID AND b.cmp_id='refuse_desicion_status'
  WHERE rg.deleted IS NULL AND rg.registryID = '$registryID' AND b.cmp_key != 0
  AND DATE(ad.modified) = DATE(NOW());" > $tmpsql 2>/dev/null

  if [ -s $tmpsql ];
  then
    # Количество строк в файле tmpsql
    countStr=`cat $tmpsql | wc -l`

    for j in `seq 2 $countStr`;
    do
      tmpuuid=`cat $tmpsql | sed -n $j'p' | cut -f1`
      echo '' >> $logFile
      logging $scriptName INFO "asfDataID: $tmpuuid"

      getAsfData "$tmpuuid"
      jsonString=`cat $tmpfile`
      asfData=$(getJsonObject "$jsonString" "data")

      modified=$(getJsonObject "$jsonString" "modified")
      modified=$(echo $modified | cut -c 2- | rev | cut -c 2- | rev )

      doc_numberObj=$(searchInJsonArr "$asfData" "app_num")
      doc_numberValue=$(getJsonObject "$doc_numberObj" "value")
      doc_numberValue=$(echo $doc_numberValue | cut -c 2- | rev | cut -c 2- | rev )

      refuse_desicion_statusObj=$(searchInJsonArr "$asfData" "refuse_desicion_status")
      refuse_desicion_statusValue=$(getJsonObject "$refuse_desicion_statusObj" "value")
      refuse_desicion_statusKey=$(getJsonObject "$refuse_desicion_statusObj" "key")
      refuse_desicion_statusValue=$(echo $refuse_desicion_statusValue | cut -c 2- | rev | cut -c 2- | rev )
      refuse_desicion_statusKey=$(echo $refuse_desicion_statusKey | cut -c 2- | rev | cut -c 2- | rev )

      srokString="не просрочено"

      send_dateObj=$(searchInJsonArr "$asfData" "send_date")
      send_dateKey=$(getJsonObject "$send_dateObj" "key")

      if [ "$send_dateKey" != "null" ]; then
        send_dateKey=$(echo $send_dateKey | cut -c 2- | rev | cut -c 2- | rev )

        days=$(date.dateToSec $send_dateKey)
        days=$(( $days/(360*24)+$srok ))
        currDays=$(date.dateToSec `now_time`)
        if [ $currDays > $days ]; then
          srokString="просрочено"
        fi

        send_dateKey=$(date.parse $send_dateKey)

        finish_date=""
        if [[ ("$refuse_desicion_statusKey" == "2") || ("$refuse_desicion_statusKey" == "3") ]]; then
          finish_date=$modified
        fi

        iData='{"doc_number": "'$doc_numberValue'"'
        iData+=',"organization_value": "'$organization'", "organization_key": "'$code'"'
        iData+=',"send_date": "'$send_dateKey'"'
        iData+=',"refuse_desicion_status_value": "'$refuse_desicion_statusValue'", "refuse_desicion_status_key": "'$refuse_desicion_statusKey'"'
        iData+=',"srok": "'$srokString'"'
        iData+=',"finish_date": "'$finish_date'"'
        iData+=',"service_name": "'$serviceName'"}'

        logging $scriptName RESULT "iData: $iData"

        elasticKey=$indexName"-"$tmpuuid
        logging $scriptName RESULT "elasticKey: $elasticKey"
        logging $scriptName INFO "Обновление индекса"
        logging $scriptName INFO "$elasticHost/$indexName/$indexType/$elasticKey"
        addIndexData "$elasticHost/$indexName/$indexType/$elasticKey" "$iData"
        logging $scriptName RESULT "`cat $tmpfile`"

      else
        logging $scriptName INFO "Не указана дата отправки"
      fi


    done
  else
    logging $scriptName INFO "нет новых данных в реесте"
  fi

done

echo '' >> $logFile
logging $scriptName STATUS "Удаление временных файлов"
rm -rf $tmpsql
rm -rf $tmpfile
logging $scriptName END "Завершение работы скрипта"
exit 0
