#!/bin/bash

source $(dirname $(readlink -e $0))/utils.class
scriptName=${0##*/}
tmpsql=$(mktemp)

indexName='expo_sold_apartments'
indexType='esa'

echo '' >> $logFile
logging $scriptName START "Начало работы скрипта"
logging $scriptName INFO "Собираем данные для индекса $indexName"

mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8;
SELECT rg.asfDataID, DATE(a.cmp_key) sendDate,
c.cmp_data sredstvaKey, c.cmp_key sredstvaValue,
k3.cmp_data rooms,
CASE k3.cmp_data
WHEN '1' THEN '1 - комн. кв.'
WHEN '2' THEN '2 - комн. кв.'
WHEN '3' THEN '3 - комн. кв.'
WHEN '4' THEN '4 - комн. кв.'
END roomsText,
CONCAT(k1.cmp_data,k2.cmp_data) prov,

IFNULL(SUM(k4.cmp_key), 0) flat_cost,
IFNULL(SUM(k5.cmp_key), 0) parking_cost,
ROUND(IFNULL(SUM(k4.cmp_key), 0)/1000000, 1) flat_costMillion,
ROUND(IFNULL(SUM(k5.cmp_key), 0)/1000000, 1) parking_costMillion

FROM registry_documents rg
LEFT JOIN asf_data ad ON ad.uuid = rg.asfDataID
LEFT JOIN asf_data_index a ON a.uuid = rg.asfDataID AND a.cmp_id='send_date'
LEFT JOIN asf_data_index b ON b.uuid = rg.asfDataID AND b.cmp_id='refuse_desicion_status'
LEFT JOIN asf_data_index c ON c.uuid = rg.asfDataID AND c.cmp_id='radio-je3ut'
LEFT JOIN asf_data_index k1 ON k1.uuid = rg.asfDataID AND k1.cmp_id LIKE 'section-%'
LEFT JOIN asf_data_index k2 ON k2.uuid = rg.asfDataID AND k2.cmp_id = CONCAT('flat-b', k1.cmp_row)
LEFT JOIN asf_data_index k3 ON k3.uuid = rg.asfDataID AND k3.cmp_id = CONCAT('rooms-b', k1.cmp_row)
LEFT JOIN asf_data_index k4 ON k4.uuid = rg.asfDataID AND k4.cmp_id = CONCAT('flat_cost-b', k1.cmp_row)
LEFT JOIN asf_data_index k5 ON k5.uuid = rg.asfDataID AND k5.cmp_id = CONCAT('parking_cost-b', k1.cmp_row)

WHERE rg.registryID = getRegistryIdByCode('bd_zayavlenie_po_expo')
AND rg.deleted IS NULL AND b.cmp_key = 2 AND k1.cmp_data != ''
GROUP BY prov;" > $tmpsql 2>/dev/null

if [ -s $tmpsql ];
then
  # Количество строк в файле tmpsql
  countStr=`cat $tmpsql | wc -l`

  for i in `seq 2 $countStr`;
  do
    asfDataID=`cat $tmpsql | sed -n $i'p' | cut -f1`
    sendDate=`cat $tmpsql | sed -n $i'p' | cut -f2`
    sredstvaKey=`cat $tmpsql | sed -n $i'p' | cut -f3`
    sredstvaValue=`cat $tmpsql | sed -n $i'p' | cut -f4`
    rooms=`cat $tmpsql | sed -n $i'p' | cut -f5`
    roomsText=`cat $tmpsql | sed -n $i'p' | cut -f6`
    prov=`cat $tmpsql | sed -n $i'p' | cut -f7`
    flat_cost=`cat $tmpsql | sed -n $i'p' | cut -f8`
    parking_cost=`cat $tmpsql | sed -n $i'p' | cut -f9`
    flat_costMillion=`cat $tmpsql | sed -n $i'p' | cut -f10`
    parking_costMillion=`cat $tmpsql | sed -n $i'p' | cut -f11`

    echo '' >> $logFile
    logging $scriptName INFO "asfDataID: $asfDataID"

    iData='{"sendDate": "'$sendDate'",'
    iData+='"sredstvaKey": '$sredstvaKey', "sredstvaValue": "'$sredstvaValue'",'
    iData+='"flat_cost": '$flat_cost', "parking_cost": '$parking_cost','
    iData+='"flat_costMillion": '$flat_costMillion', "parking_costMillion": '$parking_costMillion','
    iData+='"rooms": '$rooms', "roomsText": "'$roomsText'"}'
    logging $scriptName RESULT "iData: $iData"

    elasticKey=$indexName"-"$asfDataID"-"$i
    logging $scriptName RESULT "elasticKey: $elasticKey"

    logging $scriptName INFO "Обновление индекса"
    logging $scriptName INFO "$elasticHost/$indexName/$indexType/$elasticKey"
    addIndexData "$elasticHost/$indexName/$indexType/$elasticKey" "$iData"
    logging $scriptName RESULT "`cat $tmpfile`"

  done
else
  logging $scriptName INFO "нет новых данных"
fi

echo '' >> $logFile
logging $scriptName STATUS "Удаление временных файлов"
rm -rf $tmpsql
rm -rf $tmpfile
logging $scriptName END "Завершение работы скрипта"
exit 0
