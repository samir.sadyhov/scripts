#!/bin/bash

source $(dirname $(readlink -e $0))/utils.class
scriptName=${0##*/}
tmpsql=$(mktemp)

indexName='ke_strahovanie'
indexType='ks'

echo '' >> $logFile
logging $scriptName START "Начало работы скрипта"
logging $scriptName INFO "Собираем данные для индекса $indexName"


mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8;
SELECT asfDataID, usluga, sendDate, statusKey, statusValue,
IFNULL(regionName, 'Акмолинская область') regionName, IFNULL(regionKey, 1) regionKey,
IFNULL(ST_GeoHash(SUBSTRING_INDEX(regionPoint, ',', -1), SUBSTRING_INDEX(regionPoint, ',', 1), 12), 'v93r55ztchws') regionGeoHash

FROM (
SELECT rg.asfDataID, keg.usluga, DATE(a.cmp_key) sendDate, b.cmp_key statusKey,
CASE b.cmp_key
WHEN 1 THEN 'В работе'
WHEN 2 THEN 'Одобрено'
WHEN 3 THEN 'Отказано'
WHEN 4 THEN 'На доработке'
END statusValue,
c.cmp_data regionName, c.cmp_key regionKey, getRegionGeoPoint(c.cmp_key) regionPoint

FROM (
SELECT 'keg_strahovanie_eksportnogo_kredita_v2' code, 'Страхование экспортного кредита' usluga
UNION SELECT 'ke_strahovanie_avansovogo_platezha', 'Страхование авансового платежа'
UNION SELECT 'ke_strahovanie_bankovskoi_platezhnoi_garantii', 'Страхование банковской платежной гарантии'
UNION SELECT 'ke_strahovanie_zaima', 'Страхование займа'
UNION SELECT 'ke_strahovanie_finansovogo_lizinga', 'Страхование финансового лизинга'
UNION SELECT 'ke_hodaistvo_bnf', 'Экспортное торговое финансирование'
) keg

LEFT JOIN registries r ON r.code = keg.code
JOIN registry_documents rg ON rg.registryID = r.registryID
LEFT JOIN asf_data ad ON ad.uuid = rg.asfDataID
JOIN asf_data_index a ON a.uuid = rg.asfDataID AND a.cmp_id = 'send_date'
LEFT JOIN asf_data_index b ON b.uuid = rg.asfDataID AND b.cmp_id = 'refuse_desicion_status'
LEFT JOIN asf_data_index c ON c.uuid = rg.asfDataID AND c.cmp_id = 'listbox-dkqys7'

WHERE rg.deleted IS NULL AND b.cmp_key != 0

) t2;" > $tmpsql 2>/dev/null
#AND DATE(ad.modified) = DATE(NOW())

if [ -s $tmpsql ];
then
  # Количество строк в файле tmpsql
  countStr=`cat $tmpsql | wc -l`

  for i in `seq 2 $countStr`;
  do
    asfDataID=`cat $tmpsql | sed -n $i'p' | cut -f1`
    usluga=`cat $tmpsql | sed -n $i'p' | cut -f2`
    sendDate=`cat $tmpsql | sed -n $i'p' | cut -f3`
    statusKey=`cat $tmpsql | sed -n $i'p' | cut -f4`
    statusValue=`cat $tmpsql | sed -n $i'p' | cut -f5`
    regionName=`cat $tmpsql | sed -n $i'p' | cut -f6`
    regionKey=`cat $tmpsql | sed -n $i'p' | cut -f7`
    regionGeoHash=`cat $tmpsql | sed -n $i'p' | cut -f8`

    echo '' >> $logFile
    logging $scriptName INFO "asfDataID: $asfDataID"

    iData='{"usluga": "'$usluga'", "sendDate": "'$sendDate'",'
    iData+='"statusKey": '$statusKey', "statusValue": "'$statusValue'",'
    iData+='"regionName": "'$regionName'", "regionKey": "'$regionKey'", "regionGeoHash": "'$regionGeoHash'"}'
    logging $scriptName RESULT "iData: $iData"

    elasticKey=$indexName"-"$asfDataID
    logging $scriptName RESULT "elasticKey: $elasticKey"

    logging $scriptName INFO "Обновление индекса"
    logging $scriptName INFO "$elasticHost/$indexName/$indexType/$elasticKey"
    addIndexData "$elasticHost/$indexName/$indexType/$elasticKey" "$iData"
    logging $scriptName RESULT "`cat $tmpfile`"

  done
else
  logging $scriptName INFO "нет новых данных"
fi

echo '' >> $logFile
logging $scriptName STATUS "Удаление временных файлов"
rm -rf $tmpsql
rm -rf $tmpfile
logging $scriptName END "Завершение работы скрипта"
exit 0
