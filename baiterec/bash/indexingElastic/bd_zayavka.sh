#!/bin/bash

source $(dirname $(readlink -e $0))/utils.class
scriptName=${0##*/}
tmpsql=$(mktemp)
tmpsql2=$(mktemp)

indexName='bd_zayavka'
indexType='bz'

rInfo='[
  {
    "zayavRegCode": "bd_nurly_zher",
    "zayavName": "Заявка на финансирование в рамках программы Нурлы Жер"
  },
  {
    "zayavRegCode": "bd_zayavka_na_finansirovanie_zhil_proektov_pryamoe_finansirovanie",
    "zayavName": "Заявка на финансирование жилищных инвестиционных проектов. Прямое финансирование"
  },
  {
    "zayavRegCode": "bd_zayavka_na_finansirovanie_zhil_proektov_sredstva_mfo",
    "zayavName": "Заявка на финансирование жилищных инвестиционных проектов. Средства МФО"
  },
  {
    "zayavRegCode": "bd_zayavka_na_finansirovanie_zhil_proektov_sob_sredstva",
    "zayavName": "Заявка на финансирование жилищных инвестиционных проектов. Собственные средства"
  }
]'

echo '' >> $logFile
logging $scriptName START "Начало работы скрипта"
logging $scriptName INFO "Собираем данные для индекса $indexName"

rInfoLength=$(getRowsCount "$rInfo" "zayavRegCode")

for ((i=0; i<$rInfoLength; i++))
do
  tmpObj=$(echo $rInfo | jq '.['$i']')

  zayavRegCode=$(getJsonObject "$tmpObj" "zayavRegCode")
  zayavRegCode=$(echo $zayavRegCode | cut -c 2- | rev | cut -c 2- | rev )
  zayavRegistryID=$(getRegistryId "$zayavRegCode")

  zayavName=$(getJsonObject "$tmpObj" "zayavName")
  zayavName=$(echo $zayavName | cut -c 2- | rev | cut -c 2- | rev )

  echo '' >> $logFile
  logging $scriptName INFO "Поиск данных в реестре [$zayavName]"

  mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8;
  SELECT rg.asfDataID, a.cmp_key sendDate, b.cmp_key statusKey, b.cmp_data statusValue, c.cmp_key objectDocumentId
  FROM registry_documents rg LEFT JOIN asf_data ad ON ad.uuid=rg.asfDataID
  LEFT JOIN asf_data_index a ON a.uuid=rg.asfDataId AND a.cmp_id='send_date'
  LEFT JOIN asf_data_index b ON b.uuid=rg.asfDataId AND b.cmp_id='refuse_desicion_status'
  LEFT JOIN asf_data_index c ON c.uuid=rg.asfDataId AND c.cmp_id='object_info'
  WHERE rg.deleted IS NULL AND rg.registryID = '$zayavRegistryID'
  AND DATE(ad.modified) = DATE(NOW());" > $tmpsql 2>/dev/null

  if [ -s $tmpsql ];
  then
    # Количество строк в файле tmpsql
    countStr=`cat $tmpsql | wc -l`

    for j in `seq 2 $countStr`;
    do
      zayavAsfDataID=`cat $tmpsql | sed -n $j'p' | cut -f1`
      send_dateKey=`cat $tmpsql | sed -n $j'p' | cut -f2`
      statusKey=`cat $tmpsql | sed -n $j'p' | cut -f3`
      statusValue=`cat $tmpsql | sed -n $j'p' | cut -f4`
      objectDocumentId=`cat $tmpsql | sed -n $j'p' | cut -f5`

      echo '' >> $logFile
      logging $scriptName INFO "asfDataId: [$zayavAsfDataID]"

      if [[ ("$statusKey" != "NULL") && ("$statusKey" != "0") ]]; then
        send_dateKey=$(date.parse $send_dateKey)

        total_sum=0
        square_sum=0
        flats_sum=0
        necessary_sum=0

        if [ "$objectDocumentId" != "NULL" ]; then
          mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8;
          SELECT IFNULL(a.cmp_key, 0)*1 total_sum, IFNULL(b.cmp_key, 0)*1 square_sum,
          IFNULL(c.cmp_key, 0)*1 flats_sum, IFNULL(d.cmp_key, 0)*1 necessary_sum
          FROM registry_documents rg
          LEFT JOIN asf_data_index a ON a.uuid=rg.asfDataId AND a.cmp_id='total_sum'
          LEFT JOIN asf_data_index b ON b.uuid=rg.asfDataId AND b.cmp_id='square_sum'
          LEFT JOIN asf_data_index c ON c.uuid=rg.asfDataId AND c.cmp_id='flats_sum'
          LEFT JOIN asf_data_index d ON d.uuid=rg.asfDataId AND d.cmp_id='necessary_sum'
          WHERE rg.documentid = '$objectDocumentId' AND rg.deleted IS NULL;" > $tmpsql2 2>/dev/null

          if [ -s $tmpsql2 ];
          then
            total_sum=`cat $tmpsql2 | sed -n 2p | cut -f1`
            square_sum=`cat $tmpsql2 | sed -n 2p | cut -f2`
            flats_sum=`cat $tmpsql2 | sed -n 2p | cut -f3`
            necessary_sum=`cat $tmpsql2 | sed -n 2p | cut -f4`
          fi
        fi

        iData='{"zayavName": "'$zayavName'", "sendDate": "'$send_dateKey'",'
        iData+='"statusKey": "'$statusKey'", "statusValue": "'$statusValue'",'
        iData+='"total_sum": '$total_sum', "square_sum": '$square_sum','
        iData+='"flats_sum": '$flats_sum', "necessary_sum": '$necessary_sum'}'
        logging $scriptName RESULT "iData: $iData"

        elasticKey=$indexName"-"$zayavAsfDataID
        logging $scriptName RESULT "elasticKey: $elasticKey"

        logging $scriptName INFO "Обновление индекса"
        logging $scriptName INFO "$elasticHost/$indexName/$indexType/$elasticKey"
        addIndexData "$elasticHost/$indexName/$indexType/$elasticKey" "$iData"
        logging $scriptName RESULT "`cat $tmpfile`"

      else
        logging $scriptName RESULT "Не указан статус, пропускаем"
      fi

    done

  else
    logging $scriptName INFO "нет новых данных в реестре [$zayavName]"
  fi
done


echo '' >> $logFile
logging $scriptName STATUS "Удаление временных файлов"
rm -rf $tmpsql
rm -rf $tmpsql2
rm -rf $tmpfile
logging $scriptName END "Завершение работы скрипта"
exit 0
