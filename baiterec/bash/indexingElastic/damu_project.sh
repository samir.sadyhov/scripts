#!/bin/bash

source $(dirname $(readlink -e $0))/utils.class
scriptName=${0##*/}
tmpsql=$(mktemp)

indexName='damu_project'
indexType='dp'

echo '' >> $logFile
logging $scriptName START "Начало работы скрипта"
logging $scriptName INFO "Собираем данные для индекса $indexName"

registryID=$(getRegistryId 'damu_proekty')

mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8;
SELECT rg.asfDataID, a.cmp_data supporVid, b.cmp_key dateReport, c.cmp_key project,
ROUND(d.cmp_key/1000, 3) sumProject, e.cmp_key projectApproved, ROUND(f.cmp_key/1000, 3) sumProjectApproved
FROM registry_documents rg
LEFT JOIN asf_data ad ON ad.uuid=rg.asfDataID
LEFT JOIN asf_data_index a ON a.uuid=rg.asfDataId AND a.cmp_id='fin_list'
LEFT JOIN asf_data_index b ON b.uuid=rg.asfDataId AND b.cmp_id='fin_date'
LEFT JOIN asf_data_index c ON c.uuid=rg.asfDataId AND c.cmp_id='fin_projects'
LEFT JOIN asf_data_index d ON d.uuid=rg.asfDataId AND d.cmp_id='fin_sum'
LEFT JOIN asf_data_index e ON e.uuid=rg.asfDataId AND e.cmp_id='fin_projects_approved'
LEFT JOIN asf_data_index f ON f.uuid=rg.asfDataId AND f.cmp_id='fin_sum_approved'
WHERE rg.deleted IS NULL AND rg.registryID = '$registryID'
AND DATE(ad.modified) = DATE(NOW());" > $tmpsql 2>/dev/null

if [ -s $tmpsql ];
then
  # Количество строк в файле tmpsql
  countStr=`cat $tmpsql | wc -l`

  for i in `seq 2 $countStr`;
  do
    asfDataID=`cat $tmpsql | sed -n $i'p' | cut -f1`
    dateReport=`cat $tmpsql | sed -n $i'p' | cut -f3`
    echo '' >> $logFile
    logging $scriptName INFO "asfDataID: $asfDataID"

    if [ "$dateReport" != "null" ]; then
      supporVid=`cat $tmpsql | sed -n $i'p' | cut -f2`
      project=`cat $tmpsql | sed -n $i'p' | cut -f4`
      sumProject=`cat $tmpsql | sed -n $i'p' | cut -f5`
      projectApproved=`cat $tmpsql | sed -n $i'p' | cut -f6`
      sumProjectApproved=`cat $tmpsql | sed -n $i'p' | cut -f7`
      dateReport=$(date.parse $dateReport)

      iData='{"supporVid": "'$supporVid'", "dateReport": "'$dateReport'",'
      iData+='"project": '$project', "sumProject": '$sumProject','
      iData+='"projectApproved": '$projectApproved', "sumProjectApproved": '$sumProjectApproved'}'
      logging $scriptName RESULT "iData: $iData"

      elasticKey=$indexName"-"$asfDataID
      logging $scriptName RESULT "elasticKey: $elasticKey"

      logging $scriptName INFO "Обновление индекса"
      logging $scriptName INFO "$elasticHost/$indexName/$indexType/$elasticKey"
      addIndexData "$elasticHost/$indexName/$indexType/$elasticKey" "$iData"
      logging $scriptName RESULT "`cat $tmpfile`"
    else
      logging $scriptName RESULT "Нет даты отчёта, пропускаем"
    fi

  done

else
  logging $scriptName INFO "нет новых данных в реестре"
fi


echo '' >> $logFile
logging $scriptName STATUS "Удаление временных файлов"
rm -rf $tmpsql
rm -rf $tmpfile
logging $scriptName END "Завершение работы скрипта"
exit 0
