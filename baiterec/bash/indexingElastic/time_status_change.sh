#!/bin/bash

source $(dirname $(readlink -e $0))/utils.class
scriptName=${0##*/}
tmpsql=$(mktemp)

echo '' >> $logFile
logging $scriptName START "Начало работы скрипта"
logging $scriptName INFO "Собираем данные по квартирам ожидающих снятия с брони"


mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8;
SELECT rg.asfDataID, a.cmp_data statusChanged, b.cmp_data expoDocID, c.cmp_key dateStatusChange, NOW() currdate,
IF(UNIX_TIMESTAMP(NOW()) - UNIX_TIMESTAMP(c.cmp_key) >= 0, 1, 0) che

FROM registry_documents rg
LEFT JOIN asf_data_index a ON a.uuid = rg.asfDataID AND a.cmp_id='status_changed'
LEFT JOIN asf_data_index b ON b.uuid = rg.asfDataID AND b.cmp_id='documentID'
LEFT JOIN asf_data_index c ON c.uuid = rg.asfDataID AND c.cmp_id='date_of_status_change'

WHERE rg.registryID = getRegistryIdByCode('bd_ekspo_smena_statusa_kvartiry_po_vremeni')
AND rg.deleted IS NULL AND b.cmp_data != ''
AND (a.cmp_data IS NULL OR a.cmp_data != '1');" > $tmpsql 2>/dev/null

if [ -s $tmpsql ];
then
  # Количество строк в файле tmpsql
  countStr=`cat $tmpsql | wc -l`

  for i in `seq 2 $countStr`;
  do
    asfDataID=`cat $tmpsql | sed -n $i'p' | cut -f1`
    statusChanged=`cat $tmpsql | sed -n $i'p' | cut -f2`
    expoDocID=`cat $tmpsql | sed -n $i'p' | cut -f3`
    dateStatusChange=`cat $tmpsql | sed -n $i'p' | cut -f4`
    currdate=`cat $tmpsql | sed -n $i'p' | cut -f5`
    che=`cat $tmpsql | sed -n $i'p' | cut -f6`

    if [[ ("$che" == "1") ]]; then
      echo '' >> $logFile
      logging $scriptName STATUS "активация документа [asfDataID: $asfDataID]\n expoDocID: [$expoDocID]\n dateStatusChange: [$dateStatusChange]\n currdate: [$currdate]"
      activateRegistryRow $asfDataID
      logging $scriptName RESULT "`cat $tmpfile`"
    else
      echo '' >> $logFile
      logging $scriptName INFO "дата снятия с брони еще не наступила [asfDataID: $asfDataID]\n expoDocID: [$expoDocID]\n dateStatusChange: [$dateStatusChange]\n currdate: [$currdate]"
    fi

  done
else
  logging $scriptName INFO "нет квартир ожидающих снятия с брони"
fi

echo '' >> $logFile
logging $scriptName STATUS "Удаление временных файлов"
rm -rf $tmpsql
rm -rf $tmpfile
logging $scriptName END "Завершение работы скрипта"
exit 0
