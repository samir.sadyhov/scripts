#!/bin/bash

source $(dirname $(readlink -e $0))/utils.class
scriptName=${0##*/}
tmpsql=$(mktemp)

indexName='expo_residence_table'
indexType='er'

echo '' >> $logFile
logging $scriptName START "Начало работы скрипта"
logging $scriptName INFO "Собираем данные для индекса $indexName"

registryID=$(getRegistryId 'nuh_pul_proektov__zayavka')

mysql -h $mysqlHost -u$mysqlUser -p$mysqlPass -e "use synergy; set names utf8;
SELECT rooms,

CASE rooms
WHEN '1' THEN '1 - комн. кв.'
WHEN '2' THEN '2 - комн. кв.'
WHEN '3' THEN '3 - комн. кв.'
WHEN '4' THEN '4 - комн. кв.'
ELSE 'Машиноместа'
END roomsText,

quanAll-(rassmotCount+realizCount+rassrochkaCount) svobodCount,
ROUND((costAll-(rassmotCost+realizCost+rassrochkaCost))/1000000, 1) svobodCost,
rassmotCount, ROUND(rassmotCost/1000000, 1) rassmotCost,
realizCount-rassrochkaCount realizCount, ROUND((realizCost-rassrochkaCost)/1000000, 1) realizCost,
allObject, sobstSred, zaemSred,
quanAll, ROUND(costAll/1000000, 1) costAll,
rassrochka, realizSobstSred, realizZaemSred, realizRassrochka,
rassrochkaCount, ROUND(rassrochkaCost/1000000, 1) rassrochkaCost,
realizCount realizAllCount, ROUND((realizCost)/1000000, 1) realizAllCost,

ROUND((rassmotCost+realizCost)/1000000, 1) sprosCost

FROM (SELECT tm.rooms, allInfo.quanAll, allInfo.costAll,

IFNULL(zayavki.rassmotCount, 0) rassmotCount,
IFNULL(zayavki.rassmotCost, 0) rassmotCost,
IFNULL(zayavki.realizCount, 0) realizCount,
IFNULL(zayavki.realizCost, 0) realizCost,
zayavki.allObject, zayavki.sobstSred, zayavki.zaemSred, zayavki.rassrochka,
zayavki.realizSobstSred, zayavki.realizZaemSred, zayavki.realizRassrochka,
zayavki.rassrochkaCount, zayavki.rassrochkaCost

FROM (SELECT '1' rooms UNION SELECT '2' UNION SELECT '3' UNION SELECT '4' UNION SELECT 'car' ) tm

LEFT JOIN ( SELECT
CASE
WHEN ak1.cmp_id = 'quan1' THEN '1'
WHEN ak1.cmp_id = 'quan2' THEN '2'
WHEN ak1.cmp_id = 'quan3' THEN '3'
WHEN ak1.cmp_id = 'quan4' THEN '4'
WHEN ak1.cmp_id = 'quan_cars' THEN 'car'
END rooms,
ak1.cmp_key quanAll, as1.cmp_key costAll

FROM registry_documents rg
LEFT JOIN asf_data_index ak1 ON ak1.uuid = rg.asfDataID AND ak1.cmp_id IN('quan1', 'quan2', 'quan3', 'quan4', 'quan_cars')
LEFT JOIN asf_data_index as1 ON as1.uuid = rg.asfDataID
AND as1.cmp_id = CASE
WHEN ak1.cmp_id = 'quan1' THEN 'cost1'
WHEN ak1.cmp_id = 'quan2' THEN 'cost2'
WHEN ak1.cmp_id = 'quan3' THEN 'cost3'
WHEN ak1.cmp_id = 'quan4' THEN 'cost4'
WHEN ak1.cmp_id = 'quan_cars' THEN 'cost_car'
END

WHERE rg.registryID = getRegistryIdByCode('bd_ekspo_informatsiya_po_kvartiram_dlya_dashborda') AND rg.deleted IS NULL
GROUP BY ak1.cmp_id ) allInfo ON allInfo.rooms = tm.rooms

LEFT JOIN (
SELECT tt.rooms,
SUM(IF(tt.status IN (1, 4), 1, 0)) rassmotCount,
SUM(IF(tt.status IN (1, 4), tt.flat, 0)) rassmotCost,
SUM(IF(tt.status = 2, 1, 0)) realizCount,
SUM(IF(tt.status = 2, tt.flat, 0)) realizCost,

SUM(IF(tt.status IN (1, 2, 4), 1, 0)) allObject,

SUM(IF(tt.status IN (1, 4) AND sredstva = 1, 1, 0)) sobstSred,
SUM(IF(tt.status IN (1, 4) AND sredstva = 2, 1, 0)) zaemSred,
SUM(IF(tt.status IN (1, 4) AND sredstva = 3, 1, 0)) rassrochka,

SUM(IF(tt.status = 2 AND sredstva = 1, 1, 0)) realizSobstSred,
SUM(IF(tt.status = 2 AND sredstva = 2, 1, 0)) realizZaemSred,
SUM(IF(tt.status = 2 AND sredstva = 3, 1, 0)) realizRassrochka,

SUM(IF(tt.status = 2 AND sredstva = 3, 1, 0)) rassrochkaCount,
SUM(IF(tt.status = 2 AND sredstva = 3, tt.flat, 0)) rassrochkaCost

FROM (SELECT CONCAT(k3.cmp_data,k4.cmp_data,k2.cmp_data) prov, MAX(IF(a.cmp_key = 4, 1, a.cmp_key)) status, k1.cmp_key flat, k2.cmp_key rooms,
rr.cmp_data sredstva

FROM registry_documents rg
LEFT JOIN asf_data_index a ON a.uuid = rg.asfDataID AND a.cmp_id='refuse_desicion_status'
LEFT JOIN asf_data_index k1 ON k1.uuid = rg.asfDataID AND k1.cmp_id LIKE 'flat_cost-%'
LEFT JOIN asf_data_index k2 ON k2.uuid = rg.asfDataID AND k2.cmp_id = CONCAT('rooms-b', k1.cmp_row)
LEFT JOIN asf_data_index k3 ON k3.uuid = rg.asfDataID AND k3.cmp_id = CONCAT('section-b', k1.cmp_row)
LEFT JOIN asf_data_index k4 ON k4.uuid = rg.asfDataID AND k4.cmp_id = CONCAT('flat-b', k1.cmp_row)

LEFT JOIN asf_data_index rr ON rr.uuid = rg.asfDataID AND rr.cmp_id='radio-je3ut'

WHERE rg.registryID = getRegistryIdByCode('bd_zayavlenie_po_expo') AND rg.deleted IS NULL
AND a.cmp_key IN (1, 2, 4)

GROUP BY prov ) tt

GROUP BY tt.rooms

UNION ALL

SELECT 'car' rooms,
SUM(IF(a.cmp_key IN (1, 4) AND k1.cmp_data IS NOT NULL, 1, 0)) rassmotCount,
SUM(IF(a.cmp_key IN (1, 4) AND k1.cmp_data IS NOT NULL, k2.cmp_key, 0)) rassmotCost,
SUM(IF(a.cmp_key = 2 AND k1.cmp_data IS NOT NULL, 1, 0)) realizCount,
SUM(IF(a.cmp_key = 2 AND k1.cmp_data IS NOT NULL, k2.cmp_key, 0)) realizCost,
SUM(IF(a.cmp_key IN (1, 2, 4) AND k1.cmp_data IS NOT NULL, 1, 0)) allObject,
SUM(IF(a.cmp_key IN (1, 4) AND rr.cmp_data = 1 AND k1.cmp_data IS NOT NULL, 1, 0)) sobstSred,
SUM(IF(a.cmp_key IN (1, 4) AND rr.cmp_data = 2 AND k1.cmp_data IS NOT NULL, 1, 0)) zaemSred,
SUM(IF(a.cmp_key IN (1, 4) AND rr.cmp_data = 3 AND k1.cmp_data IS NOT NULL, 1, 0)) rassrochka,

SUM(IF(a.cmp_key = 2 AND rr.cmp_data = 1 AND k1.cmp_data IS NOT NULL, 1, 0)) realizSobstSred,
SUM(IF(a.cmp_key = 2 AND rr.cmp_data = 2 AND k1.cmp_data IS NOT NULL, 1, 0)) realizZaemSred,
SUM(IF(a.cmp_key = 2 AND rr.cmp_data = 3 AND k1.cmp_data IS NOT NULL, 1, 0)) realizRassrochka,

SUM(IF(a.cmp_key = 2 AND rr.cmp_data = 3 AND k1.cmp_data IS NOT NULL, 1, 0)) rassrochkaCount,
SUM(IF(a.cmp_key = 2 AND rr.cmp_data = 3 AND k1.cmp_data IS NOT NULL, k2.cmp_key, 0)) rassrochkaCost

FROM registry_documents rg
LEFT JOIN asf_data_index a ON a.uuid = rg.asfDataID AND a.cmp_id='refuse_desicion_status'
LEFT JOIN asf_data_index k1 ON k1.uuid = rg.asfDataID AND k1.cmp_id LIKE 'parking-%'
LEFT JOIN asf_data_index k2 ON k2.uuid = rg.asfDataID AND k2.cmp_id = CONCAT('parking_cost-b', k1.cmp_row)

LEFT JOIN asf_data_index rr ON rr.uuid = rg.asfDataID AND rr.cmp_id='radio-je3ut'

WHERE rg.registryID = getRegistryIdByCode('bd_zayavlenie_po_expo')
AND rg.deleted IS NULL ) zayavki ON zayavki.rooms = tm.rooms ) t;" > $tmpsql 2>/dev/null

if [ -s $tmpsql ];
then
  # Количество строк в файле tmpsql
  countStr=`cat $tmpsql | wc -l`

  for i in `seq 2 $countStr`;
  do
    rooms=`cat $tmpsql | sed -n $i'p' | cut -f1`
    roomsText=`cat $tmpsql | sed -n $i'p' | cut -f2`
    svobodCount=`cat $tmpsql | sed -n $i'p' | cut -f3`
    svobodCost=`cat $tmpsql | sed -n $i'p' | cut -f4`
    rassmotCount=`cat $tmpsql | sed -n $i'p' | cut -f5`
    rassmotCost=`cat $tmpsql | sed -n $i'p' | cut -f6`
    realizCount=`cat $tmpsql | sed -n $i'p' | cut -f7`
    realizCost=`cat $tmpsql | sed -n $i'p' | cut -f8`
    allObject=`cat $tmpsql | sed -n $i'p' | cut -f9`
    sobstSred=`cat $tmpsql | sed -n $i'p' | cut -f10`
    zaemSred=`cat $tmpsql | sed -n $i'p' | cut -f11`
    quanAll=`cat $tmpsql | sed -n $i'p' | cut -f12`
    costAll=`cat $tmpsql | sed -n $i'p' | cut -f13`
    rassrochka=`cat $tmpsql | sed -n $i'p' | cut -f14`
    realizSobstSred=`cat $tmpsql | sed -n $i'p' | cut -f15`
    realizZaemSred=`cat $tmpsql | sed -n $i'p' | cut -f16`
    realizRassrochka=`cat $tmpsql | sed -n $i'p' | cut -f17`
    rassrochkaCount=`cat $tmpsql | sed -n $i'p' | cut -f18`
    rassrochkaCost=`cat $tmpsql | sed -n $i'p' | cut -f19`
    realizAllCount=`cat $tmpsql | sed -n $i'p' | cut -f20`
    realizAllCost=`cat $tmpsql | sed -n $i'p' | cut -f21`
    sprosCost=`cat $tmpsql | sed -n $i'p' | cut -f22`

    iData='{"rooms": "'$rooms'", "roomsText": "'$roomsText'",'
    iData+='"svobodCount": '$svobodCount', "svobodCost": '$svobodCost','
    iData+='"rassmotCount": '$rassmotCount', "rassmotCost": '$rassmotCost','
    iData+='"realizCount": '$realizCount', "realizCost": '$realizCost', "allObject": '$allObject','
    iData+='"sobstSred": '$sobstSred', "zaemSred": '$zaemSred', "rassrochka": '$rassrochka','
    iData+='"realizSobstSred": '$realizSobstSred', "realizZaemSred": '$realizZaemSred', "realizRassrochka": '$realizRassrochka','
    iData+='"rassrochkaCount": '$rassrochkaCount', "rassrochkaCost": '$rassrochkaCost','
    iData+='"realizAllCount": '$realizAllCount', "realizAllCost": '$realizAllCost','
    iData+='"sprosCost": '$sprosCost', "quanAll": '$quanAll', "costAll": '$costAll'}'
    logging $scriptName RESULT "iData: $iData"

    elasticKey=$indexName"-rooms-"$rooms
    logging $scriptName RESULT "elasticKey: $elasticKey"

    logging $scriptName INFO "Обновление индекса"
    logging $scriptName INFO "$elasticHost/$indexName/$indexType/$elasticKey"
    addIndexData "$elasticHost/$indexName/$indexType/$elasticKey" "$iData"
    logging $scriptName RESULT "`cat $tmpfile`"

  done
fi


echo '' >> $logFile
logging $scriptName STATUS "Удаление временных файлов"
rm -rf $tmpsql
rm -rf $tmpfile
logging $scriptName END "Завершение работы скрипта"
exit 0
