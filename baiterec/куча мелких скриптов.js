// email
var pattern = new RegExp(/^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/);

var title = model.playerModel.getModelWithId('cmp-2zvk2m').value;
view.container.children('input').attr('placeholder', 'xxxx@xxxx.xx');
view.container.attr('title', title);

model.getSpecialErrors = function() {
    if (!pattern.test(model.getValue())) {
        return {id: model.asfProperty.id, errorCode: AS.FORMS.INPUT_ERROR_TYPE.wrongValue};
    }
};

model.on("valueChange", function(evt, cmpModel, value) {
  model.setValue(value.replace(/[^a-zA-Z\d.@\-_]/g, ''));
    if(!pattern.test(value)) {
        view.markInvalid();
    } else {
        view.unmarkInvalid();
    }
});


// телефон
var pattern = new RegExp(/^\+?\s?\d+[\s\-]?\(?\d{1,6}\)?[\s\-]?\d+?[\s\-]?\d+?[\s\-]?\d+?$/);

model.getSpecialErrors = function() {
    if (!pattern.test(model.getValue())) {
        return {id: model.asfProperty.id, errorCode: AS.FORMS.INPUT_ERROR_TYPE.wrongValue};
    }
};

var title = model.playerModel.getModelWithId('cmp-4bsyf0').value;
view.container.children('input').attr('placeholder', '+7 (xxx) xxx-xx-xx');
view.container.attr('title', title);

model.on("valueChange", function(evt, cmpModel, value) {
    model.setValue(value.replace(/[^0-9\s\-+()]/g, ''));
    if(!pattern.test(value)) {
        view.markInvalid();
    } else {
        view.unmarkInvalid();
    }
});


//только цифры и латинские буквы
var pattern = new RegExp(/[^A-Za-z0-9]/i);
view.container.attr('title', 'только цифры и латинские буквы');

model.getSpecialErrors = function() {
  if (pattern.test(model.getValue())) {
    return {id: model.asfProperty.id, errorCode: AS.FORMS.INPUT_ERROR_TYPE.wrongValue};
  }
};

model.on("valueChange", function(evt, cmpModel, value) {
  model.setValue(value.replace(/[^A-Za-z0-9]/g,''));
});


//только цифры и знак +
var pattern = new RegExp(/[^0-9+]/i);
view.container.attr('title', 'только цифры и знак +');

model.getSpecialErrors = function() {
  if (pattern.test(model.getValue())) {
    return {id: model.asfProperty.id, errorCode: AS.FORMS.INPUT_ERROR_TYPE.wrongValue};
  }
};

model.on("valueChange", function(evt, cmpModel, value) {
  model.setValue(value.replace(/[^0-9+]/g,''));
});


// только текст
model.on("valueChange", function(evt, cmpModel, value) {
  model.setValue(value.replace(/[^\D]/g,''));
});

//только цифры
model.on("valueChange", function(evt, cmpModel, value) {
  model.setValue(value.replace(/[^\d]/g,''));
});

// не больше 2-х символов
model.on("valueChange", function(evt, cmpModel, value) {
  if (value.length <= 2) return;
  if(value.length > 2) {
    model.setValue(value.slice(0,2));
  }
});

// выбранная дата не больше текущей даты
model.on("valueChange", function(evt, cmpModel, value) {
  if (!AS.FORMS.DateUtils.parseDate(value)) return;

  if(AS.FORMS.DateUtils.parseDate(value).getTime() > new Date().getTime()) {
    view.markInvalid();
    model.setValue(null);
    alert('Дата первичной государственной регистрации не может быть больше текущей даты');
  }

});


model.on("valueChange", function(evt, cmpModel, value) {
  if (!AS.FORMS.DateUtils.parseDate(value)) return;

  var date1 = AS.FORMS.DateUtils.parseDate(model.playerModel.getModelWithId('first_reg_date').getValue());
  var currdate = new Date().getTime();

  if (!date1) {
    model.setValue(null);
    view.markInvalid();
    alert('Необходимо заполнить дату первичной государственной регистрации');
    return;
  }

  if (AS.FORMS.DateUtils.parseDate(value).getTime() > date1.getTime()) {
    model.setValue(null);
    view.markInvalid();
    alert('Дата государственной перерегистрации не может быть больше даты первичной государственной регистрации');
    return;
  }

  if(AS.FORMS.DateUtils.parseDate(value).getTime() > currdate) {
    view.markInvalid();
    model.setValue(null);
    alert('Дата государственной перерегистрации не может быть больше текущей даты');
  }

});



// доля не больше 100%
var tableName = 'lb_table';
var cmp = 'lb_part';
var sumCmp = 'lb_part_sum';

model.on(AS.FORMS.EVENT_TYPE.valueChange, function (evt, cmpModel, value) {
    if(value > 100) {
        alert('Доля не может превышать 100%');
        model.setValue(null);
    }
    model.playerModel.sumParts(tableName, cmp, sumCmp);
});

model.playerModel.sumParts = function(tableName, cmp, sumCmp) {
    var TOTAL = 0;
    var tableModel = model.playerModel.getModelWithId(tableName);

    tableModel.modelBlocks.forEach(function(modelBlock, index){
      var tableBlockIndex = modelBlock[0].asfProperty.tableBlockIndex;
      var n = parseFloat(tableModel.getModelWithId(cmp, tableModel.asfProperty.id, tableBlockIndex).getValue());
      if(!isNaN(n)) TOTAL += n;

      if (TOTAL > 100) {
          alert('Итого по доле не может превышать 100%');
          tableModel.getModelWithId(cmp, tableModel.asfProperty.id, tableBlockIndex).setValue(null);
          view.playerView.getViewWithId(sumCmp).container.parent().parent().css({'background-color': '#FF9999'});
          setTimeout(function () {
            view.playerView.getViewWithId(sumCmp).container.parent().parent().css({'background-color': ''});
          }, 10000);
          return;
      }
    });

    if (TOTAL <= 100) model.playerModel.getModelWithId(sumCmp).setValue((Math.round(TOTAL * 100) / 100) + '');
};



// ФГЖС: Фин. отчётность. Кредитная история
var tableName = 'cmp-jnyu3g';
var contractDate = 'contract_date';
var expireDate = 'expire_date';

model.on("valueChange", function(evt, cmpModel, value) {
  if (!AS.FORMS.DateUtils.parseDate(value)) return;

  var contractModel = model.playerModel.getModelWithId(contractDate, tableName, model.asfProperty.tableBlockIndex);

  if(!AS.FORMS.DateUtils.parseDate(contractModel.getValue())) return;

  if(AS.FORMS.DateUtils.parseDate(value).getTime() < AS.FORMS.DateUtils.parseDate(contractModel.getValue()).getTime()) {
    view.markInvalid();
    model.setValue(null);
    alert('Дата завершения не может быть меньше даты заключения');
    return;
  }

});




// срок с момента государственной регистрации более 3-х лет
// Дата первичной государственной регистрации
var d = new Date();
d.setFullYear(d.getFullYear() - 3);

model.on("valueChange", function(evt, cmpModel, value) {
  if (!AS.FORMS.DateUtils.parseDate(value)) return;

  if(AS.FORMS.DateUtils.parseDate(value).getTime() > d.getTime()) {
    view.markInvalid();
    model.setValue(null);
    alert('Застройщик должен иметь срок с момента государственной регистрации более 3-х лет');
  }

});


// Дата государственной перерегистрации
model.on("valueChange", function(evt, cmpModel, value) {
  if (!AS.FORMS.DateUtils.parseDate(value)) return;

  var date1 = AS.FORMS.DateUtils.parseDate(model.playerModel.getModelWithId('first_reg_date').getValue());
  var currdate = new Date().getTime();

  if (!date1) {
    model.setValue(null);
    view.markInvalid();
    alert('Необходимо заполнить дату первичной государственной регистрации');
    return;
  }

  if (AS.FORMS.DateUtils.parseDate(value).getTime() < date1.getTime()) {
    model.setValue(null);
    view.markInvalid();
    alert('Дата государственной перерегистрации не может быть меньше даты первичной государственной регистрации');
    return;
  }

  if(AS.FORMS.DateUtils.parseDate(value).getTime() > currdate) {
    view.markInvalid();
    model.setValue(null);
    alert('Дата государственной перерегистрации не может быть больше текущей даты');
  }

});


//Стаж в отрасли и Стаж в компании не должны превышать Общий стаж
model.playerModel.recalcExperience = function(model){
  var tableName = 'pi_table'; //наименование таблицы
  var generalExp = 'pi_experience'; //Общий стаж
  var industryExp = 'pi_act_experience'; //Стаж в отрасли
  var companyExp = 'pi_comp_experience'; //Стаж в компании

  var generalExpModel = model.playerModel.getModelWithId(generalExp, tableName, model.asfProperty.tableBlockIndex);
  var industryExpModel = model.playerModel.getModelWithId(industryExp, tableName, model.asfProperty.tableBlockIndex);
  var companyExpModel = model.playerModel.getModelWithId(companyExp, tableName, model.asfProperty.tableBlockIndex);

  var generalExpValue = Number(generalExpModel.getValue());
  if(isNaN(generalExpValue)) generalExpValue = 0;

  var industryExpValue = Number(industryExpModel.getValue());
  if(isNaN(industryExpValue)) industryExpValue = 0;

  var companyExpValue = Number(companyExpModel.getValue());
  if(isNaN(companyExpValue)) companyExpValue = 0;

  model.getSpecialErrors = function() {
    if (industryExpValue + companyExpValue > generalExpValue) {
      return {id: model.asfProperty.id, errorCode: AS.FORMS.INPUT_ERROR_TYPE.wrongValue};
    }
  };

};



// загрузчик файлов, обязательные поля
var tableModel = model.playerModel.getModelWithId(model.asfProperty.ownerTableId);
model.getSpecialErrors = function() {
  if (view.container.parent().parent()[0].style.display == 'none')  {
    return;
  } else {
    if (tableModel.modelBlocks.length === 0) {
      return {id: model.asfProperty.id, errorCode: AS.FORMS.INPUT_ERROR_TYPE.emptyValue};
    }
  }
};




ФГЖС: Информация о застройщике учр фл
ФГЖС: Информация о УК учр фл
