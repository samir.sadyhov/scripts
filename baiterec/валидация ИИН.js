function chekAge(iin) {
  let year = iin.substring(0, 2);
  let ep = Number(iin.substring(6, 7));
  switch (ep) {
    case 1:
    case 2:
      year = '18' + year;
      break;
    case 3:
    case 4:
      year = '19' + year;
      break;
    case 5:
    case 6:
      year = '20' + year;
      break;
  }
  let birthDay = new Date(year, (iin.substring(2, 4) - 1), (iin.substring(4, 6)));
  return ((new Date().getTime() - birthDay.getTime()) / (24 * 3600 * 365.25 * 1000)) | 0;
}


function validIIIN(iin) {
  if (!iin) return false;
  if (iin.length != 12) return false;
  if (!(/^\d+$/.test(iin))) return false;
  if (chekAge(iin) > 100) return false;

  let s = 0;
  for (let i = 0; i < 11; i++) {
    s = s + (i + 1) * iin[i];
  }
  let k = s % 11;
  if (k === 10) {
    s = 0;
    for (let i = 0; i < 11; i++) {
      let t = (i + 3) % 11;
      if (t === 0) t = 11;
      s = s + t * iin[i];
    }
    k = s % 11;
    if (k === 10) return false;
    return (k === Number(iin.substring(11, 12)));
  }
  return (k === Number(iin.substring(11, 12)));
}

model.getSpecialErrors = function() {
  if(!validIIIN(model.getValue())) {
    return {id: model.asfProperty.id, errorCode: AS.FORMS.INPUT_ERROR_TYPE.wrongValue};
  }
}

model.on("valueChange", function(_1, _2, value) {
  if(parseInt(value)) view.checkValid();
});
