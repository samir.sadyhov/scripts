view.setVisible(false);

AS.FORMS.View.prototype.setVisible = function (visible) {
    if(this.model.asfProperty.requiredOriginal === undefined) {
        this.model.asfProperty.requiredOriginal =  this.model.asfProperty.required;
    }
    if(this.model.getErrorsOriginal === undefined) {
        this.model.getErrorsOriginal =  this.model.getErrors;
    }
    if (visible) {
        this.container.show();
        this.model.getErrors = this.model.getErrorsOriginal;
        this.model.asfProperty.required = this.model.asfProperty.requiredOriginal;
    } else {
        this.container.hide();
        this.model.getErrors = function(){return []};
        this.model.asfProperty.required = false;
    }
};


var url = AS.OPTIONS.coreUrl + "/rest/api/formPlayer/getCustomComponent?code=baiterek_user_grous";
var request = jQuery.ajax({
            url: url,
            type: "GET",
            beforeSend: AS.FORMS.ApiUtils.addAuthHeader,
            async: false
});
eval(JSON.parse(request.responseText).scriptSource);
url = AS.OPTIONS.coreUrl + "/rest/api/formPlayer/getCustomComponent?code=baiterek_user_item_codes";
request = jQuery.ajax({
            url: url,
            type: "GET",
            beforeSend: AS.FORMS.ApiUtils.addAuthHeader,
            async: false
});
eval(JSON.parse(request.responseText).scriptSource);


// загрузчик файлов, проверка на обязательность
let tableModel = model.playerModel.getModelWithId(model.asfProperty.ownerTableId);
model.getSpecialErrors = function() {
  if (view.container.parent().parent()[0].style.display == 'none') return;
  if (window.location.href.indexOf('Synergy') !== -1) return;

  let er = true;
  tableModel.modelBlocks.forEach(function(item){
    if(item[0].value) er = false;
  });

  if (tableModel.modelBlocks.length === 0 || er) {
    return {id: model.asfProperty.id, errorCode: AS.FORMS.INPUT_ERROR_TYPE.emptyValue};
  }
};

//автонумерация блоков динамической таблицы
model.on("tableRowAdd", function(){
    model.modelBlocks.forEach(function(row, index){
        row[0].setValue(index+1+'');
    });
});
model.on("tableRowDelete", function(){
    model.modelBlocks.forEach(function(row, index){
        row[0].setValue(index+1+'');
    });
});


//четатам обязательное/не обязательное
var components = ['contract_start', 'contract_end','contract_num', 'contract_date', 'contract_start', 'contract_end'];
var cmpModel = [];

components.forEach(function(cmp){
  cmpModel.push(model.playerModel.getModelWithId(cmp));
});

model.on(AS.FORMS.EVENT_TYPE.valueChange, function (evt, cm, value) {
  cmpModel.forEach(function(tmpModel){
    if(tmpModel.asfProperty.requiredOriginal === undefined) {
      tmpModel.asfProperty.requiredOriginal = tmpModel.asfProperty.required;
    }
    if(tmpModel.getErrorsOriginal === undefined) {
      tmpModel.getErrorsOriginal = tmpModel.getErrors;
    }

    if (value == 2 || window.location.href.indexOf('Synergy') == -1) {
      tmpModel.getErrors = function(){return []};
      tmpModel.asfProperty.required = false;
    } else {
      tmpModel.getErrors = tmpModel.getErrorsOriginal;
      tmpModel.asfProperty.required = tmpModel.asfProperty.requiredOriginal;
    }
  });
});
