view.setVisible(false);

// AS.FORMS.ComponentUtils.isInGroup('')
// AS.FORMS.ComponentUtils.hasRouteItem()

function disabledCustomComponent(cmps) {
  cmps.forEach(function(cmp){
    var cc = view.playerView.getViewWithId(cmp);
    if (cc) {
      cc.setEnabled(false);
      ['add', 'edit', 'browse', 'delete'].forEach(function(atr){
        cc.container.find('[innerid="'+atr+'"]').hide();
      });
    }
  });
}

// блокируем динамические таблицы
function disableTable(tables) {
  tables.forEach(function(cmp){
    var tableView = view.playerView.getViewWithId(cmp);
    if(tableView){
      tableView.setEnabled(false);
      tableView.getViewBlocks().forEach(function(viewBlock, index){
        viewBlock.views.forEach(function(component){
          component.setEnabled(false);
        });
      });
    }
  });
}

// скрыть блок загрузки файлов
function disableBlockFiles(tables) {
  tables.forEach(function(cmp){
    var tableModel = model.playerModel.getModelWithId(cmp);
    if (tableModel) {
      $('[data-asformid="table.container.'+cmp+'"]')
      .find('[data-asformid="custom.container.'+tableModel.asfProperty.properties[0].id+'"]')
      .parent().parent().hide();
      $('[data-asformid="table.removeRowButton.'+cmp+'"]').hide();
    }
  });
}

function setVisible(cmp, visible){
  if (view.playerView.getViewWithId(cmp)) view.playerView.getViewWithId(cmp).setVisible(visible);
}
function setEnabled(cmp, enabled){
  if (view.playerView.getViewWithId(cmp)) view.playerView.getViewWithId(cmp).setEnabled(enabled);
}

setTimeout(function(){

  //скрыть шаги если заявка открыта не в synergy
  if (window.location.href.indexOf('Synergy') == -1) {
    ['step18', 'step19', 'step20', 'step21'].forEach(function(id){
      $('#'+id).hide();
    });
  } else {


    // заблокировать "Форма заявки и приложенные документы" **********************************************************************
    //блокируем компоненты
    ['applicant_name', 'object_name', 'object_location', 'object_address', 'total_sum', 'sqm_sum', 'sale_sum', 'house_quantity', 'floors', 'square_sum', 'square_sum_bd', 'flats_sum', 'flats_sum_bd', 'listbox-gyyfkd', 'build_term', 'build_start_date', 'build_finish_date', 'expluatation_date', 'partner', 'necessary_sum', 'necessary_term', 'water_term', 'heating_term', 'drain_term', 'electro_term', 'gas_term', 'telephone_term', 'storm_drain_term'].forEach(function(cmp){
      setEnabled(cmp, false);
    });
    // блокируем динамические таблицы
    disableTable(['flats_table', 'land_table']);
    // скрыть блок загрузки файлов
    disableBlockFiles(['table-doc1', 'table-doc2', 'table-doc3', 'table-doc4', 'table-doc5', 'table-doc6', 'table-doc7', 'table-doc8', 'table-doc9', 'table-doc10', 'table-doc11', 'table-doc12', 'table-doc13']);
    // ******************************************************************************************************************************

// Коды групп пользователей
// ДИП - Direktsiya_investitsionnyh_proektov_v_stroitel_stve_
// ДУР - Direktsiya_upravleniya_riskami_
// ЮД - Uridicheskaya_direktsiya
// ДПРА - Direktsiya_proektnyh_rabot_i_analiza


    // ДИП
    if(AS.FORMS.ComponentUtils.isInGroup('Direktsiya_investitsionnyh_proektov_v_stroitel_stve_')) {
      //Назначить исполнителей
      if (AS.FORMS.ComponentUtils.hasRouteItem('set_ispolnitel_dip')) {
        ['custom-mq265o', 'ispol1', 'custom-qwy55r', 'ispol2', 'custom-qwy55r_copy1', 'ispol3', 'custom-qwy55r_copy2', 'dispol_dips', 'table_18_sv_dip'].forEach(function(cmp){
          setVisible(cmp, false);
        });
        ['step19', 'step20', 'step21'].forEach(function(id){
          $('#'+id).hide();
        });
      }
      //Сформировать и прикрепить экспертное заключение (Согласование)
      if (AS.FORMS.ComponentUtils.hasRouteItem('ekspert_zakluch_dip') ||
          AS.FORMS.ComponentUtils.hasRouteItem('sogl_zakluchenie_dip')) {
        ['ispol_dips', 'dispol_dips', 'table_18_dur', 'table_18_ud', 'table_18_dpra', 'table_18_sv_dip'].forEach(function(cmp){
          setVisible(cmp, false);
        });
        ['step19', 'step20', 'step21'].forEach(function(id){
          $('#'+id).hide();
        });
      }
    }

    // ДУР
    if(AS.FORMS.ComponentUtils.isInGroup('Direktsiya_upravleniya_riskami_')) {
      //Назначить исполнителей
      if (AS.FORMS.ComponentUtils.hasRouteItem('set_ispolnitel_dur')) {
        ['custom-mq265o', 'dispol1', 'table_18_ud', 'table_18_dpra', 'table_18_dip', 'table_18_sv_dip'].forEach(function(cmp){
          setVisible(cmp, false);
        });
        ['step19', 'step20', 'step21'].forEach(function(id){
          $('#'+id).hide();
        });
      }
      //Сформировать и прикрепить экспертное заключение (Согласование)
      if (AS.FORMS.ComponentUtils.hasRouteItem('ekspert_zakluch_dur') ||
          AS.FORMS.ComponentUtils.hasRouteItem('sogl_zakluchenie_dur')) {
        ['ispol1', 'dispol1', 'table_18_ud', 'table_18_dpra', 'table_18_dip', 'table_18_sv_dip'].forEach(function(cmp){
          setVisible(cmp, false);
        });
        ['step19', 'step20', 'step21'].forEach(function(id){
          $('#'+id).hide();
        });
      }
    }

    // ЮД
    if(AS.FORMS.ComponentUtils.isInGroup('Uridicheskaya_direktsiya')){
      //Назначить исполнителей
      if (AS.FORMS.ComponentUtils.hasRouteItem('set_ispolnitel_ud')) {
        ['custom-qwy55r', 'dispol2', 'table_18_dur', 'table_18_dpra', 'table_18_dip', 'table_18_sv_dip'].forEach(function(cmp){
          setVisible(cmp, false);
        });
        ['step19', 'step20', 'step21'].forEach(function(id){
          $('#'+id).hide();
        });
      }
      //Сформировать и прикрепить экспертное заключение (Согласование)
      if (AS.FORMS.ComponentUtils.hasRouteItem('ekspert_zakluch_ud') ||
          AS.FORMS.ComponentUtils.hasRouteItem('sogl_zakluchenie_ud')) {
        ['ispol2', 'dispol2', 'table_18_dur', 'table_18_dpra', 'table_18_dip', 'table_18_sv_dip'].forEach(function(cmp){
          setVisible(cmp, false);
        });
        ['step19', 'step20', 'step21'].forEach(function(id){
          $('#'+id).hide();
        });
      }
    }

    // ДПРА
    if(AS.FORMS.ComponentUtils.isInGroup('Direktsiya_proektnyh_rabot_i_analiza')){
      //Назначить исполнителей
      if (AS.FORMS.ComponentUtils.hasRouteItem('set_ispolnitel_dpra')) {
        ['custom-qwy55r_copy1', 'dispol3', 'table_18_dur', 'table_18_ud', 'table_18_dip', 'table_18_sv_dip'].forEach(function(cmp){
          setVisible(cmp, false);
        });
        ['step19', 'step20', 'step21'].forEach(function(id){
          $('#'+id).hide();
        });
      }
      //Сформировать и прикрепить экспертное заключение (Согласование)
      if (AS.FORMS.ComponentUtils.hasRouteItem('ekspert_zakluch_dpra') ||
          AS.FORMS.ComponentUtils.hasRouteItem('sogl_zakluchenie_dpra')) {
        ['ispol3', 'dispol3', 'table_18_dur', 'table_18_ud', 'table_18_dip', 'table_18_sv_dip'].forEach(function(cmp){
          setVisible(cmp, false);
        });
        ['step19', 'step20', 'step21'].forEach(function(id){
          $('#'+id).hide();
        });
      }
    }

    // Рассмотреть полученные заключения Директор ДИП
    if (AS.FORMS.ComponentUtils.hasRouteItem('rassmotret')){
      ['step19', 'step20', 'step21'].forEach(function(id){
        $('#'+id).hide();
      });
      disabledCustomComponent(['custom-mq265o', 'custom-qwy55r', 'custom-qwy55r_copy1', 'custom-qwy55r_copy2']);
      ['ispol1', 'dispol1', 'ispol2', 'dispol2', 'ispol3', 'dispol3', 'ispol_dips', 'dispol_dips', 'table_18_sv_dip'].forEach(function(cmp){
        setVisible(cmp, false);
      });
    }

    // Сформировать и прикрепить сводное заключение Испол ДИП
    if (AS.FORMS.ComponentUtils.hasRouteItem('sformirovat_zakluchenie')){
      ['step19', 'step20', 'step21'].forEach(function(id){
        $('#'+id).hide();
      });
      disabledCustomComponent(['custom-mq265o', 'custom-qwy55r', 'custom-qwy55r_copy1', 'custom-qwy55r_copy2']);
      ['ispol1', 'dispol1', 'ispol2', 'dispol2', 'ispol3', 'dispol3', 'ispol_dips', 'dispol_dips'].forEach(function(cmp){
        setVisible(cmp, false);
      });
    }

    // Принять решение о дальнейшем ходе заявки 1 Директор ДИП
    if (AS.FORMS.ComponentUtils.hasRouteItem('prinyat_reshenie1')){
      ['step20', 'step21'].forEach(function(id){
        $('#'+id).hide();
      });
      disabledCustomComponent(['custom-mq265o', 'custom-qwy55r', 'custom-qwy55r_copy1', 'custom-qwy55r_copy2', 'custom-43iu9k']);
      ['ispol1', 'dispol1', 'ispol2', 'dispol2', 'ispol3', 'dispol3', 'ispol_dips', 'dispol_dips'].forEach(function(cmp){
        setVisible(cmp, false);
      });
    }

    // Сформировать мотивированный отказ Испол ДИП
    if (AS.FORMS.ComponentUtils.hasRouteItem('motivir_otkaz')){
      ['step19', 'step21'].forEach(function(id){
        $('#'+id).hide();
      });
      disabledCustomComponent(['custom-mq265o', 'custom-qwy55r', 'custom-qwy55r_copy1', 'custom-qwy55r_copy2', 'custom-43iu9k']);
      ['ispol1', 'dispol1', 'ispol2', 'dispol2', 'ispol3', 'dispol3', 'ispol_dips', 'dispol_dips', 'table_20_poz'].forEach(function(cmp){
        setVisible(cmp, false);
      });
    }

    // Назначить исполнителя Руков ДРОН
    if (AS.FORMS.ComponentUtils.hasRouteItem('set_ispolnitel_dron')){
      ['step19', 'step20'].forEach(function(id){
        $('#'+id).hide();
      });
      disabledCustomComponent(['custom-mq265o', 'custom-qwy55r', 'custom-qwy55r_copy1', 'custom-qwy55r_copy2', 'custom-43iu9k']);
      ['ispol1', 'dispol1', 'ispol2', 'dispol2', 'ispol3', 'dispol3', 'ispol_dips', 'dispol_dips', 'table_20_poz', 'otvet_dron'].forEach(function(cmp){
        setVisible(cmp, false);
      });
    }

    // Рассмотреть заявление, сформировать ответ Испол ДРОН
    if (AS.FORMS.ComponentUtils.hasRouteItem('rassmotret_dron')){
      ['step19', 'step20'].forEach(function(id){
        $('#'+id).hide();
      });
      disabledCustomComponent(['custom-mq265o', 'custom-qwy55r', 'custom-qwy55r_copy1', 'custom-qwy55r_copy2', 'custom-43iu9k']);
      ['ispol1', 'dispol1', 'ispol2', 'dispol2', 'ispol3', 'dispol3', 'ispol_dips', 'dispol_dips', 'table_20_poz', 'ispol_dron'].forEach(function(cmp){
        setVisible(cmp, false);
      });
    }

    // Согласовать ответ Руков ДРОН
    if (AS.FORMS.ComponentUtils.hasRouteItem('sogl_otvet_dron')){
      ['step19', 'step20'].forEach(function(id){
        $('#'+id).hide();
      });
      disabledCustomComponent(['custom-mq265o', 'custom-qwy55r', 'custom-qwy55r_copy1', 'custom-qwy55r_copy2', 'custom-43iu9k', 'otvet_dron']);
      ['ispol1', 'dispol1', 'ispol2', 'dispol2', 'ispol3', 'dispol3', 'ispol_dips', 'dispol_dips', 'table_20_poz', 'ispol_dron'].forEach(function(cmp){
        setVisible(cmp, false);
      });
    }

    // Ознакомиться с ответом ДРОН. Принять решение о дальнейшем ходе заявки 2 Директор ДИП
    if (AS.FORMS.ComponentUtils.hasRouteItem('oznakom_reshenie_dron')){
      $('#step20').hide();
      disabledCustomComponent(['custom-mq265o', 'custom-qwy55r', 'custom-qwy55r_copy1', 'custom-qwy55r_copy2', 'custom-43iu9k', 'otvet_dron']);
      ['ispol1', 'dispol1', 'ispol2', 'dispol2', 'ispol3', 'dispol3', 'ispol_dips', 'dispol_dips', 'table_20_poz', 'ispol_dron'].forEach(function(cmp){
        setVisible(cmp, false);
      });
    }

    // Сформировать решение о одобрении Испол ДИП
    if (AS.FORMS.ComponentUtils.hasRouteItem('reshenie_odobr')){
      disabledCustomComponent(['custom-mq265o', 'custom-qwy55r', 'custom-qwy55r_copy1', 'custom-qwy55r_copy2', 'custom-43iu9k', 'otvet_dron']);
      ['ispol1', 'dispol1', 'ispol2', 'dispol2', 'ispol3', 'dispol3', 'ispol_dips', 'dispol_dips', 'ispol_dron', 'table_20_okz'].forEach(function(cmp){
        setVisible(cmp, false);
      });
    }

    // Согласовать решение по заявке Дир ДИП Зам пред. Правления
    if (AS.FORMS.ComponentUtils.hasRouteItem('sogl_odobr') || AS.FORMS.ComponentUtils.hasRouteItem('sogl_odobr2') ||
        AS.FORMS.ComponentUtils.hasRouteItem('sogl_otkaz') || AS.FORMS.ComponentUtils.hasRouteItem('sogl_otkaz2')) {

      disabledCustomComponent(['custom-mq265o', 'custom-qwy55r', 'custom-qwy55r_copy1', 'custom-qwy55r_copy2', 'custom-43iu9k', 'otvet_dron', 'pablic_otkaz', 'pablic_pozitiv']);

      ['ispol1', 'dispol1', 'ispol2', 'dispol2', 'ispol3', 'dispol3', 'ispol_dips', 'dispol_dips', 'ispol_dron'].forEach(function(cmp){
        setVisible(cmp, false);
      });
    }



  }

}, 500);
