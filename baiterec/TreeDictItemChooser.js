model.dialogWidth = model.dialogWidth || 800;
model.dialogHeight = model.dialogHeight || 400;

if(!editable) {
    var textView = jQuery('<div>', {class : "asf-label"});
    view.container.append(textView);
    view.updateValueFromModel = function(){
        if(model.getValue()) {
            textView.html(model.getValue().name);
        }
    };
} else {
    var inputView = jQuery('<div>', {class : "ns-tagContainer", style : "width:calc(100% - 30px)"});
    var button = jQuery('<button>', {class : "asf-browseButton"});
    view.container.append(inputView);
    view.container.append(button);

    view.updateValueFromModel = function(){
        if(model.getValue()) {
            inputView.html(model.getValue().name);
        }
    };

    $(button).click(function(){
        var treeChooser = new TreeChooser(window.location.origin + '/baiterek/rest/dictionaries/tree/'+model.dictionaryCode);
        treeChooser.initDialog(model);
        treeChooser.showDialog();
        treeChooser.on(AS.FORMS.BasicChooserEvent.applyClicked, function(){
            var selected = treeChooser.getSelectedValue();
            var value = { name : selected.text, id : selected.id, info : selected.value};
            model.setValue(value);
        });
    });

}
function getTreeItems (itemID, url, handler) {
    var params = {
        url: url,
        method: 'GET',
        dataType: 'json',
        success: handler
    };
    if(itemID) {
        params.data = 'parentID=' + itemID;
    }
    return jQuery.ajax(params).then(function(result) {
       return result;
    });
}


function TreeChooser (url){
    var instance = this;
    var selectedValue = null;
    var dialogContainer = jQuery('<div>');
    var treeContainer = jQuery('<div>', {
        style: "overflow: auto; width: " + (model.dialogWidth - 10) + "px; height: " + (model.dialogHeight - 100) + "px;"
    });
    var selectButton = jQuery('<button>', {class : "ns-approveButton"}).button().html(i18n.tr("Выбрать"));
    selectButton.attr("disabled", "true");
    dialogContainer.append(treeContainer);
    dialogContainer.append(selectButton);
    AS.FORMS.ComponentUtils.makeBus(this);

    this.initDialog = function(model) {
        $(treeContainer).jstree({
        'core' : {
            'data' : function (node, cb) {
              var nodeid = node.id;
              if (model.linked) {
                var linkedModel = model.playerModel.getModelWithId(model.linked);
                if (linkedModel) nodeid = linkedModel.getValue().id;
              }
                getTreeItems(nodeid, url, function(results) {
                  var filtiredResults = [];

                  if((results && results.length > 0 && results[0].text && results[0].text == 'Республика Казахстан') || !model.filtired){
                      filtiredResults = results;
                  } else {
                    results.forEach(function(ch){
                      filtiredResults.push({'id': ch.id, 'text': ch.text, 'value': ch.value, 'children': false});
                    });
                  }

                  cb.call(this, filtiredResults);
                });
            }
        }});
    };
    $(treeContainer).on("select_node.jstree", function(evt, data) {
        selectedValue = data.node.original;
        if(selectedValue.text != 'Республика Казахстан'){
            $(selectButton).prop('disabled', false);
        } else {
            $(selectButton).prop('disabled', true);
        }
    });
    this.showDialog = function() {
        dialogContainer.dialog({
            width: model.dialogWidth,
            height: model.dialogHeight,
            modal: true
        });
    };

    $(selectButton).click(function(event) {
        dialogContainer.dialog( "destroy" );
        dialogContainer.detach();
        instance.trigger(AS.FORMS.BasicChooserEvent.applyClicked);
    });

    this.getSelectedValue = function() {
        return selectedValue;
    };
}

model.getAsfData = function(blockNumber){
    if(model.getValue()) {
        var result = AS.FORMS.ASFDataUtils.getBaseAsfData(model.asfProperty, blockNumber, model.getValue().name , model.getValue().id);
        result.valueID = model.getValue().info;
        console.log(result);
        return result;
    } else {
        return AS.FORMS.ASFDataUtils.getBaseAsfData(model.asfProperty, blockNumber);
    }
};
model.setAsfData = function(asfData){
    if(!asfData || !asfData.value) {
        return;
    }
    var value = { name : asfData.value, id : asfData.key, info : asfData.valueID};
    model.setValue(value);
};

view.updateValueFromModel();
